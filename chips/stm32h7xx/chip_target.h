#ifndef CHIP_TARGET_STM32H7XX_H_
#define CHIP_TARGET_STM32H7XX_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "chip_dev_spec_features.h"
#include "chip_mmap.h"

#include "regs/regs_syscfg.h"
#include "regs/regs_dbgmcu.h"
#include "regs/regs_pwr.h"
#include "regs/regs_rcc.h"
#include "regs/regs_exmc.h"
#include "regs/regs_gpio.h"
#include "regs/regs_tim.h"
#include "regs/regs_iwdg.h" 
#include "regs/regs_wwdg.h"
#include "regs/regs_flash.h"
#include "regs/regs_hsem.h"
#include "regs/regs_exti.h"
#include "regs/regs_mdma.h"
#include "regs/regs_dma.h"
#include "regs/regs_bdma.h"
#include "regs/regs_dmamux.h"
#include "regs/regs_adc.h"
#include "regs/regs_art.h"
#include "regs/regs_usart.h"
#include "regs/regs_spi.h"
#include "regs/regs_eth.h"
#include "regs/regs_sai.h"
#include "regs/regs_mdios.h"
#include "regs/regs_gpv.h"


#include "chip_config.h"

#include "init/chip_init.h"
#include "init/chip_id.h"
#include "init/chip_pwr.h"
#include "init/chip_clk.h"
#include "init/chip_wdt.h"
#include "init/chip_rst.h"
#include "init/chip_flash.h"
#include "init/chip_per_ctl.h"
#include "init/chip_exmc.h"
#include "init/chip_hsem.h"
#include "init/chip_mdma.h"
#include "init/chip_pinint.h"

#include "chip_pins.h"



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CHIP_TARGET_STM32H7XX_H_ */
