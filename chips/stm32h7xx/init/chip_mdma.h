#ifndef CHIP_MDMA_H
#define CHIP_MDMA_H

#include "chip_init.h"

#if CHIP_MDMA_FUNC_EN
    #include "regs/regs_mdma.h"


    typedef void (*t_chip_mdma_ch_isr_func)(int ch_num, uint32_t irq_flags, CHIP_REGS_MDMA_CHANNEL_T *ch_regs);

    void chip_mdma_init(void);
    void chip_mdma_set_ch_isr(int ch_num, t_chip_mdma_ch_isr_func isr);
#endif

#endif // CHIP_MDMA_H
