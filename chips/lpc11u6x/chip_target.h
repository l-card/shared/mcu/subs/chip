/*
 * @brief LPC11U6x basic chip inclusion file
 *
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef CHIP_TARGET_LPC11U6X_H_
#define CHIP_TARGET_LPC11U6X_H_



#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "chip_devtype_spec_features.h"
#include "chip_std_defs.h"
#include "chip_mmap.h"


#include "regs/regs_syscon.h"
#include "regs/regs_gpio.h"
#include "regs/regs_gpiogroup.h" /** @todo */
#include "regs/regs_iocon.h"
#include "regs/regs_pinint.h"    /** @todo */
#include "regs/regs_uart_0.h"
#include "regs/regs_uart_n.h"
#include "regs/regs_usb.h"
#include "regs/regs_ssp.h"       /** @todo */
#include "regs/regs_timer.h"     /** @todo */
#include "regs/regs_pmu.h"       /** @todo */
#include "regs/regs_i2c.h"       /** @todo */
#include "regs/regs_wwdt.h"
#include "regs/regs_adc.h"       /** @todo */
#include "regs/regs_dma.h"       /** @todo */
#include "regs/regs_sct.h"


#include "regs/iap.h"

#include "chip_per_ids.h"
#include "init/chip_crp_defs.h"
#include "chip_config.h"
#include "init/chip_per_ctl.h"
#include "init/chip_init.h"
#include "init/chip_clk.h"
#include "chip_pins.h"


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CHIP_TARGET_LPC11U6X_H_ */
