#ifndef REGS_BPOR_H
#define REGS_BPOR_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t POR_CAUSE;                   /* 0x0: Power on cause */
    __IO uint32_t POR_SELECT;                  /* 0x4: Power on select */
    __IO uint32_t POR_CONFIG;                  /* 0x8: Power on reset config */
    __IO uint32_t POR_CONTROL;                 /* 0xC: Power down control */
} CHIP_REGS_BPOR_T;

#define CHIP_REGS_BPOR            ((CHIP_REGS_BPOR_T *)CHIP_MEMRGN_ADDR_BPOR)


/*********** Power on cause - POR_CAUSE / POR_SELECT **************************/
#define CHIP_REGFLD_BPOR_POR_CAUSE_WAKE_BTN             (0x00000001UL <<  0U) /* (RW1C/RW) Wake-up burron */
#define CHIP_REGFLD_BPOR_POR_CAUSE_SEC_VIOL             (0x00000001UL <<  1U) /* (RW1C/RW) Security violation */
#define CHIP_REGFLD_BPOR_POR_CAUSE_RTC_ALARM0           (0x00000001UL <<  2U) /* (RW1C/RW) RTC alarm 0 */
#define CHIP_REGFLD_BPOR_POR_CAUSE_RTC_ALARM1           (0x00000001UL <<  3U) /* (RW1C/RW) RTC alarm 1 */
#define CHIP_REGFLD_BPOR_POR_CAUSE_GPIO                 (0x00000001UL <<  4U) /* (RW1C/RW) GPIO */

/*********** Power on reset config  - POR_CONFIG ******************************/
#define CHIP_REGFLD_BPOR_POR_CONFIG_RETENTION           (0x00000001UL <<  0U) /* (RW) Retain battery domain register settings during shutdown */

/*********** Power down control - POR_CONTROL *********************************/
#define CHIP_REGFLD_BPOR_POR_CONTROL_COUNTER            (0x0000FFFFUL <<  0U) /* (RW) Shut down counter */


#endif // REGS_BPOR_H
