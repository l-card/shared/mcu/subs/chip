#ifndef REGS_I2C_H
#define REGS_I2C_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint16_t IADR;             /* I2C Address Register,                    Address offset: 0x00 */
    uint16_t RESERVED0;
    __IO uint16_t IFDR;             /* I2C Frequency Divider Register,          Address offset: 0x04 */
    uint16_t RESERVED1;
    __IO uint16_t I2CR;             /* I2C Control Register,                    Address offset: 0x08 */
    uint16_t RESERVED2;
    __IO uint16_t I2SR;             /* I2C Status Register,                     Address offset: 0x0C */
    uint16_t RESERVED3;
    __IO uint16_t I2DR;             /* I2C Data I/O Register,                   Address offset: 0x10 */
    uint16_t RESERVED4;
} CHIP_REGS_I2C_T;


#define CHIP_REGS_I2C1              ((CHIP_REGS_I2C_T *) CHIP_MEMRGN_ADDR_PERIPH_I2C1)
#define CHIP_REGS_I2C2              ((CHIP_REGS_I2C_T *) CHIP_MEMRGN_ADDR_PERIPH_I2C2)
#define CHIP_REGS_I2C3              ((CHIP_REGS_I2C_T *) CHIP_MEMRGN_ADDR_PERIPH_I2C3)
#define CHIP_REGS_I2C4              ((CHIP_REGS_I2C_T *) CHIP_MEMRGN_ADDR_PERIPH_I2C4)
#define CHIP_REGS_I2C5              ((CHIP_REGS_I2C_T *) CHIP_MEMRGN_ADDR_PERIPH_I2C5)
#define CHIP_REGS_I2C6              ((CHIP_REGS_I2C_T *) CHIP_MEMRGN_ADDR_PERIPH_I2C6)

/*********** I2C Address Register (I2C_IADR) **********************************/
#define CHIP_REGFLD_I2C_IADR_ADR                    (0x0000007FUL <<  1U)   /* (RW) Slave address (to respond in slave address) */

/*********** I2C Frequency Divider Register (I2C_IFDR) ************************/
#define CHIP_REGFLD_I2C_IFDR_IC                     (0x0000003FUL <<  0U)   /* (RW) I2C clock rate. Prescales the clock for bit-rate selection.  */

/*********** I2C Control Register (I2C_I2CR) **********************************/
#define CHIP_REGFLD_I2C_I2CR_RSTA                   (0x00000001UL <<  2U)   /* (WO) Repeat start */
#define CHIP_REGFLD_I2C_I2CR_TXAK                   (0x00000001UL <<  3U)   /* (RW) Transmit acknowledge enable (for receiver) */
#define CHIP_REGFLD_I2C_I2CR_MTX                    (0x00000001UL <<  4U)   /* (RW) Transmit/Receive mode select */
#define CHIP_REGFLD_I2C_I2CR_MSTA                   (0x00000001UL <<  5U)   /* (RW) Master/Slave mode select */
#define CHIP_REGFLD_I2C_I2CR_IIEN                   (0x00000001UL <<  6U)   /* (RW) I2C interrupt enable */
#define CHIP_REGFLD_I2C_I2CR_IEN                    (0x00000001UL <<  7U)   /* (RW) I2C enable. */

/*********** I2C Status Register (I2C_I2SR) ***********************************/
#define CHIP_REGFLD_I2C_I2SR_RXAK                   (0x00000001UL <<  0U)   /* (RO) Received acknowledge (0 - ACK received) */
#define CHIP_REGFLD_I2C_I2SR_IIF                    (0x00000001UL <<  1U)   /* (RW) I2C interrupt. Must be cleared by the software by writing a "0" to it in the interrupt routine. */
#define CHIP_REGFLD_I2C_I2SR_SRW                    (0x00000001UL <<  2U)   /* (RO) Slave read/write (0 - slave receive, 1 - slave transmit) */
#define CHIP_REGFLD_I2C_I2SR_IAL                    (0x00000001UL <<  4U)   /* (RW) Arbitration lost (clear by software) */
#define CHIP_REGFLD_I2C_I2SR_IBB                    (0x00000001UL <<  5U)   /* (RO) I2C bus busy bit */
#define CHIP_REGFLD_I2C_I2SR_IAAS                   (0x00000001UL <<  6U)   /* (RO) I2C addressed as a slave bit (slave address match) */
#define CHIP_REGFLD_I2C_I2SR_ICF                    (0x00000001UL <<  7U)   /* (RO) Data transferring complete */

/*********** I2C Data I/O Register (I2C_I2DR) *********************************/
#define CHIP_REGFLD_I2C_I2DR_DATA                   (0x000000FFUL <<  0U)   /* (RW) Data Byte */

#endif // REGS_I2C_H
