#ifndef CHIP_PER_CTL_H
#define CHIP_PER_CTL_H

#include "chip_clk.h"

/* Идентификаторы клоков периферийный устройств, которые могут быть разрешены
 * или запрещены при работе с помощью chip_per_clk_en() и chip_per_clk_dis()
 * соответственно, а также блок, для которого предназначен клок в большинстве
 * случаев может быть сброшен с помощью chip_per_rst() (за исключением
 * помеченных надписью clock only).
 * Для частот блоков, требующих отдельной настройки источника kernel-частоты,
 * управление может выполняться только если данный источник предварительно
 * настроек (клок разрешен изначально, либо определена настройка ***_KER_SRC_SETUP_EN).
 * Для избежания ошибочного разрешения ID клоков, для которых требуется, но
 * не настроен источник kernel-частоты, исключаются из списка */
typedef  enum {
    /* AHB3 */
    CHIP_PER_ID_MDMA = 0,
    CHIP_PER_ID_DMA2D,
#if CHIP_DEV_SUPPORT_JPGDEC
    CHIP_PER_ID_JPGDEC,
#endif
#if CHIP_CLK_EXMC_KER_SRC_EN
    CHIP_PER_ID_EXMC,
#endif
#if CHIP_CLK_OCTOSPI_KER_SRC_EN
    CHIP_PER_ID_OCTOSPI1,
    CHIP_PER_ID_OCTOSPI2,
    CHIP_PER_ID_IOMNGR,
    CHIP_PER_ID_OTFD1,
    CHIP_PER_ID_OTFD2,
#endif
#if CHIP_CLK_QSPI_KER_SRC_EN
    CHIP_PER_ID_QSPI,
#endif
#if CHIP_CLK_SDMMC_KER_SRC_EN
    CHIP_PER_ID_SDMMC1,
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4
    CHIP_PER_ID_CM4_FLASH, /* clock only */
    CHIP_PER_ID_CM4_DTCM1, /* clock only */
    CHIP_PER_ID_CM4_DTCM2, /* clock only */
    CHIP_PER_ID_CM4_ITCM,  /* clock only */
    CHIP_PER_ID_CM4_AXISRAM, /* clock only */
#endif
    /* AHB1 */
    CHIP_PER_ID_DMA1,
    CHIP_PER_ID_DMA2,
#if CHIP_CLK_ADC_KER_SRC_EN || CHIP_CLK_ADC1_2_SCLK_EN
    CHIP_PER_ID_ADC1_2,
    CHIP_PER_ID_ADC1 = CHIP_PER_ID_ADC1_2,
    CHIP_PER_ID_ADC2 = CHIP_PER_ID_ADC1_2,
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4
    CHIP_PER_ID_ART,
#endif
    CHIP_PER_ID_ETH1_MAC,
    CHIP_PER_ID_ETH1_TX, /* clock only */
    CHIP_PER_ID_ETH1_RX, /* clock only */
#if CHIP_CLK_USB_KER_SRC_EN
    CHIP_PER_ID_USB1_OTGHS,
#if CHIP_DEV_SUPPORT_USB2
    CHIP_PER_ID_USB2_OTGHS,
#endif
#endif
    CHIP_PER_ID_USB1_OTGHS_ULPI, /* clock only */
#if CHIP_DEV_SUPPORT_USB2
    CHIP_PER_ID_USB2_OTGHS_ULPI, /* clock only */
#endif
    /* AHB2 */
    CHIP_PER_ID_DVP,
#if CHIP_DEV_SUPPORT_PSSI
    CHIP_PER_ID_PSSI = CHIP_PER_ID_DVP,
#endif
    CHIP_PER_ID_CRYPT,
    CHIP_PER_ID_HASH,
#if CHIP_CLK_RNG_KER_SRC_EN
    CHIP_PER_ID_RNG,
#endif
#if CHIP_CLK_SDMMC_KER_SRC_EN
    CHIP_PER_ID_SDMMC2,
#endif
#if CHIP_DEV_SUPPORT_FMAC
    CHIP_PER_FMAC,
#endif
#if CHIP_DEV_SUPPORT_CORDIC
    CHIP_PER_CORDIC,
#endif
    CHIP_PER_ID_SRAM1, /* clock only */
    CHIP_PER_ID_SRAM2, /* clock only */
#if CHIP_DEV_SUPPORT_SRAM3
    CHIP_PER_ID_SRAM3, /* clock only */
#endif
    /* AHB4 */
    CHIP_PER_ID_GPIOA,
    CHIP_PER_ID_GPIOB,
    CHIP_PER_ID_GPIOC,
    CHIP_PER_ID_GPIOD,
    CHIP_PER_ID_GPIOE,
    CHIP_PER_ID_GPIOF,
    CHIP_PER_ID_GPIOG,
    CHIP_PER_ID_GPIOH,
#if CHIP_DEV_SUPPORT_PORTI
    CHIP_PER_ID_GPIOI,
#endif
    CHIP_PER_ID_GPIOJ,
    CHIP_PER_ID_GPIOK,
    CHIP_PER_ID_CRC,
    CHIP_PER_ID_BDMA,
#if CHIP_CLK_ADC_KER_SRC_EN || CHIP_CLK_ADC3_SCLK_EN
    CHIP_PER_ID_ADC3,
#endif
    CHIP_PER_ID_HSEM,
    CHIP_PER_ID_BKPRAM, /* clock only */
    /* APB3 */
    CHIP_PER_ID_LTDCEN,
#if CHIP_CLK_DSI_KER_SRC_EN
    CHIP_PER_ID_DSI,
#endif
    CHIP_PER_ID_WWDG1, /* clock only */
    /* APB1L */
    CHIP_PER_ID_TIM2,
    CHIP_PER_ID_TIM3,
    CHIP_PER_ID_TIM4,
    CHIP_PER_ID_TIM5,
    CHIP_PER_ID_TIM6,
    CHIP_PER_ID_TIM7,
    CHIP_PER_ID_TIM12,
    CHIP_PER_ID_TIM13,
    CHIP_PER_ID_TIM14,
#if CHIP_CLK_LPTIM1_KER_SRC_EN
    CHIP_PER_ID_LPTIM1,
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4
    CHIP_PER_ID_WWDG2, /* clock only */
#endif
#if CHIP_CLK_SPI1_2_3_KER_SRC_EN
    CHIP_PER_ID_SPI2,
    CHIP_PER_ID_SPI3,
#endif
#if CHIP_CLK_SPDIFRX_KER_SRC_EN
    CHIP_PER_ID_SPDIFRX,
#endif
#if CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
    CHIP_PER_ID_USART2,
    CHIP_PER_ID_USART3,
    CHIP_PER_ID_UART4,
    CHIP_PER_ID_UART5,
#endif
#if CHIP_CLK_I2C1_2_3_5_KER_SRC_EN
    CHIP_PER_ID_I2C1,
    CHIP_PER_ID_I2C2,
    CHIP_PER_ID_I2C3,
#if CHIP_DEV_SUPPORT_I2C5
    CHIP_PER_ID_I2C5,
#endif
#endif
#if CHIP_CLK_CEC_KER_SRC_EN
    CHIP_PER_ID_CEC,
#endif
    CHIP_PER_ID_DAC12,
#if CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
    CHIP_PER_ID_UART7,
    CHIP_PER_ID_UART8,
#endif
    /* APB1H */
    CHIP_PER_ID_CRS, /* clock recovery system */
#if CHIP_CLK_SWPMI_KER_SRC_EN
    CHIP_PER_ID_SWPMI,
#endif
    CHIP_PER_ID_OPAMP,
    CHIP_PER_ID_MDIOS,
#if CHIP_CLK_FDCAN_KER_SRC_EN
    CHIP_PER_ID_FDCAN,
#endif
#if CHIP_DEV_SUPPORT_TIM23_24
    CHIP_PER_ID_TIM23,
    CHIP_PER_ID_TIM24,
#endif
    /* APB2 */
    CHIP_PER_ID_TIM1,
    CHIP_PER_ID_TIM8,
#if CHIP_CLK_USART1_6_9_10_KER_SRC_EN
    CHIP_PER_ID_USART1,
    CHIP_PER_ID_USART6,
#if CHIP_DEV_SUPPORT_USART9_10
    CHIP_PER_ID_UART9,
    CHIP_PER_ID_USART10,
#endif
#endif
#if CHIP_CLK_SPI1_2_3_KER_SRC_EN
    CHIP_PER_ID_SPI1,
#endif
#if CHIP_CLK_SPI4_5_KER_SRC_EN
    CHIP_PER_ID_SPI4,
#endif
    CHIP_PER_ID_TIM15,
    CHIP_PER_ID_TIM16,
    CHIP_PER_ID_TIM17,
#if CHIP_CLK_SPI4_5_KER_SRC_EN
    CHIP_PER_ID_SPI5,
#endif
#if CHIP_CLK_SAI1_KER_SRC_EN
    CHIP_PER_ID_SAI1,
#endif
#if CHIP_CLK_SAI2_3_KER_SRC_EN
    CHIP_PER_ID_SAI2,
    CHIP_PER_ID_SAI3,
#endif
#if CHIP_CLK_DFSDM1_KER_SRC_EN
    CHIP_PER_ID_DFSDM1,
#endif
#if CHIP_DEV_SUPPORT_HRTIM
    CHIP_PER_ID_HRTIM,
#endif
    /* APB4 */
    CHIP_PER_ID_SYSCFG,
#if CHIP_CLK_LPUART1_KER_SRC_EN
    CHIP_PER_ID_LPUART1,
#endif
#if CHIP_CLK_SPI6_KER_SRC_EN
    CHIP_PER_ID_SPI6,
#endif
#if CHIP_CLK_I2C4_KER_SRC_EN
    CHIP_PER_ID_I2C4,
#endif
#if CHIP_CLK_LPTIM2_KER_SRC_EN
    CHIP_PER_ID_LPTIM2,
#endif
#if CHIP_CLK_LPTIM3_4_5_KER_SRC_EN
    CHIP_PER_ID_LPTIM3,
    CHIP_PER_ID_LPTIM4,
    CHIP_PER_ID_LPTIM5,
#endif
    CHIP_PER_ID_COMP12,
    CHIP_PER_ID_VREF,
    CHIP_PER_ID_RTC_APB, /* clock only */
#if CHIP_CLK_SAI4_KER_SRC_EN
    CHIP_PER_ID_SAI4,
#endif
#if CHIP_DEV_SUPPORT_DTS
    CHIP_PER_ID_DTS,
#endif
}  t_chip_per_id;

void chip_per_rst(t_chip_per_id per_id);
void chip_per_clk_en(t_chip_per_id per_id);
void chip_per_clk_dis(t_chip_per_id per_id);
int  chip_per_clk_is_en(t_chip_per_id per_id);



#endif // CHIP_PER_CTL_H
