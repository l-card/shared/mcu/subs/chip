#ifndef CHIP_STM32F0XX_REGS_USB_H
#define CHIP_STM32F0XX_REGS_USB_H

#include <stdint.h>

#define CHIP_USB_EPREGS_CNT 8

typedef struct {
    __IO uint16_t EPR;            /*!< USB Endpoint register,                 Address offset: 0x00 */
    __IO uint16_t RESERVED;       /*!< Reserved */
} CHIP_REGS_USB_EP_T;

typedef struct {
    CHIP_REGS_USB_EP_T EP[CHIP_USB_EPREGS_CNT];
  __IO uint16_t RESERVED7[16];   /*!< Reserved */
  __IO uint16_t CNTR;            /*!< Control register,                       Address offset: 0x40 */
  __IO uint16_t RESERVED8;       /*!< Reserved */
  __IO uint16_t ISTR;            /*!< Interrupt status register,              Address offset: 0x44 */
  __IO uint16_t RESERVED9;       /*!< Reserved */
  __IO uint16_t FNR;             /*!< Frame number register,                  Address offset: 0x48 */
  __IO uint16_t RESERVEDA;       /*!< Reserved */
  __IO uint16_t DADDR;           /*!< Device address register,                Address offset: 0x4C */
  __IO uint16_t RESERVEDB;       /*!< Reserved */
  __IO uint16_t BTABLE;          /*!< Buffer Table address register,          Address offset: 0x50 */
  __IO uint16_t RESERVEDC;       /*!< Reserved */
  __IO uint16_t LPMCSR;          /*!< LPM Control and Status register,        Address offset: 0x54 */
  __IO uint16_t RESERVEDD;       /*!< Reserved */
  __IO uint16_t BCDR;            /*!< Battery Charging detector register,     Address offset: 0x58 */
  __IO uint16_t RESERVEDE;       /*!< Reserved */
} CHIP_REGS_USB_T;


typedef struct {
    __IO uint16_t ADDR;  /* buffer address n */
    __IO uint16_t COUNT; /* byte count n */
} CHIP_REGS_USB_BUF_DESCR;

typedef struct {
    union {
        struct {
            CHIP_REGS_USB_BUF_DESCR TX;
            CHIP_REGS_USB_BUF_DESCR RX;
        } SB; /* single buffered endpoint */
        CHIP_REGS_USB_BUF_DESCR DB_TX[2]; /* double buffered tx endpoint */
        CHIP_REGS_USB_BUF_DESCR DB_RX[2]; /* double buffered rx endpoint */
    };
} CHIP_REGS_USB_BTABLE_REC_T;


#define CHIP_REGS_USB          ((CHIP_REGS_USB_T *) CHIP_MEMRGN_ADDR_PERIPH_USB)

/*********** USB control register (USB_CNTR) **********************************/
#define CHIP_REGFLD_USB_CNTR_FRES               (0x00000001UL <<  0U)   /* (RW) Force USB Reset */
#define CHIP_REGFLD_USB_CNTR_PDWN               (0x00000001UL <<  1U)   /* (RW) Power down */
#define CHIP_REGFLD_USB_CNTR_LP_MODE            (0x00000001UL <<  2U)   /* (RW1SC) Low-power mode */
#define CHIP_REGFLD_USB_CNTR_FSUSP              (0x00000001UL <<  3U)   /* (RW) Force suspend */
#define CHIP_REGFLD_USB_CNTR_RESUME             (0x00000001UL <<  4U)   /* (RW) Resume request */
#define CHIP_REGFLD_USB_CNTR_L1_RESUME          (0x00000001UL <<  5U)   /* (RW1SC) LPM L1 Resume request */
#define CHIP_REGFLD_USB_CNTR_L1_REQ_M           (0x00000001UL <<  7U)   /* (RW) LPM L1 state request interrupt mask */
#define CHIP_REGFLD_USB_CNTR_ESOF_M             (0x00000001UL <<  8U)   /* (RW) Expected start of frame interrupt mask */
#define CHIP_REGFLD_USB_CNTR_SOF_M              (0x00000001UL <<  9U)   /* (RW) Start of frame interrupt mask */
#define CHIP_REGFLD_USB_CNTR_RESET_M            (0x00000001UL << 10U)   /* (RW) Start of frame interrupt mask */
#define CHIP_REGFLD_USB_CNTR_SUSP_M             (0x00000001UL << 11U)   /* (RW) Suspend mode interrupt mask */
#define CHIP_REGFLD_USB_CNTR_WKUP_M             (0x00000001UL << 12U)   /* (RW) Wakeup interrupt mask */
#define CHIP_REGFLD_USB_CNTR_ERR_M              (0x00000001UL << 13U)   /* (RW) Error interrupt mask */
#define CHIP_REGFLD_USB_CNTR_PMAOVR_M           (0x00000001UL << 14U)   /* (RW) Packet memory area over / underrun interrupt mask */
#define CHIP_REGFLD_USB_CNTR_CTR_M              (0x00000001UL << 15U)   /* (RW) Correct transfer interrupt mask */
#define CHIP_REGMSK_USB_CNTR_RESERVED           (0x0040UL)

/*********** USB interrupt status register (USB_ISTR) *************************/
#define CHIP_REGFLD_USB_ISTR_EP_ID              (0x0000000FUL <<  0U)   /* (RO) Endpoint Identifier */
#define CHIP_REGFLD_USB_ISTR_DIR                (0x00000001UL <<  4U)   /* (RO) Direction of transaction */
#define CHIP_REGFLD_USB_ISTR_L1_REQ             (0x00000001UL <<  7U)   /* (RW0) LPM L1 state request */
#define CHIP_REGFLD_USB_ISTR_ESOF               (0x00000001UL <<  8U)   /* (RW0) Expected start of frame */
#define CHIP_REGFLD_USB_ISTR_SOF                (0x00000001UL <<  9U)   /* (RW0) Start of frame */
#define CHIP_REGFLD_USB_ISTR_RESET              (0x00000001UL << 10U)   /* (RW0) USB reset request */
#define CHIP_REGFLD_USB_ISTR_SUSP               (0x00000001UL << 11U)   /* (RW0) Suspend mode request */
#define CHIP_REGFLD_USB_ISTR_WKUP               (0x00000001UL << 12U)   /* (RW0) Wakeup */
#define CHIP_REGFLD_USB_ISTR_ERR                (0x00000001UL << 13U)   /* (RW0) Error (No Answer, CRC, Bit Stuffing, Framing) */
#define CHIP_REGFLD_USB_ISTR_PMAOVER            (0x00000001UL << 14U)   /* (RW0) Packet memory area over / underrun */
#define CHIP_REGFLD_USB_ISTR_CTR                (0x00000001UL << 15U)   /* (RO) Correct transfer */
#define CHIP_REGMSK_USB_ISTR_RESERVED           (0x0060UL)

#define CHIP_REGFLDVAL_USB_ISTR_DIR_TX          0
#define CHIP_REGFLDVAL_USB_ISTR_DIR_RX          1

/*********** USB frame number register (USB_FNR) ******************************/
#define CHIP_REGFLD_USB_FNR_FN                  (0x000007FFUL <<  0U)   /* (RO) Frame number */
#define CHIP_REGFLD_USB_FNR_LSOF                (0x00000003UL << 11U)   /* (RO) Lost SOF count */
#define CHIP_REGFLD_USB_FNR_LCK                 (0x00000001UL << 13U)   /* (RO) Locked (2 SOF received) */
#define CHIP_REGFLD_USB_FNR_RXDM                (0x00000001UL << 14U)   /* (RO) Receive data- line status */
#define CHIP_REGFLD_USB_FNR_RXDP                (0x00000001UL << 15U)   /* (RO) Receive data+ line status */

/*********** USB device address (USB_DADDR) ***********************************/
#define CHIP_REGFLD_USB_DADDR_ADD               (0x0000007FUL <<  0U)   /* (RW) Device address */
#define CHIP_REGFLD_USB_DADDR_EF                (0x00000001UL <<  7U)   /* (RW) Enable function */
#define CHIP_REGMSK_USB_DADDR_RESERVED          (0xFF00UL)

/*********** LPM control and status register (USB_LPMCSR) *********************/
#define CHIP_REGFLD_USB_LPMCSR_LPM_EN           (0x00000001UL <<  0U)   /* (RW) LPM support enable */
#define CHIP_REGFLD_USB_LPMCSR_LPM_ACK          (0x00000001UL <<  1U)   /* (RW) LPM Token acknowledge enable */
#define CHIP_REGFLD_USB_LPMCSR_REM_WAKE         (0x00000001UL <<  3U)   /* (RO) bRemoteWake value (from last ACKed LPM Token) */
#define CHIP_REGFLD_USB_LPMCSR_BESL             (0x0000000FUL <<  4U)   /* (RO) BESL value (from last ACKed LPM Token) */
#define CHIP_REGMSK_USB_LPMCSR_RESERVED         (0xFF04UL)

/*********** Battery charging detector (USB_BCDR) *****************************/
#define CHIP_REGFLD_USB_BCDR_BCD_EN             (0x00000001UL <<  0U)   /* (RW) Battery charging detector (BCD) enable */
#define CHIP_REGFLD_USB_BCDR_DCD_EN             (0x00000001UL <<  1U)   /* (RW) Data contact detection (DCD) mode enable */
#define CHIP_REGFLD_USB_BCDR_PD_EN              (0x00000001UL <<  2U)   /* (RW) Primary detection (PD) mode enable */
#define CHIP_REGFLD_USB_BCDR_SD_EN              (0x00000001UL <<  3U)   /* (RW) Secondary detection (SD) mode enable */
#define CHIP_REGFLD_USB_BCDR_DC_DET             (0x00000001UL <<  4U)   /* (RO) Data contact detection (DCD) status */
#define CHIP_REGFLD_USB_BCDR_PDET               (0x00000001UL <<  5U)   /* (RO) Primary detection (PD) status */
#define CHIP_REGFLD_USB_BCDR_SDET               (0x00000001UL <<  6U)   /* (RO) Secondary detection (SD) status */
#define CHIP_REGFLD_USB_BCDR_PS2_DET            (0x00000001UL <<  7U)   /* (RO) DM pull-up detection status */
#define CHIP_REGFLD_USB_BCDR_DPPU               (0x00000001UL << 15U)   /* (RW) DP pull-up control */
#define CHIP_REGMSK_USB_BCDR_RESERVED           (0x7F00UL)

#define CHIP_REGFLDVAL_USB_BCDR_SDET_CDP        0
#define CHIP_REGFLDVAL_USB_BCDR_SDET_DCP        1

/*********** USB endpoint n register (USB_EPnR), n=[0..7] *********************/
#define CHIP_REGFLD_USB_EPR_EA                  (0x0000000FUL <<  0U)   /* (RW)  Endpoint address */
#define CHIP_REGFLD_USB_EPR_STAT_TX             (0x00000003UL <<  4U)   /* (W1T) Status bits, for transmission transfers */
#define CHIP_REGFLD_USB_EPR_DTOG_TX             (0x00000001UL <<  6U)   /* (W1T) Data Toggle, for transmission transfers*/
#define CHIP_REGFLD_USB_EPR_CTR_TX              (0x00000001UL <<  7U)   /* (RW0) Correct Transfer for transmission */
#define CHIP_REGFLD_USB_EPR_EP_KIND             (0x00000001UL <<  8U)   /* (RW)  Endpoint kind */
#define CHIP_REGFLD_USB_EPR_EP_TYPE             (0x00000003UL <<  9U)   /* (RW)  Endpoint type */
#define CHIP_REGFLD_USB_EPR_SETUP               (0x00000001UL << 11U)   /* (RO)  Setup transaction completed */
#define CHIP_REGFLD_USB_EPR_STAT_RX             (0x00000003UL << 12U)   /* (W1T) Status bits, for reception transfers */
#define CHIP_REGFLD_USB_EPR_DTOG_RX             (0x00000001UL << 14U)   /* (W1T) Data Toggle, for reception transfers */
#define CHIP_REGFLD_USB_EPR_CTR_RX              (0x00000001UL << 15U)   /* (RW0) Correct Transfer for reception */

#define CHIP_REGFLDVAL_USB_EPR_STAT_DISABLED    0
#define CHIP_REGFLDVAL_USB_EPR_STAT_STALL       1
#define CHIP_REGFLDVAL_USB_EPR_STAT_NACK        2
#define CHIP_REGFLDVAL_USB_EPR_STAT_VALID       3

#define CHIP_REGFLDVAL_USB_EPR_EP_TYPE_BULK     0
#define CHIP_REGFLDVAL_USB_EPR_EP_TYPE_CONTROL  1
#define CHIP_REGFLDVAL_USB_EPR_EP_TYPE_ISO      2
#define CHIP_REGFLDVAL_USB_EPR_EP_TYPE_INT      3

#define CHIP_REGFLDVAL_USB_EPR_EP_KIND_DBL_BUF     1 /* double-buffering feature for bulk */
#define CHIP_REGFLDVAL_USB_EPR_EP_KIND_STATUS_OUT  1 /* status out feature for control */


/*********** Reception byte count n (USB_COUNTn_RX) *********************/
#define CHIP_REGFLD_USB_COUNT_RX_COUNT          (0x000003FFUL <<  0U)   /* (RO) Reception byte count */
#define CHIP_REGFLD_USB_COUNT_RX_NUM_BLOCK      (0x0000001FUL << 10U)   /* (RW) Number of blocks */
#define CHIP_REGFLD_USB_COUNT_RX_BLSIZE         (0x00000001UL << 15U)   /* (RW) Block size */

#define CHIP_REGFLDVAL_USB_COUNT_RX_BLSIZE_2B   0
#define CHIP_REGFLDVAL_USB_COUNT_RX_BLSIZE_32B  1

#endif // CHIP_STM32F0XX_REGS_USB_H
