#ifndef REGS_PLLCCTL_H
#define REGS_PLLCCTL_H

#include "chip_ioreg_defs.h"

#define CHIP_DEV_PLL_CNT                    5

typedef struct {
    __IO uint32_t CFG0;                    /* 0x80: PLLx config0 */
    __IO uint32_t CFG1;                    /* 0x84: PLLx config1 */
    __IO uint32_t CFG2;                    /* 0x88: PLLx config2 */
    __IO uint32_t FREQ;                    /* 0x8C: PLLx frac mode frequency adjust */
    __IO uint32_t LOCK;                    /* 0x90: PLLx lock control */
    __I  uint8_t  RESERVED0[12];           /* 0x94 - 0x9F: Reserved */
    __I  uint32_t STATUS;                  /* 0xA0: PLLx status */
    __I  uint8_t  RESERVED1[28];           /* 0xA4 - 0xBF: Reserved */
    __IO uint32_t DIV0;                    /* 0xC0: PLLx divider0 control */
    __IO uint32_t DIV1;                    /* 0xC4: PLLx divider1 control */
    __I  uint8_t  RESERVED2[56];           /* 0xC8 - 0xFF: Reserved */
} CHIP_REGS_PLL_T;

typedef struct {
    __IO uint32_t XTAL;                        /* 0x0: Crystal control and status */
    __I  uint8_t  RESERVED0[124];              /* 0x4 - 0x7F: Reserved */
    CHIP_REGS_PLL_T PLL[CHIP_DEV_PLL_CNT];
} CHIP_REGS_PLLCTL_T;

#define CHIP_REGS_PLLCTL            ((CHIP_REGS_PLLCTL_T *)CHIP_MEMRGN_ADDR_PLLCTL)

/*********** XTAL - Crystal control and status ********************************/
#define CHIP_REGFLD_PLLCTL_XTAL_RESPONSE                (0x00000001UL << 29U) /* (RO) Oscillator status */
#define CHIP_REGFLD_PLLCTL_XTAL_ENABLE                  (0x00000001UL << 28U) /* (RO) Oscillator enable status */
#define CHIP_REGFLD_PLLCTL_XTAL_RAMP_TIME               (0x000FFFFFUL <<  0U) /* (RW) Oscillator start-up time (in IRC24M clock cycles) */
#define CHIP_REGMSK_PLLCTL_XTAL_RESERVED                (0xCFF00000UL)

/*********** PLL_CFG0 - PLLx config0 ******************************************/
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_SS_RSTPTR           (0x00000001UL << 31U) /* (RW) Reset spread spectrum counter */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_REFDIV              (0x0000003FUL << 24U) /* (RW) Reference clock division ratio */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_POSTDIV1            (0x00000007UL << 20U) /* (RW) Post-division ratio */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_SS_SPREAD           (0x0000001FUL << 14U) /* (RW) Spread spectrum depth */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_SS_DIVVAL           (0x0000003FUL <<  8U) /* (RW) Spread spectrum frequency division ratio */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_SS_DOWNSPREAD       (0x00000001UL <<  7U) /* (RW) Spread spectrum mode control (0 - center, 1 - ssdm) */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_SS_RESET            (0x00000001UL <<  6U) /* (RW) Write 1 to reset spread spectrum logic */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_SS_DISABLE_SSCG     (0x00000001UL <<  5U) /* (RW) Set 1 to stop spreading, output equals input */
#define CHIP_REGFLD_PLLCTL_PLL_CFG0_DSMPD               (0x00000001UL <<  3U) /* (RW) PLL mode (0 - fractional, 1 - integer) */
#define CHIP_REGMSK_PLLCTL_PLL_CFG0_RESERVED            (0x40880017UL)


/*********** PLL_CFG1 - PLLx config1 ******************************************/
#define CHIP_REGFLD_PLLCTL_PLL_CFG1_PLLCTRL_HW_EN       (0x00000001UL << 31U) /* (RW) PLL automatic hardware control mode */
#define CHIP_REGFLD_PLLCTL_PLL_CFG1_CLKEN_SW            (0x00000001UL << 26U) /* (RW) PLL software control output enable  */
#define CHIP_REGFLD_PLLCTL_PLL_CFG1_PLLPD_SW            (0x00000001UL << 25U) /* (RW) PLL software control power down */
#define CHIP_REGFLD_PLLCTL_PLL_CFG1_LOCK_CNT_CFG        (0x00000001UL << 15U) /* (RW) PLL lock time control, measured in reference clock cycles */
#define CHIP_REGMSK_PLLCTL_PLL_CFG1_RESERVED            (0x79FF7FFFUL)

/*********** PLL_CFG2 - PLLx config2 ******************************************/
#define CHIP_REGFLD_PLLCTL_PLL_CFG2_FBDIV_INT           (0x00000FFFUL <<  0U) /* (RW) Integer mode division ratio */
#define CHIP_REGMSK_PLLCTL_PLL_CFG2_RESERVED            (0xFFFFF000UL)

/*********** PLL_FREQ - PLLx frac mode frequency adjust ***********************/
#define CHIP_REGFLD_PLLCTL_PLL_FREQ_FRAC                (0x00FFFFFFUL <<  8U) /* (RW) Fractional mode fractional division ratio */
#define CHIP_REGFLD_PLLCTL_PLL_FREQ_FBDIV_FRAC          (0x000000FFUL <<  0U) /* (RW) Fractional mode integer division ratio */


/*********** PLL_LOCK - PLLx lock control *************************************/
#define CHIP_REGFLD_PLLCTL_PLL_LOCK_SS_RSTPTR           (0x00000001UL << 31U) /* (RW) SS_RSTPTR lock */
#define CHIP_REGFLD_PLLCTL_PLL_LOCK_REFDIV              (0x00000001UL << 24U) /* (RW) REFDIV lock */
#define CHIP_REGFLD_PLLCTL_PLL_LOCK_POSTDIV1            (0x00000001UL << 20U) /* (RW) POSTDIV1 lock */
#define CHIP_REGFLD_PLLCTL_PLL_LOCK_SS_SPEAD            (0x00000001UL << 14U) /* (RW) SS_SPEAD lock */
#define CHIP_REGFLD_PLLCTL_PLL_LOCK_SS_DIVVAL           (0x00000001UL <<  8U) /* (RW) SS_DIVVAL lock */
#define CHIP_REGMSK_PLLCTL_PLL_LOCK_RESERVED            (0x7EEFBEFFUL)

/*********** PLL_STATUS - PLLx status *****************************************/
#define CHIP_REGFLD_PLLCTL_PLL_STATUS_ENABLE            (0x00000001UL << 27U) /* (RO) PLL enable bit from SYSCTL */
#define CHIP_REGFLD_PLLCTL_PLL_STATUS_RESPONSE          (0x00000001UL <<  2U) /* (RO) Response bit output to SYSCTL */
#define CHIP_REGFLD_PLLCTL_PLL_STATUS_PLL_LOCK_COMB     (0x00000001UL <<  1U) /* (RO) PLL lock status */
#define CHIP_REGFLD_PLLCTL_PLL_STATUS_PLL_LOCK_SYNC     (0x00000001UL <<  0U) /* (RO) PLL lock output */


/*********** PLL_DIVx - PLLx dividerx control *********************************/
#define CHIP_REGFLD_PLLCTL_PLL_DIV_BUSY                 (0x00000001UL << 31U) /* (RO) Busy status bit (changing state) */
#define CHIP_REGFLD_PLLCTL_PLL_DIV_RESPONSE             (0x00000001UL << 29U) /* (RO) Divider response status */
#define CHIP_REGFLD_PLLCTL_PLL_DIV_ENABLE               (0x00000001UL << 28U) /* (RO) Frequency divider enable bit */
#define CHIP_REGFLD_PLLCTL_PLL_DIV_DIV                  (0x000000FFUL <<  0U) /* (RO) Frequency division ratio */

#endif // REGS_PLLCCTL_H
