/*================================================================================================*
 * Код начальной инициализации LPC43xx
 *================================================================================================*/
#include "chip_cortexm_isr.h"
#include "chip_coretype_spec_features.h"
#include "chip.h"
#include <stddef.h>


/*================================================================================================*/


#ifndef CHIP_CFG_STACK_TRACK_UNUSED_WORDS
    #define CHIP_STACK_TRACK_UNUSED_WORDS 0
#else
    #define CHIP_STACK_TRACK_UNUSED_WORDS CHIP_CFG_STACK_TRACK_UNUSED_WORDS
#endif

#ifndef CHIP_STACK_FILLER
    #define CHIP_STACK_FILLER        0xDEADBEEF
#endif

/*================================================================================================*/

/*================================================================================================*/
/* Объявление символов, на которые ссылается таблица векторов */
/*================================================================================================*/

extern int main(void);
extern void chip_init(void);
/*================================================================================================*/

/*================================================================================================*/
/* Символы, определенные линкером (в .ld) */
/*================================================================================================*/
extern uint32_t __data_init_tab_start, __data_init_tab_end;
extern uint32_t __bss_init_tab_start, __bss_init_tab_end;
/*================================================================================================*/

/*================================================================================================*/
/* Стек */
__attribute__ ((section(".stackarea"))) CHIP_STACK_TYPE g_cpu_stack[CHIP_STACK_WORDS_LEN];

static void chip_dummy_func(void) {

}

static void chip_dummy_init_fault(int code) {

}

void chip_main_prestart(void) __attribute__ ((__weak__, __alias__("chip_dummy_func")));
void chip_user_init_prestart(void) __attribute__ ((__weak__, __alias__("chip_dummy_func")));
void chip_user_init_fault(int code) __attribute__ ((__weak__, __alias__("chip_dummy_init_fault")));

/*------------------------------------------------------------------------------------------------*/
void Reset_Handler(void) {
    register const uint32_t* ptab;

#if CHIP_STACK_TRACK_UNUSED_WORDS
    /* Заполнить стек значением STACK_FILLER (для вычисления используемой глубины стека) */
    asm volatile (
        " .syntax unified\n"
        "  mov r0, sp \n"
        "0:"
        "  subs r0, #4 \n"
        "  str %[data], [r0] \n"
        "  cmp r0, %[stack_start] \n"
        "  bhi 0b"
        :
        : [stack_start] "r" (g_cpu_stack), [data] "r" (CHIP_STACK_FILLER)
        : "r0", "cc", "memory"
    );
#endif

#if CHIP_CORETYPE_SUPPORT_VTOR
    /* Инициализация стека и VTOR по таблице векторов.
       В общем случае может быть не инициализирована, если программа запускается
       из встроенного загрузчика (например системный загрузчик stm32) */
    asm volatile ("msr MSP, %0 \n\t"
                  : : "r" (g_isr_table[0]) : "r1");
    SCB->VTOR = (uint32_t)(&g_isr_table[0]);
#endif
    /* возможность пользовательской обработки перед инициализацией */
    chip_user_init_prestart();
    /* Вызвать инициализацию аппаратуры (предже всего PLL) */
    chip_init();



    /* Копирование инициализированных данных */
    /* Формат записи списка секций: { src, dest, len } */
    for (ptab = &__data_init_tab_start; ptab < &__data_init_tab_end; ) {
        register const uint32_t *ps = (const uint32_t*)*ptab++;
        register uint32_t* pd = (uint32_t*)*ptab++;
        register uint32_t* pend = (uint32_t*)((uint32_t)pd + *ptab++);
        while (pd < pend) {
            *pd++ = *ps++;
        }
    }

    /* Обнуление bss */
    /* Формат записи списка секций: { dest, len } */
    for (ptab = &__bss_init_tab_start; ptab < &__bss_init_tab_end; ) {
        register uint32_t* pd = (uint32_t*)*ptab++;
        register uint32_t* pend = (uint32_t*)((uint32_t)pd + *ptab++);
        while (pd < pend) {
            *pd++ = 0;
        }
    }

    /* инициализация после установки данных и bss, непосредственно перед main */
    chip_main_prestart();
    /* Вызов основной программы */
    main();

    for (;;);
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* Оценка максимальной глубины использованного стека */
unsigned int chip_stack_get_unused_words (void) {
    unsigned int index = 0;
#if CHIP_STACK_TRACK_UNUSED_WORDS
    /* Считаем, что если слово в стеке содержит начальное значение CHIP_STACK_FILLER, то оно, вероятно,
       никогда не использовалось */
    while ((index < CHIP_STACK_WORDS_LEN) && (CHIP_STACK_FILLER == g_cpu_stack[index])) {
        ++index;
    }
#endif
    return index;
}
/*------------------------------------------------------------------------------------------------*/

__attribute__ ((__noinline__)) void chip_delay_clk(unsigned int clk_count) {
#define CYCLES_OVERHEAD     7
    clk_count = clk_count > CYCLES_OVERHEAD ? clk_count - CYCLES_OVERHEAD : 1;

    /* Задержка примерно на clk_count тактов, min = 11 тактов */
    asm volatile (
         " .syntax unified\n"
         "  asrs %[reg], %[reg], #2\n"  /* 4 cycles per loop */
         "0:"
         "  subs %[reg], %[reg], #0\n"
         "  subs %[reg], %[reg], #1\n"
         "  bgt 0b\n"
         " .syntax divided"
         : [reg] "+r" (clk_count)
         :
         : "cc"
        );
#undef CYCLES_OVERHEAD
}


/*================================================================================================*/
