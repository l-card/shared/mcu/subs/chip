#include "chip.h"
#include "chip_clk_constraints.h"
#include "chip_init_faults.h"
#if CHIP_LTIMER_EN
    #include "lclock.h"
#endif

void chip_clk_init(void) {


#if CHIP_LTIMER_EN
    /* разрешение ltimer */
    lclock_init();
#endif
}
