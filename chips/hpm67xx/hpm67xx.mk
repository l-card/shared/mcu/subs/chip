CHIP_TARGET_ARCHTYPE := riscv
CHIP_TARGET_CORETYPE := andes_d45


CHIP_TARGET_DEVTYPE_DIR := $(CHIP_TARGET_DIR)/devs/$(CHIP_TARGET_DEVTYPE)
CHIP_TARGET_DEV_DIR := $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEV)

ifeq (,$(CHIP_TARGET_DEVTYPE))
    $(error CHIP_TARGET_DEV variable must be set to target cpu device)
else ifeq (,$(wildcard $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEVTYPE).mk))
    $(error $(CHIP_TARGET_DEVTYPE) is not supported device type)
else
    include $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEVTYPE).mk
endif
CHIP_INC_DIRS += $(CHIP_TARGET_DEVTYPE_DIR) $(CHIP_TARGET_DEV_DIR) $(CHIP_SHARED_DIR)/stm32



LPRINTF_TARGET := hpm_uart

#CHIP_GEN_SRC += $(CHIP_TARGET_DIR)/init/chip_clk.c \
                $(CHIP_TARGET_DIR)/init/chip_per_ctl.c


