#ifndef CHIP_STM32H7XX_REGS_SYSCFG_H_
#define CHIP_STM32H7XX_REGS_SYSCFG_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

/* System configuration controller */
typedef struct {
    uint32_t RESERVED1;           /* Reserved,                                           Address offset: 0x00        */
    __IO uint32_t PMCR;           /* SYSCFG peripheral mode configuration register,      Address offset: 0x04        */
    __IO uint32_t EXTICR[4];      /* SYSCFG external interrupt configuration registers,  Address offset: 0x08-0x14   */
    __IO uint32_t CFGR;           /* SYSCFG configuration registers,                     Address offset: 0x18        */
    uint32_t RESERVED2;           /* Reserved,                                           Address offset: 0x1C        */
    __IO uint32_t CCCSR;          /* SYSCFG compensation cell control/status register,   Address offset: 0x20        */
    __IO uint32_t CCVR;           /* SYSCFG compensation cell value register,            Address offset: 0x24        */
    __IO uint32_t CCCR;           /* SYSCFG compensation cell code register,             Address offset: 0x28        */
    __IO uint32_t PWRCR;          /* PWR control register,                               Address offset: 0x2C        */
#if CHIP_DEV_SUPPORT_ADC2_ALTCON
    __IO uint32_t ADC2ALT;        /* ADC2 internal input alternate connection register,    Address offset: 0x30        */
#else
    uint32_t     RESERVED3_1;
#endif
    uint32_t     RESERVED3_2[60]; /* Reserved, 0x34-0x120                                                            */
    __IO uint32_t PKGR;           /* SYSCFG package register,                            Address offset: 0x124       */
    uint32_t     RESERVED4[118];  /* Reserved, 0x128-0x2FC                                                           */
    __IO uint32_t UR0;            /* SYSCFG user register 0,                             Address offset: 0x300       */
    __IO uint32_t UR1;            /* SYSCFG user register 1,                             Address offset: 0x304       */
    __IO uint32_t UR2;            /* SYSCFG user register 2,                             Address offset: 0x308       */
    __IO uint32_t UR3;            /* SYSCFG user register 3,                             Address offset: 0x30C       */
    __IO uint32_t UR4;            /* SYSCFG user register 4,                             Address offset: 0x310       */
    __IO uint32_t UR5;            /* SYSCFG user register 5,                             Address offset: 0x314       */
    __IO uint32_t UR6;            /* SYSCFG user register 6,                             Address offset: 0x318       */
    __IO uint32_t UR7;            /* SYSCFG user register 7,                             Address offset: 0x31C       */
    __IO uint32_t UR8;            /* SYSCFG user register 8,                             Address offset: 0x320       */
    __IO uint32_t UR9;            /* SYSCFG user register 9,                             Address offset: 0x324       */
    __IO uint32_t UR10;           /* SYSCFG user register 10,                            Address offset: 0x328       */
    __IO uint32_t UR11;           /* SYSCFG user register 11,                            Address offset: 0x32C       */
    __IO uint32_t UR12;           /* SYSCFG user register 12,                            Address offset: 0x330       */
    __IO uint32_t UR13;           /* SYSCFG user register 13,                            Address offset: 0x334       */
    __IO uint32_t UR14;           /* SYSCFG user register 14,                            Address offset: 0x338       */
    __IO uint32_t UR15;           /* SYSCFG user register 15,                            Address offset: 0x33C       */
    __IO uint32_t UR16;           /* SYSCFG user register 16,                            Address offset: 0x340       */
    __IO uint32_t UR17;           /* SYSCFG user register 17,                            Address offset: 0x344       */
    __IO uint32_t UR18;           /* SYSCFG user register 18,                            Address offset: 0x348       */
} CHIP_REGS_SYSCFG_T;

#define CHIP_REGS_SYSCFG          ((CHIP_REGS_SYSCFG_T *) CHIP_MEMRGN_ADDR_PERIPH_SYSCFG)




/*********** SYSCFG peripheral mode configuration register (SYSCFG_PMCR) ******/
#define CHIP_REGFLD_SYSCFG_PMCR_I2C1_FMP            (0x00000001UL <<  0U) /* I2C1 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_I2C2_FMP            (0x00000001UL <<  1U) /* I2C2 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_I2C3_FMP            (0x00000001UL <<  2U) /* I2C3 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_I2C4_FMP            (0x00000001UL <<  3U) /* I2C4 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_I2C_PB6_FMP         (0x00000001UL <<  4U) /* I2C PB6 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_I2C_PB7_FMP         (0x00000001UL <<  5U) /* I2C PB7 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_I2C_PB8_FMP         (0x00000001UL <<  6U) /* I2C PB8 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_I2C_PB9_FMP         (0x00000001UL <<  7U) /* I2C PB9 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_PMCR_BOOSTEN             (0x00000001UL <<  8U) /* I/O analog switch voltage booster enable */
#define CHIP_REGFLD_SYSCFG_PMCR_BOOSTVDDSEL         (0x00000001UL <<  9U) /* Analog switch supply source selection : VDD/VDDA */
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_REGFLD_SYSCFG_PMCR_I2C5_FMP            (0x00000001UL << 10U) /* I2C5 Fast mode plus */
#endif
#define CHIP_REGFLD_SYSCFG_PMCR_EPIS                (0x00000007UL << 21U) /* Ethernet PHY Interface Selection */
#define CHIP_REGFLD_SYSCFG_PMCR_PA0SO               (0x00000001UL << 24U) /* PA0 Switch Open */
#define CHIP_REGFLD_SYSCFG_PMCR_PA1SO               (0x00000001UL << 25U) /* PA1 Switch Open */
#define CHIP_REGFLD_SYSCFG_PMCR_PC2SO               (0x00000001UL << 26U) /* PC2 Switch Open */
#define CHIP_REGFLD_SYSCFG_PMCR_PC3SO               (0x00000001UL << 27U) /* PC3 Switch Open */

#define CHIP_REGFLDVAL_SYSCFG_PMCR_EPIS_MII         0
#define CHIP_REGFLDVAL_SYSCFG_PMCR_EPIS_RMII        4


/* общие определения номера регистра и битовой маски настройки выброанного номер EXTI по GPIO */
#define CHIP_SYSCFG_EXTICR_REGNUM(x)                ((x) / 4)
#define CHIP_SYSCFG_EXTICR_BITMSK(x)                (0x0000000FUL << (4 * (x) % 4))

/******* SYSCFG external interrupt configuration register 1 (SYSCFG_EXTICR1) **/
#define CHIP_REGFLD_SYSCFG_EXTICR1_EXTI0            (0x0000000FUL <<  0U) /* EXTI 0 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR1_EXTI1            (0x0000000FUL <<  4U) /* EXTI 1 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR1_EXTI2            (0x0000000FUL <<  8U) /* EXTI 2 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR1_EXTI3            (0x0000000FUL << 12U) /* EXTI 3 configuration */

/******* SYSCFG external interrupt configuration register 2 (SYSCFG_EXTICR2) **/
#define CHIP_REGFLD_SYSCFG_EXTICR2_EXTI4            (0x0000000FUL <<  0U) /* EXTI 4 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR2_EXTI5            (0x0000000FUL <<  4U) /* EXTI 5 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR2_EXTI6            (0x0000000FUL <<  8U) /* EXTI 6 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR2_EXTI7            (0x0000000FUL << 12U) /* EXTI 7 configuration */

/******* SYSCFG external interrupt configuration register 3 (SYSCFG_EXTICR3) **/
#define CHIP_REGFLD_SYSCFG_EXTICR3_EXTI8            (0x0000000FUL <<  0U) /* EXTI 8 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR3_EXTI9            (0x0000000FUL <<  4U) /* EXTI 9 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR3_EXTI10           (0x0000000FUL <<  8U) /* EXTI 10 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR3_EXTI11           (0x0000000FUL << 12U) /* EXTI 11 configuration */

/******* SYSCFG external interrupt configuration register 4 (SYSCFG_EXTICR4) **/
#define CHIP_REGFLD_SYSCFG_EXTICR4_EXTI12           (0x0000000FUL <<  0U) /* EXTI 12 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR4_EXTI13           (0x0000000FUL <<  4U) /* EXTI 13 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR4_EXTI14           (0x0000000FUL <<  8U) /* EXTI 14 configuration */
#define CHIP_REGFLD_SYSCFG_EXTICR4_EXTI15           (0x0000000FUL << 12U) /* EXTI 15 configuration */

/*********** SYSCFG configuration register (SYSCFG_CFGR) **********************/
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_SYSCFG_CFGR_CM4L                (0x00000001UL <<  0U) /* Cortex-M4 LOCKUP (Hardfault) output enable bit */
#endif
#define CHIP_REGFLD_SYSCFG_CFGR_PVDL                (0x00000001UL <<  2U) /* PVD lock enable bit */
#define CHIP_REGFLD_SYSCFG_CFGR_FLASHL              (0x00000001UL <<  3U) /* FLASH double ECC error lock bit */
#define CHIP_REGFLD_SYSCFG_CFGR_CM7L                (0x00000001UL <<  6U) /* Cortex-M7 LOCKUP (Hardfault) output enable bit */
#define CHIP_REGFLD_SYSCFG_CFGR_BKRAML              (0x00000001UL <<  7U) /* Backup SRAM double ECC error lock bit */
#define CHIP_REGFLD_SYSCFG_CFGR_SRAM4L              (0x00000001UL <<  9U) /* SRAM4 double ECC error lock bit */
#if CHIP_DEV_SUPPORT_SRAM3
#define CHIP_REGFLD_SYSCFG_CFGR_SRAM3L              (0x00000001UL << 10U) /* SRAM3 double ECC error lock bit */
#endif
#define CHIP_REGFLD_SYSCFG_CFGR_SRAM2L              (0x00000001UL << 11U) /* SRAM2 double ECC error lock bit */
#define CHIP_REGFLD_SYSCFG_CFGR_SRAM1L              (0x00000001UL << 12U) /* SRAM1 double ECC error lock bit */
#define CHIP_REGFLD_SYSCFG_CFGR_DTCML               (0x00000001UL << 13U) /* DTCM double ECC error lock bit */
#define CHIP_REGFLD_SYSCFG_CFGR_ITCML               (0x00000001UL << 14U) /* ITCM double ECC error lock bit */
#define CHIP_REGFLD_SYSCFG_CFGR_AXISRAML            (0x00000001UL << 15U) /* AXISRAM double ECC error lock bit */

/*********** SYSCFG compensation cell control/status register (SYSCFG_CCCSR) **/
#define CHIP_REGFLD_SYSCFG_CCCSR_EN                 (0x00000001UL <<  0U) /* I/O compensation cell enable */
#define CHIP_REGFLD_SYSCFG_CCCSR_CS                 (0x00000001UL <<  1U) /* I/O compensation cell code selection */
#define CHIP_REGFLD_SYSCFG_CCCSR_READY              (0x00000001UL <<  8U) /* I/O compensation cell ready flag */
#define CHIP_REGFLD_SYSCFG_CCCSR_HSLV               (0x00000001UL << 16U) /* High-speed at low-voltage */

/*********** SYSCFG compensation cell value register (SYSCFG_CCVR) ************/
#define CHIP_REGFLD_SYSCFG_CCVR_NCV                 (0x0000000FUL <<  0U) /* NMOS compensation value */
#define CHIP_REGFLD_SYSCFG_CCVR_PCV                 (0x0000000FUL <<  4U) /* PMOS compensation value */

/*********** SYSCFG compensation cell code register (SYSCFG_CCCR) *************/
#define CHIP_REGFLD_SYSCFG_CCCR_NCC                 (0x0000000FUL <<  0U) /* NMOS compensation code */
#define CHIP_REGFLD_SYSCFG_CCCR_PCC                 (0x0000000FUL <<  4U) /* PMOS compensation code */
/*********** SYSCFG power control register (SYSCFG_PWRCR) *********************/
#define CHIP_REGFLD_SYSCFG_PWRCR_ODEN               (0x00000001UL <<  0U) /* PWR overdrive enable */

/* SYSCFG ADC2 internal input alternate connection register (SYSCFG_ADC2ALT) **/
#if CHIP_DEV_SUPPORT_ADC2_ALTCON
#define CHIP_REGFLD_SYSCFG_ADC2ALT_ROUT0            (0x00000001UL <<  0U) /* ADC2 VINP[16] alternate connection */
#define CHIP_REGFLD_SYSCFG_ADC2ALT_ROUT1            (0x00000001UL <<  1U) /* ADC2 VINP[17] alternate connection */
#endif

/*********** SYSCFG package register (SYSCFG_PKGR) ****************************/
#define CHIP_REGFLD_SYSCFG_PKGR_PKG                 (0x0000000FUL <<  0U) /* Package type */

/********** SYSCFG user register 0 (SYSCFG_UR0) *******************************/
#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_REGFLD_SYSCFG_UR0_BKS                  (0x00000001UL <<  0U) /* Bank Swap */
#endif
#define CHIP_REGFLD_SYSCFG_UR0_RDP                  (0x000000FFUL << 16U) /* Readout protection */

/********** SYSCFG user register 1 (SYSCFG_UR1) *******************************/
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_SYSCFG_UR1_BCM4                 (0x00000001UL <<  0U) /* Boot Cortex-M4 */
#define CHIP_REGFLD_SYSCFG_UR1_BCM7                 (0x00000001UL << 16U) /* Boot Cortex-M7 */
#endif

/********** SYSCFG user register 2 (SYSCFG_UR2) *******************************/
#define CHIP_REGFLD_SYSCFG_UR2_BORH                 (0x00000003UL <<  0U) /* Brown Out Reset High level */
#define CHIP_REGFLD_SYSCFG_UR2_BCM7_ADD0            (0x0000FFFFUL << 16U) /* Cortex-M7 Boot Address 0 */

/********** SYSCFG user register 3 (SYSCFG_UR3) *******************************/
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_SYSCFG_UR3_BCM4_ADD0            (0x0000FFFFUL <<  0U) /* Cortex-M4 Boot Address 0 */
#endif

#define CHIP_REGFLD_SYSCFG_UR3_BCM7_ADD1            (0x0000FFFFUL << 16U) /* Cortex-M7 Boot Address 1 */

/********** SYSCFG user register 4 (SYSCFG_UR4) *******************************/
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_SYSCFG_UR4_BCM4_ADD1            (0x0000FFFFUL <<  0U) /* Cortex-M4 Boot Address 1 */
#endif
#define CHIP_REGFLD_SYSCFG_UR4_MEPAD_BANK1          (0x00000001UL << 16U) /* Mass Erase Protected Area Disabled for bank 1 */

/********** SYSCFG user register 5 (SYSCFG_UR5) *******************************/
#define CHIP_REGFLD_SYSCFG_UR5_MESAD_BANK1          (0x00000001UL <<  0U) /* Mass erase secured area disabled for bank 1 */
#define CHIP_REGFLD_SYSCFG_UR5_WRPN_BANK1           (0x000000FFUL << 16U) /* Write protection for flash bank 1 */

/********** SYSCFG user register 6 (SYSCFG_UR6) *******************************/
#define CHIP_REGFLD_SYSCFG_UR6_PA_BEG_BANK1         (0x00000FFFUL <<  0U) /* Protected area start address for bank 1 */
#define CHIP_REGFLD_SYSCFG_UR6_PA_END_BANK1         (0x00000FFFUL << 16U) /* Protected area end address for bank 1 */

/********** SYSCFG user register 7 (SYSCFG_UR7) *******************************/
#define CHIP_REGFLD_SYSCFG_UR7_SA_BEG_BANK1         (0x00000FFFUL <<  0U) /* Secured area start address for bank 1 */
#define CHIP_REGFLD_SYSCFG_UR7_SA_END_BANK1         (0x00000FFFUL << 16U) /* Secured area end address for bank 1 */

/********** SYSCFG user register 8 (SYSCFG_UR8) *******************************/
#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_REGFLD_SYSCFG_UR8_MEPAD_BANK2          (0x00000001UL <<  0U) /* Mass erase Protected area disabled for bank 2 */
#define CHIP_REGFLD_SYSCFG_UR8_MESAD_BANK2          (0x00000001UL << 16U) /* Mass Erase Secured Area Disabled for bank 2 */
#endif

/********** SYSCFG user register 9 (SYSCFG_UR9) *******************************/
#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_REGFLD_SYSCFG_UR9_WRPN_BANK2           (0x000000FFUL <<  0U) /* Write protection for flash bank 2 */
#define CHIP_REGFLD_SYSCFG_UR9_PA_BEG_BANK2         (0x00000FFFUL << 16U) /* Protected area start address for bank 2 */
#endif

/********** SYSCFG user register 10 (SYSCFG_UR10) *****************************/
#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_REGFLD_SYSCFG_UR10_PA_END_BANK2        (0x00000FFFUL <<  0U) /* Protected area end address for bank 2 */
#define CHIP_REGFLD_SYSCFG_UR10_SA_BEG_BANK2        (0x00000FFFUL << 16U) /* Secured area start address for bank 2 */
#endif

/********** SYSCFG user register 11 (SYSCFG_UR11) *****************************/
#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_REGFLD_SYSCFG_UR11_SA_END_BANK2        (0x00000FFFUL <<  0U) /* Secured area end address for bank 2 */
#endif
#define CHIP_REGFLD_SYSCFG_UR11_IWDG1M              (0x00000001UL << 16U) /* Independent Watchdog 1 mode (SW or HW) */

/********** SYSCFG user register 12 (SYSCFG_UR12) *****************************/
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_SYSCFG_UR12_IWDG2M              (0x00000001UL <<  0U)  /* Independent Watchdog 2 mode */
#endif
#define CHIP_REGFLD_SYSCFG_UR12_SECURE              (0x00000001UL << 16U)  /* Secure mode status */

/********** SYSCFG user register 13 (SYSCFG_UR13) *****************************/
#define CHIP_REGFLD_SYSCFG_UR13_SDRS                (0x00000003UL <<  0U) /* Secured DTCM RAM Size */
#define CHIP_REGFLD_SYSCFG_UR13_D1SBRST             (0x00000001UL << 16U) /* D1 Standby reset */

/********** SYSCFG user register 14 (SYSCFG_UR14) *****************************/
#define CHIP_REGFLD_SYSCFG_UR14_D1STPRST            (0x00000001UL <<  0U) /* D1 Stop Reset */
#if CHIP_DEV_SUPPORT_D2_RSTCTL
#define CHIP_REGFLD_SYSCFG_UR14_D2SBRST             (0x00000001UL << 16U) /* D2 Standby reset */
#endif

/********** SYSCFG user register 15 (SYSCFG_UR15) *****************************/
#if CHIP_DEV_SUPPORT_D2_RSTCTL
#define CHIP_REGFLD_SYSCFG_UR15_D2STPRST            (0x00000001UL <<  0U) /* D2 Stop Reset */
#endif
#define CHIP_REGFLD_SYSCFG_UR15_FZIWDGSTB           (0x00000001UL << 16U) /* Freeze independent watchdogs in Standby mode */

/********** SYSCFG user register 16 (SYSCFG_UR16) *****************************/
#define CHIP_REGFLD_SYSCFG_UR16_FZIWDGSTP           (0x00000001UL <<  0U) /* Freeze independent watchdogs in Stop mode */
#define CHIP_REGFLD_SYSCFG_UR16_PKP                 (0x00000001UL << 16U) /* Private key programmed */

/********** SYSCFG user register 17 (SYSCFG_UR17) *****************************/
#define CHIP_REGFLD_SYSCFG_UR17_IOHSLV              (0x00000001UL <<  0U) /* I/O high speed / low voltage */
#if CHIP_DEV_SUPPORT_ITCM_AXI_SHARE
#define CHIP_REGFLD_SYSCFG_UR17_TCM_AXI_SHARE_CFG   (0x00000003UL << 16U) /* ITCM-RAM / AXI-SRAM size */
#endif

/********** SYSCFG user register 18 (SYSCFG_UR18) *****************************/
#define CHIP_REGFLD_SYSCFG_UR18_CPU_FREQ_BOOST      (0x00000001UL <<  0U) /* CPU maximum frequency boost enable */

#endif // CHIP_STM32H7XX_REGS_SYSCFG_H_
