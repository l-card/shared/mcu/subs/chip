#ifndef REGS_PWM_H
#define REGS_PWM_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

#define CHIP_PWM_CH_CNT                        24
#define CHIP_PWM_CH_OUT_CNT                     8

typedef struct {
    __IO uint32_t UNLK;                        /* 0x0: Shadow registers unlock register */
    union {
        __IO uint32_t STA;                     /* 0x4: Counter start register */
#if CHIP_DEV_SUPPORT_PWM_HR
        __IO uint32_t STA_HRPWM;               /* 0x4: Counter start register */
#endif
    };
    union {
        __IO uint32_t RLD;                     /* 0x8: Counter reload register */
#if CHIP_DEV_SUPPORT_PWM_HR
        __IO uint32_t RLD_HRPWM;               /* 0x8: Counter reload register */
#endif
    };
    union {
        __IO uint32_t CMP[CHIP_PWM_CH_CNT];   /* 0xC - 0x68: Comparator register */
#if CHIP_DEV_SUPPORT_PWM_HR
        __IO uint32_t CMP_HRPWM[CHIP_PWM_CH_CNT]; /* 0xC - 0x68: Comparator register */
#endif
    };
    __I  uint8_t  RESERVED0[12];               /* 0x6C - 0x77: Reserved */
    __IO uint32_t FRCMD;                       /* 0x78: Force output mode register */
    __IO uint32_t SHLK;                        /* 0x7C: Shadow registers lock register */
    __IO uint32_t CHCFG[CHIP_PWM_CH_CNT];      /* 0x80 - 0xDC: Output channel configure register */
    __I  uint8_t  RESERVED1[16];               /* 0xE0 - 0xEF: Reserved */
    __IO uint32_t GCR;                         /* 0xF0: Global control register */
    __IO uint32_t SHCR;                        /* 0xF4: Shadow register control register */
    __I  uint8_t  RESERVED2[8];                /* 0xF8 - 0xFF: Reserved */
    __I  uint32_t CAPPOS[CHIP_PWM_CH_CNT];     /* 0x100 - 0x15C: Capture rising edge register */
    __I  uint8_t  RESERVED3[16];               /* 0x160 - 0x16F: Reserved */
    __I  uint32_t CNT;                         /* 0x170: Counter */
    __I  uint8_t  RESERVED4[12];               /* 0x174 - 0x17F: Reserved */
    __I  uint32_t CAPNEG[CHIP_PWM_CH_CNT];     /* 0x180 - 0x1DC: Capture falling edge register */
    __I  uint8_t  RESERVED5[16];               /* 0x1E0 - 0x1EF: Reserved */
    __I  uint32_t CNTCOPY;                     /* 0x1F0: Counter copy */
    __I  uint8_t  RESERVED6[12];               /* 0x1F4 - 0x1FF: Reserved */
    __IO uint32_t PWMCFG[CHIP_PWM_CH_OUT_CNT]; /* 0x200 - 0x21C: PWM channel configure register */
    __O  uint32_t SR;                          /* 0x220: Status register */
    __IO uint32_t IRQEN;                       /* 0x224: Interrupt request enable register */
    __I  uint8_t  RESERVED7[4];                /* 0x228 - 0x22B: Reserved */
    __IO uint32_t DMAEN;                       /* 0x22C: DMA request enable register */
    __IO uint32_t CMPCFG[CHIP_PWM_CH_CNT];     /* 0x230 - 0x28C: Comparator configure register */
    __I  uint8_t  RESERVED8[368];              /* 0x290 - 0x3FF: Reserved */
#if CHIP_DEV_SUPPORT_PWM_HR
    __I  uint32_t ANASTS[8];                   /* 0x400 - 0x41C: analog status register */
#else
    __I  uint32_t RESERVED9[8];
#endif
    __O  uint32_t HRPWM_CFG;                   /* 0x420: hrpwm config register */
} CHIP_REGS_PWM_T;


#define CHIP_REGS_PWM0            ((CHIP_REGS_PWM_T *)CHIP_MEMRGN_ADDR_PWM0)
#define CHIP_REGS_PWM1            ((CHIP_REGS_PWM_T *)CHIP_MEMRGN_ADDR_PWM1)
#define CHIP_REGS_PWM2            ((CHIP_REGS_PWM_T *)CHIP_MEMRGN_ADDR_PWM2)
#define CHIP_REGS_PWM3            ((CHIP_REGS_PWM_T *)CHIP_MEMRGN_ADDR_PWM3)



#define CHIP_PWM_UNLOCK_VALUE                           0xB0382607

/*********** PWM_STA - Counter start register *********************************/
#define CHIP_REGFLD_PWM_STA_XSTA                        (0x0000000FUL << 28U) /* (RW) Counter start extension value of PWM timer */
#define CHIP_REGFLD_PWM_STA_STA                         (0x00FFFFFFUL <<  4U) /* (RW) Counter start value of PWM timer */
#define CHIP_REGMSK_PWM_STA_RESERVED                    (0x0000000FUL)


/*********** PWM_RLD - Counter reload register ********************************/
#define CHIP_REGFLD_PWM_RLD_XRLD                        (0x0000000FUL << 28U) /* (RW) Counter reload extension value of PWM timer */
#define CHIP_REGFLD_PWM_RLD_RLD                         (0x00FFFFFFUL <<  4U) /* (RW) Counter reload value of PWM timer */
#define CHIP_REGMSK_PWM_RLD_RESERVED                    (0x0000000FUL)


/*********** PWM_CMP - Comparator register 0..23 ******************************/
#define CHIP_REGFLD_PWM_CMP_XCMP                        (0x0000000FUL << 28U) /* (RW) Comparator value extension */
#define CHIP_REGFLD_PWM_CMP_CMP                         (0x00FFFFFFUL <<  4U) /* (RW) Comparator value */
#define CHIP_REGFLD_PWM_CMP_CMPHLF                      (0x00000001UL <<  3U) /* (RW) Comparator half cycle comparison */
#define CHIP_REGFLD_PWM_CMP_CMPJIT                      (0x00000007UL <<  0U) /* (RW) Comparator value jitter */


/*********** PWM_FRCMD - Force output mode register ***************************/
#define CHIP_REGFLD_PWM_FRCMD_OUT(x)                    (0x00000003UL << (2U*(x))) /* (RW) Forced output mode control of PWM output channel x (0..7) */

#define CHIP_REGFLDVAL_PWM_FRCMD_OUT_FORCE_0            0 /* Force output to 0 */
#define CHIP_REGFLDVAL_PWM_FRCMD_OUT_FORCE_1            1 /* Force output to 1 */
#define CHIP_REGFLDVAL_PWM_FRCMD_OUT_FORCE_Z            2 /* Force output to high impedance */
#define CHIP_REGFLDVAL_PWM_FRCMD_OUT_FORCE_DIS          3 /* Do not force output */


/*********** PWM_SHLK - Shadow registers lock register ************************/
#define CHIP_REGFLD_PWM_SHLK_SHLK                       (0x00000001UL << 31U) /* (RW) Lock all shadow registers. */
#define CHIP_REGMSK_PWM_SHLK_RESERVED                   (0x7FFFFFFFUL)


/*********** PWM_CHCFG - Output channel configure register 0..23 **************/
#define CHIP_REGFLD_PWM_CHCFG_CMPSELEND                 (0x0000001FUL << 24U) /* (RW) Assigned to channel comparators sequence end number */
#define CHIP_REGFLD_PWM_CHCFG_CMPSELBEG                 (0x0000001FUL << 16U) /* (RW) Assigned to channel comparators sequence start number */
#define CHIP_REGFLD_PWM_CHCFG_OUTPOL                    (0x00000001UL <<  1U) /* (RW) Invert the channel output */
#define CHIP_REGMSK_PWM_CHCFG_RESERVED                  (0xE0E0FFFDUL)


/*********** PWM_GCR - Global control register ********************************/
#define CHIP_REGFLD_PWM_GCR_FAULTI_EN(x)                (0x00000001UL << (28U + (x))) /* (RW) Enable internal fault input signal x (0..3) */
#define CHIP_REGFLD_PWM_GCR_DEBUGFAULT                  (0x00000001UL << 27U) /* (RW) Enable debug mode protection */
#define CHIP_REGFLD_PWM_GCR_FRCPOL                      (0x00000001UL << 26U) /* (RW) Polarity of the output hardware force control signal FRCI (1 - high, 0 - low) */
#define CHIP_REGFLD_PWM_GCR_HWSHDWEDG                   (0x00000001UL << 24U) /* (RW) Comparator shadow registers take effect time edge selection for input capture mode event */
#define CHIP_REGFLD_PWM_GCR_CMPSHDWSEL                  (0x0000001FUL << 19U) /* (RW) Comparator shadow registers take effect hardware event comparator selection */
#define CHIP_REGFLD_PWM_GCR_FAULTRECEDG                 (0x00000001UL << 18U) /* (RW) Fault recovery comparator take effect time edge selection */
#define CHIP_REGFLD_PWM_GCR_FAULTRECHWSEL               (0x0000001FUL << 13U) /* (RW) Fault recovery comparator selection */
#define CHIP_REGFLD_PWM_GCR_FAULTE_EN(x)                (0x00000001UL << (11U + (x))) /* (RW) Enable external fault input signal x (0..1) */
#define CHIP_REGFLD_PWM_GCR_FAULTEXPOL(x)               (0x00000001UL << (9U + (x)))  /* (RW) Active polarity of external fault input x (0..1) */
#define CHIP_REGFLD_PWM_GCR_RLDSYNCEN                   (0x00000001UL <<  8U) /* (RW) Enable triggering PWM timer counter reload via SYNCI signal */
#define CHIP_REGFLD_PWM_GCR_CEN                         (0x00000001UL <<  7U) /* (RW) Enable the PWM timer counter  */
#define CHIP_REGFLD_PWM_GCR_FAULTCLR                    (0x00000001UL <<  6U) /* (RW) Fault clear */
#define CHIP_REGFLD_PWM_GCR_XRLDSYNCEN                  (0x00000001UL <<  5U) /* (RW) Enable the PWM timer counter extended value to be reloaded  triggered by the SYNCI signal */
#define CHIP_REGFLD_PWM_GCR_FRCTIME                     (0x00000003UL <<  1U) /* (RW) Forced output timing control */
#define CHIP_REGFLD_PWM_GCR_SWFRC                       (0x00000001UL <<  0U) /* (RW) Software forced output when FRCSRCSEL is set to 0 */
#define CHIP_REGMSK_PWM_GCR_RESERVED                    (0x02000018UL)


#define CHIP_REGFLDVAL_PWM_GCR_HWSHDWEDG_FALL           1 /* Falling edge */
#define CHIP_REGFLDVAL_PWM_GCR_HWSHDWEDG_RISE           0 /* Rising edge */

#define CHIP_REGFLDVAL_PWM_GCR_FAULTRECEDG_FALL         1 /* Falling edge */
#define CHIP_REGFLDVAL_PWM_GCR_FAULTRECEDG_RISE         0 /* Rising edge */

#define CHIP_REGFLDVAL_PWM_GCR_FAULTEXPOL_LOW           1 /* Active low level */
#define CHIP_REGFLDVAL_PWM_GCR_FAULTEXPOL_HIGH          0 /* Active high level */

#define CHIP_REGFLDVAL_PWM_GCR_FRCTIME_IMM              0 /* Force output takes effect immediately */
#define CHIP_REGFLDVAL_PWM_GCR_FRCTIME_RLD              1 /* Force output takes effect on the next time when counter is reloaded  */
#define CHIP_REGFLDVAL_PWM_GCR_FRCTIME_FRCSYNCI         2 /* Force output takes effect when the rising edge is captured on FRCSYNCI */


/*********** PWM_SHCR - Shadow register control register **********************/
#define CHIP_REGFLD_PWM_SHCR_FRCSHDWSEL                 (0x0000001FUL <<  8U) /* (RW) FRCMD shadow register take effect hardware event comparator selection */
#define CHIP_REGFLD_PWM_SHCR_CNTSHDWSEL                 (0x0000001FUL <<  3U) /* (RW) RLD, STA shadow registers take effect hardware event comparator selection */
#define CHIP_REGFLD_PWM_SHCR_CNTSHDWUPT                 (0x00000003UL <<  1U) /* (RW) RLD, STA shadow registers take effect time selection */
#define CHIP_REGFLD_PWM_SHCR_SHLKEN                     (0x00000001UL <<  0U) /* (RW) Turn on shadow register lock, allow shadow registers to be locked */
#define CHIP_REGMSK_PWM_SHCR_RESERVED                   (0xFFFFE000UL)


#define CHIP_REGFLDVAL_PWM_SHCR_CNTSHDWUPT_SW           0 /* It takes effect after the SHLK[SHLK] bit is set to 1 by software */
#define CHIP_REGFLDVAL_PWM_SHCR_CNTSHDWUPT_IMM          1 /* It takes effect in real time, within one cycle after register writing */
#define CHIP_REGFLDVAL_PWM_SHCR_CNTSHDWUPT_CMP          2 /* It takes effect after a match occurs in a certain CMP of the timer. The user can select one from comparators 0 to 23 through SHCR[CNTSHDWSEL]. The match can be the output comparison of the comparator or the input capture */
#define CHIP_REGFLDVAL_PWM_SHCR_CNTSHDWUPT_SHRLDSYNCI   3 /* It takes effect when the rising edge is captured on the shadow register reload trigger input SHRLDSYNCI */


/*********** PWM_CAPPOS - Capture rising edge register 0..23 ******************/
#define CHIP_REGFLD_PWM_CAPPOS_CAPPOS                   (0x0FFFFFFFUL <<  4U) /* (RW) The counter value captured at the rising edge of the input signal */
#define CHIP_REGMSK_PWM_CAPPOS_RESERVED                 (0x0000000FUL)


/*********** PWM_CNT - Counter ************************************************/
#define CHIP_REGFLD_PWM_CNT_XCNT                        (0x0000000FUL << 28U) /* (RO) Counter extension */
#define CHIP_REGFLD_PWM_CNT_CNT                         (0x00FFFFFFUL <<  4U) /* (RO) Counter */
#define CHIP_REGMSK_PWM_CNT_RESERVED                    (0x0000000FUL)


/*********** PWM_CAPNEG - Capture falling edge register 0..23 ******************/
#define CHIP_REGFLD_PWM_CAPNEG_CAPNEG                   (0x0FFFFFFFUL <<  4U) /* (RW) The counter value captured at the falling edge of the input signal */
#define CHIP_REGMSK_PWM_CAPNEG_RESERVED                 (0x0000000FUL)


/*********** PWM_CNTCOPY - Counter copy ***************************************/
#define CHIP_REGFLD_PWM_CNTCOPY_XCNT                    (0x0000000FUL << 28U) /* (RO) Counter extension */
#define CHIP_REGFLD_PWM_CNTCOPY_CNT                     (0x00FFFFFFUL <<  4U) /* (RO) Counter */
#define CHIP_REGMSK_PWM_CNTCOPY_RESERVED                (0x0000000FUL)


/*********** PWM_PWMCFG - PWM channel configure register 0..7 *****************/
#define CHIP_REGFLD_PWM_PWMCFG_OEN                      (0x00000001UL << 28U) /* (RW) PWM output enable */
#define CHIP_REGFLD_PWM_PWMCFG_FRCSHDWUPT               (0x00000003UL << 26U) /* (RW) FRCMD shadow register take effect timing selection */
#define CHIP_REGFLD_PWM_PWMCFG_FAULTMODE                (0x00000003UL << 24U) /* (RW) Fault protection mode */
#define CHIP_REGFLD_PWM_PWMCFG_FAULTRECTIME             (0x00000003UL << 22U) /* (RW) Fault recovery output timing selection */
#define CHIP_REGFLD_PWM_PWMCFG_FRCSRCSEL                (0x00000001UL << 21U) /* (RW) Forced output selection */
#define CHIP_REGFLD_PWM_PWMCFG_PAIR                     (0x00000001UL << 20U) /* (RW) PWM complementary output */
#define CHIP_REGFLD_PWM_PWMCFG_DEADAREA                 (0x000FFFFFUL <<  0U) /* (RW) The dead zone length in 0.5 clock cycles */
#define CHIP_REGMSK_PWM_PWMCFG_RESERVED                 (0xE0000000UL)



#define CHIP_REGFLDVAL_PWM_PWMCFG_FRCSHDWUPT_SW         0 /* It takes effect after the SHLK[SHLK] bit is set to 1 by software */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FRCSHDWUPT_IMM        1 /* It takes effect in real time, within one cycle after register writing */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FRCSHDWUPT_CMP        2 /* It takes effect after a match occurs in a certain CMP of the timer. The user can select one from comparators 0 to 23 through SHCR[FRCSHDWSEL]. The match can be the output comparison of the comparator or the input capture */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FRCSHDWUPT_SHRLDSYNCI 3 /* It takes effect when the rising edge is captured on the shadow register reload trigger input SHRLDSYNCI */

#define CHIP_REGFLDVAL_PWM_PWMCFG_FAULTMODE_LOW         0 /* When a fault occurs, the PWM output is forced to a low level */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FAULTMODE_HIGH        1 /* When a fault occurs, the PWM output is forced to a high level */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FAULTMODE_Z           2 /* When a fault occurs, the PWM output is forced to high impedance */

#define CHIP_REGFLDVAL_PWM_PWMCFG_FAULTRECTIME_IMM      0 /* Restore output immediately after the fault condition is removed */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FAULTRECTIME_RLD      1 /* After the fault condition is removed, the output is restored after the counter is reloaded with RLD */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FAULTRECTIME_CMP      2 /* It takes effect after a match occurs in a certain CMP of the timer. The user can select one from comparators 0 to 23 through FAULTRECHWSEL. The match can be the output comparison of the comparator or the input capture */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FAULTRECTIME_SW       3 /* After the fault condition is removed, the output is restored when software sets the FAULTCLR bit to 1 */

#define CHIP_REGFLDVAL_PWM_PWMCFG_FRCSRCSEL_FRCI        0 /* Hardware forced output, forced output switch controlled by FRCI */
#define CHIP_REGFLDVAL_PWM_PWMCFG_FRCSRCSEL_SW          1 /* Software forced output, forced output switch controlled by SWFRC bit */


/*********** PWM_SR - Status register *****************************************/
#define CHIP_REGFLD_PWM_SR_FAULTF                       (0x00000001UL << 27U) /* (W1C) Fault flag */
#define CHIP_REGFLD_PWM_SR_XRLDF                        (0x00000001UL << 26U) /* (W1C) Extended reload flag */
#define CHIP_REGFLD_PWM_SR_HALFRLDF                     (0x00000001UL << 25U) /* (W1C) Half reload flag */
#define CHIP_REGFLD_PWM_SR_RLDF                         (0x00000001UL << 24U) /* (W1C) Reload flag */
#define CHIP_REGFLD_PWM_SR_CMPFX(x)                     (0x00000001UL << (x)) /* (W1C) Comparator match event flag (x=0..23) */
#define CHIP_REGMSK_PWM_SR_RESERVED                     (0xF0000000UL)


/*********** PWM_IRQEN - Interrupt request enable register ********************/
#define CHIP_REGFLD_PWM_IRQEN_FAULTIRQE                 (0x00000001UL << 27U) /* (RW) Fault interrupt request enable bit */
#define CHIP_REGFLD_PWM_IRQEN_XRLDIRQE                  (0x00000001UL << 26U) /* (RW) Extended reload interrupt request enable bit */
#define CHIP_REGFLD_PWM_IRQEN_HALFRLDIRQE               (0x00000001UL << 25U) /* (RW) Half reload interrupt request enable bit */
#define CHIP_REGFLD_PWM_IRQEN_RLDIRQE                   (0x00000001UL << 24U) /* (RW) Reload interrupt request enable bit */
#define CHIP_REGFLD_PWM_IRQEN_CMPIRQEX(x)               (0x00000001UL << (x)) /* (RW) Comparator match event interrupt request enable bit (x=0..23) */
#define CHIP_REGMSK_PWM_IRQEN_RESERVED                  (0xF0000000UL)


/*********** PWM_DMAEN - DMA request enable register **************************/
#define CHIP_REGFLD_PWM_DMAEN_FAULTIRQE                 (0x00000001UL << 27U) /* (RW) Fault DMA request enable bit */
#define CHIP_REGFLD_PWM_DMAEN_XRLDIRQE                  (0x00000001UL << 26U) /* (RW) Extended reload DMA request enable bit */
#define CHIP_REGFLD_PWM_DMAEN_HALFRLDIRQE               (0x00000001UL << 25U) /* (RW) Half reload DMA request enable bit */
#define CHIP_REGFLD_PWM_DMAEN_RLDIRQE                   (0x00000001UL << 24U) /* (RW) Reload DMA request enable bit */
#define CHIP_REGFLD_PWM_DMAEN_CMPIRQEX(x)               (0x00000001UL << (x)) /* (RW) Comparator match event DMA request enable bit (x=0..23) */
#define CHIP_REGMSK_PWM_DMAEN_RESERVED                  (0xF0000000UL)


/*********** PWM_CMPCFG - omparator configure register 0..23 ******************/
#define CHIP_REGFLD_PWM_CMPCFG_XCNTCMPEN                (0x0000000FUL <<  4U) /* (RW) Comparator extended comparison value enable bits */
#define CHIP_REGFLD_PWM_CMPCFG_CMPSHDWUPT               (0x00000003UL <<  2U) /* (RW) Comparator shadow registers take effect time selection */
#define CHIP_REGFLD_PWM_CMPCFG_CMPMODE                  (0x00000001UL <<  1U) /* (RW) Comparator mode selection */
#define CHIP_REGFLD_PWM_CMPCFG_RESERVED                 (0xFFFFFF01UL)

#define CHIP_REGFLDVAL_PWM_CMPCFG_CMPSHDWUPT_SW         0 /* It takes effect after the SHLK[SHLK] bit is set to 1 by software */
#define CHIP_REGFLDVAL_PWM_CMPCFG_CMPSHDWUPT_IMM        1 /* It takes effect in real time, within one cycle after register writing */
#define CHIP_REGFLDVAL_PWM_CMPCFG_CMPSHDWUPT_CMP        2 /* It takes effect after a match occurs in a certain CMP of the timer. The user can select one from comparators 0 to 23 through CMPSHDWSEL. The match can be the output comparison of the comparator or the input capture. */
#define CHIP_REGFLDVAL_PWM_CMPCFG_CMPSHDWUPT_SHRLDSYNCI 3 /* It takes effect when the rising edge is captured on the shadow register reload trigger input SHRLDSYNCI */

#define CHIP_REGFLDVAL_PWM_CMPCFG_CMPMODE_OUTCMP        0 /* Output comparison mode */
#define CHIP_REGFLDVAL_PWM_CMPCFG_CMPMODE_INCAP         1 /*  Input capture mode */


#endif // REGS_PWM_H
