#ifndef CHIP_GIC_ISR_H
#define CHIP_GIC_ISR_H

#include <stdint.h>

typedef struct {
    unsigned long spsr;
    unsigned long elr;
    unsigned long esr;
    unsigned long regs[31];
} t_chip_isr_regs;

typedef enum {
    CHIP_GIC_VTYPE_CUREL_SP0_SYNC       = 0,
    CHIP_GIC_VTYPE_CUREL_SP0_IRQ        = 1,
    CHIP_GIC_VTYPE_CUREL_SP0_FIQ        = 2,
    CHIP_GIC_VTYPE_CUREL_SP0_SERROR     = 3,
    CHIP_GIC_VTYPE_CUREL_SPX_SYNC       = 4,
    CHIP_GIC_VTYPE_CUREL_SPX_IRQ        = 5,
    CHIP_GIC_VTYPE_CUREL_SPX_FIQ        = 6,
    CHIP_GIC_VTYPE_CUREL_SPX_SERROR     = 7,
    CHIP_GIC_VTYPE_LOWEL_AARCH64_SYNC   = 8,
    CHIP_GIC_VTYPE_LOWEL_AARCH64_IRQ    = 9,
    CHIP_GIC_VTYPE_LOWEL_AARCH64_FIQ    = 10,
    CHIP_GIC_VTYPE_LOWEL_AARCH64_SERROR = 11,
    CHIP_GIC_VTYPE_LOWEL_AARCH32_SYNC   = 12,
    CHIP_GIC_VTYPE_LOWEL_AARCH32_IRQ    = 13,
    CHIP_GIC_VTYPE_LOWEL_AARCH32_FIQ    = 14,
    CHIP_GIC_VTYPE_LOWEL_AARCH32_SERROR = 15,
} t_chip_gic_vector_type;

typedef struct {
    t_chip_gic_vector_type vtype;
    int irq_num;
    const t_chip_isr_regs *chip_regs;
} t_chip_isr_ctx;

typedef enum {
    CHIP_IRQ_TRIGMODE_LEVEL = 0,
    CHIP_IRQ_TRIGMODE_EDGE  = 2,
} t_chip_irq_trigmode;

typedef enum {
    CHIP_IRQ_GROUP0         = 0,
    CHIP_IRQ_GROUP1NS       = 1,
    CHIP_IRQ_GROUP1S        = 2,
} t_chip_irq_group;


typedef void (*t_chip_isr_func)(const t_chip_isr_ctx *ctx);


#define CHIP_GIC_INT_REGNUM(irq_num)  ((irq_num) >> 5)
#define CHIP_GIC_INT_REGFLD(irq_num)  (0x00000001UL << ((irq_num) & 0x1F))
#define CHIP_GIC_INT_REGNUM_2B(irq_num)  ((irq_num)/16)
#define CHIP_GIC_INT_REGFLD_2B(irq_num)  (0x00000003UL << 2U*((irq_num) & 0x0F))
#define CHIP_GIC_INT_REGNUM_8B(irq_num)  ((irq_num)/4)
#define CHIP_GIC_INT_REGFLD_8B(irq_num)  (0x000000FFUL << 8U*((irq_num) & 0x03))


#define chip_interrupt_enable(irq_num)   do { \
        if (irq_num >= CHIP_GIC_IRQNUM_SPI_MIN) { \
            CHIP_REGS_GICD->ISENABLER[CHIP_GIC_INT_REGNUM(irq_num)] = CHIP_GIC_INT_REGFLD(irq_num); \
        } else { \
            CHIP_REGS_GICR->ISENABLER[CHIP_GIC_INT_REGNUM(irq_num)] = CHIP_GIC_INT_REGFLD(irq_num); \
        }  \
    } while(0)
#define chip_interrupt_disable(irq_num) do { \
        if (irq_num >= CHIP_GIC_IRQNUM_SPI_MIN) { \
                CHIP_REGS_GICD->ICENABLER[CHIP_GIC_INT_REGNUM(irq_num)] = CHIP_GIC_INT_REGFLD(irq_num); \
        } else { \
                CHIP_REGS_GICR->ICENABLER[CHIP_GIC_INT_REGNUM(irq_num)] = CHIP_GIC_INT_REGFLD(irq_num); \
        }  \
    } while(0)


/* Установки маски прирорита прерываний.
 * Только прерывания со строго большим прироиртетом (меньшим значением) будут обработаны.
 * Должна быть меньше именно старшая реализованная часть приоритета! */
void chip_interrupts_set_priority_mask(uint8_t prio);

void chip_interrupt_set_isr(int irq_num, t_chip_isr_func isr); /* установка обработчика прерываний */
void chip_interrupt_set_trig_mode(int irq_num, t_chip_irq_trigmode mode); /* установка работы по фронту или уровню */
void chip_interrupt_set_priority(int irq_num, uint8_t prio); /* Установка приоритета прерывания (0 - высший, 0xFF - низший, младшие биты могут быть не рализованы) */
void chip_interrupt_set_group(int irq_num, t_chip_irq_group group);

/** @todo set route через GICD_IROUTER - либо конкретному, либо 1:N */

#endif // CHIP_GIC_ISR_H
