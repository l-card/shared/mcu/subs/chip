#ifndef CHIP_LPC11U6X_REGS_PMU_H
#define CHIP_LPC11U6X_REGS_PMU_H


/**
 * @brief LPC11u6x Power Management Unit register block structure
 */
typedef struct {
    __IO uint32_t PCON;        /*!< Offset: 0x000 Power control Register (R/W) */
    __IO uint32_t GPREG[4];    /*!< Offset: 0x004 General purpose Registers 0..3 (R/W) */
    __IO uint32_t DPDCTRL;    /*!< Offset: 0x014 Deep power-down control register (R/W) */
} LPC_PMU_T;

/**
 * @brief LPC11u6x low power mode type definitions
 */
typedef enum CHIP_PMU_MCUPOWER {
    PMU_MCU_SLEEP = 0,        /*!< Sleep mode */
    PMU_MCU_DEEP_SLEEP,        /*!< Deep Sleep mode */
    PMU_MCU_POWER_DOWN,        /*!< Power down mode */
    PMU_MCU_DEEP_PWRDOWN    /*!< Deep power down mode */
} CHIP_PMU_MCUPOWER_T;

/**
 * PMU PCON register bit fields & masks
 */
#define PMU_PCON_PM_SLEEP           (0x0)        /*!< ARM WFI enter sleep mode */
#define PMU_PCON_PM_DEEPSLEEP       (0x1)        /*!< ARM WFI enter Deep-sleep mode */
#define PMU_PCON_PM_POWERDOWN       (0x2)        /*!< ARM WFI enter Power-down mode */
#define PMU_PCON_PM_DEEPPOWERDOWN   (0x3)        /*!< ARM WFI enter Deep Power-down mode */
#define PMU_PCON_NODPD              (1 << 3)    /*!< Disable deep power-down mode */
#define PMU_PCON_SLEEPFLAG          (1 << 8)    /*!< Sleep mode flag */
#define PMU_PCON_DPDFLAG            (1 << 11)    /*!< Deep power-down flag */

/**
 * PMU DPDCTRL register bit fields & masks
 */
#define PMU_DPDCTRL_WAKEUPPHYS      (1 << 10)    /** Enable wake-up pin hysteresis */
#define PMU_DPDCTRL_WAKEPAD         (1 << 11)    /** Disable the Wake-up function on pin PIO0_16 */


#endif /* CHIP_LPC11U6X_REGS_PMU_H */
