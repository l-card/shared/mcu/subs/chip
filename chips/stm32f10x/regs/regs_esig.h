#ifndef CHIP_STM32F10X_REGS_ESIG_H
#define CHIP_STM32F10X_REGS_ESIG_H


#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

typedef struct {
    __I uint16_t FLASH_DENSITY;
#ifdef CHIP_DEV_GD32F10X
    __I uint16_t SRAM_DENSITY; /* gd only */
#else
    uint16_t Reserved1;
#endif
    uint32_t Reserved2;
    union {
        __I uint8_t  UIDB[12];
        __I uint32_t UIDW[3];
    };
} CHIP_REGS_ESIG_T;

#define CHIP_REGS_ESIG                  ((CHIP_REGS_ESIG_T *) CHIP_MEMRGN_ADDR_ESIG)

#endif // CHIP_STM32F10X_REGS_ESIG_H
