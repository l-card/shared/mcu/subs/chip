#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#include "chip_std_defs.h"
#include "chip_dev_spec_features.h"

/* граничные условия для внешней частоты HSE */
#define CHIP_CLK_HSE_FREQ_MIN               CHIP_MHZ(4)  /* минимальная  внешняя частота HSE */
#define CHIP_CLK_HSE_FREQ_MAX               CHIP_MHZ(20) /* максимальная внешняя частота HSE */

/* граничные условия для внешней частоты LSE */
#define CHIP_CLK_LSE_ECLK_FREQ_MAX          CHIP_MHZ(1)  /* максимальня частота LSE в режиме CHIP_CLK_EXTMODE_CLOCK */


#define CHIP_CLK_PLL_IN_FREQ_MIN            CHIP_MHZ(4)  /* минимальная  входная частота PLL */
#define CHIP_CLK_PLL_IN_FREQ_MAX            CHIP_MHZ(20) /* максимальная входная частота PLL */

#define CHIP_CLK_PLL_VCO_FREQ_MIN           CHIP_MHZ(48) /* минимальная  частота PLL VCO (PLLIN * M / N) */
#define CHIP_CLK_PLL_VCO_FREQ_MAX           CHIP_MHZ(72) /* максимальная частота PLL VCO (PLLIN * M / N) */



#define CHIP_CLK_PLL_FREQ_MIN               CHIP_MHZ(16) /* минимальная  выходная частота PLL */
#define CHIP_CLK_PLL_FREQ_MAX               CHIP_MHZ(48) /* максимальная выходная частота PLL */
#define CHIP_CLK_AHB_FREQ_MAX               CHIP_MHZ(48)
#define CHIP_CLK_APB1_FREQ_MAX              CHIP_MHZ(48)
#define CHIP_CLK_APB2_FREQ_MAX              CHIP_MHZ(48)


#define CHIP_CLK_SPI_FREQ_MAX               CHIP_MHZ(18)
#define CHIP_CLK_ADC_FREQ_MAX               CHIP_MHZ(18)


#define CHIP_CLK_FLASH_LATENCY0_MAX         CHIP_MHZ(18)
#define CHIP_CLK_FLASH_LATENCY1_MAX         CHIP_MHZ(36)



#endif // CHIP_CLK_CONSTRAINTS_H
