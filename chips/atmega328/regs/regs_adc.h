#ifndef REGS_ADC_H
#define REGS_ADC_H

#include "chip_devtype_spec_features.h"

#define CHIP_REG_ADCL       _MMIO_BYTE(0x78) /* ADC Data Low Byte Register */
#define CHIP_REG_ADCH       _MMIO_BYTE(0x79) /* ADC Data High Byte Register */
#define CHIP_REG_ADCSRA     _MMIO_BYTE(0x7A) /* ADC Control and Status Register A */
#define CHIP_REG_ADCSRB     _MMIO_BYTE(0x7B) /* ADC Control and Status Register B */
#define CHIP_REG_ADMUX      _MMIO_BYTE(0x7C) /* ADC Multiplexing Control Register */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_ADCSRC     _MMIO_BYTE(0x7D) /* ADC Control and Status Register C */
#endif
#define CHIP_REG_DIDR0      _MMIO_BYTE(0x7E) /* Digital Input Disable Control Register 0 */
#define CHIP_REG_DIDR1      _MMIO_BYTE(0x7F) /* Digital Input Disable Control Register 1 */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_DIDR2      _MMIO_BYTE(0x76) /* Digital Input Disable Control Register 2 */
#define CHIP_REG_DAPCR      _MMIO_BYTE(0xDC) /* Differential Op Amp Control Register */
#define CHIP_REG_OFR0       _MMIO_BYTE(0xA3) /* Offset Compensation Register 0 */
#define CHIP_REG_OFR1       _MMIO_BYTE(0xA4) /* Offset Compensation Register 1 */
#define CHIP_REG_ADT0L      _MMIO_BYTE(0xA5) /* Automatic monitoring lower 8 bits of the underflow threshold */
#define CHIP_REG_ADT0H      _MMIO_BYTE(0xA6) /* Automatic monitoring higher 8 bits of the underflow threshold */
#define CHIP_REG_ADT1L      _MMIO_BYTE(0xAA) /* Automatic monitoring lower 8 bits of the overflow threshold */
#define CHIP_REG_ADT1H      _MMIO_BYTE(0xAB) /* Automatic monitoring higher 8 bits of the overflow threshold */
#define CHIP_REG_ADMSC      _MMIO_BYTE(0xAC) /* Automatic monitoring status and control register */
#define CHIP_REG_ADCSRD     _MMIO_BYTE(0xAD) /* ADC Control and Status Register D */
#endif


/* ---------------- ADCSRA – ADC Control and Status Register A ---------------*/
#define CHIP_REGFLD_ADCSRA_ADEN             (0x01U << 7) /* (RW)   ADC enable */
#define CHIP_REGFLD_ADCSRA_ADSC             (0x01U << 6) /* (RW1)  ADC start conversion */
#define CHIP_REGFLD_ADCSRA_ADATE            (0x01U << 5) /* (RW)   ADC auto trigger enable */
#define CHIP_REGFLD_ADCSRA_ADIF             (0x01U << 4) /* (RW1C) ADC interrupt flag */
#define CHIP_REGFLD_ADCSRA_ADIE             (0x01U << 3) /* (RW)   ADC interrupt enable */
#define CHIP_REGFLD_ADCSRA_ADPS             (0x07U << 0) /* (RW)   ADC prescaler selection */

#define CHIP_REGFLDVAL_ADCSRA_ADPS_2        0
#define CHIP_REGFLDVAL_ADCSRA_ADPS_4        2
#define CHIP_REGFLDVAL_ADCSRA_ADPS_8        3
#define CHIP_REGFLDVAL_ADCSRA_ADPS_16       4
#define CHIP_REGFLDVAL_ADCSRA_ADPS_32       5
#define CHIP_REGFLDVAL_ADCSRA_ADPS_64       6
#define CHIP_REGFLDVAL_ADCSRA_ADPS_128      7

/* ---------------- ADCSRB – ADC Control and Status Register B ---------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_ADCSRB_ACME0            (0x03U << 6) /* (RW) Analog Comparator 0 negative input selection */
#define CHIP_REGFLD_ADCSRB_ACME1            (0x03U << 4) /* (RW) Analog Comparator 1 negative input selection */
#define CHIP_REGFLD_ADCSRB_ACTS             (0x01U << 3) /* (RW) Analog Comparator trigger source channel selection */
#define CHIP_REGFLD_ADCSRB_ADTS             (0x07U << 0) /* (RW) ADC auto trigger source selection */
#else
#define CHIP_REGFLD_ADCSRB_ACME             (0x01U << 6) /* (RW)   Analog comparator multiplexer enable */
#endif

#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLDVAL_ADCSRB_ACME0_AIN0    0
#define CHIP_REGFLDVAL_ADCSRB_ACME0_ADCMUX  1
#define CHIP_REGFLDVAL_ADCSRB_ACME0_AMP0    2

#define CHIP_REGFLDVAL_ADCSRB_ACME1_AIN2    0
#define CHIP_REGFLDVAL_ADCSRB_ACME1_ADCMUX  1
#define CHIP_REGFLDVAL_ADCSRB_ACME1_AMP1    2

#define CHIP_REGFLDVAL_ADCSRB_ACTS_AC0      0
#define CHIP_REGFLDVAL_ADCSRB_ACTS_AC1      1

#else
#define CHIP_REGFLDVAL_ADCSRB_ACME_AIN1     0
#define CHIP_REGFLDVAL_ADCSRB_ACME_ADCMUX   1
#endif

#define CHIP_REGFLDVAL_ADCSRB_ADTS_CONT     0 /* Continuous conversion mode */
#define CHIP_REGFLDVAL_ADCSRB_ADTS_AC       1 /* Analog Comparator */
#define CHIP_REGFLDVAL_ADCSRB_ADTS_EXT_INT0 2 /* External interrupt 0 */
#define CHIP_REGFLDVAL_ADCSRB_ADTS_TC0_OCMA 3 /* Timer counter 0 compare match A */
#define CHIP_REGFLDVAL_ADCSRB_ADTS_TC0_OV   4 /* Timer counter 0 overflow */
#define CHIP_REGFLDVAL_ADCSRB_ADTS_TC1_OCMB 5 /* Timer counter 1 compare match B */
#define CHIP_REGFLDVAL_ADCSRB_ADTS_TC1_OV   6 /* Timer counter 1 overflow */
#define CHIP_REGFLDVAL_ADCSRB_ADTS_TC1_CAP  7 /* Timer Counter 1 Input Capture Event */


/* ---------------- ADMUX – ADC Multiplexing Control Register ----------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_ADMUX_REFS_10           (0x03U << 6) /* (RW) Reference Selection bits 1..0 */
#else
#define CHIP_REGFLD_ADMUX_REFS              (0x03U << 6) /* (RW) Reference Selection */
#endif
#define CHIP_REGFLD_ADMUX_ADLAR             (0x03U << 5) /* (RW) The conversion result left-justified enable */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_ADMUX_CHMUX             (0x1FU << 0) /* (RW) ADC input source selection */
#else
#define CHIP_REGFLD_ADMUX_CHMUX             (0x0FU << 0) /* (RW) ADC input source selection */
#endif


#define CHIP_REGFLDVAL_ADMUX_REFS_AREF      0 /* AREF, Internal Vref turned off */
#define CHIP_REGFLDVAL_ADMUX_REFS_AVCC      1 /* AVCC with external capacitor at AREF pin */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLDVAL_ADMUX_REFS_2_048V    2 /* Internal 2.048V Voltage Reference */
#define CHIP_REGFLDVAL_ADMUX_REFS_1_024V    3 /* Internal 1.024V Voltage Reference */
#define CHIP_REGFLDVAL_ADMUX_REFS_4_096V    4 /* Internal 4.096V Voltage Reference */
#else
#define CHIP_REGFLDVAL_ADMUX_REFS_1_1V      3 /* Internal 1.1V Voltage Reference with external capacitor at AREF pin */
#endif

#define CHIP_REGFLDVAL_ADMUX_ADLAR_RJ       0 /* Conversion result is right-justified */
#define CHIP_REGFLDVAL_ADMUX_ADLAR_LJ       1 /* Conversion result is left-justified */

#define CHIP_REGFLDVAL_ADMUX_CHMUX_PC0      0x00
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PC1      0x01
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PC2      0x02
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PC3      0x03
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PC4      0x04
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PC5      0x05
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PE1      0x06
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PE3      0x07
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PC7      0x09
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PF0      0x0A
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PE6      0x0B
#define CHIP_REGFLDVAL_ADMUX_CHMUX_PE7      0x0C
#define CHIP_REGFLDVAL_ADMUX_CHMUX_VDO1_5   0x08 /* Internal voltage divider circuit 1/5 output */
#define CHIP_REGFLDVAL_ADMUX_CHMUX_VDO4_5   0x0E /* Internal voltage divider circuit 4/5 output */
#define CHIP_REGFLDVAL_ADMUX_CHMUX_IVREF    0x0D /* Internal voltage reference */
#define CHIP_REGFLDVAL_ADMUX_CHMUX_AGND     0x0F /* Analog ground */
#define CHIP_REGFLDVAL_ADMUX_CHMUX_DACO     0x10 /* Internal DAC output */
#else
#define CHIP_REGFLDVAL_ADMUX_CHMUX_TEMP     0x08 /* Temperature sensor */
#define CHIP_REGFLDVAL_ADMUX_CHMUX_IVREF    0x0E /* 1.1V (VBG) */
#define CHIP_REGFLDVAL_ADMUX_CHMUX_AGND     0x0F /* Analog ground */
#endif


/* ---------------- ADCSRC – ADC Control and Status Register C ---------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_ADCSRC_OFEN             (0x01U << 7) /* (RW) Offset compensation enabled */
#define CHIP_REGFLD_ADCSRC_SPN              (0x01U << 5) /* (RW) ADC conversion input polarity control */
#define CHIP_REGFLD_ADCSRC_AMEN             (0x01U << 4) /* (RW) Channel automatic monitoring enable */
#define CHIP_REGFLD_ADCSRC_SPD              (0x01U << 2) /* (RW) ADC high-speed conversion mode */
#define CHIP_REGFLD_ADCSRC_DIFS             (0x01U << 1) /* (RW) ADC conversion from internal differential amplifier */
#define CHIP_REGFLD_ADCSRC_ADTM             (0x01U << 1) /* (RW) Test mode, output internal reference voltage on AVREF port */
#endif

/* ---------------- DIDR0 – Digital Input Disable Control Register 0 ---------*/
#define CHIP_REGFLD_DIDR0_PE3D              (0x01U << 7) /* (RW) Disable PE3 digital input function */
#define CHIP_REGFLD_DIDR0_PE1D              (0x01U << 6) /* (RW) Disable PE1 digital input function */
#define CHIP_REGFLD_DIDR0_PC5D              (0x01U << 5) /* (RW) Disable PC5 digital input function */
#define CHIP_REGFLD_DIDR0_PC4D              (0x01U << 4) /* (RW) Disable PC4 digital input function */
#define CHIP_REGFLD_DIDR0_PC3D              (0x01U << 3) /* (RW) Disable PC3 digital input function */
#define CHIP_REGFLD_DIDR0_PC2D              (0x01U << 2) /* (RW) Disable PC2 digital input function */
#define CHIP_REGFLD_DIDR0_PC1D              (0x01U << 1) /* (RW) Disable PC1 digital input function */
#define CHIP_REGFLD_DIDR0_PC0D              (0x01U << 0) /* (RW) Disable PC0 digital input function */

/* ---------------- DIDR1 – Digital Input Disable Control Register 1 ---------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_DIDR1_PE7D              (0x01U << 7) /* (RW) Disable PE7 digital input function */
#define CHIP_REGFLD_DIDR1_PE6D              (0x01U << 6) /* (RW) Disable PE6 digital input function */
#define CHIP_REGFLD_DIDR1_PE0D              (0x01U << 5) /* (RW) Disable PE0 digital input function */
#define CHIP_REGFLD_DIDR1_C0PD              (0x01U << 4) /* (RW) Disable AC0P digital input function (LQFP48) */
#define CHIP_REGFLD_DIDR1_PF0D              (0x01U << 3) /* (RW) Disable PF0 digital input function */
#define CHIP_REGFLD_DIDR1_PC7D              (0x01U << 2) /* (RW) Disable PC7 digital input function */
#endif
#define CHIP_REGFLD_DIDR1_PD7D              (0x01U << 1) /* (RW) Disable PD7 digital input function */
#define CHIP_REGFLD_DIDR1_PD6D              (0x01U << 0) /* (RW) Disable PD6 digital input function */

#ifdef CHIP_LGT8FX8P
/* ---------------- DIDR2 – Digital Input Disable Control Register 2 ---------*/
#define CHIP_REGFLD_DIDR2_PB5D              (0x01U << 6) /* (RW) Disable PB5 digital input function */

/* ---------------- ADCSRD – ADC Control and Status Register D ---------------*/
#define CHIP_REGFLD_ADCSRD_BGEN             (0x01U << 7) /* (RW) Internal reference global enable */
#define CHIP_REGFLD_ADCSRD_REFS_2           (0x01U << 6) /* (RW) Reference Selection bit 2 */
#define CHIP_REGFLD_ADCSRD_IVSEL            (0x03U << 4) /* (RW) Internal reference voltage selection (if REFS = AFREF/AVCC) */
#define CHIP_REGFLD_ADCSRD_VDS              (0x07U << 0) /* (RW) Voltage divider circuit input source selection */

#define CHIP_REGFLDVAL_ADCSRD_IVSEL_1_024V  0
#define CHIP_REGFLDVAL_ADCSRD_IVSEL_2_048V  1
#define CHIP_REGFLDVAL_ADCSRD_IVSEL_4_096V  2

#define CHIP_REGFLDVAL_ADCSRD_VDS_DIS       0
#define CHIP_REGFLDVAL_ADCSRD_VDS_ADC0      1
#define CHIP_REGFLDVAL_ADCSRD_VDS_ADC1      2
#define CHIP_REGFLDVAL_ADCSRD_VDS_ADC4      3
#define CHIP_REGFLDVAL_ADCSRD_VDS_ADC5      4
#define CHIP_REGFLDVAL_ADCSRD_VDS_AVREF     5
#define CHIP_REGFLDVAL_ADCSRD_VDS_VCC       6

/* ---------------- DAPCR – Differential Op Amp Control Register -------------*/
#define CHIP_REGFLD_DAPCR_DAPEN             (0x01U << 7) /* (RW) Differential amplifier enable */
#define CHIP_REGFLD_DAPCR_GA                (0x03U << 5) /* (RW) Differential Amplifier Gain  */
#define CHIP_REGFLD_DAPCR_DNS               (0x07U << 2) /* (RW) Differential amplifier inverting input input source selection */
#define CHIP_REGFLD_DAPCR_DPS               (0x03U << 0) /* (RW) Differential amplifier non-inverting input input source selection */


#define CHIP_REGFLDVAL_DAPCR_GA_1           0
#define CHIP_REGFLDVAL_DAPCR_GA_8           1
#define CHIP_REGFLDVAL_DAPCR_GA_16          2
#define CHIP_REGFLDVAL_DAPCR_GA_32          3

#define CHIP_REGFLDVAL_DAPCR_DNS_ADC2       0
#define CHIP_REGFLDVAL_DAPCR_DNS_ADC3       1
#define CHIP_REGFLDVAL_DAPCR_DNS_ADC8       2
#define CHIP_REGFLDVAL_DAPCR_DNS_ADC9       3
#define CHIP_REGFLDVAL_DAPCR_DNS_PE0        4
#define CHIP_REGFLDVAL_DAPCR_DNS_ADCMUX     5
#define CHIP_REGFLDVAL_DAPCR_DNS_AGND       6
#define CHIP_REGFLDVAL_DAPCR_DNS_DIS        7

#define CHIP_REGFLDVAL_DAPCR_DPS_ADCMUX     0
#define CHIP_REGFLDVAL_DAPCR_DPS_ADC0       1
#define CHIP_REGFLDVAL_DAPCR_DPS_ADC1       2
#define CHIP_REGFLDVAL_DAPCR_DPS_AGND       3

/* ---------------- ADMSC – ADC Channel Monitoring Status and Control Register -*/
#define CHIP_REGFLD_ADMSC_AMOF              (0x01U << 7) /* (RO) Automatically monitoring event type flag */
#define CHIP_REGFLD_ADMSC_AMFC              (0x0FU << 0) /* (RW) Automatic monitoring of digital filter control (1-15 samples) */

#define CHIP_REGFLDVAL_ADMSC_AMOF_OVF       1 /* overflow */
#define CHIP_REGFLDVAL_ADMSC_AMOF_UNF       0 /* underflow */

#endif

#endif // REGS_ADC_H
