#ifndef CHIP_IRQ_NUMS_H
#define CHIP_IRQ_NUMS_H

#include "gic/v3/chip_gic_irq_nums.h"

#define CHIP_IRQNUM_BOOT                CHIP_GIC_IRQNUM_SPI(0)  /* Used to notify cores on exception condition while boot */
#define CHIP_IRQNUM_DAP                 CHIP_GIC_IRQNUM_SPI(1)  /* DAP Interrupt */
#define CHIP_IRQNUM_SDMA1               CHIP_GIC_IRQNUM_SPI(2)  /* AND of all 48 SDMA1 interrupts (events) from all the channels */
#define CHIP_IRQNUM_GPU3D               CHIP_GIC_IRQNUM_SPI(3)  /* GPU3D Interrupt */
#define CHIP_IRQNUM_SNVS_LP_HP_BTN      CHIP_GIC_IRQNUM_SPI(4)  /* LP OR HP:ON-OFF button press shorter than 5 secs (pulse event) */
#define CHIP_IRQNUM_LCDIF1              CHIP_GIC_IRQNUM_SPI(5)  /* LCDIF1 Interrupt */
#define CHIP_IRQNUM_LCDIF2              CHIP_GIC_IRQNUM_SPI(6)  /* LCDIF2 Interrupt */
#define CHIP_IRQNUM_VPU_G1              CHIP_GIC_IRQNUM_SPI(7)  /* VPU G1 Decoder Interrupt */
#define CHIP_IRQNUM_VPU_G2              CHIP_GIC_IRQNUM_SPI(8)  /* VPU G2 Decoder Interrupt */
#define CHIP_IRQNUM_QOS                 CHIP_GIC_IRQNUM_SPI(9)  /* QOS Interrupt */
#define CHIP_IRQNUM_WDOG3               CHIP_GIC_IRQNUM_SPI(10) /* Watchdog Timer reset */
#define CHIP_IRQNUM_HS_1                CHIP_GIC_IRQNUM_SPI(11) /* HS Interrupt Request */
#define CHIP_IRQNUM_APBHDMA_GPMI        CHIP_GIC_IRQNUM_SPI(12) /* GPMI operation channel 0-3 (OR4) description complete interrupt */
#define CHIP_IRQNUM_NPU                 CHIP_GIC_IRQNUM_SPI(13) /* Machine Learning Processor Interrupt */
#define CHIP_IRQNUM_RAWNAND_BCH         CHIP_GIC_IRQNUM_SPI(14) /* BCH operation complete interrupt */
#define CHIP_IRQNUM_RAWNAND_GPMI_ERR    CHIP_GIC_IRQNUM_SPI(15) /* GPMI operation TIMEOUT ERROR interrupt */
#define CHIP_IRQNUM_ISI_CH0             CHIP_GIC_IRQNUM_SPI(16) /* ISI Camera Channel 0 Interrupt */
#define CHIP_IRQNUM_MIPI_CSI1           CHIP_GIC_IRQNUM_SPI(17) /* MIPI CSI 1 Interrupt */
#define CHIP_IRQNUM_MIPI_DSI            CHIP_GIC_IRQNUM_SPI(18) /* MIPI DSI Interrupt */
#define CHIP_IRQNUM_SNVS_HP_SRTC_NTZ    CHIP_GIC_IRQNUM_SPI(19) /* SRTC Consolidated Interrupt. Non TZ. */
#define CHIP_IRQNUM_SNVS_HP_SRTC_TZ     CHIP_GIC_IRQNUM_SPI(20) /* SRTC Security Interrupt. TZ. */
#define CHIP_IRQNUM_CSU_ALARM           CHIP_GIC_IRQNUM_SPI(21) /* CSU Interrupt Request. Indicates to the processor that one or more alarm inputs were asserted */
#define CHIP_IRQNUM_USDHC1              CHIP_GIC_IRQNUM_SPI(22) /* uSDHC1 Enhanced SDHC Interrupt Request */
#define CHIP_IRQNUM_USDHC2              CHIP_GIC_IRQNUM_SPI(23) /* uSDHC2 Enhanced SDHC Interrupt Request */
#define CHIP_IRQNUM_USDHC3              CHIP_GIC_IRQNUM_SPI(24) /* uSDHC3 Enhanced SDHC Interrupt Request */
#define CHIP_IRQNUM_GPU2D               CHIP_GIC_IRQNUM_SPI(25) /* GPU2D Interrupt */
#define CHIP_IRQNUM_UART1               CHIP_GIC_IRQNUM_SPI(26) /* UART1 ORed interrupt */
#define CHIP_IRQNUM_UART2               CHIP_GIC_IRQNUM_SPI(27) /* UART2 ORed interrupt */
#define CHIP_IRQNUM_UART3               CHIP_GIC_IRQNUM_SPI(28) /* UART3 ORed interrupt */
#define CHIP_IRQNUM_UART4               CHIP_GIC_IRQNUM_SPI(29) /* UART4 ORed interrupt */
#define CHIP_IRQNUM_VPU                 CHIP_GIC_IRQNUM_SPI(30) /* VPU Encoder Interrupt */
#define CHIP_IRQNUM_ECSPI1              CHIP_GIC_IRQNUM_SPI(31) /* eCSPI1 interrupt request line to the core. */
#define CHIP_IRQNUM_ECSPI2              CHIP_GIC_IRQNUM_SPI(32) /* eCSPI2 interrupt request line to the core. */
#define CHIP_IRQNUM_ECSPI3              CHIP_GIC_IRQNUM_SPI(33) /* eCSPI3 interrupt request line to the core. */
#define CHIP_IRQNUM_SDMA3               CHIP_GIC_IRQNUM_SPI(34) /* AND of all 48 SDMA3 interrupts (events) from all the channels */
#define CHIP_IRQNUM_I2C1                CHIP_GIC_IRQNUM_SPI(35) /* I2C1 Interrupt */
#define CHIP_IRQNUM_I2C2                CHIP_GIC_IRQNUM_SPI(36) /* I2C2 Interrupt */
#define CHIP_IRQNUM_I2C3                CHIP_GIC_IRQNUM_SPI(37) /* I2C3 Interrupt */
#define CHIP_IRQNUM_I2C4                CHIP_GIC_IRQNUM_SPI(38) /* I2C4 Interrupt */
#define CHIP_IRQNUM_RDC                 CHIP_GIC_IRQNUM_SPI(39) /* RDC Interrupt */
#define CHIP_IRQNUM_USB1                CHIP_GIC_IRQNUM_SPI(40) /* USB1 Interrupt */
#define CHIP_IRQNUM_USB2                CHIP_GIC_IRQNUM_SPI(41) /* USB2 Interrupt */
#define CHIP_IRQNUM_ISI_CH1             CHIP_GIC_IRQNUM_SPI(42) /* ISI Camera Channel 1 Interrupt */
#define CHIP_IRQNUM_HDMI_TX             CHIP_GIC_IRQNUM_SPI(43) /* HDMI TX Subsystem Interrupt */
#define CHIP_IRQNUM_MICFIL_VOICE_EVT    CHIP_GIC_IRQNUM_SPI(44) /* Digital Microphone interface voice activity detector event interrupt */
#define CHIP_IRQNUM_MICFIL_VOICE_ERR    CHIP_GIC_IRQNUM_SPI(45) /* Digital Microphone interface voice activity detector error interrupt */
#define CHIP_IRQNUM_GPT6                CHIP_GIC_IRQNUM_SPI(46) /* OR of GPT Rollover interrupt line, Input Capture 1 & 2 lines, Output Compare 1, 2 & 3 Interrupt lines */
#define CHIP_IRQNUM_SCTR_0              CHIP_GIC_IRQNUM_SPI(47) /* System Counter Interrupt [0] */
#define CHIP_IRQNUM_SCTR_1              CHIP_GIC_IRQNUM_SPI(48) /* System Counter Interrupt [1] */
#define CHIP_IRQNUM_ANAMIX_TMU          CHIP_GIC_IRQNUM_SPI(49) /* TempSensor (Temperature alarm OR Temperature critical alarm). */
#define CHIP_IRQNUM_SAI3                CHIP_GIC_IRQNUM_SPI(50) /* OR4: SAI3 Receive Interrupt, Receive Async Interrupt,  Transmit Interrupt,  Transmit Async Interrupt */
#define CHIP_IRQNUM_GPT5                CHIP_GIC_IRQNUM_SPI(51) /* OR of GPT Rollover interrupt line, Input Capture 1 & 2 lines, Output Compare 1, 2 & 3 Interrupt lines */
#define CHIP_IRQNUM_GPT4                CHIP_GIC_IRQNUM_SPI(52) /* OR of GPT Rollover interrupt line, Input Capture 1 & 2 lines, Output Compare 1, 2 & 3 Interrupt lines */
#define CHIP_IRQNUM_GPT3                CHIP_GIC_IRQNUM_SPI(53) /* OR of GPT Rollover interrupt line, Input Capture 1 & 2 lines, Output Compare 1, 2 & 3 Interrupt lines */
#define CHIP_IRQNUM_GPT2                CHIP_GIC_IRQNUM_SPI(54) /* OR of GPT Rollover interrupt line, Input Capture 1 & 2 lines, Output Compare 1, 2 & 3 Interrupt lines */
#define CHIP_IRQNUM_GPT1                CHIP_GIC_IRQNUM_SPI(55) /* OR of GPT Rollover interrupt line, Input Capture 1 & 2 lines, Output Compare 1, 2 & 3 Interrupt lines */
#define CHIP_IRQNUM_GPIO1_INT7          CHIP_GIC_IRQNUM_SPI(56) /* Active HIGH Interrupt from INT7 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT6          CHIP_GIC_IRQNUM_SPI(57) /* Active HIGH Interrupt from INT6 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT5          CHIP_GIC_IRQNUM_SPI(58) /* Active HIGH Interrupt from INT5 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT4          CHIP_GIC_IRQNUM_SPI(59) /* Active HIGH Interrupt from INT4 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT3          CHIP_GIC_IRQNUM_SPI(60) /* Active HIGH Interrupt from INT3 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT2          CHIP_GIC_IRQNUM_SPI(61) /* Active HIGH Interrupt from INT2 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT1          CHIP_GIC_IRQNUM_SPI(62) /* Active HIGH Interrupt from INT1 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT0          CHIP_GIC_IRQNUM_SPI(63) /* Active HIGH Interrupt from INT0 from GPIO */
#define CHIP_IRQNUM_GPIO1_INT0_15       CHIP_GIC_IRQNUM_SPI(64) /* Combined interrupt indication for GPIO1 signal 0 throughout 15 */
#define CHIP_IRQNUM_GPIO1_INT16_31      CHIP_GIC_IRQNUM_SPI(65) /* Combined interrupt indication for GPIO1 signal 16 throughout 31 */
#define CHIP_IRQNUM_GPIO2_INT0_15       CHIP_GIC_IRQNUM_SPI(66) /* Combined interrupt indication for GPIO2 signal 0 throughout 15 */
#define CHIP_IRQNUM_GPIO2_INT16_31      CHIP_GIC_IRQNUM_SPI(67) /* Combined interrupt indication for GPIO2 signal 16 throughout 31 */
#define CHIP_IRQNUM_GPIO3_INT0_15       CHIP_GIC_IRQNUM_SPI(68) /* Combined interrupt indication for GPIO3 signal 0 throughout 15 */
#define CHIP_IRQNUM_GPIO3_INT16_31      CHIP_GIC_IRQNUM_SPI(69) /* Combined interrupt indication for GPIO3 signal 16 throughout 31 */
#define CHIP_IRQNUM_GPIO4_INT0_15       CHIP_GIC_IRQNUM_SPI(70) /* Combined interrupt indication for GPIO4 signal 0 throughout 15 */
#define CHIP_IRQNUM_GPIO4_INT16_31      CHIP_GIC_IRQNUM_SPI(71) /* Combined interrupt indication for GPIO4 signal 16 throughout 31 */
#define CHIP_IRQNUM_GPIO5_INT0_15       CHIP_GIC_IRQNUM_SPI(72) /* Combined interrupt indication for GPIO5 signal 0 throughout 15 */
#define CHIP_IRQNUM_GPIO5_INT16_31      CHIP_GIC_IRQNUM_SPI(73) /* Combined interrupt indication for GPIO5 signal 16 throughout 31 */
#define CHIP_IRQNUM_ISP1                CHIP_GIC_IRQNUM_SPI(74) /* OR: ISP1 ISP, SMIA, MIPI1, MIPI2 Interrupts */
#define CHIP_IRQNUM_ISP2                CHIP_GIC_IRQNUM_SPI(75) /* OR: ISP2 ISP, SMIA, MIPI1, MIPI2 Interrupts */
#define CHIP_IRQNUM_I2C5                CHIP_GIC_IRQNUM_SPI(76) /* I2C5 Interrupt */
#define CHIP_IRQNUM_I2C6                CHIP_GIC_IRQNUM_SPI(77) /* I2C6 Interrupt */
#define CHIP_IRQNUM_WDOG1               CHIP_GIC_IRQNUM_SPI(78) /* Watchdog 1 Timer reset */
#define CHIP_IRQNUM_WDOG2               CHIP_GIC_IRQNUM_SPI(79) /* Watchdog 2 Timer reset */
#define CHIP_IRQNUM_MIPI_CSI2           CHIP_GIC_IRQNUM_SPI(80) /* MIPI CSI 2 Interrupt */
#define CHIP_IRQNUM_PWM1                CHIP_GIC_IRQNUM_SPI(81) /* Cumulative interrupt line. OR of Rollover Interrupt line, Compare Interrupt line and FIFO Waterlevel crossing interrupt line */
#define CHIP_IRQNUM_PWM2                CHIP_GIC_IRQNUM_SPI(82) /* Cumulative interrupt line. OR of Rollover Interrupt line, Compare Interrupt line and FIFO Waterlevel crossing interrupt line */
#define CHIP_IRQNUM_PWM3                CHIP_GIC_IRQNUM_SPI(83) /* Cumulative interrupt line. OR of Rollover Interrupt line, Compare Interrupt line and FIFO Waterlevel crossing interrupt line */
#define CHIP_IRQNUM_PWM4                CHIP_GIC_IRQNUM_SPI(84) /* Cumulative interrupt line. OR of Rollover Interrupt line, Compare Interrupt line and FIFO Waterlevel crossing interrupt line */
#define CHIP_IRQNUM_CCMSRCGPCMIX_CCM1   CHIP_GIC_IRQNUM_SPI(85) /* CCM, Interrupt Request 1 */
#define CHIP_IRQNUM_CCMSRCGPCMIX_CCM2   CHIP_GIC_IRQNUM_SPI(86) /* CCM, Interrupt Request 2 */
#define CHIP_IRQNUM_CCMSRCGPCMIX_GPC1   CHIP_GIC_IRQNUM_SPI(87) /* GPC, Interrupt Request 1 */
#define CHIP_IRQNUM_MU1_A53             CHIP_GIC_IRQNUM_SPI(88) /* Interrupt to A53 (A53,M7 MU) */
#define CHIP_IRQNUM_CCMSRCGPCMIX_SRC    CHIP_GIC_IRQNUM_SPI(89) /* SRC interrupt request */
#define CHIP_IRQNUM_SAI5_6              CHIP_GIC_IRQNUM_SPI(90) /* OR4: SAI5/SAI6 Receive Interrupt, Receive Async Interrupt,  Transmit Interrupt,  Transmit Async Interrupt */
#define CHIP_IRQNUM_CAAM_RTIC           CHIP_GIC_IRQNUM_SPI(91) /* RTIC Interrupt  */
#define CHIP_IRQNUM_CPU_PERF            CHIP_GIC_IRQNUM_SPI(92) /* Performance Unit Interrupts from Quad-A53 platform */
#define CHIP_IRQNUM_CPU_CTI             CHIP_GIC_IRQNUM_SPI(93) /* CTI trigger outputs from Quad-A53 platform */
#define CHIP_IRQNUM_CCMSRCGPCMIX_WDOG   CHIP_GIC_IRQNUM_SPI(94) /* Combined CPU WDOG interrupts (4x) out of SRC. */
#define CHIP_IRQNUM_SAI1                CHIP_GIC_IRQNUM_SPI(95) /* OR4: SAI1 Receive Interrupt, Receive Async Interrupt,  Transmit Interrupt,  Transmit Async Interrupt */
#define CHIP_IRQNUM_SAI2                CHIP_GIC_IRQNUM_SPI(96) /* OR4: SAI2 Receive Interrupt, Receive Async Interrupt,  Transmit Interrupt,  Transmit Async Interrupt */
#define CHIP_IRQNUM_MU1_CM7             CHIP_GIC_IRQNUM_SPI(97) /* Interrupt to M7 (A53, M7 MU) */
#define CHIP_IRQNUM_DDR_PERF_SCRUBBER   CHIP_GIC_IRQNUM_SPI(98) /* DRAM controller Interrupt for DRAM controller's performance monitor OR DRAM controller Scrubber Interrupt indicating one full address range sweep*/
#define CHIP_IRQNUM_DDR_DFI_ERR         CHIP_GIC_IRQNUM_SPI(99) /* DRAM Controller Error Interrupt for DFI error */
#define CHIP_IRQNUM_DEWARP              CHIP_GIC_IRQNUM_SPI(100) /* Dewarp Interrupt */
#define CHIP_IRQNUM_CPU_AXI_ERR         CHIP_GIC_IRQNUM_SPI(101) /* Error indicator for AXI transaction with a write response error condition. */
#define CHIP_IRQNUM_CPU_ECC_ERR         CHIP_GIC_IRQNUM_SPI(102) /* Error indicator for L2 RAM double-bit ECC error. */
#define CHIP_IRQNUM_SDMA2               CHIP_GIC_IRQNUM_SPI(103) /* AND of all 48 SDMA2 interrupts (events) from all the channels */
#define CHIP_IRQNUM_SJC                 CHIP_GIC_IRQNUM_SPI(104) /* Interrupt triggered by SJC register */
#define CHIP_IRQNUM_CAAM_0              CHIP_GIC_IRQNUM_SPI(105) /* CAAM interrupt queue for JQ */
#define CHIP_IRQNUM_CAAM_1              CHIP_GIC_IRQNUM_SPI(106) /* CAAM interrupt queue for JQ */
#define CHIP_IRQNUM_QSPI                CHIP_GIC_IRQNUM_SPI(107) /* Flexspi Interrupt */
#define CHIP_IRQNUM_TZASC               CHIP_GIC_IRQNUM_SPI(108) /* TZASC (PL380) interrupt */
#define CHIP_IRQNUM_MICFIL              CHIP_GIC_IRQNUM_SPI(109) /* Digital Microphone interface interrupt */
#define CHIP_IRQNUM_MICFIL_ERR          CHIP_GIC_IRQNUM_SPI(110) /* Digital Microphone interface error interrupt */
#define CHIP_IRQNUM_SAI7                CHIP_GIC_IRQNUM_SPI(111) /* OR4: SAI7 Receive Interrupt, Receive Async Interrupt,  Transmit Interrupt,  Transmit Async Interrupt */
#define CHIP_IRQNUM_PERFMON1            CHIP_GIC_IRQNUM_SPI(112) /* perfmon1 general interrupt */
#define CHIP_IRQNUM_PERFMON2            CHIP_GIC_IRQNUM_SPI(113) /* perfmon1 general interrupt */
#define CHIP_IRQNUM_CAAM_2              CHIP_GIC_IRQNUM_SPI(114) /* CAAM interrupt queue for JQ */
#define CHIP_IRQNUM_CAAM_REC_ERR        CHIP_GIC_IRQNUM_SPI(115) /* Recoverable error interrupt */
#define CHIP_IRQNUM_HS_0                CHIP_GIC_IRQNUM_SPI(116) /* HS Interrupt Request */
#define CHIP_IRQNUM_CM7_CTI             CHIP_GIC_IRQNUM_SPI(117) /* CTI trigger outputs from CM7 platform */
#define CHIP_IRQNUM_ENET1_RX_TX_DONE_0  CHIP_GIC_IRQNUM_SPI(118) /* OR4: MAC 0 Receive Buffer Done, Receive Frame Done, Transmit Buffer Done, Transmit Frame Done */
#define CHIP_IRQNUM_ENET1_RX_TX_DONE_1  CHIP_GIC_IRQNUM_SPI(119) /* OR4: MAC 0 Receive Buffer Done, Receive Frame Done, Transmit Buffer Done, Transmit Frame Done */
#define CHIP_IRQNUM_ENET1               CHIP_GIC_IRQNUM_SPI(120) /* OR: MAC 0 Periodic Timer Overflow, Time Stamp Available, Payload Receive Error,  Transmit FIFO Underrun .... */
#define CHIP_IRQNUM_ENET1_PTP           CHIP_GIC_IRQNUM_SPI(121) /* MAC 0 1588 Timer Interrupt – synchronous */
#define CHIP_IRQNUM_ASRC                CHIP_GIC_IRQNUM_SPI(122) /* ASRC Interrupt */
#define CHIP_IRQNUM_PCIE_CTRL1_0        CHIP_GIC_IRQNUM_SPI(123) /* Coming from GLUE logic, of set/reset FF, driven by PCIE signals. */
#define CHIP_IRQNUM_PCIE_CTRL1_1        CHIP_GIC_IRQNUM_SPI(124) /* Coming from GLUE logic, of set/reset FF, driven by PCIE signals. */
#define CHIP_IRQNUM_PCIE_CTRL1_2        CHIP_GIC_IRQNUM_SPI(125) /* Coming from GLUE logic, of set/reset FF, driven by PCIE signals. */
#define CHIP_IRQNUM_PCIE_CTRL1_3        CHIP_GIC_IRQNUM_SPI(126) /* Coming from GLUE logic, of set/reset FF, driven by PCIE signals. */
#define CHIP_IRQNUM_PCIE_CTRL1_CH_63_32 CHIP_GIC_IRQNUM_SPI(127) /* Channels [63:32] interrupts requests. */
#define CHIP_IRQNUM_AUDIO_XCVR_0        CHIP_GIC_IRQNUM_SPI(128) /* eARC Interrupt 0 */
#define CHIP_IRQNUM_AUDIO_XCVR_1        CHIP_GIC_IRQNUM_SPI(129) /* eARC Interrupt 1 */
#define CHIP_IRQNUM_AUD2HTX             CHIP_GIC_IRQNUM_SPI(130) /* Audio to HDMI TX Audio Link Master Interrupt */
#define CHIP_IRQNUM_EDMA1_ERR           CHIP_GIC_IRQNUM_SPI(131) /* Audio Subsystem eDMA Error Interrupt */
#define CHIP_IRQNUM_EDMA1_CH_15_0       CHIP_GIC_IRQNUM_SPI(132) /* Audio Subsystem eDMA Channel Interrupts, Logical OR of channels [15:0] */
#define CHIP_IRQNUM_EDMA1_CH_31_16      CHIP_GIC_IRQNUM_SPI(133) /* Audio Subsystem eDMA Channel Interrupts, Logical OR of channels [31:16] */
#define CHIP_IRQNUM_ENET_QOS_PMT        CHIP_GIC_IRQNUM_SPI(134) /* ENET QOS TSN Interrupt from PMT */
#define CHIP_IRQNUM_ENET_QOS            CHIP_GIC_IRQNUM_SPI(135) /* OR4: ENET QOS TSN LPI RX exit Interrupt, Host System Interrupt, Host System RX Channel Interrupts, Logical OR of channels */
#define CHIP_IRQNUM_MU2_A53             CHIP_GIC_IRQNUM_SPI(136) /* Interrupt to A53 (A53, Audio Processor MU) */
#define CHIP_IRQNUM_MU2_DSP             CHIP_GIC_IRQNUM_SPI(137) /* Interrupt to Audio Processor (A53, Audio Processor MU) */
#define CHIP_IRQNUM_MU3_CM7             CHIP_GIC_IRQNUM_SPI(138) /* Interrupt to M7 (M7, Audio Processor MU) */
#define CHIP_IRQNUM_MU3_DSP             CHIP_GIC_IRQNUM_SPI(139) /* Interrupt to Audio Processor (M7, Audio Processor MU) */
#define CHIP_IRQNUM_PCIE_CTRL1_MSG      CHIP_GIC_IRQNUM_SPI(140) /* RC/EP message transaction Interrupt */
#define CHIP_IRQNUM_PCIE_CTRL1_PME_ERR  CHIP_GIC_IRQNUM_SPI(141) /* RC/EP PME Message and Error Interrupt */
#define CHIP_IRQNUM_CAN_FD1             CHIP_GIC_IRQNUM_SPI(142) /* CAN-FD1 Interrupt from busoff, Interrupt from CAN line error, ORed interrupts from ipi_int_MB, Rx/Tx warning Interrupt, wake up, match in PN, Busoff done, error */
#define CHIP_IRQNUM_CAN_FD1_ERR         CHIP_GIC_IRQNUM_SPI(143) /* CAN-FD1 Correctable error interrupt, Non correctable error int host, Non correctable error int internal */
#define CHIP_IRQNUM_CAN_FD2             CHIP_GIC_IRQNUM_SPI(144) /* CAN-FD2 Interrupt from busoff, Interrupt from CAN line error, ORed interrupts from ipi_int_MB, Rx/Tx warning Interrupt, wake up, match in PN, Busoff done, error */
#define CHIP_IRQNUM_CAN_FD2_ERR         CHIP_GIC_IRQNUM_SPI(145) /* CAN-FD2 Correctable error interrupt, Non correctable error int host, Non correctable error int internal */
#define CHIP_IRQNUM_AUDIO_XCVR_WUP      CHIP_GIC_IRQNUM_SPI(146) /* eARC PHY - SPDIF wakeup interrupt */
#define CHIP_IRQNUM_DDR_ERR             CHIP_GIC_IRQNUM_SPI(147) /* DRAM Controller Error Interrupt for address protection fault, correctable ECC error detected, uncorrectable ECC error detected */
#define CHIP_IRQNUM_USB1_WUP            CHIP_GIC_IRQNUM_SPI(148) /* USB1 Wake-up Interrupt */
#define CHIP_IRQNUM_USB2_WUP            CHIP_GIC_IRQNUM_SPI(149) /* USB2 Wake-up Interrupt */

#define CHIP_IRQNUM_MAX                 CHIP_GIC_IRQNUM_SPI(159)





#endif // CHIP_IRQ_NUMS_H
