#ifndef CHIP_GIC_REGS_H
#define CHIP_GIC_REGS_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t CTLR;                 /* Offset: 0x000 (R/W) Distributor Control Register */
    __I  uint32_t TYPER;                /* Offset: 0x004 (R/ ) Interrupt Controller Type Register */
    __I  uint32_t IIDR;                 /* Offset: 0x008 (R/ ) Distributor Implementer Identification Register */
    __I  uint32_t TYPER2;               /* Offset: 0x004 (R/ ) Interrupt Controller Type Register 2 */
    __IO uint32_t STATUSR;              /* Offset: 0x010 (R/W) Error Reporting Status Register, optional */
    uint32_t RESERVED1[11];
    __O  uint32_t SETSPI_NSR;           /* Offset: 0x040 ( /W) Set SPI Register */
    uint32_t RESERVED2;
    __O  uint32_t CLRSPI_NSR;           /* Offset: 0x048 ( /W) Clear SPI Register */
    uint32_t RESERVED3;
    __O  uint32_t SETSPI_SR;            /* Offset: 0x050 ( /W) Set SPI, Secure Register */
    uint32_t RESERVED4;
    __O  uint32_t CLRSPI_SR;            /* Offset: 0x058 ( /W) Clear SPI, Secure Register */
    uint32_t RESERVED5[9];
    __IO uint32_t IGROUPR[32];          /* Offset: 0x080 (R/W) Interrupt Group Registers */
    __IO uint32_t ISENABLER[32];        /* Offset: 0x100 (R/W) Interrupt Set-Enable Registers */
    __IO uint32_t ICENABLER[32];        /* Offset: 0x180 (R/W) Interrupt Clear-Enable Registers */
    __IO uint32_t ISPENDR[32];          /* Offset: 0x200 (R/W) Interrupt Set-Pending Registers */
    __IO uint32_t ICPENDR[32];          /* Offset: 0x280 (R/W) Interrupt Clear-Pending Registers */
    __IO uint32_t ISACTIVER[32];        /* Offset: 0x300 (R/W) Interrupt Set-Active Registers */
    __IO uint32_t ICACTIVER[32];        /* Offset: 0x380 (R/W) Interrupt Clear-Active Registers */
    __IO uint32_t IPRIORITYR[255];      /* Offset: 0x400 (R/W) Interrupt Priority Registers */
    uint32_t RESERVED6;
    __IO uint32_t  ITARGETSR[255];      /* Offset: 0x800 (R/W) Interrupt Targets Registers */
    uint32_t RESERVED7;
    __IO uint32_t ICFGR[64];            /* Offset: 0xC00 (R/W) Interrupt Configuration Registers */
    __IO uint32_t IGRPMODR[32];         /* Offset: 0xD00 (R/W) Interrupt Group Modifier Registers */
    uint32_t RESERVED8[32];
    __IO uint32_t NSACR[64];            /* Offset: 0xE00 (R/W) Non-secure Access Control Registers */
    __O  uint32_t SGIR;                 /* Offset: 0xF00 ( /W) Software Generated Interrupt Register */
    uint32_t RESERVED9[3];
    __IO uint32_t CPENDSGIR[4];         /* Offset: 0xF10 (R/W) SGI Clear-Pending Registers */
    __IO uint32_t SPENDSGIR[4];         /* Offset: 0xF20 (R/W) SGI Set-Pending Registers */
    uint32_t RESERVED10[5236];
    __IO uint64_t IROUTER[988];         /* Offset: 0x6100(R/W) Interrupt Routing Registers */
}  CHIP_REGS_GICD_T;

typedef struct {
    /* GIC physical LPI Redistributor register map (RD_base) */
    __IO uint32_t CTLR;                 /* Offset: 0x000 (RW) Redistributor Control Register */
    __I  uint32_t IIDR;                 /* Offset: 0x004 (RO) Redistributor Implementer Identification Register */
    __I  uint64_t TYPER;                /* Offset: 0x008 (RO) Redistributor Type Register */
    __IO uint32_t STATUSR;              /* Offset: 0x010 (RW) Error Reporting Status Register, optional */
    __IO uint32_t WAKER;                /* Offset: 0x014 (RW) Redistributor Wake Register */
    __IO uint32_t MPAMIDR;              /* Offset: 0x018 (RO) Report maximum PARTID and PMG Register */
    __IO uint32_t PARTIDR;              /* Offset: 0x01C (RW) Set PARTID and PMG Register */
    uint32_t RESERVED2[7];
    __O uint64_t SETLPIR;               /* Offset: 0x040 (WO) Set LPI Pending Register */
    __O uint64_t CLRLPIR;               /* Offset: 0x048 (WO) Clear LPI Pending Register */
    uint32_t RESERVED3[8];
    __IO uint64_t PROPBASER;            /* Offset: 0x070 (RW) Redistributor Properties Base Address Register */
    __IO uint64_t PENDBASER;            /* Offset: 0x078 (RW) Redistributor LPI Pending Table Base Address Register */
    uint32_t RESERVED4[8];
    __O uint64_t INVLPIR;               /* Offset: 0x0A0 (WO) Redistributor Invalidate LPI Register */
    uint32_t RESERVED5[2];
    __O uint64_t INVALLR;               /* Offset: 0x0B0 (WO) Redistributor Invalidate All Register */
    uint32_t RESERVED6[2];
    __I uint64_t SYNCR;                 /* Offset: 0x0C0 (RO) Redistributor Synchronize Register */
    uint32_t RESERVED7[2];               // +0x00C8 - RESERVED
    uint32_t RESERVED8[12];          // +0x00D0 - RESERVED
    __O uint64_t MOVLPIR;          // +0x0100 - WO - IMP DEF
    uint32_t RESERVED9[2];           // +0x0108 - RESERVED
    __O uint64_t MOVALLR;          // +0x0110 - WO - IMP DEF
    uint32_t RESERVED10[2];          // +0x0118 - RESERVED

    /* GIC Redistributor register map for the SGI and PPI registers (SGI_base) */
    uint32_t RESERVED11[32];          // +0x0000 - RESERVED
    __IO uint32_t IGROUPR[3];       // +0x0080 - RW - Interrupt Group Registers (Security Registers in GICv1)
    uint32_t RESERVED12[29];          // +0x008C - RESERVED
    __IO uint32_t ISENABLER[3];     // +0x0100 - RW - Interrupt Set-Enable Registers
    uint32_t RESERVED13[29];          // +0x010C - RESERVED
    __IO uint32_t ICENABLER[3];     // +0x0180 - RW - Interrupt Clear-Enable Registers
    uint32_t RESERVED14[29];          // +0x018C - RESERVED
    __IO uint32_t ISPENDR[3];       // +0x0200 - RW - Interrupt Set-Pending Registers
    uint32_t RESERVED15[29];          // +0x020C - RESERVED
    __IO uint32_t ICPENDR[3];       // +0x0280 - RW - Interrupt Clear-Pending Registers
    uint32_t RESERVED16[29];          // +0x028C - RESERVED
    __IO uint32_t ISACTIVER[3];     // +0x0300 - RW - Interrupt Set-Active Register
    uint32_t RESERVED17[29];          // +0x030C - RESERVED
    __IO uint32_t ICACTIVER[3];     // +0x0380 - RW - Interrupt Clear-Active Register
    uint32_t RESERVED18[29];          // +0x018C - RESERVED
    __IO uint32_t IPRIORITYR[24];   // +0x0400 - RW - Interrupt Priority Registers
    uint32_t RESERVED19[488];         // +0x0460 - RESERVED
    __IO uint32_t ICFGR[6];         // +0x0C00 - RW - Interrupt Configuration Registers
    uint32_t RESERVED20[58];     // +0x0C18 - RESERVED
    __IO uint32_t IGRPMODR[3];      // +0x0D00 - RW - Interrupt Group Modifier Register
    uint32_t RESERVED21[61];     // +0x0D0C - RESERVED
    __IO uint32_t NSACR;            // +0x0E00 - RW - Non-secure Access Control Register
} CHIP_REGS_GICR_T;


#define CHIP_REGS_GICD           ((CHIP_REGS_GICD_T *)CHIP_MEMRGN_ADDR_GICD)
#define CHIP_REGS_GICR           ((CHIP_REGS_GICR_T *)CHIP_MEMRGN_ADDR_GICR)

/*====================== Distributor =========================================*/
/*********** Distributor Control Register (GICD_CTLR) *************************/
#define CHIP_REGFLD_GICD_CTLR_RWP                   (0x00000001UL << 31U)  /* (RO) Register Write Pending */
#define CHIP_REGFLD_GICD_CTLR_E1NWF                 (0x00000001UL <<  7U)  /* (RW) Enable 1 of N Wakeup Functionality. */
#define CHIP_REGFLD_GICD_CTLR_DS                    (0x00000001UL <<  6U)  /* (RW) Disable Security. */
#define CHIP_REGFLD_GICD_CTLR_ARE_NS                (0x00000001UL <<  5U)  /* (RW) Affinity Routing Enable, Non-secure state. */
#define CHIP_REGFLD_GICD_CTLR_ARE_S                 (0x00000001UL <<  4U)  /* (RW) Affinity Routing Enable, Secure state. */
#define CHIP_REGFLD_GICD_CTLR_EN_GRP1_S             (0x00000001UL <<  2U)  /* (RW) Enable Secure Group 1 interrupts. */
#define CHIP_REGFLD_GICD_CTLR_EN_GRP1_NS            (0x00000001UL <<  1U)  /* (RW) Enable Non-secure Group 1 interrupts. */
#define CHIP_REGFLD_GICD_CTLR_EN_GRP0               (0x00000001UL <<  0U)  /* (RW) Enable Group 0 interrupts. */
#define CHIP_REGMSK_GICD_CTLR_RESERVED              (0x7FFFFF08UL)


/***********  Interrupt Controller Type Register (GICD_TYPER) *****************/
#define CHIP_REGFLD_GICD_TYPER_ESPI_RANGE           (0x0000001FUL << 27U)  /* (RO) Maximum INTID in the Extended SPI range (if ESPI=1) */
#define CHIP_REGFLD_GICD_TYPER_RSS                  (0x00000001UL << 26U)  /* (RO) Range Selector Support. The IRI supports targeted SGIs with affinity level 0 values of 0 - 15 (0) or 0 - 255. */
#define CHIP_REGFLD_GICD_TYPER_NO1N                 (0x00000001UL << 25U)  /* (RO) Whether 1 of N SPI interrupts are supported. */
#define CHIP_REGFLD_GICD_TYPER_A3V                  (0x00000001UL << 24U)  /* (RO) Affinity 3 valid. Whether the Distributor supports nonzero values of Affinity level 3. */
#define CHIP_REGFLD_GICD_TYPER_ID_BITS              (0x0000001FUL << 19U)  /* (RO) The number of interrupt identifier bits supported, minus one. */
#define CHIP_REGFLD_GICD_TYPER_DVIS                 (0x00000001UL << 18U)  /* (RO) GICv4 - whether the implementation supports Direct Virtual LPI injection. */
#define CHIP_REGFLD_GICD_TYPER_LPIS                 (0x00000001UL << 17U)  /* (RO) Whether the implementation supports LPIs. */
#define CHIP_REGFLD_GICD_TYPER_MBIS                 (0x00000001UL << 16U)  /* (RO) Whether the implementation supports message-based interrupts by writing to Distributor registers. */
#define CHIP_REGFLD_GICD_TYPER_LPI_NUM              (0x0000001FUL << 11U)  /* (RO) Number of supported LPIs (0 -> =ID_BITS, else 2^(LPI_NUM+1)-1) */
#define CHIP_REGFLD_GICD_TYPER_SEC_EXTN             (0x00000001UL << 10U)  /* (RO) Whether the GIC implementation supports two Security states. */
#define CHIP_REGFLD_GICD_TYPER_ESPI                 (0x00000001UL <<  8U)  /* (RO) Extended SPI. */
#define CHIP_REGFLD_GICD_TYPER_CPU_NUM              (0x00000007UL <<  5U)  /* (RO) Number of PEs that can be used when affinity routing is not enabled, minus 1. */
#define CHIP_REGFLD_GICD_TYPER_IT_LINE_NUM          (0x0000001FUL <<  0U)  /* (RO) Maximum SPI supported (32*(N+1) - 1) */

/***********  Interrupt Controller Type Register 2 (GICD_TYPER2) *************/
#define CHIP_REGFLD_GICD_TYPER2_NAS_SGI_CAP         (0x00000001UL <<  8U)  /* (RO) Whether SGIs can be configured to not have an active state. */
#define CHIP_REGFLD_GICD_TYPER2_VIL                 (0x00000001UL <<  7U)  /* (RO) Whether 16 bits of vPEID are implemented.. */
#define CHIP_REGFLD_GICD_TYPER2_VID                 (0x0000001FUL <<  0U)  /* (RO) Number of bits is equal to the bits of vPEID minus one (if VIL=1) */



/*====================== Redistributor =======================================*/
/*********** Redistributor Control Register (GICR_CTLR) ***********************/
#define CHIP_REGFLD_GICR_CTLR_UWW                   (0x00000001UL << 31U)  /* (RO) Upstream Write Pending */
#define CHIP_REGFLD_GICR_CTLR_DPG1_S                (0x00000001UL << 26U)  /* (RW) Disable Processor selection for Group 1 Secure interrupts. */
#define CHIP_REGFLD_GICR_CTLR_DPG1_NS               (0x00000001UL << 25U)  /* (RW) Disable Processor selection for Group 1 Non-secure interrupts. */
#define CHIP_REGFLD_GICR_CTLR_DPG0                  (0x00000001UL << 24U)  /* (RW) Disable Processor selection for Group 0 interrupts. */
#define CHIP_REGFLD_GICR_CTLR_RWP                   (0x00000001UL <<  3U)  /* (RO) Register Write Pending.. */
#define CHIP_REGFLD_GICR_CTLR_IR                    (0x00000001UL <<  2U)  /* (RO) LPI invaldiate registers supported. */
#define CHIP_REGFLD_GICR_CTLR_CES                   (0x00000001UL <<  1U)  /* (RO) Clear Enable Supported. */
#define CHIP_REGFLD_GICR_CTLR_EN_LPI                (0x00000001UL <<  0U)  /* (RW) LPI support is enabled. */

/*********** Redistributor Implementer Identification Register (GICR_IIDR) ****/
#define CHIP_REGFLD_GICR_IIDR_PRODUCT_ID            (0x000000FFUL << 24U)  /* (RO) An IMPLEMENTATION DEFINED product identifier. */
#define CHIP_REGFLD_GICR_IIDR_VARIANT               (0x0000000FUL << 16U)  /* (RO) An IMPLEMENTATION DEFINED variant number. */
#define CHIP_REGFLD_GICR_IIDR_REVISION              (0x0000000FUL << 12U)  /* (RO) An IMPLEMENTATION DEFINED revision number. */
#define CHIP_REGFLD_GICR_IIDR_IMPLEMENTER           (0x00000FFFUL <<  0U)  /* (RO) JEP106 code of the company that implemented the Redistributor. */

/*********** Redistributor Type Register (GICR_TYPER) ************************/
#define CHIP_REGFLD_GICR_TYPER_AFFIITY_VALUE        (0x000000FFULL << 32U) /* (RO) The identity of the PE associated with this Redistributor. */
#define CHIP_REGFLD_GICR_TYPER_PPI_NUM              (0x0000001FULL << 27U) /* (RO) GICv3p1: Maximum PPI INTID that a GIC implementation can support (0 - 31, 1 - 1087, 2 - 1119) */
#define CHIP_REGFLD_GICR_TYPER_VSGI                 (0x00000001ULL << 26U) /* (RO) GICv4p1: Whether vSGIs are supported. */
#define CHIP_REGFLD_GICR_TYPER_COMMON_LPI_AFF       (0x00000003ULL << 24U) /* (RO) The affinity level at which Redistributors share an LPI Configuration table */
#define CHIP_REGFLD_GICR_TYPER_CPU_NUM              (0x0000FFFFULL <<  8U) /* (RO) A unique identifier for the PE. */
#define CHIP_REGFLD_GICR_TYPER_RVPEID               (0x00000001ULL <<  7U) /* (RO) GICv4p1: Indicates how the resident vPE is specified. */
#define CHIP_REGFLD_GICR_TYPER_MPAM                 (0x00000001ULL <<  6U) /* (RO) MPAM support */
#define CHIP_REGFLD_GICR_TYPER_DPGS                 (0x00000001ULL <<  5U) /* (RO) GICR_CTLR.DPG support. */
#define CHIP_REGFLD_GICR_TYPER_LAST                 (0x00000001ULL <<  4U) /* (RO) Whether this Redistributor is the highest-numbered Redistributor in a series of contiguous Redistributor pages. */
#define CHIP_REGFLD_GICR_TYPER_DIRECT_LPI           (0x00000001ULL <<  3U) /* (RO) Whether this Redistributor supports direct injection of LPIs. */
#define CHIP_REGFLD_GICR_TYPER_DIRTY                (0x00000001ULL <<  2U) /* (RO) Controls the functionality of GICR_VPENDBASER.Dirty. */
#define CHIP_REGFLD_GICR_TYPER_VLPIS                (0x00000001ULL <<  1U) /* (RO) Whether the GIC implementation supports virtual LPIs and the direct injection of virtual LPIs. */
#define CHIP_REGFLD_GICR_TYPER_PLPIS                (0x00000001ULL <<  0U) /* (RO) Whether the GIC implementation supports physical LPIs */

#define CHIP_REGFLDVAL_GICR_TYPER_COMMON_LPI_AFF_ALL    0 /* All Redistributors must share an LPI Configuration table */
#define CHIP_REGFLDVAL_GICR_TYPER_COMMON_LPI_AFF_3      1 /* All Redistributors with the same Aff3 value must share an LPI Configuration table. */
#define CHIP_REGFLDVAL_GICR_TYPER_COMMON_LPI_AFF_2      2 /* All Redistributors with the same Aff3.Aff2 value must share an LPI Configuration table. */
#define CHIP_REGFLDVAL_GICR_TYPER_COMMON_LPI_AFF_1      3 /* All Redistributors with the same Aff3.Aff2.Aff1 value must share an LPI Configuration table. */


/*********** Redistributor Wake Register (GICR_WAKER) *************************/
#define CHIP_REGFLD_GICR_WAKER_CHILDREN_ASLEEP      (0x00000001UL <<  2U)  /* (RO) Whether the connected PE is quiescent */
#define CHIP_REGFLD_GICR_WAKER_PROCESSOR_SLEEP      (0x00000001UL <<  1U)  /* (RW) Whether the Redistributor can assert the WakeRequest signal: */



/*====================== CPU interface  ======================================*/
/* ICC_SRE (EL1, EL2, EL3), Interrupt Controller System Register Enable register */
#define CHIP_REGFLD_GIC_ICC_SRE_SRE                 (0x00000001ULL <<  0U) /* (RO/RW) System Register Enable (0 - memory, 1 - system regs) */
#define CHIP_REGFLD_GIC_ICC_SRE_DFB                 (0x00000001ULL <<  1U) /* (RW) Disable FIQ bypass */
#define CHIP_REGFLD_GIC_ICC_SRE_DIB                 (0x00000001ULL <<  2U) /* (RW) Disable IRQ bypass */
#define CHIP_REGFLD_GIC_ICC_SRE_EN                  (0x00000001ULL <<  3U) /* (RW) Enables lower Exception level access */


/* ICC_CTLR (EL1,EL3) Interrupt Controller Control Register */
#define CHIP_REGFLD_GIC_ICC_CTLR_EXTRANGE           (0x00000001ULL << 19U) /* (RO) Extended INTID range */
#define CHIP_REGFLD_GIC_ICC_CTLR_RSS                (0x00000001ULL << 18U) /* (RO) Range Selector Support for SGI (0-15 or 0-255)*/
#define CHIP_REGFLD_GIC_ICC_CTLR_NDS                (0x00000001ULL << 17U) /* (RO) EL3: Disable Security not supported */
#define CHIP_REGFLD_GIC_ICC_CTLR_A3V                (0x00000001ULL << 15U) /* (RO) Affinity 3 Valid  */
#define CHIP_REGFLD_GIC_ICC_CTLR_SEIS               (0x00000001ULL << 14U) /* (RO) Local generation of SEIs support */
#define CHIP_REGFLD_GIC_ICC_CTLR_ID_BITS            (0x00000007ULL << 11U) /* (RO) Identifier bits (0 - 16, 1 - 24) */
#define CHIP_REGFLD_GIC_ICC_CTLR_PRI_BITS           (0x00000007ULL <<  8U) /* (RO) Priority bit count minus 1 */
#define CHIP_REGFLD_GIC_ICC_CTLR_PMHE               (0x00000001ULL <<  6U) /* (RW) Priority Mask Hint Enable */
#define CHIP_REGFLD_GIC_ICC_CTLR_RM                 (0x00000001ULL <<  5U) /* (RW) EL3: Routing Modifier */
#define CHIP_REGFLD_GIC_ICC_CTLR_EOI_MODE_EL1NS     (0x00000001ULL <<  4U) /* (RW) EL3: EOI mode for interrupts handled at Non-secure EL1 and EL2 (1 - separate prio drop and deactivation) */
#define CHIP_REGFLD_GIC_ICC_CTLR_EOI_MODE_EL1S      (0x00000001ULL <<  3U) /* (RW) EL3: EOI mode for interrupts handled at Secure EL1 and EL2 (1 - separate prio drop and deactivation) */
#define CHIP_REGFLD_GIC_ICC_CTLR_EOI_MODE_EL3       (0x00000001ULL <<  2U) /* (RW) EL3: EOI mode for interrupts handled at EL3 (1 - separate prio drop and deactivation) */
#define CHIP_REGFLD_GIC_ICC_CTLR_CBPR_EL1NS         (0x00000001ULL <<  1U) /* (RW) Common Binary Point Register, EL1 Non-secure (1 - same BPR0 is for Group0 and 1) */
#define CHIP_REGFLD_GIC_ICC_CTLR_CBPR_EL1S          (0x00000001ULL <<  0U) /* (RW) Common Binary Point Register, EL1 Secure (1 - same BPR0 is for Group0 and 1) */


/* ICC_PMR (EL1) Interrupt Controller Interrupt Priority Mask */
#define CHIP_REGFLD_GIC_ICC_PMR_PRIORITY            (0x000000FFULL <<  0U) /* (RW) The priority mask level for the CPU interface. If the priority of an interrupt is higher than the value indicated by this field, the interface signals the interrupt to the PE. */

/* ICC_IGRPEN0/1 (EL1) Interrupt Controller Interrupt Group 0/1 Enable register */
#define CHIP_REGFLD_GIC_ICC_IGRPEN_ENABLE           (0x00000001ULL <<  0U) /* (RW) Enables Group interrupts. */

/* ICC_IGRPEN1 (EL3) Interrupt Controller Interrupt Group 1 Enable register */
#define CHIP_REGFLD_GIC_ICC_IGRPEN1_ENABLE_S        (0x00000001ULL <<  1U) /* (RW) Enables Group 1 interrupts for the Secure state. */
#define CHIP_REGFLD_GIC_ICC_IGRPEN1_ENABLE_NS       (0x00000001ULL <<  0U) /* (RW) Enables Group 1 interrupts for the Non-secure state. */

/* ICC_BPR0/ICC_BPR1 (EL1) Interrupt Controller Binary Point Register 0/1 */
#define CHIP_REGFLD_GIC_ICC_BPR_BINARY_POINT        (0x00000007ULL <<  0U) /* (RW) how the 8-bit interrupt priority field is split into a group priority field, that determines interrupt preemption, and a subpriority field */

/* ICC_SGI0R. SGI1R, ASGI1R (EL1) Software Generated Interrupt Group 0/1 (same sec)/1 (other sec) Register */
#define CHIP_REGFLD_GIC_ICC_SGIR_AFF3               (0x000000FFULL << 48U) /* (WO) The affinity 3 value of the affinity path of the cluster for which SGI interrupts will be generated. */
#define CHIP_REGFLD_GIC_ICC_SGIR_RS                 (0x0000000FULL << 44U) /* (WO) RangeSelector (TargetList[n] represents aff0 value ((RS * 16) + n)) when ICC_CTLR_EL1.RSS==1 and GICD_TYPER.RSS==0. */
#define CHIP_REGFLD_GIC_ICC_SGIR_IRM                (0x0000000FULL << 40U) /* (WO) Interrupt Routing Mode (0 -  by Aff3.Aff2.Aff1.<target list>, 1 - all excluding self). */
#define CHIP_REGFLD_GIC_ICC_SGIR_AFF2               (0x000000FFULL << 32U) /* (WO) The affinity 2 value of the affinity path of the cluster for which SGI interrupts will be generated. */
#define CHIP_REGFLD_GIC_ICC_SGIR_INTD               (0x0000000FULL << 24U) /* (WO) The INTID of the SGI. */
#define CHIP_REGFLD_GIC_ICC_SGIR_AFF1               (0x000000FFULL << 16U) /* (WO) The affinity 1 value of the affinity path of the cluster for which SGI interrupts will be generated. */
#define CHIP_REGFLD_GIC_ICC_SGIR_TARGET_LIST        (0x0000FFFFULL <<  0U) /* (WO) Target List. The set of PEs for which SGI interrupts will be generated. Each bit corresponds to the PE within a cluster with an Affinity 0 value equal to the bit number. */


#endif // CHIP_GIC_REGS_H

