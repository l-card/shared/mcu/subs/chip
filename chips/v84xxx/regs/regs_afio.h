#ifndef CHIP_V84XXXX_REGS_AFIO_H_
#define CHIP_V84XXXX_REGS_AFIO_H_

#include <stdint.h>

/* Alternate Function I/O */
typedef struct {
    __IO uint32_t EVCTRL;     /* Alternate event control register,                          Address offset: 0x00  */
    __IO uint32_t MAP;        /* AF remap and debug I/O configuration register,             Address offset: 0x04  */
    union {
        struct {
            __IO uint32_t EXTIC1; /* Alternate external interrupt configuration register 1, Address offset: 0x08  */
            __IO uint32_t EXTIC2; /* Alternate external interrupt configuration register 2, Address offset: 0x0C  */
            __IO uint32_t EXTIC3; /* Alternate external interrupt configuration register 3, Address offset: 0x10  */
            __IO uint32_t EXTIC4; /* Alternate external interrupt configuration register 4, Address offset: 0x14  */
        };
        __IO uint32_t EXTIC[4];
    };
    union {
        struct {
            __IO uint32_t MAP2; /* AF remap and debug I/O configuration register 2,         Address offset: 0x1C  */
            __IO uint32_t MAP3; /* AF remap and debug I/O configuration register 3,         Address offset: 0x20  */
            __IO uint32_t MAP4; /* AF remap and debug I/O configuration register 4,         Address offset: 0x24  */
            __IO uint32_t MAP5; /* AF remap and debug I/O configuration register 5,         Address offset: 0x28  */
            __IO uint32_t MAP6; /* AF remap and debug I/O configuration register 6,         Address offset: 0x2C  */
            __IO uint32_t MAP7; /* AF remap and debug I/O configuration register 7,         Address offset: 0x30  */
            __IO uint32_t MAP8; /* AF remap and debug I/O configuration register 8,         Address offset: 0x34  */
        };
        __IO uint32_t MAPH[7];
    };

} CHIP_REGS_AFIO_T;


#define CHIP_REGS_AFIO          ((CHIP_REGS_AFIO_T *) CHIP_MEMRGN_ADDR_PERIPH_AFIO)

/*********** Alternate event control register (AFIO_EVCTRL) *******************/
#define CHIP_REGFLD_AFIO_EVCTRL_EVOUT_EN            (0x00000001UL <<  7U) /* (RW) Event output enable */
#define CHIP_REGFLD_AFIO_EVCTRL_PORT                (0x00000007UL <<  4U) /* (RW) Event output port selection */
#define CHIP_REGFLD_AFIO_EVCTRL_PIN                 (0x0000000FUL <<  0U) /* (RW) Event output pin selection */
#define CHIP_REGMSK_AFIO_EVCTRL_RESERVED            (0xFFFFFF00UL)

/*********** AF remap and debug I/O configuration register (AFIO_MAP) *********/
#define CHIP_REGFLD_AFIO_MAP_SPI1_H                 (0x00000001UL << 31U) /* (RW)  SPI1 remapping bit [1] */
#define CHIP_REGFLD_AFIO_MAP_SPI3                   (0x00000001UL << 28U) /* (RW)  SPI3 alternate function remapping  */
#define CHIP_REGFLD_AFIO_MAP_SWJTAG                 (0x00000007UL << 24U) /* (RW)  JTAG configuration  */
#define CHIP_REGFLD_AFIO_MAP_ADC1_EXTRG_REG         (0x00000001UL << 18U) /* (RW)  ADC1 external trigger regular conversion remapping  */
#define CHIP_REGFLD_AFIO_MAP_ADC1_EXTRG_INJ         (0x00000001UL << 17U) /* (RW)  ADC1 external trigger injected conversion remapping  */
#define CHIP_REGFLD_AFIO_MAP_TMR5_CH4_IRMP          (0x00000001UL << 16U) /* (RW)  Timer 5 channel 4 internal remapping  */
#define CHIP_REGFLD_AFIO_MAP_CAN                    (0x00000003UL << 13U) /* (RW)  CAN alternate function remapping  */
#define CHIP_REGFLD_AFIO_MAP_TMR4                   (0x00000001UL << 12U) /* (RW)  Timer 4 remapping */
#define CHIP_REGFLD_AFIO_MAP_TMR3                   (0x00000003UL << 10U) /* (RW)  Timer 3 remapping */
#define CHIP_REGFLD_AFIO_MAP_TMR2                   (0x00000003UL <<  8U) /* (RW)  Timer 2 remapping */
#define CHIP_REGFLD_AFIO_MAP_TMR1                   (0x00000003UL <<  6U) /* (RW)  Timer 1 remapping */
#define CHIP_REGFLD_AFIO_MAP_USART3                 (0x00000003UL <<  4U) /* (RW)  USART3 remapping */
#define CHIP_REGFLD_AFIO_MAP_USART2                 (0x00000001UL <<  3U) /* (RW)  USART2 remapping */
#define CHIP_REGFLD_AFIO_MAP_USART1                 (0x00000001UL <<  2U) /* (RW)  USART1 remapping */
#define CHIP_REGFLD_AFIO_MAP_I2C1                   (0x00000001UL <<  1U) /* (RW)  I2C1 remapping */
#define CHIP_REGFLD_AFIO_MAP_SPI1_L                 (0x00000001UL <<  1U) /* (RW)  SPI1 remapping bit [0] */
#define CHIP_REGMSK_AFIO_MAP_RESERVED               (0x68F8800UL)

#define CHIP_REGFLDVAL_AFIO_MAP_SWJTAG_FULL                 0
#define CHIP_REGFLDVAL_AFIO_MAP_SWJTAG_NO_NJTRST            1
#define CHIP_REGFLDVAL_AFIO_MAP_SWJTAG_SWD                  2
#define CHIP_REGFLDVAL_AFIO_MAP_SWJTAG_DIS                  4

#define CHIP_REGFLDVAL_AFIO_MAP_ADC1_EXTRG_REG_EXTINT11     0
#define CHIP_REGFLDVAL_AFIO_MAP_ADC1_EXTRG_REG_TMR8_TRGO    1

#define CHIP_REGFLDVAL_AFIO_MAP_ADC1_EXTRG_INJ_EXTINT15     0
#define CHIP_REGFLDVAL_AFIO_MAP_ADC1_EXTRG_INJ_TMR8_CH4     1

#define CHIP_REGFLDVAL_AFIO_MAP_TMR5_CH4_IRMP_PA3           0
#define CHIP_REGFLDVAL_AFIO_MAP_TMR5_CH4_IRMP_LSI           1

/*********** AF remap and debug I/O configuration register 2 (AFIO_MAP2) ******/
#define CHIP_REGFLD_AFIO_MAP2_EXT_SPIF_EN           (0x00000001UL << 21U) /* (RW)  External SPI flash interface enable */
#define CHIP_REGFLD_AFIO_MAP2_I2C3                  (0x00000001UL << 18U) /* (RW)  I2C3 internal remap */
#define CHIP_REGFLD_AFIO_MAP2_SPI4                  (0x00000001UL << 17U) /* (RW)  SPI4 internal remap */
#define CHIP_REGFLD_AFIO_MAP2_XMC_NADV              (0x00000001UL << 10U) /* (RW)  XMCMADV connect */
#define CHIP_REGFLD_AFIO_MAP2_TMR9                  (0x00000001UL <<  5U) /* (RW)  Timer 9 channel 1/2 internal remap */
#define CHIP_REGMSK_AFIO_MAP2_RESERVED              (0xFFD9FBDF)

/*********** AF remap and debug I/O configuration register 3 (AFIO_MAP3) ******/
#define CHIP_REGFLD_AFIO_MAP3_TMR9_GRMP             (0x0000000FUL <<  0U) /* (RW)  Timer 9 remapping */
#define CHIP_REGMSK_AFIO_MAP3_RESERVED              (0xFFFFFFF0UL)

/*********** AF remap and debug I/O configuration register 4 (AFIO_MAP5) ******/
#define CHIP_REGFLD_AFIO_MAP4_TMR5_CH4_IRMP         (0x00000001UL << 19U) /* (RW)  Timer 5 channel 4 internal remapping */
#define CHIP_REGFLD_AFIO_MAP4_TMR4_GRMP             (0x0000000FUL << 12U) /* (RW)  Timer 4 remapping */
#define CHIP_REGFLD_AFIO_MAP4_TMR3_GRMP             (0x0000000FUL <<  8U) /* (RW)  Timer 3 remapping */
#define CHIP_REGFLD_AFIO_MAP4_TMR2_GRMP             (0x00000007UL <<  4U) /* (RW)  Timer 2 remapping */
#define CHIP_REGFLD_AFIO_MAP4_TMR1_GRMP             (0x0000000FUL <<  0U) /* (RW)  Timer 1 remapping */
#define CHIP_REGMSK_AFIO_MAP4_RESERVED              (0xFFF70080UL)

/*********** AF remap and debug I/O configuration register 5 (AFIO_MAP5) ******/
#define CHIP_REGFLD_AFIO_MAP5_SPI4_GRMP             (0x0000000FUL << 28U) /* (RW)  SPI4 internal remapping */
#define CHIP_REGFLD_AFIO_MAP5_SPI3_GRMP             (0x0000000FUL << 24U) /* (RW)  SPI3 internal remapping */
#define CHIP_REGFLD_AFIO_MAP5_SPI2_GRMP             (0x0000000FUL << 20U) /* (RW)  SPI2 internal remapping */
#define CHIP_REGFLD_AFIO_MAP5_SPI1_GRMP             (0x0000000FUL << 16U) /* (RW)  SPI1 internal remapping */
#define CHIP_REGFLD_AFIO_MAP5_I2C3_GRMP             (0x0000000FUL << 12U) /* (RW)  I2C3 internal remapping */
#define CHIP_REGFLD_AFIO_MAP5_I2C1_GRMP             (0x0000000FUL <<  4U) /* (RW)  I2C1 internal remapping */
#define CHIP_REGFLD_AFIO_MAP5_UART5_GRMP            (0x0000000FUL <<  0U) /* (RW)  USART5 internal remapping */
#define CHIP_REGMSK_AFIO_MAP5_RESERVED              (0x0000F00UL)

/*********** AF remap and debug I/O configuration register 6 (AFIO_MAP6) ******/
#define CHIP_REGFLD_AFIO_MAP6_UART4_GRMP            (0x0000000FUL << 28U) /* (RW)  UART4 remapping */
#define CHIP_REGFLD_AFIO_MAP6_USART3_GRMP           (0x0000000FUL << 24U) /* (RW)  USART3 remapping */
#define CHIP_REGFLD_AFIO_MAP6_USART2_GRMP           (0x0000000FUL << 20U) /* (RW)  USART2 remapping */
#define CHIP_REGFLD_AFIO_MAP6_USART1_GRMP           (0x0000000FUL << 16U) /* (RW)  USART1 remapping */
#define CHIP_REGFLD_AFIO_MAP6_CAN1_GRMP             (0x0000000FUL <<  0U) /* (RW)  CAN1 remapping */
#define CHIP_REGMSK_AFIO_MAP6_RESERVED              (0x0000FFF0UL)

/*********** AF remap and debug I/O configuration register 7 (AFIO_MAP7) ******/
#define CHIP_REGFLD_AFIO_MAP7_XMC_NADV_GRMP         (0x00000001UL << 27U) /* (RW)  XMC_NADV remapping (disable) */
#define CHIP_REGFLD_AFIO_MAP7_XMC_GRMP              (0x00000007UL << 24U) /* (RW)  XMC remapping */
#define CHIP_REGFLD_AFIO_MAP7_SWJTAG_GRMP           (0x00000007UL << 16U) /* (RW)  SWJTAG remapping */
#define CHIP_REGFLD_AFIO_MAP7_ADC1_ETR_GRMP         (0x00000007UL <<  5U) /* (RW)  ADC1 extenal trigger regular conversion remapping */
#define CHIP_REGFLD_AFIO_MAP7_ADC1_ETI_GRMP         (0x00000007UL <<  4U) /* (RW)  ADC1 extenal trigger injected conversion remapping */
#define CHIP_REGFLD_AFIO_MAP7_EXT_SPIF_GEN          (0x00000001UL <<  3U) /* (RW)  External SPI flash interface enable */
#define CHIP_REGMSK_AFIO_MAP7_RESERVED              (0xF0F8FFC7UL)

#define CHIP_REGFLDVAL_AFIO_MAP7_ADC1_ETR_EXTINT11  0
#define CHIP_REGFLDVAL_AFIO_MAP7_ADC1_ETR_TMR8_TRGO 1

#define CHIP_REGFLDVAL_AFIO_MAP7_ADC1_ETI_EXTINT15  0
#define CHIP_REGFLDVAL_AFIO_MAP7_ADC1_ETI_TMR8_CH4  1

/*********** AF remap and debug I/O configuration register 8 (AFIO_MAP8) ******/
#define CHIP_REGFLD_AFIO_MAP8_UART8_GRMP           (0x0000000FUL << 28U) /* (RW)  UART8 remapping */
#define CHIP_REGFLD_AFIO_MAP8_UART7_GRMP           (0x0000000FUL << 24U) /* (RW)  UART7 remapping */
#define CHIP_REGFLD_AFIO_MAP8_USART6_GRMP          (0x0000000FUL << 20U) /* (RW)  USART6 remapping */
#define CHIP_REGMSK_AFIO_MAP8_RESERVED             (0x000FFFFFUL)


#endif // CHIP_V84XXXX_REGS_AFIO_H_
