#include "chip.h"
#include "chip_init_faults.h"

#ifdef CHIP_CFG_PWR_BYPASS_EN
    #define CHIP_PWR_BYPASS_EN CHIP_CFG_PWR_BYPASS_EN
#else
    #define CHIP_PWR_BYPASS_EN 0
#endif

#ifdef CHIP_CFG_PWR_LDO_EN
    #define CHIP_PWR_LDO_EN CHIP_CFG_PWR_LDO_EN
    #if CHIP_PWR_BYPASS_EN
        #error invalid configuration: bypass and ldo modes must not be enabled at the same time
    #endif
#else
    #define CHIP_PWR_LDO_EN !CHIP_PWR_BYPASS_EN
#endif


#if CHIP_CFG_PWR_VOS == CHIP_PWR_VOS3
    #define CHIP_PWR_RVAL_VOS    1
#elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS2
    #define CHIP_PWR_RVAL_VOS    2
#elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS1
    #define CHIP_PWR_RVAL_VOS    3
#elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS0
    #if CHIP_REV_Y_COMPAT_EN
        #error "VOS0 mode was not supported in RevY mcu. Set CHIP_REV_Y_COMPAT_EN to 0 to disable RevY support"
    #else
        #define CHIP_PWR_RVAL_VOS 3
    #endif
#endif


void chip_pwr_init(void) {
    /* в первую очередь настраиваем конфигурацию питания в CR3 (младший байт
     * доступен на запись один раз до следущего сброса питания) */
    /** @todo настройка SMPS для поддерживающих чипов не реализована */

    CHIP_REGS_PWR->CR3 = (CHIP_REGS_PWR->CR3 & 0xF8FEFCC0)
        #if CHIP_PWR_LDO_EN
            | CHIP_REGFLD_PWR_CR3_LDOEN
        #endif
        #if CHIP_PWR_BYPASS_EN
            | CHIP_REGFLD_PWR_CR3_BYPASS
        #endif
            ;

    /* до установки ACTVOSRDY находимся в Run* режиме, где
     * нельзя обращаться к RAM или менять VOS */
    CHIP_INIT_WAIT((CHIP_REGS_PWR->CSR1 & CHIP_REGFLD_PWR_CSR1_ACTVOSRDY) != 0,
                       CHIP_INIT_FAULT_WAIT_ACTVOSRDY, 10000);

    CHIP_REGS_PWR->D3CR = LBITFIELD_SET(CHIP_REGFLD_PWR_D3CR_VOS, CHIP_PWR_RVAL_VOS);

    /* после смены VOS ожидаем завершения, после чего только можно настраивать
     * PLL */
    CHIP_INIT_WAIT((CHIP_REGS_PWR->D3CR & CHIP_REGFLD_PWR_D3CR_VOSRDY) != 0,
                       CHIP_INIT_FAULT_WAIT_D3CR_VOSRDY, 10000);

#if CHIP_CFG_PWR_VOS == CHIP_PWR_VOS0
    chip_per_clk_en(CHIP_PER_ID_SYSCFG);
    CHIP_REGS_SYSCFG->PWRCR |= CHIP_REGFLD_SYSCFG_PWRCR_ODEN;
    CHIP_INIT_WAIT((CHIP_REGS_PWR->D3CR & CHIP_REGFLD_PWR_D3CR_VOSRDY) != 0,
                       CHIP_INIT_FAULT_WAIT_D3CR_VOSRDY, 10000);
    chip_per_clk_dis(CHIP_PER_ID_SYSCFG);
#endif
}


void chip_pwr_bd_cfg_write_enable(void) {
    CHIP_REGS_PWR->CR1 |= CHIP_REGFLD_PWR_CR1_DBP;
}

void chip_pwr_bd_cfg_write_disable(void) {
    CHIP_REGS_PWR->CR1 &= ~CHIP_REGFLD_PWR_CR1_DBP;
}
