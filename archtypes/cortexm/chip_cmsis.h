#ifndef CHIP_CMSIS_H
#define CHIP_CMSIS_H

#include "chip_cmsis_cfg.h"
#include "chip_isr_cmsis.h"

#ifdef CHIP_CORETYPE_CM0P
#include "core_cm0plus.h"               /* Cortex-M0+ processor and core peripherals */
#elif defined CHIP_CORETYPE_CM0
#include "core_cm0.h"               /* Cortex-M0 processor and core peripherals */
#elif defined CHIP_CORETYPE_CM3
#include "core_cm3.h"               /* Cortex-M3 processor and core peripherals */
#elif defined CHIP_CORETYPE_CM4
#include "core_cm4.h"               /* Cortex-M4 processor and core peripherals */
#elif defined CHIP_CORETYPE_CM7
#include "core_cm4.h"               /* Cortex-M7 processor and core peripherals */
#endif

#endif // CHIP_CMSIS_H
