#ifndef STM32_USART_V1_REGS_H
#define STM32_USART_V1_REGS_H

#include "chip_ioreg_defs.h"

/* параметры, которые должны быть определены до включения данного файла в зависимости от возможностей чипа
   CHIP_SUPPORT_USART_OVER8         - поддержка режима oversampling на 8 клоков вместо 16
   CHIP_SUPPORT_USART_ONEBIT        - поддержка режима одного измерения на бит вместо 3
   */

/* Universal Synchronous Asynchronous Receiver Transmitter */
typedef struct {
    __IO uint32_t SR;         /*!< USART Status register,                   Address offset: 0x00 */
    __IO uint32_t DR;         /*!< USART Data register,                     Address offset: 0x04 */
    __IO uint32_t BRR;        /*!< USART Baud rate register,                Address offset: 0x08 */
    __IO uint32_t CR1;        /*!< USART Control register 1,                Address offset: 0x0C */
    __IO uint32_t CR2;        /*!< USART Control register 2,                Address offset: 0x10 */
    __IO uint32_t CR3;        /*!< USART Control register 3,                Address offset: 0x14 */
    __IO uint32_t GTPR;       /*!< USART Guard time and prescaler register, Address offset: 0x18 */
} CHIP_REGS_USART_T;

/*********** USART Status register (USART_SR) *********************************/
#define CHIP_REGFLD_USART_SR_PE                     (0x00000001UL <<  0U) /* (RC) Parity Error */
#define CHIP_REGFLD_USART_SR_FE                     (0x00000001UL <<  1U) /* (RC) Framing Error */
#define CHIP_REGFLD_USART_SR_NE                     (0x00000001UL <<  2U) /* (RC) Noise detected Flag */
#define CHIP_REGFLD_USART_SR_ORE                    (0x00000001UL <<  3U) /* (RC) OverRun Error */
#define CHIP_REGFLD_USART_SR_IDLE                   (0x00000001UL <<  4U) /* (RC) IDLE line detected */
#define CHIP_REGFLD_USART_SR_RXNE                   (0x00000001UL <<  5U) /* (RCW0) Read Data Register Not Empty */
#define CHIP_REGFLD_USART_SR_TC                     (0x00000001UL <<  6U) /* (RCW0) Transmission Complete */
#define CHIP_REGFLD_USART_SR_TXE                    (0x00000001UL <<  7U) /* (RO) Transmit Data Register Empty Flag */
#define CHIP_REGFLD_USART_SR_LBD                    (0x00000001UL <<  8U) /* (RCW0) LIN Break Detection Flag */
#define CHIP_REGFLD_USART_SR_CTS                    (0x00000001UL <<  9U) /* (RCW0) CTS flag */
#define CHIP_REGMSK_USART_SR_RESERVED               (0xFFFFFC00UL)

/*********** USART data register (USART_DR) ***********************************/
#define CHIP_REGFLD_USART_DR_VAL                    (0x000001FFUL <<  0U) /* (RW) Data value */
#define CHIP_REGMSK_USART_DR_RESERVED               (0xFFFFFE00UL)

/*********** USART Baud rate register (USART_BRR) *****************************/
#define CHIP_REGFLD_USART_BRR_DIV                   (0x0000FFFFUL <<  0U) /* (RW) USART  Baud rate register [15:0] */
#define CHIP_REGMSK_USART_BRR_RESERVED              (0xFFFF0000UL)

/*********** USART Control register 1 (USART_CR1) *****************************/
#define CHIP_REGFLD_USART_CR1_SBK                   (0x00000001UL <<  0U) /* (RW) Send Break */
#define CHIP_REGFLD_USART_CR1_RWU                   (0x00000001UL <<  1U) /* (RW) Receiver Mute */
#define CHIP_REGFLD_USART_CR1_RE                    (0x00000001UL <<  2U) /* (RW) Receiver Enable */
#define CHIP_REGFLD_USART_CR1_TE                    (0x00000001UL <<  3U) /* (RW) Transmitter Enable */
#define CHIP_REGFLD_USART_CR1_IDLE_IE               (0x00000001UL <<  4U) /* (RW) IDLE Interrupt Enable */
#define CHIP_REGFLD_USART_CR1_RXNE_IE               (0x00000001UL <<  5U) /* (RW) RDNE Interrupt Enable */
#define CHIP_REGFLD_USART_CR1_TC_IE                 (0x00000001UL <<  6U) /* (RW) TRAC Interrupt Enable */
#define CHIP_REGFLD_USART_CR1_TXE_IE                (0x00000001UL <<  7U) /* (RW) TDE  Interrupt Enable */
#define CHIP_REGFLD_USART_CR1_PE_IE                 (0x00000001UL <<  8U) /* (RW) PERR Interrupt Enable */
#define CHIP_REGFLD_USART_CR1_PS                    (0x00000001UL <<  9U) /* (RW) Parity Selection */
#define CHIP_REGFLD_USART_CR1_PCE                   (0x00000001UL << 10U) /* (RW) Parity Control Enable */
#define CHIP_REGFLD_USART_CR1_WAKE                  (0x00000001UL << 11U) /* (RW) Receiver Wakeup method */
#define CHIP_REGFLD_USART_CR1_M                     (0x00000001UL << 12U) /* (RW) Word length (9 rather than 8) */
#define CHIP_REGFLD_USART_CR1_UE                    (0x00000001UL << 13U) /* (RW) USART enable */
#if CHIP_SUPPORT_USART_OVER8
#define CHIP_REGFLD_USART_CR1_OVER8                 (0x00000001UL << 15U) /* (RW) Oversampling mode (8 rather than 16) */
#endif
#define CHIP_REGMSK_USART_CR1_RESERVED              (0xFFFF4000UL)


/*********** USART Control register 2 (USART_CR2) *****************************/
#define CHIP_REGFLD_USART_CR2_ADDR                  (0x0000000FUL <<  0U) /* (RW) USART node address in this device */
#define CHIP_REGFLD_USART_CR2_LBD_LEN               (0x00000001UL <<  5U) /* (RW) LIN Break Detection Length */
#define CHIP_REGFLD_USART_CR2_LBD_IE                (0x00000001UL <<  6U) /* (RW) LIN Break Detection Interrupt Enable */
#define CHIP_REGFLD_USART_CR2_LBCL                  (0x00000001UL <<  8U) /* (RW) Last Bit Clock pulse */
#define CHIP_REGFLD_USART_CR2_CPHA                  (0x00000001UL <<  9U) /* (RW) Clock Phase */
#define CHIP_REGFLD_USART_CR2_CPOL                  (0x00000001UL << 10U) /* (RW) Clock Polarity */
#define CHIP_REGFLD_USART_CR2_CLK_EN                (0x00000001UL << 11U) /* (RW) Clock Enable */
#define CHIP_REGFLD_USART_CR2_STOP                  (0x00000003UL << 12U) /* (RW) STOP bits */
#define CHIP_REGFLD_USART_CR2_LIN_EN                (0x00000001UL << 14U) /* (RW) LIN mode enable */
#define CHIP_REGMSK_USART_CR2_RESERVED              (0xFFFF8090UL)

#define CHIP_REGFLDVAL_USART_CR2_STOP_1             0
#define CHIP_REGFLDVAL_USART_CR2_STOP_0_5           1
#define CHIP_REGFLDVAL_USART_CR2_STOP_2             2
#define CHIP_REGFLDVAL_USART_CR2_STOP_1_5           3


/*********** USART Control register 3 (USART_CR3) *****************************/
#define CHIP_REGFLD_USART_CR3_ERR_IE              (0x00000001UL <<  0U) /* (RW) Error Interrupt Enable */
#define CHIP_REGFLD_USART_CR3_IR_EN               (0x00000001UL <<  1U) /* (RW) IrDA mode Enable */
#define CHIP_REGFLD_USART_CR3_IR_LP               (0x00000001UL <<  2U) /* (RW) IrDA Low-Power */
#define CHIP_REGFLD_USART_CR3_HDSEL               (0x00000001UL <<  3U) /* (RW) Half-Duplex Selection */
#define CHIP_REGFLD_USART_CR3_NACK                (0x00000001UL <<  4U) /* (RW) SmartCard NACK enable */
#define CHIP_REGFLD_USART_CR3_SC_EN               (0x00000001UL <<  5U) /* (RW) SmartCard mode enable */
#define CHIP_REGFLD_USART_CR3_DMAR                (0x00000001UL <<  6U) /* (RW) DMA Enable Receiver */
#define CHIP_REGFLD_USART_CR3_DMAT                (0x00000001UL <<  7U) /* (RW) DMA Enable Transmitter */
#define CHIP_REGFLD_USART_CR3_RTS_EN              (0x00000001UL <<  8U) /* (RW) RTS Enable */
#define CHIP_REGFLD_USART_CR3_CTS_EN              (0x00000001UL <<  9U) /* (RW) CTS Enable */
#define CHIP_REGFLD_USART_CR3_CTS_IE              (0x00000001UL << 10U) /* (RW) CTS Interrupt Enable */
#if CHIP_SUPPORT_USART_ONEBIT
#define CHIP_REGFLD_USART_CR3_ONEBIT              (0x00000001UL << 11U) /* (RW) One sample bit method enable */
#endif
#define CHIP_REGMSK_USART_CR3_RESERVED            (0xFFFFF000UL)

/*********** USART Guard time and prescaler register (USART_GTPR) *************/
#define CHIP_REGFLD_USART_GTPR_PSC                (0x000000FFUL <<  0U) /* Prescaler value */
#define CHIP_REGFLD_USART_GTPR_GT                 (0x000000FFUL <<  8U) /* Guard time value */

#endif // STM32_USART_V1_REGS_H
