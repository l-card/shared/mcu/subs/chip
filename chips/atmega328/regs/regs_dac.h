#ifndef REGS_DAC_H
#define REGS_DAC_H

#include "chip_devtype_spec_features.h"

#ifdef CHIP_LGT8FX8P
#define CHIP_REG_DACON      _MMIO_BYTE(0xA0) /* DAC Control Register */
#define CHIP_REG_DALR       _MMIO_BYTE(0xA1) /* DAC Data Register */
/* ---------------- DACON – DAC Control Register -----------------------------*/
#define CHIP_REGFLD_DACON_DACEN             (0x01U << 3) /* (RW) DAC enable */
#define CHIP_REGFLD_DACON_DAOE              (0x01U << 2) /* (RW) DAC output to external port PD4 enable */
#define CHIP_REGFLD_DACON_DAVS              (0x03U << 0) /* (RW) AC reference voltage source selection */

#define CHIP_REGFLDVAL_DACON_DAVS_VCC       0
#define CHIP_REGFLDVAL_DACON_DAVS_AVREF     1
#define CHIP_REGFLDVAL_DACON_DAVS_IVREF     2
#define CHIP_REGFLDVAL_DACON_DAVS_DIS       3



#endif

#endif // REGS_DAC_H
