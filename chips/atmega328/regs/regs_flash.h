#ifndef REGS_FLASH_H
#define REGS_FLASH_H

#include "chip_devtype_spec_features.h"

#define CHIP_REG_EEARL      _MMIO_BYTE(0x41) /* The lower 8 bits of the EFLASH/E2PROM access address */
#define CHIP_REG_EEARH      _MMIO_BYTE(0x42) /* The upper 7 bits of the EFLASH/E2PROM access address */
#define CHIP_REG_EEDR       _MMIO_BYTE(0x40) /* E2PCTL data register */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_E2PD0      _MMIO_BYTE(0x40) /* In 16/32-bit mode are used to access the least significant byte */
#define CHIP_REG_E2PD1      _MMIO_BYTE(0x5A)
#define CHIP_REG_E2PD2      _MMIO_BYTE(0x57)
#define CHIP_REG_E2PD3      _MMIO_BYTE(0x5C)
#define CHIP_REG_ECCR       _MMIO_BYTE(0x56) /* FLASH/E2PROM Mode Control Register */
#endif
#define CHIP_REG_EECR       _MMIO_BYTE(0x3F) /* FLASH Access Control Register */

#define CHIP_REG_GPIOR0     _MMIO_BYTE(0x3E) /* General Purpose I/O Register 0 */
#define CHIP_REG_GPIOR1     _MMIO_BYTE(0x4A) /* General Purpose I/O Register 1 */
#define CHIP_REG_GPIOR2     _MMIO_BYTE(0x4B) /* General Purpose I/O Register 2 */



/* ---------------- ECCR – FLASH/E2PROM Mode Control Register  ---------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_ECCR_WEN                (0x01U << 7) /* (W1SC) ECCR Write Enable Control */
#define CHIP_REGFLD_ECCR_EEN                (0x01U << 6) /* (RW) E2PROM enable */
#define CHIP_REGFLD_ECCR_ERN                (0x01U << 5) /* (RW) Writing a 1 will reset the E2PCTL controller */
#define CHIP_REGFLD_ECCR_SWM                (0x01U << 4) /* (RW) Continuous write mode for simulating E2PROM controller operation */
#define CHIP_REGFLD_ECCR_CP1                (0x01U << 3) /* (RW) Page Swap CP1 Area Enable Control */
#define CHIP_REGFLD_ECCR_CP0                (0x01U << 2) /* (RW) Page Swap CP0 Area Enable Control */
#define CHIP_REGFLD_ECCR_ECS                (0x03U << 0) /* (RW) E2PROM space configuration */

#define CHIP_REGFLDVAL_ECCR_ECS_1K          0
#define CHIP_REGFLDVAL_ECCR_ECS_2K          1
#define CHIP_REGFLDVAL_ECCR_ECS_4K          2
#define CHIP_REGFLDVAL_ECCR_ECS_8K          3
#endif


/* ---------------- EECR – FLASH/E2PROM Control Register ---------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_EECR_EEPM                (0x0FU << 4) /* (RW) EFLASH/EEPROM Programming Mode */
#else
#define CHIP_REGFLD_EECR_EEPM                (0x03U << 4) /* (RW) EEPROM Programming Mode */
#endif
#define CHIP_REGFLD_EECR_EERIE               (0x01U << 3) /* (RW) FLASH/E2PROM ready interrupt enable */
#define CHIP_REGFLD_EECR_EEMPE               (0x01U << 2) /* (W1SC) FLASH/E2PROM programming operation enable control */
#define CHIP_REGFLD_EECR_EEPE                (0x01U << 1) /* (RW) FLASH/E2PROM programming operation enable */
#define CHIP_REGFLD_EECR_EERE                (0x01U << 0) /* (RW) E2PROM read enable bit, data will be valid after two system cycles */


#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLDVAL_EECR_EEPM_RW8        0 /* 8-bit mode read/write E2PROM */
#define CHIP_REGFLDVAL_EECR_EEPM_RW16       2 /* 16-bit mode read/write E2PROM */
#define CHIP_REGFLDVAL_EECR_EEPM_RW32       4 /* 32-bit mode read/write E2PROM */
#define CHIP_REGFLDVAL_EECR_EEPM_E2P_ERASE  8 /* E2PROM Erase (Optional Operation) */
#define CHIP_REGFLDVAL_EECR_EEPM_FL_ERASE   9 /* Flash Erase */
#define CHIP_REGFLDVAL_EECR_EEPM_FL_PROG    10 /* Flash Programming */
#define CHIP_REGFLDVAL_EECR_EEPM_RST        11 /* Reset FLASH/E2PROM controller */
#else
#define CHIP_REGFLDVAL_EECR_EEPM_WR_ERASE   0 /* Erase and Write in one operation (Atomic Operation) */
#define CHIP_REGFLDVAL_EECR_EEPM_ERASE      1 /* Erase Only */
#define CHIP_REGFLDVAL_EECR_EEPM_WR         2 /* Write Only */
#endif

#endif // REGS_FLASH_H

