#ifndef CHIP_V84XXXX_REGS_GPIO_H_
#define CHIP_V84XXXX_REGS_GPIO_H_

#include <stdint.h>

/* General Purpose I/O */
typedef struct {
    union {
        struct {
            __IO uint32_t CTRLL;    /* GPIO port configuration register low,   Address offset: 0x00 */
            __IO uint32_t CTRLH;    /* GPIO port configuration register high,  Address offset: 0x04 */
        };
        __IO uint32_t CTRL[2];
    };
    __I  uint32_t IPTDT;    /* GPIO port input data register,          Address offset: 0x08 */
    __IO uint32_t OPTDT;    /* GPIO port output data register,         Address offset: 0x0C */
    __O  uint32_t BSRE;     /* GPIO port bit set/reset register,       Address offset: 0x10 */
    __O  uint32_t BRE;      /* GPIO port bit reset register,           Address offset: 0x14 */
    __IO uint32_t LOCK;     /* GPIO port configuration lock register,  Address offset: 0x18 */
    uint32_t RESERVED1;
    __IO uint32_t SRCTR;    /* GPIO port slew rate control register,   Address offset: 0x20 */
    uint32_t RESERVED2[5];
    __IO uint32_t HDRV;     /* GPIO port huge driven control register, Address offset: 0x3C */
} CHIP_REGS_GPIO_T;


#define CHIP_REGS_GPIO(i)        ((CHIP_REGS_GPIO_T *)(CHIP_MEMRGN_ADDR_PERIPH_GPIOA + (i)*(CHIP_MEMRGN_ADDR_PERIPH_GPIOB - CHIP_MEMRGN_ADDR_PERIPH_GPIOA)))
#define CHIP_REGS_GPIOA          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOA)
#define CHIP_REGS_GPIOB          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOB)
#define CHIP_REGS_GPIOC          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOC)
#define CHIP_REGS_GPIOD          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOD)
#define CHIP_REGS_GPIOE          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOE)



/*********** GPIO port configuration register low/high (GPIO_CTRLL/GPIO_CTRLH) */
#define CHIP_REGFLD_GPIO_CTRL_CONF(pinnum)          (0x00000003UL << (4 * ((pinnum) & 0x7) + 2U)) /* (RW) port configuration */
#define CHIP_REGFLD_GPIO_CTRL_MDE(pinnum)           (0x00000003UL << (4 * ((pinnum) & 0x7) + 0U)) /* (RW) port mode */
#define CHIP_REGNUM_GPIO_CTRL(pinnum)               ((pinnum) / 8)

#define CHIP_REGFLDVAL_GPIO_CTRL_MDE_IN             0 /* Input mode */
#define CHIP_REGFLDVAL_GPIO_CTRL_MDE_OUT_6MA        1 /* Output mode 6 mA strength */
#define CHIP_REGFLDVAL_GPIO_CTRL_MDE_OUT_4MA        2 /* Output mode 4 mA strength */
#define CHIP_REGFLDVAL_GPIO_CTRL_MDE_OUT_15MA       3 /* Output mode 15 mA strength */

#define CHIP_REGFLDVAL_GPIO_CTRL_CONF_IN_ANALOG     0 /* Analog mode */
#define CHIP_REGFLDVAL_GPIO_CTRL_CONF_IN_FLOATING   1 /* Floating input */
#define CHIP_REGFLDVAL_GPIO_CTRL_CONF_IN_PUPD       2 /* Input with pull-up/pull-down */
#define CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_GP_PP     0 /* General purpose output push-pull */
#define CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_GP_OD     1 /* General purpose output open drain */
#define CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_ALT_PP    2 /* Alternate output push-pull */
#define CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_ALT_OD    3 /* Alternate output open drain */

/*********** GPIO port bit set/reset register (GPIO_BSRE) ********************/
#define CHIP_REGFLD_GPIO_BSRE_BRE(pinnum)           (0x00000001UL << (16U + (pinnum))) /* (WO) port reset bit */
#define CHIP_REGFLD_GPIO_BSRE_BST(pinnum)           (0x00000001UL << ( 0U + (pinnum))) /* (WO) port set bit */

/*********** GPIO port configuration lock register (GPIO_LOCK) ***************/
#define CHIP_REGFLD_GPIO_LOCK_KEY                   (0x00000001UL << 16UL)     /* (RW) Lock key */
#define CHIP_REGFLD_GPIO_LOCK(pinnum)               (0x00000001UL << (pinnum)) /* (RW) Port lock bit */

#endif /* CHIP_V84XXXX_REGS_GPIO_H_ */
