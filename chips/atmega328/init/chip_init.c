#include "chip.h"
#if CHIP_LTIMER_EN
    #include "lclock.h"
#endif
/* --------------------- Проверка параметров внешнего осциллятора ----------- */
#if CHIP_CLK_OSC_EN
    #ifndef CHIP_CFG_CLK_OSC_FREQ
        #error "OSC Frequency is not specified"
    #else
        #if (CHIP_CFG_CLK_OSC_FREQ > CHIP_CLK_OSC_HF_FREQ_MAX) || (CHIP_CFG_CLK_OSC_FREQ < CHIP_CLK_OSC_LF_FREQ_MIN)
            #error "OSC Frequency is out of range"
        #endif

        #define CHIP_RVAL_PMCR_OSCMEN   (CHIP_CFG_CLK_OSC_FREQ > CHIP_CLK_OSC_HF_FREQ_MIN)
        #define CHIP_RVAL_PMCR_OSCKEN   !CHIP_RVAL_PMCR_OSCMEN
    #endif
#else
    #define CHIP_RVAL_PMCR_OSCMEN       0
    #define CHIP_RVAL_PMCR_OSCKEN       0
#endif


/* --------------------- Проверка параметров SystemClock -------------------- */
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HFRC_ID
    #define CHIP_RVAL_PMCR_SCLKS    CHIP_REGFLDVAL_PMCR_SCLKS_HFRC
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_LFRC_ID
    #define CHIP_RVAL_PMCR_SCLKS    CHIP_REGFLDVAL_PMCR_SCLKS_LFRC
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_OSC_ID
    #if CHIP_RVAL_PMCR_OSCMEN
        #define CHIP_RVAL_PMCR_SCLKS    CHIP_REGFLDVAL_PMCR_SCLKS_OSC_HF
    #else
        #define CHIP_RVAL_PMCR_SCLKS    CHIP_REGFLDVAL_PMCR_SCLKS_OSC_LF
    #endif
#else
    #error Invalid System Clock Source (must be from HFRC, LFRC or OSC)
#endif

#if !CHIP_CLK_SYS_EN
    #error System clock source is not enabled
#endif

#if CHIP_CLK_SYS_DIV == 1
    #define CHIP_RVAL_CLKPR_CLKPS 0
#elif CHIP_CLK_SYS_DIV == 2
    #define CHIP_RVAL_CLKPR_CLKPS 1
#elif CHIP_CLK_SYS_DIV == 4
    #define CHIP_RVAL_CLKPR_CLKPS 2
#elif CHIP_CLK_SYS_DIV == 8
    #define CHIP_RVAL_CLKPR_CLKPS 3
#elif CHIP_CLK_SYS_DIV == 16
    #define CHIP_RVAL_CLKPR_CLKPS 4
#elif CHIP_CLK_SYS_DIV == 32
    #define CHIP_RVAL_CLKPR_CLKPS 5
#elif CHIP_CLK_SYS_DIV == 64
    #define CHIP_RVAL_CLKPR_CLKPS 6
#elif CHIP_CLK_SYS_DIV == 128
    #define CHIP_RVAL_CLKPR_CLKPS 7
#elif CHIP_CLK_SYS_DIV == 256
    #define CHIP_RVAL_CLKPR_CLKPS 8
#else
    #error invalid sys clock prescaler value
#endif




#if CHIP_CLK_WDT_SRC_ID == CHIP_CLK_HFRC_DIV16_ID
    #define CHIP_RVAL_PMCR_WCLKS CHIP_REGFLDVAL_PMCR_WCLKS_HFRC_D16
#elif CHIP_CLK_WDT_SRC_ID == CHIP_CLK_LFRC_ID
    #define CHIP_RVAL_PMCR_WCLKS CHIP_REGFLDVAL_PMCR_WCLKS_LFRC
#else
    #error Invalid WDT Clock Source (must be HFRC_DIV16 or LFRC)
#endif


/* вызов функции из init3 идет при инциализации, после установки стека и zero_reg,
 * необходимых для C. Так как функция вызывается прямым переходом по метке
 * необходимо объявить как naked */
void chip_init(void) __attribute__ ((naked)) __attribute__ ((section (".init3")));



void chip_init(void) {
#if CHIP_CLK_OSC_EN
    /* разрешение режима генератора */
#if CHIP_CLK_OSC_MODE == CHIP_CLK_EXTMODE_CLOCK
    CHIP_REG_PMX2 = CHIP_REGFLD_PMX2_WCE;//разрешить изменения
    CHIP_REG_PMX2 = CHIP_REGFLD_PMX2_XIEN;//разрешить вход тактовой частоты от кварц. генератора
#endif

    /* для внешнего клока вначале включаем его, а уже потом переключаем источник
     * системного клока */
    CHIP_REG_PMCR |= CHIP_REGFLD_PMCR_PMCE;
    CHIP_REG_PMCR |= LBITFIELD_SET(CHIP_REGFLD_PMCR_OSCM_EN, CHIP_RVAL_PMCR_OSCMEN)
                  | LBITFIELD_SET(CHIP_REGFLD_PMCR_OSCK_EN, CHIP_RVAL_PMCR_OSCKEN);
    /* ожидание стабилизации осциллятора (типично 1 us из документации, но можно переопределить в настройках) */
    chip_delay_clk(CHIP_CLK_OSC_STARTUP_DELAY_US * (CHIP_CLK_CPU_INITIAL_FREQ/1000000));
#endif



    CHIP_REG_PMCR |= CHIP_REGFLD_PMCR_PMCE;
    CHIP_REG_PMCR = LBITFIELD_SET(CHIP_REGFLD_PMCR_RCM_EN, 1) /* изначально работаем от данного источника и его оставляем до переключения системного клока */
            | LBITFIELD_SET(CHIP_REGFLD_PMCR_RCK_EN, CHIP_CLK_LFRC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PMCR_OSCM_EN, CHIP_RVAL_PMCR_OSCMEN)
            | LBITFIELD_SET(CHIP_REGFLD_PMCR_OSCK_EN, CHIP_RVAL_PMCR_OSCKEN)
            | LBITFIELD_SET(CHIP_REGFLD_PMCR_WCLKS, CHIP_RVAL_PMCR_WCLKS)
            | LBITFIELD_SET(CHIP_REGFLD_PMCR_SCLKS, CHIP_RVAL_PMCR_SCLKS)
            ;
    chip_nop();




    CHIP_REG_CLKPR |= CHIP_REGFLD_CLKPR_WCE;
    CHIP_REG_CLKPR = LBITFIELD_SET(CHIP_REGFLD_CLKPR_CLKPS, CHIP_RVAL_CLKPR_CLKPS)
            | LBITFIELD_SET(CHIP_REGFLD_CLKPR_CKO_EN0, CHIP_CLK_CLKO0_EN)
            | LBITFIELD_SET(CHIP_REGFLD_CLKPR_CKO_EN1, CHIP_CLK_CLKO1_EN);

    /* если 32M клок не используется, то после смены системного клока можем его
     * отключить */
#if !CHIP_CLK_HFRC_EN
    CHIP_REG_PMCR |= CHIP_REGFLD_PMCR_PMCE;
    CHIP_REG_PMCR &= ~CHIP_REGFLD_PMCR_RCM_EN;
#endif

    CHIP_REG_PRR = LBITFIELD_SET(CHIP_REGFLD_PRR_PRADC, !CHIP_CLK_ADC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR_PRUART0,    !CHIP_CLK_USART_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR_PRSPI,      !CHIP_CLK_SPI_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR_PRTIM1,     !CHIP_CLK_TIM1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR_PRTIM0,     !CHIP_CLK_TIM0_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR_PRTIM2,     !CHIP_CLK_TIM2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR_PRTWI,      !CHIP_CLK_TWI_EN)
            ;
#ifdef CHIP_LGT8FX8P
    CHIP_REG_PRR1 = LBITFIELD_SET(CHIP_REGFLD_PRR1_PRPCI, !CHIP_CLK_PCI_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR1_PRTIM3,      !CHIP_CLK_TIM3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR1_PREFL,       !CHIP_CLK_FLASH_EN)
            | LBITFIELD_SET(CHIP_REGFLD_PRR1_PRWDT,       !CHIP_CLK_WDT_EN)
            ;


#endif
#if CHIP_LTIMER_EN
    /* разрешение ltimer */
    lclock_init();
#endif
}
