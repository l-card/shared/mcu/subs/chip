/*================================================================================================*
 * Код начальной инициализации LPC43xx
 *================================================================================================*/
#include <stdint.h>
#include <stddef.h>
#include "chip_init.h"

/*================================================================================================*/
/* Настройки стека */
#ifndef STACK_SIZE
#error "STACK_SIZE should be defined in makefile"
#endif
//#define STACK_SIZE          512
//#define STACK_FILLER        0xDEADBEEF

/* Code read protection (в секции .crp_sect) */
/* Code read protection (в секции .crp_sect) */
#define CRP_DISABLED        0xFFFFFFFF
#define CRP_NO_ISP          0x4E697370
#define CRP1                0x12345678
#define CRP2                0x87654321
#define CRP3                0x43218765

#ifndef CRP_MODE
    #define CRP_MODE            CRP_DISABLED
#endif
/*================================================================================================*/

/*================================================================================================*/
/* Объявление символов, на которые ссылается таблица векторов */
/*================================================================================================*/

#define WEAK_DECL(handler_name) \
    void __attribute__ ((__interrupt__)) handler_name(void) __attribute__ ((__weak__, __alias__("dummy_handler")))

static __attribute__ ((__interrupt__)) void dummy_handler(void);

/* Standard Cortex-M interrupts */
void __attribute__ ((__interrupt__)) Reset_ExHandler(void);
WEAK_DECL(NMI_ExHandler);
WEAK_DECL(HardFault_ExHandler);
WEAK_DECL(MMUFault_ExHandler);
WEAK_DECL(BusFault_ExHandler);
WEAK_DECL(UsageFault_ExHandler);
WEAK_DECL(SVCall_ExHandler);
WEAK_DECL(Debug_ExHandler);
WEAK_DECL(PendSV_ExHandler);
void SysTick_IRQHandler(void);

/* Chip specific interrupts */
WEAK_DECL(DAC_IRQHandler);
WEAK_DECL(MX_CORE_IRQHandler);
WEAK_DECL(DMA_IRQHandler);
WEAK_DECL(FLASHEEPROM_IRQHandler);
WEAK_DECL(ETH_IRQHandler);
WEAK_DECL(SDIO_IRQHandler);
WEAK_DECL(LCD_IRQHandler);
WEAK_DECL(USB0_IRQHandler);
WEAK_DECL(USB1_IRQHandler);
WEAK_DECL(SCT_IRQHandler);
WEAK_DECL(RIT_IRQHandler);
WEAK_DECL(TIMER0_IRQHandler);
WEAK_DECL(TIMER1_IRQHandler);
WEAK_DECL(TIMER2_IRQHandler);
WEAK_DECL(TIMER3_IRQHandler);
WEAK_DECL(MCPWM_IRQHandler);
WEAK_DECL(ADC0_IRQHandler);
WEAK_DECL(I2C0_IRQHandler);
WEAK_DECL(I2C1_IRQHandler);
WEAK_DECL(SPI_IRQHandler );
WEAK_DECL(ADC1_IRQHandler);
WEAK_DECL(SSP0_IRQHandler);
WEAK_DECL(SSP1_IRQHandler);
WEAK_DECL(UART0_IRQHandler);
WEAK_DECL(UART1_IRQHandler);
WEAK_DECL(UART2_IRQHandler);
WEAK_DECL(UART3_IRQHandler);
WEAK_DECL(I2S0_IRQHandler);
WEAK_DECL(I2S1_IRQHandler);
WEAK_DECL(SPIFI_IRQHandler);
WEAK_DECL(SGPIO_IRQHandler);
WEAK_DECL(GPIO0_IRQHandler);
WEAK_DECL(GPIO1_IRQHandler);
WEAK_DECL(GPIO2_IRQHandler);
WEAK_DECL(GPIO3_IRQHandler);
WEAK_DECL(GPIO4_IRQHandler);
WEAK_DECL(GPIO5_IRQHandler);
WEAK_DECL(GPIO6_IRQHandler);
WEAK_DECL(GPIO7_IRQHandler);
WEAK_DECL(GINT0_IRQHandler);
WEAK_DECL(GINT1_IRQHandler);
WEAK_DECL(EVRT_IRQHandler);
WEAK_DECL(CAN1_IRQHandler);
WEAK_DECL(ADCHS_IRQHandler);
WEAK_DECL(ATIMER_IRQHandler);
WEAK_DECL(RTC_IRQHandler);
WEAK_DECL(WDT_IRQHandler);
WEAK_DECL(M0SUB_IRQHandler);
WEAK_DECL(CAN0_IRQHandler);
WEAK_DECL(QEI_IRQHandler);

int main(void);
/*================================================================================================*/

/*================================================================================================*/
/* Символы, определенные линкером (в .ld) */
/*================================================================================================*/
extern uint32_t __data_init_tab_start, __data_init_tab_end;
extern uint32_t __bss_init_tab_start, __bss_init_tab_end;
/*================================================================================================*/

/*================================================================================================*/
/* Стек */
__attribute__ ((section(".stackarea"))) uint32_t g_cpu_stack[STACK_SIZE];

/* Таблица векторов прерываний */
typedef void (*t_irq_handler)(void);

__attribute__ ((section(".isr_vector"))) const t_irq_handler g_int_vectors[] =
    {
    /* IRQ# */
    /* SP */    (t_irq_handler)(g_cpu_stack + STACK_SIZE),
    /* RESET */ Reset_ExHandler,
    /* -14 */   NMI_ExHandler,      HardFault_ExHandler,
    /* -12 */   MMUFault_ExHandler, BusFault_ExHandler, UsageFault_ExHandler,
    /*  -9 */   /* reserved */ NULL, NULL, NULL, NULL,
    /*  -5 */   SVCall_ExHandler,   Debug_ExHandler,
    /*  -3 */   /* reserved */ NULL,
    /*  -2 */   PendSV_ExHandler,
    /*  -1 */   SysTick_IRQHandler,

    /*  0 */    DAC_IRQHandler,         MX_CORE_IRQHandler, DMA_IRQHandler,     NULL,
    /*  4 */    FLASHEEPROM_IRQHandler, ETH_IRQHandler,     SDIO_IRQHandler,    LCD_IRQHandler,
    /*  8 */    USB0_IRQHandler,        USB1_IRQHandler,    SCT_IRQHandler,     RIT_IRQHandler,
    /* 12 */    TIMER0_IRQHandler,      TIMER1_IRQHandler,  TIMER2_IRQHandler,  TIMER3_IRQHandler,
    /* 16 */    MCPWM_IRQHandler,       ADC0_IRQHandler,    I2C0_IRQHandler,    I2C1_IRQHandler,
    /* 20 */    SPI_IRQHandler,         ADC1_IRQHandler,    SSP0_IRQHandler,    SSP1_IRQHandler,
    /* 24 */    UART0_IRQHandler,       UART1_IRQHandler,   UART2_IRQHandler,   UART3_IRQHandler,
    /* 28 */    I2S0_IRQHandler,        I2S1_IRQHandler,    SPIFI_IRQHandler,   SGPIO_IRQHandler,
    /* 32 */    GPIO0_IRQHandler,       GPIO1_IRQHandler,   GPIO2_IRQHandler,   GPIO3_IRQHandler,
    /* 36 */    GPIO4_IRQHandler,       GPIO5_IRQHandler,   GPIO6_IRQHandler,   GPIO7_IRQHandler,
    /* 40 */    GINT0_IRQHandler,       GINT1_IRQHandler,   EVRT_IRQHandler,    CAN1_IRQHandler,
    /* 44 */    NULL,                   ADCHS_IRQHandler,   ATIMER_IRQHandler,  RTC_IRQHandler,
    /* 48 */    NULL,                   WDT_IRQHandler,     M0SUB_IRQHandler,   CAN0_IRQHandler,
    /* 52 */    QEI_IRQHandler
    };

/* CRP */
static __attribute__ ((section(".crp_sect"))) __attribute__ ((used)) const uint32_t f_crp_mode = CRP_MODE;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static __attribute__ ((__interrupt__)) void dummy_handler
    (void)
    {
    for (;;);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void __attribute__ ((__noreturn__)) __attribute__ ((__interrupt__)) Reset_ExHandler(void)
    {
    register const uint32_t* ptab;

    /* Вызвать инициализацию аппаратуры (предже всего PLL) */
    chip_init();

#ifdef STACK_FILLER
    /* Заполнить стек значением STACK_FILLER (для вычисления используемой глубины стека) */
    asm volatile
        (
        "  mov r0, sp \n"
        "0:"
        "  str %[data], [r0, #-4]! \n"
        "  cmp r0, %[stack_start] \n"
        "  bhi 0b"
        :
        : [stack_start] "r" (g_cpu_stack), [data] "r" (STACK_FILLER)
        : "r0", "cc", "memory"
        );
#endif

    /* Копирование инициализированных данных */
    /* Формат записи списка секций: { src, dest, len } */
    for (ptab = &__data_init_tab_start; ptab < &__data_init_tab_end; )
        {
        register const uint32_t *ps = (const uint32_t*)*ptab++;
        register uint32_t* pd = (uint32_t*)*ptab++;
        register uint32_t* pend = (uint32_t*)((uint32_t)pd + *ptab++);
        while (pd < pend)
            {
            *pd++ = *ps++;
            }
        }

    /* Обнуление bss */
    /* Формат записи списка секций: { dest, len } */
    for (ptab = &__bss_init_tab_start; ptab < &__bss_init_tab_end; )
        {
        register uint32_t* pd = (uint32_t*)*ptab++;
        register uint32_t* pend = (uint32_t*)((uint32_t)pd + *ptab++);
        while (pd < pend)
            {
            *pd++ = 0;
            }
        }

    /* Вызов основной программы */
    main();

    for (;;);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
unsigned int sys_stack_watermark    /* Оценка максимальной глубины использованного стека */
    (void)
    {
    unsigned int index = 0;
#ifdef STACK_FILLER
    /* Считаем, что если слово в стеке содержит начальное значение STACK_FILLER, то оно, вероятно,
       никогда не использовалось */
    while ((index < STACK_SIZE) && (STACK_FILLER == g_cpu_stack[index]))
        {
        index++;
        }
#endif
    return index;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
