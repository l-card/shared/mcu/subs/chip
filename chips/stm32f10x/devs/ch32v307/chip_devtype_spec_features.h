#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_DEV_CH32XXXX
#define CHIP_DEV_CH32VXXX
#define CHIP_DEV_CH32V3XX
#define CHIP_DEV_CH32V307
#define CHIP_DEV_CH32V30X_D8C /* CH32V307 & CH32V305 */

#ifndef CHIP_REV_NUM
#define CHIP_REV_NUM 2
#endif

#define CHIP_DEV_SUPPORT_EXMC           1
#define CHIP_DEV_SUPPORT_TIM9           1
#define CHIP_DEV_SUPPORT_TIM10          1
#define CHIP_DEV_SUPPORT_TIM11          0
#define CHIP_DEV_SUPPORT_TIM12          0
#define CHIP_DEV_SUPPORT_TIM13          0
#define CHIP_DEV_SUPPORT_TIM14          0
#define CHIP_DEV_SUPPORT_TRACE          0
#define CHIP_DEV_SUPPORT_ETH_RGMII      1
#define CHIP_DEV_SUPPORT_USBHS_PHY      1
#define CHIP_DEV_SUPPORT_RNG            1 /* */
#define CHIP_SUPPORT_ADC_INJECTED       1 /* */

#define CHIP_DEV_TIM_GP_CNT             4
#define CHIP_DEV_TIM_GP32_CNT           1
#define CHIP_DEV_TIM_ADV_CNT            3
#define CHIP_DEV_TIM_BASIC_CNT          2
#define CHIP_DEV_SPI_CNT                3
#define CHIP_DEV_I2S_CNT                2
#define CHIP_DEV_I2S_FD_CNT             0
#define CHIP_DEV_I2C_CNT                2
#define CHIP_DEV_UART_CNT               8
#define CHIP_DEV_CAN_CNT                2
#define CHIP_DEV_USB_CNT                1
#define CHIP_DEV_USBHS_CNT              1
#define CHIP_DEV_SDIO_CNT               1
#define CHIP_DEV_ETH_CNT                1
#define CHIP_DEV_ADC_CNT                2
#define CHIP_DEV_DAC_CNT                2
#define CHIP_DEV_SPIM_CNT               0
#define CHIP_DEV_QSPI_CNT               0
#define CHIP_DEV_DVP_CNT                1
#define CHIP_DEV_USBHS_CNT              1
#define CHIP_DEV_BLES_CNT               0
#define CHIP_DEV_PLL_CNT                3

#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(144)
#define CHIP_CLK_SPI_FREQ_MAX           CHIP_MHZ(72)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
