#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H

/* частота на входе в Гц */
#define CHIP_CLKF_OSC    25000000

/* настройки PLL */
/* нужно ли настраивать PLL */
#define CHIP_PLL_INIT         1
#define CHIP_PLL_MSEL_VAL     40
#define CHIP_PLL_DF_VAL       1

/* делители для различных клоков */
#define CHIP_CGU_DIV_CSEL_VAL     1
#define CHIP_CGU_DIV_SYSSEL_VAL   2
#define CHIP_CGU_DIV_S0SEL_VAL    2
#define CHIP_CGU_DIV_S1SEL_VAL    2
#define CHIP_CGU_DIV_DSEL_VAL     2
#define CHIP_CGU_DIV_OSEL_VAL     4

/* настройки WDT */
#define CHIP_WDT_EN         1
#define CHIP_WDT_CNT_VAL    CHIP_WDT_TICKS_MS(1000)

/* включить ли код для регистрации и настройки обработчиков прерываний */
#define CHIP_USE_INTERRUPT      1
/* нежно ли сбрасывать контроллер прерываний при запуске программы */
#define CHIP_INTERRUPT_RESET     0

/* ----------------------------- настройки DDR -------------------------------*/
  /** @note Нет конфигурации параметров EMR2, PADCTL и PHY_CTL */

/* нужно ли инициализировать контроллер DDR */
#define CHIP_DMC_INIT            1

#define CHIP_DMC_LPDDR           0     /* используется LPDDR (1) или DDR2 (0)  */
#define CHIP_DMC_SDRAM_SIZE      1024  /* размер памяти в MBit */

#define CHIP_DMC_RDTOWR          4  /* кол-во доп. циклов  если за записью идет чтение */
#define CHIP_DMC_ADDRMODE_PAGE   1  /* признак, используется ли Page (1) или Bank (0) Interleaving */
#define CHIP_DMC_PRECHARGE       0  /* используется ли Precharge */
#define CHIP_DMC_POSTPONE_REF    0  /* разрешение откладывать Refresh */

#define CHIP_DMC_REF_IDLECYC     4 /* кол-во idle-циклов, после которого выполняются отложенные Refresh */
#define CHIP_DMC_NUMREF          4 /* кол-во откладываемых Refresh */
#define CHIP_DMC_PRECHARGE_BANKS 0 /* маска банков, для которых разрешено precharge */
#define CHIP_DMC_WAIT_WRDATA     1 /* резрешение ожидания при записи для burst */
#define CHIP_DMC_FULL_WRDATA     0 /* ожидание, когда доступны будут все данные, перед записью */

/* прироритетные ID на crossbar */
#define CHIP_DMC_PRIO_ID1        0
#define CHIP_DMC_PRIO_ID1_MSK    0
#define CHIP_DMC_PRIO_ID2        0
#define CHIP_DMC_PRIO_ID2_MSK    0


/* timings. все настройки могут времен задаваться:
    - в нс через определения CHIP_DMC_T_XXX и макрос CHIP_SDRAM_T_NS()
    - в циклах через определения CHIP_DMC_T_XXX_CYCLES
   каждый временной параметр должен быть задан хотя бы одним способом.
   если задан двумя, то выбирается большее результирующее значение */
#define CHIP_DMC_TMRD_CYCLES   2     /* mode set to next command */
#define CHIP_DMC_TRC_NS        55    /* active to active command in the same bank */
#define CHIP_DMC_TRAS_NS       40    /* active to precharge */
#define CHIP_DMC_TRP_NS        15    /* precharge to active */
#define CHIP_DMC_TWTR_NS       7.5     /* write to read delay */
#define CHIP_DMC_TRCD_NS       15    /* RAS to CAS - active cmd to read/write assertion */
#define CHIP_DMC_TRRD_NS       10    /* Read-Read delay (active cmd bank x to active cmd bank y) */
#define CHIP_DMC_TRFC_NS       127.5 /* Refresh to active cmd delay */
#define CHIP_DMC_TREF_NS       3900  /* Refresh interval */
#define CHIP_DMC_TCKE_CYCLES   3    /* CKE minimum pulsewidth */
#define CHIP_DMC_TXP_CYCLES    2    /* exit powerdown to next cmd */
#define CHIP_DMC_TWR_NS        15    /* write recovery time */
#define CHIP_DMC_TRTP_NS       7.5  /* read to precharge time */
#define CHIP_DMC_TFAW_NS       50    /* 4-bank activated window time */


#define CHIP_DMC_PD                 0   /* powerdown mode with slow exit (1) or fast exit (0) */
#define CHIP_DMC_CL                 4   /* CAS-latency */
#define CHIP_DMC_BURST_LEN          4   /* burst len (4 or 8) */

#define CHIP_DMC_OUT_BUF_OFF  0 /* disable the SDRAM output pins */
#define CHIP_DMC_DQS_OFF      0 /* disable operation of the DQS pin */
#define CHIP_DMC_RTT         75 /* termination resistance: 0, 50, 75 or 150 */
#define CHIP_DMC_ADD_LATENCY  0 /* added latency time for CAS ops */
#define CHIP_DMC_DIC_RED      0 /* enable reduced drive strength */
#define CHIP_DMC_DLL_EN       0 /* enable dll in SDRAM */





/* -------------------------  Настройки SMC ---------------------------------*/
#define CHIP_SMC_INIT               1
#define CHIP_SMC_BUS_GRANT_EN       0

/* кол-во используемых адресных линий (до 26) */
#define CHIP_SMC_ADDR_WIDTH         25
/* разрешено ли байтовое обращение (линии ABE0/1) */
#define CHIP_SMC_BYTE_OP_EN         1

#define CHIP_SMC_B0_EN           1
#define CHIP_SMC_B1_EN           0
#define CHIP_SMC_B2_EN           0
#define CHIP_SMC_B3_EN           0

/* настройки банков - могут использоваться как общие для всех банков, так
   и часть настроек может быть изменена для конкретного банка с помощью
   параметров SHIP_SMC_B0_xxx, SHIP_SMC_B1_xxx и т.д. */
#define CHIP_SMC_BURST_TYPE_SEQ  0  /* sync burst mode wrap (0) or sequential (1) */
#define CHIP_SMC_BCLK_DIV        2  /* burst clk = SCLK/DIV (1-4) */
#define CHIP_SMC_PAGE_SIZE       4  /* flash page size (4, 8, 16) */
#define CHIP_SMC_RDY_EN          1  /* enable ARDY pin (with abort) */
#define CHIP_SMC_RDY_POL         1  /* ARDY Polarity (0 - low active, 1 - high active) */
#define CHIP_SMC_SELCTRL         SMC_SELCTRL_AMS_ONLY  /* Select control */
#define CHIP_SMC_MODE            SMC_MODE_ASYNC_FLASH
/* временные параметры могут задаваться как в циклах (через определения _CYCLES),
   так и в нс (через определения _NS). Если определены оба варианта, то
   используется наибольшее значение */
#define CHIP_SMC_RAT_CYCLES     8  /* read access time */
#define CHIP_SMC_RHT_CYCLES     1  /* read hold time */
#define CHIP_SMC_RST_CYCLES     7  /* read setup time */
#define CHIP_SMC_WAT_CYCLES     9  /* write access time */
#define CHIP_SMC_WHT_CYCLES     7  /* write hold time */
#define CHIP_SMC_WST_CYCLES     7  /* write setup time */
#define CHIP_SMC_PGWS_CYCLES    9  /* page wait states */
#define CHIP_SMC_IT_CYCLES      2  /* idle time */
#define CHIP_SMC_TT_CYCLES      2  /* transition time */
#define CHIP_SMC_PREAT_CYCLES   3  /* pre access time */
#define CHIP_SMC_PREST_CYCLES   1  /* pre setup time */

#endif // CHIP_CONFIG_H

