#ifndef CHIP_N32G03X_REGS_PWR_H
#define CHIP_N32G03X_REGS_PWR_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR;       /* Power control register,                  Address offset: 0x00 */
    __IO uint32_t CSR;      /* Power control/status register,           Address offset: 0x04 */
    __IO uint32_t CR2;      /* Power control register 2,                Address offset: 0x08 */
    uint32_t RESERVED0[2];
    __IO uint32_t CR3;      /* Power control register 3,                Address offset: 0x14 */
    uint32_t RESERVED1[2];
    __IO uint32_t CR4;      /* Power control register 4,                Address offset: 0x20 */
    __IO uint32_t CR5;      /* Power control register 5,                Address offset: 0x24 */
    __IO uint32_t CR6;      /* Power control register 6,                Address offset: 0x28 */
    uint32_t RESERVED2;
    __IO uint32_t DBG_CR;   /* Power control register 6,                Address offset: 0x30 */
} CHIP_REGS_PWR_T;

#define CHIP_REGS_PWR                   ((CHIP_REGS_PWR_T *) CHIP_MEMRGN_ADDR_PERIPH_PWR)

/*********** Power control register (PWR_CR) **********************************/
#define CHIP_REGFLD_PWR_CR_PDSTP                    (0x00000001UL <<  1U)   /* (RW)   Enter STOP/PD mode selection (0 - STOP, 1 - PD)*/
#define CHIP_REGFLD_PWR_CR_CWUF                     (0x00000001UL <<  2U)   /* (RCW1) Clear wakeup flag */
#define CHIP_REGFLD_PWR_CR_CLR_DBG_PDF              (0x00000001UL <<  3U)   /* (RCW1) (!N) Clear DBG_PD mode flag */
#define CHIP_REGFLD_PWR_CR_PVDE                     (0x00000001UL <<  4U)   /* (RW)   Programmable voltage detector (PVD) enable.*/
#define CHIP_REGFLD_PWR_CR_PLS                      (0x0000000FUL <<  5U)   /* (RW)   PVD level selection */
#define CHIP_REGFLD_PWR_CR_PDR                      (0x00000001UL <<  9U)   /* (RW)   (!N) Tune VDDD PDR trigger level during STOP mode. */
#define CHIP_REGMSK_PWR_CR_RESERVED                 (0xFFFFFC01UL)


#define CHIP_REGFLDVAL_PWR_CR_PLS_1_8V              0
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_0V              1
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_2V              2
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_4V              3
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_6V              4
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_8V              5
#define CHIP_REGFLDVAL_PWR_CR_PLS_3_0V              6
#define CHIP_REGFLDVAL_PWR_CR_PLS_3_2V              7
#define CHIP_REGFLDVAL_PWR_CR_PLS_3_4V              8
#define CHIP_REGFLDVAL_PWR_CR_PLS_3_6V              9
#define CHIP_REGFLDVAL_PWR_CR_PLS_3_8V              10
#define CHIP_REGFLDVAL_PWR_CR_PLS_4_0V              11
#define CHIP_REGFLDVAL_PWR_CR_PLS_4_2V              12
#define CHIP_REGFLDVAL_PWR_CR_PLS_4_6V              13
#define CHIP_REGFLDVAL_PWR_CR_PLS_4_8V              14
#define CHIP_REGFLDVAL_PWR_CR_PLS_5_0V              15

#define CHIP_REGFLDVAL_PWR_CR_PDR_1_0V              0
#define CHIP_REGFLDVAL_PWR_CR_PDR_1_2V              1


/*********** Power control/status register (PWR_CSR) **************************/
#define CHIP_REGFLD_PWR_CSR_WUF                     (0x00000001UL <<  0U)   /* (RO) DBGPD mode wake-up status bit. */
#define CHIP_REGFLD_PWR_CSR_DBG_PDF                 (0x00000001UL <<  1U)   /* (RO) DBGPD mode status bit. */
#define CHIP_REGFLD_PWR_CSR_PVDO                    (0x00000001UL <<  2U)   /* (RO) PVD output */
#define CHIP_REGFLD_PWR_CSR_WKUP_EN(n)              (0x00000001UL <<  (8U + (n))) /* (RW) Enable WKUP pin (0 - PA0, 1 - PC13, 2 - PA2) */
#define CHIP_REGFLD_PWR_CSR_WKUP_POL                (0x00000001UL << 11U)   /* (RW) Wakeup polarity for PA0/PA2/PC13 (0 - Falling Edge, 1 - Rising Edge) */
#define CHIP_REGMSK_PWR_CSR_RESERVED                (0xFFFFF0F8UL)

#define CHIP_REGFLDVAL_PWR_CSR_WKUP_POL_FALL        0
#define CHIP_REGFLDVAL_PWR_CSR_WKUP_POL_RISE        1

/*********** Power control register 2 (PWR_CR2) *******************************/
#define CHIP_REGFLD_PWR_CR2_IWDG_RST_EN             (0x00000001UL << 10U)   /* (RW)  Independent watchdog reset enable. */
#define CHIP_REGMSK_PWR_CR2_RESERVED                (0xFFFFFBFFUL)

/*********** Power control register 3 (PWR_CR3) *******************************/
#define CHIP_REGFLD_PWR_CR3_LSI_EN                  (0x00000001UL << 7U)   /* (RW)  PWR requesting LSI clock after system enter STOP mode. */
#define CHIP_REGMSK_PWR_CR3_RESERVED                (0xFFFFFF7FUL)

/*********** Power control register 4 (PWR_CR4) *******************************/
#define CHIP_REGFLD_PWR_CR4_FLH_WKUP                (0x00000001UL << 0U)   /* (RW) FLASH fast wakeup enable */
#define CHIP_REGFLD_PWR_CR4_STB_FLH                 (0x00000001UL << 1U)   /* (RW) FLASH deep standby mode enable */
#define CHIP_REGFLD_PWR_CR4_LPRUN_EN                (0x00000001UL << 4U)   /* (RW) LPRUN mode enable */
#define CHIP_REGFLD_PWR_CR4_LPRUN_FLH               (0x00000001UL << 5U)   /* (RW) Flash low-power control for LPRUN mode (0 - sandby, 1 - working) */
#define CHIP_REGFLD_PWR_CR4_LPRUN_STS               (0x00000001UL << 6U)   /* (RO) LPRUN mode entry status */
#define CHIP_REGMSK_PWR_CR4_RESERVED                (0xFFFFFF8CUL)

/*********** Power control register 5 (PWR_CR5) *******************************/
#define CHIP_REGFLD_PWR_CR5_SLPMR_SEL               (0x00000003UL << 2U)   /* (RW) VDDD output voltage selection after system enters STOP mode. */
#define CHIP_REGMSK_PWR_CR5_RESERVED                (0xFFFFFFF3UL)

#define CHIP_REGFLDVAL_PWR_CR5_SLPMR_SEL_1_5V       0x1
#define CHIP_REGFLDVAL_PWR_CR5_SLPMR_SEL_1_2V       0x3

/*********** Power control register 6 (PWR_CR6) *******************************/
#define CHIP_REGFLD_PWR_CR6_SLPMR_EN                (0x00000003UL << 2U)   /* (RW) VDDD output voltage selection enable. */
#define CHIP_REGMSK_PWR_CR6_RESERVED                (0xFFFFFFF3UL)

#define CHIP_REGFLDVAL_PWR_CR6_SLPMR_EN_1_5V        0x0 /* After entering STOP mode, VDDD output voltage remains at 1.5 */
#define CHIP_REGFLDVAL_PWR_CR6_SLPMR_EN_SEL         0x3 /* After entering STOP mode, VDDD output voltage is controlled by PWR_CR5.SLPMR_SEL */


/*********** Debug control register (DBG_CR) **********************************/
#define CHIP_REGFLD_PWR_DBG_CR_SLEEP                (0x00000001UL << 0U)   /* (RW) Debug SLEEP mode. */
#define CHIP_REGFLD_PWR_DBG_CR_STOP                 (0x00000001UL << 1U)   /* (RW) Debug STOP mode control. */
#define CHIP_REGFLD_PWR_DBG_CR_PD                   (0x00000001UL << 2U)   /* (RW) Debug PD mode control. */
#define CHIP_REGFLD_PWR_DBG_CR_IWDG_STP             (0x00000001UL << 8U)   /* (RW) IWDG stops working when core enters debug state. */
#define CHIP_REGFLD_PWR_DBG_CR_WWDG_STP             (0x00000001UL << 9U)   /* (RW) WWDG stops working when core enters debug state. */
#define CHIP_REGFLD_PWR_DBG_CR_TIM1_STP             (0x00000001UL << 10U)  /* (RW) TIM1 stops working when core enters debug state. */
#define CHIP_REGFLD_PWR_DBG_CR_TIM3_STP             (0x00000001UL << 12U)  /* (RW) TIM3 stops working when core enters debug state. */
#define CHIP_REGFLD_PWR_DBG_CR_I2C1_TOUT            (0x00000001UL << 15U)  /* (RW) I2C1 stops SMBUS timeout mode when core is stopped. */
#define CHIP_REGFLD_PWR_DBG_CR_I2C2_TOUT            (0x00000001UL << 16U)  /* (RW) I2C2 stops SMBUS timeout mode when core is stopped. */
#define CHIP_REGFLD_PWR_DBG_CR_LPTIM_STP            (0x00000001UL << 17U)  /* (RW) LPTIM stops working when core enters debug state. */
#define CHIP_REGFLD_PWR_DBG_CR_TIM6_STP             (0x00000001UL << 18U)  /* (RW) TIM6 stops working when core enters debug state. */
#define CHIP_REGFLD_PWR_DBG_CR_TIM8_STP             (0x00000001UL << 20U)  /* (RW) TIM8 stops working when core enters debug state. */


#endif // CHIP_N32G03X_REGS_PWR_H
