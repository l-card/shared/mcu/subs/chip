#ifndef STM32_IWDG_REGS_H
#define STM32_IWDG_REGS_H

/* CHIP_SUPPORT_IWDG_WINDOW            - поддержка управления окном */
#include "chip_ioreg_defs.h"

/* Independent WATCHDOG */
typedef struct {
    __IO uint32_t KR;   /* IWDG Key register,       Address offset: 0x00 */
    __IO uint32_t PR;   /* IWDG Prescaler register, Address offset: 0x04 */
    __IO uint32_t RLR;  /* IWDG Reload register,    Address offset: 0x08 */
    __IO uint32_t SR;   /* IWDG Status register,    Address offset: 0x0C */
#if CHIP_SUPPORT_IWDG_WINDOW
    __IO uint32_t WINR; /* IWDG Window register,    Address offset: 0x10 */
#endif
} CHIP_REGS_IWDG_T;


/*********** IWDG key register (IWDG_KR) **************************************/
#define CHIP_REGFLD_IWDG_KR_KEY          (0x0000FFFFUL <<  0U) /* Key value (write only, read 0000h) */

/*********** IWDG prescaler register (IWDG_PR) ********************************/
#define CHIP_REGFLD_IWDG_PR_PR           (0x00000007UL <<  0U) /* Prescaler divider */

/*********** IWDG reload register (IWDG_RLR) **********************************/
#define CHIP_REGFLD_IWDG_RLR_RL          (0x00000FFFUL <<  0U) /* Watchdog counter reload value */

/*********** IWDG status register (IWDG_SR) ***********************************/
#define CHIP_REGFLD_IWDG_SR_PVU          (0x00000001UL <<  0U) /* Watchdog prescaler value update */
#define CHIP_REGFLD_IWDG_SR_RVU          (0x00000001UL <<  1U) /* Watchdog counter reload value update */
#if CHIP_SUPPORT_IWDG_WINDOW
#define CHIP_REGFLD_IWDG_SR_WVU          (0x00000001UL <<  2U) /* Watchdog counter window value update */
#endif

/*********** IWDG window register (IWDG_WINR) *********************************/
#define CHIP_REGFLD_IWDG_WINR_WIN        (0x00000FFFUL <<  0U) /* Watchdog counter window value */


#endif // STM32_IWDG_REGS_H
