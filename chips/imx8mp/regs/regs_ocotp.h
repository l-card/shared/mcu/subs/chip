#ifndef REGS_OCOTP_H
#define REGS_OCOTP_H

#include "chip_ioreg_defs.h"

#define CHIP_OTP_BANK_WORDS_CNT     4
#define CHIP_OTP_WORDS_CNT          384

#define CHIP_OTP_WR_UNLOCK_VALUE    0x3E77

#define CHIP_OTP_TIME_TRELAX_MIN    17 /* (TIMING[RELAX]+1)/ipg_frequency > 16.2ns */
#define CHIP_OTP_TIME_TPGM          10000 /* ((TIMING[STROBE_PROG]+1)- 2*(TIMING[RELAX]+1))/ipg_frequency = 10000ns. */
#define CHIP_OTP_TIME_TRD_MIN       36 /*  ((TIMING[STROBE_READ]+1)- 2*(TIMING[RELAX]+1))/ipg_frequency > 36ns. */
#define CHIP_OTP_TIME_PROG_POST_WT  2000  /* Wait after programm busy -> 0: 2 us */

typedef struct {
    __IO uint32_t CTRL;             /* OTP Controller Control Register,            Address offset: 0x00 */
    __IO uint32_t CTRL_SET;         /* OTP Controller Control Set Register,        Address offset: 0x04 */
    __IO uint32_t CTRL_CLR;         /* OTP Controller Control Clear Register,      Address offset: 0x08 */
    __IO uint32_t CTRL_TOG;         /* OTP Controller Control Toggle Register,     Address offset: 0x0C */
    __IO uint32_t TIMING;           /* OTP Controller Timing Register,             Address offset: 0x10 */
    uint32_t RESERVED0[3];
    __IO uint32_t DATA;             /* OTP Controller Write Data Register,         Address offset: 0x20 */
    uint32_t RESERVED1[3];
    __IO uint32_t READ_CTRL;        /* OTP Controller Read Control Register,       Address offset: 0x30 */
    uint32_t RESERVED2[3];
    __IO uint32_t READ_FUSE_DATA;   /* OTP Controller Read Data Register,          Address offset: 0x40 */
    uint32_t RESERVED3[19];
    __I  uint32_t VERSION;          /* OTP Controller Version Register,            Address offset: 0x90 */
    uint32_t RESERVED4[219];
    struct {
        __IO  uint32_t VALUE;       /* Value of OTP Bankx Wordx,                   Address offset: 0x400 */
        uint32_t RESERVED5[3];
    } OTP[CHIP_OTP_WORDS_CNT];
} CHIP_REGS_OCOTP_T;

#define CHIP_REGS_OCOTP          ((CHIP_REGS_OCOTP_T *) CHIP_MEMRGN_ADDR_PERIPH_OCOTP_CTRL)


/*********** OTP Controller Control Register (OCOTP_CTRL) *********************/
#define CHIP_REGFLD_OCOTP_CTRL_WR_UNLOCK            (0x0000FFFFUL << 16U)   /* (RW)  Write CHIP_OTP_WR_UNLOCK_VALUE  to enable OTP write accesses (autoclear on op finish) */
#define CHIP_REGFLD_OCOTP_CTRL_RELOAD_SHADOWS       (0x00000001UL << 11U)   /* (RW1SC) Set to force re-loading the shadow registers */
#define CHIP_REGFLD_OCOTP_CTRL_ERROR                (0x00000001UL << 10U)   /* (RW1C)  Set by HW on error. Reset by CTRL_CLR reg! */
#define CHIP_REGFLD_OCOTP_CTRL_BUSY                 (0x00000001UL <<  9U)   /* (RO)  OTP controller busy status bit */
#define CHIP_REGFLD_OCOTP_CTRL_ADDR                 (0x000001FFUL <<  0U)   /* (RW)  OTP write and read access address register. */
#define CHIP_REGMSK_OCOTP_CTRL_RESERVED             (0x0000F000UL)

/*********** OTP Controller Timing Register (OCOTP_TIMING) ********************/
#define CHIP_REGFLD_OCOTP_TIMING_WAIT               (0x0000003FUL << 22U)   /* (RW) Time interval between auto read and write access in one time program */
#define CHIP_REGFLD_OCOTP_TIMING_STROBE_READ        (0x0000003FUL << 16U)   /* (RW) Strobe period in one time read OTP (Trd)*/
#define CHIP_REGFLD_OCOTP_TIMING_RELAX              (0x0000000FUL << 12U)   /* (RW) Time to add to all default timing parameters other than the Tpgm and Trd */
#define CHIP_REGFLD_OCOTP_TIMING_STROBE_PROG        (0x00000FFFUL <<  0U)   /* (RW) Strobe period in one time write OTP (Tpgm) */
#define CHIP_REGMSK_OCOTP_TIMING_RESERVED           (0xF0000000UL)

/*********** OTP Controller Read Control Register (OCOTP_READ_CTRL) ***********/
#define CHIP_REGFLD_OCOTP_READ_CTRL_READ_FUSE       (0x00000001UL <<  0U)   /* (RW1SC)  Used to initiate a read to OTP. */
#define CHIP_REGMSK_OCOTP_READ_CTRL_RESERVED        (0xFFFFFFFEUL)

/*********** OTP Controller Version Register (OCOTP_VERSION) ******************/
#define CHIP_REGFLD_OCOTP_VERSION_MAJOR             (0x000000FFUL << 24U)   /* (RO)  Fixed read-only value reflecting the MAJOR field of the RTL version. */
#define CHIP_REGFLD_OCOTP_VERSION_MINOR             (0x000000FFUL << 16U)   /* (RO)  Fixed read-only value reflecting the MINOR field of the RTL version. */
#define CHIP_REGFLD_OCOTP_VERSION_STEP              (0x0000FFFFUL <<  0U)   /* (RO)  Fixed read-only value reflecting the stepping of the RTL version. */


#endif // REGS_OCOTP_H
