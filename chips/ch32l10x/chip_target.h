#ifndef CHIP_TARGET_H
#define CHIP_TARGET_H

#include "chip_mmap.h"
#include "chip_irq_nums.h"

#include "regs/regs_esig.h"
#include "regs/regs_vendor_info.h"
#include "regs/regs_iwdg.h"
#include "regs/regs_gpio.h"
#include "regs/regs_afio.h"
#include "regs/regs_flash.h"
#include "regs/regs_rcc.h"
#include "regs/regs_exten.h"
#include "regs/regs_usart.h"
#include "regs/regs_usbpd.h"
#include "regs/regs_usbfs.h"
#include "regs/regs_tim.h"
#include "regs/regs_spi.h"
#include "regs/regs_adc.h"

#include "init/chip_cfg_defs.h"
#include "init/chip_init_faults.h"
#include "init/chip_clk.h"
#include "init/chip_clk_defs.h"
#include "init/chip_clk_constraints.h"
#include "init/chip_ioremap_defs.h"
#include "init/chip_per_ctl.h"
#include "init/chip_wdt.h"

#include "chip_pins.h"

#endif // CHIP_TARGET_H
