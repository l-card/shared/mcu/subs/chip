#ifndef CHIP_DEVTYPE_MEM_H
#define CHIP_DEVTYPE_MEM_H

#include "chip_dev_spec_features.h"

#define CHIP_RAM_MEM_SIZE                       (16 * 1024)


#endif // CHIP_DEVTYPE_MEM_H
