#ifndef CHIP_GIC_IRQ_NUMS_H
#define CHIP_GIC_IRQ_NUMS_H

#define CHIP_GIC_IRQNUM_SGI_MIN             0
#define CHIP_GIC_IRQNUM_SGI_MAX             15
#define CHIP_GIC_IRQNUM_PPI_MIN             16
#define CHIP_GIC_IRQNUM_PPI_MAX             31
#define CHIP_GIC_IRQNUM_SPI_MIN             32
#define CHIP_GIC_IRQNUM_SPI_MAX             1019

#define CHIP_GIC_IRQNUM_SGI(i)              ((i) & 0xF)            /* 0 - 15:     Software Generated Interrupts (Banked per PE):            */
#define CHIP_GIC_IRQNUM_PPI(i)              (16 + ((i) & 0xF))     /* 16 - 31:    Private Peripheral Interrupts (Banked per PE):            */
#define CHIP_GIC_IRQNUM_SPI(i)              ((32 + (i)) & 0x3FF)   /* 32 - 1019:  Shared Peripheral Interrupts:                              */
#define CHIP_GIC_IRQNUM_LPI(i)              (8192 + (i))           /* 8192 - ...: Locality-specific Peripheral Interrupts (message based):  .  */

#define CHIP_GIC_IRQNUM_SPEC_EL1_S          1020 /* Acknowledged int is expected to be handled at Secure EL1 */
#define CHIP_GIC_IRQNUM_SPEC_EL1_EL2_NS     1021 /* Acknowledged int is expected to be handled at Non-secure EL1 or EL2*/
#define CHIP_GIC_IRQNUM_SPEC_LEGACY         1022 /* This value applies to legacy operation only */
#define CHIP_GIC_IRQNUM_SPEC_NO_PEND        1023 /* No pending interrupt with sufficient priority for it to be signaled to the PE */

#endif // CHIP_GIC_IRQ_NUMS_H
