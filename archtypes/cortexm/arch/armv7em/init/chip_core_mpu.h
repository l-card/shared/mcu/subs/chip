#ifndef CHIP_ARMV7EM_CHIP_MPU_H
#define CHIP_ARMV7EM_CHIP_MPU_H


#include "chip_config.h"
#include "chip_core_mpu_defs.h"
#include "chip_core_features.h"




#if defined CHIP_CFG_MPU_SETUP_EN && CHIP_CORE_SUPPORT_MPU
    #define CHIP_MPU_SETUP_EN     CHIP_CFG_MPU_SETUP_EN
#else
    #define CHIP_MPU_SETUP_EN     0
#endif


#if CHIP_MPU_SETUP_EN
void chip_mpu_init(void);
#endif

#endif // CHIP_ARMV7EM_CHIP_MPU_H
