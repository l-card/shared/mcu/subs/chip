#ifndef CHIP_LPC11U6X_REGS_SSP_H
#define CHIP_LPC11U6X_REGS_SSP_H

#include <stdint.h>

#define CHIP_SSP_FIFO_SIZE        8

typedef struct {            /*!< SSPn Structure         */
    __IO uint32_t CR0;      /*!< Control Register 0. Selects the serial clock rate, bus type, and data size. */
    __IO uint32_t CR1;      /*!< Control Register 1. Selects master/slave and other modes. */
    __IO uint32_t DR;       /*!< Data Register. Writes fill the transmit FIFO, and reads empty the receive FIFO. */
    __I  uint32_t SR;       /*!< Status Register        */
    __IO uint32_t CPSR;     /*!< Clock Prescale Register */
    __IO uint32_t IMSC;     /*!< Interrupt Mask Set and Clear Register */
    __I  uint32_t RIS;      /*!< Raw Interrupt Status Register */
    __I  uint32_t MIS;      /*!< Masked Interrupt Status Register */
    __O  uint32_t ICR;      /*!< SSPICR Interrupt Clear Register */
    __O  uint32_t DMACR;    /*!< DMA control Register */
} CHIP_REGS_SSP_T;

#define CHIP_REGS_SSP0              ((CHIP_REGS_SSP_T *) CHIP_MEMRGN_ADDR_PERIPH_SSP0)
#define CHIP_REGS_SSP1              ((CHIP_REGS_SSP_T *) CHIP_MEMRGN_ADDR_PERIPH_SSP1)

/*********** SSP Control Register 0 (SSP_CR0) *********************************/
#define CHIP_REGFLD_SSP_CR0_DDS                     (0x0000000FUL <<  0U)   /* (RW) Data size select (0x3 (4-bit) - 0xF (16-bit)) */
#define CHIP_REGFLD_SSP_CR0_FRF                     (0x00000003UL <<  4U)   /* (RW) Frame Format */
#define CHIP_REGFLD_SSP_CR0_CPOL                    (0x00000001UL <<  6U)   /* (RW) Clock Out Polarity (SPI mode) */
#define CHIP_REGFLD_SSP_CR0_CPHA                    (0x00000001UL <<  7U)   /* (RW) Clock Out Phase (SPI mode) */
#define CHIP_REGFLD_SSP_CR0_SCR                     (0x000000FFUL <<  8U)   /* (RW) Serial Clock Rate (=PCLK / (CPSDVSR * [SCR+1])) */
#define CHIP_REGMSK_SSP_CR0_RESERVED                (0xFFFF0000UL)

#define CHIP_REGFLDVAL_SSP_CR0_DDS(size)            (size - 1)

#define CHIP_REGFLDVAL_SSP_CR0_FRF_SPI              0
#define CHIP_REGFLDVAL_SSP_CR0_FRF_TI               1
#define CHIP_REGFLDVAL_SSP_CR0_FRF_MICROWIRE        2

/*********** SSP Control Register 1 (SSP_CR1) *********************************/
#define CHIP_REGFLD_SSP_CR1_LBM                     (0x00000001UL <<  0U)   /* (RW) Loop Back Mode */
#define CHIP_REGFLD_SSP_CR1_SSE                     (0x00000001UL <<  1U)   /* (RW) SPI Enable */
#define CHIP_REGFLD_SSP_CR1_MS                      (0x00000001UL <<  2U)   /* (RW) Master(0)/Slave(1) Mode */
#define CHIP_REGFLD_SSP_CR1_SOD                     (0x00000001UL <<  3U)   /* (RW) Slave Output Disable */
#define CHIP_REGMSK_SSP_CR1_RESERVED                (0xFFFFFFF0UL)

/*********** SSP Status Register (SSP_SR) *************************************/
#define CHIP_REGFLD_SSP_SR_TFE                      (0x00000001UL <<  0U)   /* (RO) Transmit FIFO Empty */
#define CHIP_REGFLD_SSP_SR_TNF                      (0x00000001UL <<  1U)   /* (RO) Transmit FIFO Not Full */
#define CHIP_REGFLD_SSP_SR_RNE                      (0x00000001UL <<  2U)   /* (RO) Receive FIFO Not Empty */
#define CHIP_REGFLD_SSP_SR_RFF                      (0x00000001UL <<  3U)   /* (RO) Receive FIFO Full */
#define CHIP_REGFLD_SSP_SR_BSY                      (0x00000001UL <<  4U)   /* (RO) Busy */
#define CHIP_REGMSK_SSP_SR_RESERVED                 (0xFFFFFFE0UL)

/*********** SSP Clock Prescale Register (SSP_CPSR) ***************************/
#define CHIP_REGFLD_SSP_CPSR_CPSDVSR                (0x000000FFUL <<  0U)   /* (RW) This even value between 2 and 254, by which SPI_PCLK is divided to yield the prescaler output clock */

/*********** SSP Interrupt Mask/Raw Status/Status/Clear register (SSP_IMSC/RIS/MIS/OCR) ****/
#define CHIP_REGFLD_SSP_INT_ROR                     (0x00000001UL <<  0U)   /* (RW) Receive Overrun Interrupt  */
#define CHIP_REGFLD_SSP_INT_RT                      (0x00000001UL <<  1U)   /* (RW) Receive Time-out Interrupt */
#define CHIP_REGFLD_SSP_INT_RX                      (0x00000001UL <<  2U)   /* (RW) Rx FIFO is atleast half full */
#define CHIP_REGFLD_SSP_INT_TX                      (0x00000001UL <<  3U)   /* (RW) Tx FIFO is atleast half empty */

/*********** SSP  DMA Control Registe (SSP_DMACR) *****************************/
#define CHIP_REGFLD_SSP_DMACR_RXDMAE                (0x00000001UL <<  0U)   /* (RW) Receive DMA Enable */
#define CHIP_REGFLD_SSP_DMACR_TXDMAE                (0x00000001UL <<  1U)   /* (RW) Transmit DMA Enable */

#endif /* CHIP_LPC11U6X_REGS_SSP_H */
