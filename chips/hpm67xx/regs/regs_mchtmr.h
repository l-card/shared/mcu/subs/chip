#ifndef REGS_MCHTMR_H
#define REGS_MCHTMR_H

#include "chip_ioreg_defs.h"


typedef union {
    struct {
        uint32_t  l;
        uint32_t  h;
    };
    uint64_t val64;
} CHIP_REGS_MCHTMR_U64;


typedef struct {
    __IO CHIP_REGS_MCHTMR_U64 MTIME;                    /* Machine timer,                                       Address offset: 0x0000 - 0x0004 */
    __IO CHIP_REGS_MCHTMR_U64 MTIMECMP;                 /* Machine timing comparator,                           Address offset: 0x0008 - 0x000C */
} CHIP_REGS_MCHTMR_T;

#define CHIP_REGS_MCHTMR            ((CHIP_REGS_MCHTMR_T *)CHIP_MEMRGN_ADDR_MCHTMR)

#endif // REGS_MCHTMR_H
