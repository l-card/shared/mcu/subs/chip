#ifndef CHIP_STM32G0XX_REGS_FLASH_H
#define CHIP_STM32G0XX_REGS_FLASH_H

typedef struct {
  __IO uint32_t ACR;          /*!< FLASH Access Control register,                     Address offset: 0x00 */
       uint32_t RESERVED1;    /*!< Reserved1,                                         Address offset: 0x04 */
  __IO uint32_t KEYR;         /*!< FLASH Key register,                                Address offset: 0x08 */
  __IO uint32_t OPTKEYR;      /*!< FLASH Option Key register,                         Address offset: 0x0C */
  __IO uint32_t SR;           /*!< FLASH Status register,                             Address offset: 0x10 */
  __IO uint32_t CR;           /*!< FLASH Control register,                            Address offset: 0x14 */
  __IO uint32_t ECCR;          /*!< FLASH ECC bank 1 register,                        Address offset: 0x18 */
  __IO uint32_t ECC2R;         /*!< FLASH ECC bank 2 register,                        Address offset: 0x1C */
  __IO uint32_t OPTR;         /*!< FLASH Option register,                             Address offset: 0x20 */
  __IO uint32_t PCROP1ASR;    /*!< FLASH Bank PCROP area A Start address register,    Address offset: 0x24 */
  __IO uint32_t PCROP1AER;    /*!< FLASH Bank PCROP area A End address register,      Address offset: 0x28 */
  __IO uint32_t WRP1AR;       /*!< FLASH Bank WRP area A address register,            Address offset: 0x2C */
  __IO uint32_t WRP1BR;       /*!< FLASH Bank WRP area B address register,            Address offset: 0x30 */
  __IO uint32_t PCROP1BSR;    /*!< FLASH Bank PCROP area B Start address register,    Address offset: 0x34 */
  __IO uint32_t PCROP1BER;    /*!< FLASH Bank PCROP area B End address register,      Address offset: 0x38 */
       uint32_t RESERVED5[2]; /*!< Reserved5,                                         Address offset: 0x3C--0x40 */
  __IO uint32_t PCROP2ASR;    /*!< FLASH Bank2 PCROP area A Start address register,   Address offset: 0x44 */
  __IO uint32_t PCROP2AER;    /*!< FLASH Bank2 PCROP area A End address register,     Address offset: 0x48 */
  __IO uint32_t WRP2AR;       /*!< FLASH Bank2 WRP area A address register,           Address offset: 0x4C */
  __IO uint32_t WRP2BR;       /*!< FLASH Bank2 WRP area B address register,           Address offset: 0x50 */
  __IO uint32_t PCROP2BSR;    /*!< FLASH Bank2 PCROP area B Start address register,   Address offset: 0x54 */
  __IO uint32_t PCROP2BER;    /*!< FLASH Bank2 PCROP area B End address register,     Address offset: 0x58 */
       uint32_t RESERVED7[9]; /*!< Reserved7,                                         Address offset: 0x5C--0x7C */
  __IO uint32_t SECR;         /*!< FLASH security register ,                          Address offset: 0x80 */
} CHIP_REGS_FLASH_T;

#define CHIP_REGS_FLASH          ((CHIP_REGS_FLASH_T *) CHIP_MEMRGN_ADDR_PERIPH_FLASH)


/*******************  Bits definition for FLASH_ACR register  *****************/
#define CHIP_REGFLD_FLASH_ACR_LATENCY                   (0x00000007UL <<  0U)
#define CHIP_REGFLD_FLASH_ACR_PRFTEN                    (0x00000001UL <<  8U)
#define CHIP_REGFLD_FLASH_ACR_ICEN                      (0x00000001UL <<  9U)
#define CHIP_REGFLD_FLASH_ACR_ICRST                     (0x00000001UL << 11U)
#define CHIP_REGFLD_FLASH_ACR_EMPTY                     (0x00000001UL << 16U)
#define CHIP_REGFLD_FLASH_ACR_DBG_SWEN                  (0x00000001UL << 18U)

/*******************  Bits definition for FLASH_SR register  ******************/
#define CHIP_REGFLD_FLASH_SR_EOP                        (0x00000001UL <<  0U)
#define CHIP_REGFLD_FLASH_SR_OPERR                      (0x00000001UL <<  1U)
#define CHIP_REGFLD_FLASH_SR_PROGERR                    (0x00000001UL <<  3U)
#define CHIP_REGFLD_FLASH_SR_WRPERR                     (0x00000001UL <<  4U)
#define CHIP_REGFLD_FLASH_SR_PGAERR                     (0x00000001UL <<  5U)
#define CHIP_REGFLD_FLASH_SR_SIZERR                     (0x00000001UL <<  6U)
#define CHIP_REGFLD_FLASH_SR_PGSERR                     (0x00000001UL <<  7U)
#define CHIP_REGFLD_FLASH_SR_MISSERR                    (0x00000001UL <<  8U)
#define CHIP_REGFLD_FLASH_SR_FASTERR                    (0x00000001UL <<  9U)
#define CHIP_REGFLD_FLASH_SR_RDERR                      (0x00000001UL << 14U)
#define CHIP_REGFLD_FLASH_SR_OPTVERR                    (0x00000001UL << 15U)
#define CHIP_REGFLD_FLASH_SR_BSY1                       (0x00000001UL << 16U)
#define CHIP_REGFLD_FLASH_SR_BSY2                       (0x00000001UL << 17U)
#define CHIP_REGFLD_FLASH_SR_CFGBSY                     (0x00000001UL << 18U)

/*******************  Bits definition for FLASH_CR register  ******************/
#define CHIP_REGFLD_FLASH_CR_PG                         (0x00000001UL <<  0U)
#define CHIP_REGFLD_FLASH_CR_PER                        (0x00000001UL <<  1U)
#define CHIP_REGFLD_FLASH_CR_MER1                       (0x00000001UL <<  2U)
#define CHIP_REGFLD_FLASH_CR_PNB                        (0x000001FFUL <<  3U)
#define CHIP_REGFLD_FLASH_CR_BKER                       (0x00000001UL << 13U)
#define CHIP_REGFLD_FLASH_CR_MER2                       (0x00000001UL << 15U)
#define CHIP_REGFLD_FLASH_CR_STRT                       (0x00000001UL << 16U)
#define CHIP_REGFLD_FLASH_CR_OPTSTRT                    (0x00000001UL << 17U)
#define CHIP_REGFLD_FLASH_CR_FSTPG                      (0x00000001UL << 18U)
#define CHIP_REGFLD_FLASH_CR_EOPIE                      (0x00000001UL << 24U)
#define CHIP_REGFLD_FLASH_CR_ERRIE                      (0x00000001UL << 25U)
#define CHIP_REGFLD_FLASH_CR_RDERRIE                    (0x00000001UL << 26U)
#define CHIP_REGFLD_FLASH_CR_OBL_LAUNCH                 (0x00000001UL << 27U)
#define CHIP_REGFLD_FLASH_CR_SEC_PROT                   (0x00000001UL << 28U)
#define CHIP_REGFLD_FLASH_CR_SEC_PROT2                  (0x00000001UL << 29U)
#define CHIP_REGFLD_FLASH_CR_OPTLOCK                    (0x00000001UL << 30U)
#define CHIP_REGFLD_FLASH_CR_LOCK                       (0x00000001UL << 31U)

/*******************  Bits definition for FLASH_ECCR register  ****************/
#define CHIP_REGFLD_FLASH_ECCR_ADDR_ECC                 (0x00007FFFUL <<  0U)
#define CHIP_REGFLD_FLASH_ECCR_SYSF_ECC                 (0x00000001UL << 20U)
#define CHIP_REGFLD_FLASH_ECCR_ECCCIE                   (0x00000001UL << 24U)
#define CHIP_REGFLD_FLASH_ECCR_ECCC                     (0x00000001UL << 30U)
#define CHIP_REGFLD_FLASH_ECCR_ECCD                     (0x00000001UL << 31U)

/*******************  Bits definition for FLASH_OPTR register  ****************/
#define CHIP_REGFLD_FLASH_OPTR_RDP                      (0x000000FFUL <<  0U)
#define CHIP_REGFLD_FLASH_OPTR_BOR_EN                   (0x00000001UL <<  8U)
#define CHIP_REGFLD_FLASH_OPTR_BORR_LEV                 (0x00000003UL <<  9U)
#define CHIP_REGFLD_FLASH_OPTR_BORF_LEV                 (0x00000003UL << 11U)
#define CHIP_REGFLD_FLASH_OPTR_NRST_STOP                (0x00000001UL << 13U)
#define CHIP_REGFLD_FLASH_OPTR_NRST_STDBY               (0x00000001UL << 14U)
#define CHIP_REGFLD_FLASH_OPTR_NRST_SHDW                (0x00000001UL << 15U)
#define CHIP_REGFLD_FLASH_OPTR_IWDG_SW                  (0x00000001UL << 16U)
#define CHIP_REGFLD_FLASH_OPTR_IWDG_STOP                (0x00000001UL << 17U)
#define CHIP_REGFLD_FLASH_OPTR_IWDG_STDBY               (0x00000001UL << 18U)
#define CHIP_REGFLD_FLASH_OPTR_WWDG_SW                  (0x00000001UL << 19U)
#define CHIP_REGFLD_FLASH_OPTR_NSWAP_BANK               (0x00000001UL << 20U)
#define CHIP_REGFLD_FLASH_OPTR_DUAL_BANK                (0x00000001UL << 21U)
#define CHIP_REGFLD_FLASH_OPTR_RAM_PARITY_CHECK         (0x00000001UL << 22U)
#define CHIP_REGFLD_FLASH_OPTR_NBOOT_SEL                (0x00000001UL << 24U)
#define CHIP_REGFLD_FLASH_OPTR_NBOOT1                   (0x00000001UL << 25U)
#define CHIP_REGFLD_FLASH_OPTR_NBOOT0                   (0x00000001UL << 26U)
#define CHIP_REGFLD_FLASH_OPTR_NRST_MODE                (0x00000003UL << 27U)
#define CHIP_REGFLD_FLASH_OPTR_IRHEN                    (0x00000001UL << 29U)


#define CHIP_REGFLDVAL_FLASH_OPTR_NRST_MODE_INPUT       (1)
#define CHIP_REGFLDVAL_FLASH_OPTR_NRST_MODE_GPIO        (2)
#define CHIP_REGFLDVAL_FLASH_OPTR_NRST_MODE_BIDIRECT    (3)

/******************  Bits definition for FLASH_PCROPXXSR register  ************/
#define CHIP_REGFLD_FLASH_PCROPXXSR_PCROPXX_STRT        (0x000001FFUL <<  0U)

/******************  Bits definition for FLASH_PCROPXXER register  ************/
#define CHIP_REGFLD_FLASH_PCROPXXER_PCROPXX_END         (0x000001FFUL <<  0U)
#define CHIP_REGFLD_FLASH_PCROP1AER_PCROP_RDP           (0x00000001UL << 31U)

/******************  Bits definition for FLASH_WRPXXR register  ***************/
#define CHIP_REGFLD_FLASH_WRPXXR_WRPXX_STRT             (0x0000007FUL <<  0U)
#define CHIP_REGFLD_FLASH_WRPXXR_WRPXX_END              (0x0000007FUL << 16U)

/******************  Bits definition for FLASH_SECR register  *****************/
#define CHIP_REGFLD_FLASH_SECR_SEC_SIZE                 (0x000000FFUL <<  0U)
#define CHIP_REGFLD_FLASH_SECR_BOOT_LOCK                (0x00000001UL << 16U)
#define CHIP_REGFLD_FLASH_SECR_SEC_SIZE2                (0x000001FFUL << 20U)


#endif // CHIP_STM32G0XX_REGS_FLASH_H
