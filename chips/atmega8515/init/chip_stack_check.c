#include "chip_stack_check.h"


#ifndef CHIP_CFG_STACK_TRACK_UNUSED_WORDS
    #define CHIP_STACK_TRACK_UNUSED_WORDS 0
#else
    #define CHIP_STACK_TRACK_UNUSED_WORDS CHIP_CFG_STACK_TRACK_UNUSED_WORDS
#endif

#if CHIP_STACK_TRACK_UNUSED_WORDS

#define STACK_CANARY 0xc5

void StackPaint(void) __attribute__ ((naked)) __attribute__ ((section (".init1")));

extern uint8_t _end;
extern uint8_t __stack;

 void StackPaint(void) {
     __asm volatile ("    ldi r30,lo8(_end)\n"
                     "    ldi r31,hi8(_end)\n"
                     "    ldi r24,lo8(0xc5)\n" /* STACK_CANARY = 0xc5 */
                     "    ldi r25,hi8(__stack)\n"
                     "    rjmp .cmp\n"
                     ".loop:\n"
                     "    st Z+,r24\n"
                     ".cmp:\n"
                     "    cpi r30,lo8(__stack)\n"
                     "    cpc r31,r25\n"
                     "    brlo .loop\n"
                     "    breq .loop"::);
}
#endif

uint16_t chip_stack_get_unused_words (void) {
#if CHIP_STACK_TRACK_UNUSED_WORDS
     const uint8_t *p = &_end;
     uint16_t       c = 0;

     while(*p == STACK_CANARY && p <= &__stack) {
         p++;
         c++;
     }

     return c;
#else
    return 0;
#endif
 }
