#ifndef CHIP_CLK_DEFS_H
#define CHIP_CLK_DEFS_H

#include "chip_std_defs.h"


#define CHIP_CLK_LSE_DRV_LOWEST         0
#define CHIP_CLK_LSE_DRV_MEDIUM_LOW     1
#define CHIP_CLK_LSE_DRV_MEDIUM_HIGH    2
#define CHIP_CLK_LSE_DRV_HIGHEST        3


/* варианты настроек частоты (приблизительное значение) внутреннего источника HSI */
#define CHIP_CLK_HSI_FREQ_8MHZ      CHIP_MHZ(8)
#define CHIP_CLK_HSI_FREQ_16MHZ     CHIP_MHZ(16)
#define CHIP_CLK_HSI_FREQ_32MHZ     CHIP_MHZ(32)
#define CHIP_CLK_HSI_FREQ_64MHZ     CHIP_MHZ(64)

/* значения частот (приблизительные) внетренних источников LSI, CSI, HSI48 */
#define CHIP_CLK_LSI_FREQ           CHIP_KHZ(32)
#define CHIP_CLK_CSI_FREQ           CHIP_MHZ(4)
#define CHIP_CLK_HSI48_FREQ         CHIP_MHZ(48)

#define CHIP_CLK_CPU_INITIAL_FREQ   CHIP_CLK_HSI_FREQ_64MHZ



/* Номера идентификаторов различных клоков, которые могут использоваться в качестве
 * источников.
 * Номера используются исключительно для уникальной идентификации
 * для сравнения настроек, сами номера должны  быть уникальные,
 * но не связаны с какими либо значениеми самого контроллера.
 * В настройках источников сигнала используются значения без суффикса _ID,
 * а уже по этомим значениям с помощью макросов CHIP_CLK_ID_VAL получается
 * ID из таблицы. Это позволяет также по определению получить параметры выбранной
 * частоты с помощью CHIP_CLK_ID_EN() и CHIP_CLK_ID_FREQ() */
#define CHIP_CLK_HSE_ID             1
#define CHIP_CLK_LSE_ID             2
#define CHIP_CLK_HSI_ID             3
#define CHIP_CLK_HSI_KER_ID         CHIP_CLK_HSI_ID
#define CHIP_CLK_LSI_ID             4
#define CHIP_CLK_CSI_ID             5
#define CHIP_CLK_CSI_KER_ID         CHIP_CLK_CSI_ID
#define CHIP_CLK_HSI48_ID           6
#define CHIP_CLK_I2S_CKIN_ID        7 /* внешняя частота I2S CLKIN */
#define CHIP_CLK_RTC_HSE_ID         10 /* деленная частота HSE, предназначенная как вариант для подачи на RTC */
#define CHIP_CLK_RTC_ID             11
#define CHIP_CLK_PLL1_P_ID          100
#define CHIP_CLK_PLL1_Q_ID          101
#define CHIP_CLK_PLL1_R_ID          102
#define CHIP_CLK_PLL2_P_ID          103
#define CHIP_CLK_PLL2_Q_ID          104
#define CHIP_CLK_PLL2_R_ID          105
#define CHIP_CLK_PLL3_P_ID          106
#define CHIP_CLK_PLL3_Q_ID          107
#define CHIP_CLK_PLL3_R_ID          108
#define CHIP_CLK_SYS_ID             200
#define CHIP_CLK_CPU_CM7_ID         201
#define CHIP_CLK_CPU_CM4_ID         202
#define CHIP_CLK_HOST_ID            203
#define CHIP_CLK_AXI_ID             CHIP_CLK_HOST_ID
#define CHIP_CLK_AHB1_ID            CHIP_CLK_HOST_ID
#define CHIP_CLK_AHB2_ID            CHIP_CLK_HOST_ID
#define CHIP_CLK_AHB3_ID            CHIP_CLK_HOST_ID
#define CHIP_CLK_AHB4_ID            CHIP_CLK_HOST_ID
#define CHIP_CLK_APB1_ID            203
#define CHIP_CLK_APB2_ID            204
#define CHIP_CLK_APB3_ID            205
#define CHIP_CLK_APB4_ID            206
#define CHIP_CLK_TIMX_KER_ID        210
#define CHIP_CLK_TIMY_KER_ID        211
#define CHIP_CLK_HRTIM_KER_ID       212
#define CHIP_CLK_IWG1_ID            213
#define CHIP_CLK_FLASH_ID           214
#define CHIP_CLK_SPIDIFRX_SYM_ID    215
#define CHIP_CLK_DSI_PHY_ID         216
#define CHIP_CLK_PER_ID             300

#endif // CHIP_CLK_DEFS_H
