/*
 * @brief LPC18xx/43xx GIMA driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licenser disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef __GIMA_18XX_43XX_H_
#define __GIMA_18XX_43XX_H_

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup GIMA_18XX_43XX CHIP: LPC18xx/43xx GIMA driver
 * @ingroup CHIP_18XX_43XX_Drivers
 * @{
 */

/**
 * @brief Global Input Multiplexer Array (GIMA) register block structure
 */
typedef struct {						/*!< GIMA Structure */
	__IO uint32_t  CAP0_IN[4][4];		/*!< Timer x CAP0_y capture input multiplexer (GIMA output ((x*4)+y)) */
	__IO uint32_t  CTIN_IN[8];			/*!< SCT CTIN_x capture input multiplexer (GIMA output (16+x)) */
	__IO uint32_t  ADCHS_TRIGGER_IN;	/*!< ADCHS trigger input multiplexer (GIMA output 24) */
	__IO uint32_t  EVENTROUTER_13_IN;	/*!< Event router input 13 multiplexer (GIMA output 25) */
	__IO uint32_t  EVENTROUTER_14_IN;	/*!< Event router input 14 multiplexer (GIMA output 26) */
	__IO uint32_t  EVENTROUTER_16_IN;	/*!< Event router input 16 multiplexer (GIMA output 27) */
	__IO uint32_t  ADCSTART0_IN;		/*!< ADC start0 input multiplexer (GIMA output 28) */
	__IO uint32_t  ADCSTART1_IN;		/*!< ADC start1 input multiplexer (GIMA output 29) */
} LPC_GIMA_T;

/**
 * @brief Global Input Multiplexer Array (GIMA) register value constants
 */
#define LPC_GIMA_INVERT                 (1<<0)  /*!< bit 0: Invert GIMA input */

#define LPC_GIMA_MODE_DIRECT            (0<<1)  /*!< bits 3..1: Asynchronous propagation of signal to peripheral */
#define LPC_GIMA_MODE_SYNC              (2<<1)  /*!< bits 3..1: Synchronize to branch clock */
#define LPC_GIMA_MODE_SYNC_PULSE        (6<<1)  /*!< bits 3..1: Convert to 1-clock pulses */
#define LPC_GIMA_MODE_EDGE_SYNC_PULSE   (7<<1)  /*!< bits 3..1: Convert to 1-clock pulses with rising edge detector */

/* SELECT field for all registers */
#define LPC_GIMA_SEL_CAP0_0_CTIN0               (0<<4)
#define LPC_GIMA_SEL_CAP0_0_SGPIO3              (1<<4)
#define LPC_GIMA_SEL_CAP0_0_T0CAP0              (2<<4)
#define LPC_GIMA_SEL_CAP0_1_CTIN1               (0<<4)
#define LPC_GIMA_SEL_CAP0_1_USART2_TX_ACTIVE    (1<<4)
#define LPC_GIMA_SEL_CAP0_1_T0CAP1              (2<<4)
#define LPC_GIMA_SEL_CAP0_2_CTIN2               (0<<4)
#define LPC_GIMA_SEL_CAP0_2_SGPIO3_DIV          (1<<4)
#define LPC_GIMA_SEL_CAP0_2_T0CAP2              (2<<4)
#define LPC_GIMA_SEL_CAP0_3_CTOUT15_OR_T3MAT3   (0<<4)
#define LPC_GIMA_SEL_CAP0_3_T0CAP3              (1<<4)
#define LPC_GIMA_SEL_CAP0_3_T3MAT3              (2<<4)
#define LPC_GIMA_SEL_CAP1_0_CTIN0               (0<<4)
#define LPC_GIMA_SEL_CAP1_0_SGPIO12             (1<<4)
#define LPC_GIMA_SEL_CAP1_0_T1CAP0              (2<<4)
#define LPC_GIMA_SEL_CAP1_1_CTIN3               (0<<4)
#define LPC_GIMA_SEL_CAP1_1_USART0_TX_ACTIVE    (1<<4)
#define LPC_GIMA_SEL_CAP1_1_T1CAP1              (2<<4)
#define LPC_GIMA_SEL_CAP1_2_CTIN4               (0<<4)
#define LPC_GIMA_SEL_CAP1_2_USART0_RX_ACTIVE    (1<<4)
#define LPC_GIMA_SEL_CAP1_2_T1CAP2              (2<<4)
#define LPC_GIMA_SEL_CAP1_3_CTOUT3_OR_T0MAT3    (0<<4)
#define LPC_GIMA_SEL_CAP1_3_T1CAP3              (1<<4)
#define LPC_GIMA_SEL_CAP1_3_T0MAT3              (2<<4)
#define LPC_GIMA_SEL_CAP2_0_CTIN0               (0<<4)
#define LPC_GIMA_SEL_CAP2_0_SGPIO12_DIV         (1<<4)
#define LPC_GIMA_SEL_CAP2_0_T2CAP0              (2<<4)
#define LPC_GIMA_SEL_CAP2_1_CTIN1               (0<<4)
#define LPC_GIMA_SEL_CAP2_1_USART2_TX_ACTIVE    (1<<4)
#define LPC_GIMA_SEL_CAP2_1_I2S1_RX_MWS         (2<<4)
#define LPC_GIMA_SEL_CAP2_1_T2CAP1              (3<<4)
#define LPC_GIMA_SEL_CAP2_2_CTIN5               (0<<4)
#define LPC_GIMA_SEL_CAP2_2_USART2_RX_ACTIVE    (1<<4)
#define LPC_GIMA_SEL_CAP2_2_I2S1_TX_MWS         (2<<4)
#define LPC_GIMA_SEL_CAP2_2_T2CAP2              (3<<4)
#define LPC_GIMA_SEL_CAP2_3_CTOUT7_OR_T1MAT3    (0<<4)
#define LPC_GIMA_SEL_CAP2_3_T2CAP3              (1<<4)
#define LPC_GIMA_SEL_CAP2_3_T1MAT3              (2<<4)
#define LPC_GIMA_SEL_CAP3_0_CTIN0               (0<<4)
#define LPC_GIMA_SEL_CAP3_0_I2S0_RX_MWS         (1<<4)
#define LPC_GIMA_SEL_CAP3_0_T3CAP0              (2<<4)
#define LPC_GIMA_SEL_CAP3_1_CTIN6               (0<<4)
#define LPC_GIMA_SEL_CAP3_1_USART3_TX_ACTIVE    (1<<4)
#define LPC_GIMA_SEL_CAP3_1_I2S0_TX_MWS         (2<<4)
#define LPC_GIMA_SEL_CAP3_1_T3CAP1              (3<<4)
#define LPC_GIMA_SEL_CAP3_2_CTIN7               (0<<4)
#define LPC_GIMA_SEL_CAP3_2_USART3_RX_ACTIVE    (1<<4)
#define LPC_GIMA_SEL_CAP3_2_USB0_SOF            (2<<4)
#define LPC_GIMA_SEL_CAP3_2_T3CAP2              (3<<4)
#define LPC_GIMA_SEL_CAP3_3_CTOUT11_OR_T2MAT3   (0<<4)
#define LPC_GIMA_SEL_CAP3_3_USB1_SOF            (1<<4)
#define LPC_GIMA_SEL_CAP3_3_T3CAP3              (2<<4)
#define LPC_GIMA_SEL_CAP3_3_T2MAT3              (3<<4)
#define LPC_GIMA_SEL_CTIN0_CTIN0                (0<<4)
#define LPC_GIMA_SEL_CTIN0_SGPIO3               (1<<4)
#define LPC_GIMA_SEL_CTIN0_SGPIO3_DIV           (2<<4)
#define LPC_GIMA_SEL_CTIN1_CTIN1                (0<<4)
#define LPC_GIMA_SEL_CTIN1_USART2_TX_ACTIVE     (1<<4)
#define LPC_GIMA_SEL_CTIN1_SGPIO12              (2<<4)
#define LPC_GIMA_SEL_CTIN2_CTIN2                (0<<4)
#define LPC_GIMA_SEL_CTIN2_SGPIO12              (1<<4)
#define LPC_GIMA_SEL_CTIN2_SGPIO12_DIV          (2<<4)
#define LPC_GIMA_SEL_CTIN3_CTIN3                (0<<4)
#define LPC_GIMA_SEL_CTIN3_USART0_TX_ACTIVE     (1<<4)
#define LPC_GIMA_SEL_CTIN4_CTIN4                (0<<4)
#define LPC_GIMA_SEL_CTIN4_USART0_RX_ACTIVE     (1<<4)
#define LPC_GIMA_SEL_CTIN4_I2S1_RX_MWS          (2<<4)
#define LPC_GIMA_SEL_CTIN4_I2S1_TX_MWS          (3<<4)
#define LPC_GIMA_SEL_CTIN5_CTIN5                (0<<4)
#define LPC_GIMA_SEL_CTIN5_USART2_RX_ACTIVE     (1<<4)
#define LPC_GIMA_SEL_CTIN5_SGPIO12_DIV          (2<<4)
#define LPC_GIMA_SEL_CTIN6_CTIN6                (0<<4)
#define LPC_GIMA_SEL_CTIN6_USART3_TX_ACTIVE     (1<<4)
#define LPC_GIMA_SEL_CTIN6_I2S0_RX_MWS          (2<<4)
#define LPC_GIMA_SEL_CTIN6_I2S0_TX_MWS          (3<<4)
#define LPC_GIMA_SEL_CTIN7_CTIN7                (0<<4)
#define LPC_GIMA_SEL_CTIN7_USART3_RX_ACTIVE     (1<<4)
#define LPC_GIMA_SEL_CTIN7_USB0_SOF             (2<<4)
#define LPC_GIMA_SEL_CTIN7_USB1_SOF             (3<<4)
#define LPC_GIMA_SEL_ADCHS_GPIO6_28             (0<<4)
#define LPC_GIMA_SEL_ADCHS_GPIO5_3              (1<<4)
#define LPC_GIMA_SEL_ADCHS_SGPIO10              (2<<4)
#define LPC_GIMA_SEL_ADCHS_SGPIO12              (3<<4)
#define LPC_GIMA_SEL_ADCHS_MCOB2                (5<<4)
#define LPC_GIMA_SEL_ADCHS_CTOUT0_OR_T0MAT0     (6<<4)
#define LPC_GIMA_SEL_ADCHS_CTOUT8_OR_T2MAT0     (7<<4)
#define LPC_GIMA_SEL_ADCHS_T0MAT0               (8<<4)
#define LPC_GIMA_SEL_ADCHS_T2MAT0               (9<<4)
#define LPC_GIMA_SEL_EVTROUTER13_CTOUT2_OR_T0MAT2 (0<<4)
#define LPC_GIMA_SEL_EVTROUTER13_SGPIO3         (1<<4)
#define LPC_GIMA_SEL_EVTROUTER13_T0MAT2         (2<<4)
#define LPC_GIMA_SEL_EVTROUTER14_CTOUT6_OR_T1MAT2 (0<<4)
#define LPC_GIMA_SEL_EVTROUTER14_SGPIO12        (1<<4)
#define LPC_GIMA_SEL_EVTROUTER14_T1MAT2         (2<<4)
#define LPC_GIMA_SEL_EVTROUTER16_CTOUT14_OR_T3MAT2 (0<<4)
#define LPC_GIMA_SEL_EVTROUTER16_T3MAT2         (1<<4)
#define LPC_GIMA_SEL_ADCSTART0_CTOUT15_OR_T3MAT3 (0<<4) /*!< always synchronize */
#define LPC_GIMA_SEL_ADCSTART0_T0MAT0           (1<<4)  /*!< always synchronize */
#define LPC_GIMA_SEL_ADCSTART1_CTOUT8_OR_T2MAT0 (0<<4)  /*!< always synchronize */
#define LPC_GIMA_SEL_ADCSTART1_T2MAT0           (1<<4)  /*!< always synchronize */

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __GIMA_18XX_43XX_H_ */
