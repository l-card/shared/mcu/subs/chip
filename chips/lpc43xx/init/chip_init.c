/*================================================================================================*
 * I/O registers and chip configuration for LPC43xx
 *================================================================================================*/

#include "chip.h"

/*================================================================================================*/
#define MHZ(n)          (1000000UL * (n))

#define PLL0_FMIN_O     MHZ(275)
#define PLL0_FMAX_O     MHZ(550)
#define PLL1_FMIN_I     MHZ(1)
#define PLL1_FMAX_I     MHZ(50)
#define PLL1_FMIN_O     MHZ(156)
#define PLL1_FMAX_O     MHZ(320)

#ifndef MAX
    #define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif

/* Проверка параметров PLL1 */
#if LPC_USE_PLL1
    #if (LPC_PLL1_M < 1) || (LPC_PLL1_M > 256)
        #error "Invalid value of LPC_PLL1_M"
    #endif
    #if (LPC_PLL1_N < 1) || (LPC_PLL1_N > 4)
        #error "Invalid value of LPC_PLL1_N"
    #endif
    #if (LPC_PLL1_P < 1) || (LPC_PLL1_P > 16) || (LPC_PLL1_P & (LPC_PLL1_P - 1))
        #error "Invalid value of LPC_PLL1_P"
    #endif

    /* Трассировка источника клоков PLL1 */
    /* 1. IDIVB..IDIVE могут иметь источником IDIVA */
    #if LPC_PLL1_SRC == LPC_CLKSEL_IDIVB
    #define PLL1_SRC_TRACE_1    LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG)
    #elif LPC_PLL1_SRC == LPC_CLKSEL_IDIVB
    #define PLL1_SRC_TRACE_1    LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG)
    #elif LPC_PLL1_SRC == LPC_CLKSEL_IDIVB
    #define PLL1_SRC_TRACE_1    LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG)
    #elif LPC_PLL1_SRC == LPC_CLKSEL_IDIVB
    #define PLL1_SRC_TRACE_1    LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG)
    #else
    #define PLL1_SRC_TRACE_1    LPC_PLL1_SRC
    #endif
    /* 2. Если это IDIVA, взять его источник */
    #if PLL1_SRC_TRACE_1 == LPC_CLKSEL_IDIVA
    #define PLL1_SRC_TRACE_2    LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG)
    #else
    #define PLL1_SRC_TRACE_2    PLL1_SRC_TRACE_1
    #endif
    /* Проверка источника (можно IRC, OSC, GPCLKIN) */
    #if (PLL1_SRC_TRACE_2 != LPC_CLKSEL_IRC) && (PLL1_SRC_TRACE_2 != LPC_CLKSEL_OSC) && \
        (PLL1_SRC_TRACE_2 != LPC_CLKSEL_GPCLKIN)
    #error "Unsupported clock source for PLL1"
    #endif

    #if (LPC_CLKSRCF_PLL1 < PLL1_FMIN_I) || (LPC_CLKSRCF_PLL1 > PLL1_FMAX_I)
        #error "PLL1 input frequency out of range"
    #endif

    #if (LPC_PLL1_FCCO < PLL1_FMIN_O) || (LPC_PLL1_FCCO > PLL1_FMAX_O)
        #error "PLL1 CCO frequency out of range"
    #endif

#endif

/* Проверка диапазона частоты процессора */
#if LPC_SYSCLK > MHZ(204)
    #error "Main clock frequency out of range "
#endif


#if LPC_INIT_WDT
    #if !LPC_CLKEN_WDT
        #error "WDT clock must be enabled for WDT configuration"
    #endif

    #ifndef LPC_WDT_WINDOW_VAL
        #define LPC_WDT_WINDOW_VAL 0x01000000
    #endif

    #if LPC_WDT_TIMEOUT_VAL > 0x01000000
        #error "WDT timeout value too high"
    #endif
    #if LPC_WDT_TIMEOUT_VAL < 256
        #error "WDT timeout too low"
    #endif
    #if LPC_WDT_WINDOW_VAL > 0x01000000
        #error "WDT window value too high"
    #endif

    #if LPC_WDT_WARNTIME_VAL  > 1024
        #error "WDT warn time too high"
    #endif


#endif
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/

__attribute__ ((__noinline__))
void chip_delay_clk(unsigned int clk_count) {
#define CYCLES_OVERHEAD     7
    /* Задержка примерно на clk_count тактов, min = 11 тактов */
    asm volatile (
         "  subs %[reg], %[reg], %[adj]\n"
         "  asrs %[reg], %[reg], #2\n"  /* 4 cycles per loop */
         "0:"
         "  subs %[reg], %[reg], #0\n"
         "  subs %[reg], %[reg], #1\n"
         "  bgt 0b"
         : [reg] "+r" (clk_count)
         : [adj] "i" (CYCLES_OVERHEAD - 3)
         : "cc"
        );
#undef CYCLES_OVERHEAD
    }
/*------------------------------------------------------------------------------------------------*/



#if defined LPC_EMC_CONFIG && (LPC_EMC_CONFIG==1)


#if LPC_SRAM0_EN
    #if LPC_SRAM0_WIDTH == 8
        #define EMC_SRAM0_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_8
    #elif LPC_SRAM0_WIDTH == 16
        #define EMC_SRAM0_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_16
    #elif LPC_SRAM0_WIDTH == 32
        #define EMC_SRAM0_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_32
    #else
        #error "Invalid external SRAM 0 width specified"
    #endif

    /* определение параметров по-умолчанию */
    #ifndef LPC_SRAM0_PAGE_MODE
        #define LPC_SRAM0_PAGE_MODE 0
    #endif
    #ifndef LPC_SRAM0_CS_POL
        #define LPC_SRAM0_CS_POL 0
    #endif
    #ifndef LPC_SRAM0_BLS_RW
        #define LPC_SRAM0_BLS_RW 0
    #endif
    #ifndef LPC_SRAM0_EXT_WAIT
        #define LPC_SRAM0_EXT_WAIT 0
    #endif
    #ifndef LPC_SRAM0_BUF_EN
        #define LPC_SRAM0_BUF_EN 0
    #endif
    #ifndef LPC_SRAM0_WR_PROT
        #define LPC_SRAM0_WR_PROT 0
    #endif

    /* если не определены конкретные задержки, то беруться общие */
    #ifndef LPC_SRAM0_T_WEN_CYCLES
        #define LPC_SRAM0_T_WEN_CYCLES LPC_SRAM_T_WEN_CYCLES
    #endif
    #ifndef LPC_SRAM0_T_OEN_CYCLES
        #define LPC_SRAM0_T_OEN_CYCLES LPC_SRAM_T_OEN_CYCLES
    #endif
    #ifndef LPC_SRAM0_T_RD_CYCLES
        #define LPC_SRAM0_T_RD_CYCLES LPC_SRAM_T_RD_CYCLES
    #endif
    #ifndef LPC_SRAM0_T_PAGE_CYCLES
        #define LPC_SRAM0_T_PAGE_CYCLES LPC_SRAM_T_PAGE_CYCLES
    #endif
    #ifndef LPC_SRAM0_T_WR_CYCLES
        #define LPC_SRAM0_T_WR_CYCLES LPC_SRAM_T_WR_CYCLES
    #endif
    #ifndef LPC_SRAM0_T_TURN_CYCLES
        #define LPC_SRAM0_T_TURN_CYCLES LPC_SRAM_T_TURN_CYCLES
    #endif
#else
    #undef  LPC_SRAM0_WIDTH
    #define LPC_SRAM0_WIDTH 0
#endif



#ifndef LPC_SRAM1_EN
    #define LPC_SRAM1_EN 0
#endif

#if LPC_SRAM1_EN
    #if LPC_SRAM1_WIDTH == 8
        #define EMC_SRAM1_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_8
    #elif LPC_SRAM1_WIDTH == 16
        #define EMC_SRAM1_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_16
    #elif LPC_SRAM1_WIDTH == 32
        #define EMC_SRAM1_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_32
    #else
        #error "Invalid external SRAM 0 width specified"
    #endif


    #ifndef LPC_SRAM1_PAGE_MODE
        #define LPC_SRAM1_PAGE_MODE 0
    #endif
    #ifndef LPC_SRAM1_CS_POL
        #define LPC_SRAM1_CS_POL 0
    #endif
    #ifndef LPC_SRAM1_BLS_RW
        #define LPC_SRAM1_BLS_RW 0
    #endif
    #ifndef LPC_SRAM1_EXT_WAIT
        #define LPC_SRAM1_EXT_WAIT 0
    #endif
    #ifndef LPC_SRAM1_BUF_EN
        #define LPC_SRAM1_BUF_EN 0
    #endif
    #ifndef LPC_SRAM1_WR_PROT
        #define LPC_SRAM1_WR_PROT 0
    #endif

    /* если не определены конкретные задержки, то беруться общие */
    #ifndef LPC_SRAM1_T_WEN_CYCLES
        #define LPC_SRAM1_T_WEN_CYCLES LPC_SRAM_T_WEN_CYCLES
    #endif
    #ifndef LPC_SRAM1_T_OEN_CYCLES
        #define LPC_SRAM1_T_OEN_CYCLES LPC_SRAM_T_OEN_CYCLES
    #endif
    #ifndef LPC_SRAM1_T_RD_CYCLES
        #define LPC_SRAM1_T_RD_CYCLES LPC_SRAM_T_RD_CYCLES
    #endif
    #ifndef LPC_SRAM1_T_PAGE_CYCLES
        #define LPC_SRAM1_T_PAGE_CYCLES LPC_SRAM_T_PAGE_CYCLES
    #endif
    #ifndef LPC_SRAM1_T_WR_CYCLES
        #define LPC_SRAM1_T_WR_CYCLES LPC_SRAM_T_WR_CYCLES
    #endif
    #ifndef LPC_SRAM1_T_TURN_CYCLES
        #define LPC_SRAM1_T_TURN_CYCLES LPC_SRAM_T_TURN_CYCLES
    #endif
#else
    #undef  LPC_SRAM1_WIDTH
    #define LPC_SRAM1_WIDTH 0
#endif

#ifndef LPC_SRAM2_EN
    #define LPC_SRAM2_EN 0
#endif

#if LPC_SRAM2_EN
    #if LPC_SRAM2_WIDTH == 8
        #define EMC_SRAM2_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_8
    #elif LPC_SRAM2_WIDTH == 16
        #define EMC_SRAM2_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_16
    #elif LPC_SRAM2_WIDTH == 32
        #define EMC_SRAM2_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_32
    #else
        #error "Invalid external SRAM 0 width specified"
    #endif

    #ifndef LPC_SRAM2_PAGE_MODE
        #define LPC_SRAM2_PAGE_MODE 0
    #endif
    #ifndef LPC_SRAM2_CS_POL
        #define LPC_SRAM2_CS_POL 0
    #endif
    #ifndef LPC_SRAM2_BLS_RW
        #define LPC_SRAM2_BLS_RW 0
    #endif
    #ifndef LPC_SRAM2_EXT_WAIT
        #define LPC_SRAM2_EXT_WAIT 0
    #endif
    #ifndef LPC_SRAM2_BUF_EN
        #define LPC_SRAM2_BUF_EN 0
    #endif
    #ifndef LPC_SRAM2_WR_PROT
        #define LPC_SRAM2_WR_PROT 0
    #endif

    /* если не определены конкретные задержки, то беруться общие */
    #ifndef LPC_SRAM2_T_WEN_CYCLES
        #define LPC_SRAM2_T_WEN_CYCLES LPC_SRAM_T_WEN_CYCLES
    #endif
    #ifndef LPC_SRAM2_T_OEN_CYCLES
        #define LPC_SRAM2_T_OEN_CYCLES LPC_SRAM_T_OEN_CYCLES
    #endif
    #ifndef LPC_SRAM2_T_RD_CYCLES
        #define LPC_SRAM2_T_RD_CYCLES LPC_SRAM_T_RD_CYCLES
    #endif
    #ifndef LPC_SRAM2_T_PAGE_CYCLES
        #define LPC_SRAM2_T_PAGE_CYCLES LPC_SRAM_T_PAGE_CYCLES
    #endif
    #ifndef LPC_SRAM2_T_WR_CYCLES
        #define LPC_SRAM2_T_WR_CYCLES LPC_SRAM_T_WR_CYCLES
    #endif
    #ifndef LPC_SRAM2_T_TURN_CYCLES
        #define LPC_SRAM2_T_TURN_CYCLES LPC_SRAM_T_TURN_CYCLES
    #endif
#else
    #undef  LPC_SRAM2_WIDTH
    #define LPC_SRAM2_WIDTH 0
#endif

#ifndef LPC_SRAM3_EN
    #define LPC_SRAM3_EN 0
#endif

#if LPC_SRAM3_EN
    #if LPC_SRAM3_WIDTH == 8
        #define EMC_SRAM3_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_8
    #elif LPC_SRAM3_WIDTH == 16
        #define EMC_SRAM3_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_16
    #elif LPC_SRAM3_WIDTH == 32
        #define EMC_SRAM3_WIDTH_CFG EMC_STATIC_CONFIG_MEM_WIDTH_32
    #else
        #error "Invalid external SRAM 0 width specified"
    #endif

    #ifndef LPC_SRAM3_PAGE_MODE
        #define LPC_SRAM3_PAGE_MODE 0
    #endif
    #ifndef LPC_SRAM3_CS_POL
        #define LPC_SRAM3_CS_POL 0
    #endif
    #ifndef LPC_SRAM3_BLS_RW
        #define LPC_SRAM3_BLS_RW 0
    #endif
    #ifndef LPC_SRAM3_EXT_WAIT
        #define LPC_SRAM3_EXT_WAIT 0
    #endif

    #ifndef LPC_SRAM3_BUF_EN
        #define LPC_SRAM3_BUF_EN 0
    #endif

    #ifndef LPC_SRAM3_WR_PROT
        #define LPC_SRAM3_WR_PROT 0
    #endif

    /* если не определены конкретные задержки, то беруться общие */
    #ifndef LPC_SRAM3_T_WEN_CYCLES
        #define LPC_SRAM3_T_WEN_CYCLES LPC_SRAM_T_WEN_CYCLES
    #endif
    #ifndef LPC_SRAM3_T_OEN_CYCLES
        #define LPC_SRAM3_T_OEN_CYCLES LPC_SRAM_T_OEN_CYCLES
    #endif
    #ifndef LPC_SRAM3_T_RD_CYCLES
        #define LPC_SRAM3_T_RD_CYCLES LPC_SRAM_T_RD_CYCLES
    #endif
    #ifndef LPC_SRAM3_T_PAGE_CYCLES
        #define LPC_SRAM3_T_PAGE_CYCLES LPC_SRAM_T_PAGE_CYCLES
    #endif
    #ifndef LPC_SRAM3_T_WR_CYCLES
        #define LPC_SRAM3_T_WR_CYCLES LPC_SRAM_T_WR_CYCLES
    #endif
    #ifndef LPC_SRAM3_T_TURN_CYCLES
        #define LPC_SRAM3_T_TURN_CYCLES LPC_SRAM_T_TURN_CYCLES
    #endif
#else
    #undef  LPC_SRAM3_WIDTH
    #define LPC_SRAM3_WIDTH 0
#endif


#define LPC_SRAM_CONFIGURE(num) do { \
        LPC_EMC->STATICCONFIG##num      = (EMC_SRAM##num##_WIDTH_CFG | \
                                            ((LPC_SRAM##num##_PAGE_MODE&1) << 3) | \
                                            ((LPC_SRAM##num##_CS_POL&1) << 6) | \
                                            ((LPC_SRAM##num##_BLS_RW&1) << 7) | \
                                            ((LPC_SRAM##num##_EXT_WAIT&1) << 8) | \
                                            ((LPC_SRAM##num##_BUF_EN&1) << 19) | \
                                            ((LPC_SRAM##num##_WR_PROT&1) << 20)); \
        LPC_EMC->STATICWAITWEN##num     = f_conv_time(emmc_clk, 0, LPC_SRAM##num##_T_WEN_CYCLES, 1); \
        LPC_EMC->STATICWAITOEN##num     = f_conv_time(emmc_clk, 0, LPC_SRAM##num##_T_OEN_CYCLES, 0); \
        LPC_EMC->STATICWAITRD##num      = f_conv_time(emmc_clk, 0, LPC_SRAM##num##_T_RD_CYCLES, 1); \
        LPC_EMC->STATICWAITPAG##num     = f_conv_time(emmc_clk, 0, LPC_SRAM##num##_T_PAGE_CYCLES, 1); \
        LPC_EMC->STATICWAITWR##num      = f_conv_time(emmc_clk, 0, LPC_SRAM##num##_T_WR_CYCLES, 2); \
        LPC_EMC->STATICWAITTURN##num    = f_conv_time(emmc_clk, 0, LPC_SRAM##num##_T_TURN_CYCLES, 1); \
    } while(0);


#define LPC_SRAM_WIDTH  (MAX(MAX(LPC_SRAM0_WIDTH, LPC_SRAM1_WIDTH), \
                            MAX(LPC_SRAM2_WIDTH, LPC_SRAM3_WIDTH)))


#ifndef LPC_SDRAM0_EN
    #define LPC_SDRAM0_EN 0
#endif
#ifndef LPC_SDRAM1_EN
    #define LPC_SDRAM1_EN 0
#else
    #error "SDRAM CS1 currently is not supported!"
#endif
#ifndef LPC_SDRAM2_EN
    #define LPC_SDRAM2_EN 0
#else
    #error "SDRAM CS2 currently is not supported!"
#endif
#ifndef LPC_SDRAM3_EN
    #define LPC_SDRAM3_EN 0
#else
    #error "SDRAM CS3 currently is not supported!"
#endif

#if !LPC_SDRAM0_EN
    #undef  LPC_SDRAM0_DATA_BUS_WIDTH
    #define LPC_SDRAM0_DATA_BUS_WIDTH 0
#endif
#if !LPC_SDRAM1_EN
    #undef  LPC_SDRAM1_DATA_BUS_WIDTH
    #define LPC_SDRAM1_DATA_BUS_WIDTH 0
#endif
#if !LPC_SDRAM2_EN
    #undef  LPC_SDRAM2_DATA_BUS_WIDTH
    #define LPC_SDRAM2_DATA_BUS_WIDTH 0
#endif
#if !LPC_SDRAM3_EN
    #undef  LPC_SDRAM3_DATA_BUS_WIDTH
    #define LPC_SDRAM3_DATA_BUS_WIDTH 0
#endif

/* SDRAM разрешена, если разрешен хотя бы один банк */
#define LPC_SDRAM_EN (LPC_SDRAM0_EN || LPC_SDRAM1_EN || LPC_SDRAM2_EN || LPC_SDRAM3_EN)


#define LPC_SDRAM_DATA_BUS_WIDTH MAX(MAX(LPC_SDRAM0_DATA_BUS_WIDTH, LPC_SDRAM1_DATA_BUS_WIDTH), \
                                    MAX(LPC_SDRAM2_DATA_BUS_WIDTH, LPC_SDRAM3_DATA_BUS_WIDTH))





#ifndef LPC_SDRAM_T_RP_CYCLES
    #define LPC_SDRAM_T_RP_CYCLES       0
#endif
#ifndef LPC_SDRAM_T_RAS_CYCLES
    #define LPC_SDRAM_T_RAS_CYCLES      0
#endif
#ifndef LPC_SDRAM_T_SREX_CYCLES
    #define LPC_SDRAM_T_SREX_CYCLES     0
#endif
#ifndef LPC_SDRAM_T_APR_CYCLES
    #define LPC_SDRAM_T_APR_CYCLES      0
#endif
#ifndef LPC_SDRAM_T_DAL_CYCLES
    #define LPC_SDRAM_T_DAL_CYCLES      0
#endif
#ifndef LPC_SDRAM_T_WR_CYCLES
    #define LPC_SDRAM_T_WR_CYCLES       0
#endif
#ifndef LPC_SDRAM_T_RC_CYCLES
    #define LPC_SDRAM_T_RC_CYCLES       0
#endif
#ifndef LPC_SDRAM_T_RFC_CYCLES
    #define LPC_SDRAM_T_RFC_CYCLES      0
#endif
#ifndef LPC_SDRAM_T_XSR_CYCLES
    #define LPC_SDRAM_T_XSR_CYCLES      0
#endif
#ifndef LPC_SDRAM_T_RRD_CYCLES
    #define LPC_SDRAM_T_RRD_CYCLES      0
#endif
#ifndef LPC_SDRAM_T_MRD_CYCLES
    #define LPC_SDRAM_T_MRD_CYCLES      0
#endif




#define LPC_EMC_MAX_BUS_WIDTH MAX(LPC_SRAM_WIDTH, LPC_SDRAM_DATA_BUS_WIDTH)


static uint32_t f_conv_time(uint32_t emmc_clk, uint32_t input_ns, uint32_t input_clk, uint32_t adjust) {
    uint32_t temp;

    temp = emmc_clk / 1000000;		/* MHz calculation */
    temp = temp * input_ns / 1000;

    /* round up */
    temp += 0xFF;

    /* convert to simple integer number format */
    temp >>= 8;

    if (temp < input_clk)
        temp = input_clk;

    return (temp > adjust) ? temp - adjust : 0;
}

static void f_emc_init(void) {

#if (LPC_EMC_MAX_BUS_WIDTH >= 8)
    CHIP_PIN_CONFIG(CHIP_PIN_P1_7_EMC_D0,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_8_EMC_D1,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_9_EMC_D2,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_10_EMC_D3,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_11_EMC_D4,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_12_EMC_D5,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_13_EMC_D6,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_14_EMC_D7,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif

#if (LPC_EMC_MAX_BUS_WIDTH >= 16)
    CHIP_PIN_CONFIG(CHIP_PIN_P5_4_EMC_D8,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P5_5_EMC_D9,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P5_6_EMC_D10,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P5_7_EMC_D11,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P5_0_EMC_D12,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P5_1_EMC_D13,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P5_2_EMC_D14,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P5_3_EMC_D15,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif

#if (LPC_EMC_MAX_BUS_WIDTH >= 32)
    CHIP_PIN_CONFIG(CHIP_PIN_PD_2_EMC_D16,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_3_EMC_D17,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_4_EMC_D18,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_5_EMC_D19,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_6_EMC_D20,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_7_EMC_D21,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_8_EMC_D22,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_9_EMC_D23,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_5_EMC_D24,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_6_EMC_D25,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_7_EMC_D26,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_8_EMC_D27,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_9_EMC_D28,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_10_EMC_D29,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_11_EMC_D30,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_12_EMC_D31,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif


#if (LPC_EMC_ADDR_WIDTH > 0)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_9_EMC_A0,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 1)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_10_EMC_A1,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 2)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_11_EMC_A2,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 3)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_12_EMC_A3,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 4)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_13_EMC_A4,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 5)
    CHIP_PIN_CONFIG(CHIP_PIN_P1_0_EMC_A5,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 6)
    CHIP_PIN_CONFIG(CHIP_PIN_P1_1_EMC_A6,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 7)
    CHIP_PIN_CONFIG(CHIP_PIN_P1_2_EMC_A7,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 8)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_8_EMC_A8,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 9)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_7_EMC_A9,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 10)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_6_EMC_A10,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 11)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_2_EMC_A11,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 12)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_1_EMC_A12,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 13)
    CHIP_PIN_CONFIG(CHIP_PIN_P2_0_EMC_A13,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 14)
    CHIP_PIN_CONFIG(CHIP_PIN_P6_8_EMC_A14,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 15)
    CHIP_PIN_CONFIG(CHIP_PIN_P6_7_EMC_A15,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 16)
    CHIP_PIN_CONFIG(CHIP_PIN_PD_16_EMC_A16,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 17)
    CHIP_PIN_CONFIG(CHIP_PIN_PD_15_EMC_A17,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 18)
    CHIP_PIN_CONFIG(CHIP_PIN_PE_0_EMC_A18,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 19)
    CHIP_PIN_CONFIG(CHIP_PIN_PE_1_EMC_A19,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 20)
    CHIP_PIN_CONFIG(CHIP_PIN_PE_2_EMC_A20,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 21)
    CHIP_PIN_CONFIG(CHIP_PIN_PE_3_EMC_A21,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 22)
    CHIP_PIN_CONFIG(CHIP_PIN_PE_4_EMC_A22,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if (LPC_EMC_ADDR_WIDTH > 23)
    CHIP_PIN_CONFIG(CHIP_PIN_PA_4_EMC_A23,  1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif


    CHIP_PIN_CONFIG(CHIP_PIN_P1_3_EMC_OE,         1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P1_6_EMC_WE,         1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);

    CHIP_PIN_CLKCONFIG(CHIP_PIN_CLK0_EMC_CLK0,    1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CLKCONFIG(CHIP_PIN_CLK1_EMC_CLK1,    1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CLKCONFIG(CHIP_PIN_CLK2_EMC_CLK3,    1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CLKCONFIG(CHIP_PIN_CLK3_EMC_CLK2,    1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);


#if 1// LPC_SDRAM_EN
    /* сигналы SDRAM */

    CHIP_PIN_CONFIG(CHIP_PIN_P6_4_EMC_CAS,        1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P6_5_EMC_RAS,        1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);


    CHIP_PIN_CONFIG(CHIP_PIN_P6_12_EMC_DQMOUT0,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P6_10_EMC_DQMOUT1,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);

#if LPC_SDRAM_DATA_BUS_WIDTH >= 32
    CHIP_PIN_CONFIG(CHIP_PIN_PD_0_EMC_DQMOUT2,    1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_13_EMC_DQMOUT3,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif

#if LPC_SDRAM0_EN
    CHIP_PIN_CONFIG(CHIP_PIN_P6_9_EMC_DYCS0,      1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P6_11_EMC_CKEOUT0,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif

#if LPC_SDRAM1_EN
    CHIP_PIN_CONFIG(CHIP_PIN_P6_1_EMC_DYCS1,      1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_P6_2_EMC_CKEOUT1,    1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif

#if LPC_SDRAM2_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PD_14_EMC_DYCS2,     1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_1_EMC_CKEOUT2,    1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif

#if LPC_SDRAM3_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PE_14_EMC_DYCS3,     1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PE_15_EMC_CKEOUT3,   1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#endif


    /* сигналы SRAM */
#if LPC_SRAM0_EN
    CHIP_PIN_CONFIG(CHIP_PIN_P1_5_EMC_CS0,        1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if LPC_SRAM1_EN
    CHIP_PIN_CONFIG(CHIP_PIN_P6_3_EMC_CS1,        1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if LPC_SRAM2_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PD_12_EMC_CS2,       1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if LPC_SRAM3_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PD_11_EMC_CS3,       1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif


#if LPC_SRAM_WIDTH >= 8
    CHIP_PIN_CONFIG(CHIP_PIN_P1_4_EMC_BLS0,       1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif
#if LPC_SRAM_WIDTH >= 16
    CHIP_PIN_CONFIG(CHIP_PIN_P6_6_EMC_BLS1,       1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif

#if LPC_SRAM_WIDTH >= 32
    CHIP_PIN_CONFIG(CHIP_PIN_PD_13_EMC_BLS2,      1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
    CHIP_PIN_CONFIG(CHIP_PIN_PD_10_EMC_BLS3,      1, CHIP_PIN_MODE_NOFILTER | CHIP_PIN_MODE_NOPULL | CHIP_PIN_MODE_FAST);
#endif



    uint32_t emmc_clk = LPC_EMCCLK;

    LPC_EMC->CONTROL = EMC_CONTROL_ENABLE;

#if LPC_SDRAM_EN

#if LPC_SDRAM0_EN
    LPC_EMC->DYNAMICRASCAS0 = (LPC_SDRAM0_LATENCY_CAS << 8) | LPC_SDRAM0_LATENCY_RAS;


    uint32_t cfg = EMC_DYN_CONFIG_LPSDRAM;
    uint32_t burst;

#if (LPC_SDRAM0_SIZE == 16)
    cfg |= EMC_DYN_CONFIG_DEV_SIZE_16Mb;
#elif (LPC_SDRAM0_SIZE == 64)
    cfg |= EMC_DYN_CONFIG_DEV_SIZE_64Mb;
#elif (LPC_SDRAM0_SIZE == 128)
    cfg |= EMC_DYN_CONFIG_DEV_SIZE_128Mb;
#elif (LPC_SDRAM0_SIZE == 256)
    cfg |= EMC_DYN_CONFIG_DEV_SIZE_256Mb;
#elif (LPC_SDRAM0_SIZE == 512)
    cfg |= EMC_DYN_CONFIG_DEV_SIZE_512Mb;
#else
    #error "Invalid SDRAM size specified"
#endif


#if (LPC_SDRAM0_DATA_BUS_WIDTH == 16)
    cfg |= EMC_DYN_CONFIG_DATA_BUS_16;
    burst = EMC_DYN_MODE_BURST_LEN_8;
#elif (LPC_SDRAM0_DATA_BUS_WIDTH == 32)
    cfg |= EMC_DYN_CONFIG_DATA_BUS_32;
    burst = EMC_DYN_MODE_BURST_LEN_4;
#else
    #error "Invalid SDRAM databus width specified"
#endif

#if (LPC_SDRAM0_DEV_BUS_WIDTH == 8)
    cfg |= EMC_DYN_CONFIG_DEV_BUS_8;
#elif (LPC_SDRAM0_DEV_BUS_WIDTH == 16)
    cfg |= EMC_DYN_CONFIG_DEV_BUS_16;
#elif (LPC_SDRAM0_DEV_BUS_WIDTH == 32)
    cfg |= EMC_DYN_CONFIG_DEV_BUS_32;
#else
    #error "Invalid SDRAM device bus width specified"
#endif

    LPC_EMC->DYNAMICCONFIG0 = cfg;
#endif



    LPC_EMC->DYNAMICREADCONFIG = LPC_SDRAM_READ_DELAY;

    LPC_EMC->DYNAMICRP     = f_conv_time(emmc_clk, LPC_SDRAM_T_RP,      LPC_SDRAM_T_RP_CYCLES,   1);
    LPC_EMC->DYNAMICRAS    = f_conv_time(emmc_clk, LPC_SDRAM_T_RAS,     LPC_SDRAM_T_RAS_CYCLES,  1);
    LPC_EMC->DYNAMICSREX   = f_conv_time(emmc_clk, LPC_SDRAM_T_SREX,    LPC_SDRAM_T_SREX_CYCLES, 1);
    LPC_EMC->DYNAMICAPR    = f_conv_time(emmc_clk, LPC_SDRAM_T_APR,     LPC_SDRAM_T_APR_CYCLES,  1);
    LPC_EMC->DYNAMICDAL    = f_conv_time(emmc_clk, LPC_SDRAM_T_DAL,     LPC_SDRAM_T_DAL_CYCLES,  0);
    LPC_EMC->DYNAMICWR     = f_conv_time(emmc_clk, LPC_SDRAM_T_WR,      LPC_SDRAM_T_WR_CYCLES,   1);
    LPC_EMC->DYNAMICRC     = f_conv_time(emmc_clk, LPC_SDRAM_T_RC,      LPC_SDRAM_T_RC_CYCLES,   1);
    LPC_EMC->DYNAMICRFC    = f_conv_time(emmc_clk, LPC_SDRAM_T_RFC,     LPC_SDRAM_T_RFC_CYCLES,  1);
    LPC_EMC->DYNAMICXSR    = f_conv_time(emmc_clk, LPC_SDRAM_T_XSR,     LPC_SDRAM_T_XSR_CYCLES,  1);
    LPC_EMC->DYNAMICRRD    = f_conv_time(emmc_clk, LPC_SDRAM_T_RRD,     LPC_SDRAM_T_RRD_CYCLES,  1);
    LPC_EMC->DYNAMICMRD    = f_conv_time(emmc_clk, LPC_SDRAM_T_MRD,     LPC_SDRAM_T_MRD_CYCLES,  1);




    chip_delay_clk(100 * LPC_SYSCLK/1000000);

    LPC_EMC->DYNAMICCONTROL    = 0x00000183;  /* Issue NOP command */

    chip_delay_clk(200 * LPC_SYSCLK/1000000);

    LPC_EMC->DYNAMICCONTROL    = 0x00000103;  /* Issue PALL command */

    LPC_EMC->DYNAMICREFRESH = 2;              /* ( 2 * 16 ) -> 32 clock cycles */

    chip_delay_clk(200 * LPC_SYSCLK/1000000);


    uint32_t tmpclk = (f_conv_time(emmc_clk, LPC_SDRAM_T_REFRESH, 0, 0) + 15)/16;
    LPC_EMC->DYNAMICREFRESH    = tmpclk;

    LPC_EMC->DYNAMICCONTROL    = 0x00000083;  /* Issue MODE command */






#if LPC_SDRAM0_EN
    uint32_t mode_reg = burst | (LPC_SDRAM0_LATENCY_CAS << EMC_DYN_MODE_CAS_BIT);

    /** @todo вычисление значений на основе параметров. в RBC режиме еще и кол-во банков участвует */
    unsigned offs = LPC_SDRAM0_COL_CNT;
#if (LPC_SDRAM0_DATA_BUS_WIDTH == 16)
    offs+=1;
#else
    offs+=2;
#endif

    unsigned addr = EMC_ADDRESS_DYCS0 + (mode_reg << offs);

    volatile uint32_t tmp __attribute__((unused));
    tmp = *((volatile uint32_t*)addr);
#endif


    LPC_EMC->DYNAMICCONTROL = 0;

    /* enable buffers */
#if LPC_SDRAM0_EN
    LPC_EMC->DYNAMICCONFIG0    |= EMC_CONFIG_BUFFER_ENABLE;
#endif
#if LPC_SDRAM1_EN
    LPC_EMC->DYNAMICCONFIG1    |= EMC_CONFIG_BUFFER_ENABLE;
#endif
#if LPC_SDRAM2_EN
    LPC_EMC->DYNAMICCONFIG2    |= EMC_CONFIG_BUFFER_ENABLE;
#endif
#if LPC_SDRAM3_EN
    LPC_EMC->DYNAMICCONFIG3    |= EMC_CONFIG_BUFFER_ENABLE;
#endif

#endif




#if LPC_SRAM0_EN
    LPC_SRAM_CONFIGURE(0);
#endif
#if LPC_SRAM1_EN
    LPC_SRAM_CONFIGURE(1);
#endif
#if LPC_SRAM2_EN
    LPC_SRAM_CONFIGURE(2);
#endif
#if LPC_SRAM3_EN
    LPC_SRAM_CONFIGURE(3);
#endif
}

#endif





/*------------------------------------------------------------------------------------------------*/
void chip_init(void) {
    /* если разрешен Watchdog, то в первую очередь настраиваем его */
    LPC_CCU_CFG(CLK_MX_WWDT,        LPC_CLKEN_WDT);
#if LPC_INIT_WDT
    LPC_WWDT->TC = LPC_WDT_TIMEOUT_VAL - 1;
    LPC_WWDT->MOD |=  WWDT_WDMOD_WDRESET | WWDT_WDMOD_WDINT | WWDT_WDMOD_WDEN;

    LPC_WWDT->WINDOW = LPC_WDT_WINDOW_VAL - 1;
    LPC_WWDT->WARNINT = LPC_WDT_WARNTIME_VAL - 1;

    LPC_WWDT->FEED = 0xAA;
    LPC_WWDT->FEED = 0x55;
#endif



    /* Сначала переключиться на IRC */
    LPC_CGU->BASE_CLK[CLK_BASE_MX] = LPC_BASECFG(LPC_CLKSEL_IRC, 0);

#if LPC_CLKF_OSC > 0
    /* Включение кварца */
#define XTAL_OSC_CTRL_VAL   BF_SET(LPC_CGU_XTAL_OSC_CTRL_HF, (LPC_CLKF_OSC > MHZ(15)))
    if (LPC_CGU->XTAL_OSC_CTRL != XTAL_OSC_CTRL_VAL)
        {
        LPC_CGU->XTAL_OSC_CTRL |= LPC_CGU_XTAL_OSC_CTRL_PD;
        LPC_CGU->XTAL_OSC_CTRL = LPC_CGU_XTAL_OSC_CTRL_PD | XTAL_OSC_CTRL_VAL;
        LPC_CGU->XTAL_OSC_CTRL &= ~LPC_CGU_XTAL_OSC_CTRL_PD;
        chip_delay_clk(LPC_CLKF_IRC / 1000000 * 250);
        }
#else
    LPC_CGU->XTAL_OSC_CTRL |= LPC_CGU_XTAL_OSC_CTRL_PD;
#endif

#if LPC_CLKF_GPCLKIN > 0
    /* Включение ножки GPCLKIN */
    CHIP_PIN_CONFIG(LPC_GPCLKIN_PIN_ID, 1,
        CHIP_PIN_MODE_NOPULL | ((LPC_CLKF_GPCLKIN > MHZ(30)) ? CHIP_PIN_MODE_NOFILTER : 0));
#endif

    /* Установка делителей IDIV */
    LPC_CGU->IDIV_CTRL[CLK_IDIV_A] = LPC_CLKDIVA_CFG;
    LPC_CGU->IDIV_CTRL[CLK_IDIV_B] = LPC_CLKDIVB_CFG;
    LPC_CGU->IDIV_CTRL[CLK_IDIV_C] = LPC_CLKDIVC_CFG;
    LPC_CGU->IDIV_CTRL[CLK_IDIV_D] = LPC_CLKDIVD_CFG;
    LPC_CGU->IDIV_CTRL[CLK_IDIV_E] = LPC_CLKDIVE_CFG;

#if LPC_USE_PLL1
    /* Инициализация PLL1 */

#define PLL1_CTRL_VAL(m, n, p)  ( \
    BF_SET(LPC_CGU_PLL1_CTRL_PD, 0) | \
    BF_SET(LPC_CGU_PLL1_CTRL_BYPASS, 0) | \
    BF_SET(LPC_CGU_PLL1_CTRL_FBSEL, 0) | \
    BF_SET(LPC_CGU_PLL1_CTRL_DIRECT, 1 == (p)) | \
    BF_SET(LPC_CGU_PLL1_CTRL_PSEL, (p) / 4 - (p) / 16) | \
    BF_SET(LPC_CGU_PLL1_CTRL_AUTOBLOCK, LPC_PLL1_AUTOBLK) | \
    BF_SET(LPC_CGU_PLL1_CTRL_NSEL, (n) - 1) | \
    BF_SET(LPC_CGU_PLL1_CTRL_MSEL, (m) - 1) | \
    BF_SET(LPC_CGU_PLL1_CTRL_CLK_SEL, LPC_PLL1_SRC) \
    )

#if (LPC_CLKCFG_SRC(LPC_BASE_M4_CFG) == LPC_CLKSEL_PLL1) && (LPC_SYSCLK > MHZ(110))
    /* Включение PLL в 2 стадии: сначала с половинной частотой, потом с полной */
    LPC_CGU->PLL1_CTRL = PLL1_CTRL_VAL(LPC_PLL1_M, LPC_PLL1_N, LPC_PLL1_P * 2);
    while (!(LPC_CGU->PLL1_STAT & LPC_CGU_PLL1_STAT_LOCK));
    /* Включить тактирование CPU от половинной частоты */
    LPC_CGU->BASE_CLK[CLK_BASE_MX] = LPC_BASE_M4_CFG;
    chip_delay_clk(LPC_SYSCLK / 2 / 1000000 * 50);
    LPC_CGU->PLL1_CTRL = PLL1_CTRL_VAL(LPC_PLL1_M, LPC_PLL1_N, LPC_PLL1_P);
#else
    LPC_CGU->PLL1_CTRL = PLL1_CTRL_VAL(LPC_PLL1_M, LPC_PLL1_N, LPC_PLL1_P);
    while (!(LPC_CGU->PLL1_STAT & LPC_CGU_PLL1_STAT_LOCK));
#endif

#else
    LPC_CGU->PLL1_CTRL = LPC_CGU_PLL1_CTRL_PD;
#endif /* LPC_USE_PLL1 */

#if LPC_USE_PLL0USB
    /* Инициализация PLL0USB на фиксированную частоту 480 МГц */

    #if LPC_CLKSRCF_PLL0USB == MHZ(2)       /* M = 480/2/Fin = 120 SELP=31 SELI=28 */
    #define USB_MDIV_VAL    0x073E2DAD                                            
    #elif LPC_CLKSRCF_PLL0USB == MHZ(3)     /* M = 480/2/Fin = 80  SELP=31 SELI=44 */
    #define USB_MDIV_VAL    0x0B3E34B1                                               
    #elif LPC_CLKSRCF_PLL0USB == MHZ(4)     /* M = 480/2/Fin = 60  SELP=31 SELI=56 */
    #define USB_MDIV_VAL    0x0E3E7777                                           
    #elif LPC_CLKSRCF_PLL0USB == MHZ(5)     /* M = 480/2/Fin = 48  SELP=25 SELI=52 */
    #define USB_MDIV_VAL    0x0D326667                                               
    #elif LPC_CLKSRCF_PLL0USB == MHZ(6)     /* M = 480/2/Fin = 40  SELP=21 SELI=44 */
    #define USB_MDIV_VAL    0x0B2A2A66                                               
    #elif LPC_CLKSRCF_PLL0USB == MHZ(8)     /* M = 480/2/Fin = 30  SELP=16 SELI=32 */
    #define USB_MDIV_VAL    0x08206AAA                                               
    #elif LPC_CLKSRCF_PLL0USB == MHZ(10)    /* M = 480/2/Fin = 24  SELP=13 SELI=28 */
    #define USB_MDIV_VAL    0x071A7FAA                                             
    #elif LPC_CLKSRCF_PLL0USB == MHZ(12)    /* M = 480/2/Fin = 20  SELP=11 SELI=24 */
    #define USB_MDIV_VAL    0x06167FFA                                               
    #elif LPC_CLKSRCF_PLL0USB == MHZ(15)    /* M = 480/2/Fin = 16  SELP=9 SELI=20  */
    #define USB_MDIV_VAL    0x05123FFF                                               
    #elif LPC_CLKSRCF_PLL0USB == MHZ(16)    /* M = 480/2/Fin = 15  SELP=8 SELI=16  */
    #define USB_MDIV_VAL    0x04101FFF                                               
    #elif LPC_CLKSRCF_PLL0USB == MHZ(20)    /* M = 480/2/Fin = 12  SELP=7 SELI=16  */
    #define USB_MDIV_VAL    0x040E03FF                                             
    #elif LPC_CLKSRCF_PLL0USB == MHZ(24)    /* M = 480/2/Fin = 10  SELP=6 SELI=12  */
    #define USB_MDIV_VAL    0x030C00FF
    #else
    #error "Unsupported PLL0USB source clock frequency"
    #endif

    #define USB_NPDIV_VAL   0x00302062      /* N = 1, P = 1 */

    LPC_CGU->PLL[CGU_USB_PLL].PLL_CTRL = 
        BF_SET(LPC_CGU_PLL0_CTRL_PD, 1) |
        BF_SET(LPC_CGU_PLL0_CTRL_BYPASS, 0) |
        BF_SET(LPC_CGU_PLL0_CTRL_DIRECTI, 1) |
        BF_SET(LPC_CGU_PLL0_CTRL_DIRECTO, 1) |
        BF_SET(LPC_CGU_PLL0_CTRL_CLKEN, 0) |
        BF_SET(LPC_CGU_PLL0_CTRL_FRM, 0) |
        BF_SET(LPC_CGU_PLL0_CTRL_AUTOBLOCK, LPC_PLL0USB_AUTOBLK) |
        BF_SET(LPC_CGU_PLL0_CTRL_CLK_SEL, LPC_PLL0USB_SRC);
    LPC_CGU->PLL[CGU_USB_PLL].PLL_MDIV = USB_MDIV_VAL;
    LPC_CGU->PLL[CGU_USB_PLL].PLL_NP_DIV = USB_NPDIV_VAL;
    LPC_CGU->PLL[CGU_USB_PLL].PLL_CTRL &= ~LPC_CGU_PLL0_CTRL_PD;
    while (!(LPC_CGU->PLL[CGU_USB_PLL].PLL_STAT & LPC_CGU_PLL0_STAT_LOCK));
    LPC_CGU->PLL[CGU_USB_PLL].PLL_CTRL |= LPC_CGU_PLL0_CTRL_CLKEN;

#else
    LPC_CGU->PLL[CGU_USB_PLL].PLL_CTRL = LPC_CGU_PLL0_CTRL_PD;
#endif /* LPC_USE_PLL0USB */

    /* Включиение тактирования CPU от выбранного источника */
    LPC_CGU->BASE_CLK[CLK_BASE_MX] =        LPC_BASE_M4_CFG;

    /* Включение тактирования периферии от выбранных источников */
#define BASE_APB1_EN    (LPC_CLKEN_MCPWM || LPC_CLKEN_I2C0 || LPC_CLKEN_I2S || LPC_CLKEN_CAN1)
#define BASE_APB3_EN    (LPC_CLKEN_I2C1 || LPC_CLKEN_DAC || LPC_CLKEN_ADC0 || LPC_CLKEN_ADC1 || LPC_CLKEN_CAN0)

#if LPC_CLKEN_CAN0 && (LPC_CLKEN_I2C1 || LPC_CLKEN_DAC || LPC_CLKEN_ADC0 || LPC_CLKEN_ADC1)
#error "Cannot use CAN0 with other APB3 peripherals (see LPC43xx errata)"
#endif
#if LPC_CLKEN_CAN1 && (LPC_CLKEN_MCPWM || LPC_CLKEN_I2C0 || LPC_CLKEN_I2S)
#error "Cannot use CAN1 with other APB1 peripherals (see LPC43xx errata)"
#endif

/* включение (0) или выключение (1) базового генератора */
#define BASE_PD(clken)  (!((clken) || LPC_CLK_BASES_ALWAYS_ON))

    LPC_CGU->BASE_CLK[CLK_BASE_USB0] =      LPC_BASE_USB0_CFG   | BASE_PD(LPC_CLKEN_USB0);
    LPC_CGU->BASE_CLK[CLK_BASE_PERIPH] =    LPC_BASE_PERIPH_CFG | BASE_PD(LPC_CLKEN_PERIPH);
    LPC_CGU->BASE_CLK[CLK_BASE_USB1] =      LPC_BASE_USB1_CFG   | BASE_PD(LPC_CLKEN_USB1);
    LPC_CGU->BASE_CLK[CLK_BASE_SPIFI] =     LPC_BASE_SPIFI_CFG  | BASE_PD(LPC_CLKEN_SPIFI);
    LPC_CGU->BASE_CLK[CLK_BASE_SPI] =       LPC_BASE_SPI_CFG    | BASE_PD(LPC_CLKEN_SPI);
    LPC_CGU->BASE_CLK[CLK_BASE_PHY_RX] =    LPC_BASE_PHY_RX_CFG | BASE_PD(LPC_CLKEN_ENET);
    LPC_CGU->BASE_CLK[CLK_BASE_PHY_TX] =    LPC_BASE_PHY_TX_CFG | BASE_PD(LPC_CLKEN_ENET);
    LPC_CGU->BASE_CLK[CLK_BASE_APB1] =      LPC_BASE_APB1_CFG   | BASE_PD(BASE_APB1_EN);
    LPC_CGU->BASE_CLK[CLK_BASE_APB3] =      LPC_BASE_APB3_CFG   | BASE_PD(BASE_APB3_EN);
    LPC_CGU->BASE_CLK[CLK_BASE_LCD] =       LPC_BASE_LCD_CFG    | BASE_PD(LPC_CLKEN_LCD);
    LPC_CGU->BASE_CLK[CLK_BASE_ADCHS] =      LPC_BASE_ADCHS_CFG  | BASE_PD(LPC_CLKEN_ADCHS);
    LPC_CGU->BASE_CLK[CLK_BASE_SDIO] =      LPC_BASE_SDIO_CFG   | BASE_PD(LPC_CLKEN_SDIO);
    LPC_CGU->BASE_CLK[CLK_BASE_SSP0] =      LPC_BASE_SSP0_CFG   | BASE_PD(LPC_CLKEN_SSP0);
    LPC_CGU->BASE_CLK[CLK_BASE_SSP1] =      LPC_BASE_SSP1_CFG   | BASE_PD(LPC_CLKEN_SSP1);
    LPC_CGU->BASE_CLK[CLK_BASE_UART0] =     LPC_BASE_UART0_CFG  | BASE_PD(LPC_CLKEN_UART0);
    LPC_CGU->BASE_CLK[CLK_BASE_UART1] =     LPC_BASE_UART1_CFG  | BASE_PD(LPC_CLKEN_UART1);
    LPC_CGU->BASE_CLK[CLK_BASE_UART2] =     LPC_BASE_UART2_CFG  | BASE_PD(LPC_CLKEN_UART2);
    LPC_CGU->BASE_CLK[CLK_BASE_UART3] =     LPC_BASE_UART3_CFG  | BASE_PD(LPC_CLKEN_UART3);
    LPC_CGU->BASE_CLK[CLK_BASE_OUT] =       LPC_BASE_OUT_CFG    | BASE_PD(LPC_CLKEN_OUT);
    LPC_CGU->BASE_CLK[CLK_BASE_APLL] =      LPC_BASE_AUDIO_CFG  | BASE_PD(LPC_CLKEN_I2S);
    LPC_CGU->BASE_CLK[CLK_BASE_CGU_OUT0] =  LPC_BASE_CGOUT0_CFG | BASE_PD(LPC_CLKEN_CGOUT0);
    LPC_CGU->BASE_CLK[CLK_BASE_CGU_OUT1] =  LPC_BASE_CGOUT1_CFG | BASE_PD(LPC_CLKEN_CGOUT1);


#ifdef LPC_CLK_DELAY_EMC
    LPC_SCU->EMCDELAYCLK = LPC_CLK_DELAY_EMC;
#endif






    //LPC_CCU_CFG(CLK_MX_BUS, 1);
    LPC_CCU_CFG(CLK_MX_SPIFI,       LPC_CLKEN_SPIFI);
    LPC_CCU_CFG(CLK_MX_GPIO, 1);
    LPC_CCU_CFG(CLK_MX_LCD,         LPC_CLKEN_LCD);
    LPC_CCU_CFG(CLK_MX_ETHERNET,    LPC_CLKEN_ENET);
    LPC_CCU_CFG(CLK_MX_USB0,        LPC_CLKEN_USB0);
    LPC_CCU_CFG(CLK_MX_EMC,         LPC_CLKEN_EMC);
    LPC_CCU_CFG(CLK_MX_SDIO,        LPC_CLKEN_SDIO);
    LPC_CCU_CFG(CLK_MX_DMA,         LPC_CLKEN_DMA);
    //LPC_CCU_CFG(CLK_MX_MXCORE, 1);
    LPC_CCU_CFG(CLK_MX_SCT,         LPC_CLKEN_SCT);
    LPC_CCU_CFG(CLK_MX_USB1,        LPC_CLKEN_USB1);
    LPC_CCU_CFG(CLK_MX_EMC_DIV,     LPC_CLKEN_EMC);
    //LPC_CCU_CFG(CLK_MX_FLASHA, 1);
    //LPC_CCU_CFG(CLK_MX_FLASHB, 1);
    LPC_CCU_CFG(CLK_M4_M0APP,       LPC_CLKEN_M0APP);
    LPC_CCU_CFG(CLK_MX_ADCHS,       LPC_CLKEN_ADCHS);
    LPC_CCU_CFG(CLK_MX_EEPROM,      LPC_CLKEN_EEPROM);

    LPC_CCU_CFG(CLK_MX_UART0,       LPC_CLKEN_UART0);
    LPC_CCU_CFG(CLK_MX_UART1,       LPC_CLKEN_UART1);
    LPC_CCU_CFG(CLK_MX_SSP0,        LPC_CLKEN_SSP0);
    LPC_CCU_CFG(CLK_MX_TIMER0,      LPC_CLKEN_TIMER0);
    LPC_CCU_CFG(CLK_MX_TIMER1,      LPC_CLKEN_TIMER1);
    //LPC_CCU_CFG(CLK_MX_SCU, 1);
    //LPC_CCU_CFG(CLK_MX_CREG, 1);
    LPC_CCU_CFG(CLK_MX_RITIMER,     LPC_CLKEN_RITIMER);
    LPC_CCU_CFG(CLK_MX_UART2,       LPC_CLKEN_UART2);
    LPC_CCU_CFG(CLK_MX_UART3,       LPC_CLKEN_UART3);
    LPC_CCU_CFG(CLK_MX_TIMER2,      LPC_CLKEN_TIMER2);
    LPC_CCU_CFG(CLK_MX_TIMER3,      LPC_CLKEN_TIMER3);
    LPC_CCU_CFG(CLK_MX_SSP1,        LPC_CLKEN_SSP1);
    LPC_CCU_CFG(CLK_MX_QEI,         LPC_CLKEN_QEI);

/* If some BASE_CLK is off, accessing related registers hangs LPC ! */
#if LPC_CLK_BASES_ALWAYS_ON || BASE_APB3_EN
    LPC_CCU_CFG(CLK_APB3_BUS,       1);
    LPC_CCU_CFG(CLK_APB3_I2C1,      LPC_CLKEN_I2C1);
    LPC_CCU_CFG(CLK_APB3_DAC,       LPC_CLKEN_DAC);
    LPC_CCU_CFG(CLK_APB3_ADC0,      LPC_CLKEN_ADC0);
    LPC_CCU_CFG(CLK_APB3_ADC1,      LPC_CLKEN_ADC1);
    LPC_CCU_CFG(CLK_APB3_CAN0,      LPC_CLKEN_CAN0);
#endif

#if LPC_CLK_BASES_ALWAYS_ON || BASE_APB1_EN
    LPC_CCU_CFG(CLK_APB1_BUS,       1);
    LPC_CCU_CFG(CLK_APB1_MOTOCON,   LPC_CLKEN_MCPWM);
    LPC_CCU_CFG(CLK_APB1_I2C0,      LPC_CLKEN_I2C0);
    LPC_CCU_CFG(CLK_APB1_I2S,       LPC_CLKEN_I2S);
    LPC_CCU_CFG(CLK_APB1_CAN1,      LPC_CLKEN_CAN1);
#endif

#if LPC_CLK_BASES_ALWAYS_ON
    LPC_CCU_CFG(CLK_SPIFI,          LPC_CLKEN_SPIFI);
    LPC_CCU_CFG(CLK_PERIPH_BUS,     LPC_CLKEN_PERIPH);
    LPC_CCU_CFG(CLK_PERIPH_CORE,    LPC_CLKEN_PERIPH);
    LPC_CCU_CFG(CLK_PERIPH_SGPIO,   LPC_CLKEN_PERIPH);
    LPC_CCU_CFG(CLK_USB0,           LPC_CLKEN_USB0);
    LPC_CCU_CFG(CLK_USB1,           LPC_CLKEN_USB1);
    LPC_CCU_CFG(CLK_SPI,            LPC_CLKEN_SPI);
    LPC_CCU_CFG(CLK_ADCHS,          LPC_CLKEN_ADCHS);
    LPC_CCU_CFG(CLK_APLL,           LPC_CLKEN_I2S);
    LPC_CCU_CFG(CLK_APB2_UART3,     LPC_CLKEN_UART3);
    LPC_CCU_CFG(CLK_APB2_UART2,     LPC_CLKEN_UART2);
    LPC_CCU_CFG(CLK_APB0_UART1,     LPC_CLKEN_UART1);
    LPC_CCU_CFG(CLK_APB0_UART0,     LPC_CLKEN_UART0);
    LPC_CCU_CFG(CLK_APB2_SSP1,      LPC_CLKEN_SSP1);
    LPC_CCU_CFG(CLK_APB0_SSP0,      LPC_CLKEN_SSP0);
    LPC_CCU_CFG(CLK_APB2_SDIO,      LPC_CLKEN_SDIO);
#endif






    /* Настройка таймингов flash */
#define FLASHTIM_VAL    ((LPC_SYSCLK + 21500000UL - 1UL) / 21500000UL - 1)
    LPC_CREG->FLASHCFGA = (LPC_CREG->FLASHCFGA & (~(0xFUL << 12))) | (FLASHTIM_VAL << 12);
    LPC_CREG->FLASHCFGB = (LPC_CREG->FLASHCFGB & (~(0xFUL << 12))) | (FLASHTIM_VAL << 12);


#ifdef LPC_CLK_EMC_DIV2
    LPC_CCU1->CLKCCU[CLK_MX_EMC_DIV].CFG |= BF_SET(LPC_CCU_CFG_DIV, 1);
    LPC_CREG->CREG6 |= (1<<16);
#endif


#if defined LPC_EMC_CONFIG && LPC_EMC_CONFIG
    f_emc_init();
#endif

}

/*------------------------------------------------------------------------------------------------*/


/*================================================================================================*/
