#ifndef REGS_SAI_H
#define REGS_SAI_H

#include "regs_ccm_ana.h"

typedef struct {
    __IO uint32_t           CLKEN0;             /* IP Clock Enable Control Register 0,                   Address offset: 0x0000 */
    __IO uint32_t           CLKEN1;             /* IP Clock Enable Control Register 1,                   Address offset: 0x0004 */
    uint32_t                RESERED1[62];
    __I  uint32_t           ADSP_EXPSTATE;      /* AudioDSP EXPSTATE Register,                           Address offset: 0x0100 */
    __IO uint32_t           ADSP_IMPWIRE;       /* AudioDSP IMPWIRE Register,                            Address offset: 0x0104 */
    __IO uint32_t           ADSP_XOCDMODE;      /* AudioDSP XOCDMODE Register,                           Address offset: 0x0108 */
    __IO uint32_t           ADSP_PID;           /* AudioDSP PID Register,                                Address offset: 0x010C */
    uint32_t                RESERED2[60];
    __IO uint32_t           EARC;               /* EARC Control Register,                                Address offset: 0x0200 */
    uint32_t                RESERED3[63];
    __IO uint32_t           SAI_MCLK_SEL[6];    /* SAIx MCLK SELECT Register,                            Address offset: 0x0300 - 0x0314 */
    __IO uint32_t           PDM_CLK;            /* PDM Root Clock Select Register,                       Address offset: 0x0318 */
    uint32_t                RESERED4[57];
    CHIP_REGS_CCM_PLL_EX_T  SAI_PLL;            /* SAI PLL Regisgers,                                    Address offset: 0x0400 - 0x0410 */
    uint32_t                RESERED5[59];
    __IO uint32_t           AUDIO_EXT_ADDR;     /* AUDIOMIX Extra Addr Bits Register,                    Address offset: 0x0500 */
    __IO uint32_t           IPG_LP_CTRL;        /* IPG Low Power Control Register,                       Address offset: 0x0504 */
    __IO uint32_t           AUDIO_AXI_LIMIT;    /* AUDIOMIX AXI LIMIT CTRL Register,                     Address offset: 0x0508 */
} CHIP_REGS_AUDIO_BLK_CTRL_T;


#define CHIP_REGS_AUDIO_BLK_CTRL ((CHIP_REGS_AUDIO_BLK_CTRL_T *)CHIP_MEMRGN_ADDR_PERIPH_AUDIO_BLK_CTRL)

/*********** IP Clock Enable Control Register 0 (CLKEN0) **********************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_EARC                   (0x00000001UL << 31U)  /* (RW) EARC clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_ADSP_DEBUG             (0x00000001UL << 30U)  /* (RW) AudioDSP DEBUG clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_ADSP                   (0x00000001UL << 29U)  /* (RW) AudioDSP core clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SPBA2                  (0x00000001UL << 28U)  /* (RW) SPBA2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SDMA3                  (0x00000001UL << 27U)  /* (RW) SDMA3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SDMA2                  (0x00000001UL << 26U)  /* (RW) SDMA2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_PDM                    (0x00000001UL << 25U)  /* (RW) PDM clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_ASRC                   (0x00000001UL << 24U)  /* (RW) ASRC clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI7_MCLK3             (0x00000001UL << 23U)  /* (RW) SAI7 mclk3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI7_MCLK2             (0x00000001UL << 22U)  /* (RW) SAI7 mclk2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI7_MCLK1             (0x00000001UL << 21U)  /* (RW) SAI7 mclk1 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI7                   (0x00000001UL << 20U)  /* (RW) SAI7 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI6_MCLK3             (0x00000001UL << 19U)  /* (RW) SAI6 mclk3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI6_MCLK2             (0x00000001UL << 18U)  /* (RW) SAI6 mclk2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI6_MCLK1             (0x00000001UL << 17U)  /* (RW) SAI6 mclk1 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI6                   (0x00000001UL << 16U)  /* (RW) SAI6 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI5_MCLK3             (0x00000001UL << 15U)  /* (RW) SAI5 mclk3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI5_MCLK2             (0x00000001UL << 14U)  /* (RW) SAI5 mclk2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI5_MCLK1             (0x00000001UL << 13U)  /* (RW) SAI5 mclk1 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI5                   (0x00000001UL << 12U)  /* (RW) SAI5 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI3_MCLK3             (0x00000001UL << 11U)  /* (RW) SAI3 mclk3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI3_MCLK2             (0x00000001UL << 10U)  /* (RW) SAI3 mclk2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI3_MCLK1             (0x00000001UL <<  9U)  /* (RW) SAI3 mclk1 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI3                   (0x00000001UL <<  8U)  /* (RW) SAI3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI2_MCLK3             (0x00000001UL <<  7U)  /* (RW) SAI2 mclk3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI2_MCLK2             (0x00000001UL <<  6U)  /* (RW) SAI2 mclk2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI2_MCLK1             (0x00000001UL <<  5U)  /* (RW) SAI2 mclk1 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI2                   (0x00000001UL <<  4U)  /* (RW) SAI2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI1_MCLK3             (0x00000001UL <<  3U)  /* (RW) SAI1 mclk3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI1_MCLK2             (0x00000001UL <<  2U)  /* (RW) SAI1 mclk2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI1_MCLK1             (0x00000001UL <<  1U)  /* (RW) SAI1 mclk1 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN0_SAI1                   (0x00000001UL <<  0U)  /* (RW) SAI1 clock enable */

/*********** IP Clock Enable Control Register 1 (CLKEN1) **********************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN1_EARC_PH                (0x00000001UL <<  6U)  /* (RW) EARC PHY audio ss clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN1_MU3                    (0x00000001UL <<  5U)  /* (RW) MU3 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN1_MU2                    (0x00000001UL <<  4U)  /* (RW) MU2 clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN1_PLL                    (0x00000001UL <<  3U)  /* (RW) PLL clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN1_EDMA                   (0x00000001UL <<  2U)  /* (RW) EDMA clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN1_AUD2HTX                (0x00000001UL <<  1U)  /* (RW) EDMA clock enable */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_CLKEN1_OCRAM_A                (0x00000001UL <<  0U)  /* (RW) OCRAM_A clock enable */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_CLKEN1_RESERVED               (0xFFFFFF80UL)

/*********** AudioDSP XOCDMODE Register (AudioDSP_REG2) **********************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_A53_WAIT_MODE   (0x00000001UL << 14U)  /* (RW) CA53 in Wait Mode for MU2 */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_A53_DSM_MODE    (0x00000001UL << 13U)  /* (RW) CA53 in DSM Mode for MU2 */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_M7_WAIT_MODE    (0x00000001UL << 12U)  /* (RW) M7 in Wait Mode for MU3 */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_M7_DSM_MODE     (0x00000001UL << 11U)  /* (RW) M7 in DSM Mode for MU3 */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_WAIT_MODE       (0x00000001UL << 10U)  /* (RW) AudioDSP in Wait Mode for MU */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_DSM_MODE        (0x00000001UL <<  9U)  /* (RW) AudioDSP in DSM Mode for MU */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_ADDR_MODE       (0x00000001UL <<  8U)  /* (RW) AudioDSP Addr Mode(Alias) Register. Refer to Chip memory map for details. */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_STAT_VECTOR_SEL (0x00000001UL <<  6U)  /* (RW) Selects between one of two stationary vector bases. */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_RUN_STALL       (0x00000001UL <<  5U)  /* (RW) AudioDSP RunStall control bit. */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_OCDHALTONRESET  (0x00000001UL <<  4U)  /* (RW) AudioDSP enters OCDHaltMode if this signal is samped asserted on reset. */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_PWAITMODE       (0x00000001UL <<  1U)  /* (RO) Indicates that the AudioDSP is in sleep mode. */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_ADSP_XOCDMODE_XOCDMODE        (0x00000001UL <<  1U)  /* (RO) Indicates that the AudioDSP is in OCD halt mode. */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_ADSP_XOCDMODE_RESERVED        (0xFFFF808CUL)

/*********** EARC Control Register (EARC) *************************************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_EARC_PHY_RESETB               (0x00000001UL <<  1U)  /* (RW) Earc PHY Software Reset (0 - reset) */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_EARC_RESETB                   (0x00000001UL <<  0U)  /* (RW) Earc Software Reset (0 - reset) */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_EARC_RESERVED                 (0xFFFFFFFCUL)

/*********** SAI MCLK SELECT Register (SAI_MCLK_SEL) *************************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2            (0x0000000FUL <<  1U)  /* (RW) MCLK2 Select */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK1            (0x00000001UL <<  0U)  /* (RW) MCLK1 Select */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_SAI_MCLK_SEL_RESERVED         (0xFFFFFFE0UL)


#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI1_ROOT    0 /* SAI1_CLK_ROOT is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI2_ROOT    1 /* SAI2_CLK_ROOT is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI3_ROOT    2 /* SAI3_CLK_ROOT is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI5_ROOT    4 /* SAI5_CLK_ROOT is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI6_ROOT    5 /* SAI6_CLK_ROOT is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI7_ROOT    6 /* SAI7_CLK_ROOT is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI1_MCLK    7 /* SAI1.MCLK is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI2_MCLK    8 /* SAI2.MCLK is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI3_MCLK    9 /* SAI3.MCLK is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI5_MCLK    11 /* SAI5.MCLK is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI6_MCLK    12 /* SAI6.MCLK is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SAI7_MCLK    13 /* SAI7.MCLK is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK2_SPDIF_EXTCLK 14 /* SPDIF.ETXCLK is selected */

#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK1_CLK_ROOT     0 /* SAIx_CLK_ROOT is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_SAI_MCLK_SEL_MCLK1_MCLK         1 /* SAIx.MCLK is selected */

/*********** PDM Root Clock Select Register (PDM_CLK) *************************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_PDM_CLK_SEL                       (0x00000003UL <<  0U)  /* (RW) PDM Root Clock Select Bits */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_PDM_CLK_RESERVED                  (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_PDM_CLK_SEL_CCM_PDM            0 /* ccm pdm clock is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_PDM_CLK_SEL_SAI_PLL_DIV2       1 /* sai_pll div2 is selected */
#define CHIP_REGFLDVAL_AUDIO_BLK_CTRL_PDM_CLK_SEL_SAI1_MCLK          2 /* SAI1_MCLK is selected */

/*********** AUDIOMIX Extra Addr Bits Register (AUDIO_EXT_ADDR) ***************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_AUDIO_EXT_ADDR_EDMA               (0x00000003UL <<  4U)  /* (RW) EDMA extra Addr Bits */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_AUDIO_EXT_ADDR_SDMA3              (0x00000003UL <<  2U)  /* (RW) SDMA3 extra Addr Bits */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_AUDIO_EXT_ADDR_SDMA2              (0x00000003UL <<  0U)  /* (RW) SDMA2 extra Addr Bits */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_AUDIO_EXT_ADDR_RESERVED           (0xFFFFFFC0UL)

/*********** IPG Low Power Control Register (IPG_LP_CTRL) *********************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI7_IPG_STOP_ACK     (0x00000001UL << 19U)  /* (RO) SAI7 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI6_IPG_STOP_ACK     (0x00000001UL << 18U)  /* (RO) SAI6 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI5_IPG_STOP_ACK     (0x00000001UL << 17U)  /* (RO) SAI5 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI3_IPG_STOP_ACK     (0x00000001UL << 16U)  /* (RO) SAI3 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI2_IPG_STOP_ACK     (0x00000001UL << 15U)  /* (RO) SAI2 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI1_IPG_STOP_ACK     (0x00000001UL << 14U)  /* (RO) SAI1 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_PDM_IPG_STOP_ACK      (0x00000001UL << 13U)  /* (RO) PDM IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SDMA3_IPG_STOP_ACK    (0x00000001UL << 12U)  /* (RO) SDMA3 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SDMA2_IPG_STOP_ACK    (0x00000001UL << 11U)  /* (RO) SDMA2 IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_EDMA_IPG_STOP_ACK     (0x00000001UL << 10U)  /* (RO) EDMA IPG_STOP_ACK Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI7_IPG_STOP         (0x00000001UL <<  9U)  /* (RW) SAI7 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI6_IPG_STOP         (0x00000001UL <<  8U)  /* (RW) SAI6 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI5_IPG_STOP         (0x00000001UL <<  7U)  /* (RW) SAI5 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI3_IPG_STOP         (0x00000001UL <<  6U)  /* (RW) SAI3 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI2_IPG_STOP         (0x00000001UL <<  5U)  /* (RW) SAI2 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SAI1_IPG_STOP         (0x00000001UL <<  4U)  /* (RW) SAI1 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_PDM_IPG_STOP          (0x00000001UL <<  3U)  /* (RW) PDM IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SDMA3_IPG_STOP        (0x00000001UL <<  2U)  /* (RW) SDMA3 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_SDMA2_IPG_STOP        (0x00000001UL <<  1U)  /* (RW) SDMA2 IPG_STOP Bit */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_IPG_LP_CTRL_EDMA_IPG_STOP         (0x00000001UL <<  0U)  /* (RW) EDMA IPG_STOP Bit */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_IPG_LP_CTRL_RESERVED              (0xFFF00000UL)

/*********** AUDIOMIX AXI LIMIT CTRL Register (AUDIO_AXI_LIMIT) ***************/
#define CHIP_REGFLD_AUDIO_BLK_CTRL_AUDIO_AXI_LIMIT_BEAT_LIMIT        (0x0000FFFFUL <<  4U)  /* (RW) Beat Limit. Limit the burst beat number from AudioDSP. */
#define CHIP_REGFLD_AUDIO_BLK_CTRL_AUDIO_AXI_LIMIT_ENABLE            (0x00000001UL <<  0U)  /* (RW) AXI Limit enable */
#define CHIP_REGMSK_AUDIO_BLK_CTRL_AUDIO_AXI_LIMIT_RESERVED          (0xFFF0000EUL)


#endif // REGS_SAI_H


