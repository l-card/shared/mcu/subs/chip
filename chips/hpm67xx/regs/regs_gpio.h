#ifndef REGS_GPIO_H
#define REGS_GPIO_H

#include "regs_common_types.h"


typedef struct {
    CHIP_REGS_BITSETCLR_T DI[16];               /* 0x0: GPIO input value */
    CHIP_REGS_BITSETCLR_T DO[16];               /* 0x100: GPIO output value */
    CHIP_REGS_BITSETCLR_T OE[16];               /* 0x200: GPIO direction */
    CHIP_REGS_BITSETCLR_T IF[16];               /* 0x300: GPIO interrupt flag */
    CHIP_REGS_BITSETCLR_T IE[16];               /* 0x400: GPIO interrupt enable */
    CHIP_REGS_BITSETCLR_T PL[16];               /* 0x400: GPIO interrupt polarity */
    CHIP_REGS_BITSETCLR_T TP[16];               /* 0x400: GPIO interrupt type */
    CHIP_REGS_BITSETCLR_T AS[16];               /* 0x400: GPIO interrupt asynchronous */
    CHIP_REGS_BITSETCLR_T PD[16];               /* 0x400: GPIO dual edge interrupt enable */
} CHIP_REGS_GPIO_T;

#define CHIP_REGS_FGPIO             ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_FGPIO)
#define CHIP_REGS_GPIO0             ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_GPIO0)
#define CHIP_REGS_GPIO1             ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_GPIO1)
#define CHIP_REGS_PGPIO             ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PGPIO)
#define CHIP_REGS_BGPIO             ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_BGPIO)


#endif // REGS_GPIO_H
