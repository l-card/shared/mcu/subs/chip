CHIP_DEFS += CHIP_CORETYPE_QINGKE_V4


CHIP_CORE_MODEL_DIR := $(CHIP_CORETYPE_DIR)/models/$(CHIP_TARGET_CORE_MODEL)
ifeq (,$(wildcard  ${CHIP_CORE_MODEL_DIR}/chip_core_model.h))
    $(error $(CHIP_TARGET_CORE_MODEL) is not supported quinke core model)
else
    CHIP_INC_DIRS += ${CHIP_CORE_MODEL_DIR}
endif



CHIP_TOOLCHAIN_TARGET ?= riscv-none-embed-
ifeq ($(CHIP_TARGET_CORE_MODEL), v4f)
    CHIP_CORE_FLAGS = -march=rv32imacf
else
    CHIP_CORE_FLAGS = -march=rv32imac
endif

LTIMER_PORT = wch_systick

CHIP_GEN_SRC += ${CHIP_CORETYPE_DIR}/chip_coretype_init.c

CHIP_CFLAGS  += $(CHIP_CORE_FLAGS)
CHIP_ASFLAGS += $(CHIP_CORE_FLAGS)
CHIP_LDFLAGS += $(CHIP_CORE_FLAGS)


