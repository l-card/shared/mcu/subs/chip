#ifndef CHIP_LPC11U6X_REGS_TIMER_H
#define CHIP_LPC11U6X_REGS_TIMER_H


/** @defgroup TIMER_11U6X CHIP: LPC11u6x 16/32-bit Timer driver
 * @ingroup CHIP_11U6X_Drivers
 * <b>IMPORTANT NOTE ABOUT lpc11u6x TIMERS</b><br>
 * For timer 0 on both the 16-bit and 32-bit timers, the capture functions
 * use index 0 for capture 0 functions and index 2 for capture 1 functions,
 * while timer 1 for both the 16-bit and 32-bit timers uses index 0 and
 * index 1. Use care when selecting
 * The LPC11U6X User manual is inconsistent in it's designation of capture
 * channels for each timer. See the comments for each function for special
 * handling per timer when dealing with capture channels.
 * @{
 */

/**
 * @brief 32-bit Standard timer register block structure
 */
typedef struct {                /*!< TIMERn Structure       */
    __IO uint32_t IR;           /*!< Interrupt Register. The IR can be written to clear interrupts. The IR can be read to identify which of eight possible interrupt sources are pending. */
    __IO uint32_t TCR;          /*!< Timer Control Register. The TCR is used to control the Timer Counter functions. The Timer Counter can be disabled or reset through the TCR. */
    __IO uint32_t TC;           /*!< Timer Counter. The 32 bit TC is incremented every PR+1 cycles of PCLK. The TC is controlled through the TCR. */
    __IO uint32_t PR;           /*!< Prescale Register. The Prescale Counter (below) is equal to this value, the next clock increments the TC and clears the PC. */
    __IO uint32_t PC;           /*!< Prescale Counter. The 32 bit PC is a counter which is incremented to the value stored in PR. When the value in PR is reached, the TC is incremented and the PC is cleared. The PC is observable and controllable through the bus interface. */
    __IO uint32_t MCR;          /*!< Match Control Register. The MCR is used to control if an interrupt is generated and if the TC is reset when a Match occurs. */
    __IO uint32_t MR[4];        /*!< Match Register. MR can be enabled through the MCR to reset the TC, stop both the TC and PC, and/or generate an interrupt every time MR matches the TC. */
    __IO uint32_t CCR;          /*!< Capture Control Register. The CCR controls which edges of the capture inputs are used to load the Capture Registers and whether or not an interrupt is generated when a capture takes place. */
    __IO uint32_t CR[4];        /*!< Capture Register. CR is loaded with the value of TC when there is an event on the CAPn.0 input. */
    __IO uint32_t EMR;            /*!< External Match Register. The EMR controls the external match pins MATn.0-3 (MAT0.0-3 and MAT1.0-3 respectively). */
    __I  uint32_t RESERVED0[12];
    __IO uint32_t CTCR;            /*!< Count Control Register. The CTCR selects between Timer and Counter mode, and in Counter mode selects the signal and edge(s) for counting. */
    __IO uint32_t PWMC;
} LPC_TIMER_T;


/** Macro to clear interrupt pending */
#define TIMER_IR_CLR(n)        (1UL << (n))

/** Macro for getting a timer match interrupt bit */
#define TIMER_MATCH_INT(n)      (1UL << ((n) & 0x0F))
/** Macro for getting a capture event interrupt bit */
#define TIMER_CAP_INT(n)        (1UL << ((((n) & 0x0F) + 4)))

/** Timer/counter enable bit */
#define TIMER_ENABLE            ((uint32_t) (1 << 0))
/** Timer/counter reset bit */
#define TIMER_RESET             ((uint32_t) (1 << 1))

/** Bit location for interrupt on MRx match, n = 0 to 3 */
#define TIMER_INT_ON_MATCH(n)   (1UL << (((n) * 3)))
/** Bit location for reset on MRx match, n = 0 to 3 */
#define TIMER_RESET_ON_MATCH(n) (1UL << ((((n) * 3) + 1)))
/** Bit location for stop on MRx match, n = 0 to 3 */
#define TIMER_STOP_ON_MATCH(n)  (1UL << ((((n) * 3) + 2)))

/** Bit location for CAP.n on CRx rising edge, n = 0 to 3 */
#define TIMER_CAP_RISING(n)     (1UL << (((n) * 3)))
/** Bit location for CAP.n on CRx falling edge, n = 0 to 3 */
#define TIMER_CAP_FALLING(n)    (1UL << ((((n) * 3) + 1)))
/** Bit location for CAP.n on CRx interrupt enable, n = 0 to 3 */
#define TIMER_INT_ON_CAP(n)     (1UL << ((((n) * 3) + 2)))

/* CTCR register */
#define TIMER_CTCR_MODE_NORMAL          0
#define TIMER_CTCR_MODE_CAPnRISE(n)     (1U | (((n) & 3U) << 2))
#define TIMER_CTCR_MODE_CAPnFALL(n)     (2U | (((n) & 3U) << 2))
#define TIMER_CTCR_MODE_CAPnBOTH(n)     (3U | (((n) & 3U) << 2))
#define TIMER_CTCR_CLR_CAPnRISE(n)      ((1U << 4) | (((n) * 2U) << 5))
#define TIMER_CTCR_CLR_CAPnFALL(n)      ((1U << 4) | (((n) * 2U + 1U) << 5))

/* EMR register */
#define TIMER_EMR_EM(n)                 (1U << ((n) & 3))
#define TIMER_EMR_EMC_CLEAR(n)          (1U << (4 + (((n) & 3) * 2)))
#define TIMER_EMR_EMC_SET(n)            (2U << (4 + (((n) & 3) * 2)))
#define TIMER_EMR_EMC_TOGGLE(n)         (3U << (4 + (((n) & 3) * 2)))

#endif /* CHIP_LPC11U6X_REGS_TIMER_H */
