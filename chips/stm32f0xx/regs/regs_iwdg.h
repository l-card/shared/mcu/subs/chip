#ifndef CHIP_STM32F0XX_REGS_IWDG_H
#define CHIP_STM32F0XX_REGS_IWDG_H

#include <stdint.h>

typedef struct {
  __IO uint32_t KR;          /*!< IWDG Key register,       Address offset: 0x00 */
  __IO uint32_t PR;          /*!< IWDG Prescaler register, Address offset: 0x04 */
  __IO uint32_t RLR;         /*!< IWDG Reload register,    Address offset: 0x08 */
  __IO uint32_t SR;          /*!< IWDG Status register,    Address offset: 0x0C */
  __IO uint32_t WINR;        /*!< IWDG Window register,    Address offset: 0x10 */
}  CHIP_REGS_IWDG_T ;

#define CHIP_REGS_IWDG          ((CHIP_REGS_IWDG_T *) CHIP_MEMRGN_ADDR_PERIPH_IWDG)

/*******************  Bit definition for IWDG_KR register  ********************/
#define CHIP_REGFLD_IWDG_KR_KEY                         (0x0000FFFFUL <<  0U)

/*******************  Bit definition for IWDG_PR register  ********************/
#define CHIP_REGFLD_IWDG_PR_PR                          (0x00000007UL <<  0U)

/*******************  Bit definition for IWDG_RLR register  *******************/
#define CHIP_REGFLD_IWDG_RLR_RL                         (0x00000FFFUL <<  0U)

/*******************  Bit definition for IWDG_SR register  ********************/
#define CHIP_REGFLD_IWDG_SR_PVU                         (0x00000001UL <<  0U)
#define CHIP_REGFLD_IWDG_SR_RVU                         (0x00000001UL <<  1U)
#define CHIP_REGFLD_IWDG_SR_WVU                         (0x00000001UL <<  2U)

/*******************  Bit definition for IWDG_KR register  ********************/
#define CHIP_REGFLD_IWDG_WINR_WIN                       (0x00000FFFUL <<  0U)

#endif // CHIP_STM32F0XX_REGS_IWDG_H
