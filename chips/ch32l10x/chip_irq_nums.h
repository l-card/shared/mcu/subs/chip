#ifndef CHIP_IRQ_NUMS_H
#define CHIP_IRQ_NUMS_H

#include "regs_pfic.h"

#define CHIP_IRQNUM_WWDG                16  /* Window watchdog timer interrupt */
#define CHIP_IRQNUM_PVD                 17  /* Supply Voltage Detection Interrupt (EXTI) */
#define CHIP_IRQNUM_TAMPER              18  /* Tamper detection interrupt */
#define CHIP_IRQNUM_RTC                 19  /* Real-time clock interrupt */
#define CHIP_IRQNUM_FLASH               20  /* Flash global interrupt */
#define CHIP_IRQNUM_RCC                 21  /* Reset and clock control interrupts */
#define CHIP_IRQNUM_EXTI0               22  /* EXTI line 0 interrupt */
#define CHIP_IRQNUM_EXTI1               23  /* EXTI line 1 interrupt */
#define CHIP_IRQNUM_EXTI2               24  /* EXTI line 2 interrupt */
#define CHIP_IRQNUM_EXTI3               25  /* EXTI line 3 interrupt */
#define CHIP_IRQNUM_EXTI4               26  /* EXTI line 4 interrupt */
#define CHIP_IRQNUM_DMA_CH1             27  /* DMA1 channel 1 global interrupt */
#define CHIP_IRQNUM_DMA_CH2             28  /* DMA1 channel 2 global interrupt */
#define CHIP_IRQNUM_DMA_CH3             29  /* DMA1 channel 3 global interrupt */
#define CHIP_IRQNUM_DMA_CH4             30  /* DMA1 channel 4 global interrupt */
#define CHIP_IRQNUM_DMA_CH5             31  /* DMA1 channel 5 global interrupt */
#define CHIP_IRQNUM_DMA_CH6             32  /* DMA1 channel 6 global interrupt */
#define CHIP_IRQNUM_DMA_CH7             33  /* DMA1 channel 7 global interrupt */
#define CHIP_IRQNUM_ADC                 34  /* ADC global interrupt  */
#define CHIP_IRQNUM_USB_HP_CAN_TX       35  /* USB_HP or CAN_TX global interrupt */
#define CHIP_IRQNUM_USB_LP_CAN_RX0      36  /* USB_LP or CAN_RX0 global interrupt */
#define CHIP_IRQNUM_CAN_RX1             37  /* CAN_RX1 global interrupt */
#define CHIP_IRQNUM_CAN_SCE             38  /* CAN_SCE global interrupt */
#define CHIP_IRQNUM_EXTI9_5             39  /* EXTI line[9:5] interrupts */
#define CHIP_IRQNUM_TIM1_BRK            40  /* TIM1 break interrupt */
#define CHIP_IRQNUM_TIM1_UP             41  /* TIM1 update interrupt */
#define CHIP_IRQNUM_TIM1_TRG_COM        42  /* TIM1 trigger and communication interrupts */
#define CHIP_IRQNUM_TIM1_CC             43  /* TIM1 capture compare interrupt */
#define CHIP_IRQNUM_TIM2                44  /* TIM2 global interrupt */
#define CHIP_IRQNUM_TIM3                45  /* TIM3 global interrupt */
#define CHIP_IRQNUM_TIM4                46  /* TIM4 global interrupt */
#define CHIP_IRQNUM_I2C1_EV             47  /* I2C1 event interrupt */
#define CHIP_IRQNUM_I2C1_ER             48  /* I2C1 error interrupt */
#define CHIP_IRQNUM_I2C2_EV             49  /* I2C2 event interrupt */
#define CHIP_IRQNUM_I2C2_ER             50  /* I2C2 error interrupt */
#define CHIP_IRQNUM_SPI1                51  /* SPI1 interrupt */
#define CHIP_IRQNUM_SPI2                52  /* SPI2 interrupt */
#define CHIP_IRQNUM_USART1              53  /* USART1 interrupt */
#define CHIP_IRQNUM_USART2              54  /* USART2 interrupt */
#define CHIP_IRQNUM_USART3              55  /* USART3 interrupt */
#define CHIP_IRQNUM_EXTI15_10           56  /* EXTI line[15:10] interrupts */
#define CHIP_IRQNUM_RTC_ALARM           57  /* RTC alarm clock (EXTI) */
#define CHIP_IRQNUM_LPTIM_WKUP          58  /* LPTIM wakeup interrupt */
#define CHIP_IRQNUM_USBFS               59  /* USBFS global interrupt */
#define CHIP_IRQNUM_USBFS_WKUP          60  /* USBFS wakeup interrupt */
#define CHIP_IRQNUM_USART4              61  /* USART4 global interrupt */
#define CHIP_IRQNUM_DMA_CH8             62  /* DMA channel 8 global interrupt */
#define CHIP_IRQNUM_LPTIM               63  /* LPTIM global interrupt */
#define CHIP_IRQNUM_OPA                 64  /* OPA global interrupt */
#define CHIP_IRQNUM_USBPD               65  /* USBPD global interrupt */
#define CHIP_IRQNUM_USBPD_WKUP          67  /* USBPD wakeup interrupt */
#define CHIP_IRQNUM_CMP_WKUP            68  /* CMP wakeup interrupt */


#endif // CHIP_IRQ_NUMS_H
