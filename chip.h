#ifndef __CHIP_H_
#define __CHIP_H_

#ifndef __ASSEMBLER__
#include <stdint.h>
#endif

/* запрет inline на все RAM-функции, т.к. иеначе компилятор их может вставить до разделения на секции */
#define CHIP_RAM_FUNC __attribute__((section(".ram_func"))) __attribute__ ((__noinline__))

#include "lbitfield.h"
#include "chip_pkg_types.h"
#include "chip_archtype.h"
#include "chip_target.h"




#endif /* __CHIP_H_ */
