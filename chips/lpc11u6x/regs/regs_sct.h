#ifndef CHIP_LPC11U6X_REGS_SCT_H
#define CHIP_LPC11U6X_REGS_SCT_H

#include <stdint.h>

#define CHIP_SCT_MATCHCAP_CNT         5
#define CHIP_SCT_EVT_CNT              6
#define CHIP_SCT_OUT_CNT              4
#define CHIP_SCT_IN_CNT               4
#define CHIP_SCT_DMAREQ_CNT           2

typedef union {
     __IO uint32_t U;           /* unified 32-bit register */
     struct {
         __IO uint16_t L;       /* low couner 16-bit */
         __IO uint16_t H;       /* high couner 16-bit */
     };
} CHIP_SCT_UNION_REG;

typedef struct {
    __IO uint32_t CONFIG;               /* SCT configuration register,                  0x000 */
    __IO CHIP_SCT_UNION_REG CTRL;       /* SCT control register,                        0x004 */
    __IO CHIP_SCT_UNION_REG LIMIT;      /* SCT limit register,                          0x008 */
    __IO CHIP_SCT_UNION_REG HALT;       /* SCT halt condition register,                 0x00C */
    __IO CHIP_SCT_UNION_REG STOP;       /* SCT stop condition register,                 0x010 */
    __IO CHIP_SCT_UNION_REG START;      /* SCT start condition register,                0x014 */
    uint32_t Reserved[10];              /* Reserved,                                    0x018 - 0x03C */
    __IO CHIP_SCT_UNION_REG COUNT;      /* SCT counter register,                        0x040 */
    __IO CHIP_SCT_UNION_REG STATE;      /* SCT state register,                          0x044 */
    __I  uint32_t INPUT;                /* SCT input register,                          0x048 */
    __IO CHIP_SCT_UNION_REG REGMODE;    /* SCT match/capture registers mode register,   0x04C */
    __IO CHIP_SCT_UNION_REG OUTPUT;     /* SCT output register,                         0x050 */
    __IO uint32_t OUTPUT_DIRCTL;        /* SCT output counter direction control register, 0x054 */
    __IO uint32_t RES;                  /* SCT conflict resolution register,            0x058 */
    union {
        __IO uint32_t DMAREQ[2];        /* SCT DMA request register,                    0x05C - 0x060 */
        struct {
            __IO uint32_t DMAREQ0;
            __IO uint32_t DMAREQ1;
        };
    };
    uint32_t Reserved2[35];              /* Reserved,                                    0x064 - 0x0EC */
    __IO uint32_t EV_EN;                 /* SCT event enable register,                  0x0F0 **/
    __IO uint32_t EV_FLAG;               /* SCT event flag register,                    0x0F4 **/
    __IO uint32_t CON_EN;                /* SCT conflict enable register,               0x0F8 **/
    __IO uint32_t CON_FLAG;              /* SCT conflict flag register,                 0x0FC **/
    union {
        __IO CHIP_SCT_UNION_REG MATCH[5]; /* SCT match value of match channels 0-4      0x100 - 0x110 */
        __IO CHIP_SCT_UNION_REG CAP[5];   /* SCT capture of capture channel 0 to 4;     0x100 - 0x110 */
    };
    uint32_t Reserved3[59];               /* Reserved                                   0x114 - 0x1FC */
    union {
        __IO CHIP_SCT_UNION_REG MATCHREL[5]; /* SCT match reload value channels 0-4     0x200 - 0x210 */
        __IO CHIP_SCT_UNION_REG CAPCTRL[5];  /* SCT capture control channel 0 to 4;     0x200 - 0x210 */
    };
    uint32_t Reserved4[59];               /* Reserved                                   0x214 - 0x2FC */
    struct {
        __IO uint32_t STATE;              /* SCT event state register,                  0x300 - 0x328 */
        __IO uint32_t CTRL;               /* SCT event control register,                0x304 - 0x32C */
    } EV[CHIP_SCT_EVT_CNT];
    uint32_t Reserved5[116];              /* Reserved                                   0x330 - 0x4FC */
    struct {
        __IO uint32_t SET;               /* SCT output set register,                    0x500 - 0x518 */
        __IO uint32_t CLR;               /* SCT output clear register,                  0x504 - 0x51C */
    } OUT[CHIP_SCT_OUT_CNT];
} CHIP_REGS_SCT_T;

#define CHIP_REGS_SCT0              ((CHIP_REGS_SCT_T *) CHIP_MEMRGN_ADDR_PERIPH_SCT0)
#define CHIP_REGS_SCT1              ((CHIP_REGS_SCT_T *) CHIP_MEMRGN_ADDR_PERIPH_SCT1)

/*********** SCT configuration register (SCT_CONFIG) **************************/
#define CHIP_REGFLD_SCT_CONFIG_UNIFY                (0x00000001UL <<  0U)   /* (RW) SCT Operation as unified 32-bit couner (0 - 2x16) */
#define CHIP_REGFLD_SCT_CONFIG_CLKMODE              (0x00000003UL <<  1U)   /* (RW) SCT clock mode */
#define CHIP_REGFLD_SCT_CONFIG_CLKSEL               (0x00000007UL <<  3U)   /* (RW) SCT clock select */
#define CHIP_REGFLD_SCT_CONFIG_NORELOAD_L           (0x00000001UL <<  7U)   /* (RW) Prevents the lower or unified match registers from being reloaded */
#define CHIP_REGFLD_SCT_CONFIG_NORELOAD_H           (0x00000001UL <<  8U)   /* (RW) Prevents the high match registers from being reloaded */
#define CHIP_REGFLD_SCT_CONFIG_INSYNC(n)            (0x00000001UL <<  (9U + (n)))  /* (RW) Synhronization for input n */
#define CHIP_REGFLD_SCT_CONFIG_AUTOLIMIT_L          (0x00000001UL << 17U)   /* (RW) MATCH0 lower or unified as LIMIT without events */
#define CHIP_REGFLD_SCT_CONFIG_AUTOLIMIT_H          (0x00000001UL << 18U)   /* (RW) MATCH0 higher as LIMIT without events */
#define CHIP_REGMSK_SCT_CONFIG_RESERVED             (0xFFF9E000UL)


#define CHIP_REGFLDVAL_SCT_CONFIG_CLKMODE_AHB           0 /* The bus clock clocks the SCT and prescalers. */
#define CHIP_REGFLDVAL_SCT_CONFIG_CLKMODE_AHB_CLKSELEN  1 /* The SCT clock is the bus clock, enabled by CLKSEL input */
#define CHIP_REGFLDVAL_SCT_CONFIG_CLKMODE_CLKSEL_SYNC   2 /* The input selected by CKSEL clocks the SCT and prescalers.
                                                             The input issynchronized to the bus clock */
#define CHIP_REGFLDVAL_SCT_CONFIG_CLKMODE_CLKSEL_DIRECT 3 /* Prescaled SCT input. The SCT and prescalers are clocked by the input edge
                                                             selected by the CKSEL field */

#define CHIP_REGFLDVAL_SCT_CONFIG_CLKSEL_IN_RISE(innum) (2 * (innum))
#define CHIP_REGFLDVAL_SCT_CONFIG_CLKSEL_IN_FALL(innum) (2 * (innum) + 1)


/*********** SCT control register (SCT_CTRL) **********************************/
#define CHIP_REGFLD_SCT_CTRL_DOWN_L                 (0x00000001UL <<  0U)   /* (RW) counter is counting down */
#define CHIP_REGFLD_SCT_CTRL_STOP_L                 (0x00000001UL <<  1U)   /* (RW) counter does not run, but I/O eventsrelated to the counter can occur. */
#define CHIP_REGFLD_SCT_CTRL_HALT_L                 (0x00000001UL <<  2U)   /* (RW) counter does not run and no events can occur */
#define CHIP_REGFLD_SCT_CTRL_CLRCTR_L               (0x00000001UL <<  3U)   /* (W1) write a 1 to clear couner */
#define CHIP_REGFLD_SCT_CTRL_BIDIR_L                (0x00000001UL <<  4U)   /* (RW) couner bidirection select */
#define CHIP_REGFLD_SCT_CTRL_PRE_L                  (0x000000FFUL <<  5U)   /* (RW) clock prescaler */
#define CHIP_REGFLD_SCT_CTRL_DOWN_H                 (0x00000001UL << 16U)   /* (RW) */
#define CHIP_REGFLD_SCT_CTRL_STOP_H                 (0x00000001UL << 17U)   /* (RW) */
#define CHIP_REGFLD_SCT_CTRL_HALT_H                 (0x00000001UL << 18U)   /* (RW) */
#define CHIP_REGFLD_SCT_CTRL_CLRCTR_H               (0x00000001UL << 19U)   /* (W1) */
#define CHIP_REGFLD_SCT_CTRL_BIDIR_H                (0x00000001UL << 20U)   /* (RW) */
#define CHIP_REGFLD_SCT_CTRL_PRE_H                  (0x000000FFUL << 21U)   /* (RW) */
#define CHIP_REGMSK_SCT_CTRL_RESERVED               (0xE000E000UL)

/*********** SCT limit register (SCT_LIMIT) ***********************************/
#define CHIP_REGFLD_SCT_LIMIT_LIMMSK_L(n)           (0x00000001UL << ( 0U + (n))) /* (RW) event n is used as a counter limit */
#define CHIP_REGFLD_SCT_LIMIT_LIMMSK_H(n)           (0x00000001UL << (16U + (n))) /* (RW) */

/*********** SCT halt condition register (SCT_HALT) ***************************/
#define CHIP_REGFLD_SCT_HALT_HALTMSK_L(n)           (0x00000001UL << ( 0U + (n))) /* (RW) event n sets the HALT_L bit in the CTRL */
#define CHIP_REGFLD_SCT_HALT_HALTMSK_H(n)           (0x00000001UL << (16U + (n))) /* (RW) event n sets the HALT_H bit in the CTRL */

/*********** SCT stop condition register (SCT_STOP) ***************************/
#define CHIP_REGFLD_SCT_STOP_STOPMSK_L(n)           (0x00000001UL << ( 0U + (n))) /* (RW) event n sets the STOP_L bit in the CTRL */
#define CHIP_REGFLD_SCT_STOP_STOPMSK_H(n)           (0x00000001UL << (16U + (n))) /* (RW) event n sets the STOP_H bit in the CTRL */

/*********** SCT start condition register (SCT_START) *************************/
#define CHIP_REGFLD_SCT_START_STARTMSK_L(n)         (0x00000001UL << ( 0U + (n))) /* (RW) event n clear the STOP_L bit in the CTRL */
#define CHIP_REGFLD_SCT_START_STARTMSK_H(n)         (0x00000001UL << (16U + (n))) /* (RW) event n clear the STOP_H bit in the CTRL */

/*********** SCT counter register (SCT_COUNT) *********************************/
#define CHIP_REGFLD_SCT_COUNT_CTR_L                 (0x0000FFFFUL <<  0U) /* (RW) 16-bit L counter value */
#define CHIP_REGFLD_SCT_COUNT_CTR_H                 (0x0000FFFFUL << 16U) /* (RW) 16-bit H counter value */

/*********** SCT state register (SCT_STATE) ***********************************/
#define CHIP_REGFLD_SCT_STATE_L                     (0x0000001FUL <<  0U) /* (RW) state variable */
#define CHIP_REGFLD_SCT_STATE_H                     (0x0000001FUL << 16U) /* (RW) state variable */

/*********** SCT input register (SCT_INPUT) ***********************************/
#define CHIP_REGFLD_SCT_INPUT_AIN(n)                (0x00000001UL << ( 0U + (n))) /* (RO) Input n state. Direct read. */
#define CHIP_REGFLD_SCT_INPUT_SIN(n)                (0x00000001UL << (16U + (n))) /* (RO) Input n state. Syncronized to clk if INSYNC set */

/*********** SCT match/capture registers mode register (SCT_REGMODE) **********/
#define CHIP_REGFLD_SCT_REGMODE_L(n)                (0x00000001UL << ( 0U + (n))) /* (RW) select match/capture for ragister n */
#define CHIP_REGFLD_SCT_REGMODE_H(n)                (0x00000001UL << (16U + (n))) /* (RW) */

#define CHIP_REGFLDVAL_SCT_REGMODE_MATCH            0
#define CHIP_REGFLDVAL_SCT_REGMODE_CAPTURE          1

/*********** SCT output register (SCT_OUTPUT) *********************************/
#define CHIP_REGFLD_SCT_OUTPUT_OUT(n)               (0x00000001UL << ( 0U + (n))) /* (RW) set output N when counters halted, read out state */

/*********** SCT bidirectional output control register (SCT_OUTPUT_DIRCTL) ****/
#define CHIP_REGFLD_SCT_OUTPUT_DIRCTL_SETCLR(n)     (0x00000003UL << ( 0U + 2*(n))) /* (RW) Set/clear operation on output n */

#define CHIP_REGFLDVAL_SCT_OUTPUT_DIRCTL_SETCLR_NOTDEP      0 /* Set and clear do not depend on any counter. */
#define CHIP_REGFLDVAL_SCT_OUTPUT_DIRCTL_SETCLR_REV_DOWN_L  1 /*  Set and clear are reversed when counter L or the unified counter is counting down. */
#define CHIP_REGFLDVAL_SCT_OUTPUT_DIRCTL_SETCLR_REV_DOWN_H  2 /*  Set and clear are reversed when counter H is counting down. */

/*********** SCT conflict resolution register (SCT_RES) ***********************/
#define CHIP_REGFLD_SCT_RES_O(n)                    (0x00000003UL << ( 0U + 2*(n))) /* (RW) Effect of simultaneous set and clear on output n */

#define CHIP_REGFLDVAL_SCT_RES_O_NO_CHANGE          0 /* No change. */
#define CHIP_REGFLDVAL_SCT_RES_O_SET                1 /* Set output (or clear based on the SETCLR0 field). */
#define CHIP_REGFLDVAL_SCT_RES_O_CLEAR              2 /* Clear output (or set based on the SETCLR0 field). */
#define CHIP_REGFLDVAL_SCT_RES_O_TOGGLE             3 /* Toggle output */

/*********** SCT DMA request 0 and 1 registers (SCT_DMAREQ) *******************/
#define CHIP_REGFLD_SCT_DMAREQ_DEV(n)               (0x00000001UL << ( 0U + (n))) /* (RW) event n set DMA request */
#define CHIP_REGFLD_SCT_DMAREQ_DRL                  (0x00000001UL << 30U) /* (RW) DMA request when it loads the Match_L/Unified registers from the Reload_L/Unified     registers */
#define CHIP_REGFLD_SCT_DMAREQ_DRQ                  (0x00000001UL << 31U) /* (RO) DMA request state */

/*********** SCT event enable register (SCT_EV_EN) ****************************/
#define CHIP_REGFLD_SCT_EV_EN_IEN(n)                (0x00000001UL << ( 0U + (n))) /* (RW) SCT requests an interrupt when bit n of this register and the event flag register are both one */

/*********** SCT event flag register (SCT_EV_FLAG) ****************************/
#define CHIP_REGFLD_SCT_EV_FLAG(n)                  (0x00000001UL << ( 0U + (n))) /* (RW1C) event n has occured since reset or a 1 was last written to this bit */

/*********** SCT conflict enable register (SCT_CON_EN) ************************/
#define CHIP_REGFLD_SCT_CON_EN_NCEN(n)              (0x00000001UL << ( 0U + (n))) /* (RW) The SCT requests interrupt when bit n of this register and the SCT 0 conflict flag register are both one */

/*********** SCT conflict flag register (SCT_CON_FLAG) ************************/
#define CHIP_REGFLD_SCT_CON_FLAG_NCFLAG(n)          (0x00000001UL << ( 0U + (n))) /* (RW1C) Bit n is one if a no-change conflict event occurred on output n since reset or a 1 was last written to this bit */
#define CHIP_REGFLD_SCT_CON_FLAG_BUSERR_L           (0x00000001UL << 30U) /* (RW1C) The most recent bus error from this SCT involved writing CTR L/Unified, STATE L/Unified, MATCH L/Unified, or the Output register when the L/U counter was not halted. */
#define CHIP_REGFLD_SCT_CON_FLAG_BUSERR_H           (0x00000001UL << 31U) /* (RW1C) The most recent bus error from this SCT involved writing CTR H, STATE H, MATCH H, or the Output register when the H counter was not halted */

/*********** SCT match registers 0 to 4 (SCT_MATCH) ***************************/
#define CHIP_REGFLD_SCT_MATCH_L                     (0x0000FFFFUL <<  0U) /* (RW) 16-bit value to be compared to the L counter. */
#define CHIP_REGFLD_SCT_MATCH_H                     (0x0000FFFFUL << 16U) /* (RW) 16-bit value to be compared to the H counter. */

/*********** SCT capture registers 0 to 4 (SCT_CAP) ***************************/
#define CHIP_REGFLD_SCT_CAP_L                       (0x0000FFFFUL <<  0U) /* (RO) 16-bit counter value at which this register was last captured. */
#define CHIP_REGFLD_SCT_CAP_H                       (0x0000FFFFUL << 16U) /* (RO) */

/*********** SCT match reload registers 0 to 4 (SCT_MATCHREL) *****************/
#define CHIP_REGFLD_SCT_MATCHREL_L                  (0x0000FFFFUL <<  0U) /* (RW) 16-bit value to be loaded into the SCTMATCHn_L register */
#define CHIP_REGFLD_SCT_MATCHREL_H                  (0x0000FFFFUL << 16U) /* (RW) 16-bit value to be loaded into the SCTMATCHn_H register */

/*********** SCT capture control registers 0 to 4 (SCT_CAPCTRL) ***************/
#define CHIP_REGFLD_SCT_CAPCTRL_L(n)                (0x00000001UL << ( 0U + (n))) /* (RW) event n causes the CAPn_L or the 0 CAPn to be loaded */
#define CHIP_REGFLD_SCT_CAPCTRL_H(n)                (0x00000001UL << (16U + (n))) /* (RW) event n causes the CAPn_H to be loaded */

/*********** SCT event state registers 0 to 5 (SCT_EV_STATE) ******************/
#define CHIP_REGFLD_SCT_EV_STATE_MSK(n)             (0x00000001UL << ( 0U + (n))) /* (RW) event happens in state n of the counter selected by the HEVENT bit */

/*********** SCT event control registers 0 to 5 (SCT_EV_CTRL) *****************/
#define CHIP_REGFLD_SCT_EV_CTRL_MATCHSEL            (0x0000000FUL <<  0U) /* (RW) Selects the Match register associated with this event (if any) */
#define CHIP_REGFLD_SCT_EV_CTRL_HEVENT              (0x00000001UL <<  4U) /* (RW) Select L(0)/H(1) counter. Do not set this bit if UNIFY = 1 */
#define CHIP_REGFLD_SCT_EV_CTRL_OUTSEL              (0x00000001UL <<  5U) /* (RW) Input(0)/Output(1) select */
#define CHIP_REGFLD_SCT_EV_CTRL_IOSEL               (0x0000000FUL <<  5U) /* (RW) Select the nput or output signal number (0 to 3) associated with this event (if any) */
#define CHIP_REGFLD_SCT_EV_CTRL_IOCOND              (0x00000003UL << 10U) /* (RW) Select I/O condition for event */
#define CHIP_REGFLD_SCT_EV_CTRL_COMBMODE            (0x00000003UL << 12U) /* (RW) Selects how the specified match and I/O condition are used and combined. */
#define CHIP_REGFLD_SCT_EV_CTRL_STATE_LD            (0x00000001UL << 14U) /* (RW) how the STATEV value modifies the state selected by HEVENT (added(0) or loaded (1)) */
#define CHIP_REGFLD_SCT_EV_CTRL_STATE_EV            (0x0000001FUL << 15U) /* (RW) This value is loaded into or added to the state selected by HEVENT */
#define CHIP_REGFLD_SCT_EV_CTRL_MATCH_MEM           (0x00000001UL << 20U) /* (RW) Grether than or eq up/less then down or eq or match equal only  */
#define CHIP_REGFLD_SCT_EV_CTRL_DIRECTION           (0x00000003UL << 21U) /* (RW) Direction qualifier for event generation for BIDIR mode */

#define CHIP_REGFLDVAL_SCT_EV_CTRL_IOCOND_LOW       0
#define CHIP_REGFLDVAL_SCT_EV_CTRL_IOCOND_RISE      1
#define CHIP_REGFLDVAL_SCT_EV_CTRL_IOCOND_FALL      2
#define CHIP_REGFLDVAL_SCT_EV_CTRL_IOCOND_HIGH      3

#define CHIP_REGFLDVAL_SCT_EV_CTRL_COMBMODE_OR      0 /* The event occurs when either the specified match or I/O condition occurs. */
#define CHIP_REGFLDVAL_SCT_EV_CTRL_COMBMODE_MATCH   1 /* Uses the specified match only. */
#define CHIP_REGFLDVAL_SCT_EV_CTRL_COMBMODE_IO      2 /* Uses the specified I/O condition only. */
#define CHIP_REGFLDVAL_SCT_EV_CTRL_COMBMODE_AND     3 /*  The event occurs when the specified match and I/O condition occur simultaneously. */

#define CHIP_REGFLDVAL_SCT_EV_CTRL_DIRECTION_ANY    0 /* Direction independent. This event is triggered regardless of the count direction */
#define CHIP_REGFLDVAL_SCT_EV_CTRL_DIRECTION_UP     1 /* Counting up. This event is triggered only during up-counting when BIDIR = 1. */
#define CHIP_REGFLDVAL_SCT_EV_CTRL_DIRECTION_DOWN   2 /* Counting down. This event is triggered only during down-counting when BIDIR = 1. */

/*********** SCT output set registers 0 to 3 (SCT_OUT_SET) ********************/
#define CHIP_REGFLD_SCT_OUT_SET(n)                  (0x00000001UL << ( 0U + (n))) /* (RW) event n set corresponding output */

/*********** SCT output clear registers 0 to 3 (SCT_OUT_CLR) ******************/
#define CHIP_REGFLD_SCT_OUT_CLR(n)                  (0x00000001UL << ( 0U + (n))) /* (RW) event n clear corresponding output */


#endif // CHIP_LPC11U6X_REGS_SCT_H
