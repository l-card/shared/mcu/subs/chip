#ifndef REGS_ENET_H
#define REGS_ENET_H

#include "chip_ioreg_defs.h"


typedef struct {
    __IO uint32_t MACCFG;                      /* 0x0: MAC Configuration Register */
    __IO uint32_t MACFF;                       /* 0x4: MAC Frame Filter */
    __IO uint32_t HASH_H;                      /* 0x8: Hash Table High Register */
    __IO uint32_t HASH_L;                      /* 0xC: Hash Table Low Register */
    __IO uint32_t GMII_ADDR;                   /* 0x10: GMII Address Register */
    __IO uint32_t GMII_DATA;                   /* 0x14: GMII Data Register */
    __IO uint32_t FLOWCTRL;                    /* 0x18: Flow Control Register */
    __IO uint32_t VLAN_TAG;                    /* 0x1C: VLAN Tag Register */
    __I  uint8_t  RESERVED0[8];                /* 0x20 - 0x27: Reserved */
    __IO uint32_t RWKFRMFILT;                  /* 0x28: Remote Wake-Up Frame Filter Register */
    __IO uint32_t PMT_CSR;                     /* 0x2C: PMT Control and Status Register */
    __IO uint32_t LPI_CSR;                     /* 0x30: LPI Control and Status Register */
    __IO uint32_t LPI_TCR;                     /* 0x34: LPI Timers Control Register */
    __I  uint32_t INTR_STATUS;                 /* 0x38: Interrupt Status Register */
    __IO uint32_t INTR_MASK;                   /* 0x3C: Interrupt Mask Register */
    __IO uint32_t MAC_ADDR_0_HIGH;             /* 0x40: MAC Address 0 High Register */
    __IO uint32_t MAC_ADDR_0_LOW;              /* 0x44: MAC Address 0 Low Register */
    struct {
        __IO uint32_t HIGH;                    /* 0x48: MAC Address High Register */
        __IO uint32_t LOW;                     /* 0x4C: MAC Address Low Register */
    } MAC_ADDR[4];
    __I  uint8_t  RESERVED1[112];              /* 0x68 - 0xD7: Reserved */
    __IO uint32_t XMII_CSR;                    /* 0xD8: SGMII/RGMII/SMII Control and Status Register */
    __IO uint32_t WDOG_WTO;                    /* 0xDC: Watchdog Timeout Register */
    __I  uint8_t  RESERVED2[32];               /* 0xE0 - 0xFF: Reserved */
    __IO uint32_t MMC_CNTRL;                   /* 0x100: MMC Control establishes the operating mode of MMC. */
    __IO uint32_t MMC_INTR_RX;                 /* 0x104: MMC Receive Interrupt maintains the interrupt generated from all of the receive statistic counters. */
    __IO uint32_t MMC_INTR_TX;                 /* 0x108: MMC Transmit Interrupt maintains the interrupt generated from all of the transmit statistic counters */
    __IO uint32_t MMC_INTR_MASK_RX;            /* 0x10C: MMC Receive Interrupt mask maintains the mask for the interrupt generated from all of the receive statistic counters */
    __IO uint32_t MMC_INTR_MASK_TX;            /* 0x110: MMC Transmit Interrupt Mask */
    __IO uint32_t TXOCTETCOUNT_GB;             /* 0x114: Number of bytes transmitted, exclusive of preamble and retried bytes, in good and bad frames. */
    __IO uint32_t TXFRAMECOUNT_GB;             /* 0x118: Number of good and bad frames transmitted, exclusive of retried frames. */
    __IO uint32_t TXBROADCASTFRAMES_G;         /* 0x11C: Number of good broadcast frames transmitted */
    __IO uint32_t TXMLTICASTFRAMES_G;          /* 0x120: Number of good multicast frames transmitted */
    __IO uint32_t TX64OCTETS_GB;               /* 0x124: Number of good and bad frames transmitted with length 64 bytes, exclusive of preamble and retried frames. */
    __IO uint32_t TX65TO127OCTETS_GB;          /* 0x128: Number of good and bad frames transmitted with length between 65 and 127 (inclusive) bytes, exclusive of preamble and retried frames. */
    __IO uint32_t TX128TO255OCTETS_GB;         /* 0x12C: Number of good and bad frames transmitted with length between 128 and 255 (inclusive) bytes, exclusive of preamble and retried frames. */
    __IO uint32_t TX256TO511OCTETS_GB;         /* 0x130: Number of good and bad frames transmitted with length between 256 and 511 (inclusive) bytes, exclusive of preamble and retried frames. */
    __IO uint32_t TX512TO1023OCTETS_GB;        /* 0x134: Number of good and bad frames transmitted with length between 512 and 1,023 (inclusive) bytes, exclusive of preamble and retried frames. */
    __IO uint32_t TX1024TOMAXOCTETS_GB;        /* 0x138: Number of good and bad frames transmitted with length between 1,024 and maxsize (inclusive) bytes, exclusive of preamble and retried frames. */
    __I  uint8_t  RESERVED3[68];               /* 0x13C - 0x17F: Reserved */
    __IO uint32_t RXFRAMECOUNT_GB;             /* 0x180: Number of good and bad frames received */
    __I  uint8_t  RESERVED4[124];              /* 0x184 - 0x1FF: Reserved */
    __IO uint32_t MMC_IPC_INTR_MASK_RX;        /* 0x200: MMC IPC Receive Checksum Offload Interrupt Mask maintains the mask for the interrupt generated from the receive IPC statistic counters. */
    __I  uint8_t  RESERVED5[4];                /* 0x204 - 0x207: Reserved */
    __IO uint32_t MMC_IPC_INTR_RX;             /* 0x208: MMC Receive Checksum Offload Interrupt maintains the interrupt that the receive IPC statistic counters generate. See Table 4-25 for further detail. */
    __I  uint8_t  RESERVED6[4];                /* 0x20C - 0x20F: Reserved */
    __IO uint32_t RXIPV4_GD_FMS;               /* 0x210: Number of good IPv4 datagrams received with the TCP, UDP, or ICMP payload */
    __I  uint8_t  RESERVED7[492];              /* 0x214 - 0x3FF: Reserved */
    struct {
        __IO uint32_t L3_L4_CTRL;              /* 0x400: Layer 3 and Layer 4 Control Register */
        __IO uint32_t L4_ADDR;                 /* 0x404: Layer 4 Address Register */
        __I  uint8_t  RESERVED0[8];            /* 0x408 - 0x40F: Reserved */
        __IO uint32_t L3_ADDR_0;               /* 0x410: Layer 3 Address 0 Register */
        __IO uint32_t L3_ADDR_1;               /* 0x414: Layer 3 Address 1 Register */
        __IO uint32_t L3_ADDR_2;               /* 0x418: Layer 3 Address 2 Register */
        __IO uint32_t L3_ADDR_3;               /* 0x41C: Layer 3 Address 3 Register */
    } L3_L4_CFG[1];
    __I  uint8_t  RESERVED8[356];              /* 0x420 - 0x583: Reserved */
    __IO uint32_t VLAN_TAG_INC_RPL;            /* 0x584: VLAN Tag Inclusion or Replacement Register */
    __IO uint32_t VLAN_HASH;                   /* 0x588: VLAN Hash Table Register */
    __I  uint8_t  RESERVED9[372];              /* 0x58C - 0x6FF: Reserved */
    __IO uint32_t TS_CTRL;                     /* 0x700: Timestamp Control Register */
    __IO uint32_t SUB_SEC_INCR;                /* 0x704: Sub-Second Increment Register */
    __I  uint32_t SYST_SEC;                    /* 0x708: System Time - Seconds Register */
    __I  uint32_t SYST_NSEC;                   /* 0x70C: System Time - Nanoseconds Register */
    __IO uint32_t SYST_SEC_UPD;                /* 0x710: System Time - Seconds Update Register */
    __IO uint32_t SYST_NSEC_UPD;               /* 0x714: System Time - Nanoseconds Update Register */
    __IO uint32_t TS_ADDEND;                   /* 0x718: Timestamp Addend Register */
    __IO uint32_t TGTTM_SEC;                   /* 0x71C: Target Time Seconds Register */
    __IO uint32_t TGTTM_NSEC;                  /* 0x720: Target Time Nanoseconds Register */
    __IO uint32_t SYSTM_H_SEC;                 /* 0x724: System Time - Higher Word Seconds Register */
    __I  uint32_t TS_STATUS;                   /* 0x728: Timestamp Status Register */
    __IO uint32_t PPS_CTRL;                    /* 0x72C: PPS Control Register */
    __I  uint32_t AUX_TS_NSEC;                 /* 0x730: Auxiliary Timestamp - Nanoseconds Register */
    __I  uint32_t AUX_TS_SEC;                  /* 0x734: Auxiliary Timestamp - Seconds Register */
    __I  uint8_t  RESERVED10[40];              /* 0x738 - 0x75F: Reserved */
    __IO uint32_t PPS0_INTERVAL;               /* 0x760: PPS0 Interval Register */
    __IO uint32_t PPS0_WIDTH;                  /* 0x764: PPS0 Width Register */
    __I  uint8_t  RESERVED11[24];              /* 0x768 - 0x77F: Reserved */
    struct {
        __IO uint32_t TGTTM_SEC;               /* 0x780: PPS Target Time Seconds Register */
        __IO uint32_t TGTTM_NSEC;              /* 0x784: PPS Target Time Nanoseconds Register */
        __IO uint32_t INTERVAL;                /* 0x788: PPS Interval Register */
        __IO uint32_t WIDTH;                   /* 0x78C: PPS Width Register */
        __I  uint8_t  RESERVED0[16];           /* 0x790 - 0x79F: Reserved */
    } PPS[3];
    __I  uint8_t  RESERVED12[2080];            /* 0x7E0 - 0xFFF: Reserved */
    __IO uint32_t DMA_BUS_MODE;                /* 0x1000: Bus Mode Register */
    __IO uint32_t DMA_TX_POLL_DEMAND;          /* 0x1004: Transmit Poll Demand Register */
    __IO uint32_t DMA_RX_POLL_DEMAND;          /* 0x1008: Receive Poll Demand Register */
    __IO uint32_t DMA_RX_DESC_LIST_ADDR;       /* 0x100C: Receive Descriptor List Address Register */
    __IO uint32_t DMA_TX_DESC_LIST_ADDR;       /* 0x1010: Transmit Descriptor List Address Register */
    __IO uint32_t DMA_STATUS;                  /* 0x1014: Status Register */
    __IO uint32_t DMA_OP_MODE;                 /* 0x1018: Operation Mode Register */
    __IO uint32_t DMA_INTR_EN;                 /* 0x101C: Interrupt Enable Register */
    __IO uint32_t DMA_MISS_OVF_CNT;            /* 0x1020: Missed Frame And Buffer Overflow Counter Register */
    __IO uint32_t DMA_RX_INTR_WDOG;            /* 0x1024: Receive Interrupt Watchdog Timer Register */
    __IO uint32_t DMA_AXI_MODE;                /* 0x1028: AXI Bus Mode Register */
    __IO uint32_t DMA_BUS_STATUS;              /* 0x102C: AHB or AXI Status Register */
    __I  uint8_t  RESERVED13[24];              /* 0x1030 - 0x1047: Reserved */
    __IO uint32_t DMA_CURR_HOST_TX_DESC;       /* 0x1048: Current Host Transmit Descriptor Register */
    __IO uint32_t DMA_CURR_HOST_RX_DESC;       /* 0x104C: Current Host Receive Descriptor Register */
    __IO uint32_t DMA_CURR_HOST_TX_BUF;        /* 0x1050: Current Host Transmit Buffer Address Register */
    __IO uint32_t DMA_CURR_HOST_RX_BUF;        /* 0x1054: Current Host Receive Buffer Address Register */
    __I  uint8_t  RESERVED14[8104];            /* 0x1058 - 0x2FFF: Reserved */
    __IO uint32_t CTRL0;                       /* 0x3000: Control Register 0 */
    __I  uint8_t  RESERVED15[4];               /* 0x3004 - 0x3007: Reserved */
    __IO uint32_t CTRL2;                       /* 0x3008: Control  Register 1 */
    __I  uint8_t  RESERVED16[28];              /* 0x300C - 0x3027: Reserved */
} CHIP_REGS_ENET_T;

#define CHIP_REGS_ENET0           ((CHIP_REGS_ENET_T *)CHIP_MEMRGN_ADDR_ENET0)
#define CHIP_REGS_ENET1           ((CHIP_REGS_ENET_T *)CHIP_MEMRGN_ADDR_ENET1)

/*********** MACCFG - MAC Configuration Register  */
#define CHIP_REGFLD_ENET_MACCFG_SARC                    (0x00000007UL << 28U) /* (RW) Source address insertion or replacement control */
#define CHIP_REGFLD_ENET_MACCFG_TWOKPE                  (0x00000001UL << 27U) /* (RW) Support IEEE 802.3as 2K packets */
#define CHIP_REGFLD_ENET_MACCFG_SFTERR                  (0x00000001UL << 26U) /* (RW) SMII Force Transmit Error */
#define CHIP_REGFLD_ENET_MACCFG_CST                     (0x00000001UL << 25U) /* (RW) CRC stripping of type frames */
#define CHIP_REGFLD_ENET_MACCFG_TC                      (0x00000001UL << 24U) /* (RW) Transmit configuration in RGMII, SGMII or SMII */
#define CHIP_REGFLD_ENET_MACCFG_WD                      (0x00000001UL << 23U) /* (RW) Watchdog disabled */
#define CHIP_REGFLD_ENET_MACCFG_JD                      (0x00000001UL << 22U) /* (RW) Jabber disabled */
#define CHIP_REGFLD_ENET_MACCFG_BE                      (0x00000001UL << 21U) /* (RW) Frame burst enable */
#define CHIP_REGFLD_ENET_MACCFG_JE                      (0x00000001UL << 20U) /* (RW) Jumbo frames enabled */
#define CHIP_REGFLD_ENET_MACCFG_IFG                     (0x00000007UL << 17U) /* (RW) Interframe gap */
#define CHIP_REGFLD_ENET_MACCFG_DCRS                    (0x00000001UL << 16U) /* (RW) Disable carrier sense during transmission */
#define CHIP_REGFLD_ENET_MACCFG_PS                      (0x00000001UL << 15U) /* (RW) Port selection (1 - 1000 MBit/s, 0 - 100 or 10 (see FES)) */
#define CHIP_REGFLD_ENET_MACCFG_FES                     (0x00000001UL << 14U) /* (RW) Speed selection (1 - 100 MBit/s, 0 - 10 Mbit/s) */
#define CHIP_REGFLD_ENET_MACCFG_DO                      (0x00000001UL << 13U) /* (RW) Disable Receive Own in half-duplex mode */
#define CHIP_REGFLD_ENET_MACCFG_LM                      (0x00000001UL << 12U) /* (RW) Loopback mode */
#define CHIP_REGFLD_ENET_MACCFG_DM                      (0x00000001UL << 11U) /* (RW) Duplex mode */
#define CHIP_REGFLD_ENET_MACCFG_IPC                     (0x00000001UL << 10U) /* (RW) Checksum offload */
#define CHIP_REGFLD_ENET_MACCFG_DR                      (0x00000001UL <<  9U) /* (RW) Disable retries */
#define CHIP_REGFLD_ENET_MACCFG_LUD                     (0x00000001UL <<  8U) /* (RW) Link up or down */
#define CHIP_REGFLD_ENET_MACCFG_ACS                     (0x00000001UL <<  7U) /* (RW) Automatic pad or CRC stripping */
#define CHIP_REGFLD_ENET_MACCFG_BL                      (0x00000003UL <<  5U) /* (RW) Back-Off limit */
#define CHIP_REGFLD_ENET_MACCFG_DC                      (0x00000001UL <<  4U) /* (RW) Deferral check */
#define CHIP_REGFLD_ENET_MACCFG_TE                      (0x00000001UL <<  3U) /* (RW) Transmitter enabled */
#define CHIP_REGFLD_ENET_MACCFG_RE                      (0x00000001UL <<  2U) /* (RW) Receiver enabled */
#define CHIP_REGFLD_ENET_MACCFG_PRELEN                  (0x00000003UL <<  0U) /* (RW) The length of the preamble of the transmitted frame */
#define CHIP_REGMSK_ENET_MACCFG_RESERVED                (0x80000000UL)

#define CHIP_REGFLDVAL_ENET_MACCFG_SARC_DISABLED        0 /* The input signals mti_sa_ctrl_i and ati_sa_ctrl_i control the generation of the SA field. */
#define CHIP_REGFLDVAL_ENET_MACCFG_SARC_INSERT_ADDR0    2 /* Insert the source address from MAC Address 0. */
#define CHIP_REGFLDVAL_ENET_MACCFG_SARC_REPLACE_ADDR0   3 /* Replace the source address from MAC Address 0 */
#define CHIP_REGFLDVAL_ENET_MACCFG_SARC_INSERT_ADDR1    6 /* Insert the source address from MAC Address 1 */
#define CHIP_REGFLDVAL_ENET_MACCFG_SARC_REPLACE_ADDR1   7 /* Replace the source address from MAC Address 1 */

#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_96               0 /* 96-bit time */
#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_88               1 /* 88-bit time */
#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_80               2 /* 80-bit time */
#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_72               3 /* 72-bit time */
#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_64               4 /* 64-bit time */
#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_56               5 /* 56-bit time */
#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_48               6 /* 48-bit time */
#define CHIP_REGFLDVAL_ENET_MACCFG_IFG_40               7 /* 40-bit time */

#define CHIP_REGFLDVAL_ENET_MACCFG_BL_10                0 /* k= min (n, 10) */
#define CHIP_REGFLDVAL_ENET_MACCFG_BL_8                 1 /* k= min (n, 8) */
#define CHIP_REGFLDVAL_ENET_MACCFG_BL_4                 2 /* k= min (n, 4) */
#define CHIP_REGFLDVAL_ENET_MACCFG_BL_1                 3 /* k= min (n, 1) */

#define CHIP_REGFLDVAL_ENET_MACCFG_PRELEN_7             0 /* 7-byte preamble */
#define CHIP_REGFLDVAL_ENET_MACCFG_PRELEN_5             1 /* 5-byte preamble */
#define CHIP_REGFLDVAL_ENET_MACCFG_PRELEN_3             2 /* 3-byte preamble */


/*********** MACFF - MAC Frame Filter Register ********************************/
#define CHIP_REGFLD_ENET_MACFF_RA                       (0x00000001UL << 31U) /* (RW) Receive all */
#define CHIP_REGFLD_ENET_MACFF_DNTU                     (0x00000001UL << 21U) /* (RW) Drop non-TCP/UDP over IP frames */
#define CHIP_REGFLD_ENET_MACFF_IPFE                     (0x00000001UL << 20U) /* (RW) Layer 3 and 4 filters enabled */
#define CHIP_REGFLD_ENET_MACFF_VTFE                     (0x00000001UL << 15U) /* (RW) VLAN tag filter that enable */
#define CHIP_REGFLD_ENET_MACFF_HPF                      (0x00000001UL << 10U) /* (RW) Hash filter or perfect filter */
#define CHIP_REGFLD_ENET_MACFF_SAF                      (0x00000001UL <<  9U) /* (RW) Enable source address filter */
#define CHIP_REGFLD_ENET_MACFF_SAIF                     (0x00000001UL <<  8U) /* (RW) SA inverse filtering */
#define CHIP_REGFLD_ENET_MACFF_PCF                      (0x00000003UL <<  6U) /* (RW) Forward control frames */
#define CHIP_REGFLD_ENET_MACFF_DBF                      (0x00000001UL <<  5U) /* (RW) Disable broadcast frames */
#define CHIP_REGFLD_ENET_MACFF_PM                       (0x00000001UL <<  4U) /* (RW) Pass through all multicast frames */
#define CHIP_REGFLD_ENET_MACFF_DAIF                     (0x00000001UL <<  3U) /* (RW) Destination address inverse filtration */
#define CHIP_REGFLD_ENET_MACFF_HMC                      (0x00000001UL <<  2U) /* (RW) Hash multicast */
#define CHIP_REGFLD_ENET_MACFF_HUC                      (0x00000001UL <<  1U) /* (RW) Hash Unicast */
#define CHIP_REGFLD_ENET_MACFF_PR                       (0x00000001UL <<  0U) /* (RW) Promiscuous mode */
#define CHIP_REGMSK_ENET_MACFF_RESERVED                 (0x7FCF7800UL)


#define CHIP_REGFLDVAL_ENET_MACFF_PCF_FLT_ALL           0 /* MAC filters all control frames from reaching the application */
#define CHIP_REGFLDVAL_ENET_MACFF_PCF_FWD_ALL_NO_PAUSE  1 /* MAC forwards all control frames except Pause packets to application even if they fail the Address Filter */
#define CHIP_REGFLDVAL_ENET_MACFF_PCF_FWD_ALL           2 /* MAC forwards all control frames to application even if they fail the Address Filter */
#define CHIP_REGFLDVAL_ENET_MACFF_PCF_FWD_PASS          3 /* MAC forwards control frames that pass the Address Filter. */


/*********** GMII_ADDR - GMII Address Register ********************************/
#define CHIP_REGFLD_ENET_GMII_ADDR_PA                   (0x0000001FUL << 11U) /* (RW) Physical layer address */
#define CHIP_REGFLD_ENET_GMII_ADDR_GR                   (0x0000001FUL <<  6U) /* (RW) GMII register address */
#define CHIP_REGFLD_ENET_GMII_ADDR_CR                   (0x0000001FUL <<  2U) /* (RW) CSR clock range */
#define CHIP_REGFLD_ENET_GMII_ADDR_GW                   (0x00000001UL <<  1U) /* (RW) GMII write */
#define CHIP_REGFLD_ENET_GMII_ADDR_GB                   (0x00000001UL <<  0U) /* (RW1SC) GMII busy */
#define CHIP_REGMSK_ENET_GMII_ADDR_RESERVED             (0xFFFF0000UL)

#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV42          0 /* CSR clock is 60-100 MHz, MDC clock is CSR clock/42 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV62          1 /* CSR clock is 100-150 MHz, MDC clock is CSR clock/62 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV16          2 /* CSR clock is 20-35 MHz, MDC clock is CSR clock/16 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV26          3 /* CSR clock is 35-60 MHz, MDC clock is CSR clock/26 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV102         4 /* CSR clock is 150-250 MHz, MDC clock is CSR clock/102 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV124         5 /* CSR clock is 250-300 MHz, MDC clock is CSR clock/124 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV4AR         8 /* CSR clock/4 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV6AR         9 /* CSR clock/6 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV8AR         10 /* CSR clock/8 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV10AR        11 /* CSR clock/10 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV12AR        12 /* CSR clock/12 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV14AR        13 /* CSR clock/14 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV16AR        14 /* CSR clock/16 */
#define CHIP_REGFLDVAL_ENET_GMII_ADDR_CR_DIV18AR        15 /* CSR clock/18 */


/*********** GMII_DATA - GMII Data Register  **********************************/
#define CHIP_REGFLD_ENET_GMII_DATA_GD                   (0x0000FFFFUL <<  0U) /* (RW) GMII data */
#define CHIP_REGMSK_ENET_GMII_DATA_RESERVED             (0xFFFF0000UL)


/*********** FLOWCTRL - Flow Control Register ********************************/
#define CHIP_REGFLD_ENET_FLOWCTRL_PT                    (0x0000FFFFUL << 16U) /* (RW) Pause time */
#define CHIP_REGFLD_ENET_FLOWCTRL_DZPQ                  (0x00000001UL <<  7U) /* (RW) Disable zero quantum pause */
#define CHIP_REGFLD_ENET_FLOWCTRL_PLT                   (0x00000003UL <<  4U) /* (RW) Pause low threshold */
#define CHIP_REGFLD_ENET_FLOWCTRL_UP                    (0x00000001UL <<  3U) /* (RW) Unicast pause frame detection */
#define CHIP_REGFLD_ENET_FLOWCTRL_RFE                   (0x00000001UL <<  2U) /* (RW) Receive flow control enable */
#define CHIP_REGFLD_ENET_FLOWCTRL_TFE                   (0x00000001UL <<  1U) /* (RW) Transmit flow control enable */
#define CHIP_REGFLD_ENET_FLOWCTRL_FCB_BPA               (0x00000001UL <<  0U) /* (RW1SC) Flow control busy or backpressure activate */
#define CHIP_REGMSK_ENET_FLOWCTRL_RESERVED              (0x0000FF40UL)

#define CHIP_REGFLDVAL_ENET_FLOWCTRL_PLT_4              0 /* Threshold is pause time minus 4 timeslots (PT – 4 timeslots) */
#define CHIP_REGFLDVAL_ENET_FLOWCTRL_PLT_28             1 /* Threshold is pause time minus 28 timeslots (PT – 28 timeslots) */
#define CHIP_REGFLDVAL_ENET_FLOWCTRL_PLT_144            2 /* Threshold is pause time minus 144 timeslots (PT – 144 timeslots) */
#define CHIP_REGFLDVAL_ENET_FLOWCTRL_PLT_256            3 /* Threshold is pause time minus 256 timeslots (PT – 256 timeslots) */


/*********** VLAN_TAG - VLAN Tag Register *************************************/
#define CHIP_REGFLD_ENET_VLAN_TAG_VTHM                  (0x00000001UL << 19U) /* (RW) VLAN tag hash table matching enable */
#define CHIP_REGFLD_ENET_VLAN_TAG_ESVL                  (0x00000001UL << 18U) /* (RW) Enable S-VLAN */
#define CHIP_REGFLD_ENET_VLAN_TAG_VTIM                  (0x00000001UL << 17U) /* (RW) Inverse VLAN tag matching enable */
#define CHIP_REGFLD_ENET_VLAN_TAG_ETV                   (0x00000001UL << 16U) /* (RW) Enable 12-bit VLAN tag comparison */
#define CHIP_REGFLD_ENET_VLAN_TAG_VL                    (0x0000FFFFUL <<  0U) /* (RW) VLAN tag identifier of received frame */
#define CHIP_REGMSK_ENET_VLAN_TAG_RESERVED              (0xFFF00000UL)

/*********** PMT_CSR - PMT Control and Status Register ************************/
#define CHIP_REGFLD_ENET_PMT_CSR_RWKFILTRST             (0x00000001UL << 31U) /* (RW1SC) Remote wakeup frame filter register pointer reset */
#define CHIP_REGFLD_ENET_PMT_CSR_RWKPTR                 (0x0000001FUL << 24U) /* (RW)   Remote wake-up FIFO pointer */
#define CHIP_REGFLD_ENET_PMT_CSR_GLBLUCAST              (0x00000001UL <<  9U) /* (RW)   Global unicast */
#define CHIP_REGFLD_ENET_PMT_CSR_RWKPRCVD               (0x00000001UL <<  6U) /* (RWRC) Remote wakeup frame received */
#define CHIP_REGFLD_ENET_PMT_CSR_MGKPRCVD               (0x00000001UL <<  5U) /* (RWRC) Wake-up magic packet received */
#define CHIP_REGFLD_ENET_PMT_CSR_RWKPKTEN               (0x00000001UL <<  2U) /* (RW)   Remote wake frame enabled */
#define CHIP_REGFLD_ENET_PMT_CSR_MGKPKTEN               (0x00000001UL <<  1U) /* (RW)   Wakeup magic packet enabled */
#define CHIP_REGFLD_ENET_PMT_CSR_PWRDWN                 (0x00000001UL <<  0U) /* (RWSC) Power Down */
#define CHIP_REGMSK_ENET_PMT_CSR_RESERVED               (0x60FFFD98UL)

/*********** LPI_CSR - LPI Control and Status Register ************************/
#define CHIP_REGFLD_ENET_LPI_CSR_LPITXA                 (0x00000001UL << 19U) /* (RW)  LPI TX automate */
#define CHIP_REGFLD_ENET_LPI_CSR_PLSEN                  (0x00000001UL << 18U) /* (RW)  Physical layer link status enable */
#define CHIP_REGFLD_ENET_LPI_CSR_PLS                    (0x00000001UL << 17U) /* (RO?) Physical layer link status */
#define CHIP_REGFLD_ENET_LPI_CSR_LPIEN                  (0x00000001UL << 16U) /* (RW)  LPI enable */
#define CHIP_REGFLD_ENET_LPI_CSR_RLPIST                 (0x00000001UL <<  9U) /* (RO?) Receive LPI status */
#define CHIP_REGFLD_ENET_LPI_CSR_TLPIST                 (0x00000001UL <<  8U) /* (RO?) Transmit LPI status */
#define CHIP_REGFLD_ENET_LPI_CSR_RLPIEX                 (0x00000001UL <<  3U) /* (RC)  Receive LPI exit */
#define CHIP_REGFLD_ENET_LPI_CSR_RLPIEN                 (0x00000001UL <<  2U) /* (RC)  Receive LPI entry */
#define CHIP_REGFLD_ENET_LPI_CSR_TLPIEX                 (0x00000001UL <<  1U) /* (RC)  Transmit LPI exit */
#define CHIP_REGFLD_ENET_LPI_CSR_TLPIEN                 (0x00000001UL <<  0U) /* (RC)  Transmit LPI entry */
#define CHIP_REGMSK_ENET_LPI_CSR_RESERVED               (0xFFF0FCF0UL)

/*********** LPI_TCR - LPI Timers Control Register ****************************/
#define CHIP_REGFLD_ENET_LPI_TCR_LST                    (0x000003FFUL << 16U) /* (RW)  LPI LS timer - min time (ms) link up before LPI pattern is transmitted to the PHY, IEEE = 1000 */
#define CHIP_REGFLD_ENET_LPI_TCR_TWT                    (0x0000FFFFUL <<  0U) /* (RW)  LPI TW timer - min time (ms) to resume normal tx after LPI pattern is transmitted to the PHY */
#define CHIP_REGMSK_ENET_LPI_TCR_RESERVED               (0xFC000000UL)




/*------------------------ Descriptors ---------------------------------------*/
/*********** TDES0 ************************************************************/
#define CHIP_ENET_TDES0_OWN                 (0x00000001UL << 31U) /* Owned by the DMA */
#define CHIP_ENET_TDES0_IC                  (0x00000001UL << 30U) /* Interrupt on Completion */
#define CHIP_ENET_TDES0_LS                  (0x00000001UL << 29U) /* Last Segment */
#define CHIP_ENET_TDES0_FS                  (0x00000001UL << 28U) /* First Segment */
#define CHIP_ENET_TDES0_DC                  (0x00000001UL << 27U) /* Disable CRC */
#define CHIP_ENET_TDES0_DP                  (0x00000001UL << 26U) /* Disable Pad */
#define CHIP_ENET_TDES0_TTSE                (0x00000001UL << 25U) /* Transmit Timestamp Enabled */
#define CHIP_ENET_TDES0_CRCR                (0x00000001UL << 24U) /* CRC Replacement Control */
#define CHIP_ENET_TDES0_CIC                 (0x00000003UL << 22U) /* Checksum Insertion Control */
#define CHIP_ENET_TDES0_TER                 (0x00000001UL << 21U) /* Transmit End of Ring */
#define CHIP_ENET_TDES0_TCH                 (0x00000001UL << 20U) /* Second Address Chained */
#define CHIP_ENET_TDES0_VLIC                (0x00000003UL << 18U) /* VLAN Insertion Control */
#define CHIP_ENET_TDES0_TTSS                (0x00000001UL << 17U) /* Transmit Timestamp Status */
#define CHIP_ENET_TDES0_IHE                 (0x00000001UL << 16U) /* IP Header Error */
#define CHIP_ENET_TDES0_ES                  (0x00000001UL << 15U) /* Error Summary */
#define CHIP_ENET_TDES0_JT                  (0x00000001UL << 14U) /* Jabber Timeout */
#define CHIP_ENET_TDES0_FF                  (0x00000001UL << 13U) /* Frame Flushed */
#define CHIP_ENET_TDES0_IPE                 (0x00000001UL << 12U) /* IP Payload Error */
#define CHIP_ENET_TDES0_LOC                 (0x00000001UL << 11U) /* Loss of Carrier */
#define CHIP_ENET_TDES0_NC                  (0x00000001UL << 10U) /* No Carrier */
#define CHIP_ENET_TDES0_LC                  (0x00000001UL <<  9U) /* Late Collision */
#define CHIP_ENET_TDES0_VF                  (0x00000001UL <<  8U) /* VLAN Frame */
#define CHIP_ENET_TDES0_CC                  (0x0000000FUL <<  3U) /* Collision Count (Status field) */
#define CHIP_ENET_TDES0_ED                  (0x00000001UL <<  2U) /* Excessive Deferral */
#define CHIP_ENET_TDES0_UF                  (0x00000001UL <<  1U) /* Underflow Error */
#define CHIP_ENET_TDES0_DB                  (0x00000001UL <<  0U) /* Deferred Bit */

#define CHIP_ENET_TDES0_CIC_DISABLED        0 /* Checksum Insertion Disabled. */
#define CHIP_ENET_TDES0_CIC_IPHDR           1 /* Only IP header checksum calculation and insertion are enabled. */
#define CHIP_ENET_TDES0_CIC_IPHDR_DATA      2 /* IP header checksum and payload checksum calculation and insertion are enabled, but pseudoheader checksum is not calculated in hardware. */
#define CHIP_ENET_TDES0_CIC_IP_FULL         3 /* IP Header checksum and payload checksum calculation and insertion are enabled, and pseudoheader checksum is calculated in hardware. */

#define CHIP_ENET_TDES0_VLIC_DISABLED       0 /* Do not add a VLAN tag. */
#define CHIP_ENET_TDES0_VLIC_REMOVE         1 /* Remove the VLAN tag from the frames before transmission */
#define CHIP_ENET_TDES0_VLIC_INSERT         2 /* Insert a VLAN tag with the tag value from VLAN Tag Inclusion or Replacement Register */
#define CHIP_ENET_TDES0_VLIC_REPLACE        3 /* Replace the VLAN tag in frames with the Tag value programmed in Register VLAN_TAG_INC_RPL (offset 0x584) (VLAN Tag Inclusion or Replacement Register) */

/*********** TDES1 ************************************************************/
#define CHIP_ENET_TDES1_SAIC                (0x00000007UL << 29U) /* SA Insertion Control */
#define CHIP_ENET_TDES1_TBS2                (0x00001FFFUL << 16U) /* Transmit Buffer 2 Size */
#define CHIP_ENET_TDES1_TBS1                (0x00001FFFUL <<  0U) /* Transmit Buffer 1 Size */

#define CHIP_ENET_TDES1_SAIC_DISABLED       0 /* Do not include the source address. */
#define CHIP_ENET_TDES1_SAIC_INSERT_ADDR0   1 /* Insert the source address from MAC Address 0. */
#define CHIP_ENET_TDES1_SAIC_REPLACE_ADDR0  2 /* Replace the source address from MAC Address 0 */
#define CHIP_ENET_TDES1_SAIC_INSERT_ADDR1   5 /* Insert the source address from MAC Address 1 */
#define CHIP_ENET_TDES1_SAIC_REPLACE_ADDR1  6 /* Replace the source address from MAC Address 1 */

/*********** TDES2-TDES7 ******************************************************/
#define CHIP_ENET_TDES2_BUF1AP              (0xFFFFFFFFUL <<  0U)   /* Buffer 1 Address Pointer */
#define CHIP_ENET_TDES3_BUF2AP              (0xFFFFFFFFUL <<  0U)   /* Buffer 2 Address Pointer (Next Descriptor Address) */
#define CHIP_ENET_TDES6_TTSL                (0xFFFFFFFFUL <<  0U)   /* Transmit Frame Timestamp Low */
#define CHIP_ENET_TDES7_TTSH                (0xFFFFFFFFUL <<  0U)   /* Transmit Frame Timestamp High */


/*********** RDES0 ************************************************************/
#define CHIP_ENET_RDES0_OWN                 (0x00000001UL << 31U) /* Owned by the DMA */
#define CHIP_ENET_RDES0_AFM                 (0x00000001UL << 30U) /* Destination Address Filter Fail */
#define CHIP_ENET_RDES0_FL                  (0x00003FFFUL << 16U) /* Frame Length */
#define CHIP_ENET_RDES0_ES                  (0x00000001UL << 15U) /* Error Summary */
#define CHIP_ENET_RDES0_DE                  (0x00000001UL << 14U) /* Descriptor Error */
#define CHIP_ENET_RDES0_SAF                 (0x00000001UL << 13U) /* Source Address Filter Fail */
#define CHIP_ENET_RDES0_LE                  (0x00000001UL << 12U) /* Length Error */
#define CHIP_ENET_RDES0_OE                  (0x00000001UL << 11U) /* Overflow Error */
#define CHIP_ENET_RDES0_VLAN                (0x00000001UL << 10U) /* VLAN Tag */
#define CHIP_ENET_RDES0_FS                  (0x00000001UL <<  9U) /* First Descriptor */
#define CHIP_ENET_RDES0_LS                  (0x00000001UL <<  8U) /* Last Descriptor */
#define CHIP_ENET_RDES0_TSA                 (0x00000001UL <<  7U) /* Timestamp Available (when Advanced Timestamp feature is present) */
#define CHIP_ENET_RDES0_IPCE                (0x00000001UL <<  7U) /* IP Checksum Error (Type1) (when Advanced Timestamp feature is present) */
#define CHIP_ENET_RDES0_GF                  (0x00000001UL <<  7U) /* Giant Frame */
#define CHIP_ENET_RDES0_LC                  (0x00000001UL <<  6U) /* Late Collision */
#define CHIP_ENET_RDES0_FT                  (0x00000001UL <<  5U) /* Frame Type (1 - Ethernet-type frame) */
#define CHIP_ENET_RDES0_RWT                 (0x00000001UL <<  4U) /* Receive Watchdog Timeout */
#define CHIP_ENET_RDES0_RE                  (0x00000001UL <<  3U) /* Receive Error */
#define CHIP_ENET_RDES0_DBE                 (0x00000001UL <<  2U) /* Dribble Bit Error */
#define CHIP_ENET_RDES0_CE                  (0x00000001UL <<  1U) /* CRC Error */
#define CHIP_ENET_RDES0_ESA                 (0x00000001UL <<  0U) /* Extended Status Available */
#define CHIP_ENET_RDES0_RXMA                (0x00000001UL <<  0U) /* Rx MAC Address */


/*********** RDES1 ************************************************************/
#define CHIP_ENET_RDES1_DIC                 (0x00000001UL << 31U) /* Disable Interrupt on Completion */
#define CHIP_ENET_RDES1_RBS2                (0x00001FFFUL << 16U) /* Receive Buffer 2 Size */
#define CHIP_ENET_RDES1_RER                 (0x00000001UL << 15U) /* Receive End of Ring */
#define CHIP_ENET_RDES1_RCH                 (0x00000001UL << 14U) /* Second Address Chained */
#define CHIP_ENET_RDES1_RBS1                (0x00001FFFUL <<  0U) /* Receive Buffer 1 Size */

#define CHIP_ENET_RDES2_BUF1AP              (0xFFFFFFFFUL <<  0U) /* Buffer 1 Address Pointer */
#define CHIP_ENET_RDES3_BUF2AP              (0xFFFFFFFFUL <<  0U) /* Buffer 2 Address Pointer (Next Descriptor Address) */

/*********** RDES4 ************************************************************/
#define CHIP_ENET_RDES4_L3L4FN              (0x00000003UL << 26U) /* Layer 3 and Layer 4 Filter Number Matched */
#define CHIP_ENET_RDES4_L4FM                (0x00000001UL << 25U) /* Layer 4 Filter Match */
#define CHIP_ENET_RDES4_L3FM                (0x00000001UL << 24U) /* Layer 3 Filter Match */
#define CHIP_ENET_RDES4_TD                  (0x00000001UL << 14U) /* Timestamp dropped */
#define CHIP_ENET_RDES4_PV                  (0x00000001UL << 13U) /* PTP Version (1 - v2, 0 - v1) */
#define CHIP_ENET_RDES4_PFT                 (0x00000001UL << 12U) /* PTP Packet Type */
#define CHIP_ENET_RDES4_PMT                 (0x0000000FUL <<  8U) /* PTP Message Type */
#define CHIP_ENET_RDES4_IPV6                (0x00000001UL <<  7U) /* IPv6 Packet Received */
#define CHIP_ENET_RDES4_IPV4                (0x00000001UL <<  6U) /* IPv4 Packet Received */
#define CHIP_ENET_RDES4_IPCB                (0x00000001UL <<  5U) /* IP Checksum Bypassed */
#define CHIP_ENET_RDES4_IPPE                (0x00000001UL <<  4U) /* IP Payload Error */
#define CHIP_ENET_RDES4_IPHE                (0x00000001UL <<  3U) /* IP Header Error */
#define CHIP_ENET_RDES4_PT                  (0x00000007UL <<  0U) /* IP Payload Type */

#define CHIP_ENET_RDES4_PMT_NOMSG           0 /* No PTP message received */
#define CHIP_ENET_RDES4_PMT_SYNC            1 /* SYNC (all clock types) */
#define CHIP_ENET_RDES4_PMT_FUP             2 /* Follow_Up (all clock types) */
#define CHIP_ENET_RDES4_PMT_DEL_REQ         3 /* Delay_Req (all clock types) */
#define CHIP_ENET_RDES4_PMT_DEL_RESP        4 /* Delay_Resp (all clock types) */
#define CHIP_ENET_RDES4_PMT_PDEL_REQ        5 /* Pdelay_Req (in peer-to-peer transparent clock) */
#define CHIP_ENET_RDES4_PMT_PDEL_RESP       6 /* Pdelay_Resp (in peer-to-peer transparent clock) */
#define CHIP_ENET_RDES4_PMT_PDEL_RESP_FU    7 /* Pdelay_Resp_Follow_Up (in peer-to-peer transparent clock) */
#define CHIP_ENET_RDES4_PMT_ANNOUNCE        8 /* Announce */
#define CHIP_ENET_RDES4_PMT_MNGMNT          9 /* Management */
#define CHIP_ENET_RDES4_PMT_SIGNALING       10 /* Signaling */

#define CHIP_ENET_RDES4_PT_UNKNOWN          0 /* Unknown or did not process IP payload */
#define CHIP_ENET_RDES4_PT_UDP              1
#define CHIP_ENET_RDES4_PT_TCP              2
#define CHIP_ENET_RDES4_PT_ICMP             3

#define CHIP_ENET_RDES6_RTSL                (0xFFFFFFFFUL <<  0U) /* Receive Frame Timestamp Low */
#define CHIP_ENET_RDES7_RTSH                (0xFFFFFFFFUL <<  0U) /* Receive Frame Timestamp High */


#endif // REGS_ENET_H



