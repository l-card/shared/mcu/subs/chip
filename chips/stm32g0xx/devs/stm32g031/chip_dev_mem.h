#ifndef CHIP_DEV_MEM_H
#define CHIP_DEV_MEM_H

#include "chip_config.h"

#if CHIP_CFG_MEM_SUFFIX == 4
    #define CHIP_FLASH_MEM_SIZE      (16*1024)
#elif CHIP_CFG_MEM_SUFFIX == 6
    #define CHIP_FLASH_MEM_SIZE      (32*1024)
#elif CHIP_CFG_MEM_SUFFIX == 8
    #define CHIP_FLASH_MEM_SIZE      (64*1024)
#else
    #error invalid device memory suffix is specified
#endif

#define CHIP_RAM_MEM_SIZE            (8 * 1024)

#endif // CHIP_DEV_MEM_H
