#ifndef CHIP_ARMV7EM_REGS_CORE_FEATURES_H
#define CHIP_ARMV7EM_REGS_CORE_FEATURES_H

#include "stdint.h"

typedef struct {
    __IM  uint32_t ID_PFR[2U];             /* Offset: 0x040 (R/ )  Processor Feature Register */
    __IM  uint32_t ID_DFR;                 /* Offset: 0x048 (R/ )  Debug Feature Register */
    __IM  uint32_t ID_AFR;                 /* Offset: 0x04C (R/ )  Auxiliary Feature Register */
    __IM  uint32_t ID_MFR[4U];             /* Offset: 0x050 (R/ )  Memory Model Feature Register */
    __IM  uint32_t ID_ISAR[5U];            /* Offset: 0x060 (R/ )  Instruction Set Attributes Register */
    uint32_t RESERVED0[1U];
    __IM  uint32_t CLIDR;                  /* Offset: 0x078 (R/ )  Cache Level ID register */
    __IM  uint32_t CTR;                    /* Offset: 0x07C (R/ )  Cache Type register */
    __IM  uint32_t CCSIDR;                 /* Offset: 0x080 (R/ )  Cache Size ID Register */
    __IOM uint32_t CSSELR;                 /* Offset: 0x084 (R/W)  Cache Size Selection Register */
} CHIP_REGS_ARMV7EM_ARM_FEATURES_T;


#define CHIP_REGS_ARM_FEATURES                  ((CHIP_REGS_ARMV7EM_ARM_FEATURES_T *) CHIP_MEMRGN_ADDR_ARMV7EM_ARM_FEATURES)

/*********** Cache level ID register (ARM_FEATURES_CLIDR) *********************/
#define CHIP_REGFLD_ARM_FEATURES_CL_ICACHE(lvl) (0x00000001UL << (3U * ((lvl)-1) + 0U)) /* An instruction cache is implemented on level l (1..7) */
#define CHIP_REGFLD_ARM_FEATURES_CL_DCACHE(lvl) (0x00000001UL << (3U * ((lvl)-1) + 1U)) /* Data cache is implemented on level l (1..7) */
#define CHIP_REGFLD_ARM_FEATURES_CL_UCACHE(lvl) (0x00000001UL << (3U * ((lvl)-1) + 2U)) /* Unified cache is implemented on level l (1..7) */
#define CHIP_REGFLD_ARM_FEATURES_CLIDR_LOUIS    (0x00000007UL << 21U) /* RAZ */
#define CHIP_REGFLD_ARM_FEATURES_CLIDR_LOC      (0x00000007UL << 24U) /* (RO) Level of Coherency */
#define CHIP_REGFLD_ARM_FEATURES_CLIDR_LOUU     (0x00000007UL << 27U) /* (RO) Level of Unification */

/*********** Cache type register (ARM_FEATURES_CTR) ******************************/
#define CHIP_REGFLD_ARM_FEATURES_CTR_IMINLINE   (0x0000000FUL <<  0U) /* (RO) Smallest cache line of all the instruction caches under the control of the processor. */
#define CHIP_REGFLD_ARM_FEATURES_CTR_DMINLINE   (0x0000000FUL << 16U) /* (RO) Smallest cache line of all the data and unified caches under the core control */
#define CHIP_REGFLD_ARM_FEATURES_CTR_EGR        (0x0000000FUL << 20U) /* (RO) Exclusives Reservation Granule */
#define CHIP_REGFLD_ARM_FEATURES_CTR_CWG        (0x0000000FUL << 24U) /* (RO) Cache Writeback Granule */
#define CHIP_REGFLD_ARM_FEATURES_CTR_FORMAT     (0x00000007UL << 29U) /* (RO) Register format */

#define CHIP_REGFLDVAL_ARM_FEATURES_CTR_FORMAT_ARMV7    0x4

/*********** Cache size ID register (ARM_FEATURES_CCSIDR) ************************/
#define CHIP_REGFLD_ARM_FEATURES_CCSIDR_LINESIZE       (0x00000007UL <<  0U) /* (RO) Indicates the number of words in each cache line (encoded as 2 less than log(2) of the number of words in the cache line) */
#define CHIP_REGFLD_ARM_FEATURES_CCSIDR_ASSOCIATIVITY  (0x000003FFUL <<  3U) /* (RO) Indicates the number of ways as (number of ways) - 1 */
#define CHIP_REGFLD_ARM_FEATURES_CCSIDR_NUMSETS        (0x00007FFFUL << 13U) /* (RO) Indicates the number of sets as (number of sets) - 1 */
#define CHIP_REGFLD_ARM_FEATURES_CCSIDR_WA             (0x00000001UL << 28U) /* (RO) Indicates support available for write allocation */
#define CHIP_REGFLD_ARM_FEATURES_CCSIDR_RA             (0x00000001UL << 29U) /* (RO) Indicates support available for read allocation */
#define CHIP_REGFLD_ARM_FEATURES_CCSIDR_WB             (0x00000001UL << 30U) /* (RO) Indicates support available for Write-Back */
#define CHIP_REGFLD_ARM_FEATURES_CCSIDR_WT             (0x00000001UL << 31U) /* (RO) Indicates support available for Write-Through */

/*********** Cache size selection register (ARM_FEATURES_CSSELR) *****************/
#define CHIP_REGFLD_ARM_FEATURES_CSSELR_IND      (0x00000001UL <<  0U) /* Enables selection of instruction or data cache */
#define CHIP_REGFLD_ARM_FEATURES_CSSELR_LEVEL    (0x00000007UL <<  1U) /* Identifies the cache level selected */

#define CHIP_REGFLDVAL_ARM_FEATURES_CSSELR_DCACHE  0
#define CHIP_REGFLDVAL_ARM_FEATURES_CSSELR_ICACHE  1



#endif // CHIP_ARMV7EM_REGS_CORE_FEATURES_H
