#ifndef CHIP_ARMV7EM_REGS_CORE_FPU_H
#define CHIP_ARMV7EM_REGS_CORE_FPU_H

#include <stdint.h>

typedef struct {
    /* регистр CPACR вне региона FPU -> отсчитываем от начала SCB */
    uint32_t RESERVED0_0[0x88/4];
    __IOM uint32_t CPACR;                  /* Offset: 0x088 (R/W)  Coprocessor Access Control Register */
    uint32_t RESERVED0_1[(CHIP_MEMRGN_ADDR_ARMV7EM_FPU - (CHIP_MEMRGN_ADDR_ARMV7EM_SCB + 0x88))/4 - 1];
    uint32_t RESERVED0[1U];
    __IOM uint32_t FPCCR;                  /* Offset: 0x004 (R/W)  Floating-Point Context Control Register */
    __IOM uint32_t FPCAR;                  /* Offset: 0x008 (R/W)  Floating-Point Context Address Register */
    __IOM uint32_t FPDSCR;                 /* Offset: 0x00C (R/W)  Floating-Point Default Status Control Register */
    __IM  uint32_t MVFR0;                  /* Offset: 0x010 (R/ )  Media and FP Feature Register 0 */
    __IM  uint32_t MVFR1;                  /* Offset: 0x014 (R/ )  Media and FP Feature Register 1 */
    __IM  uint32_t MVFR2;                  /* Offset: 0x018 (R/ )  Media and FP Feature Register 2 */
} CHIP_REGS_ARMV7EM_FPU_T;

#define CHIP_REGS_FPU                           ((CHIP_REGS_ARMV7EM_FPU_T *) CHIP_MEMRGN_ADDR_ARMV7EM_SCB)

/* фиксированные номера сопроцессоров */
#define CHIP_CP_NUM_VFP1                        10
#define CHIP_CP_NUM_VFP2                        11
#define CHIP_CP_NUM_DBG_ETM                     14
#define CHIP_CP_NUM_SYSCTL                      15


/*********** Coprocessor access control register (FPU_CPACR) ******************/
#define CHIP_REGFLD_MPU_TYPE_SEPARATE           (0x00000001UL <<  0U) /* Indicates support for unified or separate instruction and date memory maps */
#define CHIP_REGFLD_FPU_CPACR_CP(n)             (0x00000003UL << (2U * (n))) /* Access privileges for coprocessor n = 10 */

#define CHIP_REGFLDVAL_FPU_CPACR_CP_NOACC       0 /* Access denied. Any attempted access generates a NOCP UsageFault. */
#define CHIP_REGFLDVAL_FPU_CPACR_CP_PRIVLG      1 /* Privileged access only. An unprivileged access generates a NOCP fault. */
#define CHIP_REGFLDVAL_FPU_CPACR_CP_FULL        3 /* Full access */

/*********** Floating-point context control register (FPU_CPACR) **************/
#define CHIP_REGFLD_FPU_FPCCR_LSPACT            (0x00000001UL <<  0U) /* Lazy state preservation */
#define CHIP_REGFLD_FPU_FPCCR_USER              (0x00000001UL <<  1U) /* Privilege level was user when the floating-point stack frame was allocated */
#define CHIP_REGFLD_FPU_FPCCR_THREAD            (0x00000001UL <<  3U) /* Mode was Thread Mode when the floating-point stack frame was allocated */
#define CHIP_REGFLD_FPU_FPCCR_HFRDY             (0x00000001UL <<  4U) /* Priority permitted setting the HardFault handler to the pending state when the floating-point stack frame was allocated */
#define CHIP_REGFLD_FPU_FPCCR_MMRDY             (0x00000001UL <<  5U) /* MemManage is enabled and priority permitted setting the MemManage handler to the pending state when the floating-point stack frame was allocated*/
#define CHIP_REGFLD_FPU_FPCCR_BFRDY             (0x00000001UL <<  6U) /* BusFault is enabled and priority permitted setting the BusFault handler to the pending state when the floating-point stack frame was allocated */
#define CHIP_REGFLD_FPU_FPCCR_MONRDY            (0x00000001UL <<  8U) /* DebugMonitor is enabled and priority permits setting MON_PEND when the floating-point stack frame was allocated */
#define CHIP_REGFLD_FPU_FPCCR_LSPEN             (0x00000001UL << 30U) /* Enable automatic lazy state preservation for floating-point context */
#define CHIP_REGFLD_FPU_FPCCR_ASPEN             (0x00000001UL << 31U) /* Enable CONTROL.FPCA setting on execution of a floating-point instruction */
#define CHIP_REGMSK_FPU_FPCCR_RESERVED          (0x3FFFFE00UL)

/*********** Floating-point context address register (FPU_FPCAR) **************/
#define CHIP_REGFLD_FPU_FPCAR_ADDRESS           (0x1FFFFFFFUL <<  3U)  /* The location of the unpopulated floating-point register space allocated on an exception stack frame */

/*********** Floating-point status control register (FPU_FPSCR) (indirect access) */
#define CHIP_REGFLD_FPU_FPSCR_IOC               (0x00000001UL <<  0U) /* Invalid Operation cumulative exception bit. */
#define CHIP_REGFLD_FPU_FPSCR_DZC               (0x00000001UL <<  1U) /* Division by Zero cumulative exception bit. */
#define CHIP_REGFLD_FPU_FPSCR_OFC               (0x00000001UL <<  2U) /* Overflow cumulative exception bit. */
#define CHIP_REGFLD_FPU_FPSCR_UFC               (0x00000001UL <<  3U) /* Underflow cumulative exception bit. */
#define CHIP_REGFLD_FPU_FPSCR_IXC               (0x00000001UL <<  4U) /* Inexact cumulative exception bit. */
#define CHIP_REGFLD_FPU_FPSCR_IDC               (0x00000001UL <<  7U) /* Input Denormal cumulative exception bit. */
#define CHIP_REGFLD_FPU_FPSCR_RMODE             (0x00000003UL << 22U) /* Rounding Mode control field. */
#define CHIP_REGFLD_FPU_FPSCR_FZ                (0x00000001UL << 24U) /* Flush-to-zero mode control bit. */
#define CHIP_REGFLD_FPU_FPSCR_DN                (0x00000001UL << 25U) /* Default NaN mode control bit. */
#define CHIP_REGFLD_FPU_FPSCR_AHP               (0x00000001UL << 26U) /* Alternative half-precision control bit */
#define CHIP_REGFLD_FPU_FPSCR_V                 (0x00000001UL << 28U) /* Overflow condition code flag */
#define CHIP_REGFLD_FPU_FPSCR_C                 (0x00000001UL << 29U) /* Carry condition code flag */
#define CHIP_REGFLD_FPU_FPSCR_Z                 (0x00000001UL << 30U) /* Zero condition code flag */
#define CHIP_REGFLD_FPU_FPSCR_N                 (0x00000001UL << 31U) /* Negative condition code flag */

/*********** Floating-point default status control register (FPU_FPDSCR) ******/
#define CHIP_REGFLD_FPU_FPDSCR_RMODE            (0x00000003UL << 22U) /* Default FPSCR_RMODE */
#define CHIP_REGFLD_FPU_FPDSCR_FZ               (0x00000001UL << 24U) /* Default FPSCR_FZ */
#define CHIP_REGFLD_FPU_FPDSCR_DN               (0x00000001UL << 25U) /* Default FPSCR_DN */
#define CHIP_REGFLD_FPU_FPDSCR_AHP              (0x00000001UL << 26U) /* Default FPSCR_AHP */


#define CHIP_REGFLDVAL_FPU_FPDSCR_RMODE_NEAREST 0 /* Round to Nearest (RN) mode. */
#define CHIP_REGFLDVAL_FPU_FPDSCR_RMODE_PLUS    1 /* Round towards Plus Infinity (RP) mode */
#define CHIP_REGFLDVAL_FPU_FPDSCR_RMODE_MINUS   2 /* Round towards Minus Infinity (RM) mode */
#define CHIP_REGFLDVAL_FPU_FPDSCR_RMODE_ZERO    3 /* Round towards Zero (RZ) mode */


/***********  Media and FP Feature Register 0 Definitions (FPU_MVFR0) *********/
#define CHIP_REGFLD_FPU_MVFR0_FP_RMODES         (0x0000000FUL << 28U) /* FP rounding modes */
#define CHIP_REGFLD_FPU_MVFR0_FP_SHORT_VECTORS  (0x0000000FUL << 24U) /* FP Short vectors */
#define CHIP_REGFLD_FPU_MVFR0_SQUARE_ROOT       (0x0000000FUL << 20U) /* Square root */
#define CHIP_REGFLD_FPU_MVFR0_DIVIDE            (0x0000000FUL << 16U) /* Divide */
#define CHIP_REGFLD_FPU_MVFR0_FP_EXC_TRAP       (0x0000000FUL << 12U) /* FP exception trapping */
#define CHIP_REGFLD_FPU_MVFR0_DBL_PREC          (0x0000000FUL <<  8U) /* Double-precision */
#define CHIP_REGFLD_FPU_MVFR0_SGL_PREC          (0x0000000FUL <<  4U) /* Single-precision */
#define CHIP_REGFLD_FPU_MVFR0_A_SIMD_REGS       (0x0000000FUL <<  0U) /* A_SIMD registers*/

/***********  Media and FP Feature Register 1 Definitions (FPU_MVFR1) *********/
#define CHIP_REGFLD_FPU_MVFR1_FP_FUSED_MAC      (0x0000000FUL << 28U) /* FP fused MAC */
#define CHIP_REGFLD_FPU_MVFR1_FP_HPFP           (0x0000000FUL << 24U) /* FP HPFP */
#define CHIP_REGFLD_FPU_MVFR1_D_NAN_MODE        (0x0000000FUL <<  4U) /* D_NaN mode */
#define CHIP_REGFLD_FPU_MVFR1_FTZ_MODE          (0x0000000FUL <<  0U) /* FtZ mode */

/***********  Media and FP Feature Register 2 Definitions (FPU_MVFR2) *********/
#define CHIP_REGFLD_FPU_MVFR2_VFP_MISC          (0x0000000FUL <<  4U) /* VFP Misc */

#endif // REGS_CORE_FPU_H


