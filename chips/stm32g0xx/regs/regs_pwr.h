#ifndef CHIP_STM32G0XX_REGS_PWR_H
#define CHIP_STM32G0XX_REGS_PWR_H

typedef struct {
  __IO uint32_t CR1;          /*!< PWR Power Control Register 1,                     Address offset: 0x00 */
  __IO uint32_t CR2;          /*!< PWR Power Control Register 2,                     Address offset: 0x04 */
  __IO uint32_t CR3;          /*!< PWR Power Control Register 3,                     Address offset: 0x08 */
  __IO uint32_t CR4;          /*!< PWR Power Control Register 4,                     Address offset: 0x0C */
  __IO uint32_t SR1;          /*!< PWR Power Status Register 1,                      Address offset: 0x10 */
  __IO uint32_t SR2;          /*!< PWR Power Status Register 2,                      Address offset: 0x14 */
  __IO uint32_t SCR;          /*!< PWR Power Status Clear Register,                  Address offset: 0x18 */
       uint32_t RESERVED1;    /*!< Reserved,                                         Address offset: 0x1C */
  __IO uint32_t PUCRA;        /*!< PWR Pull-Up Control Register of port A,           Address offset: 0x20 */
  __IO uint32_t PDCRA;        /*!< PWR Pull-Down Control Register of port A,         Address offset: 0x24 */
  __IO uint32_t PUCRB;        /*!< PWR Pull-Up Control Register of port B,           Address offset: 0x28 */
  __IO uint32_t PDCRB;        /*!< PWR Pull-Down Control Register of port B,         Address offset: 0x2C */
  __IO uint32_t PUCRC;        /*!< PWR Pull-Up Control Register of port C,           Address offset: 0x30 */
  __IO uint32_t PDCRC;        /*!< PWR Pull-Down Control Register of port C,         Address offset: 0x34 */
  __IO uint32_t PUCRD;        /*!< PWR Pull-Up Control Register of port D,           Address offset: 0x38 */
  __IO uint32_t PDCRD;        /*!< PWR Pull-Down Control Register of port D,         Address offset: 0x3C */
  __IO uint32_t PUCRE;        /*!< PWR Pull-Up Control Register of port E,           Address offset: 0x40 */
  __IO uint32_t PDCRE;        /*!< PWR Pull-Down Control Register of port E,         Address offset: 0x44 */
  __IO uint32_t PUCRF;        /*!< PWR Pull-Up Control Register of port F,           Address offset: 0x48 */
  __IO uint32_t PDCRF;        /*!< PWR Pull-Down Control Register of port F,         Address offset: 0x4C */
} CHIP_REGS_PWR_T;

#define CHIP_REGS_PWR          ((CHIP_REGS_PWR_T *) CHIP_MEMRGN_ADDR_PERIPH_PWR)


/********************  Bit definition for PWR_CR1 register  ********************/
#define CHIP_REGFLD_PWR_CR1_LPMS              (0x00000007UL <<  0U)  /* Low Power Mode Selection */
#define CHIP_REGFLD_PWR_CR1_FPD_STOP          (0x00000001UL <<  3U)  /* Flash power down mode during stop */
#define CHIP_REGFLD_PWR_CR1_FPD_LPRUN         (0x00000001UL <<  4U)  /* Flash power down mode during run */
#define CHIP_REGFLD_PWR_CR1_FPD_LPSLP         (0x00000001UL <<  5U)  /* Flash power down mode during sleep */
#define CHIP_REGFLD_PWR_CR1_DBP               (0x00000001UL <<  8U)  /* Disable Backup Domain write protection */
#define CHIP_REGFLD_PWR_CR1_VOS               (0x00000003UL <<  9U)  /* Voltage scaling */
#define CHIP_REGFLD_PWR_CR1_LPR               (0x00000001UL << 14U)  /* Regulator Low-Power Run mode */

/********************  Bit definition for PWR_CR2 register  ********************/
#define CHIP_REGFLD_PWR_CR2_PVDE              (0x00000001UL <<  0U)  /* Programmable Voltage Detector Enable */
#define CHIP_REGFLD_PWR_CR2_PVDFT             (0x00000007UL <<  1U)  /* PVD Falling Threshold Selection bit field */
#define CHIP_REGFLD_PWR_CR2_PVDRT             (0x00000007UL <<  4U)  /* PVD Rising Threshold Selection bit field */
#define CHIP_REGFLD_PWR_CR2_PVMEN_DAC         (0x00000001UL <<  7U)
#define CHIP_REGFLD_PWR_CR2_PVMEN_USB         (0x00000001UL <<  8U)  /* USB Peripheral Voltage Monitor Enable */
#define CHIP_REGFLD_PWR_CR2_IOSV              (0x00000001UL <<  9U)  /* VDDIO2 independent I/Os Supply Valid */
#define CHIP_REGFLD_PWR_CR2_USV               (0x00000001UL << 10U)  /* VDD USB Supply Valid */

/********************  Bit definition for PWR_CR3 register  ********************/
#define CHIP_REGFLD_PWR_CR3_EWUP              (0x0000003FUL <<  0U)  /* Enable all Wake-Up Pins  */
#define CHIP_REGFLD_PWR_CR3_EWUP1             (0x00000001UL <<  0U)  /* Enable WKUP pin 1 */
#define CHIP_REGFLD_PWR_CR3_EWUP2             (0x00000001UL <<  1U)  /* Enable WKUP pin 2 */
#define CHIP_REGFLD_PWR_CR3_EWUP3             (0x00000001UL <<  2U)  /* Enable WKUP pin 3 */
#define CHIP_REGFLD_PWR_CR3_EWUP4             (0x00000001UL <<  3U)  /* Enable WKUP pin 4 */
#define CHIP_REGFLD_PWR_CR3_EWUP5             (0x00000001UL <<  4U)  /* Enable WKUP pin 5 */
#define CHIP_REGFLD_PWR_CR3_EWUP6             (0x00000001UL <<  5U)  /* Enable WKUP pin 6 */
#define CHIP_REGFLD_PWR_CR3_RRS               (0x00000001UL <<  8U)  /* RAM retention in Standby mode */
#define CHIP_REGFLD_PWR_CR3_ENB_ULP           (0x00000001UL <<  9U)  /* Enable sampling resistor bridge in the LPMU_RESET block */
#define CHIP_REGFLD_PWR_CR3_APC               (0x00000001UL << 10U)  /* Apply pull-up and pull-down configuration */
#define CHIP_REGFLD_PWR_CR3_EIWUL             (0x00000001UL << 15U)  /* Enable Internal Wake-up line */

/********************  Bit definition for PWR_CR4 register  ********************/
#define CHIP_REGFLD_PWR_CR4_WP                (0x0000003FUL <<  0U)  /* all Wake-Up Pin polarity */
#define CHIP_REGFLD_PWR_CR4_WP1               (0x00000001UL <<  0U)  /* Wake-Up Pin 1 polarity */
#define CHIP_REGFLD_PWR_CR4_WP2               (0x00000001UL <<  1U)  /* Wake-Up Pin 2 polarity */
#define CHIP_REGFLD_PWR_CR4_WP3               (0x00000001UL <<  2U)  /* Wake-Up Pin 3 polarity */
#define CHIP_REGFLD_PWR_CR4_WP4               (0x00000001UL <<  3U)  /* Wake-Up Pin 4 polarity */
#define CHIP_REGFLD_PWR_CR4_WP5               (0x00000001UL <<  4U)  /* Wake-Up Pin 5 polarity */
#define CHIP_REGFLD_PWR_CR4_WP6               (0x00000001UL <<  5U)  /* Wake-Up Pin 6 polarity */
#define CHIP_REGFLD_PWR_CR4_VBE               (0x00000001UL <<  8U)  /* VBAT Battery charging Enable  */
#define CHIP_REGFLD_PWR_CR4_VBRS              (0x00000001UL <<  9U)  /* VBAT Battery charging Resistor Selection */

/********************  Bit definition for PWR_SR1 register  ********************/
#define CHIP_REGFLD_PWR_SR1_WUF               (0x0000003FUL <<  0U) /* Wakeup Flags  */
#define CHIP_REGFLD_PWR_SR1_WUF1              (0x00000001UL <<  0U) /* Wakeup Flag 1 */
#define CHIP_REGFLD_PWR_SR1_WUF2              (0x00000001UL <<  1U) /* Wakeup Flag 2 */
#define CHIP_REGFLD_PWR_SR1_WUF3              (0x00000001UL <<  2U) /* Wakeup Flag 3 */
#define CHIP_REGFLD_PWR_SR1_WUF4              (0x00000001UL <<  3U) /* Wakeup Flag 4 */
#define CHIP_REGFLD_PWR_SR1_WUF5              (0x00000001UL <<  4U) /* Wakeup Flag 5 */
#define CHIP_REGFLD_PWR_SR1_WUF6              (0x00000001UL <<  5U) /* Wakeup Flag 6 */
#define CHIP_REGFLD_PWR_SR1_SBF               (0x00000001UL <<  8U) /* Standby Flag  */
#define CHIP_REGFLD_PWR_SR1_WUFI              (0x00000001UL << 15U) /* Wakeup Flag Internal */

/********************  Bit definition for PWR_SR2 register  ********************/
#define CHIP_REGFLD_PWR_SR2_FLASH_RDY         (0x00000001UL <<  7U) /* Flash Ready */
#define CHIP_REGFLD_PWR_SR2_REGLPS            (0x00000001UL <<  8U) /* Regulator Low Power started */
#define CHIP_REGFLD_PWR_SR2_REGLPF            (0x00000001UL <<  9U) /* Regulator Low Power flag    */
#define CHIP_REGFLD_PWR_SR2_VOSF              (0x00000001UL << 10U) /* Voltage Scaling Flag */
#define CHIP_REGFLD_PWR_SR2_PVDO              (0x00000001UL << 11U) /* Power voltage detector output */
#define CHIP_REGFLD_PWR_SR2_PVMO_USB          (0x00000001UL << 12U) /* USB Peripheral Voltage Monitoring Output */
#define CHIP_REGFLD_PWR_SR2_PVMO_DAC          (0x00000001UL << 15U)

/********************  Bit definition for PWR_SCR register  ********************/
#define CHIP_REGFLD_PWR_SCR_CWUF              (0x0000003FUL <<  0U) /* Clear Wake-up Flags  */
#define CHIP_REGFLD_PWR_SCR_CWUF1             (0x00000001UL <<  0U) /* Clear Wake-up Flag 1 */
#define CHIP_REGFLD_PWR_SCR_CWUF2             (0x00000001UL <<  1U) /* Clear Wake-up Flag 2 */
#define CHIP_REGFLD_PWR_SCR_CWUF3             (0x00000001UL <<  2U) /* Clear Wake-up Flag 3 */
#define CHIP_REGFLD_PWR_SCR_CWUF4             (0x00000001UL <<  3U) /* Clear Wake-up Flag 4 */
#define CHIP_REGFLD_PWR_SCR_CWUF5             (0x00000001UL <<  4U) /* Clear Wake-up Flag 5 */
#define CHIP_REGFLD_PWR_SCR_CWUF6             (0x00000001UL <<  5U) /* Clear Wake-up Flag 6 */
#define CHIP_REGFLD_PWR_SCR_CSBF              (0x00000001UL <<  8U) /* Clear Standby Flag  */

/********************  Bit definition for PWR_PUCRX register  *****************/
#define CHIP_REGFLD_PWR_PUCRX_PU0             (0x00000001UL <<  0U) /* Pin Px0 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU1             (0x00000001UL <<  1U) /* Pin Px1 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU2             (0x00000001UL <<  2U) /* Pin Px2 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU3             (0x00000001UL <<  3U) /* Pin Px3 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU4             (0x00000001UL <<  4U) /* Pin Px4 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU5             (0x00000001UL <<  5U) /* Pin Px5 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU6             (0x00000001UL <<  6U) /* Pin Px6 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU7             (0x00000001UL <<  7U) /* Pin Px7 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU8             (0x00000001UL <<  8U) /* Pin Px8 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU9             (0x00000001UL <<  9U) /* Pin Px9 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU10            (0x00000001UL << 10U) /* Pin Px10 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU11            (0x00000001UL << 11U) /* Pin Px11 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU12            (0x00000001UL << 12U) /* Pin Px12 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU13            (0x00000001UL << 13U) /* Pin Px13 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU14            (0x00000001UL << 14U) /* Pin Px14 Pull-Up set */
#define CHIP_REGFLD_PWR_PUCRX_PU15            (0x00000001UL << 15U) /* Pin Px15 Pull-Up set */

/********************  Bit definition for PWR_PDCRX register  *****************/
#define CHIP_REGFLD_PWR_PDCRX_PD0             (0x00000001UL <<  0U) /* Pin Px0 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD1             (0x00000001UL <<  1U) /* Pin Px1 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD2             (0x00000001UL <<  2U) /* Pin Px2 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD3             (0x00000001UL <<  3U) /* Pin Px3 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD4             (0x00000001UL <<  4U) /* Pin Px4 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD5             (0x00000001UL <<  5U) /* Pin Px5 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD6             (0x00000001UL <<  6U) /* Pin Px6 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD7             (0x00000001UL <<  7U) /* Pin Px7 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD8             (0x00000001UL <<  8U) /* Pin Px8 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD9             (0x00000001UL <<  9U) /* Pin Px9 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD10            (0x00000001UL << 10U) /* Pin Px10 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD11            (0x00000001UL << 11U) /* Pin Px11 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD12            (0x00000001UL << 12U) /* Pin Px12 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD13            (0x00000001UL << 13U) /* Pin Px13 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD14            (0x00000001UL << 14U) /* Pin Px14 Pull-Down set */
#define CHIP_REGFLD_PWR_PDCRX_PD15            (0x00000001UL << 15U) /* Pin Px15 Pull-Down set */


#endif // CHIP_STM32G0XX_REGS_PWR_H
