#ifndef CHIP_PINS_H
#define CHIP_PINS_H

#include "regs/regs_ioc.h"


#define CHIP_PIN_FUNC_ANALOG    0x3E
//#define CHIP_PIN_FUNC_GPIO      0x3F


#define CHIP_PIN_CFG_OTYPE              (0x1UL << 0)
#define CHIP_PIN_CFG_OTYPE_VAL(v)       (((v) & 0x1UL) << 0)
#define CHIP_PIN_CFG_OTYPE_PP           CHIP_PIN_CFG_OTYPE_VAL(0) /* push-pull */
#define CHIP_PIN_CFG_OTYPE_OD           CHIP_PIN_CFG_OTYPE_VAL(1) /* open-drain */

#define CHIP_PIN_CFG_DSTRENGTH          (0x7UL << 1)
#define CHIP_PIN_CFG_DSTRENGTH_VAL(v)   (((v) & 0x7UL) << 1)
#define CHIP_PIN_CFG_DSTRENGTH_GET(cfg) ((cfg >> 1) & 0x7UL)

#define CHIP_PIN_CFG_PUPD               (0x3UL << 4)
#define CHIP_PIN_CFG_PUPD_VAL(v)        (((v) & 0x3UL) << 4)
#define CHIP_PIN_CFG_NOPULL             CHIP_PIN_CFG_PUPD_VAL(0)
#define CHIP_PIN_CFG_PULLUP             CHIP_PIN_CFG_PUPD_VAL(1)
#define CHIP_PIN_CFG_PULLDOWN           CHIP_PIN_CFG_PUPD_VAL(2)

#define CHIP_PIN_CFG_IN_SHMITT          (0x1UL << 6)
#define CHIP_PIN_CFG_IN_LOOPBACK        (0x1UL << 7)

#define CHIP_PIN_CFG_VSUP               (0x1UL << 8)
#define CHIP_PIN_CFG_VSUP_VAL(v)        (((v) & 0x1UL) << 8)
#define CHIP_PIN_CFG_VSUP_3_3           CHIP_PIN_CFG_VSUP_VAL(0)
#define CHIP_PIN_CFG_VSUP_1_8           CHIP_PIN_CFG_VSUP_VAL(1)

#define CHIP_PIN_CFG_FAST_GPIO          (0x1UL << 9)



#ifndef CHIP_PIN_CFG_DSTRENGTH
    #define CHIP_PIN_CFG_DSTRENGTH 0
#endif


#ifndef CHIP_PIN_CFG_STD
    #define CHIP_PIN_CFG_STD            (CHIP_PIN_CFG_DSTRENGTH | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_PP)
#endif
#ifndef CHIP_PIN_CFG_STD_OD
    #define CHIP_PIN_CFG_STD_OD         (CHIP_PIN_CFG_DSTRENGTH | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_OD)
#endif


#define CHIP_PIN_ID_EX(port, pin, func, subfunc, cfg) ((((port) & 0xF)  << 28) | \
                                                    (((pin)  & 0x1F)  << 23) | \
                                                    (((func) & 0x3F) << 17) | \
                                                    (((subfunc) & 0x3) << 15) | \
                                                    (((cfg)  & 0x3FF) << 0))




#define CHIP_PIN_ID_PORT(id)            (((id) >> 28) & 0x0F)
#define CHIP_PIN_ID_PIN(id)             (((id) >> 23) & 0x1F)
#define CHIP_PIN_ID_FUNC(id)            (((id) >> 17) & 0x3F)
#define CHIP_PIN_ID_SUBFUNC(id)         (((id) >> 15) & 0x03)
#define CHIP_PIN_ID_CFG(id)             (((id) >>  0) & 0x3FF)

#define CHIP_PIN_ID_INVALID             0xFFFFFFFF

#define CHIP_PIN_ID(port, pin, func)                CHIP_PIN_ID_EX(port, pin, func, 0, CHIP_PIN_CFG_STD)
#define CHIP_PIN_ID_OD(port, pin, func)             CHIP_PIN_ID_EX(port, pin, func, 0, CHIP_PIN_CFG_STD_OD)
#define CHIP_PIN_ID_SUB(port, pin, func, subfunc)   CHIP_PIN_ID_EX(port, pin, func, subfunc, CHIP_PIN_CFG_STD)



#define CHIP_PIN_PAD_REGNUM(id)         (32 * CHIP_PIN_ID_PORT(id) + CHIP_PIN_ID_PIN(id))


#define CHIP_PIN_IOC_CONFIG(ioc_regs, id, func, cfg) do { \
        ioc_regs->PAD[CHIP_PIN_PAD_REGNUM(id)].PAD_CTL = \
            LBITFIELD_SET(CHIP_REGFLD_IOC_PAD_CTL_MS, (cfg & CHIP_PIN_CFG_VSUP) == CHIP_PIN_CFG_VSUP_1_8) \
            | LBITFIELD_SET(CHIP_REGFLD_IOC_PAD_CTL_OD, (cfg & CHIP_PIN_CFG_OTYPE_OD) != 0) \
            | LBITFIELD_SET(CHIP_REGFLD_IOC_PAD_CTL_SMT, (cfg & CHIP_PIN_CFG_IN_SHMITT) != 0) \
            | LBITFIELD_SET(CHIP_REGFLD_IOC_PAD_CTL_PS, (cfg & CHIP_PIN_CFG_PULLUP) == CHIP_PIN_CFG_PULLUP) \
            | LBITFIELD_SET(CHIP_REGFLD_IOC_PAD_CTL_PE, (cfg & CHIP_PIN_CFG_PUPD) != CHIP_PIN_CFG_NOPULL) \
            | LBITFIELD_SET(CHIP_REGFLD_IOC_PAD_CTL_DS, CHIP_PIN_CFG_DSTRENGTH_GET(cfg)) \
        ; \
        ioc_regs->PAD[CHIP_PIN_PAD_REGNUM(id)].FUNC_CTL = \
            LBITFIELD_SET(CHIP_REGFLD_IOC_FUNC_CTL_ALT_SELECT, func) \
            | LBITFIELD_SET(CHIP_REGFLD_IOC_FUNC_CTL_ANALOG, CHIP_PIN_ID_FUNC(id) == CHIP_PIN_FUNC_ANALOG) \
            | LBITFIELD_SET(CHIP_REGFLD_IOC_FUNC_CTL_LOOP_BACK, ((cfg) & CHIP_PIN_CFG_IN_LOOPBACK) != 0); \
    } while(0)

/* функция, настраивающая пин по ID, с явным заданием специальных режимов через объединение по или CHIP_PIN_CFG */


#define CHIP_PIN_CONFIG_EX(id, cfg) do { \
    CHIP_PIN_IOC_CONFIG(CHIP_REGS_IOC, id, CHIP_PIN_ID_FUNC(id), cfg); \
    if (CHIP_PIN_IS_PDOMAIN(id)) { \
        CHIP_PIN_IOC_CONFIG(CHIP_REGS_PIOC, id, CHIP_PIN_ID_SUBFUNC(id), cfg); \
    } \
    if (CHIP_PIN_IS_BDOMAIN(id)) { \
        CHIP_PIN_IOC_CONFIG(CHIP_REGS_BIOC, id, CHIP_PIN_ID_SUBFUNC(id), cfg); \
    } \
} while (0)

/* функция, настраивающая пин по ID, использующая спец. настройки по умолчанию */
#define CHIP_PIN_CONFIG(id) do { \
    CHIP_PIN_CONFIG_EX(id, CHIP_PIN_ID_CFG(id)); \
} while(0)

/* настройка пина на оптимальный режим, если не используется (аналоговый режим для отключения входа/выхода) */
#define CHIP_PIN_CONFIG_DIS(id) do { \
    CHIP_PIN_CONFIG(CHIP_PIN_ID(CHIP_PIN_ID_PORT(id), CHIP_PIN_ID_PIN(id), CHIP_PIN_FUNC_ANALOG)); \
} while(0)





/** @todo сделать выбор GPIO0 или 1 по ядру, сделать выбор FGPIO, PGPIO и BGPIO по свойствам пина */
#define CHIP_PIN_GPIOCTL_REGS(id)       CHIP_REGS_GPIO0




#define CHIP_PIN_GPIOREG(reg, id)    (CHIP_PIN_GPIOCTL_REGS(id)->reg[CHIP_PIN_ID_PORT(id)])


#define CHIP_PIN_SET_DIR_IN(id)       (CHIP_PIN_GPIOREG(OE,id).CLEAR = (1 << CHIP_PIN_ID_PIN(id)))
#define CHIP_PIN_SET_DIR_OUT(id)      (CHIP_PIN_GPIOREG(OE,id).SET   = (1 << CHIP_PIN_ID_PIN(id)))



/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val)       if (val) { CHIP_PIN_GPIOREG(DO,id).SET = (1 << CHIP_PIN_ID_PIN(id)); } else { CHIP_PIN_GPIOREG(DO,id).CLEAR = (1 << CHIP_PIN_ID_PIN(id)); }

#define CHIP_PIN_TOGGLE(id)         (CHIP_PIN_GPIOREG(DO,id).TOGGLE = (1 << CHIP_PIN_ID_PIN(id)))



/* вспомогательный макрос, который конфигурирует порт на вывод с установленным
   начальным значением */
#define CHIP_PIN_CONFIG_OUT(id, val) do { \
                                        CHIP_PIN_OUT(id, val); \
                                        CHIP_PIN_SET_DIR_OUT(id); \
                                        CHIP_PIN_CONFIG(id); \
                                    } while(0)
/* вспомогательный макрос для настройки пина и конфигурирования его на вход */
#define CHIP_PIN_CONFIG_IN(id) do { \
                                    CHIP_PIN_SET_DIR_IN(id); \
                                    CHIP_PIN_CONFIG(id); \
                                } while(0)

#define CHIP_PIN_CONFIG_OUT_EX(id, cfg, val) do { \
                                                CHIP_PIN_OUT(id, val); \
                                                CHIP_PIN_SET_DIR_OUT(id); \
                                                CHIP_PIN_CONFIG_EX(id, cfg); \
                                            } while(0)

#define CHIP_PIN_CONFIG_IN_EX(id, cfg) do { \
                                            CHIP_PIN_SET_DIR_IN(id); \
                                            CHIP_PIN_CONFIG_EX(id, cfg); \
                                        } while(0)




#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4
#define CHIP_PORT_F 5
#define CHIP_PORT_X 13
#define CHIP_PORT_Y 14
#define CHIP_PORT_Z 15

#define CHIP_PIN_IS_PDOMAIN(id)      (CHIP_PIN_ID_PORT(id) == CHIP_PORT_Y)
#define CHIP_PIN_IS_BDOMAIN(id)      (CHIP_PIN_ID_PORT(id) == CHIP_PORT_Z)



//#define IOC_PAD_PY06 (454UL) //u0_txd
//#define IOC_PAD_PY07 (455UL) //u0_rxd


/*----- Пин PB11 (HS) ------------------------------------------------------- */
#define CHIP_PIN_PB11_GPIO                  CHIP_PIN_ID     (CHIP_PORT_B, 11, 0)
#define CHIP_PIN_PB11_UART12_DE             CHIP_PIN_ID     (CHIP_PORT_B, 12, 2)
#define CHIP_PIN_PB11_UART12_RTS            CHIP_PIN_ID     (CHIP_PORT_B, 12, 3)
#define CHIP_PIN_PB11_I2C0_SCL              CHIP_PIN_ID_OD  (CHIP_PORT_B, 12, 4)
#define CHIP_PIN_PB11_CAN2_STBY             CHIP_PIN_ID     (CHIP_PORT_B, 12, 7)
#define CHIP_PIN_PB11_I2S3_FCLK             CHIP_PIN_ID     (CHIP_PORT_B, 12, 8)
#define CHIP_PIN_PB11_TRGM1_P_01            CHIP_PIN_ID     (CHIP_PORT_B, 12, 16)
#define CHIP_PIN_PB11_CAM1_VSYNC            CHIP_PIN_ID     (CHIP_PORT_B, 12, 22)

/*----- Пин PB12 (HS) ------------------------------------------------------- */
#define CHIP_PIN_PB12_GPIO                  CHIP_PIN_ID     (CHIP_PORT_B, 12, 0)
#define CHIP_PIN_PB12_USART3_DE             CHIP_PIN_ID     (CHIP_PORT_B, 12, 2)
#define CHIP_PIN_PB12_USART3_RTS            CHIP_PIN_ID     (CHIP_PORT_B, 12, 3)
#define CHIP_PIN_PB12_CAN3_TXD              CHIP_PIN_ID     (CHIP_PORT_B, 12, 7)
#define CHIP_PIN_PB12_I2S3_TXD_2            CHIP_PIN_ID     (CHIP_PORT_B, 12, 8)
#define CHIP_PIN_PB12_I2S2_TXD_0            CHIP_PIN_ID     (CHIP_PORT_B, 12, 9)
#define CHIP_PIN_PB12_TRGM0_P_06            CHIP_PIN_ID     (CHIP_PORT_B, 12, 16)
#define CHIP_PIN_PB12_CAM1_D_7              CHIP_PIN_ID     (CHIP_PORT_B, 12, 22)

/*----- Пин PB13 (HS) ------------------------------------------------------- */
#define CHIP_PIN_PB13_GPIO                  CHIP_PIN_ID     (CHIP_PORT_B, 13, 0)
#define CHIP_PIN_PB13_UART15_CTS            CHIP_PIN_ID     (CHIP_PORT_B, 13, 3)
#define CHIP_PIN_PB13_I2C3_SDA              CHIP_PIN_ID_OD  (CHIP_PORT_B, 13, 4)
#define CHIP_PIN_PB13_CAN1_STBY             CHIP_PIN_ID     (CHIP_PORT_B, 13, 7)
#define CHIP_PIN_PB13_I2S3_TXD_1            CHIP_PIN_ID     (CHIP_PORT_B, 13, 8)
#define CHIP_PIN_PB13_TRGM1_P_03            CHIP_PIN_ID     (CHIP_PORT_B, 13, 16)
#define CHIP_PIN_PB13_CAM1_D                CHIP_PIN_ID     (CHIP_PORT_B, 13, 22)

#define CHIP_PIN_PY06_UART0_TX              CHIP_PIN_ID_SUB(CHIP_PORT_Y, 6, 2, 3)
#define CHIP_PIN_PY07_UART0_RX              CHIP_PIN_ID_SUB(CHIP_PORT_Y, 7, 2, 3)





#endif // CHIP_PINS_H
