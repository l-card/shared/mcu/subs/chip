#ifndef CHIP_CACHE_H
#define CHIP_CACHE_H

#define CHIP_CACHE_LINE_SIZE   32

static inline void chip_cache_flush(uint32_t addr, uint32_t size) {
    uint32_t end_addr = addr + size;
    for (addr = addr & ~(CHIP_CACHE_LINE_SIZE-1); addr < end_addr; addr+=CHIP_CACHE_LINE_SIZE) {
        __builtin_flush(addr);
    }
}

static inline void chip_cache_invalidate(uint32_t addr, uint32_t size) {
    uint32_t end_addr = addr + size;
    for (addr = addr & ~(CHIP_CACHE_LINE_SIZE-1); addr < end_addr; addr+=CHIP_CACHE_LINE_SIZE) {
        __builtin_flushinv(addr);
    }
}

#endif // CHIP_CACHE_H

