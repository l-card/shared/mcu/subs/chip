#ifndef CHIP_CORETYPE_H
#define CHIP_CORETYPE_H

#define CHIP_RISCV_FEAT_MTVEC_MODE  1

#include "chip_core_model.h"
#include "regs_systick.h"
#include "regs_pfic.h"
#include "chip_coretype_csr.h"


#endif // CHIP_CORETYPE_H
