CHIP_TARGET_ARCHTYPE := cortexm
CHIP_TARGET_CORETYPE := cm4

CHIP_DEFS = CHIP_V84XXX
CHIP_GEN_SRC += $(CHIP_TARGET_DIR)/init/chip_clk.c \
                $(CHIP_TARGET_DIR)/init/chip_per_ctl.c


CHIP_STARTUP_SRC += $(CHIP_TARGET_DIR)/init/chip_isr_table.c

CHIP_TARGET_DEV_DIR := $(CHIP_TARGET_DIR)/devs/$(CHIP_TARGET_DEV)
ifeq (,$(CHIP_TARGET_DEV))
    $(error CHIP_TARGET_DEV variable must be set to target cpu device)
else ifeq (,$(wildcard $(CHIP_TARGET_DEV_DIR)/$(CHIP_TARGET_DEV).mk))
    $(error $(CHIP_TARGET_DEV) is not supported device for target ($(CHIP_TARGET)))
else
    include $(CHIP_TARGET_DEV_DIR)/$(CHIP_TARGET_DEV).mk
endif
CHIP_INC_DIRS += $(CHIP_TARGET_DEV_DIR) $(CHIP_SHARED_DIR)/stm32

LPRINTF_TARGET := stm32_uart_v1

