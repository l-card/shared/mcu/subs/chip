#ifndef REGS_TMR0_H
#define REGS_TMR0_H

#include "chip_devtype_spec_features.h"

/****************************** Timer Prescaler *******************************/
#define CHIP_REG_GTCCR      _MMIO_BYTE(0x43) /* General timer counter control register */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_PSSR       _MMIO_BYTE(0xE2) /* Prescaler Select Register */
#endif

/* ---------------- GTCCR – General timer counter control register -----------*/
#define CHIP_REGFLD_GTCCR_TSM               (0x01U << 7) /* (RW) Timer counter synchronization mode control bit */
#define CHIP_REGFLD_GTCCR_PSRASY            (0x01U << 1) /* (WO) Prescaler CPS2 Reset */
#define CHIP_REGFLD_GTCCR_PSRSYNC           (0x01U << 0) /* (WO) Prescaler CPS310 Reset */

/* ---------------- PSSR – Prescaler Select Register -------------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_PSSR_PSS1               (0x01U << 7) /* (RW) TC1 prescaler selection */
#define CHIP_REGFLD_PSSR_PSS3               (0x01U << 6) /* (RW) TC3 prescaler selection */
#define CHIP_REGFLD_PSSR_PSR3               (0x01U << 1) /* (WSC) Prescaler CPS3 reset */
#define CHIP_REGFLD_PSSR_PSR1               (0x01U << 0) /* (WSC) Prescaler CPS1 reset */
#endif


/****************************** Timer 0 ***************************************/
#define CHIP_REG_TCCR0A     _MMIO_BYTE(0x44) /* TC0 Control Register A */
#define CHIP_REG_TCCR0B     _MMIO_BYTE(0x45) /* TC0 Control Register B */
#define CHIP_REG_TCNT0      _MMIO_BYTE(0x46) /* TC0 count value register */
#define CHIP_REG_OCR0A      _MMIO_BYTE(0x47) /* TC0 output compare register A */
#define CHIP_REG_OCR0B      _MMIO_BYTE(0x48) /* TC0 output compare register B */
#define CHIP_REG_TIMSK0     _MMIO_BYTE(0x6E) /* Timer Counter 0 Interrupt Mask Register */
#define CHIP_REG_TIFR0      _MMIO_BYTE(0x35) /* Timer Counter 0 Interrupt Flag Register */

#ifdef CHIP_LGT8FX8P
#define CHIP_REG_TCCR0C     _MMIO_BYTE(0x49) /* TC0 Control Register C */
#define CHIP_REG_DTR0       _MMIO_BYTE(0x4F) /* TC0 Dead Time Register */
#define CHIP_REG_TCKCSR     _MMIO_BYTE(0xEC) /* TC Clock Control and Status Register */
#endif

/* ---------------- TCCR0A - TC0 Control Register A --------------------------*/
#define CHIP_REGFLD_TCCR0A_COM0A            (0x03U << 6) /* (RW) TC0 compare match A output mode */
#define CHIP_REGFLD_TCCR0A_COM0B            (0x03U << 4) /* (RW) TC0 compare match B output mode */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_TCCR0A_DOC0B            (0x01U << 3) /* (RW) TC0 compare match B output shut-down  enable */
#define CHIP_REGFLD_TCCR0A_DOC0A            (0x01U << 2) /* (RW) TC0 compare match A output shut-down  enable */
#endif
#define CHIP_REGFLD_TCCR0A_WGM0_01          (0x03U << 0) /* (RW) TC0 waveform generation mode control bit 0..1 */


#define CHIP_REGFLDVAL_TC_COM_DISC          0 /* Disconnected */
#define CHIP_REGFLDVAL_TC_COM_CTC_TOGGLE    1 /* Toggle OC0x signal on compare match (CTC mode) */
#define CHIP_REGFLDVAL_TC_COM_CTC_CLR       2 /* Clear OC0x signal on compare match (CTC mode) */
#define CHIP_REGFLDVAL_TC_COM_CTC_SET       3 /* Set OC0x signal on compare match (CTC mode) */
#define CHIP_REGFLDVAL_TC_COM_FPWM_MCLR     2 /* Clear OC0x on compare match, set on maximum (Fast PWM mode) */
#define CHIP_REGFLDVAL_TC_COM_FPWM_MSET     3 /* Set OC0x on compare match, clear on maximum (Fast PWM mode) */
#define CHIP_REGFLDVAL_TC_COM_PCPWM_ASC_CLR 2 /* Clear OC0x on ascending compare match, set on descending (Phase correct PWM mode) */
#define CHIP_REGFLDVAL_TC_COM_PCPWM_ASC_SET 3 /* Set OC0x on ascending compare match, clear on descending (Phase correct PWM mode) */

#define CHIP_REGFLDVAL_TC0_WGM_NORMAL       0 /* Normal mode - count to MAX */
#define CHIP_REGFLDVAL_TC0_WGM_PCPWM_MAX    1 /* Phase Correct PWM to MAX */
#define CHIP_REGFLDVAL_TC0_WGM_CTC          2 /* Clear on OCR0A match */
#define CHIP_REGFLDVAL_TC0_WGM_FPWM_MAX     3 /* Fast PWM  to MAX */
#define CHIP_REGFLDVAL_TC0_WGM_PCPWM_MATCH  5 /* Phase Correct PWM to OCR0A */
#define CHIP_REGFLDVAL_TC0_WGM_FPWM_MATCH   7 /* Fast PWM to OCR0A */

/* ---------------- TCCR0B - TC0 Control Register B --------------------------*/
#define CHIP_REGFLD_TCCR0B_FOC0A            (0x01U << 7) /* (WO) TC0 Force output compare A control bit */
#define CHIP_REGFLD_TCCR0B_FOC0B            (0x01U << 6) /* (WO) TC0 Force output compare B control bit */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_TCCR0B_OC0AS            (0x01U << 5) /* (RW) OC0A Output port selection control bit (PD6 or PE4) */
#define CHIP_REGFLD_TCCR0B_DTEN0            (0x01U << 4) /* (RW) TC0 dead time enable control bit */
#endif
#define CHIP_REGFLD_TCCR0B_WGM0_2           (0x01U << 3) /* (RW) TC0 waveform generation mode control bit 2 */
#define CHIP_REGFLD_TCCR0B_CS               (0x07U << 0) /* (RW) TC0 clock select */


#define CHIP_REGFLDVAL_TCCR0B_OC0AS_PD6     0
#define CHIP_REGFLDVAL_TCCR0B_OC0AS_PE4     1

#define CHIP_REGFLDVAL_TCCR0B_CS_DIS        0 /* No clock source, stop counting */
#define CHIP_REGFLDVAL_TCCR0B_CS_FSYS_1     1 /* Fsys */
#define CHIP_REGFLDVAL_TCCR0B_CS_FSYS_8     2 /* Fsys/8, from prescaler */
#define CHIP_REGFLDVAL_TCCR0B_CS_FSYS_64    3 /* Fsys/64, from prescaler */
#define CHIP_REGFLDVAL_TCCR0B_CS_FSYS_256   4 /* Fsys/256, from prescaler */
#define CHIP_REGFLDVAL_TCCR0B_CS_FSYS_1024  5 /* Fsys/1024, from prescaler */
#define CHIP_REGFLDVAL_TCCR0B_CS_EXT_FALL   6 /* External clock T0 pin, falling edge trigger */
#define CHIP_REGFLDVAL_TCCR0B_CS_EXT_RISE   7 /* External clock T0 pin, rising edge trigger */


/* ---------------- TCCR0C - TC0 Control Register C --------------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_TCCR0C_DSX_TC1_OV       (0x01U << 7) /* (RW) TC0 trigger source on TC1 overflow enable */
#define CHIP_REGFLD_TCCR0C_DSX_TC2_OV       (0x01U << 6) /* (RW) TC0 trigger source on TC2 overflow enable */
#define CHIP_REGFLD_TCCR0C_DSX_PCINT0       (0x01U << 5) /* (RW) TC0 trigger source on pin change interrupt 0 enable */
#define CHIP_REGFLD_TCCR0C_DSX_EXT_INT0     (0x01U << 4) /* (RW) TC0 trigger source on external interrupt 0 enable */
#define CHIP_REGFLD_TCCR0C_DSX_AC1          (0x01U << 1) /* (RW) TC0 trigger source on analog comparator 1 enable */
#define CHIP_REGFLD_TCCR0C_DSX_AC0          (0x01U << 0) /* (RW) TC0 trigger source on analog comparator 0 enable */
#endif

/* ---------------- TIMSK0 – TC0 Interrupt Mask Register ---------------------*/
#define CHIP_REGFLD_TIMSK0_OCIE0B           (0x01U << 2) /* (RW) TC0 output compare B match interrupt enable */
#define CHIP_REGFLD_TIMSK0_OCIE0A           (0x01U << 1) /* (RW) TC0 output compare A match interrupt enable */
#define CHIP_REGFLD_TIMSK0_TOIE0            (0x01U << 0) /* (RW) TC0 overflow interrupt enable bit */

/* ---------------- TIFR0 - TC0 Interrupt Flag Register ----------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_TIFR0_OC0A              (0x01U << 7) /* (RO) The comparison waveform signal OC0A state */
#define CHIP_REGFLD_TIFR0_OC0B              (0x01U << 6) /* (RO) The comparison waveform signal OC0B state */
#endif
#define CHIP_REGFLD_TIFR0_OCF0B             (0x01U << 2) /* (RW1C) TC0 output compare B match flag */
#define CHIP_REGFLD_TIFR0_OCF0A             (0x01U << 1) /* (RW1C) TC0 output compare A match flag */
#define CHIP_REGFLD_TIFR0_TOV0              (0x01U << 0) /* (RW1C) TC0 overflow flag */

/* ---------------- DTR0 – TC0 Dead Time Control Register --------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_DTR0_DTR0H              (0x0FU << 4) /* (RW) TC0 Dead time register high bits (OC0B). */
#define CHIP_REGFLD_DTR0_DTR0L              (0x0FU << 0) /* (RW) TC0 Dead time register low bits (OC0A). */
#endif

/* ---------------- TCKSCR – TC Clock Control and Status Register ------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_TCKSCR_F2XEN            (0x01U << 6) /* (RW) RC 32M Multiplier output enable control bit */
#define CHIP_REGFLD_TCKSCR_TC2XF1           (0x01U << 5) /* (RO) TC1 high-speed clock mode flag bit 1 */
#define CHIP_REGFLD_TCKSCR_TC2XF0           (0x01U << 4) /* (RO) TC0 high-speed clock mode flag bit 0 */
#define CHIP_REGFLD_TCKSCR_TC2XS1           (0x01U << 1) /* (RW) TC1 high-speed clock mode selection control bit 1 */
#define CHIP_REGFLD_TCKSCR_TC2XS0           (0x01U << 0) /* (RW) TC0 high-speed clock mode selection control bit 0 */
#endif










/****************************** Timer 1 ***************************************/
#define CHIP_REG_TCCR1A     _MMIO_BYTE(0x80) /* TC1 Control Register A */
#define CHIP_REG_TCCR1B     _MMIO_BYTE(0x81) /* TC1 Control Register B */
#define CHIP_REG_TCCR1C     _MMIO_BYTE(0x82) /* TC1 Control Register C */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_TCCR1D     _MMIO_BYTE(0x83) /* TC1 Control Register D (TC1 trigger source control register) */
#endif
#define CHIP_REG_TCNT1L     _MMIO_BYTE(0x84) /* TC1 count value register low byte */
#define CHIP_REG_TCNT1H     _MMIO_BYTE(0x85) /* TC1 count value register high byte */
#define CHIP_REG_ICR1L      _MMIO_BYTE(0x86) /* TC1 input capture register low byte */
#define CHIP_REG_ICR1H      _MMIO_BYTE(0x87) /* TC1 input capture register high byte */
#define CHIP_REG_OCR1AL     _MMIO_BYTE(0x88) /* TC1 output compare register A low byte */
#define CHIP_REG_OCR1AH     _MMIO_BYTE(0x89) /* TC1 output compare register A high byte */
#define CHIP_REG_OCR1BL     _MMIO_BYTE(0x8A) /* TC1 output compare register B low byte */
#define CHIP_REG_OCR1BH     _MMIO_BYTE(0x8B) /* TC1 output compare register B high byte */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_DTR1L      _MMIO_BYTE(0x8C) /* TC1 Dead Time Control Register Low Byte */
#define CHIP_REG_DTR1H      _MMIO_BYTE(0x8D) /* TC1 Dead Time Control Register High Byte */
#endif
#define CHIP_REG_TIMSK1     _MMIO_BYTE(0x6F) /* Timer Counter Interrupt Mask Register */
#define CHIP_REG_TIFR1      _MMIO_BYTE(0x36) /* Timer Counter Interrupt Flag Register */


/* ---------------- TCCR1A - TC1 Control Register A --------------------------*/
#define CHIP_REGFLD_TCCR1A_COM0A            (0x03U << 6) /* (RW) TC1 compare match A output mode */
#define CHIP_REGFLD_TCCR1A_COM0B            (0x03U << 4) /* (RW) TC1 compare match B output mode */
#define CHIP_REGFLD_TCCR1A_WGM0_01          (0x03U << 0) /* (RW) TC1 waveform generation mode control bit 0..1 */


#define CHIP_REGFLDVAL_TC1_WGM_NORMAL        0 /* Normal mode - count to MAX */
#define CHIP_REGFLDVAL_TC1_WGM_PCPWM_MAX_8   1 /* Phase Correct PWM to 8-bit MAX (0x0FF) */
#define CHIP_REGFLDVAL_TC1_WGM_PCPWM_MAX_9   2 /* Phase Correct PWM to 9-bit MAX (0x1FF) */
#define CHIP_REGFLDVAL_TC1_WGM_PCPWM_MAX_10  3 /* Phase Correct PWM to 10-bit MAX (0x3FF) */
#define CHIP_REGFLDVAL_TC1_WGM_CTC_OCRA      4 /* Clear on OCR0A match */
#define CHIP_REGFLDVAL_TC1_WGM_FPWM_MAX_8    5 /* Fast PWM  to 8-bit MAX (0xFF) */
#define CHIP_REGFLDVAL_TC1_WGM_FPWM_MAX_9    6 /* Fast PWM  to 9-bit MAX (0x1FF) */
#define CHIP_REGFLDVAL_TC1_WGM_FPWM_MAX_10   7 /* Fast PWM  to 10-bit MAX (0x3FF) */
#define CHIP_REGFLDVAL_TC1_WGM_PFCPWM_ICR    8 /* Phase Correct PWM to ICRx */
#define CHIP_REGFLDVAL_TC1_WGM_PFCPWM_OCRA   9 /* Phase Correct PWM to OCRxA */
#define CHIP_REGFLDVAL_TC1_WGM_PCPWM_ICR     10 /* Phase Correct PWM to ICRx */
#define CHIP_REGFLDVAL_TC1_WGM_PCPWM_OCRA    11 /* Phase Correct PWM to OCRxA */
#define CHIP_REGFLDVAL_TC1_WGM_CTC_ICR       12 /* Clear on ICR match */
#define CHIP_REGFLDVAL_TC1_WGM_FPWM_ICR      14 /* Fast PWM to ICRx */
#define CHIP_REGFLDVAL_TC1_WGM_FPWM_OCRA     15 /* Fast PWM to OCRxA */

/* ---------------- TCCR1B - TC1 Control Register B --------------------------*/
#define CHIP_REGFLD_TCCR1B_ICNC1            (0x01U << 7) /* (RW) TC1 input capture noise suppression enable */
#define CHIP_REGFLD_TCCR1B_ICES1            (0x01U << 6) /* (WO) TC1 input capture trigger edge select control bit. */

#define CHIP_REGFLD_TCCR1B_WGM0_23          (0x03U << 3) /* (RW) TC1 waveform generation mode control bit 2..3 */
#define CHIP_REGFLD_TCCR1B_CS               (0x07U << 0) /* (RW) TC1 clock select */

#define CHIP_REGFLDVAL_TCCR1B_ICES1_FALL    0
#define CHIP_REGFLDVAL_TCCR1B_ICES1_RISE    1

#define CHIP_REGFLDVAL_TCCR1B_CS_DIS        0 /* No clock source, stop counting */
#define CHIP_REGFLDVAL_TCCR1B_CS_FSYS_1     1 /* Fsys */
#define CHIP_REGFLDVAL_TCCR1B_CS_FSYS_8     2 /* Fsys/8, from prescaler */
#define CHIP_REGFLDVAL_TCCR1B_CS_FSYS_64    3 /* Fsys/64, from prescaler */
#define CHIP_REGFLDVAL_TCCR1B_CS_FSYS_256   4 /* Fsys/256, from prescaler */
#define CHIP_REGFLDVAL_TCCR1B_CS_FSYS_1024  5 /* Fsys/1024, from prescaler */
#define CHIP_REGFLDVAL_TCCR1B_CS_EXT_FALL   6 /* External clock T1 pin, falling edge trigger */
#define CHIP_REGFLDVAL_TCCR1B_CS_EXT_RISE   7 /* External clock T1 pin, rising edge trigger */


/* ---------------- TCCR1C - TC1 Control Register C --------------------------*/
#define CHIP_REGFLD_TCCR1C_FOC1A            (0x01U << 7) /* (WO) TC1 Force output compare A control bit */
#define CHIP_REGFLD_TCCR1C_FOC1B            (0x01U << 6) /* (WO) TC1 Force output compare B control bit */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_TCCR1C_DOC1B            (0x01U << 5) /* (RW) TC1 compare match B output shut-down  enable */
#define CHIP_REGFLD_TCCR1C_DOC1A            (0x01U << 4) /* (RW) TC1 compare match A output shut-down  enable */
#define CHIP_REGFLD_TCCR1C_DTEN1            (0x01U << 3) /* (RW) TC1 dead time enable control bit */
#endif

#ifdef CHIP_LGT8FX8P
/* ---------------- TCCR1D - TC1 Control Register D --------------------------*/
#define CHIP_REGFLD_TCCR1D_DSX_TC0_OV       (0x01U << 7) /* (RW) TC1 trigger source on TC0 overflow enable */
#define CHIP_REGFLD_TCCR1D_DSX_TC2_OV       (0x01U << 6) /* (RW) TC1 trigger source on TC2 overflow enable */
#define CHIP_REGFLD_TCCR1D_DSX_PCINT0       (0x01U << 5) /* (RW) TC1 trigger source on pin change interrupt 0 enable */
#define CHIP_REGFLD_TCCR1D_DSX_EXT_INT0     (0x01U << 4) /* (RW) TC1 trigger source on external interrupt 0 enable */
#define CHIP_REGFLD_TCCR1D_DSX_AC1          (0x01U << 1) /* (RW) TC1 trigger source on analog comparator 1 enable */
#define CHIP_REGFLD_TCCR1D_DSX_AC0          (0x01U << 0) /* (RW) TC1 trigger source on analog comparator 0 enable */
#endif

/* ---------------- TIMSK1 – TC1 Interrupt Mask Register ---------------------*/
#define CHIP_REGFLD_TIMSK1_ICIE1            (0x01U << 5) /* (RW) TC1 Input capture interrupt enable */
#define CHIP_REGFLD_TIMSK1_OCIE1B           (0x01U << 2) /* (RW) TC1 output compare B match interrupt enable */
#define CHIP_REGFLD_TIMSK1_OCIE1A           (0x01U << 1) /* (RW) TC1 output compare A match interrupt enable */
#define CHIP_REGFLD_TIMSK1_TOIE1            (0x01U << 0) /* (RW) TC1 overflow interrupt enable bit */

/* ---------------- TIFR1 - TC1 Interrupt Flag Register ----------------------*/
#define CHIP_REGFLD_TIFR1_ICF1              (0x01U << 5) /* (RW1C) TC1 Input capture flag */
#define CHIP_REGFLD_TIFR1_OCF1B             (0x01U << 2) /* (RW1C) TC1 output compare B match flag */
#define CHIP_REGFLD_TIFR1_OCF1A             (0x01U << 1) /* (RW1C) TC1 output compare A match flag */
#define CHIP_REGFLD_TIFR1_TOV1              (0x01U << 0) /* (RW1C) TC1 overflow flag */














/****************************** Timer 2 ***************************************/
#define CHIP_REG_TCCR2A     _MMIO_BYTE(0xB0) /* TC2 Control Register A */
#define CHIP_REG_TCCR2B     _MMIO_BYTE(0xB1) /* TC2 Control Register B */
#define CHIP_REG_TCNT2      _MMIO_BYTE(0xB2) /* TC2 count value register */
#define CHIP_REG_OCR2A      _MMIO_BYTE(0xB3) /* TC2 output compare register A */
#define CHIP_REG_OCR2B      _MMIO_BYTE(0xB4) /* TC2 output compare register B */
#define CHIP_REG_TIMSK2     _MMIO_BYTE(0x70) /* TC2 Interrupt Mask Register */
#define CHIP_REG_TIFR2      _MMIO_BYTE(0x37) /* TC2 Interrupt Flag Register */
#define CHIP_REG_ASSR       _MMIO_BYTE(0xB6) /* Asynchronous Interface Status Register */

/* ---------------- TCCR2A - TC2 Control Register A --------------------------*/
#define CHIP_REGFLD_TCCR2A_COM2A            (0x03U << 6) /* (RW) TC2 compare match A output mode */
#define CHIP_REGFLD_TCCR2A_COM2B            (0x03U << 4) /* (RW) TC2 compare match B output mode */
#define CHIP_REGFLD_TCCR2A_WGM2_01          (0x03U << 0) /* (RW) TC2 waveform generation mode control bit 0..1 */

#define CHIP_REGFLDVAL_TC2_WGM_NORMAL       0 /* Normal mode - count to MAX */
#define CHIP_REGFLDVAL_TC2_WGM_PCPWM_MAX    1 /* Phase Correct PWM to MAX */
#define CHIP_REGFLDVAL_TC2_WGM_CTC          2 /* Clear on OCR0A match */
#define CHIP_REGFLDVAL_TC2_WGM_FPWM_MAX     3 /* Fast PWM  to MAX */
#define CHIP_REGFLDVAL_TC2_WGM_PCPWM_MATCH  5 /* Phase Correct PWM to OCR0A */
#define CHIP_REGFLDVAL_TC2_WGM_FPWM_MATCH   7 /* Fast PWM to OCR0A */

/* ---------------- TCCR2B - TC2 Control Register B --------------------------*/
#define CHIP_REGFLD_TCCR2B_FOC2A            (0x01U << 7) /* (WO) TC2 Force output compare A control bit */
#define CHIP_REGFLD_TCCR2B_FOC2B            (0x01U << 6) /* (WO) TC2 Force output compare B control bit */
#define CHIP_REGFLD_TCCR2B_WGM2_2           (0x01U << 3) /* (RW) TC2 waveform generation mode control bit 2 */
#define CHIP_REGFLD_TCCR2B_CS               (0x07U << 0) /* (RW) TC2 clock select */


#define CHIP_REGFLDVAL_TCCR0B_OC0AS_PD6     0
#define CHIP_REGFLDVAL_TCCR0B_OC0AS_PE4     1

#define CHIP_REGFLDVAL_TCCR2B_CS_DIS        0 /* No clock source, stop counting */
#define CHIP_REGFLDVAL_TCCR2B_CS_FSYS_1     1 /* Ft2s */
#define CHIP_REGFLDVAL_TCCR2B_CS_FSYS_8     2 /* Ft2s/8, from prescaler */
#define CHIP_REGFLDVAL_TCCR2B_CS_FSYS_32    3 /* Ft2s/32, from prescaler */
#define CHIP_REGFLDVAL_TCCR2B_CS_FSYS_64    4 /* Ft2s/64, from prescaler */
#define CHIP_REGFLDVAL_TCCR2B_CS_FSYS_128   5 /* Ft2s/128, from prescaler */
#define CHIP_REGFLDVAL_TCCR2B_CS_FSYS_256   6 /* Ft2s/256, from prescaler */
#define CHIP_REGFLDVAL_TCCR2B_CS_FSYS_1024  7 /* Ft2s/1024, from prescaler */

/* ---------------- TIMSK2 – TC2 Interrupt Mask Register ---------------------*/
#define CHIP_REGFLD_TIMSK2_OCIE2B           (0x01U << 2) /* (RW) TC2 output compare B match interrupt enable */
#define CHIP_REGFLD_TIMSK2_OCIE2A           (0x01U << 1) /* (RW) TC2 output compare A match interrupt enable */
#define CHIP_REGFLD_TIMSK2_TOIE2            (0x01U << 0) /* (RW) TC2 overflow interrupt enable bit */

/* ---------------- TIFR2 - TC2 Interrupt Flag Register ----------------------*/
#define CHIP_REGFLD_TIFR2_OCF2B             (0x01U << 2) /* (RW1C) TC2 output compare B match flag */
#define CHIP_REGFLD_TIFR2_OCF2A             (0x01U << 1) /* (RW1C) TC2 output compare A match flag */
#define CHIP_REGFLD_TIFR2_TOV2              (0x01U << 0) /* (RW1C) TC2 overflow flag */

/* ---------------- ASSR – Asynchronous Interface Status Register ------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_ASSR_INTCK              (0x01U << 7) /* (RW) Asynchronous clock select */
#else
#define CHIP_REGFLD_ASSR_EXCLK              (0x01U << 6) /* (RW) Enable External Clock Input */
#endif
#define CHIP_REGFLD_ASSR_AS2                (0x01U << 5) /* (RW) Timer 2 asynchronous mode */
#define CHIP_REGFLD_ASSR_TCN2UB             (0x01U << 4) /* (RW) TCNT2 register update flag */
#define CHIP_REGFLD_ASSR_OCR2AUB            (0x01U << 3) /* (RW) OCR2A register update flag */
#define CHIP_REGFLD_ASSR_OCR2BUB            (0x01U << 2) /* (RW) OCR2B register update flag */
#define CHIP_REGFLD_ASSR_TCR2AUB            (0x01U << 1) /* (RW) TCCR2A register update flag */
#define CHIP_REGFLD_ASSR_TCR2BUB            (0x01U << 0) /* (RW) TCCR2B register update flag. */

#define CHIP_REGFLDVAL_ASSR_INTCK_RC32K     1
#define CHIP_REGFLDVAL_ASSR_INTCK_OSC       0









#ifdef CHIP_LGT8FX8P
/****************************** Timer 3 ***************************************/
#define CHIP_REG_TCCR3A     _MMIO_BYTE(0x90) /* TC3 Control Register A */
#define CHIP_REG_TCCR3B     _MMIO_BYTE(0x91) /* TC3 Control Register B */
#define CHIP_REG_TCCR3C     _MMIO_BYTE(0x92) /* TC3 Control Register C */
#define CHIP_REG_TCCR3D     _MMIO_BYTE(0x93) /* TC3 Control Register D (TC1 trigger source control register) */
#define CHIP_REG_TCNT3L     _MMIO_BYTE(0x94) /* TC3 count value register low byte */
#define CHIP_REG_TCNT3H     _MMIO_BYTE(0x95) /* TC3 count value register high byte */
#define CHIP_REG_ICR3L      _MMIO_BYTE(0x96) /* TC3 input capture register low byte */
#define CHIP_REG_ICR3H      _MMIO_BYTE(0x97) /* TC3 input capture register high byte */
#define CHIP_REG_OCR3AL     _MMIO_BYTE(0x98) /* TC3 output compare register A low byte */
#define CHIP_REG_OCR3AH     _MMIO_BYTE(0x99) /* TC3 output compare register A high byte */
#define CHIP_REG_OCR3BL     _MMIO_BYTE(0x9A) /* TC3 output compare register B low byte */
#define CHIP_REG_OCR3BH     _MMIO_BYTE(0x9B) /* TC3 output compare register B high byte */
#define CHIP_REG_DTR3L      _MMIO_BYTE(0x9C) /* TC3 Dead Time Control Register low byte */
#define CHIP_REG_DTR3H      _MMIO_BYTE(0x9D) /* TC3 Dead Time Control Register high byte */
#define CHIP_REG_OCR3CL     _MMIO_BYTE(0x9E) /* TC3 output compare register C low byte */
#define CHIP_REG_OCR3CH     _MMIO_BYTE(0x9F) /* TC3 output compare register C high byte */
#define CHIP_REG_TIMSK3     _MMIO_BYTE(0x71) /* TC3 Interrupt Mask Register */
#define CHIP_REG_TIFR3      _MMIO_BYTE(0x38) /* TC3 Interrupt Flag Register */



/* ---------------- TCCR3A - TC3 Control Register A --------------------------*/
#define CHIP_REGFLD_TCCR3A_COM0A            (0x03U << 6) /* (RW) TC3 compare match A output mode */
#define CHIP_REGFLD_TCCR3A_COM0B            (0x03U << 4) /* (RW) TC3 compare match B output mode */
#define CHIP_REGFLD_TCCR3A_COM0C            (0x03U << 2) /* (RW) TC3 compare match C output mode */
#define CHIP_REGFLD_TCCR3A_WGM0_01          (0x03U << 0) /* (RW) TC3 waveform generation mode control bit 0..1 */

#define CHIP_REGFLDVAL_TC3_WGM_NORMAL        0 /* Normal mode - count to MAX */
#define CHIP_REGFLDVAL_TC3_WGM_PCPWM_MAX_8   1 /* Phase Correct PWM to 8-bit MAX (0x0FF) */
#define CHIP_REGFLDVAL_TC3_WGM_PCPWM_MAX_9   2 /* Phase Correct PWM to 9-bit MAX (0x1FF) */
#define CHIP_REGFLDVAL_TC3_WGM_PCPWM_MAX_10  3 /* Phase Correct PWM to 10-bit MAX (0x3FF) */
#define CHIP_REGFLDVAL_TC3_WGM_CTC_OCRA      4 /* Clear on OCR0A match */
#define CHIP_REGFLDVAL_TC3_WGM_FPWM_MAX_8    5 /* Fast PWM  to 8-bit MAX (0xFF) */
#define CHIP_REGFLDVAL_TC3_WGM_FPWM_MAX_9    6 /* Fast PWM  to 9-bit MAX (0x1FF) */
#define CHIP_REGFLDVAL_TC3_WGM_FPWM_MAX_10   7 /* Fast PWM  to 10-bit MAX (0x3FF) */
#define CHIP_REGFLDVAL_TC3_WGM_PFCPWM_ICR    8 /* Phase Correct PWM to ICRx */
#define CHIP_REGFLDVAL_TC3_WGM_PFCPWM_OCRA   9 /* Phase Correct PWM to OCRxA */
#define CHIP_REGFLDVAL_TC3_WGM_PCPWM_ICR     10 /* Phase Correct PWM to ICRx */
#define CHIP_REGFLDVAL_TC3_WGM_PCPWM_OCRA    11 /* Phase Correct PWM to OCRxA */
#define CHIP_REGFLDVAL_TC3_WGM_CTC_ICR       12 /* Clear on ICR match */
#define CHIP_REGFLDVAL_TC3_WGM_FPWM_ICR      14 /* Fast PWM to ICRx */
#define CHIP_REGFLDVAL_TC3_WGM_FPWM_OCRA     15 /* Fast PWM to OCRxA */

/* ---------------- TCCR3B - TC3 Control Register B --------------------------*/
#define CHIP_REGFLD_TCCR3B_ICNC3            (0x01U << 7) /* (RW) TC3 input capture noise suppression enable */
#define CHIP_REGFLD_TCCR3B_ICES3            (0x01U << 6) /* (WO) TC3 input capture trigger edge select control bit. */
#define CHIP_REGFLD_TCCR1B_WGM0_23          (0x03U << 3) /* (RW) TC3 waveform generation mode control bit 2..3 */
#define CHIP_REGFLD_TCCR1B_CS               (0x07U << 0) /* (RW) TC3 clock select */

#define CHIP_REGFLDVAL_TCCR1B_ICES3_FALL    0
#define CHIP_REGFLDVAL_TCCR1B_ICES3_RISE    1

#define CHIP_REGFLDVAL_TCCR3B_CS_DIS        0 /* No clock source, stop counting */
#define CHIP_REGFLDVAL_TCCR3B_CS_FSYS_1     1 /* Fsys */
#define CHIP_REGFLDVAL_TCCR3B_CS_FSYS_8     2 /* Fsys/8, from prescaler */
#define CHIP_REGFLDVAL_TCCR3B_CS_FSYS_64    3 /* Fsys/64, from prescaler */
#define CHIP_REGFLDVAL_TCCR3B_CS_FSYS_256   4 /* Fsys/256, from prescaler */
#define CHIP_REGFLDVAL_TCCR3B_CS_FSYS_1024  5 /* Fsys/1024, from prescaler */
#define CHIP_REGFLDVAL_TCCR3B_CS_EXT_FALL   6 /* External clock T3 pin, falling edge trigger */
#define CHIP_REGFLDVAL_TCCR3B_CS_EXT_RISE   7 /* External clock T3 pin, rising edge trigger */

/* ---------------- TCCR3C - TC3 Control Register C --------------------------*/
#define CHIP_REGFLD_TCCR3C_FOC3A            (0x01U << 7) /* (WO) TC3 Force output compare A control bit */
#define CHIP_REGFLD_TCCR3C_FOC3B            (0x01U << 6) /* (WO) TC3 Force output compare B control bit */
#define CHIP_REGFLD_TCCR3C_DOC3B            (0x01U << 5) /* (RW) TC3 compare match B output shut-down  enable */
#define CHIP_REGFLD_TCCR3C_DOC3A            (0x01U << 4) /* (RW) TC3 compare match A output shut-down  enable */
#define CHIP_REGFLD_TCCR3C_DTEN3            (0x01U << 3) /* (RW) TC3 dead time enable control bit */
#define CHIP_REGFLD_TCCR3C_DOC3C            (0x01U << 1) /* (RW) TC3 compare match C output shut-down  enable */
#define CHIP_REGFLD_TCCR3C_FOC3C            (0x01U << 0) /* (WO) TC3 Force output compare C control bit */

/* ---------------- TCCR3D - TC3 Control Register D --------------------------*/
#define CHIP_REGFLD_TCCR3D_DSX_TC0_OV       (0x01U << 7) /* (RW) TC3 trigger source on TC0 overflow enable */
#define CHIP_REGFLD_TCCR3D_DSX_TC2_OV       (0x01U << 6) /* (RW) TC3 trigger source on TC2 overflow enable */
#define CHIP_REGFLD_TCCR3D_DSX_PCINT0       (0x01U << 5) /* (RW) TC3 trigger source on pin change interrupt 0 enable */
#define CHIP_REGFLD_TCCR3D_DSX_EXT_INT0     (0x01U << 4) /* (RW) TC3 trigger source on external interrupt 0 enable */
#define CHIP_REGFLD_TCCR3D_DSX_AC1          (0x01U << 1) /* (RW) TC3 trigger source on analog comparator 1 enable */
#define CHIP_REGFLD_TCCR3D_DSX_AC0          (0x01U << 0) /* (RW) TC3 trigger source on analog comparator 0 enable */


/* ---------------- TIMSK3 – TC3 Interrupt Mask Register ---------------------*/
#define CHIP_REGFLD_TIMSK3_ICIE3            (0x01U << 5) /* (RW) TC3 Input capture interrupt enable */
#define CHIP_REGFLD_TIMSK3_OCIE3C           (0x01U << 3) /* (RW) TC3 output compare C match interrupt enable */
#define CHIP_REGFLD_TIMSK3_OCIE3B           (0x01U << 2) /* (RW) TC3 output compare B match interrupt enable */
#define CHIP_REGFLD_TIMSK3_OCIE3A           (0x01U << 1) /* (RW) TC3 output compare A match interrupt enable */
#define CHIP_REGFLD_TIMSK3_TOIE3            (0x01U << 0) /* (RW) TC3 overflow interrupt enable bit */

/* ---------------- TIFR3 - TC3 Interrupt Flag Register ----------------------*/
#define CHIP_REGFLD_TIFR3_ICF3              (0x01U << 5) /* (RW1C) Input capture flag (not self cleared!) */
#define CHIP_REGFLD_TIFR3_OCF3C             (0x01U << 3) /* (RW1C) TC3 output compare C match flag (not self cleared!) */
#define CHIP_REGFLD_TIFR3_OCF3B             (0x01U << 2) /* (RW1C) TC3 output compare B match flag (not self cleared!) */
#define CHIP_REGFLD_TIFR3_OCF3A             (0x01U << 1) /* (RW1C) TC3 output compare A match flag (not self cleared!) */
#define CHIP_REGFLD_TIFR3_TOV3              (0x01U << 0) /* (RW1C) TC3 overflow flag (not self cleared!) */


#endif

#endif // REGS_TMR0_H

