#ifndef CHIP_DEV_SPEC_FEATURES_H
#define CHIP_DEV_SPEC_FEATURES_H

#include "chip_pkg_types.h"

#define CHIP_DEV_PREFIX_APM 
#define CHIP_DEV_PKG                             CHIP_PKG_LQFP48
#define CHIP_FLASH_MEM_SIZE                      (128*1024)

#endif // CHIP_DEV_SPEC_FEATURES_H
