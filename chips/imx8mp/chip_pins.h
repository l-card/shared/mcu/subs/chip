#ifndef CHIP_PINS_H
#define CHIP_PINS_H

#include "chip_pin_defs.h"



/* GPIO1_IO00, BALL A7, NVCC_GPIO (RU_29, 3.3V, LVDS_CTP_INT) */
#define CHIP_PIN_IO1_0_GPIO                             CHIP_PIN_ID(0,  0,   0, 0,   9, -1,-1)
#define CHIP_PIN_IO1_0_CCM_ENET_PHY_REF_CLK_ROOT        CHIP_PIN_ID(0,  0,   0, 1,   9, -1,-1)
#define CHIP_PIN_IO1_0_ISP_FL_TRIG0                     CHIP_PIN_ID(0,  0,   0, 3,   9, 69, 0)
#define CHIP_PIN_IO1_0_ISP_REF_CLK_32K                  CHIP_PIN_ID(0,  0,   0, 5,   9, -1,-1)
#define CHIP_PIN_IO1_0_CCM_EXT_CLK1                     CHIP_PIN_ID(0,  0,   0, 6,   9, -1,-1)


/* GPIO1_IO01, BALL E8, NVCC_GPIO, RST OUTL (RU_43, 3.3V, DSI_BL_PWM) */
#define CHIP_PIN_IO1_1_GPIO                             CHIP_PIN_ID(0,  1,   1, 0,  10, -1,-1)
#define CHIP_PIN_IO1_1_PWM1_OUT                         CHIP_PIN_ID(0,  1,   1, 1,  10, -1,-1)
#define CHIP_PIN_IO1_1_ISP_SHUTTER_TRIG0                CHIP_PIN_ID(0,  1,   1, 3,  10, 71, 0)
#define CHIP_PIN_IO1_1_REF_CLK_24M                      CHIP_PIN_ID(0,  1,   1, 5,  10, -1,-1)
#define CHIP_PIN_IO1_1_ALT6_CCM_EXT_CLK2                CHIP_PIN_ID(0,  1,   1, 6,  10, -1,-1)

/* GPIO1_IO02, BALL B6, NVCC_GPIO (RU_37, 3.3V, WDOG_B) */
#define CHIP_PIN_IO1_2_GPIO                             CHIP_PIN_ID(0,  2,   2, 0,  11, -1,-1)
#define CHIP_PIN_IO1_2_WDOG1_WDOG_B                     CHIP_PIN_ID(0,  2,   2, 1,  11, -1,-1)
#define CHIP_PIN_IO1_2_ISP_FLASH_TRIG0                  CHIP_PIN_ID(0,  2,   2, 3,  11, -1,-1)
#define CHIP_PIN_IO1_2_WDOG1_WDOG_ANY                   CHIP_PIN_ID(0,  2,   2, 5,  11, -1,-1)
#define CHIP_PIN_IO1_2_SJC_DE_B                         CHIP_PIN_ID(0,  2,   2, 7,  11, -1,-1)

/* GPIO1_IO03, BALL D6, NVCC_GPIO (x, PMIC INTn) */
#define CHIP_PIN_IO1_3_GPIO                             CHIP_PIN_ID(0,  3,   3, 0,  12, -1,-1)
#define CHIP_PIN_IO1_3_USDHC1_VSELECT                   CHIP_PIN_ID(0,  3,   3, 1,  12, -1,-1)
#define CHIP_PIN_IO1_3_ISP_PRELIGHT_TRIG0               CHIP_PIN_ID(0,  3,   3, 3,  12, -1,-1)
#define CHIP_PIN_IO1_3_SDMA1_EXT_EVENT0                 CHIP_PIN_ID(0,  3,   3, 5,  12, -1,-1)

/* GPIO1_IO04, BALL E6, NVCC_GPIO (x, PMIC SD_VSEL) */
#define CHIP_PIN_IO1_4_GPIO                             CHIP_PIN_ID(0,  4,   4, 0,  13, -1,-1)
#define CHIP_PIN_IO1_4_USDHC2_VSELECT                   CHIP_PIN_ID(0,  4,   4, 1,  13, -1,-1)
#define CHIP_PIN_IO1_4_ISP_SHUTTER_OPEN0                CHIP_PIN_ID(0,  4,   4, 3,  13, -1,-1)
#define CHIP_PIN_IO1_4_SDMA1_EXT_EVENT1                 CHIP_PIN_ID(0,  4,   4, 5,  13, -1,-1)

/* GPIO1_IO05, BALL B4, NVCC_GPIO, RST OUTH (RU_17, 3.3V, CSI1_SYNC3.3) */
#define CHIP_PIN_IO1_5_GPIO                             CHIP_PIN_ID(0,  5,   5, 0,  14, -1,-1)
#define CHIP_PIN_IO1_5_M7_NMI                           CHIP_PIN_ID(0,  5,   5, 1,  14, -1,-1)
#define CHIP_PIN_IO1_5_ISP_FL_TRIG1                     CHIP_PIN_ID(0,  5,   5, 3,  14, 70, 0)
#define CHIP_PIN_IO1_5_CCM_PMIC_READY                   CHIP_PIN_ID(0,  5,   5, 5,  14, 37, 0)

/* GPIO1_IO06, BALL A3, NVCC_GPIO (RU_15, 3.3V, CSI1_nRST3.3) */
#define CHIP_PIN_IO1_6_GPIO                             CHIP_PIN_ID(0,  6,   6, 0,  15, -1,-1)
#define CHIP_PIN_IO1_6_ENET_QOS_MDC                     CHIP_PIN_ID(0,  6,   6, 1,  15, -1,-1)
#define CHIP_PIN_IO1_6_ISP_SHUTTER_TRIG1                CHIP_PIN_ID(0,  6,   6, 3,  15, 72, 0)
#define CHIP_PIN_IO1_6_USDHC1_CD_B                      CHIP_PIN_ID(0,  6,   6, 5,  15, -1,-1)
#define CHIP_PIN_IO1_6_CCM_EXT_CLK3                     CHIP_PIN_ID(0,  6,   6, 6,  15, -1,-1)

/* GPIO1_IO07, BALL F6, NVCC_GPIO (RU_39, 3.3V, DSI_EN) */
#define CHIP_PIN_IO1_7_GPIO                             CHIP_PIN_ID(0,  7,   7, 0,  16, -1,-1)
#define CHIP_PIN_IO1_7_ENET_QOS_MDIO                    CHIP_PIN_ID(0,  7,   7, 1,  16, 52, 0)
#define CHIP_PIN_IO1_7_ISP_FLASH_TRIG1                  CHIP_PIN_ID(0,  7,   7, 3,  16, -1,-1)
#define CHIP_PIN_IO1_7_USDHC1_WP                        CHIP_PIN_ID(0,  7,   7, 5,  16, -1,-1)
#define CHIP_PIN_IO1_7_CCM_EXT_CLK4                     CHIP_PIN_ID(0,  7,   7, 6,  16, -1,-1)

/* GPIO1_IO08, BALL A8, NVCC_GPIO (RU_23, 3.3V, PCIE_RST) */
#define CHIP_PIN_IO1_8_GPIO                             CHIP_PIN_ID(0,  8,   8, 0,  17, -1,-1)
#define CHIP_PIN_IO1_8_ENET_QOS_1588_EVENT0_IN          CHIP_PIN_ID(0,  8,   8, 1,  17, -1,-1)
#define CHIP_PIN_IO1_8_PWM1_OUT                         CHIP_PIN_ID(0,  8,   8, 2,  17, -1,-1)
#define CHIP_PIN_IO1_8_ISP_PRELIGHT_TRIG1               CHIP_PIN_ID(0,  8,   8, 3,  17, -1,-1)
#define CHIP_PIN_IO1_8_ENET_QOS_1588_EVENT0_AUX_IN      CHIP_PIN_ID(0,  8,   8, 4,  17, -1,-1)
#define CHIP_PIN_IO1_8_USDHC2_RESET_B                   CHIP_PIN_ID(0,  8,   8, 5,  17, -1,-1)

/* GPIO1_IO09, BALL B8, NVCC_GPIO (RU_27, 3.3V, ACC_INT) */
#define CHIP_PIN_IO1_9_GPIO                             CHIP_PIN_ID(0,  9,   9, 0,  18, -1,-1)
#define CHIP_PIN_IO1_9_ENET_QOS_1588_EVENT0_OUT         CHIP_PIN_ID(0,  9,   9, 1,  18, -1,-1)
#define CHIP_PIN_IO1_9_PWM2_OUT                         CHIP_PIN_ID(0,  9,   9, 2,  18, -1,-1)
#define CHIP_PIN_IO1_9_ISP_SHUTTER_OPEN1                CHIP_PIN_ID(0,  9,   9, 3,  18, -1,-1)
#define CHIP_PIN_IO1_9_USDHC3_RESET_B                   CHIP_PIN_ID(0,  9,   9, 4,  18, -1,-1)
#define CHIP_PIN_IO1_9_SDMA2_EXT_EVENT0                 CHIP_PIN_ID(0,  9,   9, 5,  18, -1,-1)

/* GPIO1_IO10, BALL B7, NVCC_GPIO (RU_33, 3.3V, LVDS_CTP_RS) */
#define CHIP_PIN_IO1_10_GPIO                            CHIP_PIN_ID(0, 10,  10, 0,  19, -1,-1)
#define CHIP_PIN_IO1_10_USB1_ID                         CHIP_PIN_ID(0, 10,  10, 1,  19, -1,-1)
#define CHIP_PIN_IO1_10_PWM3_OUT                        CHIP_PIN_ID(0, 10,  10, 2,  19, -1,-1)

/* GPIO1_IO11, BALL D8, NVCC_GPIO (RU_41, 3.3V, LVDS_PWM) */
#define CHIP_PIN_IO1_11_GPIO                            CHIP_PIN_ID(0, 11,  11, 0,  20, -1,-1)
#define CHIP_PIN_IO1_11_USB2_ID                         CHIP_PIN_ID(0, 11,  11, 1,  20, -1,-1)
#define CHIP_PIN_IO1_11_PWM2_OUT                        CHIP_PIN_ID(0, 11,  11, 2,  20, -1,-1)
#define CHIP_PIN_IO1_11_USDHC3_VSELECT                  CHIP_PIN_ID(0, 11,  11, 4,  20, -1,-1)
#define CHIP_PIN_IO1_11_CCM_PMIC_READY                  CHIP_PIN_ID(0, 11,  11, 5,  20, 37, 1)

/* GPIO1_IO12, BALL A5, NVCC_GPIO (RU_25, 3.3V, TP_INT) */
#define CHIP_PIN_IO1_12_GPIO                            CHIP_PIN_ID(0, 12,  12, 0,  21, -1,-1)
#define CHIP_PIN_IO1_12_USB1_PWR                        CHIP_PIN_ID(0, 12,  12, 1,  21, -1,-1)
#define CHIP_PIN_IO1_12_SDMA2_EXT_EVENT1                CHIP_PIN_ID(0, 12,  12, 5,  21, -1,-1)

/* GPIO1_IO13, BALL A6, NVCC_GPIO (RU_35, 3.3V, LVDS_PWR_EN) */
#define CHIP_PIN_IO1_13_GPIO                            CHIP_PIN_ID(0, 13,  13, 0,  22, -1,-1)
#define CHIP_PIN_IO1_13_USB1_OC                         CHIP_PIN_ID(0, 13,  13, 1,  22, -1,-1)
#define CHIP_PIN_IO1_13_PWM2_OUT                        CHIP_PIN_ID(0, 13,  13, 5,  22, -1,-1)

/* GPIO1_IO14, BALL A4, NVCC_GPIO (RU_19, 3.3V, TYPEC_HOST_EN) */
#define CHIP_PIN_IO1_14_GPIO                            CHIP_PIN_ID(0, 14,  14, 0,  23, -1,-1)
#define CHIP_PIN_IO1_14_USB2_PWR                        CHIP_PIN_ID(0, 14,  14, 1,  23, -1,-1)
#define CHIP_PIN_IO1_14_USDHC3_CD_B                     CHIP_PIN_ID(0, 14,  14, 4,  23, 82, 0)
#define CHIP_PIN_IO1_14_PWM3_OUT                        CHIP_PIN_ID(0, 14,  14, 5,  23, -1,-1)
#define CHIP_PIN_IO1_14_CCM_CLKO1                       CHIP_PIN_ID(0, 14,  14, 6,  23, -1,-1)

/* GPIO1_IO15, BALL B5, NVCC_GPIO (RU_21, 3.3V, CSI_MCLK3.3) */
#define CHIP_PIN_IO1_15_GPIO                            CHIP_PIN_ID(0, 15,  15, 0,  24, -1,-1)
#define CHIP_PIN_IO1_15_USB2_OC                         CHIP_PIN_ID(0, 15,  15, 1,  24, -1,-1)
#define CHIP_PIN_IO1_15_USDHC3_WP                       CHIP_PIN_ID(0, 15,  15, 4,  24, 93, 0)
#define CHIP_PIN_IO1_15_PWM4_OUT                        CHIP_PIN_ID(0, 15,  15, 5,  24, -1,-1)
#define CHIP_PIN_IO1_15_CCM_CLKO2                       CHIP_PIN_ID(0, 15,  15, 6,  24, -1,-1)

/* GPIO1_IO16 (ENET_MDC), BALL AH28, NVCC_ENET (LD_45, 1.8V, ENET_MDC) */
#define CHIP_PIN_IO1_16_ENET_QOS_MDC                    CHIP_PIN_ID(0, 16,  16, 0,  25, -1,-1)
#define CHIP_PIN_IO1_16_SAI6_TX_DATA0                   CHIP_PIN_ID(0, 16,  16, 2,  25, -1,-1)
#define CHIP_PIN_IO1_16_GPIO                            CHIP_PIN_ID(0, 16,  16, 5,  25, -1,-1)
#define CHIP_PIN_IO1_16_USDHC3_STROBE                   CHIP_PIN_ID(0, 16,  16, 6,  25, 92, 0)

/* GPIO1_IO17 (ENET_MDIO), BALL AH29, NVCC_ENET (LD_47, 1.8V, ENET_MDIO) */
#define CHIP_PIN_IO1_17_ENET_QOS_MDIO                   CHIP_PIN_ID(0, 17,  17, 0,  26, 52, 1)
#define CHIP_PIN_IO1_17_SAI6_TX_SYNC                    CHIP_PIN_ID(0, 17,  17, 2,  26, 26, 0)
#define CHIP_PIN_IO1_17_PDM_BIT_STREAM3                 CHIP_PIN_ID(0, 17,  17, 3,  26,  3, 0)
#define CHIP_PIN_IO1_17_GPIO                            CHIP_PIN_ID(0, 17,  17, 5,  26, -1,-1)
#define CHIP_PIN_IO1_17_USDHC3_DATA5                    CHIP_PIN_ID(0, 17,  17, 6,  26, 89, 0)

/* GPIO1_IO18 (ENET_TD3), BALL AD24, NVCC_ENET (LD_49, 1.8V, ENET_TD3) */
#define CHIP_PIN_IO1_18_ENET_QOS_RGMII_TD3              CHIP_PIN_ID(0, 18,  18, 0,  27, -1,-1)
#define CHIP_PIN_IO1_18_SAI6_TX_BCLK                    CHIP_PIN_ID(0, 18,  18, 2,  27, 25, 0)
#define CHIP_PIN_IO1_18_PDM_BIT_STREAM2                 CHIP_PIN_ID(0, 18,  18, 3,  27,  2, 0)
#define CHIP_PIN_IO1_18_GPIO                            CHIP_PIN_ID(0, 18,  18, 5,  27, -1,-1)
#define CHIP_PIN_IO1_18_USDHC3_DATA6                    CHIP_PIN_ID(0, 18,  18, 6,  27, 90, 0)

/* GPIO1_IO19 (ENET_TD2), BALL AF26, NVCC_ENET (LD_51, 1.8V, ENET_TD2) */
#define CHIP_PIN_IO1_19_ENET_QOS_RGMII_TD2              CHIP_PIN_ID(0, 19,  19, 0,  28, -1,-1)
#define CHIP_PIN_IO1_19_CCM_ENET_QOS_CLOCKGEN_REF_CLK   CHIP_PIN_ID(0, 19,  19, 1,  28, -1,-1)
#define CHIP_PIN_IO1_19_SAI6_RX_DATA0                   CHIP_PIN_ID(0, 19,  19, 2,  28, 23, 0)
#define CHIP_PIN_IO1_19_PDM_BIT_STREAM1                 CHIP_PIN_ID(0, 19,  19, 3,  28,  1, 0)
#define CHIP_PIN_IO1_19_GPIO                            CHIP_PIN_ID(0, 19,  19, 5,  28, -1,-1)
#define CHIP_PIN_IO1_19_USDHC3_DATA7                    CHIP_PIN_ID(0, 19,  19, 6,  28, 91, 0)

/* GPIO1_IO20 (ENET_TD1), BALL AE26, NVCC_ENET (LD_53, 1.8V, ENET_TD1) */
#define CHIP_PIN_IO1_20_ENET_QOS_RGMII_TD1              CHIP_PIN_ID(0, 20,  20, 0,  29, -1,-1)
#define CHIP_PIN_IO1_20_SAI6_RX_SYNC                    CHIP_PIN_ID(0, 20,  20, 2,  29, 24, 0)
#define CHIP_PIN_IO1_20_PDM_BIT_STREAM0                 CHIP_PIN_ID(0, 20,  20, 3,  29,  0, 0)
#define CHIP_PIN_IO1_20_GPIO                            CHIP_PIN_ID(0, 20,  20, 5,  29, -1,-1)
#define CHIP_PIN_IO1_20_USDHC3_CD_B                     CHIP_PIN_ID(0, 20,  20, 6,  29, 82, 1)

/* GPIO1_IO21 (ENET_TD0), BALL AC25, NVCC_ENET (LD_55, 1.8V, ENET_TD0) */
#define CHIP_PIN_IO1_21_ENET_QOS_RGMII_TD0              CHIP_PIN_ID(0, 21,  21, 0,  30, -1,-1)
#define CHIP_PIN_IO1_21_SAI6_RX_BCLK                    CHIP_PIN_ID(0, 21,  21, 2,  30, 22, 0)
#define CHIP_PIN_IO1_21_PDM_CLK                         CHIP_PIN_ID(0, 21,  21, 3,  30, -1,-1)
#define CHIP_PIN_IO1_21_GPIO                            CHIP_PIN_ID(0, 21,  21, 5,  30, -1,-1)
#define CHIP_PIN_IO1_21_USDHC3_WP                       CHIP_PIN_ID(0, 21,  21, 6,  30, 93, 1)

/* GPIO1_IO22 (ENET_TX_CTL), BALL AF24, NVCC_ENET (LD_61, 1.8V, ENET_TX_CTL) */
#define CHIP_PIN_IO1_22_ENET_QOS_RGMII_TX_CTL           CHIP_PIN_ID(0, 22,  22, 0,  31, -1,-1)
#define CHIP_PIN_IO1_22_SAI6_MCLK                       CHIP_PIN_ID(0, 22,  22, 2,  31, 21, 0)
#define CHIP_PIN_IO1_22_SPDIF1_OUT                      CHIP_PIN_ID(0, 22,  22, 3,  31, -1,-1)
#define CHIP_PIN_IO1_22_GPIO                            CHIP_PIN_ID(0, 22,  22, 5,  31, -1,-1)
#define CHIP_PIN_IO1_22_USDHC3_DATA0                    CHIP_PIN_ID(0, 22,  22, 6,  31, 84, 0)

/* GPIO1_IO23 (ENET_TXC), BALL AE24, NVCC_ENET (LD_59, 1.8V, ENET_TXC) */
#define CHIP_PIN_IO1_23_CCM_ENET_QOS_CLOCKGEN_TX_CLK    CHIP_PIN_ID(0, 23,  23, 0,  32, -1,-1)
#define CHIP_PIN_IO1_23_ENET_QOS_TX_ER                  CHIP_PIN_ID(0, 23,  23, 1,  32, -1,-1)
#define CHIP_PIN_IO1_23_SAI7_TX_DATA0                   CHIP_PIN_ID(0, 23,  23, 2,  32, -1,-1)
#define CHIP_PIN_IO1_23_GPIO                            CHIP_PIN_ID(0, 23,  23, 5,  32, -1,-1)
#define CHIP_PIN_IO1_23_USDHC3_DATA1                    CHIP_PIN_ID(0, 23,  23, 6,  32, 85, 0)

/* GPIO1_IO24 (ENET_RX_CTL), BALL AE28, NVCC_ENET (LD_67, 1.8V, ENET_RX_CTL) */
#define CHIP_PIN_IO1_24_ENET_QOS_RGMII_RX_CTL           CHIP_PIN_ID(0, 24,  24, 0,  33, -1,-1)
#define CHIP_PIN_IO1_24_SAI7_TX_SYNC                    CHIP_PIN_ID(0, 24,  24, 2,  33, 32, 0)
#define CHIP_PIN_IO1_24_PDM_BIT_STREAM3                 CHIP_PIN_ID(0, 24,  24, 3,  33,  3, 1)
#define CHIP_PIN_IO1_24_GPIO                            CHIP_PIN_ID(0, 24,  24, 5,  33, -1,-1)
#define CHIP_PIN_IO1_24_USDHC3_DATA2                    CHIP_PIN_ID(0, 24,  24, 6,  33, 86, 0)

/* GPIO1_IO25 (ENET_RXC), BALL AE29, NVCC_ENET (LD_65, 1.8V, ENET_RXC) */
#define CHIP_PIN_IO1_25_CCM_ENET_QOS_CLOCKGEN_RX_CLK    CHIP_PIN_ID(0, 25,  25, 0,  34, -1,-1)
#define CHIP_PIN_IO1_25_ENET_QOS_RX_ER                  CHIP_PIN_ID(0, 25,  25, 1,  34, -1,-1)
#define CHIP_PIN_IO1_25_SAI7_TX_BCLK                    CHIP_PIN_ID(0, 25,  25, 2,  34, 31, 0)
#define CHIP_PIN_IO1_25_PDM_BIT_STREAM2                 CHIP_PIN_ID(0, 25,  25, 3,  34,  2, 1)
#define CHIP_PIN_IO1_25_GPIO                            CHIP_PIN_ID(0, 25,  25, 5,  34, -1,-1)
#define CHIP_PIN_IO1_25_USDHC3_DATA3                    CHIP_PIN_ID(0, 25,  25, 6,  34, 87, 0)

/* GPIO1_IO26 (ENET_RD0), BALL AG29, NVCC_ENET (LD_71, 1.8V, ENET_RD0) */
#define CHIP_PIN_IO1_26_ENET_QOS_RGMII_RD0              CHIP_PIN_ID(0, 26,  26, 0,  35, -1,-1)
#define CHIP_PIN_IO1_26_SAI7_RX_DATA0                   CHIP_PIN_ID(0, 26,  26, 2,  35, 29, 0)
#define CHIP_PIN_IO1_26_PDM_BIT_STREAM1                 CHIP_PIN_ID(0, 26,  26, 3,  35,  1, 1)
#define CHIP_PIN_IO1_26_GPIO                            CHIP_PIN_ID(0, 26,  26, 5,  35, -1,-1)
#define CHIP_PIN_IO1_26_USDHC3_DATA4                    CHIP_PIN_ID(0, 26,  26, 6,  35, 88, 0)

/* GPIO1_IO27 (ENET_RD1), BALL AG28, NVCC_ENET (LD_73, 1.8V, ENET_RD1) */
#define CHIP_PIN_IO1_27_ENET_QOS_RGMII_RD1              CHIP_PIN_ID(0, 27,  27, 0,  36, -1,-1)
#define CHIP_PIN_IO1_27_SAI7_RX_SYNC                    CHIP_PIN_ID(0, 27,  27, 2,  36, 30, 0)
#define CHIP_PIN_IO1_27_PDM_BIT_STREAM0                 CHIP_PIN_ID(0, 27,  27, 3,  36,  0, 1)
#define CHIP_PIN_IO1_27_GPIO                            CHIP_PIN_ID(0, 27,  27, 5,  36, -1,-1)
#define CHIP_PIN_IO1_27_USDHC3_RESET_B                  CHIP_PIN_ID(0, 27,  27, 6,  36, -1,-1)

/* GPIO1_IO28 (ENET_RD2), BALL AF29, NVCC_ENET (LD_75, 1.8V, ENET_RD2) */
#define CHIP_PIN_IO1_28_ENET_QOS_RGMII_RD2              CHIP_PIN_ID(0, 28,  28, 0,  37, -1,-1)
#define CHIP_PIN_IO1_28_SAI7_RX_BCLK                    CHIP_PIN_ID(0, 28,  28, 2,  37, 28, 0)
#define CHIP_PIN_IO1_28_PDM_CLK                         CHIP_PIN_ID(0, 28,  28, 3,  37, -1,-1)
#define CHIP_PIN_IO1_28_GPIO                            CHIP_PIN_ID(0, 28,  28, 5,  37, -1,-1)
#define CHIP_PIN_IO1_28_USDHC3_CLK                      CHIP_PIN_ID(0, 28,  28, 6,  37, 81, 0)

/* GPIO1_IO29 (ENET_RD3), BALL AF28, NVCC_ENET (LD_77, 1.8V, ENET_RD3) */
#define CHIP_PIN_IO1_29_ENET_QOS_RGMII_RD3              CHIP_PIN_ID(0, 29,  29, 0,  38, -1,-1)
#define CHIP_PIN_IO1_29_SAI7_MCLK                       CHIP_PIN_ID(0, 29,  29, 2,  38, 27, 0)
#define CHIP_PIN_IO1_29_SPDIF1_IN                       CHIP_PIN_ID(0, 29,  29, 3,  38, 33, 0)
#define CHIP_PIN_IO1_29_GPIO                            CHIP_PIN_ID(0, 29,  29, 5,  38, -1,-1)
#define CHIP_PIN_IO1_29_USDHC3_CMD                      CHIP_PIN_ID(0, 29,  29, 6,  38, 83, 0)


/* GPIO2_IO00 (SD1_CLK), BALL W28, NVCC_SD1 (LD_74, 1.8V, SD1_CLK) */
#define CHIP_PIN_IO2_0_USDHC1_CLK                       CHIP_PIN_ID(1,  0,  30, 0,  39, -1,-1)
#define CHIP_PIN_IO2_0_ENET1_MDC                        CHIP_PIN_ID(1,  0,  30, 1,  39, -1,-1)
#define CHIP_PIN_IO2_0_I2C5_SCL                         CHIP_PIN_ID(1,  0,  30, 3,  39, 65, 0)
#define CHIP_PIN_IO2_0_UART1_TX                         CHIP_PIN_ID(1,  0,  30, 4,  39, -1,-1)
#define CHIP_PIN_IO2_0_UART1_DTE_RX                     CHIP_PIN_ID(1,  0,  30, 4,  39, 74, 0)
#define CHIP_PIN_IO2_0_GPIO                             CHIP_PIN_ID(1,  0,  30, 5,  39, -1,-1)

/* GPIO2_IO01 (SD1_CMD), BALL W29, NVCC_SD1 (LD_70, 1.8V, SD1_CMD) */
#define CHIP_PIN_IO2_1_USDHC1_CMD                       CHIP_PIN_ID(1,  1,  31, 0,  40, -1,-1)
#define CHIP_PIN_IO2_1_ENET1_MDIO                       CHIP_PIN_ID(1,  1,  31, 1,  40, 47, 0)
#define CHIP_PIN_IO2_1_I2C5_SDA                         CHIP_PIN_ID(1,  1,  31, 3,  40, 66, 0)
#define CHIP_PIN_IO2_1_UART1_RX                         CHIP_PIN_ID(1,  1,  31, 4,  40, 74, 1)
#define CHIP_PIN_IO2_1_UART1_DTE_TX                     CHIP_PIN_ID(1,  1,  31, 4,  40, -1,-1)
#define CHIP_PIN_IO2_1_GPIO                             CHIP_PIN_ID(1,  1,  31, 5,  40, -1,-1)

/* GPIO2_IO02 (SD1_DATA0), BALL Y29, NVCC_SD1 (LD_76, 1.8V, SD1_DATA0) */
#define CHIP_PIN_IO2_2_USDHC1_DATA0                     CHIP_PIN_ID(1,  2,  32, 0,  41, -1,-1)
#define CHIP_PIN_IO2_2_ENET1_RGMII_TD1                  CHIP_PIN_ID(1,  2,  32, 1,  41, -1,-1)
#define CHIP_PIN_IO2_2_I2C6_SCL                         CHIP_PIN_ID(1,  2,  32, 3,  41, 67, 0)
#define CHIP_PIN_IO2_2_UART1_RTS_B                      CHIP_PIN_ID(1,  2,  32, 4,  41, 73, 0)
#define CHIP_PIN_IO2_2_UART1_DTE_CTS_B                  CHIP_PIN_ID(1,  2,  32, 4,  41, -1,-1)
#define CHIP_PIN_IO2_2_GPIO                             CHIP_PIN_ID(1,  2,  32, 5,  41, -1,-1)

/* GPIO2_IO03 (SD1_DATA1), BALL Y28, NVCC_SD1 (LD_78, 1.8V, SD1_DATA1) */
#define CHIP_PIN_IO2_3_USDHC1_DATA1                     CHIP_PIN_ID(1,  3,  33, 0,  42, -1,-1)
#define CHIP_PIN_IO2_3_ENET1_RGMII_TD0                  CHIP_PIN_ID(1,  3,  33, 1,  42, -1,-1)
#define CHIP_PIN_IO2_3_I2C6_SDA                         CHIP_PIN_ID(1,  3,  33, 3,  42, 68, 0)
#define CHIP_PIN_IO2_3_UART1_CTS_B                      CHIP_PIN_ID(1,  3,  33, 4,  42, -1,-1)
#define CHIP_PIN_IO2_3_UART1_DTE_RTS_B                  CHIP_PIN_ID(1,  3,  33, 4,  42, 73, 1)
#define CHIP_PIN_IO2_3_GPIO                             CHIP_PIN_ID(1,  3,  33, 5,  42, -1,-1)

/* GPIO2_IO04 (SD1_DATA2), BALL V29, NVCC_SD1 (LD_66, 1.8V, SD1_DATA2) */
#define CHIP_PIN_IO2_4_USDHC1_DATA2                     CHIP_PIN_ID(1,  4,  34, 0,  43, -1,-1)
#define CHIP_PIN_IO2_4_ENET1_RGMII_RD0                  CHIP_PIN_ID(1,  4,  34, 1,  43, 48, 0)
#define CHIP_PIN_IO2_4_I2C4_SCL                         CHIP_PIN_ID(1,  4,  34, 3,  43, 63, 0)
#define CHIP_PIN_IO2_4_UART2_TX                         CHIP_PIN_ID(1,  4,  34, 4,  43, -1,-1)
#define CHIP_PIN_IO2_4_UART2_DTE_RX                     CHIP_PIN_ID(1,  4,  34, 4,  43, 76, 0)
#define CHIP_PIN_IO2_4_GPIO                             CHIP_PIN_ID(1,  4,  34, 5,  43, -1,-1)

/* GPIO2_IO05 (SD1_DATA3), BALL V28, NVCC_SD1 (LD_68, 1.8V, SD1_DATA3) */
#define CHIP_PIN_IO2_5_USDHC1_DATA3                     CHIP_PIN_ID(1,  5,  35, 0,  44, -1,-1)
#define CHIP_PIN_IO2_5_ENET1_RGMII_RD1                  CHIP_PIN_ID(1,  5,  35, 1,  44, 49, 0)
#define CHIP_PIN_IO2_5_I2C4_SDA                         CHIP_PIN_ID(1,  5,  35, 3,  44, 64, 0)
#define CHIP_PIN_IO2_5_UART2_RX                         CHIP_PIN_ID(1,  5,  35, 4,  44, 76, 1)
#define CHIP_PIN_IO2_5_UART2_DTE_TX                     CHIP_PIN_ID(1,  5,  35, 4,  44, -1,-1)
#define CHIP_PIN_IO2_5_GPIO                             CHIP_PIN_ID(1,  5,  35, 5,  44, -1,-1)

/* GPIO2_IO06 (SD1_DATA4), BALL U26, NVCC_SD1 (LD_52, 1.8V, BT_HOST_WAKE) */
#define CHIP_PIN_IO2_6_USDHC1_DATA4                     CHIP_PIN_ID(1,  6,  36, 0,  45, -1,-1)
#define CHIP_PIN_IO2_6_ENET1_RGMII_TX_CTL               CHIP_PIN_ID(1,  6,  36, 1,  45, -1,-1)
#define CHIP_PIN_IO2_6_I2C1_SCL                         CHIP_PIN_ID(1,  6,  36, 3,  45, 57, 0)
#define CHIP_PIN_IO2_6_UART2_RTS_B                      CHIP_PIN_ID(1,  6,  36, 4,  45, 75, 0)
#define CHIP_PIN_IO2_6_UART2_DTE_CTS_B                  CHIP_PIN_ID(1,  6,  36, 4,  45, -1,-1)
#define CHIP_PIN_IO2_6_GPIO                             CHIP_PIN_ID(1,  6,  36, 5,  45, -1,-1)

/* GPIO2_IO07 (SD1_DATA5), BALL AA29, NVCC_SD1 (LD_54, 1.8V, BT_WAKE_B) */
#define CHIP_PIN_IO2_7_USDHC1_DATA5                     CHIP_PIN_ID(1,  7,  37, 0,  46, -1,-1)
#define CHIP_PIN_IO2_7_ENET1_TX_ER                      CHIP_PIN_ID(1,  7,  37, 1,  46, -1,-1)
#define CHIP_PIN_IO2_7_I2C1_SDA                         CHIP_PIN_ID(1,  7,  37, 3,  46, 58, 0)
#define CHIP_PIN_IO2_7_UART2_CTS_B                      CHIP_PIN_ID(1,  7,  37, 4,  46, -1,-1)
#define CHIP_PIN_IO2_7_UART2_DTE_RTS_B                  CHIP_PIN_ID(1,  7,  37, 4,  46, 75, 1)
#define CHIP_PIN_IO2_7_GPIO                             CHIP_PIN_ID(1,  7,  37, 5,  46, -1,-1)

/* GPIO2_IO08 (SD1_DATA6), BALL AA28, NVCC_SD1 (LD_56, 1.8V, WIFI_REG_ON) */
#define CHIP_PIN_IO2_8_USDHC1_DATA6                     CHIP_PIN_ID(1,  8,  38, 0,  47, -1,-1)
#define CHIP_PIN_IO2_8_ENET1_RGMII_RX_CTL               CHIP_PIN_ID(1,  8,  38, 1,  47, 50, 0)
#define CHIP_PIN_IO2_8_I2C2_SCL                         CHIP_PIN_ID(1,  8,  38, 3,  47, 59, 0)
#define CHIP_PIN_IO2_8_UART3_TX                         CHIP_PIN_ID(1,  8,  38, 4,  47, -1,-1)
#define CHIP_PIN_IO2_8_UART3_DTE_RX                     CHIP_PIN_ID(1,  8,  38, 4,  47, 78, 0)
#define CHIP_PIN_IO2_8_GPIO                             CHIP_PIN_ID(1,  8,  38, 5,  47, -1,-1)

/* GPIO2_IO09 (SD1_DATA7), BALL U25, NVCC_SD1 (LD_58, 1.8V, WIFI_HOST_WAKE_B) */
#define CHIP_PIN_IO2_9_USDHC1_DATA7                     CHIP_PIN_ID(1,  9,  39, 0,  48, -1,-1)
#define CHIP_PIN_IO2_9_ENET1_RX_ER                      CHIP_PIN_ID(1,  9,  39, 1,  48, 51, 0)
#define CHIP_PIN_IO2_9_I2C2_SDA                         CHIP_PIN_ID(1,  9,  39, 3,  48, 60, 0)
#define CHIP_PIN_IO2_9_UART3_RX                         CHIP_PIN_ID(1,  9,  39, 4,  48, 78, 1)
#define CHIP_PIN_IO2_9_UART3_DTE_TX                     CHIP_PIN_ID(1,  9,  39, 4,  48, -1,-1)
#define CHIP_PIN_IO2_9_GPIO                             CHIP_PIN_ID(1,  9,  39, 5,  48, -1,-1)

/* GPIO2_IO10 (SD1_RESET_B), BALL W25, NVCC_SD1 (LD_64, 1.8V, BT_REG_ON) */
#define CHIP_PIN_IO2_10_USDHC1_RESET_B                  CHIP_PIN_ID(1, 10,  40, 0,  49, -1,-1)
#define CHIP_PIN_IO2_10_ENET1_TX_CLK                    CHIP_PIN_ID(1, 10,  40, 1,  49, 46, 0)
#define CHIP_PIN_IO2_10_I2C3_SCL                        CHIP_PIN_ID(1, 10,  40, 3,  49, 61, 0)
#define CHIP_PIN_IO2_10_UART3_RTS_B                     CHIP_PIN_ID(1, 10,  40, 4,  49, 77, 0)
#define CHIP_PIN_IO2_10_UART3_DTE_CTS_B                 CHIP_PIN_ID(1, 10,  40, 4,  49, -1,-1)
#define CHIP_PIN_IO2_10_GPIO                            CHIP_PIN_ID(1, 10,  40, 5,  49, -1,-1)

/* GPIO2_IO11 (SD1_STROBE), BALL W26, NVCC_SD1 (LD_62, 1.8V, CSI1_PWDN) */
#define CHIP_PIN_IO2_11_USDHC1_STROBE                   CHIP_PIN_ID(1, 11,  41, 0,  50, -1,-1)
#define CHIP_PIN_IO2_11_I2C3_SDA                        CHIP_PIN_ID(1, 11,  41, 3,  50, 62, 0)
#define CHIP_PIN_IO2_11_UART3_CTS_B                     CHIP_PIN_ID(1, 11,  41, 4,  50, -1,-1)
#define CHIP_PIN_IO2_11_UART3_DTE_RTS_B                 CHIP_PIN_ID(1, 11,  41, 4,  50, 77, 1)
#define CHIP_PIN_IO2_11_GPIO                            CHIP_PIN_ID(1, 11,  41, 5,  50, -1,-1)

/* GPIO2_IO12 (SD2_CD_B), BALL W26, NVCC_SD2 (LD_46, 1.8/3.3V, SD2_nCD) */
#define CHIP_PIN_IO2_12_USDHC2_CD_B                     CHIP_PIN_ID(1, 12,  42, 0,  51, -1,-1)
#define CHIP_PIN_IO2_12_GPIO                            CHIP_PIN_ID(1, 12,  42, 5,  51, -1,-1)

/* GPIO2_IO13 (SD2_CLK), BALL AB29, NVCC_SD2 (LD_38, 1.8/3.3V, SD2_CLK) */
#define CHIP_PIN_IO2_13_USDHC2_CLK                      CHIP_PIN_ID(1, 13,  43, 0,  52, -1,-1)
#define CHIP_PIN_IO2_13_ECSPI2_SCLK                     CHIP_PIN_ID(1, 13,  43, 2,  52, 42, 0)
#define CHIP_PIN_IO2_13_UART4_RX                        CHIP_PIN_ID(1, 13,  43, 3,  52, 80, 0)
#define CHIP_PIN_IO2_13_UART4_DTE_TX                    CHIP_PIN_ID(1, 13,  43, 3,  52, -1,-1)
#define CHIP_PIN_IO2_13_GPIO                            CHIP_PIN_ID(1, 13,  43, 4,  52, -1,-1)

/* GPIO2_IO14 (SD2_CMD), BALL AB28, NVCC_SD2 (LD_36, 1.8/3.3V, SD2_CMD) */
#define CHIP_PIN_IO2_14_USDHC2_CMD                      CHIP_PIN_ID(1, 14,  44, 0,  53, -1,-1)
#define CHIP_PIN_IO2_14_ECSPI2_MOSI                     CHIP_PIN_ID(1, 14,  44, 2,  53, 44, 0)
#define CHIP_PIN_IO2_14_UART4_TX                        CHIP_PIN_ID(1, 14,  44, 3,  53, -1,-1)
#define CHIP_PIN_IO2_14_UART4_DTE_RX                    CHIP_PIN_ID(1, 14,  44, 3,  53, 80, 1)
#define CHIP_PIN_IO2_14_PDM_CLK                         CHIP_PIN_ID(1, 14,  44, 4,  53, -1,-1)
#define CHIP_PIN_IO2_14_GPIO                            CHIP_PIN_ID(1, 14,  44, 5,  53, -1,-1)

/* GPIO2_IO15 (SD2_DATA0), BALL AC28, NVCC_SD2 (LD_40, 1.8/3.3V, SD2_DATA0) */
#define CHIP_PIN_IO2_15_USDHC2_DATA0                    CHIP_PIN_ID(1, 15,  45, 0,  54, -1,-1)
#define CHIP_PIN_IO2_15_I2C4_SDA                        CHIP_PIN_ID(1, 15,  45, 2,  54, 64, 1)
#define CHIP_PIN_IO2_15_UART2_RX                        CHIP_PIN_ID(1, 15,  45, 3,  54, 76, 2)
#define CHIP_PIN_IO2_15_UART2_DTE_TX                    CHIP_PIN_ID(1, 15,  45, 3,  54, -1,-1)
#define CHIP_PIN_IO2_15_PDM_BIT_STREAM0                 CHIP_PIN_ID(1, 15,  45, 4,  54,  0, 2)
#define CHIP_PIN_IO2_15_GPIO                            CHIP_PIN_ID(1, 15,  45, 5,  54, -1,-1)

/* GPIO2_IO16 (SD2_DATA1), BALL AC29, NVCC_SD2 (LD_42, 1.8/3.3V, SD2_DATA1) */
#define CHIP_PIN_IO2_16_USDHC2_DATA1                    CHIP_PIN_ID(1, 16,  46, 0,  55, -1,-1)
#define CHIP_PIN_IO2_16_I2C4_SCL                        CHIP_PIN_ID(1, 16,  46, 2,  55, 63, 1)
#define CHIP_PIN_IO2_16_UART2_TX                        CHIP_PIN_ID(1, 16,  46, 3,  55, -1,-1)
#define CHIP_PIN_IO2_16_UART2_DTE_RX                    CHIP_PIN_ID(1, 16,  46, 3,  55, 76, 3)
#define CHIP_PIN_IO2_16_PDM_BIT_STREAM1                 CHIP_PIN_ID(1, 16,  46, 4,  55,  1, 2)
#define CHIP_PIN_IO2_16_GPIO                            CHIP_PIN_ID(1, 16,  46, 5,  55, -1,-1)

/* GPIO2_IO17 (SD2_DATA2), BALL AA26, NVCC_SD2 (LD_32, 1.8/3.3V, SD2_DATA2) */
#define CHIP_PIN_IO2_17_USDHC2_DATA2                    CHIP_PIN_ID(1, 17,  47, 0,  56, -1,-1)
#define CHIP_PIN_IO2_17_ECSPI2_SS0                      CHIP_PIN_ID(1, 17,  47, 2,  56, 45, 0)
#define CHIP_PIN_IO2_17_SPDIF1_OUT                      CHIP_PIN_ID(1, 17,  47, 3,  56, -1,-1)
#define CHIP_PIN_IO2_17_PDM_BIT_STREAM2                 CHIP_PIN_ID(1, 17,  47, 4,  56,  2, 2)
#define CHIP_PIN_IO2_17_GPIO                            CHIP_PIN_ID(1, 17,  47, 5,  56, -1,-1)

/* GPIO2_IO18 (SD2_DATA3), BALL AA25, NVCC_SD2 (LD_34, 1.8/3.3V, SD2_DATA3) */
#define CHIP_PIN_IO2_18_USDHC2_DATA3                    CHIP_PIN_ID(1, 18,  48, 0,  57, -1,-1)
#define CHIP_PIN_IO2_18_ECSPI2_MISO                     CHIP_PIN_ID(1, 18,  48, 2,  57, 43, 0)
#define CHIP_PIN_IO2_18_SPDIF1_IN                       CHIP_PIN_ID(1, 18,  48, 3,  57, 33, 1)
#define CHIP_PIN_IO2_18_PDM_BIT_STREAM3                 CHIP_PIN_ID(1, 18,  48, 4,  57,  3, 2)
#define CHIP_PIN_IO2_18_GPIO                            CHIP_PIN_ID(1, 18,  48, 5,  57, -1,-1)

/* GPIO2_IO19 (SD2_RESET_B), BALL AD28, NVCC_SD2 (LD_44, 1.8/3.3V, x) */
#define CHIP_PIN_IO2_19_USDHC2_RESET_B                  CHIP_PIN_ID(1, 19,  49, 0,  58, -1,-1)
#define CHIP_PIN_IO2_19_GPIO                            CHIP_PIN_ID(1, 19,  49, 5,  58, -1,-1)

/* GPIO2_IO20 (SD2_WP), BALL AC26, NVCC_SD2 (LD_48, 1.8/3.3V, TYPEC_EN_B) */
#define CHIP_PIN_IO2_20_USDHC2_WP                       CHIP_PIN_ID(1, 20,  50, 0,  59, -1,-1)
#define CHIP_PIN_IO2_20_GPIO                            CHIP_PIN_ID(1, 20,  50, 5,  59, -1,-1)
#define CHIP_PIN_IO2_20_CORESIGHT_EVENTI                CHIP_PIN_ID(1, 20,  50, 6,  59, -1,-1)

/* GPIO3_IO0 (NAND_ALE), BALL N25, NVCC_NAND (x, QSPI SCLK) */
#define CHIP_PIN_IO3_0_NAND_ALE                         CHIP_PIN_ID(2,  0,  51, 0,  60, -1,-1)
#define CHIP_PIN_IO3_0_FLEXSPI_A_SCLK                   CHIP_PIN_ID(2,  0,  51, 1,  60, -1,-1)
#define CHIP_PIN_IO3_0_SAI3_TX_BCLK                     CHIP_PIN_ID(2,  0,  51, 2,  60, 10, 0)
#define CHIP_PIN_IO3_0_ISP_FL_TRIG0                     CHIP_PIN_ID(2,  0,  51, 3,  60, 69, 1)
#define CHIP_PIN_IO3_0_UART3_RX                         CHIP_PIN_ID(2,  0,  51, 4,  60, 78, 2)
#define CHIP_PIN_IO3_0_UART3_DTE_TX                     CHIP_PIN_ID(2,  0,  51, 4,  60, -1,-1)
#define CHIP_PIN_IO3_0_GPIO                             CHIP_PIN_ID(2,  0,  51, 5,  60, -1,-1)
#define CHIP_PIN_IO3_0_CORESIGHT_TRACE_CLK              CHIP_PIN_ID(2,  0,  51, 6,  60, -1,-1)

/* GPIO3_IO1 (NAND_CE0_B), BALL L26, NVCC_NAND (x, QSPI CS) */
#define CHIP_PIN_IO3_1_NAND_CE0_B                       CHIP_PIN_ID(2,  1,  52, 0,  61, -1,-1)
#define CHIP_PIN_IO3_1_FLEXSPI_A_SS0_B                  CHIP_PIN_ID(2,  1,  52, 1,  61, -1,-1)
#define CHIP_PIN_IO3_1_SAI3_TX_DATA0                    CHIP_PIN_ID(2,  1,  52, 2,  61, -1,-1)
#define CHIP_PIN_IO3_1_ISP_SHUTTER_TRIG0                CHIP_PIN_ID(2,  1,  52, 3,  61, 71, 1)
#define CHIP_PIN_IO3_1_UART3_TX                         CHIP_PIN_ID(2,  1,  52, 4,  61, -1,-1)
#define CHIP_PIN_IO3_1_UART3_DTE_RX                     CHIP_PIN_ID(2,  1,  52, 4,  61, 78, 3)
#define CHIP_PIN_IO3_1_GPIO                             CHIP_PIN_ID(2,  1,  52, 5,  61, -1,-1)
#define CHIP_PIN_IO3_1_CORESIGHT_TRACE_CTL              CHIP_PIN_ID(2,  1,  52, 6,  61, -1,-1)

/* GPIO3_IO2 (NAND_CE1_B), BALL T29, NVCC_NAND (x) */
#define CHIP_PIN_IO3_2_NAND_CE1_B                       CHIP_PIN_ID(2,  2,  53, 0,  62, -1,-1)
#define CHIP_PIN_IO3_2_FLEXSPI_A_SS1_B                  CHIP_PIN_ID(2,  2,  53, 1,  62, -1,-1)
#define CHIP_PIN_IO3_2_USDHC3_STROBE                    CHIP_PIN_ID(2,  2,  53, 2,  62, 92, 1)
#define CHIP_PIN_IO3_2_I2C4_SCL                         CHIP_PIN_ID(2,  2,  53, 4,  62, 63, 2)
#define CHIP_PIN_IO3_2_GPIO                             CHIP_PIN_ID(2,  2,  53, 5,  62, -1,-1)
#define CHIP_PIN_IO3_2_CORESIGHT_TRACE0                 CHIP_PIN_ID(2,  2,  53, 6,  62, -1,-1)

/* GPIO3_IO3 (NAND_CE2_B), BALL P28, NVCC_NAND (x, EMMC_DATA5) */
#define CHIP_PIN_IO3_3_NAND_CE2_B                       CHIP_PIN_ID(2,  3,  54, 0,  63, -1,-1)
#define CHIP_PIN_IO3_3_FLEXSPI_B_SS0_B                  CHIP_PIN_ID(2,  3,  54, 1,  63, -1,-1)
#define CHIP_PIN_IO3_3_USDHC3_DATA5                     CHIP_PIN_ID(2,  3,  54, 2,  63, 89, 1)
#define CHIP_PIN_IO3_3_I2C4_SDA                         CHIP_PIN_ID(2,  3,  54, 4,  63, 64, 2)
#define CHIP_PIN_IO3_3_GPIO                             CHIP_PIN_ID(2,  3,  54, 5,  63, -1,-1)
#define CHIP_PIN_IO3_3_CORESIGHT_TRACE1                 CHIP_PIN_ID(2,  3,  54, 6,  63, -1,-1)

/* GPIO3_IO4 (NAND_CE3_B), BALL N28, NVCC_NAND (x, EMMC_DATA6) */
#define CHIP_PIN_IO3_4_NAND_CE3_B                       CHIP_PIN_ID(2,  4,  55, 0,  64, -1,-1)
#define CHIP_PIN_IO3_4_FLEXSPI_B_SS1_B                  CHIP_PIN_ID(2,  4,  55, 1,  64, -1,-1)
#define CHIP_PIN_IO3_4_USDHC3_DATA6                     CHIP_PIN_ID(2,  4,  55, 2,  64, 90, 1)
#define CHIP_PIN_IO3_4_I2C3_SDA                         CHIP_PIN_ID(2,  4,  55, 4,  64, 62, 1)
#define CHIP_PIN_IO3_4_GPIO                             CHIP_PIN_ID(2,  4,  55, 5,  64, -1,-1)
#define CHIP_PIN_IO3_4_CORESIGHT_TRACE2                 CHIP_PIN_ID(2,  4,  55, 6,  64, -1,-1)

/* GPIO3_IO5 (NAND_CLE), BALL M28, NVCC_NAND (x, EMMC_DATA7) */
#define CHIP_PIN_IO3_5_NAND_CLE                         CHIP_PIN_ID(2,  5,  56, 0,  65, -1,-1)
#define CHIP_PIN_IO3_5_FLEXSPI_B_SCLK                   CHIP_PIN_ID(2,  5,  56, 1,  65, -1,-1)
#define CHIP_PIN_IO3_5_USDHC3_DATA7                     CHIP_PIN_ID(2,  5,  56, 2,  65, 91, 1)
#define CHIP_PIN_IO3_5_UART4_RX                         CHIP_PIN_ID(2,  5,  56, 4,  65, 80, 2)
#define CHIP_PIN_IO3_5_UART4_DTE_TX                     CHIP_PIN_ID(2,  5,  56, 4,  65, -1,-1)
#define CHIP_PIN_IO3_5_GPIO                             CHIP_PIN_ID(2,  5,  56, 5,  65, -1,-1)
#define CHIP_PIN_IO3_5_CORESIGHT_TRACE3                 CHIP_PIN_ID(2,  5,  56, 6,  65, -1,-1)

/* GPIO3_IO6 (NAND_DATA00), BALL R25, NVCC_NAND (x, QSPI_D0) */
#define CHIP_PIN_IO3_6_NAND_DATA00                      CHIP_PIN_ID(2,  6,  57, 0,  66, -1,-1)
#define CHIP_PIN_IO3_6_FLEXSPI_A_DATA0                  CHIP_PIN_ID(2,  6,  57, 1,  66, -1,-1)
#define CHIP_PIN_IO3_6_SAI3_RX_DATA0                    CHIP_PIN_ID(2,  6,  57, 2,  66,  9, 0)
#define CHIP_PIN_IO3_6_ISP_FLASH_TRIG0                  CHIP_PIN_ID(2,  6,  57, 3,  66, -1,-1)
#define CHIP_PIN_IO3_6_UART4_RX                         CHIP_PIN_ID(2,  6,  57, 4,  66, 80, 3)
#define CHIP_PIN_IO3_6_UART4_DTE_TX                     CHIP_PIN_ID(2,  6,  57, 4,  66, -1,-1)
#define CHIP_PIN_IO3_6_GPIO                             CHIP_PIN_ID(2,  6,  57, 5,  66, -1,-1)
#define CHIP_PIN_IO3_6_CORESIGHT_TRACE4                 CHIP_PIN_ID(2,  6,  57, 6,  66, -1,-1)

/* GPIO3_IO7 (NAND_DATA01), BALL L25, NVCC_NAND (x, QSPI_D1) */
#define CHIP_PIN_IO3_7_NAND_DATA01                      CHIP_PIN_ID(2,  7,  58, 0,  67, -1,-1)
#define CHIP_PIN_IO3_7_FLEXSPI_A_DATA1                  CHIP_PIN_ID(2,  7,  58, 1,  67, -1,-1)
#define CHIP_PIN_IO3_7_SAI3_TX_SYNC                     CHIP_PIN_ID(2,  7,  58, 2,  67, 11, 0)
#define CHIP_PIN_IO3_7_ISP_PRELIGHT_TRIG0               CHIP_PIN_ID(2,  7,  58, 3,  67, -1,-1)
#define CHIP_PIN_IO3_7_UART4_TX                         CHIP_PIN_ID(2,  7,  58, 4,  67, -1,-1)
#define CHIP_PIN_IO3_7_UART4_DTE_RX                     CHIP_PIN_ID(2,  7,  58, 4,  67, 80, 4)
#define CHIP_PIN_IO3_7_GPIO                             CHIP_PIN_ID(2,  7,  58, 5,  67, -1,-1)
#define CHIP_PIN_IO3_7_CORESIGHT_TRACE5                 CHIP_PIN_ID(2,  7,  58, 6,  67, -1,-1)

/* GPIO3_IO8 (NAND_DATA02), BALL L24, NVCC_NAND (x, QSPI_D2) */
#define CHIP_PIN_IO3_8_NAND_DATA02                      CHIP_PIN_ID(2,  8,  59, 0,  68, -1,-1)
#define CHIP_PIN_IO3_8_FLEXSPI_A_DATA2                  CHIP_PIN_ID(2,  8,  59, 1,  68, -1,-1)
#define CHIP_PIN_IO3_8_USDHC3_CD_B                      CHIP_PIN_ID(2,  8,  59, 2,  68, 82, 2)
#define CHIP_PIN_IO3_8_UART4_CTS_B                      CHIP_PIN_ID(2,  8,  59, 3,  68, -1,-1)
#define CHIP_PIN_IO3_8_UART4_DTE_RTS_B                  CHIP_PIN_ID(2,  8,  59, 3,  68, 79, 0)
#define CHIP_PIN_IO3_8_I2C4_SDA                         CHIP_PIN_ID(2,  8,  59, 4,  68, 64, 3)
#define CHIP_PIN_IO3_8_GPIO                             CHIP_PIN_ID(2,  8,  59, 5,  68, -1,-1)
#define CHIP_PIN_IO3_8_CORESIGHT_TRACE6                 CHIP_PIN_ID(2,  8,  59, 6,  68, -1,-1)

/* GPIO3_IO9 (NAND_DATA03), BALL N24, NVCC_NAND (x, QSPI_D3) */
#define CHIP_PIN_IO3_9_NAND_DATA03                      CHIP_PIN_ID(2,  9,  60, 0,  69, -1,-1)
#define CHIP_PIN_IO3_9_FLEXSPI_A_DATA3                  CHIP_PIN_ID(2,  9,  60, 1,  69, -1,-1)
#define CHIP_PIN_IO3_9_USDHC3_WP                        CHIP_PIN_ID(2,  9,  60, 2,  69, 93, 2)
#define CHIP_PIN_IO3_9_UART4_RTS_B                      CHIP_PIN_ID(2,  9,  60, 3,  69, 79, 1)
#define CHIP_PIN_IO3_9_UART4_DTE_CTS_B                  CHIP_PIN_ID(2,  9,  60, 3,  69, -1,-1)
#define CHIP_PIN_IO3_9_ISP_FL_TRIG1                     CHIP_PIN_ID(2,  9,  60, 4,  69, 70, 1)
#define CHIP_PIN_IO3_9_GPIO                             CHIP_PIN_ID(2,  9,  60, 5,  69, -1,-1)
#define CHIP_PIN_IO3_9_CORESIGHT_TRACE7                 CHIP_PIN_ID(2,  9,  60, 6,  69, -1,-1 )

/* GPIO3_IO10 (NAND_DATA04), BALL P29, NVCC_NAND (x, EMMC_DATA0) */
#define CHIP_PIN_IO3_10_NAND_DATA04                     CHIP_PIN_ID(2, 10,  61, 0,  70, -1,-1)
#define CHIP_PIN_IO3_10_FLEXSPI_B_DATA0                 CHIP_PIN_ID(2, 10,  61, 1,  70, -1,-1 )
#define CHIP_PIN_IO3_10_USDHC3_DATA0                    CHIP_PIN_ID(2, 10,  61, 2,  70, 84, 1)
#define CHIP_PIN_IO3_10_FLEXSPI_A_DATA4                 CHIP_PIN_ID(2, 10,  61, 3,  70, -1,-1 )
#define CHIP_PIN_IO3_10_ISP_SHUTTER_TRIG1               CHIP_PIN_ID(2, 10,  61, 4,  70, 72, 1)
#define CHIP_PIN_IO3_10_GPIO                            CHIP_PIN_ID(2, 10,  61, 5,  70, -1,-1)
#define CHIP_PIN_IO3_10_CORESIGHT_TRACE8                CHIP_PIN_ID(2, 10,  61, 6,  70, -1,-1)

/* GPIO3_IO11 (NAND_DATA05), BALL N29, NVCC_NAND (x, EMMC_DATA1) */
#define CHIP_PIN_IO3_11_NAND_DATA05                     CHIP_PIN_ID(2, 11,  62, 0,  71, -1,-1)
#define CHIP_PIN_IO3_11_FLEXSPI_B_DATA1                 CHIP_PIN_ID(2, 11,  62, 1,  71, -1,-1)
#define CHIP_PIN_IO3_11_USDHC3_DATA1                    CHIP_PIN_ID(2, 11,  62, 2,  71, 85, 1)
#define CHIP_PIN_IO3_11_FLEXSPI_A_DATA5                 CHIP_PIN_ID(2, 11,  62, 3,  71, -1,-1)
#define CHIP_PIN_IO3_11_ISP_FLASH_TRIG1                 CHIP_PIN_ID(2, 11,  62, 4,  71, -1,-1)
#define CHIP_PIN_IO3_11_GPIO                            CHIP_PIN_ID(2, 11,  62, 5,  71, -1,-1)
#define CHIP_PIN_IO3_11_CORESIGHT_TRACE9                CHIP_PIN_ID(2, 11,  62, 6,  71, -1,-1)

/* GPIO3_IO12 (NAND_DATA06), BALL M29, NVCC_NAND (x, EMMC_DATA2) */
#define CHIP_PIN_IO3_12_NAND_DATA06                     CHIP_PIN_ID(2, 12,  63, 0,  72, -1,-1)
#define CHIP_PIN_IO3_12_FLEXSPI_B_DATA2                 CHIP_PIN_ID(2, 12,  63, 1,  72, -1,-1)
#define CHIP_PIN_IO3_12_USDHC3_DATA2                    CHIP_PIN_ID(2, 12,  63, 2,  72, 86, 1)
#define CHIP_PIN_IO3_12_FLEXSPI_A_DATA6                 CHIP_PIN_ID(2, 12,  63, 3,  72, -1,-1)
#define CHIP_PIN_IO3_12_ISP_PRELIGHT_TRIG1              CHIP_PIN_ID(2, 12,  63, 4,  72, -1,-1)
#define CHIP_PIN_IO3_12_GPIO                            CHIP_PIN_ID(2, 12,  63, 5,  72, -1,-1)
#define CHIP_PIN_IO3_12_CORESIGHT_TRACE10               CHIP_PIN_ID(2, 12,  63, 6,  72, -1,-1)

/* GPIO3_IO13 (NAND_DATA07), BALL R29, NVCC_NAND (x) */
#define CHIP_PIN_IO3_13_NAND_DATA07                     CHIP_PIN_ID(2, 13,  64, 0,  73, -1,-1)
#define CHIP_PIN_IO3_13_FLEXSPI_B_DATA3                 CHIP_PIN_ID(2, 13,  64, 1,  73, -1,-1)
#define CHIP_PIN_IO3_13_USDHC3_DATA3                    CHIP_PIN_ID(2, 13,  64, 2,  73, 87, 1)
#define CHIP_PIN_IO3_13_FLEXSPI_A_DATA7                 CHIP_PIN_ID(2, 13,  64, 3,  73, -1,-1)
#define CHIP_PIN_IO3_13_ISP_SHUTTER_OPEN1               CHIP_PIN_ID(2, 13,  64, 4,  73, -1,-1)
#define CHIP_PIN_IO3_13_GPIO                            CHIP_PIN_ID(2, 13,  64, 5,  73, -1,-1)
#define CHIP_PIN_IO3_13_CORESIGHT_TRACE11               CHIP_PIN_ID(2, 13,  64, 6,  73, -1,-1)

/* GPIO3_IO14 (NAND_DQS), BALL R26, NVCC_NAND (RU_75, 1.8V, x) */
#define CHIP_PIN_IO3_14_NAND_DQS                        CHIP_PIN_ID(2, 14,  65, 0,  74, -1,-1)
#define CHIP_PIN_IO3_14_FLEXSPI_A_DQS                   CHIP_PIN_ID(2, 14,  65, 1,  74, -1,-1)
#define CHIP_PIN_IO3_14_SAI3_MCLK                       CHIP_PIN_ID(2, 14,  65, 2,  74,  8, 0)
#define CHIP_PIN_IO3_14_ISP_SHUTTER_OPEN0               CHIP_PIN_ID(2, 14,  65, 3,  74, -1,-1)
#define CHIP_PIN_IO3_14_I2C3_SCL                        CHIP_PIN_ID(2, 14,  65, 4,  74, 61, 1)
#define CHIP_PIN_IO3_14_GPIO                            CHIP_PIN_ID(2, 14,  65, 5,  74, -1,-1)
#define CHIP_PIN_IO3_14_CORESIGHT_TRACE12               CHIP_PIN_ID(2, 14,  65, 6,  74, -1,-1)

/* GPIO3_IO15 (NAND_RE_B), BALL R28, NVCC_NAND (x, EMMC_DATA4) */
#define CHIP_PIN_IO3_15_NAND_RE_B                       CHIP_PIN_ID(2, 15,  66, 0,  75, -1,-1)
#define CHIP_PIN_IO3_15_FLEXSPI_B_DQS                   CHIP_PIN_ID(2, 15,  66, 1,  75, -1,-1)
#define CHIP_PIN_IO3_15_USDHC3_DATA4                    CHIP_PIN_ID(2, 15,  66, 2,  75, 88, 1)
#define CHIP_PIN_IO3_15_UART4_TX                        CHIP_PIN_ID(2, 15,  66, 4,  75, -1,-1)
#define CHIP_PIN_IO3_15_UART4_DTE_RX                    CHIP_PIN_ID(2, 15,  66, 4,  75, 80, 5)
#define CHIP_PIN_IO3_15_GPIO                            CHIP_PIN_ID(2, 15,  66, 5,  75, -1,-1)
#define CHIP_PIN_IO3_15_CORESIGHT_TRACE13               CHIP_PIN_ID(2, 15,  66, 6,  75, -1,-1)

/* GPIO3_IO16 (NAND_READY_B), BALL T28, NVCC_NAND (x, heartbeat led) */
#define CHIP_PIN_IO3_16_NAND_READY_B                    CHIP_PIN_ID(2, 16,  67, 0,  76, -1,-1)
#define CHIP_PIN_IO3_16_USDHC3_RESET_B                  CHIP_PIN_ID(2, 16,  67, 2,  76, -1,-1)
#define CHIP_PIN_IO3_16_I2C3_SCL                        CHIP_PIN_ID(2, 16,  67, 4,  76, 61, 2)
#define CHIP_PIN_IO3_16_GPIO                            CHIP_PIN_ID(2, 16,  67, 5,  76, -1,-1)
#define CHIP_PIN_IO3_16_CORESIGHT_TRACE14               CHIP_PIN_ID(2, 16,  67, 6,  76, -1,-1)

/* GPIO3_IO17 (NAND_WE_B), BALL U28, NVCC_NAND (x, EMMC_CLK) */
#define CHIP_PIN_IO3_17_NAND_WE_B                       CHIP_PIN_ID(2, 17,  68, 0,  77, -1,-1)
#define CHIP_PIN_IO3_17_USDHC3_CLK                      CHIP_PIN_ID(2, 17,  68, 2,  77, 81, 1)
#define CHIP_PIN_IO3_17_I2C3_SDA                        CHIP_PIN_ID(2, 17,  68, 4,  77, 62, 2)
#define CHIP_PIN_IO3_17_GPIO                            CHIP_PIN_ID(2, 17,  68, 5,  77, -1,-1)
#define CHIP_PIN_IO3_17_CORESIGHT_TRACE15               CHIP_PIN_ID(2, 17,  68, 6,  77, -1,-1)

/* GPIO3_IO18 (NAND_WP_B), BALL U29, NVCC_NAND (x, EMMC_CMD) */
#define CHIP_PIN_IO3_18_NAND_WP_B                       CHIP_PIN_ID(2, 18,  69, 0,  78, -1,-1)
#define CHIP_PIN_IO3_18_USDHC3_CMD                      CHIP_PIN_ID(2, 18,  69, 2,  78, 83, 1)
#define CHIP_PIN_IO3_18_I2C4_SCL                        CHIP_PIN_ID(2, 18,  69, 4,  78, 63, 3)
#define CHIP_PIN_IO3_18_GPIO                            CHIP_PIN_ID(2, 18,  69, 5,  78, -1,-1)
#define CHIP_PIN_IO3_18_CORESIGHT_EVENTO                CHIP_PIN_ID(2, 18,  69, 6,  78, -1,-1)

/* GPIO3_IO19 (SAI5_RXFS), BALL AC14, NVCC_SAI1_SAI5 (LU_16, 1.8V, CSI_P2_RESET) */
#define CHIP_PIN_IO3_19_SAI5_RX_SYNC                    CHIP_PIN_ID(2, 19,  70, 0,  79, 18, 0)
#define CHIP_PIN_IO3_19_SAI1_TX_DATA0                   CHIP_PIN_ID(2, 19,  70, 1,  79, -1,-1)
#define CHIP_PIN_IO3_19_PWM4_OUT                        CHIP_PIN_ID(2, 19,  70, 2,  79, -1,-1)
#define CHIP_PIN_IO3_19_I2C6_SCL                        CHIP_PIN_ID(2, 19,  70, 3,  79, 67, 1)
#define CHIP_PIN_IO3_19_GPIO                            CHIP_PIN_ID(2, 19,  70, 5,  79, -1,-1)

/* GPIO3_IO20 (SAI5_RXC), BALL AD14, NVCC_SAI1_SAI5 (LU_18, 1.8V, CSI_P2_PWDN) */
#define CHIP_PIN_IO3_20_SAI5_RX_BCLK                    CHIP_PIN_ID(2, 20,  71, 0,  80, 13, 0)
#define CHIP_PIN_IO3_20_SAI1_TX_DATA1                   CHIP_PIN_ID(2, 20,  71, 1,  80, -1,-1)
#define CHIP_PIN_IO3_20_PWM3_OUT                        CHIP_PIN_ID(2, 20,  71, 2,  80, -1,-1)
#define CHIP_PIN_IO3_20_I2C6_SDA                        CHIP_PIN_ID(2, 20,  71, 3,  80, 68, 1)
#define CHIP_PIN_IO3_20_PDM_CLK                         CHIP_PIN_ID(2, 20,  71, 4,  80, -1,-1)
#define CHIP_PIN_IO3_20_GPIO                            CHIP_PIN_ID(2, 20,  71, 5,  80, -1,-1)

/* GPIO3_IO21 (SAI5_RXD0), BALL AE16, NVCC_SAI1_SAI5 (LU_28, 1.8V, PCM_IN) */
#define CHIP_PIN_IO3_21_SAI5_RX_DATA0                   CHIP_PIN_ID(2, 21,  72, 0,  81, 14, 0)
#define CHIP_PIN_IO3_21_SAI1_TX_DATA2                   CHIP_PIN_ID(2, 21,  72, 1,  81, -1,-1)
#define CHIP_PIN_IO3_21_PWM2_OUT                        CHIP_PIN_ID(2, 21,  72, 2,  81, -1,-1)
#define CHIP_PIN_IO3_21_I2C5_SCL                        CHIP_PIN_ID(2, 21,  72, 3,  81, 65, 1)
#define CHIP_PIN_IO3_21_PDM_BIT_STREAM0                 CHIP_PIN_ID(2, 21,  72, 4,  81,  0, 3)
#define CHIP_PIN_IO3_21_GPIO                            CHIP_PIN_ID(2, 21,  72, 5,  81, -1,-1)

/* GPIO3_IO22 (SAI5_RXD1), BALL AD16, NVCC_SAI1_SAI5 (LU_22, 1.8V, PCM_SYNC) */
#define CHIP_PIN_IO3_22_SAI5_RX_DATA1                   CHIP_PIN_ID(2, 22,  73, 0,  82, 15, 0)
#define CHIP_PIN_IO3_22_SAI1_TX_DATA3                   CHIP_PIN_ID(2, 22,  73, 1,  82, -1,-1)
#define CHIP_PIN_IO3_22_SAI1_TX_SYNC                    CHIP_PIN_ID(2, 22,  73, 2,  82,  6, 0)
#define CHIP_PIN_IO3_22_SAI5_TX_SYNC                    CHIP_PIN_ID(2, 22,  73, 3,  82, 20, 0)
#define CHIP_PIN_IO3_22_PDM_BIT_STREAM1                 CHIP_PIN_ID(2, 22,  73, 4,  82,  1, 3)
#define CHIP_PIN_IO3_22_GPIO                            CHIP_PIN_ID(2, 22,  73, 5,  82, -1,-1)
#define CHIP_PIN_IO3_22_CAN1_TX                         CHIP_PIN_ID(2, 22,  73, 6,  82, -1,-1)

/* GPIO3_IO23 (SAI5_RXD2), BALL AF16, NVCC_SAI1_SAI5 (LU_20, 1.8V, HOST_WL_WA) */
#define CHIP_PIN_IO3_23_SAI5_RX_DATA2                   CHIP_PIN_ID(2, 23,  74, 0,  83, 16, 0)
#define CHIP_PIN_IO3_23_SAI1_TX_DATA4                   CHIP_PIN_ID(2, 23,  74, 1,  83, -1,-1)
#define CHIP_PIN_IO3_23_SAI1_TX_SYNC                    CHIP_PIN_ID(2, 23,  74, 2,  83,  6, 1)
#define CHIP_PIN_IO3_23_SAI5_TX_BCLK                    CHIP_PIN_ID(2, 23,  74, 3,  83, 19, 0)
#define CHIP_PIN_IO3_23_PDM_BIT_STREAM2                 CHIP_PIN_ID(2, 23,  74, 4,  83,  2, 3)
#define CHIP_PIN_IO3_23_GPIO                            CHIP_PIN_ID(2, 23,  74, 5,  83, -1,-1)
#define CHIP_PIN_IO3_23_CAN1_RX                         CHIP_PIN_ID(2, 23,  74, 6,  83, 35, 0)

/* GPIO3_IO24 (SAI5_RXD3), BALL AE14, NVCC_SAI1_SAI5 (LU_24, 1.8V, PCM_OUT) */
#define CHIP_PIN_IO3_24_SAI5_RX_DATA3                   CHIP_PIN_ID(2, 24,  75, 0,  84, 17, 0)
#define CHIP_PIN_IO3_24_SAI1_TX_DATA5                   CHIP_PIN_ID(2, 24,  75, 1,  84, -1,-1)
#define CHIP_PIN_IO3_24_SAI1_TX_SYNC                    CHIP_PIN_ID(2, 24,  75, 2,  84,  6, 2)
#define CHIP_PIN_IO3_24_SAI5_TX_DATA0                   CHIP_PIN_ID(2, 24,  75, 3,  84, -1,-1)
#define CHIP_PIN_IO3_24_PDM_BIT_STREAM3                 CHIP_PIN_ID(2, 24,  75, 4,  84,  3, 3)
#define CHIP_PIN_IO3_24_GPIO                            CHIP_PIN_ID(2, 24,  75, 5,  84, -1,-1)
#define CHIP_PIN_IO3_24_CAN2_TX                         CHIP_PIN_ID(2, 24,  75, 6,  84, -1,-1)

/* GPIO3_IO25 (SAI5_MCLK), BALL AF14, NVCC_SAI1_SAI5 (LU_26, 1.8V, PCM_CLK) */
#define CHIP_PIN_IO3_25_SAI5_MCLK                       CHIP_PIN_ID(2, 25,  76, 0,  85, 12, 0)
#define CHIP_PIN_IO3_25_SAI1_TX_BCLK                    CHIP_PIN_ID(2, 25,  76, 1,  85,  5, 0)
#define CHIP_PIN_IO3_25_PWM1_OUT                        CHIP_PIN_ID(2, 25,  76, 2,  85, -1,-1)
#define CHIP_PIN_IO3_25_I2C5_SDA                        CHIP_PIN_ID(2, 25,  76, 3,  85, 66, 1)
#define CHIP_PIN_IO3_25_GPIO                            CHIP_PIN_ID(2, 25,  76, 5,  85, -1,-1)
#define CHIP_PIN_IO3_25_CAN2_RX                         CHIP_PIN_ID(2, 25,  76, 6,  85, 36, 0)

/* GPIO4_IO00 (SAI1_RXFS), BALL AJ9, NVCC_SAI1_SAI5 (LU_33, 1.8V, 4G/5G_PWR) */
#define CHIP_PIN_IO4_0_SAI1_RX_SYNC                     CHIP_PIN_ID(3,  0,  77, 0,  86,  4, 0)
#define CHIP_PIN_IO4_0_ENET1_1588_EVENT0_IN             CHIP_PIN_ID(3,  0,  77, 4,  86, -1,-1)
#define CHIP_PIN_IO4_0_GPIO                             CHIP_PIN_ID(3,  0,  77, 5,  86, -1,-1)

/* GPIO4_IO01 (SAI1_RXC), BALL AH8, NVCC_SAI1_SAI5 (LU_27, 1.8V, 4G_RST) */
#define CHIP_PIN_IO4_1_SAI1_RX_BCL                      CHIP_PIN_ID(3,  1,  78, 0,  87, -1,-1)
#define CHIP_PIN_IO4_1_PDM_CLK                          CHIP_PIN_ID(3,  1,  78, 3,  87, -1,-1)
#define CHIP_PIN_IO4_1_ENET1_1588_EVENT0_OUT            CHIP_PIN_ID(3,  1,  78, 4,  87, -1,-1)
#define CHIP_PIN_IO4_1_GPIO                             CHIP_PIN_ID(3,  1,  78, 5,  87, -1,-1)

/* GPIO4_IO02 (SAI1_RXD0), BALL AC10, NVCC_SAI1_SAI5 (LU_31, 1.8V, 5GPWR_ON/OFF) */
#define CHIP_PIN_IO4_2_SAI1_RX_DATA0                    CHIP_PIN_ID(3,  2,  79, 0,  88, -1,-1)
#define CHIP_PIN_IO4_2_SAI1_TX_DATA1                    CHIP_PIN_ID(3,  2,  79, 2,  88, -1,-1)
#define CHIP_PIN_IO4_2_PDM_BIT_STREAM0                  CHIP_PIN_ID(3,  2,  79, 3,  88,  0, 4)
#define CHIP_PIN_IO4_2_ENET1_1588_EVENT1_IN             CHIP_PIN_ID(3,  2,  79, 4,  88, -1,-1)
#define CHIP_PIN_IO4_2_GPIO                             CHIP_PIN_ID(3,  2,  79, 5,  88, -1,-1)

/* GPIO4_IO03 (SAI1_RXD1), BALL AF10, NVCC_SAI1_SAI5 (LU_29, 1.8V, 5GPWR_RESET) */
#define CHIP_PIN_IO4_3_SAI1_RX_DATA1                    CHIP_PIN_ID(3,  3,  80, 0,  89, -1,-1)
#define CHIP_PIN_IO4_3_PDM_BIT_STREAM1                  CHIP_PIN_ID(3,  3,  80, 3,  89,  1, 4)
#define CHIP_PIN_IO4_3_ENET1_1588_EVENT1_OUT            CHIP_PIN_ID(3,  3,  80, 4,  89, -1,-1)
#define CHIP_PIN_IO4_3_GPIO                             CHIP_PIN_ID(3,  3,  80, 5,  89, -1,-1)

/* GPIO4_IO04 (SAI1_RXD2), BALL AH9, NVCC_SAI1_SAI5 (LU_37, 1.8V, ENET1_MDC) */
#define CHIP_PIN_IO4_4_SAI1_RX_DATA2                    CHIP_PIN_ID(3,  4,  81, 0,  90, -1,-1)
#define CHIP_PIN_IO4_4_PDM_BIT_STREAM2                  CHIP_PIN_ID(3,  4,  81, 3,  90,  2, 4)
#define CHIP_PIN_IO4_4_ENET1_MDC                        CHIP_PIN_ID(3,  4,  81, 4,  90, -1,-1)
#define CHIP_PIN_IO4_4_GPIO                             CHIP_PIN_ID(3,  4,  81, 5,  90, -1,-1)

/* GPIO4_IO05 (SAI1_RXD3), BALL AJ8, NVCC_SAI1_SAI5 (LU_39, 1.8V, ENET1_MDIO) */
#define CHIP_PIN_IO4_5_SAI1_RX_DATA3                    CHIP_PIN_ID(3,  5,  82, 0,  91, -1,-1)
#define CHIP_PIN_IO4_5_PDM_BIT_STREAM3                  CHIP_PIN_ID(3,  5,  82, 3,  91,  3, 4)
#define CHIP_PIN_IO4_5_ENET1_MDIO                       CHIP_PIN_ID(3,  5,  82, 4,  91, 47, 1)
#define CHIP_PIN_IO4_5_GPIO                             CHIP_PIN_ID(3,  5,  82, 5,  91, -1,-1)

/* GPIO4_IO06 (SAI1_RXD4), BALL AD10, NVCC_SAI1_SAI5 (LU_63, 1.8V, ENET1_RD0) */
#define CHIP_PIN_IO4_6_SAI1_RX_DATA4                    CHIP_PIN_ID(3,  6,  83, 0,  92, -1,-1)
#define CHIP_PIN_IO4_6_SAI6_TX_BCLK                     CHIP_PIN_ID(3,  6,  83, 1,  92, 25, 1)
#define CHIP_PIN_IO4_6_SAI6_RX_BCLK                     CHIP_PIN_ID(3,  6,  83, 2,  92, 22, 1)
#define CHIP_PIN_IO4_6_ENET1_RGMII_RD0                  CHIP_PIN_ID(3,  6,  83, 4,  92, 48, 1)
#define CHIP_PIN_IO4_6_GPIO                             CHIP_PIN_ID(3,  6,  83, 5,  92, -1,-1)

/* GPIO4_IO07 (SAI1_RXD5), BALL AE10, NVCC_SAI1_SAI5 (LU_65, 1.8V, ENET1_RD1) */
#define CHIP_PIN_IO4_7_SAI1_RX_DATA5                    CHIP_PIN_ID(3,  7,  84, 0,  93, -1,-1)
#define CHIP_PIN_IO4_7_SAI6_TX_DATA0                    CHIP_PIN_ID(3,  7,  84, 1,  93, -1,-1)
#define CHIP_PIN_IO4_7_SAI6_RX_DATA0                    CHIP_PIN_ID(3,  7,  84, 2,  93, 23, 1)
#define CHIP_PIN_IO4_7_SAI1_RX_SYNC                     CHIP_PIN_ID(3,  7,  84, 3,  93,  4, 1)
#define CHIP_PIN_IO4_7_ENET1_RGMII_RD1                  CHIP_PIN_ID(3,  7,  84, 4,  93, 49, 1)
#define CHIP_PIN_IO4_7_GPIO                             CHIP_PIN_ID(3,  7,  84, 5,  93, -1,-1)

/* GPIO4_IO08 (SAI1_RXD6), BALL AH10, NVCC_SAI1_SAI5 (LU_67, 1.8V, ENET1_RD2) */
#define CHIP_PIN_IO4_8_SAI1_RX_DATA6                    CHIP_PIN_ID(3,  8,  85, 0,  94, -1,-1)
#define CHIP_PIN_IO4_8_SAI6_TX_SYNC                     CHIP_PIN_ID(3,  8,  85, 1,  94, 26, 1)
#define CHIP_PIN_IO4_8_SAI6_RX_SYNC                     CHIP_PIN_ID(3,  8,  85, 2,  94, 24, 1)
#define CHIP_PIN_IO4_8_ENET1_RGMII_RD2                  CHIP_PIN_ID(3,  8,  85, 4,  94, -1,-1)
#define CHIP_PIN_IO4_8_GPIO                             CHIP_PIN_ID(3,  8,  85, 5,  94, -1,-1)

/* GPIO4_IO09 (SAI1_RXD7), BALL AH12, NVCC_SAI1_SAI5 (LU_69, 1.8V, ENET1_RD3) */
#define CHIP_PIN_IO4_9_SAI1_RX_DATA7                    CHIP_PIN_ID(3,  9,  86, 0,  95, -1,-1)
#define CHIP_PIN_IO4_9_SAI6_MCLK                        CHIP_PIN_ID(3,  9,  86, 1,  95, 21, 1)
#define CHIP_PIN_IO4_9_SAI1_TX_SYNC                     CHIP_PIN_ID(3,  9,  86, 2,  95,  6, 3)
#define CHIP_PIN_IO4_9_SAI1_TX_DATA4                    CHIP_PIN_ID(3,  9,  86, 3,  95, -1,-1)
#define CHIP_PIN_IO4_9_ENET1_RGMII_RD3                  CHIP_PIN_ID(3,  9,  86, 4,  95, -1,-1)
#define CHIP_PIN_IO4_9_GPIO                             CHIP_PIN_ID(3,  9,  86, 5,  95, -1,-1)

/* GPIO4_IO10 (SAI1_TXFS), BALL AF12, NVCC_SAI1_SAI5 (LU_59, 1.8V, ENET1_RX_CTL) */
#define CHIP_PIN_IO4_10_SAI1_TX_SYNC                    CHIP_PIN_ID(3, 10,  87, 0,  96,  6, 4)
#define CHIP_PIN_IO4_10_ENET1_RGMII_RX_CTL              CHIP_PIN_ID(3, 10,  87, 4,  96, 50, 1)
#define CHIP_PIN_IO4_10_GPIO                            CHIP_PIN_ID(3, 10,  87, 5,  96, -1,-1)

/* GPIO4_IO11 (SAI1_TXC), BALL AJ12, NVCC_SAI1_SAI5 (LU_57, 1.8V, ENET1_RXC) */
#define CHIP_PIN_IO4_11_SAI1_TX_BCLK                    CHIP_PIN_ID(3, 11,  88, 0,  97,  5, 1)
#define CHIP_PIN_IO4_11_ENET1_RGMII_RXC                 CHIP_PIN_ID(3, 11,  88, 4,  97, -1,-1)
#define CHIP_PIN_IO4_11_GPIO                            CHIP_PIN_ID(3, 11,  88, 5,  97, -1,-1)

/* GPIO4_IO12 (SAI1_TXD0), BALL AJ11, NVCC_SAI1_SAI5 (LU_47, 1.8V, ENET1_TD0) */
#define CHIP_PIN_IO4_12_SAI1_TX_DATA0                   CHIP_PIN_ID(3, 12,  89, 0,  98, -1,-1)
#define CHIP_PIN_IO4_12_ENET1_RGMII_TD0                 CHIP_PIN_ID(3, 12,  89, 4,  98, -1,-1)
#define CHIP_PIN_IO4_12_GPIO                            CHIP_PIN_ID(3, 12,  89, 5,  98, -1,-1)

/* GPIO4_IO13 (SAI1_TXD1), BALL AJ10, NVCC_SAI1_SAI5 (LU_45, 1.8V, ENET1_TD1) */
#define CHIP_PIN_IO4_13_SAI1_TX_DATA1                   CHIP_PIN_ID(3, 13,  90, 0,  99, -1,-1)
#define CHIP_PIN_IO4_13_ENET1_RGMII_TD1                 CHIP_PIN_ID(3, 13,  90, 4,  99, -1,-1)
#define CHIP_PIN_IO4_13_GPIO                            CHIP_PIN_ID(3, 13,  90, 5,  99, -1,-1)

/* GPIO4_IO14 (SAI1_TXD2), BALL AH11, NVCC_SAI1_SAI5 (LU_43, 1.8V, ENET1_TD2) */
#define CHIP_PIN_IO4_14_SAI1_TX_DATA2                   CHIP_PIN_ID(3, 14,  91, 0, 100, -1,-1)
#define CHIP_PIN_IO4_14_ENET1_RGMII_TD2                 CHIP_PIN_ID(3, 14,  91, 4, 100, -1,-1)
#define CHIP_PIN_IO4_14_GPIO                            CHIP_PIN_ID(3, 14,  91, 5, 100, -1,-1)

/* GPIO4_IO15 (SAI1_TXD3), BALL AD12, NVCC_SAI1_SAI5 (LU_41, 1.8V, ENET1_TD3) */
#define CHIP_PIN_IO4_15_SAI1_TX_DATA3                   CHIP_PIN_ID(3, 15,  92, 0, 101, -1,-1)
#define CHIP_PIN_IO4_15_ENET1_RGMII_TD3                 CHIP_PIN_ID(3, 15,  92, 4, 101, -1,-1)
#define CHIP_PIN_IO4_15_GPIO                            CHIP_PIN_ID(3, 15,  92, 5, 101, -1,-1)

/* GPIO4_IO16 (SAI1_TXD4), BALL AH13, NVCC_SAI1_SAI5 (LU_53, 1.8V, ENET1_TX_CTL) */
#define CHIP_PIN_IO4_16_SAI1_TX_DATA4                   CHIP_PIN_ID(3, 16,  93, 0, 102, -1,-1)
#define CHIP_PIN_IO4_16_SAI6_RX_BCLK                    CHIP_PIN_ID(3, 16,  93, 1, 102, 22, 2)
#define CHIP_PIN_IO4_16_SAI6_TX_BCLK                    CHIP_PIN_ID(3, 16,  93, 2, 102, 25, 2)
#define CHIP_PIN_IO4_16_ENET1_RGMII_TX_CTL              CHIP_PIN_ID(3, 16,  93, 4, 102, -1,-1)
#define CHIP_PIN_IO4_16_GPIO                            CHIP_PIN_ID(3, 16,  93, 5, 102, -1,-1)

/* GPIO4_IO17 (SAI1_TXD5), BALL AH14, NVCC_SAI1_SAI5 (LU_51, 1.8V, ENET1_TXC) */
#define CHIP_PIN_IO4_17_SAI1_TX_DATA5                   CHIP_PIN_ID(3, 17,  94, 0, 103, -1,-1)
#define CHIP_PIN_IO4_17_SAI6_RX_DATA0                   CHIP_PIN_ID(3, 17,  94, 1, 103, 23, 2)
#define CHIP_PIN_IO4_17_SAI6_TX_DATA0                   CHIP_PIN_ID(3, 17,  94, 2, 103, -1,-1)
#define CHIP_PIN_IO4_17_ENET1_RGMII_TXC                 CHIP_PIN_ID(3, 17,  94, 4, 103, -1,-1)
#define CHIP_PIN_IO4_17_GPIO                            CHIP_PIN_ID(3, 17,  94, 5, 103, -1,-1)

/* GPIO4_IO18 (SAI1_TXD6), BALL AC12, NVCC_SAI1_SAI5 (LU_73, 1.8V, GPIO_KEY2) */
#define CHIP_PIN_IO4_18_SAI1_TX_DATA6                   CHIP_PIN_ID(3, 18,  95, 0, 104, -1,-1)
#define CHIP_PIN_IO4_18_SAI6_RX_SYNC                    CHIP_PIN_ID(3, 18,  95, 1, 104, 24, 2)
#define CHIP_PIN_IO4_18_SAI6_TX_SYNC                    CHIP_PIN_ID(3, 18,  95, 2, 104, 26, 2)
#define CHIP_PIN_IO4_18_ENET1_RX_ER                     CHIP_PIN_ID(3, 18,  95, 4, 104, 51, 1)
#define CHIP_PIN_IO4_18_GPIO                            CHIP_PIN_ID(3, 18,  95, 5, 104, -1,-1)

/* GPIO4_IO19 (SAI1_TXD7), BALL AJ13, NVCC_SAI1_SAI5 (LU_75, 1.8V, CC_nINT) */
#define CHIP_PIN_IO4_19_SAI1_TX_DATA7                   CHIP_PIN_ID(3, 19,  96, 0, 105, -1,-1)
#define CHIP_PIN_IO4_19_SAI6_MCLK                       CHIP_PIN_ID(3, 19,  96, 1, 105, 21, 2)
#define CHIP_PIN_IO4_19_PDM_CLK                         CHIP_PIN_ID(3, 19,  96, 3, 105, -1,-1)
#define CHIP_PIN_IO4_19_ENET1_TX_ER                     CHIP_PIN_ID(3, 19,  96, 4, 105, -1,-1)
#define CHIP_PIN_IO4_19_GPIO                            CHIP_PIN_ID(3, 19,  96, 5, 105, -1,-1)

/* GPIO4_IO20 (SAI1_MCLK), BALL AE12, NVCC_SAI1_SAI5 (LU_77, 1.8V, USB1_SS_SEL) */
#define CHIP_PIN_IO4_20_SAI1_MCLK                       CHIP_PIN_ID(3, 20,  97, 0, 106, -1,-1)
#define CHIP_PIN_IO4_20_SAI1_TX_BCLK                    CHIP_PIN_ID(3, 20,  97, 2, 106,  5, 2)
#define CHIP_PIN_IO4_20_ENET1_TX_CLK                    CHIP_PIN_ID(3, 20,  97, 4, 106, 46, 1)
#define CHIP_PIN_IO4_20_GPIO                            CHIP_PIN_ID(3, 20,  97, 5, 106, -1,-1)

/* GPIO4_IO21 (SAI2_RXFS), BALL AH17, NVCC_SAI2_SAI3_SPDIF (LU_36, 3.3V, SPI_INT) */
#define CHIP_PIN_IO4_21_SAI2_RX_SYNC                    CHIP_PIN_ID(3, 21,  98, 0, 107, -1,-1)
#define CHIP_PIN_IO4_21_SAI5_TX_SYNC                    CHIP_PIN_ID(3, 21,  98, 1, 107, 20, 2)
#define CHIP_PIN_IO4_21_SAI5_TX_DATA1                   CHIP_PIN_ID(3, 21,  98, 2, 107, -1,-1)
#define CHIP_PIN_IO4_21_SAI2_RX_DATA1                   CHIP_PIN_ID(3, 21,  98, 3, 107,  7, 0)
#define CHIP_PIN_IO4_21_UART1_TX                        CHIP_PIN_ID(3, 21,  98, 4, 107, -1,-1)
#define CHIP_PIN_IO4_21_UART1_DTE_RX                    CHIP_PIN_ID(3, 21,  98, 4, 107, 74, 2)
#define CHIP_PIN_IO4_21_GPIO                            CHIP_PIN_ID(3, 21,  98, 5, 107, -1,-1)
#define CHIP_PIN_IO4_21_PDM_BIT_STREAM2                 CHIP_PIN_ID(3, 21,  98, 6, 107,  2, 5)

/* GPIO4_IO22 (SAI2_RXC), BALL AJ16, NVCC_SAI2_SAI3_SPDIF (LU_32, 3.3V, CAN1_TX) */
#define CHIP_PIN_IO4_22_SAI2_RX_BCLK                    CHIP_PIN_ID(3, 22,  99, 0, 108, -1,-1)
#define CHIP_PIN_IO4_22_SAI5_TX_BCLK                    CHIP_PIN_ID(3, 22,  99, 1, 108, 19, 2)
#define CHIP_PIN_IO4_22_CAN1_TX                         CHIP_PIN_ID(3, 22,  99, 3, 108, -1,-1)
#define CHIP_PIN_IO4_22_UART1_RX                        CHIP_PIN_ID(3, 22,  99, 4, 108, 74, 3)
#define CHIP_PIN_IO4_22_UART1_DTE_TX                    CHIP_PIN_ID(3, 22,  99, 4, 108, -1,-1)
#define CHIP_PIN_IO4_22_GPIO                            CHIP_PIN_ID(3, 22,  99, 5, 108, -1,-1)
#define CHIP_PIN_IO4_22_PDM_BIT_STREAM1                 CHIP_PIN_ID(3, 22,  99, 6, 108,  1, 5)

/* GPIO4_IO23 (SAI2_RXD0), BALL AJ14, NVCC_SAI2_SAI3_SPDIF (LU_44, 3.3V, USB_HUB_RST) */
#define CHIP_PIN_IO4_23_SAI2_RX_DATA0                   CHIP_PIN_ID(3, 23, 100, 0, 109, -1,-1)
#define CHIP_PIN_IO4_23_SAI5_TX_DATA0                   CHIP_PIN_ID(3, 23, 100, 1, 109, -1,-1)
#define CHIP_PIN_IO4_23_ENET_QOS_1588_EVENT2_OUT        CHIP_PIN_ID(3, 23, 100, 2, 109, -1,-1)
#define CHIP_PIN_IO4_23_SAI2_TX_DATA1                   CHIP_PIN_ID(3, 23, 100, 3, 109, -1,-1)
#define CHIP_PIN_IO4_23_UART1_RTS_B                     CHIP_PIN_ID(3, 23, 100, 4, 109, 73, 2)
#define CHIP_PIN_IO4_23_UART1_DTE_CTS_B                 CHIP_PIN_ID(3, 23, 100, 4, 109, -1,-1)
#define CHIP_PIN_IO4_23_GPIO                            CHIP_PIN_ID(3, 23, 100, 5, 109, -1,-1)
#define CHIP_PIN_IO4_23_PDM_BIT_STREAM3                 CHIP_PIN_ID(3, 23, 100, 6, 109,  3, 5)

/* GPIO4_IO24 (SAI2_TXFS), BALL AJ17, NVCC_SAI2_SAI3_SPDIF (LU_38, 3.3V, GPIO_KEY1) */
#define CHIP_PIN_IO4_24_SAI2_TX_SYNC                    CHIP_PIN_ID(3, 24, 101, 0, 110, -1,-1)
#define CHIP_PIN_IO4_24_SAI5_TX_DATA1                   CHIP_PIN_ID(3, 24, 101, 1, 110, -1,-1)
#define CHIP_PIN_IO4_24_ENET_QOS_1588_EVENT3_OUT        CHIP_PIN_ID(3, 24, 101, 2, 110, -1,-1)
#define CHIP_PIN_IO4_24_SAI2_TX_DATA1                   CHIP_PIN_ID(3, 24, 101, 3, 110, -1,-1)
#define CHIP_PIN_IO4_24_UART1_CTS_B                     CHIP_PIN_ID(3, 24, 101, 4, 110, -1,-1)
#define CHIP_PIN_IO4_24_UART1_DTE_RTS_B                 CHIP_PIN_ID(3, 24, 101, 4, 110, 73, 3)
#define CHIP_PIN_IO4_24_GPIO                            CHIP_PIN_ID(3, 24, 101, 5, 110, -1,-1)
#define CHIP_PIN_IO4_24_PDM_BIT_STREAM2                 CHIP_PIN_ID(3, 24, 101, 6, 110,  2, 6)

/* GPIO4_IO25 (SAI2_TXC), BALL AH15, NVCC_SAI2_SAI3_SPDIF (LU_34, 3.3V, CAN1_RX) */
#define CHIP_PIN_IO4_25_SAI2_TX_BCLK                    CHIP_PIN_ID(3, 25, 102, 0, 111, -1,-1)
#define CHIP_PIN_IO4_25_SAI5_TX_DATA2                   CHIP_PIN_ID(3, 25, 102, 1, 111, -1,-1)
#define CHIP_PIN_IO4_25_CAN1_RX                         CHIP_PIN_ID(3, 25, 102, 3, 111, 35, 1)
#define CHIP_PIN_IO4_25_GPIO                            CHIP_PIN_ID(3, 25, 102, 5, 111, -1,-1)
#define CHIP_PIN_IO4_25_PDM_BIT_STREAM1                 CHIP_PIN_ID(3, 25, 102, 6, 111,  1, 6)

/* GPIO4_IO26 (SAI2_TXD0), BALL AH16, NVCC_SAI2_SAI3_SPDIF (LU_40, 3.3V, CAN2_TX) */
#define CHIP_PIN_IO4_26_SAI2_TX_DATA0                   CHIP_PIN_ID(3, 26, 103, 0, 112, -1,-1)
#define CHIP_PIN_IO4_26_SAI5_TX_DATA3                   CHIP_PIN_ID(3, 26, 103, 1, 112, -1,-1)
#define CHIP_PIN_IO4_26_ENET_QOS_1588_EVENT2_IN         CHIP_PIN_ID(3, 26, 103, 2, 112, -1,-1)
#define CHIP_PIN_IO4_26_CAN2_TX                         CHIP_PIN_ID(3, 26, 103, 3, 112, -1,-1)
#define CHIP_PIN_IO4_26_ENET_QOS_1588_EVENT2_AUX_IN     CHIP_PIN_ID(3, 26, 103, 4, 112, -1,-1)
#define CHIP_PIN_IO4_26_GPIO                            CHIP_PIN_ID(3, 26, 103, 5, 112, -1,-1)

/* GPIO4_IO27 (SAI2_MCLK), BALL AJ15, NVCC_SAI2_SAI3_SPDIF (LU_42, 3.3V, CAN2_RX) */
#define CHIP_PIN_IO4_27_SAI2_MCLK                       CHIP_PIN_ID(3, 27, 104, 0, 113, -1,-1)
#define CHIP_PIN_IO4_27_SAI5_MCLK                       CHIP_PIN_ID(3, 27, 104, 1, 113, 12, 2)
#define CHIP_PIN_IO4_27_ENET_QOS_1588_EVENT3_IN         CHIP_PIN_ID(3, 27, 104, 2, 113, -1,-1)
#define CHIP_PIN_IO4_27_CAN2_RX                         CHIP_PIN_ID(3, 27, 104, 3, 113, 36, 1)
#define CHIP_PIN_IO4_27_ENET_QOS_1588_EVENT3_AUX_IN     CHIP_PIN_ID(3, 27, 104, 4, 113, -1,-1)
#define CHIP_PIN_IO4_27_GPIO                            CHIP_PIN_ID(3, 27, 104, 5, 113, -1,-1)
#define CHIP_PIN_IO4_27_SAI3_MCLK                       CHIP_PIN_ID(3, 27, 104, 6, 113,  8, 1)

/* GPIO4_IO28 (SAI3_RXFS), BALL AJ19, NVCC_SAI2_SAI3_SPDIF (LU_50, 3.3V, AUD_nINT) */
#define CHIP_PIN_IO4_28_SAI3_RX_SYNC                    CHIP_PIN_ID(3, 28, 105, 0, 114, -1,-1)
#define CHIP_PIN_IO4_28_SAI2_RX_DATA1                   CHIP_PIN_ID(3, 28, 105, 1, 114,  7, 1)
#define CHIP_PIN_IO4_28_SAI5_RX_SYNC                    CHIP_PIN_ID(3, 28, 105, 2, 114, 18, 2)
#define CHIP_PIN_IO4_28_SAI3_RX_DATA1                   CHIP_PIN_ID(3, 28, 105, 3, 114, -1,-1)
#define CHIP_PIN_IO4_28_SPDIF1_IN                       CHIP_PIN_ID(3, 28, 105, 4, 114, 33, 2)
#define CHIP_PIN_IO4_28_GPIO                            CHIP_PIN_ID(3, 28, 105, 5, 114, -1,-1)
#define CHIP_PIN_IO4_28_PDM_BIT_STREAM0                 CHIP_PIN_ID(3, 28, 105, 6, 114,  0, 5)

/* GPIO4_IO29 (SAI3_RXC), BALL AJ18, NVCC_SAI2_SAI3_SPDIF (LU_48, 3.3V, CODEC_PWRE) */
#define CHIP_PIN_IO4_29_SAI3_RX_BCLK                    CHIP_PIN_ID(3, 29, 106, 0, 115, -1,-1)
#define CHIP_PIN_IO4_29_SAI2_RX_DATA2                   CHIP_PIN_ID(3, 29, 106, 1, 115, -1,-1)
#define CHIP_PIN_IO4_29_SAI5_RX_BCLK                    CHIP_PIN_ID(3, 29, 106, 2, 115, 13, 2)
#define CHIP_PIN_IO4_29_GPT1_CLK                        CHIP_PIN_ID(3, 29, 106, 3, 115, 55, 0)
#define CHIP_PIN_IO4_29_UART2_CTS_B                     CHIP_PIN_ID(3, 29, 106, 4, 115, -1,-1)
#define CHIP_PIN_IO4_29_UART2_DTE_RTS_B                 CHIP_PIN_ID(3, 29, 106, 4, 115, 75, 2)
#define CHIP_PIN_IO4_29_GPIO                            CHIP_PIN_ID(3, 29, 106, 5, 115, -1,-1)
#define CHIP_PIN_IO4_29_PDM_CLK                         CHIP_PIN_ID(3, 29, 106, 6, 115, -1,-1)

/* GPIO4_IO30 (SAI3_RXD), BALL AF18, NVCC_SAI2_SAI3_SPDIF (LU_62, 3.3V, SAI3_RXD) */
#define CHIP_PIN_IO4_30_SAI3_RX_DATA0                   CHIP_PIN_ID(3, 30, 107, 0, 116,  9, 1)
#define CHIP_PIN_IO4_30_SAI2_RX_DATA3                   CHIP_PIN_ID(3, 30, 107, 1, 116, -1,-1)
#define CHIP_PIN_IO4_30_SAI5_RX_DATA0                   CHIP_PIN_ID(3, 30, 107, 2, 116, 14, 2)
#define CHIP_PIN_IO4_30_UART2_RTS_B                     CHIP_PIN_ID(3, 30, 107, 4, 116, 75, 3)
#define CHIP_PIN_IO4_30_UART2_DTE_CTS_B                 CHIP_PIN_ID(3, 30, 107, 4, 116, -1,-1)
#define CHIP_PIN_IO4_30_GPIO                            CHIP_PIN_ID(3, 30, 107, 5, 116, -1,-1)
#define CHIP_PIN_IO4_30_PDM_BIT_STREAM1                 CHIP_PIN_ID(3, 30, 107, 6, 116,  1, 7)

/* GPIO4_IO31 (SAI3_TXFS), BALL AC16, NVCC_SAI2_SAI3_SPDIF (LU_58, 3.3V, SAI3_TXFS) */
#define CHIP_PIN_IO4_31_SAI3_TX_SYNC                    CHIP_PIN_ID(3, 31, 108, 0, 117, 11, 1)
#define CHIP_PIN_IO4_31_SAI2_TX_DATA1                   CHIP_PIN_ID(3, 31, 108, 1, 117, -1,-1)
#define CHIP_PIN_IO4_31_SAI5_RX_DATA1                   CHIP_PIN_ID(3, 31, 108, 2, 117, 15, 2)
#define CHIP_PIN_IO4_31_SAI3_TX_DATA1                   CHIP_PIN_ID(3, 31, 108, 3, 117, -1,-1)
#define CHIP_PIN_IO4_31_UART2_RX                        CHIP_PIN_ID(3, 31, 108, 4, 117, 76, 4)
#define CHIP_PIN_IO4_31_UART2_DTE_TX                    CHIP_PIN_ID(3, 31, 108, 4, 117, -1,-1)
#define CHIP_PIN_IO4_31_GPIO                            CHIP_PIN_ID(3, 31, 108, 5, 117, -1,-1)
#define CHIP_PIN_IO4_31_PDM_BIT_STREAM3                 CHIP_PIN_ID(3, 31, 108, 6, 117,  3, 6)

/* GPIO5_IO00 (SAI3_TXC), BALL AH19, NVCC_SAI2_SAI3_SPDIF (LU_56, 3.3V, SAI3_TXC) */
#define CHIP_PIN_IO5_0_SAI3_TX_BCLK                     CHIP_PIN_ID(4,  0, 109, 0, 118, 10, 0)
#define CHIP_PIN_IO5_0_SAI2_TX_DATA2                    CHIP_PIN_ID(4,  0, 109, 1, 118, -1,-1)
#define CHIP_PIN_IO5_0_SAI5_RX_DATA2                    CHIP_PIN_ID(4,  0, 109, 2, 118, 16, 2)
#define CHIP_PIN_IO5_0_GPT1_CAPTURE1                    CHIP_PIN_ID(4,  0, 109, 3, 118, 53, 0)
#define CHIP_PIN_IO5_0_UART2_TX                         CHIP_PIN_ID(4,  0, 109, 4, 118, -1,-1)
#define CHIP_PIN_IO5_0_UART2_DTE_RX                     CHIP_PIN_ID(4,  0, 109, 4, 118, 76, 5)
#define CHIP_PIN_IO5_0_GPIO                             CHIP_PIN_ID(4,  0, 109, 5, 118, -1,-1)
#define CHIP_PIN_IO5_0_PDM_BIT_STREAM2                  CHIP_PIN_ID(4,  0, 109, 6, 118,  2, 7)

/* GPIO5_IO01 (SAI3_TXD), BALL AH18, NVCC_SAI2_SAI3_SPDIF (LU_60, 3.3V, SAI3_TXD) */
#define CHIP_PIN_IO5_1_SAI3_TX_DATA0                    CHIP_PIN_ID(4,  1, 110, 0, 119, -1,-1)
#define CHIP_PIN_IO5_1_SAI2_TX_DATA3                    CHIP_PIN_ID(4,  1, 110, 1, 119, -1,-1)
#define CHIP_PIN_IO5_1_SAI5_RX_DATA3                    CHIP_PIN_ID(4,  1, 110, 2, 119, 17, 2)
#define CHIP_PIN_IO5_1_GPT1_CAPTURE2                    CHIP_PIN_ID(4,  1, 110, 3, 119, 54, 0)
#define CHIP_PIN_IO5_1_SPDIF1_EXT_CLK                   CHIP_PIN_ID(4,  1, 110, 4, 119, 34, 0)
#define CHIP_PIN_IO5_1_GPIO                             CHIP_PIN_ID(4,  1, 110, 5, 119, -1,-1)

/* GPIO5_IO02 (SAI3_MCLK), BALL AJ20, NVCC_SAI2_SAI3_SPDIF (LU_52, 3.3V, SAI3_MCLK) */
#define CHIP_PIN_IO5_2_SAI3_MCLK                        CHIP_PIN_ID(4,  2, 111, 0, 120,  8, 2)
#define CHIP_PIN_IO5_2_PWM4_OUT                         CHIP_PIN_ID(4,  2, 111, 1, 120, -1,-1)
#define CHIP_PIN_IO5_2_SAI5_MCLK                        CHIP_PIN_ID(4,  2, 111, 2, 120, 12, 3)
#define CHIP_PIN_IO5_2_SPDIF1_OUT                       CHIP_PIN_ID(4,  2, 111, 4, 120, -1,-1)
#define CHIP_PIN_IO5_2_GPIO                             CHIP_PIN_ID(4,  2, 111, 5, 120, -1,-1)
#define CHIP_PIN_IO5_2_SPDIF1_IN                        CHIP_PIN_ID(4,  2, 111, 6, 120, 33, 3)

/* GPIO5_IO03 (SPDIF_TX), BALL AE18, NVCC_SAI2_SAI3_SPDIF (LD_8, 3.3V, ENET_nINT) */
#define CHIP_PIN_IO5_3_SPDIF1_OUT                       CHIP_PIN_ID(4,  3, 112, 0, 121, -1,-1)
#define CHIP_PIN_IO5_3_PWM3_OUT                         CHIP_PIN_ID(4,  3, 112, 1, 121, -1,-1)
#define CHIP_PIN_IO5_3_I2C5_SCL                         CHIP_PIN_ID(4,  3, 112, 2, 121, 65, 2)
#define CHIP_PIN_IO5_3_GPT1_COMPARE1                    CHIP_PIN_ID(4,  3, 112, 3, 121, -1,-1)
#define CHIP_PIN_IO5_3_CAN1_TX                          CHIP_PIN_ID(4,  3, 112, 4, 121, -1,-1)
#define CHIP_PIN_IO5_3_GPIO                             CHIP_PIN_ID(4,  3, 112, 5, 121, -1,-1)

/* GPIO5_IO04 (SPDIF_RX), BALL AD18, NVCC_SAI2_SAI3_SPDIF (LD_6, 3.3V, ENET1_nRST) */
#define CHIP_PIN_IO5_4_SPDIF1_IN                        CHIP_PIN_ID(4,  4, 113, 0, 122, 33, 4)
#define CHIP_PIN_IO5_4_PWM2_OUT                         CHIP_PIN_ID(4,  4, 113, 1, 122, -1,-1)
#define CHIP_PIN_IO5_4_I2C5_SDA                         CHIP_PIN_ID(4,  4, 113, 2, 122, 66, 2)
#define CHIP_PIN_IO5_4_GPT1_COMPARE2                    CHIP_PIN_ID(4,  4, 113, 3, 122, -1,-1)
#define CHIP_PIN_IO5_4_CAN1_RX                          CHIP_PIN_ID(4,  4, 113, 4, 122, 35, 2)
#define CHIP_PIN_IO5_4_GPIO                             CHIP_PIN_ID(4,  4, 113, 5, 122, -1,-1)

/* GPIO5_IO05 (SPDIF_EXT_CLK), BALL AC18, NVCC_SAI2_SAI3_SPDIF (LD_4, 3.3V, ENET_nRST) */
#define CHIP_PIN_IO5_5_SPDIF1_EXT_CLK                   CHIP_PIN_ID(4,  5, 114, 0, 123, 34, 1)
#define CHIP_PIN_IO5_5_PWM1_OUT                         CHIP_PIN_ID(4,  5, 114, 1, 123, -1,-1)
#define CHIP_PIN_IO5_5_GPT1_COMPARE3                    CHIP_PIN_ID(4,  5, 114, 3, 123, -1,-1)
#define CHIP_PIN_IO5_5_GPIO                             CHIP_PIN_ID(4,  5, 114, 5, 123, -1,-1)

/* GPIO5_IO06 (ECSPI1_SCLK), BALL AF20, NVCC_ECSPI_HDMI (LD_16, 3.3V, UART3_RXD) */
#define CHIP_PIN_IO5_6_ECSPI1_SCLK                      CHIP_PIN_ID(4,  6, 115, 0, 124, 38, 0)
#define CHIP_PIN_IO5_6_UART3_RX                         CHIP_PIN_ID(4,  6, 115, 1, 124, 78, 4)
#define CHIP_PIN_IO5_6_UART3_DTE_TX                     CHIP_PIN_ID(4,  6, 115, 1, 124, -1,-1)
#define CHIP_PIN_IO5_6_I2C1_SCL                         CHIP_PIN_ID(4,  6, 115, 2, 124, 57, 1)
#define CHIP_PIN_IO5_6_SAI7_RX_SYNC                     CHIP_PIN_ID(4,  6, 115, 3, 124, 30, 1)
#define CHIP_PIN_IO5_6_GPIO                             CHIP_PIN_ID(4,  6, 115, 5, 124, -1,-1)

/* GPIO5_IO07 (ECSPI1_MOSI), BALL AC20, NVCC_ECSPI_HDMI (LD_18, 3.3V, UART3_TXD) */
#define CHIP_PIN_IO5_7_ECSPI1_MOSI                      CHIP_PIN_ID(4,  7, 116, 0, 125, 40, 0)
#define CHIP_PIN_IO5_7_UART3_TX                         CHIP_PIN_ID(4,  7, 116, 1, 125, -1,-1)
#define CHIP_PIN_IO5_7_UART3_DTE_RX                     CHIP_PIN_ID(4,  7, 116, 1, 125, 78, 5)
#define CHIP_PIN_IO5_7_I2C1_SDA                         CHIP_PIN_ID(4,  7, 116, 2, 125, 58, 1)
#define CHIP_PIN_IO5_7_SAI7_RX_BCLK                     CHIP_PIN_ID(4,  7, 116, 3, 125, 28, 1)
#define CHIP_PIN_IO5_7_GPIO                             CHIP_PIN_ID(4,  7, 116, 5, 125, -1,-1)

/* GPIO5_IO08 (ECSPI1_MISO), BALL AD20, NVCC_ECSPI_HDMI (LD_12, 3.3V, GPIO_LED1) */
#define CHIP_PIN_IO5_8_ECSPI1_MISO                      CHIP_PIN_ID(4,  8, 117, 0, 126, 39, 0)
#define CHIP_PIN_IO5_8_UART3_CTS_B                      CHIP_PIN_ID(4,  8, 117, 1, 126, -1,-1)
#define CHIP_PIN_IO5_8_UART3_DTE_RTS_B                  CHIP_PIN_ID(4,  8, 117, 1, 126, 77, 2)
#define CHIP_PIN_IO5_8_I2C2_SCL                         CHIP_PIN_ID(4,  8, 117, 2, 126, 59, 1)
#define CHIP_PIN_IO5_8_SAI7_RX_DATA0                    CHIP_PIN_ID(4,  8, 117, 3, 126, 29, 1)
#define CHIP_PIN_IO5_8_GPIO                             CHIP_PIN_ID(4,  8, 117, 5, 126, -1,-1)

/* GPIO5_IO09 (ECSPI1_SS0), BALL AE20, NVCC_ECSPI_HDMI (LD_14, 3.3V, GPIO_LED2) */
#define CHIP_PIN_IO5_9_ECSPI1_SS0                       CHIP_PIN_ID(4,  9, 118, 0, 127, 41, 0)
#define CHIP_PIN_IO5_9_UART3_RTS_B                      CHIP_PIN_ID(4,  9, 118, 1, 127, 77, 3)
#define CHIP_PIN_IO5_9_UART3_DTE_CTS_B                  CHIP_PIN_ID(4,  9, 118, 1, 127, -1,-1)
#define CHIP_PIN_IO5_9_I2C2_SDA                         CHIP_PIN_ID(4,  9, 118, 2, 127, 60, 1)
#define CHIP_PIN_IO5_9_SAI7_TX_SYNC                     CHIP_PIN_ID(4,  9, 118, 3, 127, 32, 1)
#define CHIP_PIN_IO5_9_GPIO                             CHIP_PIN_ID(4,  9, 118, 5, 127, -1,-1)

/* GPIO5_IO10 (ECSPI2_SCLK), BALL AH21, NVCC_ECSPI_HDMI (LD_22, 3.3V, ECSPI2_SCLK) */
#define CHIP_PIN_IO5_10_ECSPI2_SCLK                     CHIP_PIN_ID(4, 10, 119, 0, 128, 42, 1)
#define CHIP_PIN_IO5_10_UART4_RX                        CHIP_PIN_ID(4, 10, 119, 1, 128, 80, 6)
#define CHIP_PIN_IO5_10_UART4_DTE_TX                    CHIP_PIN_ID(4, 10, 119, 1, 128, -1,-1)
#define CHIP_PIN_IO5_10_I2C3_SCL                        CHIP_PIN_ID(4, 10, 119, 2, 128, 61, 3)
#define CHIP_PIN_IO5_10_SAI7_TX_BCLK                    CHIP_PIN_ID(4, 10, 119, 3, 128, 31, 1)
#define CHIP_PIN_IO5_10_GPIO                            CHIP_PIN_ID(4, 10, 119, 5, 128, -1,-1)

/* GPIO5_IO11 (ECSPI2_MOSI), BALL AJ21, NVCC_ECSPI_HDMI (LD_26, 3.3V, ECSPI2_MOSI) */
#define CHIP_PIN_IO5_11_ECSPI2_MOSI                     CHIP_PIN_ID(4, 11, 120, 0, 129, 44, 1)
#define CHIP_PIN_IO5_11_UART4_TX                        CHIP_PIN_ID(4, 11, 120, 1, 129, -1,-1)
#define CHIP_PIN_IO5_11_UART4_DTE_RX                    CHIP_PIN_ID(4, 11, 120, 1, 129, 80, 7)
#define CHIP_PIN_IO5_11_I2C3_SDA                        CHIP_PIN_ID(4, 11, 120, 2, 129, 62, 3)
#define CHIP_PIN_IO5_11_SAI7_TX_DATA0                   CHIP_PIN_ID(4, 11, 120, 3, 129, -1,-1)
#define CHIP_PIN_IO5_11_GPIO                            CHIP_PIN_ID(4, 11, 120, 5, 129, -1,-1)

/* GPIO5_IO12 (ECSPI2_MISO), BALL AH20, NVCC_ECSPI_HDMI (LD_28, 3.3V, ECSPI2_MISO) */
#define CHIP_PIN_IO5_12_ECSPI2_MISO                     CHIP_PIN_ID(4, 12, 121, 0, 130, 43, 1)
#define CHIP_PIN_IO5_12_UART4_CTS_B                     CHIP_PIN_ID(4, 12, 121, 1, 130, -1,-1)
#define CHIP_PIN_IO5_12_UART4_DTE_RTS_B                 CHIP_PIN_ID(4, 12, 121, 1, 130, 79, 2)
#define CHIP_PIN_IO5_12_I2C4_SCL                        CHIP_PIN_ID(4, 12, 121, 2, 130, 63, 4)
#define CHIP_PIN_IO5_12_SAI7_MCLK                       CHIP_PIN_ID(4, 12, 121, 3, 130, 27, 1)
#define CHIP_PIN_IO5_12_CCM_CLKO1                       CHIP_PIN_ID(4, 12, 121, 4, 130, -1,-1)
#define CHIP_PIN_IO5_12_GPIO                            CHIP_PIN_ID(4, 12, 121, 5, 130, -1,-1)

/* GPIO5_IO13 (ECSPI2_SS0), BALL AJ22, NVCC_ECSPI_HDMI (LD_24, 3.3V, ECSPI2_SS0) */
#define CHIP_PIN_IO5_13_ECSPI2_SS0                      CHIP_PIN_ID(4, 13, 122, 0, 131, 45, 1)
#define CHIP_PIN_IO5_13_UART4_RTS_B                     CHIP_PIN_ID(4, 13, 122, 1, 131, 79, 3)
#define CHIP_PIN_IO5_13_UART4_DTE_CTS_B                 CHIP_PIN_ID(4, 13, 122, 1, 131, -1,-1)
#define CHIP_PIN_IO5_13_I2C4_SDA                        CHIP_PIN_ID(4, 13, 122, 2, 131, 64, 4)
#define CHIP_PIN_IO5_13_CCM_CLKO2                       CHIP_PIN_ID(4, 13, 122, 4, 131, -1,-1)
#define CHIP_PIN_IO5_13_GPIO                            CHIP_PIN_ID(4, 13, 122, 5, 131, -1,-1)


/* GPIO5_IO14 (I2C1_SCL), BALL AC8, NVCC_I2C_UART (x, PMIC) */
#define CHIP_PIN_IO5_14_I2C1_SCL                        CHIP_PIN_ID(4, 14, 123, 0, 132, 57, 2)
#define CHIP_PIN_IO5_14_ENET_QOS_MDC                    CHIP_PIN_ID(4, 14, 123, 1, 132, -1,-1)
#define CHIP_PIN_IO5_14_ECSPI1_SCLK                     CHIP_PIN_ID(4, 14, 123, 3, 132, 38, 1)
#define CHIP_PIN_IO5_14_GPIO                            CHIP_PIN_ID(4, 14, 123, 5, 132, -1,-1)

/* GPIO5_IO15 (I2C1_SDA), BALL AH7, NVCC_I2C_UART (x, PMIC) */
#define CHIP_PIN_IO5_15_I2C1_SDA                        CHIP_PIN_ID(4, 15, 124, 0, 133, 58, 2)
#define CHIP_PIN_IO5_15_ENET_QOS_MDIO                   CHIP_PIN_ID(4, 15, 124, 1, 133, 52, 2)
#define CHIP_PIN_IO5_15_ECSPI1_MOSI                     CHIP_PIN_ID(4, 15, 124, 3, 133, 40, 1)
#define CHIP_PIN_IO5_15_GPIO                            CHIP_PIN_ID(4, 15, 124, 5, 133, -1,-1)

/* GPIO5_IO16 (I2C2_SCL), BALL AH6, NVCC_I2C_UART (LU_17, 3.3V, I2C2_SCL) */
#define CHIP_PIN_IO5_16_I2C2_SCL                        CHIP_PIN_ID(4, 16, 125, 0, 134, 59, 2)
#define CHIP_PIN_IO5_16_ENET_QOS_1588_EVENT1_IN         CHIP_PIN_ID(4, 16, 125, 1, 134, -1,-1)
#define CHIP_PIN_IO5_16_USDHC3_CD_B                     CHIP_PIN_ID(4, 16, 125, 2, 134, 82, 3)
#define CHIP_PIN_IO5_16_ECSPI1_MISO                     CHIP_PIN_ID(4, 16, 125, 3, 134, 39, 1)
#define CHIP_PIN_IO5_16_ENET_QOS_1588_EVENT1_AUX_IN     CHIP_PIN_ID(4, 16, 125, 4, 134, -1,-1)
#define CHIP_PIN_IO5_16_GPIO                            CHIP_PIN_ID(4, 16, 125, 5, 134, -1,-1)

/* GPIO5_IO17 (I2C2_SDA), BALL AE8, NVCC_I2C_UART (LU_19, 3.3V, I2C2_SDA) */
#define CHIP_PIN_IO5_17_I2C2_SDA                        CHIP_PIN_ID(4, 17, 126, 0, 135, 60, 2)
#define CHIP_PIN_IO5_17_ENET_QOS_1588_EVENT1_OUT        CHIP_PIN_ID(4, 17, 126, 1, 135, -1,-1)
#define CHIP_PIN_IO5_17_USDHC3_WP                       CHIP_PIN_ID(4, 17, 126, 2, 135, 93, 3)
#define CHIP_PIN_IO5_17_ECSPI1_SS0                      CHIP_PIN_ID(4, 17, 126, 3, 135, 41, 1)
#define CHIP_PIN_IO5_17_GPIO                            CHIP_PIN_ID(4, 17, 126, 5, 135, -1,-1)

/* GPIO5_IO18 (I2C3_SCL), BALL AJ7, NVCC_I2C_UART (LU_23, 3.3V, I2C3_SCL) */
#define CHIP_PIN_IO5_18_I2C3_SCL                        CHIP_PIN_ID(4, 18, 127, 0, 136, 61, 4)
#define CHIP_PIN_IO5_18_PWM4_OUT                        CHIP_PIN_ID(4, 18, 127, 1, 136, -1,-1)
#define CHIP_PIN_IO5_18_GPT2_CLK                        CHIP_PIN_ID(4, 18, 127, 2, 136, -1,-1)
#define CHIP_PIN_IO5_18_ECSPI2_SCLK                     CHIP_PIN_ID(4, 18, 127, 3, 136, 42, 2)
#define CHIP_PIN_IO5_18_GPIO                            CHIP_PIN_ID(4, 18, 127, 5, 136, -1,-1)

/* GPIO5_IO19 (I2C3_SDA), BALL AJ6, NVCC_I2C_UART (LU_21, 3.3V, I2C3_SDA) */
#define CHIP_PIN_IO5_19_I2C3_SDA                        CHIP_PIN_ID(4, 19, 128, 0, 137, 62, 4)
#define CHIP_PIN_IO5_19_PWM3_OUT                        CHIP_PIN_ID(4, 19, 128, 1, 137, -1,-1)
#define CHIP_PIN_IO5_19_GPT3_CLK                        CHIP_PIN_ID(4, 19, 128, 2, 137, -1,-1)
#define CHIP_PIN_IO5_19_ECSPI2_MOSI                     CHIP_PIN_ID(4, 19, 128, 3, 137, 44, 2)
#define CHIP_PIN_IO5_19_GPIO                            CHIP_PIN_ID(4, 19, 128, 5, 137, -1,-1)

/* GPIO5_IO20 (I2C4_SCL), BALL AF8, NVCC_I2C_UART (LU_11, 3.3V, I2C4_SCL) */
#define CHIP_PIN_IO5_20_I2C4_SCL                        CHIP_PIN_ID(4, 20, 129, 0, 138, 63, 5)
#define CHIP_PIN_IO5_20_PWM2_OUT                        CHIP_PIN_ID(4, 20, 129, 1, 138, -1,-1)
#define CHIP_PIN_IO5_20_PCIE_CLKREQ_B                   CHIP_PIN_ID(4, 20, 129, 2, 138, 56, 0)
#define CHIP_PIN_IO5_20_ECSPI2_MISO                     CHIP_PIN_ID(4, 20, 129, 3, 138, 43, 2)
#define CHIP_PIN_IO5_20_GPIO                            CHIP_PIN_ID(4, 20, 129, 5, 138, -1,-1)

/* GPIO5_IO21 (I2C4_SDA), BALL AD8, NVCC_I2C_UART (LU_13, 3.3V, I2C4_SDA) */
#define CHIP_PIN_IO5_21_I2C4_SDA                        CHIP_PIN_ID(4, 21, 130, 0, 139, 64, 5)
#define CHIP_PIN_IO5_21_PWM1_OUT                        CHIP_PIN_ID(4, 21, 130, 1, 139, -1,-1)
#define CHIP_PIN_IO5_21_ECSPI2_SS0                      CHIP_PIN_ID(4, 21, 130, 3, 139, 45, 2)
#define CHIP_PIN_IO5_21_GPIO                            CHIP_PIN_ID(4, 21, 130, 5, 139, -1,-1)

/* GPIO5_IO22 (UART1_RXD), BALL AD6, NVCC_I2C_UART (LU_6, 3.3V, UART1_RXD) */
#define CHIP_PIN_IO5_22_UART1_RX                        CHIP_PIN_ID(4, 22, 131, 0, 140, 74, 4)
#define CHIP_PIN_IO5_22_UART1_DTE_TX                    CHIP_PIN_ID(4, 22, 131, 0, 140, -1,-1)
#define CHIP_PIN_IO5_22_ECSPI3_SCLK                     CHIP_PIN_ID(4, 22, 131, 1, 140, -1,-1)
#define CHIP_PIN_IO5_22_GPIO                            CHIP_PIN_ID(4, 22, 131, 5, 140, -1,-1)

/* GPIO5_IO23 (UART1_TXD), BALL AJ3, NVCC_I2C_UART (LU_4, 3.3V, UART1_TXD) */
#define CHIP_PIN_IO5_23_UART1_TX                        CHIP_PIN_ID(4, 23, 132, 0, 141, -1,-1)
#define CHIP_PIN_IO5_23_UART1_DTE_RX                    CHIP_PIN_ID(4, 23, 132, 0, 141, 74, 5)
#define CHIP_PIN_IO5_23_ECSPI3_MOSI                     CHIP_PIN_ID(4, 23, 132, 1, 141, -1,-1)
#define CHIP_PIN_IO5_23_GPIO                            CHIP_PIN_ID(4, 23, 132, 5, 141, -1,-1)

/* GPIO5_IO24 (UART2_RXD), BALL AF6, NVCC_I2C_UART (LU_5, 3.3V, UART2_RXD (A53 debug)) */
#define CHIP_PIN_IO5_24_UART2_RX                        CHIP_PIN_ID(4, 24, 133, 0, 142, 76, 6)
#define CHIP_PIN_IO5_24_UART2_DTE_TX                    CHIP_PIN_ID(4, 24, 133, 0, 142, -1,-1)
#define CHIP_PIN_IO5_24_ECSPI3_MISO                     CHIP_PIN_ID(4, 24, 133, 1, 142, -1,-1)
#define CHIP_PIN_IO5_24_GPT1_COMPARE3                   CHIP_PIN_ID(4, 24, 133, 3, 142, -1,-1)
#define CHIP_PIN_IO5_24_GPIO                            CHIP_PIN_ID(4, 24, 133, 5, 142, -1,-1)

/* GPIO5_IO25 (UART2_TXD), BALL AH4, NVCC_I2C_UART (LU_3, 3.3V, UART2_TXD (A53 debug)) */
#define CHIP_PIN_IO5_25_UART2_TX                        CHIP_PIN_ID(4, 25, 134, 0, 143, -1,-1)
#define CHIP_PIN_IO5_25_UART2_DTE_RX                    CHIP_PIN_ID(4, 25, 134, 0, 143, 76, 7)
#define CHIP_PIN_IO5_25_ECSPI3_SS0                      CHIP_PIN_ID(4, 25, 134, 1, 143, -1,-1)
#define CHIP_PIN_IO5_25_GPT1_COMPARE2                   CHIP_PIN_ID(4, 25, 134, 3, 143, -1,-1)
#define CHIP_PIN_IO5_25_GPIO                            CHIP_PIN_ID(4, 25, 134, 5, 143, -1,-1)

/* GPIO5_IO26 (UART3_RXD), BALL AE6, NVCC_I2C_UART (LU_7, 3.3V, UART1_CTS) */
#define CHIP_PIN_IO5_26_UART3_RX                        CHIP_PIN_ID(4, 26, 135, 0, 144, 78, 6)
#define CHIP_PIN_IO5_26_UART3_DTE_TX                    CHIP_PIN_ID(4, 26, 135, 0, 144, -1,-1)
#define CHIP_PIN_IO5_26_UART1_CTS_B                     CHIP_PIN_ID(4, 26, 135, 1, 144, -1,-1)
#define CHIP_PIN_IO5_26_UART1_DTE_RTS_B                 CHIP_PIN_ID(4, 26, 135, 1, 144, 73, 4)
#define CHIP_PIN_IO5_26_USDHC3_RESET_B                  CHIP_PIN_ID(4, 26, 135, 2, 144, -1,-1)
#define CHIP_PIN_IO5_26_GPT1_CAPTURE2                   CHIP_PIN_ID(4, 26, 135, 3, 144, 54, 1)
#define CHIP_PIN_IO5_26_CAN2_TX                         CHIP_PIN_ID(4, 26, 135, 4, 144, -1,-1)
#define CHIP_PIN_IO5_26_GPIO                            CHIP_PIN_ID(4, 26, 135, 5, 144, -1,-1)

/* GPIO5_IO27 (UART3_TXD), BALL AJ4, NVCC_I2C_UART (LU_9, 3.3V, UART1_RTS) */
#define CHIP_PIN_IO5_27_UART3_TX                        CHIP_PIN_ID(4, 27, 136, 0, 145, -1,-1)
#define CHIP_PIN_IO5_27_UART3_DTE_RX                    CHIP_PIN_ID(4, 27, 136, 0, 145, 78, 7)
#define CHIP_PIN_IO5_27_UART1_RTS_B                     CHIP_PIN_ID(4, 27, 136, 1, 145, 73, 5)
#define CHIP_PIN_IO5_27_UART1_DTE_CTS_B                 CHIP_PIN_ID(4, 27, 136, 1, 145, -1,-1)
#define CHIP_PIN_IO5_27_USDHC3_VSELECT                  CHIP_PIN_ID(4, 27, 136, 2, 145, -1,-1)
#define CHIP_PIN_IO5_27_GPT1_CLK                        CHIP_PIN_ID(4, 27, 136, 3, 145, 55, 1)
#define CHIP_PIN_IO5_27_CAN2_RX                         CHIP_PIN_ID(4, 27, 136, 4, 145, 36, 2)
#define CHIP_PIN_IO5_27_GPIO                            CHIP_PIN_ID(4, 27, 136, 5, 145, -1,-1)

/* GPIO5_IO28 (UART4_RXD), BALL AJ5, NVCC_I2C_UART (LU_12, 3.3V, UART4_RXD (M7 debug)) */
#define CHIP_PIN_IO5_28_UART4_RX                        CHIP_PIN_ID(4, 28, 137, 0, 146, 80, 8)
#define CHIP_PIN_IO5_28_UART4_DTE_TX                    CHIP_PIN_ID(4, 28, 137, 0, 146, -1,-1)
#define CHIP_PIN_IO5_28_UART2_CTS_B                     CHIP_PIN_ID(4, 28, 137, 1, 146, -1,-1)
#define CHIP_PIN_IO5_28_UART2_DTE_RTS_B                 CHIP_PIN_ID(4, 28, 137, 1, 146, 75, 4)
#define CHIP_PIN_IO5_28_PCIE_CLKREQ_B                   CHIP_PIN_ID(4, 28, 137, 2, 146, 56, 1)
#define CHIP_PIN_IO5_28_GPT1_COMPARE1                   CHIP_PIN_ID(4, 28, 137, 3, 146, -1,-1)
#define CHIP_PIN_IO5_28_I2C6_SCL                        CHIP_PIN_ID(4, 28, 137, 4, 146, 67, 2)
#define CHIP_PIN_IO5_28_GPIO                            CHIP_PIN_ID(4, 28, 137, 5, 146, -1,-1)

/* GPIO5_IO29 (UART4_TXD), BALL AH5, NVCC_I2C_UART (LU_10, 3.3V, UART4_TXD (M7 debug)) */
#define CHIP_PIN_IO5_29_UART4_TX                        CHIP_PIN_ID(4, 29, 138, 0, 147, -1,-1)
#define CHIP_PIN_IO5_29_UART4_DTE_RX                    CHIP_PIN_ID(4, 29, 138, 0, 147, 80, 9)
#define CHIP_PIN_IO5_29_UART2_RTS_B                     CHIP_PIN_ID(4, 29, 138, 1, 147, 75, 5)
#define CHIP_PIN_IO5_29_UART2_DTE_CTS_B                 CHIP_PIN_ID(4, 29, 138, 1, 147, -1,-1)
#define CHIP_PIN_IO5_29_GPT1_CAPTURE1                   CHIP_PIN_ID(4, 29, 138, 3, 147, 53, 1)
#define CHIP_PIN_IO5_29_I2C6_SDA                        CHIP_PIN_ID(4, 29, 138, 4, 147, 68, 2)
#define CHIP_PIN_IO5_29_GPIO                            CHIP_PIN_ID(4, 29, 138, 5, 147, -1,-1)

/* GPIO3_IO26 (HDMI_DDC_SCL), BALL AC22, NVCC_ECSPI_HDMI (LD_9, 3.3V, HDMI_DDC_SC) */
#define CHIP_PIN_IO3_26_HDMI_SCL                        CHIP_PIN_ID(2, 26, 139, 0, 148, -1,-1)
#define CHIP_PIN_IO3_26_I2C5_SCL                        CHIP_PIN_ID(2, 26, 139, 3, 148, 65, 3)
#define CHIP_PIN_IO3_26_CAN1_TX                         CHIP_PIN_ID(2, 26, 139, 4, 148, -1,-1)
#define CHIP_PIN_IO3_26_GPIO                            CHIP_PIN_ID(2, 26, 139, 5, 148, -1,-1)

/* GPIO3_IO27 (HDMI_DDC_SDA), BALL AF22, NVCC_ECSPI_HDMI (LD_5, 3.3V, HDMI_DDC_SD) */
#define CHIP_PIN_IO3_27_HDMI_SDA                        CHIP_PIN_ID(2, 27, 140, 0, 149, -1,-1)
#define CHIP_PIN_IO3_27_I2C5_SDA                        CHIP_PIN_ID(2, 27, 140, 3, 149, 66, 3)
#define CHIP_PIN_IO3_27_CAN1_RX                         CHIP_PIN_ID(2, 27, 140, 4, 149, 35, 3)
#define CHIP_PIN_IO3_27_GPIO                            CHIP_PIN_ID(2, 27, 140, 5, 149, -1,-1)

/* GPIO3_IO28 (HDMI_CEC), BALL AD22, NVCC_ECSPI_HDMI (LD_11, 3.3V, HDMI_CEC) */
#define CHIP_PIN_IO3_28_HDMI_CEC                        CHIP_PIN_ID(2, 28, 141, 0, 150, -1,-1)
#define CHIP_PIN_IO3_28_I2C6_SCL                        CHIP_PIN_ID(2, 28, 141, 3, 150, 67, 3)
#define CHIP_PIN_IO3_28_CAN2_TX                         CHIP_PIN_ID(2, 28, 141, 4, 150, -1,-1)
#define CHIP_PIN_IO3_28_GPIO                            CHIP_PIN_ID(2, 28, 141, 5, 150, -1,-1)

/* GPIO3_IO29 (HDMI_HPD), BALL AE22, NVCC_ECSPI_HDMI (LD_3, 3.3V, HDMI_HPD) */
#define CHIP_PIN_IO3_29_HDMI_HPD                        CHIP_PIN_ID(2, 29, 142, 0, 151, -1,-1)
#define CHIP_PIN_IO3_29_HDMI_HPD_O                      CHIP_PIN_ID(2, 29, 142, 1, 151, -1,-1)
#define CHIP_PIN_IO3_29_I2C6_SDA                        CHIP_PIN_ID(2, 29, 142, 3, 151, 68, 3)
#define CHIP_PIN_IO3_29_CAN2_RX                         CHIP_PIN_ID(2, 29, 142, 4, 151, 36, 3)
#define CHIP_PIN_IO3_29_GPIO                            CHIP_PIN_ID(2, 29, 142, 5, 151, -1,-1)


/* BOOT_MODE0, BALL G10, NVCC_JTAG (RU_51, 3.3V, BOOT_MODE0) */
#define CHIP_PIN_BOOT_MODE0                             CHIP_PIN_ID(-1,-1,  -1,-1,   0, -1,-1)
/* BOOT_MODE1, BALL  F8, NVCC_JTAG (RU_49, 3.3V, BOOT_MODE1) */
#define CHIP_PIN_BOOT_MODE1                             CHIP_PIN_ID(-1,-1,  -1,-1,   1, -1,-1)
/* BOOT_MODE2, BALL  G8, NVCC_JTAG (RU_47, 3.3V, BOOT_MODE2) */
#define CHIP_PIN_BOOT_MODE2                             CHIP_PIN_ID(-1,-1,  -1,-1,   2, -1,-1)
/* BOOT_MODE3, BALL G12, NVCC_JTAG (RU_53, 3.3V, BOOT_MODE3) */
#define CHIP_PIN_BOOT_MODE3                             CHIP_PIN_ID(-1,-1,  -1,-1,   3, -1,-1)
/* JTAG_MOD,   BALL G20, NVCC_JTAG (RU_65, 3.3V, JTAG_MOD) */
#define CHIP_PIN_JTAG_MOD                               CHIP_PIN_ID(-1,-1,  -1,-1,   4, -1,-1)
/* JTAG_TDI,   BALL G16, NVCC_JTAG (RU_61, 3.3V, JTAG_TDI) */
#define CHIP_PIN_JTAG_TDI                               CHIP_PIN_ID(-1,-1,  -1,-1,   5, -1,-1)
/* JTAG_TMS,   BALL G14, NVCC_JTAG (RU_61, 3.3V, JTAG_TMS) */
#define CHIP_PIN_JTAG_TMS                               CHIP_PIN_ID(-1,-1,  -1,-1,   6, -1,-1)
/* JTAG_TCK,   BALL G18, NVCC_JTAG (RU_63, 3.3V, JTAG_TCK) */
#define CHIP_PIN_JTAG_TCK                               CHIP_PIN_ID(-1,-1,  -1,-1,   7, -1,-1)
/* JTAG_TDO,   BALL F14, NVCC_JTAG (RU_59, 3.3V, JTAG_TDO) */
#define CHIP_PIN_JTAG_TDO                               CHIP_PIN_ID(-1,-1,  -1,-1,   8, -1,-1)

/* CLKIN1,     BALL K28, NVCC_CLK  (LU_66, 3.3V, CLKIN1) */
#define CHIP_PIN_CLKIN1                                 CHIP_PIN_ID(-1,-1,  -1,-1, 152, -1,-1)
/* CLKIN2,     BALL L28, NVCC_CLK  (LU_74, 3.3V, CLKIN1) */
#define CHIP_PIN_CLKIN2                                 CHIP_PIN_ID(-1,-1,  -1,-1, 153, -1,-1)
/* CLKOUT1,    BALL K29, NVCC_CLK  (LU_70, 3.3V, CLKOUT1) */
#define CHIP_PIN_CLKOUT1                                CHIP_PIN_ID(-1,-1,  -1,-1, 154, -1,-1)
/* CLKOUT2,    BALL L29, NVCC_CLK  (LU_78, 3.3V, CLKOUT2) */
#define CHIP_PIN_CLKOUT2                                CHIP_PIN_ID(-1,-1,  -1,-1, 155, -1,-1)



#endif // CHIP_PINS_H
