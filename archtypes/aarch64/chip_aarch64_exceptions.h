#ifndef CHIP_AARCH64_EXCEPTIONS_H
#define CHIP_AARCH64_EXCEPTIONS_H


/* Exception Syndrome Register (EL1/EL2/EL3) */
#define CHIP_REGFLD_AARCH_ESR_ISS                       (0x01FFFFFFUL <<  0U) /* (RO)  */
#define CHIP_REGFLD_AARCH_ESR_IL                        (0x00000001UL << 15U) /* (RO)  */
#define CHIP_REGFLD_AARCH_ESR_EC                        (0x0000003FUL << 26U) /* (RO) Exception Class */
#define CHIP_REGFLD_AARCH_ESR_ISS2                      (0x00FFFFFFULL<< 32U) /* (RO) ISS2 encoding for an exception  */



/* page 6439 */
#define CHIP_AARCH_EC_UNKNOWN                           0x00 /* Unknown reason. */
#define CHIP_AARCH_EC_TRAP_WF                           0x01 /* Trapped WF* instruction execution. */


#define CHIP_AARCH_EC_DATA_ABORT                        0x25 /* Data Abort exception: MMU faults, alignment faults, and synchronous External aborts */

/*********** ISS encoding for an exception from a Data Abort ******************/
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_ISV                (0x00000001UL << 24U) /* (RO) Instruction Syndrome Valid (bits 23:14)  */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_SAS                (0x00000003UL << 22U) /* (RO) (ISV == 1) Syndrome Access Size  */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_SSE                (0x00000001UL << 21U) /* (RO) (ISV == 1) Syndrome Sign Extend*/
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_SRT                (0x0000001FUL << 16U) /* (RO) (ISV == 1) Syndrome Register Transfer. The register number of the Wt/Xt/Rt operand of the faulting instruction. */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_SF                 (0x00000001UL << 15U) /* (RO) (ISV == 1) 64-bit general-purpose register transfer */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_FNP                (0x00000001UL << 15U) /* (RO) (ISV == 0) FAR not Precise (for FEAT_SME FAR holds any virtual address within the naturally-aligned granule) */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_AR                 (0x00000001UL << 14U) /* (RO) (ISV == 1) Acquire/Release (instruction did have acquire/release semantics) */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_VNCR               (0x00000001UL << 13U) /* (RO) Indicates that the fault came from use of VNCR_EL2 register by EL1 code. */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_LST                (0x00000003UL << 11U) /* (RO) (DFSC == 0b00xxxx || DFSC == 0b101011) && DFSC != 0b0000xx: Load/Store Type */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_SET                (0x00000003UL << 11U) /* (RO) FEAT_RAS: (DFSC == 0b010000, or DFSC == 0b01001x or DFSC == 0b0101xx): Synchronous Error Type */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_FNV                (0x00000001UL << 10U) /* (RO) FAR not Valid */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_EA                 (0x00000001UL <<  9U) /* (RO) External abort type */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_CM                 (0x00000001UL <<  8U) /* (RO) Cache maintenance or a synchronous fault on the execution of an address translation instruction */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_S1PTW              (0x00000001UL <<  7U) /* (RO) Fault on the stage 2 translation of an access for a stage 1 translation table walk */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_WNR                (0x00000001UL <<  6U) /* (RO) Write not Read */
#define CHIP_REGFLD_AARCH_ESR_ISS_DA_DFSC               (0x0000003FUL <<  0U) /* (RO) Data Fault Status Code */

#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_SAS_BYTE        0
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_SAS_HALFWRD     1
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_SAS_WRD         2
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_SAS_DWRD        3

#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_LST_UNSPEC      0 /* The instruction that generated the Data Abort is not specified. */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_LST_ST64BV      1 /* FEAT_LS64_V: An ST64BV instruction generated the Data Abort. */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_LST_LD_ST64B    2 /* FEAT_LS64: An LD64B or ST64B instruction generated the Data Abort. */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_LST_ST64BV0     3 /* FEAT_LS64_ACCDATA: An ST64BV0 instruction generated the Data Abort. */

#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_SET_UER         0 /* Recoverable state */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_SET_UC          2 /* Uncontainable */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_SET_UEO         3 /* Restartable state */

#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_WNR_RD          0 /* Abort caused by an instruction reading from a memory location. */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_WNR_WR          1 /* Abort caused by an instruction writing to a memory location. */

#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ADDRSZ_LVL0    0x00 /* Address size fault, level 0 of translation or translation table base register */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ADDRSZ_LVL1    0x01 /* Address size fault, level 1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ADDRSZ_LVL2    0x02 /* Address size fault, level 2 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ADDRSZ_LVL3    0x03 /* Address size fault, level 3 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_TRANSL_LVL0    0x04 /* Translation fault, level 0 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_TRANSL_LVL1    0x05 /* Translation fault, level 1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_TRANSL_LVL2    0x06 /* Translation fault, level 2 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_TRANSL_LVL3    0x07 /* Translation fault, level 3 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ACCFLG_LVL0    0x08 /* FEAT_LPA2: Access flag fault, level 0 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ACCFLG_LVL1    0x09 /* Access flag fault, level 1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ACCFLG_LVL2    0x0A /* Access flag fault, level 2 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ACCFLG_LVL3    0x0B /* Access flag fault, level 3 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_PERM_LVL0      0x0C /* FEAT_LPA2: Permission fault, level 0 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_PERM_LVL1      0x0D /* Permission fault, level 1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_PERM_LVL2      0x0E /* Permission fault, level 2 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_PERM_LVL3      0x0F /* Permission fault, level 3 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_EXT            0x10 /* Synchronous External abort, not on translation table walk or hardware update of translation table */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_TAGCHK         0x11 /* FEAT_MTE2: Synchronous Tag Check Fault */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_EXT_LVLN1      0x13 /* FEAT_LPA2: Synchronous External abort on translation table walk or hardware update of translation table, level -1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_EXT_LVL0       0x14 /* Synchronous External abort on translation table walk or hardware update of translation table, level 0 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_EXT_LVL1       0x15 /* Synchronous External abort on translation table walk or hardware update of translation table, level 1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_EXT_LVL2       0x16 /* Synchronous External abort on translation table walk or hardware update of translation table, level 2 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_EXT_LVL3       0x17 /* Synchronous External abort on translation table walk or hardware update of translation table, level 3 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ECC            0x18 /* !FEAT_RAS: Synchronous parity or ECC error on memory access, not on translation table walk */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ECC_LVLN1      0x1B /* !FEAT_RAS && FEAT_LPA2: Synchronous parity or ECC error on memory access on translation table walk or hardware update of translation table, level -1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ECC_LVL0       0x1C /* !FEAT_RAS: Synchronous parity or ECC error on memory access on translation table walk or hardware update of translation table, level 0 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ECC_LVL1       0x1D /* !FEAT_RAS: Synchronous parity or ECC error on memory access on translation table walk or hardware update of translation table, level 1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ECC_LVL2       0x1E /* !FEAT_RAS: Synchronous parity or ECC error on memory access on translation table walk or hardware update of translation table, level 2 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ECC_LVL3       0x1F /* !FEAT_RAS: Synchronous parity or ECC error on memory access on translation table walk or hardware update of translation table, level 3 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ALIGN          0x21 /* Alignment fault. */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_GRNPRROT_LVLN1 0x23 /* FEAT_RME && FEAT_LPA2: Granule Protection Fault on translation table walk or hardware update of translation table, level -1.*/
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_GRNPRROT_LVL0  0x24 /* FEAT_RME: Granule Protection Fault on translation table walk or hardware update of translation table, level 0 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_GRNPRROT_LVL1  0x25 /* FEAT_RME: Granule Protection Fault on translation table walk or hardware update of translation table, level 1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_GRNPRROT_LVL2  0x26 /* FEAT_RME: Granule Protection Fault on translation table walk or hardware update of translation table, level 2 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_GRNPRROT_LVL3  0x27 /* FEAT_RME: Granule Protection Fault on translation table walk or hardware update of translation table, level 3 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_GRNPRROT       0x28 /* FEAT_RME: Granule Protection Fault, not on translation table walk or hardware update of translation table */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ADDRSZ_LVLN1   0x29 /* FEAT_LPA2: Address size fault, level -1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_TRANSL_LVLN1   0x2B /* FEAT_LPA2: Translation fault, level -1 */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_TLB            0x30 /* TLB conflict abort. */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_ATOMIC_HWUP    0x31 /* FEAT_HAFDBS: Unsupported atomic hardware update fault. */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_IMPL_LOCKDOWN  0x34 /* IMPLEMENTATION DEFINED fault (Lockdown) */
#define CHIP_REGFLDVAL_AARCH_ESR_ISS_DA_DFSC_IMPL_ATOMIC    0x35 /* IMPLEMENTATION DEFINED fault (Unsupported Exclusive or Atomic access) */

#endif // CHIP_AARCH64_EXCEPTIONS_H
