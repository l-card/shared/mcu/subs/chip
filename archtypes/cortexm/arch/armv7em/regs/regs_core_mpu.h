#ifndef CHIP_ARMV7EM_REGS_MPU_H
#define CHIP_ARMV7EM_REGS_MPU_H

#include <stdint.h>

typedef struct {
    __IM  uint32_t TYPE;                   /* Offset: 0x000 (R/ )  MPU Type Register */
    __IOM uint32_t CTRL;                   /* Offset: 0x004 (R/W)  MPU Control Register */
    __IOM uint32_t RNR;                    /* Offset: 0x008 (R/W)  MPU Region RNRber Register */
    __IOM uint32_t RBAR;                   /* Offset: 0x00C (R/W)  MPU Region Base Address Register */
    __IOM uint32_t RASR;                   /* Offset: 0x010 (R/W)  MPU Region Attribute and Size Register */
    __IOM uint32_t RBAR_A1;                /* Offset: 0x014 (R/W)  MPU Alias 1 Region Base Address Register */
    __IOM uint32_t RASR_A1;                /* Offset: 0x018 (R/W)  MPU Alias 1 Region Attribute and Size Register */
    __IOM uint32_t RBAR_A2;                /* Offset: 0x01C (R/W)  MPU Alias 2 Region Base Address Register */
    __IOM uint32_t RASR_A2;                /* Offset: 0x020 (R/W)  MPU Alias 2 Region Attribute and Size Register */
    __IOM uint32_t RBAR_A3;                /* Offset: 0x024 (R/W)  MPU Alias 3 Region Base Address Register */
    __IOM uint32_t RASR_A3;                /* Offset: 0x028 (R/W)  MPU Alias 3 Region Attribute and Size Register */
} CHIP_REGS_ARMV7EM_MPU_T;


#define CHIP_REGS_MPU                          ((CHIP_REGS_ARMV7EM_MPU_T *) CHIP_MEMRGN_ADDR_ARMV7EM_MPU)


/*********** MPU type register (MPU_TYPE) *************************************/
#define CHIP_REGFLD_MPU_TYPE_SEPARATE           (0x00000001UL <<  0U) /* Indicates support for unified or separate instruction and date memory maps */
#define CHIP_REGFLD_MPU_TYPE_DREGION            (0x000000FFUL <<  8U) /* Indicates the number of supported MPU data regions */
#define CHIP_REGFLD_MPU_TYPE_IREGION            (0x000000FFUL << 16U) /* Indicates the number of supported MPU instruction regions. */
#define CHIP_REGMSK_MPU_TYPE_RESERVED           (0xFF0000FEUL)

/*********** MPU control register (MPU_CTRL) **********************************/
#define CHIP_REGFLD_MPU_CTRL_ENABLE             (0x00000001UL <<  0U) /* Enables the MPU */
#define CHIP_REGFLD_MPU_CTRL_HFNMIENA           (0x00000001UL <<  1U) /* Enables the operation of MPU during hard fault, NMI, and FAULTMASK handlers */
#define CHIP_REGFLD_MPU_CTRL_PRIVDEFENA         (0x00000001UL <<  2U) /* Enables privileged software access to the default memory map */
#define CHIP_REGMSK_MPU_CTRL_RESERVED           (0xFFFFFFF8UL)

/*********** MPU region number register (MPU_RNR) *****************************/
#define CHIP_REGFLD_MPU_RNR_REGION              (0x000000FFUL <<  0U) /* Indicates the MPU region referenced by the MPU_RBAR and MPU_RASR registers.*/
#define CHIP_REGMSK_MPU_RNR_RESERVED            (0xFFFFFF00UL)

/*********** MPU region base address register (MPU_RBAR) **********************/
#define CHIP_REGFLD_MPU_RBAR_REGION             (0x0000000FUL <<  0U) /* MPU region field */
#define CHIP_REGFLD_MPU_RBAR_VALID              (0x00000001UL <<  4U) /* MPU Region Number valid bit */
#define CHIP_REGFLD_MPU_RBAR_ADDR               (0x07FFFFFFUL <<  5U) /* Region base address field */

/*********** MPU region attribute and size register (MPU_RASR) ****************/
#define CHIP_REGFLD_MPU_RASR_ENABLE             (0x00000001UL <<  0U) /* Region enable bit */
#define CHIP_REGFLD_MPU_RASR_SIZE               (0x0000001FUL <<  1U) /* Specifies the size of the MPU protection region */
#define CHIP_REGFLD_MPU_RASR_SRD                (0x000000FFUL <<  8U) /* Subregion disable mask */
#define CHIP_REGFLD_MPU_RASR_B                  (0x00000001UL << 16U) /* Memory access attribute B (bufferable) */
#define CHIP_REGFLD_MPU_RASR_C                  (0x00000001UL << 17U) /* Memory access attribute С (cachable) */
#define CHIP_REGFLD_MPU_RASR_S                  (0x00000001UL << 18U) /* Shareable bit */
#define CHIP_REGFLD_MPU_RASR_TEX                (0x00000007UL << 19U) /* Memory access attribute TEX */
#define CHIP_REGFLD_MPU_RASR_AP                 (0x00000007UL << 24U) /* Access permission field */
#define CHIP_REGFLD_MPU_RASR_XN                 (0x00000001UL << 28U) /* Instruction access disable bit */
#define CHIP_REGMSK_MPU_RASR_RESERVED           (0xE8C000C0UL)



/* MPU Region sizes */
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_32B        4
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_64B        5
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_128B       6
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_256B       7
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_512B       8
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_1KB        9
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_2KB        10
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_4KB        11
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_8KB        12
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_16KB       13
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_32KB       14
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_64KB       15
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_128KB      16
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_256KB      17
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_512KB      18
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_1MB        19
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_2MB        20
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_4MB        21
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_8MB        22
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_16MB       23
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_32MB       24
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_64MB       25
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_128MB      26
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_256MB      27
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_512MB      28
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_1GB        29
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_2GB        30
#define CHIP_REGFLDVAL_MPU_RASR_SIZE_4GB        31


#define CHIP_REGFLDVAL_MPU_RASR_AP_NA_NA        0 /* All accesses generate a permission fault */
#define CHIP_REGFLDVAL_MPU_RASR_AP_RW_NA        1 /* Access from privileged software only */
#define CHIP_REGFLDVAL_MPU_RASR_AP_RW_RO        2 /* Writes by unprivileged software generate a permission fault */
#define CHIP_REGFLDVAL_MPU_RASR_AP_RW_RW        3 /* Full access */
#define CHIP_REGFLDVAL_MPU_RASR_AP_RO_NA        5 /* Reads by privileged software only */
#define CHIP_REGFLDVAL_MPU_RASR_AP_RO_RO        6 /* Read only, by privileged or unprivileged software */


#endif // CHIP_ARMV7EM_REGS_MPU_H
