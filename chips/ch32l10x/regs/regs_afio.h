#ifndef CHIP_STM32F10X_REGS_AFIO_H
#define CHIP_STM32F10X_REGS_AFIO_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

/* Alternate Function I/O */
typedef struct {
    __IO uint32_t EVCR;       /* Event control register,                                    Address offset: 0x00  */
    __IO uint32_t MAPR;       /* AF remap and debug I/O configuration register,             Address offset: 0x04  */
    union {
        struct {
            __IO uint32_t EXTICR1; /* Alternate external interrupt configuration register 1, Address offset: 0x08  */
            __IO uint32_t EXTICR2; /* Alternate external interrupt configuration register 2, Address offset: 0x0C  */
            __IO uint32_t EXTICR3; /* Alternate external interrupt configuration register 3, Address offset: 0x10  */
            __IO uint32_t EXTICR4; /* Alternate external interrupt configuration register 4, Address offset: 0x14  */
        };
        __IO uint32_t EXTICR[4];
    };
    __IO uint32_t CR;              /* Control register,                                      Address offset: 0x18  */
    __IO uint32_t MAPR2;           /* AF remap and debug I/O configuration register 2,       Address offset: 0x1C  */
} CHIP_REGS_AFIO_T;


#define CHIP_REGS_AFIO                              ((CHIP_REGS_AFIO_T *) CHIP_MEMRGN_ADDR_PERIPH_AFIO)

/*********** Event control register (AFIO_EVCR) *******************************/
#define CHIP_REGFLD_AFIO_EVCR_EVOE                (0x00000001UL <<  7U) /* (RW) Event output enable */
#define CHIP_REGFLD_AFIO_EVCR_PORT                (0x00000007UL <<  4U) /* (RW) Event output port selection */
#define CHIP_REGFLD_AFIO_EVCR_PIN                 (0x0000000FUL <<  0U) /* (RW) Event output pin selection */
#define CHIP_REGMSK_AFIO_EVCR_RESERVED            (0xFFFFFF00UL)

/*********** AF remap and debug I/O configuration register (AFIO_MAPR) ********/
#define CHIP_REGFLD_AFIO_MAPR_SW_CFG                (0x00000007UL << 24U) /* (RW)  debug interface configuration (0 - enabled, 0x4 - disabled) */
#define CHIP_REGFLD_AFIO_MAPR_PD01_REMAP            (0x00000001UL << 15U) /* (RW)  Port D0/Port D1 mapping on OSC_IN/OSC_OUT */
#define CHIP_REGFLD_AFIO_MAPR_CAN1                  (0x00000003UL << 13U) /* (RW)  CAN alternate function remapping  */
#define CHIP_REGFLD_AFIO_MAPR_TIM4                  (0x00000001UL << 12U) /* (RW)  Timer 4 remapping */
#define CHIP_REGFLD_AFIO_MAPR_TIM3                  (0x00000001UL << 10U) /* (RW)  Timer 3 remapping */
#define CHIP_REGFLD_AFIO_MAPR_TIM2                  (0x00000003UL <<  8U) /* (RW)  Timer 2 remapping bits [0..1] */
#define CHIP_REGFLD_AFIO_MAPR_TIM1                  (0x00000003UL <<  6U) /* (RW)  Timer 1 remapping bits [0..1] */
#define CHIP_REGFLD_AFIO_MAPR_USART3                (0x00000003UL <<  4U) /* (RW)  USART3 remapping */
#define CHIP_REGFLD_AFIO_MAPR_USART2                (0x00000001UL <<  3U) /* (RW)  USART2 remapping bit [0] */
#define CHIP_REGFLD_AFIO_MAPR_USART1                (0x00000001UL <<  2U) /* (RW)  USART1 remapping bit [0] */
#define CHIP_REGFLD_AFIO_MAPR_I2C1                  (0x00000001UL <<  1U) /* (RW)  I2C1 remapping bit [0] */
#define CHIP_REGFLD_AFIO_MAPR_SPI1                  (0x00000001UL <<  0U) /* (RW)  SPI1 remapping bit [0] */
#define CHIP_REGMSK_AFIO_MAPR_RESERVED              (0xF8FF0800UL)


/*********** AF remap and debug I/O configuration register 2 (AFIO_MAP2) ******/
#define CHIP_REGFLD_AFIO_MAPR2_LPTIM1               (0x00000001UL << 25U) /* (RW)  LPTIM1 remapping  */
#define CHIP_REGFLD_AFIO_MAPR2_SPI1_H               (0x00000001UL << 24U) /* (RW)  SPI1 remapping bit [1]  */
#define CHIP_REGFLD_AFIO_MAPR2_I2C1_H               (0x00000001UL << 23U) /* (RW)  I2C1 remapping bit [1]  */
#define CHIP_REGFLD_AFIO_MAPR2_TIM1_H               (0x00000001UL << 22U) /* (RW)  Timer 1 remapping bit [2]  */
#define CHIP_REGFLD_AFIO_MAPR2_TIM2_H               (0x00000001UL << 21U) /* (RW)  Timer 2 remapping bit [2]  */
#define CHIP_REGFLD_AFIO_MAPR2_USART1_H             (0x00000003UL << 19U) /* (RW)  USART1 remapping bits [2..1]  */
#define CHIP_REGFLD_AFIO_MAPR2_USART2_H             (0x00000001UL << 18U) /* (RW)  USART2 remapping bits [1]  */
#define CHIP_REGFLD_AFIO_MAPR2_USART4               (0x00000003UL << 16U) /* (RW)  USART4 remapping  */
#define CHIP_REGMSK_AFIO_MAPR2_RESERVED             (0xFC02FFFF)


/*********** Control Register (AFIO_CR) ***************************************/
#define CHIP_REGFLD_AFIO_CR_UDM_BC_CMPO             (0x00000001UL << 21U) /* (RO)  PA11/UDM pin BC protocol comparator status ( > BC protocol reference value VBC_REF) */
#define CHIP_REGFLD_AFIO_CR_UDP_BC_CMPO             (0x00000001UL << 20U) /* (RO)  PA12/UDP pin BC protocol comparator status ( > BC protocol reference value VBC_REF) */
#define CHIP_REGFLD_AFIO_CR_UDM_BC_CMPE             (0x00000001UL << 19U) /* (RW)  PA11/UDM pin BC protocol comparator enable */
#define CHIP_REGFLD_AFIO_CR_UDP_BC_CMPE             (0x00000001UL << 18U) /* (RW)  PA12/UDP pin BC protocol comparator enable */
#define CHIP_REGFLD_AFIO_CR_UDM_BC_VSRC             (0x00000001UL << 17U) /* (RW)  PA11/UDM pin BC protocol source voltage VBC_SRC enable */
#define CHIP_REGFLD_AFIO_CR_UDP_BC_VSRC             (0x00000001UL << 16U) /* (RW)  PA12/UDP pin BC protocol source voltage VBC_SRC enable */
#define CHIP_REGFLD_AFIO_CR_USBPD_IN_HVT            (0x00000001UL <<  9U) /* (RW)  PD pin PB6/PB7 high threshold input mode (2.2V, reduces I/O power consumption during PD communication) */
#define CHIP_REGMSK_AFIO_CR_RESERVED                (0xFFC0FDFFUL)

#endif // CHIP_STM32F10X_REGS_AFIO_H
