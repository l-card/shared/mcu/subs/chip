#ifndef CHIP_PINS_H
#define CHIP_PINS_H

#include "chip_dev_spec_features.h"
#include "regs/regs_iocon.h"
#include "lbitfield.h"

typedef unsigned t_chip_pin_id;

#ifndef CHIP_PIN_CFG_STD
    #define CHIP_PIN_CFG_STD      0
#endif

#define CHIP_PIN_ID_INVALID       (0xFFFFFFFF)

#define CHIP_PIN_ID_EX(port, pin, func, cfg)   ((((port) & 0x07) << 29) | \
                                                (((pin)  & 0x1F) << 24) | \
                                                (((func) & 0x07) <<  0)  | \
                                                (((cfg) & 0xFFF8) << 0))

#define CHIP_PIN_ID(port, pin, func)           CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_CFG_STD)
#define CHIP_PIN_ID_ANALOG(port, pin, func)    CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_CFG_ANALOG)

/* Извлечение полей ID (internal use) */
#define CHIP_PIN_ID_PORT(id)  (((id) >> 29) & 0x07)
#define CHIP_PIN_ID_PIN(id)   (((id) >> 24) & 0x1F)
#define CHIP_PIN_ID_FUNC(id)  (((id) >>  0) & 0x07)
#define CHIP_PIN_ID_CFG(id)   (((id) >>  0) & 0xFFF8)


#define CHIP_PIN_IS_ANALOG(id)      (((CHIP_PIN_ID_PORT(id) == 0) && (((CHIP_PIN_ID_PIN(id) >=  11) && (CHIP_PIN_ID_PIN(id) <=  16)) \
                                                                     || (CHIP_PIN_ID_PIN(id) ==  22) \
                                                                     || (CHIP_PIN_ID_PIN(id) ==  23))) \
                                    || ((CHIP_PIN_ID_PORT(id) == 1) && ((CHIP_PIN_ID_PIN(id) ==  3) \
                                                                     || (CHIP_PIN_ID_PIN(id) ==  9) \
                                                                     || (CHIP_PIN_ID_PIN(id) ==  22) \
                                                                     || (CHIP_PIN_ID_PIN(id) ==  29))) \
                                    || ((CHIP_PIN_ID_PORT(id) == 2) && ((CHIP_PIN_ID_PIN(id) ==  0) \
                                                                     || (CHIP_PIN_ID_PIN(id) ==  1))))

#define CHIP_PIN_IS_TRUE_OD(id)     ((CHIP_PIN_ID_PORT(id) == 0) && ((CHIP_PIN_ID_PIN(id) ==  4) && (CHIP_PIN_ID_PIN(id) ==  5)))
#define CHIP_PIN_IS_HIGH_DRIVE(id)  (((CHIP_PIN_ID_PORT(id) == 0) && (CHIP_PIN_ID_PIN(id) ==  7)) \
                                    || ((CHIP_PIN_ID_PORT(id) == 1) && (CHIP_PIN_ID_PIN(id) ==  31)))


#define CHIP_PIN_CFG_NOPULL         LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_MODE, CHIP_REGFLDVAL_IOCON_PIO_MODE_NO_PULL)
#define CHIP_PIN_CFG_PULLUP         LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_MODE, CHIP_REGFLDVAL_IOCON_PIO_MODE_PU)
#define CHIP_PIN_CFG_PULLDOWN       LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_MODE, CHIP_REGFLDVAL_IOCON_PIO_MODE_PD)
#define CHIP_PIN_CFG_REPEATER       LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_MODE, CHIP_REGFLDVAL_IOCON_PIO_MODE_REPEATER)

#define CHIP_PIN_CFG_OTYPE_PP       LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_OD, 0) /* push-pull */
#define CHIP_PIN_CFG_OTYPE_OD       LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_OD, 1) /* open-drain */

#define CHIP_PIN_CFG_HYST           LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_HYS, 1)
#define CHIP_PIN_CFG_INVERTED       LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_INV, 1)
/* Признак аналогового пина. В самом регистре наоборот 1 - запрещение, но только для пинов с поддержкой,
 * что не удобно в конфигурации, т.к. требует не забывать флаг для всех функций и настроек,
 * поэтому при конфигурации инвертируем этот бит. */
#define CHIP_PIN_CFG_ANALOG         LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_ADMODE, 1)

#define CHIP_PIN_CFG_FLT(clkcnt, clkdiv) (LBITFIELD_SET(CHIP_REGFLD_IOCON_PIO_SMODE, clkcnt) | BITFIELD_SET(CHIP_REGFLD_IOCON_PIO_CLKDIV, clkdiv))







/* Адресация регистров LPC по pin ID */
#define CHIP_PIN_PIOREG_IOCON(id)   (CHIP_PIN_ID_PORT(id) == 0 ? &CHIP_REGS_IOCON->PIO0[CHIP_PIN_ID_PIN(id)] : \
                                     CHIP_PIN_ID_PORT(id) == 1 ? &CHIP_REGS_IOCON->PIO1[CHIP_PIN_ID_PIN(id)] : \
                                     (CHIP_PIN_ID_PORT(id) == 2) && (CHIP_PIN_ID_PIN(id) < 2) ?  \
                                        &CHIP_REGS_IOCON->PIO2A[CHIP_PIN_ID_PIN(id) < 2 ? CHIP_PIN_ID_PIN(id)  : 0] : \
                                        &CHIP_REGS_IOCON->PIO2B[CHIP_PIN_ID_PIN(id) >= 2 ? CHIP_PIN_ID_PIN(id)-2 : 0])

#define CHIP_PIN_CONFIG_EX_(id, iocon_mode) do { \
    *CHIP_PIN_PIOREG_IOCON(id) = (CHIP_PIN_ID_FUNC(id) & 0x7) | ((CHIP_PIN_IS_ANALOG(id) ? iocon_mode ^ CHIP_PIN_CFG_ANALOG : iocon_mode) & 0xFFFFFFF8); \
    } while(0)

#define CHIP_PIN_CONFIG_EX(id, iocon_mode) do { \
    int iocon_is_en = chip_per_clk_is_en(CHIP_PER_ID_IOCON); \
    chip_per_clk_en(CHIP_PER_ID_IOCON); \
    CHIP_PIN_CONFIG_EX_(id, iocon_mode); \
    if (!iocon_is_en) \
        chip_per_clk_dis(CHIP_PER_ID_IOCON); \
    } while(0)

#define CHIP_PIN_CONFIG(id)          CHIP_PIN_CONFIG_EX(id, CHIP_PIN_ID_CFG(id))

#define CHIP_PIN_GPIOREG(reg, id)    CHIP_REGS_GPIO->reg[CHIP_PIN_ID_PORT(id)]
#define CHIP_PIN_GPIOB(id)           CHIP_REGS_GPIO->B[CHIP_PIN_ID_PORT(id)][CHIP_PIN_ID_PIN(id)]
#define CHIP_PIN_BITMSK(id)          (1UL << CHIP_PIN_ID_PIN(id))





/* Управление ножками GPIO */
/* управление направлением */
#define CHIP_PIN_DIR_GET(id)        (!!(CHIP_PIN_GPIOREG(DIR, (id)) & CHIP_PIN_BITMSK(id)))
#define CHIP_PIN_SET_DIR_IN(id)     (CHIP_PIN_GPIOREG(DIR, (id)) &= ~CHIP_PIN_BITMSK(id))
#define CHIP_PIN_SET_DIR_OUT(id)    (CHIP_PIN_GPIOREG(DIR, (id)) |= CHIP_PIN_BITMSK(id))
/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val)       (CHIP_PIN_GPIOB(id) = (val))
/* чтение уровня на ножке (0 или 1) */
#define CHIP_PIN_IN(id)             CHIP_PIN_GPIOB(id)
/* атомарное инвертирование уровня на ножке */
#define CHIP_PIN_TOGGLE(id)         (CHIP_PIN_GPIOREG(NOT, (id)) = CHIP_PIN_BITMSK(id))
/* состояние регистра выхода (независимо от физического уровня) */
#define CHIP_PIN_GET_OUT_VAL(id)    (!!(CHIP_PIN_GPIOREG(SET, (id)) & CHIP_PIN_BITMSK(id)))


/* вспомогательный макрос, который конфигурирует порт на вывод с установленным
   начальным значением */
#define CHIP_PIN_CONFIG_OUT(id, val) do { \
                                        CHIP_PIN_OUT(id, val); \
                                        CHIP_PIN_SET_DIR_OUT(id); \
                                        CHIP_PIN_CONFIG(id); \
                                    } while(0)
/* вспомогательный макрос для настройки пина и конфигурирования его на вход */
#define CHIP_PIN_CONFIG_IN(id) do { \
                                    CHIP_PIN_SET_DIR_IN(id); \
                                    CHIP_PIN_CONFIG(id); \
                                } while(0)

#define CHIP_PIN_CONFIG_OUT_EX(id, cfg, val) do { \
                                        CHIP_PIN_OUT(id, val); \
                                        CHIP_PIN_SET_DIR_OUT(id); \
                                        CHIP_PIN_CONFIG_EX(id, cfg); \
                                    } while(0)

#define CHIP_PIN_CONFIG_IN_EX(id, cfg) do { \
                                        CHIP_PIN_SET_DIR_IN(id); \
                                        CHIP_PIN_CONFIG_EX(id, cfg); \
                                    } while(0)



/* ----------------------- Port 0 --------------------------------------------*/
/* RESET_PIO0_0 */
#define CHIP_PIN_P0_0_RESET         CHIP_PIN_ID         (0,0,0)
#define CHIP_PIN_P0_0_GPIO          CHIP_PIN_ID         (0,0,1)

/* PIO0_1 */
#define CHIP_PIN_P0_1_GPIO          CHIP_PIN_ID         (0,1,0)
#define CHIP_PIN_P0_1_CLKOUT        CHIP_PIN_ID         (0,1,1)
#define CHIP_PIN_P0_1_CT32B0_MAT2   CHIP_PIN_ID         (0,1,2)
#define CHIP_PIN_P0_1_USB_FTOGGLE   CHIP_PIN_ID         (0,1,3)

/* PIO0_2 */
#define CHIP_PIN_P0_2_GPIO          CHIP_PIN_ID         (0,2,0)
#define CHIP_PIN_P0_2_SSP0_SSEL     CHIP_PIN_ID         (0,2,1)
#define CHIP_PIN_P0_2_CT16B0_CAP0   CHIP_PIN_ID         (0,2,2)
/* -----                            CHIP_PIN_ID         (0,2,3) */

/* PIO0_3 */
#define CHIP_PIN_P0_3_GPIO          CHIP_PIN_ID         (0,3,0)
#define CHIP_PIN_P0_3_USB_VBUS      CHIP_PIN_ID         (0,3,1)
/* -----                            CHIP_PIN_ID         (0,3,2) */
/* -----                            CHIP_PIN_ID         (0,3,3) */

/* PIO0_4 - True OD */
#define CHIP_PIN_P0_4_GPIO          CHIP_PIN_ID         (0,4,0)
#define CHIP_PIN_P0_4_I2C0_SCL      CHIP_PIN_ID         (0,4,1)
/* -----                            CHIP_PIN_ID         (0,4,2) */
/* -----                            CHIP_PIN_ID         (0,4,3) */

/* PIO0_5 - True OD */
#define CHIP_PIN_P0_5_GPIO          CHIP_PIN_ID         (0,5,0)
#define CHIP_PIN_P0_5_I2C0_SDA      CHIP_PIN_ID         (0,5,1)
/* -----                            CHIP_PIN_ID         (0,5,2) */
/* -----                            CHIP_PIN_ID         (0,5,3) */

/* PIO0_6 */
#define CHIP_PIN_P0_6_GPIO          CHIP_PIN_ID         (0,6,0)
/* -----                            CHIP_PIN_ID         (0,6,1) */
#define CHIP_PIN_P0_6_SSP0_SCK      CHIP_PIN_ID         (0,6,2)
/* -----                            CHIP_PIN_ID         (0,6,3) */

/* PIO0_7 - High drive */
#define CHIP_PIN_P0_7_GPIO          CHIP_PIN_ID         (0,7,0)
#define CHIP_PIN_P0_7_U0_CTS        CHIP_PIN_ID         (0,7,1)
/* -----                            CHIP_PIN_ID         (0,7,2) */
#define CHIP_PIN_P0_7_I2C1_SCL      CHIP_PIN_ID         (0,7,3)

/* PIO0_8 */
#define CHIP_PIN_P0_8_GPIO          CHIP_PIN_ID         (0,8,0)
#define CHIP_PIN_P0_8_SSP0_MISO     CHIP_PIN_ID         (0,8,1)
#define CHIP_PIN_P0_8_CT16B0_MAT0   CHIP_PIN_ID         (0,8,2)
/* -----                            CHIP_PIN_ID         (0,8,3) */

/* PIO0_9 */
#define CHIP_PIN_P0_9_GPIO          CHIP_PIN_ID         (0,9,0)
#define CHIP_PIN_P0_9_SSP0_MOSI     CHIP_PIN_ID         (0,9,1)
#define CHIP_PIN_P0_9_CT16B0_MAT1   CHIP_PIN_ID         (0,9,2)
/* -----                            CHIP_PIN_ID         (0,9,3) */

/* SWCLK_PIO0_10 */
#define CHIP_PIN_P0_10_SWCLK        CHIP_PIN_ID         (0,10,0)
#define CHIP_PIN_P0_10_GPIO         CHIP_PIN_ID         (0,10,1)
#define CHIP_PIN_P0_10_SSP0_SCK     CHIP_PIN_ID         (0,10,2)
#define CHIP_PIN_P0_10_CT16B0_MAT2  CHIP_PIN_ID         (0,10,3)

/* TDI_PIO0_11 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_11_TDI          CHIP_PIN_ID         (0,11,0)
#define CHIP_PIN_P0_11_GPIO         CHIP_PIN_ID         (0,11,1)
#define CHIP_PIN_P0_11_ADC_9        CHIP_PIN_ID_ANALOG  (0,11,2)
#define CHIP_PIN_P0_11_CT32B0_MAT3  CHIP_PIN_ID         (0,11,3)
#define CHIP_PIN_P0_11_U1_RTS       CHIP_PIN_ID         (0,11,4)

/* TMS_PIO0_12 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_12_TMS          CHIP_PIN_ID         (0,12,0)
#define CHIP_PIN_P0_12_GPIO         CHIP_PIN_ID         (0,12,1)
#define CHIP_PIN_P0_12_ADC_8        CHIP_PIN_ID_ANALOG  (0,12,2)
#define CHIP_PIN_P0_12_CT32B1_CAP0  CHIP_PIN_ID         (0,12,3)
#define CHIP_PIN_P0_12_U1_CTS       CHIP_PIN_ID         (0,12,4)

/* TDO_PIO0_13 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_13_TDO          CHIP_PIN_ID         (0,13,0)
#define CHIP_PIN_P0_13_GPIO         CHIP_PIN_ID         (0,13,1)
#define CHIP_PIN_P0_13_ADC_7        CHIP_PIN_ID_ANALOG  (0,13,2)
#define CHIP_PIN_P0_13_CT32B1_MAT0  CHIP_PIN_ID         (0,13,3)
#define CHIP_PIN_P0_13_U1_RXD       CHIP_PIN_ID         (0,13,4)

/* TRST_PIO0_14 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_14_TRST         CHIP_PIN_ID         (0,14,0)
#define CHIP_PIN_P0_14_GPIO         CHIP_PIN_ID         (0,14,1)
#define CHIP_PIN_P0_14_ADC_6        CHIP_PIN_ID_ANALOG  (0,14,2)
#define CHIP_PIN_P0_14_CT32B1_MAT1  CHIP_PIN_ID         (0,14,3)
#define CHIP_PIN_P0_14_U1_TXD       CHIP_PIN_ID         (0,14,4)

/* SWDIO_PIO0_15 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_15_SWDIO        CHIP_PIN_ID         (0,15,0)
#define CHIP_PIN_P0_15_GPIO         CHIP_PIN_ID         (0,15,1)
#define CHIP_PIN_P0_15_ADC_3        CHIP_PIN_ID_ANALOG  (0,15,2)
#define CHIP_PIN_P0_15_CT32B1_MAT2  CHIP_PIN_ID         (0,15,3)

/* PIO0_16 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_16_GPIO         CHIP_PIN_ID         (0,16,0)
#define CHIP_PIN_P0_16_ADC_2        CHIP_PIN_ID_ANALOG  (0,16,1)
#define CHIP_PIN_P0_16_CT32B1_MAT3  CHIP_PIN_ID         (0,16,2)
/* -----                            CHIP_PIN_ID         (0,16,3) */

/* PIO0_17 */
#define CHIP_PIN_P0_17_GPIO         CHIP_PIN_ID         (0,17,0)
#define CHIP_PIN_P0_17_U0_RTS       CHIP_PIN_ID         (0,17,1)
#define CHIP_PIN_P0_17_CT32B0_CAP0  CHIP_PIN_ID         (0,17,2)
#define CHIP_PIN_P0_17_U0_SCLK      CHIP_PIN_ID         (0,17,3)

/* PIO0_18 */
#define CHIP_PIN_P0_18_GPIO         CHIP_PIN_ID         (0,18,0)
#define CHIP_PIN_P0_18_U0_RXD       CHIP_PIN_ID         (0,18,1)
#define CHIP_PIN_P0_18_CT32B0_MAT0  CHIP_PIN_ID         (0,18,2)

/* PIO0_19 */
#define CHIP_PIN_P0_19_GPIO         CHIP_PIN_ID         (0,19,0)
#define CHIP_PIN_P0_19_U0_TXD       CHIP_PIN_ID         (0,19,1)
#define CHIP_PIN_P0_19_CT32B0_MAT1  CHIP_PIN_ID         (0,19,2)

/* PIO0_20 */
#define CHIP_PIN_P0_20_GPIO         CHIP_PIN_ID         (0,20,0)
#define CHIP_PIN_P0_20_CT16B1_CAP0  CHIP_PIN_ID         (0,20,1)
#define CHIP_PIN_P0_20_U2_RXD       CHIP_PIN_ID         (0,20,2)

/* PIO0_21 */
#define CHIP_PIN_P0_21_GPIO         CHIP_PIN_ID         (0,21,0)
#define CHIP_PIN_P0_21_CT16B1_MAT0  CHIP_PIN_ID         (0,21,1)
#define CHIP_PIN_P0_21_SSP1_MOSI    CHIP_PIN_ID         (0,21,2)

/* PIO0_22 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_22_GPIO         CHIP_PIN_ID         (0,22,0)
#define CHIP_PIN_P0_22_ADC_11       CHIP_PIN_ID_ANALOG  (0,22,1)
#define CHIP_PIN_P0_22_CT16B1_CAP1  CHIP_PIN_ID         (0,22,2)
#define CHIP_PIN_P0_22_SSP1_MISO    CHIP_PIN_ID         (0,22,3)

/* PIO0_23 - Analog, Glitch Filter*/
#define CHIP_PIN_P0_23_GPIO         CHIP_PIN_ID         (0,23,0)
#define CHIP_PIN_P0_23_ADC_1        CHIP_PIN_ID_ANALOG  (0,23,1)
#define CHIP_PIN_P0_23_R_9          CHIP_PIN_ID         (0,23,2)
#define CHIP_PIN_P0_23_U0_RI        CHIP_PIN_ID         (0,23,3)
#define CHIP_PIN_P0_23_SSP1_SSEL    CHIP_PIN_ID         (0,23,4)



/* ------------------------ Port 1 ------------------------------------------*/
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
/* PIO1_0 */
#define CHIP_PIN_P1_0_GPIO          CHIP_PIN_ID         (1,0,0)
#define CHIP_PIN_P1_0_CT32B1_MAT0   CHIP_PIN_ID         (1,0,1)
#define CHIP_PIN_P1_0_R_10          CHIP_PIN_ID         (1,0,2)
#define CHIP_PIN_P1_0_U2_TXD        CHIP_PIN_ID         (1,0,3)
#endif

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO1_1 */
#define CHIP_PIN_P1_1_GPIO          CHIP_PIN_ID         (1,1,0)
#define CHIP_PIN_P1_1_CT32B1_MAT1   CHIP_PIN_ID         (1,1,1)
/* -----                            CHIP_PIN_ID         (1,1,2) */
#define CHIP_PIN_P1_1_U0_DTR        CHIP_PIN_ID         (1,1,3)

/* PIO1_2 */
#define CHIP_PIN_P1_2_GPIO          CHIP_PIN_ID         (1,2,0)
#define CHIP_PIN_P1_2_CT32B1_MAT2   CHIP_PIN_ID         (1,2,1)
/* -----                            CHIP_PIN_ID         (1,2,2) */
#define CHIP_PIN_P1_2_U1_RXD        CHIP_PIN_ID         (1,2,3)

/* PIO1_3 - Analog, Glitch Filter */
#define CHIP_PIN_P1_3_GPIO          CHIP_PIN_ID         (1,3,0)
#define CHIP_PIN_P1_3_CT32B1_MAT3   CHIP_PIN_ID         (1,3,1)
/* -----                            CHIP_PIN_ID         (1,3,2) */
#define CHIP_PIN_P1_3_I2C1_SDA      CHIP_PIN_ID         (1,3,3)
#define CHIP_PIN_P1_3_ADC_5         CHIP_PIN_ID_ANALOG  (1,3,4)

/* PIO1_4 */
#define CHIP_PIN_P1_4_GPIO          CHIP_PIN_ID         (1,4,0)
#define CHIP_PIN_P1_4_CT32B1_CAP0   CHIP_PIN_ID         (1,4,1)
/* -----                            CHIP_PIN_ID         (1,4,2) */
#define CHIP_PIN_P1_4_U0_DSR        CHIP_PIN_ID         (1,4,3)

/* PIO1_5 */
#define CHIP_PIN_P1_5_GPIO          CHIP_PIN_ID         (1,5,0)
#define CHIP_PIN_P1_5_CT32B1_CAP1   CHIP_PIN_ID         (1,5,1)
/* -----                            CHIP_PIN_ID         (1,5,2) */
#define CHIP_PIN_P1_5_U0_DCD        CHIP_PIN_ID         (1,5,3)

/* PIO1_6 */
#define CHIP_PIN_P1_6_GPIO          CHIP_PIN_ID         (1,6,0)
/* -----                            CHIP_PIN_ID         (1,6,1) */
#define CHIP_PIN_P1_6_U2_RXD        CHIP_PIN_ID         (1,6,2)
#define CHIP_PIN_P1_6_CT32B0_CAP1   CHIP_PIN_ID         (1,6,3)
#endif

/* PIO1_7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_P1_7_GPIO          CHIP_PIN_ID         (1,7,0)
/* -----                            CHIP_PIN_ID         (1,7,1) */
#define CHIP_PIN_P1_7_U2_CTS        CHIP_PIN_ID         (1,7,2)
#define CHIP_PIN_P1_7_CT32B1_CAP0   CHIP_PIN_ID         (1,7,3)
#endif

/* PIO1_8 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_P1_8_GPIO          CHIP_PIN_ID         (1,8,0)
/* -----                            CHIP_PIN_ID         (1,8,1) */
#define CHIP_PIN_P1_8_U1_TXD        CHIP_PIN_ID         (1,8,2)
#define CHIP_PIN_P1_8_CT16B0_CAP0   CHIP_PIN_ID         (1,8,3)
#endif


#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
/* PIO1_9 - Analog, Glitch Filter */
#define CHIP_PIN_P1_9_GPIO          CHIP_PIN_ID         (1,9,0)
#define CHIP_PIN_P1_9_U0_CTS        CHIP_PIN_ID         (1,9,1)
#define CHIP_PIN_P1_9_CT16b1_MAT1   CHIP_PIN_ID         (1,9,2)
#define CHIP_PIN_P1_9_ADC_0         CHIP_PIN_ID_ANALOG  (1,9,3)

/* PIO1_10 */
#define CHIP_PIN_P1_10_GPIO         CHIP_PIN_ID         (1,10,0)
#define CHIP_PIN_P1_10_U2_RTS       CHIP_PIN_ID         (1,10,1)
#define CHIP_PIN_P1_10_U2_SCLK      CHIP_PIN_ID         (1,10,2)
#define CHIP_PIN_P1_10_CT16B1_MAT0  CHIP_PIN_ID         (1,10,3)
#endif

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO1_11 */
#define CHIP_PIN_P1_11_GPIO         CHIP_PIN_ID         (1,11,0)
#define CHIP_PIN_P1_11_I2C1_SCL     CHIP_PIN_ID         (1,11,1)
#define CHIP_PIN_P1_11_CT16B0_MAT2  CHIP_PIN_ID         (1,11,2)
#define CHIP_PIN_P1_11_U0_RI        CHIP_PIN_ID         (1,11,3)

/* PIO1_12 */
#define CHIP_PIN_P1_12_GPIO         CHIP_PIN_ID         (1,12,0)
#define CHIP_PIN_P1_12_SSP0_MOSI    CHIP_PIN_ID         (1,12,1)
#define CHIP_PIN_P1_12_CT16B0_MAT1  CHIP_PIN_ID         (1,12,2)
/* -----                            CHIP_PIN_ID         (1,12,3) */
#endif

/* PIO1_13 */
#define CHIP_PIN_P1_13_GPIO         CHIP_PIN_ID         (1,13,0)
#define CHIP_PIN_P1_13_U1_CTS       CHIP_PIN_ID         (1,13,1)
#define CHIP_PIN_P1_13_SCT0_OUT3    CHIP_PIN_ID         (1,13,2)
/* -----                            CHIP_PIN_ID         (1,13,3) */

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO1_14 */
#define CHIP_PIN_P1_14_GPIO         CHIP_PIN_ID         (1,14,0)
#define CHIP_PIN_P1_14_I2C1_SDA     CHIP_PIN_ID         (1,14,1)
#define CHIP_PIN_P1_14_CT32B1_MAT2  CHIP_PIN_ID         (1,14,2)
/* -----                            CHIP_PIN_ID         (1,14,3) */

/* PIO1_15 */
#define CHIP_PIN_P1_15_GPIO         CHIP_PIN_ID         (1,15,0)
#define CHIP_PIN_P1_15_SSP0_SSEL    CHIP_PIN_ID         (1,15,1)
#define CHIP_PIN_P1_15_CT32B1_MAT3  CHIP_PIN_ID         (1,15,2)
/* -----                            CHIP_PIN_ID         (1,15,3) */

/* PIO1_16 */
#define CHIP_PIN_P1_16_GPIO         CHIP_PIN_ID         (1,16,0)
#define CHIP_PIN_P1_16_SSP0_MISO    CHIP_PIN_ID         (1,16,1)
#define CHIP_PIN_P1_16_CT16B0_MAT0  CHIP_PIN_ID         (1,16,2)
/* -----                            CHIP_PIN_ID         (1,16,3) */

/* PIO1_17 */
#define CHIP_PIN_P1_17_GPIO         CHIP_PIN_ID         (1,17,0)
#define CHIP_PIN_P1_17_CT16B0_CAP2  CHIP_PIN_ID         (1,17,1)
#define CHIP_PIN_P1_17_U0_RXD       CHIP_PIN_ID         (1,17,2)
/* -----                            CHIP_PIN_ID         (1,17,3) */

/* PIO1_18 */
#define CHIP_PIN_P1_18_GPIO         CHIP_PIN_ID         (1,18,0)
#define CHIP_PIN_P1_18_CT16B1_CAP1  CHIP_PIN_ID         (1,18,1)
#define CHIP_PIN_P1_18_U0_TXD       CHIP_PIN_ID         (1,18,2)
/* -----                            CHIP_PIN_ID         (1,18,3) */
#endif

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
/* PIO1_19 */
#define CHIP_PIN_P1_19_GPIO         CHIP_PIN_ID         (1,19,0)
#define CHIP_PIN_P1_19_U2_CTS       CHIP_PIN_ID         (1,19,1)
#define CHIP_PIN_P1_19_SCT0_OUT0    CHIP_PIN_ID         (1,19,2)
/* -----                            CHIP_PIN_ID         (1,19,3) */
#endif

/* PIO1_20 */
#define CHIP_PIN_P1_20_GPIO         CHIP_PIN_ID         (1,20,0)
#define CHIP_PIN_P1_20_U0_DSR       CHIP_PIN_ID         (1,20,1)
#define CHIP_PIN_P1_20_SSP1_SCK     CHIP_PIN_ID         (1,20,2)
#define CHIP_PIN_P1_20_CT16B0_MAT0  CHIP_PIN_ID         (1,20,3)

/* PIO1_21 */
#define CHIP_PIN_P1_21_GPIO         CHIP_PIN_ID         (1,21,0)
#define CHIP_PIN_P1_21_U0_DCD       CHIP_PIN_ID         (1,21,1)
#define CHIP_PIN_P1_21_SSP1_MISO    CHIP_PIN_ID         (1,21,2)
#define CHIP_PIN_P1_21_CT16B0_CAP1  CHIP_PIN_ID         (1,21,3)


#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO1_22 - Analog, Glitch Filter */
#define CHIP_PIN_P1_22_GPIO         CHIP_PIN_ID         (1,22,0)
#define CHIP_PIN_P1_22_SSP1_MOSI    CHIP_PIN_ID         (1,22,1)
#define CHIP_PIN_P1_22_CT32B1_CAP1  CHIP_PIN_ID         (1,22,2)
#define CHIP_PIN_P1_22_ADC_4        CHIP_PIN_ID_ANALOG  (1,22,3)
/* -----                            CHIP_PIN_ID         (1,12,4) */
#endif

/* PIO1_23 */
#define CHIP_PIN_P1_23_GPIO         CHIP_PIN_ID         (1,23,0)
#define CHIP_PIN_P1_23_CT16B1_MAT1  CHIP_PIN_ID         (1,23,1)
#define CHIP_PIN_P1_23_SSP1_SSEL    CHIP_PIN_ID         (1,23,2)
#define CHIP_PIN_P1_23_U2_TXD       CHIP_PIN_ID         (1,23,3)

/* PIO1_24 */
#define CHIP_PIN_P1_24_GPIO         CHIP_PIN_ID         (1,24,0)
#define CHIP_PIN_P1_24_CT32B0_MAT0  CHIP_PIN_ID         (1,24,1)
#define CHIP_PIN_P1_24_I2C1_SDA     CHIP_PIN_ID         (1,24,2)

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO1_25 */
#define CHIP_PIN_P1_25_GPIO         CHIP_PIN_ID         (1,25,0)
#define CHIP_PIN_P1_25_U2_RTS       CHIP_PIN_ID         (1,25,1)
#define CHIP_PIN_P1_25_U2_SCLK      CHIP_PIN_ID         (1,25,2)
#define CHIP_PIN_P1_25_SCT0_IN0     CHIP_PIN_ID         (1,25,3)
/* -----                            CHIP_PIN_ID         (1,25,4) */
#endif

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
/* PIO1_26 */
#define CHIP_PIN_P1_26_GPIO         CHIP_PIN_ID         (1,26,0)
#define CHIP_PIN_P1_26_CT32B0_MAT2  CHIP_PIN_ID         (1,26,1)
#define CHIP_PIN_P1_26_U0_RXD       CHIP_PIN_ID         (1,26,2)
/* -----                            CHIP_PIN_ID         (1,26,3) */

/* PIO1_27 */
#define CHIP_PIN_P1_27_GPIO         CHIP_PIN_ID         (1,27,0)
#define CHIP_PIN_P1_27_CT32B0_MAT3  CHIP_PIN_ID         (1,27,1)
#define CHIP_PIN_P1_27_U0_TXD       CHIP_PIN_ID         (1,27,2)
/* -----                            CHIP_PIN_ID         (1,27,3) */
#define CHIP_PIN_P1_27_SSP1_SCK     CHIP_PIN_ID         (1,27,4)

/* PIO1_28 */
#define CHIP_PIN_P1_28_GPIO         CHIP_PIN_ID         (1,28,0)
#define CHIP_PIN_P1_28_CT32B0_CAP0  CHIP_PIN_ID         (1,28,1)
#define CHIP_PIN_P1_28_U0_SCLK      CHIP_PIN_ID         (1,28,2)
#define CHIP_PIN_P1_28_U0_RTS       CHIP_PIN_ID         (1,28,3)

/* PIO1_29 - Analog, Glitch Filter */
#define CHIP_PIN_P1_29_GPIO         CHIP_PIN_ID         (1,29,0)
#define CHIP_PIN_P1_29_SSP0_SCK     CHIP_PIN_ID         (1,29,1)
#define CHIP_PIN_P1_29_CT32B0_CAP1  CHIP_PIN_ID         (1,29,2)
#define CHIP_PIN_P1_29_U0_DTRn      CHIP_PIN_ID         (1,29,3)
#define CHIP_PIN_P1_29_ADC_10       CHIP_PIN_ID_ANALOG  (1,29,4)

/* PIO1_30 */
#define CHIP_PIN_P1_30_GPIO         CHIP_PIN_ID         (1,30,0)
#define CHIP_PIN_P1_30_I2C1_SCL     CHIP_PIN_ID         (1,30,1)
#define CHIP_PIN_P1_30_SCT0_IN3     CHIP_PIN_ID         (1,30,2)
/* -----                            CHIP_PIN_ID         (1,30,3) */
#endif

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO1_31 - High Drive */
#define CHIP_PIN_P1_31_GPIO         CHIP_PIN_ID         (1,31,0)
#endif


/* --------------------- Port 2 ----------------------------------------------*/
/* PIO2_0 - Analog, Glitch Filter */
#define CHIP_PIN_P2_0_GPIO          CHIP_PIN_ID         (2,0,0)
#define CHIP_PIN_P2_0_XTALIN        CHIP_PIN_ID_ANALOG  (2,0,1)

/* PIO2_1 - Analog, Glitch Filter */
#define CHIP_PIN_P2_1_GPIO          CHIP_PIN_ID         (2,1,0)
#define CHIP_PIN_P2_1_XTALOUT       CHIP_PIN_ID_ANALOG  (2,1,1)

/* PIO2_2 */
#define CHIP_PIN_P2_2_GPIO          CHIP_PIN_ID         (2,2,0)
#define CHIP_PIN_P2_2_U3_RTS        CHIP_PIN_ID         (2,2,1)
#define CHIP_PIN_P2_2_U3_SCLK       CHIP_PIN_ID         (2,2,2)
#define CHIP_PIN_P2_2_SCT0_OUT1     CHIP_PIN_ID         (2,2,3)

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO2_3 */
#define CHIP_PIN_P2_3_GPIO          CHIP_PIN_ID         (2,3,0)
#define CHIP_PIN_P2_3_U3_RXD        CHIP_PIN_ID         (2,3,1)
#define CHIP_PIN_P2_3_CT32B0_MAT1   CHIP_PIN_ID         (2,3,2)

/* PIO2_4 */
#define CHIP_PIN_P2_4_GPIO          CHIP_PIN_ID         (2,4,0)
#define CHIP_PIN_P2_4_U3_TXD        CHIP_PIN_ID         (2,4,1)
#define CHIP_PIN_P2_4_CT32B0_MAT2   CHIP_PIN_ID         (2,4,2)
#endif

/* PIO2_5 */
#define CHIP_PIN_P2_5_GPIO          CHIP_PIN_ID         (2,5,0)
#define CHIP_PIN_P2_5_U3_CTS        CHIP_PIN_ID         (2,5,1)
#define CHIP_PIN_P2_5_SCT0_IN1      CHIP_PIN_ID         (2,5,2)

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
/* PIO2_6 */
#define CHIP_PIN_P2_6_GPIO          CHIP_PIN_ID         (2,6,0)
#define CHIP_PIN_P2_6_U1_RTS        CHIP_PIN_ID         (2,6,1)
#define CHIP_PIN_P2_6_U1_SCLK       CHIP_PIN_ID         (2,6,2)
#define CHIP_PIN_P2_6_SCT0_IN2      CHIP_PIN_ID         (2,6,3)
#endif

/* PIO2_7 */
#define CHIP_PIN_P2_7_GPIO          CHIP_PIN_ID         (2,7,0)
#define CHIP_PIN_P2_7_SSP0_SCK      CHIP_PIN_ID         (2,7,1)
#define CHIP_PIN_P2_7_SCT0_OUT2     CHIP_PIN_ID         (2,7,2)

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO2_8 */
#define CHIP_PIN_P2_8_GPIO          CHIP_PIN_ID         (2,8,0)
#define CHIP_PIN_P2_8_SCT1_IN0      CHIP_PIN_ID         (2,8,1)

/* PIO2_9 */
#define CHIP_PIN_P2_9_GPIO          CHIP_PIN_ID         (2,9,0)
#define CHIP_PIN_P2_9_SCT1_IN1      CHIP_PIN_ID         (2,9,1)

/* PIO2_10 */
#define CHIP_PIN_P2_10_GPIO         CHIP_PIN_ID         (2,10,0)
#define CHIP_PIN_P2_10_U4_RTS       CHIP_PIN_ID         (2,10,1)
#define CHIP_PIN_P2_10_U4_SCLK      CHIP_PIN_ID         (2,10,2)

/* PIO2_11 */
#define CHIP_PIN_P2_11_GPIO         CHIP_PIN_ID         (2,11,0)
#define CHIP_PIN_P2_11_U4_RXD       CHIP_PIN_ID         (2,11,1)

/* PIO2_12 */
#define CHIP_PIN_P2_12_GPIO         CHIP_PIN_ID         (2,12,0)
#define CHIP_PIN_P2_12_U4_TXD       CHIP_PIN_ID         (2,12,1)

/* PIO2_13 */
#define CHIP_PIN_P2_13_GPIO         CHIP_PIN_ID         (2,13,0)
#define CHIP_PIN_P2_13_U4_CTS       CHIP_PIN_ID         (2,13,1)

/* PIO2_14 */
#define CHIP_PIN_P2_14_GPIO         CHIP_PIN_ID         (2,14,0)
#define CHIP_PIN_P2_14_SCT1_IN2     CHIP_PIN_ID         (2,14,1)
#endif


#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
/* PIO2_15 */
#define CHIP_PIN_P2_15_GPIO         CHIP_PIN_ID         (2,15,0)
#define CHIP_PIN_P2_15_SCT1_IN3     CHIP_PIN_ID         (2,15,1)
#endif


#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO2_16 */
#define CHIP_PIN_P2_16_GPIO         CHIP_PIN_ID         (2,16,0)
#define CHIP_PIN_P2_16_SCT1_OUT0    CHIP_PIN_ID         (2,16,1)

/* PIO2_17 */
#define CHIP_PIN_P2_17_GPIO         CHIP_PIN_ID         (2,17,0)
#define CHIP_PIN_P2_17_SCT1_OUT1    CHIP_PIN_ID         (2,17,1)
#endif

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
/* PIO2_18 */
#define CHIP_PIN_P2_18_GPIO         CHIP_PIN_ID         (2,18,0)
#define CHIP_PIN_P2_18_SCT1_OUT2    CHIP_PIN_ID         (2,18,1)

/* PIO2_19 */
#define CHIP_PIN_P2_19_GPIO         CHIP_PIN_ID         (2,19,0)
#define CHIP_PIN_P2_19_SCT1_OUT3    CHIP_PIN_ID         (2,19,1)
#endif

#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
/* PIO2_20 */
#define CHIP_PIN_P2_20_GPIO         CHIP_PIN_ID         (2,20,0)

/* PIO2_21 */
#define CHIP_PIN_P2_21_GPIO         CHIP_PIN_ID         (2,21,0)

/* PIO2_22 */
#define CHIP_PIN_P2_22_GPIO         CHIP_PIN_ID         (2,22,0)

/* PIO2_23 */
#define CHIP_PIN_P2_23_GPIO         CHIP_PIN_ID         (2,23,0)
#endif

#endif // CHIP_PINS_H
