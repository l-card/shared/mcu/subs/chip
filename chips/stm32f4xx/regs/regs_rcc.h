#ifndef CHIP_STM32F4XX_REGS_RCC_H
#define CHIP_STM32F4XX_REGS_RCC_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR;            /*!< RCC clock control register,                                  Address offset: 0x00 */
    __IO uint32_t PLLCFGR;       /*!< RCC PLL configuration register,                              Address offset: 0x04 */
    __IO uint32_t CFGR;          /*!< RCC clock configuration register,                            Address offset: 0x08 */
    __IO uint32_t CIR;           /*!< RCC clock interrupt register,                                Address offset: 0x0C */
    __IO uint32_t AHB1RSTR;      /*!< RCC AHB1 peripheral reset register,                          Address offset: 0x10 */
    __IO uint32_t AHB2RSTR;      /*!< RCC AHB2 peripheral reset register,                          Address offset: 0x14 */
    __IO uint32_t AHB3RSTR;      /*!< RCC AHB3 peripheral reset register,                          Address offset: 0x18 */
    uint32_t      RESERVED0;     /*!< Reserved, 0x1C                                                                    */
    __IO uint32_t APB1RSTR;      /*!< RCC APB1 peripheral reset register,                          Address offset: 0x20 */
    __IO uint32_t APB2RSTR;      /*!< RCC APB2 peripheral reset register,                          Address offset: 0x24 */
    uint32_t      RESERVED1[2];  /*!< Reserved, 0x28-0x2C                                                               */
    __IO uint32_t AHB1ENR;       /*!< RCC AHB1 peripheral clock register,                          Address offset: 0x30 */
    __IO uint32_t AHB2ENR;       /*!< RCC AHB2 peripheral clock register,                          Address offset: 0x34 */
    __IO uint32_t AHB3ENR;       /*!< RCC AHB3 peripheral clock register,                          Address offset: 0x38 */
    uint32_t      RESERVED2;     /*!< Reserved, 0x3C                                                                    */
    __IO uint32_t APB1ENR;       /*!< RCC APB1 peripheral clock enable register,                   Address offset: 0x40 */
    __IO uint32_t APB2ENR;       /*!< RCC APB2 peripheral clock enable register,                   Address offset: 0x44 */
    uint32_t      RESERVED3[2];  /*!< Reserved, 0x48-0x4C                                                               */
    __IO uint32_t AHB1LPENR;     /*!< RCC AHB1 peripheral clock enable in low power mode register, Address offset: 0x50 */
    __IO uint32_t AHB2LPENR;     /*!< RCC AHB2 peripheral clock enable in low power mode register, Address offset: 0x54 */
    __IO uint32_t AHB3LPENR;     /*!< RCC AHB3 peripheral clock enable in low power mode register, Address offset: 0x58 */
    uint32_t      RESERVED4;     /*!< Reserved, 0x5C                                                                    */
    __IO uint32_t APB1LPENR;     /*!< RCC APB1 peripheral clock enable in low power mode register, Address offset: 0x60 */
    __IO uint32_t APB2LPENR;     /*!< RCC APB2 peripheral clock enable in low power mode register, Address offset: 0x64 */
    uint32_t      RESERVED5[2];  /*!< Reserved, 0x68-0x6C                                                               */
    __IO uint32_t BDCR;          /*!< RCC Backup domain control register,                          Address offset: 0x70 */
    __IO uint32_t CSR;           /*!< RCC clock control & status register,                         Address offset: 0x74 */
    uint32_t      RESERVED6[2];  /*!< Reserved, 0x78-0x7C                                                               */
    __IO uint32_t SSCGR;         /*!< RCC spread spectrum clock generation register,               Address offset: 0x80 */
    __IO uint32_t PLLI2SCFGR;    /*!< RCC PLLI2S configuration register,                           Address offset: 0x84 */
  } CHIP_REGS_RCC_T;

#define CHIP_REGS_RCC          ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)

/*********** RCC Clock control register (RCC_CR) ******************************/
#define CHIP_REGFLD_RCC_CR_HSI_ON                   (0x00000001UL <<  0U)   /* (RW) HSI clock enable */
#define CHIP_REGFLD_RCC_CR_HSI_RDY                  (0x00000001UL <<  1U)   /* (RO) HSI clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSI_TRIM                 (0x0000001FUL <<  3U)   /* (RW) HSI clock trimming */
#define CHIP_REGFLD_RCC_CR_HSI_CAL                  (0x000000FFUL <<  8U)   /* (RO) HSI clock calibration */
#define CHIP_REGFLD_RCC_CR_HSE_ON                   (0x00000001UL << 16U)   /* (RW) HSE clock enable */
#define CHIP_REGFLD_RCC_CR_HSE_RDY                  (0x00000001UL << 17U)   /* (RO) HSE clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSE_BYP                  (0x00000001UL << 18U)   /* (RW) HSE crystal oscillator bypass */
#define CHIP_REGFLD_RCC_CR_CSS_ON                   (0x00000001UL << 19U)   /* (RW) Clock security system enable */
#define CHIP_REGFLD_RCC_CR_PLL_ON                   (0x00000001UL << 24U)   /* (RW) PLL enable */
#define CHIP_REGFLD_RCC_CR_PLL_RDY                  (0x00000001UL << 25U)   /* (RW) PLL clock ready flag */
#define CHIP_REGFLD_RCC_CR_PLL_I2S_ON               (0x00000001UL << 26U)   /* (RW) PLLI2S enable */
#define CHIP_REGFLD_RCC_CR_PLL_I2S_RDY              (0x00000001UL << 27U)   /* (RW) PLLI2S clock ready flag */
#define CHIP_REGMSK_RCC_CR_RESERVED                 (0xF0F00004UL)

/*********** RCC PLL configuration register (RCC_PLLCFGR) *********************/
#define CHIP_REGFLD_RCC_PLLCFGR_PLL_M               (0x0000003FUL <<  0U)   /* (RW) Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL_N               (0x000001FFUL <<  6U)   /* (RW) Main PLL (PLL) multiplication factor for VCO */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL_P               (0x00000003UL << 16U)   /* (RW) Main PLL (PLL) division factor for main system clock */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL_SRC             (0x00000001UL << 22U)   /* (RW) Main PLL(PLL) and audio PLL (PLLI2S) entry clock source */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL_Q               (0x0000000FUL << 24U)   /* (RW) Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks */
#define CHIP_REGMSK_RCC_PLLCFGR_RESERVED            (0xF0BC8000UL)


#define CHIP_REGFLDVAL_RCC_PLLCFGR_PLL_P_2          0
#define CHIP_REGFLDVAL_RCC_PLLCFGR_PLL_P_4          1
#define CHIP_REGFLDVAL_RCC_PLLCFGR_PLL_P_6          2
#define CHIP_REGFLDVAL_RCC_PLLCFGR_PLL_P_8          3

#define CHIP_REGFLDVAL_RCC_PLLCFGR_PLL_SRC_HSI      0
#define CHIP_REGFLDVAL_RCC_PLLCFGR_PLL_SRC_HSE      1

/*********** RCC clock configuration register (RCC_CFGR) **********************/
#define CHIP_REGFLD_RCC_CFGR_SW                     (0x00000003UL <<  0U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_SWS                    (0x00000003UL <<  2U)   /* (RO) */
#define CHIP_REGFLD_RCC_CFGR_HPRE                   (0x0000000FUL <<  4U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_PPRE1                  (0x00000007UL << 10U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_PPRE2                  (0x00000007UL << 10U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_RTCPRE                 (0x0000001FUL << 16U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_MCO1                   (0x00000003UL << 21U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_I2S_SRC                (0x00000001UL << 23U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_MCO1_PRE               (0x00000007UL << 24U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_MCO2_PRE               (0x00000007UL << 27U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_MCO2                   (0x00000003UL << 30U)   /* (RW) Microcontroller clock output 2 */
#define CHIP_REGMSK_RCC_CFGR_RESERVED               (0x00000300UL)


#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSI              0
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSE              1
#define CHIP_REGFLDVAL_RCC_CFGR_SW_PLL              2

#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_2              8
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_4              9
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_8              10
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_16             11
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_64             12
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_128            13
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_256            14
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_512            15


#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_2              4
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_4              5
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_8              6
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_16             7

#define CHIP_REGFLDVAL_RCC_CFGR_I2S_SRC_PLL_I2S     0
#define CHIP_REGFLDVAL_RCC_CFGR_I2S_SRC_I2S_CKIN    1

#define CHIP_REGFLDVAL_RCC_CFGR_MCO1_HSI            0
#define CHIP_REGFLDVAL_RCC_CFGR_MCO1_LSE            1
#define CHIP_REGFLDVAL_RCC_CFGR_MCO1_HSE            2
#define CHIP_REGFLDVAL_RCC_CFGR_MCO1_PLL            3

#define CHIP_REGFLDVAL_RCC_CFGR_MCO2_SYSCLK         0
#define CHIP_REGFLDVAL_RCC_CFGR_MCO2_PLL_I2S        1
#define CHIP_REGFLDVAL_RCC_CFGR_MCO2_HSE            2
#define CHIP_REGFLDVAL_RCC_CFGR_MCO2_PLL            3

#define CHIP_REGFLDVAL_RCC_CFGR_MCOPRE_1            0
#define CHIP_REGFLDVAL_RCC_CFGR_MCOPRE_2            4
#define CHIP_REGFLDVAL_RCC_CFGR_MCOPRE_3            5
#define CHIP_REGFLDVAL_RCC_CFGR_MCOPRE_4            6
#define CHIP_REGFLDVAL_RCC_CFGR_MCOPRE_5            7


/*********** RCC clock interrupt register (RCC_CIR) ***************************/
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_F               (0x00000001UL <<  0U)   /* (RO) LSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_F               (0x00000001UL <<  1U)   /* (RO) LSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_F               (0x00000001UL <<  2U)   /* (RO) HSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_F               (0x00000001UL <<  3U)   /* (RO) HSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_F               (0x00000001UL <<  4U)   /* (RO) Main PLL (PLL) ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_PLL_I2S_RDY_F           (0x00000001UL <<  5U)   /* (RO) PLL_I2S ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_CSS_F                   (0x00000001UL <<  7U)   /* (RO) Clock security system interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_IE              (0x00000001UL <<  8U)   /* (RW) LSI ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_IE              (0x00000001UL <<  9U)   /* (RW) LSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_IE              (0x00000001UL << 10U)   /* (RW) HSI ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_IE              (0x00000001UL << 11U)   /* (RW) HSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_IE              (0x00000001UL << 12U)   /* (RW) Main PLL (PLL) ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_PLL_I2S_RDY_IE          (0x00000001UL << 13U)   /* (RW) PLL_I2S ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_C               (0x00000001UL << 16U)   /* (RW1C) LSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_C               (0x00000001UL << 17U)   /* (RW1C) LSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_C               (0x00000001UL << 18U)   /* (RW1C) HSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_C               (0x00000001UL << 19U)   /* (RW1C) HSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_C               (0x00000001UL << 20U)   /* (RW1C) Main PLL(PLL) ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_PLL_I2S_RDY_C           (0x00000001UL << 21U)   /* (RW1C) PLL_I2S ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_CSS_C                   (0x00000001UL << 23U)   /* (RW1C) Clock security system interrupt clear */
#define CHIP_REGMSK_RCC_CIR_RESERVED                (0xFF40C040UL)

/*********** RCC AHB1 peripheral reset register (RCC_AHB1RSTR) ****************/
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOA_RST          (0x00000001UL <<  0U)   /* (RW) IO port A reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOB_RST          (0x00000001UL <<  1U)   /* (RW) IO port B reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOC_RST          (0x00000001UL <<  2U)   /* (RW) IO port C reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOD_RST          (0x00000001UL <<  3U)   /* (RW) IO port D reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOE_RST          (0x00000001UL <<  4U)   /* (RW) IO port E reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOF_RST          (0x00000001UL <<  5U)   /* (RW) IO port F reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOG_RST          (0x00000001UL <<  6U)   /* (RW) IO port G reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOH_RST          (0x00000001UL <<  7U)   /* (RW) IO port H reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_GPIOI_RST          (0x00000001UL <<  8U)   /* (RW) IO port I reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_CRC_RST            (0x00000001UL << 12U)   /* (RW) CRC reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_DMA1_RST           (0x00000001UL << 21U)   /* (RW) DMA1 reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_DMA2_RST           (0x00000001UL << 22U)   /* (RW) DMA2 reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_ETH_RST            (0x00000001UL << 25U)   /* (RW) Ethernet MAC reset */
#define CHIP_REGFLD_RCC_AHB1RSTR_USBHS_RST          (0x00000001UL << 29U)   /* (RW) USB OTG HS module reset */
#define CHIP_REGMSK_RCC_AHB1RSTR_RESERVED           (0xDD9FEE00UL)

/*********** RCC AHB2 peripheral reset register (RCC_AHB2RSTR) ****************/
#define CHIP_REGFLD_RCC_AHB2RSTR_DVP_RST            (0x00000001UL <<  0U)   /* (RW) Camera interface reset */
#define CHIP_REGFLD_RCC_AHB2RSTR_CRYP_RST           (0x00000001UL <<  4U)   /* (RW) Cryptographic module reset */
#define CHIP_REGFLD_RCC_AHB2RSTR_HASH_RST           (0x00000001UL <<  5U)   /* (RW) Hash module reset */
#define CHIP_REGFLD_RCC_AHB2RSTR_RNG_RST            (0x00000001UL <<  6U)   /* (RW) Random number generator module reset */
#define CHIP_REGFLD_RCC_AHB2RSTR_USBFS_RST          (0x00000001UL <<  7U)   /* (RW) USB OTG FS module reset */
#define CHIP_REGMSK_RCC_AHB2RSTR_RESERVED           (0xFFFFFF0EUL)

/*********** RCC AHB3 peripheral reset register (RCC_AHB3RSTR) ****************/
#define CHIP_REGFLD_RCC_AHB3RSTR_EXMC_RST           (0x00000001UL <<  0U)   /* (RW)  External memory controller module reset */
#define CHIP_REGMSK_RCC_AHB3RSTR_RESERVED           (0xFFFFFFFEUL)

/*********** RCC APB1 peripheral reset register (RCC_APB1RSTR) ****************/
#define CHIP_REGFLD_RCC_APB1RSTR_TIM2_RST           (0x00000001UL <<  0U)   /* (RW) TIM2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM3_RST           (0x00000001UL <<  1U)   /* (RW) TIM3 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM4_RST           (0x00000001UL <<  2U)   /* (RW) TIM4 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM5_RST           (0x00000001UL <<  3U)   /* (RW) TIM5 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM6_RST           (0x00000001UL <<  4U)   /* (RW) TIM6 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM7_RST           (0x00000001UL <<  5U)   /* (RW) TIM7 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM12_RST          (0x00000001UL <<  6U)   /* (RW) TIM12 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM13_RST          (0x00000001UL <<  7U)   /* (RW) TIM13 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM14_RST          (0x00000001UL <<  8U)   /* (RW) TIM14 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_WWDG_RST           (0x00000001UL << 11U)   /* (RW) Window watchdog reset */
#define CHIP_REGFLD_RCC_APB1RSTR_SPI2_RST           (0x00000001UL << 14U)   /* (RW) SPI2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_SPI3_RST           (0x00000001UL << 15U)   /* (RW) SPI3 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART2_RST         (0x00000001UL << 17U)   /* (RW) USART2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART3_RST         (0x00000001UL << 18U)   /* (RW) USART3 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_UART4_RST          (0x00000001UL << 19U)   /* (RW) USART4 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_UART5_RST          (0x00000001UL << 20U)   /* (RW) UART5 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C1_RST           (0x00000001UL << 21U)   /* (RW) I2C1 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C2_RST           (0x00000001UL << 22U)   /* (RW) I2C2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C3_RST           (0x00000001UL << 23U)   /* (RW) I2C3 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_CAN1_RST           (0x00000001UL << 25U)   /* (RW) CAN1 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_CAN2_RST           (0x00000001UL << 26U)   /* (RW) CAN2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_PWR_RST            (0x00000001UL << 28U)   /* (RW) Power interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_DAC_RST            (0x00000001UL << 29U)   /* (RW) DAC reset */
#define CHIP_REGMSK_RCC_APB1RSTR_RESERVED           (0xC9013500UL)

/*********** RCC APB2 peripheral reset register (RCC_APB2RSTR) ****************/
#define CHIP_REGFLD_RCC_APB2RSTR_TIM1_RST           (0x00000001UL <<  0U)   /* (RW) TIM1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM8_RST           (0x00000001UL <<  1U)   /* (RW) TIM8 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART1_RST         (0x00000001UL <<  4U)   /* (RW) USART1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART6_RST         (0x00000001UL <<  5U)   /* (RW) USART6 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_ADC_RST            (0x00000001UL <<  8U)   /* (RW) ADC interface reset (common to all ADCs) */
#define CHIP_REGFLD_RCC_APB2RSTR_SDIO_RST           (0x00000001UL << 11U)   /* (RW) SDIO reset */
#define CHIP_REGFLD_RCC_APB2RSTR_SPI1_RST           (0x00000001UL << 12U)   /* (RW) SPI1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_SYSCFG_RST         (0x00000001UL << 14U)   /* (RW) System configuration controller reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM9_RST           (0x00000001UL << 16U)   /* (RW) TIM9 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM10_RST          (0x00000001UL << 17U)   /* (RW) TIM10 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM11_RST          (0x00000001UL << 18U)   /* (RW) TIM11 reset */
#define CHIP_REGMSK_RCC_APB2RSTR_RESERVED           (0xFFF8A5CCUL)

/*********** RCC AHB1 peripheral clock enable register (RCC_AHB1ENR) ****************/
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOA_EN            (0x00000001UL <<  0U)   /* (RW) IO port A clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOB_EN            (0x00000001UL <<  1U)   /* (RW) IO port B clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOC_EN            (0x00000001UL <<  2U)   /* (RW) IO port C clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOD_EN            (0x00000001UL <<  3U)   /* (RW) IO port D clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOE_EN            (0x00000001UL <<  4U)   /* (RW) IO port E clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOF_EN            (0x00000001UL <<  5U)   /* (RW) IO port F clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOG_EN            (0x00000001UL <<  6U)   /* (RW) IO port G clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOH_EN            (0x00000001UL <<  7U)   /* (RW) IO port H clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_GPIOI_EN            (0x00000001UL <<  8U)   /* (RW) IO port I clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_CRC_EN              (0x00000001UL << 12U)   /* (RW) CRC clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_BKP_SRAM_EN         (0x00000001UL << 18U)   /* (RW) Backup SRAM interface clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_CCM_RAM_EN          (0x00000001UL << 20U)   /* (RW) CCM data RAM clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_DMA1_EN             (0x00000001UL << 21U)   /* (RW) DMA1 clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_DMA2_EN             (0x00000001UL << 22U)   /* (RW) DMA2 clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_ETH_EN              (0x00000001UL << 25U)   /* (RW) Ethernet MAC clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_ETH_TX_EN           (0x00000001UL << 26U)   /* (RW) Ethernet Transmission clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_ETH_RX_EN           (0x00000001UL << 27U)   /* (RW) Ethernet Reception clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_ETH_PTP_EN          (0x00000001UL << 28U)   /* (RW) Ethernet PTP clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_USBHS_EN            (0x00000001UL << 29U)   /* (RW) USB OTG HS clock enable */
#define CHIP_REGFLD_RCC_AHB1ENR_USBHS_ULPI_EN       (0x00000001UL << 30U)   /* (RW) USB OTG HSULPI clock enable */
#define CHIP_REGMSK_RCC_AHB1ENR_RESERVED            (0x818BEE00UL)

/*********** RCC AHB2 peripheral clock enable register (RCC_AHB2ENR) **********/
#define CHIP_REGFLD_RCC_AHB2ENR_DVP_EN              (0x00000001UL <<  0U)   /* (RW) Camera interface  clock enable */
#define CHIP_REGFLD_RCC_AHB2ENR_CRYP_EN             (0x00000001UL <<  4U)   /* (RW) Cryptographic clock enable */
#define CHIP_REGFLD_RCC_AHB2ENR_HASH_EN             (0x00000001UL <<  5U)   /* (RW) Hash clock enable */
#define CHIP_REGFLD_RCC_AHB2ENR_RNG_EN              (0x00000001UL <<  6U)   /* (RW) Random number generator clock enable */
#define CHIP_REGFLD_RCC_AHB2ENR_USBFS_EN            (0x00000001UL <<  7U)   /* (RW) USB OTG FS clock enable */
#define CHIP_REGMSK_RCC_AHB2ENR_RESERVED            (0xFFFFFF0EUL)

/*********** RCC AHB3 peripheral clock enable register (RCC_AHB3ENR) **********/
#define CHIP_REGFLD_RCC_AHB3ENR_EXMC_EN             (0x00000001UL <<  0U)   /* (RW)  External memory controller module clock enable */
#define CHIP_REGMSK_RCC_AHB3ENR_RESERVED            (0xFFFFFFFEUL)

/*********** RCC APB1 peripheral clock enable register (RCC_APB1ENR) **********/
#define CHIP_REGFLD_RCC_APB1ENR_TIM2_EN             (0x00000001UL <<  0U)   /* (RW) TIM2 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM3_EN             (0x00000001UL <<  1U)   /* (RW) TIM3 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM4_EN             (0x00000001UL <<  2U)   /* (RW) TIM4 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM5_EN             (0x00000001UL <<  3U)   /* (RW) TIM5 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM6_EN             (0x00000001UL <<  4U)   /* (RW) TIM6 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM7_EN             (0x00000001UL <<  5U)   /* (RW) TIM7 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM12_EN            (0x00000001UL <<  6U)   /* (RW) TIM12 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM13_EN            (0x00000001UL <<  7U)   /* (RW) TIM13 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM14_EN            (0x00000001UL <<  8U)   /* (RW) TIM14 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_WWDG_EN             (0x00000001UL << 11U)   /* (RW) Window watchdog interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_SPI2_EN             (0x00000001UL << 14U)   /* (RW) SPI2 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_SPI3_EN             (0x00000001UL << 15U)   /* (RW) SPI3 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART2_EN           (0x00000001UL << 17U)   /* (RW) USART2 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART3_EN           (0x00000001UL << 18U)   /* (RW) USART3 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_UART4_EN            (0x00000001UL << 19U)   /* (RW) USART4 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_UART5_EN            (0x00000001UL << 20U)   /* (RW) UART5 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C1_EN             (0x00000001UL << 21U)   /* (RW) I2C1 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C2_EN             (0x00000001UL << 22U)   /* (RW) I2C2 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C3_EN             (0x00000001UL << 23U)   /* (RW) I2C3 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_CAN1_EN             (0x00000001UL << 25U)   /* (RW) CAN1 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_CAN2_EN             (0x00000001UL << 26U)   /* (RW) CAN2 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_PWR_EN              (0x00000001UL << 28U)   /* (RW) Power interface  clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_DAC_EN              (0x00000001UL << 29U)   /* (RW) DAC interface clock enable */
#define CHIP_REGMSK_RCC_APB1ENR_RESERVED            (0xC9013500UL)

/*********** RCC APB2 peripheral clock enable register (RCC_APB2ENR) **********/
#define CHIP_REGFLD_RCC_APB2ENR_TIM1_EN             (0x00000001UL <<  0U)   /* (RW) TIM1 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM8_EN             (0x00000001UL <<  1U)   /* (RW) TIM8 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART1_EN           (0x00000001UL <<  4U)   /* (RW) USART1 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART6_EN           (0x00000001UL <<  5U)   /* (RW) USART6 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_ADC1_EN             (0x00000001UL <<  8U)   /* (RW) ADC1 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_ADC2_EN             (0x00000001UL <<  9U)   /* (RW) ADC2 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_ADC3_EN             (0x00000001UL << 10U)   /* (RW) ADC3 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_SDIO_EN             (0x00000001UL << 11U)   /* (RW) SDIO interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_SPI1_EN             (0x00000001UL << 12U)   /* (RW) SPI1 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_SYSCFG_EN           (0x00000001UL << 14U)   /* (RW) System configuration controller interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM9_EN             (0x00000001UL << 16U)   /* (RW) TIM9 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM10_EN            (0x00000001UL << 17U)   /* (RW) TIM10 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM11_EN            (0x00000001UL << 18U)   /* (RW) TIM11 interface clock enable */
#define CHIP_REGMSK_RCC_APB2ENR_RESERVED            (0xFFF8A0CCUL)

/*********** RCC AHB1 peripheral clock enable in low power mode register (RCC_AHB1LPENR) */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOA_LPEN        (0x00000001UL <<  0U)   /* (RW) IO port A clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOB_LPEN        (0x00000001UL <<  1U)   /* (RW) IO port B clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOC_LPEN        (0x00000001UL <<  2U)   /* (RW) IO port C clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOD_LPEN        (0x00000001UL <<  3U)   /* (RW) IO port D clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOE_LPEN        (0x00000001UL <<  4U)   /* (RW) IO port E clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOF_LPEN        (0x00000001UL <<  5U)   /* (RW) IO port F clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOG_LPEN        (0x00000001UL <<  6U)   /* (RW) IO port G clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOH_LPEN        (0x00000001UL <<  7U)   /* (RW) IO port H clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_GPIOI_LPEN        (0x00000001UL <<  8U)   /* (RW) IO port I clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_CRC_LPEN          (0x00000001UL << 12U)   /* (RW) CRC clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_FLITF_LPEN        (0x00000001UL << 15U)   /* (RW) Flash interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_SRAM1_LPEN        (0x00000001UL << 16U)   /* (RW) SRAM 1interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_SRAM2_LPEN        (0x00000001UL << 17U)   /* (RW) SRAM 2 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_BKP_SRAM_LPEN     (0x00000001UL << 18U)   /* (RW) Backup SRAM interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_DMA1_LPEN         (0x00000001UL << 21U)   /* (RW) DMA1 clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_DMA2_LPEN         (0x00000001UL << 22U)   /* (RW) DMA2 clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_ETH_LPEN          (0x00000001UL << 25U)   /* (RW) Ethernet MAC clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_ETH_TX_LPEN       (0x00000001UL << 26U)   /* (RW) Ethernet transmission clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_ETH_RX_LPEN       (0x00000001UL << 27U)   /* (RW) Ethernet reception clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_ETH_PTP_LPEN      (0x00000001UL << 28U)   /* (RW) Ethernet PTP clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_USBHS_LPEN        (0x00000001UL << 29U)   /* (RW) USB OTG HS clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB1LPENR_USBHS_ULPI_LPEN   (0x00000001UL << 30U)   /* (RW) USB OTG HS ULPI clock enable during Sleep mode */
#define CHIP_REGMSK_RCC_AHB1LPENR_RESERVED          (0x7E985E00UL)

/*********** RCC AHB2 peripheral clock enable in low power mode register (RCC_AHB2LPENR) */
#define CHIP_REGFLD_RCC_AHB2LPENR_DVP_LPEN          (0x00000001UL <<  0U)   /* (RW) Camera interface  clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB2LPENR_CRYP_LPEN         (0x00000001UL <<  4U)   /* (RW) Cryptographic clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB2LPENR_HASH_LPEN         (0x00000001UL <<  5U)   /* (RW) Hash clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB2LPENR_RNG_LPEN          (0x00000001UL <<  6U)   /* (RW) Random number generator clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_AHB2LPENR_USBFS_LPEN        (0x00000001UL <<  7U)   /* (RW) USB OTG FS clock enable */
#define CHIP_REGMSK_RCC_AHB2LPENR_RESERVED          (0xFFFFFF0EUL)

/*********** RCC AHB3 peripheral clock enable in low power mode register (RCC_AHB3LPENR) */
#define CHIP_REGFLD_RCC_AHB3LPENR_EXMC_LPEN         (0x00000001UL <<  0U)   /* (RW)  External memory controller module clock enable */
#define CHIP_REGMSK_RCC_AHB3LPENR_RESERVED          (0xFFFFFFFEUL)

/*********** RCC APB1 peripheral clock enable in low power mode register (RCC_APB1LPENR) */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM2_LPEN         (0x00000001UL <<  0U)   /* (RW) TIM2 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM3_LPEN         (0x00000001UL <<  1U)   /* (RW) TIM3 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM4_LPEN         (0x00000001UL <<  2U)   /* (RW) TIM4 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM5_LPEN         (0x00000001UL <<  3U)   /* (RW) TIM5 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM6_LPEN         (0x00000001UL <<  4U)   /* (RW) TIM6 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM7_LPEN         (0x00000001UL <<  5U)   /* (RW) TIM7 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM12_LPEN        (0x00000001UL <<  6U)   /* (RW) TIM12 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM13_LPEN        (0x00000001UL <<  7U)   /* (RW) TIM13 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_TIM14_LPEN        (0x00000001UL <<  8U)   /* (RW) TIM14 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_WWDG_LPEN         (0x00000001UL << 11U)   /* (RW) Window watchdog interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_SPI2_LPEN         (0x00000001UL << 14U)   /* (RW) SPI2 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_SPI3_LPEN         (0x00000001UL << 15U)   /* (RW) SPI3 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_USART2_LPEN       (0x00000001UL << 17U)   /* (RW) USART2 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_USART3_LPEN       (0x00000001UL << 18U)   /* (RW) USART3 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_UART4_LPEN        (0x00000001UL << 19U)   /* (RW) USART4 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_UART5_LPEN        (0x00000001UL << 20U)   /* (RW) UART5 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_I2C1_LPEN         (0x00000001UL << 21U)   /* (RW) I2C1 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_I2C2_LPEN         (0x00000001UL << 22U)   /* (RW) I2C2 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_I2C3_LPEN         (0x00000001UL << 23U)   /* (RW) I2C3 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_CAN1_LPEN         (0x00000001UL << 25U)   /* (RW) CAN1 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_CAN2_LPEN         (0x00000001UL << 26U)   /* (RW) CAN2 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_PWR_LPEN          (0x00000001UL << 28U)   /* (RW) Power interface  clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB1LPENR_DAC_LPEN          (0x00000001UL << 29U)   /* (RW) DAC interface clock enable during Sleep mode */
#define CHIP_REGMSK_RCC_APB1LPENR_RESERVED          (0xC9013500UL)

/*********** RCC APB2 peripheral clock enabled in low power mode register (RCC_APB2LPENR) */
#define CHIP_REGFLD_RCC_APB2LPENR_TIM1_LPEN         (0x00000001UL <<  0U)   /* (RW) TIM1 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB2LPENR_TIM8_LPEN         (0x00000001UL <<  1U)   /* (RW) TIM8 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB2LPENR_USART1_LPEN       (0x00000001UL <<  4U)   /* (RW) USART1 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB2LPENR_USART6_LPEN       (0x00000001UL <<  5U)   /* (RW) USART6 interface clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB2LPENR_ADC1_LPEN         (0x00000001UL <<  8U)   /* (RW) ADC1 clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB2LPENR_ADC2_LPEN         (0x00000001UL <<  9U)   /* (RW) ADC2 clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB2LPENR_ADC3_LPEN         (0x00000001UL << 10U)   /* (RW) ADC3 clock enable during Sleep mode */
#define CHIP_REGFLD_RCC_APB2LPENR_SDIO_LPEN         (0x00000001UL << 11U)   /* (RW) SDIO reset */
#define CHIP_REGFLD_RCC_APB2LPENR_SPI1_LPEN         (0x00000001UL << 12U)   /* (RW) SPI1 reset */
#define CHIP_REGFLD_RCC_APB2LPENR_SYSCFG_LPEN       (0x00000001UL << 14U)   /* (RW) System configuration controller reset */
#define CHIP_REGFLD_RCC_APB2LPENR_TIM9_LPEN         (0x00000001UL << 16U)   /* (RW) TIM9 reset */
#define CHIP_REGFLD_RCC_APB2LPENR_TIM10_LPEN        (0x00000001UL << 17U)   /* (RW) TIM10 reset */
#define CHIP_REGFLD_RCC_APB2LPENR_TIM11_LPEN        (0x00000001UL << 18U)   /* (RW) TIM11 reset */
#define CHIP_REGMSK_RCC_APB2LPENR_RESERVED          (0xFFF8A0CCUL)

/*********** RCC Backup domain control register (RCC_BDCR) ********************/
#define CHIP_REGFLD_RCC_BDCR_LSE_ON                 (0x00000001UL <<  0U)   /* (RW) External low-speed oscillator enable */
#define CHIP_REGFLD_RCC_BDCR_LSE_RDY                (0x00000001UL <<  1U)   /* (RO) External low-speed oscillator ready */
#define CHIP_REGFLD_RCC_BDCR_LSE_BYP                (0x00000001UL <<  2U)   /* (RW) External low-speed oscillator bypass */
#define CHIP_REGFLD_RCC_BDCR_RTC_SEL                (0x00000003UL <<  8U)   /* (RW) RTC clock source selection */
#define CHIP_REGFLD_RCC_BDCR_RTC_EN                 (0x00000001UL << 15U)   /* (RW) RTC clock enable */
#define CHIP_REGFLD_RCC_BDCR_BD_RST                 (0x00000001UL << 16U)   /* (RW) Backup domain software reset */
#define CHIP_REGMSK_RCC_BDCR_RESERVED               (0xFFFE7CF8UL)

#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_OFF         0
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSE         1
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSI         2
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_HSE         3

/*********** RCC clock control & status register (RCC_CSR) ********************/
#define CHIP_REGFLD_RCC_CSR_LSI_ON                  (0x00000001UL <<  0U)   /* (RW) Internal low-speed oscillator enable */
#define CHIP_REGFLD_RCC_CSR_LSI_RDY                 (0x00000001UL <<  1U)   /* (RO) Internal low-speed oscillator ready */
#define CHIP_REGFLD_RCC_CSR_RMV_F                   (0x00000001UL << 24U)   /* (W1) Remove reset flag */
#define CHIP_REGFLD_RCC_CSR_BOR_RST_F               (0x00000001UL << 25U)   /* (RO) BOR reset flag */
#define CHIP_REGFLD_RCC_CSR_PIN_RST_F               (0x00000001UL << 26U)   /* (RO) PIN reset flag */
#define CHIP_REGFLD_RCC_CSR_POR_RST_F               (0x00000001UL << 27U)   /* (RO) POR/PDR reset flag */
#define CHIP_REGFLD_RCC_CSR_SFT_RST_F               (0x00000001UL << 27U)   /* (RO) Software reset flag */
#define CHIP_REGFLD_RCC_CSR_IWDG_RST_F              (0x00000001UL << 27U)   /* (RO) Independent watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_WWDG_RST_F              (0x00000001UL << 27U)   /* (RO) Window watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_LPW_RST_F               (0x00000001UL << 27U)   /* (RO) Low-power reset flag */

/*********** RCC spread spectrum clock generation register (RCC_SSCGR) ********/
#define CHIP_REGFLD_RCC_SSCGR_MOD_PER               (0x00001FFFUL <<  0U)   /* (RW) Modulation period */
#define CHIP_REGFLD_RCC_SSCGR_INC_STEP              (0x00007FFFUL << 13U)   /* (RW) Incrementation step */
#define CHIP_REGFLD_RCC_SSCGR_SPREAD_SEL            (0x00000001UL << 30U)   /* (RW) Spread Select */
#define CHIP_REGFLD_RCC_SSCGR_SSCG_EN               (0x00000001UL << 31U)   /* (RW) Spread spectrum modulation enable */

#define CHIP_REGFLDVAL_RCC_SSCGR_SPREAD_SEL_CENTER  0
#define CHIP_REGFLDVAL_RCC_SSCGR_SPREAD_SEL_DOWN    1

/*********** RCC PLLI2S configuration register (RCC_PLLI2SCFGR) ***************/
#define CHIP_REGFLD_RCC_PLLI2SCFGR_N                (0x000001FFUL <<  6U)   /* (RW) PLLI2S multiplication factor for VCO */
#define CHIP_REGFLD_RCC_PLLI2SCFGR_R                (0x00000007UL << 28U)   /* (RW) PLLI2S division factor for I2S clocks */
#define CHIP_REGMSK_RCC_PLLI2SCFGR_RESERVED         (0x8FFF8030UL)

#endif // CHIP_STM32F4XX_REGS_RCC_H

