#ifndef CHIP_REGS_DBGMCU_STM32H7XX_H_
#define CHIP_REGS_DBGMCU_STM32H7XX_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

/** Debug MCU */
typedef struct {
    __IO uint32_t IDCODE;         /* MCU device ID code,                   Address offset: 0x00 */
    __IO uint32_t CR;          /* Debug MCU configuration register,     Address offset: 0x04 */
    uint32_t RESERVED4[11];    /* Reserved,                             Address offset: 0x08 */
    __IO uint32_t APB3FZ1;     /* Debug MCU APB3FZ1 freeze register,    Address offset: 0x34 */
    uint32_t RESERVED5;        /* Reserved,                             Address offset: 0x38 */
    __IO uint32_t APB1LFZ1;    /* Debug MCU APB1LFZ1 freeze register,   Address offset: 0x3C */
    uint32_t RESERVED6;        /* Reserved,                             Address offset: 0x40 */
    __IO uint32_t APB1HFZ1;    /* Debug MCU APB1LFZ1 freeze register,   Address offset: 0x44 */
    uint32_t RESERVED7;        /* Reserved,                             Address offset: 0x48 */
    __IO uint32_t APB2FZ1;     /* Debug MCU APB2FZ1 freeze register,    Address offset: 0x4C */
    uint32_t RESERVED8;        /* Reserved,                             Address offset: 0x50 */
    __IO uint32_t APB4FZ1;     /* Debug MCU APB4FZ1 freeze register,    Address offset: 0x54 */
} CHIP_REGS_DBGMCU_T;

#define CHIP_REGS_DBGMCU         ((CHIP_REGS_DBGMCU_T *) CHIP_MEMRGN_ADDR_DBGMCU)

/****** DBGMCU identity code register (DBGMCU_IDC) ****************************/
#define CHIP_REGFLD_DBGMCU_IDCODE_DEV_ID        (0x00000FFFUL <<  0U)
#define CHIP_REGFLD_DBGMCU_IDCODE_REV_ID        (0x0000FFFFUL << 16U)

/* revision codes */
#define CHIP_REV_ID_A      0x1000
#define CHIP_REV_ID_Z      0x1001
#define CHIP_REV_ID_Y      0x1003
#define CHIP_REV_ID_X      0x2001
#define CHIP_REV_ID_V      0x2003

#define CHIP_DEV_ID_H74X   0x450
#define CHIP_DEV_ID_H72X   0x483


/****** DBGMCU configuration register (DBGMCU_CR) *****************************/
#define CHIP_REGFLD_DBGMCU_CR_DBG_SLEEP_D1      (0x00000001UL <<  0U) /* D1 domain debug in Sleep mode enable */
#define CHIP_REGFLD_DBGMCU_CR_DBG_STOP_D1       (0x00000001UL <<  1U) /* D1 domain debug in Stop mode enable */
#define CHIP_REGFLD_DBGMCU_CR_DBG_STANDBY_D1    (0x00000001UL <<  2U) /* D1 domain debug in Standby mode enable */
#if CHIP_DEV_SUPPORT_D2_RSTCTL
#define CHIP_REGFLD_DBGMCU_CR_DBG_SLEEP_D2      (0x00000001UL <<  3U) /* D2 domain debug in Sleep mode enable */
#define CHIP_REGFLD_DBGMCU_CR_DBG_STOP_D2       (0x00000001UL <<  4U) /* D2 domain debug in Stop mode enable */
#define CHIP_REGFLD_DBGMCU_CR_DBG_STANDBY_D2    (0x00000001UL <<  5U) /* D2 domain debug in Standby mode enable */
#endif
#define CHIP_REGFLD_DBGMCU_CR_DBG_TRACECKEN     (0x00000001UL << 20U) /* Trace port clock enable */
#define CHIP_REGFLD_DBGMCU_CR_DBG_CKD1EN        (0x00000001UL << 21U) /* D1 debug clock enable */
#define CHIP_REGFLD_DBGMCU_CR_DBG_CKD3EN        (0x00000001UL << 22U) /* D3 debug clock enable */
#define CHIP_REGFLD_DBGMCU_CR_DBG_TRGOEN        (0x00000001UL << 28U) /* External trigger output enable */

/****** DBGMCU APB3 peripheral freeze register CPU1/2 (DBGMCU_APB3FZ1/2) ******/
#define CHIP_REGFLD_DBGMCU_APB3FZ_DBG_WWDG1     (0x00000001UL <<  6U) /* WWDG1 stop in debug */

/****** DBGMCU APB1L peripheral freeze register CPU1/2 (DBGMCU_APB1LFZ1/2) ****/
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM2     (0x00000001UL <<  0U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM3     (0x00000001UL <<  1U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM4     (0x00000001UL <<  2U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM5     (0x00000001UL <<  3U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM6     (0x00000001UL <<  4U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM7     (0x00000001UL <<  5U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM12    (0x00000001UL <<  6U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM13    (0x00000001UL <<  7U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_TIM14    (0x00000001UL <<  8U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_LPTIM1   (0x00000001UL <<  9U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_WWDG2    (0x00000001UL << 11U)
#endif
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_I2C1     (0x00000001UL << 21U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_I2C2     (0x00000001UL << 22U)
#define CHIP_REGFLD_DBGMCU_APB1LFZ_DBG_I2C3     (0x00000001UL << 23U)

/********************  Bit definition for APB1HFZ1 register  ************/
#define CHIP_REGFLD_DBGMCU_APB1HFZ_DBG_FDCAN    (0x00000001UL <<  8U)

/****** DBGMCU APB2 peripheral freeze register CPU1/2 (DBGMCU_APB2FZ1/2) ******/
#define CHIP_REGFLD_DBGMCU_APB2FZ_DBG_TIM1      (0x00000001UL <<  0U)
#define CHIP_REGFLD_DBGMCU_APB2FZ_DBG_TIM8      (0x00000001UL <<  1U)
#define CHIP_REGFLD_DBGMCU_APB2FZ_DBG_TIM15     (0x00000001UL << 16U)
#define CHIP_REGFLD_DBGMCU_APB2FZ_DBG_TIM16     (0x00000001UL << 17U)
#define CHIP_REGFLD_DBGMCU_APB2FZ_DBG_TIM17     (0x00000001UL << 18U)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_REGFLD_DBGMCU_APB2FZ_DBG_HRTIM     (0x00000001UL << 29U)
#endif

/****** DBGMCU APB4 peripheral freeze register CPU1 (DBGMCU_APB4FZ1) **********/
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_I2C4     (0x00000001UL <<  7U)
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_LPTIM2   (0x00000001UL <<  9U)
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_LPTIM3   (0x00000001UL << 10U)
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_LPTIM4   (0x00000001UL << 11U)
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_LPTIM5   (0x00000001UL << 12U)
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_RTC      (0x00000001UL << 16U)
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_IWDG1    (0x00000001UL << 18U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_DBGMCU_APB4FZ1_DBG_IWDG2    (0x00000001UL << 19U)
#endif

#endif // CHIP_REGS_DBGMCU_STM32H7XX_H_
