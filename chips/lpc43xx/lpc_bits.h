/*================================================================================================*
 * Дополнительные битовые маски LPC43xx, не описанные в стандартных заголовках
 *================================================================================================*/

#ifndef LPC_BITS_H_
#define LPC_BITS_H_

#include "bitfields.h"

/*================================================================================================*/
/* CGU: XTAL_OSC_CTRL */
#define LPC_CGU_XTAL_OSC_CTRL_PD        (1UL << 0)
#define LPC_CGU_XTAL_OSC_CTRL_BYPASS    (1UL << 1)
#define LPC_CGU_XTAL_OSC_CTRL_HF        (1UL << 2)

/* CGU: PLL1_STAT */
#define LPC_CGU_PLL1_STAT_LOCK          (1UL << 0)

/* CGU: PLL1_CTRL */
#define LPC_CGU_PLL1_CTRL_PD            (1UL << 0)
#define LPC_CGU_PLL1_CTRL_BYPASS        (1UL << 1)
#define LPC_CGU_PLL1_CTRL_FBSEL         (1UL << 6)
#define LPC_CGU_PLL1_CTRL_DIRECT        (1UL << 7)
#define LPC_CGU_PLL1_CTRL_PSEL          (3UL << 8)
#define LPC_CGU_PLL1_CTRL_AUTOBLOCK     (1UL << 11)
#define LPC_CGU_PLL1_CTRL_NSEL          (3UL << 12)
#define LPC_CGU_PLL1_CTRL_MSEL          (0xFFUL << 16)
#define LPC_CGU_PLL1_CTRL_CLK_SEL       (0x1FUL << 24)

/* CGU: PLL0_STAT */
#define LPC_CGU_PLL0_STAT_LOCK          (1UL << 0)
#define LPC_CGU_PLL0_STAT_FR            (1UL << 1)

/* CGU: PLL0_CTRL */
#define LPC_CGU_PLL0_CTRL_PD            (1UL << 0)
#define LPC_CGU_PLL0_CTRL_BYPASS        (1UL << 1)
#define LPC_CGU_PLL0_CTRL_DIRECTI       (1UL << 2)
#define LPC_CGU_PLL0_CTRL_DIRECTO       (1UL << 3)
#define LPC_CGU_PLL0_CTRL_CLKEN         (1UL << 4)
#define LPC_CGU_PLL0_CTRL_FRM           (1UL << 6)
#define LPC_CGU_PLL0_CTRL_AUTOBLOCK     (1UL << 11)
#define LPC_CGU_PLL0_CTRL_CLK_SEL       (0x1FUL << 24)

/* CCU: CLK_x_CFG */
#define LPC_CCU_CFG_RUN                 (1UL << 0)
#define LPC_CCU_CFG_AUTO                (1UL << 1)
#define LPC_CCU_CFG_WAKEUP              (1UL << 2)

#define LPC_CCU_CFG_DIV                 (7UL << 5)

/* SCU: SFSP[port][pin] */
/* Notes:
 * EHS = 0: normal pins <= 50 MHz, high-speed (CLKn, P3_3) <= 75 MHz
 * EHS = 1: normal pins <= 80 MHz, high-speed (CLKn, P3_3) up to sysclk
 * ZIF should be 1 for inputs > 30 MHz
 * High-drive pins (P1_17, P2_3..5, P8_0..2, PA_1..3) do not have EHS.
 * EZI must be 1 to have any digital input from the pin.
 */
#define LPC_SCU_SFSP_MODE               (7UL << 0)  /* function, 0..7 */
#define LPC_SCU_SFSP_EPD                (1UL << 3)  /* enable pull-down */
#define LPC_SCU_SFSP_EPUN               (1UL << 4)  /* disable pull-up */
#define LPC_SCU_SFSP_EHS                (1UL << 5)  /* (non-HD) enable high-speed */
#define LPC_SCU_SFSP_EZI                (1UL << 6)  /* enable input buffer */
#define LPC_SCU_SFSP_ZIF                (1UL << 7)  /* disable input glitch filter */
#define LPC_SCU_SFSP_EHD                (3UL << 8)  /* (HD) 0..3 = 4, 8, 12, 20 mA drive */
/*================================================================================================*/

#endif /* LPC_BITS_H_ */
