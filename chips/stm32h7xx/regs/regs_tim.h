#ifndef CHIP_REGS_TIM_STM32H7XX_H_
#define CHIP_REGS_TIM_STM32H7XX_H_


#define CHIP_SUPPORT_TIM_UIF_REMAP              1
#define CHIP_SUPPORT_TIM_EXT_SYNC_MODE          1
#define CHIP_SUPPORT_TIM_OUT_5_6                1
#define CHIP_SUPPORT_TIM_EXT_BREAK              1
#define CHIP_SUPPORT_TIM_EXT_OUT_MODE           1
#define CHIP_SUPPORT_TIM_CH_GROUP               1
#define CHIP_SUPPORT_TIM_ALTFUNC                1


#include "chip_devtype_spec_features.h"
#include "stm32_tim_gen_regs.h"


#define CHIP_REGS_TIM1          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM1)
#define CHIP_REGS_TIM2          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM2)
#define CHIP_REGS_TIM3          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM3)
#define CHIP_REGS_TIM4          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM4)
#define CHIP_REGS_TIM5          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM5)
#define CHIP_REGS_TIM6          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM6)
#define CHIP_REGS_TIM7          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM7)
#define CHIP_REGS_TIM8          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM8)
#define CHIP_REGS_TIM12         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM12)
#define CHIP_REGS_TIM13         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM13)
#define CHIP_REGS_TIM14         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM14)
#define CHIP_REGS_TIM15         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM15)
#define CHIP_REGS_TIM16         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM16)
#define CHIP_REGS_TIM17         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM17)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_REGS_TIM23         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM23)
#define CHIP_REGS_TIM24         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM24)
#endif

/* Internal triggers mapping */
#define CHIP_TIM1_ITR_TIM15             CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM15 -> TIM1 */
#define CHIP_TIM1_ITR_TIM2              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1  /* TIM2  -> TIM1 */
#define CHIP_TIM1_ITR_TIM3              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM3  -> TIM1 */
#define CHIP_TIM1_ITR_TIM4              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3  /* TIM4  -> TIM1 */

#define CHIP_TIM1_ETR_COMP1_OUT         1
#define CHIP_TIM1_ETR_COMP2_OUT         2
#define CHIP_TIM1_ETR_ADC1_AWD1         3
#define CHIP_TIM1_ETR_ADC1_AWD2         4
#define CHIP_TIM1_ETR_ADC1_AWD3         5
#define CHIP_TIM1_ETR_ADC3_AWD1         6
#define CHIP_TIM1_ETR_ADC3_AWD2         7
#define CHIP_TIM1_ETR_ADC3_AWD3         8

#define CHIP_TIM8_ITR_TIM1              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1  -> TIM8 */
#define CHIP_TIM8_ITR_TIM2              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1  /* TIM2  -> TIM8 */
#define CHIP_TIM8_ITR_TIM4              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM4  -> TIM8 */
#define CHIP_TIM8_ITR_TIM5              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3  /* TIM5  -> TIM8 */

#define CHIP_TIM8_ETR_COMP1_OUT         1
#define CHIP_TIM8_ETR_COMP2_OUT         2
#define CHIP_TIM8_ETR_ADC2_AWD1         3
#define CHIP_TIM8_ETR_ADC2_AWD2         4
#define CHIP_TIM8_ETR_ADC2_AWD3         5
#define CHIP_TIM8_ETR_ADC3_AWD1         6
#define CHIP_TIM8_ETR_ADC3_AWD2         7
#define CHIP_TIM8_ETR_ADC3_AWD3         8

#define CHIP_TIM2_ITR_TIM1              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1  -> TIM2 */
#define CHIP_TIM2_ITR_TIM8              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1  /* TIM8  -> TIM2 */
#define CHIP_TIM2_ITR_TIM3              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM3  -> TIM2 */
#define CHIP_TIM2_ITR_TIM4              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3  /* TIM4  -> TIM2 */
#define CHIP_TIM2_ITR_ETH_PPS           CHIP_REGFLDVAL_TIM_SMCR_TS_ITR4  /* ETH_PPS          -> TIM2 */
#define CHIP_TIM2_ITR_USB1_OTG_HS_SOF   CHIP_REGFLDVAL_TIM_SMCR_TS_ITR5  /* USB1_OTG_HS_SOF  -> TIM2 */
#define CHIP_TIM2_ITR_USB2_OTG_HS_SOF   CHIP_REGFLDVAL_TIM_SMCR_TS_ITR6  /* USB2_OTG_HS_SOF  -> TIM2 */

#define CHIP_TIM2_ETR_COMP1_OUT         1
#define CHIP_TIM2_ETR_COMP2_OUT         2
#define CHIP_TIM2_ETR_LSE_CK            3
#define CHIP_TIM2_ETR_SAI1_FS_A         4
#define CHIP_TIM2_ETR_SAI1_FS_B         5

#define CHIP_TIM3_ITR_TIM1              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1  -> TIM3 */
#define CHIP_TIM3_ITR_TIM2              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1  /* TIM2  -> TIM3 */
#define CHIP_TIM3_ITR_TIM15             CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM15 -> TIM3 */
#define CHIP_TIM3_ITR_TIM4              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3  /* TIM4  -> TIM3 */
#define CHIP_TIM3_ITR_ETH_PPS           CHIP_REGFLDVAL_TIM_SMCR_TS_ITR4  /* ETH_PPS -> TIM3 */

#define CHIP_TIM3_ETR_COMP1_OUT         1

#define CHIP_TIM4_ITR_TIM1              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1  -> TIM4 */
#define CHIP_TIM4_ITR_TIM2              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1  /* TIM2  -> TIM4 */
#define CHIP_TIM4_ITR_TIM3              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM3  -> TIM4 */
#define CHIP_TIM4_ITR_TIM8              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3  /* TIM8  -> TIM4 */

#define CHIP_TIM5_ITR_TIM1              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1  -> TIM5 */
#define CHIP_TIM5_ITR_TIM8              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1  /* TIM8  -> TIM5 */
#define CHIP_TIM5_ITR_TIM3              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM3  -> TIM5 */
#define CHIP_TIM5_ITR_TIM4              CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3  /* TIM4  -> TIM5 */
#define CHIP_TIM5_ITR_FDCAN1_SOC        CHIP_REGFLDVAL_TIM_SMCR_TS_ITR6  /* ETH_PPS          -> TIM5 */
#define CHIP_TIM5_ITR_USB1_OTG_HS_SOF   CHIP_REGFLDVAL_TIM_SMCR_TS_ITR7  /* USB1_OTG_HS_SOF  -> TIM5 */
#define CHIP_TIM5_ITR_USB2_OTG_HS_SOF   CHIP_REGFLDVAL_TIM_SMCR_TS_ITR8  /* USB2_OTG_HS_SOF  -> TIM5 */

#define CHIP_TIM5_ETR_SAI2_FS_A         1
#define CHIP_TIM5_ETR_SAI2_FS_B         2

#define CHIP_TIM15_ITR_TIM1             CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1  -> TIM15 */
#define CHIP_TIM15_ITR_TIM3             CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1  /* TIM3  -> TIM15 */
#define CHIP_TIM15_ITR_TIM16_OC1        CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM16_OC1  -> TIM15 */
#define CHIP_TIM15_ITR_TIM17_OC1        CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3  /* TIM17_OC1  -> TIM15 */


/*********** TIM1 alternate function option register 1 (TIM1_AF1) *************/
#define CHIP_REGFLD_TIM1_AF1_BKINE                  (0x00000001UL <<  0U) /* BKINE Break input enable bit */
#define CHIP_REGFLD_TIM1_AF1_BKCMP1E                (0x00000001UL <<  1U) /* BKCMP1E Break Compare1 Enable bit */
#define CHIP_REGFLD_TIM1_AF1_BKCMP2E                (0x00000001UL <<  2U) /* BKCMP1E Break Compare2 Enable bit */
#define CHIP_REGFLD_TIM1_AF1_BKDF1BK0E              (0x00000001UL <<  8U) /* BKDF1BK0E Break input DFSDM Break 0 */
#define CHIP_REGFLD_TIM1_AF1_BKINP                  (0x00000001UL <<  9U) /* BRKINP Break input polarity */
#define CHIP_REGFLD_TIM1_AF1_BKCMP1P                (0x00000001UL << 10U) /* BKCMP1P Break COMP1 input polarity */
#define CHIP_REGFLD_TIM1_AF1_BKCMP2P                (0x00000001UL << 11U) /* BKCMP2P Break COMP2 input polarity */
#define CHIP_REGFLD_TIM1_AF1_ETRSEL                 (0x0000000FUL << 14U) /* ETR source selection */

#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_IO           0 /* ETR input is connected to I/O */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_COMP1        1 /* COMP1 output */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_COMP2        2 /* COMP2 output */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC1_AWD1    3 /* ADC1 AWD1 */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC1_AWD2    4 /* ADC1 AWD2 */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC1_AWD3    5 /* ADC1 AWD3 */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC3_AWD1    6 /* ADC1 AWD1 */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC3_AWD2    7 /* ADC1 AWD2 */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC3_AWD3    8 /* ADC1 AWD3 */

/*********** TIM1 Alternate function register 2 (TIM1_AF2) ********************/
#define CHIP_REGFLD_TIM1_AF2_BK2INE                 (0x00000001UL <<  0U) /* BK2INE Break input 2 enable bit */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP1E               (0x00000001UL <<  1U) /* BK2CMP1E Break2 Compare1 Enable bit */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP2E               (0x00000001UL <<  2U) /* BK2CMP1E Break2 Compare2 Enable bit  */
#define CHIP_REGFLD_TIM1_AF2_BK2DFBK1E              (0x00000001UL <<  8U) /* BK2DFBK1E Break input2 DFSDM Break 1 */
#define CHIP_REGFLD_TIM1_AF2_BK2INP                 (0x00000001UL <<  9U) /* BRKINP Break2 input polarity */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP1P               (0x00000001UL << 10U) /* BKCMP1P Break2 COMP1 input polarity */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP2P               (0x00000001UL << 11U) /* BKCMP2P Break2 COMP2 input polarity */


/*********** TIM8 Alternate function option register 1 ************************/
#define CHIP_REGFLD_TIM8_AF1_BKINE                  (0x00000001UL <<  0U) /* BKINE Break input enable bit */
#define CHIP_REGFLD_TIM8_AF1_BKCMP1E                (0x00000001UL <<  1U) /* BKCMP1E Break Compare1 Enable bit */
#define CHIP_REGFLD_TIM8_AF1_BKCMP2E                (0x00000001UL <<  2U) /* BKCMP1E Break Compare2 Enable bit  */
#define CHIP_REGFLD_TIM8_AF1_BKDFBK2E               (0x00000001UL <<  8U) /* BKDFBK2E Break input DFSDM Break 2 */
#define CHIP_REGFLD_TIM8_AF1_BKINP                  (0x00000001UL <<  9U) /* BRKINP Break input polarity */
#define CHIP_REGFLD_TIM8_AF1_BKCMP1P                (0x00000001UL << 10U) /* BKCMP1P Break COMP1 input polarity */
#define CHIP_REGFLD_TIM8_AF1_BKCMP2P                (0x00000001UL << 11U) /* BKCMP2P Break COMP2 input polarity */
#define CHIP_REGFLD_TIM8_AF1_ETRSEL                 (0x0000000FUL << 14U) /* ETR source selection */

#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_IO           0 /* ETR input is connected to I/O */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_COMP1        1 /* COMP1 output */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_COMP2        2 /* COMP2 output */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_ADC1_AWD1    3 /* ADC1 AWD1 */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_ADC1_AWD2    4 /* ADC1 AWD2 */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_ADC1_AWD3    5 /* ADC1 AWD3 */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_ADC3_AWD1    6 /* ADC1 AWD1 */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_ADC3_AWD2    7 /* ADC1 AWD2 */
#define CHIP_REGFLDVAL_TIM8_AF1_ETRSEL_ADC3_AWD3    8 /* ADC1 AWD3 */

/*********** TIM8 Alternate function option register 2 (TIM8_AF2) *************/
#define CHIP_REGFLD_TIM8_AF2_BK2INE                 (0x00000001UL <<  0U) /* BK2INE Break input 2 enable bit */
#define CHIP_REGFLD_TIM8_AF2_BK2CMP1E               (0x00000001UL <<  1U) /* BK2CMP1E Break2 Compare1 Enable bit */
#define CHIP_REGFLD_TIM8_AF2_BK2CMP2E               (0x00000001UL <<  2U) /* BK2CMP1E Break2 Compare2 Enable bit  */
#define CHIP_REGFLD_TIM8_AF2_BK2DFBK3E              (0x00000001UL <<  8U) /* BK2DFBK1E Break input2 DFSDM Break 3 */
#define CHIP_REGFLD_TIM8_AF2_BK2INP                 (0x00000001UL <<  9U) /* BRKINP Break2 input polarity */
#define CHIP_REGFLD_TIM8_AF2_BK2CMP1P               (0x00000001UL << 10U) /* BKCMP1P Break2 COMP1 input polarity */
#define CHIP_REGFLD_TIM8_AF2_BK2CMP2P               (0x00000001UL << 11U) /* BKCMP2P Break2 COMP2 input polarity */

/*********** TIM2 alternate function option register 1 ************************/
#define CHIP_REGFLD_TIM2_AF1_ETRSEL                 (0x0000000FUL << 14U) /* ETR source selection */

#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_IO           0 /* ETR input is connected to I/O */
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_COMP1        1 /* COMP1 output */
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_COMP2        2 /* COMP2 output */
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_LSE          3
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_SAI1_FS_A    4
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_SAI1_FS_B    5

/*********** TIM3 alternate function option register 1 (TIM3_AF1) *************/
#define CHIP_REGFLD_TIM3_AF1_ETRSEL                 (0x0000000FUL << 14U) /* ETR source selection */

#define CHIP_REGFLDVAL_TIM3_AF1_ETRSEL_IO           0 /* ETR input is connected to I/O */
#define CHIP_REGFLDVAL_TIM3_AF1_ETRSEL_COMP1        1 /* COMP1 output */

/*********** TIM5 alternate function option register 1 (TIM5_AF1) *************/
#define CHIP_REGFLD_TIM5_AF1_ETRSEL                 (0x0000000FUL << 14U) /* ETR source selection */

#define CHIP_REGFLDVAL_TIM5_AF1_ETRSEL_IO           0 /* ETR input is connected to I/O */
#define CHIP_REGFLDVAL_TIM5_AF1_ETRSEL_SAI2_FS_A    1
#define CHIP_REGFLDVAL_TIM5_AF1_ETRSEL_SAI2_FS_B    2

/*********** TIM15 alternate register 1 (TIM15_AF1) ***************************/
#define CHIP_REGFLD_TIM15_AF1_BKINE                 (0x00000001UL <<  0U) /* BKINE Break input enable bit */
#define CHIP_REGFLD_TIM15_AF1_BKCMP1E               (0x00000001UL <<  1U) /* BKCMP1E Break Compare1 Enable bit */
#define CHIP_REGFLD_TIM15_AF1_BKCMP2E               (0x00000001UL <<  2U) /* BKCMP1E Break Compare2 Enable bit  */
#define CHIP_REGFLD_TIM15_AF1_BKDF1BK2E             (0x00000001UL <<  8U) /* BRK dfsdm1_break[0] enable */
#define CHIP_REGFLD_TIM15_AF1_BKINP                 (0x00000001UL <<  9U) /* BRKINP Break input polarity */
#define CHIP_REGFLD_TIM15_AF1_BKCMP1P               (0x00000001UL << 10U) /* BKCMP1P Break COMP1 input polarity */
#define CHIP_REGFLD_TIM15_AF1_BKCMP2P               (0x00000001UL << 11U) /* BKCMP2P Break COMP2 input polarity */

/*********** TIM16 alternate function register 1 (TIM16_AF1) ******************/
#define CHIP_REGFLD_TIM16_AF1_BKINE                 (0x00000001UL <<  0U) /* BKINE Break input enable bit */
#define CHIP_REGFLD_TIM16_AF1_BKCMP1E               (0x00000001UL <<  1U) /* BKCMP1E Break Compare1 Enable bit */
#define CHIP_REGFLD_TIM16_AF1_BKCMP2E               (0x00000001UL <<  2U) /* BKCMP1E Break Compare2 Enable bit  */
#define CHIP_REGFLD_TIM16_AF1_BKDF1BK2E             (0x00000001UL <<  8U) /* BRK dfsdm1_break[1] enable */
#define CHIP_REGFLD_TIM16_AF1_BKINP                 (0x00000001UL <<  9U) /* BRKINP Break input polarity */
#define CHIP_REGFLD_TIM16_AF1_BKCMP1P               (0x00000001UL << 10U) /* BKCMP1P Break COMP1 input polarity */
#define CHIP_REGFLD_TIM16_AF1_BKCMP2P               (0x00000001UL << 11U) /* BKCMP2P Break COMP2 input polarity */

/*********** TIM17 alternate function register 1 (TIM17_AF1) ******************/
#define CHIP_REGFLD_TIM17_AF1_BKINE                 (0x00000001UL <<  0U) /* BKINE Break input enable bit */
#define CHIP_REGFLD_TIM17_AF1_BKCMP1E               (0x00000001UL <<  1U) /* BKCMP1E Break Compare1 Enable bit */
#define CHIP_REGFLD_TIM17_AF1_BKCMP2E               (0x00000001UL <<  2U) /* BKCMP1E Break Compare2 Enable bit  */
#define CHIP_REGFLD_TIM17_AF1_BKDF1BK2E             (0x00000001UL <<  8U) /* BRK dfsdm1_break[2] enable */
#define CHIP_REGFLD_TIM17_AF1_BKINP                 (0x00000001UL <<  9U) /* BRKINP Break input polarity */
#define CHIP_REGFLD_TIM17_AF1_BKCMP1P               (0x00000001UL << 10U) /* BKCMP1P Break COMP1 input polarity */
#define CHIP_REGFLD_TIM17_AF1_BKCMP2P               (0x00000001UL << 11U) /* BKCMP2P Break COMP2 input polarity */

#endif // CHIP_REGS_TIM_STM32H7XX_H_
