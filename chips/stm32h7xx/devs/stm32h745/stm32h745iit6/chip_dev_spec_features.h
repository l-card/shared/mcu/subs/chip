#ifndef CHIP_DEV_SPEC_FEATURES_H
#define CHIP_DEV_SPEC_FEATURES_H

#include "chip_pkg_types.h"

#define CHIP_DEV_PKG                             CHIP_PKG_LQFP176
#define CHIP_FLASH_MEM_SIZE                      (2*1024*1024)

#endif // CHIP_DEV_SPEC_FEATURES_H
