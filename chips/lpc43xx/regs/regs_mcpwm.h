/*
 * @brief LPC18xx/43xx Motor Control PWM driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef __MCPWM_18XX_43XX_H_
#define __MCPWM_18XX_43XX_H_

/** @defgroup MCPWM_18XX_43XX CHIP: LPC18xx/43xx Motor Control PWM driver
 * @ingroup CHIP_18XX_43XX_Drivers
 * @{
 */

/**
 * @brief Motor Control PWM register block structure
 */
typedef struct {                    /*!< MCPWM Structure        */
    __I  uint32_t  CON;             /*!< PWM Control read address */
    __O  uint32_t  CON_SET;         /*!< PWM Control set address */
    __O  uint32_t  CON_CLR;         /*!< PWM Control clear address */
    __I  uint32_t  CAPCON;          /*!< Capture Control read address */
    __O  uint32_t  CAPCON_SET;      /*!< Capture Control set address */
    __O  uint32_t  CAPCON_CLR;      /*!< Event Control clear address */
    __IO uint32_t TC[3];            /*!< Timer Counter register */
    __IO uint32_t LIM[3];           /*!< Limit register         */
    __IO uint32_t MAT[3];           /*!< Match register         */
    __IO uint32_t  DT;              /*!< Dead time register     */
    __IO uint32_t  CCP;             /*!< Communication Pattern register */
    __I  uint32_t CAP[3];           /*!< Capture register       */
    __I  uint32_t  INTEN;           /*!< Interrupt Enable read address */
    __O  uint32_t  INTEN_SET;       /*!< Interrupt Enable set address */
    __O  uint32_t  INTEN_CLR;       /*!< Interrupt Enable clear address */
    __I  uint32_t  CNTCON;          /*!< Count Control read address */
    __O  uint32_t  CNTCON_SET;      /*!< Count Control set address */
    __O  uint32_t  CNTCON_CLR;      /*!< Count Control clear address */
    __I  uint32_t  INTF;            /*!< Interrupt flags read address */
    __O  uint32_t  INTF_SET;        /*!< Interrupt flags set address */
    __O  uint32_t  INTF_CLR;        /*!< Interrupt flags clear address */
    __O  uint32_t  CAP_CLR;         /*!< Capture clear address  */
} LPC_MCPWM_T;

/**
 * @brief MCPWM CON register
 */
#define MCPWM_CON_RUN(n)            (0x01UL << (8 * (n)))       /* 1 = Run, n = 0..2 */
#define MCPWM_CON_CENTER(n)         (0x02UL << (8 * (n)))       /* 1 = Up/down count, n = 0..2 */
#define MCPWM_CON_POLA(n)           (0x04UL << (8 * (n)))       /* 1 = Inverted polarity, n = 0..2 */
#define MCPWM_CON_DTE(n)            (0x08UL << (8 * (n)))       /* 1 = Dead-time enable, n = 0..2 */
#define MCPWM_CON_DISUP(n)          (0x10UL << (8 * (n)))       /* 1 = Disable LIM/MAT/CP update, n = 0..2 */
#define MCPWM_CON_INVBDC            (1UL << 29)                 /* 1 = MCOB same polarity as MCOA in DC mode */
#define MCPWM_CON_ACMODE            (1UL << 30)                 /* 1 = All channels use counter/limit 0 */
#define MCPWM_CON_DCMODE            (1UL << 31)                 /* 1 = Route MCOA0 through CCP mask to all outputs */

/**
 * @brief MCPWM CAPCON register
 */
#define MCPWM_CAPCON_RISE(n, m)     (1UL << ((3 * (n) + (m)) * 2))  /* Channel n (0..2) capture on rising edge of MCIm (0..2) */
#define MCPWM_CAPCON_FALL(n, m)     (2UL << ((3 * (n) + (m)) * 2))  /* Channel n (0..2) capture on falling edge of MCIm (0..2) */
#define MCPWM_CAPCON_RT(n)          (1UL << (18 + (n)))             /* Reset TCn (0..2) on channel capture event */

/**
 * @brief MCPWM DT register
 */
#define MCPWM_DT_DT(n, x)           (((x) & 0x3FFUL) << (10 * (n))) /* Dead time x for channel n (0..2) */

/**
 * @brief MCPWM CCP register
 */
#define MCPWM_CCP_CCPA0             (1UL << 0)
#define MCPWM_CCP_CCPB0             (1UL << 1)
#define MCPWM_CCP_CCPA1             (1UL << 2)
#define MCPWM_CCP_CCPB1             (1UL << 3)
#define MCPWM_CCP_CCPA2             (1UL << 4)
#define MCPWM_CCP_CCPB2             (1UL << 5)

/**
 * @brief MCPWM Interrupt registers (INTEN, INTF)
 */
#define MCPWM_INT_ILIM(n)           (1UL << (4 * (n)))          /* Limit interrupt for channel n (0..2) */
#define MCPWM_INT_IMAT(n)           (2UL << (4 * (n)))          /* Match interrupt for channel n (0..2) */
#define MCPWM_INT_ICAP(n)           (4UL << (4 * (n)))          /* Capture interrupt for channel n (0..2) */
#define MCPWM_INT_ABORT             (1UL << 15)                 /* Fast abort interrupt */

/**
 * @brief MCPWM CNTCON register
 */
#define MCPWM_CNTCON_RISE(n, m)     (1UL << ((3 * (n) + (m)) * 2))  /* 1 = Channel n (0..2) counts rising edges of MCIm (0..2) */
#define MCPWM_CNTCON_FALL(n, m)     (2UL << ((3 * (n) + (m)) * 2))  /* 1 = Channel n (0..2) counts falling edges of MCIm (0..2) */
#define MCPWM_CNTCON_CNTR(n)        (1UL << (29 + (n)))             /* 1 = Channel n (0..2) in counter mode, 0 = timer mode */

/**
 * @brief MCPWM CAP_CLR register
 */
#define MCPWM_CAPCLR(n)             (1UL << (n))                /* 1 = Clear CAP[n] */

/**
 * @}
 */


#endif /* __MCPWM_18XX_43XX_H_ */
