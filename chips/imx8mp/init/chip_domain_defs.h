#ifndef CHIP_DOMAIN_DEF_H
#define CHIP_DOMAIN_DEF_H


#define CHIP_DOMAIN_CNT         4 /* общее число доменов, доступных в RDC-контроллере */

#define CHIP_DOMAIN_NUM_A53     0 /* номер домена, который будет использоваться для Cortex-A */
#define CHIP_DOMAIN_NUM_CM7     1 /* номер домена, который будет использоваться для Cortex-M */

/* Определение домена, используемого для текущего процессора */
#if defined CHIP_CORETYPE_CA53
    #define CHIP_CUR_DOMAIN_NUM     CHIP_DOMAIN_NUM_A53
#elif defined CHIP_CORETYPE_CM7
    #define CHIP_CUR_DOMAIN_NUM     CHIP_DOMAIN_NUM_CM7
#endif


#endif // CHIP_DOMAIN_DEF_H
