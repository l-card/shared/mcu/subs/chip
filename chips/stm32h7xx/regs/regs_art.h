#ifndef CHIP_STM32H7XX_REGS_ART_H_
#define CHIP_STM32H7XX_REGS_ART_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"
#include "lcspec.h"

#if CHIP_DEV_SUPPORT_CORE_CM4

typedef struct {
    __IO uint32_t  CTR;        /* ART accelerator - control register */
} CHIP_REGS_ART_T;

#define CHIP_REGS_ART                           ((CHIP_REGS_ART_T *) CHIP_MEMRGN_ADDR_PERIPH_ART)

/****************** ART accelerator - control register (ART_CTR) **************/
#define CHIP_REGFLD_ART_CTR_EN                  (0x00000001UL <<  0U)  /* Cache enable*/
#define CHIP_REGFLD_ART_CTR_PCACHEADDR          (0x00000FFFUL <<  8U)  /* Cacheable page index */
#define CHIP_REGMSK_ART_CTR_RESERVED            (0xFFF000FEUL)


static LINLINE void chip_art_cache_enable(uint32_t memrgn_addr) {
    CHIP_REGS_ART->CTR = (CHIP_REGS_ART->CTR & CHIP_REGMSK_ART_CTR_RESERVED) |
            LBITFIELD_SET(CHIP_REGFLD_ART_CTR_PCACHEADDR, ((memrgn_addr) >> 20)) |
            LBITFIELD_SET(CHIP_REGFLD_ART_CTR_EN, 1);
}

static LINLINE void chip_art_cache_disable(void) {
    CHIP_REGS_ART->CTR &= ~CHIP_REGFLD_ART_CTR_EN;
}

#endif

#endif // CHIP_STM32H7XX_REGS_ART_H_
