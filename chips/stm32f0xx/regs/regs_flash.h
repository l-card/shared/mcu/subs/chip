#ifndef CHIP_STM32F0XX_REGS_FLASH_H
#define CHIP_STM32F0XX_REGS_FLASH_H

#include <stdint.h>
#include "init/chip_clk_constraints.h"

#define CHIP_SUPPORT_FLASH_BANK2                0
#define CHIP_SUPPORT_FLASH_FORCE_OB_LOAD        1
#if defined CHIP_DEV_STM32F04X || defined CHIP_DEV_STM32F09X
    #define CHIP_SUPPORT_FLASH_OPTION_NBOOT0    1
    #define CHIP_SUPPORT_FLASH_OPTION_BOOT_SEL  1
#else
    #define CHIP_SUPPORT_FLASH_OPTION_NBOOT0    0
    #define CHIP_SUPPORT_FLASH_OPTION_BOOT_SEL  0
#endif
#define CHIP_SUPPORT_FLASH_REG_WS_EN            0
#define CHIP_SUPPORT_FLASH_REG_PID              0
#define CHIP_SUPPORT_FLASH_ECC                  0
#define CHIP_FLASH_CUSTOM_OPTIONS               0

#include "stm32_flash_v1.h"

#endif // CHIP_STM32F0XX_REGS_FLASH_H
