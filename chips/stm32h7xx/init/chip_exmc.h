#ifndef CHIP_EXMC_H
#define CHIP_EXMC_H

#include "chip_exmc_cfg.h"

#if CHIP_CFG_EXMC_SETUP_EN
void chip_exmc_init(void);
#endif

#endif // CHIP_EXMC_H
