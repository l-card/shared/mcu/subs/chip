#include "chip.h"
#include <stddef.h>

#if CHIP_MDMA_FUNC_EN

static t_chip_mdma_ch_isr_func f_ch_isr[CHIP_MDMA_CHANNEL_CNT];

void chip_mdma_init() {
    chip_per_clk_en(CHIP_PER_ID_MDMA);
    chip_per_rst(CHIP_PER_ID_MDMA);

    chip_interrupt_enable(MDMA_IRQn);
}

void chip_mdma_set_ch_isr(int ch_num, t_chip_mdma_ch_isr_func isr) {
    f_ch_isr[ch_num] = isr;
}



void MDMA_IRQHandler(void) {
    const uint32_t girq = CHIP_REGS_MDMA->GISR0;
    for (int ch = 0; ch < CHIP_MDMA_CHANNEL_CNT; ++ch) {
        if (girq & (1 << ch)) {
            CHIP_REGS_MDMA_CHANNEL_T *ch_regs = &CHIP_REGS_MDMA->CH[ch];
            uint32_t irq_flags = ch_regs->CISR;
            ch_regs->CIFCR = irq_flags;
            if (f_ch_isr[ch] != NULL) {
                f_ch_isr[ch](ch, irq_flags, ch_regs);
            }
        }
    }
    chip_dsb();
}

#endif

