#ifndef CHIP_INIT_FAULTS_H
#define CHIP_INIT_FAULTS_H

#include "chip_config.h"
#include "chip_init_wait.h"

#define CHIP_INIT_FAULT_WAIT_ACTVOSRDY          1
#define CHIP_INIT_FAULT_WAIT_D3CR_VOSRDY        2
#define CHIP_INIT_FAULT_WAIT_LSI_RDY            3
#define CHIP_INIT_FAULT_WAIT_HSI_RDY            4
#define CHIP_INIT_FAULT_WAIT_BASECLK_RDY        5
#define CHIP_INIT_FAULT_WAIT_LSE_RDY            6
#define CHIP_INIT_FAULT_WAIT_PLL_RDY            7
#define CHIP_INIT_FAULT_WAIT_D1_2_RDY           8



#endif // CHIP_INIT_FAULTS_H
