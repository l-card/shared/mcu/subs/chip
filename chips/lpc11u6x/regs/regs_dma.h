#ifndef CHIP_LPC11U6X_REGS_DMA_H
#define CHIP_LPC11U6X_REGS_DMA_H

#include <stdint.h>

#define CHIP_DMA_CH_CNT                 16

#define CHIP_DMA_REQ_CH_SSP0_RX         0  /* SSP0 receive DMA channel */
#define CHIP_DMA_REQ_CH_SSP0_TX         1  /* SSP0 transmit DMA channel */
#define CHIP_DMA_REQ_CH_SSP1_RX         2  /* SSP1 receive DMA channel */
#define CHIP_DMA_REQ_CH_SSP1_TX         3  /* SSP1 transmit DMA channel */
#define CHIP_DMA_REQ_CH_USART0_RX       4  /* USART0 receive DMA channel */
#define CHIP_DMA_REQ_CH_USART0_TX       5  /* USART0 transmit DMA channel */
#define CHIP_DMA_REQ_CH_USART1_RX       6  /* USART1 receive DMA channel */
#define CHIP_DMA_REQ_CH_USART1_TX       7  /* USART1 transmit DMA channel */
#define CHIP_DMA_REQ_CH_USART2_RX       8  /* USART2 receive DMA channel */
#define CHIP_DMA_REQ_CH_USART2_TX       9  /* USART2 transmit DMA channel */
#define CHIP_DMA_REQ_CH_USART3_RX       10  /* USART3 receive DMA channel */
#define CHIP_DMA_REQ_CH_USART3_TX       11  /* USART3 transmit DMA channel */
#define CHIP_DMA_REQ_CH_USART4_RX       12  /* USART4 receive DMA channel */
#define CHIP_DMA_REQ_CH_USART4_TX       13  /* USART5 transmit DMA channel */

#define CHIP_DMA_TRIG_ADC0_SEQA         0
#define CHIP_DMA_TRIG_ADC0_SEQB         1
#define CHIP_DMA_TRIG_CT16B0_MAT0       2
#define CHIP_DMA_TRIG_CT16B1_MAT0       3
#define CHIP_DMA_TRIG_CT32B0_MAT0       4
#define CHIP_DMA_TRIG_CT32B1_MAT0       5
#define CHIP_DMA_TRIG_PINT0             6
#define CHIP_DMA_TRIG_PINT1             7
#define CHIP_DMA_TRIG_SCT0_DMA0         8
#define CHIP_DMA_TRIG_SCT0_DMA1         9
#define CHIP_DMA_TRIG_SCT1_DMA0         10
#define CHIP_DMA_TRIG_SCT1_DMA1         11


/**
 * @brief DMA Controller shared registers structure
 */
typedef struct {                    /*!< DMA channel register structure */
    __IO uint32_t  CFG;                /*!< DMA Configuration register */
    __I  uint32_t  CTLSTAT;            /*!< DMA Control and status register */
    __IO uint32_t  XFERCFG;            /*!< DMA Transfer configuration register */
    __I  uint32_t  RESERVED;
} CHIP_REGS_DMA_CHANNEL_T;


/**
 * @brief DMA Controller register block structure
 */
typedef struct {                    /*!< DMA Structure */
    __IO uint32_t  CTRL;            /*!< DMA control register */
    __I  uint32_t  INTSTAT;            /*!< DMA Interrupt status register */
    __IO uint32_t  SRAMBASE;        /*!< DMA SRAM address of the channel configuration table */
    __I  uint32_t  RESERVED1[5];
    __IO uint32_t  ENABLESET;       /*!< DMA Channel Enable read and Set for all DMA channels */
    __I  uint32_t  RESERVED2;
    __O  uint32_t  ENABLECLR;       /*!< DMA Channel Enable Clear for all DMA channels */
    __I  uint32_t  RESERVED3;
    __I  uint32_t  ACTIVE;          /*!< DMA Channel Active status for all DMA channels */
    __I  uint32_t  RESERVED4;
    __I  uint32_t  BUSY;            /*!< DMA Channel Busy status for all DMA channels */
    __I  uint32_t  RESERVED5;
    __IO uint32_t  ERRINT;          /*!< DMA Error Interrupt status for all DMA channels */
    __I  uint32_t  RESERVED6;
    __IO uint32_t  INTENSET;        /*!< DMA Interrupt Enable read and Set for all DMA channels */
    __I  uint32_t  RESERVED7;
    __O  uint32_t  INTENCLR;        /*!< DMA Interrupt Enable Clear for all DMA channels */
    __I  uint32_t  RESERVED8;
    __IO uint32_t  INTA;            /*!< DMA Interrupt A status for all DMA channels */
    __I  uint32_t  RESERVED9;
    __IO uint32_t  INTB;            /*!< DMA Interrupt B status for all DMA channels */
    __I  uint32_t  RESERVED10;
    __O  uint32_t  SETVALID;        /*!< DMA Set ValidPending control bits for all DMA channels */
    __I  uint32_t  RESERVED11;
    __O  uint32_t  SETTRIG;          /*!< DMA Set Trigger control bits for all DMA channels */
    __I  uint32_t  RESERVED12;
    __O  uint32_t  ABORT;            /*!< DMA Channel Abort control for all DMA channels */
    __I  uint32_t  RESERVED0[225];
    CHIP_REGS_DMA_CHANNEL_T CH[CHIP_DMA_CH_CNT];    /*!< DMA channel registers */
} CHIP_REGS_DMA_T;



/**
 * @brief DMA trigger pin muxing structure
 */
typedef struct {                    /*!< DMA trigger pin muxing register structure */
    __IO uint32_t  INMUX[CHIP_DMA_CH_CNT];    /*!< Trigger input select register for DMA channels */
} CHIP_REGS_DMA_TRIGMUX_T;



/* DMA channel source/address/next descriptor */
typedef struct {
    uint32_t  XFERCFG;        /*!< Transfer configuration (only used in linked lists and ping-pong configs) */
    uint32_t  SRC;            /*!< DMA transfer source end address */
    uint32_t  DST;            /*!< DMA transfer desintation end address */
    uint32_t  NEXT;           /*!< Link to next DMA descriptor, must be 16 byte aligned */
} CHIP_DMA_DESC_T;


#define CHIP_REGS_DMA              ((CHIP_REGS_DMA_T *) CHIP_MEMRGN_ADDR_PERIPH_DMA)
#define CHIP_REGS_DMA_TRIGMUX      ((CHIP_REGS_DMA_TRIGMUX_T *) CHIP_MEMRGN_ADDR_PERIPH_DMA_TRIGMUX)

/*********** DMA Control Register (DMA_CTRL) **********************************/
#define CHIP_REGFLD_DMA_CTRL_ENABLE                 (0x00000001UL <<  0U)   /* (RW) DMA controller master enable. */

/*********** DMA Interrupt Status register (DMA_INTSTAT) **********************/
#define CHIP_REGFLD_DMA_INTSTAT_ACTIVE_INT          (0x00000001UL <<  1U)   /* (RO) Whether any enabled interrupts are pending */
#define CHIP_REGFLD_DMA_INTSTAT_ACTIVE_ERRINT       (0x00000001UL <<  2U)   /* (RO) Whether any error interrupts are pending */

/*********** Channel configuration registers (DMA_CH_CFG) *********************/
#define CHIP_REGFLD_DMA_CH_CFG_PERIPH_REQ_EN        (0x00000001UL <<  0U)   /* (RW) Peripheral request Enable */
#define CHIP_REGFLD_DMA_CH_CFG_HWTRIG_EN            (0x00000001UL <<  1U)   /* (RW) Hardware Triggering Enable for this channel */
#define CHIP_REGFLD_DMA_CH_CFG_TRIG_POL             (0x00000001UL <<  4U)   /* (RW) Trigger Polarity */
#define CHIP_REGFLD_DMA_CH_CFG_TRIG_TYPE            (0x00000001UL <<  5U)   /* (RW) Trigger Polarity */
#define CHIP_REGFLD_DMA_CH_CFG_TRIG_BURST           (0x00000001UL <<  6U)   /* (RW) Trigger Burst. Selects whether hardware triggers cause a single or burst transfer.*/
#define CHIP_REGFLD_DMA_CH_CFG_BURST_POWER          (0x0000000FUL <<  8U)   /* (RW) Burst Power (2^n). Address wrap size + burst request len.*/
#define CHIP_REGFLD_DMA_CH_CFG_SRC_BURST_WRAP       (0x00000001UL << 14U)   /* (RW) Source Burst Wrap */
#define CHIP_REGFLD_DMA_CH_CFG_DST_BURST_WRAP       (0x00000001UL << 15U)   /* (RW) Destination Burst Wrap */
#define CHIP_REGFLD_DMA_CH_CFG_PRIORITY             (0x00000003UL << 16U)   /* (RW) Channel priority (0 - highest, 3 - lowest) */

#define CHIP_REGFLDVAL_DMA_CH_CFG_TRIG_POL_LOW      0 /* Active low/Falling edge */
#define CHIP_REGFLDVAL_DMA_CH_CFG_TRIG_POL_HIGH     1 /* Active high/Rising edge */
#define CHIP_REGFLDVAL_DMA_CH_CFG_TRIG_TYPE_EDGE    0 /* Edge. Hardware trigger is edge triggered. */
#define CHIP_REGFLDVAL_DMA_CH_CFG_TRIG_TYPE_LEVEL   1 /* Level. Hardware trigger is level triggered. Transfers continue as long as the trigger level is asserted. O */

/*********** Channel control and status registers (DMA_CH_CTLSTAT) *************/
#define CHIP_REGFLD_DMA_CH_CTLSTAT_VALID_PENDING    (0x00000001UL <<  0U)   /* (RO) Valid Pending flag */
#define CHIP_REGFLD_DMA_CH_CTLSTAT_TRIG             (0x00000001UL <<  2U)   /* (RO) Trigger flag */

/*********** Channel transfer configuration registers (DMA_CH_XFERCFG) ********/
#define CHIP_REGFLD_DMA_CH_XFERCFG_CFG_VALID        (0x00000001UL <<  0U)   /* (RW) Configuration Valid flag */
#define CHIP_REGFLD_DMA_CH_XFERCFG_RELOAD           (0x00000001UL <<  1U)   /* (RW) Whether the channel’s control structure will be reloaded when the current descriptor is exhausted.*/
#define CHIP_REGFLD_DMA_CH_XFERCFG_SW_TRIG          (0x00000001UL <<  2U)   /* (RW) Software Trigger */
#define CHIP_REGFLD_DMA_CH_XFERCFG_CLR_TRIG         (0x00000001UL <<  3U)   /* (RW) Clear Trigger when this descriptor is exhausted */
#define CHIP_REGFLD_DMA_CH_XFERCFG_SET_INTA         (0x00000001UL <<  4U)   /* (RW) Set Interrupt flag A for this channel. */
#define CHIP_REGFLD_DMA_CH_XFERCFG_SET_INTB         (0x00000001UL <<  5U)   /* (RW) Set Interrupt flag B for this channel. */
#define CHIP_REGFLD_DMA_CH_XFERCFG_WIDTH            (0x00000003UL <<  8U)   /* (RW) Transfer width used for this DMA channel. */
#define CHIP_REGFLD_DMA_CH_XFERCFG_SRC_INC          (0x00000003UL << 12U)   /* (RW) Whether the source address is incremented for each DMA transfer. */
#define CHIP_REGFLD_DMA_CH_XFERCFG_DST_INC          (0x00000003UL << 14U)   /* (RW) Whether the desctination address is incremented for each DMA transfer. */
#define CHIP_REGFLD_DMA_CH_XFERCFG_XFER_COUNT       (0x000003FFUL << 16U)   /* (RW) Total number of transfers to be performed, minus 1 encoded */

#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_8             0
#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_16            1
#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_32            2

#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_INC_NO        0 /* no increment */
#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_INC_1         1 /* 1x width increment */
#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_INC_2         2 /* 2x width increment */
#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_INC_4         3 /* 4x width increment */

#define CHIP_REGFLDVAL_DMA_CH_XFERCFG_XFER_COUNT(n) (n-1)


#endif /* CHIP_LPC11U6X_REGS_DMA_H */

