#ifndef CHIP_DEV_MEM_H
#define CHIP_DEV_MEM_H

#include "chip_dev_spec_features.h"

#define CHIP_RAM_MEM_SIZE            (16 * 1024)
#define CHIP_SYS_MEM_SIZE            (12 * 1024)

#endif // CHIP_DEV_MEM_H
