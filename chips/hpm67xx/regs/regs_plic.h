#ifndef REGS_PLIC_H
#define REGS_PLIC_H

#include "chip_ioreg_defs.h"

#define CHIP_PLIC_IRQ_CNT                   128
#define CHIP_PLIC_BITREG_CNT                ((CHIP_PLIC_IRQ_CNT + 31)/32)
#define CHIP_PLIC_TARGET_CNT                2


typedef struct {
    __IO uint32_t INTEN[CHIP_PLIC_BITREG_CNT];          /* (STD) Machine interrupt enable */
    uint32_t RESERVED3[32 - CHIP_PLIC_BITREG_CNT];
} CHIP_REGS_PLIC_TARGETINT_T;

typedef struct {
    __IO uint32_t THRESHOLD;                            /* (STD) Machine interrupt priority threshold,          Address offset: 0x0000 */
    __IO uint32_t CLAIM;                                /* (STD) Interrupt response and completion,             Address offset: 0x0004 */
    uint32_t RESERVED1[254];
    __IO uint32_t PTP;                                  /* (STD) Interrupt preemption status,                   Address offset: 0x0400 */
    uint32_t RESERVED2[767];
} CHIP_REGS_PLIC_TARGETCONFIG_T;

typedef struct {
    __IO uint32_t FEATURE;                              /* (HPM) Feature Enable Register,                       Address offset: 0x0000 */
    __IO uint32_t PRIORITY[CHIP_PLIC_IRQ_CNT-1];        /* (STD) Interrupt Priority Register (1-127),           Address offset: 0x0004 - 0x01FC */
    uint32_t RESERVED1[1024 - CHIP_PLIC_IRQ_CNT];
    __IO uint32_t PENDING[CHIP_PLIC_BITREG_CNT];        /* (STD) Pending Interrupt List (0-3),                  Address offset: 0x1000 - 0x100C */
    uint32_t RESERVED2[32 - CHIP_PLIC_BITREG_CNT];
    __IO uint32_t TRIGGER[CHIP_PLIC_BITREG_CNT];        /* (HPM) Interrupt triggering mode configuration (0-3), Address offset: 0x1080 - 0x108C */
    uint32_t RESERVED3[32 - CHIP_PLIC_BITREG_CNT];
    __IO uint32_t NUMBER;                               /* (HPM) PLIC attribute query,                          Address offset: 0x1100 */
    __IO uint32_t INFO;                                 /* (HPM) PLIC attribute query,                          Address offset: 0x1104 */
    uint32_t RESERVED4[958];
    CHIP_REGS_PLIC_TARGETINT_T TARGETINT[CHIP_PLIC_TARGET_CNT]; /* (STD)                                        Address offset: 0x2000 - 0x200C, 0x2080 - 0x208C */
    uint32_t RESERVED[130551];
    CHIP_REGS_PLIC_TARGETCONFIG_T TARGETCONFIG[CHIP_PLIC_TARGET_CNT];  /*                                       Address offset: 0x200000,  0x201000 */
} CHIP_REGS_PLIC_T;

#define CHIP_REGS_PLIC            ((CHIP_REGS_PLIC_T *)CHIP_MEMRGN_ADDR_PLIC)


/*********** Feature Enable Register - PLIC_FEATURE  **************************/
#define CHIP_REGFLD_PLIC_FEATURE_VECTORED               (0x00000001UL <<  1U) /* (RW) Vectored interrupt enable */
#define CHIP_REGFLD_PLIC_FEATURE_PREEMPT                (0x00000001UL <<  0U) /* (RW) Interrupt priority preemption enable */

/*********** PLIC attribute query - PLIC_NUMBER  ******************************/
#define CHIP_REGFLD_PLIC_NUMBER_NUM_TARGET              (0x0000FFFFUL << 16U) /* (RO) Number of targets supported  */
#define CHIP_REGFLD_PLIC_NUMBER_NUM_INTERRUPT           (0x0000FFFFUL <<  0U) /* (RO) Number of supported interrupt sources  */

/*********** PLIC attribute query - PLIC_INFO *********************************/
#define CHIP_REGFLD_PLIC_INFO_MAX_PRIORITY              (0x0000FFFFUL << 16U) /* (RO) Maximum supported priority  */
#define CHIP_REGFLD_PLIC_INFO_VERSION                   (0x0000FFFFUL <<  0U) /* (RO) IP version  */

#endif // REGS_PLIC_H
