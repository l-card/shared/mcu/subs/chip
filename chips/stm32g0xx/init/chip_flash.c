#include "chip.h"
#include <stddef.h>

#if CHIP_CFG_LTIMER_EN
    #include "ltimer.h"
#endif



#if CHIP_CFG_PWR_VOS == CHIP_PWR_VOS1
    #if (CHIP_CLK_FLASH_FREQ > CHIP_MHZ(48))
        #define CHIP_FLASH_WS_CNT       2
    #elif (CHIP_CLK_FLASH_FREQ > CHIP_MHZ(24))
        #define CHIP_FLASH_WS_CNT       1
    #else
        #define CHIP_FLASH_WS_CNT       0
    #endif
#elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS2
    #if (CHIP_CLK_FLASH_FREQ > CHIP_MHZ(8))
        #define CHIP_FLASH_WS_CNT       1
    #else
        #define CHIP_FLASH_WS_CNT       0
    #endif
#endif


#define CHIP_FLASH_WRP_PREG(area) ((area ? &CHIP_REGS_FLASH->WRP1AR : &CHIP_REGS_FLASH->WRP1BR))

#define CHIP_FLASH_RDP_L0_CODE 0xAA
#define CHIP_FLASH_RDP_L2_CODE 0xCC


/* для быстрой записи частота flash должна быть не менее 8 МГц. Также можно запретить быструю запись в конфигурации */
#ifndef CHIP_CFG_FLASH_ROW_WR_ENABLED
    #define CHIP_CFG_FLASH_ROW_WR_ENABLED 1
#endif
#define CHIP_FLASH_ROW_WR_ENABLED (CHIP_CFG_FLASH_ROW_WR_ENABLED && (CHIP_CLK_FLASH_FREQ >= CHIP_MHZ(8)))


#define CHIP_FLASH_KEY1     0x45670123
#define CHIP_FLASH_KEY2     0xCDEF89AB
#define CHIP_FLASH_OPT_KEY1 0x08192A3B
#define CHIP_FLASH_OPT_KEY2 0x4C5D6E7F

static CHIP_RAM_FUNC t_chip_flash_err flash_wait_op_done_ram(void);
static t_chip_flash_err flash_wait_op_done_us(unsigned timeout_us);
#if CHIP_FLASH_ROW_WR_ENABLED
static t_chip_flash_pagenum f_last_err_page = -1;
#endif

void chip_flash_init_freq(void) {
    /* подстраиваем количество вейтстетов по настройкам VOS и FLASH_FREQ */
    LBITFIELD_UPD(CHIP_REGS_FLASH->ACR, CHIP_REGFLD_FLASH_ACR_LATENCY, CHIP_FLASH_WS_CNT);
    /* проверяем, что значения вступили в силу */
    while (LBITFIELD_GET(CHIP_REGS_FLASH->ACR, CHIP_REGFLD_FLASH_ACR_LATENCY) != CHIP_FLASH_WS_CNT) {
        continue;
    }

    /* разрешаем prefetch, если используется хотя бы один вайтстейт, как рекомендованно в RM */
    LBITFIELD_UPD(CHIP_REGS_FLASH->ACR, CHIP_REGFLD_FLASH_ACR_PRFTEN, CHIP_FLASH_WS_CNT > 0);
}


#if CHIP_CFG_FLASH_FUNC_EN

static LINLINE t_chip_flash_err flash_check_unlocked(void) {
    return CHIP_REGS_FLASH->CR & CHIP_REGFLD_FLASH_CR_LOCK ? CHIP_FLASH_ERR_LOCKED : CHIP_FLASH_ERR_OK;
}

t_chip_flash_err chip_flash_check_rdy(void) {
    /** @todo multibank: в случае нескольких банков может использоваться BSY2 */
    return CHIP_REGS_FLASH->SR & CHIP_REGFLD_FLASH_SR_BSY1 ? CHIP_FLASH_ERR_NOT_RDY : CHIP_FLASH_ERR_OK;
}



void chip_flash_unlock(void) {
    if (flash_check_unlocked() != CHIP_FLASH_ERR_OK) {
        CHIP_REGS_FLASH->KEYR = CHIP_FLASH_KEY1;
        CHIP_REGS_FLASH->KEYR = CHIP_FLASH_KEY2;
    }
}

void chip_flash_lock() {
    CHIP_REGS_FLASH->CR |= CHIP_REGFLD_FLASH_CR_LOCK;
}

const void *chip_flash_rd_ptr(uint32_t addr) {
    return (const void *)(CHIP_MEMRGN_ADDR_FLASH + addr);
}

static void *chip_flash_ptr(uint32_t addr) {
    return (void *)(CHIP_MEMRGN_ADDR_FLASH + addr);
}

#if CHIP_FLASH_ROW_WR_ENABLED
static uint32_t chip_flash_offs(void *ptr) {
    return ((intptr_t)ptr - CHIP_MEMRGN_ADDR_FLASH);
}
#endif


#define CHIP_FLASH_ERROR_MASK (CHIP_REGFLD_FLASH_SR_PROGERR  \
    | CHIP_REGFLD_FLASH_SR_WRPERR \
    | CHIP_REGFLD_FLASH_SR_PGAERR \
    | CHIP_REGFLD_FLASH_SR_SIZERR \
    | CHIP_REGFLD_FLASH_SR_PGSERR \
    | CHIP_REGFLD_FLASH_SR_MISSERR \
    | CHIP_REGFLD_FLASH_SR_FASTERR \
    | CHIP_REGFLD_FLASH_SR_RDERR \
    | CHIP_REGFLD_FLASH_SR_OPTVERR \
    )


static void flash_clear_en(void) {
    /* сброс изменных разрешений на стирание */
    CHIP_REGS_FLASH->CR  &= ~(CHIP_REGFLD_FLASH_CR_PER | CHIP_REGFLD_FLASH_CR_PG | CHIP_REGFLD_FLASH_CR_FSTPG | CHIP_REGFLD_FLASH_CR_PNB);
}

static LINLINE void flash_clear_errs(void) {
    CHIP_REGS_FLASH->SR = CHIP_FLASH_ERROR_MASK;
}

static t_chip_flash_err flash_parse_status(unsigned status) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    if (status & CHIP_FLASH_ERROR_MASK) {
        if (status & CHIP_REGFLD_FLASH_SR_PROGERR) {
            err = CHIP_FLASH_ERR_STAT_PROG_NOT_EMPTY;
        } else if (status & CHIP_REGFLD_FLASH_SR_WRPERR) {
            err = CHIP_FLASH_ERR_STAT_WRITE_PROTECT;
        } else if (status & CHIP_REGFLD_FLASH_SR_PGAERR) {
            err = CHIP_FLASH_ERR_STAT_PROG_NOT_ALIGN;
        } else if (status & CHIP_REGFLD_FLASH_SR_SIZERR) {
            err = CHIP_FLASH_ERR_STAT_PROG_SIZE;
        } else if (status & CHIP_REGFLD_FLASH_SR_PGSERR) {
            err = CHIP_FLASH_ERR_STAT_PROG_SEQ;
        } else if (status & CHIP_REGFLD_FLASH_SR_MISSERR) {
            err = CHIP_FLASH_ERR_STAT_FASTPROG_DATAMISS;
        } else if (status & CHIP_REGFLD_FLASH_SR_FASTERR) {
            err = CHIP_FLASH_ERR_STAT_FASTPROG;
        } else if (status & CHIP_REGFLD_FLASH_SR_RDERR) {
            err = CHIP_FLASH_ERR_STAT_READ_PROT;
        } else if (status & CHIP_REGFLD_FLASH_SR_OPTVERR) {
            err = CHIP_FLASH_ERR_STAT_OPT_VALIDITY;
        }
    }
    return err;
}

CHIP_RAM_FUNC t_chip_flash_err chip_flash_check_err(void) {
    t_chip_flash_err err = flash_parse_status(CHIP_REGS_FLASH->SR);
    if (err != CHIP_FLASH_ERR_OK) {
        flash_clear_errs();
    }
    return  err;
}


static CHIP_RAM_FUNC void chip_flash_op_start_ram(uint32_t flags) {
    CHIP_REGS_FLASH->SR = CHIP_REGFLD_FLASH_SR_EOP | CHIP_REGFLD_FLASH_SR_OPERR;
    CHIP_REGS_FLASH->CR |= (flags | CHIP_REGFLD_FLASH_CR_EOPIE | CHIP_REGFLD_FLASH_CR_ERRIE);
}

static void chip_flash_op_start(uint32_t flags) {
    CHIP_REGS_FLASH->SR = CHIP_REGFLD_FLASH_SR_EOP | CHIP_REGFLD_FLASH_SR_OPERR;
    CHIP_REGS_FLASH->CR |= (flags | CHIP_REGFLD_FLASH_CR_EOPIE | CHIP_REGFLD_FLASH_CR_ERRIE);
}

t_chip_flash_err chip_flash_get_page_nums(uint32_t start_addr, uint32_t size,
                                          t_chip_flash_pagenum *pstart_page,
                                          t_chip_flash_pagenum *pend_page) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;

    if ((start_addr + size) > CHIP_FLASH_MEM_SIZE) {
        err = CHIP_FLASH_ERR_INVALID_ADDR;
    } else {
        unsigned start_page = start_addr/CHIP_FLASH_PAGE_SIZE;
        unsigned end_page = (start_addr + size - 1)/CHIP_FLASH_PAGE_SIZE;

        if (pstart_page)
            *pstart_page = start_page & 0x3FF;
        if (pend_page)
            *pend_page = end_page & 0x3FF;
    }
    return err;
}



static CHIP_RAM_FUNC t_chip_flash_err flash_wait_op_done_ram(void) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;

    /* не используем проверку времени, т.к. функции задержки реализованы во
     * flash, к которой до завершения операции не можем обращаться, а во вторых
     * если и отслеживать таймаут через функции RAM, то в случае его истчения
     * не понятно, что делать, т.к. основной код во flash, в которую нельзя возвращаться */
    while (!(CHIP_REGS_FLASH->SR & (CHIP_REGFLD_FLASH_SR_EOP | CHIP_REGFLD_FLASH_SR_OPERR))) {
        continue;
    }

    uint32_t status = CHIP_REGS_FLASH->SR;
    if (status & CHIP_REGFLD_FLASH_SR_OPERR) {
        err = flash_parse_status(status);
        flash_clear_errs();
    }

    return  err;
}


static t_chip_flash_err flash_wait_op_done_us(unsigned tout_us) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;

    while (!(CHIP_REGS_FLASH->SR & (CHIP_REGFLD_FLASH_SR_EOP | CHIP_REGFLD_FLASH_SR_OPERR))
           && (tout_us > 0)) {
        /* задержка на 1 мкс. не используем ltimer, чтобы не задействовать случайно
         * код из flash */
        chip_delay_clk(CHIP_CLK_CPU_FREQ/CHIP_MHZ(1));
        --tout_us;
    }

    uint32_t status = CHIP_REGS_FLASH->SR;
    if (status & CHIP_REGFLD_FLASH_SR_OPERR) {
        err = flash_parse_status(status);
        flash_clear_errs();
    } else if (!(status & CHIP_REGFLD_FLASH_SR_EOP)) {
        err = CHIP_FLASH_ERR_WAIT_OPRDY_TOUT;
    }

    return  err;
}


CHIP_RAM_FUNC t_chip_flash_err chip_flash_erase_page_start(t_chip_flash_pagenum sector) {
    t_chip_flash_err err = flash_check_unlocked();
    if (err == CHIP_FLASH_ERR_OK)
        err = chip_flash_check_rdy();
    if (err == CHIP_FLASH_ERR_OK) {
        flash_clear_errs();

        /** @todo multibank: выбор банка с помощью BKER */
        CHIP_REGS_FLASH->CR = (CHIP_REGS_FLASH->CR &~CHIP_REGFLD_FLASH_CR_PNB)
                                   | CHIP_REGFLD_FLASH_CR_PER
                                   | LBITFIELD_SET(CHIP_REGFLD_FLASH_CR_PNB, sector)
                                   ;
        chip_flash_op_start_ram(CHIP_REGFLD_FLASH_CR_STRT);

        chip_dmb();
    }
    return err;
}

void chip_flash_erase_finish(void) {
    flash_clear_en();
}

static CHIP_RAM_FUNC t_chip_flash_err flash_page_erase(t_chip_flash_pagenum pg_num) {
    unsigned var;
    chip_irq_save(var);
    chip_irq_disable();

    t_chip_flash_err err = chip_flash_erase_page_start(pg_num);
    if (err == CHIP_FLASH_ERR_OK)
        err = flash_wait_op_done_ram();

    chip_irq_restore(var);
#if CHIP_FLASH_ROW_WR_ENABLED
    if (err == CHIP_FLASH_ERR_OK) {
        f_last_err_page = pg_num;
    }
#endif

    return err;
}



t_chip_flash_err chip_flash_erase(uint32_t start_addr, uint32_t size, void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    t_chip_flash_pagenum start_page, end_page;

    err = chip_flash_get_page_nums(start_addr, size, &start_page, &end_page);

    if (err == CHIP_FLASH_ERR_OK) {
        err = flash_check_unlocked();
    }

    if (err == CHIP_FLASH_ERR_OK) {
        flash_clear_en();

        for (unsigned pg_num = start_page; (pg_num <= end_page) && (err == CHIP_FLASH_ERR_OK); ++pg_num) {
            /* т.к. во время начала стирания, если стираемый сектор находится
             * в том же банке, из которого выполняется программа, произойдет
             * останов программы, то явно вызываем cb перед и после стиранием
             * для возможности сброса wdt() */
            if (cb_func)
                cb_func();

            err = flash_page_erase(pg_num);
        }
        chip_flash_erase_finish();
    }

    return err;
}



#if CHIP_FLASH_ROW_WR_ENABLED

static CHIP_RAM_FUNC t_chip_flash_err flash_write_row(const uint32_t *src, uint32_t *dest) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    /* запрет прерываний обеспечивает гарантию непревышения времени записи слов строки,
     * а также того, что не будут вызываны обработчики из FLASH */
    unsigned var;
    chip_irq_save(var);
    chip_irq_disable();

    chip_flash_op_start_ram(CHIP_REGFLD_FLASH_CR_FSTPG);


    chip_dsb();
    /* копирование 256 байт сделано явно на ams, чтобы избежать зависимости
     * от компилятора, т.к. на эту запись есть явные ограничения по времени
     * и последовательности команд */
    asm volatile (
                " .syntax unified\n"
       "movs r2, %0 \n\t"
       "movs r3, %1 \n\t"
       "movs r4, #32 \n\t"
       "loop: \n\t"
       "LDR  r1, [r3, #0] \n\t"
       "STR  r1, [r2, #0] \n\t"
       "LDR  r1, [r3, #4] \n\t"
       "STR  r1, [r2, #4] \n\t"
       "ADDS r2, #8 \n\t"
       "ADDS r3, #8 \n\t"
       "SUBS r4, #1 \n\t"
       "BNE loop \n\t"
       : : "l" (dest), "l" (src)
                : "r1", "r2", "r3", "r4", "memory" );
    chip_dsb();

    err = flash_wait_op_done_ram();
    if (err == CHIP_FLASH_ERR_OK) {
        err = chip_flash_check_err();
    }

    CHIP_REGS_FLASH->CR &= ~CHIP_REGFLD_FLASH_CR_FSTPG;
    chip_irq_restore(var);
    return err;
}

#endif

static CHIP_RAM_FUNC  t_chip_flash_err flash_write_word(const uint32_t *src, uint32_t *dest) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;

    unsigned var;
    chip_irq_save(var);
    chip_irq_disable();

    chip_flash_op_start_ram(CHIP_REGFLD_FLASH_CR_PG);

    chip_dmb();
    *dest++ = *src++;
    *dest++ = *src++;
    chip_dmb();

    err = flash_wait_op_done_ram();

    CHIP_REGS_FLASH->CR &= ~CHIP_REGFLD_FLASH_CR_PG;
    chip_irq_restore(var);
    return err;
}


t_chip_flash_err chip_flash_write(uint32_t start_addr, const void *data, uint32_t size, void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    if ((start_addr & (CHIP_FLASH_WRWORD_SIZE - 1)) != 0) {
        err = CHIP_FLASH_ERR_INVALID_ADDR;
    } else if ((size & (CHIP_FLASH_WRWORD_SIZE - 1)) != 0) {
        err = CHIP_FLASH_ERR_DATA_SIZE;
    }

    if (err == CHIP_FLASH_ERR_OK) {
        err = flash_check_unlocked();
    }

    if (err == CHIP_FLASH_ERR_OK) {
        uint32_t *dst_wrds = (uint32_t *)chip_flash_ptr(start_addr);
        const uint32_t *src_wrds = (const uint32_t *)data;
        unsigned rem_wrds_cnt = size/4;


        flash_clear_errs();

        while ((err == CHIP_FLASH_ERR_OK) && (rem_wrds_cnt > 0)) {
            unsigned wr_words = 0;
            /* в случае выравненной записи на строку можем использовать
             * быструю запись, иначе - по 64-битным словам */
#if CHIP_FLASH_ROW_WR_ENABLED
            if (((((intptr_t)dst_wrds) & (CHIP_FLASH_WRROW_SIZE - 1)) == 0)
                    && (rem_wrds_cnt >= CHIP_FLASH_WRROW_SIZE/4)) {
                /* Для быстрой записи возникает ошибка, если последнее стирание было
                 * для страницы, отличной от той, куда идет быстрая запись
                 * (например, если стиралось несколько страниц, то начать писать с первой нельзя).
                 * Поэтому приходится отслеживать номер последней стираемой страницы
                 * и при несовпадении выполнять стирание заново
                 * (выпполняется быстро, т.к. страница уже должна быть стерта) */
                t_chip_flash_pagenum pgnum;
                chip_flash_get_page_nums(chip_flash_offs(dst_wrds), CHIP_FLASH_WRROW_SIZE, &pgnum, NULL);
                if (pgnum != f_last_err_page) {
                    err = flash_page_erase(pgnum);
                    chip_flash_erase_finish();
                }

                if (err == CHIP_FLASH_ERR_OK)
                    err = flash_write_row(src_wrds, dst_wrds);

                wr_words = CHIP_FLASH_WRROW_SIZE/4;
            } else {
#endif
                err = flash_write_word(src_wrds, dst_wrds);
                wr_words = CHIP_FLASH_WRWORD_SIZE/4;
#if CHIP_FLASH_ROW_WR_ENABLED
            }
#endif
            if (cb_func)
                cb_func();

            src_wrds += wr_words;
            dst_wrds += wr_words;
            rem_wrds_cnt -= wr_words;
        }
    }

    return err;
}
#endif


void chip_flash_opts_unlock(void) {
    if (flash_check_unlocked() != CHIP_FLASH_ERR_OK)
        chip_flash_unlock();

    if (CHIP_REGS_FLASH->CR & CHIP_REGFLD_FLASH_CR_OPTLOCK) {
        CHIP_REGS_FLASH->OPTKEYR = CHIP_FLASH_OPT_KEY1;
        CHIP_REGS_FLASH->OPTKEYR = CHIP_FLASH_OPT_KEY2;
        chip_dmb();
    }
}

void chip_flash_opts_lock(void) {
    chip_flash_lock();
}

/* установка опций может вызываться до инициализации RAM, поэтому
 * не используем тут RAM-функций */
t_chip_flash_err chip_flash_opts_write(void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    if (CHIP_REGS_FLASH->CR & CHIP_REGFLD_FLASH_CR_OPTLOCK) {
        err = CHIP_FLASH_ERR_LOCKED;
    } else {
        flash_clear_errs();

        if (cb_func)
            cb_func();

        chip_flash_op_start(CHIP_REGFLD_FLASH_CR_OPTSTRT);
        err = flash_wait_op_done_us(CHIP_FLASH_WROPT_TIME_US);

        if (cb_func)
            cb_func();
    }

    return err;
}

static t_chip_flash_err flash_check_area_num(uint8_t area_num) {
    return area_num < CHIP_FLASH_PROT_AREA_COUNT ? CHIP_FLASH_ERR_OK : CHIP_FLASH_ERR_INVALID_AREA_NUM;
}

t_chip_flash_err chip_flash_opts_wrp_area_set_pages(uint8_t area_num, t_chip_flash_pagenum start_page, t_chip_flash_pagenum end_page) {
    t_chip_flash_err err = flash_check_area_num(area_num);
    if (err == CHIP_FLASH_ERR_OK) {
        /** @todo multibank */
        __IO uint32_t *pwrp = CHIP_FLASH_WRP_PREG(area_num);
        *pwrp = (*pwrp & ~(CHIP_REGFLD_FLASH_WRPXXR_WRPXX_STRT | CHIP_REGFLD_FLASH_WRPXXR_WRPXX_END))
                | LBITFIELD_SET(CHIP_REGFLD_FLASH_WRPXXR_WRPXX_STRT, start_page)
                | LBITFIELD_SET(CHIP_REGFLD_FLASH_WRPXXR_WRPXX_END, end_page)
                ;
    }
    return err;
}

t_chip_flash_err chip_flash_opts_wrp_area_get_pages(uint8_t area_num, t_chip_flash_pagenum *start_page, t_chip_flash_pagenum *end_page) {
    t_chip_flash_err err = flash_check_area_num(area_num);
    if (err == CHIP_FLASH_ERR_OK) {
        uint32_t wrp_val = *CHIP_FLASH_WRP_PREG(area_num);
        *start_page = LBITFIELD_GET(wrp_val, CHIP_REGFLD_FLASH_WRPXXR_WRPXX_STRT);
        *end_page = LBITFIELD_GET(wrp_val, CHIP_REGFLD_FLASH_WRPXXR_WRPXX_END);
    }
    return err;
}

t_chip_flash_err chip_flash_opts_wrp_area_is_protected(uint8_t area_num) {
    t_chip_flash_pagenum start_page, end_page;
    t_chip_flash_err err = chip_flash_opts_wrp_area_get_pages(area_num, &start_page, &end_page);
    if (err == CHIP_FLASH_ERR_OK) {
        if (start_page > end_page) {
            err = CHIP_FLASH_ERR_UNPROTECTED;
        }
    }
    return err;
}



t_chip_flash_err chip_flash_opts_wrp_area_set(uint8_t area_num, uint32_t start_addr, uint32_t size) {
    t_chip_flash_pagenum start_page, end_page;
    t_chip_flash_err err = chip_flash_get_page_nums(start_addr, size, &start_page, &end_page);
    if (err == CHIP_FLASH_ERR_OK) {
        err = chip_flash_opts_wrp_area_set_pages(area_num, start_page, end_page);
    }
    return err;
}

t_chip_flash_err chip_flash_opts_wrp_area_clear(uint8_t area_num) {
    return chip_flash_opts_wrp_area_set_pages(area_num, 0x3FF, 0);
}


t_chip_flash_err chip_flash_opts_check_wr_protected(uint32_t start_addr, uint32_t size) {
    t_chip_flash_pagenum start_page, end_page;
    t_chip_flash_err err = chip_flash_get_page_nums(start_addr, size, &start_page, &end_page);    
    if (err == CHIP_FLASH_ERR_OK) {
        int protected = 0;
        /** @note не рассматривается вариант, когда две области защиты идут друг за другом и мы проверяем
         *  часть памяти, которая принадлежит обоим областям и ими и поарывается */
        for (uint8_t area_num = 0; (area_num < CHIP_FLASH_PROT_AREA_COUNT) && !protected; ++area_num) {
            if (chip_flash_opts_wrp_area_is_protected(area_num) == CHIP_FLASH_ERR_OK) {
                t_chip_flash_pagenum wrp_start_page, wrp_end_page;
                chip_flash_opts_wrp_area_get_pages(area_num, &wrp_start_page, &wrp_end_page);
                if ((wrp_start_page <= start_page) && (wrp_end_page >= end_page)) {
                    protected = 1;
                }
            }
        }

        if (!protected) {
            err = CHIP_FLASH_ERR_UNPROTECTED;
        }
    }
    return err;
}

/* получить текущий уровень защиты всей памяти от постороннего чтения */
t_chip_flash_rdp_level chip_flash_opts_get_rdp_level(void) {
    uint8_t rdp_code = LBITFIELD_GET(CHIP_REGS_FLASH->OPTR, CHIP_REGFLD_FLASH_OPTR_RDP);
    return rdp_code == CHIP_FLASH_RDP_L0_CODE ? CHIP_FLASH_RDP_LEVEL0 :
           rdp_code == CHIP_FLASH_RDP_L2_CODE ? CHIP_FLASH_RDP_LEVEL2 : CHIP_FLASH_RDP_LEVEL1;
}

/* установить уровень защиты всей памяти от постороннего чтения */
t_chip_flash_err chip_flash_opts_set_rdp_level(t_chip_flash_rdp_level lvl) {
    uint8_t rdp_code = lvl == CHIP_FLASH_RDP_LEVEL0 ? CHIP_FLASH_RDP_L0_CODE :
                       lvl == CHIP_FLASH_RDP_LEVEL2 ? CHIP_FLASH_RDP_L2_CODE : 0;
    chip_flash_opts_unlock();
    LBITFIELD_UPD(CHIP_REGS_FLASH->OPTR, CHIP_REGFLD_FLASH_OPTR_RDP, rdp_code);

    return CHIP_FLASH_ERR_OK;
}
