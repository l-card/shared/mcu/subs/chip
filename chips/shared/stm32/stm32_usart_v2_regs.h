#ifndef STM32_USART_GEN_REGS_H
#define STM32_USART_GEN_REGS_H

#include "chip_ioreg_defs.h"

/* параметры, которые должны быть определены до включения данного файла в зависимости от возможностей чипа
   CHIP_SUPPORT_USART_PRESC         - поддержка регистра для задания предделителя частоты
   CHIP_SUPPORT_USART_FIFO          - поддержка FIFO
   CHIP_SUPPORT_USART_SYNC_SLAVE    - поддержка USART в режиме синхронного slave
   CHIP_SUPPORT_USART_TCBGT_FLAG    - поддержка флага Transmit Complete Before Guard Time
   */

/* Universal Synchronous Asynchronous Receiver Transmitter */
typedef struct {
    __IO uint32_t CR1;    /* USART Control register 1,                 Address offset: 0x00 */
    __IO uint32_t CR2;    /* USART Control register 2,                 Address offset: 0x04 */
    __IO uint32_t CR3;    /* USART Control register 3,                 Address offset: 0x08 */
    __IO uint32_t BRR;    /* USART Baud rate register,                 Address offset: 0x0C */
    __IO uint32_t GTPR;   /* USART Guard time and prescaler register,  Address offset: 0x10 */
    __IO uint32_t RTOR;   /* USART Receiver Time Out register,         Address offset: 0x14 */
    __IO uint32_t RQR;    /* USART Request register,                   Address offset: 0x18 */
    __IO uint32_t ISR;    /* USART Interrupt and status register,      Address offset: 0x1C */
    __IO uint32_t ICR;    /* USART Interrupt flag Clear register,      Address offset: 0x20 */
    __IO uint32_t RDR;    /* USART Receive Data register,              Address offset: 0x24 */
    __IO uint32_t TDR;    /* USART Transmit Data register,             Address offset: 0x28 */
#if CHIP_SUPPORT_USART_PRESC
    __IO uint32_t PRESC;  /* USART clock Prescaler register,           Address offset: 0x2C */
#endif
} CHIP_REGS_USART_T;


/*********** USART Control register 1 (USART_CR1) *****************************/
#define CHIP_REGFLD_USART_CR1_UE                    (0x00000001UL <<  0U) /* (RW) USART enable */
#define CHIP_REGFLD_USART_CR1_UESM                  (0x00000001UL <<  1U) /* (RW) USART enable in Stop mode */
#define CHIP_REGFLD_USART_CR1_RE                    (0x00000001UL <<  2U) /* (RW) Receiver enable */
#define CHIP_REGFLD_USART_CR1_TE                    (0x00000001UL <<  3U) /* (RW) Transmitter enable */
#define CHIP_REGFLD_USART_CR1_IDLE_IE               (0x00000001UL <<  4U) /* (RW) IDLE interrupt enable */
#define CHIP_REGFLD_USART_CR1_RXNE_IE               (0x00000001UL <<  5U) /* (RW) RXNE interrupt enable */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_CR1_RXFNE_IE              (0x00000001UL <<  5U) /* (RW) RX FIFO Not Empty Interrupt Enable */
#endif
#define CHIP_REGFLD_USART_CR1_TC_IE                 (0x00000001UL <<  6U) /* (RW) Transmission complete interrupt enable */
#define CHIP_REGFLD_USART_CR1_TXE_IE                (0x00000001UL <<  7U) /* (RW) TXE interrupt enable */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_CR1_TXFNFIE               (0x00000001UL <<  7U) /* (RW) TX FIFO Not Full Interrupt Enable */
#endif
#define CHIP_REGFLD_USART_CR1_PE_IE                 (0x00000001UL <<  8U) /* (RW) PE interrupt enable */
#define CHIP_REGFLD_USART_CR1_PS                    (0x00000001UL <<  9U) /* (RW) Parity selection */
#define CHIP_REGFLD_USART_CR1_PCE                   (0x00000001UL << 10U) /* (RW) Parity control enable */
#define CHIP_REGFLD_USART_CR1_WAKE                  (0x00000001UL << 11U) /* (RW) Receiver wakeup method */
#define CHIP_REGFLD_USART_CR1_M0                    (0x00000001UL << 12U) /* (RW) Word length*/
#define CHIP_REGFLD_USART_CR1_MME                   (0x00000001UL << 13U) /* (RW) Mute mode enable */
#define CHIP_REGFLD_USART_CR1_CM_IE                 (0x00000001UL << 14U) /* (RW) Character match interrupt enable */
#define CHIP_REGFLD_USART_CR1_OVER8                 (0x00000001UL << 15U) /* (RW) Oversampling mode (16 or 8)*/
#define CHIP_REGFLD_USART_CR1_DEDT                  (0x0000001FUL << 16U) /* (RW) Driver Enable de-assertion time */
#define CHIP_REGFLD_USART_CR1_DEAT                  (0x0000001FUL << 21U) /* (RW) Driver Enable assertion time */
#define CHIP_REGFLD_USART_CR1_RTO_IE                (0x00000001UL << 26U) /* (RW) Receiver timeout interrupt enable */
#define CHIP_REGFLD_USART_CR1_EOB_IE                (0x00000001UL << 27U) /* (RW) End of Block interrupt enable */
#define CHIP_REGFLD_USART_CR1_M1                    (0x00000001UL << 28U) /* (RW) Word length */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_CR1_FIFO_EN               (0x00000001UL << 29U) /* (RW) FIFO mode enable */
#define CHIP_REGFLD_USART_CR1_TXFE_IE               (0x00000001UL << 30U) /* (RW) TXFIFO empty interrupt enable */
#define CHIP_REGFLD_USART_CR1_RXFF_IE               (0x00000001UL << 30U) /* (RW) RXFIFO Full interrupt enable */
#endif

#define CHIP_REGFLDVAL_USART_CR1_PS_EVEN            0
#define CHIP_REGFLDVAL_USART_CR1_PS_ODD             1

#define CHIP_REGFLDVAL_USART_CR1_WAKE_IDLE_LINE     0
#define CHIP_REGFLDVAL_USART_CR1_WAKE_ADDR_MARK     1

/*********** USART control register 2 (USART_CR2) *****************************/
#if CHIP_SUPPORT_USART_SYNC_SLAVE
#define CHIP_REGFLD_USART_CR2_SLV_EN                (0x00000001UL <<  0U) /* (RW) Synchronous Slave mode Enable */
#define CHIP_REGFLD_USART_CR2_DIS_NSS               (0x00000001UL <<  3U) /* (RW) Negative Slave Select (NSS) pin management */
#endif
#define CHIP_REGFLD_USART_CR2_ADDM7                 (0x00000001UL <<  4U) /* (RW) 7-bit or 4-bit Address Detection */
#define CHIP_REGFLD_USART_CR2_LBDL                  (0x00000001UL <<  5U) /* (RW) LIN Break Detection Length */
#define CHIP_REGFLD_USART_CR2_LBD_IE                (0x00000001UL <<  6U) /* (RW) LIN Break Detection Interrupt Enable */
#define CHIP_REGFLD_USART_CR2_LBCL                  (0x00000001UL <<  8U) /* (RW) Last Bit Clock pulse */
#define CHIP_REGFLD_USART_CR2_CPHA                  (0x00000001UL <<  9U) /* (RW) Clock Phase */
#define CHIP_REGFLD_USART_CR2_CPOL                  (0x00000001UL << 10U) /* (RW) Clock Polarity */
#define CHIP_REGFLD_USART_CR2_CLK_EN                (0x00000001UL << 11U) /* (RW) Clock Enable */
#define CHIP_REGFLD_USART_CR2_STOP                  (0x00000003UL << 12U) /* (RW) STOP bits */
#define CHIP_REGFLD_USART_CR2_LIN_EN                (0x00000001UL << 14U) /* (RW) LIN mode enable */
#define CHIP_REGFLD_USART_CR2_SWAP                  (0x00000001UL << 15U) /* (RW) SWAP TX/RX pins */
#define CHIP_REGFLD_USART_CR2_RX_INV                (0x00000001UL << 16U) /* (RW) RX pin active level inversion */
#define CHIP_REGFLD_USART_CR2_TX_INV                (0x00000001UL << 17U) /* (RW) TX pin active level inversion */
#define CHIP_REGFLD_USART_CR2_DATA_INV              (0x00000001UL << 18U) /* (RW) Binary data inversion */
#define CHIP_REGFLD_USART_CR2_MSB_FIRST             (0x00000001UL << 19U) /* (RW) Most Significant Bit First */
#define CHIP_REGFLD_USART_CR2_ABR_EN                (0x00000001UL << 20U) /* (RW) Auto Baud-Rate Enable*/
#define CHIP_REGFLD_USART_CR2_ABR_MODE              (0x00000003UL << 21U) /* (RW) Auto Baud-Rate Mode */
#define CHIP_REGFLD_USART_CR2_RTO_EN                (0x00000001UL << 23U) /* (RW) Receiver Time-Out enable */
#define CHIP_REGFLD_USART_CR2_ADD                   (0x000000FFUL << 24U) /* (RW) Address of the USART node */


#define CHIP_REGFLDVAL_USART_CR2_STOP_1             0 /* 1 stop bits */
#define CHIP_REGFLDVAL_USART_CR2_STOP_0_5           1 /* 0.5 stop bits */
#define CHIP_REGFLDVAL_USART_CR2_STOP_2             2 /* 2 stop bits */
#define CHIP_REGFLDVAL_USART_CR2_STOP_1_5           3 /* 1.5 stop bits */

#define CHIP_REGFLDVAL_USART_CR2_ABR_MODE_SB        0 /* Measurement of the start bit is used to detect the baud rate. */
#define CHIP_REGFLDVAL_USART_CR2_ABR_MODE_F2F       1 /* Falling edge to falling edge measurement (the received frame must start with a single bit = 1 -> Frame = Start10xxxxxx) */
#define CHIP_REGFLDVAL_USART_CR2_ABR_MODE_FR_7F     2 /* 0x7F frame detection */
#define CHIP_REGFLDVAL_USART_CR2_ABR_MODE_FR_55     3 /* 0x55 frame detection */

/*********** USART control register 3 (USART_CR3) *****************************/
#define CHIP_REGFLD_USART_CR3_E_IE                  (0x00000001UL <<  0U) /* (RW) Error Interrupt Enable */
#define CHIP_REGFLD_USART_CR3_IR_EN                 (0x00000001UL <<  1U) /* (RW) IrDA mode Enable */
#define CHIP_REGFLD_USART_CR3_IR_LP                 (0x00000001UL <<  2U) /* (RW) IrDA Low-Power */
#define CHIP_REGFLD_USART_CR3_HDSEL                 (0x00000001UL <<  3U) /* (RW) Half-Duplex Selection */
#define CHIP_REGFLD_USART_CR3_NACK                  (0x00000001UL <<  4U) /* (RW) SmartCard NACK enable */
#define CHIP_REGFLD_USART_CR3_SC_EN                 (0x00000001UL <<  5U) /* (RW) SmartCard mode enable */
#define CHIP_REGFLD_USART_CR3_DMAR                  (0x00000001UL <<  6U) /* (RW) DMA Enable Receiver */
#define CHIP_REGFLD_USART_CR3_DMAT                  (0x00000001UL <<  7U) /* (RW) DMA Enable Transmitter */
#define CHIP_REGFLD_USART_CR3_RTS_EN                (0x00000001UL <<  8U) /* (RW) RTS Enable */
#define CHIP_REGFLD_USART_CR3_CTS_EN                (0x00000001UL <<  9U) /* (RW) CTS Enable */
#define CHIP_REGFLD_USART_CR3_CTS_IE                (0x00000001UL << 10U) /* (RW) CTS Interrupt Enable */
#define CHIP_REGFLD_USART_CR3_ONEBIT                (0x00000001UL << 11U) /* (RW) One sample bit method enable */
#define CHIP_REGFLD_USART_CR3_OVR_DIS               (0x00000001UL << 12U) /* (RW) Overrun Disable */
#define CHIP_REGFLD_USART_CR3_DDRE                  (0x00000001UL << 13U) /* (RW) DMA Disable on Reception Error */
#define CHIP_REGFLD_USART_CR3_DEM                   (0x00000001UL << 14U) /* (RW) Driver Enable Mode */
#define CHIP_REGFLD_USART_CR3_DEP                   (0x00000001UL << 15U) /* (RW) Driver Enable Polarity Selection */
#define CHIP_REGFLD_USART_CR3_SC_ARCNT              (0x00000007UL << 17U) /* (RW) SmartCard Auto-Retry Count */
#define CHIP_REGFLD_USART_CR3_WUS                   (0x00000003UL << 20U) /* (RW) Wake UP Interrupt Flag Selection */
#define CHIP_REGFLD_USART_CR3_WUF_IE                (0x00000001UL << 22U) /* (RW) Wake Up Interrupt Enable */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_CR3_TXFT_IE               (0x00000001UL << 23U) /* (RW) TXFIFO threshold interrupt enable */
#endif
#if CHIP_SUPPORT_USART_TCBGT_FLAG
#define CHIP_REGFLD_USART_CR3_TCBGT_IE              (0x00000001UL << 24U) /* (RW) Transmission Complete before guard time, interrupt enable */
#endif
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_CR3_RXFT_CFG              (0x00000007UL << 25U) /* (RW) Receive FIFO threshold configuration */
#define CHIP_REGFLD_USART_CR3_RXFT_IE               (0x00000001UL << 28U) /* (RW) RXFIFO threshold interrupt enable */
#define CHIP_REGFLD_USART_CR3_TXFT_CFG              (0x00000007UL << 29U) /* (RW) TXFIFO threshold configuration */
#endif

/*********** USART baud rate register (USART_BRR) *****************************/
#define CHIP_REGFLD_USART_BRR_USART_BRR             (0x0000FFFFUL <<  0U) /* (RW) USART  Baud rate register [15:0] */
#define CHIP_REGFLD_USART_BRR_LPUART_BRR            (0x000FFFFFUL <<  0U) /* (RW) LPUART Baud rate register [19:0] */

/*********** USART guard time and prescaler register (USART_GTPR) *************/
#define CHIP_REGFLD_USART_GTPR_PSC                  (0x000000FFUL <<  0U) /* (RW) Prescaler value */
#define CHIP_REGFLD_USART_GTPR_GT                   (0x000000FFUL <<  8U) /* (RW) Guard time value */

/*********** USART receiver timeout register (USART_RTOR) *********************/
#define CHIP_REGFLD_USART_RTOR_RTO                  (0x00FFFFFFUL <<  0U) /* (RW) Receiver Time Out Value */
#define CHIP_REGFLD_USART_RTOR_BL_EN                (0x000000FFUL << 24U) /* (RW) Block Length */

/*********** USART request register (USART_RQR) *******************************/
#define CHIP_REGFLD_USART_RQR_ABRRQ                 (0x00000001UL <<  0U) /* (W1) Auto-Baud Rate Request */
#define CHIP_REGFLD_USART_RQR_SBKRQ                 (0x00000001UL <<  1U) /* (W1) Send Break Request */
#define CHIP_REGFLD_USART_RQR_MMRQ                  (0x00000001UL <<  2U) /* (W1) Mute Mode Request */
#define CHIP_REGFLD_USART_RQR_RXFRQ                 (0x00000001UL <<  3U) /* (W1) Receive Data flush Request */
#define CHIP_REGFLD_USART_RQR_TXFRQ                 (0x00000001UL <<  4U) /* (W1) Transmit data flush Request */

/*********** USART interrupt and status register (USART_ISR)  *****************/
#define CHIP_REGFLD_USART_ISR_PE                    (0x00000001UL <<  0U) /* (RO) Parity Error */
#define CHIP_REGFLD_USART_ISR_FE                    (0x00000001UL <<  1U) /* (RO) Framing Error */
#define CHIP_REGFLD_USART_ISR_NE                    (0x00000001UL <<  2U) /* (RO) Noise detected Flag */
#define CHIP_REGFLD_USART_ISR_ORE                   (0x00000001UL <<  3U) /* (RO) OverRun Error */
#define CHIP_REGFLD_USART_ISR_IDLE                  (0x00000001UL <<  4U) /* (RO) IDLE line detected */
#define CHIP_REGFLD_USART_ISR_RXNE                  (0x00000001UL <<  5U) /* (RO) Read Data Register Not Empty (no fifo) */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_ISR_RXFNE                 (0x00000001UL <<  5U) /* (RO) RX FIFO Not Empty */
#endif
#define CHIP_REGFLD_USART_ISR_TC                    (0x00000001UL <<  6U) /* (RO) Transmission Complete */
#define CHIP_REGFLD_USART_ISR_TXE                   (0x00000001UL <<  7U) /* (RO) Transmit Data Register Empty Flag (no fifo) */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_ISR_TXFNF                 (0x00000001UL <<  7U) /* (RO) TX FIFO Not Full Flag */
#endif
#define CHIP_REGFLD_USART_ISR_LBDF                  (0x00000001UL <<  8U) /* (RO) LIN Break Detection Flag */
#define CHIP_REGFLD_USART_ISR_CTSIF                 (0x00000001UL <<  9U) /* (RO) CTS interrupt flag */
#define CHIP_REGFLD_USART_ISR_CTS                   (0x00000001UL << 10U) /* (RO) CTS flag */
#define CHIP_REGFLD_USART_ISR_RTOF                  (0x00000001UL << 11U) /* (RO) Receiver Time Out */
#define CHIP_REGFLD_USART_ISR_EOBF                  (0x00000001UL << 12U) /* (RO) End Of Block Flag */
#if CHIP_SUPPORT_USART_SYNC_SLAVE
#define CHIP_REGFLD_USART_ISR_UDR                   (0x00000001UL << 13U) /* (RO) SPI slave underrun error flag */
#endif
#define CHIP_REGFLD_USART_ISR_ABRE                  (0x00000001UL << 14U) /* (RO) Auto-Baud Rate Error */
#define CHIP_REGFLD_USART_ISR_ABRF                  (0x00000001UL << 15U) /* (RO) Auto-Baud Rate Flag */
#define CHIP_REGFLD_USART_ISR_BUSY                  (0x00000001UL << 16U) /* (RO) Busy Flag */
#define CHIP_REGFLD_USART_ISR_CMF                   (0x00000001UL << 17U) /* (RO) Character Match Flag */
#define CHIP_REGFLD_USART_ISR_SBKF                  (0x00000001UL << 18U) /* (RO) Send Break Flag */
#define CHIP_REGFLD_USART_ISR_RWU                   (0x00000001UL << 19U) /* (RO) Receive Wake Up from mute mode Flag */
#define CHIP_REGFLD_USART_ISR_WUF                   (0x00000001UL << 20U) /* (RO) Wake Up from stop mode Flag */
#define CHIP_REGFLD_USART_ISR_TE_ACK                (0x00000001UL << 21U) /* (RO) Transmit Enable Acknowledge Flag */
#define CHIP_REGFLD_USART_ISR_RE_ACK                (0x00000001UL << 22U) /* (RO) Receive Enable Acknowledge Flag */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_ISR_TXFE                  (0x00000001UL << 23U) /* (RO) TXFIFO Empty */
#define CHIP_REGFLD_USART_ISR_RXFF                  (0x00000001UL << 24U) /* (RO) RXFIFO Full Flag */
#endif
#if CHIP_SUPPORT_USART_TCBGT_FLAG
#define CHIP_REGFLD_USART_ISR_TCBGT                 (0x00000001UL << 25U) /* (RO) Transmission complete before guard time Flag */
#endif
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_ISR_RXFT                  (0x00000001UL << 26U) /* (RO) RXFIFO threshold Flag */
#define CHIP_REGFLD_USART_ISR_TXFT                  (0x00000001UL << 27U) /* (RO) TXFIFO threshold Flag */
#endif

/*********** USART interrupt flag clear register (USART_ICR) ******************/
#define CHIP_REGFLD_USART_ICR_PE_CF                 (0x00000001UL <<  0U) /* (W1) Parity Error Clear Flag */
#define CHIP_REGFLD_USART_ICR_FE_CF                 (0x00000001UL <<  1U) /* (W1) Framing Error Clear Flag */
#define CHIP_REGFLD_USART_ICR_NE_CF                 (0x00000001UL <<  2U) /* (W1) Noise detected Clear Flag */
#define CHIP_REGFLD_USART_ICR_ORE_CF                (0x00000001UL <<  3U) /* (W1) OverRun Error Clear Flag */
#define CHIP_REGFLD_USART_ICR_IDLE_CF               (0x00000001UL <<  4U) /* (W1) IDLE line detected Clear Flag */
#if CHIP_SUPPORT_USART_FIFO
#define CHIP_REGFLD_USART_ICR_TXFE_CF               (0x00000001UL <<  5U) /* (W1) TXFIFO empty clear flag */
#endif
#define CHIP_REGFLD_USART_ICR_TC_CF                 (0x00000001UL <<  6U) /* (W1) Transmission Complete Clear Flag */
#if CHIP_SUPPORT_USART_TCBGT_FLAG
#define CHIP_REGFLD_USART_ICR_TCBGT_CF              (0x00000001UL <<  7U) /* (W1) Transmission complete before guard time Clear Flag */
#endif
#define CHIP_REGFLD_USART_ICR_LBD_CF                (0x00000001UL <<  8U) /* (W1) LIN Break Detection Clear Flag */
#define CHIP_REGFLD_USART_ICR_CTS_CF                (0x00000001UL <<  9U) /* (W1) CTS Interrupt Clear Flag */
#define CHIP_REGFLD_USART_ICR_RTO_CF                (0x00000001UL << 11U) /* (W1) Receiver Time Out Clear Flag */
#define CHIP_REGFLD_USART_ICR_EOB_CF                (0x00000001UL << 12U) /* (W1) End Of Block Clear Flag */
#if CHIP_SUPPORT_USART_SYNC_SLAVE
#define CHIP_REGFLD_USART_ICR_UDR_CF                (0x00000001UL << 13U) /* (W1) SPI slave underrun clear flag */
#endif
#define CHIP_REGFLD_USART_ICR_CM_CF                 (0x00000001UL << 17U) /* (W1) Character Match Clear Flag */
#define CHIP_REGFLD_USART_ICR_WU_CF                 (0x00000001UL << 20U) /* (W1) Wake Up from stop mode Clear Flag */

/*********** USART receive data register (USART_RDR) **************************/
#define CHIP_REGFLD_USART_RDR_RDR                   (0x000001FFUL <<  0U) /* (RO) Receive Data value */

/*********** USART transmit data register (USART_TDR) *************************/
#define CHIP_REGFLD_USART_TDR_TDR                   (0x000001FFUL <<  0U) /* (RW) Transmit Data value */

/*********** USART prescaler register (USART_PRESC) **************************/
#if CHIP_SUPPORT_USART_PRESC
#define CHIP_REGFLD_USART_PRESC_PRESCALER           (0x0000000FUL <<  0U) /* (RW) Clock prescaler */
#endif




#endif // STM32_USART_GEN_REGS_H
