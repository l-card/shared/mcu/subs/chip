#ifndef CHIP_CLK_DEFS_H
#define CHIP_CLK_DEFS_H


#include "chip_std_defs.h"



/* исходное значение частоты HSI */
#define CHIP_CLK_HSI_SRC_FREQ       CHIP_MHZ(48)
#define CHIP_CLK_HSI_DIV_FREQ       (CHIP_CLK_HSI_SRC_FREQ/6)
#define CHIP_CLK_HSI_PLLIN_FREQ     (CHIP_CLK_HSI_DIV_FREQ/2)


#define CHIP_CLK_CPU_INITIAL_FREQ   CHIP_CLK_HSI_DIV_FREQ


#define CHIP_CLK_PLL_RANGE_LVL       CHIP_MHZ(72)
#define CHIP_CLK_PLL_AUTOSTEP_LVL    CHIP_MHZ(108)



/* Номера идентификаторов различных клоков, которые могут использоваться в качестве
 * источников.
 * Номера используются исключительно для уникальной идентификации
 * для сравнения настроек, сами номера должны  быть уникальные,
 * но не связаны с какими либо значениеми самого контроллера.
 * В настройках источников сигнала используются значения без суффикса _ID,
 * а уже по этомим значениям с помощью макросов CHIP_CLK_ID_VAL получается
 * ID из таблицы. Это позволяет также по определению получить параметры выбранной
 * частоты с помощью CHIP_CLK_ID_EN() и CHIP_CLK_ID_FREQ() */
#define CHIP_CLK_HSE_ID             1
#define CHIP_CLK_LSE_ID             2
#define CHIP_CLK_HSI_ID             3
#define CHIP_CLK_HSI_DIV_ID         4
#define CHIP_CLK_HSI_PLLIN_ID       5
#define CHIP_CLK_HSI_SYSIN_ID       6
#define CHIP_CLK_HSE_PLLIN_ID       7
#define CHIP_CLK_PLL_ID             8



#endif // CHIP_CLK_DEFS_H
