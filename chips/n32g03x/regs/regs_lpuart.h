#ifndef CHIP_N32G03X_REGS_LPUART_H
#define CHIP_N32G03X_REGS_LPUART_H

#include <stdint.h>

typedef struct {
    __IO uint16_t STS;      /* 0x00: LPUART Status Register */
    uint16_t RESERVED0;
    __IO uint8_t INTEN;     /* 0x04: LPUART Interrupt Enable Register */
    uint8_t RESERVED1;
    uint16_t RESERVED2;
    __IO uint16_t CTRL;     /* 0x08: LPUART Control Register */
    uint16_t RESERVED3;
    __IO uint16_t BRCFG1;   /* 0x0C: LPUART Baud Rate Configuration Register 1 */
    uint16_t RESERVED4;
    __IO uint8_t DAT;       /* 0x10: LPUART Data Register */
    uint8_t RESERVED5;
    uint16_t RESERVED6;
    __IO uint8_t BRCFG2;    /* 0x14: LPUART Baud Rate Configuration Register 2 */
    uint8_t RESERVED7;
    uint16_t RESERVED8;
    __IO uint32_t WUDAT;    /* 0x18: LPUART Wakeup Data Register */
}  CHIP_REGS_LPUART_T;

#define CHIP_REGS_LPUART1        ((CHIP_REGS_LPUART_T *)CHIP_MEMRGN_ADDR_PERIPH_LPUART1)
#if CHIP_DEV_LPUART_CNT >= 2
#define CHIP_REGS_LPUART2        ((CHIP_REGS_LPUART_T *)CHIP_MEMRGN_ADDR_PERIPH_LPUART2)
#endif

#define CHIP_REGS_LPTIM         ((CHIP_REGS_LPTIM_T *)CHIP_MEMRGN_ADDR_PERIPH_LPTIM)

/*********** LPUART Status Register (LPUART_STS)  *****************************/
#define CHIP_REGFLD_LPUART_STS_NF                   (0x00000001UL <<  8U)     /* (RW1C) Noise detected flag */
#define CHIP_REGFLD_LPUART_STS_WUF                  (0x00000001UL <<  7U)     /* (RW1C) Wakeup from Stop mode flag */
#define CHIP_REGFLD_LPUART_STS_CTS                  (0x00000001UL <<  6U)     /* (RO)   CTS signal (hardware flow control) flag */
#define CHIP_REGFLD_LPUART_STS_FIFO_NE              (0x00000001UL <<  5U)     /* (RW1C) Receive FIFO non-empty flag */
#define CHIP_REGFLD_LPUART_STS_FIFO_HF              (0x00000001UL <<  4U)     /* (RW1C) Receive FIFO half full flag */
#define CHIP_REGFLD_LPUART_STS_FIFO_FU              (0x00000001UL <<  3U)     /* (RW1C) Receive FIFO full flag */
#define CHIP_REGFLD_LPUART_STS_FIFO_OV              (0x00000001UL <<  2U)     /* (RW1C) Receive FIFO overflow flag */
#define CHIP_REGFLD_LPUART_STS_TXC                  (0x00000001UL <<  1U)     /* (RW1C) TX complete flag */
#define CHIP_REGFLD_LPUART_STS_PE                   (0x00000001UL <<  0U)     /* (RW1C) Parity check error flag. */
#define CHIP_REGMSK_LPUART_STS_RESERVED             (0xFFFFFE00UL)

/*********** LPUART Interrupt Enable Register (LPUART_INTEN)  *****************/
#define CHIP_REGFLD_LPUART_INTEN_WUF                (0x00000001UL <<  6U)     /* (RW)   Wake-up interrupt enable */
#define CHIP_REGFLD_LPUART_INTEN_FIFO_NE            (0x00000001UL <<  5U)     /* (RW)   Receive FIFO not empty interrupt enable */
#define CHIP_REGFLD_LPUART_INTEN_FIFO_HF            (0x00000001UL <<  4U)     /* (RW)   Receive FIFO half full interrupt enable */
#define CHIP_REGFLD_LPUART_INTEN_FIFO_FU            (0x00000001UL <<  3U)     /* (RW)   Receive FIFO full interrupt enable */
#define CHIP_REGFLD_LPUART_INTEN_FIFO_OV            (0x00000001UL <<  2U)     /* (RW)   Receive FIFO overflow interrupt enable */
#define CHIP_REGFLD_LPUART_INTEN_TX                 (0x00000001UL <<  1U)     /* (RW)   TX end interrupt enable */
#define CHIP_REGFLD_LPUART_INTEN_PE                 (0x00000001UL <<  0U)     /* (RW)   Parity check error interrupt enable */
#define CHIP_REGMSK_LPUART_INTEN_RESERVED           (0xFFFFFF80UL)

/*********** LPUART Control Register (LPUART_CTRL)  ***************************/
#define CHIP_REGFLD_LPUART_CTRL_SMP_CNT             (0x00000001UL << 14U)     /* (RW)   Sampling method (0 - 3 sample, DIV>=10, 1 - 1 sample) */
#define CHIP_REGFLD_LPUART_CTRL_WU_SEL              (0x00000003UL << 12U)     /* (RW)   Wakeup event selection */
#define CHIP_REGFLD_LPUART_CTRL_CTS_EN              (0x00000001UL << 11U)     /* (RW)   CTS hardware flow control enable */
#define CHIP_REGFLD_LPUART_CTRL_RTS_EN              (0x00000001UL << 10U)     /* (RW)   RTS hardware flow control enable */
#define CHIP_REGFLD_LPUART_CTRL_RTS_TH_SEL          (0x00000003UL <<  8U)     /* (RW)   RTS threshold selection */
#define CHIP_REGFLD_LPUART_CTRL_WU_STP              (0x00000001UL <<  7U)     /* (RW)   LPUART wake-up from STOP mode enable */
#define CHIP_REGFLD_LPUART_CTRL_DMA_RX_EN           (0x00000001UL <<  6U)     /* (RW)   DMA RX Request Enable */
#define CHIP_REGFLD_LPUART_CTRL_DMA_TX_EN           (0x00000001UL <<  5U)     /* (RW)   DMA TX Request Enable */
#define CHIP_REGFLD_LPUART_CTRL_LOOPBACK            (0x00000001UL <<  4U)     /* (RW)   Loopback self-test */
#define CHIP_REGFLD_LPUART_CTRL_PC_DIS              (0x00000001UL <<  3U)     /* (RW)   Parity control disable */
#define CHIP_REGFLD_LPUART_CTRL_FLUSH               (0x00000001UL <<  2U)     /* (RW)   Clear receive buffer */
#define CHIP_REGFLD_LPUART_CTRL_TX_EN               (0x00000001UL <<  1U)     /* (RW)   TX enable */
#define CHIP_REGFLD_LPUART_CTRL_PS_SEL              (0x00000001UL <<  0U)     /* (RW)   Odd parity bit enabled */


#define CHIP_REGFLDVAL_LPUART_CTRL_SMP_CNT_3        0 /* 3 sample, noise detection, DIV > 10 */
#define CHIP_REGFLDVAL_LPUART_CTRL_SMP_CNT_1        1 /* 1 sample */

#define CHIP_REGFLDVAL_LPUART_CTRL_WU_SEL_START     0 /* Start bit detection */
#define CHIP_REGFLDVAL_LPUART_CTRL_WU_SEL_FIFO_NE   1 /* Receive FIFO non-empty detection */
#define CHIP_REGFLDVAL_LPUART_CTRL_WU_SEL_CFG_BYTE  2 /* Configurable byte is received */
#define CHIP_REGFLDVAL_LPUART_CTRL_WU_SEL_CFG_FRAME 3 /* Configurable 4-byte framee is received*/

#define CHIP_REGFLDVAL_LPUART_CTRL_RTS_TH_SEL_HF    0 /* RTS 1 when FIFO is half full */
#define CHIP_REGFLDVAL_LPUART_CTRL_RTS_TH_SEL_3_4   1 /* RTS 1 when FIFO is 3/4 full */
#define CHIP_REGFLDVAL_LPUART_CTRL_RTS_TH_SEL_FU    2 /* RTS 1 when FIFO is full */

#define CHIP_REGFLDVAL_LPUART_CTRL_PS_SEL_EVEN      0
#define CHIP_REGFLDVAL_LPUART_CTRL_PS_SEL_ODD       1

#endif // CHIP_N32G03X_REGS_LPUART_H
