#ifndef CHIP_STM32H7XX_REGS_DMA_H_
#define CHIP_STM32H7XX_REGS_DMA_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

#define CHIP_DMA_STREAM_CNT      8
#define CHIP_DMA_CNT             2
#define CHIP_DMA_BDMA_NUM        0

typedef struct {
    __IO uint32_t CR;     /* DMA stream x configuration register      */
    __IO uint32_t NDTR;   /* DMA stream x number of data register     */
    __IO uint32_t PAR;    /* DMA stream x peripheral address register */
    __IO uint32_t M0AR;   /* DMA stream x memory 0 address register   */
    __IO uint32_t M1AR;   /* DMA stream x memory 1 address register   */
    __IO uint32_t FCR;    /* DMA stream x FIFO control register       */
} CHIP_REGS_DMA_STREAM_T;


typedef struct {
    /* доступ к регистрам либо по оригинальным именам L и H, либо по индексу */
    union {
        struct {
            __IO uint32_t LISR;   /* DMA low interrupt status register,      Address offset: 0x00 */
            __IO uint32_t HISR;   /* DMA high interrupt status register,     Address offset: 0x04 */
        };
        __IO uint32_t ISR[2];
    };
    union {
        struct {
            __IO uint32_t LIFCR;  /* DMA low interrupt flag clear register,  Address offset: 0x08 */
            __IO uint32_t HIFCR;  /* DMA high interrupt flag clear register, Address offset: 0x0C */
        };
        __IO uint32_t IFCR[2];
    };
    CHIP_REGS_DMA_STREAM_T STREAM[CHIP_DMA_STREAM_CNT];
} CHIP_REGS_DMA_T;

#define CHIP_REGS_DMA1                          ((CHIP_REGS_DMA_T *) CHIP_MEMRGN_ADDR_PERIPH_DMA1)
#define CHIP_REGS_DMA2                          ((CHIP_REGS_DMA_T *) CHIP_MEMRGN_ADDR_PERIPH_DMA2)

#define CHIP_DMA_IF_REGNUM(x)                   (((x) % 8)/4) /* номер регистра ISR/IFCR для флагов указанного канала */

/* макросы для определения регистров по заданному номеру контроллера (от 1) и канала (от 0) */
#define CHIP_REGS_DMA(num)                      CHIP_REGS_DMA_(num)
#define CHIP_REGS_DMA_(num)                     CHIP_REGS_DMA ## num

#define CHIP_REGS_DMA_STREAM(num, ch)           CHIP_REGS_DMA_STREAM_(num, ch)
#define CHIP_REGS_DMA_STREAM_(num, ch)          (&CHIP_REGS_DMA(num)->STREAM[ch])

#define CHIP_DMA_STREAM_IRQN(num, ch)           CHIP_DMA_STREAM_IRQN_(num, ch)
#define CHIP_DMA_STREAM_IRQN_(num, ch)          DMA ## num ## _Stream ## ch ## _IRQn

#define CHIP_DMA_STREAM_IRQ_HANDLER(num, ch)    CHIP_DMA_STREAM_IRQ_HANDLER_(num, ch)
#define CHIP_DMA_STREAM_IRQ_HANDLER_(num, ch)   DMA ## num ## _Stream ## ch ## _IRQHandler

#define CHIP_DMA_STREAM_REG_ISR(num, ch)        CHIP_DMA_STREAM_REG_ISR_(num,ch)
#define CHIP_DMA_STREAM_REG_ISR_(num, ch)       CHIP_REGS_DMA(num)->ISR[CHIP_DMA_IF_REGNUM(ch)]

#define CHIP_DMA_STREAM_REG_IFCR(num, ch)       CHIP_DMA_STREAM_REG_IFCR_(num,ch)
#define CHIP_DMA_STREAM_REG_IFCR_(num, ch)      CHIP_REGS_DMA(num)->IFCR[CHIP_DMA_IF_REGNUM(ch)]



/* DMA interrupt status/clear registers (DMA_LISR, DMA_HISR, DMA_LIFCR, DMA_HIFCR) */
#define CHIP_REGFLD_DMA_INT_FLAG(ch, intnum)    (0x00000001UL <<  ((intnum) + ((ch) % 2) * 6 + ((ch) % 4)/2 * 16))
#define CHIP_REGFLD_DMA_IF_TC(x)                CHIP_REGFLD_DMA_INT_FLAG(x, 5) /* stream x transfer complete interrupt flag */
#define CHIP_REGFLD_DMA_IF_HT(x)                CHIP_REGFLD_DMA_INT_FLAG(x, 4) /* stream x half transfer interrupt flag */
#define CHIP_REGFLD_DMA_IF_TE(x)                CHIP_REGFLD_DMA_INT_FLAG(x, 3) /* stream x transfer error interrupt flag */
#define CHIP_REGFLD_DMA_IF_DME(x)               CHIP_REGFLD_DMA_INT_FLAG(x, 2) /* stream x direct mode error interrupt flag */
#define CHIP_REGFLD_DMA_IF_FE(x)                CHIP_REGFLD_DMA_INT_FLAG(x, 0) /* stream x FIFO error interrupt flag */

#define CHIP_REGFLD_DMA_IF_ALL(x)               (CHIP_REGFLD_DMA_IF_TC(x) \
                                                  | CHIP_REGFLD_DMA_IF_HT(x) \
                                                  | CHIP_REGFLD_DMA_IF_TE(x) \
                                                  | CHIP_REGFLD_DMA_IF_DME(x) \
                                                  | CHIP_REGFLD_DMA_IF_FE(x))



/*********** DMA stream x configuration register (DMA_SxCR) *******************/
#define CHIP_REGFLD_DMA_SCR_EN                  (0x00000001UL <<  0U) /* Stream enable / flag stream ready when read low */
#define CHIP_REGFLD_DMA_SCR_DMEIE               (0x00000001UL <<  1U) /* Direct mode error interrupt enable */
#define CHIP_REGFLD_DMA_SCR_TEIE                (0x00000001UL <<  2U) /* Transfer error interrupt enable */
#define CHIP_REGFLD_DMA_SCR_HTIE                (0x00000001UL <<  3U) /* Half transfer interrupt enable */
#define CHIP_REGFLD_DMA_SCR_TCIE                (0x00000001UL <<  4U) /* Transfer complete interrupt enable */
#define CHIP_REGFLD_DMA_SCR_PFCTRL              (0x00000001UL <<  5U) /* Peripheral flow controller */
#define CHIP_REGFLD_DMA_SCR_DIR                 (0x00000003UL <<  6U) /* Data transfer direction */
#define CHIP_REGFLD_DMA_SCR_CIRC                (0x00000001UL <<  8U) /* Circular mode */
#define CHIP_REGFLD_DMA_SCR_PINC                (0x00000001UL <<  9U) /* Peripheral increment mode */
#define CHIP_REGFLD_DMA_SCR_MINC                (0x00000001UL << 10U) /* Memory increment mode */
#define CHIP_REGFLD_DMA_SCR_PSIZE               (0x00000003UL << 11U) /* Peripheral data size */
#define CHIP_REGFLD_DMA_SCR_MSIZE               (0x00000003UL << 13U) /* Memory data size */
#define CHIP_REGFLD_DMA_SCR_PINCOS              (0x00000001UL << 15U) /* Peripheral increment offset size */
#define CHIP_REGFLD_DMA_SCR_PL                  (0x00000003UL << 16U) /* Priority level */
#define CHIP_REGFLD_DMA_SCR_DBM                 (0x00000001UL << 18U) /* Double buffer mode */
#define CHIP_REGFLD_DMA_SCR_CT                  (0x00000001UL << 19U) /* Current target (only in double buffer mode) */
#define CHIP_REGFLD_DMA_SCR_TRBUFF              (0x00000001UL << 20U) /* bufferable transfers enabled/disable */
#define CHIP_REGFLD_DMA_SCR_PBURST              (0x00000003UL << 21U) /* Peripheral burst transfer configuration */
#define CHIP_REGFLD_DMA_SCR_MBURST              (0x00000003UL << 23U) /* Memory burst transfer configuration */
#define CHIP_REGMSK_DMA_SCR_RESERVED            (0xFE000000UL)


#define CHIP_REGFLDVAL_DMA_SCR_DIR_PTOM         0 /* peripheral-to-memory */
#define CHIP_REGFLDVAL_DMA_SCR_DIR_MTOP         1 /* memory-to-peripheral */
#define CHIP_REGFLDVAL_DMA_SCR_DIR_MTOM         2 /* memory-to-memory */

#define CHIP_REGFLDVAL_DMA_SCR_SIZE_8           0
#define CHIP_REGFLDVAL_DMA_SCR_SIZE_16          1
#define CHIP_REGFLDVAL_DMA_SCR_SIZE_32          2

#define CHIP_REGFLDVAL_DMA_SCR_PINCOS_PSIZE     0 /* The offset size for the peripheral address calculation is linked to the PSIZE */
#define CHIP_REGFLDVAL_DMA_SCR_PINCOS_32        1 /* The offset size for the peripheral address calculation is fixed to 4 (32-bit alignment) */

#define CHIP_REGFLDVAL_DMA_SCR_PL_LOW           0
#define CHIP_REGFLDVAL_DMA_SCR_PL_MEDIUM        1
#define CHIP_REGFLDVAL_DMA_SCR_PL_HIGH          2
#define CHIP_REGFLDVAL_DMA_SCR_PL_VHIGH         3

#define CHIP_REGFLDVAL_DMA_SCR_BURST_SINGLE     0
#define CHIP_REGFLDVAL_DMA_SCR_BURST_INCR4      1
#define CHIP_REGFLDVAL_DMA_SCR_BURST_INCR8      2
#define CHIP_REGFLDVAL_DMA_SCR_BURST_INCR16     3


/*********** DMA stream x number of data register (DMA_SxNDTR) ****************/
#define CHIP_REGFLD_DMA_SNDTR_NDT               (0x0000FFFFUL <<  0U) /* Number of data items to transfer */

#define CHIP_REGMSK_DMA_SNDTR_RESERVED          (0xFFFF0000UL)

/*********** DMA stream x peripheral address register (DMA_SxPAR) *************/
#define CHIP_REGFLD_DMA_SPAR_PA                 (0xFFFFFFFFUL <<  0U) /* Peripheral Address */

/*********** DMA stream x memory 0 address register (DMA_SxM0AR) **************/
#define CHIP_REGFLD_DMA_SM0AR_M0A               (0xFFFFFFFFUL <<  0U) /* Memory 0 Address */

/*********** DMA stream x memory 1 address register (DMA_SxM1AR) **************/
#define CHIP_REGFLD_DMA_SM1AR_M1A               (0xFFFFFFFFUL <<  0U) /* Memory 1 Address */

/*********** DMA stream x FIFO control register (DMA_SxFCR) *******************/
#define CHIP_REGFLD_DMA_SFCR_FTH                (0x00000003UL <<  0U) /* FIFO threshold selection */
#define CHIP_REGFLD_DMA_SFCR_DMDIS              (0x00000001UL <<  2U) /* Direct mode disable */
#define CHIP_REGFLD_DMA_SFCR_FS                 (0x00000007UL <<  3U) /* FIFO status */
#define CHIP_REGFLD_DMA_SFCR_FEIE               (0x00000001UL <<  7U)

#define CHIP_REGMSK_DMA_SFCR_RESERVED           (0xFFFFFF40UL)

#define CHIP_REGFLDVAL_DMA_SFCR_FTH_1_4         0
#define CHIP_REGFLDVAL_DMA_SFCR_FTH_1_2         1
#define CHIP_REGFLDVAL_DMA_SFCR_FTH_3_4         2
#define CHIP_REGFLDVAL_DMA_SFCR_FTH_FULL        3

#define CHIP_REGFLDVAL_DMA_SFCR_FS_LESS_1_4     0
#define CHIP_REGFLDVAL_DMA_SFCR_FS_LESS_1_2     1
#define CHIP_REGFLDVAL_DMA_SFCR_FS_LESS_3_4     2
#define CHIP_REGFLDVAL_DMA_SFCR_FS_LESS_FULL    3
#define CHIP_REGFLDVAL_DMA_SFCR_FS_EMPTY        4
#define CHIP_REGFLDVAL_DMA_SFCR_FS_FULL         5

#endif // CHIP_STM32H7XX_REGS_DMA_H_
