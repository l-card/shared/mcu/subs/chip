#include "chip.h"

#if CHIP_PWR_PVD_EN
    #if CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL0_VALUE
        #define CHIP_RVAL_PWR_CR_PLS    0
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL1_VALUE
        #define CHIP_RVAL_PWR_CR_PLS    1
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL2_VALUE
        #define CHIP_RVAL_PWR_CR_PLS    2
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL3_VALUE
        #define CHIP_RVAL_PWR_CR_PLS    3
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL4_VALUE
        #define CHIP_RVAL_PWR_CR_PLS    4
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL5_VALUE
        #define CHIP_RVAL_PWR_CR_PLS    5
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL6_VALUE
        #define CHIP_RVAL_PWR_CR_PLS    6
    #else
        #define CHIP_RVAL_PWR_CR_PLS    7
    #endif
#endif


void chip_pwr_init(void) {
    /* для изменения настроек PWR включаем его питание */
    CHIP_REGS_RCC->APB1ENR |= CHIP_REGFLD_RCC_APB1ENR_PWR_EN;
    CHIP_REGS_PWR->CR = (CHIP_REGS_PWR->CR & ~(
                CHIP_REGFLD_PWR_CR_PVDE |
                CHIP_REGFLD_PWR_CR_PLS
                ))
            | LBITFIELD_SET(CHIP_REGFLD_PWR_CR_PVDE, CHIP_PWR_PVD_EN)
#if CHIP_PWR_PVD_EN
            | LBITFIELD_SET(CHIP_REGFLD_PWR_CR_PLS, CHIP_RVAL_PWR_CR_PLS)
#endif
            ;
    /* оставляем PWR включенным, т.к. далее может потребоваться доступ к backup регистрам,
     * финальное состояние включения в любом слчае определится после завершения настройки
     * клоков */
}

bool chip_pwr_pvd_fail(void) {
    return (CHIP_REGS_PWR->CSR & CHIP_REGFLD_PWR_CSR_PVDO) != 0;
}
void chip_pwr_bd_cfg_write_enable(void) {
    CHIP_REGS_PWR->CSR |= CHIP_REGFLD_PWR_CR_DBP;
}

void chip_pwr_bd_cfg_write_disable(void) {
    CHIP_REGS_PWR->CSR &= ~CHIP_REGFLD_PWR_CR_DBP;
}

