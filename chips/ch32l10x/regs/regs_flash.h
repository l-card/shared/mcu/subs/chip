#ifndef REGS_FLASH_H
#define REGS_FLASH_H

#include "chip_ioreg_defs.h"
#include "init/chip_clk_constraints.h"

#define CHIP_SUPPORT_FLASH_BANK2                0
#define CHIP_SUPPORT_FLASH_FORCE_OB_LOAD        0
#define CHIP_SUPPORT_FLASH_OPTION_NBOOT0        0
#define CHIP_SUPPORT_FLASH_OPTION_BOOT_SEL      0
#define CHIP_SUPPORT_FLASH_REG_WS_EN            0
#define CHIP_SUPPORT_FLASH_REG_PID              0
#define CHIP_SUPPORT_FLASH_ECC                  0
#define CHIP_SUPPORT_FLASH_STD_PROG             0
#define CHIP_SUPPORT_FLASH_QUICK_PROG           1
#define CHIP_SUPPORT_FLASH_WAKE_CTL             1
#define CHIP_FLASH_CUSTOM_OPTIONS               1

#include "stm32_flash_v1.h"


#define CHIP_REGFLD_FLASH_OBR_OPT_ERR               (0x00000001UL <<  0U)   /* (RO) Option byte error */
#define CHIP_REGFLD_FLASH_OBR_RD_PTR                (0x00000003UL <<  1U)   /* (RO) Read protection level status */
#define CHIP_REGFLD_FLASH_OBR_IWDG_SW               (0x00000001UL <<  2U)   /* (RO) Independent Watchdog Dog (IWDG) hardware enable, active low */
#define CHIP_REGFLD_FLASH_OBR_NRST_STOP             (0x00000001UL <<  3U)   /* (RO) System reset control in Stop mode, active low. */
#define CHIP_REGFLD_FLASH_OBR_NRST_STDBY            (0x00000001UL <<  4U)   /* (RO) System reset control in Standby mode, active low  */
#define CHIP_REGFLD_FLASH_OBR_CFG_CANM              (0x00000001UL <<  7U)   /* (RO) Configuring CAN offline recovery time (1 - faster, 0 - by specification)  */
#define CHIP_REGFLD_FLASH_OBR_RES1                  (0x00000003UL <<  8U)   /* (RO) Fixed to 11*/
#define CHIP_REGFLD_FLASH_OBR_DATA0                 (0x000000FFUL << 12U)   /* (RO) DATA0 (invalid offset in RM!!) */
#define CHIP_REGFLD_FLASH_OBR_DATA1                 (0x000000FFUL << 20U)   /* (RO) DATA1 */
#define CHIP_REGMSK_FLASH_OBR_RESERVED              (0xFC000060UL)


#endif // REGS_FLASH_H
