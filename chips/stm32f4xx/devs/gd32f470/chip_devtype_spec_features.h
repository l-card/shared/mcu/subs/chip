#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_DEV_GD32F4XX
#define CHIP_DEV_GD32F4X0
#define CHIP_DEV_GD32F450


#define CHIP_DEV_SUPPORT_LCD            1
#define CHIP_DEV_SUPPORT_EXMC           1
#define CHIP_DEV_SUPPORT_SDRAM          (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144)
#define CHIP_DEV_SUPPORT_EXMC_D32       1
#define CHIP_DEV_SUPPORT_SAI            0
#define CHIP_DEV_SUPPORT_DVP            1
#define CHIP_DEV_SUPPORT_ETH            1
#define CHIP_DEV_SUPPORT_USBHS          1
#define CHIP_DEV_SUPPORT_USBHS_PHY      0
#define CHIP_DEV_SUPPORT_CRS            1
#define CHIP_DEV_SUPPORT_QSPI           1
#define CHIP_DEV_SUPPORT_I2C_TXFRAME    1
#define CHIP_DEV_SUPPORT_TRACE          0

#define CHIP_DEV_SPI_CNT                ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144) ? 6 : 5)
#define CHIP_DEV_I2S_CNT                2
#define CHIP_DEV_I2S_FD_CNT             2
#define CHIP_DEV_I2C_CNT                3
#define CHIP_DEV_UART_CNT              8
#define CHIP_DEV_TIM_GP_CNT             10
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          2


#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(240)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
