#ifndef CHIP_EXMC_DEFS_H
#define CHIP_EXMC_DEFS_H

#include "chip_mmap.h"
#include "chip_sdram.h"
/** @note до умножения не округляем в большую строну, т.к. можем слишком завысить время. считаем, что точность в 1 КГц допустима */
#define CHIP_SDRAM_CLK_NUM(x) ((x) > 0 ? (((((CHIP_CLK_SDRAM_FREQ) / 1000) * (x) + 999999)/1000000 + 0xFF) / 256) : ((-(x)) /256))
#define CHIP_SDRAM_T_RVAL(x, adj) ((CHIP_SDRAM_CLK_NUM(x) - adj) > 0 ? (CHIP_SDRAM_CLK_NUM(x) - adj) : 0)


#endif // CHIP_EXMC_DEFS_H
