#ifndef _CMSIS_STM32H7XX_H_
#define _CMSIS_STM32H7XX_H_
/**
 * @brief STM32H7XX Interrupt Number Definition, according to the selected device
 *        in @ref Library_configuration_section
 */

#include "chip_dev_spec_features.h"

typedef enum {
    /* -------------------------  Cortex-M4 Processor Exceptions Numbers  ----------------------------- */
    Reset_IRQn                        = -15,/*!<   1  Reset Vector, invoked on Power up and warm reset */
    NonMaskableInt_IRQn               = -14,/*!<   2  Non maskable Interrupt, cannot be stopped or preempted */
    HardFault_IRQn                    = -13,/*!<   3  Hard Fault, all classes of Fault */
    MemoryManagement_IRQn             = -12,/*!<   4  Memory Management, MPU mismatch, including Access Violation and No Match */
    BusFault_IRQn                     = -11,/*!<   5  Bus Fault, Pre-Fetch-, Memory Access Fault, other address/memory related Fault */
    UsageFault_IRQn                   = -10,/*!<   6  Usage Fault, i.e. Undef Instruction, Illegal State Transition */
    SVCall_IRQn                       =  -5,/*!<  11  System Service Call via SVC instruction */
    DebugMonitor_IRQn                 =  -4,/*!<  12  Debug Monitor                    */
    PendSV_IRQn                       =  -2,/*!<  14  Pendable request for system service */
    SysTick_IRQn                      =  -1,/*!<  15  System Tick Timer                */
    /******  STM32 specific Interrupt Numbers **********************************************************************/
    WWDG_IRQn                   = 0,      /*!< Window WatchDog Interrupt                            */
    PVD_IRQn                    = 1,      /*!< PVD through EXTI Line detection Interrupt            */
    TAMPER_IRQn                 = 2,      /*!< Tamper Interrupt                                     */
    RTC_IRQn                    = 3,      /*!< RTC global Interrupt                                 */
    FLASH_IRQn                  = 4,      /*!< FLASH global Interrupt                               */
    RCC_IRQn                    = 5,      /*!< RCC global Interrupt                                 */
    EXTI0_IRQn                  = 6,      /*!< EXTI Line0 Interrupt                                 */
    EXTI1_IRQn                  = 7,      /*!< EXTI Line1 Interrupt                                 */
    EXTI2_IRQn                  = 8,      /*!< EXTI Line2 Interrupt                                 */
    EXTI3_IRQn                  = 9,      /*!< EXTI Line3 Interrupt                                 */
    EXTI4_IRQn                  = 10,     /*!< EXTI Line4 Interrupt                                 */
    DMA1_Channel1_IRQn          = 11,     /*!< DMA1 Channel 1 global Interrupt                      */
    DMA1_Channel2_IRQn          = 12,     /*!< DMA1 Channel 2 global Interrupt                      */
    DMA1_Channel3_IRQn          = 13,     /*!< DMA1 Channel 3 global Interrupt                      */
    DMA1_Channel4_IRQn          = 14,     /*!< DMA1 Channel 4 global Interrupt                      */
    DMA1_Channel5_IRQn          = 15,     /*!< DMA1 Channel 5 global Interrupt                      */
    DMA1_Channel6_IRQn          = 16,     /*!< DMA1 Channel 6 global Interrupt                      */
    DMA1_Channel7_IRQn          = 17,     /*!< DMA1 Channel 7 global Interrupt                      */
    ADC_IRQn                    = 18,     /*!< ADC global Interrupt                       */
    USB_HP_CAN1_TX_IRQn         = 19,     /*!< USB High Priority or CAN1 TX Interrupts              */
    USB_LP_CAN1_RX0_IRQn        = 20,     /*!< USB Low Priority or CAN1 RX0 Interrupts              */
    CAN_RX1_IRQn                = 21,     /*!< CAN RX1 Interrupt                                    */
    CAN_SCE_IRQn                = 22,     /*!< CAN SCE Interrupt                                    */
    EXTI9_5_IRQn                = 23,     /*!< External Line[9:5] Interrupts                        */
    TMR1_BRK_TMR9_IRQn          = 24,     /*!< TMR1 break interrupt and TMR9 global interrupt       */
    TMR1_OV_TMR10_IRQn          = 25,     /*!< TMR1 update interrupt and TMR10 global interrupt     */
    TMR1_TRG_COM_TMR11_IRQn     = 26,     /*!< TMR1 trigger and communication interrupt and TMR11 global interrupt */
    TMR1_CC_IRQn                = 27,     /*!< TMR1 Capture Compare Interrupt                       */
    TMR2_IRQn                   = 28,     /*!< TMR2 global Interrupt                                */
    TMR3_IRQn                   = 29,     /*!< TMR3 global Interrupt                                */
    TMR4_IRQn                   = 30,     /*!< TMR4 global Interrupt                                */
    I2C1_EV_IRQn                = 31,     /*!< I2C1 Event Interrupt                                 */
    I2C1_ER_IRQn                = 32,     /*!< I2C1 Error Interrupt                                 */
    I2C2_EV_IRQn                = 33,     /*!< I2C2 Event Interrupt                                 */
    I2C2_ER_IRQn                = 34,     /*!< I2C2 Error Interrupt                                 */
    SPI1_IRQn                   = 35,     /*!< SPI1 global Interrupt                                */
    SPI2_I2S2EXT_IRQn           = 36,     /*!< SPI2 and I2S2EXT global Interrupt                    */
    USART1_IRQn                 = 37,     /*!< USART1 global Interrupt                              */
    USART2_IRQn                 = 38,     /*!< USART2 global Interrupt                              */
    USART3_IRQn                 = 39,     /*!< USART3 global Interrupt                              */
    EXTI15_10_IRQn              = 40,     /*!< External Line[15:10] Interrupts                      */
    RTC_Alarm_IRQn              = 41,     /*!< RTC Alarm through EXTI Line Interrupt                */
    USBWakeUp_IRQn              = 42,     /*!< USB Device WakeUp from suspend through EXTI Line Interrupt */
    TMR8_BRK_TMR12_IRQn         = 43,     /*!< TMR8 Break Interrupt and TMR12 global interrupt                   */
    TMR8_OV_TMR13_IRQn          = 44,     /*!< TMR8 Update Interrupt and TMR13 global interrupt                  */
    TMR8_TRG_COM_TMR14_IRQn     = 45,     /*!< TMR8 Trigger and Commutation Interrupt and TMR14 global interrupt */
    TMR8_CC_IRQn                = 46,     /*!< TMR8 Capture Compare global interrupt                             */
    ADC3_IRQn                   = 47,     /*!< ADC3 global interrupt (??)                                        */
    XMC_IRQn                    = 48,     /*!< XMC global Interrupt                                              */
    /* 49 - Reserved */
    TMR5_IRQn                   = 50,     /*!< TMR5 global Interrupt                                             */
    SPI3_I2S3EXT_IRQn           = 51,     /*!< SPI3 and I2S3EXT global Interrupt                                 */
    UART4_IRQn                  = 52,     /*!< UART4 global Interrupt                                            */
    UART5_IRQn                  = 53,     /*!< UART5 global Interrupt                                            */
    TMR6_IRQn                   = 54,     /*!< TMR6 global interrupt                                             */
    TMR7_IRQn                   = 55,     /*!< TMR7 global interrupt                                             */
    DMA2_Channel1_IRQn          = 56,     /*!< DMA2 Channel 1 global Interrupt                                   */
    DMA2_Channel2_IRQn          = 57,     /*!< DMA2 Channel 2 global Interrupt                                   */
    DMA2_Channel3_IRQn          = 58,     /*!< DMA2 Channel 3 global Interrupt                                   */
    DMA2_Channel4_5_IRQn        = 59,     /*!< DMA2 Channel 4 and 5 global Interrupt                             */
    /* 60 - ?? */
    I2C3_EV_IRQn                = 61,     /*!< I2C3 event interrupt                                              */
    I2C3_ER_IRQn                = 62,     /*!< I2C3 error interrupt                                              */
    SPI4_IRQn                   = 63,     /*!< SPI4 global Interrupt                                             */
    /* 64 - 72 - Reserved */
    USB_HP_IRQn                 = 73,     /*!< USB High Priority interrupt                                       */
    USB_LP_IRQn                 = 74,     /*!< USB Low Priority interrupt                                        */
    DMA2_Channel6_7_IRQn        = 75,     /*!< DMA2 Channel 6 and 7 global Interrupt                             */
    USART6_IRQn                 = 76,     /*!< USART6 global Interrupt                                            */
    UART7_IRQn                  = 77,     /*!< UART7 global Interrupt                                            */
    UART8_IRQn                  = 78,     /*!< UART8 global Interrupt                                            */
} IRQn_Type;

/**
  * @}
  */

/** @addtogroup Configuration_section_for_CMSIS
  * @{
  */

#define __CM4_REV                 0x0001  /*!< Cortex-M4 revision r0p1                       */
#define __MPU_PRESENT             1       /*!< CM4 provides an MPU                           */
#define __NVIC_PRIO_BITS          4       /*!< CM4 uses 4 Bits for the Priority Levels       */
#define __Vendor_SysTickConfig    0       /*!< Set to 1 if different SysTick Config is used  */
#define __FPU_PRESENT             1       /*!< FPU present */

#include "core_cm4.h"

#endif /*_CMSIS_STM32H7XX_H_*/
