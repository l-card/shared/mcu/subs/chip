/*
 * @brief LPC11U6X System Control registers and control functions
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef CHIP_LPC11U6X_REGS_SYSCON_H
#define CHIP_LPC11U6X_REGS_SYSCON_H

#include <stdint.h>

/**
 * @brief LPC11U6X System Control block structure
 */
typedef struct {                        /*!< SYSCON Structure */
    __IO uint32_t  SYSMEMREMAP;         /*!< System Memory remap register */
    __IO uint32_t  PRESETCTRL;          /*!< Peripheral reset Control register */
    __IO uint32_t  SYSPLLCTRL;          /*!< System PLL control register */
    __I  uint32_t  SYSPLLSTAT;          /*!< System PLL status register */
    __IO uint32_t  USBPLLCTRL;          /*!< USB PLL control register */
    __I  uint32_t  USBPLLSTAT;          /*!< USB PLL status register */
    __I  uint32_t  RESERVED1[1];
    __IO uint32_t  RTCOSCCTRL;          /*!< RTC Oscillator control register */
    __IO uint32_t  SYSOSCCTRL;          /*!< System Oscillator control register */
    __IO uint32_t  WDTOSCCTRL;          /*!< Watchdog Oscillator control register */
    __I  uint32_t  RESERVED2[2];
    __IO uint32_t  SYSRSTSTAT;          /*!< System Reset Status register */
    __I  uint32_t  RESERVED3[3];
    __IO uint32_t  SYSPLLCLKSEL;        /*!< System PLL clock source select register */
    __IO uint32_t  SYSPLLCLKUEN;        /*!< System PLL clock source update enable register*/
    __IO uint32_t  USBPLLCLKSEL;        /*!< USB PLL clock source select register */
    __IO uint32_t  USBPLLCLKUEN;        /*!< USB PLL clock source update enable register */
    __I  uint32_t  RESERVED4[8];
    __IO uint32_t  MAINCLKSEL;          /*!< Main clock source select register */
    __IO uint32_t  MAINCLKUEN;          /*!< Main clock source update enable register */
    __IO uint32_t  SYSAHBCLKDIV;        /*!< System Clock divider register */
    __I  uint32_t  RESERVED5;
    __IO uint32_t  SYSAHBCLKCTRL;       /*!< System clock control register */
    __I  uint32_t  RESERVED6[4];
    __IO uint32_t  SSP0CLKDIV;          /*!< SSP0 clock divider register */
    __IO uint32_t  USART0CLKDIV;        /*!< UART clock divider register */
    __IO uint32_t  SSP1CLKDIV;          /*!< SSP1 clock divider register */
    __IO uint32_t  FRGCLKDIV;           /*!< FRG clock divider (USARTS 1 - 4) register */
    __I  uint32_t  RESERVED7[7];
    __IO uint32_t  USBCLKSEL;           /*!< USB clock source select register */
    __IO uint32_t  USBCLKUEN;           /*!< USB clock source update enable register */
    __IO uint32_t  USBCLKDIV;           /*!< USB clock source divider register */
    __I  uint32_t  RESERVED8[5];
    __IO uint32_t  CLKOUTSEL;           /*!< Clock out source select register */
    __IO uint32_t  CLKOUTUEN;           /*!< Clock out source update enable register */
    __IO uint32_t  CLKOUTDIV;           /*!< Clock out divider register */
    __I  uint32_t  RESERVED9;
    __IO uint32_t  UARTFRGDIV;          /*!< USART fractional generator divider (USARTS 1 - 4) register */
    __IO uint32_t  UARTFRGMULT;         /*!< USART fractional generator multiplier (USARTS 1 - 4) register */
    __I  uint32_t  RESERVED10;
    __IO uint32_t  EXTTRACECMD;         /*!< External trace buffer command register */
    __I  uint32_t  PIOPORCAP[3];        /*!< POR captured PIO status registers */
    __I  uint32_t  RESERVED11[10];
    __IO uint32_t  IOCONCLKDIV[7];      /*!< IOCON block for programmable glitch filter divider registers */
    __IO uint32_t  BODCTRL;             /*!< Brown Out Detect register */
    __IO uint32_t  SYSTCKCAL;           /*!< System tick counter calibration register */
    __I  uint32_t  RESERVED12[6];
    __IO uint32_t  IRQLATENCY;          /*!< IRQ delay register */
    __IO uint32_t  NMISRC;              /*!< NMI source control register */
    __IO uint32_t  PINTSEL[8];          /*!< GPIO pin interrupt select register 0-7 */
    __IO uint32_t  USBCLKCTRL;          /*!< USB clock control register */
    __I  uint32_t  USBCLKST;            /*!< USB clock status register */
    __I  uint32_t  RESERVED13[25];
    __IO uint32_t  STARTERP0;           /*!< Start logic 0 interrupt wake-up enable register */
    __I  uint32_t  RESERVED14[3];
    __IO uint32_t  STARTERP1;           /*!< Start logic 1 interrupt wake-up enable register */
    __I  uint32_t  RESERVED15[6];
    __IO uint32_t  PDSLEEPCFG;          /*!< Power down states in deep sleep mode register */
    __IO uint32_t  PDWAKECFG;           /*!< Power down states in wake up from deep sleep register */
    __IO uint32_t  PDRUNCFG;            /*!< Power configuration register*/
    __I  uint32_t  RESERVED16[110];
    __I  uint32_t  DEVICEID;            /*!< Device ID register */
}  CHIP_REGS_SYSCON_T;

#define CHIP_REGS_SYSCON        ((CHIP_REGS_SYSCON_T *) CHIP_MEMRGN_ADDR_PERIPH_SYSCON)

#define CHIP_DEVID_LPC11U68JBD48    (0x00007C08UL)
#define CHIP_DEVID_LPC11U68JBD64    (0x00007C08UL)
#define CHIP_DEVID_LPC11U68JBD100   (0x00007C00UL)
#define CHIP_DEVID_LPC11U67JBD100   (0x0000BC80UL)
#define CHIP_DEVID_LPC11U67JBD64    (0x0000BC88UL)
#define CHIP_DEVID_LPC11U66JBD48    (0x0000DCC8UL)
#define CHIP_DEVID_LPC11E67JBD48    (0x0000BC81UL)
#define CHIP_DEVID_LPC11E68JBD64    (0x00007C01UL)
#define CHIP_DEVID_LPC11E68JBD100   (0x00007C01UL)
#define CHIP_DEVID_LPC11E68JBD48    (0x00007C01UL)
#define CHIP_DEVID_LPC11E67JBD100   (0x0000BC81UL)
#define CHIP_DEVID_LPC11E67JBD64    (0x0000BC81UL)
#define CHIP_DEVID_LPC11E66JBD48    (0x0000DCC1UL)


/*********** System memory remap (SYSCON_SYSMEMREMAP) *************************/
#define CHIP_REGFLD_SYSCON_SYSMEMREMAP_MAP          (0x00000003UL <<  0U)   /* (RW) System memory remap */
#define CHIP_REGMSK_SYSCON_SYSMEMREMAP_RESERVED     (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCON_SYSMEMREMAP_MAP_BOOT  0 /*  Boot Loader Mode. Interrupt vectors are re-mapped to Boot ROM. */
#define CHIP_REGFLDVAL_SYSCON_SYSMEMREMAP_MAP_RAM   1 /*  User RAM Mode. Interrupt vectors are re-mapped to Static RAM. */
#define CHIP_REGFLDVAL_SYSCON_SYSMEMREMAP_MAP_FLASH 2 /*  User Flash Mode. Interrupt vectors are not re-mapped and reside in Flash. */

/*********** Peripheral reset control register (SYSCON_PRESETCTRL) *************************/
#define CHIP_REGFLD_SYSCON_PRESETCTRL_SSP0_RSTN     (0x00000001UL <<  0U)   /* (RW) SSP0 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_I2C0_RSTN     (0x00000001UL <<  1U)   /* (RW) I2C0 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_SSP1_RSTN     (0x00000001UL <<  2U)   /* (RW) SSP1 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_I2C1_RSTN     (0x00000001UL <<  3U)   /* (RW) I2C1 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_FRG_RSTN      (0x00000001UL <<  4U)   /* (RW) FRG reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_USART1_RSTN   (0x00000001UL <<  5U)   /* (RW) USART1 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_USART2_RSTN   (0x00000001UL <<  6U)   /* (RW) USART2 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_USART3_RSTN   (0x00000001UL <<  7U)   /* (RW) USART3 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_USART4_RSTN   (0x00000001UL <<  8U)   /* (RW) USART4 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_SCT0_RSTN     (0x00000001UL <<  9U)   /* (RW) SCT0 reset control (0 - reset) */
#define CHIP_REGFLD_SYSCON_PRESETCTRL_SCT1_RSTN     (0x00000001UL << 10U)   /* (RW) SCT1 reset control (0 - reset) */
#define CHIP_REGMSK_SYSCON_PRESETCTRL_RESERVED      (0xFFFFF800UL)

/*********** System PLL control register (SYSCON_SYSPLLCTRL) ******************/
#define CHIP_REGFLD_SYSCON_SYSPLLCTRL_MSEL          (0x0000001FUL <<  0U)   /* (RW) Feedback divider value  */
#define CHIP_REGFLD_SYSCON_SYSPLLCTRL_PSEL          (0x00000003UL <<  5U)   /* (RW) Post divider ratio P. The division ratio is 2 x P */
#define CHIP_REGMSK_SYSCON_SYSPLLCTRL_RESERVED      (0xFFFFFF80UL)

#define CHIP_REGFLDVAL_SYSCON_SYSPLLCTRL_PSEL_1     0
#define CHIP_REGFLDVAL_SYSCON_SYSPLLCTRL_PSEL_2     1
#define CHIP_REGFLDVAL_SYSCON_SYSPLLCTRL_PSEL_4     2
#define CHIP_REGFLDVAL_SYSCON_SYSPLLCTRL_PSEL_8     3

/*********** System PLL status register (SYSCON_SYSPLLSTAT) *******************/
#define CHIP_REGFLD_SYSCON_SYSPLLSTAT_LOCK          (0x00000001UL <<  0U)   /* (RO) PLL lock status  */
#define CHIP_REGMSK_SYSCON_SYSPLLSTAT_RESERVED      (0xFFFFFFFEUL)


/*********** USB PLL control register (SYSCON_USBPLLCTRL) *********************/
#define CHIP_REGFLD_SYSCON_USBPLLCTRL_MSEL          (0x0000001FUL <<  0U)   /* (RW) Feedback divider value  */
#define CHIP_REGFLD_SYSCON_USBPLLCTRL_PSEL          (0x00000003UL <<  5U)   /* (RW) Post divider ratio P. The division ratio is 2 x P */
#define CHIP_REGMSK_SYSCON_USBPLLCTRL_RESERVED      (0xFFFFFF80UL)

#define CHIP_REGFLDVAL_SYSCON_USBPLLCTRL_PSEL_1     0
#define CHIP_REGFLDVAL_SYSCON_USBPLLCTRL_PSEL_2     1
#define CHIP_REGFLDVAL_SYSCON_USBPLLCTRL_PSEL_4     2
#define CHIP_REGFLDVAL_SYSCON_USBPLLCTRL_PSEL_8     3

/*********** USB PLL status register (SYSCON_USBPLLSTAT) **********************/
#define CHIP_REGFLD_SYSCON_USBPLLSTAT_LOCK          (0x00000001UL <<  0U)   /* (RO) PLL lock status  */
#define CHIP_REGMSK_SYSCON_USBPLLSTAT_RESERVED      (0xFFFFFFFEUL)


/*********** RTC oscillator 32 kHz output control register (SYSCON_RTCOSCCTRL)*/
#define CHIP_REGFLD_SYSCON_RTCOSCCTRL_RTCOSCEN      (0x00000001UL <<  0U)   /* (RW)  Enable the RTC 32 kHz output. */
#define CHIP_REGMSK_SYSCON_RTCOSCCTRL_RESERVED      (0xFFFFFFFEUL <<  0U)   /*reserved  */

/*********** System oscillator control register (SYSCON_SYSOSCCTRL) ***********/
#define CHIP_REGFLD_SYSCON_SYSOSCCTRL_BYPASS        (0x00000001UL <<  0U)   /* (RW) Bypass enabled. PLL input (sys_osc_clk) is fed directly from the XTALIN pin bypassing the oscillator. */
#define CHIP_REGFLD_SYSCON_SYSOSCCTRL_FREQRANGE     (0x00000001UL <<  1U)   /* (RW) Determines frequency range for Low-power oscillator. */
#define CHIP_REGMSK_SYSCON_SYSOSCCTRL_RESERVED      (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCON_SYSOSCCTRL_FREQRANGE_L20  0 /* 1 - 20 MHz */
#define CHIP_REGFLDVAL_SYSCON_SYSOSCCTRL_FREQRANGE_H15  1 /* 15 - 25 MHz */

/*********** Watchdog oscillator control register (SYSCON_WDTOSCCTRL) ********/
#define CHIP_REGFLD_SYSCON_WDTOSCCTRL_DIVSEL        (0x0000001FUL <<  0U)   /* (RW) Select divider for Fclkana: wdt_osc_clk = Fclkana/ (2 * (1 + DIVSEL)). */
#define CHIP_REGFLD_SYSCON_WDTOSCCTRL_FREQSEL       (0x0000000FUL <<  5U)   /* (RW)  Select watchdog oscillator analog output frequency (Fclkana). */
#define CHIP_REGMSK_SYSCON_WDTOSCCTRL_RESERVED      (0xFFFFFE00UL)

#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_0_6MHZ     1
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_1_05MHZ    2
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_1_4MHZ     3
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_1_75MHZ    4
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_2_1MHZ     5
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_2_4MHZ     6
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_2_7MHZ     7
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_3_0MHZ     8
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_3_25MHZ    9
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_3_5MHZ     10
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_3_75MHZ    11
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_4_0MHZ     12
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_4_2MHZ     13
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_4_4MHZ     14
#define CHIP_REGFLDVAL_SYSCON_WDTOSCCTRL_FREQSEL_4_6MHZ     15


/*********** Internal resonant crystal control register (SYSCON_IRCCTRL) ******/
#define CHIP_REGFLD_SYSCON_IRCCTRL_TRIM             (0x000000FFUL <<  0U)   /* (RW) Trim value */
#define CHIP_REGMSK_SYSCON_IRCCTRL_RESERVED         (0xFFFFFF00UL)

/*********** System reset status register (SYSCON_SYSRSTSTAT) *****************/
#define CHIP_REGFLD_SYSCON_SYSRSTSTAT_POR           (0x00000001UL <<  0U)   /* (RW1C) POR reset status*/
#define CHIP_REGFLD_SYSCON_SYSRSTSTAT_EXTRST        (0x00000001UL <<  1U)   /* (RW1C) Status of the external RESET pin */
#define CHIP_REGFLD_SYSCON_SYSRSTSTAT_WDT           (0x00000001UL <<  2U)   /* (RW1C) Status of the Watchdog reset */
#define CHIP_REGFLD_SYSCON_SYSRSTSTAT_BOD           (0x00000001UL <<  3U)   /* (RW1C) Status of the Brown-out detect reset */
#define CHIP_REGFLD_SYSCON_SYSRSTSTAT_SYSRST        (0x00000001UL <<  4U)   /* (RW1C) Status of the software system reset */
#define CHIP_REGMSK_SYSCON_SYSRSTSTAT_RESERVED      (0xFFFFFFE0UL)

/*********** System PLL clock source select register (SYSCON_SYSPLLCLKSEL) ****/
#define CHIP_REGFLD_SYSCON_SYSPLLCLKSEL_SEL         (0x00000003UL <<  0U)   /* (RW) System PLL clock source */
#define CHIP_REGMSK_SYSCON_SYSPLLCLKSEL_RESERVED    (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCON_SYSPLLCLKSEL_SEL_IRC      0
#define CHIP_REGFLDVAL_SYSCON_SYSPLLCLKSEL_SEL_SYSOSC   1
#define CHIP_REGFLDVAL_SYSCON_SYSPLLCLKSEL_SEL_RTCOSC   3

/*********** System PLL clock source update register (SYSCON_SYSPLLCLKUEN) ****/
#define CHIP_REGFLD_SYSCON_SYSPLLCLKUEN_ENA         (0x00000001UL <<  0U)   /* (RW) Enable system PLL clock source update */
#define CHIP_REGMSK_SYSCON_SYSPLLCLKUEN_RESERVED    (0xFFFFFFFEUL)


/*********** USB PLL clock source select register (SYSCON_USBPLLCLKSEL) *******/
#define CHIP_REGFLD_SYSCON_USBPLLCLKSEL_SEL         (0x00000003UL <<  0U)   /* (RW) USB PLL clock source */
#define CHIP_REGMSK_SYSCON_USBPLLCLKSEL_RESERVED    (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCON_USBPLLCLKSEL_SEL_IRC      0
#define CHIP_REGFLDVAL_SYSCON_USBPLLCLKSEL_SEL_SYSOSC   1

/*********** USB PLL clock source update register (SYSCON_USBPLLCLKUEN) *******/
#define CHIP_REGFLD_SYSCON_USBPLLCLKUEN_ENA         (0x00000001UL <<  0U)   /* (RW) Enable USB PLL clock source update */
#define CHIP_REGMSK_SYSCON_USBPLLCLKUEN_RESERVED    (0xFFFFFFFEUL)


/*********** Main clock source select register (SYSCON_MAINCLKSEL) ************/
#define CHIP_REGFLD_SYSCON_MAINCLKSEL_SEL           (0x00000003UL <<  0U)   /* (RW) USB PLL clock source */
#define CHIP_REGMSK_SYSCON_MAINCLKSEL_RESERVED      (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCON_MAINCLKSEL_SEL_IRC    0
#define CHIP_REGFLDVAL_SYSCON_MAINCLKSEL_SEL_PLL_IN 1
#define CHIP_REGFLDVAL_SYSCON_MAINCLKSEL_SEL_WDT    2
#define CHIP_REGFLDVAL_SYSCON_MAINCLKSEL_SEL_PLL    3

/*********** Main clock source update register (SYSCON_MAINCLKUEN) ************/
#define CHIP_REGFLD_SYSCON_MAINCLKUEN_ENA           (0x00000001UL <<  0U)   /* (RW) Enable main clock source update */
#define CHIP_REGMSK_SYSCON_MAINCLKUEN_RESERVED      (0xFFFFFFFEUL)

/*********** System clock divider register (SYSCON_SYSAHBCLKDIV) **************/
#define CHIP_REGFLD_SYSCON_SYSAHBCLKDIV_DIV         (0x000000FFUL <<  0U)   /* (RW) System AHB clock divider  */
#define CHIP_REGMSK_SYSCON_SYSAHBCLKDIV_RESERVED    (0xFFFFFF00UL)

/*********** System clock control register (SYSCON_SYSAHBCLKCTRL) *************/
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_SYS        (0x00000001UL <<  0U)   /* (RO) Always-on clock for the AHB */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_ROM        (0x00000001UL <<  1U)   /* (RW) Enables clock for ROM */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_RAM0       (0x00000001UL <<  2U)   /* (RW) Enables clock for SRAM0 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_FLASHREG   (0x00000001UL <<  3U)   /* (RW) Enables clock for flash register interface */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_FLASHARRAY (0x00000001UL <<  4U)   /* (RW) Enables clock for flash access */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_I2C0       (0x00000001UL <<  5U)   /* (RW) Enables clock for I2C0 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_GPIO       (0x00000001UL <<  6U)   /* (RW) Enables clock for GPIO port registers */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_CT16B0     (0x00000001UL <<  7U)   /* (RW) Enables clock for 16-bit counter/timer 0 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_CT16B1     (0x00000001UL <<  8U)   /* (RW) Enables clock for 16-bit counter/timer 1 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_CT32B0     (0x00000001UL <<  9U)   /* (RW) Enables clock for 32-bit counter/timer 0 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_CT32B1     (0x00000001UL << 10U)   /* (RW) Enables clock for 32-bit counter/timer 1 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_SSP0       (0x00000001UL << 11U)   /* (RW) Enables clock for SSP0 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_USART0     (0x00000001UL << 12U)   /* (RW) Enables clock for USART0 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_ADC        (0x00000001UL << 13U)   /* (RW) Enables clock for ADC */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_USB        (0x00000001UL << 14U)   /* (RW) Enables clock for USB register interface */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_WWDT       (0x00000001UL << 15U)   /* (RW) Enables clock for WWDT */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_IOCON      (0x00000001UL << 16U)   /* (RW) Enables clock for I/O configuration block. */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_SSP1       (0x00000001UL << 18U)   /* (RW) Enables clock for SSP1 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_PINT       (0x00000001UL << 19U)   /* (RW) Enables clock to GPIO Pin interrupt register interface. */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_USART1     (0x00000001UL << 20U)   /* (RW) Enables clock for USART1 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_USART2     (0x00000001UL << 21U)   /* (RW) Enables clock for USART2 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_USART3_4   (0x00000001UL << 22U)   /* (RW) Enables clock to USART3 and USART4 register interfaces. */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_GROUP0INT  (0x00000001UL << 23U)   /* (RW) Enables clock to GPIO GROUP0 interrupt register interface. */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_GROUP1INT  (0x00000001UL << 24U)   /* (RW) Enables clock to GPIO GROUP1 interrupt register interface. */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_I2C1       (0x00000001UL << 25U)   /* (RW) Enables clock for I2C1 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_RAM1       (0x00000001UL << 26U)   /* (RW) Enables clock for SRAM1 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_USBRAM     (0x00000001UL << 27U)   /* (RW) Enables clock for ISB SRAM/SRAM2 */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_CRC        (0x00000001UL << 28U)   /* (RW) Enables clock for CRC */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_DMA        (0x00000001UL << 29U)   /* (RW) Enables clock for DMA */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_RTC        (0x00000001UL << 30U)   /* (RW) Enables clock for RTC register interface */
#define CHIP_REGFLD_SYSCON_SYSAHBCLKCTRL_SCT0_1     (0x00000001UL << 31U)   /* (RW) Enables clock for SCT0 and SCT1 register interface */


/*********** SSP0 clock divider register (SYSCON_SSP0CLKDIV) ******************/
#define CHIP_REGFLD_SYSCON_SSP0CLKDIV_DIV           (0x000000FFUL <<  0U)   /* (RW) SPI0_PCLK clock divider values  */
#define CHIP_REGMSK_SYSCON_SSP0CLKDIV_RESERVED      (0xFFFFFF00UL)

/*********** USART0 clock divider register (SYSCON_USART0CLKDIV) **************/
#define CHIP_REGFLD_SYSCON_USART0CLKDIV_DIV         (0x000000FFUL <<  0U)   /* (RW) UART_PCLK clock divider values  */
#define CHIP_REGMSK_SYSCON_USART0CLKDIV_RESERVED    (0xFFFFFF00UL)

/*********** SSP1 clock divider register (SYSCON_SSP1CLKDIV) ******************/
#define CHIP_REGFLD_SYSCON_SSP1CLKDIV_DIV           (0x000000FFUL <<  0U)   /* (RW) SPI1_PCLK clock divider values  */
#define CHIP_REGMSK_SYSCON_SSP1CLKDIV_RESERVED      (0xFFFFFF00UL)

/*********** UART Fractional baud rate clock divider register (SYSCON_FRGCLKDIV) */
#define CHIP_REGFLD_SYSCON_FRGCLKDIV_DIV            (0x000000FFUL <<  0U)   /* (RW) USART fractional baud rate generator clock divider values */
#define CHIP_REGMSK_SYSCON_FRGCLKDIV_RESERVED       (0xFFFFFF00UL)

/*********** USB clock source select register (SYSCON_USBCLKSEL) **************/
#define CHIP_REGFLD_SYSCON_USBCLKSEL_SEL            (0x00000003UL <<  0U)   /* (RW) USB clock source */
#define CHIP_REGMSK_SYSCON_USBCLKSEL_RESERVED       (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCON_USBCLKSEL_SEL_USBPLL  0
#define CHIP_REGFLDVAL_SYSCON_USBCLKSEL_SEL_MAINCLK 1

/*********** USB clock source update enable register (SYSCON_USBCLKUEN) *******/
#define CHIP_REGFLD_SYSCON_USBCLKUEN_ENA            (0x00000001UL <<  0U)   /* (RW) Enable USB clock source update */
#define CHIP_REGMSK_SYSCON_USBCLKUEN_RESERVED       (0xFFFFFFFEUL)

/*********** USB clock source divider register (SYSCON_USBCLKDIV) *************/
#define CHIP_REGFLD_SYSCON_USBCLKDIV_DIV            (0x000000FFUL <<  0U)   /* (RW) USB clock divider values */
#define CHIP_REGMSK_SYSCON_USBCLKDIV_RESERVED       (0xFFFFFF00UL)

/*********** CLKOUT clock source select register (SYSCON_CLKOUTSEL) ***********/
#define CHIP_REGFLD_SYSCON_CLKOUTSEL_SEL            (0x00000003UL <<  0U)   /* (RW) CLKOUT clock source */
#define CHIP_REGMSK_SYSCON_CLKOUTSEL_RESERVED       (0xFFFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCON_CLKOUTSEL_SEL_IRC     0
#define CHIP_REGFLDVAL_SYSCON_CLKOUTSEL_SEL_SYSOSC  1
#define CHIP_REGFLDVAL_SYSCON_CLKOUTSEL_SEL_WDT     2
#define CHIP_REGFLDVAL_SYSCON_CLKOUTSEL_SEL_MAINCLK 3

/*********** CLKOUT clock source update enable register (SYSCON_CLKOUTUEN) ****/
#define CHIP_REGFLD_SYSCON_CLKOUTUEN_ENA            (0x00000001UL <<  0U)   /* (RW) Enable CLKOUT clock source update */
#define CHIP_REGMSK_SYSCON_CLKOUTUEN_RESERVED       (0xFFFFFFFEUL)

/*********** CLKOUT clock divider register (SYSCON_CLKOUTDIV) *****************/
#define CHIP_REGFLD_SYSCON_CLKOUTDIV_DIV            (0x000000FFUL <<  0U)   /* (RW) CLKOUT clock divider values */
#define CHIP_REGMSK_SYSCON_CLKOUTDIV_RESERVED       (0xFFFFFF00UL)


/*********** USART fractional generator divider value register (SYSCON_CLKOUTDIV) */
#define CHIP_REGFLD_SYSCON_UARTFRGDIV_DIV           (0x000000FFUL <<  0U)   /* (RW) Denominator of the fractional divider. */
#define CHIP_REGMSK_SYSCON_UARTFRGDIV_RESERVED      (0xFFFFFF00UL)

/*********** USART fractional generator multiplier value register (SYSCON_UARTFRGMULT) **/
#define CHIP_REGFLD_SYSCON_UARTFRGMULT_MULT         (0x000000FFUL <<  0U)   /* (RW) Numerator of the fractional divider. MULT is equal to the programmed value. */
#define CHIP_REGMSK_SYSCON_UARTFRGMULT_RESERVED     (0xFFFFFF00UL)

/*********** External trace buffer command register (SYSCON_EXTTRACECMD) ******/
#define CHIP_REGFLD_SYSCON_EXTTRACECMD_START        (0x00000001UL <<  0U)   /* (RW) Trace start command. */
#define CHIP_REGFLD_SYSCON_EXTTRACECMD_STOP         (0x00000001UL <<  1U)   /* (RW) Trace stop command. */
#define CHIP_REGMSK_SYSCON_EXTTRACECMD_RESERVED     (0xFFFFFFFCUL)

/*********** IOCON glitch filter clock divider registers 6 to 0 (SYSCON_IOCONCLKDIV) */
#define CHIP_REGFLD_SYSCON_IOCONCLKDIV_DIV          (0x000000FFUL <<  0U)   /* (RW) IOCON glitch filter clock divider values */
#define CHIP_REGMSK_SYSCON_IOCONCLKDIV_RESERVED     (0xFFFFFF00UL)

/*********** Brown-Out Detect register (SYSCON_BODCTRL) ***********************/
#define CHIP_REGFLD_SYSCON_BODCTRL_BODRSTLEV        (0x00000003UL <<  0U)   /* (RW) BOD reset level */
#define CHIP_REGFLD_SYSCON_BODCTRL_BODINTVAL        (0x00000003UL <<  2U)   /* (RW) BOD interrupt level */
#define CHIP_REGFLD_SYSCON_BODCTRL_BODRSTENA        (0x00000001UL <<  4U)   /* (RW) BOD reset enable */
#define CHIP_REGMSK_SYSCON_BODCTRL_RESERVED         (0xFFFFFFE0UL)

/*********** System tick counter calibration register (SYSCON_SYSTCKCAL) ******/
#define CHIP_REGFLD_SYSCON_SYSTCKCAL_CAL            (0x03FFFFFFUL <<  0U)   /* (RW) System tick timer calibration value */
#define CHIP_REGMSK_SYSCON_SYSTCKCAL_RESERVED       (0xFC000000UL)

/*********** IRQ delay register (SYSCON_IRQLATENCY) ***************************/
#define CHIP_REGFLD_SYSCON_IRQLATENCY_LATENCY       (0x000000FFUL <<  0U)   /* (RW) 8-bit latency value */
#define CHIP_REGMSK_SYSCON_IRQLATENCY_RESERVED      (0xFFFFFF00UL)

/*********** NMI Source Control register (SYSCON_NMISRC) **********************/
#define CHIP_REGFLD_SYSCON_NMISRC_IRQN              (0x0000001FUL <<  0U)   /* (RW) The IRQ number of the interrupt that acts as NMI */
#define CHIP_REGFLD_SYSCON_NMISRC_NMIEN             (0x00000001UL << 31U)   /* (RW) Tnable the Non-Maskable Interrupt (NMI) source */
#define CHIP_REGMSK_SYSCON_NMISRC_RESERVED          (0x7FFFFFE0UL)

/*********** Pin Interrupt Select registers 0 to 7 (SYSCON_PINTSEL) ***********/
#define CHIP_REGFLD_SYSCON_PINTSEL_INTPIN           (0x0000003FUL <<  0U)   /* (RW) Pin number */
#define CHIP_REGMSK_SYSCON_PINTSEL_RESERVED         (0xFFFFFFC0UL)

/***********  USB clock control register (SYSCON_USBCLKCTRL) ******************/
#define CHIP_REGFLD_SYSCON_USBCLKCTRL_AP_CLK        (0x00000001UL <<  0U)   /* (RW) USB need_clock signal control (hardware or forced high) */
#define CHIP_REGFLD_SYSCON_USBCLKCTRL_POL_CLK       (0x00000001UL <<  1U)   /* (RW) USB need_clock polarity for triggering the USB wake-up interrupt */
#define CHIP_REGMSK_SYSCON_USBCLKCTRL_RESERVED      (0xFFFFFFFCUL)

/***********  USB clock status register (SYSCON_USBCLKST) *********************/
#define CHIP_REGFLD_SYSCON_USBCLKST_NEED_CLKST      (0x00000001UL <<  0U)   /* (RO) USB need_clock signal status */
#define CHIP_REGMSK_SYSCON_USBCLKST_RESERVED        (0xFFFFFFFEUL)

/***********  Start logic 0 interrupt wake-up enable register (SYSCON_STARTERP0) */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT0          (0x00000001UL <<  0U)   /* (RW) Pin interrupt 0 wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT1          (0x00000001UL <<  1U)   /* (RW) Pin interrupt 1 wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT2          (0x00000001UL <<  2U)   /* (RW) Pin interrupt 2 wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT3          (0x00000001UL <<  3U)   /* (RW) Pin interrupt 3 wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT4          (0x00000001UL <<  4U)   /* (RW) Pin interrupt 4 wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT5          (0x00000001UL <<  5U)   /* (RW) Pin interrupt 5 wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT6          (0x00000001UL <<  6U)   /* (RW) Pin interrupt 6 wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP0_PINT7          (0x00000001UL <<  7U)   /* (RW) Pin interrupt 7 wake-up */
#define CHIP_REGMSK_SYSCON_STARTERP0_RESERVED       (0xFFFFFF00UL)

/***********  Start logic 1 interrupt wake-up enable register (SYSCON_STARTERP1) */
#define CHIP_REGFLD_SYSCON_STARTERP1_RTCINT         (0x00000001UL << 12U)   /* (RW) RTC interrupt wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP1_WWDT_BODINT    (0x00000001UL << 13U)   /* (RW) Combined WWDT interrupt or Brown Out Detect (BOD) interrupt wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP1_USB_WAKEUP     (0x00000001UL << 19U)   /* (RW) USB need_clock signal wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP1_GROUP0INT      (0x00000001UL << 20U)   /* (RW) GPIO GROUP0 interrupt wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP1_GROUP1INT      (0x00000001UL << 21U)   /* (RW) GPIO GROUP1 interrupt wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP1_USART1_4       (0x00000001UL << 23U)   /* (RW) Combined USART1 and USART4 interrupt wake-up */
#define CHIP_REGFLD_SYSCON_STARTERP1_USART2_3       (0x00000001UL << 24U)   /* (RW) Combined USART2 and USART3 interrupt wake-up */
#define CHIP_REGMSK_SYSCON_STARTERP1_RESERVED       (0xFE47CFFFUL)

/*********** Deep-sleep mode configuration register (SYSCON_PDSLEEPCFG) ******/
#define CHIP_REGFLD_SYSCON_PDSLEEPCFG_BOD_PD        (0x00000001UL <<  3U)   /* (RW) BOD power-down control for Deep-sleep and Power-down mode */
#define CHIP_REGFLD_SYSCON_PDSLEEPCFG_WDTOSC_PD     (0x00000001UL <<  6U)   /* (RW) Watchdog oscillator power-down control for Deep-sleep and Power-down mode */
#define CHIP_REGMSK_SYSCON_PDSLEEPCFG_RESERVED      (0xFFFFFFB7UL)

/*********** Wake-up configuration register (SYSCON_PDAWAKECFG) ***************/
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_IRCOUT_PD     (0x00000001UL <<  0U)   /* (RW) IRC oscillator output wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_IRC_PD        (0x00000001UL <<  1U)   /* (RW) IRC oscillator power-down wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_FLASH_PD      (0x00000001UL <<  2U)   /* (RW) Flash wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_BOD_PD        (0x00000001UL <<  3U)   /* (RW) BOD wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_ADC_PD        (0x00000001UL <<  4U)   /* (RW) ADC wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_SYSOSC_PD     (0x00000001UL <<  5U)   /* (RW) Crystal oscillator wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_WDTOSC_PD     (0x00000001UL <<  6U)   /* (RW) Watchdog oscillator wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_SYSPLL_PD     (0x00000001UL <<  7U)   /* (RW) System PLL wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_USBPLL_PD     (0x00000001UL <<  8U)   /* (RW) USB PLL wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_USBPAD_PD     (0x00000001UL << 10U)   /* (RW) USB transceiver wake-up configuration */
#define CHIP_REGFLD_SYSCON_PDAWAKECFG_TEMPSENSE_PD  (0x00000001UL << 13U)   /* (RW) Temperature sensor wake-up configuration  */
#define CHIP_REGMSK_SYSCON_PDAWAKECFG_RESERVED      (0xFFFFDA00UL)

/*********** Power configuration register (SYSCON_PDRUNCFG) *******************/
#define CHIP_REGFLD_SYSCON_PDRUNCFG_IRCOUT_PD       (0x00000001UL <<  0U)   /* (RW) IRC oscillator output power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_IRC_PD          (0x00000001UL <<  1U)   /* (RW) IRC oscillator power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_FLASH_PD        (0x00000001UL <<  2U)   /* (RW) Flash power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_BOD_PD          (0x00000001UL <<  3U)   /* (RW) BOD power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_ADC_PD          (0x00000001UL <<  4U)   /* (RW) ADC power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_SYSOSC_PD       (0x00000001UL <<  5U)   /* (RW) Crystal oscillator power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_WDTOSC_PD       (0x00000001UL <<  6U)   /* (RW) Watchdog oscillator  power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_SYSPLL_PD       (0x00000001UL <<  7U)   /* (RW) System PLL power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_USBPLL_PD       (0x00000001UL <<  8U)   /* (RW) USB PLL power-down */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_USBPAD_PD       (0x00000001UL << 10U)   /* (RW) USB transceiver power-down  */
#define CHIP_REGFLD_SYSCON_PDRUNCFG_TEMPSENSE_PD    (0x00000001UL << 13U)   /* (RW) Temperature sensor power-down */
#define CHIP_REGMSK_SYSCON_PDRUNCFG_RESERVED        (0xFFFFDA00UL)


#endif /*!< CHIP_LPC11U6X_REGS_SYSCON_H */
