#ifndef CHIP_PER_CTL_H
#define CHIP_PER_CTL_H


#include "chip_mmap.h"
#include "lcspec.h"
#include "lbitfield.h"
#include "regs/regs_ccm.h"
#include "chip_domain_defs.h"

#define CHIP_CLK_ON_NONE        0 /* Domain clocks not needed */
#define CHIP_CLK_ON_RUN         1 /* Domain clocks needed when in RUN */
#define CHIP_CLK_ON_RUN_WAIT    2 /* Domain clocks needed when in RUN and WAIT */
#define CHIP_CLK_ON_ALL         3 /* Domain clocks needed all the time */
#define CHIP_CLK_ON_DEFAULT     CHIP_CLK_ON_RUN_WAIT



#define CHIP_PER_CLK_NUM_DVFS       0 /* ? */
#define CHIP_PER_CLK_NUM_ANAMIX     1
#define CHIP_PER_CLK_NUM_CPU        2
#define CHIP_PER_CLK_NUM_CSU        3
#define CHIP_PER_CLK_NUM_DEBUG      4
#define CHIP_PER_CLK_NUM_DRAM       5
#define CHIP_PER_CLK_NUM_ECSPI1     7
#define CHIP_PER_CLK_NUM_ECSPI2     8
#define CHIP_PER_CLK_NUM_ECSPI3     9
#define CHIP_PER_CLK_NUM_ENET1      10
#define CHIP_PER_CLK_NUM_GPIO1      11
#define CHIP_PER_CLK_NUM_GPIO2      12
#define CHIP_PER_CLK_NUM_GPIO3      13
#define CHIP_PER_CLK_NUM_GPIO4      14
#define CHIP_PER_CLK_NUM_GPIO5      15
#define CHIP_PER_CLK_NUM_GPT1       16
#define CHIP_PER_CLK_NUM_GPT2       17
#define CHIP_PER_CLK_NUM_GPT3       18
#define CHIP_PER_CLK_NUM_GPT4       19
#define CHIP_PER_CLK_NUM_GPT5       120
#define CHIP_PER_CLK_NUM_GPT6       21
#define CHIP_PER_CLK_NUM_HS         22
#define CHIP_PER_CLK_NUM_I2C1       23
#define CHIP_PER_CLK_NUM_I2C2       24
#define CHIP_PER_CLK_NUM_I2C3       25
#define CHIP_PER_CLK_NUM_I2C4       26
#define CHIP_PER_CLK_NUM_IOMUX      27
#define CHIP_PER_CLK_NUM_IPMUX1     28 /* AIPSTZ1 */
#define CHIP_PER_CLK_NUM_IPMUX2     29 /* AIPSTZ2 */
#define CHIP_PER_CLK_NUM_IPMUX3     30 /* AIPSTZ3 */
#define CHIP_PER_CLK_NUM_MU         33
#define CHIP_PER_CLK_NUM_OCOTP      34
#define CHIP_PER_CLK_NUM_OCRAM      35
#define CHIP_PER_CLK_NUM_OCRAMS     36
#define CHIP_PER_CLK_NUM_PCIE       37
#define CHIP_PER_CLK_NUM_PERFMON1   38
#define CHIP_PER_CLK_NUM_PERFMON2   39
#define CHIP_PER_CLK_NUM_PWM1       40
#define CHIP_PER_CLK_NUM_PWM2       41
#define CHIP_PER_CLK_NUM_PWM3       42
#define CHIP_PER_CLK_NUM_PWM4       43
#define CHIP_PER_CLK_NUM_QOS        44
#define CHIP_PER_CLK_NUM_QOS_ENET   46
#define CHIP_PER_CLK_NUM_QSPI       47
#define CHIP_PER_CLK_NUM_RAWNAND    48 /* NAND_USDHC */
#define CHIP_PER_CLK_NUM_RDC        49
#define CHIP_PER_CLK_NUM_ROM        50
#define CHIP_PER_CLK_NUM_I2C5       51
#define CHIP_PER_CLK_NUM_I2C6       52
#define CHIP_PER_CLK_NUM_CAN1       53
#define CHIP_PER_CLK_NUM_CAN2       54
#define CHIP_PER_CLK_NUM_SYSCTR     57
#define CHIP_PER_CLK_NUM_SDMA1      58
#define CHIP_PER_CLK_NUM_ENET_QOS   59
#define CHIP_PER_CLK_NUM_SEC_DEBUG  60
#define CHIP_PER_CLK_NUM_SEMA1      61
#define CHIP_PER_CLK_NUM_SEMA2      62
#define CHIP_PER_CLK_NUM_IRQ_STEER  63
#define CHIP_PER_CLK_NUM_SIM_ENET   64
#define CHIP_PER_CLK_NUM_SIM_M      65
#define CHIP_PER_CLK_NUM_SIM_MAIN   66
#define CHIP_PER_CLK_NUM_SIM_S      67
#define CHIP_PER_CLK_NUM_SIM_WAKEUP 68
#define CHIP_PER_CLK_NUM_GPU2D      69
#define CHIP_PER_CLK_NUM_GPU3D      70
#define CHIP_PER_CLK_NUM_SNVS       71
#define CHIP_PER_CLK_NUM_ARM_TRACE  72
#define CHIP_PER_CLK_NUM_UART1      73
#define CHIP_PER_CLK_NUM_UART2      74
#define CHIP_PER_CLK_NUM_UART3      75
#define CHIP_PER_CLK_NUM_UART4      76
#define CHIP_PER_CLK_NUM_USB        77
#define CHIP_PER_CLK_NUM_USB_PHY    79
#define CHIP_PER_CLK_NUM_USDHC1     81
#define CHIP_PER_CLK_NUM_USDHC2     82
#define CHIP_PER_CLK_NUM_WDOG1      83
#define CHIP_PER_CLK_NUM_WDOG2      84
#define CHIP_PER_CLK_NUM_WDOG3      85
#define CHIP_PER_CLK_NUM_VPU_G1     86
#define CHIP_PER_CLK_NUM_GPU        87
#define CHIP_PER_CLK_NUM_NOC_WRAP   88
#define CHIP_PER_CLK_NUM_VPU_VC8KE  89
#define CHIP_PER_CLK_NUM_VPU_G2     90
#define CHIP_PER_CLK_NUM_NPU        91
#define CHIP_PER_CLK_NUM_HSIO       92
#define CHIP_PER_CLK_NUM_MEDIA      93
#define CHIP_PER_CLK_NUM_USDHC3     94
#define CHIP_PER_CLK_NUM_HDMI       95
#define CHIP_PER_CLK_NUM_XTAL       96
#define CHIP_PER_CLK_NUM_PLL        97
#define CHIP_PER_CLK_NUM_TSENSOR    98
#define CHIP_PER_CLK_NUM_VPU        99
#define CHIP_PER_CLK_NUM_MRPR       100
#define CHIP_PER_CLK_NUM_AUDIO      101 /* ASR (AUDIOMUX) */




static LINLINE void chip_per_clk_on_mode_set(uint8_t perclk, uint8_t mode, uint8_t domain) {
    CHIP_REGS_CCM_GRP_T *gprreg = &CHIP_REGS_CCM->CCGR[perclk];
    const uint32_t mask = CHIP_REGFLD_CCM_CCGR_SET(domain);
    const uint32_t val = LBITFIELD_SET(CHIP_REGFLD_CCM_CCGR_SET(domain), mode);
    const uint32_t curval = gprreg->VAL & mask;
    const uint32_t diff_val = curval ^ val;
    if (diff_val != 0) {
        gprreg->TOG = diff_val;
    }
}

static LINLINE void chip_per_clk_en(uint8_t perclk) {
    chip_per_clk_on_mode_set(perclk, CHIP_CLK_ON_DEFAULT, CHIP_CUR_DOMAIN_NUM);
}
static LINLINE void chip_per_clk_dis(uint8_t perclk) {
    chip_per_clk_on_mode_set(perclk, CHIP_CLK_ON_NONE, CHIP_CUR_DOMAIN_NUM);
}
static LINLINE int chip_per_clk_is_en(uint8_t perclk) {
    return LBITFIELD_GET(CHIP_REGS_CCM->CCGR[perclk].VAL, CHIP_REGFLD_CCM_CCGR_SET(CHIP_CUR_DOMAIN_NUM));
}


#endif // CHIP_PER_CTL_H

