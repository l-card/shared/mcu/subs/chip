#ifndef CHIP_RISCV_CSR_H
#define CHIP_RISCV_CSR_H

#define CHIP_CSR_USTATUS            (0x000) /* User mode status (x) */
#define CHIP_CSR_UIE                (0x004) /* User mode interrupt enable (x) */
#define CHIP_CSR_UTVEC              (0x005) /* User mode trap vector base address (x) */
#define CHIP_CSR_USCRATCH           (0x040) /* User mode scratch (x) */
#define CHIP_CSR_UEPC               (0x041) /* User mode exception program counter (x) */
#define CHIP_CSR_UCAUSE             (0x042) /* User mode trap cause (x) */
#define CHIP_CSR_UTVAL              (0x043) /* User mode trap value (x) */
#define CHIP_CSR_UIP                (0x044) /* User mode interrupt pending (x) */
#define CHIP_CSR_SSTATUS            (0x100) /* Supervisor mode status */
#define CHIP_CSR_SEDELEG            (0x102) /* Supervisor mode exception delegation */
#define CHIP_CSR_SIDELEG            (0x103) /* Supervisor mode interrupt delegation */
#define CHIP_CSR_SIE                (0x104) /* Supervisor mode interrupt enable */
#define CHIP_CSR_STVEC              (0x105) /* Supervisor mode trap vector base address */
#define CHIP_CSR_SCOUNTEREN         (0x106) /* Supervisor mode counter enable register */
#define CHIP_CSR_SSCRATCH           (0x140) /* Supervisor mode scratch register */
#define CHIP_CSR_SEPC               (0x141) /* Supervisor mode exception program counter */
#define CHIP_CSR_SCAUSE             (0x142) /* Supervisor mode cause register */
#define CHIP_CSR_STVAL              (0x143) /* Supervisor mode trap value register */
#define CHIP_CSR_SIP                (0x144) /* Supervisor mode interrupt pending */
#define CHIP_CSR_SATP               (0x180) /* Supervisor mode address translation and protection */
#define CHIP_CSR_MSTATUS            (0x300) /* Machine mode status */
#define CHIP_CSR_MISA               (0x301) /* Machine mode ISA */
#define CHIP_CSR_MEDELEG            (0x302) /* Machine mode exception delegation */
#define CHIP_CSR_MIDELEG            (0x303) /* Machine mode interrupt delegation */
#define CHIP_CSR_MIE                (0x304) /* Machine mode interrupt enable */
#define CHIP_CSR_MTVEC              (0x305) /* Machine mode trap vector base address */
#define CHIP_CSR_MCOUNTEREN         (0x306) /* Machine mode counter enable register */
#define CHIP_CSR_MCOUNTINHIBIT      (0x320) /* Machine mode counter inhibit */
#define CHIP_CSR_MHPMEVENT3         (0x323) /* Machine mode performance monitoring event selector */
#define CHIP_CSR_MHPMEVENT4         (0x324) /* Machine mode performance monitoring event selector */
#define CHIP_CSR_MHPMEVENT5         (0x325) /* Machine mode performance monitoring event selector */
#define CHIP_CSR_MHPMEVENT6         (0x326) /* Machine mode performance monitoring event selector */
#define CHIP_CSR_MSCRATCH           (0x340) /* Machine mode scratch register */
#define CHIP_CSR_MEPC               (0x341) /* Machine mode exception program counter  */
#define CHIP_CSR_MCAUSE             (0x342) /* Machine mode trap cause */
#define CHIP_CSR_MTVAL              (0x343) /* Machine mode trap value register */
#define CHIP_CSR_MIP                (0x344) /* Machine mode interrupt pending */
#define CHIP_CSR_PMPCFG0            (0x3A0) /* PMP configuration register 0 */
#define CHIP_CSR_PMPCFG1            (0x3A1) /* PMP configuration register 1 */
#define CHIP_CSR_PMPCFG2            (0x3A2) /* PMP configuration register 2 */
#define CHIP_CSR_PMPCFG3            (0x3A3) /* PMP configuration register 3 */
#define CHIP_CSR_PMPADDR0           (0x3B0) /* PMP address register 0 */
#define CHIP_CSR_PMPADDR1           (0x3B1) /* PMP address register 1 */
#define CHIP_CSR_PMPADDR2           (0x3B2) /* PMP address register 2 */
#define CHIP_CSR_PMPADDR3           (0x3B3) /* PMP address register 3 */
#define CHIP_CSR_PMPADDR4           (0x3B4) /* PMP address register 4 */
#define CHIP_CSR_PMPADDR5           (0x3B5) /* PMP address register 5 */
#define CHIP_CSR_PMPADDR6           (0x3B6) /* PMP address register 6 */
#define CHIP_CSR_PMPADDR7           (0x3B7) /* PMP address register 7 */
#define CHIP_CSR_PMPADDR8           (0x3B8) /* PMP address register 8 */
#define CHIP_CSR_PMPADDR9           (0x3B9) /* PMP address register 9 */
#define CHIP_CSR_PMPADDR10          (0x3BA) /* PMP address register 10 */
#define CHIP_CSR_PMPADDR11          (0x3BB) /* PMP address register 11 */
#define CHIP_CSR_PMPADDR12          (0x3BC) /* PMP address register 12 */
#define CHIP_CSR_PMPADDR13          (0x3BD) /* PMP address register 13 */
#define CHIP_CSR_PMPADDR14          (0x3BE) /* PMP address register 14 */
#define CHIP_CSR_PMPADDR15          (0x3BF) /* PMP address register 15 */
#define CHIP_CSR_TSELECT            (0x7A0) /* Trigger selection */
#define CHIP_CSR_TDATA1             (0x7A1) /* Trigger data 1 */
#define CHIP_CSR_MCONTROL           (0x7A1) /* Match control */
#define CHIP_CSR_ICOUNT             (0x7A1) /* Instruction count */
#define CHIP_CSR_ITRIGGER           (0x7A1) /* Interrupt trigger */
#define CHIP_CSR_ETRIGGER           (0x7A1) /* Exception trigger */
#define CHIP_CSR_TDATA2             (0x7A2) /* Trigger data 2 */
#define CHIP_CSR_TDATA3             (0x7A3) /* Trigger data 3 */
#define CHIP_CSR_TEXTRA             (0x7A3) /* Extra trigger */
#define CHIP_CSR_TINFO              (0x7A4) /* Trigger information */
#define CHIP_CSR_TCONTROL           (0x7A5) /* Trigger control */
#define CHIP_CSR_MCONTEXT           (0x7A8) /* Machine mode context */
#define CHIP_CSR_SCONTEXT           (0x7AA) /* Supervisor mode context */
#define CHIP_CSR_DCSR               (0x7B0) /* Debug control and status registers */
#define CHIP_CSR_DPC                (0x7B1) /* Debug program counter */
#define CHIP_CSR_DSCRATCH0          (0x7B2) /* Debug scratchpad 0 */
#define CHIP_CSR_DSCRATCH1          (0x7B3) /* Debug scratchpad 1 */
#define CHIP_CSR_MCYCLE             (0xB00) /* Machine mode cycle counter */
#define CHIP_CSR_MINSTRET           (0xB02) /* Machine instructions-retired counter */
#define CHIP_CSR_MHPMCOUNTER3       (0xB03) /* Machine performance-monitoring counter */
#define CHIP_CSR_MHPMCOUNTER4       (0xB04) /* Machine performance-monitoring counter */
#define CHIP_CSR_MHPMCOUNTER5       (0xB05) /* Machine performance-monitoring counter */
#define CHIP_CSR_MHPMCOUNTER6       (0xB06) /* Machine performance-monitoring counter */
#define CHIP_CSR_MCYCLEH            (0xB80) /* Machine mode cycle counter */
#define CHIP_CSR_MINSTRETH          (0xB82) /* Machine instructions-retired counter */
#define CHIP_CSR_MHPMCOUNTER3H      (0xB83) /* Machine performance-monitoring counter */
#define CHIP_CSR_MHPMCOUNTER4H      (0xB84) /* Machine performance-monitoring counter */
#define CHIP_CSR_MHPMCOUNTER5H      (0xB85) /* Machine performance-monitoring counter */
#define CHIP_CSR_MHPMCOUNTER6H      (0xB86) /* Machine performance-monitoring counter */
#define CHIP_CSR_PMACFG0            (0xBC0) /* PMA configuration register */
#define CHIP_CSR_PMACFG1            (0xBC1) /* PMA configuration register */
#define CHIP_CSR_PMACFG2            (0xBC2) /* PMA configuration register */
#define CHIP_CSR_PMACFG3            (0xBC3) /* PMA configuration register */
#define CHIP_CSR_PMAADDR0           (0xBD0) /* PMA address register */
#define CHIP_CSR_PMAADDR1           (0xBD1) /* PMA address register */
#define CHIP_CSR_PMAADDR2           (0xBD2) /* PMA address register */
#define CHIP_CSR_PMAADDR3           (0xBD3) /* PMA address register */
#define CHIP_CSR_PMAADDR4           (0xBD4) /* PMA address register */
#define CHIP_CSR_PMAADDR5           (0xBD5) /* PMA address register */
#define CHIP_CSR_PMAADDR6           (0xBD6) /* PMA address register */
#define CHIP_CSR_PMAADDR7           (0xBD7) /* PMA address register */
#define CHIP_CSR_PMAADDR8           (0xBD8) /* PMA address register */
#define CHIP_CSR_PMAADDR9           (0xBD9) /* PMA address register */
#define CHIP_CSR_PMAADDR10          (0xBDA) /* PMA address register */
#define CHIP_CSR_PMAADDR11          (0xBDB) /* PMA address register */
#define CHIP_CSR_PMAADDR12          (0xBDC) /* PMA address register */
#define CHIP_CSR_PMAADDR13          (0xBDD) /* PMA address register */
#define CHIP_CSR_PMAADDR14          (0xBDE) /* PMA address register */
#define CHIP_CSR_PMAADDR15          (0xBDF) /* PMA address register */
#define CHIP_CSR_CYCLE              (0xC00) /* CYCLE register */
#define CHIP_CSR_CYCLEH             (0xC80) /* CYCLE high 32-bit register */
#define CHIP_CSR_MVENDORID          (0xF11) /* Machine vendor ID register */
#define CHIP_CSR_MARCHID            (0xF12) /* Machine mode architecture ID register */
#define CHIP_CSR_MIMPID             (0xF13) /* Machine mode implementation ID register */
#define CHIP_CSR_MHARTID            (0xF14) /* HartID register */






/*********** User mode status - USTATUS (0x000) *******************************/
#define CHIP_REGFLD_CSR_USTATUS_UPIE                    (0x00000001UL <<  4U) /* (RW) UPIE saves the value of the UIE bit before the exception */
#define CHIP_REGFLD_CSR_USTATUS_UIE                     (0x00000001UL <<  0U) /* (RW) U-mode interrupt enable */
#define CHIP_REGMSK_CSR_USTATUS_RESERVED                (0xFFFFFFEEUL)

/*********** User mode interrupt enable - UIE (0x004) *************************/
#define CHIP_REGFLD_CSR_UIE_UEIE                        (0x00000001UL <<  8U) /* (RW) U-mode external interrupt enable */
#define CHIP_REGFLD_CSR_UIE_UTIE                        (0x00000001UL <<  4U) /* (RW) U-mode timer interrupt enable */
#define CHIP_REGFLD_CSR_UIE_USIE                        (0x00000001UL <<  0U) /* (RW) U-mode software interrupt enable */
#define CHIP_REGMSK_CSR_UIE_RESERVED                    (0xFFFFFEEEUL)

/*********** User mode trap cause - UCAUSE (0x042) ****************************/
#define CHIP_REGFLD_CSR_UCAUSE_INTERRUPT                (0x00000001UL << 31U) /* (RW) If the trap was caused by an interrupt*/
#define CHIP_REGFLD_CSR_UCAUSE_EXCEPTION_CODE           (0x00000FFFUL <<  0U) /* (RW) Exception code (see mcause) */

/*********** User mode interrupt pending - UIP (0x044) ************************/
#define CHIP_REGFLD_CSR_UIP_UEIP                        (0x00000001UL <<  8U) /* (RW) U-mode external interrupt pending bit. */
#define CHIP_REGFLD_CSR_UIP_UTIP                        (0x00000001UL <<  4U) /* (RW) U-mode timer interrupt pending bit. */
#define CHIP_REGFLD_CSR_UIP_USIP                        (0x00000001UL <<  0U) /* (RW) U-mode software interrupt pending bit. */
#define CHIP_REGMSK_CSR_UIP_RESERVED                    (0xFFFFFEEEUL)


/*********** Supervisor mode status - SSTATUS (0x100) *************************/
#define CHIP_REGFLD_CSR_SSTATUS_SD                      (0x00000001UL << 31U) /* (RO) Summarize of FS or XS */
#define CHIP_REGFLD_CSR_SSTATUS_MXR                     (0x00000001UL << 19U) /* (RW) Make eXecutable Readable. Execution only pages is readable */
#define CHIP_REGFLD_CSR_SSTATUS_SUM                     (0x00000001UL << 18U) /* (RW) Permit Supervisor User Memory access */
#define CHIP_REGFLD_CSR_SSTATUS_XS                      (0x00000003UL << 15U) /* (RO) Architectural state (ACE register) of the ACE instruction  */
#define CHIP_REGFLD_CSR_SSTATUS_FS                      (0x00000003UL << 13U) /* (RW) Floating point unit architectural state */
#define CHIP_REGFLD_CSR_SSTATUS_SPP                     (0x00000001UL <<  8U) /* (RW) Privileged mode before trap */
#define CHIP_REGFLD_CSR_SSTATUS_SPIE                    (0x00000001UL <<  5U) /* (RW) SIE bit before trap */
#define CHIP_REGFLD_CSR_SSTATUS_UPIE                    (0x00000001UL <<  4U) /* (RW) UIE bit before trap */
#define CHIP_REGFLD_CSR_SSTATUS_SIE                     (0x00000001UL <<  1U) /* (RW) S‑mode interrupt enable bit. */
#define CHIP_REGFLD_CSR_SSTATUS_UIE                     (0x00000001UL <<  0U) /* (RW) U‑mode interrupt enable bit. */

#define CHIP_REGFLDVAL_CSR_SSTATUS_XS_OFF               0 /* All off */
#define CHIP_REGFLDVAL_CSR_SSTATUS_XS_INITIAL           1 /* None dirty or clean, some on */
#define CHIP_REGFLDVAL_CSR_SSTATUS_XS_CLEAN             2 /* None dirty, some clean */
#define CHIP_REGFLDVAL_CSR_SSTATUS_XS_DIRTY             3 /* Some dirty */

#define CHIP_REGFLDVAL_CSR_SSTATUS_FS_OFF               0
#define CHIP_REGFLDVAL_CSR_SSTATUS_FS_INITIAL           1
#define CHIP_REGFLDVAL_CSR_SSTATUS_FS_CLEAN             2
#define CHIP_REGFLDVAL_CSR_SSTATUS_FS_DIRTY             3

#define CHIP_REGFLDVAL_CSR_SSTATUS_SPP_SMODE            1
#define CHIP_REGFLDVAL_CSR_SSTATUS_SPP_UMODE            0

/***********  Supervisor mode exception delegation - SEDELEG (0x102) **********/
#define CHIP_REGFLD_CSR_SEDELEG_SPF                     (0x00000001UL << 15U) /* (RW) delegate Store/AMO page fault exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_LPF                     (0x00000001UL << 13U) /* (RW) delegate loading page error exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_IPF                     (0x00000001UL << 12U) /* (RW) delegate instruction page fault exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_UEC                     (0x00000001UL <<  8U) /* (RW) delegate exceptions triggered by environment calls from U‑mode to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_SAF                     (0x00000001UL <<  7U) /* (RW) delegate Store/AMO access failure exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_SAM                     (0x00000001UL <<  6U) /* (RW) delegate Store/AMO address misalignment exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_LAF                     (0x00000001UL <<  5U) /* (RW) delegate load access failure exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_LAM                     (0x00000001UL <<  4U) /* (RW) delegate load address misalignment exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_B                       (0x00000001UL <<  3U) /* (RW) delegate exceptions triggered by breakpoints to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_II                      (0x00000001UL <<  2U) /* (RW) delegate illegal instruction exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_IAF                     (0x00000001UL <<  1U) /* (RW) delegate instruction access error exceptions to U‑mode */
#define CHIP_REGFLD_CSR_SEDELEG_IAM                     (0x00000001UL <<  0U) /* (RW) delegate instruction address misalignment exceptions to U‑mode */

/***********  Supervisor mode interrupt delegation - SIDELEG (0x103) **********/
#define CHIP_REGFLD_CSR_SIDELEG_UEI                     (0x00000001UL <<  8U) /* (RW) delegate U‑mode external interrupts to S‑mode */
#define CHIP_REGFLD_CSR_SIDELEG_UTI                     (0x00000001UL <<  4U) /* (RW) delegate U‑mode timer interrupts to S‑mode */
#define CHIP_REGFLD_CSR_SIDELEG_USI                     (0x00000001UL <<  0U) /* (RW) delegate U‑mode software interrupts to S-mode */


/***********  Supervisor mode interrupt delegation - SIE (0x104) **************/
#define CHIP_REGFLD_CSR_SIE_SEIE                        (0x00000001UL <<  9U) /* (RW) S‑mode external interrupt enable */
#define CHIP_REGFLD_CSR_SIE_UEIE                        (0x00000001UL <<  8U) /* (RW) U‑mode external interrupt enable */
#define CHIP_REGFLD_CSR_SIE_STIE                        (0x00000001UL <<  5U) /* (RW) S‑mode timer interrupt enable */
#define CHIP_REGFLD_CSR_SIE_UTIE                        (0x00000001UL <<  4U) /* (RW) U‑mode timer interrupt enable */
#define CHIP_REGFLD_CSR_SIE_SSIE                        (0x00000001UL <<  1U) /* (RW) S‑mode software interrupt enable */
#define CHIP_REGFLD_CSR_SIE_USIE                        (0x00000001UL <<  0U) /* (RW) U‑mode software interrupt enable */

/***********  Supervisor mode counter enable register - SCOUNTEREN (0x106) ****/
#define CHIP_REGFLD_CSR_SCOUNTEREN_HPM6                 (0x00000001UL <<  6U) /* (RW) hpmcounter6 register U-mode access enable */
#define CHIP_REGFLD_CSR_SCOUNTEREN_HPM5                 (0x00000001UL <<  5U) /* (RW) hpmcounter5 register U-mode access enable */
#define CHIP_REGFLD_CSR_SCOUNTEREN_HPM4                 (0x00000001UL <<  4U) /* (RW) hpmcounter4 register U-mode access enable */
#define CHIP_REGFLD_CSR_SCOUNTEREN_HPM3                 (0x00000001UL <<  3U) /* (RW) hpmcounter3 register U-mode access enable */
#define CHIP_REGFLD_CSR_SCOUNTEREN_IR                   (0x00000001UL <<  2U) /* (RW) instret register U-mode access enable */
#if CHIP_RISCV_FEAT_UMODE_TIME_REG
#define CHIP_REGFLD_CSR_SCOUNTEREN_TM                   (0x00000001UL <<  1U) /* (RW) time register U-mode access enable */
#endif
#define CHIP_REGFLD_CSR_SCOUNTEREN_CY                   (0x00000001UL <<  0U) /* (RW) cycle register U-mode access enable */


/*********** Supervisor mode trap cause - MCAUSE (0x142) *************************/
#define CHIP_REGFLD_CSR_SCAUSE_INTERRUPT                (0x00000001UL << 31U) /* (RW) If the trap was caused by an interrupt*/
#define CHIP_REGFLD_CSR_SCAUSE_EXCEPTION_CODE           (0x00000FFFUL <<  0U) /* (RW) Exception code */

#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_U_SW          0  /* User software interrupt */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_S_SW          1  /* Supervisor software interrupt */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_U_TMR         4  /* User timer interrupt */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_S_TMR         5  /* Supervisor timer interrupt */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_U_EXT         8  /* User external interrupt */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_S_EXT         9  /* Supervisor external interrupt */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_S_ECC         (256+16) /* Bus write transaction error interrupt (S-mode) */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_S_BUS_WR      (256+17) /* Slave port ECC error interrupt (S-mode) */
#define CHIP_REGFLDVAL_CSR_SCAUSE_INTCODE_S_PMON        (256+18) /* Performance monitor overflow interrupt (S-mode) */

#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_IA_MALIGN     0  /* Instruction address misaligned */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_INST_ACC_FLT  1  /* Instruction access faul */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_INST_ILLEGAL  2  /* Illegal instruction */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_BREAKPT       3  /* Breakpoint */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_LDA_MALIGN    4  /* Load address misaligned */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_LD_ACC_FLT    5  /* Load access faul */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_STA_MALIGN    6  /* Store/AMO address misaligned */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_ST_ACC_FLT    7  /* Store/AMO access faul */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_U_ENV_CALL    8  /* Environment call from U-mode */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_S_ENV_CALL    9  /* Environment call from S-mode */

#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_INST_PAGE_FLT 12 /* Instruction page fault */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_LD_PAGE_FLT   13 /* Load page fault */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_ST_PAGE_FLT   15 /* Store/AMO page fault */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_STACK_OVF     32 /* Stack overflow exception */
#define CHIP_REGFLDVAL_CSR_SCAUSE_EXCCODE_STACK_UVF     33 /* Stack underflow exception */


/*********** Supervisor address translation and protection - SATP (0x180) *****/
#define CHIP_REGFLD_CSR_SATP_MODE                       (0x00000001UL << 31U) /* (RW) Page translation mode (0 - bare, 1 - page translateion) */
#define CHIP_REGFLD_CSR_SATP_ASID                       (0x000001FFUL << 22U) /* (RW) Address space identifier */
#define CHIP_REGFLD_CSR_SATP_PPN                        (0x003FFFFFUL <<  0U) /* (RW) Physical page number of the root page table */


/*********** Machine mode status - MSTATUS (0x300) ****************************/
#define CHIP_REGFLD_CSR_MSTATUS_SD                      (0x00000001UL << 31U) /* (RO) Summarize of FS or XS */
#define CHIP_REGFLD_CSR_MSTATUS_TSR                     (0x00000001UL << 22U) /* (RW) Trap SRET. SRET instruction throw exception in S-mode */
#define CHIP_REGFLD_CSR_MSTATUS_TW                      (0x00000001UL << 21U) /* (RW) Timeout Wait. WFI instruction throw exception in S-mode */
#define CHIP_REGFLD_CSR_MSTATUS_TWM                     (0x00000001UL << 20U) /* (RW) Trap Virtual Memory. Virtual memory operation throw exception in S-mode */
#define CHIP_REGFLD_CSR_MSTATUS_MXR                     (0x00000001UL << 19U) /* (RW) Make eXecutable Readable. Execution only pages is readable */
#define CHIP_REGFLD_CSR_MSTATUS_SUM                     (0x00000001UL << 18U) /* (RW) Permit Supervisor User Memory access */
#define CHIP_REGFLD_CSR_MSTATUS_MPVR                    (0x00000001UL << 17U) /* (RW) Modify PRiVilege. Memory access permissions for loads and stores are specified by the MPP  */
#define CHIP_REGFLD_CSR_MSTATUS_XS                      (0x00000003UL << 15U) /* (RO) Architectural state (ACE register) of the ACE instruction  */
#define CHIP_REGFLD_CSR_MSTATUS_FS                      (0x00000003UL << 13U) /* (RW) Floating point unit architectural state */
#define CHIP_REGFLD_CSR_MSTATUS_MPP                     (0x00000003UL << 11U) /* (RW) Privileged mode before trap */
#if CHIP_RISCV_FEAT_VECTOR_EXT
#define CHIP_REGFLD_CSR_MSTATUS_VS                      (0x00000003UL <<  9U) /* (RW) Status of the vector extension state */
#endif
#define CHIP_REGFLD_CSR_MSTATUS_SPP                     (0x00000001UL <<  8U) /* (RW) Privileged mode before trap */
#define CHIP_REGFLD_CSR_MSTATUS_MPIE                    (0x00000001UL <<  7U) /* (RW) MIE bit before trap */
#define CHIP_REGFLD_CSR_MSTATUS_SPIE                    (0x00000001UL <<  5U) /* (RW) SIE bit before trap */
#define CHIP_REGFLD_CSR_MSTATUS_UPIE                    (0x00000001UL <<  4U) /* (RW) UIE bit before trap */
#define CHIP_REGFLD_CSR_MSTATUS_MIE                     (0x00000001UL <<  3U) /* (RW) M‑mode interrupt enable bit. */
#define CHIP_REGFLD_CSR_MSTATUS_SIE                     (0x00000001UL <<  1U) /* (RW) S‑mode interrupt enable bit. */
#define CHIP_REGFLD_CSR_MSTATUS_UIE                     (0x00000001UL <<  0U) /* (RW) U‑mode interrupt enable bit. */

#define CHIP_REGFLDVAL_CSR_MSTATUS_XS_OFF               0 /* All off */
#define CHIP_REGFLDVAL_CSR_MSTATUS_XS_INITIAL           1 /* None dirty or clean, some on */
#define CHIP_REGFLDVAL_CSR_MSTATUS_XS_CLEAN             2 /* None dirty, some clean */
#define CHIP_REGFLDVAL_CSR_MSTATUS_XS_DIRTY             3 /* Some dirty */

#define CHIP_REGFLDVAL_CSR_MSTATUS_FS_OFF               0
#define CHIP_REGFLDVAL_CSR_MSTATUS_FS_INITIAL           1
#define CHIP_REGFLDVAL_CSR_MSTATUS_FS_CLEAN             2
#define CHIP_REGFLDVAL_CSR_MSTATUS_FS_DIRTY             3


/*********** Machine mode ISA - MISA (0x301) **********************************/
#define CHIP_REGFLD_CSR_MISA_XL                         (0x00000003UL << 29U) /* (RO) Machine XLEN - integer ISA width */
#define CHIP_REGFLD_CSR_MISA_Z                          (0x00000001UL << 25U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_Y                          (0x00000001UL << 24U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_X                          (0x00000001UL << 23U) /* (RO) Non-standard extensions present */
#define CHIP_REGFLD_CSR_MISA_W                          (0x00000001UL << 22U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_V                          (0x00000001UL << 21U) /* (RO) Tentatively reserved for Vector extension */
#define CHIP_REGFLD_CSR_MISA_U                          (0x00000001UL << 20U) /* (RO) User mode implemented */
#define CHIP_REGFLD_CSR_MISA_T                          (0x00000001UL << 19U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_S                          (0x00000001UL << 18U) /* (RO) Supervisor mode implemented */
#define CHIP_REGFLD_CSR_MISA_R                          (0x00000001UL << 17U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_Q                          (0x00000001UL << 16U) /* (RO) Quad-precision floating-point extension */
#define CHIP_REGFLD_CSR_MISA_P                          (0x00000001UL << 15U) /* (RO) Tentatively reserved for Packed-SIMD extensio */
#define CHIP_REGFLD_CSR_MISA_O                          (0x00000001UL << 14U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_N                          (0x00000001UL << 13U) /* (RO) Tentatively reserved for User-Level Interrupts extension */
#define CHIP_REGFLD_CSR_MISA_M                          (0x00000001UL << 12U) /* (RO) Integer Multiply/Divide extension */
#define CHIP_REGFLD_CSR_MISA_L                          (0x00000001UL << 11U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_K                          (0x00000001UL << 10U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_J                          (0x00000001UL <<  9U) /* (RO) Tentatively reserved for Dynamically Translated Languages extension */
#define CHIP_REGFLD_CSR_MISA_I                          (0x00000001UL <<  8U) /* (RO) RV32I/64I/128I base ISA */
#define CHIP_REGFLD_CSR_MISA_H                          (0x00000001UL <<  7U) /* (RO) Hypervisor extension */
#define CHIP_REGFLD_CSR_MISA_G                          (0x00000001UL <<  6U) /* (RO) Reserved */
#define CHIP_REGFLD_CSR_MISA_F                          (0x00000001UL <<  5U) /* (RO) Single-precision floating-point extension */
#define CHIP_REGFLD_CSR_MISA_E                          (0x00000001UL <<  4U) /* (RO) RV32E base ISA */
#define CHIP_REGFLD_CSR_MISA_D                          (0x00000001UL <<  3U) /* (RO) Double-precision floating-point extension */
#define CHIP_REGFLD_CSR_MISA_C                          (0x00000001UL <<  2U) /* (RO) Compressed extension */
#define CHIP_REGFLD_CSR_MISA_B                          (0x00000001UL <<  1U) /* (RO) Tentatively reserved for Bit-Manipulation extension */
#define CHIP_REGFLD_CSR_MISA_A                          (0x00000001UL <<  0U) /* (RO) Atomic extension */

#define CHIP_REGFLDVAL_CSR_MISA_XL_32                   1
#define CHIP_REGFLDVAL_CSR_MISA_XL_64                   2
#define CHIP_REGFLDVAL_CSR_MISA_XL_128                  3



/***********  Machine mode exception delegation - MEDELEG (0x302) **********/
#define CHIP_REGFLD_CSR_MEDELEG_SPF                     (0x00000001UL << 15U) /* (RW) delegate Store/AMO page fault exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_LPF                     (0x00000001UL << 13U) /* (RW) delegate loading page error exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_IPF                     (0x00000001UL << 12U) /* (RW) delegate instruction page fault exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_SEC                     (0x00000001UL << 98U) /* (RW) delegate exceptions triggered by environment calls from S‑mode to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_UEC                     (0x00000001UL <<  8U) /* (RW) delegate exceptions triggered by environment calls from U‑mode to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_SAF                     (0x00000001UL <<  7U) /* (RW) delegate Store/AMO access failure exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_SAM                     (0x00000001UL <<  6U) /* (RW) delegate Store/AMO address misalignment exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_LAF                     (0x00000001UL <<  5U) /* (RW) delegate load access failure exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_LAM                     (0x00000001UL <<  4U) /* (RW) delegate load address misalignment exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_B                       (0x00000001UL <<  3U) /* (RW) delegate exceptions triggered by breakpoints to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_II                      (0x00000001UL <<  2U) /* (RW) delegate illegal instruction exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_IAF                     (0x00000001UL <<  1U) /* (RW) delegate instruction access error exceptions to S‑mode */
#define CHIP_REGFLD_CSR_MEDELEG_IAM                     (0x00000001UL <<  0U) /* (RW) delegate instruction address misalignment exceptions to S‑mode */


/***********  Machine mode interrupt delegation - MIDELEG (0x303) *************/
#define CHIP_REGFLD_CSR_MIDELEG_SEI                     (0x00000001UL <<  9U) /* (RW) delegate S‑mode external interrupts to S‑mode */
#define CHIP_REGFLD_CSR_MIDELEG_UEI                     (0x00000001UL <<  8U) /* (RW) delegate U‑mode external interrupts to S‑mode */
#define CHIP_REGFLD_CSR_MIDELEG_STI                     (0x00000001UL <<  5U) /* (RW) delegate S‑mode timer interrupts to S‑mode */
#define CHIP_REGFLD_CSR_MIDELEG_UTI                     (0x00000001UL <<  4U) /* (RW) delegate U‑mode timer interrupts to S‑mode */
#define CHIP_REGFLD_CSR_MIDELEG_SSI                     (0x00000001UL <<  1U) /* (RW) delegate S‑mode software interrupts to S-mode */
#define CHIP_REGFLD_CSR_MIDELEG_USI                     (0x00000001UL <<  0U) /* (RW) delegate U‑mode software interrupts to S-mode */


/***********  Machine mode interrupt delegation - MIE (0x304) *****************/
#define CHIP_REGFLD_CSR_MIE_PMOVI                       (0x00000001UL << 18U) /* (RW) Performance monitor overflow local interrupt enable */
#define CHIP_REGFLD_CSR_MIE_BWEI                        (0x00000001UL << 17U) /* (RW) Bus read/write transaction error local interrupt enable. The processor may receive bus errors for load/store instructions or cache writeback. */
#define CHIP_REGFLD_CSR_MIE_IMECCI                      (0x00000001UL << 16U) /* (RW) Imprecise ECC error local interrupt enable. The processor may receive imprecise ECC errors on slave port access or cache writeback. */
#define CHIP_REGFLD_CSR_MIE_MEIE                        (0x00000001UL << 11U) /* (RW) M‑mode external interrupt enable */
#define CHIP_REGFLD_CSR_MIE_SEIE                        (0x00000001UL <<  9U) /* (RW) S‑mode external interrupt enable */
#define CHIP_REGFLD_CSR_MIE_UEIE                        (0x00000001UL <<  8U) /* (RW) U‑mode external interrupt enable */
#define CHIP_REGFLD_CSR_MIE_MTIE                        (0x00000001UL <<  7U) /* (RW) M‑mode timer interrupt enable */
#define CHIP_REGFLD_CSR_MIE_STIE                        (0x00000001UL <<  5U) /* (RW) S‑mode timer interrupt enable */
#define CHIP_REGFLD_CSR_MIE_UTIE                        (0x00000001UL <<  4U) /* (RW) U‑mode timer interrupt enable */
#define CHIP_REGFLD_CSR_MIE_MSIE                        (0x00000001UL <<  3U) /* (RW) M‑mode software interrupt enable */
#define CHIP_REGFLD_CSR_MIE_SSIE                        (0x00000001UL <<  1U) /* (RW) S‑mode software interrupt enable */
#define CHIP_REGFLD_CSR_MIE_USIE                        (0x00000001UL <<  0U) /* (RW) U‑mode software interrupt enable */



/*********** Machine mode trap vector base address - MTVEC (0x305) ************/
#if CHIP_RISCV_FEAT_MTVEC_MODE
#define CHIP_REGFLD_CSR_MTVEC_MODE                      (0x00000003UL <<  0U) /* (RW) Exception code */
#endif
#define CHIP_REGFLD_CSR_MTVEC_BASE                      (0x3FFFFFFFUL <<  2U) /* (RW) vector base address  */

#define CHIP_REGFLDVAL_CSR_MTVEC_MODE_DIRECT            0 /* All exceptions set pc to BASE. */
#define CHIP_REGFLDVAL_CSR_MTVEC_MODE_VECTORED          1 /* Asynchronous interrupts set pc to BASE+4×cause. */

/***********  Machine mode counter enable register - MCOUNTEREN (0x306) *******/
#define CHIP_REGFLD_CSR_MCOUNTEREN_HPM6                 (0x00000001UL <<  6U) /* (RW) hpmcounter6 register S-mode access enable */
#define CHIP_REGFLD_CSR_MCOUNTEREN_HPM5                 (0x00000001UL <<  5U) /* (RW) hpmcounter5 register S-mode access enable */
#define CHIP_REGFLD_CSR_MCOUNTEREN_HPM4                 (0x00000001UL <<  4U) /* (RW) hpmcounter4 register S-mode access enable */
#define CHIP_REGFLD_CSR_MCOUNTEREN_HPM3                 (0x00000001UL <<  3U) /* (RW) hpmcounter3 register S-mode access enable */
#define CHIP_REGFLD_CSR_MCOUNTEREN_IR                   (0x00000001UL <<  2U) /* (RW) instret register S-mode access enable */
#define CHIP_REGFLD_CSR_MCOUNTEREN_TM                   (0x00000001UL <<  1U) /* (RW) time register S-mode access enable */
#define CHIP_REGFLD_CSR_MCOUNTEREN_CY                   (0x00000001UL <<  0U) /* (RW) cycle register S-mode access enable */


/***********  Machine mode counter inhibit register - MCOUNTINHIBIT (0x320) ***/
#define CHIP_REGFLD_CSR_MCOUNTINHIBIT_HPM6              (0x00000001UL <<  6U) /* (RW) hpmcounter6 count inhibit */
#define CHIP_REGFLD_CSR_MCOUNTINHIBIT_HPM5              (0x00000001UL <<  5U) /* (RW) hpmcounter5 count inhibit */
#define CHIP_REGFLD_CSR_MCOUNTINHIBIT_HPM4              (0x00000001UL <<  4U) /* (RW) hpmcounter4 count inhibit */
#define CHIP_REGFLD_CSR_MCOUNTINHIBIT_HPM3              (0x00000001UL <<  3U) /* (RW) hpmcounter3 count inhibit */
#define CHIP_REGFLD_CSR_MCOUNTINHIBIT_IR                (0x00000001UL <<  2U) /* (RW) instret count inhibit */
#define CHIP_REGFLD_CSR_MCOUNTINHIBIT_TM                (0x00000001UL <<  1U) /* (RW) time register S-mode access enable */
#define CHIP_REGFLD_CSR_MCOUNTINHIBIT_CY                (0x00000001UL <<  0U) /* (RW) cycle register S-mode access enable */

/***********  Machine mode performance monitoring event selector - MHPMEVENT (0x323-xxx) ***/
#define CHIP_REGFLD_CSR_MHPMEVENT_SEL                   (0x0000001FUL <<  4U) /* (RW) counting event selection */
#define CHIP_REGFLD_CSR_MHPMEVENT_TYPE                  (0x0000000FUL <<  0U) /* (RW) counting event type selection */


/*********** Machine mode trap cause - MCAUSE (0x342) *************************/
#define CHIP_REGFLD_CSR_MCAUSE_INTERRUPT                (0x00000001UL << 31U) /* (RW) If the trap was caused by an interrupt*/
#define CHIP_REGFLD_CSR_MCAUSE_EXCEPTION_CODE           (0x00000FFFUL <<  0U) /* (RW) Exception code */

#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_U_SW          0  /* User software interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_S_SW          1  /* Supervisor software interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_M_SW          3  /* Machine software interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_U_TMR         4  /* User timer interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_S_TMR         5  /* Supervisor timer interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_M_TMR         7  /* Machine timer interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_U_EXT         8  /* User external interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_S_EXT         9  /* Supervisor external interrupt */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_M_EXT         11 /* Machine external interrupt */

#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_IA_MALIGN     0  /* Instruction address misaligned */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_INST_ACC_FLT  1  /* Instruction access faul */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_INST_ILLEGAL  2  /* Illegal instruction */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_BREAKPT       3  /* Breakpoint */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_LDA_MALIGN    4  /* Load address misaligned */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_LD_ACC_FLT    5  /* Load access faul */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_STA_MALIGN    6  /* Store/AMO address misaligned */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_ST_ACC_FLT    7  /* Store/AMO access faul */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_U_ENV_CALL    8  /* Environment call from U-mode */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_S_ENV_CALL    9  /* Environment call from S-mode */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_M_ENV_CALL    11 /* Environment call from M-mode */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_INST_PAGE_FLT 12 /* Instruction page fault */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_LD_PAGE_FLT   13 /* Load page fault */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_ST_PAGE_FLT   15 /* Store/AMO page fault */

/*********** PMP configuration register x - PMPCFGx (0x3A0 + x) ***************/
#define CHIP_REGFLD_CSR_PMPCFG_FLD(x)                   ((0x000000FFUL <<  8*((x) & 0x3))
#define CHIP_REGFLD_CSR_PMPCFG_REGNUM(x)                ((x) / 4)

#define CHIP_REGFLD_CSR_PMPCFG_L                        (0x00000001UL <<  7U) /* (W1S) Write lock and permission enforcement bit for M‑mode */
#define CHIP_REGFLD_CSR_PMPCFG_A                        (0x00000003UL <<  3U) /* (RW)  Address matching mode */
#define CHIP_REGFLD_CSR_PMPCFG_X                        (0x00000001UL <<  2U) /* (RW)  Instruction execution control */
#define CHIP_REGFLD_CSR_PMPCFG_W                        (0x00000001UL <<  1U) /* (RW)  Write access control */
#define CHIP_REGFLD_CSR_PMPCFG_R                        (0x00000001UL <<  0U) /* (RW)  Read access control */

#define CHIP_REGFLDVAL_CSR_PMPCFG_A_OFF                 0 /* Null region */
#define CHIP_REGFLDVAL_CSR_PMPCFG_A_TOR                 1 /* Top of range (PMPADDRi, PMPADDRi-1] */
#define CHIP_REGFLDVAL_CSR_PMPCFG_A_NAPOT               3 /* Naturally aligned power-of-2 region */


/*********** Trigger data 1 - TDATA1 0x7A1 ************************************/
#define CHIP_REGFLD_CSR_TDATA1_TYPE                     (0x0000000FUL << 28U) /* (RW) Trigger type */
#define CHIP_REGFLD_CSR_TDATA1_DMODE                    (0x00000001UL << 27U) /* (RW) Only Debug-mode can write to the currently selected trigger register. Ignore writes from M‑mode */
#define CHIP_REGFLD_CSR_TDATA1_DATA                     (0x07FFFFFFUL << 28U) /* (RW) Data for a specific trigger */

#define CHIP_REGFLDVAL_CSR_TDATA1_TYPE_INVALID          0 /* The selected trigger is invalid */
#define CHIP_REGFLDVAL_CSR_TDATA1_TYPE_DA_MATCH         2 /* Address/data matching trigger */
#define CHIP_REGFLDVAL_CSR_TDATA1_TYPE_ICNTR            3 /* Instruction count trigger */
#define CHIP_REGFLDVAL_CSR_TDATA1_TYPE_INT              4 /* Interrupt trigger */
#define CHIP_REGFLDVAL_CSR_TDATA1_TYPE_EXC              5 /* Exception trigger */


/*********** Match control - MCONTROL 0x7A1 ***********************************/
#define CHIP_REGFLD_CSR_MCONTROL_TYPE                   (0x0000000FUL << 28U) /* (RW) Trigger type */
#define CHIP_REGFLD_CSR_MCONTROL_DMODE                  (0x00000001UL << 27U) /* (RW) Only Debug-mode can write to the currently selected trigger register. Ignore writes from M‑mode */
#define CHIP_REGFLD_CSR_MCONTROL_MASKMAX                (0x0000003FUL << 21U) /* (RO) Maximum natural alignment range supported by the hardware */
#define CHIP_REGFLD_CSR_MCONTROL_ACTION                 (0x0000000FUL << 12U) /* (RW) Action when this trigger matches. */
#define CHIP_REGFLD_CSR_MCONTROL_CHAIN                  (0x00000001UL << 11U) /* (RW) Enable trigger chains (prevents the trigger of the next index from matching) */
#define CHIP_REGFLD_CSR_MCONTROL_MATCH                  (0x0000000FUL <<  7U) /* (RW) Select a matching scheme.. */
#define CHIP_REGFLD_CSR_MCONTROL_M                      (0x00000001UL <<  6U) /* (RW) Enable trigger in M‑mode */
#define CHIP_REGFLD_CSR_MCONTROL_S                      (0x00000001UL <<  4U) /* (RW) Enable trigger in S‑mode */
#define CHIP_REGFLD_CSR_MCONTROL_U                      (0x00000001UL <<  3U) /* (RW) Enable trigger in U‑mode */
#define CHIP_REGFLD_CSR_MCONTROL_EXECUTE                (0x00000001UL <<  2U) /* (RW) Enable this trigger to compare the virtual addresses of instructions */
#define CHIP_REGFLD_CSR_MCONTROL_STORE                  (0x00000001UL <<  1U) /* (RW) Enable this trigger to compare the store's virtual address */
#define CHIP_REGFLD_CSR_MCONTROL_LOAD                   (0x00000001UL <<  0U) /* (RW) Enable this trigger to compare the load's virtual address */

#define CHIP_REGFLDVAL_CSR_MCONTROL_ACTION_BP_EXC       0 /* Throw breakpoint exception */
#define CHIP_REGFLDVAL_CSR_MCONTROL_ACTION_DBG_MODE     1 /* Enter debugging mode. (Only supported when DMODE is 1) */


/*********** Instruction count - ICOUNT 0x7A1 *********************************/
#define CHIP_REGFLD_CSR_ICOUNT_TYPE                     (0x0000000FUL << 28U) /* (RW) Trigger type */
#define CHIP_REGFLD_CSR_ICOUNT_DMODE                    (0x00000001UL << 27U) /* (RW) Only Debug-mode can write to the currently selected trigger register. Ignore writes from M‑mode */
#define CHIP_REGFLD_CSR_ICOUNT_COUNT                    (0x00000001UL << 10U) /* (RO) This field is hardwired to 1 to support single step execution */
#define CHIP_REGFLD_CSR_ICOUNT_M                        (0x00000001UL <<  9U) /* (RW) Enable trigger in M‑mode */
#define CHIP_REGFLD_CSR_ICOUNT_S                        (0x00000001UL <<  7U) /* (RW) Enable trigger in S‑mode */
#define CHIP_REGFLD_CSR_ICOUNT_U                        (0x00000001UL <<  6U) /* (RW) Enable trigger in U‑mode */
#define CHIP_REGFLD_CSR_ICOUNT_ACTION                   (0x0000003FUL <<  0U) /* (RW) Action when this trigger matches. */

#define CHIP_REGFLDVAL_CSR_ICOUNT_ACTION_BP_EXC         0 /* Throw breakpoint exception */
#define CHIP_REGFLDVAL_CSR_ICOUNT_ACTION_DBG_MODE       1 /* Enter debugging mode. (Only supported when DMODE is 1) */


/*********** Interrupt trigger - ITRIGGER 0x7A1 *******************************/
#define CHIP_REGFLD_CSR_ITRIGGER_TYPE                   (0x0000000FUL << 28U) /* (RW) Trigger type */
#define CHIP_REGFLD_CSR_ITRIGGER_DMODE                  (0x00000001UL << 27U) /* (RW) Only Debug-mode can write to the currently selected trigger register. Ignore writes from M‑mode */
#define CHIP_REGFLD_CSR_ITRIGGER_M                      (0x00000001UL <<  9U) /* (RW) Enable trigger in M‑mode */
#define CHIP_REGFLD_CSR_ITRIGGER_S                      (0x00000001UL <<  7U) /* (RW) Enable trigger in S‑mode */
#define CHIP_REGFLD_CSR_ITRIGGER_U                      (0x00000001UL <<  6U) /* (RW) Enable trigger in U‑mode */
#define CHIP_REGFLD_CSR_ITRIGGER_ACTION                 (0x0000003FUL <<  0U) /* (RW) Action when this trigger matches. */

#define CHIP_REGFLDVAL_CSR_ITRIGGER_ACTION_BP_EXC       0 /* Throw breakpoint exception */
#define CHIP_REGFLDVAL_CSR_ITRIGGER_ACTION_DBG_MODE     1 /* Enter debugging mode. (Only supported when DMODE is 1) */


/*********** Exception trigger - ETRIGGER 0x7A1 *******************************/
#define CHIP_REGFLD_CSR_ETRIGGER_TYPE                   (0x0000000FUL << 28U) /* (RW) Trigger type */
#define CHIP_REGFLD_CSR_ETRIGGER_DMODE                  (0x00000001UL << 27U) /* (RW) Only Debug-mode can write to the currently selected trigger register. Ignore writes from M‑mode */
#define CHIP_REGFLD_CSR_ETRIGGER_NMI                    (0x00000001UL << 10U) /* (RW) Enable trigger on non-maskable interrupts regardless of the values of S, U, and M */
#define CHIP_REGFLD_CSR_ETRIGGER_M                      (0x00000001UL <<  9U) /* (RW) Enable trigger in M‑mode */
#define CHIP_REGFLD_CSR_ETRIGGER_S                      (0x00000001UL <<  7U) /* (RW) Enable trigger in S‑mode */
#define CHIP_REGFLD_CSR_ETRIGGER_U                      (0x00000001UL <<  6U) /* (RW) Enable trigger in U‑mode */
#define CHIP_REGFLD_CSR_ETRIGGER_ACTION                 (0x0000003FUL <<  0U) /* (RW) Action when this trigger matches. */

#define CHIP_REGFLDVAL_CSR_ETRIGGER_ACTION_BP_EXC       0 /* Throw breakpoint exception */
#define CHIP_REGFLDVAL_CSR_ETRIGGER_ACTION_DBG_MODE     1 /* Enter debugging mode. (Only supported when DMODE is 1) */

/*********** Extra trigger - TEXTRA 0x7A3 *************************************/
#define CHIP_REGFLD_CSR_TEXTRA_MVALUE                   (0x0000003FUL << 26U) /* (RW) Data for use with MSELECT */
#define CHIP_REGFLD_CSR_TEXTRA_MSELECT                  (0x00000001UL << 25U) /* (RW) 0 - ignore, 1 - MVALUE = mcontext */
#define CHIP_REGFLD_CSR_TEXTRA_SVALUE                   (0x000001FFUL <<  2U) /* (RW) Data for use with SSELECT */
#define CHIP_REGFLD_CSR_TEXTRA_SSELECT                  (0x00000003UL <<  0U) /* (RW) 0 - ignore, 1 - SVALUE = mcontext, 2 SVALUE = satp.ASID */


/*********** Trigger information - TINFO 0x7A4 ********************************/
#define CHIP_REGFLD_CSR_TINFO_NOT_EXISTS                (0x00000001UL <<  0U) /* (RO) Selected trigger is not exists */
#define CHIP_REGFLD_CSR_TINFO_AD_MATCH                  (0x00000001UL <<  2U) /* (RO) Selected trigger supports address/data matching trigger type */
#define CHIP_REGFLD_CSR_TINFO_INST_CNT                  (0x00000001UL <<  3U) /* (RO) Selected trigger supports the instruction count trigger type */
#define CHIP_REGFLD_CSR_TINFO_INT                       (0x00000001UL <<  4U) /* (RO) Selected trigger supports interrupt trigger type */
#define CHIP_REGFLD_CSR_TINFO_EXC                       (0x00000001UL <<  5U) /* (RO) Selected trigger supports the exception trigger type */
#define CHIP_REGFLD_CSR_TINFO_UNAVAIL                   (0x00000001UL << 15U) /* (RO) Selected trigger exists (so the enumeration should not terminate), but is currently unavailable */


/*********** Trigger control - TCONTROL 0x7A5 *********************************/
#define CHIP_REGFLD_CSR_TCONTROL_MPTE                   (0x00000001UL <<  7U) /* (RW) M‑mode previously trigger enable field. When trapped in M‑mode, MPTE is set to the value of MTE. */
#define CHIP_REGFLD_CSR_TCONTROL_MTE                    (0x00000001UL <<  3U) /* (RW) M‑mode trigger enable field */


/*********** Debug control and status registers - DCSR 0x7B0 ******************/
#define CHIP_REGFLD_CSR_DCSR_XDEBUGVER                  (0x0000000FUL << 28U) /* (RO) The version of the external debugger */
#define CHIP_REGFLD_CSR_DCSR_EBREAKM                    (0x00000001UL << 15U) /* (RW) Behavior of the EBREAK instruction in machine mode */
#define CHIP_REGFLD_CSR_DCSR_EBREAKS                    (0x00000001UL << 13U) /* (RW) Behavior of the EBREAK instruction in supervisor mode */
#define CHIP_REGFLD_CSR_DCSR_EBREAKU                    (0x00000001UL << 12U) /* (RW) Behavior of the EBREAK instruction in user mode */
#define CHIP_REGFLD_CSR_DCSR_STEPIE                     (0x00000001UL << 11U) /* (RW) Enable single-step interrupts */
#define CHIP_REGFLD_CSR_DCSR_STOPCOUNT                  (0x00000001UL << 10U) /* (RW) Stop counter in debug mode */
#define CHIP_REGFLD_CSR_DCSR_STOPTIME                   (0x00000001UL <<  9U) /* (RW) Stop the timer in debug mode */
#define CHIP_REGFLD_CSR_DCSR_CAUSE                      (0x00000007UL <<  6U) /* (RW) Reason for entering debug mode */
#define CHIP_REGFLD_CSR_DCSR_MPRVEN                     (0x00000001UL <<  4U) /* (RW) MPRV in mstatus takes effect in Debug mode. */
#define CHIP_REGFLD_CSR_DCSR_NMIP                       (0x00000001UL <<  3U) /* (RO) Non-maskable interrupt (NMI) pending */
#define CHIP_REGFLD_CSR_DCSR_STEP                       (0x00000001UL <<  2U) /* (RW) Single step mode enabled */
#define CHIP_REGFLD_CSR_DCSR_PRV                        (0x00000003UL <<  0U) /* (RW) The permission level at which hart runs when entering debug mode */


#define CHIP_REGFLDVAL_CSR_DCSR_EBREAK_BP_EXC           0 /* Generate a regular breakpoint exception */
#define CHIP_REGFLDVAL_CSR_DCSR_EBREAK_DBG_MODE         1 /* Enter debugging mode */

#define CHIP_REGFLDVAL_CSR_DCSR_CAUSE_EBREAK            1
#define CHIP_REGFLDVAL_CSR_DCSR_CAUSE_TRIG_MODULE       2
#define CHIP_REGFLDVAL_CSR_DCSR_CAUSE_HART_REQ          3
#define CHIP_REGFLDVAL_CSR_DCSR_CAUSE_SINGLE_STEP       4
#define CHIP_REGFLDVAL_CSR_DCSR_CAUSE_HALT_ON_RST       5

#define CHIP_REGFLDVAL_CSR_DCSR_PRV_U                   0 /* User/Application */
#define CHIP_REGFLDVAL_CSR_DCSR_PRV_S                   1 /* Supervisor */
#define CHIP_REGFLDVAL_CSR_DCSR_PRV_M                   3 /* Machine */


/*********** PMA configuration register x - PMPCFGx (0xBC0 + x) ***************/
#define CHIP_REGFLD_CSR_PMACFG_FLD(x)                   ((0x000000FFUL <<  8*((x) & 0x3))
#define CHIP_REGFLD_CSR_PMACFG_REGNUM(x)                ((x) / 4)

#define CHIP_REGFLD_CSR_PMACFG_NAMO                     (0x00000001UL <<  6U) /* (RW) AMO (Atomic Memory Operation) instructions (including LR/SC) are not supported */
#define CHIP_REGFLD_CSR_PMACFG_MTYPE                    (0x0000000FUL <<  2U) /* (RW) Memory type attribute  */
#define CHIP_REGFLD_CSR_PMACFG_ETYP                     (0x00000003UL <<  0U) /* (RW) Entry address matching mode. */


#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_DEV_NBUF        0  /* Device, Non-bufferable */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_DEV_BUF         1  /* Device, bufferable */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_NCACHE_NBUF 2  /* Memory, Non-cacheable, Non-bufferable */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_NCACHE_BUF  3  /* Memory, Non-cacheable, Bufferable */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_WT_NOALLOC  4  /* Memory, Write-through, No-allocate */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_WT_RDALLOC  5  /* Memory, Write-through, Read-allocate */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_WB_NODALLOC 8  /* Memory, Write-back, No-allocate */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_WB_RDDALLOC 9  /* Memory, Write-back, Read-allocate */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_WB_WRDALLOC 10 /* Memory, Write-back, Write-allocate */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_MEM_WB_RWDALLOC 11 /* Memory, Write-back, Read and Write-allocate */
#define CHIP_REGFLDVAL_CSR_PMACFG_MTYPE_EMPTY_HOLE      15 /* Empty hole, nothing exists */

#define CHIP_REGFLDVAL_CSR_PMACFG_A_OFF                 0 /* Null region */
#define CHIP_REGFLDVAL_CSR_PMACFG_A_NAPOT               3 /* Naturally aligned power-of-2 region */



#endif // CHIP_RISCV_CSR_H
