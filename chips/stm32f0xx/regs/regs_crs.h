#ifndef CHIP_STM32F0XX_REGS_CRS_H
#define CHIP_STM32F0XX_REGS_CRS_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

#if CHIP_DEV_SUPPORT_CRS

typedef struct {
    __IO uint32_t CR;     /*!< CRS ccontrol register,              Address offset: 0x00 */
    __IO uint32_t CFGR;   /*!< CRS configuration register,         Address offset: 0x04 */
    __IO uint32_t ISR;    /*!< CRS interrupt and status register,  Address offset: 0x08 */
    __IO uint32_t ICR;    /*!< CRS interrupt flag clear register,  Address offset: 0x0C */
} CHIP_REGS_CRS_T;

#define CHIP_REGS_CRS          ((CHIP_REGS_CRS_T *) CHIP_MEMRGN_ADDR_PERIPH_CRS)

/*********** CRS control register (CRS_CR) ************************************/
#define CHIP_REGFLD_CRS_CR_SYNC_OK_IE               (0x00000001UL <<  0U)   /* (RW) SYNC event OK interrupt enable */
#define CHIP_REGFLD_CRS_CR_SYNC_WARN_IE             (0x00000001UL <<  1U)   /* (RW) SYNC warning interrupt enable */
#define CHIP_REGFLD_CRS_CR_ERR_IE                   (0x00000001UL <<  2U)   /* (RW) Synchronization or trimming error interrupt enable */
#define CHIP_REGFLD_CRS_CR_ESYNC_IE                 (0x00000001UL <<  3U)   /* (RW) Expected SYNC interrupt enable */
#define CHIP_REGFLD_CRS_CR_CEN                      (0x00000001UL <<  5U)   /* (RW) Frequency error counter enable */
#define CHIP_REGFLD_CRS_CR_AUTOTRIM_EN              (0x00000001UL <<  6U)   /* (RW) Automatic trimming enable */
#define CHIP_REGFLD_CRS_CR_SW_SYNC                  (0x00000001UL <<  7U)   /* (W1) Generate software SYNC event */
#define CHIP_REGFLD_CRS_CR_TRIM                     (0x0000003FUL <<  8U)   /* (RW) HSI48 oscillator smooth trimming */
#define CHIP_REGMSK_CRS_CR_RESERVED                 (0xFFFFC010UL)

/*********** CRS configuration register (CRS_CFGR) ****************************/
#define CHIP_REGFLD_CRS_CFGR_RELOAD                 (0x0000FFFFUL <<  0U)   /* (RW) Counter reload value */
#define CHIP_REGFLD_CRS_CFGR_FE_LIM                 (0x000000FFUL << 16U)   /* (RW) Frequency error limit */
#define CHIP_REGFLD_CRS_CFGR_SYNC_DIV               (0x00000007UL << 24U)   /* (RW) SYNC divider */
#define CHIP_REGFLD_CRS_CFGR_SYNC_SRC               (0x00000003UL << 28U)   /* (RW) SYNC signal source selection */
#define CHIP_REGFLD_CRS_CFGR_SYNC_POL               (0x00000001UL << 31U)   /* (RW) SYNC polarity selection */
#define CHIP_REGMSK_CRS_CFGR_RESERVED               (0x48000000UL)

#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_1          0
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_2          1
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_4          2
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_8          3
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_16         4
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_32         5
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_64         6
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_128        7

#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_SRC_GPIO       0
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_SRC_LSE        1
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_SRC_USB        2

#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_POL_RISE       0
#define CHIP_REGFLDVAL_CRS_CFGR_SYNC_POL_FALL       1

/*********** CRS interrupt and status register (CRS_ISR) **********************/
#define CHIP_REGFLD_CRS_ISR_SYNCOK_F                (0x00000001UL <<  0U)   /* (RO) SYNC event OK flag */
#define CHIP_REGFLD_CRS_ISR_SYNWARN_F               (0x00000001UL <<  1U)   /* (RO) SYNC warning flag */
#define CHIP_REGFLD_CRS_ISR_ERR_F                   (0x00000001UL <<  2U)   /* (RO) Error flag */
#define CHIP_REGFLD_CRS_ISR_ESYNC_F                 (0x00000001UL <<  3U)   /* (RO) Expected SYNC flag */
#define CHIP_REGFLD_CRS_ISR_SYNC_ERR                (0x00000001UL <<  8U)   /* (RO) SYNC error */
#define CHIP_REGFLD_CRS_ISR_SYNC_MISS               (0x00000001UL <<  9U)   /* (RO) SYNC missed */
#define CHIP_REGFLD_CRS_ISR_TRIM_OVF                (0x00000001UL << 10U)   /* (RO) Trimming overflow or underflow */
#define CHIP_REGFLD_CRS_ISR_FE_DIR                  (0x00000001UL << 15U)   /* (RO) Frequency error direction */
#define CHIP_REGFLD_CRS_ISR_FE_CAP                  (0x0000FFFFUL << 16U)   /* (RO) Frequency error capture */
#define CHIP_REGMSK_CRS_ISR_RESERVED                (0x000078F0UL)

#define CHIP_REGFLDVAL_CRS_ISR_FE_DIR_UP            0 /* Upcounting */
#define CHIP_REGFLDVAL_CRS_ISR_FE_DIR_DOWN          1 /* Downcounting */

/*********** CRS interrupt flag clear register (CRS_ICR) **********************/
#define CHIP_REGFLD_CRS_ICR_SYNCOK_C                (0x00000001UL <<  0U)   /* (W1) SYNC event OK clear */
#define CHIP_REGFLD_CRS_ICR_SYNWARN_C               (0x00000001UL <<  1U)   /* (W1) SYNC warning clear flag */
#define CHIP_REGFLD_CRS_ICR_ERR_C                   (0x00000001UL <<  2U)   /* (W1) Error clear flag */
#define CHIP_REGFLD_CRS_ICR_ESYNC_C                 (0x00000001UL <<  3U)   /* (W1) Expected SYNC clear flag */
#define CHIP_REGMSK_CRS_ICR_RESERVED                (0xFFFFFFF0UL)

#endif

#endif // CHIP_STM32F0XX_REGS_CRS_H
