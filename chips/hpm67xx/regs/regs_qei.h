#ifndef REGS_QEI_H
#define REGS_QEI_H

#include "chip_ioreg_defs.h"

typedef struct {
    __I  uint32_t Z;                       /* 0x30: Z counter */
    __I  uint32_t PH;                      /* 0x34: Phase counter */
    __I  uint32_t SPD;                     /* 0x38: Speed counter */
    __I  uint32_t TMR;                     /* 0x3C: Timer counter */
} CHIP_REGS_QEI_COUNT_T;

typedef struct {
    __IO uint32_t CR;                          /* 0x0: Control register */
    __IO uint32_t PHCFG;                       /* 0x4: Phase configure register */
    __IO uint32_t WDGCFG;                      /* 0x8: Watchdog configure register */
    __IO uint32_t PHIDX;                       /* 0xC: Phase index register */
    __IO uint32_t TRGOEN;                      /* 0x10: Tigger output enable register */
    __IO uint32_t READEN;                      /* 0x14: Read event enable register */
    __IO uint32_t ZCMP;                        /* 0x18: Z comparator */
    __IO uint32_t PHCMP;                       /* 0x1C: Phase comparator */
    __IO uint32_t SPDCMP;                      /* 0x20: Speed comparator */
    __IO uint32_t DMAEN;                       /* 0x24: DMA request enable register */
    __IO uint32_t SR;                          /* 0x28: Status register */
    __IO uint32_t IRQEN;                       /* 0x2C: Interrupt request register */
    struct {
        CHIP_REGS_QEI_COUNT_T CURRENT;         /* 0x30 - 0x3C: Current counters */
        CHIP_REGS_QEI_COUNT_T READ;            /* 0x40 - 0x4C: Read counters */
        CHIP_REGS_QEI_COUNT_T SNAP[2];         /* 0x50 - 0x6C: Snapshot counters */
    } COUNT;
    __I  uint32_t SPDHIS[4];                   /* 0x70 - 0x7C: Speed history */
} CHIP_REGS_QEI_T;


#define CHIP_REGS_QEI0            ((CHIP_REGS_QEI_T *)CHIP_MEMRGN_ADDR_QEI0)
#define CHIP_REGS_QEI1            ((CHIP_REGS_QEI_T *)CHIP_MEMRGN_ADDR_QEI1)
#define CHIP_REGS_QEI2            ((CHIP_REGS_QEI_T *)CHIP_MEMRGN_ADDR_QEI2)
#define CHIP_REGS_QEI3            ((CHIP_REGS_QEI_T *)CHIP_MEMRGN_ADDR_QEI3)


/*********** QEI_CR - Control register *********************************/
#define CHIP_REGFLD_QEI_CR_READ                         (0x00000001UL << 31U) /* (W1SC) Generate a software read event and load the values of PHCNT, ZCNT, SPDCNT and TMRCNT registers into the corresponding read registers. */
#define CHIP_REGFLD_QEI_CR_HRSTSPD                      (0x00000001UL << 18U) /* (RW) When H‑phase input is active, reset SPDCNT */
#define CHIP_REGFLD_QEI_CR_HRSTPH                       (0x00000001UL << 17U) /* (RW) When H‑phase input is active, reset PHCNT */
#define CHIP_REGFLD_QEI_CR_HRSTZ                        (0x00000001UL << 16U) /* (RW) When H‑phase input is active, reset ZCNT */
#define CHIP_REGFLD_QEI_CR_PAUSESPD                     (0x00000001UL << 14U) /* (RW) When PAUSE input is active, temporarily fix SPDCNT */
#define CHIP_REGFLD_QEI_CR_PAUSEPH                      (0x00000001UL << 13U) /* (RW) When PAUSE input is active, temporarily fix PHCNT */
#define CHIP_REGFLD_QEI_CR_PAUSEZ                       (0x00000001UL << 12U) /* (RW) When PAUSE input is active, temporarily fix ZCNT */
#define CHIP_REGFLD_QEI_CR_HRDIR1                       (0x00000001UL << 11U) /* (RW) The HOMEF flag is set to 1 at the rising edge of the H‑phase input when the motor rotates backward (DIR == 1) */
#define CHIP_REGFLD_QEI_CR_HRDIR0                       (0x00000001UL << 10U) /* (RW) The HOMEF flag is set to 1 at the rising edge of the H‑phase input  when the motor rotates forward (DIR == 0) */
#define CHIP_REGFLD_QEI_CR_HFDIR1                       (0x00000001UL <<  9U) /* (RW) The HOMEF flag is set to 1 at the falling edge of the H‑phase input and the motor rotates backward (DIR == 1) */
#define CHIP_REGFLD_QEI_CR_HFDIR0                       (0x00000001UL <<  8U) /* (RW) The HOMEF flag is set to 1 at the falling edge of the H‑phase input and the motor rotates forward (DIR == 0) */
#define CHIP_REGFLD_QEI_CR_SNAPEN                       (0x00000001UL <<  5U) /* (RW) Turn on the snapshot function. When the SNAPI input is active, load the values of the PHCNT, ZCNT, SPDCNT and TMRCNT registers into the corresponding snapshot registers. */
#define CHIP_REGFLD_QEI_CR_RSTCNT                       (0x00000001UL <<  4U) /* (RW) Resets ZCNT, TMRCNT and SPDCNT to 0, resets PHCNT to the value of PHIDX */
#define CHIP_REGFLD_QEI_CR_ENCTYP                       (0x00000003UL <<  0U) /* (RW) Decoder mode control */
#define CHIP_REGMSK_QEI_CR_RESERVED                     (0x7FF880CCUL)


#define CHIP_REGFLDVAL_QEI_CR_ENCTYP_ABZ                0 /* ABZ mode */
#define CHIP_REGFLDVAL_QEI_CR_ENCTYP_PD                 1 /* PD mode */
#define CHIP_REGFLDVAL_QEI_CR_ENCTYP_UD                 2 /* UD mode */


/*********** QEI_PHCFG - Phase configure register *****************************/
#define CHIP_REGFLD_QEI_PHCFG_ZCNTCFG                   (0x00000001UL << 22U) /* (RW) ZCNT mode (0 - by Z input, 1 on PHCNT) */
#define CHIP_REGFLD_QEI_PHCFG_PHCALIZ                   (0x00000001UL << 21U) /* (RW) When the Z‑phase input is active, reset PHCNT to PHIDX */
#define CHIP_REGFLD_QEI_PHCFG_PHMAX                     (0x001FFFFFUL <<  0U) /* (RW) The upper limit of the count of PHCNT. When the PHCNT count reaches PHMAX, it will roll back to 0; when the PHCNT count decreases to 0, it will roll back to PHMAX */
#define CHIP_REGMSK_QEI_PHCFG_RESERVED                  (0xFF800000UL)


/*********** QEI_WDGCFG - Watchdog configure register *************************/
#define CHIP_REGFLD_QEI_WDGCFG_WDGEN                    (0x00000001UL << 31U) /* (RW) Enable watchdog counter */
#define CHIP_REGFLD_QEI_WDGCFG_WDGTO                    (0x7FFFFFFFUL <<  0U) /* (RW) Watchdog counter timeout value */

/*********** QEI_PHIDX - Phase index register *********************************/
#define CHIP_REGFLD_QEI_PHIDX_PHIDX                     (0x001FFFFFUL <<  0U) /* (RW) The reset value of PHCNT, if PHCALIZ is set to 1, when the Z‑phase input is active, PHCNT is reset to PHIDX */
#define CHIP_REGMSK_QEI_PHIDX_RESERVED                  (0xFFE00000UL)

/*********** QEI_TRGOEN - Tigger output enable register ***********************/
#define CHIP_REGFLD_QEI_TRGOEN_WDGFEN                   (0x00000001UL << 31U) /* (RW) WDGF flag trigger TRGO output enable */
#define CHIP_REGFLD_QEI_TRGOEN_HOMEFEN                  (0x00000001UL << 30U) /* (RW) HOMEF flag trigger TRGO output enable */
#define CHIP_REGFLD_QEI_TRGOEN_POSCMPFEN                (0x00000001UL << 29U) /* (RW) POSCMPF flag trigger TRGO output enable */
#define CHIP_REGFLD_QEI_TRGOEN_ZPHFEN                   (0x00000001UL << 28U) /* (RW) ZPHF flag trigger TRGO output enable */
#define CHIP_REGMSK_QEI_TRGOEN_RESERVED                 (0x0FFFFFFFUL)


/*********** QEI_READEN - Read event enable register **************************/
#define CHIP_REGFLD_QEI_READEN_WDGFEN                   (0x00000001UL << 31U) /* (RW) WDGF flag read event generate  enable */
#define CHIP_REGFLD_QEI_READEN_HOMEFEN                  (0x00000001UL << 30U) /* (RW) HOMEF flag read event generate  enable */
#define CHIP_REGFLD_QEI_READEN_POSCMPFEN                (0x00000001UL << 29U) /* (RW) POSCMPF flag read event generate  enable */
#define CHIP_REGFLD_QEI_READEN_ZPHFEN                   (0x00000001UL << 28U) /* (RW) ZPHF flag read event generate  enable */
#define CHIP_REGMSK_QEI_READEN_RESERVED                 (0x0FFFFFFFUL)


/*********** QEI_PHCMP - Phase comparator *************************************/
#define CHIP_REGFLD_QEI_PHCMP_ZCMPDIS                   (0x00000001UL << 31U) /* (RW) When the location matches, ignore ZCMP */
#define CHIP_REGFLD_QEI_PHCMP_DIRCMPDIS                 (0x00000001UL << 30U) /* (RW) When position matching, ignore the motor rotation direction DIR */
#define CHIP_REGFLD_QEI_PHCMP_DIRCMP                    (0x00000001UL << 29U) /* (RW) Rotation direction comparison when configuring matching */
#define CHIP_REGFLD_QEI_PHCMP_PHCMP                     (0x001FFFFFUL <<  0U) /* (RW) PHCNT comparison value for timer position matching. When PHCNT == PHCMP, the match is established. */
#define CHIP_REGMSK_QEI_PHCMP_RESERVED                  (0x1FE00000UL)

#define CHIP_REGFLDVAL_QEI_PHCMP_DIRCMP_BKWRD           1 /* When position is matched, the motor needs to rotate in backward direction (DIR == 1) */
#define CHIP_REGFLDVAL_QEI_PHCMP_DIRCMP_FWRD            0 /* When position is matched, the motor needs to rotate in the forward direction (DIR == 0) */


/*********** QEI_DMAEN - DMA request enable register **************************/
#define CHIP_REGFLD_QEI_DMAEN_WDGFEN                    (0x00000001UL << 31U) /* (RW) WDGF flag DMA request generate enable */
#define CHIP_REGFLD_QEI_DMAEN_HOMEFEN                   (0x00000001UL << 30U) /* (RW) HOMEF flag DMA request generate  enable */
#define CHIP_REGFLD_QEI_DMAEN_POSCMPFEN                 (0x00000001UL << 29U) /* (RW) POSCMPF flag DMA request generate  enable */
#define CHIP_REGFLD_QEI_DMAEN_ZPHFEN                    (0x00000001UL << 28U) /* (RW) ZPHF flag DMA request generate  enable */
#define CHIP_REGMSK_QEI_DMAEN_RESERVED                  (0x0FFFFFFFUL)


/*********** QEI_SR - Status register *****************************************/
#define CHIP_REGFLD_QEI_SR_WDGF                         (0x00000001UL << 31U) /* (RW) Watchdog timeout flag */
#define CHIP_REGFLD_QEI_SR_HOMEF                        (0x00000001UL << 30U) /* (RW) HOMEF flag */
#define CHIP_REGFLD_QEI_SR_POSCMPFEN                    (0x00000001UL << 29U) /* (RW) Position match flag */
#define CHIP_REGFLD_QEI_SR_ZPHF                         (0x00000001UL << 28U) /* (RW) ZPHF flag */
#define CHIP_REGMSK_QEI_SR_RESERVED                     (0x0FFFFFFFUL)


/*********** QEI_IRQEN - Interrupt request register ***************************/
#define CHIP_REGFLD_QEI_IRQEN_WDGFIE                    (0x00000001UL << 31U) /* (RW) WDGF flag interrupt request generate enable */
#define CHIP_REGFLD_QEI_IRQEN_HOMEFIE                   (0x00000001UL << 30U) /* (RW) HOMEF flag interrupt request generate  enable */
#define CHIP_REGFLD_QEI_IRQEN_POSCMPFIE                 (0x00000001UL << 29U) /* (RW) POSCMPF flag interrupt request generate  enable */
#define CHIP_REGFLD_QEI_IRQEN_ZPHFIE                    (0x00000001UL << 28U) /* (RW) ZPHF flag interrupt request generate  enable */
#define CHIP_REGMSK_QEI_IRQEN_RESERVED                  (0x0FFFFFFFUL)

/*********** QEI_COUNTPH - Phase counter *************************************/
#define CHIP_REGFLD_QEI_COUNTPH_DIR                     (0x00000001UL << 30U) /* (RO) Rotation reverse */
#define CHIP_REGFLD_QEI_COUNTPH_ASTAT                   (0x00000001UL << 26U) /* (RO) A-phase input signal status */
#define CHIP_REGFLD_QEI_COUNTPH_BSTAT                   (0x00000001UL << 25U) /* (RO) B-phase input signal status */
#define CHIP_REGFLD_QEI_COUNTPH_PHCNT                   (0x001FFFFFUL <<  0U) /* (RO) Phase counter */

#define CHIP_REGFLDVAL_QEI_COUNTPH_DIR_BKWRD            1 /* Motor rotates in reverse direction */
#define CHIP_REGFLDVAL_QEI_COUNTPH_DIR_FWRD             0 /* Motor rotates in forward direction */

/*********** QEI_COUNTSPD - Speed counter *************************************/
#define CHIP_REGFLD_QEI_COUNTSPD_DIR                    (0x00000001UL << 31U) /* (RO) Rotation reverse */
#define CHIP_REGFLD_QEI_COUNTSPD_ASTAT                  (0x00000001UL << 30U) /* (RO) A-phase input signal status */
#define CHIP_REGFLD_QEI_COUNTSPD_BSTAT                  (0x00000001UL << 29U) /* (RO) B-phase input signal status */
#define CHIP_REGFLD_QEI_COUNTSPD_SPDCNT                 (0x0FFFFFFFUL <<  0U) /* (RO) Speed counter */

#define CHIP_REGFLDVAL_QEI_COUNTSPD_DIR_BKWRD           1 /* Motor rotates in reverse direction */
#define CHIP_REGFLDVAL_QEI_COUNTSPD_DIR_FWRD            0 /* Motor rotates in forward direction */







#endif // REGS_QEI_H
