#ifndef CHIP_CORE_FPU_H
#define CHIP_CORE_FPU_H

#include "chip_config.h"
#include "chip_core_features.h"
#include "regs/regs_core_fpu.h"

#if CHIP_CORE_SUPPORT_FPU
    #ifdef CHIP_CFG_FPU_EN
        #define CHIP_FPU_EN     CHIP_CFG_FPU_EN
    #else
        #define CHIP_FPU_EN     1
    #endif
#else
    #define CHIP_FPU_ENABLE 0
#endif

#if CHIP_CORE_SUPPORT_FPU
static inline void chip_fpu_set_access(uint8_t code) {
    CHIP_REGS_FPU->CPACR = (CHIP_REGS_FPU->CPACR & ~(CHIP_REGFLD_FPU_CPACR_CP(CHIP_CP_NUM_VFP1) | CHIP_REGFLD_FPU_CPACR_CP(CHIP_CP_NUM_VFP2)))
            | LBITFIELD_SET(CHIP_REGFLD_FPU_CPACR_CP(CHIP_CP_NUM_VFP1), code)
            | LBITFIELD_SET(CHIP_REGFLD_FPU_CPACR_CP(CHIP_CP_NUM_VFP2), code);
}

static inline void chip_fpu_enable(void) {
    chip_fpu_set_access(CHIP_REGFLDVAL_FPU_CPACR_CP_FULL);
}

static inline void chip_fpu_disable(void) {
    chip_fpu_set_access(CHIP_REGFLDVAL_FPU_CPACR_CP_NOACC);
}
#endif

static inline void chip_fpu_init(void) {
#if CHIP_CORE_SUPPORT_FPU
#if CHIP_FPU_EN
    chip_fpu_enable();
#else
    chip_fpu_disable();
#endif
#endif
}

#endif // CHIP_CORE_FPU_H
