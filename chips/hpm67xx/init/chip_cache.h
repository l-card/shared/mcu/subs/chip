#ifndef CHIP_CACHE_H
#define CHIP_CACHE_H

/* Cache commands */
#define CHIP_CACHE_CMD_L1D_VA_INVAL         0  /* VA */
#define CHIP_CACHE_CMD_L1D_VA_WB            1  /* VA */
#define CHIP_CACHE_CMD_L1D_VA_WBINVAL       2  /* VA */
#define CHIP_CACHE_CMD_L1D_VA_LOCK          3  /* VA, CCTL_DATA */
#define CHIP_CACHE_CMD_L1D_VA_UNLOCK        4  /* VA */
#define CHIP_CACHE_CMD_L1D_WBINVAL_ALL      6  /* - */
#define CHIP_CACHE_CMD_L1D_WB_ALL           7  /* - */
#define CHIP_CACHE_CMD_L1I_VA_INVAL         8  /* VA */
#define CHIP_CACHE_CMD_L1I_VA_LOCK         11  /* VA, CCTL_DATA */
#define CHIP_CACHE_CMD_L1I_VA_UNLOCK       12  /* VA */
#define CHIP_CACHE_CMD_L1D_IX_INVAL        16  /* Index */
#define CHIP_CACHE_CMD_L1D_IX_WB           17  /* Index */
#define CHIP_CACHE_CMD_L1D_IX_WBINVAL      18  /* Index */
#define CHIP_CACHE_CMD_L1D_IX_RTAG         19  /* Index, CCTL_DATA */
#define CHIP_CACHE_CMD_L1D_IX_RDATA        20  /* Index, CCTL_DATA */
#define CHIP_CACHE_CMD_L1D_IX_WTAG         21  /* Index, CCTL_DATA */
#define CHIP_CACHE_CMD_L1D_IX_WDATA        22  /* Index, CCTL_DATA */
#define CHIP_CACHE_CMD_L1D_INVAL_ALL       23  /* - */
#define CHIP_CACHE_CMD_L1I_IX_INVAL        24  /* Index */
#define CHIP_CACHE_CMD_L1I_IX_RTAG         27  /* Index, CCTL_DATA */
#define CHIP_CACHE_CMD_L1I_IX_RDATA        28  /* Index, CCTL_DATA */
#define CHIP_CACHE_CMD_L1I_IX_WTAG         29  /* Index, CCTL_DATA */
#define CHIP_CACHE_CMD_L1I_IX_WDATA        30  /* Index, CCTL_DATA */




#endif // CHIP_CACHE_H
