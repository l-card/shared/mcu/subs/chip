#ifndef CHIP_PER_CTL_H
#define CHIP_PER_CTL_H

#include <stdbool.h>

/* Идентификаторы клоков периферийный устройств, которые могут быть разрешены
 * или запрещены при работе с помощью chip_per_clk_en() и chip_per_clk_dis()
 * соответственно, а также блок, для которого предназначен клок в большинстве
 * случаев может быть сброшен с помощью chip_per_rst() (за исключением
 * помеченных надписью clock only). */
typedef  enum {
    CHIP_PER_ID_FLASH,
    CHIP_PER_ID_SRAM,
    CHIP_PER_ID_CRC,

    CHIP_PER_ID_BKP,
    CHIP_PER_ID_PWR,

    CHIP_PER_ID_AFIO,
    CHIP_PER_ID_GPIOA,
    CHIP_PER_ID_GPIOB,
    CHIP_PER_ID_GPIOC,
    CHIP_PER_ID_GPIOD,

    CHIP_PER_ID_DMA1,

    CHIP_PER_ID_TIM1,
    CHIP_PER_ID_TIM2,
    CHIP_PER_ID_TIM3,
    CHIP_PER_ID_TIM4,
    CHIP_PER_ID_LPTIM1,
    CHIP_PER_ID_USART1,
    CHIP_PER_ID_USART2,
    CHIP_PER_ID_USART3,
    CHIP_PER_ID_USART4,
    CHIP_PER_ID_SPI1,
    CHIP_PER_ID_SPI2,
    CHIP_PER_ID_I2C1,
    CHIP_PER_ID_I2C2,
    CHIP_PER_ID_ADC1,
    CHIP_PER_ID_WWDG,

    CHIP_PER_ID_CAN1,
    CHIP_PER_ID_USBFS,
    CHIP_PER_ID_USBPD,

    CHIP_PER_ID_CLKOUT
}  t_chip_per_id;

void chip_per_rst(t_chip_per_id per_id);
void chip_per_clk_en(t_chip_per_id per_id);
void chip_per_clk_dis(t_chip_per_id per_id);
bool chip_per_clk_is_en(t_chip_per_id per_id);

#endif // CHIP_PER_CTL_H
