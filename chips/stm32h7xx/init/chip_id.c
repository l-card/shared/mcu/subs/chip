#include "chip.h"


static const struct {
    uint16_t id;
    const char *name;
} f_dev_names[] = {
    {CHIP_DEV_ID_H74X, "stm32h74x"},
    {CHIP_DEV_ID_H72X, "stm32h72x"},
};

static const struct {
    uint16_t id;
    char sym;
} f_rev_syms[] = {
    { CHIP_REV_ID_V, 'V'},
    { CHIP_REV_ID_X, 'X'},
    { CHIP_REV_ID_Y, 'Y'},
    { CHIP_REV_ID_Z, 'Z'},
    { CHIP_REV_ID_A, 'A'},
};



const char *chip_get_device_name() {
    const uint16_t dev_id = chip_get_device_id();
    const char *dev_name = "";
    for (unsigned i = 0; i < sizeof(f_dev_names)/sizeof(f_dev_names[0]); ++i) {
        if (dev_id == f_dev_names[i].id) {
            dev_name = f_dev_names[i].name;
            break;
        }
    }
    return dev_name;
}


char chip_get_revision_symbol() {
    const uint16_t rev_id = chip_get_revision_id();
    char rev_sym = '\x0';
    for (unsigned i = 0; i < sizeof(f_rev_syms)/sizeof(f_rev_syms[0]); ++i) {
        if (rev_id == f_rev_syms[i].id) {
            rev_sym = f_rev_syms[i].sym;
            break;
        }
    }
    return rev_sym;
}

