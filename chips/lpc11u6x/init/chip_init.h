#ifndef CHIP_INIT_H
#define CHIP_INIT_H

#include "chip_config.h"
#include "chip_cfg_defs.h"





#define chip_wdt_reset() do { \
        CHIP_REGS_WWDT->FEED = 0xAA; \
        CHIP_REGS_WWDT->FEED = 0x55; \
    } while(0)

#endif // CHIP_INIT_H
