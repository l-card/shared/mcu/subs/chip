#ifndef CHIP_STM32H7XX_REGS_MDMA_H_
#define CHIP_STM32H7XX_REGS_MDMA_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"
#include "regs_dma.h"

#define CHIP_MDMA_CHANNEL_CNT   16

#define CHIP_MDMA_LL_ALIGN_SIZE  8 /* размер, на который должны быть выравнены Linked-List структуры */



typedef struct {
    __IO uint32_t CTCR;     /* New CTCR register configuration for the given MDMA linked list node   */
    __IO uint32_t CBNDTR;   /* New CBNDTR register configuration for the given MDMA linked list node */
    __IO uint32_t CSAR;     /* New CSAR register configuration for the given MDMA linked list node   */
    __IO uint32_t CDAR;     /* New CDAR register configuration for the given MDMA linked list node   */
    __IO uint32_t CBRUR;    /* New CBRUR register configuration for the given MDMA linked list node  */
    __IO uint32_t CLAR;     /* New CLAR register configuration for the given MDMA linked list node   */
    __IO uint32_t CTBR;     /* New CTBR register configuration for the given MDMA linked list node   */
    __IO uint32_t Reserved; /* Reserved register*/
    __IO uint32_t CMAR;     /* New CMAR register configuration for the given MDMA linked list node   */
    __IO uint32_t CMDR;     /* New CMDR register configuration for the given MDMA linked list node   */
} CHIP_REGS_MDMA_LL_T;

typedef struct {
    __IO uint32_t  CISR;      /* MDMA channel x interrupt/status register,             Address offset: 0x40 */
    __IO uint32_t  CIFCR;     /* MDMA channel x interrupt flag clear register,         Address offset: 0x44 */
    __IO uint32_t  CESR;      /* MDMA Channel x error status register,                 Address offset: 0x48 */
    /* возможность доступа как ко всему регистру, так и к старшей части, для
     * установки SWRQ без изменения остальной части */
    union {
        __IO uint32_t  CCR;   /* MDMA channel x control register,                      Address offset: 0x4C */
        struct {
            uint16_t CCR_RESERVED1;
            __IO uint8_t CCRH;
            uint8_t CCR_RESERVED2;
        };
    };
    /* возможность оперировать с регистрами как напрямую, так и как
     * со структурой для linked-list */
    union {
        struct {
            __IO uint32_t  CTCR;      /* MDMA channel x Transfer Configuration register,       Address offset: 0x50 */
            __IO uint32_t  CBNDTR;    /* MDMA Channel x block number of data register,         Address offset: 0x54 */
            __IO uint32_t  CSAR;      /* MDMA channel x source address register,               Address offset: 0x58 */
            __IO uint32_t  CDAR;      /* MDMA channel x destination address register,          Address offset: 0x5C */
            __IO uint32_t  CBRUR;     /* MDMA channel x Block Repeat address Update register,  Address offset: 0x60 */
            __IO uint32_t  CLAR;      /* MDMA channel x Link Address register,                 Address offset: 0x64 */
            __IO uint32_t  CTBR;      /* MDMA channel x Trigger and Bus selection Register,    Address offset: 0x68 */
            uint32_t       RESERVED0; /* Reserved,                                             Address offset: 0x6C */
            __IO uint32_t  CMAR;      /* MDMA channel x Mask address register,                 Address offset: 0x70 */
            __IO uint32_t  CMDR;      /* MDMA channel x Mask Data register,                    Address offset: 0x74 */
        };
        CHIP_REGS_MDMA_LL_T LL;
    };
    uint32_t       RESERVED1[2];
} CHIP_REGS_MDMA_CHANNEL_T;

typedef struct {
    __IO uint32_t  GISR0;   /* MDMA Global Interrupt/Status Register 0,          Address offset: 0x00 */
    uint32_t       RESERVED[15];
    CHIP_REGS_MDMA_CHANNEL_T CH[CHIP_MDMA_CHANNEL_CNT];
} CHIP_REGS_MDMA_T;

#define CHIP_REGS_MDMA                          ((CHIP_REGS_MDMA_T *) CHIP_MEMRGN_ADDR_PERIPH_MDMA)


/* MDMA Trigger Request */
#define CHIP_MDMA_REQ_DMA_TC_IF(dma, ch)        (CHIP_DMA_STREAM_CNT*((dma) - 1) + (ch)) /* DMAx stream y transfer complete */
#define CHIP_MDMA_REQ_LTDC_LI_IT                16             /* LTDC line interrupt */
#if CHIP_DEV_SUPPORT_JPGDEC
#define CHIP_MDMA_REQ_JPEG_IFT_TRG              17             /* JPEG input FIFO threshold */
#define CHIP_MDMA_REQ_JPEG_IFNF_TRG             18             /* JPEG input FIFO not full */
#define CHIP_MDMA_REQ_JPEG_OFT_TRG              19             /* JPEG output FIFO threshold */
#define CHIP_MDMA_REQ_JPEG_OFNE_TRG             20             /* JPEG output FIFO not empty */
#define CHIP_MDMA_REQ_JPEG_OEC_TRG              21             /* JPEG end of conversion */
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_MDMA_REQ_OCTOSPI1_FT_TRG           22             /* OCTOSPI1 FIFO threshold */
#define CHIP_MDMA_REQ_OCTOSPI1_TC_TRG           23             /* OCTOSPI1 transfer complete */
#else
#define CHIP_MDMA_REQ_QUADSPI_FT_TRG            22             /* QUADSPI FIFO threshold */
#define CHIP_MDMA_REQ_QUADSPI_TC_TRG            23             /* QUADSPI transfer complete */
#endif
#define CHIP_MDMA_REQ_DMA2D_CLUT_TRG            24             /* DMA2D CLUT transfer complete */
#define CHIP_MDMA_REQ_DMA2D_TC_TRG              25             /* DMA2D transfer complete */
#define CHIP_MDMA_REQ_DMA2D_TW_TRG              26             /* DMA2D transfer watermark */
#if CHIP_DEV_SUPPORT_DSI
#define CHIP_MDMA_REQ_DSI_TE_TRG                27             /* Tearing effect */
#define CHIP_MDMA_REQ_DSI_EOR_TRG               28             /* End of refresh */
#endif
#define CHIP_MDMA_REQ_SDMMC1_DATAEND_TRG        29             /* SDMMC1 end of data */
#define CHIP_MDMA_REQ_SDMMC1_BUFFEND_TRG        30             /* SDMMC1 end of buffer */
#define CHIP_MDMA_REQ_SDMMC1_CMDEND_TRG         31             /* SDMMC1 end of command */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_MDMA_REQ_OCTOSPI2_FT_TRG           32             /* OCTOSPI2 FIFO threshold */
#define CHIP_MDMA_REQ_OCTOSPI2_TC_TRG           33             /* OCTOSPI2 transfer complete */
#endif



/*********** MDMA global interrupt/status register (MDMA_GISR0) ***************/
#define CHIP_REGFLD_MDMA_GISR0_GIF(x)           (0x00000001UL <<  (x)) /* Channel x global interrupt flag */

#define CHIP_REGMSK_MDMA_GISR0_RESERVED         (0xFFFF0000UL)

/*********** MDMA channel x interrupt/status register (MDMA_CxISR) ************/
#define CHIP_REGFLD_MDMA_CISR_TEIF              (0x00000001UL <<  0U) /* Channel x transfer error interrupt flag */
#define CHIP_REGFLD_MDMA_CISR_CTCIF             (0x00000001UL <<  1U) /* Channel x Channel Transfer Complete interrupt flag */
#define CHIP_REGFLD_MDMA_CISR_BRTIF             (0x00000001UL <<  2U) /* Channel x block repeat transfer complete interrupt flag */
#define CHIP_REGFLD_MDMA_CISR_BTIF              (0x00000001UL <<  3U) /* Channel x block transfer complete interrupt flag */
#define CHIP_REGFLD_MDMA_CISR_TCIF              (0x00000001UL <<  4U) /* Channel x buffer transfer complete interrupt flag */
#define CHIP_REGFLD_MDMA_CISR_CRQA              (0x00000001UL << 16U) /* Channel x request active flag */

#define CHIP_REGMSK_MDMA_CISR_RESERVED          (0xFFFEFFE0UL)

/*********** MDMA channel x interrupt flag clear register (MDMA_CxIFCR) *******/
#define CHIP_REGFLD_MDMA_CIFCR_CTEIF            (0x00000001UL <<  0U) /* Channel x clear transfer error interrupt flag */
#define CHIP_REGFLD_MDMA_CIFCR_CCTCIF           (0x00000001UL <<  1U) /* Clear Channel transfer complete interrupt flag for channel x */
#define CHIP_REGFLD_MDMA_CIFCR_CBRTIF           (0x00000001UL <<  2U) /* Channel x clear block repeat transfer complete interrupt flag */
#define CHIP_REGFLD_MDMA_CIFCR_CBTIF            (0x00000001UL <<  3U) /* Channel x Clear block transfer complete interrupt flag */
#define CHIP_REGFLD_MDMA_CIFCR_CLTCIF           (0x00000001UL <<  4U) /* CLear Transfer buffer Complete Interrupt Flag for channel */

#define CHIP_REGMSK_MDMA_CIFCR_ALL              (0x0000001FUL)
#define CHIP_REGMSK_MDMA_CIFCR_RESERVED         (0xFFFFFFE0UL)

/*********** MDMA channel x error status register (MDMA_CxESR)*****************/
#define CHIP_REGFLD_MDMA_CESR_TEA               (0x0000007FUL <<  0U) /* Transfer Error Address */
#define CHIP_REGFLD_MDMA_CESR_TED               (0x00000001UL <<  7U) /* Transfer Error Direction */
#define CHIP_REGFLD_MDMA_CESR_TELD              (0x00000001UL <<  8U) /* Transfer Error Link Data */
#define CHIP_REGFLD_MDMA_CESR_TEMD              (0x00000001UL <<  9U) /* Transfer Error Mask Data */
#define CHIP_REGFLD_MDMA_CESR_ASE               (0x00000001UL << 10U) /* Address/Size Error */
#define CHIP_REGFLD_MDMA_CESR_BSE               (0x00000001UL << 11U) /* Block Size Error */

#define CHIP_REGMSK_MDMA_CESR_RESERVED          (0xFFFFF000UL)

/*********** MDMA channel x control register (MDMA_CxCR) **********************/
#define CHIP_REGFLD_MDMA_CCR_EN                 (0x00000001UL <<  0U) /* Channel enable / flag channel ready when read low */
#define CHIP_REGFLD_MDMA_CCR_TEIE               (0x00000001UL <<  1U) /* Transfer error interrupt enable */
#define CHIP_REGFLD_MDMA_CCR_CTCIE              (0x00000001UL <<  2U) /* Channel Transfer Complete interrupt enable */
#define CHIP_REGFLD_MDMA_CCR_BRTIE              (0x00000001UL <<  3U) /* Block Repeat transfer interrupt enable */
#define CHIP_REGFLD_MDMA_CCR_BTIE               (0x00000001UL <<  4U) /* Block Transfer interrupt enable */
#define CHIP_REGFLD_MDMA_CCR_TCIE               (0x00000001UL <<  5U) /* buffer Transfer Complete interrupt enable */
#define CHIP_REGFLD_MDMA_CCR_PL                 (0x00000003UL <<  6U) /* Priority level */
#define CHIP_REGFLD_MDMA_CCR_BEX                (0x00000001UL << 12U) /* Byte Endianness eXchange */
#define CHIP_REGFLD_MDMA_CCR_HEX                (0x00000001UL << 13U) /* Half word Endianness eXchange */
#define CHIP_REGFLD_MDMA_CCR_WEX                (0x00000001UL << 14U) /* Word Endianness eXchange */
#define CHIP_REGFLD_MDMA_CCR_SWRQ               (0x00000001UL << 16U) /* SW ReQuest */

#define CHIP_REGFLD_MDMA_CCRH_SWRQ              (0x00000001UL <<  0U) /* SW ReQuest (byte access)*/

#define CHIP_REGMSK_MDMA_CCR_RESERVED           (0xFFFE8F00UL)

#define CHIP_REGFLDVAL_MDMA_CCR_PL_LOW          0
#define CHIP_REGFLDVAL_MDMA_CCR_PL_MED          1
#define CHIP_REGFLDVAL_MDMA_CCR_PL_HIGH         2
#define CHIP_REGFLDVAL_MDMA_CCR_PL_VHIGH        3


/*********** MDMA channel x transfer configuration register (MDMA_CxTCR) ******/
#define CHIP_REGFLD_MDMA_CTCR_SINC              (0x00000003UL <<  0U) /* Source increment mode */
#define CHIP_REGFLD_MDMA_CTCR_DINC              (0x00000003UL <<  2U) /* Destination increment mode */
#define CHIP_REGFLD_MDMA_CTCR_SSIZE             (0x00000003UL <<  4U) /* Source data size */
#define CHIP_REGFLD_MDMA_CTCR_DSIZE             (0x00000003UL <<  6U) /* Destination data size */
#define CHIP_REGFLD_MDMA_CTCR_SINCOS            (0x00000003UL <<  8U) /* Source increment offset size */
#define CHIP_REGFLD_MDMA_CTCR_DINCOS            (0x00000003UL << 10U) /* Destination increment offset size */
#define CHIP_REGFLD_MDMA_CTCR_SBURST            (0x00000007UL << 12U) /* Source burst transfer configuration */
#define CHIP_REGFLD_MDMA_CTCR_DBURST            (0x00000007UL << 15U) /* Destination burst transfer configuration */
#define CHIP_REGFLD_MDMA_CTCR_TLEN              (0x0000007FUL << 18U) /* buffer Transfer Length (number of bytes - 1) */
#define CHIP_REGFLD_MDMA_CTCR_PKE               (0x00000001UL << 25U) /* PacK Enable */
#define CHIP_REGFLD_MDMA_CTCR_PAM               (0x00000003UL << 26U) /* Padding/Alignment Mode */
#define CHIP_REGFLD_MDMA_CTCR_TRGM              (0x00000003UL << 28U) /* Trigger Mode */
#define CHIP_REGFLD_MDMA_CTCR_SWRM              (0x00000001UL << 30U) /* SW Request Mode */
#define CHIP_REGFLD_MDMA_CTCR_BWM               (0x00000001UL << 31U) /* Bufferable Write Mode */


#define CHIP_REGFLDVAL_MDMA_INC_FIXED           0 /* address pointer is fixed */
#define CHIP_REGFLDVAL_MDMA_INC_INC             2 /* address pointer is incremented after each data transfer */
#define CHIP_REGFLDVAL_MDMA_INC_DEC             3 /* address pointer is decremented after each data transfer */

#define CHIP_REGFLDVAL_MDMA_CTCR_SIZE_8         0
#define CHIP_REGFLDVAL_MDMA_CTCR_SIZE_16        1
#define CHIP_REGFLDVAL_MDMA_CTCR_SIZE_32        2
#define CHIP_REGFLDVAL_MDMA_CTCR_SIZE_64        3

#define CHIP_REGFLDVAL_MDMA_CTCR_INCOS_8        0
#define CHIP_REGFLDVAL_MDMA_CTCR_INCOS_16       1
#define CHIP_REGFLDVAL_MDMA_CTCR_INCOS_32       2
#define CHIP_REGFLDVAL_MDMA_CTCR_INCOS_64       3

#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_1        0
#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_2        1
#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_4        2
#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_8        3
#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_16       4
#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_32       5
#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_64       6
#define CHIP_REGFLDVAL_MDMA_CTCR_BURST_128      7

#define CHIP_REGFLDVAL_MDMA_CTCR_PAM_RAL_ZPAD   0 /* Right aligned, padded w/ 0s */
#define CHIP_REGFLDVAL_MDMA_CTCR_PAM_RAL_SPAD   1 /* Right aligned, sign extended */
#define CHIP_REGFLDVAL_MDMA_CTCR_PAM_LAL_ZPAD   1 /* Left aligned (padded with 0s)*/

#define CHIP_REGFLDVAL_MDMA_CTCR_TRGM_BUF       0 /* Each MDMA request triggers a buffer transfer */
#define CHIP_REGFLDVAL_MDMA_CTCR_TRGM_BLOCK     1 /* Each MDMA request triggers a block transfer */
#define CHIP_REGFLDVAL_MDMA_CTCR_TRGM_RP_BLOCK  2 /* Each MDMA request triggers a repeated block transfer */
#define CHIP_REGFLDVAL_MDMA_CTCR_TRGM_FULL      3 /* Each MDMA request triggers the transfer of the whole data (llist) */


/*********** MDMA channel x block number of data register (MDMA_CxBNDTR) ******/
#define CHIP_REGFLD_MDMA_CBNDTR_BNDT            (0x0001FFFFUL <<  0U) /* Block Number of data bytes to transfer */
#define CHIP_REGFLD_MDMA_CBNDTR_BRSUM           (0x00000001UL << 18U) /* Block Repeat Source address Update Mode */
#define CHIP_REGFLD_MDMA_CBNDTR_BRDUM           (0x00000001UL << 19U) /* Block Repeat Destination address Update Mode */
#define CHIP_REGFLD_MDMA_CBNDTR_BRC             (0x00000FFFUL << 20U) /* Block Repeat Count */

#define CHIP_REGMSK_MDMA_CBNDTR_RESERVED        (0xFFFDFFFFUL)

#define CHIP_REGFLDVAL_MDMA_CBNDTR_BRUM_ADD     0
#define CHIP_REGFLDVAL_MDMA_CBNDTR_BRUM_SUB     1

/*********** MDMA channel x source address register (MDMA_CxSAR) **************/
#define CHIP_REGFLD_MDMA_CSAR_SAR               (0xFFFFFFFFUL <<  0U) /* Source address */

/*********** MDMA channel x destination address register (MDMA_CxDAR) *********/
#define CHIP_REGFLD_MDMA_CDAR_DAR               (0xFFFFFFFFUL <<  0U) /* Destination address */

/*********** MDMA channel x block repeat address update register (MDMA_CxBRUR)*/
#define CHIP_REGFLD_MDMA_CBRUR_SUV              (0x0000FFFFUL <<  0U) /* Source address Update Value */
#define CHIP_REGFLD_MDMA_CBRUR_DUV              (0x0000FFFFUL << 16U) /* Destination address Update Value */

/*********** MDMA channel x link address register (MDMA_CxLAR) ****************/
#define CHIP_REGFLD_MDMA_CLAR_LAR               (0xFFFFFFFFUL <<  0U) /* Link Address Register */

/*********** MDMA channel x trigger and bus selection register (MDMA_CxTBR) ***/
#define CHIP_REGFLD_MDMA_CTBR_TSEL              (0x0000003FUL <<  0U) /* Trigger SELection */
#define CHIP_REGFLD_MDMA_CTBR_SBUS              (0x00000001UL << 16U) /* Source BUS select */
#define CHIP_REGFLD_MDMA_CTBR_DBUS              (0x00000001UL << 17U) /* Destination BUS select */

#define CHIP_REGMSK_MDMA_CTBR_RESERVED          (0xFFFCFFC0UL)

#define CHIP_REGFLD_MDMA_CTBR_BUS_SYS_AXI       0 /* the system/AXI bus is used */
#define CHIP_REGFLD_MDMA_CTBR_BUS_AHB_TCM       1 /* the AHB bus/TCM is used */

#ifdef CHIP_CORETYPE_CM7
#define CHIP_MDMA_MDMA_CTBR_BUS_VAL(addr) ((((addr) >= CHIP_MEMRGN_ADDR_DTCM) && ((addr) < (CHIP_MEMRGN_ADDR_DTCM + CHIP_MEMRGN_SIZE_DTCM))) ? CHIP_REGFLD_MDMA_CTBR_BUS_AHB_TCM : CHIP_REGFLD_MDMA_CTBR_BUS_SYS_AXI)
#else
#define CHIP_MDMA_MDMA_CTBR_BUS_VAL(addr) CHIP_REGFLD_MDMA_CTBR_BUS_SYS_AXI
#endif

/*********** MDMA channel x mask address register (MDMA_CxMAR) ****************/
#define CHIP_REGFLD_MDMA_CMAR_MAR               (0xFFFFFFFFUL <<  0U) /* Mask address */

/*********** MDMA channel x mask data register (MDMA_CxMDR) *******************/
#define CHIP_REGFLD_MDMA_CMDR_MDR               (0xFFFFFFFFUL <<  0U) /* Mask Data */

#endif // CHIP_STM32H7XX_REGS_MDMA_H_
