#include "chip.h"

void chip_clk_init(void);



/* CRP */
static __attribute__ ((section(".crp_sect"))) __attribute__ ((used)) const uint32_t f_crp_mode = CHIP_CRP_MODE;


void chip_init(void) {
    chip_clk_init();
}
