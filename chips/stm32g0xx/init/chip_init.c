#include "chip.h"

void chip_clk_init(void);
void chip_pwr_init(void);


#ifdef CHIP_CFG_PIN_REMAP_PA11_PA9
    #define CHIP_PIN_REMAP_PA11_PA9     CHIP_CFG_PIN_REMAP_PA11_PA9
#else
    #define CHIP_PIN_REMAP_PA11_PA9     0
#endif
#ifdef CHIP_CFG_PIN_REMAP_PA12_PA10
    #define CHIP_PIN_REMAP_PA12_PA10    CHIP_CFG_PIN_REMAP_PA12_PA10
#else
    #define CHIP_PIN_REMAP_PA12_PA10    0
#endif


void chip_init(void){
    chip_pwr_init();
    chip_clk_init();

#if CHIP_PIN_REMAP_PA11_PA9 || CHIP_PIN_REMAP_PA12_PA10
#if !CHIP_CFG_CLK_SYSCFG_EN
    chip_per_clk_en(CHIP_PER_ID_SYSCFG);
#endif
    CHIP_REGS_SYSCFG->CFGR1 |=
            (CHIP_PIN_REMAP_PA11_PA9 ? CHIP_REGFLD_SYSCFG_CFGR1_PA11_RMP : 0)
            | (CHIP_PIN_REMAP_PA12_PA10 ? CHIP_REGFLD_SYSCFG_CFGR1_PA12_RMP : 0);
#if !CHIP_CFG_CLK_SYSCFG_EN
    chip_per_clk_dis(CHIP_PER_ID_SYSCFG);
#endif
#endif
}

