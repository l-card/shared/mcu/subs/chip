#ifndef REGS_BUTN_H
#define REGS_BUTN_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t BTN_STATUS;                   /* 0x0: Button status */
    __IO uint32_t BTN_IRQ_MASK;                 /* 0x4: Button interrupt mask */
    __IO uint32_t LED_INTENSE;                  /* 0x8: Indicator brightness */
} CHIP_REGS_BUTN_T;

#define CHIP_REGS_BUTN            ((CHIP_REGS_BUTN_T *)CHIP_MEMRGN_ADDR_BUTN)


/*********** Button status / Button interrupt mask - BTN_STATUS / BTN_IRQ_MASK */
#define CHIP_REGFLD_BUTN_BTN_STATUS_XWCLICK_3           (0x00000001UL << 30U) /* (RW1C/RW) The power button remains pressed and the wake button is triple clicked. */
#define CHIP_REGFLD_BUTN_BTN_STATUS_XWCLICK_2           (0x00000001UL << 29U) /* (RW1C/RW) The power button remains pressed and the wake button is double clicked. */
#define CHIP_REGFLD_BUTN_BTN_STATUS_XWCLICK_1           (0x00000001UL << 28U) /* (RW1C/RW) The power button remains pressed and the wake button is single clicked. */
#define CHIP_REGFLD_BUTN_BTN_STATUS_WCLICK_3            (0x00000001UL << 26U) /* (RW1C/RW) Wake button tripple click status */
#define CHIP_REGFLD_BUTN_BTN_STATUS_WCLICK_2            (0x00000001UL << 25U) /* (RW1C/RW) Wake button double click status */
#define CHIP_REGFLD_BUTN_BTN_STATUS_WCLICK_1            (0x00000001UL << 24U) /* (RW1C/RW) Wake button single click status */
#define CHIP_REGFLD_BUTN_BTN_STATUS_XPCLICK_3           (0x00000001UL << 22U) /* (RW1C/RW) The wake button remains pressed and the power button is triple clicked. */
#define CHIP_REGFLD_BUTN_BTN_STATUS_XPCLICK_2           (0x00000001UL << 21U) /* (RW1C/RW) The wake button remains pressed and the power button is double clicked. */
#define CHIP_REGFLD_BUTN_BTN_STATUS_XPCLICK_1           (0x00000001UL << 20U) /* (RW1C/RW) The wake button remains pressed and the power button is single clicked. */
#define CHIP_REGFLD_BUTN_BTN_STATUS_PCLICK_3            (0x00000001UL << 18U) /* (RW1C/RW) Wake wake tripple click status */
#define CHIP_REGFLD_BUTN_BTN_STATUS_PCLICK_2            (0x00000001UL << 17U) /* (RW1C/RW) Wake wake double click status */
#define CHIP_REGFLD_BUTN_BTN_STATUS_PCLICK_1            (0x00000001UL << 16U) /* (RW1C/RW) Wake wake single click status */
#define CHIP_REGFLD_BUTN_BTN_STATUS_DBTN_PRESS_16S      (0x00000001UL << 11U) /* (RW1C/RW) Double button press state (more than 16 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_DBTN_PRESS_8S       (0x00000001UL << 10U) /* (RW1C/RW) Double button press state (more than 8 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_DBTN_PRESS_0_5S     (0x00000001UL <<  9U) /* (RW1C/RW) Double button press state (0.5 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_DBTN_TOUCH          (0x00000001UL <<  8U) /* (RW1C/RW) Double button press state (touch) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_WBTN_PRESS_16S      (0x00000001UL <<  7U) /* (RW1C/RW) Wake-up button press state (more than 16 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_WBTN_PRESS_8S       (0x00000001UL <<  6U) /* (RW1C/RW) Wake-up button press state (more than 8 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_WBTN_PRESS_0_5S     (0x00000001UL <<  5U) /* (RW1C/RW) Wake-up button press state (0.5 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_WBTN_TOUCH          (0x00000001UL <<  4U) /* (RW1C/RW) Wake-up button press state (touch) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_PBTN_PRESS_16S      (0x00000001UL <<  3U) /* (RW1C/RW) Power button press state (more than 16 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_PBTN_PRESS_8S       (0x00000001UL <<  2U) /* (RW1C/RW) Power button press state (more than 8 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_PBTN_PRESS_0_5S     (0x00000001UL <<  1U) /* (RW1C/RW) Power button press state (0.5 second) */
#define CHIP_REGFLD_BUTN_BTN_STATUS_PBTN_TOUCH          (0x00000001UL <<  0U) /* (RW1C/RW) Power button press state (touch) */
#define CHIP_REGMSK_BUTN_BTN_STATUS_RESERVED            (0x8888F000UL)

/*********** Indicator brightness - LED_INTENSE *******************************/
#define CHIP_REGFLD_BUTN_LED_INTENSE_RLED               (0x0000000FUL << 16U) /* (RW) Wake-up indicator light brightness (0-15) */
#define CHIP_REGFLD_BUTN_LED_INTENSE_PLED               (0x0000000FUL <<  0U) /* (RW) Power indicator light brightness (0-15) */
#define CHIP_REGMSK_BUTN_LED_INTENSE_RESERVED           (0xFFF0FFF0UL)

#endif // REGS_BUTN_H
