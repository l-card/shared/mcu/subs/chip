#ifndef CHIP_STM32F0XX_REGS_SYSCFG_H
#define CHIP_STM32F0XX_REGS_SYSCFG_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
  __IO uint32_t CFGR1;       /*!< SYSCFG configuration register 1,                           Address offset: 0x00 */
       uint32_t RESERVED;    /*!< Reserved,                                                                  0x04 */
  __IO uint32_t EXTICR[4];   /*!< SYSCFG external interrupt configuration register,     Address offset: 0x14-0x08 */
  __IO uint32_t CFGR2;       /*!< SYSCFG configuration register 2,                           Address offset: 0x18 */
  uint32_t RESERVED1[25];  /*!< Reserved                                                           0x1C */
__IO uint32_t ITLINE[32];     /*!< SYSCFG configuration ITLINE register,             Address offset: 0x80 */
}  CHIP_REGS_SYSCFG_T;

#define CHIP_REGS_SYSCFG          ((CHIP_REGS_SYSCFG_T *) CHIP_MEMRGN_ADDR_PERIPH_SYSCFG)


/*********** SYSCFG configuration register 1 (SYSCFG_CFGR1) *******************/
#define CHIP_REGFLD_SYSCFG_CFGR1_MEM_MODE               (0x00000003UL <<  0U)  /* (RW) Memory mapping selection bits */
#if defined CHIP_DEV_STM32F04X
#define CHIP_REGFLD_SYSCFG_CFGR1_PA11_PA12_RMP          (0x00000001UL <<  4U)  /* (RW) PA11 and PA12 remapping bit for small packages (28 and 20 pins). Available on STM32F04x devices only. */
#endif
#if defined CHIP_DEV_STM32F09X
#define CHIP_REGFLD_SYSCFG_CFGR1_IR_MOD                 (0x00000003UL <<  6U)  /* (RW) IR Modulation Envelope signal selection. Available on STM32F09x devices only. */
#endif
#if defined CHIP_DEV_STM32F03X || defined CHIP_DEV_STM32F04X || defined CHIP_DEV_STM32F05X || defined CHIP_DEV_STM32F07X
#define CHIP_REGFLD_SYSCFG_CFGR1_ADC_DMA_RMP            (0x00000001UL <<  8U)  /* (RW) ADC DMA request remapping bit. Available on STM32F03x, STM32F04x, STM32F05x and STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_USART1_TX_DMA_RMP      (0x00000001UL <<  9U)  /* (RW) USART1_TX DMA request remapping bit. Available on STM32F03x, STM32F04x, STM32F05x and STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_USART1_RX_DMA_RMP      (0x00000001UL << 10U)  /* (RW) USART1_RX DMA request remapping bit. Available on STM32F03x, STM32F04x, STM32F05x and STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_TIM16_DMA_RMP          (0x00000001UL << 11U)  /* (RW) TIM16 DMA request remapping bit. Available on STM32F03x, STM32F04x, STM32F05x and STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_TIM17_DMA_RMP          (0x00000001UL << 12U)  /* (RW) TIM17 DMA request remapping bit. Available on STM32F03x, STM32F04x, STM32F05x and STM32F07x devices only. */
#endif
#ifdef CHIP_DEV_STM32F07X
#define CHIP_REGFLD_SYSCFG_CFGR1_TIM16_DMA_RMP2         (0x00000001UL << 13U)  /* (RW) TIM16 alternate DMA request remapping bit. Available on STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_TIM17_DMA_RMP2         (0x00000001UL << 14U)  /* (RW) TIM17 alternate DMA request remapping bit. Available on STM32F07x devices only. */
#endif
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB6_FMP            (0x00000001UL << 16U)  /* (RW) FM+ driving capability activation for PB6. */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB7_FMP            (0x00000001UL << 17U)  /* (RW) FM+ driving capability activation for PB7. */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB8_FMP            (0x00000001UL << 18U)  /* (RW) FM+ driving capability activation for PB8. */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB9_FMP            (0x00000001UL << 19U)  /* (RW) FM+ driving capability activation for PB9. */
#if !defined CHIP_DEV_STM32F05X
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C1_FMP               (0x00000001UL << 20U)  /* (RW) FM+ driving capability activation for I2C1. Not available on STM32F05x devices. */
#endif
#if defined CHIP_DEV_STM32F07X || defined CHIP_DEV_STM32F09X
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C2_FMP               (0x00000001UL << 21U)  /* (RW) FM+ driving capability activation for I2C2. Available on STM32F07x and STM32F09x devices only. */
#endif
#if defined CHIP_DEV_STM32F03X || defined CHIP_DEV_STM32F04X || defined CHIP_DEV_STM32F09X
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PA9_FMP            (0x00000001UL << 22U)  /* (RW) FM+ driving capability activation for PA9.  Available on STM32F03x, STM32F04x and STM32F09x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PA10_FMP           (0x00000001UL << 23U)  /* (RW) FM+ driving capability activation for PA10. Available on STM32F03x, STM32F04x and STM32F09x devices only.  */
#endif
#ifdef CHIP_DEV_STM32F07X
#define CHIP_REGFLD_SYSCFG_CFGR1_SPI2_DMA_RMP           (0x00000001UL << 24U)  /* (RW) SPI2 DMA request remapping bit Available on STM32F07x devices only.  */
#define CHIP_REGFLD_SYSCFG_CFGR1_USART2_DMA_RMP         (0x00000001UL << 25U)  /* (RW) USART2 DMA request remapping bit Available on STM32F07x devices only.  */
#define CHIP_REGFLD_SYSCFG_CFGR1_USART3_DMA_RMP         (0x00000001UL << 26U)  /* (RW) USART3 DMA request remapping bit. Available on STM32F07x devices only.  */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C1_DMA_RMP           (0x00000001UL << 27U)  /* (RW) I2C1 DMA request remapping bit. Available on STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_TIM1_DMA_RMP           (0x00000001UL << 28U)  /* (RW) TIM1 DMA request remapping bit. Available on STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_TIM2_DMA_RMP           (0x00000001UL << 29U)  /* (RW) TIM2 DMA request remapping bit. Available on STM32F07x devices only. */
#define CHIP_REGFLD_SYSCFG_CFGR1_TIM31_DMA_RMP          (0x00000001UL << 30U)  /* (RW) TIM3 DMA request remapping bit. Available on STM32F07x devices only. */
#endif
#define CHIP_REGMSK_SYSCFG_CFGR1_RESERVED               (0x8000802CUL)

#define CHIP_REGFLDVAL_SYSCFG_CFGR1_MEM_MODE_MAIN_FLASH 0
#define CHIP_REGFLDVAL_SYSCFG_CFGR1_MEM_MODE_SYS_FLASH  1
#define CHIP_REGFLDVAL_SYSCFG_CFGR1_MEM_MODE_SRAM       2

#define CHIP_REGFLDVAL_SYSCFG_CFGR1_IR_MOD_TIM16        0
#define CHIP_REGFLDVAL_SYSCFG_CFGR1_IR_MOD_USART1       1
#define CHIP_REGFLDVAL_SYSCFG_CFGR1_IR_MOD_USART4       2


/* общие определения номера регистра и битовой маски настройки выброанного номер EXTI по GPIO */
#define CHIP_SYSCFG_EXTICR_REGNUM(x)                    ((x) / 4)
#define CHIP_SYSCFG_EXTICR_BITMSK(x)                    (0x0000000FUL << (4 * (x) % 4))


/*********** SYSCFG configuration register 2 (SYSCFG_CFGR2) *******************/
#define CHIP_REGFLD_SYSCFG_CFGR2_LOCKUP_LOCK            (0x00000001UL <<  0U)  /* (RW1) Cortex-M0 LOCKUP output connected to TIM1/15/16/17 Break input */
#define CHIP_REGFLD_SYSCFG_CFGR2_SRAM_PARITY_LOCK       (0x00000001UL <<  1U)  /* (RW1) SRAM parity error lock to TIM1/15/16/17 Break input */
#define CHIP_REGFLD_SYSCFG_CFGR2_PVD_LOCK               (0x00000001UL <<  2U)  /* (RW1) PVD interrupt lock to TIM1/15/16/17 Break input */
#define CHIP_REGFLD_SYSCFG_CFGR2_SRAM_PEF               (0x00000001UL <<  8U)  /* (RW1C) SRAM parity error flag */
#define CHIP_REGMSK_SYSCFG_CFGR2_RESERVED               (0xFFFFFEF8UL)


/*********** SYSCFG interrupt line x status registers (SYSCFG_ITLINEx) (x = 0..31) ****/
#define CHIP_REGFLD_SYSCFG_ITLINE0_WWDG                 (0x00000001UL <<  0U)  /* (RO) WWDG interrupt request pending */

#if !defined CHIP_DEV_STM32F0X8
#define CHIP_REGFLD_SYSCFG_ITLINE1_PVDOUT               (0x00000001UL <<  0U)  /* (RO) PVD supply monitoring interrupt request pending (EXTI line 16). */
#endif
#define CHIP_REGFLD_SYSCFG_ITLINE1_VDDIO2               (0x00000001UL <<  1U)  /* (RO) VDDIO2 supply monitoring interrupt request pending (EXTI line 31) */

#define CHIP_REGFLD_SYSCFG_ITLINE2_RTC_WAKEUP           (0x00000001UL <<  0U)  /* (RO) RTC Wake Up interrupt request pending (EXTI line 20)*/
#define CHIP_REGFLD_SYSCFG_ITLINE2_RTC_TSTAMP           (0x00000001UL <<  1U)  /* (RO) RTC Tamper and TimeStamp interrupt request pending (EXTI line 19) */
#define CHIP_REGFLD_SYSCFG_ITLINE2_RTC_ALARM            (0x00000001UL <<  2U)  /* (RO) RTC Alarm interrupt request pending (EXTI line 17) */

#define CHIP_REGFLD_SYSCFG_ITLINE3_FLASH_ITF            (0x00000001UL <<  0U)  /* (RO) Flash interface interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE4_RCC                  (0x00000001UL <<  0U)  /* (RO) Reset and clock control interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE4_CRS                  (0x00000001UL <<  1U)  /* (RO) Clock recovery system interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE5_EXTI0                (0x00000001UL <<  0U)  /* (RO) EXTI line 0 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE5_EXTI1                (0x00000001UL <<  1U)  /* (RO) EXTI line 1 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE6_EXTI2                (0x00000001UL <<  0U)  /* (RO) EXTI line 2 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE6_EXTI3                (0x00000001UL <<  1U)  /* (RO) EXTI line 3 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI4                (0x00000001UL <<  0U)  /* (RO) EXTI line 4 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI5                (0x00000001UL <<  1U)  /* (RO) EXTI line 5 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI6                (0x00000001UL <<  2U)  /* (RO) EXTI line 6 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI7                (0x00000001UL <<  3U)  /* (RO) EXTI line 7 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI8                (0x00000001UL <<  4U)  /* (RO) EXTI line 8 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI9                (0x00000001UL <<  5U)  /* (RO) EXTI line 9 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI10               (0x00000001UL <<  6U)  /* (RO) EXTI line 10 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI11               (0x00000001UL <<  7U)  /* (RO) EXTI line 11 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI12               (0x00000001UL <<  8U)  /* (RO) EXTI line 12 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI13               (0x00000001UL <<  9U)  /* (RO) EXTI line 13 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI14               (0x00000001UL << 10U)  /* (RO) EXTI line 14 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI15               (0x00000001UL << 11U)  /* (RO) EXTI line 15 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE8_TSC_MCE              (0x00000001UL <<  0U)  /* (RO) Touch sensing controller max count error interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE8_TSC_EOA              (0x00000001UL <<  1U)  /* (RO) Touch sensing controller end of acquisition interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE9_DMA1_CH1             (0x00000001UL <<  0U)  /* (RO) DMA1 channel 1 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE10_DMA1_CH2            (0x00000001UL <<  0U)  /* (RO) DMA1 channel 2 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE10_DMA1_CH3            (0x00000001UL <<  1U)  /* (RO) DMA1 channel 3 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE10_DMA2_CH1            (0x00000001UL <<  2U)  /* (RO) DMA2 channel 1 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE10_DMA2_CH2            (0x00000001UL <<  3U)  /* (RO) DMA2 channel 2 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH4            (0x00000001UL <<  0U)  /* (RO) DMA1 channel 4 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH5            (0x00000001UL <<  1U)  /* (RO) DMA1 channel 5 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH6            (0x00000001UL <<  2U)  /* (RO) DMA1 channel 6 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH7            (0x00000001UL <<  3U)  /* (RO) DMA1 channel 7 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH3            (0x00000001UL <<  4U)  /* (RO) DMA2 channel 3 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH4            (0x00000001UL <<  5U)  /* (RO) DMA2 channel 4 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH5            (0x00000001UL <<  6U)  /* (RO) DMA2 channel 5 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE12_ADC                 (0x00000001UL <<  0U)  /* (RO) ADC interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE12_COMP1               (0x00000001UL <<  1U)  /* (RO) Comparator 1 interrupt request pending (EXTI line 21) */
#define CHIP_REGFLD_SYSCFG_ITLINE12_COMP2               (0x00000001UL <<  2U)  /* (RO) Comparator 2 interrupt request pending (EXTI line 22) */

#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_CCU            (0x00000001UL <<  0U)  /* (RO) Timer 1 commutation interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_TRG            (0x00000001UL <<  1U)  /* (RO) Timer 1 trigger interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_UPD            (0x00000001UL <<  2U)  /* (RO) Timer 1 update interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_BRK            (0x00000001UL <<  3U)  /* (RO) Timer 1 break interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE14_TIM1_CC             (0x00000001UL <<  0U)  /* (RO) Timer 1 capture compare interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE15_TIM2                (0x00000001UL <<  0U)  /* (RO) Timer 2 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE16_TIM3                (0x00000001UL <<  0U)  /* (RO) Timer 3 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE17_TIM6                (0x00000001UL <<  0U)  /* (RO) Timer 6 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE17_DAC                 (0x00000001UL <<  1U)  /* (RO) DAC underrun interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE18_TIM7                (0x00000001UL <<  0U)  /* (RO) Timer 7 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE19_TIM14               (0x00000001UL <<  0U)  /* (RO) Timer 14 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE20_TIM15               (0x00000001UL <<  0U)  /* (RO) Timer 15 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE21_TIM16               (0x00000001UL <<  0U)  /* (RO) Timer 16 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE22_TIM17               (0x00000001UL <<  0U)  /* (RO) Timer 17 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE23_I2C1                (0x00000001UL <<  0U)  /* (RO) I2C1 interrupt request pending, combined with EXTI line 23 */

#define CHIP_REGFLD_SYSCFG_ITLINE24_I2C2                (0x00000001UL <<  0U)  /* (RO) I2C2 interrupt request pending*/

#define CHIP_REGFLD_SYSCFG_ITLINE25_SPI1                (0x00000001UL <<  0U)  /* (RO) SPI1 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE26_SPI2                (0x00000001UL <<  0U)  /* (RO) SPI1 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE27_USART1              (0x00000001UL <<  0U)  /* (RO) USART1 interrupt request pending, combined with EXTI line 25 */

#define CHIP_REGFLD_SYSCFG_ITLINE28_USART2              (0x00000001UL <<  0U)  /* (RO) USART2 interrupt request pending, combined with EXTI line 26 */

#define CHIP_REGFLD_SYSCFG_ITLINE29_USART3              (0x00000001UL <<  0U)  /* (RO) USART3 interrupt request pending, combined with EXTI line 28. */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART4              (0x00000001UL <<  1U)  /* (RO) USART4 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART5              (0x00000001UL <<  2U)  /* (RO) USART5 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART6              (0x00000001UL <<  3U)  /* (RO) USART6 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART7              (0x00000001UL <<  4U)  /* (RO) USART7 interrupt request pending */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART8              (0x00000001UL <<  5U)  /* (RO) USART8 interrupt request pending */

#define CHIP_REGFLD_SYSCFG_ITLINE30_CEC                 (0x00000001UL <<  0U)  /* (RO) CEC interrupt request pending, combined with EXTI line 27 */
#define CHIP_REGFLD_SYSCFG_ITLINE30_CAN                 (0x00000001UL <<  1U)  /* (RO) CAN interrupt request pending */


#endif // REGS_SYSCFG_H
