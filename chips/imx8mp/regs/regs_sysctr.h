#ifndef REGS_SYSCTR_H
#define REGS_SYSCTR_H

#include "chip_ioreg_defs.h"

#define CHIP_SYSCTR_CMP_FRAME_CNT   2

typedef struct {
    __IO uint32_t CNTCR;            /* Counter Control Register,                Address offset: 0x00 */
    __I  uint32_t CNTSR;            /* Counter Status Register,                 Address offset: 0x04 */
    __IO uint32_t CNTCV0;           /* Counter Count Value Low Register,        Address offset: 0x08 */
    __IO uint32_t CNTCV1;           /* Counter Count Value High Register,       Address offset: 0x0C */
    uint32_t RESERVED0[4];
    __I  uint32_t CNTFID[3];        /* Frequency Modes Table x Register ,       Address offset: 0x20..0x28 */
    uint32_t RESERVED1[1001];
    __I  uint32_t CNTID0;           /* Counter ID Register,                     Address offset: 0xFD0 */
} CHIP_REGS_SYSCTR_CTRL_T;

typedef struct {
    __I uint32_t CNTCV0;            /* Counter Count Value Low Register,        Address offset: 0x00 */
    __I uint32_t CNTCV1;            /* Counter Count Value High Register,       Address offset: 0x04 */
    uint32_t RESERVED1[1010];
    __I  uint32_t CNTID0;           /* Counter ID Register,                     Address offset: 0xFD0 */
} CHIP_REGS_SYSCTR_RD_T;


typedef struct {
    uint32_t RESERVED0[8];
    __I uint32_t CMPCVL;            /* Compare Count Value Low Register        Address offset: 0x20 */
    __I uint32_t CMPCVH;            /* Compare Count Value High Register       Address offset: 0x24 */
    uint32_t RESERVED1;
    __IO uint32_t CMPCR;            /* Compare Control Register,               Address offset: 0x2C */
    uint32_t RESERVED2[52];
} CHIP_REGS_SYSCTR_CMP_FRAME_T;

typedef struct {
    CHIP_REGS_SYSCTR_CMP_FRAME_T FRAME[CHIP_SYSCTR_CMP_FRAME_CNT];
    uint32_t RESERVED1[884];
    __I  uint32_t CNTID0;           /* Counter ID Register,                     Address offset: 0xFD0 */
} CHIP_REGS_SYSCTR_CMP_T;

#define CHIP_REGS_SYSCTR_CTRL       ((CHIP_REGS_SYSCTR_CTRL_T *) CHIP_MEMRGN_ADDR_PERIPH_SYSCTR_CTRL)
#define CHIP_REGS_SYSCTR_RD         ((CHIP_REGS_SYSCTR_RD_T *)   CHIP_MEMRGN_ADDR_PERIPH_SYSCTR_RD)
#define CHIP_REGS_SYSCTR_CMP        ((CHIP_REGS_SYSCTR_CMP_T *)  CHIP_MEMRGN_ADDR_PERIPH_SYSCTR_CMP)


/*********** Counter Control Register (SYSCTR_CNTCR) **************************/
#define CHIP_REGFLD_SYSCTR_CNTCR_FCR(n)             (0x00000001UL << (8U + (n))) /* (W1SC) Frequency Change Request, ID x (x=0..1) */
#define CHIP_REGFLD_SYSCTR_CNTCR_HDBG               (0x00000001UL <<  1U)  /* (RW)  Enable Debug, 1 - The assertion of the debug input causes the System Counter to halt. */
#define CHIP_REGFLD_SYSCTR_CNTCR_EN                 (0x00000001UL <<  0U)  /* (RW)  Enable Counting */
#define CHIP_REGMSK_SYSCTR_CNTCR_RESERVED           (0xFFFFFCFCUL)

/*********** Counter Status Register (SYSCTR_CNTSR) ***************************/
#define CHIP_REGFLD_SYSCTR_CNTSR_FCA(n)             (0x00000001UL << (8U + (n))) /* (RO) Frequency Change Acknowledge, ID x (x=0..1) */
#define CHIP_REGFLD_SYSCTR_CNTSR_DBGH               (0x00000001UL <<  0U)  /* (RO)  Debug Halt (1 - Counter is halted by debug) */


/*********** Compare Control Register (SYSCTR_CMPCR) *********************/
#define CHIP_REGFLD_SYSCTR_CMPCR_ISTAT              (0x00000001UL <<  2U)  /* (RO)  Compare (interrupt) status (0 - counter value is less than the compare value or compare is disabled) **/
#define CHIP_REGFLD_SYSCTR_CMPCR_IMASK              (0x00000001UL <<  1U)  /* (RW)  Interrupt request mask (1 - masked) */
#define CHIP_REGFLD_SYSCTR_CMPCR_EN                 (0x00000001UL <<  0U)  /* (RW)  Enable the compare function */
#define CHIP_REGMSK_SYSCTR_CMPCR_RESERVED           (0xFFFFFFF8UL)



#endif // REGS_SYSCTR_H
