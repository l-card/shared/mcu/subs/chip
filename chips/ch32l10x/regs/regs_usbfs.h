#ifndef REGS_USB_H
#define REGS_USB_H

#include "chip_ioreg_defs.h"

/* USBFS Registers */
typedef struct {
    __IO uint8_t  CTRL;            /* USB Control Register,                Address offset: 0x00 */
    __IO uint8_t  UDEV_CTRL;
    __IO uint8_t  INT_EN;               /* USB interrupt enable register,       Address offset: 0x02 */
    __IO uint8_t  DEV_ADDR;             /* USB device address register,         Address offset: 0x03 */
    uint8_t  RESERVED0;
    __IO uint8_t  MIS_ST;               /* USB miscellaneous status register,   Address offset: 0x05 */
    __IO uint8_t  INT_FG;               /* USB interrupt flag register,         Address offset: 0x06 */
    __IO uint8_t  INT_ST;               /* USB interrupt status register,       Address offset: 0x07 */
    __IO uint16_t RX_LEN;               /* USB receive length register,         Address offset: 0x07 */
    uint16_t RESERVED1;
    __IO uint8_t  UEP4_1_MOD;
    __IO uint8_t  UEP2_3_MOD;
    __IO uint8_t  UEP5_6_MOD;
    __IO uint8_t  UEP7_MOD;
    __IO uint32_t UEP0_DMA;
    __IO uint32_t UEP1_DMA;
    __IO uint32_t UEP2_DMA;
    __IO uint32_t UEP3_DMA;
    __IO uint32_t UEP4_DMA;
    __IO uint32_t UEP5_DMA;
    __IO uint32_t UEP6_DMA;
    __IO uint32_t UEP7_DMA;
    __IO uint16_t UEP0_TX_LEN;
    union{
        __IO uint16_t  UEP0_CTRL;
        struct{
            __IO uint8_t  UEP0_TX_CTRL;
            __IO uint8_t  UEP0_RX_CTRL;
        };
    };
    __IO uint16_t UEP1_TX_LEN;
    union{
        __IO uint16_t  UEP1_CTRL;
        struct{
            __IO uint8_t  UEP1_TX_CTRL;
            __IO uint8_t  UEP1_RX_CTRL;
        };
    };
    __IO uint16_t UEP2_TX_LEN;
    union{
        __IO uint16_t  UEP2_CTRL;
        struct{
            __IO uint8_t  UEP2_TX_CTRL;
            __IO uint8_t  UEP2_RX_CTRL;
        };
    };
    __IO uint16_t UEP3_TX_LEN;
    union{
        __IO uint16_t  UEP3_CTRL;
        struct{
            __IO uint8_t  UEP3_TX_CTRL;
            __IO uint8_t  UEP3_RX_CTRL;
        };
    };
    __IO uint16_t UEP4_TX_LEN;
    union{
        __IO uint16_t  UEP4_CTRL;
        struct{
            __IO uint8_t  UEP4_TX_CTRL;
            __IO uint8_t  UEP4_RX_CTRL;
        };
    };
    __IO uint16_t UEP5_TX_LEN;
    union{
        __IO uint16_t  UEP5_CTRL;
        struct{
            __IO uint8_t  UEP5_TX_CTRL;
            __IO uint8_t  UEP5_RX_CTRL;
        };
    };
    __IO uint16_t UEP6_TX_LEN;
    union{
        __IO uint16_t  UEP6_CTRL;
        struct{
            __IO uint8_t  UEP6_TX_CTRL;
            __IO uint8_t  UEP6_RX_CTRL;
        };
    };
    __IO uint16_t UEP7_TX_LEN;
    union{
        __IO uint16_t  UEP7_CTRL;
        struct{
            __IO uint8_t  UEP7_TX_CTRL;
            __IO uint8_t  UEP7_RX_CTRL;
        };
    };
    __IO uint32_t Reserve1;
    __IO uint32_t OTG_CR;
    __IO uint32_t OTG_SR;
} CHIP_REGS_USBFSD_T;

typedef struct {
    __IO uint8_t  CTRL;    /* USB Control Register,                        Address offset: 0x00 */
    __IO uint8_t  HOST_CTRL;    /* USB host physical port control register,     Address offset: 0x01 */
    __IO uint8_t  INT_EN;       /* USB interrupt enable register,               Address offset: 0x02 */
    __IO uint8_t  DEV_ADDR;     /* USB device address register,                 Address offset: 0x03 */
    uint8_t  RESERVED0;
    __IO uint8_t  MIS_ST;       /* USB miscellaneous status register,           Address offset: 0x05 */
    __IO uint8_t  INT_FG;       /* USB interrupt flag register,                 Address offset: 0x06 */
    __IO uint8_t  INT_ST;       /* USB interrupt status register,               Address offset: 0x07 */
    __IO uint16_t RX_LEN;       /* USB receive length register,                 Address offset: 0x08 */
    uint16_t RESERVED1;
    uint8_t  RESERVED2;
    __IO uint8_t  HOST_EP_MOD;  /* USB host endpoint mode control register,     Address offset: 0x0D */
    uint16_t RESERVED3;
    uint32_t RESERVED4[2];
    __IO uint32_t HOST_RX_DMA;  /* USB host receive buffer start address,       Address offset: 0x18 */
    __IO uint32_t HOST_TX_DMA;  /* USB host transmit buffer start address,      Address offset: 0x1C */
    uint32_t RESERVED5[5];
    uint16_t RESERVED6;
    __IO uint16_t HOST_SETUP;   /* USB host auxiliary setting register,         Address offset: 0x36 */
    __IO uint8_t  HOST_EP_PID;  /* USB host token setting register,             Address offset: 0x38 */
    uint8_t  RESERVED7;
    uint8_t  RESERVED8;
    __IO uint8_t  HOST_RX_CTRL; /* USB host receive endpoint control register,  Address offset: 0x3B */
    __IO uint16_t HOST_TX_LEN;  /* USB host transmit length register,           Address offset: 0x3C */
    __IO uint8_t  HOST_TX_CTRL; /* USB host transmit endpoint control register, Address offset: 0x3E */
    uint8_t  RESERVED9;
    uint32_t RESERVED10[5];
    __IO uint32_t OTG_CR;
    __IO uint32_t OTG_SR;
} CHIP_REGS_USBFSH_T;


#define CHIP_REGS_USBFSD                                ((CHIP_REGS_USBFSD_T *) CHIP_MEMRGN_ADDR_PERIPH_USBFS)
#define CHIP_REGS_USBFSH                                ((CHIP_REGS_USBFSH_T *) CHIP_MEMRGN_ADDR_PERIPH_USBFS)

/*********** USB Control Register (USB_CTRL) **********************************/
#define CHIP_REGFLD_USB_CTRL_HOST_MODE                  (0x01UL << 7U) /* (RW) USB operating mode selection (1 - Host, 0 - Device) */
#define CHIP_REGFLD_USB_CTRL_LOW_SPEED                  (0x01UL << 6U) /* (RW) USB bus signal transfer rate selection (1 - LS, 0 - FS) */
#define CHIP_REGFLD_USB_CTRL_DEV_PU_EN                  (0x01UL << 5U) /* (RW) Device mode: Enable internal pull-up */
#define CHIP_REGFLD_USB_CTRL_DEV_EN                     (0x01UL << 4U) /* (RW) Device mode: Enable USB device */
#define CHIP_REGFLD_USB_CTRL_HST_SYS_CTRL               (0x03UL << 4U) /* (RW) Host system control */
#define CHIP_REGFLD_USB_CTRL_INT_BUSY                   (0x01UL << 3U) /* (RW) Pause USB communication on transfer completion unitil clear completion flag */
#define CHIP_REGFLD_USB_CTRL_RST_SIE                    (0x01UL << 2U) /* (RW) USB protocol processor reset (Disable USB and use PA11/PA12 as GPIO) */
#define CHIP_REGFLD_USB_CTRL_CLR_ALL                    (0x01UL << 1U) /* (RW) USB FIFO and interrupt flags clear */
#define CHIP_REGFLD_USB_CTRL_DMA_EN                     (0x01UL << 0U) /* (RW) Enable DMA function and DMA interrupt */


#define CHIP_REGFLDVAL_USB_CTRL_HST_SYS_CTRL_NORMAL     0 /* Normal working state*/
#define CHIP_REGFLDVAL_USB_CTRL_HST_SYS_CTRL_SE0        1 /* USB host mode, forcing DP/DM to output SE0 state */
#define CHIP_REGFLDVAL_USB_CTRL_HST_SYS_CTRL_J          2 /* USB host mode, forces DP/DM to output J state */
#define CHIP_REGFLDVAL_USB_CTRL_HST_SYS_CTRL_K_WKUP     3 /* USB host mode, force DP/DM to output K state/wake up */


/*********** USB interrupt enable register (INT_EN) **************************/
#define CHIP_REGFLD_USB_INT_EN_DEV_NAK                  (0x01UL << 6U) /* (RW) USB device mode: receive NAK interrupt */
#define CHIP_REGFLD_USB_INT_EN_1_WIRE                   (0x01UL << 5U) /* (RW) USB 1-wire mode enable */
#define CHIP_REGFLD_USB_INT_EN_FIFO_OV                  (0x01UL << 4U) /* (RW) FIFO overflow interrupt */
#define CHIP_REGFLD_USB_INT_EN_HST_SOF                  (0x01UL << 3U) /* (RW) USB host mode: SOF timing interrupt */
#define CHIP_REGFLD_USB_INT_EN_SUSPEND                  (0x01UL << 2U) /* (RW) USB bus suspend or wakeup event interrupt */
#define CHIP_REGFLD_USB_INT_EN_TRANSFER                 (0x01UL << 1U) /* (RW) USB transfer complete interrupt */
#define CHIP_REGFLD_USB_INT_EN_HST_DETECT               (0x01UL << 0U) /* (RW) USB host mode: USB device connect or disconnect event interrupt */
#define CHIP_REGFLD_USB_INT_EN_DEV_BUS_RST              (0x01UL << 0U) /* (RW) USB device mode: USB bus reset event interrupt */
#define CHIP_REGMSK_USB_INT_EN_RESERVED                 (0x80UL)

/*********** USB device address register (DEV_ADDR) ***************************/
#define CHIP_REGFLD_USB_DEV_ADDR_GP_BIT                 (0x01UL << 7U) /* (RW) USB general-purpose flag, user-defined. */
#define CHIP_REGFLD_USB_DEV_ADDR_USB_ADDR               (0x7FUL << 0U) /* (RW) Host mode: current operated USB device address, Device Mode: USB own address */

/*********** USB miscellaneous status register (MIS_ST) ***************************/
#define CHIP_REGFLD_USB_MIS_ST_SOF_PRES                 (0x01UL << 7U) /* (RO) Host Mode: SOF packet presage status - SOF packet is about to be transmitted */
#define CHIP_REGFLD_USB_MIS_ST_SOF_ACT                  (0x01UL << 6U) /* (RO) Host Mode: SOF packet transfer status - SOF packet is being sent */
#define CHIP_REGFLD_USB_MIS_ST_SIE_FREE                 (0x01UL << 5U) /* (RO) USB protocol handler free (not busy) */
#define CHIP_REGFLD_USB_MIS_ST_R_FIFO_RDY               (0x01UL << 4U) /* (RO) USB receive FIFO data ready (not empty) */
#define CHIP_REGFLD_USB_MIS_ST_BUS_RST                  (0x01UL << 3U) /* (RO) USB bus is in reset state */
#define CHIP_REGFLD_USB_MIS_ST_SUSPEND                  (0x01UL << 2U) /* (RO) USB bus is in a suspended state */
#define CHIP_REGFLD_USB_MIS_ST_DM_LEVEL                 (0x01UL << 1U) /* (RO) USB host mode: level state of the DM pin (for speed detection, 1 - LS, 0 - FS) */
#define CHIP_REGFLD_USB_MIS_ST_DEV_ATTACH               (0x01UL << 0U) /* (RO) USB host mode: USB device attach status for the port */

/*********** USB interrupt flag register (INT_FG) *****************************/
#define CHIP_REGFLD_USB_INT_FG_IS_NAK                   (0x01UL << 7U) /* (RO) Device mode: NAK response status (1 - Respond to NAK during the current USB transfer) */
#define CHIP_REGFLD_USB_INT_FG_TOG_OK                   (0x01UL << 6U) /* (RO) USB transfer DATA0/1 is synchronized */
#define CHIP_REGFLD_USB_INT_FG_SIE_FREE                 (0x01UL << 5U) /* (RO) USB Protocol Handler free (idle) */
#define CHIP_REGFLD_USB_INT_FG_FIFO_OV                  (0x01UL << 4U) /* (RW1C) FIFO overflow flag */
#define CHIP_REGFLD_USB_INT_FG_HST_SOF                  (0x01UL << 3U) /* (RW1C) USB Host: SOF timer interrupt flag */
#define CHIP_REGFLD_USB_INT_FG_SUSPEND                  (0x01UL << 2U) /* (RW1C) USB bus suspend or wake-up event interrupt */
#define CHIP_REGFLD_USB_INT_FG_TRANSFER                 (0x01UL << 1U) /* (RW1C) USB transfer completion interrupt flag */
#define CHIP_REGFLD_USB_INT_FG_HST_DETECT               (0x01UL << 0U) /* (RW1C) USB host mode: USB device connect or disconnect event interrupt flag */
#define CHIP_REGFLD_USB_INT_FG_DEV_BUS_RST              (0x01UL << 0U) /* (RW1C) USB device mode: USB bus reset event interrupt flag */

/*********** USB interrupt status register (INT_ST) ***************************/
#define CHIP_REGFLD_USB_INT_ST_SETUP_ACT                (0x01UL << 7U) /* (RO) Device mode: SETUP request packet has been successfully received */
#define CHIP_REGFLD_USB_INT_ST_TOG_OK                   (0x01UL << 6U) /* (RO) USB transfer DATA0/1 is synchronized  */
#define CHIP_REGFLD_USB_INT_ST_TOKEN                    (0x03UL << 4U) /* (RO) Device mode: token PID identity of the current USB transaction  */
#define CHIP_REGFLD_USB_INT_ST_DEV_ENDP                 (0x0FUL << 0U) /* (RO) Device mode: endpoint number of the current USB transaction  */
#define CHIP_REGFLD_USB_INT_ST_HST_RES                  (0x0FUL << 0U) /* (RO) Host mode: reply PID identification of the current USB transaction (OUT/SETUP -> ACK/NACK/STALL, IN -> DATA0/DATA!*/

#define CHIP_REGFLDVAL_USB_INT_ST_TOKEN_OUT             0
#define CHIP_REGFLDVAL_USB_INT_ST_TOKEN_IN              2
#define CHIP_REGFLDVAL_USB_INT_ST_TOKEN_SETUP           3


/*------------------------ Host Mode -----------------------------------------*/
/*********** USB host physical port control register (HOST_CTRL) **************/
#define CHIP_REGFLD_USB_HOST_CTRL_PD_DIS                (0x01UL << 7U) /* (RW) USB host port UD+/UD- pin internal 15K pull-down resistance disable */
#define CHIP_REGFLD_USB_HOST_CTRL_DP_PIN                (0x01UL << 5U) /* (RO) Current UD+ pin status (1- High, 0 - Low) */
#define CHIP_REGFLD_USB_HOST_CTRL_DM_PIN                (0x01UL << 4U) /* (RO) Current UD- pin status (1- High, 0 - Low) */
#define CHIP_REGFLD_USB_HOST_CTRL_LOW_SPEED             (0x01UL << 2U) /* (RW) USB host port low-speed mode enable */
#define CHIP_REGFLD_USB_HOST_CTRL_BUS_RESET             (0x01UL << 1U) /* (RW) USB host mode bus reset control */
#define CHIP_REGMSK_USB_HOST_CTRL_RESERVED              (0x48UL)

/*********** USB host endpoint mode control register (HOST_EP_MOD) ************/
#define CHIP_REGFLD_USB_HOST_EP_MOD_EP_TX_EN            (0x01UL << 6U) /* (RW) Host send endpoint transmit (SETUP/OUT) enable */
#define CHIP_REGFLD_USB_HOST_EP_MOD_EP_TBUF_MOD         (0x01UL << 4U) /* (RW) Host transmitting endpoint transmits data buffer mode control (0 - Single, 1 - double buf) */
#define CHIP_REGFLD_USB_HOST_EP_MOD_EP_RX_EN            (0x01UL << 3U) /* (RW) Host receive endpoint receive (IN) enable */
#define CHIP_REGFLD_USB_HOST_EP_MOD_EP_RBUF_MOD         (0x01UL << 0U) /* (RW) Host receiving endpoint receives the data buffer mode control (0 - Single, 1 - double buf) */
#define CHIP_REGMSK_USB_HOST_EP_MOD_RESERVED            (0xA6UL)

/*********** USB host auxiliary setting register (HOST_SETUP) *****************/
#define CHIP_REGFLD_USB_HOST_SETUP_PRE_PID_EN           (0x0001UL << 10U) /* (RW) Enable PREPID  level of low-speed pilot package (communicate with low-speed USB devices through external HUB) */
#define CHIP_REGFLD_USB_HOST_SETUP_SOF_EN               (0x0001UL <<  2U) /* (RW) Enable automatically generate SOF packets */
#define CHIP_REGMSK_USB_HOST_SETUP_RESERVED             (0xFBFBUL)

/*********** USB host token setting register (HOST_EP_PID) ********************/
#define CHIP_REGFLD_USB_HOST_EP_PID_TOKEN               (0x0FUL <<  4U) /* (RW) Token PID package identity for this USB transport transaction */
#define CHIP_REGFLD_USB_HOST_EP_PID_ENDP                (0x0FUL <<  0U) /* (RW) Endpoint number of the target device to be operated */

/*********** USB host receive endpoint control register (HOST_RX_CTRL) ********/
#define CHIP_REGFLD_USB_HOST_RX_CTRL_AUTO_TOG           (0x01UL <<  3U) /* (RW) Automatically toggle  synchronisation bit after successful data reception */
#define CHIP_REGFLD_USB_HOST_RX_CTRL_TOG                (0x01UL <<  2U) /* (RW) Expected data synchronisation bit value (0 - DATA0, 1 - DATA1) */
#define CHIP_REGFLD_USB_HOST_RX_CTRL_RES                (0x01UL <<  0U) /* (RW) Receiver response control bit for IN transactions (0 - Reply ACK, 1 - no response) */
#define CHIP_REGMSK_USB_HOST_RX_CTRL_RESERVED           (0xF2UL)

/*********** USB host transmit endpoint control registe (HOST_TX_CTRL) ********/
#define CHIP_REGFLD_USB_HOST_TX_CTRL_AUTO_TOG           (0x01UL <<  3U) /* (RW) Automatically toggle  synchronisation bit after successful data is sent successfully */
#define CHIP_REGFLD_USB_HOST_TX_CTRL_TOG                (0x01UL <<  2U) /* (RW) Data synchronisation bit value in next transmitted SETUP/OUT packet (0 - DATA0, 1 - DATA1) */
#define CHIP_REGFLD_USB_HOST_TX_CTRL_RES                (0x01UL <<  0U) /* (RW) Transmitter response control bit for SETUP/OUT transactions (0 - expect ACK, 1 - no response is expected) */


#endif // REGS_USB_H
