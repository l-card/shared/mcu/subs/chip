#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#define CHIP_DEV_LPC11U6X
#define CHIP_DEV_LPC11U68

#include "chip_dev_spec_features.h"

#define CHIP_DEV_UART_CNT               ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) ? 5 : 3)
#define CHIP_DEV_USB_CNT                1
#define CHIP_DEV_I2C_CNT                2
#define CHIP_DEV_SPI_CNT                2
#define CHIP_DEV_TIM_CNT                6



#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
