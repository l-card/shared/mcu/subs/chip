#include "chip.h"
#if CHIP_CFG_LTIMER_EN
    #include "ltimer.h"
#endif

#define CHIP_FLASH_KEY1     0x45670123
#define CHIP_FLASH_KEY2     0xCDEF89AB
#define CHIP_FLASH_OPT_KEY1 0x08192A3B
#define CHIP_FLASH_OPT_KEY2 0x4C5D6E7F

#define CHIP_FLASH_RDP_L0_CODE 0xAA
#define CHIP_FLASH_RDP_L2_CODE 0xCC

#if CHIP_CFG_PWR_VOS == CHIP_PWR_VOS3
    #if CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(45)
        #define CHIP_RVAL_FLASH_WS    0
        #define CHIP_RVAL_FLASH_DELAY 0
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(90)
        #define CHIP_RVAL_FLASH_WS    1
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(135)
        #define CHIP_RVAL_FLASH_WS    2
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(180)
        #define CHIP_RVAL_FLASH_WS    3
        #define CHIP_RVAL_FLASH_DELAY 2
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(225)
        #define CHIP_RVAL_FLASH_WS    4
        #define CHIP_RVAL_FLASH_DELAY 2
    #else
        #error Flash frequency is out of range
    #endif
#elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS2
    #if CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(55)
        #define CHIP_RVAL_FLASH_WS    0
        #define CHIP_RVAL_FLASH_DELAY 0
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(110)
        #define CHIP_RVAL_FLASH_WS    1
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(165)
        #define CHIP_RVAL_FLASH_WS    2
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ < CHIP_MHZ(225)
        #define CHIP_RVAL_FLASH_WS    3
        #define CHIP_RVAL_FLASH_DELAY 2
        /* вообще условие 225 в RefMan включено в оба интервала...
         * используем более консервативный вариант */
    #elif CHIP_CLK_FLASH_FREQ == CHIP_MHZ(225)
        #define CHIP_RVAL_FLASH_WS    4
        #define CHIP_RVAL_FLASH_DELAY 2
    #else
        #error Flash frequency is out of range
    #endif
#elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS1
    #if CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(70)
        #define CHIP_RVAL_FLASH_WS    0
        #define CHIP_RVAL_FLASH_DELAY 0
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(140)
        #define CHIP_RVAL_FLASH_WS    1
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(185)
        #define CHIP_RVAL_FLASH_WS    2
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(210)
        #define CHIP_RVAL_FLASH_WS    2
        #define CHIP_RVAL_FLASH_DELAY 2
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(225)
        #define CHIP_RVAL_FLASH_WS    3
        #define CHIP_RVAL_FLASH_DELAY 2
    #else
        #error Flash frequency is out of range
    #endif
#elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS0
    #if CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(70)
        #define CHIP_RVAL_FLASH_WS    0
        #define CHIP_RVAL_FLASH_DELAY 0
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(140)
        #define CHIP_RVAL_FLASH_WS    1
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(185)
        #define CHIP_RVAL_FLASH_WS    2
        #define CHIP_RVAL_FLASH_DELAY 1
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(210)
        #define CHIP_RVAL_FLASH_WS    2
        #define CHIP_RVAL_FLASH_DELAY 2
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(225)
        #define CHIP_RVAL_FLASH_WS    3
        #define CHIP_RVAL_FLASH_DELAY 2
    #elif CHIP_CLK_FLASH_FREQ <= CHIP_MHZ(240)
        #define CHIP_RVAL_FLASH_WS    4
        #define CHIP_RVAL_FLASH_DELAY 2
    #else
        #error Flash frequency is out of range
    #endif
#endif


void chip_flash_init_freq(void) {
    CHIP_REGS_FLASH->ACR = (CHIP_REGS_FLASH->ACR & ~(CHIP_REGFLD_FLASH_ACR_LATENCY | CHIP_REGFLD_FLASH_ACR_WRHIGHFREQ))
            | LBITFIELD_SET(CHIP_REGFLD_FLASH_ACR_LATENCY, CHIP_RVAL_FLASH_WS)
            | LBITFIELD_SET(CHIP_REGFLD_FLASH_ACR_WRHIGHFREQ, CHIP_RVAL_FLASH_DELAY)
            ;
}


#if CHIP_FLASH_FUNC_EN
/* по количеству разрешенных параллельных бит для стирания определяется
 * максимальные времена стирания/записи для flash-памяти */
#if CHIP_CFG_FLASH_WR_PARALLEL_BITS == 64
    #define CHIP_FLASH_RVAL_PSIZE           3
    #define CHIP_FLASH_WR_WORD_TIME_MAX     200
    #define CHIP_FLASH_SEC_ERASE_TIME_MAX   3600
    #define CHIP_FLASH_MASS_ERASE_TIME_MAX  10000
#elif CHIP_CFG_FLASH_WR_PARALLEL_BITS == 32
    #define CHIP_FLASH_RVAL_PSIZE           2
    #define CHIP_FLASH_WR_WORD_TIME_MAX     260
    #define CHIP_FLASH_SEC_ERASE_TIME_MAX   3600
    #define CHIP_FLASH_MASS_ERASE_TIME_MAX  12000
#elif CHIP_CFG_FLASH_WR_PARALLEL_BITS == 16
    #define CHIP_FLASH_RVAL_PSIZE           1
    #define CHIP_FLASH_WR_WORD_TIME_MAX     360
    #define CHIP_FLASH_SEC_ERASE_TIME_MAX   3600
    #define CHIP_FLASH_MASS_ERASE_TIME_MAX  16000
#elif CHIP_CFG_FLASH_WR_PARALLEL_BITS == 8
    #define CHIP_FLASH_RVAL_PSIZE           0
    #define CHIP_FLASH_WR_WORD_TIME_MAX     580
    #define CHIP_FLASH_SEC_ERASE_TIME_MAX   4000
    #define CHIP_FLASH_MASS_ERASE_TIME_MAX  26000
#else
    #error Invalid flash parallelism configuration
#endif


#define CHIP_FLASH_WR_WORD_TOUT    CHIP_FLASH_WR_WORD_TIME_MAX
#define CHIP_FLASH_SEC_ERASE_TOUT  CHIP_FLASH_SEC_ERASE_TIME_MAX
#define CHIP_FLASH_MASS_ERASE_TOUT CHIP_FLASH_MASS_ERASE_TIME_MAX
/* вообще не очень понятно, где именно описано время изменения опций.
 * берем равным стиранию сектора (в реальном измерении было в 3 раза меньше) */
#define CHIP_FLASH_WR_OPT_TOUT     CHIP_FLASH_SEC_ERASE_TIME_MAX


static const intptr_t flash_bank_memrgn_addrs[CHIP_FLASH_BANK_CNT] = {
    CHIP_MEMRGN_ADDR_FLASH_BANK1,
#if CHIP_DEV_SUPPORT_FLASH_BANK2
    CHIP_MEMRGN_ADDR_FLASH_BANK2
#endif
};


static void *flash_addr(t_chip_flash_bank bank, uint32_t addr) {
    return (void*)(flash_bank_memrgn_addrs[bank] + addr);
}


#define CHIP_FLASH_ERROR_MASK (CHIP_REGFLD_FLASH_SR_WRPERR \
                                 | CHIP_REGFLD_FLASH_SR_PGSERR \
                                 | CHIP_REGFLD_FLASH_SR_STRBERR \
                                 | CHIP_REGFLD_FLASH_SR_INCERR \
                                 | CHIP_REGFLD_FLASH_SR_OPERR \
                                 | CHIP_REGFLD_FLASH_SR_RDPERR \
                                 | CHIP_REGFLD_FLASH_SR_RDSERR \
                                 | CHIP_REGFLD_FLASH_SR_SNECCERR \
                                 | CHIP_REGFLD_FLASH_SR_DBECCERR)


static void flash_clear_errs(t_chip_flash_bank bank) {
    CHIP_REGS_FLASH->BANK[bank].CCR = CHIP_FLASH_ERROR_MASK;
}

t_chip_flash_err chip_flash_check_err(t_chip_flash_bank bank) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    const uint32_t status = CHIP_REGS_FLASH->BANK[bank].SR;
    if (status & CHIP_FLASH_ERROR_MASK) {
        if (status & CHIP_REGFLD_FLASH_SR_WRPERR) {
            err = CHIP_FLASH_ERR_STAT_WRITE_PROTECT;
        } else if (status & CHIP_REGFLD_FLASH_SR_PGSERR) {
            err = CHIP_FLASH_ERR_STAT_PROG_SEQ;
        } else if (status & CHIP_REGFLD_FLASH_SR_STRBERR) {
            err = CHIP_FLASH_ERR_STAT_STROBE;
        } else if (status & CHIP_REGFLD_FLASH_SR_INCERR) {
            err = CHIP_FLASH_ERR_STAT_INCONSIST;
        } else if (status & CHIP_REGFLD_FLASH_SR_OPERR) {
            err = CHIP_FLASH_ERR_STAT_OPERATION;
        } else if (status & CHIP_REGFLD_FLASH_SR_RDPERR) {
            err = CHIP_FLASH_ERR_STAT_RDP;
        } else if (status & CHIP_REGFLD_FLASH_SR_RDSERR) {
            err = CHIP_FLASH_ERR_STAT_READ_SECURE;
        } else if (status & CHIP_REGFLD_FLASH_SR_DBECCERR) {
            err = CHIP_FLASH_ERR_STAT_ECC_DOUBLE;
        } else if (CHIP_REGS_FLASH->BANK[bank].SR & CHIP_REGFLD_FLASH_SR_SNECCERR) {
            err = CHIP_FLASH_ERR_STAT_ECC_SINGLE;
        }
        flash_clear_errs(bank);
    }
    return  err;
}

t_chip_flash_err chip_flash_get_sector_nums(uint32_t start_addr, uint32_t size,
                                            t_chip_flash_sec_num *pstart_sec, t_chip_flash_sec_num *pend_sec) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    if ((start_addr + size) > CHIP_FLASH_MEM_SIZE) {
        err = CHIP_FLASH_ERR_INVALID_ADDR;
    } else {
        unsigned start_sec = start_addr/CHIP_FLASH_SECTOR_SIZE;
        unsigned end_sec = (start_addr + size - 1)/CHIP_FLASH_SECTOR_SIZE;
        if (pstart_sec)
            *pstart_sec = start_sec & 0xFF;
        if (pend_sec)
            *pend_sec = end_sec & 0xFF;
    }
    return err;
}


static t_chip_flash_err flash_check_unlocked(t_chip_flash_bank bank) {
    return CHIP_REGS_FLASH->BANK[bank].CR & CHIP_REGFLD_FLASH_CR_LOCK ? CHIP_FLASH_ERR_LOCKED : CHIP_FLASH_ERR_OK;
}

t_chip_flash_err chip_flash_check_rdy(t_chip_flash_bank bank) {
    return CHIP_REGS_FLASH->BANK[bank].SR & CHIP_REGFLD_FLASH_SR_QW ? CHIP_FLASH_ERR_NOT_RDY : CHIP_FLASH_ERR_OK;
}


t_chip_flash_err chip_flash_wait_rdy(t_chip_flash_bank bank, unsigned tout_ms, void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
#if CHIP_CFG_LTIMER_EN
    t_ltimer tmr;
    ltimer_set_ms(&tmr, tout_ms);
#endif

    while ((chip_flash_check_rdy(bank) == CHIP_FLASH_ERR_NOT_RDY)
#if CHIP_CFG_LTIMER_EN
           && !ltimer_expired(&tmr)
#endif
           ) {
        if (cb_func)
            cb_func();
        continue;
    }

    if ((CHIP_REGS_FLASH->BANK[bank].SR & CHIP_REGFLD_FLASH_SR_QW) != 0)
        err = CHIP_FLASH_ERR_WAIT_OPRDY_TOUT;
    return  err;
}

static t_chip_flash_err flash_wait_options_rdy(unsigned tout_ms, void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
#if CHIP_CFG_LTIMER_EN
    t_ltimer tmr;
    ltimer_set_ms(&tmr, tout_ms);
#endif

    while (((CHIP_REGS_FLASH->OPTCR & CHIP_REGFLD_FLASH_OPTSR_OPT_BUSY) != 0)
#if CHIP_CFG_LTIMER_EN
           && !ltimer_expired(&tmr)
#endif
           ) {
        if (cb_func)
            cb_func();
        continue;
    }

    if ((CHIP_REGS_FLASH->OPTCR & CHIP_REGFLD_FLASH_OPTSR_OPT_BUSY) != 0)
        err = CHIP_FLASH_ERR_WAIT_OPRDY_TOUT;
    return  err;
}

void chip_flash_opts_unlock(void) {
    if (CHIP_REGS_FLASH->OPTCR & CHIP_REGFLD_FLASH_OPTCR_OPTLOCK) {
        CHIP_REGS_FLASH->OPTKEYR = CHIP_FLASH_OPT_KEY1;
        CHIP_REGS_FLASH->OPTKEYR = CHIP_FLASH_OPT_KEY2;
        chip_dmb();
    }
}

void chip_flash_opts_lock(void) {
    CHIP_REGS_FLASH->OPTCR |= CHIP_REGFLD_FLASH_OPTCR_OPTLOCK;
}

t_chip_flash_err chip_flash_opts_write(void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    if (CHIP_REGS_FLASH->OPTCR & CHIP_REGFLD_FLASH_OPTCR_OPTLOCK) {
        err = CHIP_FLASH_ERR_LOCKED;
    } else {
        /* т.к. по OPTSTART программа при выполнении из flash
         * может остановиться на время выполнения записи опций, то
         * явно вызываем cb перед и после OPTSTART для возможности
         * явно сбросить wdt */
        if (cb_func)
            cb_func();
        CHIP_REGS_FLASH->OPTCR |= CHIP_REGFLD_FLASH_OPTCR_OPTSTART;
        chip_dmb();
        if (cb_func)
            cb_func();
        err = flash_wait_options_rdy(CHIP_FLASH_WR_OPT_TOUT, cb_func);
    }

    if (err == CHIP_FLASH_ERR_OK) {
        if (CHIP_REGS_FLASH->OPTSR_CUR & CHIP_REGFLD_FLASH_OPTSR_OPTCHANGEERR) {
            err = CHIP_FLASH_ERR_OPTIONS_CHANGE;
        }
    }

    return err;
}

void chip_flash_init(void) {
    LBITFIELD_UPD(CHIP_REGS_FLASH->CR1, CHIP_REGFLD_FLASH_CR_PSIZE, CHIP_FLASH_RVAL_PSIZE);
#if CHIP_DEV_SUPPORT_FLASH_BANK2
    LBITFIELD_UPD(CHIP_REGS_FLASH->CR2, CHIP_REGFLD_FLASH_CR_PSIZE, CHIP_FLASH_RVAL_PSIZE);
#endif
}





void chip_flash_unlock(t_chip_flash_bank bank) {
    if (CHIP_REGS_FLASH->BANK[bank].CR & CHIP_REGFLD_FLASH_CR_LOCK) {
        CHIP_REGS_FLASH->BANK[bank].KEYR = CHIP_FLASH_KEY1;
        CHIP_REGS_FLASH->BANK[bank].KEYR = CHIP_FLASH_KEY2;
    }
}

void chip_flash_lock(t_chip_flash_bank bank) {
    CHIP_REGS_FLASH->BANK[bank].CR |= CHIP_REGFLD_FLASH_CR_LOCK;
}


t_chip_flash_err chip_flash_write(t_chip_flash_bank bank, uint32_t start_addr,
                                  const void *data, uint32_t size,
                                  void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    if ((start_addr & (CHIP_FLASH_WRWORD_SIZE - 1)) != 0) {
        err = CHIP_FLASH_ERR_INVALID_ADDR;
    }

    if (err == CHIP_FLASH_ERR_OK) {
        err = flash_check_unlocked(bank);
    }

    if (err == CHIP_FLASH_ERR_OK) {
        uint32_t *dst_wrds = (uint32_t *)flash_addr(bank, start_addr);
        const uint32_t *src_wrds = (const uint32_t *)data;
        unsigned rem_wrds_cnt = (size + 3)/4;
        unsigned remainder = size % 4;

        flash_clear_errs(bank);
        CHIP_REGS_FLASH->BANK[bank].CR |= CHIP_REGFLD_FLASH_CR_PG;

        while ((err == CHIP_FLASH_ERR_OK) && (rem_wrds_cnt > 0)) {
            unsigned cur_word_cnt = 0;            
            chip_dmb();
            CHIP_REGS_FLASH->CCR1 = CHIP_REGFLD_FLASH_CCR_CLR_EOP;

            if (cb_func)
                cb_func();

            for (; (cur_word_cnt < CHIP_FLASH_WRWORD_SIZE/sizeof(uint32_t)) && (rem_wrds_cnt > 0); cur_word_cnt++) {
                if ((remainder != 0) && (rem_wrds_cnt == 1)) {
                    uint8_t *dst_bytes = (uint8_t *)dst_wrds;
                    const uint8_t *src_bytes = (const uint8_t *)src_wrds;
                    for (unsigned r = 0; r < remainder; r++)
                        *dst_bytes++ = *src_bytes++;
                } else {
                    *dst_wrds++ = *src_wrds++;
                }
                rem_wrds_cnt--;                
            }

            if (cb_func)
                cb_func();

            chip_dmb();
            if (cur_word_cnt != CHIP_FLASH_WRWORD_SIZE/sizeof(uint32_t)) {
                CHIP_REGS_FLASH->BANK[bank].CR |= CHIP_REGFLD_FLASH_CR_FW;
            }

            chip_dmb();
            err = chip_flash_wait_rdy(bank, CHIP_FLASH_WR_WORD_TOUT, cb_func);
            if (err == CHIP_FLASH_ERR_OK) {
                err = chip_flash_check_err(bank);
            }
        }

        CHIP_REGS_FLASH->BANK[bank].CR &= ~CHIP_REGFLD_FLASH_CR_PG;
    }

    return  err;
}


t_chip_flash_err chip_flash_erase_sector_start(t_chip_flash_bank bank, uint8_t sector) {
    t_chip_flash_err err = flash_check_unlocked(bank);
    if (err == CHIP_FLASH_ERR_OK)
        err = chip_flash_check_rdy(bank);
    if (err == CHIP_FLASH_ERR_OK) {
        CHIP_REGS_FLASH->BANK[bank].CR = (CHIP_REGS_FLASH->BANK[bank].CR &
                                   ~(CHIP_REGFLD_FLASH_CR_SNB
                                     | CHIP_REGFLD_FLASH_CR_SER
                                     | CHIP_REGFLD_FLASH_CR_BER
                                     ))
                                   | CHIP_REGFLD_FLASH_CR_SER
                                   | LBITFIELD_SET(CHIP_REGFLD_FLASH_CR_SNB, sector)
                                   ;
        CHIP_REGS_FLASH->BANK[bank].CR |= CHIP_REGFLD_FLASH_CR_START;
        chip_dmb();
    }
    return err;
}

void chip_flash_erase_finish(t_chip_flash_bank bank) {

    /* сброс изменных разрешений на стирание */
    CHIP_REGS_FLASH->BANK[bank].CR  &= ~(CHIP_REGFLD_FLASH_CR_SNB
                                  | CHIP_REGFLD_FLASH_CR_SER
                                  | CHIP_REGFLD_FLASH_CR_BER);
}

t_chip_flash_err chip_flash_erase(t_chip_flash_bank bank,
                                  uint32_t start_addr, uint32_t size,
                                  void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    uint8_t start_sec, end_sec;

    err = chip_flash_get_sector_nums(start_addr, size, &start_sec, &end_sec);

    if (err == CHIP_FLASH_ERR_OK) {
        err = flash_check_unlocked(bank);
    }

    if (err == CHIP_FLASH_ERR_OK) {
        flash_clear_errs(bank);

        for (unsigned sec = start_sec; (sec <= end_sec) && (err == CHIP_FLASH_ERR_OK); sec++) {
            /* т.к. во время начала стирания, если стираемый сектор находится
             * в том же банке, из которого выполняется программа, произойдет
             * останов программы, то явно вызываем cb перед и после стиранием
             * для возможности сброса wdt() */
            if (cb_func)
                cb_func();
            err = chip_flash_erase_sector_start(bank, sec);
            if (cb_func)
                cb_func();
            if (err == CHIP_FLASH_ERR_OK)
                err = chip_flash_wait_rdy(bank, CHIP_FLASH_SEC_ERASE_TOUT, cb_func);
            if (err == CHIP_FLASH_ERR_OK) {
                err = chip_flash_check_err(bank);
            }
        }
        chip_flash_erase_finish(bank);
    }

    return err;
}

const void *chip_flash_rd_ptr(t_chip_flash_bank bank, uint32_t addr) {
    return (void*)(flash_bank_memrgn_addrs[bank] + addr);
}




static t_chip_flash_err flash_sectors_wr_protect_change(t_chip_flash_bank bank, uint32_t sector_mask, uint8_t en, void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    uint32_t cur_wp = LBITFIELD_GET(CHIP_REGS_FLASH->BANK[bank].WPSN_CUR, CHIP_REGFLD_FLASH_WPSN_WRPSN);

    if ((cur_wp & sector_mask) != (en ? 0 : sector_mask)) {
        uint32_t set_wp = en ? (cur_wp & ~sector_mask) : (cur_wp | sector_mask);
        chip_flash_opts_unlock();
        LBITFIELD_UPD(CHIP_REGS_FLASH->BANK[bank].WPSN_PRG, CHIP_REGFLD_FLASH_WPSN_WRPSN, set_wp);
        chip_dmb();
        err = chip_flash_opts_write(cb_func);
        chip_flash_opts_lock();
    }
    return err;

}

static t_chip_flash_err flash_wr_protect_change(t_chip_flash_bank bank, uint32_t start_addr, uint32_t size, uint8_t en, void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    t_chip_flash_sec_num start_sec, end_sec;
    err = chip_flash_get_sector_nums(start_addr, size, &start_sec, &end_sec);
    if (err == CHIP_FLASH_ERR_OK) {
        uint32_t sec_msk = 0;
        for (uint8_t sec = start_sec; sec <= end_sec; sec++) {
            sec_msk |= 1 << sec;
        }
        err = flash_sectors_wr_protect_change(bank, sec_msk, en, cb_func);
    }
    return  err;
}


t_chip_flash_err chip_flash_wr_protect(t_chip_flash_bank bank, uint32_t start_addr, uint32_t size, void (*cb_func)(void)) {
    return flash_wr_protect_change(bank, start_addr, size, 1, cb_func);
}

t_chip_flash_err chip_flash_wr_unprotect(t_chip_flash_bank bank, uint32_t start_addr, uint32_t size, void (*cb_func)(void)) {
    return flash_wr_protect_change(bank, start_addr, size, 0, cb_func);
}

t_chip_flash_err chip_flash_sectors_wr_protect(t_chip_flash_bank bank, uint32_t sector_mask, void (*cb_func)(void)) {
    return flash_sectors_wr_protect_change(bank, sector_mask, 1, cb_func);
}

t_chip_flash_err chip_flash_sectors_wr_unprotect(t_chip_flash_bank bank, uint32_t sector_mask, void (*cb_func)(void)) {
    return flash_sectors_wr_protect_change(bank, sector_mask, 0, cb_func);
}


uint32_t chip_flash_get_sectors_wr_protection(t_chip_flash_bank bank) {
    return LBITFIELD_GET(CHIP_REGS_FLASH->BANK[bank].WPSN_CUR, CHIP_REGFLD_FLASH_WPSN_WRPSN) ^ 0xFF;
}

t_chip_flash_rdp_level chip_flash_opts_get_rdp_level(void) {
    uint8_t rdp_code = LBITFIELD_GET(CHIP_REGS_FLASH->OPTSR_CUR, CHIP_REGFLD_FLASH_OPTSR_RDP);
    return rdp_code == CHIP_FLASH_RDP_L0_CODE ? CHIP_FLASH_RDP_LEVEL0 :
           rdp_code == CHIP_FLASH_RDP_L2_CODE ? CHIP_FLASH_RDP_LEVEL2 : CHIP_FLASH_RDP_LEVEL1;
}


t_chip_flash_err chip_flash_opts_set_rdp_level(t_chip_flash_rdp_level lvl, void (*cb_func)(void)) {
    t_chip_flash_err err = CHIP_FLASH_ERR_OK;
    if (lvl != chip_flash_opts_get_rdp_level()) {
        uint8_t rdp_code = lvl == CHIP_FLASH_RDP_LEVEL0 ? CHIP_FLASH_RDP_L0_CODE :
                           lvl == CHIP_FLASH_RDP_LEVEL2 ? CHIP_FLASH_RDP_L2_CODE : 0;
        chip_flash_opts_unlock();
        LBITFIELD_UPD(CHIP_REGS_FLASH->OPTSR_PRG, CHIP_REGFLD_FLASH_OPTSR_RDP, rdp_code);
        chip_dmb();
        err = chip_flash_opts_write(cb_func);
        chip_flash_opts_lock();
    }
    return err;
}

#endif


