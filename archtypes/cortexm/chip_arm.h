#ifndef CHIP_ARM_H
#define CHIP_ARM_H


#include "chip_coretype_spec_features.h"
#include "chip_cmsis.h"

/*================================================================================================*
 * ---- Атомарный доступ к битам памяти и регистров (bit-banding) ----
 * BIT_BAND(var, bitno) = 0 или 1
 *================================================================================================*/
#define CHIP_BIT_BAND(var, bitno) \
    (*((volatile uint32_t*)(((uint32_t)(&(var)) & 0xFE000000) + 0x02000000 + \
    (((((uint32_t)(&(var)) & 0xFFFFF) << 3) + (bitno)) << 2))))


/*================================================================================================*
 * ---- Intrinsic функции управления процессором ----
 * (В CMSIS описаны неудачно, без memory clobber. В линуксе - clobber есть.)
 *================================================================================================*/
#define chip_sev() __asm__ volatile ("sev" ::: "memory")   /* send event (multiproc) */
#define chip_wfe() __asm__ volatile ("wfe" ::: "memory")   /* wait for event (multiproc) */
#define chip_wfi() __asm__ volatile ("wfi" ::: "memory")   /* wait for interrupt */
#define chip_isb() __asm__ volatile ("isb" ::: "memory")   /* instruction sync barrier */
#define chip_dsb() __asm__ volatile ("dsb" ::: "memory")   /* data sync barrier, e.g. after disable IRQ */
#define chip_dmb() __asm__ volatile ("dmb" ::: "memory")   /* data memory barrier, e.g. before DMA */


#define chip_interrupt_enable(irq_num)   NVIC_EnableIRQ(irq_num)
#define chip_interrupt_disable(irq_num)  NVIC_DisableIRQ(irq_num)

#define chip_interrupts_enable()    __asm__ volatile ("cpsie i" ::: "memory", "cc")
#define chip_interrupts_disable()   __asm__ volatile ("cpsid i" ::: "memory", "cc")
#define chip_fault_enable()         __asm__ volatile ("cpsie f" ::: "memory", "cc")
#define chip_fault_disable()        __asm__ volatile ("cpsid f" ::: "memory", "cc")
#define chip_interrupts_save(var) \
    __asm__ volatile ("mrs %[reg], primask" : [reg] "=r" (var) :: "memory", "cc")
#define chip_interrupts_restore(var) \
    __asm__ volatile ("msr primask, %[reg]" : : [reg] "r" (var): "memory", "cc")



#ifdef CHIP_ARCH_ARMV7EM
    #include "arch/armv7em/chip_armv7em.h"
#elif defined CHIP_ARCH_ARMV7M
    #include "arch/armv7m/chip_armv7m.h"
#endif


#endif // CHIP_ARM_H
