#ifndef CHIP_ISR_H
#define CHIP_ISR_H

#include <stdint.h>

/* общее кол-во источников прерываний */
#define CHIP_INTERRUPT_CNT  140

typedef void (*t_chip_isr_func)(uint8_t int_id);

/* инициализация контроллера, должен вызываться перед остальными функциями */
void chip_interrupt_init(void);
/* сброс контроллера прерываний */
void chip_interrupt_reset(void);

/* разрешение блока обработки прерываний для заданного ядра */
void chip_interrupt_core_enable(int core);
void chip_interrupt_core_disable(int core);


/* установка обработчика прерываний для заданного interrupt-id и
   приоритета прерывания. при этом все остальные настройки сбарсываются,
   а прерывание назначается текущему ядру, на котором выполняется вызов данной функции,
   т.е. chip_interrupt_set_group(), chip_interrupt_set_edge() и chip_interrupt_set_core()
   должны вызываться после данной функции при необходимости установки значений
   не равных занчению по-умолчанию. */
void chip_interrupt_cfg(uint8_t int_id, t_chip_isr_func isr, int prio);
/* назначение прерывания указанному ядру процессора. chip_interrupt_cfg() назначает
   прерывание ядру, которое и вызвало данную функции. если необходимо изменить
   назначение ядра, то нужно вызвать данную функцию после chip_interrupt_cfg() */
void chip_interrupt_set_core(uint8_t int_id, int core);
/* установка поля группы для заданного прерывания */
void chip_interrupt_set_group(uint8_t int_id, int group);
/* установка срабатывания прерывания по фронту (edge = 1) или уровню (edge = 0 - по-умолчанию) */
void chip_interrupt_set_edge(uint8_t int_id, int edge);

/* разрешение выбранного прерывания */
void chip_interrupt_enable(uint8_t int_id);
/* запрет выбранного прерывания */
void chip_interrupt_disable(uint8_t int_id);
/* программное взведение прерывания с указанным id */
void chip_interrupt_raise(uint8_t int_id);


#endif // CHIP_ISR_H

