#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H

#define CHIP_CFG_PACKAGE                CHIP_PACKAGE_SO8N         /* выбор используемого корпуса */
#define CHIP_CFG_MEM_SUFFIX             6                        /* последняя цифра в полном названии чипа, определяющая размер flash-памяти */

#define CHIP_CFG_STACK_WORDS_LEN            128
#define CHIP_CFG_STACK_TRACK_UNUSED_WORDS   0

#define CHIP_CFG_PIN_REMAP_PA11_PA9     1                         /* назначение PA9  вместо PA11  */
#define CHIP_CFG_PIN_REMAP_PA12_PA10    0                         /* назначение PA10 вместо PA12  */


#define CHIP_CFG_PWR_VOS                CHIP_PWR_VOS1             /* управление напряжением ядра: VOS1 (до 64МГц) или VOS2 (до 16 MHz) */


/* ------------------ Внешний высокочастотный источник частоты HSE -----------*/
#define CHIP_CFG_CLK_HSE_MODE           CHIP_CLK_EXTMODE_DIS    /* режим внешнего источника HSE - внешний клок */
#define CHIP_CFG_CLK_HSE_FREQ           CHIP_MHZ(32)             /* значение внешней частоты HSE */
#define CHIP_CFG_CLK_HSE_CSS_EN         1                         /* разрешение контроля внешней частоты HSE */


/* ------------------ Внешний низкочастотный источник частоты LSE ------------*/
#define CHIP_CFG_CLK_LSE_SETUP_EN       0                          /* Разрешение изменения конфигурации LSE. */
#define CHIP_CFG_CLK_LSE_MODE           CHIP_CLK_EXTMODE_OSC       /* Режим внешнего источника LSE - осциллятор.
                                                                      Для осциллятора можно не указывать частоты, т.к. фиксированная */
#define CHIP_CFG_CLK_LSE_DRV            CHIP_CLK_LSE_DRV_LOWEST    /* Сила драйвера LSE осциллятора (LOWEST, MEDIUM_LOW, MEDIUM_HIGH, HIGHEST) */
#define CHIP_CFG_CLK_LSE_CSS_EN         1                          /* Разрешение контроля внешней частоты LSE */

#define CHIP_CFG_CLK_LSE_RDY_TOUT_MS    100                       /* время ожидания готовности LSE. Если < 0 - бесконечное */




/* ------------------ Внутренний высокочастотный источник частоты HSI16 --------*/
#define CHIP_CFG_CLK_HSI16_EN           1    /* разрешение источника частоты HSI16 */
#define CHIP_CFG_CLK_HSI16_ON_STOP_EN   0    /* разрешение источника частоты HSI16 в режиме STOP */

#define CHIP_CFG_CLK_HSISYS_DIV         1    /* делитель частоты HSI16 для получения HSISYS (1,2,4,8,16,32,64,128) */




/* ---------------------- Настройки PLL ---------------------------------------*/
/*******************************************************************************
 * В контроллере 1 PLL с 3-мя выходами (P,R,Q).
 * Для PLL должно быть настроено:
 *   - Разрешение PLL и всех ее настроек (EN)
 *   - Делитель входной частоты DIVM от 1 до 8 (результирующая частота fin должна быть от 2.66-16 МГц)
 *   - Множитель DIVN (8..86) fvco = fin * divn (дробной части не предусмотрено)
 *     fvco должна быть в диапазоне: 96-344 МГц (128 для VOS2)
 *   - Для каждого выхода настраивается его разрешение (DIVP_EN, DIVR_EN, DIVQ_EN)
 *      и собственно значение делителя (DIVP, DIVR, DIVQ):
 *      DIVP - 2..32, частота 30,9..122 МГц (40 МГц для VOS2)
 *      DIVQ - 2..8,  частота 12..128 МГц (32 МГц для VOS2)
 *      DIVR - 2..8,  частота 12..64 МГц  (16 МГц для VOS2)
 ******************************************************************************/
#define CHIP_CFG_CLK_PLL_EN         1
#define CHIP_CFG_CLK_PLL_SRC        CHIP_CLK_HSI16  /* Источник частоты PLL (HSI16 или HSE). Если все PLL запрещен, то не используется */
#define CHIP_CFG_CLK_PLL_DIVM       2   /* Делитель входной частоты PLL (1..8). Результирующая частота должна быть 2.66-16 МГц. */
#define CHIP_CFG_CLK_PLL_DIVN       16   /* Множитель, для получения частоты fvco = fin * divn.  (8 - 86)  */
#define CHIP_CFG_CLK_PLL_DIVP_EN    0   /* разреение выхода P */
#define CHIP_CFG_CLK_PLL_DIVQ_EN    0
#define CHIP_CFG_CLK_PLL_DIVR_EN    1
#define CHIP_CFG_CLK_PLL_DIVR       2


/* --------------------- Системный клок --------------------------------------*/
#define CHIP_CFG_CLK_SYS_SRC        CHIP_CLK_PLLR /* источник частоты для системного клока: LSE, LSI, HSE, PLLR, HSISYS  */


#define CHIP_CFG_CLK_AHB_DIV        1   /* делитель для получения частоты AHB (HCLK) из SYSCLK (1,2,4,8,16,64,128,256,512) */
#define CHIP_CFG_CLK_APB_DIV        1   /* делитель для получения частоты APB (PCLK) из HCLK (1,2,4,8,16) */


/* ------------- настройки вывода клока на выходы MCO и MCO2 -----------------*/
#define CHIP_CFG_CLK_MCO_EN         0  /* разрешение настройки выхода MCO */
#define CHIP_CFG_CLK_MCO_DIV        128  /* делитель частоты MCO (1,2,4,8,16,32,64,128, а также 256,512,1024 для STM32G0B1 и STM32G0C1) */
#define CHIP_CFG_CLK_MCO_SRC        CHIP_CLK_SYS  /* источник частоты MCO (SYS, HSI48, HSE, PLLR, LSI, LSE, PLLP, а также PLLQ, RTC, RTC_WAKEUP для STM32G0B1 и STM32G0C1) */
#define CHIP_CFG_CLK_MCO_PIN        CHIP_PIN_PA9_MCO  /* используемый пин для вывода частоты MCO */

/* ---------- настройки MCO2 доступны только для STM32G0B1 и STM32G0C1 -------*/
#define CHIP_CFG_CLK_MCO2_EN        0
/*#define CHIP_CFG_CLK_MCO_DIV        128 */
/*#define CHIP_CFG_CLK_MCO_SRC        CHIP_CLK_SYS */
/*#define CHIP_CFG_CLK_MCO_PIN        CHIP_PIN_PA9_MCO */


/* ------------------------- RTC ---------------------------------------------*/
#define CHIP_CFG_CLK_RTC_SETUP_EN    0 /* разрешение изменения конфигурации клока RTC. */
#define CHIP_CFG_CLK_RTC_EN          0 /* разрешение клока RTC */
#define CHIP_CFG_CLK_RTC_SRC         CHIP_CLK_LSE /* источник частоты для RTC (LSE, LSI, HSE_DIV32) */

/* ------------------------- Вход LSCO --------------------------------------*/
#define CHIP_CFG_CLK_LSCO_SETUP_EN    0 /* разрешение изменения конфигурации выхода LSCO */
#define CHIP_CFG_CLK_LSCO_EN          0 /* разрешение выхода LSCO */
#define CHIP_CFG_CLK_LSCO_SRC         CHIP_CLK_LSE /* источник частоты для выхода LSCO (LSI, LSE) */




/* ----------------------------- ltimer ----------------------------------------*/
/* Разрешение инициализации ltimer и включение возможности использования его для
 * вычисления таймаутов */
#define CHIP_CFG_LTIMER_EN          1

/* *****************************************************************************
   Разрашение клоков для периферийных блоков.
   Настройка определяет, нужно ли включать клок для этого блока при начальной
   инициализации. */
#define CHIP_CFG_CLK_FLASH_EN       1
#define CHIP_CFG_CLK_PWR_EN         1
#define CHIP_CFG_CLK_SYSCFG_EN      1 /* SYSCFG, COMP & VREFBUF clock */
#define CHIP_CFG_CLK_DBG_EN         1

#define CHIP_CFG_CLK_GPIOA_EN       1
#define CHIP_CFG_CLK_GPIOB_EN       1
#define CHIP_CFG_CLK_GPIOC_EN       1
#define CHIP_CFG_CLK_GPIOD_EN       1
/*#define CHIP_CFG_CLK_GPIOE_EN       0 */
#define CHIP_CFG_CLK_GPIOF_EN       1

#define CHIP_CFG_CLK_DMA1_EN        0
/*#define CHIP_CFG_CLK_DMA2_EN        0 */

#define CHIP_CFG_CLK_TIM1_EN        0
#define CHIP_CFG_CLK_TIM2_EN        0
#define CHIP_CFG_CLK_TIM3_EN        0
/* #define CHIP_CFG_CLK_TIM4_EN        0 */
/* #define CHIP_CFG_CLK_TIM6_EN        0 */
/* #define CHIP_CFG_CLK_TIM7_EN        0 */
#define CHIP_CFG_CLK_TIM14_EN       0
/* #define CHIP_CFG_CLK_TIM15_EN       0 */
#define CHIP_CFG_CLK_TIM16_EN       0
#define CHIP_CFG_CLK_TIM17_EN       0
#define CHIP_CFG_CLK_LPTIM1_EN      0
#define CHIP_CFG_CLK_LPTIM2_EN      0

#define CHIP_CFG_CLK_USART1_EN      0
#define CHIP_CFG_CLK_USART2_EN      0
/* #define CHIP_CFG_CLK_USART3_EN      0 */
/* #define CHIP_CFG_CLK_USART4_EN      0 */
/* #define CHIP_CFG_CLK_USART5_EN      0 */
/* #define CHIP_CFG_CLK_USART6_EN      0 */
#define CHIP_CFG_CLK_LPUART1_EN     0
/* #define CHIP_CFG_CLK_LPUART2_EN     0 */

#define CHIP_CFG_CLK_SPI1_EN        0
#define CHIP_CFG_CLK_SPI2_EN        0
/* #define CHIP_CFG_CLK_SPI3_EN        0 */

#define CHIP_CFG_CLK_I2C1_EN        0
#define CHIP_CFG_CLK_I2C2_EN        0
/* #define CHIP_CFG_CLK_I2C3_EN        0 */

#define CHIP_CFG_CLK_ADC_EN        0
/* #define CHIP_CFG_CLK_DAC1_EN        0 */

#define CHIP_CFG_CLK_CRC_EN         0
/* #define CHIP_CFG_CLK_AES_EN         0 */
/* #define CHIP_CFG_CLK_RNG_EN         0 */

#define CHIP_CFG_CLK_RTCAPB_EN      0
#define CHIP_CFG_CLK_WWDG_EN        0
/* #define CHIP_CFG_CLK_FDCAN_EN       0 */
/* #define CHIP_CFG_CLK_USB_EN         0 */
/* #define CHIP_CFG_CLK_CRS_EN         0 */
/* #define CHIP_CFG_CLK_CEC_EN         0 */
/* #define CHIP_CFG_CLK_UCPD1_EN       0 */
/* #define CHIP_CFG_CLK_UCPD2_EN       0 */


/* Выбор источника клока для части периферии */
/* #define CHIP_CFG_CLK_TIM1_SRC         CHIP_CLK_TIMP */            /* TIMP, PLLQ */
/* #define CHIP_CFG_CLK_TIM15_SRC        CHIP_CLK_TIMP */            /* TIMP, PLLQ */
/* #define CHIP_CFG_CLK_LPTIM1_SRC       CHIP_CLK_AHB  */            /* APB, LSI, HSI16, LSE */
/* #define CHIP_CFG_CLK_LPTIM2_SRC       CHIP_CLK_AHB  */            /* APB, LSI, HSI16, LSE */
#define CHIP_CFG_CLK_USART1_KER_SRC   CHIP_CLK_APB              /* APB, SYS, HSI16, LSE */
/* #define CHIP_CFG_CLK_USART2_KER_SRC   CHIP_CLK_APB */             /* APB, SYS, HSI16, LSE */
/* #define CHIP_CFG_CLK_USART3_SRC       CHIP_CLK_APB */             /* APB, SYS, HSI16, LSE */
/* #define CHIP_CFG_CLK_LPUART1_SRC      CHIP_CLK_APB */             /* APB, SYS, HSI16, LSE */
/* #define CHIP_CFG_CLK_I2C1_SRC         CHIP_CLK_APB */             /* APB, SYS, HSI16 */
/* #define CHIP_CFG_CLK_I2C2_SRC         CHIP_CLK_APB */             /* APB, SYS, HSI16 */
/* #define CHIP_CFG_CLK_I2S1_SRC         CHIP_CLK_SYS */             /* SYS, PLLP, HSI16, I2S_CKIN */
/* #define CHIP_CFG_CLK_I2S2_SRC         CHIP_CLK_SYS */             /* SYS, PLLP, HSI16, I2S_CKIN */
/* #define CHIP_CFG_CLK_ADC_SRC          CHIP_CLK_SYS */             /* SYS, PLLP, HSI16 */

/* #define CHIP_CFG_CLK_CEC_SRC          CHIP_CLK_HSI16_DIV488 */    /* HSI16_DIV488 , LSE */
/* #define CHIP_CFG_CLK_FDCAN_KER_SRC    CHIP_CLK_APB   */           /* APB, PLLQ, HSE */
/* #define CHIP_CFG_CLK_USB_KER_SRC      CHIP_CLK_HSI48 */           /* HSI48, PLLQ, HSE */
/* #define CHIP_CFG_CLK_RNG_KER_SRC      CHIP_CLK_HSI16 */           /* HSI16, SYS, PLLQ */
/* #define CHIP_CFG_CLK_RNG_DIV          1              */           /* делитель частоты RNG (1,2,4,8) */




/* ---------------------------- Watchdog -------------------------------------*/
/* включать ли Watchdog (используется IWDG) */
#define CHIP_CFG_WDT_SETUP_EN           1
/* Время сброса контроллера в мс, если WDT не будет сброшен  */
#define CHIP_CFG_WDT_TIMEOUT_MS         10000
/* Разрешение аппаратного режима WDT (включается всегда по сбросу,
 * иначе запускается программно при старте) */
#define CHIP_CFG_WDT_HW_MODE_EN         1
/* Разрешение работы WDT в Standby режиме процессора (иначе - останов) */
#define CHIP_CFG_WDT_ON_STANDBY_EN      1
/* Разрешение работы WDT в Stop режиме процессора (иначе - останов) */
#define CHIP_CFG_WDT_ON_STOP_EN         1

/* ----------------------------- Flash ----------------------------------------*/
/* включение функций управление flash-памятью в сборку */
#define CHIP_CFG_FLASH_FUNC_EN            1
/* разрешение быстрой записи блоками в строку */
#define CHIP_CFG_FLASH_ROW_WR_ENABLED     1

#endif /*  CHIP_CONFIG_H */
