#ifndef CHIP_EXTINT_H
#define CHIP_EXTINT_H

/* Макросы для работы с прерываниями по внешним пинам */

/* включения стандартного кода обработки прерываний */
#ifdef CHIP_CFG_PININT_ISR_PROC_EN
    #define CHIP_PININT_ISR_PROC_EN  CHIP_CFG_PININT_ISR_PROC_EN
#else
    #define CHIP_PININT_ISR_PROC_EN  0
#endif


#define CHIP_PININT_CNT          16

#define CHIP_PININT_MODE_RISE    (1UL << 0)
#define CHIP_PININT_MODE_FALL    (1UL << 1)

/* номер события в EXTI для выбранного пина */
#define CHIP_PININT_EXTI_EVTIN_NUM(pin)    CHIP_EXTI_EVTIN_EXTI(CHIP_PIN_ID_PIN(pin))
/* номер прерывания в NVIC (может резделяться между несколькими пинами) для выбранного пина */
#define CHIP_PININT_NVIC_IRQ_NUM(pin)     (CHIP_PININT_EXTI_EVTIN_NUM(pin) == 0 ? EXTI0_IRQn : \
                                           CHIP_PININT_EXTI_EVTIN_NUM(pin) == 1 ? EXTI1_IRQn : \
                                           CHIP_PININT_EXTI_EVTIN_NUM(pin) == 2 ? EXTI2_IRQn : \
                                           CHIP_PININT_EXTI_EVTIN_NUM(pin) == 3 ? EXTI3_IRQn : \
                                           CHIP_PININT_EXTI_EVTIN_NUM(pin) == 4 ? EXTI4_IRQn : \
                                           CHIP_PININT_EXTI_EVTIN_NUM(pin) <= 9 ? EXTI9_5_IRQn : \
                                                                                  EXTI15_10_IRQn)


/* Настройка прерывания для выбранного пина.
 *  Для каждого номера ножки внутри порта может для прерываний использоваться только один
 *  порт (т.е. если используется PC1, то не могут быть использованы PA1, PB1, PD1 и т.д.).
 *  В случае повторого вызова с тем же номером пина, но на другом порту, будет активна только последняя
 *   насройка.
 * Макрос выполняет следующие действия:
 *  - конфигурация ножки на вход
 *  - конфигурация в SYSCFG номера порта для выбранного пина
 *  - настройка разрешения детектирования фронта и спада в соответствии с флагами
 *    CHIP_PININT_MODE_RISE и CHIP_PININT_MODE_FALL в mode */
#define CHIP_PININT_CONFIG(pin, mode) do { \
    CHIP_PIN_CONFIG_IN(pin); \
    LBITFIELD_UPD(CHIP_REGS_SYSCFG->EXTICR[CHIP_SYSCFG_EXTICR_REGNUM(CHIP_PININT_EXTI_EVTIN_NUM(pin))], \
        CHIP_SYSCFG_EXTICR_BITMSK(CHIP_PININT_EXTI_EVTIN_NUM(pin)), CHIP_PIN_ID_PORT(pin)); \
    CHIP_EXTI_EVTIN_CFG_RISE(CHIP_PININT_EXTI_EVTIN_NUM(pin), (mode & CHIP_PININT_MODE_RISE)); \
    CHIP_EXTI_EVTIN_CFG_FALL(CHIP_PININT_EXTI_EVTIN_NUM(pin), (mode & CHIP_PININT_MODE_FALL)); \
} while(0)


/* Макросы разрешения и запрета отслеживания прерываний по ножкам.
 * После разрешния CHIP_PININT_EN() можно проверять статус CHIP_PININT_IS_PEND() и
 * CHIP_PININT_PEND_CLR().
 *
 * Если же требуется обработка в прерывании NVIC и разрешена стандартная обработка
 * через CHIP_CFG_PININT_ISR_PROC_EN, то эти макросы вручную как правило использовать не нужно,
 * а используются CHIP_PININT_ISR_xxx */
#define CHIP_PININT_EN(pin)         (CHIP_EXTI_EVTIN_INT_EN(CHIP_PININT_EXTI_EVTIN_NUM(pin)))
#define CHIP_PININT_DIS(pin)        (CHIP_EXTI_EVTIN_INT_DIS(CHIP_PININT_EXTI_EVTIN_NUM(pin)))
#define CHIP_PININT_IS_PEND(pin)    (CHIP_EXTI_EVTIN_IS_PEND(CHIP_PININT_EXTI_EVTIN_NUM(pin)))
#define CHIP_PININT_PEND_CLR(pin)   (CHIP_EXTI_EVTIN_PEND_CLR(CHIP_PININT_EXTI_EVTIN_NUM(pin)))

#if CHIP_PININT_ISR_PROC_EN
    /* Тип функции обработчика события по пину.
     * Регистрируется с помощью CHIP_PININT_ISR_REGISTER() индивидуально для каждого пина.
     * Не должен анализировать и сбрасывать флаг PEND, т.к. это уже делает
     * код перед вызовом обработчика */
    typedef void (*t_chip_pinint_isr)(void);

    void chip_pinint_nvic_set_isr(uint8_t intnum, t_chip_pinint_isr isr);
    void chip_pinint_nvic_set_irq_en(uint8_t intnum, IRQn_Type irqnum, bool en);

    /* Установка обработчика для прерывания по выбранной ножке.
     * Обработчик независим и вызывается только на фронт/спад указанного пина,
     * даже если если несколько пинов используют один вектор NVIC.
     * Обработчик не должен анализировать и сбразывать флаг PEND, т.к. это уже делает
     * код перед вызовом обработчика */
    #define CHIP_PININT_ISR_REGISTER(pin, isr) chip_pinint_nvic_set_isr(CHIP_PININT_EXTI_EVTIN_NUM(pin), isr)

    /* Разрешение/запрет отслеживания по выбранной ножке и разрешения прерывания для NVIC.
     * До этого ножка уже должна быть сконфигурирована на отслеживание изменений с помощью
     * CHIP_PININT_CONFIG() и установлен обработчик с помощью CHIP_PININT_ISR_REGISTER().
     * Если несколько ножек используют один вектор NVIC, то прерывание по NVIC
     * будет разрешено, если данная функция вызвана хотя бы для одной ножки, и запрещено,
     * только когда будут запрещены прерывания по ножкам, относящимся к данному вектору */
    #define CHIP_PININT_ISR_EN(pin)  do { \
        chip_pinint_nvic_set_irq_en(CHIP_PININT_EXTI_EVTIN_NUM(pin), CHIP_PININT_NVIC_IRQ_NUM(pin), true); \
        CHIP_PININT_EN(pin); \
    } while(0)

    #define CHIP_PININT_ISR_DIS(pin)  do { \
        CHIP_PININT_DIS(pin); \
        chip_pinint_nvic_set_irq_en(CHIP_PININT_EXTI_EVTIN_NUM(pin), CHIP_PININT_NVIC_IRQ_NUM(pin), false); \
    } while(0)

#endif


#endif // CHIP_EXTINT_H

