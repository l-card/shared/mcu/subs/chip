#include "chip.h"
#include "chip_clk_constraints.h"
#include "chip_init_faults.h"
#if CHIP_CFG_LTIMER_EN
    #include "ltimer.h"
#endif

/** @todo dsi clock select */


#define CHIP_CLK_BD_CFG_SETUP_EN (CHIP_CFG_CLK_LSE_SETUP_EN | \
    CHIP_CFG_CLK_RTC_SETUP_EN | \
    CHIP_CFG_PWR_MON_SETUP_EN  | \
    CHIP_CFG_PWR_BACKUP_REG_SETUP_EN)


/*---------- определение параметров основных источников частоты --------------*/
#if CHIP_CFG_CLK_HSE_MODE != CHIP_CLK_EXTMODE_DIS
    #if CHIP_CFG_CLK_HSE_FREQ < CHIP_CLK_HSE_FREQ_MIN
        #error HSE frequency must be no less than 4 MHz
    #endif
    #if CHIP_CFG_CLK_HSE_FREQ > CHIP_CLK_HSE_FREQ_MAX
        #error HSE frequency must be up to 48 MHz
    #endif
#endif

#if CHIP_CFG_CLK_LSE_SETUP_EN
    #if CHIP_CFG_CLK_LSE_MODE == CHIP_CLK_EXTMODE_CLOCK
        #if CHIP_CLK_LSE_FREQ > CHIP_CLK_LSE_ECLK_FREQ_MAX
            #error LSE frequency in external clock mode must be up to 1 MHz
        #endif
    #endif

    #if (CHIP_CFG_CLK_LSE_DRV < CHIP_CLK_LSE_DRV_LOWEST) || \
        (CHIP_CFG_CLK_LSE_DRV > CHIP_CLK_LSE_DRV_HIGHEST)
        #error Invalid LSE oscillator driving capability
    #endif

    #define CHIP_RVAL_RCC_BDCR_LSEDRV CHIP_CFG_CLK_LSE_DRV
#endif

#define CHIP_RVAL_RCC_CR_HSIDIV_DEF        0
#if CHIP_CFG_CLK_HSI_EN
    #if CHIP_CFG_CLK_HSI_FREQ == CHIP_CLK_HSI_FREQ_64MHZ
        #define CHIP_RVAL_RCC_CR_HSIDIV     0
    #elif CHIP_CFG_CLK_HSI_FREQ == CHIP_CLK_HSI_FREQ_32MHZ
        #define CHIP_RVAL_RCC_CR_HSIDIV     1
    #elif CHIP_CFG_CLK_HSI_FREQ == CHIP_CLK_HSI_FREQ_16MHZ
        #define CHIP_RVAL_RCC_CR_HSIDIV     2
    #elif CHIP_CFG_CLK_HSI_FREQ == CHIP_CLK_HSI_FREQ_8MHZ
        #define CHIP_RVAL_RCC_CR_HSIDIV     3
    #else
        #error HSI frequency must be 64 MHz, 32 MHz, 16 MHz or 8 MHz
    #endif
#else
    #define CHIP_RVAL_RCC_CR_HSIDIV         CHIP_RVAL_RCC_CR_HSIDIV_DEF
#endif


#define CHIP_RCC_CR_CLK_WTRDY_MSK ((CHIP_CFG_CLK_HSI_EN ? (CHIP_REGFLD_RCC_CR_HSIRDY | CHIP_REGFLD_RCC_CR_HSIDIVF) : 0) \
                              | (CHIP_CFG_CLK_CSI_EN    ? CHIP_REGFLD_RCC_CR_CSIRDY : 0) \
                              | (CHIP_CFG_CLK_HSI48_EN  ? CHIP_REGFLD_RCC_CR_HSI48RDY : 0) \
                              | (CHIP_CLK_HSE_EN        ? CHIP_REGFLD_RCC_CR_HSERDY : 0))


/* -------- определение настроек источников частот при пробуждении -----------*/
#define CHIP_CLK_WU_SRC_ID CHIP_CLK_ID_VAL(CHIP_CFG_CLK_WU_SRC)
#if CHIP_CLK_WU_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_RVAL_RCC_STOPWUCK  0
#elif CHIP_CLK_WU_SRC_ID == CHIP_CLK_CSI_ID
    #define CHIP_RVAL_RCC_STOPWUCK  1
#else
    #error Invalid wake up clock from system Stop
#endif


#define CHIP_CLK_WU_KER_SRC_ID CHIP_CLK_ID_VAL(CHIP_CFG_CLK_WU_KER_SRC)
#if CHIP_CLK_WU_KER_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_RVAL_RCC_STOPKERWUCK  0
#elif CHIP_CLK_WU_KER_SRC_ID == CHIP_CLK_CSI_ID
    #define CHIP_RVAL_RCC_STOPKERWUCK  1
#else
    #error Invalid wake up kernel clock from system Stop
#endif


/* --------------------------- проверка параметров PLL -----------------------*/
#if !CHIP_CFG_CLK_PLL1_EN && !CHIP_CFG_CLK_PLL2_EN && !CHIP_CFG_CLK_PLL3_EN
    #define CHIP_CLK_PLL_EN 0
    #define CHIP_RVAL_RCC_PLLSRC 3
#else
    #define CHIP_CLK_PLL_EN 1


    #define CHIP_RCC_PLL_ON_MSK ((CHIP_CFG_CLK_PLL1_EN  ? CHIP_REGFLD_RCC_CR_PLL1ON : 0) \
                           | (CHIP_CFG_CLK_PLL2_EN      ? CHIP_REGFLD_RCC_CR_PLL2ON : 0) \
                           | (CHIP_CFG_CLK_PLL3_EN      ? CHIP_REGFLD_RCC_CR_PLL3ON : 0))
    #define CHIP_RCC_PLL_RDY_MSK ((CHIP_CFG_CLK_PLL1_EN ? CHIP_REGFLD_RCC_CR_PLL1RDY : 0) \
                               | (CHIP_CFG_CLK_PLL2_EN  ? CHIP_REGFLD_RCC_CR_PLL2RDY : 0) \
                               | (CHIP_CFG_CLK_PLL3_EN  ? CHIP_REGFLD_RCC_CR_PLL3RDY : 0))


    /* проверка источника частоты PLL */
    #if CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_PLLSRC 0
    #elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_CSI_ID
        #define CHIP_RVAL_RCC_PLLSRC 1
    #elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_PLLSRC 2
    #else
        #error invalid PLL clock source (must be HSI, CSI, HSE or all PLL must be disabled)
    #endif

    #if (!CHIP_CLK_ID_EN(CHIP_CLK_PLL_SRC))
        #error PLL input frequency source must be enabled
    #endif
#endif



#if CHIP_CFG_CLK_PLL1_EN
    #if (CHIP_CFG_CLK_PLL1_DIVM < CHIP_CLK_PLL_DIVM_MIN) || (CHIP_CFG_CLK_PLL1_DIVM > CHIP_CLK_PLL_DIVM_MAX)
        #error invalid PLL1 DIVM value
    #endif

    #if (CHIP_CLK_PLL1_IN_FREQ < CHIP_CLK_PLL_IN_FREQ_MIN) || (CHIP_CLK_PLL1_IN_FREQ > CHIP_CLK_PLL_IN_FREQ_MAX)
        #error invalid PLL1 input frequency
    #endif

    #if (CHIP_CFG_CLK_PLL1_FRACN > CHIP_CLK_PLL_FRACN_MAX)
        #error invalid PLL1 FRACN value
    #endif

    /* выбор VCOL или VCOH по входной частоте. максимум и минимум VCO зависит от этого выбора */
    #if (CHIP_CLK_PLL1_IN_FREQ > CHIP_CLK_PLL_IN_VCOL_FREQ_MAX)
        #define CHIP_RVAL_RCC_PLL1_VCOSEL 0
        #define CHIP_CLK_PLL1_VCO_FREQ_MAX  CHIP_CLK_PLL_VCOH_FREQ_MAX
        #define CHIP_CLK_PLL1_VCO_FREQ_MIN  CHIP_CLK_PLL_VCOH_FREQ_MIN
        #define CHIP_CLK_PLL1_OUT_FREQ_MIN  CHIP_CLK_PLL_VCOH_OUT_FREQ_MIN
        #define CHIP_CLK_PLL1_OUT_FREQ_MAX  CHIP_CLK_PLL_VCOH_OUT_FREQ_MAX
    #else
        #define CHIP_RVAL_RCC_PLL1_VCOSEL 1
        #define CHIP_CLK_PLL1_VCO_FREQ_MAX  CHIP_CLK_PLL_VCOL_FREQ_MAX
        #define CHIP_CLK_PLL1_VCO_FREQ_MIN  CHIP_CLK_PLL_VCOL_FREQ_MIN
        #define CHIP_CLK_PLL1_OUT_FREQ_MIN  CHIP_CLK_PLL_VCOL_OUT_FREQ_MIN
        #define CHIP_CLK_PLL1_OUT_FREQ_MAX  CHIP_CLK_PLL_VCOL_OUT_FREQ_MAX
    #endif

    /* определение кода настройки диапазона входной частоты для PLL */
    #if (CHIP_CLK_PLL1_IN_FREQ > CHIP_MHZ(8))
        #define CHIP_RVAL_RCC_PLL1_RGE 3
    #elif (CHIP_CLK_PLL1_IN_FREQ > CHIP_MHZ(4))
        #define CHIP_RVAL_RCC_PLL1_RGE 2
    #elif (CHIP_CLK_PLL1_IN_FREQ > CHIP_MHZ(2))
        #define CHIP_RVAL_RCC_PLL1_RGE 1
    #else
        #define CHIP_RVAL_RCC_PLL1_RGE 0
    #endif


    #if (CHIP_CLK_PLL1_VCO_FREQ < CHIP_CLK_PLL1_VCO_FREQ_MIN) || \
            (CHIP_CLK_PLL1_VCO_FREQ > CHIP_CLK_PLL1_VCO_FREQ_MAX)
        #error PLL1 VCO Frequency out of range
    #endif

    #if (CHIP_CFG_CLK_PLL1_DIVN < CHIP_CLK_PLL_DIVN_MIN)  || \
         (CHIP_CFG_CLK_PLL1_DIVN > CHIP_CLK_PLL_DIVN_MAX)
        #error invalid PLL1 DIVN value
    #endif

    #if CHIP_CFG_CLK_PLL1_DIVP_EN
        #if  (CHIP_CFG_CLK_PLL1_DIVP < CHIP_CLK_PLL_DIVP_MIN) || \
             (CHIP_CFG_CLK_PLL1_DIVP > CHIP_CLK_PLL_DIVP_MAX)
            #error invalid PLL1 DIVP value
        #endif

        #if !CHIP_DEV_SUPPORT_PLL1_DIVP_ODD
            #if (CHIP_CFG_CLK_PLL1_DIVP % 2 !=0)
                #error PLL1 P Divider must be even
            #endif
        #endif

        #if (CHIP_CLK_PLL1_P_FREQ < CHIP_CLK_PLL1_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL1_P_FREQ > CHIP_CLK_PLL1_OUT_FREQ_MAX)
            #error invalid PLL1 P output freqence
        #endif
    #endif

    #if CHIP_CFG_CLK_PLL1_DIVQ_EN
        #if  (CHIP_CFG_CLK_PLL1_DIVQ < CHIP_CLK_PLL_DIVQ_MIN) || \
             (CHIP_CFG_CLK_PLL1_DIVQ > CHIP_CLK_PLL_DIVQ_MAX)
            #error invalid PLL1 DIVQ value
        #endif

        #if (CHIP_CLK_PLL1_Q_FREQ < CHIP_CLK_PLL1_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL1_Q_FREQ > CHIP_CLK_PLL1_OUT_FREQ_MAX)
            #error invalid PLL1 Q output freqence
        #endif
    #endif

    #if CHIP_CFG_CLK_PLL1_DIVR_EN
        #if  (CHIP_CFG_CLK_PLL1_DIVR < CHIP_CLK_PLL_DIVR_MIN) || \
             (CHIP_CFG_CLK_PLL1_DIVR > CHIP_CLK_PLL_DIVR_MAX)
            #error invalid PLL1 DIVR value
        #endif

        #if (CHIP_CLK_PLL1_R_FREQ < CHIP_CLK_PLL1_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL1_R_FREQ > CHIP_CLK_PLL1_OUT_FREQ_MAX)
            #error invalid PLL1 R output freqence
        #endif
    #endif
#endif


#if CHIP_CFG_CLK_PLL2_EN
    #if (CHIP_CFG_CLK_PLL2_DIVM < CHIP_CLK_PLL_DIVM_MIN) || (CHIP_CFG_CLK_PLL2_DIVM > CHIP_CLK_PLL_DIVM_MAX)
        #error invalid PLL2 DIVM value
    #endif

    #if (CHIP_CLK_PLL2_IN_FREQ < CHIP_CLK_PLL_IN_FREQ_MIN) || (CHIP_CLK_PLL2_IN_FREQ > CHIP_CLK_PLL_IN_FREQ_MAX)
        #error invalid PLL2 input frequency
    #endif

    #if (CHIP_CFG_CLK_PLL2_FRACN > CHIP_CLK_PLL_FRACN_MAX)
        #error invalid PLL2 FRACN value
    #endif

    /* выбор VCOL или VCOH по входной частоте. максимум и минимум VCO зависит от этого выбора */
    #if (CHIP_CLK_PLL2_IN_FREQ > CHIP_CLK_PLL_IN_VCOL_FREQ_MAX)
        #define CHIP_RVAL_RCC_PLL2_VCOSEL 0
        #define CHIP_CLK_PLL2_VCO_FREQ_MAX  CHIP_CLK_PLL_VCOH_FREQ_MAX
        #define CHIP_CLK_PLL2_VCO_FREQ_MIN  CHIP_CLK_PLL_VCOH_FREQ_MIN
        #define CHIP_CLK_PLL2_OUT_FREQ_MIN  CHIP_CLK_PLL_VCOH_OUT_FREQ_MIN
        #define CHIP_CLK_PLL2_OUT_FREQ_MAX  CHIP_CLK_PLL_VCOH_OUT_FREQ_MAX
    #else
        #define CHIP_RVAL_RCC_PLL2_VCOSEL 1
        #define CHIP_CLK_PLL2_VCO_FREQ_MAX  CHIP_CLK_PLL_VCOL_FREQ_MAX
        #define CHIP_CLK_PLL2_VCO_FREQ_MIN  CHIP_CLK_PLL_VCOL_FREQ_MIN
        #define CHIP_CLK_PLL2_OUT_FREQ_MIN  CHIP_CLK_PLL_VCOL_OUT_FREQ_MIN
        #define CHIP_CLK_PLL2_OUT_FREQ_MAX  CHIP_CLK_PLL_VCOL_OUT_FREQ_MAX
    #endif

    /* определение кода настройки диапазона входной частоты для PLL */
    #if (CHIP_CLK_PLL2_IN_FREQ > CHIP_MHZ(8))
        #define CHIP_RVAL_RCC_PLL2_RGE 3
    #elif (CHIP_CLK_PLL2_IN_FREQ > CHIP_MHZ(4))
        #define CHIP_RVAL_RCC_PLL2_RGE 2
    #elif (CHIP_CLK_PLL2_IN_FREQ > CHIP_MHZ(2))
        #define CHIP_RVAL_RCC_PLL2_RGE 1
    #else
        #define CHIP_RVAL_RCC_PLL2_RGE 0
    #endif


    #if (CHIP_CLK_PLL2_VCO_FREQ < CHIP_CLK_PLL2_VCO_FREQ_MIN) || \
            (CHIP_CLK_PLL2_VCO_FREQ > CHIP_CLK_PLL2_VCO_FREQ_MAX)
        #error PLL2 VCO Frequency out of range
    #endif

    #if (CHIP_CFG_CLK_PLL2_DIVN < CHIP_CLK_PLL_DIVN_MIN)  || \
         (CHIP_CFG_CLK_PLL2_DIVN > CHIP_CLK_PLL_DIVN_MAX)
        #error invalid PLL2 DIVN value
    #endif

    #if CHIP_CFG_CLK_PLL2_DIVP_EN
        #if  (CHIP_CFG_CLK_PLL2_DIVP < CHIP_CLK_PLL_DIVP_MIN) || \
             (CHIP_CFG_CLK_PLL2_DIVP > CHIP_CLK_PLL_DIVP_MAX)
            #error invalid PLL2 DIVP value
        #endif

        #if (CHIP_CLK_PLL2_P_FREQ < CHIP_CLK_PLL2_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL2_P_FREQ > CHIP_CLK_PLL2_OUT_FREQ_MAX)
            #error invalid PLL2 P output freqence
        #endif
    #endif

    #if CHIP_CFG_CLK_PLL2_DIVQ_EN
        #if  (CHIP_CFG_CLK_PLL2_DIVQ < CHIP_CLK_PLL_DIVQ_MIN) || \
             (CHIP_CFG_CLK_PLL2_DIVQ > CHIP_CLK_PLL_DIVQ_MAX)
            #error invalid PLL2 DIVQ value
        #endif

        #if (CHIP_CLK_PLL2_Q_FREQ < CHIP_CLK_PLL2_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL2_Q_FREQ > CHIP_CLK_PLL2_OUT_FREQ_MAX)
            #error invalid PLL2 Q output freqence
        #endif
    #endif

    #if CHIP_CFG_CLK_PLL2_DIVR_EN
        #if  (CHIP_CFG_CLK_PLL2_DIVR < CHIP_CLK_PLL_DIVR_MIN) || \
             (CHIP_CFG_CLK_PLL2_DIVR > CHIP_CLK_PLL_DIVR_MAX)
            #error invalid PLL2 DIVR value
        #endif

        #if (CHIP_CLK_PLL2_R_FREQ < CHIP_CLK_PLL2_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL2_R_FREQ > CHIP_CLK_PLL2_OUT_FREQ_MAX)
            #error invalid PLL2 R output freqence
        #endif
    #endif
#endif

#if CHIP_CFG_CLK_PLL3_EN
    #if (CHIP_CFG_CLK_PLL3_DIVM < CHIP_CLK_PLL_DIVM_MIN) || (CHIP_CFG_CLK_PLL3_DIVM > CHIP_CLK_PLL_DIVM_MAX)
        #error invalid PLL3 DIVM value
    #endif

    #if (CHIP_CLK_PLL3_IN_FREQ < CHIP_CLK_PLL_IN_FREQ_MIN) || (CHIP_CLK_PLL3_IN_FREQ > CHIP_CLK_PLL_IN_FREQ_MAX)
        #error invalid PLL3 input frequency
    #endif

    #if (CHIP_CFG_CLK_PLL3_FRACN > CHIP_CLK_PLL_FRACN_MAX)
        #error invalid PLL3 FRACN value
    #endif
    /* выбор VCOL или VCOH по входной частоте. максимум и минимум VCO зависит от этого выбора */
    #if (CHIP_CLK_PLL3_IN_FREQ > CHIP_CLK_PLL_IN_VCOL_FREQ_MAX)
        #define CHIP_RVAL_RCC_PLL3_VCOSEL 0
        #define CHIP_CLK_PLL3_VCO_FREQ_MAX  CHIP_CLK_PLL_VCOH_FREQ_MAX
        #define CHIP_CLK_PLL3_VCO_FREQ_MIN  CHIP_CLK_PLL_VCOH_FREQ_MIN
        #define CHIP_CLK_PLL3_OUT_FREQ_MIN  CHIP_CLK_PLL_VCOH_OUT_FREQ_MIN
        #define CHIP_CLK_PLL3_OUT_FREQ_MAX  CHIP_CLK_PLL_VCOH_OUT_FREQ_MAX
    #else
        #define CHIP_RVAL_RCC_PLL3_VCOSEL 1
        #define CHIP_CLK_PLL3_VCO_FREQ_MAX  CHIP_CLK_PLL_VCOL_FREQ_MAX
        #define CHIP_CLK_PLL3_VCO_FREQ_MIN  CHIP_CLK_PLL_VCOL_FREQ_MIN
        #define CHIP_CLK_PLL3_OUT_FREQ_MIN  CHIP_CLK_PLL_VCOL_OUT_FREQ_MIN
        #define CHIP_CLK_PLL3_OUT_FREQ_MAX  CHIP_CLK_PLL_VCOL_OUT_FREQ_MAX
    #endif
    /* определение кода настройки диапазона входной частоты для PLL */
    #if (CHIP_CLK_PLL3_IN_FREQ > CHIP_MHZ(8))
        #define CHIP_RVAL_RCC_PLL3_RGE 3
    #elif (CHIP_CLK_PLL3_IN_FREQ > CHIP_MHZ(4))
        #define CHIP_RVAL_RCC_PLL3_RGE 2
    #elif (CHIP_CLK_PLL3_IN_FREQ > CHIP_MHZ(2))
        #define CHIP_RVAL_RCC_PLL3_RGE 1
    #else
        #define CHIP_RVAL_RCC_PLL3_RGE 0
    #endif


    #if (CHIP_CLK_PLL3_VCO_FREQ < CHIP_CLK_PLL3_VCO_FREQ_MIN) || \
            (CHIP_CLK_PLL3_VCO_FREQ > CHIP_CLK_PLL3_VCO_FREQ_MAX)
        #error PLL3 VCO Frequency out of range
    #endif

    #if (CHIP_CFG_CLK_PLL3_DIVN < CHIP_CLK_PLL_DIVN_MIN)  || \
         (CHIP_CFG_CLK_PLL3_DIVN > CHIP_CLK_PLL_DIVN_MAX)
        #error invalid PLL3 DIVN value
    #endif

    #if CHIP_CFG_CLK_PLL3_DIVP_EN
        #if  (CHIP_CFG_CLK_PLL3_DIVP < CHIP_CLK_PLL_DIVP_MIN) || \
             (CHIP_CFG_CLK_PLL3_DIVP > CHIP_CLK_PLL_DIVP_MAX)
            #error invalid PLL3 DIVP value
        #endif

        #if (CHIP_CLK_PLL3_P_FREQ < CHIP_CLK_PLL3_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL3_P_FREQ > CHIP_CLK_PLL3_OUT_FREQ_MAX)
            #error invalid PLL3 P output freqence
        #endif
    #endif

    #if CHIP_CFG_CLK_PLL3_DIVQ_EN
        #if  (CHIP_CFG_CLK_PLL3_DIVQ < CHIP_CLK_PLL_DIVQ_MIN) || \
             (CHIP_CFG_CLK_PLL3_DIVQ > CHIP_CLK_PLL_DIVQ_MAX)
            #error invalid PLL3 DIVQ value
        #endif

        #if (CHIP_CLK_PLL3_Q_FREQ < CHIP_CLK_PLL3_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL3_Q_FREQ > CHIP_CLK_PLL3_OUT_FREQ_MAX)
            #error invalid PLL3 Q output freqence
        #endif
    #endif

    #if CHIP_CFG_CLK_PLL3_DIVR_EN
        #if  (CHIP_CFG_CLK_PLL3_DIVR < CHIP_CLK_PLL_DIVR_MIN) || \
             (CHIP_CFG_CLK_PLL3_DIVR > CHIP_CLK_PLL_DIVR_MAX)
            #error invalid PLL3 DIVR value
        #endif

        #if (CHIP_CLK_PLL3_R_FREQ < CHIP_CLK_PLL3_OUT_FREQ_MIN) || \
            (CHIP_CLK_PLL3_R_FREQ > CHIP_CLK_PLL3_OUT_FREQ_MAX)
            #error invalid PLL3 R output freqence
        #endif
    #endif
#endif



/* --------------------- Проверка параметров SystemClock -------------------- */
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_HSI
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_CSI_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_CSI
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_HSE
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLL1_P_ID    
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_PLL1
#else
    #error Invalid System Clock Source (must be from HSI, CSI, HSE, PLL1_P)
#endif

#if !CHIP_CLK_SYS_EN
    #error System clock source is not enabled
#endif


/* ------ Проверка и определение кодов для делителей частот CPU и шин ------*/
#if CHIP_CFG_CLK_D1_CPU_DIV == 1
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0x0
#elif CHIP_CFG_CLK_D1_CPU_DIV == 2
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0x8
#elif CHIP_CFG_CLK_D1_CPU_DIV == 4
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0x9
#elif CHIP_CFG_CLK_D1_CPU_DIV == 8
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0xA
#elif CHIP_CFG_CLK_D1_CPU_DIV == 16
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0xB
#elif CHIP_CFG_CLK_D1_CPU_DIV == 64
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0xC
#elif CHIP_CFG_CLK_D1_CPU_DIV == 128
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0xD
#elif CHIP_CFG_CLK_D1_CPU_DIV == 256
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0xE
#elif CHIP_CFG_CLK_D1_CPU_DIV == 512
    #define CHIP_RVAL_RCC_D1CFGR_D1CPRE 0xF
#else
    #error Invalid D1CPRE divider value
#endif

#if CHIP_CFG_CLK_HOST_DIV == 1
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0x0
#elif CHIP_CFG_CLK_HOST_DIV == 2
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0x8
#elif CHIP_CFG_CLK_HOST_DIV == 4
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0x9
#elif CHIP_CFG_CLK_HOST_DIV == 8
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0xA
#elif CHIP_CFG_CLK_HOST_DIV == 16
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0xB
#elif CHIP_CFG_CLK_HOST_DIV == 64
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0xC
#elif CHIP_CFG_CLK_HOST_DIV == 128
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0xD
#elif CHIP_CFG_CLK_HOST_DIV == 256
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0xE
#elif CHIP_CFG_CLK_HOST_DIV == 512
    #define CHIP_RVAL_RCC_D1CFGR_HPRE 0xF
#else
    #error Invalid HPRE divider value
#endif



#if CHIP_CFG_CLK_APB1_DIV == 1
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE1 0x0
#elif CHIP_CFG_CLK_APB1_DIV == 2
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE1 0x4
#elif CHIP_CFG_CLK_APB1_DIV == 4
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE1 0x5
#elif CHIP_CFG_CLK_APB1_DIV == 8
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE1 0x6
#elif CHIP_CFG_CLK_APB1_DIV == 16
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE1 0x7
#else
    #error Invalid APB1 divider value
#endif

#if CHIP_CFG_CLK_APB2_DIV == 1
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE2 0x0
#elif CHIP_CFG_CLK_APB2_DIV == 2
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE2 0x4
#elif CHIP_CFG_CLK_APB2_DIV == 4
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE2 0x5
#elif CHIP_CFG_CLK_APB2_DIV == 8
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE2 0x6
#elif CHIP_CFG_CLK_APB2_DIV == 16
    #define CHIP_RVAL_RCC_D2CFGR_D2PPRE2 0x7
#else
    #error Invalid APB2 divider value
#endif

#if CHIP_CFG_CLK_APB3_DIV == 1
    #define CHIP_RVAL_RCC_D1CFGR_D1PPRE 0x0
#elif CHIP_CFG_CLK_APB3_DIV == 2
    #define CHIP_RVAL_RCC_D1CFGR_D1PPRE 0x4
#elif CHIP_CFG_CLK_APB3_DIV == 4
    #define CHIP_RVAL_RCC_D1CFGR_D1PPRE 0x5
#elif CHIP_CFG_CLK_APB3_DIV == 8
    #define CHIP_RVAL_RCC_D1CFGR_D1PPRE 0x6
#elif CHIP_CFG_CLK_APB3_DIV == 16
    #define CHIP_RVAL_RCC_D1CFGR_D1PPRE 0x7
#else
    #error Invalid APB3 divider value
#endif


#if CHIP_CFG_CLK_APB4_DIV == 1
    #define CHIP_RVAL_RCC_D3CFGR_D3PPRE 0x0
#elif CHIP_CFG_CLK_APB4_DIV == 2
    #define CHIP_RVAL_RCC_D3CFGR_D3PPRE 0x4
#elif CHIP_CFG_CLK_APB3_DIV == 4
    #define CHIP_RVAL_RCC_D3CFGR_D3PPRE 0x5
#elif CHIP_CFG_CLK_APB3_DIV == 8
    #define CHIP_RVAL_RCC_D3CFGR_D3PPRE 0x6
#elif CHIP_CFG_CLK_APB3_DIV == 16
    #define CHIP_RVAL_RCC_D3CFGR_D3PPRE 0x7
#else
    #error Invalid APB3 divider value
#endif


/* проверка пределов частоты CPU и AHB */
#if CHIP_CLK_CPU_CM7_FREQ > CHIP_CLK_CPU_CM7_FREQ_MAX
    #error CPU frequency must be up to CHIP_CLK_CPU_CM7_FREQ_MAX MHz
#endif

#if CHIP_CLK_HOST_FREQ > CHIP_CLK_AHB_FREQ_MAX
    #error HPRE (AHB) frequency must be up to CHIP_CLK_AHB_FREQ_MAX MHz
#endif





/* определяем бит TIMPRE для таймера X: 0,1 или -1 (без разницы 0 или 1) */
#if CHIP_CFG_CLK_TIMX_DIV == 1
    #if (CHIP_CFG_CLK_APB1_DIV == 1) || (CHIP_CFG_CLK_APB1_DIV == 2)
        #define CHIP_RVAL_RCC_TIMX_PRE -1
    #elif (CHIP_CFG_CLK_APB1_DIV == 4)
        #define CHIP_RVAL_RCC_TIMX_PRE 1
    #else
        #error Incompatible TIMX_DIV and D2_APB1_DIV values
    #endif
#elif CHIP_CFG_CLK_TIMX_DIV == 2
    #if (CHIP_CFG_CLK_APB1_DIV == 4)
        #define CHIP_RVAL_RCC_TIMX_PRE 0
    #elif (CHIP_CFG_CLK_APB1_DIV == 8)
        #define CHIP_RVAL_RCC_TIMX_PRE 1
    #else
        #error Incompatible TIMX_DIV and D2_APB1_DIV values
    #endif
#elif CHIP_CFG_CLK_TIMX_DIV == 4
    #if (CHIP_CFG_CLK_APB1_DIV == 8)
        #define CHIP_RVAL_RCC_TIMX_PRE 0
    #elif (CHIP_CFG_CLK_APB1_DIV == 16)
        #define CHIP_RVAL_RCC_TIMX_PRE 1
    #else
        #error Incompatible TIMX_DIV and D2_APB1_DIV values
    #endif
#elif CHIP_CFG_CLK_TIMX_DIV == 8
    #if (CHIP_CFG_CLK_APB1_DIV == 16)
        #define CHIP_RVAL_RCC_TIMX_PRE 0
    #else
        #error Incompatible TIMX_DIV and D2_APB1_DIV values
    #endif
#else
    #error Invalid TIMX_DIV value
#endif

/* определяем бит TIMPRE для таймера Y: 0,1 или -1 (без разницы 0 или 1) */
#if CHIP_CFG_CLK_TIMY_DIV == 1
    #if (CHIP_CFG_CLK_APB2_DIV == 1) || (CHIP_CFG_CLK_APB2_DIV == 2)
        #define CHIP_RVAL_RCC_TIMY_PRE -1
    #elif (CHIP_CFG_CLK_APB2_DIV == 4)
        #define CHIP_RVAL_RCC_TIMY_PRE 1
    #else
        #error Incompatible TIMY_DIV and D2_APB2_DIV values
    #endif
#elif CHIP_CFG_CLK_TIMY_DIV == 2
    #if (CHIP_CFG_CLK_APB2_DIV == 4)
        #define CHIP_RVAL_RCC_TIMY_PRE 0
    #elif (CHIP_CFG_CLK_APB2_DIV == 8)
        #define CHIP_RVAL_RCC_TIMY_PRE 1
    #else
        #error Incompatible TIMY_DIV and D2_APB2_DIV values
    #endif
#elif CHIP_CFG_CLK_TIMY_DIV == 4
    #if (CHIP_CFG_CLK_APB2_DIV == 8)
        #define CHIP_RVAL_RCC_TIMY_PRE 0
    #elif (CHIP_CFG_CLK_APB2_DIV == 16)
        #define CHIP_RVAL_RCC_TIMY_PRE 1
    #else
        #error Incompatible TIMY_DIV and D2_APB2_DIV values
    #endif
#elif CHIP_CFG_CLK_TIMY_DIV == 8
    #if (CHIP_CFG_CLK_APB2_DIV == 16)
        #define CHIP_RVAL_RCC_TIMY_PRE 0
    #else
        #error Incompatible TIMY_DIV and D2_APB2_DIV values
    #endif
#else
    #error Invalid TIMY_DIV value
#endif

/* Определяем результирующий бит TIMPRE по таймерам X и Y.
 * Если оба не важны, берем 0, если один не важен - берем другой,
 * иначе оба должны совпасть */
#if (CHIP_RVAL_RCC_TIMX_PRE == -1) && (CHIP_RVAL_RCC_TIMY_PRE == -1)
    #define CHIP_RVAL_RCC_TIM_PRE 0
#elif (CHIP_RVAL_RCC_TIMX_PRE == -1)
    #define CHIP_RVAL_RCC_TIM_PRE CHIP_RVAL_RCC_TIMY_PRE
#elif (CHIP_RVAL_RCC_TIMY_PRE == -1)
    #define CHIP_RVAL_RCC_TIM_PRE CHIP_RVAL_RCC_TIMX_PRE
#elif (CHIP_RVAL_RCC_TIMX_PRE == CHIP_RVAL_RCC_TIMY_PRE)
    #define CHIP_RVAL_RCC_TIM_PRE CHIP_RVAL_RCC_TIMX_PRE
#else
    #error Incompatible TIMX_DIV and TIMY_DIV values
#endif

#if CHIP_DEV_SUPPORT_HRTIM
#if CHIP_CLK_HRTIM_KER_SRC_ID == CHIP_CLK_TIMY_KER_ID
    #define CHIP_RVAL_RCC_HRTIMSEL 0
#elif CHIP_CLK_HRTIM_KER_SRC_ID == CHIP_CLK_CPU_CM7_ID
    #define CHIP_RVAL_RCC_HRTIMSEL 1
#else
    #error Invalid clock source for HRTIM (must be TIMY_KER or CPU)
#endif
#endif



/* ------------------- Параметры клока RTC -----------------------------------*/
#if CHIP_CFG_CLK_RTC_SETUP_EN
    #if CHIP_CLK_RTC_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_RTCSEL 1
    #elif CHIP_CLK_RTC_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_RTCSEL 2
    #elif CHIP_CLK_RTC_SRC_ID == CHIP_CLK_RTC_HSE_ID
        #define CHIP_RVAL_RCC_RTCSEL 3
        #if ((CHIP_CFG_CLK_RTC_HSE_DIV < CHIP_CLK_RTC_HSE_DIV_MIN) || \
              (CHIP_CFG_CLK_RTC_HSE_DIV > CHIP_CLK_RTC_HSE_DIV_MAX))
            #error invalid HSE for RTC divider value
        #endif

        #define CHIP_RVAL_RCC_RTCPRE  CHIP_CFG_CLK_RTC_HSE_DIV
    #else
        #error invalid RTC clock source
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_RTC_SRC)
        #error source clock for RTC is not enabled
    #endif

    #if CHIP_CLK_RTC_FREQ > CHIP_CLK_RTC_FREQ_MAX
        #error RTC frequency is out of range
    #endif
#else
    #define CHIP_RVAL_RCC_RTCSEL 0
#endif

/* для режимов, не использующих RTC_HSE, отключаем данный делитель */
#ifndef CHIP_RVAL_RCC_RTCPRE
    #define CHIP_RVAL_RCC_RTCPRE 0
#endif


/* ------------------- Параметры клока MCO -----------------------------------*/
#if CHIP_CFG_CLK_MCO1_EN
    #if (CHIP_CFG_CLK_MCO1_DIV < CHIP_CLK_MCO_DIV_MIN) || (CHIP_CFG_CLK_MCO1_DIV > CHIP_CLK_MCO_DIV_MAX)
        #error Invalid MCO1 divider
    #endif

    #define CHIP_RVAL_RCC_MCO1PRE CHIP_CFG_CLK_MCO1_DIV

    #define CHIP_CLK_MCO1_SRC_ID CHIP_CLK_ID_VAL(CHIP_CFG_CLK_MCO1_SRC)
    #if CHIP_CLK_MCO1_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_MCO1 0
    #elif CHIP_CLK_MCO1_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_MCO1 1
    #elif CHIP_CLK_MCO1_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_MCO1 2
    #elif CHIP_CLK_MCO1_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_MCO1 3
    #elif CHIP_CLK_MCO1_SRC_ID == CHIP_CLK_HSI48_ID
        #define CHIP_RVAL_RCC_MCO1 4
    #else
        #error Invalid MCO1 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_MCO1_SRC)
        #error Source clock for MCO1 is not enabled
    #endif

    #if !CHIP_CLK_GPIOA_EN
        #error MCO1 GPIO Port clock is not enabled
    #endif
#endif

#if CHIP_CFG_CLK_MCO2_EN
    #if (CHIP_CFG_CLK_MCO2_DIV < CHIP_CLK_MCO_DIV_MIN) || (CHIP_CFG_CLK_MCO2_DIV > CHIP_CLK_MCO_DIV_MAX)
        #error Invalid MCO2 divider
    #endif

    #define CHIP_RVAL_RCC_MCO2PRE CHIP_CFG_CLK_MCO2_DIV

    #define CHIP_CLK_MCO2_SRC_ID CHIP_CLK_ID_VAL(CHIP_CFG_CLK_MCO2_SRC)
    #if CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_MCO2 0
    #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_PLL2_P
        #define CHIP_RVAL_RCC_MCO2 1
    #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_MCO2 2
    #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_PLL1_P_ID
        #define CHIP_RVAL_RCC_MCO2 3
    #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_CSI_ID
        #define CHIP_RVAL_RCC_MCO2 4
    #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_MCO2 5
    #else
        #error Invalid MCO2 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_MCO2_SRC)
        #error Source clock for MCO2 is not enabled
    #endif

    #if !CHIP_CLK_GPIOC_EN
        #error MCO2 GPIO Port clock is not enabled
    #endif
#endif



/* ------------------------ Периферия ----------------------------------------*/
#if CHIP_CLK_PER_SRC_ID == CHIP_CLK_HSI_KER_ID
    #define CHIP_RVAL_RCC_CKPERSEL    0
#elif CHIP_CLK_PER_SRC_ID == CHIP_CLK_CSI_KER_ID
    #define CHIP_RVAL_RCC_CKPERSEL    1
#elif CHIP_CLK_PER_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_RVAL_RCC_CKPERSEL    2
#else
    #error Invalid PER source clock
#endif

#define CHIP_RCC_RMSK_CKPERSEL CHIP_REGFLD_RCC_D1CCIPR_CKPERSEL

#if CHIP_CLK_EXMC_KER_SRC_EN
    #define CHIP_CLK_EXMC_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_EXMC_KER_SRC)
    #if CHIP_CLK_EXMC_SRC_ID == CHIP_CLK_AHB3_ID
        #define CHIP_RVAL_RCC_EXMCSEL 0
    #elif CHIP_CLK_EXMC_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_EXMCSEL 1
    #elif CHIP_CLK_EXMC_SRC_ID == CHIP_CLK_PLL2_R_ID
        #define CHIP_RVAL_RCC_EXMCSEL 2
    #elif CHIP_CLK_EXMC_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_EXMCSEL 3
    #else
        #error Invalid EXMC source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_EXMC_KER_SRC)
        #error Source clock for EXMC is not enabled
    #endif

    #if CHIP_CLK_EXMC_KER_FREQ > CHIP_CLK_EXMC_KER_FREQ_MAX
        #error EXMC kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_EXMCSEL  CHIP_REGFLD_RCC_D1CCIPR_EXMCSEL
#else
    #define CHIP_RCC_RMSK_EXMCSEL 0
    #define CHIP_RVAL_RCC_EXMCSEL 0
#endif

#if CHIP_DEV_SUPPORT_OCTOSPI
    #if CHIP_CLK_OCTOSPI_KER_SRC_EN
        #define CHIP_CLK_OCTOSPI_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_OCTOSPI_KER_SRC)
        #if CHIP_CLK_OCTOSPI_SRC_ID == CHIP_CLK_AHB3_ID
            #define CHIP_RVAL_RCC_OCTOSPISEL 0
        #elif CHIP_CLK_OCTOSPI_SRC_ID == CHIP_CLK_PLL1_Q_ID
            #define CHIP_RVAL_RCC_OCTOSPISEL 1
        #elif CHIP_CLK_OCTOSPI_SRC_ID == CHIP_CLK_PLL2_R_ID
            #define CHIP_RVAL_RCC_OCTOSPISEL 2
        #elif CHIP_CLK_OCTOSPI_SRC_ID == CHIP_CLK_PER_ID
            #define CHIP_RVAL_RCC_OCTOSPISEL 3
        #else
            #error Invalid OCTOSPI source clock
        #endif

        #if !CHIP_CLK_ID_EN(CHIP_CLK_OCTOSPI_KER_SRC)
            #error Source clock for OCTOSPI is not enabled
        #endif

        #if CHIP_CLK_OCTOSPI_KER_FREQ > CHIP_CLK_OCTOSPI_KER_FREQ_MAX
            #error OCTOSPI kernel frequency is out of range
        #endif

        #define CHIP_RCC_RMSK_OCTOSPISEL  CHIP_REGFLD_RCC_D1CCIPR_OCTOSPISEL
    #else
        #define CHIP_RCC_RMSK_OCTOSPISEL  0
        #define CHIP_RVAL_RCC_OCTOSPISEL  0
    #endif
#else
    #if CHIP_CLK_QSPI_KER_SRC_EN
        #define CHIP_CLK_QSPI_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_QSPI_KER_SRC)
        #if CHIP_CLK_QSPI_SRC_ID == CHIP_CLK_AHB3_ID
            #define CHIP_RVAL_RCC_QSPISEL 0
        #elif CHIP_CLK_QSPI_SRC_ID == CHIP_CLK_PLL1_Q_ID
            #define CHIP_RVAL_RCC_QSPISEL 1
        #elif CHIP_CLK_QSPI_SRC_ID == CHIP_CLK_PLL2_R_ID
            #define CHIP_RVAL_RCC_QSPISEL 2
        #elif CHIP_CLK_QSPI_SRC_ID == CHIP_CLK_PER_ID
            #define CHIP_RVAL_RCC_QSPISEL 3
        #else
            #error Invalid QSPI source clock
        #endif

        #if !CHIP_CLK_ID_EN(CHIP_CLK_QSPI_KER_SRC)
            #error Source clock for QSPI is not enabled
        #endif

        #if CHIP_CLK_QSPI_KER_FREQ > CHIP_CLK_QSPI_KER_FREQ_MAX
            #error QSPI kernel frequency is out of range
        #endif

        #define CHIP_RCC_RMSK_QSPISEL  CHIP_REGFLD_RCC_D1CCIPR_QSPISEL
    #else
        #define CHIP_RCC_RMSK_QSPISEL  0
        #define CHIP_RVAL_RCC_QSPISEL  0
    #endif
#endif


#if CHIP_CLK_SDMMC_KER_SRC_EN
    #define CHIP_CLK_SDMMC_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SDMMC_KER_SRC)
    #if CHIP_CLK_SDMMC_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_SDMMCSEL 0
    #elif CHIP_CLK_SDMMC_SRC_ID == CHIP_CLK_PLL2_R_ID
        #define CHIP_RVAL_RCC_SDMMCSEL 1
    #else
        #error Invalid SDMMC source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SDMMC_KER_SRC)
        #error Source clock for SDMMC is not enabled
    #endif

    #if CHIP_CLK_SDMMC_KER_FREQ > CHIP_CLK_SDMMC_KER_FREQ_MAX
        #error SDMMC kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SDMMCSEL  CHIP_REGFLD_RCC_D1CCIPR_SDMMCSEL
#else
    #define CHIP_RCC_RMSK_SDMMCSEL  0
    #define CHIP_RVAL_RCC_SDMMCSEL  0
#endif


#if CHIP_CLK_SAI1_KER_SRC_EN
    #define CHIP_CLK_SAI1_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SAI1_KER_SRC)
    #if CHIP_CLK_SAI1_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_SAI1SEL 0
    #elif CHIP_CLK_SAI1_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_SAI1SEL 1
    #elif CHIP_CLK_SAI1_SRC_ID == CHIP_CLK_PLL3_P_ID
        #define CHIP_RVAL_RCC_SAI1SEL 2
    #elif CHIP_CLK_SAI1_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #define CHIP_RVAL_RCC_SAI1SEL 3
    #elif CHIP_CLK_SAI1_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_SAI1SEL 4
    #else
        #error Invalid SAI1 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SAI1_KER_SRC)
        #error Source clock for SAI1 is not enabled
    #endif

    #if CHIP_CLK_SAI1_KER_FREQ > CHIP_CLK_SAI1_KER_FREQ_MAX
        #error SAI1 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SAI1SEL  CHIP_REGFLD_RCC_D2CCIP1R_SAI1SEL
#else
    #define CHIP_RCC_RMSK_SAI1SEL 0
    #define CHIP_RVAL_RCC_SAI1SEL 0
#endif

#if CHIP_DEV_SUPPORT_SAI2_3
#if CHIP_CLK_SAI2_3_KER_SRC_EN
    #define CHIP_CLK_SAI2_3_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SAI2_3_KER_SRC)
    #if CHIP_CLK_SAI2_3_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_SAI23SEL 0
    #elif CHIP_CLK_SAI2_3_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_SAI23SEL 1
    #elif CHIP_CLK_SAI2_3_SRC_ID == CHIP_CLK_PLL3_P_ID
        #define CHIP_RVAL_RCC_SAI23SEL 2
    #elif CHIP_CLK_SAI2_3_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #define CHIP_RVAL_RCC_SAI23SEL 3
    #elif CHIP_CLK_SAI2_3_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_SAI23SEL 4
    #else
        #error Invalid SAI23 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SAI2_3_KER_SRC)
        #error Source clock for SAI2,3 is not enabled
    #endif

    #if CHIP_CLK_SAI2_3_KER_FREQ > CHIP_CLK_SAI2_3_KER_FREQ_MAX
        #error SAI23 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SAI23SEL  CHIP_REGFLD_RCC_D2CCIP1R_SAI23SEL
#else
    #define CHIP_RCC_RMSK_SAI23SEL 0
    #define CHIP_RVAL_RCC_SAI23SEL 0
#endif
#endif

#if CHIP_CLK_SPI1_2_3_KER_SRC_EN
    #define CHIP_CLK_SPI1_2_3_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SPI1_2_3_KER_SRC)
    #if CHIP_CLK_SPI1_2_3_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_SPI123SEL 0
    #elif CHIP_CLK_SPI1_2_3_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_SPI123SEL 1
    #elif CHIP_CLK_SPI1_2_3_SRC_ID == CHIP_CLK_PLL3_P_ID
        #define CHIP_RVAL_RCC_SPI123SEL 2
    #elif CHIP_CLK_SPI1_2_3_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #define CHIP_RVAL_RCC_SPI123SEL 3
    #elif CHIP_CLK_SPI1_2_3_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_SPI123SEL 4
    #else
        #error Invalid SPI123 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SPI1_2_3_KER_SRC)
        #error Source clock for SPI123 is not enabled
    #endif

    #if CHIP_CLK_SPI1_2_3_KER_FREQ > CHIP_CLK_SPI1_2_3_KER_FREQ_MAX
        #error SPI123 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SPI123SEL  CHIP_REGFLD_RCC_D2CCIP1R_SPI123SEL
#else
    #define CHIP_RCC_RMSK_SPI123SEL 0
    #define CHIP_RVAL_RCC_SPI123SEL 0
#endif


#if CHIP_CLK_SPI4_5_KER_SRC_EN
    #define CHIP_CLK_SPI4_5_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SPI4_5_KER_SRC)
    #if CHIP_CLK_SPI4_5_SRC_ID == CHIP_CLK_APB2_ID
        #define CHIP_RVAL_RCC_SPI45SEL 0
    #elif CHIP_CLK_SPI4_5_SRC_ID == CHIP_CLK_PLL2_Q_ID
        #define CHIP_RVAL_RCC_SPI45SEL 1
    #elif CHIP_CLK_SPI4_5_SRC_ID == CHIP_CLK_PLL3_Q_ID
        #define CHIP_RVAL_RCC_SPI45SEL 2
    #elif CHIP_CLK_SPI4_5_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_SPI45SEL 3
    #elif CHIP_CLK_SPI4_5_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_SPI45SEL 4
    #elif CHIP_CLK_SPI4_5_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_SPI45SEL 5
    #else
        #error Invalid SPI45 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SPI4_5_KER_SRC)
        #error Source clock for SPI45 is not enabled
    #endif

    #if CHIP_CLK_SPI4_5_KER_FREQ > CHIP_CLK_SPI4_5_KER_FREQ_MAX
        #error SPI45 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SPI45SEL  CHIP_REGFLD_RCC_D2CCIP1R_SPI45SEL
#else
    #define CHIP_RCC_RMSK_SPI45SEL 0
    #define CHIP_RVAL_RCC_SPI45SEL 0
#endif


#if CHIP_CLK_SPDIFRX_KER_SRC_EN
    #define CHIP_CLK_SPDIFRX_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SPDIFRX_KER_SRC)
    #if CHIP_CLK_SPDIFRX_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_SPDIFSEL 0
    #elif CHIP_CLK_SPDIFRX_SRC_ID == CHIP_CLK_PLL2_R_ID
        #define CHIP_RVAL_RCC_SPDIFSEL 1
    #elif CHIP_CLK_SPDIFRX_SRC_ID == CHIP_CLK_PLL3_R_ID
        #define CHIP_RVAL_RCC_SPDIFSEL 2
    #elif CHIP_CLK_SPDIFRX_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_SPDIFSEL 3
    #else
        #error Invalid SPDIFRX source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SPDIFRX_KER_SRC)
        #error Source clock for SPDIFRX is not enabled
    #endif

    #if CHIP_CLK_SPDIFRX_KER_FREQ > CHIP_CLK_SPDIFRX_KER_FREQ_MAX
        #error SPDIF kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SPDIFSEL  CHIP_REGFLD_RCC_D2CCIP1R_SPDIFSEL
#else
    #define CHIP_RCC_RMSK_SPDIFSEL 0
    #define CHIP_RVAL_RCC_SPDIFSEL 0
#endif


#if CHIP_CLK_DFSDM1_KER_SRC_EN
    #define CHIP_CLK_DFSDM1_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_DFSDM1_KER_SRC)
    #if CHIP_CLK_DFSDM1_SRC_ID == CHIP_CLK_APB2_ID
        #define CHIP_RVAL_RCC_DFSDM1SEL 0
    #elif CHIP_CLK_DFSDM1_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_DFSDM1SEL 1
    #else
        #error Invalid DFSDM1 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_DFSDM1_KER_SRC)
        #error Source clock for DFSDM1 is not enabled
    #endif

    #if CHIP_CLK_DFSDM1_KER_FREQ > CHIP_CLK_DFSDM1_KER_FREQ_MAX
        #error DFSDM1 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_DFSDM1SEL  CHIP_REGFLD_RCC_D2CCIP1R_DFSDM1SEL
#else
    #define CHIP_RCC_RMSK_DFSDM1SEL 0
    #define CHIP_RVAL_RCC_DFSDM1SEL 0
#endif


#if CHIP_CLK_FDCAN_KER_SRC_EN
    #define CHIP_CLK_FDCAN_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_FDCAN_KER_SRC)
    #if CHIP_CLK_FDCAN_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_FDCANSEL 0
    #elif CHIP_CLK_FDCAN_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_FDCANSEL 1
    #elif CHIP_CLK_FDCAN_SRC_ID == CHIP_CLK_PLL2_Q_ID
        #define CHIP_RVAL_RCC_FDCANSEL 2
    #else
        #error Invalid FDCAN source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_FDCAN_KER_SRC)
        #error Source clock for FDCAN is not enabled
    #endif

    #if CHIP_CLK_FDCAN_KER_FREQ > CHIP_CLK_FDCAN_KER_FREQ_MAX
        #error FDCAN kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_FDCANSEL  CHIP_REGFLD_RCC_D2CCIP1R_FDCANSEL
#else
    #define CHIP_RCC_RMSK_FDCANSEL 0
    #define CHIP_RVAL_RCC_FDCANSEL 0
#endif


#if CHIP_CLK_SWPMI_KER_SRC_EN
    #define CHIP_CLK_SWPMI_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SWPMI_KER_SRC)
    #if CHIP_CLK_SWPMI_SRC_ID == CHIP_CLK_APB1_ID
        #define CHIP_RVAL_RCC_SWPSEL 0
    #elif CHIP_CLK_SWPMI_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_SWPSEL 1
    #else
        #error Invalid SWP source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SWPMI_KER_SRC)
        #error Source clock for SWPMI is not enabled
    #endif

    #if CHIP_CLK_SWPMI_KER_FREQ > CHIP_CLK_SWPMI_KER_FREQ_MAX
        #error SWP kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SWPSEL  CHIP_REGFLD_RCC_D2CCIP1R_SWPSEL
#else
    #define CHIP_RCC_RMSK_SWPSEL 0
    #define CHIP_RVAL_RCC_SWPSEL 0
#endif


#if CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
    #define CHIP_CLK_USART2_3_4_5_7_8_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_USART2_3_4_5_7_8_KER_SRC)
    #if CHIP_CLK_USART2_3_4_5_7_8_SRC_ID == CHIP_CLK_APB1_ID
        #define CHIP_RVAL_RCC_USART234578SEL 0
    #elif CHIP_CLK_USART2_3_4_5_7_8_SRC_ID == CHIP_CLK_PLL2_Q_ID
        #define CHIP_RVAL_RCC_USART234578SEL 1
    #elif CHIP_CLK_USART2_3_4_5_7_8_SRC_ID == CHIP_CLK_PLL3_Q_ID
        #define CHIP_RVAL_RCC_USART234578SEL 2
    #elif CHIP_CLK_USART2_3_4_5_7_8_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_USART234578SEL 3
    #elif CHIP_CLK_USART2_3_4_5_7_8_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_USART234578SEL 4
    #elif CHIP_CLK_USART2_3_4_5_7_8_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_USART234578SEL 5
    #else
        #error Invalid USART234578 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_USART2_3_4_5_7_8_KER_SRC)
        #error Source clock for USART234578 is not enabled
    #endif

    #if CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ > CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ_MAX
        #error USART234578 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_USART234578SEL CHIP_REGFLD_RCC_D2CCIP2R_USART28SEL
#else
    #define CHIP_RCC_RMSK_USART234578SEL 0
    #define CHIP_RVAL_RCC_USART234578SEL 0
#endif


#if CHIP_CLK_USART1_6_9_10_KER_SRC_EN
    #define CHIP_CLK_USART1_6_9_10_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_USART1_6_9_10_KER_SRC)
    #if CHIP_CLK_USART1_6_9_10_KER_SRC_ID == CHIP_CLK_APB2_ID
        #define CHIP_RVAL_RCC_USART16SEL 0
    #elif CHIP_CLK_USART1_6_9_10_KER_SRC_ID == CHIP_CLK_PLL2_Q_ID
        #define CHIP_RVAL_RCC_USART16SEL 1
    #elif CHIP_CLK_USART1_6_9_10_KER_SRC_ID == CHIP_CLK_PLL3_Q_ID
        #define CHIP_RVAL_RCC_USART16SEL 2
    #elif CHIP_CLK_USART1_6_9_10_KER_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_USART16SEL 3
    #elif CHIP_CLK_USART1_6_9_10_KER_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_USART16SEL 4
    #elif CHIP_CLK_USART1_6_9_10_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_USART16SEL 5
    #else
        #error Invalid USART1/6/9/10 source clock
    #endif


    #if !CHIP_CLK_ID_EN(CHIP_CLK_USART1_6_9_10_KER_SRC)
        #error Source clock for USART16 is not enabled
    #endif

    #if CHIP_CLK_USART1_6_9_10_KER_FREQ > CHIP_CLK_USART1_6_9_10_KER_FREQ_MAX
        #error USART1,6,9,10 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_USART16SEL  CHIP_REGFLD_RCC_D2CCIP2R_USART16SEL
#else
    #define CHIP_RCC_RMSK_USART16SEL 0
    #define CHIP_RVAL_RCC_USART16SEL 0
#endif


#if CHIP_CLK_RNG_KER_SRC_EN
    #define CHIP_CLK_RNG_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_RNG_KER_SRC)
    #if CHIP_CLK_RNG_KER_SRC_ID == CHIP_CLK_HSI48_ID
        #define CHIP_RVAL_RCC_RNGSEL 0
    #elif CHIP_CLK_RNG_KER_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_RNGSEL 1
    #elif CHIP_CLK_RNG_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_RNGSEL 2
    #elif CHIP_CLK_RNG_KER_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_RNGSEL 3
    #else
        #error Invalid RNG source clock
    #endif


    #if !CHIP_CLK_ID_EN(CHIP_CLK_RNG_KER_SRC)
        #error Source clock for RNG is not enabled
    #endif

    #if CHIP_CLK_RNG_KER_FREQ > CHIP_CLK_RNG_KER_FREQ_MAX
        #error RNG kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_RNGSEL  CHIP_REGFLD_RCC_D2CCIP2R_RNGSEL
#else
    #define CHIP_RCC_RMSK_RNGSEL 0
    #define CHIP_RVAL_RCC_RNGSEL 0
#endif


#if CHIP_CLK_I2C1_2_3_5_KER_SRC_EN
    #define CHIP_CLK_I2C1_2_3_5_SRC_ID  CHIP_CLK_ID_VAL(CHIP_CLK_I2C1_2_3_5_KER_SRC)
    #if CHIP_CLK_I2C1_2_3_5_SRC_ID == CHIP_CLK_APB1_ID
        #define CHIP_RVAL_RCC_I2C123SEL 0
    #elif CHIP_CLK_I2C1_2_3_5_SRC_ID == CHIP_CLK_PLL3_R_ID
        #define CHIP_RVAL_RCC_I2C123SEL 1
    #elif CHIP_CLK_I2C1_2_3_5_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_I2C123SEL 2
    #elif CHIP_CLK_I2C1_2_3_5_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_I2C123SEL 3
    #else
        #error Invalid I2C1,2,3,5 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_I2C1_2_3_5_KER_SRC)
        #error Source clock for I2C123 is not enabled
    #endif

    #if CHIP_CLK_I2C1_2_3_5_KER_FREQ > CHIP_CLK_I2C1_2_3_5_KER_FREQ_MAX
        #error I2C123 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_I2C123SEL  CHIP_REGFLD_RCC_D2CCIP2R_I2C123SEL
#else
    #define CHIP_RCC_RMSK_I2C123SEL 0
    #define CHIP_RVAL_RCC_I2C123SEL 0
#endif

#if CHIP_CLK_USB_KER_SRC_EN
    #define CHIP_CLK_USB_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_USB_KER_SRC)
    #if CHIP_CLK_USB_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_USBSEL 1
    #elif CHIP_CLK_USB_SRC_ID == CHIP_CLK_PLL3_Q_ID
        #define CHIP_RVAL_RCC_USBSEL 2
    #elif CHIP_CLK_USB_SRC_ID == CHIP_CLK_HSI48_ID
        #define CHIP_RVAL_RCC_USBSEL 3
    #else
        #error Invalid USB source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_USB_KER_SRC)
        #error Source clock for USB is not enabled
    #endif

    #if CHIP_CLK_USB_KER_FREQ > CHIP_CLK_USBOTG_KER_FREQ_MAX
        #error USB kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_USBSEL  CHIP_REGFLD_RCC_D2CCIP2R_USBSEL
#else
    #define CHIP_RCC_RMSK_USBSEL 0
    #define CHIP_RVAL_RCC_USBSEL 0
#endif


#if CHIP_CLK_CEC_KER_SRC_EN
    #define CHIP_CLK_CEC_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_CEC_KER_SRC)
    #if CHIP_CLK_CEC_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CECSEL 0
    #elif CHIP_CLK_CEC_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_CECSEL 1
    #elif CHIP_CLK_CEC_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_CECSEL 2
    #else
        #error Invalid CEC source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_CEC_KER_SRC)
        #error Source clock for CEC is not enabled
    #endif

    #if CHIP_CLK_CEC_KER_FREQ > CHIP_CLK_CEC_KER_FREQ_MAX
        #error CEC kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_CECSEL  CHIP_REGFLD_RCC_D2CCIP2R_CECSEL
#else
    #define CHIP_RCC_RMSK_CECSEL 0
    #define CHIP_RVAL_RCC_CECSEL 0
#endif


#if CHIP_CLK_LPTIM1_KER_SRC_EN
    #define CHIP_CLK_LPTIM1_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_LPTIM1_KER_SRC)
    #if CHIP_CLK_LPTIM1_SRC_ID == CHIP_CLK_APB1_ID
        #define CHIP_RVAL_RCC_LPTIM1SEL 0
    #elif CHIP_CLK_LPTIM1_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_LPTIM1SEL 1
    #elif CHIP_CLK_LPTIM1_SRC_ID == CHIP_CLK_PLL3_R_ID
        #define CHIP_RVAL_RCC_LPTIM1SEL 2
    #elif CHIP_CLK_LPTIM1_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_LPTIM1SEL 3
    #elif CHIP_CLK_LPTIM1_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_LPTIM1SEL 4
    #elif CHIP_CLK_LPTIM1_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_LPTIM1SEL 5
    #else
        #error Invalid LPTIM1 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_LPTIM1_KER_SRC)
        #error Source clock for LPTIM1 is not enabled
    #endif

    #if CHIP_CLK_LPTIM1_KER_FREQ > CHIP_CLK_LPTIM1_KER_FREQ_MAX
        #error LPTIM1 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_LPTIM1SEL  CHIP_REGFLD_RCC_D2CCIP2R_LPTIM1SEL
#else
    #define CHIP_RCC_RMSK_LPTIM1SEL 0
    #define CHIP_RVAL_RCC_LPTIM1SEL 0
#endif


#if CHIP_CLK_LPUART1_KER_SRC_EN
    #define CHIP_CLK_LPUART1_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_LPUART1_KER_SRC)
    #if CHIP_CLK_LPUART1_SRC_ID == CHIP_CLK_APB4_ID
        #define CHIP_RVAL_RCC_LPUART1SEL 0
    #elif CHIP_CLK_LPUART1_SRC_ID == CHIP_CLK_PLL2_Q_ID
        #define CHIP_RVAL_RCC_LPUART1SEL 1
    #elif CHIP_CLK_LPUART1_SRC_ID == CHIP_CLK_PLL3_Q_ID
        #define CHIP_RVAL_RCC_LPUART1SEL 2
    #elif CHIP_CLK_LPUART1_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_LPUART1SEL 3
    #elif CHIP_CLK_LPUART1_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_LPUART1SEL 4
    #elif CHIP_CLK_LPUART1_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_LPUART1SEL 5
    #else
        #error Invalid LPUART1 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_LPUART1_KER_SRC)
        #error Source clock for LPUART1 is not enabled
    #endif

    #if CHIP_CLK_LPUART1_KER_FREQ > CHIP_CLK_LPUART1_KER_FREQ_MAX
        #error LPUART1 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_LPUART1SEL  CHIP_REGFLD_D3CCIPR_LPUART1SEL
#else
    #define CHIP_RCC_RMSK_LPUART1SEL 0
    #define CHIP_RVAL_RCC_LPUART1SEL 0
#endif


#if CHIP_CLK_I2C4_KER_SRC_EN
    #define CHIP_CLK_I2C4_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_I2C4_KER_SRC)
    #if CHIP_CLK_I2C4_SRC_ID == CHIP_CLK_APB4_ID
        #define CHIP_RVAL_RCC_I2C4SEL 0
    #elif CHIP_CLK_I2C4_SRC_ID == CHIP_CLK_PLL3_R_ID
        #define CHIP_RVAL_RCC_I2C4SEL 1
    #elif CHIP_CLK_I2C4_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_I2C4SEL 2
    #elif CHIP_CLK_I2C4_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_I2C4SEL 3
    #else
        #error Invalid I2C4 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_I2C4_KER_SRC)
        #error Source clock for I2C4 is not enabled
    #endif

    #if CHIP_CLK_I2C4_KER_FREQ > CHIP_CLK_I2C4_KER_FREQ_MAX
        #error I2C4 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_I2C4SEL  CHIP_REGFLD_RCC_D3CCIPR_I2C4SEL
#else
    #define CHIP_RCC_RMSK_I2C4SEL 0
    #define CHIP_RVAL_RCC_I2C4SEL 0
#endif


#if CHIP_CLK_LPTIM2_KER_SRC_EN
    #define CHIP_CLK_LPTIM2_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_LPTIM2_KER_SRC)
    #if CHIP_CLK_LPTIM2_SRC_ID == CHIP_CLK_APB4_ID
        #define CHIP_RVAL_RCC_LPTIM2SEL 0
    #elif CHIP_CLK_LPTIM2_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_LPTIM2SEL 1
    #elif CHIP_CLK_LPTIM2_SRC_ID == CHIP_CLK_PLL3_R_ID
        #define CHIP_RVAL_RCC_LPTIM2SEL 2
    #elif CHIP_CLK_LPTIM2_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_LPTIM2SEL 3
    #elif CHIP_CLK_LPTIM2_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_LPTIM2SEL 4
    #elif CHIP_CLK_LPTIM2_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_LPTIM2SEL 5
    #else
        #error Invalid LPTIM2 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_LPTIM2_KER_SRC)
        #error Source clock for LPTIM2 is not enabled
    #endif

    #if CHIP_CLK_LPTIM2_KER_FREQ > CHIP_CLK_LPTIM2_KER_FREQ_MAX
        #error LPTIM2 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_LPTIM2SEL  CHIP_REGFLD_RCC_D3CCIPR_LPTIM2SEL
#else
    #define CHIP_RCC_RMSK_LPTIM2SEL 0
    #define CHIP_RVAL_RCC_LPTIM2SEL 0
#endif

#if CHIP_CLK_LPTIM3_4_5_KER_SRC_EN
    #define CHIP_CLK_LPTIM3_4_5_SRC_ID  CHIP_CLK_ID_VAL(CHIP_CLK_LPTIM3_4_5_KER_SRC)
    #if CHIP_CLK_LPTIM3_4_5_SRC_ID == CHIP_CLK_APB4_ID
        #define CHIP_RVAL_RCC_LPTIM345SEL 0
    #elif CHIP_CLK_LPTIM3_4_5_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_LPTIM345SEL 1
    #elif CHIP_CLK_LPTIM3_4_5_SRC_ID == CHIP_CLK_PLL3_R_ID
        #define CHIP_RVAL_RCC_LPTIM345SEL 2
    #elif CHIP_CLK_LPTIM3_4_5_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_LPTIM345SEL 3
    #elif CHIP_CLK_LPTIM3_4_5_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_LPTIM345SEL 4
    #elif CHIP_CLK_LPTIM3_4_5_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_LPTIM345SEL 5
    #else
        #error Invalid LPTIM345 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_LPTIM3_4_5_KER_SRC)
        #error Source clock for LPTIM345 is not enabled
    #endif

    #if CHIP_CLK_LPTIM3_4_5_KER_FREQ > CHIP_CLK_LPTIM3_4_5_KER_FREQ_MAX
        #error LPTIM345 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_LPTIM345SEL  CHIP_REGFLD_RCC_D3CCIPR_LPTIM345SEL
#else
    #define CHIP_RCC_RMSK_LPTIM345SEL 0
    #define CHIP_RVAL_RCC_LPTIM345SEL 0
#endif


#if CHIP_CLK_ADC_KER_SRC_EN
    #define CHIP_CLK_ADC_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_ADC_KER_SRC)
    #if CHIP_CLK_ADC_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_ADCSEL 0
    #elif CHIP_CLK_ADC_SRC_ID == CHIP_CLK_PLL3_R_ID
        #define CHIP_RVAL_RCC_ADCSEL 1
    #elif CHIP_CLK_ADC_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_ADCSEL 2
    #else
        #error Invalid ADC source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_ADC_KER_SRC)
        #error Source clock for ADC is not enabled
    #endif

    #if CHIP_CLK_ADC_KER_FREQ > CHIP_CLK_ADC_KER_FREQ_MAX
        #error ADC kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_ADCSEL  CHIP_REGFLD_RCC_D3CCIPR_ADCSEL
#else
    #define CHIP_RCC_RMSK_ADCSEL 0
    #define CHIP_RVAL_RCC_ADCSEL 0
#endif


#if CHIP_CLK_SAI4A_KER_SRC_EN
    #define CHIP_CLK_SAI4A_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SAI4A_KER_SRC)
    #if CHIP_CLK_SAI4A_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_SAI4ASEL 0
    #elif CHIP_CLK_SAI4A_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_SAI4ASEL 1
    #elif CHIP_CLK_SAI4A_SRC_ID == CHIP_CLK_PLL3_P_ID
        #define CHIP_RVAL_RCC_SAI4ASEL 2
    #elif CHIP_CLK_SAI4A_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #define CHIP_RVAL_RCC_SAI4ASEL 3
    #elif CHIP_CLK_SAI4A_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_SAI4ASEL 4
    #elif CHIP_CLK_SAI4A_SRC_ID == CHIP_CLK_SPDIFRX_SYM_ID
        #if CHIP_DEV_SUPPORT_SPDIFRX_SYM_FREQGEN
            #define CHIP_RVAL_RCC_SAI4ASEL 5
        #else
            #error spdifrx_sym_ck geneartion is not supported by selected device
        #endif
    #else
        #error Invalid SAI4A source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SAI4A_KER_SRC)
        #error Source clock for SAI4A is not enabled
    #endif

    #if CHIP_CLK_SAI4A_KER_FREQ > CHIP_CLK_SAI4A_KER_FREQ_MAX
        #error SAI4A kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SAI4ASEL  CHIP_REGFLD_RCC_D3CCIPR_SAI4ASEL
#else
    #define CHIP_RCC_RMSK_SAI4ASEL 0
    #define CHIP_RVAL_RCC_SAI4ASEL 0
#endif

#if CHIP_CLK_SAI4B_KER_SRC_EN
    #define CHIP_CLK_SAI4B_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SAI4B_KER_SRC)
    #if CHIP_CLK_SAI4B_SRC_ID == CHIP_CLK_PLL1_Q_ID
        #define CHIP_RVAL_RCC_SAI4BSEL 0
    #elif CHIP_CLK_SAI4B_SRC_ID == CHIP_CLK_PLL2_P_ID
        #define CHIP_RVAL_RCC_SAI4BSEL 1
    #elif CHIP_CLK_SAI4B_SRC_ID == CHIP_CLK_PLL3_P_ID
        #define CHIP_RVAL_RCC_SAI4BSEL 2
    #elif CHIP_CLK_SAI4B_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #define CHIP_RVAL_RCC_SAI4BSEL 3
    #elif CHIP_CLK_SAI4B_SRC_ID == CHIP_CLK_PER_ID
        #define CHIP_RVAL_RCC_SAI4BSEL 4
    #elif CHIP_CLK_SAI4B_SRC_ID == CHIP_CLK_SPDIFRX_SYM_ID
        #if CHIP_DEV_SUPPORT_SPDIFRX_SYM_FREQGEN
            #define CHIP_RVAL_RCC_SAI4BSEL 5
        #else
            #error spdifrx_sym_ck geneartion is not supported by selected device
        #endif
    #else
        #error Invalid SAI4B source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SAI4B_KER_SRC)
        #error Source clock for SAI4B is not enabled
    #endif

    #if CHIP_CLK_SAI4B_KER_FREQ > CHIP_CLK_SAI4B_KER_FREQ_MAX
        #error SAI4B kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SAI4BSEL  CHIP_REGFLD_RCC_D3CCIPR_SAI4BSEL
#else
    #define CHIP_RCC_RMSK_SAI4BSEL 0
    #define CHIP_RVAL_RCC_SAI4BSEL 0
#endif

#if CHIP_CLK_SPI6_KER_SRC_EN
    #define CHIP_CLK_SPI6_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SPI6_KER_SRC)
    #if CHIP_CLK_SPI6_SRC_ID == CHIP_CLK_APB4_ID
        #define CHIP_RVAL_RCC_SPI6SEL 0
    #elif CHIP_CLK_SPI6_SRC_ID == CHIP_CLK_PLL2_Q_ID
        #define CHIP_RVAL_RCC_SPI6SEL 1
    #elif CHIP_CLK_SPI6_SRC_ID == CHIP_CLK_PLL3_Q_ID
        #define CHIP_RVAL_RCC_SPI6SEL 2
    #elif CHIP_CLK_SPI6_SRC_ID == CHIP_CLK_HSI_KER_ID
        #define CHIP_RVAL_RCC_SPI6SEL 3
    #elif CHIP_CLK_SPI6_SRC_ID == CHIP_CLK_CSI_KER_ID
        #define CHIP_RVAL_RCC_SPI6SEL 4
    #elif CHIP_CLK_SPI6_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_SPI6SEL 5
    #elif CHIP_CLK_SPI6_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #if CHIP_DEV_SUPPORT_I2S6
            #define CHIP_RVAL_RCC_SPI6SEL 6
        #else
            #error I2S CLKIN is not supported as SPI6 src clock for this chip
        #endif
    #else
        #error Invalid SPI6 source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_SPI6_KER_SRC)
        #error Source clock for SPI6 is not enabled
    #endif

    #if CHIP_CLK_SPI6_KER_FREQ > CHIP_CLK_SPI6_KER_FREQ_MAX
        #error SPI6 kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_SPI6SEL  CHIP_REGFLD_RCC_D3CCIPR_SPI6SEL
#else
    #define CHIP_RCC_RMSK_SPI6SEL 0
    #define CHIP_RVAL_RCC_SPI6SEL 0
#endif



#if CHIP_CLK_DSI_KER_SRC_EN
    #define CHIP_CLK_DSI_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_DSI_KER_SRC)
    #if CHIP_CLK_DSI_SRC_ID == CHIP_CLK_DSI_PHY_ID
        #define CHIP_RVAL_RCC_DSISEL 0
    #elif CHIP_CLK_DSI_SRC_ID == CHIP_CLK_PLL2_Q_ID
        #define CHIP_RVAL_RCC_DSISEL 1
    #else
        #error Invalid DSI source clock
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CLK_DSI_KER_SRC)
        #error Source clock for DSI is not enabled
    #endif

    #if CHIP_CLK_DSI_KER_FREQ > CHIP_CLK_DSI_KER_FREQ_MAX
        #error DSI kernel frequency is out of range
    #endif

    #define CHIP_RCC_RMSK_DSISEL  CHIP_REGFLD_RCC_D3CCIPR_DSISEL
#else
    #define CHIP_RCC_RMSK_DSISEL 0
    #define CHIP_RVAL_RCC_DSISEL 0
#endif


/* признак, разрешен ли хоть один модуль из домена D2 */
#define CHIP_D2_CLK_EN        (CHIP_CLK_DMA1_EN \
                              || CHIP_CLK_DMA2_EN \
                              || CHIP_CLK_ADC1_2_EN \
                              || CHIP_CLK_ART_EN \
                              || CHIP_CLK_ETH1_MAC_EN \
                              || CHIP_CLK_ETH1_TX_EN \
                              || CHIP_CLK_ETH1_RX_EN \
                              || CHIP_CLK_USB1_OTGHS_EN \
                              || CHIP_CLK_USB1_OTGHS_ULPI_EN \
                              || CHIP_CLK_USB2_OTGHS_EN \
                              || CHIP_CLK_USB2_OTGHS_ULPI_EN \
                              || CHIP_CLK_CAMITF_EN \
                              || CHIP_CLK_CRYPT_EN \
                              || CHIP_CLK_HASH_EN \
                              || CHIP_CLK_RNG_EN \
                              || CHIP_CLK_SDMMC2_EN \
                              || CHIP_CLK_FMAC_EN \
                              || CHIP_CLK_CORDIC_EN \
                              || CHIP_CLK_SRAM1_EN \
                              || CHIP_CLK_SRAM2_EN \
                              || CHIP_CLK_SRAM3_EN \
                              || CHIP_CLK_TIM2_EN \
                              || CHIP_CLK_TIM3_EN \
                              || CHIP_CLK_TIM4_EN \
                              || CHIP_CLK_TIM5_EN \
                              || CHIP_CLK_TIM6_EN \
                              || CHIP_CLK_TIM7_EN \
                              || CHIP_CLK_TIM12_EN \
                              || CHIP_CLK_TIM13_EN \
                              || CHIP_CLK_TIM14_EN \
                              || CHIP_CLK_LPTIM1_EN \
                              || CHIP_CLK_SPI2_EN \
                              || CHIP_CLK_SPI3_EN \
                              || CHIP_CLK_SPDIFRX_EN \
                              || CHIP_CLK_USART2_EN \
                              || CHIP_CLK_USART3_EN \
                              || CHIP_CLK_UART4_EN \
                              || CHIP_CLK_UART5_EN \
                              || CHIP_CLK_I2C1_EN \
                              || CHIP_CLK_I2C2_EN \
                              || CHIP_CLK_I2C3_EN \
                              || CHIP_CLK_I2C5_EN \
                              || CHIP_CLK_CEC_EN \
                              || CHIP_CLK_DAC12_EN \
                              || CHIP_CLK_UART7_EN \
                              || CHIP_CLK_UART8_EN \
                              || CHIP_CLK_CRS_EN \
                              || CHIP_CLK_SWPMI_EN \
                              || CHIP_CLK_OPAMP_EN \
                              || CHIP_CLK_MDIOS_EN \
                              || CHIP_CLK_FDCAN_EN \
                              || CHIP_CLK_TIM23_EN \
                              || CHIP_CLK_TIM23_EN \
                              || CHIP_CLK_TIM1_EN  \
                              || CHIP_CLK_TIM8_EN \
                              || CHIP_CLK_USART1_EN \
                              || CHIP_CLK_USART6_EN \
                              || CHIP_CLK_UART9_EN \
                              || CHIP_CLK_USART10_EN \
                              || CHIP_CLK_SPI1_EN \
                              || CHIP_CLK_SPI4_EN \
                              || CHIP_CLK_TIM15_EN \
                              || CHIP_CLK_TIM16_EN \
                              || CHIP_CLK_TIM17_EN \
                              || CHIP_CLK_SPI5_EN \
                              || CHIP_CLK_SAI1_EN \
                              || CHIP_CLK_SAI2_EN \
                              || CHIP_CLK_SAI3_EN \
                              || CHIP_CLK_DFSDM1_EN \
                              || CHIP_CLK_HRTIM_EN)

/* флаг ожидания домена D2 устанавливаем равным 1 или 0 в зависимости от
 * того, разрешен ли клок в хотя бы одном модуле D2 */
#if CHIP_D2_CLK_EN
    #define CHIP_RVAL_RCC_D2CKRDY CHIP_REGFLD_RCC_CR_D2CKRDY
#elif
    #define CHIP_RVAL_RCC_D2CKRDY 0
#endif



void chip_clk_init(void) {
#if !CHIP_SHARED_NO_INIT
    /* включение LSI либо если разрешен явно, либо если требуется настройка IWDG.
     * Т.к. IWDG следует разрешить как много раньше, включаем этот клок до
     * настройки остального */
#if CHIP_CLK_LSI_EN
    CHIP_REGS_RCC->CSR |= CHIP_REGFLD_RCC_CSR_LSION;
#endif

#if CHIP_WDT_SETUP_EN
    CHIP_INIT_WAIT((CHIP_REGS_RCC->CSR & CHIP_REGFLD_RCC_CSR_LSIRDY) == CHIP_REGFLD_RCC_CSR_LSIRDY,
                       CHIP_INIT_FAULT_WAIT_LSI_RDY, 10000);
    chip_wdt_init();
#endif
#if CHIP_FLASH_FUNC_EN
    chip_flash_init();
#endif

    /* Если процессор работает не от HSI, то переходим в режим по умолчанию:
     * включаем HSI и ждем готовности, переводим процессор на работу от него */
    if (LBITFIELD_GET(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_SWS) != CHIP_REGFLDVAL_RCC_CFGR_SW_HSI) {
        LBITFIELD_UPD(CHIP_REGS_RCC->CR, CHIP_REGFLD_RCC_CR_HSIDIV, CHIP_RVAL_RCC_CR_HSIDIV);
        CHIP_REGS_RCC->CR |= CHIP_REGFLD_RCC_CR_HSION;

        CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_REGFLD_RCC_CR_HSIRDY) == CHIP_REGFLD_RCC_CR_HSIRDY,
                           CHIP_INIT_FAULT_WAIT_HSI_RDY, 10000);


        LBITFIELD_UPD(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_SW, CHIP_REGFLDVAL_RCC_CFGR_SW_HSI);
    }




    /* предполагаем, что результирующая частота AXI всегда выше чатоты по умолчанию,
     * поэтому можем изменить настройки задержек до. Иначе следовало бы
     * сделать изменение только после */
    chip_flash_init_freq();


    /* включение базовых источников частоты.
     * HSI оставляем включенным независимо от настроек, т.к. от него
     * изначально работает CPU. Выключить можно будет только в конце */
    CHIP_REGS_RCC->CR = (CHIP_REGS_RCC->CR & 0xC0F00C40)
        | CHIP_REGFLD_RCC_CR_HSION
        | LBITFIELD_SET(CHIP_REGFLD_RCC_CR_HSIDIV, CHIP_RVAL_RCC_CR_HSIDIV)
        #if CHIP_CFG_CLK_HSI_ON_STOP_EN
            | CHIP_REGFLD_RCC_CR_HSIKERON
        #endif

        #if CHIP_CFG_CLK_CSI_EN
            | CHIP_REGFLD_RCC_CR_CSION
            #if CHIP_CFG_CLK_CSI_ON_STOP_EN
                | CHIP_REGFLD_RCC_CR_CSIKERON
            #endif
        #endif
        #if CHIP_CFG_CLK_HSI48_EN
            | CHIP_REGFLD_RCC_CR_HSI48ON
        #endif
        #if CHIP_CLK_HSE_EN
            | CHIP_REGFLD_RCC_CR_HSEON
            #if CHIP_CFG_CLK_HSE_MODE == CHIP_CLK_EXTMODE_CLOCK
                | CHIP_REGFLD_RCC_CR_HSEBYP
            #endif
            #if CHIP_CFG_CLK_HSE_CSS_EN
                | CHIP_REGFLD_RCC_CR_HSECSSON
            #endif
        #endif
            ;

    /* настройки Backup Domain (LSE & RTC) */
#if CHIP_CLK_BD_CFG_SETUP_EN
    #if CHIP_CFG_CLK_RTC_SETUP_EN
        LBITFIELD_UPD(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_RTCPRE, CHIP_RVAL_RCC_RTCPRE);
    #endif

    const uint32_t bd_msk = 0
        #if CHIP_CFG_CLK_RTC_SETUP_EN
            | CHIP_REGFLD_RCC_BDCR_RTCEN
            | CHIP_REGFLD_RCC_BDCR_RTCSEL
        #endif
        #if CHIP_CFG_CLK_LSE_SETUP_EN
            | CHIP_REGFLD_RCC_BDCR_LSEON
            | CHIP_REGFLD_RCC_BDCR_LSEBYP
            | CHIP_REGFLD_RCC_BDCR_LSECSSON
            | CHIP_REGFLD_RCC_BDCR_LSEDRV
        #endif
            ;

    const uint32_t bd_cfg = 0
        #if CHIP_CFG_CLK_RTC_SETUP_EN
            #ifdef CHIP_CFG_CLK_RTC_EN
                | CHIP_REGFLD_RCC_BDCR_RTCEN
            #endif
                | LBITFIELD_SET(CHIP_REGFLD_RCC_BDCR_RTCSEL, CHIP_RVAL_RCC_RTCSEL)
        #endif
        #if CHIP_CFG_CLK_LSE_SETUP_EN
            #if CHIP_CLK_LSE_EN
                | CHIP_REGFLD_RCC_BDCR_LSEON
                #if CHIP_CFG_CLK_LSE_MODE == CHIP_CLK_EXTMODE_CLOCK
                    | CHIP_REGFLD_RCC_BDCR_LSEBYP
                #endif
                #if CHIP_CFG_CLK_LSE_CSS_EN
                    | CHIP_REGFLD_RCC_BDCR_LSECSSON
                #endif
            #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_BDCR_LSEDRV, CHIP_RVAL_RCC_BDCR_LSEDRV)
        #endif
            ;

    const uint32_t pwr_cr2_msk = 0
        #if CHIP_CFG_PWR_BACKUP_REG_SETUP_EN
            | CHIP_REGFLD_PWR_CR2_BREN
        #endif
        #if CHIP_CFG_PWR_MON_SETUP_EN
            | CHIP_REGFLD_PWR_CR2_MONEN
        #endif
            ;

    const uint32_t pwr_cr2_val = 0
        #if CHIP_CFG_PWR_BACKUP_REG_EN
            | CHIP_REGFLD_PWR_CR2_BREN
        #endif
        #if CHIP_CFG_PWR_MON_EN
            | CHIP_REGFLD_PWR_CR2_MONEN
        #endif
            ;

    if (((CHIP_REGS_RCC->BDCR & bd_msk) != bd_cfg) ||
        ((CHIP_REGS_PWR->CR2 & pwr_cr2_msk) != pwr_cr2_val)) {

        chip_pwr_bd_cfg_write_enable();
        CHIP_REGS_RCC->BDCR = (CHIP_REGS_RCC->BDCR & ~bd_msk) | bd_cfg;
        CHIP_REGS_PWR->CR2 = (CHIP_REGS_PWR->CR2 & ~pwr_cr2_msk) | pwr_cr2_val;
        chip_pwr_bd_cfg_write_disable();
    }
#endif
    /* ожидание установки всех базовых клоков */
    if (CHIP_RCC_CR_CLK_WTRDY_MSK != 0)  {
        CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_RCC_CR_CLK_WTRDY_MSK) == CHIP_RCC_CR_CLK_WTRDY_MSK,
                           CHIP_INIT_FAULT_WAIT_BASECLK_RDY, 10000);
    }
#if CHIP_CFG_CLK_LSE_SETUP_EN && CHIP_CLK_LSE_EN
#if CHIP_CFG_CLK_LSE_RDY_TOUT_MS >= 0
    int cntr = 0;
    while ((CHIP_REGS_RCC->BDCR & CHIP_REGFLD_RCC_BDCR_LSERDY) != CHIP_REGFLD_RCC_BDCR_LSERDY) {

        if (cntr >= CHIP_CFG_CLK_LSE_RDY_TOUT_MS)
            break;
        chip_delay_clk(CHIP_CLK_HSI_FREQ/1000);
        cntr++;
    }
#else
    CHIP_INIT_WAIT((CHIP_REGS_RCC->BDCR & CHIP_REGFLD_RCC_BDCR_LSERDY) == CHIP_REGFLD_RCC_BDCR_LSERDY,
                       CHIP_INIT_FAULT_WAIT_LSE_RDY, 10000);
#endif
#endif

    /* ------------------- настройка PLL -------------------------*/
    /* 1. выбор источника клока */
    CHIP_REGS_RCC->PLLCKSELR = (CHIP_REGS_RCC->PLLCKSELR & 0xFC0C0C0C)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCKSELR_PLLSRC, CHIP_RVAL_RCC_PLLSRC);

    /* 2. Настройка предделителей */
    CHIP_REGS_RCC->PLLCKSELR = (CHIP_REGS_RCC->PLLCKSELR & 0xFC0C0C0C)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCKSELR_PLLSRC, CHIP_RVAL_RCC_PLLSRC)
        #if CHIP_CFG_CLK_PLL1_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCKSELR_DIVM1, CHIP_CFG_CLK_PLL1_DIVM)
        #endif
        #if CHIP_CFG_CLK_PLL2_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCKSELR_DIVM2, CHIP_CFG_CLK_PLL2_DIVM)
        #endif
        #if CHIP_CFG_CLK_PLL3_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCKSELR_DIVM3, CHIP_CFG_CLK_PLL3_DIVM)
        #endif
            ;

    /* 3. Настройка дробного делителя, если резрешен */
#if CHIP_CFG_CLK_PLL1_EN && CHIP_CFG_CLK_PLL1_FRACN > 0
    CHIP_REGS_RCC->PLL1FRACR = (CHIP_REGS_RCC->PLL1FRACR & 0xFFFF0007) |
            LBITFIELD_SET(CHIP_REGFLD_RCC_PLL1FRACR_FRACN1, CHIP_CFG_CLK_PLL1_FRACN);
#endif
#if CHIP_CFG_CLK_PLL2_EN && CHIP_CFG_CLK_PLL2_FRACN > 0
    CHIP_REGS_RCC->PLL2FRACR = (CHIP_REGS_RCC->PLL2FRACR & 0xFFFF0007) |
            LBITFIELD_SET(CHIP_REGFLD_RCC_PLL2FRACR_FRACN2, CHIP_CFG_CLK_PLL2_FRACN);
#endif
#if CHIP_CFG_CLK_PLL3_EN && CHIP_CFG_CLK_PLL3_FRACN > 0
    CHIP_REGS_RCC->PLL3FRACR = (CHIP_REGS_RCC->PLL3FRACR & 0xFFFF0007) |
            LBITFIELD_SET(CHIP_REGFLD_RCC_PLL3FRACR_FRACN3, CHIP_CFG_CLK_PLL3_FRACN);
#endif


    /* 4. Настройка режима PLL */
    CHIP_REGS_RCC->PLLCFGR = (CHIP_REGS_RCC->PLLCFGR & 0xFE00F000)
        #if CHIP_CFG_CLK_PLL1_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLL1VCOSEL, CHIP_RVAL_RCC_PLL1_VCOSEL)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLL1RGE, CHIP_RVAL_RCC_PLL1_RGE)
            #if CHIP_CFG_CLK_PLL1_FRACN > 0
                | CHIP_REGFLD_RCC_PLLCFGR_PLL1FRACEN
            #endif
            #if CHIP_CFG_CLK_PLL1_DIVP_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVP1EN
            #endif
            #if CHIP_CFG_CLK_PLL1_DIVQ_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVQ1EN
            #endif
            #if CHIP_CFG_CLK_PLL1_DIVR_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVR1EN
            #endif
        #endif
        #if CHIP_CFG_CLK_PLL2_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLL2VCOSEL, CHIP_RVAL_RCC_PLL2_VCOSEL)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLL2RGE, CHIP_RVAL_RCC_PLL2_RGE)
            #if CHIP_CFG_CLK_PLL2_FRACN > 0
                | CHIP_REGFLD_RCC_PLLCFGR_PLL2FRACEN
            #endif
            #if CHIP_CFG_CLK_PLL2_DIVP_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVP2EN
            #endif
            #if CHIP_CFG_CLK_PLL2_DIVQ_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVQ2EN
            #endif
            #if CHIP_CFG_CLK_PLL2_DIVR_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVR2EN
            #endif
        #endif
        #if CHIP_CFG_CLK_PLL3_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLL3VCOSEL, CHIP_RVAL_RCC_PLL3_VCOSEL)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLL3RGE, CHIP_RVAL_RCC_PLL3_RGE)
            #if CHIP_CFG_CLK_PLL3_FRACN > 0
                | CHIP_REGFLD_RCC_PLLCFGR_PLL3FRACEN
            #endif
            #if CHIP_CFG_CLK_PLL3_DIVP_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVP3EN
            #endif
            #if CHIP_CFG_CLK_PLL3_DIVQ_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVQ3EN
            #endif
            #if CHIP_CFG_CLK_PLL3_DIVR_EN
                | CHIP_REGFLD_RCC_PLLCFGR_DIVR3EN
            #endif
        #endif
            ;

    /* 5. Настройка делителей PLL */
#if CHIP_CFG_CLK_PLL1_EN
    CHIP_REGS_RCC->PLL1DIVR = (CHIP_REGS_RCC->PLL1DIVR & 0x80800000)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL1DIVR_N1, CHIP_CFG_CLK_PLL1_DIVN-1)
        #if CHIP_CFG_CLK_PLL1_DIVP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL1DIVR_P1, CHIP_CFG_CLK_PLL1_DIVP-1)
        #endif
        #if CHIP_CFG_CLK_PLL1_DIVQ_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL1DIVR_Q1, CHIP_CFG_CLK_PLL1_DIVQ-1)
        #endif
        #if CHIP_CFG_CLK_PLL1_DIVR_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL1DIVR_R1, CHIP_CFG_CLK_PLL1_DIVR-1)
        #endif
            ;
#endif
#if CHIP_CFG_CLK_PLL2_EN
    CHIP_REGS_RCC->PLL2DIVR = (CHIP_REGS_RCC->PLL2DIVR & 0x80800000)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL2DIVR_N2, CHIP_CFG_CLK_PLL2_DIVN-1)
        #if CHIP_CFG_CLK_PLL2_DIVP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL2DIVR_P2, CHIP_CFG_CLK_PLL2_DIVP-1)
        #endif
        #if CHIP_CFG_CLK_PLL2_DIVQ_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL2DIVR_Q2, CHIP_CFG_CLK_PLL2_DIVQ-1)
        #endif
        #if CHIP_CFG_CLK_PLL2_DIVR_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL2DIVR_R2, CHIP_CFG_CLK_PLL2_DIVR-1)
        #endif
            ;
#endif
#if CHIP_CFG_CLK_PLL3_EN
    CHIP_REGS_RCC->PLL3DIVR = (CHIP_REGS_RCC->PLL3DIVR & 0x80800000)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL3DIVR_N3, CHIP_CFG_CLK_PLL3_DIVN-1)
        #if CHIP_CFG_CLK_PLL3_DIVP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL3DIVR_P3, CHIP_CFG_CLK_PLL3_DIVP-1)
        #endif
        #if CHIP_CFG_CLK_PLL3_DIVQ_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL3DIVR_Q3, CHIP_CFG_CLK_PLL3_DIVQ-1)
        #endif
        #if CHIP_CFG_CLK_PLL3_DIVR_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLL3DIVR_R3, CHIP_CFG_CLK_PLL3_DIVR-1)
        #endif
            ;
#endif

    /* 6. Ожидание готовности PLL */
#if CHIP_CLK_PLL_EN
    CHIP_REGS_RCC->CR |= CHIP_RCC_PLL_ON_MSK;
    CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_RCC_PLL_RDY_MSK) == CHIP_RCC_PLL_RDY_MSK,
                       CHIP_INIT_FAULT_WAIT_PLL_RDY, 100000);
#endif


    /* устанавливаем делители до установки CLK_SYS, чтобы временно не выйти
     * за допустимые пределы (т.к. установка делителей может только уменьшить
     * частоту, делать заранее безопасно) */
    CHIP_REGS_RCC->D1CFGR = (CHIP_REGS_RCC->D1CFGR & 0xFFFFF080)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_D1CFGR_D1CPRE, CHIP_RVAL_RCC_D1CFGR_D1CPRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_D1CFGR_HPRE, CHIP_RVAL_RCC_D1CFGR_HPRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_D1CFGR_D1PPRE, CHIP_RVAL_RCC_D1CFGR_D1PPRE)
            ;

    CHIP_REGS_RCC->D2CFGR = (CHIP_REGS_RCC->D2CFGR & 0xFFFFF880)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_D2CFGR_D2PPRE1, CHIP_RVAL_RCC_D2CFGR_D2PPRE1)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_D2CFGR_D2PPRE2, CHIP_RVAL_RCC_D2CFGR_D2PPRE2)
            ;

    CHIP_REGS_RCC->D3CFGR = (CHIP_REGS_RCC->D3CFGR & 0xFFFFFF80)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_D3CFGR_D3PPRE, CHIP_RVAL_RCC_D3CFGR_D3PPRE)
            ;

    /* CHIP_REGFLD_RCC_CFGR_RTCPRE обновляется ранее при настройке клоков RTC */
    CHIP_REGS_RCC->CFGR = (CHIP_REGS_RCC->CFGR & (0x00030000 | CHIP_REGFLD_RCC_CFGR_RTCPRE))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_SW, CHIP_RVAL_RCC_CFGR_SW)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_STOPWUCK, CHIP_RVAL_RCC_STOPWUCK)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_STOPKERWUCK, CHIP_RVAL_RCC_STOPKERWUCK)
        #if CHIP_DEV_SUPPORT_HRTIM
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_HRTIMSEL, CHIP_RVAL_RCC_HRTIMSEL)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_TIMPRE, CHIP_RVAL_RCC_TIM_PRE)
        #if CHIP_CFG_CLK_MCO1_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO1PRE, CHIP_RVAL_RCC_MCO1PRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO1SEL, CHIP_RVAL_RCC_MCO1)
        #endif
        #if CHIP_CFG_CLK_MCO2_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO2PRE, CHIP_RVAL_RCC_MCO2PRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO2SEL, CHIP_RVAL_RCC_MCO2)
        #endif
            ;

    /* Если HSI не требуется, то его отключение можно выполнить как только
     * будет перенастроена частота CPU и шин, которые работают от HSI по
     * умолчанию, на нужный источник частоты */
#if !CHIP_CFG_CLK_HSI_EN
    CHIP_REGS_RCC->CR &= ~CHIP_REGFLD_RCC_CR_HSION;
#endif

#if CHIP_CFG_CLK_I2S_CKIN_EN
    /* если разрешена частота I2S_CKIN, настраиваем соответствующую функцию
     * пина до настроек источников периферии. Так как пин только один, отдельная
     * настройка пина не требуется */
    CHIP_PIN_CONFIG(CHIP_PIN_PC9_I2S_CLKIN);
#endif

    /* настройка периферийных устройств */
    CHIP_REGS_RCC->D1CCIPR = (CHIP_REGS_RCC->D1CCIPR & ~(0U
                        | CHIP_RCC_RMSK_EXMCSEL
#if CHIP_DEV_SUPPORT_OCTOSPI
                        | CHIP_RCC_RMSK_OCTOSPISEL
#else
                        | CHIP_RCC_RMSK_QSPISEL
#endif
#if CHIP_DEV_SUPPORT_DSI
                        | CHIP_RCC_RMSK_DSISEL
#endif
                        | CHIP_RCC_RMSK_SDMMCSEL
                        | CHIP_RCC_RMSK_CKPERSEL))
            | LBITFIELD_SET(CHIP_RCC_RMSK_EXMCSEL,       CHIP_RVAL_RCC_EXMCSEL)
#if CHIP_DEV_SUPPORT_OCTOSPI
            | LBITFIELD_SET(CHIP_RCC_RMSK_OCTOSPISEL,   CHIP_RVAL_RCC_OCTOSPISEL)
#else
            | LBITFIELD_SET(CHIP_RCC_RMSK_QSPISEL,      CHIP_RVAL_RCC_QSPISEL)
#endif
#if CHIP_DEV_SUPPORT_DSI
            | LBITFIELD_SET(CHIP_RCC_RMSK_DSISEL,       CHIP_RVAL_RCC_DSISEL)
#endif
            | LBITFIELD_SET(CHIP_RCC_RMSK_SDMMCSEL,     CHIP_RVAL_RCC_SDMMCSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_CKPERSEL,     CHIP_RVAL_RCC_CKPERSEL)
            ;
    CHIP_REGS_RCC->D2CCIP1R = (CHIP_REGS_RCC->D2CCIP1R & ~(0U
                         | CHIP_RCC_RMSK_SAI1SEL
#if CHIP_DEV_SUPPORT_SAI2_3
                         | CHIP_RCC_RMSK_SAI23SEL
#endif
                         | CHIP_RCC_RMSK_SPI123SEL
                         | CHIP_RCC_RMSK_SPI45SEL
                         | CHIP_RCC_RMSK_SPDIFSEL
                         | CHIP_RCC_RMSK_DFSDM1SEL
                         | CHIP_RCC_RMSK_FDCANSEL
                         | CHIP_RCC_RMSK_SWPSEL))
            | LBITFIELD_SET(CHIP_RCC_RMSK_SAI1SEL,   CHIP_RVAL_RCC_SAI1SEL)
#if CHIP_DEV_SUPPORT_SAI2_3
            | LBITFIELD_SET(CHIP_RCC_RMSK_SAI23SEL,  CHIP_RVAL_RCC_SAI23SEL)
#endif
            | LBITFIELD_SET(CHIP_RCC_RMSK_SPI123SEL, CHIP_RVAL_RCC_SPI123SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_SPI45SEL,  CHIP_RVAL_RCC_SPI45SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_SPDIFSEL,  CHIP_RVAL_RCC_SPDIFSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_DFSDM1SEL, CHIP_RVAL_RCC_DFSDM1SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_FDCANSEL,  CHIP_RVAL_RCC_FDCANSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_SWPSEL,    CHIP_RVAL_RCC_SWPSEL)
            ;



    CHIP_REGS_RCC->D2CCIP2R = (CHIP_REGS_RCC->D2CCIP2R & ~(0U
                      | CHIP_RCC_RMSK_USART234578SEL
                      | CHIP_RCC_RMSK_USART16SEL
                      | CHIP_RCC_RMSK_RNGSEL
                      | CHIP_RCC_RMSK_I2C123SEL
                      | CHIP_RCC_RMSK_USBSEL
                      | CHIP_RCC_RMSK_CECSEL
                      | CHIP_RCC_RMSK_LPTIM1SEL))
            | LBITFIELD_SET(CHIP_RCC_RMSK_USART234578SEL, CHIP_RVAL_RCC_USART234578SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_USART16SEL,     CHIP_RVAL_RCC_USART16SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_RNGSEL,         CHIP_RVAL_RCC_RNGSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_I2C123SEL,      CHIP_RVAL_RCC_I2C123SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_USBSEL,         CHIP_RVAL_RCC_USBSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_CECSEL,         CHIP_RVAL_RCC_CECSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_LPTIM1SEL,      CHIP_RVAL_RCC_LPTIM1SEL)
            ;

    CHIP_REGS_RCC->D3CCIPR = (CHIP_REGS_RCC->D3CCIPR & ~(0U
                      | CHIP_RCC_RMSK_LPUART1SEL
                      | CHIP_RCC_RMSK_I2C4SEL
                      | CHIP_RCC_RMSK_LPTIM2SEL
                      | CHIP_RCC_RMSK_LPTIM345SEL
                      | CHIP_RCC_RMSK_ADCSEL
                      | CHIP_RCC_RMSK_SAI4ASEL
                      | CHIP_RCC_RMSK_SAI4BSEL
                      | CHIP_RCC_RMSK_SPI6SEL))
            | LBITFIELD_SET(CHIP_RCC_RMSK_LPUART1SEL,    CHIP_RVAL_RCC_LPUART1SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_I2C4SEL,       CHIP_RVAL_RCC_I2C4SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_LPTIM2SEL,     CHIP_RVAL_RCC_LPTIM2SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_LPTIM345SEL,   CHIP_RVAL_RCC_LPTIM345SEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_ADCSEL,        CHIP_RVAL_RCC_ADCSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_SAI4ASEL,      CHIP_RVAL_RCC_SAI4ASEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_SAI4BSEL,      CHIP_RVAL_RCC_SAI4BSEL)
            | LBITFIELD_SET(CHIP_RCC_RMSK_SPI6SEL,       CHIP_RVAL_RCC_SPI6SEL)
            ;
#endif

    CHIP_REGS_RCC->AHB3ENR = (CHIP_REGS_RCC->AHB3ENR & CHIP_REGMSK_RCC_AHB3ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_MDMAEN,         !!CHIP_CLK_MDMA_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_DMA2DEN,        !!CHIP_CLK_DMA2D_EN)
        #if CHIP_DEV_SUPPORT_JPGDEC
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_JPGDECEN,       !!CHIP_CLK_JPGDEC_EN)
        #endif
        #if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_FLITFEN,        !!CHIP_CLK_CM4_FLASH_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_EXMCEN,          !!CHIP_CLK_EXMC_KER_EN)
        #if CHIP_DEV_SUPPORT_OCTOSPI
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_OCTOSPI1EN,     !!CHIP_CLK_OCTOSPI1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_OCTOSPI2EN,     !!CHIP_CLK_OCTOSPI2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_IOMNGREN,       !!CHIP_CLK_IOMNGR_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_OTFD1EN,        !!CHIP_CLK_OTFD1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_OTFD2EN,        !!CHIP_CLK_OTFD2_EN)
        #else
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_QSPIEN,         !!CHIP_CLK_QSPI_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_SDMMC1EN,       !!CHIP_CLK_SDMMC1_EN)
        #if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_DTCM1EN,        !!CHIP_CLK_CM4_DTCM1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_DTCM2EN,        !!CHIP_CLK_CM4_DTCM2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_ITCMEN,         !!CHIP_CLK_CM4_ITCM_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB3ENR_AXISRAMEN,      !!CHIP_CLK_CM4_AXISRAM_EN)
         #endif
            ;

    CHIP_REGS_RCC->AHB1ENR = (CHIP_REGS_RCC->AHB1ENR & CHIP_REGMSK_RCC_AHB1ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_DMA1EN,          !!CHIP_CLK_DMA1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_DMA2EN,          !!CHIP_CLK_DMA2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_ADC12EN,         !!CHIP_CLK_ADC1_2_EN)
        #if CHIP_DEV_SUPPORT_CORE_CM4
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_ARTEN,           !!CHIP_CLK_ART_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_ETH1MACEN,       !!CHIP_CLK_ETH1_MAC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_ETH1TXEN,        !!CHIP_CLK_ETH1_TX_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_ETH1RXEN,        !!CHIP_CLK_ETH1_RX_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_USB1OTGHSEN,     !!CHIP_CLK_USB1_OTGHS_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_USB1OTGHSULPIEN, !!CHIP_CLK_USB1_OTGHS_ULPI_EN)
        #if CHIP_DEV_SUPPORT_USB2
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_USB2OTGHSEN,     !!CHIP_CLK_USB2_OTGHS_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB1ENR_USB2OTGHSULPIEN, !!CHIP_CLK_USB2_OTGHS_ULPI_EN)
        #endif
            ;

    CHIP_REGS_RCC->AHB2ENR = (CHIP_REGS_RCC->AHB2ENR & CHIP_REGMSK_RCC_AHB2ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_CAMITFEN,   !!CHIP_CLK_CAMITF_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_CRYPEN,     !!CHIP_CLK_CRYPT_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_HASHEN,     !!CHIP_CLK_HASH_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_RNGEN,      !!CHIP_CLK_RNG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_SDMMC2EN,   !!CHIP_CLK_SDMMC2_EN)
        #if CHIP_DEV_SUPPORT_FMAC
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_FMACEN,     !!CHIP_CLK_FMAC_EN)
        #endif
        #if CHIP_DEV_SUPPORT_CORDIC
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_CORDICEN,   !!CHIP_CLK_CORDIC_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_SRAM1EN,    !!CHIP_CLK_SRAM1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_SRAM2EN,    !!CHIP_CLK_SRAM2_EN)
        #if CHIP_DEV_SUPPORT_SRAM3
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB2ENR_SRAM3EN,    !!CHIP_CLK_SRAM3_EN)
        #endif
            ;

    CHIP_REGS_RCC->AHB4ENR = (CHIP_REGS_RCC->AHB4ENR & CHIP_REGMSK_RCC_AHB4ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOAEN,    !!CHIP_CLK_GPIOA_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOBEN,    !!CHIP_CLK_GPIOB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOCEN,    !!CHIP_CLK_GPIOC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIODEN,    !!CHIP_CLK_GPIOD_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOEEN,    !!CHIP_CLK_GPIOE_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOFEN,    !!CHIP_CLK_GPIOF_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOGEN,    !!CHIP_CLK_GPIOG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOHEN,    !!CHIP_CLK_GPIOH_EN)
        #if CHIP_DEV_SUPPORT_PORTI
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOIEN,    !!CHIP_CLK_GPIOI_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOJEN,    !!CHIP_CLK_GPIOJ_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_GPIOKEN,    !!CHIP_CLK_GPIOK_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_CRCEN,      !!CHIP_CLK_CRC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_BDMAEN,     !!CHIP_CLK_BDMA_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_ADC3EN,     !!CHIP_CLK_ADC3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_HSEMEN,     !!CHIP_CLK_HSEM_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHB4ENR_BKPRAMEN,   !!CHIP_CLK_BKPRAM_EN)
            ;

#if !CHIP_DEV_SUPPORT_CORE_CM4 && CHIP_CLK_WWDG1_EN
    /* До разрешения клока WWDG1 для одноядерных контроллеров необходимо установить
     * тип сброса на системный в GCR, иначе будет не определенное поведение */
    CHIP_REGS_RCC->GCR |= LBITFIELD_SET(CHIP_REGFLD_RCC_GCR_WW1RSC, 1);
#endif

    CHIP_REGS_RCC->APB3ENR = (CHIP_REGS_RCC->APB3ENR & 0xFFFFFFA7)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB3ENR_LTDCEN,     !!CHIP_CLK_LTDC_EN)
        #if CHIP_DEV_SUPPORT_DSI
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB3ENR_DSIEN,      !!CHIP_CLK_DSI_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB3ENR_WWDG1EN,    !!CHIP_CLK_WWDG1_EN)
            ;

    CHIP_REGS_RCC->APB1LENR = (CHIP_REGS_RCC->APB1LENR & CHIP_REGMSK_RCC_APB1LENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM2EN,    !!CHIP_CLK_TIM2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM3EN,    !!CHIP_CLK_TIM3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM4EN,    !!CHIP_CLK_TIM4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM5EN,    !!CHIP_CLK_TIM5_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM6EN,    !!CHIP_CLK_TIM6_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM7EN,    !!CHIP_CLK_TIM7_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM12EN,   !!CHIP_CLK_TIM12_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM13EN,   !!CHIP_CLK_TIM13_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_TIM14EN,   !!CHIP_CLK_TIM14_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_LPTIM1EN,  !!CHIP_CLK_LPTIM1_EN)
        #if CHIP_DEV_SUPPORT_CORE_CM4
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_WWDG2EN,   !!CHIP_CLK_WWDG2_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_SPI2EN,    !!CHIP_CLK_SPI2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_SPI3EN,    !!CHIP_CLK_SPI3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_SPDIFRXEN, !!CHIP_CLK_SPDIFRX_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_USART2EN,  !!CHIP_CLK_USART2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_USART3EN,  !!CHIP_CLK_USART3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_UART4EN,   !!CHIP_CLK_UART4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_UART5EN,   !!CHIP_CLK_UART5_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_I2C1EN,    !!CHIP_CLK_I2C1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_I2C2EN,    !!CHIP_CLK_I2C2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_I2C3EN,    !!CHIP_CLK_I2C3_EN)
        #if CHIP_DEV_SUPPORT_I2C5
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_I2C5EN,    !!CHIP_CLK_I2C5_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_CECEN,     !!CHIP_CLK_CEC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_DAC12EN,   !!CHIP_CLK_DAC12_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_UART7EN,   !!CHIP_CLK_UART7_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1LENR_UART8EN,   !!CHIP_CLK_UART8_EN)

            ;

    CHIP_REGS_RCC->APB1HENR = (CHIP_REGS_RCC->APB1HENR & CHIP_REGMSK_RCC_APB1HENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1HENR_CRSEN,   !!CHIP_CLK_CRS_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1HENR_SWPMIEN, !!CHIP_CLK_SWPMI_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1HENR_OPAMPEN, !!CHIP_CLK_OPAMP_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1HENR_MDIOSEN, !!CHIP_CLK_MDIOS_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1HENR_FDCANEN, !!CHIP_CLK_FDCAN_EN)
        #if CHIP_DEV_SUPPORT_TIM23_24
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1HENR_TIM23EN, !!CHIP_CLK_TIM23_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1HENR_TIM24EN, !!CHIP_CLK_TIM24_EN)
        #endif
            ;



    CHIP_REGS_RCC->APB2ENR = (CHIP_REGS_RCC->APB2ENR & CHIP_REGMSK_RCC_APB2ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM1EN,   !!CHIP_CLK_TIM1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM8EN,   !!CHIP_CLK_TIM8_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART1EN, !!CHIP_CLK_USART1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART6EN, !!CHIP_CLK_USART6_EN)
        #if CHIP_DEV_SUPPORT_USART9_10
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART1EN, !!CHIP_CLK_UART9_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART6EN, !!CHIP_CLK_USART10_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SPI1EN,   !!CHIP_CLK_SPI1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SPI4EN,   !!CHIP_CLK_SPI4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM15EN,  !!CHIP_CLK_TIM15_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM16EN,  !!CHIP_CLK_TIM16_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM17EN,  !!CHIP_CLK_TIM17_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SPI5EN,   !!CHIP_CLK_SPI5_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SAI1EN,   !!CHIP_CLK_SAI1_EN)
        #if CHIP_DEV_SUPPORT_SAI2_3
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SAI2EN,   !!CHIP_CLK_SAI2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SAI3EN,   !!CHIP_CLK_SAI3_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_DFSDM1EN, !!CHIP_CLK_DFSDM1_EN)
        #if CHIP_DEV_SUPPORT_HRTIM
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_HRTIMEN,  !!CHIP_CLK_HRTIM_EN)
        #endif
            ;

    CHIP_REGS_RCC->APB4ENR = (CHIP_REGS_RCC->APB4ENR & CHIP_REGMSK_RCC_APB4ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_SYSCFGEN,  !!CHIP_CLK_SYSCFG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_LPUART1EN, !!CHIP_CLK_LPUART1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_SPI6EN,    !!CHIP_CLK_SPI6_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_I2C4EN,    !!CHIP_CLK_I2C4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_LPTIM2EN,  !!CHIP_CLK_LPTIM2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_LPTIM3EN,  !!CHIP_CLK_LPTIM3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_LPTIM4EN,  !!CHIP_CLK_LPTIM4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_LPTIM5EN,  !!CHIP_CLK_LPTIM5_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_COMP12EN,  !!CHIP_CLK_COMP12_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_VREFEN,    !!CHIP_CLK_VREF_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_RTCAPBEN,  !!CHIP_CLK_RTC_APB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_SAI4EN,    !!CHIP_CLK_SAI4_EN)
        #if CHIP_DEV_SUPPORT_DTS
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB4ENR_DTSEN,     !!CHIP_CLK_DTS_EN)
        #endif
            ;


#if !CHIP_SHARED_NO_INIT
    /* ожидание установки частоты для доменов D1 (всегда разрешен пока работает CPU),
     * а также для D2, если хотя бы один клок в D2 разрешен */
    CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & (CHIP_REGFLD_RCC_CR_D1CKRDY | CHIP_RVAL_RCC_D2CKRDY)) ==
            (CHIP_REGFLD_RCC_CR_D1CKRDY | CHIP_RVAL_RCC_D2CKRDY),
                       CHIP_INIT_FAULT_WAIT_D1_2_RDY, 10000);
    /* настройка пинов MCO1 и MCO2 на соответствующие функции
     * (для каждой MCO существует только один пин, поэтому не требуется
     * явно настраивать */
#if CHIP_CFG_CLK_MCO1_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PA8_MCO1);
#endif
#if CHIP_CFG_CLK_MCO2_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PC9_MCO2);
#endif

#endif

#if CHIP_CFG_LTIMER_EN
    /* разрешение ltimer */
    lclock_init();
#endif
}

