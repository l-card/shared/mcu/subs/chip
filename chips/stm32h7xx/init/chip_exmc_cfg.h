#ifndef CHIP_EXMC_CFG_H
#define CHIP_EXMC_CFG_H

#include "chip_config.h"
#include "chip_exmc_defs.h"
#include "chip_clk.h"
#include "lbitfield.h"

#ifndef MIN
    #define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
    #define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

#ifndef CHIP_CFG_EXMC_SETUP_EN
    #define CHIP_CFG_EXMC_SETUP_EN 0
#endif



#define CHIP_SDRAM_DEFAULT_POWERUP_TIME_US 100
#define CHIP_SDRAM_DEFAULT_AUTOREF_CNT     8

#define CHIP_SDRAM_REF_CYCLES(tms)  (((CHIP_CLK_SDRAM_FREQ) / 1000) * (tms))
#define CHIP_CALC_SDRAM_MODE_REG(cas) (LBITFIELD_SET(CHIP_SDRAM_REGFLD_MODE_BURST_LEN, CHIP_SDRAM_REGFLDVAL_MODE_BURST_LEN_1) \
    | LBITFIELD_SET(CHIP_SDRAM_REGFLD_MODE_BURST_TYPE, CHIP_SDRAM_REGFLDVAL_MODE_BURST_TYPE_SEQ) \
    | LBITFIELD_SET(CHIP_SDRAM_REGFLD_MODE_CAS_LATENCY, cas) \
    | LBITFIELD_SET(CHIP_SDRAM_REGFLD_MODE_OPMODE, CHIP_SDRAM_REGFLDVAL_MODE_OPMODE_STD) \
    | LBITFIELD_SET(CHIP_SDRAM_REGFLD_MODE_WR_BURST, CHIP_SDRAM_REGFLDVAL_MODE_WR_BURST_PROG) \
    )

#ifdef CHIP_CFG_SDRAM1_EN
    #define CHIP_SDRAM1_EN  CHIP_CFG_SDRAM1_EN
#else
    #define CHIP_SDRAM1_EN  0
#endif

#if CHIP_SDRAM1_EN
    #ifndef CHIP_CFG_SDRAM1_PIN_CKE
        #error CKE pin for SDRAM1 is not defined
    #endif
    #ifndef CHIP_CFG_SDRAM1_PIN_CS
        #error CS pin for SDRAM1 is not defined
    #endif

    /* sdram data bus width */
    #ifdef CHIP_CFG_SDRAM1_DATA_WIDTH
        #define CHIP_SDRAM1_DATA_WIDTH CHIP_CFG_SDRAM1_DATA_WIDTH
    #else
        #ifdef CHIP_CFG_SDRAM_DATA_WIDTH
            #define CHIP_SDRAM1_DATA_WIDTH CHIP_CFG_SDRAM_DATA_WIDTH
        #else
            #error SDRAM1 data bus width is not defined
        #endif
    #endif
    #if CHIP_SDRAM1_DATA_WIDTH == 8
        #define CHIP_RVAL_SDCR1_MWID 0
    #elif CHIP_SDRAM1_DATA_WIDTH == 16
        #define CHIP_RVAL_SDCR1_MWID 1
    #elif CHIP_SDRAM1_DATA_WIDTH == 32
        #define CHIP_RVAL_SDCR1_MWID 2
    #else
        #error invalid SDRAM1 data bus width
    #endif

    /* sdram internal banks count */
    #ifdef CHIP_CFG_SDRAM1_BANKS_CNT
        #define CHIP_SDRAM1_BANKS_CNT CHIP_CFG_SDRAM1_BANKS_CNT
    #else
        #ifdef CHIP_CFG_SDRAM_BANKS_CNT
            #define CHIP_SDRAM1_BANKS_CNT CHIP_CFG_SDRAM_BANKS_CNT
        #else
            #error SDRAM1 internal banks count is not defined
        #endif
    #endif

    #if CHIP_SDRAM1_BANKS_CNT == 2
        #define CHIP_RVAL_SDCR1_NB 0
        #define CHIP_SDRAM1_BA_WIDTH 1
    #elif CHIP_SDRAM1_BANKS_CNT == 4
        #define CHIP_RVAL_SDCR1_NB 1
        #define CHIP_SDRAM1_BA_WIDTH 1
    #else
        #error invalid SDRAM1 internal banks count
    #endif

    /* sdram row address bits */
    #ifdef CHIP_CFG_SDRAM1_ROW_ADDR_WIDTH
        #define CHIP_SDRAM1_ROW_ADDR_WIDTH CHIP_CFG_SDRAM1_ROW_ADDR_WIDTH
    #else
        #ifdef CHIP_CFG_SDRAM_ROW_ADDR_WIDTH
            #define CHIP_SDRAM1_ROW_ADDR_WIDTH CHIP_CFG_SDRAM_ROW_ADDR_WIDTH
        #else
            #error SDRAM1 row address width is not defined
        #endif
    #endif

    #if CHIP_SDRAM1_ROW_ADDR_WIDTH == 11
        #define CHIP_RVAL_SDCR1_NR  0
    #elif CHIP_SDRAM1_ROW_ADDR_WIDTH == 12
        #define CHIP_RVAL_SDCR1_NR  1
    #elif CHIP_SDRAM1_ROW_ADDR_WIDTH == 13
        #define CHIP_RVAL_SDCR1_NR  2
    #else
        #error invalid SDRAM1 row address width
    #endif

    /* sdram columns address bits */
    #ifdef CHIP_CFG_SDRAM1_COL_ADDR_WIDTH
        #define CHIP_SDRAM1_COL_ADDR_WIDTH CHIP_CFG_SDRAM1_COL_ADDR_WIDTH
    #else
        #ifdef CHIP_CFG_SDRAM_COL_ADDR_WIDTH
            #define CHIP_SDRAM1_COL_ADDR_WIDTH CHIP_CFG_SDRAM_COL_ADDR_WIDTH
        #else
            #error SDRAM1 column address width is not defined
        #endif
    #endif

    #if CHIP_SDRAM1_COL_ADDR_WIDTH == 8
        #define CHIP_RVAL_SDCR1_NC  0
    #elif CHIP_SDRAM1_COL_ADDR_WIDTH == 9
        #define CHIP_RVAL_SDCR1_NC  1
    #elif CHIP_SDRAM1_COL_ADDR_WIDTH == 10
        #define CHIP_RVAL_SDCR1_NC  2
    #elif CHIP_SDRAM1_COL_ADDR_WIDTH == 11
        #define CHIP_RVAL_SDCR1_NC  3
    #else
        #error invalid SDRAM1 column address width
    #endif


    #ifdef CHIP_CFG_SDRAM1_CAS_LATENCY
        #define CHIP_SDRAM1_CAS_LATENCY CHIP_CFG_SDRAM1_CAS_LATENCY
    #else
        #ifdef CHIP_CFG_SDRAM_CAS_LATENCY
            #define CHIP_SDRAM1_CAS_LATENCY CHIP_CFG_SDRAM_CAS_LATENCY
        #else
            #error SDRAM1 CAS latency is not defined
        #endif
    #endif

    #if CHIP_SDRAM1_CAS_LATENCY == 1
        #define CHIP_RVAL_SDCR1_CAS 1
    #elif CHIP_SDRAM1_CAS_LATENCY == 2
        #define CHIP_RVAL_SDCR1_CAS 2
    #elif CHIP_SDRAM1_CAS_LATENCY == 3
        #define CHIP_RVAL_SDCR1_CAS 3
    #else
        #error Invalid SDRAM1 CAS Latency
    #endif


    #ifdef CHIP_CFG_SDRAM1_T_RCD
        #define CHIP_SDRAM1_T_RCD CHIP_CFG_SDRAM1_T_RCD
    #else
        #ifdef CHIP_CFG_SDRAM_T_RCD
            #define CHIP_SDRAM1_T_RCD CHIP_CFG_SDRAM_T_RCD
        #else
            #error SDRAM1 RCD time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR1_TRCD  CHIP_SDRAM_T_RVAL(CHIP_SDRAM1_T_RCD, 1)
    #if CHIP_RVAL_SDTR1_TRCD > 0xF
        #error invlid SDRAM1 RCD time
    #endif

    #ifdef CHIP_CFG_SDRAM1_T_RP
        #define CHIP_SDRAM1_T_RP CHIP_CFG_SDRAM1_T_RP
    #else
        #ifdef CHIP_CFG_SDRAM_T_RP
            #define CHIP_SDRAM1_T_RP CHIP_CFG_SDRAM_T_RP
        #else
            #error SDRAM1 RP time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR1_TRP  CHIP_SDRAM_T_RVAL(CHIP_SDRAM1_T_RP, 1)
    #if CHIP_RVAL_SDTR1_TRP > 0xF
        #error invlid SDRAM1 RP time
    #endif

    #ifdef CHIP_CFG_SDRAM1_T_RAS
        #define CHIP_SDRAM1_T_RAS CHIP_CFG_SDRAM1_T_RAS
    #else
        #ifdef CHIP_CFG_SDRAM_T_RAS
            #define CHIP_SDRAM1_T_RAS CHIP_CFG_SDRAM_T_RAS
        #else
            #error SDRAM1 RAS time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR1_TRAS  CHIP_SDRAM_T_RVAL(CHIP_SDRAM1_T_RAS, 1)
    #if CHIP_RVAL_SDTR1_TRAS > 0xF
        #error invlid SDRAM1 RAS time
    #endif

    #ifdef CHIP_CFG_SDRAM1_T_RC
        #define CHIP_SDRAM1_T_RC CHIP_CFG_SDRAM1_T_RC
    #else
        #ifdef CHIP_CFG_SDRAM_T_RC
            #define CHIP_SDRAM1_T_RC CHIP_CFG_SDRAM_T_RC
        #else
            #error SDRAM1 RC time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR1_TRC  CHIP_SDRAM_T_RVAL(CHIP_SDRAM1_T_RC, 1)
    #if CHIP_RVAL_SDTR1_TRC > 0xF
        #error invlid SDRAM1 RC time
    #endif

    #ifdef CHIP_CFG_SDRAM1_T_XSR
        #define CHIP_SDRAM1_T_XSR CHIP_CFG_SDRAM1_T_XSR
    #else
        #ifdef CHIP_CFG_SDRAM_T_XSR
            #define CHIP_SDRAM1_T_XSR CHIP_CFG_SDRAM_T_XSR
        #else
            #error SDRAM1 XSR time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR1_TXSR  CHIP_SDRAM_T_RVAL(CHIP_SDRAM1_T_XSR, 1)
    #if CHIP_RVAL_SDTR1_TXSR > 0xF
        #error invlid SDRAM1 XSR time
    #endif

    #ifdef CHIP_CFG_SDRAM1_T_MRD
        #define CHIP_SDRAM1_T_MRD CHIP_CFG_SDRAM1_T_MRD
    #else
        #ifdef CHIP_CFG_SDRAM_T_MRD
            #define CHIP_SDRAM1_T_MRD CHIP_CFG_SDRAM_T_MRD
        #else
            #error SDRAM1 MRD time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR1_TMRD  CHIP_SDRAM_T_RVAL(CHIP_SDRAM1_T_MRD, 1)
    #if CHIP_RVAL_SDTR1_TMRD > 0xF
        #error invlid SDRAM1 MRD time
    #endif

    #ifdef CHIP_CFG_SDRAM1_T_WR
        #define CHIP_SDRAM1_T_WR CHIP_CFG_SDRAM1_T_WR
    #elif defined CHIP_CFG_SDRAM_T_WR
        #define CHIP_SDRAM1_T_WR CHIP_CFG_SDRAM_T_WR
    #endif

    /* Должно выполняться условие Twr >= Tras - Trcd и Twr >= Trc - Trcd - Trp.
     * Если Twr задано явно, то проверяем эти условия, иначе выбираем минимальное
     * из значений */
    #define CHIP_RVAL_SDTR1_TWR_MIN1    (CHIP_RVAL_SDTR1_TRAS - CHIP_RVAL_SDTR1_TRCD)
    #define CHIP_RVAL_SDTR1_TWR_MIN2    (CHIP_RVAL_SDTR1_TRC - CHIP_RVAL_SDTR1_TRCD - CHIP_RVAL_SDTR1_TRP + 1)
    #define CHIP_RVAL_SDTR1_TWR_MIN     MAX(CHIP_RVAL_SDTR1_TWR_MIN1, CHIP_RVAL_SDTR1_TWR_MIN2)

    #ifdef CHIP_SDRAM1_T_WR
        #define CHIP_RVAL_SDTR1_TWR  CHIP_SDRAM_T_RVAL(CHIP_SDRAM1_T_WR, 1)
        #if (CHIP_RVAL_SDTR1_TWR < CHIP_RVAL_SDTR1_TWR_MIN)
            #error Invalid SDRAM1 TWR value
        #endif
    #else
        #define CHIP_RVAL_SDTR1_TWR CHIP_RVAL_SDTR1_TWR_MIN
    #endif
    #if (CHIP_RVAL_SDTR1_TWR > 0xF) || (CHIP_RVAL_SDTR1_TWR < 0)
        #error Invlid SDRAM1 WR time
    #endif

    #ifdef CHIP_CFG_SDRAM1_AUTOREF_CNT
        #define CHIP_SDRAM1_AUTOREF_CNT CHIP_CFG_SDRAM1_AUTOREF_CNT
    #elif defined CHIP_CFG_SDRAM_AUTOREF_CNT
        #define CHIP_SDRAM1_AUTOREF_CNT CHIP_CFG_SDRAM_AUTOREF_CNT
    #else
        #define CHIP_SDRAM1_AUTOREF_CNT CHIP_SDRAM_DEFAULT_AUTOREF_CNT
    #endif

    #ifdef CHIP_CFG_SDRAM1_REF_TIME_MS
        #define CHIP_SDRAM1_REF_TIME_MS CHIP_CFG_SDRAM1_REF_TIME_MS
    #else
        #ifdef CHIP_CFG_SDRAM_REF_TIME_MS
            #define CHIP_SDRAM1_REF_TIME_MS CHIP_CFG_SDRAM_REF_TIME_MS
        #else
            #error SDRAM1 Refresh Time is not defined
        #endif
    #endif

    #ifdef CHIP_CFG_SDRAM1_POWERUP_TIME_US
        #define CHIP_SDRAM1_POWERUP_TIME_US CHIP_CFG_SDRAM1_POWERUP_TIME_US
    #else
        #ifdef CHIP_CFG_SDRAM_POWERUP_TIME_US
            #define CHIP_SDRAM1_POWERUP_TIME_US CHIP_CFG_SDRAM_POWERUP_TIME_US
        #else
            #define CHIP_SDRAM1_POWERUP_TIME_US CHIP_SDRAM_DEFAULT_POWERUP_TIME_US
        #endif
    #endif

    #define CHIP_RVAL_SDRAM1_MODE_REG CHIP_CALC_SDRAM_MODE_REG(CHIP_SDRAM1_CAS_LATENCY)


    #define CHIP_SDRAM1_SIZE (\
                (1 << CHIP_SDRAM1_ROW_ADDR_WIDTH) *\
                (1 << CHIP_SDRAM1_COL_ADDR_WIDTH) *\
                (CHIP_SDRAM1_DATA_WIDTH >> 3) *\
                (1 << CHIP_SDRAM1_BA_WIDTH))
#else
    #define CHIP_SDRAM1_DATA_WIDTH      0
    #define CHIP_SDRAM1_BA_WIDTH        0
    #define CHIP_SDRAM1_ROW_ADDR_WIDTH  0
    #define CHIP_SDRAM1_COL_ADDR_WIDTH  0
    #define CHIP_SDRAM1_SIZE            0
    #define CHIP_RVAL_SDRAM1_MODE_REG   0
#endif


#ifdef CHIP_CFG_SDRAM2_EN
    #define CHIP_SDRAM2_EN  CHIP_CFG_SDRAM2_EN
#else
    #define CHIP_SDRAM2_EN  0
#endif

#if CHIP_SDRAM2_EN
    #ifndef CHIP_CFG_SDRAM2_PIN_CKE
        #error CKE pin for SDRAM2 is not defined
    #endif
    #ifndef CHIP_CFG_SDRAM2_PIN_CS
        #error CS pin for SDRAM2 is not defined
    #endif

    /* sdram data bus width */
    #ifdef CHIP_CFG_SDRAM2_DATA_WIDTH
        #define CHIP_SDRAM2_DATA_WIDTH CHIP_CFG_SDRAM2_DATA_WIDTH
    #else
        #ifdef CHIP_CFG_SDRAM_DATA_WIDTH
            #define CHIP_SDRAM2_DATA_WIDTH CHIP_CFG_SDRAM_DATA_WIDTH
        #else
            #error SDRAM2 data bus width is not defined
        #endif
    #endif
    #if CHIP_SDRAM2_DATA_WIDTH == 8
        #define CHIP_RVAL_SDCR2_MWID 0
    #elif CHIP_SDRAM2_DATA_WIDTH == 16
        #define CHIP_RVAL_SDCR2_MWID 1
    #elif CHIP_SDRAM2_DATA_WIDTH == 32
        #define CHIP_RVAL_SDCR2_MWID 2
    #else
        #error invalid SDRAM2 data bus width
    #endif

    /* sdram internal banks count */
    #ifdef CHIP_CFG_SDRAM2_BANKS_CNT
        #define CHIP_SDRAM2_BANKS_CNT CHIP_CFG_SDRAM2_BANKS_CNT
    #else
        #ifdef CHIP_CFG_SDRAM_BANKS_CNT
            #define CHIP_SDRAM2_BANKS_CNT CHIP_CFG_SDRAM_BANKS_CNT
        #else
            #error SDRAM2 internal banks count is not defined
        #endif
    #endif

    #if CHIP_SDRAM2_BANKS_CNT == 2
        #define CHIP_RVAL_SDCR2_NB 0
        #define CHIP_SDRAM2_BA_WIDTH 1
    #elif CHIP_SDRAM2_BANKS_CNT == 4
        #define CHIP_RVAL_SDCR2_NB 1
        #define CHIP_SDRAM2_BA_WIDTH 2
    #else
        #error invalid SDRAM2 internal banks count
    #endif

    /* sdram row address bits */
    #ifdef CHIP_CFG_SDRAM2_ROW_ADDR_WIDTH
        #define CHIP_SDRAM2_ROW_ADDR_WIDTH CHIP_CFG_SDRAM2_ROW_ADDR_WIDTH
    #else
        #ifdef CHIP_CFG_SDRAM_ROW_ADDR_WIDTH
            #define CHIP_SDRAM2_ROW_ADDR_WIDTH CHIP_CFG_SDRAM_ROW_ADDR_WIDTH
        #else
            #error SDRAM2 row address width is not defined
        #endif
    #endif

    #if CHIP_SDRAM2_ROW_ADDR_WIDTH == 11
        #define CHIP_RVAL_SDCR2_NR  0
    #elif CHIP_SDRAM2_ROW_ADDR_WIDTH == 12
        #define CHIP_RVAL_SDCR2_NR  1
    #elif CHIP_SDRAM2_ROW_ADDR_WIDTH == 13
        #define CHIP_RVAL_SDCR2_NR  2
    #else
        #error invalid SDRAM2 row address width
    #endif

    /* sdram columns address bits */
    #ifdef CHIP_CFG_SDRAM2_COL_ADDR_WIDTH
        #define CHIP_SDRAM2_COL_ADDR_WIDTH CHIP_CFG_SDRAM2_COL_ADDR_WIDTH
    #else
        #ifdef CHIP_CFG_SDRAM_COL_ADDR_WIDTH
            #define CHIP_SDRAM2_COL_ADDR_WIDTH CHIP_CFG_SDRAM_COL_ADDR_WIDTH
        #else
            #error SDRAM2 column address width is not defined
        #endif
    #endif

    #if CHIP_SDRAM2_COL_ADDR_WIDTH == 8
        #define CHIP_RVAL_SDCR2_NC  0
    #elif CHIP_SDRAM2_COL_ADDR_WIDTH == 9
        #define CHIP_RVAL_SDCR2_NC  1
    #elif CHIP_SDRAM2_COL_ADDR_WIDTH == 10
        #define CHIP_RVAL_SDCR2_NC  2
    #elif CHIP_SDRAM2_COL_ADDR_WIDTH == 11
        #define CHIP_RVAL_SDCR2_NC  3
    #else
        #error invalid SDRAM2 column address width
    #endif


    #ifdef CHIP_CFG_SDRAM2_CAS_LATENCY
        #define CHIP_SDRAM2_CAS_LATENCY CHIP_CFG_SDRAM2_CAS_LATENCY
    #else
        #ifdef CHIP_CFG_SDRAM_CAS_LATENCY
            #define CHIP_SDRAM2_CAS_LATENCY CHIP_CFG_SDRAM_CAS_LATENCY
        #else
            #error SDRAM2 CAS latency is not defined
        #endif
    #endif

    #if CHIP_SDRAM2_CAS_LATENCY == 1
        #define CHIP_RVAL_SDCR2_CAS 1
    #elif CHIP_SDRAM2_CAS_LATENCY == 2
        #define CHIP_RVAL_SDCR2_CAS 2
    #elif CHIP_SDRAM2_CAS_LATENCY == 3
        #define CHIP_RVAL_SDCR2_CAS 3
    #else
        #error Invalid SDRAM2 CAS Latency
    #endif


    #ifdef CHIP_CFG_SDRAM2_T_RCD
        #define CHIP_SDRAM2_T_RCD CHIP_CFG_SDRAM2_T_RCD
    #else
        #ifdef CHIP_CFG_SDRAM_T_RCD
            #define CHIP_SDRAM2_T_RCD CHIP_CFG_SDRAM_T_RCD
        #else
            #error SDRAM2 RCD time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR2_TRCD  CHIP_SDRAM_T_RVAL(CHIP_SDRAM2_T_RCD, 1)
    #if CHIP_RVAL_SDTR2_TRCD > 0xF
        #error invlid SDRAM2 RCD time
    #endif

    #ifdef CHIP_CFG_SDRAM2_T_RP
        #define CHIP_SDRAM2_T_RP CHIP_CFG_SDRAM2_T_RP
    #else
        #ifdef CHIP_CFG_SDRAM_T_RP
            #define CHIP_SDRAM2_T_RP CHIP_CFG_SDRAM_T_RP
        #else
            #error SDRAM2 RP time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR2_TRP  CHIP_SDRAM_T_RVAL(CHIP_SDRAM2_T_RP, 1)
    #if CHIP_RVAL_SDTR2_TRP > 0xF
        #error invlid SDRAM2 RP time
    #endif

    #ifdef CHIP_CFG_SDRAM2_T_RAS
        #define CHIP_SDRAM2_T_RAS CHIP_CFG_SDRAM2_T_RAS
    #else
        #ifdef CHIP_CFG_SDRAM_T_RAS
            #define CHIP_SDRAM2_T_RAS CHIP_CFG_SDRAM_T_RAS
        #else
            #error SDRAM2 RAS time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR2_TRAS  CHIP_SDRAM_T_RVAL(CHIP_SDRAM2_T_RAS, 1)
    #if CHIP_RVAL_SDTR2_TRAS > 0xF
        #error invlid SDRAM2 RAS time
    #endif

    #ifdef CHIP_CFG_SDRAM2_T_RC
        #define CHIP_SDRAM2_T_RC CHIP_CFG_SDRAM2_T_RC
    #else
        #ifdef CHIP_CFG_SDRAM_T_RC
            #define CHIP_SDRAM2_T_RC CHIP_CFG_SDRAM_T_RC
        #else
            #error SDRAM2 RC time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR2_TRC  CHIP_SDRAM_T_RVAL(CHIP_SDRAM2_T_RC, 1)
    #if CHIP_RVAL_SDTR2_TRC > 0xF
        #error invlid SDRAM2 RC time
    #endif

    #ifdef CHIP_CFG_SDRAM2_T_XSR
        #define CHIP_SDRAM2_T_XSR CHIP_CFG_SDRAM2_T_XSR
    #else
        #ifdef CHIP_CFG_SDRAM_T_XSR
            #define CHIP_SDRAM2_T_XSR CHIP_CFG_SDRAM_T_XSR
        #else
            #error SDRAM2 XSR time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR2_TXSR  CHIP_SDRAM_T_RVAL(CHIP_SDRAM2_T_XSR, 1)
    #if CHIP_RVAL_SDTR2_TXSR > 0xF
        #error invlid SDRAM2 XSR time
    #endif

    #ifdef CHIP_CFG_SDRAM2_T_MRD
        #define CHIP_SDRAM2_T_MRD CHIP_CFG_SDRAM2_T_MRD
    #else
        #ifdef CHIP_CFG_SDRAM_T_MRD
            #define CHIP_SDRAM2_T_MRD CHIP_CFG_SDRAM_T_MRD
        #else
            #error SDRAM2 MRD time is not defined
        #endif
    #endif
    #define CHIP_RVAL_SDTR2_TMRD  CHIP_SDRAM_T_RVAL(CHIP_SDRAM2_T_MRD, 1)
    #if CHIP_RVAL_SDTR2_TMRD > 0xF
        #error invlid SDRAM2 MRD time
    #endif

    #ifdef CHIP_CFG_SDRAM2_T_WR
        #define CHIP_SDRAM2_T_WR CHIP_CFG_SDRAM2_T_WR
    #elif defined CHIP_CFG_SDRAM_T_WR
        #define CHIP_SDRAM2_T_WR CHIP_CFG_SDRAM_T_WR
    #endif

    /* Должно выполняться условие Twr >= Tras - Trcd и Twr >= Trc - Trcd - Trp.
     * Если Twr задано явно, то проверяем эти условия, иначе выбираем минимальное
     * из значений */
    #define CHIP_RVAL_SDTR2_TWR_MIN1    (CHIP_RVAL_SDTR2_TRAS - CHIP_RVAL_SDTR2_TRCD)
    #define CHIP_RVAL_SDTR2_TWR_MIN2    (CHIP_RVAL_SDTR2_TRC - CHIP_RVAL_SDTR2_TRCD - CHIP_RVAL_SDTR2_TRP + 1)
    #define CHIP_RVAL_SDTR2_TWR_MIN     MAX(CHIP_RVAL_SDTR2_TWR_MIN1, CHIP_RVAL_SDTR2_TWR_MIN2)

    #ifdef CHIP_SDRAM2_T_WR
        #define CHIP_RVAL_SDTR2_TWR  CHIP_SDRAM_T_RVAL(CHIP_SDRAM2_T_WR, 1)
        #if (CHIP_RVAL_SDTR2_TWR < CHIP_RVAL_SDTR2_TWR_MIN)
            #error Invalid SDRAM2 TWR value
        #endif
    #else
        #define CHIP_RVAL_SDTR2_TWR CHIP_RVAL_SDTR2_TWR_MIN
    #endif
    #if (CHIP_RVAL_SDTR2_TWR > 0xF) || (CHIP_RVAL_SDTR2_TWR < 0)
        #error Invlid SDRAM2 WR time
    #endif

    #ifdef CHIP_CFG_SDRAM2_AUTOREF_CNT
        #define CHIP_SDRAM2_AUTOREF_CNT CHIP_CFG_SDRAM2_AUTOREF_CNT
    #elif defined CHIP_CFG_SDRAM_AUTOREF_CNT
        #define CHIP_SDRAM2_AUTOREF_CNT CHIP_CFG_SDRAM_AUTOREF_CNT
    #else
        #define CHIP_SDRAM2_AUTOREF_CNT CHIP_SDRAM_DEFAULT_AUTOREF_CNT
    #endif

    #ifdef CHIP_CFG_SDRAM2_REF_TIME_MS
        #define CHIP_SDRAM2_REF_TIME_MS CHIP_CFG_SDRAM2_REF_TIME_MS
    #else
        #ifdef CHIP_CFG_SDRAM_REF_TIME_MS
            #define CHIP_SDRAM2_REF_TIME_MS CHIP_CFG_SDRAM_REF_TIME_MS
        #else
            #error SDRAM2 Refresh Time is not defined
        #endif
    #endif

    #ifdef CHIP_CFG_SDRAM2_POWERUP_TIME_US
        #define CHIP_SDRAM2_POWERUP_TIME_US CHIP_CFG_SDRAM2_POWERUP_TIME_US
    #else
        #ifdef CHIP_CFG_SDRAM_POWERUP_TIME_US
            #define CHIP_SDRAM2_POWERUP_TIME_US CHIP_CFG_SDRAM_POWERUP_TIME_US
        #else
            #define CHIP_SDRAM2_POWERUP_TIME_US CHIP_SDRAM_DEFAULT_POWERUP_TIME_US
        #endif
    #endif

    #define CHIP_RVAL_SDRAM2_MODE_REG CHIP_CALC_SDRAM_MODE_REG(CHIP_SDRAM2_CAS_LATENCY)


    #define CHIP_SDRAM2_SIZE (\
                (1 << CHIP_SDRAM2_ROW_ADDR_WIDTH) *\
                (1 << CHIP_SDRAM2_COL_ADDR_WIDTH) *\
                (CHIP_SDRAM2_DATA_WIDTH >> 3) *\
                (1 << CHIP_SDRAM2_BA_WIDTH))
#else
    #define CHIP_SDRAM2_DATA_WIDTH      0
    #define CHIP_SDRAM2_BA_WIDTH        0
    #define CHIP_SDRAM2_ROW_ADDR_WIDTH  0
    #define CHIP_SDRAM2_COL_ADDR_WIDTH  0
    #define CHIP_SDRAM2_SIZE            0
    #define CHIP_RVAL_SDRAM2_MODE_REG   0
#endif




/* определение общих параметров для обоих SDRAM на основе параметров каждого */
#define CHIP_SDRAM_EN  (CHIP_SDRAM1_EN || CHIP_SDRAM2_EN)
#if CHIP_SDRAM1_DATA_WIDTH > CHIP_SDRAM2_DATA_WIDTH
    #define CHIP_SDRAM_DATA_WIDTH CHIP_SDRAM1_DATA_WIDTH
#else
    #define CHIP_SDRAM_DATA_WIDTH CHIP_SDRAM2_DATA_WIDTH
#endif
/* размер адреса столбца всегда меньше строки для действительных значений, поэтому
 * для определения кол-ва используемых пинов адреса SDRAM используем ROW_ADDR_WIDTH */
#if CHIP_SDRAM1_ROW_ADDR_WIDTH > CHIP_SDRAM2_ROW_ADDR_WIDTH
    #define CHIP_SDRAM_ADDR_WIDTH CHIP_SDRAM1_ROW_ADDR_WIDTH
#else
    #define CHIP_SDRAM_ADDR_WIDTH CHIP_SDRAM2_ROW_ADDR_WIDTH
#endif
#if CHIP_SDRAM1_BA_WIDTH > CHIP_SDRAM2_BA_WIDTH
    #define CHIP_SDRAM_BA_WIDTH CHIP_SDRAM1_BA_WIDTH
#else
    #define CHIP_SDRAM_BA_WIDTH CHIP_SDRAM2_BA_WIDTH
#endif

#if CHIP_SDRAM_EN
    #ifndef CHIP_CFG_SDRAM_PIN_WE
        #error SDRAM we pin is not defined
    #endif



    #ifndef CHIP_CFG_SDRAM_CLK_DIV
        #error SDRAM CLK_DIV is not defined!
    #else
        #if CHIP_CFG_SDRAM_CLK_DIV == 2
            #define CHIP_RVAL_SDCR_SDCLK 2
        #elif CHIP_CFG_SDRAM_CLK_DIV == 3
            #define CHIP_RVAL_SDCR_SDCLK 3
        #else
            #error Invalid SDRAM CLK_DIV value
        #endif
    #endif

    /* определение параметов, для которых задается общее значение для SDRAM1
     * и SDRAM2 */
    #if CHIP_SDRAM1_EN && CHIP_SDRAM2_EN
        #define CHIP_RVAL_SDTR_TRP          MAX(CHIP_RVAL_SDTR1_TRP, CHIP_RVAL_SDTR2_TRP)
        #define CHIP_RVAL_SDTR_TRC          MAX(CHIP_RVAL_SDTR1_TRC, CHIP_RVAL_SDTR2_TRC)
        #define CHIP_RVAL_SDTR_TXSR         MAX(CHIP_RVAL_SDTR1_TXSR, CHIP_RVAL_SDTR2_TXSR)
        #define CHIP_SDRAM_REF_TIME_MS      MAX(CHIP_SDRAM1_REF_TIME_MS, CHIP_SDRAM2_REF_TIME_MS)
        #define CHIP_SDRAM_POWERUP_TIME_US  MAX(CHIP_SDRAM1_POWERUP_TIME_US, CHIP_SDRAM2_POWERUP_TIME_US)
        #define CHIP_SDRAM_AUTOREF_CNT      MAX(CHIP_SDRAM1_AUTOREF_CNT, CHIP_SDRAM2_AUTOREF_CNT)
        #define CHIP_RVAL_SDTR_TWR          MAX(CHIP_RVAL_SDTR1_TWR, CHIP_RVAL_SDTR2_TWR)
        #define CHIP_RVAL_SDCMR_CTB         (CHIP_REGFLD_EXMC_SDCMR_CTB1 | CHIP_REGFLD_EXMC_SDCMR_CTB2)
        #if CHIP_SDRAM1_CAS_LATENCY == CHIP_SDRAM2_CAS_LATENCY
            #define CHIP_RVAL_SDRAM_MODE_REG    CHIP_RVAL_SDRAM1_MODE_REG
        #endif
    #elif CHIP_SDRAM1_EN
        #define CHIP_RVAL_SDTR_TRP          CHIP_RVAL_SDTR1_TRP
        #define CHIP_RVAL_SDTR_TRC          CHIP_RVAL_SDTR1_TRC
        #define CHIP_RVAL_SDTR_TXSR         CHIP_RVAL_SDTR1_TXSR
        #define CHIP_SDRAM_REF_TIME_MS      CHIP_SDRAM1_REF_TIME_MS
        #define CHIP_SDRAM_POWERUP_TIME_US  CHIP_SDRAM1_POWERUP_TIME_US
        #define CHIP_SDRAM_AUTOREF_CNT      CHIP_SDRAM1_AUTOREF_CNT
        #define CHIP_RVAL_SDTR_TWR          CHIP_RVAL_SDTR1_TWR
        #define CHIP_RVAL_SDCMR_CTB         CHIP_REGFLD_EXMC_SDCMR_CTB1
        #define CHIP_RVAL_SDRAM_MODE_REG    CHIP_RVAL_SDRAM1_MODE_REG
    #elif CHIP_SDRAM2_EN
        #define CHIP_RVAL_SDTR_TRP          CHIP_RVAL_SDTR2_TRP
        #define CHIP_RVAL_SDTR_TRC          CHIP_RVAL_SDTR2_TRC
        #define CHIP_RVAL_SDTR_TXSR         CHIP_RVAL_SDTR2_TXSR
        #define CHIP_SDRAM_REF_TIME_MS      CHIP_SDRAM2_REF_TIME_MS
        #define CHIP_SDRAM_POWERUP_TIME_US  CHIP_SDRAM2_POWERUP_TIME_US
        #define CHIP_SDRAM_AUTOREF_CNT      CHIP_SDRAM2_AUTOREF_CNT
        #define CHIP_RVAL_SDTR_TWR          CHIP_RVAL_SDTR2_TWR
        #define CHIP_RVAL_SDCMR_CTB         CHIP_REGFLD_EXMC_SDCMR_CTB2
        #define CHIP_RVAL_SDRAM_MODE_REG    CHIP_RVAL_SDRAM2_MODE_REG
    #endif

    #define CHIP_RVAL_SDCMR_NRFS            MAX(CHIP_SDRAM_AUTOREF_CNT-1, 0)

    #define CHIP_RVAL_SDRTR_COUNT (CHIP_SDRAM_REF_CYCLES(CHIP_SDRAM_REF_TIME_MS)/(1 << CHIP_CFG_SDRAM_ROW_ADDR_WIDTH) - 20)
    #if (CHIP_RVAL_SDRTR_COUNT < 1) || (CHIP_RVAL_SDRTR_COUNT > ((0x1UL << 12) - 1))
        #error Invalid SDRAM refresh time
    #endif
#endif



/* определение адресов SDRAM/NOR/PSRAM/NAND */
#ifdef CHIP_CFG_EXMC_BANK_MAP
    #define CHIP_EXMC_BANK_MAP  CHIP_CFG_EXMC_BANK_MAP
#else
    #define CHIP_EXMC_BANK_MAP  0
#endif

#define CHIP_NAND_ADDR                  CHIP_MEMRGN_ADDR_EXMC_BANK3
#if CHIP_EXMC_BANK_MAP == 0
    #define CHIP_NOR_PSRAM_ADDR         CHIP_MEMRGN_ADDR_EXMC_BANK1
    #define CHIP_SDRAM1_ADDR            CHIP_MEMRGN_ADDR_EXMC_BANK5
    #define CHIP_SDRAM2_ADDR            CHIP_MEMRGN_ADDR_EXMC_BANK6
    #define CHIP_EXMC_BANK2_SDRAM_NUM    1
#elif CHIP_EXMC_BANK_MAP == 1
    #define CHIP_NOR_PSRAM_ADDR         CHIP_MEMRGN_ADDR_EXMC_BANK5
    #define CHIP_SDRAM1_ADDR            CHIP_MEMRGN_ADDR_EXMC_BANK1
    #define CHIP_SDRAM2_ADDR            CHIP_MEMRGN_ADDR_EXMC_BANK2
    #define CHIP_EXMC_BANK2_SDRAM_NUM    2
#elif CHIP_EXMC_BANK_MAP == 2
    #define CHIP_NOR_PSRAM_ADDR         CHIP_MEMRGN_ADDR_EXMC_BANK1
    #define CHIP_SDRAM1_ADDR            CHIP_MEMRGN_ADDR_EXMC_BANK5
    #define CHIP_SDRAM2_ADDR            CHIP_MEMRGN_ADDR_EXMC_BANK6
    #define CHIP_EXMC_BANK2_SDRAM_NUM    2
#else
    #error Invalid EXMC Bank Map value
#endif

#endif // CHIP_EXMC_CFG_H
