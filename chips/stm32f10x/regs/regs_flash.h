#ifndef CHIP_STM32F10X_REGS_FLASH_H
#define CHIP_STM32F10X_REGS_FLASH_H

#include "chip_ioreg_defs.h"
#include "init/chip_clk_constraints.h"

#define CHIP_SUPPORT_FLASH_BANK2                0 /** @table XL devices */
#define CHIP_SUPPORT_FLASH_FORCE_OB_LOAD        0
#define CHIP_SUPPORT_FLASH_OPTION_NBOOT0        0
#define CHIP_SUPPORT_FLASH_OPTION_BOOT_SEL      0
#ifdef CHIP_DEV_GD32F10X
    #define CHIP_SUPPORT_FLASH_REG_WS_EN        1
    #define CHIP_SUPPORT_FLASH_REG_PID          1
#else
    #define CHIP_SUPPORT_FLASH_REG_WS_EN        0
    #define CHIP_SUPPORT_FLASH_REG_PID          0
#endif
#define CHIP_SUPPORT_FLASH_ECC                  0
#define CHIP_FLASH_CUSTOM_OPTIONS               0

#include "stm32_flash_v1.h"

#endif // CHIP_STM32F10X_REGS_FLASH_H
