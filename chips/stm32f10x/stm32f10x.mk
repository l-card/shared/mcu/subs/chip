CHIP_GEN_SRC += $(CHIP_TARGET_DIR)/init/chip_clk.c \
                $(CHIP_TARGET_DIR)/init/chip_pwr.c \
                $(CHIP_TARGET_DIR)/init/chip_ioremap.c \
                $(CHIP_TARGET_DIR)/init/chip_per_ctl.c \
                $(CHIP_SHARED_DIR)/stm32/stm32_iwdg.c \
                $(CHIP_SHARED_DIR)/stm32/stm32_flash_v1.c



CHIP_TARGET_DEVTYPE_DIR := $(CHIP_TARGET_DIR)/devs/$(CHIP_TARGET_DEVTYPE)
CHIP_TARGET_DEV_DIR := $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEV)
ifeq (,$(CHIP_TARGET_DEVTYPE))
    $(error CHIP_TARGET_DEVTYPE variable must be set to target cpu device type)
else ifeq (,$(wildcard $(CHIP_TARGET_DEVTYPE_DIR)/chip_devtype_spec_features.h))
    $(error $(CHIP_TARGET_DEVTYPE) is not supported device type)
endif

ifneq (,$(wildcard $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEVTYPE).mk))
    include $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEVTYPE).mk
endif

CHIP_TARGET_ARCHTYPE ?= cortexm
CHIP_TARGET_CORETYPE ?= cm3

CHIP_STARTUP_SRC += $(CHIP_TARGET_DIR)/chip_isr_table.c
CHIP_INC_DIRS += $(CHIP_TARGET_DEVTYPE_DIR) ${CHIP_TARGET_DEV_DIR}  $(CHIP_SHARED_DIR)/stm32

LPRINTF_TARGET := stm32_uart_v1
SPI_TARGET := stm32_spi_v1
