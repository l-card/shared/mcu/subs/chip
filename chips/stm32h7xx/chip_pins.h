#ifndef CHIP_PINS_H
#define CHIP_PINS_H

#include "chip_devtype_spec_features.h"

#define CHIP_PIN_USE_OSPEED_VHIGH

#include "chips/shared/stm32/chip_pin_defs_v2.h"
#include "chip_config.h"


#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4
#define CHIP_PORT_F 5
#define CHIP_PORT_G 6
#define CHIP_PORT_H 7
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PORT_I 8
#endif
#define CHIP_PORT_J 9
#define CHIP_PORT_K 10


/* ------------------------ Функции пинов ------------------------------------*/

/********************************** Порт A ************************************/
/*----- Пин PA0  ------------------------------------------------------------ */
#define CHIP_PIN_PA0_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 0, 0) */
#define CHIP_PIN_PA0_TIM2_CH1               CHIP_PIN_ID(CHIP_PORT_A, 0, 1)
#define CHIP_PIN_PA0_TIM2_ETR               CHIP_PIN_ID(CHIP_PORT_A, 0, 1)
#define CHIP_PIN_PA0_TIM5_CH1               CHIP_PIN_ID(CHIP_PORT_A, 0, 2)
#define CHIP_PIN_PA0_TIM8_ETR               CHIP_PIN_ID(CHIP_PORT_A, 0, 3)
#define CHIP_PIN_PA0_TIM15_BKIN             CHIP_PIN_ID(CHIP_PORT_A, 0, 4)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PA0_SPI6_NSS               CHIP_PIN_ID(CHIP_PORT_A, 0, 5)
#define CHIP_PIN_PA0_I2S6_WS                CHIP_PIN_ID(CHIP_PORT_A, 0, 5)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 0, 6) */
#define CHIP_PIN_PA0_USART2_CTS             CHIP_PIN_ID(CHIP_PORT_A, 0, 7)
#define CHIP_PIN_PA0_USART2_NSS             CHIP_PIN_ID(CHIP_PORT_A, 0, 7)
#define CHIP_PIN_PA0_UART4_TX               CHIP_PIN_ID(CHIP_PORT_A, 0, 8)
#define CHIP_PIN_PA0_SDMMC2_CMD             CHIP_PIN_ID(CHIP_PORT_A, 0, 9)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PA0_SAI2_SD_B              CHIP_PIN_ID(CHIP_PORT_A, 0, 10)
#else
#define CHIP_PIN_PA0_SAI4_SD_B              CHIP_PIN_ID(CHIP_PORT_A, 0, 10)
#endif
#define CHIP_PIN_PA0_ETH_MII_CRS            CHIP_PIN_ID(CHIP_PORT_A, 0, 11)
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA0_EXMC_A19               CHIP_PIN_ID(CHIP_PORT_A, 0, 12)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 0, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 0, 14) */
#define CHIP_PIN_PA0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 0, 15)
#define CHIP_PIN_PA0_ADC1_INP16             CHIP_PIN_ID(CHIP_PORT_A, 0, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PA0C_ADC1_2_INN1           CHIP_PIN_ID(CHIP_PORT_A, 0, CHIP_PIN_FUNC_ANALOG) /* PA0_C - ANALOG ONLY */
#define CHIP_PIN_PA0C_ADC1_2_INP0           CHIP_PIN_ID(CHIP_PORT_A, 0, CHIP_PIN_FUNC_ANALOG) /* PA0_C - ANALOG ONLY */


/*----- Пин PA1  ------------------------------------------------------------ */
#define CHIP_PIN_PA1_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 1, 0) */
#define CHIP_PIN_PA1_TIM2_CH2               CHIP_PIN_ID(CHIP_PORT_A, 1, 1)
#define CHIP_PIN_PA1_TIM5_CH2               CHIP_PIN_ID(CHIP_PORT_A, 1, 2)
#define CHIP_PIN_PA1_LPTIM3_OUT             CHIP_PIN_ID(CHIP_PORT_A, 1, 3)
#define CHIP_PIN_PA1_TIM15_CH1N             CHIP_PIN_ID(CHIP_PORT_A, 1, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 1, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 1, 6) */
#define CHIP_PIN_PA1_USART2_RTS             CHIP_PIN_ID(CHIP_PORT_A, 1, 7)
#define CHIP_PIN_PA1_USART2_DE              CHIP_PIN_ID(CHIP_PORT_A, 1, 7)
#define CHIP_PIN_PA1_UART4_RX               CHIP_PIN_ID(CHIP_PORT_A, 1, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PA1_OCTOSPIM_P1_IO3        CHIP_PIN_ID(CHIP_PORT_A, 1, 9)
#else
#define CHIP_PIN_PA1_QUADSPI_BK1_IO3        CHIP_PIN_ID(CHIP_PORT_A, 1, 9)
#endif
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PA1_SAI2_MCK_B             CHIP_PIN_ID(CHIP_PORT_A, 1, 10)
#else
#define CHIP_PIN_PA1_SAI4_MCK_B             CHIP_PIN_ID(CHIP_PORT_A, 1, 10)
#endif
#define CHIP_PIN_PA1_ETH_MII_RX_CLK         CHIP_PIN_ID(CHIP_PORT_A, 1, 11)
#define CHIP_PIN_PA1_ETH_RMII_REF_CLK       CHIP_PIN_ID(CHIP_PORT_A, 1, 11)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PA1_OCTOSPIM_P1_DQS        CHIP_PIN_ID(CHIP_PORT_A, 1, 12)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 1, 13) */
#define CHIP_PIN_PA1_LCD_R2                 CHIP_PIN_ID(CHIP_PORT_A, 1, 14)
#define CHIP_PIN_PA1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 1, 15)
#define CHIP_PIN_PA1_ADC1_INN16             CHIP_PIN_ID(CHIP_PORT_A, 1, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PA1_ADC1_INP17             CHIP_PIN_ID(CHIP_PORT_A, 1, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PA1C_ADC1_2_INP1           CHIP_PIN_ID(CHIP_PORT_A, 1, CHIP_PIN_FUNC_ANALOG) /* PA1_C - ANALOG ONLY */


/*----- Пин PA2  ------------------------------------------------------------ */
#define CHIP_PIN_PA2_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 2, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 2, 0) */
#define CHIP_PIN_PA2_TIM2_CH3               CHIP_PIN_ID(CHIP_PORT_A, 2, 1)
#define CHIP_PIN_PA2_TIM5_CH3               CHIP_PIN_ID(CHIP_PORT_A, 2, 2)
#define CHIP_PIN_PA2_LPTIM4_OUT             CHIP_PIN_ID(CHIP_PORT_A, 2, 3)
#define CHIP_PIN_PA2_TIM15_CH1              CHIP_PIN_ID(CHIP_PORT_A, 2, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 2, 5) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PA2_OCTOSPIM_P1_IO0        CHIP_PIN_ID(CHIP_PORT_A, 2, 6)
#endif
#define CHIP_PIN_PA2_USART2_TX              CHIP_PIN_ID(CHIP_PORT_A, 2, 7)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PA2_SAI2_SCK_B             CHIP_PIN_ID(CHIP_PORT_A, 2, 8)
#else
#define CHIP_PIN_PA2_SAI4_SCK_B             CHIP_PIN_ID(CHIP_PORT_A, 2, 8)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 2, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 2, 10) */
#define CHIP_PIN_PA2_ETH_MDIO               CHIP_PIN_ID(CHIP_PORT_A, 2, 11)
#define CHIP_PIN_PA2_MDIOS_MDIO             CHIP_PIN_ID(CHIP_PORT_A, 2, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 2, 13) */
#define CHIP_PIN_PA2_LCD_R1                 CHIP_PIN_ID(CHIP_PORT_A, 2, 14)
#define CHIP_PIN_PA2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 2, 15)
#define CHIP_PIN_PA2_ADC1_2_INP14           CHIP_PIN_ID(CHIP_PORT_A, 2, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PA3  ------------------------------------------------------------ */
#define CHIP_PIN_PA3_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 3, 0) */
#define CHIP_PIN_PA3_TIM2_CH4               CHIP_PIN_ID(CHIP_PORT_A, 3, 1)
#define CHIP_PIN_PA3_TIM5_CH4               CHIP_PIN_ID(CHIP_PORT_A, 3, 2)
#define CHIP_PIN_PA3_LPTIM5_OUT             CHIP_PIN_ID(CHIP_PORT_A, 3, 3)
#define CHIP_PIN_PA3_TIM15_CH2              CHIP_PIN_ID(CHIP_PORT_A, 3, 4)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PA3_I2S6_MCK               CHIP_PIN_ID(CHIP_PORT_A, 3, 5)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PA3_OCTOSPIM_P1_IO2        CHIP_PIN_ID(CHIP_PORT_A, 3, 6)
#endif
#define CHIP_PIN_PA3_USART2_RX              CHIP_PIN_ID(CHIP_PORT_A, 3, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 3, 8) */
#define CHIP_PIN_PA3_LCD_B2                 CHIP_PIN_ID(CHIP_PORT_A, 3, 9)
#define CHIP_PIN_PA3_OTG_HS_ULPI_D0         CHIP_PIN_ID(CHIP_PORT_A, 3, 10)
#define CHIP_PIN_PA3_ETH_MII_COL            CHIP_PIN_ID(CHIP_PORT_A, 3, 11)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PA3_OCTOSPIM_P1_CLK        CHIP_PIN_ID(CHIP_PORT_A, 3, 12)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 3, 13) */
#define CHIP_PIN_PA3_LCD_B5                 CHIP_PIN_ID(CHIP_PORT_A, 3, 14)
#define CHIP_PIN_PA3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 3, 15)
#define CHIP_PIN_PA3_ADC1_2_INP15           CHIP_PIN_ID(CHIP_PORT_A, 3, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PA4  ------------------------------------------------------------ */
#define CHIP_PIN_PA4_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 4, CHIP_PIN_FUNC_GPIO)
#if !CHIP_REV_Y_COMPAT_EN
#define CHIP_PIN_PA4_D1PWREN                CHIP_PIN_ID(CHIP_PORT_A, 4, 0)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 4, 1) */
#define CHIP_PIN_PA4_TIM5_ETR               CHIP_PIN_ID(CHIP_PORT_A, 4, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 4, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 4, 4) */
#define CHIP_PIN_PA4_SPI1_NSS               CHIP_PIN_ID(CHIP_PORT_A, 4, 5)
#define CHIP_PIN_PA4_I2S1_WS                CHIP_PIN_ID(CHIP_PORT_A, 4, 5)
#define CHIP_PIN_PA4_SPI3_NSS               CHIP_PIN_ID(CHIP_PORT_A, 4, 6)
#define CHIP_PIN_PA4_I2S3_WS                CHIP_PIN_ID(CHIP_PORT_A, 4, 6)
#define CHIP_PIN_PA4_USART2_CK              CHIP_PIN_ID(CHIP_PORT_A, 4, 7)
#define CHIP_PIN_PA4_SPI6_NSS               CHIP_PIN_ID(CHIP_PORT_A, 4, 8)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PA4_I2S6_WS                CHIP_PIN_ID(CHIP_PORT_A, 4, 8)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 4, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 4, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 4, 11) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA4_EXMC_D8                CHIP_PIN_ID(CHIP_PORT_A, 4, 12)
#define CHIP_PIN_PA4_EXMC_DA8               CHIP_PIN_ID(CHIP_PORT_A, 4, 12)
#else
#define CHIP_PIN_PA4_OTG_HS_SOF             CHIP_PIN_ID(CHIP_PORT_A, 4, 12)
#endif
#define CHIP_PIN_PA4_DVP_HSYNC              CHIP_PIN_ID(CHIP_PORT_A, 4, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PA4_PSSI_DE                CHIP_PIN_ID(CHIP_PORT_A, 4, 13)
#endif
#define CHIP_PIN_PA4_LCD_VSYNC              CHIP_PIN_ID(CHIP_PORT_A, 4, 14)
#define CHIP_PIN_PA4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 4, 15)
#define CHIP_PIN_PA4_ADC1_2_INP18           CHIP_PIN_ID(CHIP_PORT_A, 4, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PA5 ------------------------------------------------------------- */
#define CHIP_PIN_PA5_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 5, CHIP_PIN_FUNC_GPIO)
#if !CHIP_REV_Y_COMPAT_EN
#define CHIP_PIN_PA5_D2PWREN                CHIP_PIN_ID(CHIP_PORT_A, 5, 0)
#endif
#define CHIP_PIN_PA5_TIM2_CH1               CHIP_PIN_ID(CHIP_PORT_A, 5, 1)
#define CHIP_PIN_PA5_TIM2_ETR               CHIP_PIN_ID(CHIP_PORT_A, 5, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 5, 2) */
#define CHIP_PIN_PA5_TIM8_CH1N              CHIP_PIN_ID(CHIP_PORT_A, 5, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 5, 4) */
#define CHIP_PIN_PA5_SPI1_SCK               CHIP_PIN_ID(CHIP_PORT_A, 5, 5)
#define CHIP_PIN_PA5_I2S1_CK                CHIP_PIN_ID(CHIP_PORT_A, 5, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 5, 7) */
#define CHIP_PIN_PA5_SPI6_SCK               CHIP_PIN_ID(CHIP_PORT_A, 5, 8)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PA5_I2S6_CK                CHIP_PIN_ID(CHIP_PORT_A, 5, 8)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 5, 9) */
#define CHIP_PIN_PA5_OTG_HS_ULPI_CK         CHIP_PIN_ID(CHIP_PORT_A, 5, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 5, 11) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA5_EXMC_D9                CHIP_PIN_ID(CHIP_PORT_A, 5, 12)
#define CHIP_PIN_PA5_EXMC_AD9               CHIP_PIN_ID(CHIP_PORT_A, 5, 12)
#endif
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PA5_PSSI_D14               CHIP_PIN_ID(CHIP_PORT_A, 5, 13)
#endif
#define CHIP_PIN_PA5_LCD_R4                 CHIP_PIN_ID(CHIP_PORT_A, 5, 14)
#define CHIP_PIN_PA5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 5, 15)
#define CHIP_PIN_PA5_ADC1_2_INN18           CHIP_PIN_ID(CHIP_PORT_A, 5, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PA5_ADC1_2_INP19           CHIP_PIN_ID(CHIP_PORT_A, 5, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PA6  ------------------------------------------------------------ */
#define CHIP_PIN_PA6_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 6, 0) */
#define CHIP_PIN_PA6_TIM1_BKIN              CHIP_PIN_ID(CHIP_PORT_A, 6, 1)
#define CHIP_PIN_PA6_TIM3_CH1               CHIP_PIN_ID(CHIP_PORT_A, 6, 2)
#define CHIP_PIN_PA6_TIM8_BKIN              CHIP_PIN_ID(CHIP_PORT_A, 6, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 6, 4) */
#define CHIP_PIN_PA6_SPI1_MISO              CHIP_PIN_ID(CHIP_PORT_A, 6, 5)
#define CHIP_PIN_PA6_I2S1_SDI               CHIP_PIN_ID(CHIP_PORT_A, 6, 5)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PA6_OCTOSPIM_P1_IO3        CHIP_PIN_ID(CHIP_PORT_A, 6, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 6, 7) */
#define CHIP_PIN_PA6_SPI6_MISO              CHIP_PIN_ID(CHIP_PORT_A, 6, 8)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PA6_I2S6_MDI               CHIP_PIN_ID(CHIP_PORT_A, 6, 8)
#endif
#define CHIP_PIN_PA6_TIM13_CH1              CHIP_PIN_ID(CHIP_PORT_A, 6, 9)
#define CHIP_PIN_PA6_TIM8_BKIN_COMP12       CHIP_PIN_ID(CHIP_PORT_A, 6, 10)
#define CHIP_PIN_PA6_MDIOS_MDC              CHIP_PIN_ID(CHIP_PORT_A, 6, 11)
#define CHIP_PIN_PA6_TIM1_BKIN_COMP12       CHIP_PIN_ID(CHIP_PORT_A, 6, 12)
#define CHIP_PIN_PA6_DVP_PIX_CLK            CHIP_PIN_ID(CHIP_PORT_A, 6, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PA6_PSSI_PDCK              CHIP_PIN_ID(CHIP_PORT_A, 6, 13)
#endif
#define CHIP_PIN_PA6_LCD_G2                 CHIP_PIN_ID(CHIP_PORT_A, 6, 14)
#define CHIP_PIN_PA6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 6, 15)
#define CHIP_PIN_PA6_ADC1_2_INP3            CHIP_PIN_ID(CHIP_PORT_A, 6, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PA7  ------------------------------------------------------------ */
#define CHIP_PIN_PA7_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 7, 0) */
#define CHIP_PIN_PA7_TIM1_CH1N              CHIP_PIN_ID(CHIP_PORT_A, 7, 1)
#define CHIP_PIN_PA7_TIM3_CH2               CHIP_PIN_ID(CHIP_PORT_A, 7, 2)
#define CHIP_PIN_PA7_TIM8_CH1N              CHIP_PIN_ID(CHIP_PORT_A, 7, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 7, 4) */
#define CHIP_PIN_PA7_SPI1_MOSI              CHIP_PIN_ID(CHIP_PORT_A, 7, 5)
#define CHIP_PIN_PA7_I2S1_SDO               CHIP_PIN_ID(CHIP_PORT_A, 7, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 7, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 7, 7) */
#define CHIP_PIN_PA7_SPI6_MOSI              CHIP_PIN_ID(CHIP_PORT_A, 7, 8)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PA7_I2S6_SDO               CHIP_PIN_ID(CHIP_PORT_A, 7, 8)
#endif
#define CHIP_PIN_PA7_TIM14_CH1              CHIP_PIN_ID(CHIP_PORT_A, 7, 9)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PA7_OCTOSPIM_P1_IO2        CHIP_PIN_ID(CHIP_PORT_A, 7, 10)
#endif
#define CHIP_PIN_PA7_ETH_MII_RX_DV          CHIP_PIN_ID(CHIP_PORT_A, 7, 11)
#define CHIP_PIN_PA7_ETH_RMII_CRS_DV        CHIP_PIN_ID(CHIP_PORT_A, 7, 11)
#define CHIP_PIN_PA7_EXMC_SDN_WE            CHIP_PIN_ID(CHIP_PORT_A, 7, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 7, 13) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA7_LCD_VSYNC              CHIP_PIN_ID(CHIP_PORT_A, 7, 14)
#endif
#define CHIP_PIN_PA7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 7, 15)
#define CHIP_PIN_PA7_ADC1_2_INN3            CHIP_PIN_ID(CHIP_PORT_A, 7, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PA7_ADC1_2_INP7            CHIP_PIN_ID(CHIP_PORT_A, 7, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PA8  ------------------------------------------------------------ */
#define CHIP_PIN_PA8_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 8, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PA8_MCO1                   CHIP_PIN_ID(CHIP_PORT_A, 8, 0)
#define CHIP_PIN_PA8_TIM1_CH1               CHIP_PIN_ID(CHIP_PORT_A, 8, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PA8_HRTIM_CHB2             CHIP_PIN_ID(CHIP_PORT_A, 8, 2)
#endif
#define CHIP_PIN_PA8_TIM8_BKIN2             CHIP_PIN_ID(CHIP_PORT_A, 8, 3)
#define CHIP_PIN_PA8_I2C3_SCL               CHIP_PIN_ID(CHIP_PORT_A, 8, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 8, 5) */
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PA8_I2C5_SCL               CHIP_PIN_ID(CHIP_PORT_A, 8, 6)
#endif
#define CHIP_PIN_PA8_USART1_CK              CHIP_PIN_ID(CHIP_PORT_A, 8, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 8, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 8, 9) */
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_PIN_PA8_OTG_FS_SOF             CHIP_PIN_ID(CHIP_PORT_A, 8, 10)
#else
#define CHIP_PIN_PA8_OTG_HS_SOF             CHIP_PIN_ID(CHIP_PORT_A, 8, 10)
#endif
#define CHIP_PIN_PA8_UART7_RX               CHIP_PIN_ID(CHIP_PORT_A, 8, 11)
#define CHIP_PIN_PA8_TIM8_BKIN2_COMP12      CHIP_PIN_ID(CHIP_PORT_A, 8, 12)
#define CHIP_PIN_PA8_LCD_B3                 CHIP_PIN_ID(CHIP_PORT_A, 8, 13)
#define CHIP_PIN_PA8_LCD_R6                 CHIP_PIN_ID(CHIP_PORT_A, 8, 14)
#define CHIP_PIN_PA8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 8, 15)


/*----- Пин PA9  ------------------------------------------------------------ */
#define CHIP_PIN_PA9_GPIO                   CHIP_PIN_ID(CHIP_PORT_A, 9, CHIP_PIN_FUNC_GPIO)
/* -                                        CHIP_PIN_ID(CHIP_PORT_A, 9, 0) */
#define CHIP_PIN_PA9_TIM1_CH2               CHIP_PIN_ID(CHIP_PORT_A, 9, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PA9_HRTIM_CHC1             CHIP_PIN_ID(CHIP_PORT_A, 9, 2)
#endif
#define CHIP_PIN_PA9_LPUART1_TX             CHIP_PIN_ID(CHIP_PORT_A, 9, 3)
#define CHIP_PIN_PA9_I2C3_SMBA              CHIP_PIN_ID(CHIP_PORT_A, 9, 4)
#define CHIP_PIN_PA9_SPI2_SCK               CHIP_PIN_ID(CHIP_PORT_A, 9, 5)
#define CHIP_PIN_PA9_I2S2_CK                CHIP_PIN_ID(CHIP_PORT_A, 9, 5)
/* -                                        CHIP_PIN_ID(CHIP_PORT_A, 9, 6) */
#define CHIP_PIN_PA9_USART1_TX              CHIP_PIN_ID(CHIP_PORT_A, 9, 7)
/* -                                        CHIP_PIN_ID(CHIP_PORT_A, 9, 7) */
#if !CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PA9_FDCAN1_RXFD_MODE       CHIP_PIN_ID(CHIP_PORT_A, 9, 9)
#endif
/* -                                        CHIP_PIN_ID(CHIP_PORT_A, 9, 8) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA9_ETH_TX_ER              CHIP_PIN_ID(CHIP_PORT_A, 9, 11)
#endif
/* -                                        CHIP_PIN_ID(CHIP_PORT_A, 9, 12) */
#define CHIP_PIN_PA9_DVP_D0                 CHIP_PIN_ID(CHIP_PORT_A, 9, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PA9_PSSI_D0                CHIP_PIN_ID(CHIP_PORT_A, 9, 13)
#endif
#define CHIP_PIN_PA9_LCD_R5                 CHIP_PIN_ID(CHIP_PORT_A, 9, 14)
#define CHIP_PIN_PA9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_A, 9, 15)


/*----- Пин PA10 ------------------------------------------------------------ */
#define CHIP_PIN_PA10_GPIO                  CHIP_PIN_ID(CHIP_PORT_A, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 10, 0) */
#define CHIP_PIN_PA10_TIM1_CH3              CHIP_PIN_ID(CHIP_PORT_A, 10, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PA10_HRTIM_CHC2            CHIP_PIN_ID(CHIP_PORT_A, 10, 2)
#endif
#define CHIP_PIN_PA10_LPUART1_RX            CHIP_PIN_ID(CHIP_PORT_A, 10, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 10, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 10, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 10, 6) */
#define CHIP_PIN_PA10_USART1_RX             CHIP_PIN_ID(CHIP_PORT_A, 10, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 10, 8) */
#if !CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PA10_FDCAN1_TXFD_MODE      CHIP_PIN_ID(CHIP_PORT_A, 10, 9)
#endif
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_PIN_PA10_OTG_FS_ID             CHIP_PIN_ID(CHIP_PORT_A, 10, 10)
#else
#define CHIP_PIN_PA10_OTG_HS_ID             CHIP_PIN_ID(CHIP_PORT_A, 10, 10)
#endif
#define CHIP_PIN_PA10_MDIOS_MDIO            CHIP_PIN_ID(CHIP_PORT_A, 10, 11)
#define CHIP_PIN_PA10_LCD_B4                CHIP_PIN_ID(CHIP_PORT_A, 10, 12)
#define CHIP_PIN_PA10_DVP_D1                CHIP_PIN_ID(CHIP_PORT_A, 10, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PA10_PSSI_D1               CHIP_PIN_ID(CHIP_PORT_A, 10, 13)
#endif
#define CHIP_PIN_PA10_LCD_B1                CHIP_PIN_ID(CHIP_PORT_A, 10, 14)
#define CHIP_PIN_PA10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_A, 10, 15)


/*----- Пин PA11 ------------------------------------------------------------ */
#define CHIP_PIN_PA11_GPIO                  CHIP_PIN_ID(CHIP_PORT_A, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 11, 0) */
#define CHIP_PIN_PA11_TIM1_CH4              CHIP_PIN_ID(CHIP_PORT_A, 11, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PA11_HRTIM_CHD1            CHIP_PIN_ID(CHIP_PORT_A, 11, 2)
#endif
#define CHIP_PIN_PA11_LPUART1_CTS           CHIP_PIN_ID(CHIP_PORT_A, 11, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 11, 4) */
#define CHIP_PIN_PA11_SPI2_NSS              CHIP_PIN_ID(CHIP_PORT_A, 11, 5)
#define CHIP_PIN_PA11_I2S2_WS               CHIP_PIN_ID(CHIP_PORT_A, 11, 5)
#define CHIP_PIN_PA11_UART4_RX              CHIP_PIN_ID(CHIP_PORT_A, 11, 6)
#define CHIP_PIN_PA11_USART1_CTS            CHIP_PIN_ID(CHIP_PORT_A, 11, 7)
#define CHIP_PIN_PA11_USART1_NSS            CHIP_PIN_ID(CHIP_PORT_A, 11, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 11, 8) */
#define CHIP_PIN_PA11_FDCAN1_RX             CHIP_PIN_ID(CHIP_PORT_A, 11, 9)
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_PIN_PA11_OTG_FS_DM             CHIP_PIN_ID(CHIP_PORT_A, 11, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 11, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 11, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 11, 13) */
#define CHIP_PIN_PA11_LCD_R4                CHIP_PIN_ID(CHIP_PORT_A, 11, 14)
#define CHIP_PIN_PA11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_A, 11, 15)


/*----- Пин PA12 ------------------------------------------------------------ */
#define CHIP_PIN_PA12_GPIO                  CHIP_PIN_ID(CHIP_PORT_A, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 12, 0) */
#define CHIP_PIN_PA12_TIM1_ETR              CHIP_PIN_ID(CHIP_PORT_A, 12, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PA12_HRTIM_CHD2            CHIP_PIN_ID(CHIP_PORT_A, 12, 2)
#endif
#define CHIP_PIN_PA12_LPUART1_RTS           CHIP_PIN_ID(CHIP_PORT_A, 12, 3)
#define CHIP_PIN_PA12_LPUART1_DE            CHIP_PIN_ID(CHIP_PORT_A, 12, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 12, 4) */
#define CHIP_PIN_PA12_SPI2_SCK              CHIP_PIN_ID(CHIP_PORT_A, 12, 5)
#define CHIP_PIN_PA12_I2S2_CK               CHIP_PIN_ID(CHIP_PORT_A, 12, 5)
#define CHIP_PIN_PA12_UART4_TX              CHIP_PIN_ID(CHIP_PORT_A, 12, 6)
#define CHIP_PIN_PA12_USART1_RTS            CHIP_PIN_ID(CHIP_PORT_A, 12, 7)
#define CHIP_PIN_PA12_USART1_DE             CHIP_PIN_ID(CHIP_PORT_A, 12, 7)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PA12_SAI2_FS_B             CHIP_PIN_ID(CHIP_PORT_A, 12, 8)
#else
#define CHIP_PIN_PA12_SAI4_FS_B             CHIP_PIN_ID(CHIP_PORT_A, 12, 8)
#endif
#define CHIP_PIN_PA12_FDCAN1_TX             CHIP_PIN_ID(CHIP_PORT_A, 12, 9)
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_PIN_PA12_OTG_FS_DP             CHIP_PIN_ID(CHIP_PORT_A, 12, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 12, 11) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA12_TIM1_BKIN2            CHIP_PIN_ID(CHIP_PORT_A, 12, 12)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 12, 13) */
#define CHIP_PIN_PA12_LCD_R5                CHIP_PIN_ID(CHIP_PORT_A, 12, 14)
#define CHIP_PIN_PA12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_A, 12, 15)


/*----- Пин PA13 ------------------------------------------------------------ */
#define CHIP_PIN_PA13_GPIO                  CHIP_PIN_ID(CHIP_PORT_A, 13, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PA13_JTMS_SWDIO            CHIP_PIN_ID(CHIP_PORT_A, 13, 0)
#define CHIP_PIN_PA13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_A, 13, 15)


/*----- Пин PA14 ------------------------------------------------------------ */
#define CHIP_PIN_PA14_GPIO                  CHIP_PIN_ID(CHIP_PORT_A, 14, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PA14_JTCK_SWCLK            CHIP_PIN_ID(CHIP_PORT_A, 14, 0)
#define CHIP_PIN_PA14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_A, 14, 15)


/*----- Пин PA15 ------------------------------------------------------------ */
#define CHIP_PIN_PA15_GPIO                  CHIP_PIN_ID(CHIP_PORT_A, 15, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PA15_JTDI                  CHIP_PIN_ID(CHIP_PORT_A, 15, 0)
#define CHIP_PIN_PA15_TIM2_CH1              CHIP_PIN_ID(CHIP_PORT_A, 15, 1)
#define CHIP_PIN_PA15_TIM2_ETR              CHIP_PIN_ID(CHIP_PORT_A, 15, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PA15_HRTIM_FLT1            CHIP_PIN_ID(CHIP_PORT_A, 15, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 15, 3)  */
#define CHIP_PIN_PA15_HDMI_CEC              CHIP_PIN_ID(CHIP_PORT_A, 15, 4)
#define CHIP_PIN_PA15_SPI1_NSS              CHIP_PIN_ID(CHIP_PORT_A, 15, 5)
#define CHIP_PIN_PA15_I2S1_WS               CHIP_PIN_ID(CHIP_PORT_A, 15, 5)
#define CHIP_PIN_PA15_SPI3_NSS              CHIP_PIN_ID(CHIP_PORT_A, 15, 6)
#define CHIP_PIN_PA15_I2S3_WS               CHIP_PIN_ID(CHIP_PORT_A, 15, 6)
#define CHIP_PIN_PA15_SPI6_NSS              CHIP_PIN_ID(CHIP_PORT_A, 15, 7)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PA15_I2S6_WS               CHIP_PIN_ID(CHIP_PORT_A, 15, 7)
#endif
#define CHIP_PIN_PA15_UART4_RTS             CHIP_PIN_ID(CHIP_PORT_A, 15, 8)
#define CHIP_PIN_PA15_UART4_DE              CHIP_PIN_ID(CHIP_PORT_A, 15, 8)
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA15_LCD_R3                CHIP_PIN_ID(CHIP_PORT_A, 15, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 15, 10) */
#define CHIP_PIN_PA15_UART7_TX              CHIP_PIN_ID(CHIP_PORT_A, 15, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 15, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_A, 15, 13) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PA15_LCD_B6                CHIP_PIN_ID(CHIP_PORT_A, 15, 14)
#endif
#define CHIP_PIN_PA15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_A, 15, 15)


/*********************************** Порт B ***********************************/
/*----- Пин PB0  ------------------------------------------------------------ */
#define CHIP_PIN_PB0_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 0, 0) */
#define CHIP_PIN_PB0_TIM1_CH2N              CHIP_PIN_ID(CHIP_PORT_B, 0, 1)
#define CHIP_PIN_PB0_TIM3_CH3               CHIP_PIN_ID(CHIP_PORT_B, 0, 2)
#define CHIP_PIN_PB0_TIM8_CH2N              CHIP_PIN_ID(CHIP_PORT_B, 0, 3)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB0_OCTOSPIM_P1_IO1        CHIP_PIN_ID(CHIP_PORT_B, 0, 4)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 0, 5) */
#define CHIP_PIN_PB0_DFSDM1_CKOUT           CHIP_PIN_ID(CHIP_PORT_B, 0, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 0, 7) */
#define CHIP_PIN_PB0_UART4_CTS              CHIP_PIN_ID(CHIP_PORT_B, 0, 8)
#define CHIP_PIN_PB0_LCD_R3                 CHIP_PIN_ID(CHIP_PORT_B, 0, 9)
#define CHIP_PIN_PB0_OTG_HS_ULPI_D1         CHIP_PIN_ID(CHIP_PORT_B, 0, 10)
#define CHIP_PIN_PB0_ETH_MII_RXD2           CHIP_PIN_ID(CHIP_PORT_B, 0, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 0, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 0, 13) */
#define CHIP_PIN_PB0_LCD_G1                 CHIP_PIN_ID(CHIP_PORT_B, 0, 14)
#define CHIP_PIN_PB0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 0, 15)
#define CHIP_PIN_PB0_ADC1_2_INN5            CHIP_PIN_ID(CHIP_PORT_B, 0, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PB0_ADC1_2_INP9            CHIP_PIN_ID(CHIP_PORT_B, 0, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PB1  ------------------------------------------------------------ */
#define CHIP_PIN_PB1_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 1, 0) */
#define CHIP_PIN_PB1_TIM1_CH3N              CHIP_PIN_ID(CHIP_PORT_B, 1, 1)
#define CHIP_PIN_PB1_TIM3_CH4               CHIP_PIN_ID(CHIP_PORT_B, 1, 2)
#define CHIP_PIN_PB1_TIM8_CH3N              CHIP_PIN_ID(CHIP_PORT_B, 1, 3)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB1_OCTOSPIM_P1_IO0        CHIP_PIN_ID(CHIP_PORT_B, 1, 4)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 1, 5) */
#define CHIP_PIN_PB1_DFSDM1_DATIN1          CHIP_PIN_ID(CHIP_PORT_B, 1, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 1, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 1, 8) */
#define CHIP_PIN_PB1_LCD_R6                 CHIP_PIN_ID(CHIP_PORT_B, 1, 9)
#define CHIP_PIN_PB1_OTG_HS_ULPI_D2         CHIP_PIN_ID(CHIP_PORT_B, 1, 10)
#define CHIP_PIN_PB1_ETH_MII_RXD3           CHIP_PIN_ID(CHIP_PORT_B, 1, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 1, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 1, 13) */
#define CHIP_PIN_PB1_LCD_G0                 CHIP_PIN_ID(CHIP_PORT_B, 1, 14)
#define CHIP_PIN_PB1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 1, 15)
#define CHIP_PIN_PB1_ADC1_2_INP5            CHIP_PIN_ID(CHIP_PORT_B, 1, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PB2  ------------------------------------------------------------ */
#define CHIP_PIN_PB2_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 2, CHIP_PIN_FUNC_GPIO)
#if !CHIP_REV_Y_COMPAT_EN
#define CHIP_PIN_PB2_RTC_OUT                CHIP_PIN_ID(CHIP_PORT_B, 2, 0)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB2_SAI4_D1                CHIP_PIN_ID(CHIP_PORT_B, 2, 1) /* переназначено с функции 10, на которой вывыод OCTOSPI */
#endif
#define CHIP_PIN_PB2_SAI1_D1                CHIP_PIN_ID(CHIP_PORT_B, 2, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 2, 3) */
#define CHIP_PIN_PB2_DFSDM1_CKIN1           CHIP_PIN_ID(CHIP_PORT_B, 2, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 2, 5) */
#define CHIP_PIN_PB2_SAI1_SD_A              CHIP_PIN_ID(CHIP_PORT_B, 2, 6)
#define CHIP_PIN_PB2_SPI3_MOSI              CHIP_PIN_ID(CHIP_PORT_B, 2, 7)
#define CHIP_PIN_PB2_I2S3_SDO               CHIP_PIN_ID(CHIP_PORT_B, 2, 7)
#define CHIP_PIN_PB2_SAI4_SD_A              CHIP_PIN_ID(CHIP_PORT_B, 2, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB2_OCTOSPIM_P1_CLK        CHIP_PIN_ID(CHIP_PORT_B, 2, 9)
#else
#define CHIP_PIN_PB2_QUADSPI_CLK            CHIP_PIN_ID(CHIP_PORT_B, 2, 9)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB2_OCTOSPIM_P1_DQS        CHIP_PIN_ID(CHIP_PORT_B, 2, 10)
#else
#define CHIP_PIN_PB2_SAI4_D1                CHIP_PIN_ID(CHIP_PORT_B, 2, 10)
#endif
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PB2_ETH_TX_ER              CHIP_PIN_ID(CHIP_PORT_B, 2, 11)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 2, 12) */
#if CHIP_DEV_SUPPORT_TIM23_24
 #define CHIP_PIN_PB2_TIM23_ETR             CHIP_PIN_ID(CHIP_PORT_B, 2, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 2, 14) */
#define CHIP_PIN_PB2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 2, 15)


/*----- Пин PB3  ------------------------------------------------------------ */
#define CHIP_PIN_PB3_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 3, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PB3_JTDO                   CHIP_PIN_ID(CHIP_PORT_B, 3, 0)
#define CHIP_PIN_PB3_TRACESWO               CHIP_PIN_ID(CHIP_PORT_B, 3, 0)
#define CHIP_PIN_PB3_TIM2_CH2               CHIP_PIN_ID(CHIP_PORT_B, 3, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PB3_HRTIM_FLT4             CHIP_PIN_ID(CHIP_PORT_B, 3, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 3, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 3, 4) */
#define CHIP_PIN_PB3_SPI1_SCK               CHIP_PIN_ID(CHIP_PORT_B, 3, 5)
#define CHIP_PIN_PB3_I2S1_CK                CHIP_PIN_ID(CHIP_PORT_B, 3, 5)
#define CHIP_PIN_PB3_SPI3_SCK               CHIP_PIN_ID(CHIP_PORT_B, 3, 6)
#define CHIP_PIN_PB3_I2S3_CK                CHIP_PIN_ID(CHIP_PORT_B, 3, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 3, 7) */
#define CHIP_PIN_PB3_SPI6_SCK               CHIP_PIN_ID(CHIP_PORT_B, 3, 8)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PB3_I2S6_CK                CHIP_PIN_ID(CHIP_PORT_B, 3, 8)
#endif
#define CHIP_PIN_PB3_SDMMC2_D2              CHIP_PIN_ID(CHIP_PORT_B, 3, 9)
#if !CHIP_REV_Y_COMPAT_EN
#define CHIP_PIN_PB3_CRS_SYNC               CHIP_PIN_ID(CHIP_PORT_B, 3, 10)
#endif
#define CHIP_PIN_PB3_UART7_RX               CHIP_PIN_ID(CHIP_PORT_B, 3, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 3, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 3, 13) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PB3_TIM24_ETR              CHIP_PIN_ID(CHIP_PORT_B, 3, 14)
#endif
#define CHIP_PIN_PB3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 3, 15)


/*----- Пин PB4  ------------------------------------------------------------ */
#define CHIP_PIN_PB4_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 4, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PB4_NJTRST                 CHIP_PIN_ID(CHIP_PORT_B, 4, 0)
#define CHIP_PIN_PB4_TIM16_BKIN             CHIP_PIN_ID(CHIP_PORT_B, 4, 1)
#define CHIP_PIN_PB4_TIM3_CH1               CHIP_PIN_ID(CHIP_PORT_B, 4, 2)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PB4_HRTIM_EEV6             CHIP_PIN_ID(CHIP_PORT_B, 4, 3)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 4, 4) */
#define CHIP_PIN_PB4_SPI1_MISO              CHIP_PIN_ID(CHIP_PORT_B, 4, 5)
#define CHIP_PIN_PB4_I2S1_SDI               CHIP_PIN_ID(CHIP_PORT_B, 4, 5)
#define CHIP_PIN_PB4_SPI3_MISO              CHIP_PIN_ID(CHIP_PORT_B, 4, 6)
#define CHIP_PIN_PB4_I2S3_SDI               CHIP_PIN_ID(CHIP_PORT_B, 4, 6)
#define CHIP_PIN_PB4_SPI2_NSS               CHIP_PIN_ID(CHIP_PORT_B, 4, 7)
#define CHIP_PIN_PB4_I2S2_WS                CHIP_PIN_ID(CHIP_PORT_B, 4, 7)
#define CHIP_PIN_PB4_SPI6_MISO              CHIP_PIN_ID(CHIP_PORT_B, 4, 8)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PB4_I2S6_SDI               CHIP_PIN_ID(CHIP_PORT_B, 4, 8)
#endif
#define CHIP_PIN_PB4_SDMMC2_D3              CHIP_PIN_ID(CHIP_PORT_B, 4, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 4, 10) */
#define CHIP_PIN_PB4_UART7_TX               CHIP_PIN_ID(CHIP_PORT_B, 4, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 4, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 4, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 4, 14) */
#define CHIP_PIN_PB4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 4, 15)


/*----- Пин PB5  ------------------------------------------------------------ */
#define CHIP_PIN_PB5_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 5, 0) */
#define CHIP_PIN_PB5_TIM17_BKIN             CHIP_PIN_ID(CHIP_PORT_B, 5, 1)
#define CHIP_PIN_PB5_TIM3_CH2               CHIP_PIN_ID(CHIP_PORT_B, 5, 2)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PB5_HRTIM_EEV7             CHIP_PIN_ID(CHIP_PORT_B, 5, 3)
#else
#define CHIP_PIN_PB5_LCD_B5                 CHIP_PIN_ID(CHIP_PORT_B, 5, 3)
#endif
#define CHIP_PIN_PB5_I2C1_SMBA              CHIP_PIN_ID(CHIP_PORT_B, 5, 4)
#define CHIP_PIN_PB5_SPI1_MOSI              CHIP_PIN_ID(CHIP_PORT_B, 5, 5)
#define CHIP_PIN_PB5_I2S1_SDO               CHIP_PIN_ID(CHIP_PORT_B, 5, 5)
#define CHIP_PIN_PB5_I2C4_SMBA              CHIP_PIN_ID(CHIP_PORT_B, 5, 6)
#define CHIP_PIN_PB5_SPI3_MOSI              CHIP_PIN_ID(CHIP_PORT_B, 5, 7)
#define CHIP_PIN_PB5_I2S3_SDO               CHIP_PIN_ID(CHIP_PORT_B, 5, 7)
#define CHIP_PIN_PB5_SPI6_MOSI              CHIP_PIN_ID(CHIP_PORT_B, 5, 8)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PB5_I2S6_SDO               CHIP_PIN_ID(CHIP_PORT_B, 5, 8)
#endif
#define CHIP_PIN_PB5_FDCAN2_RX              CHIP_PIN_ID(CHIP_PORT_B, 5, 9)
#define CHIP_PIN_PB5_OTG_HS_ULPI_D7         CHIP_PIN_ID(CHIP_PORT_B, 5, 10)
#define CHIP_PIN_PB5_ETH_PPS_OUT            CHIP_PIN_ID(CHIP_PORT_B, 5, 11)
#define CHIP_PIN_PB5_EXMC_SDCKE1            CHIP_PIN_ID(CHIP_PORT_B, 5, 12)
#define CHIP_PIN_PB5_DVP_D10                CHIP_PIN_ID(CHIP_PORT_B, 5, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PB5_PSSI_D10               CHIP_PIN_ID(CHIP_PORT_B, 5, 13)
#endif
#define CHIP_PIN_PB5_UART5_RX               CHIP_PIN_ID(CHIP_PORT_B, 5, 14)
#define CHIP_PIN_PB5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 5, 15)


/*----- Пин PB6  ------------------------------------------------------------ */
#define CHIP_PIN_PB6_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 6, 0) */
#define CHIP_PIN_PB6_TIM16_CH1N             CHIP_PIN_ID(CHIP_PORT_B, 6, 1)
#define CHIP_PIN_PB6_TIM4_CH1               CHIP_PIN_ID(CHIP_PORT_B, 6, 2)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PB6_HRTIM_EEV8             CHIP_PIN_ID(CHIP_PORT_B, 6, 3)
#endif
#define CHIP_PIN_PB6_I2C1_SCL               CHIP_PIN_ID(CHIP_PORT_B, 6, 4)
#define CHIP_PIN_PB6_HDMI_CEC               CHIP_PIN_ID(CHIP_PORT_B, 6, 5)
#define CHIP_PIN_PB6_I2C4_SCL               CHIP_PIN_ID(CHIP_PORT_B, 6, 6)
#define CHIP_PIN_PB6_USART1_TX              CHIP_PIN_ID(CHIP_PORT_B, 6, 7)
#define CHIP_PIN_PB6_LPUART1_TX             CHIP_PIN_ID(CHIP_PORT_B, 6, 8)
#define CHIP_PIN_PB6_FDCAN2_TX              CHIP_PIN_ID(CHIP_PORT_B, 6, 9)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB6_OCTOSPIM_P1_NCS        CHIP_PIN_ID(CHIP_PORT_B, 6, 10)
#else
#define CHIP_PIN_PB6_QUADSPI_BK1_NCS        CHIP_PIN_ID(CHIP_PORT_B, 6, 10)
#endif
#define CHIP_PIN_PB6_DFSDM1_DATIN5          CHIP_PIN_ID(CHIP_PORT_B, 6, 11)
#define CHIP_PIN_PB6_EXMC_SDNE1             CHIP_PIN_ID(CHIP_PORT_B, 6, 12)
#define CHIP_PIN_PB6_DVP_D5                 CHIP_PIN_ID(CHIP_PORT_B, 6, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PB6_PSSI_D5                CHIP_PIN_ID(CHIP_PORT_B, 6, 13)
#endif
#define CHIP_PIN_PB6_UART5_TX               CHIP_PIN_ID(CHIP_PORT_B, 6, 14)
#define CHIP_PIN_PB6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 6, 15)


/*----- Пин PB7  ------------------------------------------------------------ */
#define CHIP_PIN_PB7_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 7, 0) */
#define CHIP_PIN_PB7_TIM17_CH1N             CHIP_PIN_ID(CHIP_PORT_B, 7, 1)
#define CHIP_PIN_PB7_TIM4_CH2               CHIP_PIN_ID(CHIP_PORT_B, 7, 2)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PB7_HRTIM_EEV9             CHIP_PIN_ID(CHIP_PORT_B, 7, 3)
#endif
#define CHIP_PIN_PB7_I2C1_SDA               CHIP_PIN_ID(CHIP_PORT_B, 7, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 7, 5) */
#define CHIP_PIN_PB7_I2C4_SDA               CHIP_PIN_ID(CHIP_PORT_B, 7, 6)
#define CHIP_PIN_PB7_USART1_RX              CHIP_PIN_ID(CHIP_PORT_B, 7, 7)
#define CHIP_PIN_PB7_LPUART1_RX             CHIP_PIN_ID(CHIP_PORT_B, 7, 8)
#ifndef CHIP_DEV_STM32H723
#define CHIP_PIN_PB7_FDCAN2_TXFD_MODE       CHIP_PIN_ID(CHIP_PORT_B, 7, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 7, 10) */
#define CHIP_PIN_PB7_DFSDM1_CKIN5           CHIP_PIN_ID(CHIP_PORT_B, 7, 11)
#define CHIP_PIN_PB7_EXMC_NL                CHIP_PIN_ID(CHIP_PORT_B, 7, 12)
#define CHIP_PIN_PB7_DVP_VSYNC              CHIP_PIN_ID(CHIP_PORT_B, 7, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PB7_PSSI_RDY               CHIP_PIN_ID(CHIP_PORT_B, 7, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 7, 14) */
#define CHIP_PIN_PB7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 7, 15)


/*----- Пин PB8  ------------------------------------------------------------ */
#define CHIP_PIN_PB8_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 8, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 8, 0) */
#define CHIP_PIN_PB8_TIM16_CH1              CHIP_PIN_ID(CHIP_PORT_B, 8, 1)
#define CHIP_PIN_PB8_TIM4_CH3               CHIP_PIN_ID(CHIP_PORT_B, 8, 2)
#define CHIP_PIN_PB8_DFSDM1_CKIN7           CHIP_PIN_ID(CHIP_PORT_B, 8, 3)
#define CHIP_PIN_PB8_I2C1_SCL               CHIP_PIN_ID(CHIP_PORT_B, 8, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 8, 5) */
#define CHIP_PIN_PB8_I2C4_SCL               CHIP_PIN_ID(CHIP_PORT_B, 8, 6)
#define CHIP_PIN_PB8_SDMMC1_CKIN            CHIP_PIN_ID(CHIP_PORT_B, 8, 7)
#define CHIP_PIN_PB8_UART4_RX               CHIP_PIN_ID(CHIP_PORT_B, 8, 8)
#define CHIP_PIN_PB8_FDCAN1_RX              CHIP_PIN_ID(CHIP_PORT_B, 8, 9)
#define CHIP_PIN_PB8_SDMMC2_D4              CHIP_PIN_ID(CHIP_PORT_B, 8, 10)
#define CHIP_PIN_PB8_ETH_MII_TXD3           CHIP_PIN_ID(CHIP_PORT_B, 8, 11)
#define CHIP_PIN_PB8_SDMMC1_D4              CHIP_PIN_ID(CHIP_PORT_B, 8, 12)
#define CHIP_PIN_PB8_DVP_D6                 CHIP_PIN_ID(CHIP_PORT_B, 8, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PB8_PSSI_D6                CHIP_PIN_ID(CHIP_PORT_B, 8, 13)
#endif
#define CHIP_PIN_PB8_LCD_B6                 CHIP_PIN_ID(CHIP_PORT_B, 8, 14)
#define CHIP_PIN_PB8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 8, 15)


/*----- Пин PB9  ------------------------------------------------------------ */
#define CHIP_PIN_PB9_GPIO                   CHIP_PIN_ID(CHIP_PORT_B, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 9, 0) */
#define CHIP_PIN_PB9_TIM17_CH1              CHIP_PIN_ID(CHIP_PORT_B, 9, 1)
#define CHIP_PIN_PB9_TIM4_CH4               CHIP_PIN_ID(CHIP_PORT_B, 9, 2)
#define CHIP_PIN_PB9_DFSDM1_DATIN7          CHIP_PIN_ID(CHIP_PORT_B, 9, 3)
#define CHIP_PIN_PB9_I2C1_SDA               CHIP_PIN_ID(CHIP_PORT_B, 9, 4)
#define CHIP_PIN_PB9_SPI2_NSS               CHIP_PIN_ID(CHIP_PORT_B, 9, 5)
#define CHIP_PIN_PB9_I2S2_WS                CHIP_PIN_ID(CHIP_PORT_B, 9, 5)
#define CHIP_PIN_PB9_I2C4_SDA               CHIP_PIN_ID(CHIP_PORT_B, 9, 6)
#define CHIP_PIN_PB9_SDMMC1_CDIR            CHIP_PIN_ID(CHIP_PORT_B, 9, 7)
#define CHIP_PIN_PB9_UART4_TX               CHIP_PIN_ID(CHIP_PORT_B, 9, 8)
#define CHIP_PIN_PB9_FDCAN1_TX              CHIP_PIN_ID(CHIP_PORT_B, 9, 9)
#define CHIP_PIN_PB9_SDMMC2_D5              CHIP_PIN_ID(CHIP_PORT_B, 9, 10)
#define CHIP_PIN_PB9_I2C4_SMBA              CHIP_PIN_ID(CHIP_PORT_B, 9, 11)
#define CHIP_PIN_PB9_SDMMC1_D5              CHIP_PIN_ID(CHIP_PORT_B, 9, 12)
#define CHIP_PIN_PB9_DVP_D7                 CHIP_PIN_ID(CHIP_PORT_B, 9, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PB9_PSSI_D7                CHIP_PIN_ID(CHIP_PORT_B, 9, 13)
#endif
#define CHIP_PIN_PB9_LCD_B7                 CHIP_PIN_ID(CHIP_PORT_B, 9, 14)
#define CHIP_PIN_PB9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_B, 9, 15)


/*----- Пин PB10 ------------------------------------------------------------ */
#define CHIP_PIN_PB10_GPIO                  CHIP_PIN_ID(CHIP_PORT_B, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 10, 0) */
#define CHIP_PIN_PB10_TIM2_CH3              CHIP_PIN_ID(CHIP_PORT_B, 10, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PB10_HRTIM_SCOUT           CHIP_PIN_ID(CHIP_PORT_B, 10, 2)
#endif
#define CHIP_PIN_PB10_LPTIM2_IN1            CHIP_PIN_ID(CHIP_PORT_B, 10, 3)
#define CHIP_PIN_PB10_I2C2_SCL              CHIP_PIN_ID(CHIP_PORT_B, 10, 4)
#define CHIP_PIN_PB10_SPI2_SCK              CHIP_PIN_ID(CHIP_PORT_B, 10, 5)
#define CHIP_PIN_PB10_I2S2_CK               CHIP_PIN_ID(CHIP_PORT_B, 10, 5)
#define CHIP_PIN_PB10_DFSDM1_DATIN7         CHIP_PIN_ID(CHIP_PORT_B, 10, 6)
#define CHIP_PIN_PB10_USART3_TX             CHIP_PIN_ID(CHIP_PORT_B, 10, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 10, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB10_OCTOSPIM_P1_NCS       CHIP_PIN_ID(CHIP_PORT_B, 10, 9)
#else
#define CHIP_PIN_PB10_QUADSPI_BK1_NCS       CHIP_PIN_ID(CHIP_PORT_B, 10, 9)
#endif
#define CHIP_PIN_PB10_OTG_HS_ULPI_D3        CHIP_PIN_ID(CHIP_PORT_B, 10, 10)
#define CHIP_PIN_PB10_ETH_MII_RX_ERR        CHIP_PIN_ID(CHIP_PORT_B, 10, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 10, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 10, 13) */
#define CHIP_PIN_PB10_LCD_G4                CHIP_PIN_ID(CHIP_PORT_B, 10, 14)
#define CHIP_PIN_PB10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_B, 10, 15)


/*----- Пин PB11 ------------------------------------------------------------ */
#define CHIP_PIN_PB11_GPIO                  CHIP_PIN_ID(CHIP_PORT_B, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 11, 0) */
#define CHIP_PIN_PB11_TIM2_CH4              CHIP_PIN_ID(CHIP_PORT_B, 11, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PB11_HRTIM_SCIN            CHIP_PIN_ID(CHIP_PORT_B, 11, 2)
#endif
#define CHIP_PIN_PB11_LPTIM2_ETR            CHIP_PIN_ID(CHIP_PORT_B, 11, 3)
#define CHIP_PIN_PB11_I2C2_SDA              CHIP_PIN_ID(CHIP_PORT_B, 11, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 11, 5) */
#define CHIP_PIN_PB11_DFSDM1_CKIN7          CHIP_PIN_ID(CHIP_PORT_B, 11, 6)
#define CHIP_PIN_PB11_USART3_RX             CHIP_PIN_ID(CHIP_PORT_B, 11, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 11, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 11, 9) */
#define CHIP_PIN_PB11_OTG_HS_ULPI_D4        CHIP_PIN_ID(CHIP_PORT_B, 11, 10)
#define CHIP_PIN_PB11_ETH_MII_TX_EN         CHIP_PIN_ID(CHIP_PORT_B, 11, 11)
#define CHIP_PIN_PB11_ETH_RMII_TX_EN        CHIP_PIN_ID(CHIP_PORT_B, 11, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 11, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 11, 13) */
#define CHIP_PIN_PB11_LCD_G5                CHIP_PIN_ID(CHIP_PORT_B, 11, 14)
#define CHIP_PIN_PB11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_B, 11, 15)


/*----- Пин PB12 ------------------------------------------------------------ */
#define CHIP_PIN_PB12_GPIO                  CHIP_PIN_ID(CHIP_PORT_B, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 12, 0) */
#define CHIP_PIN_PB12_TIM1_BKIN             CHIP_PIN_ID(CHIP_PORT_B, 12, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 12, 2) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB12_OCTOSPIM_P1_NCLK      CHIP_PIN_ID(CHIP_PORT_B, 12, 3)
#endif
#define CHIP_PIN_PB12_I2C2_SMBA             CHIP_PIN_ID(CHIP_PORT_B, 12, 4)
#define CHIP_PIN_PB12_SPI2_NSS              CHIP_PIN_ID(CHIP_PORT_B, 12, 5)
#define CHIP_PIN_PB12_I2S2_WS               CHIP_PIN_ID(CHIP_PORT_B, 12, 5)
#define CHIP_PIN_PB12_DFSDM1_DATIN1         CHIP_PIN_ID(CHIP_PORT_B, 12, 6)
#define CHIP_PIN_PB12_USART3_CK             CHIP_PIN_ID(CHIP_PORT_B, 12, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 12, 8) */
#define CHIP_PIN_PB12_FDCAN2_RX             CHIP_PIN_ID(CHIP_PORT_B, 12, 9)
#define CHIP_PIN_PB12_OTG_HS_ULPI_D5        CHIP_PIN_ID(CHIP_PORT_B, 12, 10)
#define CHIP_PIN_PB12_ETH_MII_TXD0          CHIP_PIN_ID(CHIP_PORT_B, 12, 11)
#define CHIP_PIN_PB12_ETH_RMII_TXD0         CHIP_PIN_ID(CHIP_PORT_B, 12, 11)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB12_OCTOSPIM_P1_IO0       CHIP_PIN_ID(CHIP_PORT_B, 12, 12)
#else
#define CHIP_PIN_PB12_OTG_HS_ID             CHIP_PIN_ID(CHIP_PORT_B, 12, 12)
#endif
#define CHIP_PIN_PB12_TIM1_BKIN_COMP12      CHIP_PIN_ID(CHIP_PORT_B, 12, 13)
#define CHIP_PIN_PB12_UART5_RX              CHIP_PIN_ID(CHIP_PORT_B, 12, 14)
#define CHIP_PIN_PB12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_B, 12, 15)

/*----- Пин PB13 ------------------------------------------------------------ */
#define CHIP_PIN_PB13_GPIO                  CHIP_PIN_ID(CHIP_PORT_B, 13, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 13, 0) */
#define CHIP_PIN_PB13_TIM1_CH1N             CHIP_PIN_ID(CHIP_PORT_B, 13, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 13, 2) */
#define CHIP_PIN_PB13_LPTIM2_OUT            CHIP_PIN_ID(CHIP_PORT_B, 13, 3)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PB13_OCTOSPIM_P1_IO2       CHIP_PIN_ID(CHIP_PORT_B, 13, 4)
#endif
#define CHIP_PIN_PB13_SPI2_SCK              CHIP_PIN_ID(CHIP_PORT_B, 13, 5)
#define CHIP_PIN_PB13_I2S2_CK               CHIP_PIN_ID(CHIP_PORT_B, 13, 5)
#define CHIP_PIN_PB13_DFSDM1_CKIN1          CHIP_PIN_ID(CHIP_PORT_B, 13, 6)
#define CHIP_PIN_PB13_USART3_CTS            CHIP_PIN_ID(CHIP_PORT_B, 13, 7)
#define CHIP_PIN_PB13_USART3_NSS            CHIP_PIN_ID(CHIP_PORT_B, 13, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 13, 8) */
#define CHIP_PIN_PB13_FDCAN2_TX             CHIP_PIN_ID(CHIP_PORT_B, 13, 9)
#define CHIP_PIN_PB13_OTG_HS_ULPI_D6        CHIP_PIN_ID(CHIP_PORT_B, 13, 10)
#define CHIP_PIN_PB13_ETH_MII_TXD1          CHIP_PIN_ID(CHIP_PORT_B, 13, 11)
#define CHIP_PIN_PB13_ETH_RMII_TXD1         CHIP_PIN_ID(CHIP_PORT_B, 13, 11)
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PB13_SDMMC1_D0             CHIP_PIN_ID(CHIP_PORT_B, 13, 12)
#define CHIP_PIN_PB13_DVP_D2                CHIP_PIN_ID(CHIP_PORT_B, 13, 13)
#define CHIP_PIN_PB13_PSSI_D2               CHIP_PIN_ID(CHIP_PORT_B, 13, 13)
#endif
#define CHIP_PIN_PB13_UART5_TX              CHIP_PIN_ID(CHIP_PORT_B, 13, 14)
#define CHIP_PIN_PB13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_B, 13, 15)


/*----- Пин PB14 ------------------------------------------------------------ */
#define CHIP_PIN_PB14_GPIO                  CHIP_PIN_ID(CHIP_PORT_B, 14, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 14, 0) */
#define CHIP_PIN_PB14_TIM1_CH2N             CHIP_PIN_ID(CHIP_PORT_B, 14, 1)
#define CHIP_PIN_PB14_TIM12_CH1             CHIP_PIN_ID(CHIP_PORT_B, 14, 2)
#define CHIP_PIN_PB14_TIM8_CH2N             CHIP_PIN_ID(CHIP_PORT_B, 14, 3)
#define CHIP_PIN_PB14_USART1_TX             CHIP_PIN_ID(CHIP_PORT_B, 14, 4)
#define CHIP_PIN_PB14_SPI2_MISO             CHIP_PIN_ID(CHIP_PORT_B, 14, 5)
#define CHIP_PIN_PB14_I2S2_SDI              CHIP_PIN_ID(CHIP_PORT_B, 14, 5)
#define CHIP_PIN_PB14_DFSDM1_DATIN2         CHIP_PIN_ID(CHIP_PORT_B, 14, 6)
#define CHIP_PIN_PB14_USART3_RTS            CHIP_PIN_ID(CHIP_PORT_B, 14, 7)
#define CHIP_PIN_PB14_USART3_DE             CHIP_PIN_ID(CHIP_PORT_B, 14, 7)
#define CHIP_PIN_PB14_UART4_RTS             CHIP_PIN_ID(CHIP_PORT_B, 14, 8)
#define CHIP_PIN_PB14_UART4_DE              CHIP_PIN_ID(CHIP_PORT_B, 14, 8)
#define CHIP_PIN_PB14_SDMMC2_D0             CHIP_PIN_ID(CHIP_PORT_B, 14, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 14, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 14, 11) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PB14_EXMC_D10              CHIP_PIN_ID(CHIP_PORT_B, 14, 12)
#define CHIP_PIN_PB14_EXMC_DA10             CHIP_PIN_ID(CHIP_PORT_B, 14, 12)
#else
#define CHIP_PIN_PB14_OTG_HS_DM             CHIP_PIN_ID(CHIP_PORT_B, 14, 12)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 14, 13) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PB14_LCD_CLK               CHIP_PIN_ID(CHIP_PORT_B, 14, 14)
#endif
#define CHIP_PIN_PB14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_B, 14, 15)


/*----- Пин PB15 ------------------------------------------------------------ */
#define CHIP_PIN_PB15_GPIO                  CHIP_PIN_ID(CHIP_PORT_B, 15, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PB15_RTC_REFIN             CHIP_PIN_ID(CHIP_PORT_B, 15, 0)
#define CHIP_PIN_PB15_TIM1_CH3N             CHIP_PIN_ID(CHIP_PORT_B, 15, 1)
#define CHIP_PIN_PB15_TIM12_CH2             CHIP_PIN_ID(CHIP_PORT_B, 15, 2)
#define CHIP_PIN_PB15_TIM8_CH3N             CHIP_PIN_ID(CHIP_PORT_B, 15, 3)
#define CHIP_PIN_PB15_USART1_RX             CHIP_PIN_ID(CHIP_PORT_B, 15, 4)
#define CHIP_PIN_PB15_SPI2_MOSI             CHIP_PIN_ID(CHIP_PORT_B, 15, 5)
#define CHIP_PIN_PB15_I2S2_SDO              CHIP_PIN_ID(CHIP_PORT_B, 15, 5)
#define CHIP_PIN_PB15_DFSDM1_CKIN2          CHIP_PIN_ID(CHIP_PORT_B, 15, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 15, 7) */
#define CHIP_PIN_PB15_UART4_CTS             CHIP_PIN_ID(CHIP_PORT_B, 15, 8)
#define CHIP_PIN_PB15_SDMMC2_D1             CHIP_PIN_ID(CHIP_PORT_B, 15, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 15, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 15, 11) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PB15_EXMC_D11              CHIP_PIN_ID(CHIP_PORT_B, 15, 12)
#define CHIP_PIN_PB15_EXMC_DA11             CHIP_PIN_ID(CHIP_PORT_B, 15, 12)
#else
#define CHIP_PIN_PB15_OTG_HS_DP             CHIP_PIN_ID(CHIP_PORT_B, 15, 12)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_B, 15, 13) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PB15_LCD_G7                CHIP_PIN_ID(CHIP_PORT_B, 15, 14) */
#endif
#define CHIP_PIN_PB15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_B, 15, 15)


/************************************ Порт C **********************************/
/*----- Пин PC0 ------------------------------------------------------------- */
#define CHIP_PIN_PC0_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 0, 0) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PC0_EXMC_D12               CHIP_PIN_ID(CHIP_PORT_C, 0, 1)
#define CHIP_PIN_PC0_EXMC_DA12              CHIP_PIN_ID(CHIP_PORT_C, 0, 1)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 0, 2) */
#define CHIP_PIN_PC0_DFSDM1_CKIN0           CHIP_PIN_ID(CHIP_PORT_C, 0, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 0, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 0, 5) */
#define CHIP_PIN_PC0_DFSDM1_DATIN4          CHIP_PIN_ID(CHIP_PORT_C, 0, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 0, 7) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PC0_SAI2_FS_B              CHIP_PIN_ID(CHIP_PORT_C, 0, 8)
#else
#define CHIP_PIN_PC0_SAI4_FS_B              CHIP_PIN_ID(CHIP_PORT_C, 0, 8)
#endif
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PC0_EXMC_A25               CHIP_PIN_ID(CHIP_PORT_C, 0, 9)
#endif
#define CHIP_PIN_PC0_OTG_HS_ULPI_STP        CHIP_PIN_ID(CHIP_PORT_C, 0, 10)
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PC0_LCD_G2                 CHIP_PIN_ID(CHIP_PORT_C, 0, 11)
#endif
#define CHIP_PIN_PC0_EXMC_SDNWE             CHIP_PIN_ID(CHIP_PORT_C, 0, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 0, 13) */
#define CHIP_PIN_PC0_LCD_R5                 CHIP_PIN_ID(CHIP_PORT_C, 0, 14)
#define CHIP_PIN_PC0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 0, 15)
#define CHIP_PIN_PC0_ADC1_2_3_INP10         CHIP_PIN_ID(CHIP_PORT_C, 0, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PC1 ------------------------------------------------------------- */
#define CHIP_PIN_PC1_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 1, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC1_TRACED0                CHIP_PIN_ID(CHIP_PORT_C, 1, 0)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC1_SAI4_D1                CHIP_PIN_ID(CHIP_PORT_C, 1, 1)
#endif
#define CHIP_PIN_PC1_SAI1_D1                CHIP_PIN_ID(CHIP_PORT_C, 1, 2)
#define CHIP_PIN_PC1_DFSDM1_DATIN0          CHIP_PIN_ID(CHIP_PORT_C, 1, 3)
#define CHIP_PIN_PC1_DFSDM1_CKIN4           CHIP_PIN_ID(CHIP_PORT_C, 1, 4)
#define CHIP_PIN_PC1_SPI2_MOSI              CHIP_PIN_ID(CHIP_PORT_C, 1, 5)
#define CHIP_PIN_PC1_I2S2_SDO               CHIP_PIN_ID(CHIP_PORT_C, 1, 5)
#define CHIP_PIN_PC1_SAI1_SD_A              CHIP_PIN_ID(CHIP_PORT_C, 1, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 1, 7) */
#define CHIP_PIN_PC1_SAI4_SD_A              CHIP_PIN_ID(CHIP_PORT_C, 1, 8)
#define CHIP_PIN_PC1_SDMMC2_CK              CHIP_PIN_ID(CHIP_PORT_C, 1, 9)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC1_OCTOSPIM_P1_IO4        CHIP_PIN_ID(CHIP_PORT_C, 1, 10)
#else
#define CHIP_PIN_PC1_SAI4_D1                CHIP_PIN_ID(CHIP_PORT_C, 1, 10)
#endif
#define CHIP_PIN_PC1_ETH_MDC                CHIP_PIN_ID(CHIP_PORT_C, 1, 11)
#define CHIP_PIN_PC1_MDIOS_MDC              CHIP_PIN_ID(CHIP_PORT_C, 1, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 1, 13) */
#ifdef CHIP_DEV_STM32H723
#define CHIP_PIN_PC1_LCD_G5                 CHIP_PIN_ID(CHIP_PORT_C, 1, 14)
#endif
#define CHIP_PIN_PC1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 1, 15)
#define CHIP_PIN_PC1_ADC1_2_3_INN10         CHIP_PIN_ID(CHIP_PORT_C, 1, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PC1_ADC1_2_3_INP11         CHIP_PIN_ID(CHIP_PORT_C, 1, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PC2 ------------------------------------------------------------- */
#define CHIP_PIN_PC2_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_FUNC_GPIO)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_PIN_PC2_C1DSLEEP               CHIP_PIN_ID(CHIP_PORT_C, 2, 0)
#elif !CHIP_REV_Y_COMPAT_EN
#define CHIP_PIN_PC2_CDSLEEP                CHIP_PIN_ID(CHIP_PORT_C, 2, 0)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 2, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 2, 2) */
#define CHIP_PIN_PC2_DFSDM1_CKIN1           CHIP_PIN_ID(CHIP_PORT_C, 2, 3)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC2_OCTOSPIM_P1_IO5        CHIP_PIN_ID(CHIP_PORT_C, 2, 4)
#endif
#define CHIP_PIN_PC2_SPI2_MISO              CHIP_PIN_ID(CHIP_PORT_C, 2, 5)
#define CHIP_PIN_PC2_I2S2_SDI               CHIP_PIN_ID(CHIP_PORT_C, 2, 5)
#define CHIP_PIN_PC2_DFSDM1_CKOUT           CHIP_PIN_ID(CHIP_PORT_C, 2, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 2, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 2, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC2_OCTOSPIM_P1_IO2        CHIP_PIN_ID(CHIP_PORT_C, 2, 9)
#endif
#define CHIP_PIN_PC2_OTG_HS_ULPI_DIR        CHIP_PIN_ID(CHIP_PORT_C, 2, 10)
#define CHIP_PIN_PC2_ETH_MII_TXD2           CHIP_PIN_ID(CHIP_PORT_C, 2, 11)
#define CHIP_PIN_PC2_EXMC_SDNE0             CHIP_PIN_ID(CHIP_PORT_C, 2, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 2, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 2, 14) */
#define CHIP_PIN_PC2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 2, 15)
#define CHIP_PIN_PC2_ADC1_2_3_INN11         CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PC2_ADC1_2_3_INP12         CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PC2C_ADC3_INN1             CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_FUNC_ANALOG) /* PC2_C - ANALOG ONLY */
#define CHIP_PIN_PC2C_ADC3_INP0             CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_FUNC_ANALOG) /* PC2_C - ANALOG ONLY */


/*----- Пин PC3 ------------------------------------------------------------- */
#define CHIP_PIN_PC3_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_FUNC_GPIO)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_PIN_PC3_C1SLEEP                CHIP_PIN_ID(CHIP_PORT_C, 3, 0)
#elif !CHIP_REV_Y_COMPAT_EN
#define CHIP_PIN_PC3_CSLEEP                 CHIP_PIN_ID(CHIP_PORT_C, 3, 0)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 3, 2) */
#define CHIP_PIN_PC3_DFSDM1_DATIN1          CHIP_PIN_ID(CHIP_PORT_C, 3, 3)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC3_OCTOSPIM_P1_IO6        CHIP_PIN_ID(CHIP_PORT_C, 3, 4)
#endif
#define CHIP_PIN_PC3_SPI2_MOSI              CHIP_PIN_ID(CHIP_PORT_C, 3, 5)
#define CHIP_PIN_PC3_I2S2_SDO               CHIP_PIN_ID(CHIP_PORT_C, 3, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 3, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 3, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 3, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC3_OCTOSPIM_P1_IO0        CHIP_PIN_ID(CHIP_PORT_C, 3, 9)
#endif
#define CHIP_PIN_PC3_OTG_HS_ULPI_NXT        CHIP_PIN_ID(CHIP_PORT_C, 3, 10)
#define CHIP_PIN_PC3_ETH_MII_TX_CLK         CHIP_PIN_ID(CHIP_PORT_C, 3, 11)
#define CHIP_PIN_PC3_EXMC_SDCKE0            CHIP_PIN_ID(CHIP_PORT_C, 3, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 3, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 3, 14) */
#define CHIP_PIN_PC3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 3, 15)
#define CHIP_PIN_PC3_ADC1_2_INN12           CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PC3_ADC1_2_INP13           CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PC3C_ADC3_INP1             CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_FUNC_ANALOG) /* PC3_C - ANALOG ONLY */


/*----- Пин PC4 ------------------------------------------------------------- */
#define CHIP_PIN_PC4_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 4, CHIP_PIN_FUNC_GPIO)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_PIN_PC4_C2DSLEEP               CHIP_PIN_ID(CHIP_PORT_C, 4, 0)
#elif defined CHIP_DEV_STM32H23
#define CHIP_PIN_PC4_CDSLEEP                CHIP_PIN_ID(CHIP_PORT_C, 4, 0)
#endif
#if defined CHIP_DEV_STM32H23
#define CHIP_PIN_PC4_EXMC_A22               CHIP_PIN_ID(CHIP_PORT_C, 4, 1)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 4, 2) */
#define CHIP_PIN_PC4_DFSDM1_CKIN2           CHIP_PIN_ID(CHIP_PORT_C, 4, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 4, 4) */
#define CHIP_PIN_PC4_I2S1_MCK               CHIP_PIN_ID(CHIP_PORT_C, 4, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 4, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 4, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 4, 8) */
#define CHIP_PIN_PC4_SPDIFRX1_IN3           CHIP_PIN_ID(CHIP_PORT_C, 4, 9)
#if defined CHIP_DEV_STM32H23
#define CHIP_PIN_PC4_SDMMC2_CKIN            CHIP_PIN_ID(CHIP_PORT_C, 4, 10)
#endif
#define CHIP_PIN_PC4_ETH_MII_RXD0           CHIP_PIN_ID(CHIP_PORT_C, 4, 11)
#define CHIP_PIN_PC4_ETH_RMII_RXD0          CHIP_PIN_ID(CHIP_PORT_C, 4, 11)
#define CHIP_PIN_PC4_EXMC_SDNE0             CHIP_PIN_ID(CHIP_PORT_C, 4, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 4, 13) */
#if defined CHIP_DEV_STM32H23
#define CHIP_PIN_PC4_LCD_R7                 CHIP_PIN_ID(CHIP_PORT_C, 4, 14)
#endif
#define CHIP_PIN_PC4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 4, 15)
#define CHIP_PIN_PC4_ADC1_2_INP4            CHIP_PIN_ID(CHIP_PORT_C, 4, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PC5 ------------------------------------------------------------- */
#define CHIP_PIN_PC5_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 5, CHIP_PIN_FUNC_GPIO)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_PIN_PC5_C2SLEEP                CHIP_PIN_ID(CHIP_PORT_C, 5, 0)
#elif defined CHIP_DEV_STM32H23
#define CHIP_PIN_PC5_CSLEEP                 CHIP_PIN_ID(CHIP_PORT_C, 5, 0)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC5_SAI4_D3                CHIP_PIN_ID(CHIP_PORT_C, 5, 2)
#endif
#define CHIP_PIN_PC5_SAI1_D3                CHIP_PIN_ID(CHIP_PORT_C, 5, 2)
#define CHIP_PIN_PC5_DFSDM1_DATIN2          CHIP_PIN_ID(CHIP_PORT_C, 5, 3)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC5_PSSI_D15               CHIP_PIN_ID(CHIP_PORT_C, 5, 4)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 5, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 5, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 5, 8) */
#define CHIP_PIN_PC5_SPDIFRX1_IN4           CHIP_PIN_ID(CHIP_PORT_C, 5, 9)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC5_OCTOSPIM_P1_DQS        CHIP_PIN_ID(CHIP_PORT_C, 5, 10)
#else
#define CHIP_PIN_PC5_SAI4_D3                CHIP_PIN_ID(CHIP_PORT_C, 5, 10)
#endif
#define CHIP_PIN_PC5_ETH_MII_RXD1           CHIP_PIN_ID(CHIP_PORT_C, 5, 11)
#define CHIP_PIN_PC5_ETH_RMII_RXD1          CHIP_PIN_ID(CHIP_PORT_C, 5, 11)
#define CHIP_PIN_PC5_EXMC_SDCKE0            CHIP_PIN_ID(CHIP_PORT_C, 5, 12)
#define CHIP_PIN_PC5_COMP1_OUT              CHIP_PIN_ID(CHIP_PORT_C, 5, 13)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PC5_LCD_DE                 CHIP_PIN_ID(CHIP_PORT_C, 5, 14)
#endif
#define CHIP_PIN_PC5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 5, 15)
#define CHIP_PIN_PC5_ADC1_2_INN4            CHIP_PIN_ID(CHIP_PORT_C, 5, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PC5_ADC1_2_INP8            CHIP_PIN_ID(CHIP_PORT_C, 5, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PC6 ------------------------------------------------------------- */
#define CHIP_PIN_PC6_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 6, 0) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PC6_HRTIM_CHA1             CHIP_PIN_ID(CHIP_PORT_C, 6, 1)
#endif
#define CHIP_PIN_PC6_TIM3_CH1               CHIP_PIN_ID(CHIP_PORT_C, 6, 2)
#define CHIP_PIN_PC6_TIM8_CH1               CHIP_PIN_ID(CHIP_PORT_C, 6, 3)
#define CHIP_PIN_PC6_DFSDM1_CKIN3           CHIP_PIN_ID(CHIP_PORT_C, 6, 4)
#define CHIP_PIN_PC6_I2S2_MCK               CHIP_PIN_ID(CHIP_PORT_C, 6, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 6, 6) */
#define CHIP_PIN_PC6_USART6_TX              CHIP_PIN_ID(CHIP_PORT_C, 6, 7)
#define CHIP_PIN_PC6_SDMMC1_D0DIR           CHIP_PIN_ID(CHIP_PORT_C, 6, 8)
#define CHIP_PIN_PC6_EXMC_NWAIT             CHIP_PIN_ID(CHIP_PORT_C, 6, 9)
#define CHIP_PIN_PC6_SDMMC2_D6              CHIP_PIN_ID(CHIP_PORT_C, 6, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 6, 11) */
#define CHIP_PIN_PC6_SDMMC1_D6              CHIP_PIN_ID(CHIP_PORT_C, 6, 12)
#define CHIP_PIN_PC6_DVP_D0                 CHIP_PIN_ID(CHIP_PORT_C, 6, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC6_PSSI_D0                CHIP_PIN_ID(CHIP_PORT_C, 6, 13)
#endif
#define CHIP_PIN_PC6_LCD_HSYNC              CHIP_PIN_ID(CHIP_PORT_C, 6, 14)
#define CHIP_PIN_PC6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 6, 15)

/*----- Пин PC7 ------------------------------------------------------------- */
#define CHIP_PIN_PC7_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 7, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC7_TRGIO                  CHIP_PIN_ID(CHIP_PORT_C, 7, 0)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PC7_HRTIM_CHA2             CHIP_PIN_ID(CHIP_PORT_C, 7, 1)
#endif
#define CHIP_PIN_PC7_TIM3_CH2               CHIP_PIN_ID(CHIP_PORT_C, 7, 2)
#define CHIP_PIN_PC7_TIM8_CH2               CHIP_PIN_ID(CHIP_PORT_C, 7, 3)
#define CHIP_PIN_PC7_DFSDM1_DATIN3          CHIP_PIN_ID(CHIP_PORT_C, 7, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 7, 5) */
#define CHIP_PIN_PC7_I2S3_MCK               CHIP_PIN_ID(CHIP_PORT_C, 7, 6)
#define CHIP_PIN_PC7_USART6_RX              CHIP_PIN_ID(CHIP_PORT_C, 7, 7)
#define CHIP_PIN_PC7_SDMMC1_D123DIR         CHIP_PIN_ID(CHIP_PORT_C, 7, 8)
#define CHIP_PIN_PC7_EXMC_NE1               CHIP_PIN_ID(CHIP_PORT_C, 7, 9)
#define CHIP_PIN_PC7_SDMMC2_D7              CHIP_PIN_ID(CHIP_PORT_C, 7, 10)
#define CHIP_PIN_PC7_SWPMI_TX               CHIP_PIN_ID(CHIP_PORT_C, 7, 11)
#define CHIP_PIN_PC7_SDMMC1_D7              CHIP_PIN_ID(CHIP_PORT_C, 7, 12)
#define CHIP_PIN_PC7_DVP_D1                 CHIP_PIN_ID(CHIP_PORT_C, 7, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC7_PSSI_D1                CHIP_PIN_ID(CHIP_PORT_C, 7, 13)
#endif
#define CHIP_PIN_PC7_LCD_G6                 CHIP_PIN_ID(CHIP_PORT_C, 7, 14)
#define CHIP_PIN_PC7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 7, 15)


/*----- Пин PC8 ------------------------------------------------------------- */
#define CHIP_PIN_PC8_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 8, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC8_TRACED1                CHIP_PIN_ID(CHIP_PORT_C, 8, 0)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PC8_HRTIM_CHB1             CHIP_PIN_ID(CHIP_PORT_C, 8, 1)
#endif
#define CHIP_PIN_PC8_TIM3_CH3               CHIP_PIN_ID(CHIP_PORT_C, 8, 2)
#define CHIP_PIN_PC8_TIM8_CH3               CHIP_PIN_ID(CHIP_PORT_C, 8, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 8, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 8, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 8, 6) */
#define CHIP_PIN_PC8_USART6_CK              CHIP_PIN_ID(CHIP_PORT_C, 8, 7)
#define CHIP_PIN_PC8_UART5_RTS              CHIP_PIN_ID(CHIP_PORT_C, 8, 8)
#define CHIP_PIN_PC8_UART5_DE               CHIP_PIN_ID(CHIP_PORT_C, 8, 8)
#define CHIP_PIN_PC8_EXMC_NE2               CHIP_PIN_ID(CHIP_PORT_C, 8, 9)
#define CHIP_PIN_PC8_EXMC_NCE               CHIP_PIN_ID(CHIP_PORT_C, 8, 9)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PC8_EXMC_INIT              CHIP_PIN_ID(CHIP_PORT_C, 8, 10)
#endif
#define CHIP_PIN_PC8_SWPMI_RX               CHIP_PIN_ID(CHIP_PORT_C, 8, 11)
#define CHIP_PIN_PC8_SDMMC1_D0              CHIP_PIN_ID(CHIP_PORT_C, 8, 12)
#define CHIP_PIN_PC8_DVP_D2                 CHIP_PIN_ID(CHIP_PORT_C, 8, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC8_PSSI_D2                CHIP_PIN_ID(CHIP_PORT_C, 8, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 8, 14) */
#define CHIP_PIN_PC8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 8, 15)


/*----- Пин PC9 ------------------------------------------------------------- */
#define CHIP_PIN_PC9_GPIO                   CHIP_PIN_ID(CHIP_PORT_C, 9, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC9_MCO2                   CHIP_PIN_ID(CHIP_PORT_C, 9, 0)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 9, 1) */
#define CHIP_PIN_PC9_TIM3_CH4               CHIP_PIN_ID(CHIP_PORT_C, 9, 2)
#define CHIP_PIN_PC9_TIM8_CH4               CHIP_PIN_ID(CHIP_PORT_C, 9, 3)
#define CHIP_PIN_PC9_I2C3_SDA               CHIP_PIN_ID(CHIP_PORT_C, 9, 4)
#define CHIP_PIN_PC9_I2S_CKIN               CHIP_PIN_ID(CHIP_PORT_C, 9, 5)
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PC9_I2C5_SDA               CHIP_PIN_ID(CHIP_PORT_C, 9, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 9, 7) */
#define CHIP_PIN_PC9_UART5_CTS              CHIP_PIN_ID(CHIP_PORT_C, 9, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC9_OCTOSPIM_P1_IO0        CHIP_PIN_ID(CHIP_PORT_C, 9, 9)
#else
#define CHIP_PIN_PC9_QUADSPI_BK1_IO0        CHIP_PIN_ID(CHIP_PORT_C, 9, 9)
#endif
#define CHIP_PIN_PC9_LCD_G3                 CHIP_PIN_ID(CHIP_PORT_C, 9, 10)
#define CHIP_PIN_PC9_SWPMI_SUSPEND          CHIP_PIN_ID(CHIP_PORT_C, 9, 11)
#define CHIP_PIN_PC9_SDMMC1_D1              CHIP_PIN_ID(CHIP_PORT_C, 9, 12)
#define CHIP_PIN_PC9_DVP_D3                 CHIP_PIN_ID(CHIP_PORT_C, 9, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC9_PSSI_D3                CHIP_PIN_ID(CHIP_PORT_C, 9, 13)
#endif
#define CHIP_PIN_PC9_LCD_B2                 CHIP_PIN_ID(CHIP_PORT_C, 9, 14)
#define CHIP_PIN_PC9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_C, 9, 15)

/*----- Пин PC10 ------------------------------------------------------------- */
#define CHIP_PIN_PC10_GPIO                  CHIP_PIN_ID(CHIP_PORT_C, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 10, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 10, 1) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PC10_HRTIM_EEV1            CHIP_PIN_ID(CHIP_PORT_C, 10, 2)
#endif
#define CHIP_PIN_PC10_DFSDM1_CKIN5          CHIP_PIN_ID(CHIP_PORT_C, 10, 3)
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PC10_I2C5_SDA              CHIP_PIN_ID(CHIP_PORT_C, 10, 4)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 10, 5) */
#define CHIP_PIN_PC10_SPI3_SCK              CHIP_PIN_ID(CHIP_PORT_C, 10, 6)
#define CHIP_PIN_PC10_I2S3_CK               CHIP_PIN_ID(CHIP_PORT_C, 10, 6)
#define CHIP_PIN_PC10_USART3_TX             CHIP_PIN_ID(CHIP_PORT_C, 10, 7)
#define CHIP_PIN_PC10_UART4_TX              CHIP_PIN_ID(CHIP_PORT_C, 10, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC10_OCTOSPIM_P1_IO1       CHIP_PIN_ID(CHIP_PORT_C, 10, 9)
#else
#define CHIP_PIN_PC10_QUADSPI_BK1_IO1       CHIP_PIN_ID(CHIP_PORT_C, 10, 9)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PC10_LCD_B1                CHIP_PIN_ID(CHIP_PORT_C, 10, 10)
#define CHIP_PIN_PC10_SWPMI_RX              CHIP_PIN_ID(CHIP_PORT_C, 10, 11)
#endif
#define CHIP_PIN_PC10_SDMMC1_D2             CHIP_PIN_ID(CHIP_PORT_C, 10, 12)
#define CHIP_PIN_PC10_DVP_D8                CHIP_PIN_ID(CHIP_PORT_C, 10, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC10_PSSI_D8               CHIP_PIN_ID(CHIP_PORT_C, 10, 13)
#endif
#define CHIP_PIN_PC10_LCD_R2                CHIP_PIN_ID(CHIP_PORT_C, 10, 14)
#define CHIP_PIN_PC10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_C, 10, 15)

/*----- Пин PC11 ------------------------------------------------------------- */
#define CHIP_PIN_PC11_GPIO                  CHIP_PIN_ID(CHIP_PORT_C, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 11, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 11, 1) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PC11_HRTIM_FLT2            CHIP_PIN_ID(CHIP_PORT_C, 11, 2)
#endif
#define CHIP_PIN_PC11_DFSDM1_DATIN5         CHIP_PIN_ID(CHIP_PORT_C, 11, 3)
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PC11_I2C5_SCL              CHIP_PIN_ID(CHIP_PORT_C, 11, 4)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 11, 5) */
#define CHIP_PIN_PC11_SPI3_MISO             CHIP_PIN_ID(CHIP_PORT_C, 11, 6)
#define CHIP_PIN_PC11_I2S3_SDI              CHIP_PIN_ID(CHIP_PORT_C, 11, 6)
#define CHIP_PIN_PC11_USART3_RX             CHIP_PIN_ID(CHIP_PORT_C, 11, 7)
#define CHIP_PIN_PC11_UART4_RX              CHIP_PIN_ID(CHIP_PORT_C, 11, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PC11_OCTOSPIM_P1_NCS       CHIP_PIN_ID(CHIP_PORT_C, 11, 9)
#else
#define CHIP_PIN_PC11_QUADSPI_BK2_NCS       CHIP_PIN_ID(CHIP_PORT_C, 11, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 11, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 11, 11) */
#define CHIP_PIN_PC11_SDMMC1_D3             CHIP_PIN_ID(CHIP_PORT_C, 11, 12)
#define CHIP_PIN_PC11_DVP_D4                CHIP_PIN_ID(CHIP_PORT_C, 11, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC11_PSSI_D4               CHIP_PIN_ID(CHIP_PORT_C, 11, 13)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PC11_LCD_B4                CHIP_PIN_ID(CHIP_PORT_C, 11, 14)
#endif
#define CHIP_PIN_PC11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_C, 11, 15)


/*----- Пин PC12 ------------------------------------------------------------- */
#define CHIP_PIN_PC12_GPIO                  CHIP_PIN_ID(CHIP_PORT_C, 12, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC12_TRACED3               CHIP_PIN_ID(CHIP_PORT_C, 12, 0)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PC12_EXMC_D6               CHIP_PIN_ID(CHIP_PORT_C, 12, 1)
#define CHIP_PIN_PC12_EXMC_DA6              CHIP_PIN_ID(CHIP_PORT_C, 12, 1)
#endif
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PC12_HRTIM_EEV2            CHIP_PIN_ID(CHIP_PORT_C, 12, 2)
#else
#define CHIP_PIN_PC12_TIM15_CH1             CHIP_PIN_ID(CHIP_PORT_C, 12, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 12, 3) */
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PC12_I2C5_SMBA             CHIP_PIN_ID(CHIP_PORT_C, 12, 4)
#endif
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PC12_SPI6_MOSI             CHIP_PIN_ID(CHIP_PORT_C, 12, 5)
#define CHIP_PIN_PC12_I2S6_SDO              CHIP_PIN_ID(CHIP_PORT_C, 12, 5)
#endif
#define CHIP_PIN_PC12_SPI3_MOSI             CHIP_PIN_ID(CHIP_PORT_C, 12, 6)
#define CHIP_PIN_PC12_I2S3_SDO              CHIP_PIN_ID(CHIP_PORT_C, 12, 6)
#define CHIP_PIN_PC12_USART3_CK             CHIP_PIN_ID(CHIP_PORT_C, 12, 7)
#define CHIP_PIN_PC12_UART5_TX              CHIP_PIN_ID(CHIP_PORT_C, 12, 8)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 12, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 12, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_C, 12, 11) */
#define CHIP_PIN_PC12_SDMMC1_CK             CHIP_PIN_ID(CHIP_PORT_C, 12, 12)
#define CHIP_PIN_PC12_DVP_D9                CHIP_PIN_ID(CHIP_PORT_C, 12, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PC12_PSSI_D9               CHIP_PIN_ID(CHIP_PORT_C, 12, 13)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PC12_LCD_R6                CHIP_PIN_ID(CHIP_PORT_C, 12, 14)
#endif
#define CHIP_PIN_PC12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_C, 12, 15)


/*----- Пин PC13 ------------------------------------------------------------- */
#define CHIP_PIN_PC13_GPIO                  CHIP_PIN_ID(CHIP_PORT_C, 13, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_C, 13, 15)


/*----- Пин PC14 ------------------------------------------------------------- */
#define CHIP_PIN_PC14_GPIO                  CHIP_PIN_ID(CHIP_PORT_C, 14, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_C, 14, 15)


/*----- Пин PC15 ------------------------------------------------------------- */
#define CHIP_PIN_PC15_GPIO                  CHIP_PIN_ID(CHIP_PORT_C, 15, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PC15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_C, 15, 15)


/*********************************** Порт D ***********************************/
/*----- Пин PD0  ------------------------------------------------------------- */
#define CHIP_PIN_PD0_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 2) */
#define CHIP_PIN_PD0_DFSDM1_CKIN6           CHIP_PIN_ID(CHIP_PORT_D, 0, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD0_SAI3_SCK_A             CHIP_PIN_ID(CHIP_PORT_D, 0, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 7) */
#define CHIP_PIN_PD0_UART4_RX               CHIP_PIN_ID(CHIP_PORT_D, 0, 8)
#define CHIP_PIN_PD0_FDCAN1_RX              CHIP_PIN_ID(CHIP_PORT_D, 0, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PD0_UART9_CTS              CHIP_PIN_ID(CHIP_PORT_D, 0, 11)
#endif
#define CHIP_PIN_PD0_EXMC_D2                CHIP_PIN_ID(CHIP_PORT_D, 0, 12)
#define CHIP_PIN_PD0_EXMC_DA2               CHIP_PIN_ID(CHIP_PORT_D, 0, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 0, 13) */
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD0_LCD_B1                 CHIP_PIN_ID(CHIP_PORT_D, 0, 14)
#endif
#define CHIP_PIN_PD0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 0, 15)


/*----- Пин PD1  ------------------------------------------------------------- */
#define CHIP_PIN_PD1_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 2) */
#define CHIP_PIN_PD1_DFSDM1_DATIN6          CHIP_PIN_ID(CHIP_PORT_D, 1, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD1_SAI3_SD_A              CHIP_PIN_ID(CHIP_PORT_D, 1, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 7) */
#define CHIP_PIN_PD1_UART4_TX               CHIP_PIN_ID(CHIP_PORT_D, 1, 8)
#define CHIP_PIN_PD1_FDCAN1_TX              CHIP_PIN_ID(CHIP_PORT_D, 1, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 11) */
#define CHIP_PIN_PD1_EXMC_D3                CHIP_PIN_ID(CHIP_PORT_D, 1, 12)
#define CHIP_PIN_PD1_EXMC_DA3               CHIP_PIN_ID(CHIP_PORT_D, 1, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 1, 14) */
#define CHIP_PIN_PD1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 1, 15)


/*----- Пин PD2  ------------------------------------------------------------- */
#define CHIP_PIN_PD2_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 2, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PD2_TRACED2                CHIP_PIN_ID(CHIP_PORT_D, 2, 0)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD2_EXMC_D7                CHIP_PIN_ID(CHIP_PORT_D, 2, 1)
#define CHIP_PIN_PD2_EXMC_DA7               CHIP_PIN_ID(CHIP_PORT_D, 2, 1)
#endif
#define CHIP_PIN_PD2_TIM3_ETR               CHIP_PIN_ID(CHIP_PORT_D, 2, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 2, 3) */
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD2_TIM15_BKIN             CHIP_PIN_ID(CHIP_PORT_D, 2, 4)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 2, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 2, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 2, 7) */
#define CHIP_PIN_PD2_UART5_RX               CHIP_PIN_ID(CHIP_PORT_D, 2, 8)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD2_LCD_B7                 CHIP_PIN_ID(CHIP_PORT_D, 2, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 2, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 2, 11) */
#define CHIP_PIN_PD2_SDMMC1_CMD             CHIP_PIN_ID(CHIP_PORT_D, 2, 12)
#define CHIP_PIN_PD2_DVP_D11                CHIP_PIN_ID(CHIP_PORT_D, 2, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PD2_PSSI_D11               CHIP_PIN_ID(CHIP_PORT_D, 2, 13)
#endif
#if defined CHIP_DEV_STM32H745
#define CHIP_PIN_PD2_LCD_B2                 CHIP_PIN_ID(CHIP_PORT_D, 2, 14)
#endif
#define CHIP_PIN_PD2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 2, 15)


/*----- Пин PD3  ------------------------------------------------------------- */
#define CHIP_PIN_PD3_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 2) */
#define CHIP_PIN_PD3_DFSDM1_CKOUT           CHIP_PIN_ID(CHIP_PORT_D, 3, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 4) */
#define CHIP_PIN_PD3_SPI2_SCK               CHIP_PIN_ID(CHIP_PORT_D, 3, 5)
#define CHIP_PIN_PD3_I2S2_CK                CHIP_PIN_ID(CHIP_PORT_D, 3, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 6) */
#define CHIP_PIN_PD3_USART2_CTS             CHIP_PIN_ID(CHIP_PORT_D, 3, 7)
#define CHIP_PIN_PD3_USART2_NSS             CHIP_PIN_ID(CHIP_PORT_D, 3, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 3, 11) */
#define CHIP_PIN_PD3_EXMC_CLK               CHIP_PIN_ID(CHIP_PORT_D, 3, 12)
#define CHIP_PIN_PD3_DVP_D5                 CHIP_PIN_ID(CHIP_PORT_D, 3, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PD3_PSSI_D5                CHIP_PIN_ID(CHIP_PORT_D, 3, 13)
#endif
#define CHIP_PIN_PD3_LCD_G7                 CHIP_PIN_ID(CHIP_PORT_D, 3, 14)
#define CHIP_PIN_PD3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 3, 15)


/*----- Пин PD4  ------------------------------------------------------------- */
#define CHIP_PIN_PD4_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 4, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 1) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PD4_HRTIM_FLT3             CHIP_PIN_ID(CHIP_PORT_D, 4, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD4_SAI3_FS_A              CHIP_PIN_ID(CHIP_PORT_D, 4, 6)
#endif
#define CHIP_PIN_PD4_USART2_RTS             CHIP_PIN_ID(CHIP_PORT_D, 4, 7)
#define CHIP_PIN_PD4_USART2_DE              CHIP_PIN_ID(CHIP_PORT_D, 4, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 8) */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD4_FDCAN1_RXFD_MODE       CHIP_PIN_ID(CHIP_PORT_D, 4, 9)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD4_OCTOSPIM_P1_IO4        CHIP_PIN_ID(CHIP_PORT_D, 4, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 11) */
#define CHIP_PIN_PD4_EXMC_NOE               CHIP_PIN_ID(CHIP_PORT_D, 4, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 4, 14) */
#define CHIP_PIN_PD4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 4, 15)

/*----- Пин PD5  ------------------------------------------------------------- */
#define CHIP_PIN_PD5_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 1) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PD5_HRTIM_EEV3             CHIP_PIN_ID(CHIP_PORT_D, 5, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 6) */
#define CHIP_PIN_PD5_USART2_TX              CHIP_PIN_ID(CHIP_PORT_D, 5, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 8) */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD5_FDCAN1_TXFD_MODE       CHIP_PIN_ID(CHIP_PORT_D, 5, 9)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD5_OCTOSPIM_P1_IO5        CHIP_PIN_ID(CHIP_PORT_D, 5, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 11) */
#define CHIP_PIN_PD5_EXMC_NWE               CHIP_PIN_ID(CHIP_PORT_D, 5, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 5, 14) */
#define CHIP_PIN_PD5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 5, 15)


/*----- Пин PD6  ------------------------------------------------------------- */
#define CHIP_PIN_PD6_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 6, 0) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD6_SAI4_D1                CHIP_PIN_ID(CHIP_PORT_D, 6, 1)
#endif
#define CHIP_PIN_PD6_SAI1_D1                CHIP_PIN_ID(CHIP_PORT_D, 6, 2)
#define CHIP_PIN_PD6_DFSDM1_CKIN4           CHIP_PIN_ID(CHIP_PORT_D, 6, 3)
#define CHIP_PIN_PD6_DFSDM1_DATIN1          CHIP_PIN_ID(CHIP_PORT_D, 6, 4)
#define CHIP_PIN_PD6_SPI3_MOSI              CHIP_PIN_ID(CHIP_PORT_D, 6, 5)
#define CHIP_PIN_PD6_I2S3_SDO               CHIP_PIN_ID(CHIP_PORT_D, 6, 5)
#define CHIP_PIN_PD6_SAI1_SD_A              CHIP_PIN_ID(CHIP_PORT_D, 6, 6)
#define CHIP_PIN_PD6_USART2_RX              CHIP_PIN_ID(CHIP_PORT_D, 6, 7)
#define CHIP_PIN_PD6_SAI4_SD_A              CHIP_PIN_ID(CHIP_PORT_D, 6, 8)
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD6_FDCAN2_RXFD_MODE       CHIP_PIN_ID(CHIP_PORT_D, 6, 9)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD6_OCTOSPIM_P1_IO6        CHIP_PIN_ID(CHIP_PORT_D, 6, 10)
#else
#define CHIP_PIN_PD6_SAI4_D1                CHIP_PIN_ID(CHIP_PORT_D, 6, 10)
#endif
#define CHIP_PIN_PD6_SDMMC2_CK              CHIP_PIN_ID(CHIP_PORT_D, 6, 11)
#define CHIP_PIN_PD6_EXMC_NWAIT             CHIP_PIN_ID(CHIP_PORT_D, 6, 12)
#define CHIP_PIN_PD6_DVP_D10                CHIP_PIN_ID(CHIP_PORT_D, 6, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PD6_PSSI_D10               CHIP_PIN_ID(CHIP_PORT_D, 6, 13)
#endif
#define CHIP_PIN_PD6_LCD_B2                 CHIP_PIN_ID(CHIP_PORT_D, 6, 14)
#define CHIP_PIN_PD6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 6, 15)


/*----- Пин PD7  ------------------------------------------------------------- */
#define CHIP_PIN_PD7_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 7, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 7, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 7, 2) */
#define CHIP_PIN_PD7_DFSDM1_DATIN4          CHIP_PIN_ID(CHIP_PORT_D, 7, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 7, 4) */
#define CHIP_PIN_PD7_SPI1_MOSI              CHIP_PIN_ID(CHIP_PORT_D, 7, 5)
#define CHIP_PIN_PD7_I2S1_SDO               CHIP_PIN_ID(CHIP_PORT_D, 7, 5)
#define CHIP_PIN_PD7_DFSDM1_CKIN1           CHIP_PIN_ID(CHIP_PORT_D, 7, 6)
#define CHIP_PIN_PD7_USART2_CK              CHIP_PIN_ID(CHIP_PORT_D, 7, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 7, 8) */
#define CHIP_PIN_PD7_SPDIFRX1_IN1           CHIP_PIN_ID(CHIP_PORT_D, 7, 9)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD7_OCTOSPIM_P1_IO7        CHIP_PIN_ID(CHIP_PORT_D, 7, 10)
#endif
#define CHIP_PIN_PD7_SDMMC2_CMD             CHIP_PIN_ID(CHIP_PORT_D, 7, 11)
#define CHIP_PIN_PD7_EXMC_NE1               CHIP_PIN_ID(CHIP_PORT_D, 7, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 7, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 7, 14) */
#define CHIP_PIN_PD7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 7, 15)


/*----- Пин PD8  ------------------------------------------------------------- */
#define CHIP_PIN_PD8_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 8, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 2) */
#define CHIP_PIN_PD8_DFSDM1_CKIN3           CHIP_PIN_ID(CHIP_PORT_D, 8, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD8_SAI3_SCK_B             CHIP_PIN_ID(CHIP_PORT_D, 8, 6)
#endif
#define CHIP_PIN_PD8_USART3_TX              CHIP_PIN_ID(CHIP_PORT_D, 8, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 8) */
#define CHIP_PIN_PD8_SPDIFRX1_IN2           CHIP_PIN_ID(CHIP_PORT_D, 8, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 11) */
#define CHIP_PIN_PD8_EXMC_D13               CHIP_PIN_ID(CHIP_PORT_D, 8, 12)
#define CHIP_PIN_PD8_EXMC_DA13              CHIP_PIN_ID(CHIP_PORT_D, 8, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 8, 14) */
#define CHIP_PIN_PD8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 8, 15)


/*----- Пин PD9  ------------------------------------------------------------- */
#define CHIP_PIN_PD9_GPIO                   CHIP_PIN_ID(CHIP_PORT_D, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 2) */
#define CHIP_PIN_PD9_DFSDM1_DATIN3          CHIP_PIN_ID(CHIP_PORT_D, 9, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD9_SAI3_SD_B              CHIP_PIN_ID(CHIP_PORT_D, 9, 6)
#endif
#define CHIP_PIN_PD9_USART3_RX              CHIP_PIN_ID(CHIP_PORT_D, 9, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 8) */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD9_FDCAN2_RXFD_MODE       CHIP_PIN_ID(CHIP_PORT_D, 9, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 11) */
#define CHIP_PIN_PD9_EXMC_D14               CHIP_PIN_ID(CHIP_PORT_D, 9, 12)
#define CHIP_PIN_PD9_EXMC_DA14              CHIP_PIN_ID(CHIP_PORT_D, 9, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 9, 14) */
#define CHIP_PIN_PD9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_D, 9, 15)


/*----- Пин PD10  ------------------------------------------------------------- */
#define CHIP_PIN_PD10_GPIO                  CHIP_PIN_ID(CHIP_PORT_D, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 2) */
#define CHIP_PIN_PD10_DFSDM1_CKOUT          CHIP_PIN_ID(CHIP_PORT_D, 10, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD10_SAI3_FS_B             CHIP_PIN_ID(CHIP_PORT_D, 10, 6)
#endif
#define CHIP_PIN_PD10_USART3_CK             CHIP_PIN_ID(CHIP_PORT_D, 10, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 8) */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD10_FDCAN2_TXFD_MODE      CHIP_PIN_ID(CHIP_PORT_D, 10, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 11) */
#define CHIP_PIN_PD10_EXMC_D15              CHIP_PIN_ID(CHIP_PORT_D, 10, 12)
#define CHIP_PIN_PD10_EXMC_DA15             CHIP_PIN_ID(CHIP_PORT_D, 10, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 10, 13) */
#define CHIP_PIN_PD10_LCD_B3                CHIP_PIN_ID(CHIP_PORT_D, 10, 14)
#define CHIP_PIN_PD10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_D, 10, 15)


/*----- Пин PD11 ------------------------------------------------------------- */
#define CHIP_PIN_PD11_GPIO                  CHIP_PIN_ID(CHIP_PORT_D, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 2) */
#define CHIP_PIN_PD11_LPTIM2_IN2            CHIP_PIN_ID(CHIP_PORT_D, 11, 3)
#define CHIP_PIN_PD11_I2C4_SMBA             CHIP_PIN_ID(CHIP_PORT_D, 11, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 6) */
#define CHIP_PIN_PD11_USART3_CTS            CHIP_PIN_ID(CHIP_PORT_D, 11, 7)
#define CHIP_PIN_PD11_USART3_NSS            CHIP_PIN_ID(CHIP_PORT_D, 11, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD11_OCTOSPIM_P1_IO0       CHIP_PIN_ID(CHIP_PORT_D, 11, 9)
#else
#define CHIP_PIN_PD11_QUADSPI_BK1_IO0       CHIP_PIN_ID(CHIP_PORT_D, 11, 9)
#endif
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD11_SAI2_SD_A             CHIP_PIN_ID(CHIP_PORT_D, 11, 10)
#else
#define CHIP_PIN_PD11_SAI4_SD_A             CHIP_PIN_ID(CHIP_PORT_D, 11, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 11) */
#define CHIP_PIN_PD11_EXMC_A16              CHIP_PIN_ID(CHIP_PORT_D, 11, 12)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD11_EXMC_CLE              CHIP_PIN_ID(CHIP_PORT_D, 11, 12)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 11, 14) */
#define CHIP_PIN_PD11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_D, 11, 15)


/*----- Пин PD12 ------------------------------------------------------------- */
#define CHIP_PIN_PD12_GPIO                  CHIP_PIN_ID(CHIP_PORT_D, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 12, 0) */
#define CHIP_PIN_PD12_LPTIM1_IN1            CHIP_PIN_ID(CHIP_PORT_D, 12, 1)
#define CHIP_PIN_PD12_TIM4_CH1              CHIP_PIN_ID(CHIP_PORT_D, 12, 2)
#define CHIP_PIN_PD12_LPTIM2_IN1            CHIP_PIN_ID(CHIP_PORT_D, 12, 3)
#define CHIP_PIN_PD12_I2C4_SCL              CHIP_PIN_ID(CHIP_PORT_D, 12, 4)
#if CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PD12_FDCAN3_RX             CHIP_PIN_ID(CHIP_PORT_D, 12, 5)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 12, 6) */
#define CHIP_PIN_PD12_USART3_RTS            CHIP_PIN_ID(CHIP_PORT_D, 12, 7)
#define CHIP_PIN_PD12_USART3_DE             CHIP_PIN_ID(CHIP_PORT_D, 12, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 12, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD12_OCTOSPIM_P1_IO1       CHIP_PIN_ID(CHIP_PORT_D, 12, 9)
#else
#define CHIP_PIN_PD12_QUADSPI_BK1_IO1       CHIP_PIN_ID(CHIP_PORT_D, 12, 9)
#endif
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD12_SAI2_FS_A             CHIP_PIN_ID(CHIP_PORT_D, 12, 10)
#else
#define CHIP_PIN_PD12_SAI4_FS_A             CHIP_PIN_ID(CHIP_PORT_D, 12, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 12, 11) */
#define CHIP_PIN_PD12_EXMC_A17              CHIP_PIN_ID(CHIP_PORT_D, 12, 12)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD12_EXMC_ALE              CHIP_PIN_ID(CHIP_PORT_D, 12, 12)
#define CHIP_PIN_PD12_DVP_D12               CHIP_PIN_ID(CHIP_PORT_D, 12, 13) */
#define CHIP_PIN_PD12_DVP_PSSI              CHIP_PIN_ID(CHIP_PORT_D, 12, 13) */
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 12, 14) */
#define CHIP_PIN_PD12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_D, 12, 15)


/*----- Пин PD13 ------------------------------------------------------------- */
#define CHIP_PIN_PD13_GPIO                  CHIP_PIN_ID(CHIP_PORT_D, 13, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 13, 0) */
#define CHIP_PIN_PD13_LPTIM1_OUT            CHIP_PIN_ID(CHIP_PORT_D, 13, 1)
#define CHIP_PIN_PD13_TIM4_CH2              CHIP_PIN_ID(CHIP_PORT_D, 13, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 13, 3) */
#define CHIP_PIN_PD13_I2C4_SDA              CHIP_PIN_ID(CHIP_PORT_D, 13, 4)
#if CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PD13_FDCAN3_TX             CHIP_PIN_ID(CHIP_PORT_D, 13, 5)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 13, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 13, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 13, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PD13_OCTOSPIM_P1_IO3       CHIP_PIN_ID(CHIP_PORT_D, 13, 9)
#else
#define CHIP_PIN_PD13_QUADSPI_BK1_IO3       CHIP_PIN_ID(CHIP_PORT_D, 13, 9)
#endif
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD13_SAI2_SCK_A            CHIP_PIN_ID(CHIP_PORT_D, 13, 10)
#else
#define CHIP_PIN_PD13_SAI4_SCK_A            CHIP_PIN_ID(CHIP_PORT_D, 13, 10)
#endif
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PD13_UART9_RTS             CHIP_PIN_ID(CHIP_PORT_D, 13, 11)
#define CHIP_PIN_PD13_UART9_DE              CHIP_PIN_ID(CHIP_PORT_D, 13, 11)
#endif
#define CHIP_PIN_PD13_EXMC_A18              CHIP_PIN_ID(CHIP_PORT_D, 13, 12)
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PD13_DVP_D13               CHIP_PIN_ID(CHIP_PORT_D, 13, 13)
#define CHIP_PIN_PD13_PSSI_D13              CHIP_PIN_ID(CHIP_PORT_D, 13, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 13, 14) */
#define CHIP_PIN_PD13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_D, 13, 15)


/*----- Пин PD14 ------------------------------------------------------------- */
#define CHIP_PIN_PD14_GPIO                  CHIP_PIN_ID(CHIP_PORT_D, 14, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 1) */
#define CHIP_PIN_PD14_TIM4_CH3              CHIP_PIN_ID(CHIP_PORT_D, 14, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD14_SAI3_MCLK_B           CHIP_PIN_ID(CHIP_PORT_D, 14, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 7) */
#define CHIP_PIN_PD14_UART8_CTS             CHIP_PIN_ID(CHIP_PORT_D, 14, 8)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PD14_UART9_RX              CHIP_PIN_ID(CHIP_PORT_D, 14, 11)
#endif
#define CHIP_PIN_PD14_EXMC_D0               CHIP_PIN_ID(CHIP_PORT_D, 14, 12)
#define CHIP_PIN_PD14_EXMC_DA0              CHIP_PIN_ID(CHIP_PORT_D, 14, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 14, 14) */
#define CHIP_PIN_PD14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_D, 14, 15)


/*----- Пин PD15 ------------------------------------------------------------- */
#define CHIP_PIN_PD15_GPIO                  CHIP_PIN_ID(CHIP_PORT_D, 15, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 1) */
#define CHIP_PIN_PD15_TIM4_CH4              CHIP_PIN_ID(CHIP_PORT_D, 15, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 5) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PD15_SAI3_MCLK_A           CHIP_PIN_ID(CHIP_PORT_D, 15, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 7) */
#define CHIP_PIN_PD15_UART8_RTS             CHIP_PIN_ID(CHIP_PORT_D, 15, 8)
#define CHIP_PIN_PD15_UART8_DE              CHIP_PIN_ID(CHIP_PORT_D, 15, 8)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PD15_UART9_TX              CHIP_PIN_ID(CHIP_PORT_D, 15, 11)
#endif
#define CHIP_PIN_PD15_EXMC_D1               CHIP_PIN_ID(CHIP_PORT_D, 15, 12)
#define CHIP_PIN_PD15_EXMC_DA1              CHIP_PIN_ID(CHIP_PORT_D, 15, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_D, 15, 14) */
#define CHIP_PIN_PD15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_D, 15, 15)


/*********************************** Порт E ************************************/
/*----- Пин PE0  ------------------------------------------------------------- */
#define CHIP_PIN_PE0_GPIO                   CHIP_PIN_ID(CHIP_PORT_E, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 0, 0) */
#define CHIP_PIN_PE0_LPTIM1_ETR             CHIP_PIN_ID(CHIP_PORT_E, 0, 1)
#define CHIP_PIN_PE0_TIM4_ETR               CHIP_PIN_ID(CHIP_PORT_E, 0, 2)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PE0_HRTIM_SCIN             CHIP_PIN_ID(CHIP_PORT_E, 0, 3)
#endif
#define CHIP_PIN_PE0_LPTIM2_ETR             CHIP_PIN_ID(CHIP_PORT_E, 0, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 0, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 0, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 0, 7) */
#define CHIP_PIN_PE0_UART8_RX               CHIP_PIN_ID(CHIP_PORT_E, 0, 8)
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PE0_FDCAN1_RXFD_MODE       CHIP_PIN_ID(CHIP_PORT_E, 0, 9)
#endif
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PE0_SAI2_MCLK_A            CHIP_PIN_ID(CHIP_PORT_E, 0, 10)
#else
#define CHIP_PIN_PE0_SAI4_MCLK_A            CHIP_PIN_ID(CHIP_PORT_E, 0, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 0, 11) */
#define CHIP_PIN_PE0_EXMC_NBL0              CHIP_PIN_ID(CHIP_PORT_E, 0, 12)
#define CHIP_PIN_PE0_DVP_D2                 CHIP_PIN_ID(CHIP_PORT_E, 0, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PE0_PSSI_D2                CHIP_PIN_ID(CHIP_PORT_E, 0, 13)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PE0_LCD_R0                 CHIP_PIN_ID(CHIP_PORT_E, 0, 14)
#endif
#define CHIP_PIN_PE0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_E, 0, 15)


/*----- Пин PE1  ------------------------------------------------------------- */
#define CHIP_PIN_PE1_GPIO                   CHIP_PIN_ID(CHIP_PORT_E, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 0) */
#define CHIP_PIN_PE1_LPTIM1_IN2             CHIP_PIN_ID(CHIP_PORT_E, 1, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 2) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PE1_HRTIM_SCOUT            CHIP_PIN_ID(CHIP_PORT_E, 1, 3)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 7) */
#define CHIP_PIN_PE1_UART8_TX               CHIP_PIN_ID(CHIP_PORT_E, 1, 8)
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PE1_FDCAN1_TXFD_MODE       CHIP_PIN_ID(CHIP_PORT_E, 1, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 1, 11) */
#define CHIP_PIN_PE1_EXMC_NBL1              CHIP_PIN_ID(CHIP_PORT_E, 1, 12)
#define CHIP_PIN_PE1_DVP_D3                 CHIP_PIN_ID(CHIP_PORT_E, 1, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PE1_PSSI_D3                CHIP_PIN_ID(CHIP_PORT_E, 1, 13)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PE1_LCD_R6                 CHIP_PIN_ID(CHIP_PORT_E, 1, 14)
#endif
#define CHIP_PIN_PE1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_E, 1, 15)


/*----- Пин PE2  ------------------------------------------------------------- */
#define CHIP_PIN_PE2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 2)
#define CHIP_PIN_PE2_TRACE_CLK            CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 1) */
#define CHIP_PIN_PE2_SAI1_CK1             CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 2)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 3) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PE2_USART10_RX           CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 4)
#endif
#define CHIP_PIN_PE2_SPI4_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 5)
#define CHIP_PIN_PE2_SAI1_MCLK_A          CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 6)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 7) */
#define CHIP_PIN_PE2_SAI4_MCLK_A          CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PE2_OCTOSPIM_P1_IO2      CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 9)
#else
#define CHIP_PIN_PE2_QUADSPI_BK1_IO2      CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 9)
#endif
#define CHIP_PIN_PE2_SAI4_CK1             CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 10)
#define CHIP_PIN_PE2_ETH_MII_TXD3         CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 11)
#define CHIP_PIN_PE2_EXMC_A23             CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 12)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 14) */
#define CHIP_PIN_PE2_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 15)


/*----- Пин PE3  ------------------------------------------------------------- */
#define CHIP_PIN_PE3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 3)
#define CHIP_PIN_PE3_TRACE_D0             CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 1) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 3) */
#define CHIP_PIN_PE3_TIM15_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 5) */
#define CHIP_PIN_PE3_SAI1_SD_B            CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 6)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 7) */
#define CHIP_PIN_PE3_SAI4_SD_B            CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 8)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 9) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PE3_USART10_TX           CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 11)
#endif
#define CHIP_PIN_PE3_EXMC_A19             CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 12)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 14) */
#define CHIP_PIN_PE3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 15)


/*----- Пин PE4  ------------------------------------------------------------- */
#define CHIP_PIN_PE4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 4)
#define CHIP_PIN_PE4_TRACE_D1             CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 1) */
#define CHIP_PIN_PE4_SAI1_D2              CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 2)
#define CHIP_PIN_PE4_DFSDM1_DATIN3        CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 3)
#define CHIP_PIN_PE4_TIM15_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 4)
#define CHIP_PIN_PE4_SPI4_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 5)
#define CHIP_PIN_PE4_SAI1_FS_A            CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 6)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 7) */
#define CHIP_PIN_PE4_SAI4_FS_A            CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 8)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 9) */
#define CHIP_PIN_PE4_SAI4_D2              CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 11) */
#define CHIP_PIN_PE4_EXMC_A20             CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 12)
#define CHIP_PIN_PE4_DVP_D4               CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PE4_PSSI_D4              CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 13)
#endif
#define CHIP_PIN_PE4_LCD_B0               CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 14)
#define CHIP_PIN_PE4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 15)


/*----- Пин PE5  ------------------------------------------------------------- */
#define CHIP_PIN_PE5_GPIO                   CHIP_PIN_ID(CHIP_PORT_E, 5, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PE5_TRACED2                CHIP_PIN_ID(CHIP_PORT_E, 5, 0)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 5, 1) */
#define CHIP_PIN_PE5_SAI1_CK2               CHIP_PIN_ID(CHIP_PORT_E, 5, 2)
#define CHIP_PIN_PE5_DFSDM1_CKIN3           CHIP_PIN_ID(CHIP_PORT_E, 5, 3)
#define CHIP_PIN_PE5_TIM15_CH1              CHIP_PIN_ID(CHIP_PORT_E, 5, 4)
#define CHIP_PIN_PE5_SPI4_MISO              CHIP_PIN_ID(CHIP_PORT_E, 5, 5)
#define CHIP_PIN_PE5_SAI1_SCK_A             CHIP_PIN_ID(CHIP_PORT_E, 5, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 5, 7) */
#define CHIP_PIN_PE5_SAI4_SCK_A             CHIP_PIN_ID(CHIP_PORT_E, 5, 8)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 5, 9) */
#define CHIP_PIN_PE5_SAI4_CK2               CHIP_PIN_ID(CHIP_PORT_E, 5, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 5, 11) */
#define CHIP_PIN_PE5_EXMC_A21               CHIP_PIN_ID(CHIP_PORT_E, 5, 12)
#define CHIP_PIN_PE5_DVP_D6                 CHIP_PIN_ID(CHIP_PORT_E, 5, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PE5_PSSI_D6                CHIP_PIN_ID(CHIP_PORT_E, 5, 13)
#endif
#define CHIP_PIN_PE5_LCD_G0                 CHIP_PIN_ID(CHIP_PORT_E, 5, 14)
#define CHIP_PIN_PE5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_E, 5, 15)


/*----- Пин PE6  ------------------------------------------------------------- */
#define CHIP_PIN_PE6_GPIO                   CHIP_PIN_ID(CHIP_PORT_E, 6, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PE6_TRACED3                CHIP_PIN_ID(CHIP_PORT_E, 6, 0)
#define CHIP_PIN_PE6_TIM1_BKIN2             CHIP_PIN_ID(CHIP_PORT_E, 6, 1)
#define CHIP_PIN_PE6_SAI1_D1                CHIP_PIN_ID(CHIP_PORT_E, 6, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 6, 3) */
#define CHIP_PIN_PE6_TIM15_CH2              CHIP_PIN_ID(CHIP_PORT_E, 6, 4)
#define CHIP_PIN_PE6_SPI4_MOSI              CHIP_PIN_ID(CHIP_PORT_E, 6, 5)
#define CHIP_PIN_PE6_SAI1_SD_A              CHIP_PIN_ID(CHIP_PORT_E, 6, 6)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 6, 7) */
#define CHIP_PIN_PE6_SAI4_SD_A              CHIP_PIN_ID(CHIP_PORT_E, 6, 8)
#define CHIP_PIN_PE6_SAI4_D1                CHIP_PIN_ID(CHIP_PORT_E, 6, 9)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PE6_SAI2_MCLK_B            CHIP_PIN_ID(CHIP_PORT_E, 6, 10)
#else
#define CHIP_PIN_PE6_SAI4_MCLK_B            CHIP_PIN_ID(CHIP_PORT_E, 6, 10)
#endif
#define CHIP_PIN_PE6_TIM1_BKIN2_COMP12      CHIP_PIN_ID(CHIP_PORT_E, 6, 11)
#define CHIP_PIN_PE6_EXMC_A22               CHIP_PIN_ID(CHIP_PORT_E, 6, 12)
#define CHIP_PIN_PE6_DVP_D7                 CHIP_PIN_ID(CHIP_PORT_E, 6, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PE6_PSSI_D7                CHIP_PIN_ID(CHIP_PORT_E, 6, 13)
#endif
#define CHIP_PIN_PE6_LCD_G1                 CHIP_PIN_ID(CHIP_PORT_E, 6, 14)
#define CHIP_PIN_PE6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_E, 6, 15)


/*----- Пин PE7  ------------------------------------------------------------- */
#define CHIP_PIN_PE7_GPIO                   CHIP_PIN_ID(CHIP_PORT_E, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 0) */
#define CHIP_PIN_PE7_TIM1_ETR               CHIP_PIN_ID(CHIP_PORT_E, 7, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 2) */
#define CHIP_PIN_PE7_DFSDM1_DATIN2          CHIP_PIN_ID(CHIP_PORT_E, 7, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 6) */
#define CHIP_PIN_PE7_UART7_RX               CHIP_PIN_ID(CHIP_PORT_E, 7, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 9) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PE7_OCTOSPIM_P1_IO4        CHIP_PIN_ID(CHIP_PORT_E, 7, 10)
#else
#define CHIP_PIN_PE7_QUADSPI_BK2_IO0        CHIP_PIN_ID(CHIP_PORT_E, 7, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 11) */
#define CHIP_PIN_PE7_EXMC_D4                CHIP_PIN_ID(CHIP_PORT_E, 7, 12)
#define CHIP_PIN_PE7_EXMC_DA4               CHIP_PIN_ID(CHIP_PORT_E, 7, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 7, 14) */
#define CHIP_PIN_PE7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_E, 7, 15)


/*----- Пин PE8  ------------------------------------------------------------- */
#define CHIP_PIN_PE8_GPIO                   CHIP_PIN_ID(CHIP_PORT_E, 8, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 0) */
#define CHIP_PIN_PE8_TIM1_CH1N              CHIP_PIN_ID(CHIP_PORT_E, 8, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 2) */
#define CHIP_PIN_PE8_DFSDM1_CKIN2           CHIP_PIN_ID(CHIP_PORT_E, 8, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 6) */
#define CHIP_PIN_PE8_UART7_TX               CHIP_PIN_ID(CHIP_PORT_E, 8, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 9) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PE8_OCTOSPIM_P1_IO5        CHIP_PIN_ID(CHIP_PORT_E, 8, 10)
#else
#define CHIP_PIN_PE8_QUADSPI_BK2_IO1        CHIP_PIN_ID(CHIP_PORT_E, 8, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 11) */
#define CHIP_PIN_PE8_EXMC_D5                CHIP_PIN_ID(CHIP_PORT_E, 8, 12)
#define CHIP_PIN_PE8_EXMC_DA5               CHIP_PIN_ID(CHIP_PORT_E, 8, 12)
#define CHIP_PIN_PE8_COMP2_OUT              CHIP_PIN_ID(CHIP_PORT_E, 8, 13)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 8, 14) */
#define CHIP_PIN_PE8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_E, 8, 15)


/*----- Пин PE9  ------------------------------------------------------------- */
#define CHIP_PIN_PE9_GPIO                   CHIP_PIN_ID(CHIP_PORT_E, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 0) */
#define CHIP_PIN_PE9_TIM1_CH1               CHIP_PIN_ID(CHIP_PORT_E, 9, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 2) */
#define CHIP_PIN_PE9_DFSDM1_CKOUT           CHIP_PIN_ID(CHIP_PORT_E, 9, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 6) */
#define CHIP_PIN_PE9_UART7_RTS              CHIP_PIN_ID(CHIP_PORT_E, 9, 7)
#define CHIP_PIN_PE9_UART7_DE               CHIP_PIN_ID(CHIP_PORT_E, 9, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 9) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PE9_OCTOSPIM_P1_IO6        CHIP_PIN_ID(CHIP_PORT_E, 9, 10)
#else
#define CHIP_PIN_PE9_QUADSPI_BK2_IO2        CHIP_PIN_ID(CHIP_PORT_E, 9, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 11) */
#define CHIP_PIN_PE9_EXMC_D6                CHIP_PIN_ID(CHIP_PORT_E, 9, 12)
#define CHIP_PIN_PE9_EXMC_DA6               CHIP_PIN_ID(CHIP_PORT_E, 9, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 9, 14) */
#define CHIP_PIN_PE9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_E, 9, 15)


/*----- Пин PE10 ------------------------------------------------------------- */
#define CHIP_PIN_PE10_GPIO                  CHIP_PIN_ID(CHIP_PORT_E, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 0) */
#define CHIP_PIN_PE10_TIM1_CH2N             CHIP_PIN_ID(CHIP_PORT_E, 10, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 2) */
#define CHIP_PIN_PE10_DFSDM1_DATIN4         CHIP_PIN_ID(CHIP_PORT_E, 10, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 6) */
#define CHIP_PIN_PE10_UART7_CTS             CHIP_PIN_ID(CHIP_PORT_E, 10, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 9) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PE10_OCTOSPIM_P1_IO7       CHIP_PIN_ID(CHIP_PORT_E, 10, 10)
#else
#define CHIP_PIN_PE10_QUADSPI_BK2_IO3       CHIP_PIN_ID(CHIP_PORT_E, 10, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 11) */
#define CHIP_PIN_PE10_EXMC_D7               CHIP_PIN_ID(CHIP_PORT_E, 10, 12)
#define CHIP_PIN_PE10_EXMC_DA7              CHIP_PIN_ID(CHIP_PORT_E, 10, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 10, 14) */
#define CHIP_PIN_PE10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_E, 10, 15)


/*----- Пин PE11 ------------------------------------------------------------- */
#define CHIP_PIN_PE11_GPIO                  CHIP_PIN_ID(CHIP_PORT_E, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 0) */
#define CHIP_PIN_PE11_TIM1_CH2              CHIP_PIN_ID(CHIP_PORT_E, 11, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 2) */
#define CHIP_PIN_PE11_DFSDM1_CKIN4          CHIP_PIN_ID(CHIP_PORT_E, 11, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 4) */
#define CHIP_PIN_PE11_SPI4_NSS              CHIP_PIN_ID(CHIP_PORT_E, 11, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 9) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PE11_SAI2_SD_B             CHIP_PIN_ID(CHIP_PORT_E, 11, 10)
#else
#define CHIP_PIN_PE11_SAI4_SD_B             CHIP_PIN_ID(CHIP_PORT_E, 11, 10)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PE11_OCTOSPIM_P1_NCS       CHIP_PIN_ID(CHIP_PORT_E, 11, 11)
#endif
#define CHIP_PIN_PE11_EXMC_D8               CHIP_PIN_ID(CHIP_PORT_E, 11, 12)
#define CHIP_PIN_PE11_EXMC_DA8              CHIP_PIN_ID(CHIP_PORT_E, 11, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 11, 13) */
#define CHIP_PIN_PE11_LCD_G3                CHIP_PIN_ID(CHIP_PORT_E, 11, 14)
#define CHIP_PIN_PE11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_E, 11, 15)


/*----- Пин PE12 ------------------------------------------------------------- */
#define CHIP_PIN_PE12_GPIO                  CHIP_PIN_ID(CHIP_PORT_E, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 0) */
#define CHIP_PIN_PE12_TIM1_CH3N             CHIP_PIN_ID(CHIP_PORT_E, 12, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 2) */
#define CHIP_PIN_PE12_DFSDM1_DATIN5         CHIP_PIN_ID(CHIP_PORT_E, 12, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 4) */
#define CHIP_PIN_PE12_SPI4_SCK              CHIP_PIN_ID(CHIP_PORT_E, 12, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 9) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PE12_SAI2_SCK_B            CHIP_PIN_ID(CHIP_PORT_E, 12, 10)
#else
#define CHIP_PIN_PE12_SAI4_SCK_B            CHIP_PIN_ID(CHIP_PORT_E, 12, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 12, 11) */
#define CHIP_PIN_PE12_EXMC_D9               CHIP_PIN_ID(CHIP_PORT_E, 12, 12)
#define CHIP_PIN_PE12_EXMC_DA9              CHIP_PIN_ID(CHIP_PORT_E, 12, 12)
#define CHIP_PIN_PE12_COMP1_OUT             CHIP_PIN_ID(CHIP_PORT_E, 12, 13)
#define CHIP_PIN_PE12_LCD_B4                CHIP_PIN_ID(CHIP_PORT_E, 12, 14)
#define CHIP_PIN_PE12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_E, 12, 15)


/*----- Пин PE13 ------------------------------------------------------------- */
#define CHIP_PIN_PE13_GPIO                  CHIP_PIN_ID(CHIP_PORT_E, 13, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 0) */
#define CHIP_PIN_PE13_TIM1_CH3              CHIP_PIN_ID(CHIP_PORT_E, 13, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 2) */
#define CHIP_PIN_PE13_DFSDM1_CKIN5          CHIP_PIN_ID(CHIP_PORT_E, 13, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 4) */
#define CHIP_PIN_PE13_SPI4_MISO             CHIP_PIN_ID(CHIP_PORT_E, 13, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 9) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PE13_SAI2_FS_B             CHIP_PIN_ID(CHIP_PORT_E, 13, 10)
#else
#define CHIP_PIN_PE13_SAI4_FS_B             CHIP_PIN_ID(CHIP_PORT_E, 13, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 13, 11) */
#define CHIP_PIN_PE13_EXMC_D10              CHIP_PIN_ID(CHIP_PORT_E, 13, 12)
#define CHIP_PIN_PE13_EXMC_DA10             CHIP_PIN_ID(CHIP_PORT_E, 13, 12)
#define CHIP_PIN_PE13_COMP2_OUT             CHIP_PIN_ID(CHIP_PORT_E, 13, 13)
#define CHIP_PIN_PE13_LCD_DE                CHIP_PIN_ID(CHIP_PORT_E, 13, 14)
#define CHIP_PIN_PE13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_E, 13, 15)

/*----- Пин PE14 ------------------------------------------------------------- */
#define CHIP_PIN_PE14_GPIO                  CHIP_PIN_ID(CHIP_PORT_E, 14, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 0) */
#define CHIP_PIN_PE14_TIM1_CH4              CHIP_PIN_ID(CHIP_PORT_E, 14, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 4) */
#define CHIP_PIN_PE14_SPI4_MOSI             CHIP_PIN_ID(CHIP_PORT_E, 14, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 9) */
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PE14_SAI2_MCLK_B           CHIP_PIN_ID(CHIP_PORT_E, 14, 10)
#else
#define CHIP_PIN_PE14_SAI4_MCLK_B           CHIP_PIN_ID(CHIP_PORT_E, 14, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 11) */
#define CHIP_PIN_PE14_EXMC_D11              CHIP_PIN_ID(CHIP_PORT_E, 14, 12)
#define CHIP_PIN_PE14_EXMC_DA11             CHIP_PIN_ID(CHIP_PORT_E, 14, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 14, 13) */
#define CHIP_PIN_PE14_LCD_CLK               CHIP_PIN_ID(CHIP_PORT_E, 14, 14)
#define CHIP_PIN_PE14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_E, 14, 15)


/*----- Пин PE15 ------------------------------------------------------------- */
#define CHIP_PIN_PE15_GPIO                  CHIP_PIN_ID(CHIP_PORT_E, 15, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 0) */
#define CHIP_PIN_PE15_TIM1_BKIN             CHIP_PIN_ID(CHIP_PORT_E, 15, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_E, 15, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PE15_USART10_CK            CHIP_PIN_ID(CHIP_PORT_E, 15, 11)
#endif
#define CHIP_PIN_PE15_EXMC_D12              CHIP_PIN_ID(CHIP_PORT_E, 15, 12)
#define CHIP_PIN_PE15_EXMC_DA12             CHIP_PIN_ID(CHIP_PORT_E, 15, 12)
#define CHIP_PIN_PE15_TIM1_BKIN_COMP12      CHIP_PIN_ID(CHIP_PORT_E, 15, 13)
#define CHIP_PIN_PE15_LCD_R7                CHIP_PIN_ID(CHIP_PORT_E, 15, 14)
#define CHIP_PIN_PE15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_E, 15, 15)


/*********************************** Порт F ***********************************/
/*----- Пин PF0  ------------------------------------------------------------- */
#define CHIP_PIN_PF0_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 3) */
#define CHIP_PIN_PF0_I2C2_SDA               CHIP_PIN_ID(CHIP_PORT_F, 0, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 5) */
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PF0_I2C5_SDA               CHIP_PIN_ID(CHIP_PORT_F, 0, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF0_OCTOSPIM_P2_IO0        CHIP_PIN_ID(CHIP_PORT_F, 0, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 11) */
#define CHIP_PIN_PF0_EXMC_A0                CHIP_PIN_ID(CHIP_PORT_F, 0, 12)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF0_TIM23_CH1              CHIP_PIN_ID(CHIP_PORT_F, 0, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 0, 14) */
#define CHIP_PIN_PF0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 0, 15)


/*----- Пин PF1  ------------------------------------------------------------- */
#define CHIP_PIN_PF1_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 3) */
#define CHIP_PIN_PF1_I2C2_SCL               CHIP_PIN_ID(CHIP_PORT_F, 1, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 5) */
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PF1_I2C5_SCL               CHIP_PIN_ID(CHIP_PORT_F, 1, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF1_OCTOSPIM_P2_IO1        CHIP_PIN_ID(CHIP_PORT_F, 1, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 11) */
#define CHIP_PIN_PF1_EXMC_A1                CHIP_PIN_ID(CHIP_PORT_F, 1, 12)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF1_TIM23_CH2              CHIP_PIN_ID(CHIP_PORT_F, 1, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 1, 14) */
#define CHIP_PIN_PF1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 1, 15)


/*----- Пин PF2  ------------------------------------------------------------- */
#define CHIP_PIN_PF2_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 2, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 3) */
#define CHIP_PIN_PF2_I2C2_SMBA              CHIP_PIN_ID(CHIP_PORT_F, 2, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 5) */
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_PIN_PF2_I2C5_SMBA              CHIP_PIN_ID(CHIP_PORT_F, 2, 6)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF2_OCTOSPIM_P2_IO2        CHIP_PIN_ID(CHIP_PORT_F, 2, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 11) */
#define CHIP_PIN_PF2_EXMC_A2                CHIP_PIN_ID(CHIP_PORT_F, 2, 12)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF2_TIM23_CH3              CHIP_PIN_ID(CHIP_PORT_F, 2, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 2, 14) */
#define CHIP_PIN_PF2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 2, 15)


/*----- Пин PF3  ------------------------------------------------------------- */
#define CHIP_PIN_PF3_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF3_OCTOSPIM_P2_IO3        CHIP_PIN_ID(CHIP_PORT_F, 3, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 11) */
#define CHIP_PIN_PF3_EXMC_A3                CHIP_PIN_ID(CHIP_PORT_F, 3, 12)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF3_TIM23_CH4              CHIP_PIN_ID(CHIP_PORT_F, 3, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 3, 14) */
#define CHIP_PIN_PF3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 3, 15)
#define CHIP_PIN_PF3_ADC3_INP5              CHIP_PIN_ID(CHIP_PORT_F, 3, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF4  ------------------------------------------------------------- */
#define CHIP_PIN_PF4_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 4, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF4_OCTOSPIM_P2_CLK        CHIP_PIN_ID(CHIP_PORT_F, 4, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 11) */
#define CHIP_PIN_PF4_EXMC_A4                CHIP_PIN_ID(CHIP_PORT_F, 4, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 4, 14) */
#define CHIP_PIN_PF4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 4, 15)
#define CHIP_PIN_PF4_ADC3_INN5              CHIP_PIN_ID(CHIP_PORT_F, 4, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PF4_ADC3_INP9              CHIP_PIN_ID(CHIP_PORT_F, 4, CHIP_PIN_FUNC_ANALOG)

/*----- Пин PF5  ------------------------------------------------------------- */
#define CHIP_PIN_PF5_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF4_OCTOSPIM_P2_NCLK       CHIP_PIN_ID(CHIP_PORT_F, 5, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 11) */
#define CHIP_PIN_PF5_EXMC_A5                CHIP_PIN_ID(CHIP_PORT_F, 5, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 5, 14) */
#define CHIP_PIN_PF5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 5, 15)
#define CHIP_PIN_PF5_ADC3_INP4              CHIP_PIN_ID(CHIP_PORT_F, 5, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF6  ------------------------------------------------------------- */
#define CHIP_PIN_PF6_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 6, 0) */
#define CHIP_PIN_PF6_TIM16_CH1              CHIP_PIN_ID(CHIP_PORT_F, 6, 1)
#if CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PF6_FDCAN3_RX              CHIP_PIN_ID(CHIP_PORT_F, 6, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 6, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 6, 4) */
#define CHIP_PIN_PF6_SPI5_NSS               CHIP_PIN_ID(CHIP_PORT_F, 6, 5)
#define CHIP_PIN_PF6_SAI1_SD_B              CHIP_PIN_ID(CHIP_PORT_F, 6, 6)
#define CHIP_PIN_PF6_UART7_RX               CHIP_PIN_ID(CHIP_PORT_F, 6, 7)
#define CHIP_PIN_PF6_SAI4_SD_B              CHIP_PIN_ID(CHIP_PORT_F, 6, 8)
#if !CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF6_QUADSPI_BK1_IO3        CHIP_PIN_ID(CHIP_PORT_F, 6, 9)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF6_OCTOSPIM_P1_IO3        CHIP_PIN_ID(CHIP_PORT_F, 6, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 6, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 6, 12) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF6_TIM23_CH1              CHIP_PIN_ID(CHIP_PORT_F, 6, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 6, 14) */
#define CHIP_PIN_PF6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 6, 15)
#define CHIP_PIN_PF6_ADC3_INN4              CHIP_PIN_ID(CHIP_PORT_F, 6, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PF6_ADC3_INP8              CHIP_PIN_ID(CHIP_PORT_F, 6, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF7  ------------------------------------------------------------- */
#define CHIP_PIN_PF7_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 7, 0) */
#define CHIP_PIN_PF7_TIM17_CH1              CHIP_PIN_ID(CHIP_PORT_F, 7, 1)
#if CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PF7_FDCAN3_TX              CHIP_PIN_ID(CHIP_PORT_F, 7, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 7, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 7, 4) */
#define CHIP_PIN_PF7_SPI5_SCK               CHIP_PIN_ID(CHIP_PORT_F, 7, 5)
#define CHIP_PIN_PF7_SAI1_MCLK_B            CHIP_PIN_ID(CHIP_PORT_F, 7, 6)
#define CHIP_PIN_PF7_UART7_TX               CHIP_PIN_ID(CHIP_PORT_F, 7, 7)
#define CHIP_PIN_PF7_SAI4_MCLK_B            CHIP_PIN_ID(CHIP_PORT_F, 7, 8)
#if !CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF7_QUADSPI_BK1_IO2        CHIP_PIN_ID(CHIP_PORT_F, 7, 9)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF7_OCTOSPIM_P1_IO2        CHIP_PIN_ID(CHIP_PORT_F, 7, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 7, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 7, 12) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF7_TIM23_CH2              CHIP_PIN_ID(CHIP_PORT_F, 7, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 7, 14) */
#define CHIP_PIN_PF7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 7, 15)
#define CHIP_PIN_PF7_ADC3_INP3              CHIP_PIN_ID(CHIP_PORT_F, 7, CHIP_PIN_FUNC_ANALOG)

/*----- Пин PF8  ------------------------------------------------------------- */
#define CHIP_PIN_PF8_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 8, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 8, 0) */
#define CHIP_PIN_PF8_TIM16_CH1N             CHIP_PIN_ID(CHIP_PORT_F, 8, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 8, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 8, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 8, 4) */
#define CHIP_PIN_PF8_SPI5_MISO              CHIP_PIN_ID(CHIP_PORT_F, 8, 5)
#define CHIP_PIN_PF8_SAI1_SCK_B             CHIP_PIN_ID(CHIP_PORT_F, 8, 6)
#define CHIP_PIN_PF8_UART7_RTS              CHIP_PIN_ID(CHIP_PORT_F, 8, 7)
#define CHIP_PIN_PF8_UART7_DE               CHIP_PIN_ID(CHIP_PORT_F, 8, 7)
#define CHIP_PIN_PF8_SAI4_SCK_B             CHIP_PIN_ID(CHIP_PORT_F, 8, 8)
#define CHIP_PIN_PF8_TIM13_CH1              CHIP_PIN_ID(CHIP_PORT_F, 8, 9)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF8_OCTOSPIM_P1_IO0        CHIP_PIN_ID(CHIP_PORT_F, 8, 10)
#else
#define CHIP_PIN_PF8_QUADSPI_BK1_IO0        CHIP_PIN_ID(CHIP_PORT_F, 8, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 8, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 8, 12) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF8_TIM23_CH3              CHIP_PIN_ID(CHIP_PORT_F, 8, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 8, 14) */
#define CHIP_PIN_PF8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 8, 15)
#define CHIP_PIN_PF8_ADC3_INN3              CHIP_PIN_ID(CHIP_PORT_F, 8, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PF8_ADC3_INP7              CHIP_PIN_ID(CHIP_PORT_F, 8, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF9  ------------------------------------------------------------- */
#define CHIP_PIN_PF9_GPIO                   CHIP_PIN_ID(CHIP_PORT_F, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 9, 0) */
#define CHIP_PIN_PF9_TIM17_CH1N             CHIP_PIN_ID(CHIP_PORT_F, 9, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 9, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 9, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 9, 4) */
#define CHIP_PIN_PF9_SPI5_MOSI              CHIP_PIN_ID(CHIP_PORT_F, 9, 5)
#define CHIP_PIN_PF9_SAI1_FS_B              CHIP_PIN_ID(CHIP_PORT_F, 9, 6)
#define CHIP_PIN_PF9_UART7_CTS              CHIP_PIN_ID(CHIP_PORT_F, 9, 7)
#define CHIP_PIN_PF9_SAI4_FS_B              CHIP_PIN_ID(CHIP_PORT_F, 9, 8)
#define CHIP_PIN_PF9_TIM14_CH1              CHIP_PIN_ID(CHIP_PORT_F, 9, 9)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF9_OCTOSPIM_P1_IO1        CHIP_PIN_ID(CHIP_PORT_F, 9, 10)
#else
#define CHIP_PIN_PF9_QUADSPI_BK1_IO1        CHIP_PIN_ID(CHIP_PORT_F, 9, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 9, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 9, 12) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF9_TIM23_CH4              CHIP_PIN_ID(CHIP_PORT_F, 9, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 9, 14) */
#define CHIP_PIN_PF9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_F, 9, 15)
#define CHIP_PIN_PF9_ADC3_INP2              CHIP_PIN_ID(CHIP_PORT_F, 9, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF10 ------------------------------------------------------------- */
#define CHIP_PIN_PF10_GPIO                  CHIP_PIN_ID(CHIP_PORT_F, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 0) */
#define CHIP_PIN_PF10_TIM16_BKIN            CHIP_PIN_ID(CHIP_PORT_F, 10, 1)
#define CHIP_PIN_PF10_SAI1_D3               CHIP_PIN_ID(CHIP_PORT_F, 10, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 3) */
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PF10_PSSI_D15              CHIP_PIN_ID(CHIP_PORT_F, 10, 4)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF10_OCTOSPIM_P1_CLK       CHIP_PIN_ID(CHIP_PORT_F, 10, 9)
#else
#define CHIP_PIN_PF10_QUADSPI_CLK           CHIP_PIN_ID(CHIP_PORT_F, 10, 9)
#endif
#define CHIP_PIN_PF10_SAI4_D3               CHIP_PIN_ID(CHIP_PORT_F, 10, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 10, 12) */
#define CHIP_PIN_PF10_DVP_D11               CHIP_PIN_ID(CHIP_PORT_F, 10, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PF10_PSSI_D11              CHIP_PIN_ID(CHIP_PORT_F, 10, 13)
#endif
#define CHIP_PIN_PF10_LCD_DE                CHIP_PIN_ID(CHIP_PORT_F, 10, 14)
#define CHIP_PIN_PF10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_F, 10, 15)
#define CHIP_PIN_PF10_ADC3_INN2             CHIP_PIN_ID(CHIP_PORT_F, 10, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PF10_ADC3_INP6             CHIP_PIN_ID(CHIP_PORT_F, 10, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF11 ------------------------------------------------------------- */
#define CHIP_PIN_PF11_GPIO                  CHIP_PIN_ID(CHIP_PORT_F, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 4) */
#define CHIP_PIN_PF11_SPI5_MOSI             CHIP_PIN_ID(CHIP_PORT_F, 11, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF11_OCTOSPIM_P1_NCLK      CHIP_PIN_ID(CHIP_PORT_F, 11, 9)
#endif
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PF11_SAI2_SD_B             CHIP_PIN_ID(CHIP_PORT_F, 11, 10)
#else
#define CHIP_PIN_PF11_SAI4_SD_B             CHIP_PIN_ID(CHIP_PORT_F, 11, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 11, 11) */
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PF11_EXMC_NRAS             CHIP_PIN_ID(CHIP_PORT_F, 11, 12)
#else
#define CHIP_PIN_PF11_EXMC_SDNRAS           CHIP_PIN_ID(CHIP_PORT_F, 11, 12)
#endif
#define CHIP_PIN_PF11_DVP_D12               CHIP_PIN_ID(CHIP_PORT_F, 11, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PF11_PSSI_D12              CHIP_PIN_ID(CHIP_PORT_F, 11, 13)
#endif
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF11_TIM24_CH1             CHIP_PIN_ID(CHIP_PORT_F, 11, 14)
#endif
#define CHIP_PIN_PF11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_F, 11, 15)
#define CHIP_PIN_PF11_ADC1_INP2             CHIP_PIN_ID(CHIP_PORT_F, 11, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF12 ------------------------------------------------------------- */
#define CHIP_PIN_PF12_GPIO                  CHIP_PIN_ID(CHIP_PORT_F, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PF12_OCTOSPIM_P2_DQS       CHIP_PIN_ID(CHIP_PORT_F, 12, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 11) */
#define CHIP_PIN_PF12_EXMC_A6               CHIP_PIN_ID(CHIP_PORT_F, 12, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 12, 13) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF12_TIM24_CH2             CHIP_PIN_ID(CHIP_PORT_F, 12, 14)
#endif
#define CHIP_PIN_PF12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_F, 12, 15)
#define CHIP_PIN_PF12_ADC1_INN2             CHIP_PIN_ID(CHIP_PORT_F, 12, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PF12_ADC1_INP6             CHIP_PIN_ID(CHIP_PORT_F, 12, CHIP_PIN_FUNC_ANALOG)


/*----- Пин PF13 ------------------------------------------------------------- */
#define CHIP_PIN_PF13_GPIO                  CHIP_PIN_ID(CHIP_PORT_F, 13, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 2) */
#define CHIP_PIN_PF13_DFSDM1_DATIN6         CHIP_PIN_ID(CHIP_PORT_F, 13, 3)
#define CHIP_PIN_PF13_I2C4_SMBA             CHIP_PIN_ID(CHIP_PORT_F, 13, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 11) */
#define CHIP_PIN_PF13_EXMC_A7               CHIP_PIN_ID(CHIP_PORT_F, 13, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 13, 13) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF13_TIM24_CH3             CHIP_PIN_ID(CHIP_PORT_F, 13, 14)
#endif
#define CHIP_PIN_PF13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_F, 13, 15)
#define CHIP_PIN_PF13_ADC2_INP2             CHIP_PIN_ID(CHIP_PORT_F, 13, CHIP_PIN_FUNC_ANALOG)
\

/*----- Пин PF14 ------------------------------------------------------------- */
#define CHIP_PIN_PF14_GPIO                  CHIP_PIN_ID(CHIP_PORT_F, 14, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 2) */
#define CHIP_PIN_PF14_DFSDM1_CKIN6          CHIP_PIN_ID(CHIP_PORT_F, 14, 3)
#define CHIP_PIN_PF14_I2C4_SCL              CHIP_PIN_ID(CHIP_PORT_F, 14, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 11) */
#define CHIP_PIN_PF14_EXMC_A8               CHIP_PIN_ID(CHIP_PORT_F, 14, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 14, 13) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PF14_TIM24_CH4             CHIP_PIN_ID(CHIP_PORT_F, 14, 14)
#endif
#define CHIP_PIN_PF14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_F, 14, 15)
#define CHIP_PIN_PF14_ADC2_INN2             CHIP_PIN_ID(CHIP_PORT_F, 14, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PF14_ADC2_INP6             CHIP_PIN_ID(CHIP_PORT_F, 14, CHIP_PIN_FUNC_ANALOG)

/*----- Пин PF15 ------------------------------------------------------------- */
#define CHIP_PIN_PF15_GPIO                  CHIP_PIN_ID(CHIP_PORT_F, 15, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 3) */
#define CHIP_PIN_PF15_I2C4_SDA              CHIP_PIN_ID(CHIP_PORT_F, 15, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 11) */
#define CHIP_PIN_PF15_EXMC_A9               CHIP_PIN_ID(CHIP_PORT_F, 15, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_F, 15, 14) */
#define CHIP_PIN_PF15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_F, 15, 15)



/*********************************** Порт G ************************************/
/*----- Пин PG0  ------------------------------------------------------------- */
#define CHIP_PIN_PG0_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG0_OCTOSPIM_P2_IO4        CHIP_PIN_ID(CHIP_PORT_G, 0, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PG0_UART9_RX               CHIP_PIN_ID(CHIP_PORT_G, 0, 11)
#endif
#define CHIP_PIN_PG0_EXMC_A10               CHIP_PIN_ID(CHIP_PORT_G, 0, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 0, 14) */
#define CHIP_PIN_PG0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 0, 15)


/*----- Пин PG1  ------------------------------------------------------------- */
#define CHIP_PIN_PG1_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG1_OCTOSPIM_P2_IO5        CHIP_PIN_ID(CHIP_PORT_G, 1, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PG1_UART9_TX               CHIP_PIN_ID(CHIP_PORT_G, 1, 11)
#endif
#define CHIP_PIN_PG1_EXMC_A11               CHIP_PIN_ID(CHIP_PORT_G, 1, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 1, 14) */
#define CHIP_PIN_PG1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 1, 15)


/*----- Пин PG2  ------------------------------------------------------------- */
#define CHIP_PIN_PG2_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 2, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 2) */
#define CHIP_PIN_PG2_TIM8_BKIN              CHIP_PIN_ID(CHIP_PORT_G, 2, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 10) */
#define CHIP_PIN_PG2_TIM8_BKIN_COMP12       CHIP_PIN_ID(CHIP_PORT_G, 2, 11)
#define CHIP_PIN_PG2_EXMC_A12               CHIP_PIN_ID(CHIP_PORT_G, 2, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 2, 13) */
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PG2_TIM24_ETR              CHIP_PIN_ID(CHIP_PORT_G, 2, 14)
#endif
#define CHIP_PIN_PG2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 2, 15)


/*----- Пин PG3  ------------------------------------------------------------- */
#define CHIP_PIN_PG3_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 2) */
#define CHIP_PIN_PG3_TIM8_BKIN2             CHIP_PIN_ID(CHIP_PORT_G, 3, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 10) */
#define CHIP_PIN_PG3_TIM8_BKIN2_COMP12      CHIP_PIN_ID(CHIP_PORT_G, 3, 11)
#define CHIP_PIN_PG3_EXMC_A13               CHIP_PIN_ID(CHIP_PORT_G, 3, 12)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PG3_TIM23_ETR              CHIP_PIN_ID(CHIP_PORT_G, 3, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 3, 14) */
#define CHIP_PIN_PG3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 3, 15)


/*----- Пин PG4  ------------------------------------------------------------- */
#define CHIP_PIN_PG4_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 4, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 0) */
#define CHIP_PIN_PG4_TIM1_BKIN2             CHIP_PIN_ID(CHIP_PORT_G, 4, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 10) */
#define CHIP_PIN_PG4_TIM1_BKIN2_COMP12      CHIP_PIN_ID(CHIP_PORT_G, 4, 11)
#define CHIP_PIN_PG4_EXMC_A14               CHIP_PIN_ID(CHIP_PORT_G, 4, 12)
#define CHIP_PIN_PG4_EXMC_BA0               CHIP_PIN_ID(CHIP_PORT_G, 4, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 4, 14) */
#define CHIP_PIN_PG4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 4, 15)


/*----- Пин PG5  ------------------------------------------------------------- */
#define CHIP_PIN_PG5_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 0) */
#define CHIP_PIN_PG5_TIM1_ETR               CHIP_PIN_ID(CHIP_PORT_G, 5, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 11) */
#define CHIP_PIN_PG5_EXMC_A15               CHIP_PIN_ID(CHIP_PORT_G, 5, 12)
#define CHIP_PIN_PG5_EXMC_BA1               CHIP_PIN_ID(CHIP_PORT_G, 5, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 5, 14) */
#define CHIP_PIN_PG5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 5, 15)


/*----- Пин PG6  ------------------------------------------------------------- */
#define CHIP_PIN_PG6_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 0) */
#define CHIP_PIN_PG6_TIM17_BKIN             CHIP_PIN_ID(CHIP_PORT_G, 6, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PG6_HRTIM_CHE1             CHIP_PIN_ID(CHIP_PORT_G, 6, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 9) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG6_OCTOSPIM_P1_NCS        CHIP_PIN_ID(CHIP_PORT_G, 6, 10)
#else
#define CHIP_PIN_PG6_QUADSPI_BK1_NCS        CHIP_PIN_ID(CHIP_PORT_G, 6, 10)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 6, 11) */
#define CHIP_PIN_PG6_EXMC_NE3               CHIP_PIN_ID(CHIP_PORT_G, 6, 12)
#define CHIP_PIN_PG6_DVP_D12                CHIP_PIN_ID(CHIP_PORT_G, 6, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PG6_PSSI_D12               CHIP_PIN_ID(CHIP_PORT_G, 6, 13)
#endif
#define CHIP_PIN_PG6_LCD_R7                 CHIP_PIN_ID(CHIP_PORT_G, 6, 14)
#define CHIP_PIN_PG6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 6, 15)


/*----- Пин PG7  ------------------------------------------------------------- */
#define CHIP_PIN_PG7_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 1) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PG7_HRTIM_CHE2             CHIP_PIN_ID(CHIP_PORT_G, 7, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 5) */
#define CHIP_PIN_PG7_SAI1_MCLK_A            CHIP_PIN_ID(CHIP_PORT_G, 7, 6)
#define CHIP_PIN_PG7_USART6_CK              CHIP_PIN_ID(CHIP_PORT_G, 7, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG7_OCTOSPIM_P2_DQS        CHIP_PIN_ID(CHIP_PORT_G, 7, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 7, 11) */
#define CHIP_PIN_PG7_EXMC_INT               CHIP_PIN_ID(CHIP_PORT_G, 7, 12)
#define CHIP_PIN_PG7_DVP_D13                CHIP_PIN_ID(CHIP_PORT_G, 7, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PG7_PSSI_D13               CHIP_PIN_ID(CHIP_PORT_G, 7, 13)
#endif
#define CHIP_PIN_PG7_LCD_CLK                CHIP_PIN_ID(CHIP_PORT_G, 7, 14)
#define CHIP_PIN_PG7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 7, 15)


/*----- Пин PG8  ------------------------------------------------------------- */
#define CHIP_PIN_PG8_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 8, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 2) */
#define CHIP_PIN_PG8_TIM8_ETR               CHIP_PIN_ID(CHIP_PORT_G, 8, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 4) */
#define CHIP_PIN_PG8_SPI6_NSS               CHIP_PIN_ID(CHIP_PORT_G, 8, 5)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PG8_I2S6_WS                CHIP_PIN_ID(CHIP_PORT_G, 8, 5)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 6) */
#define CHIP_PIN_PG8_USART6_RTS             CHIP_PIN_ID(CHIP_PORT_G, 8, 7)
#define CHIP_PIN_PG8_USART6_DE              CHIP_PIN_ID(CHIP_PORT_G, 8, 7)
#define CHIP_PIN_PG8_SPDIFRX1_IN3           CHIP_PIN_ID(CHIP_PORT_G, 8, 8)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 10) */
#define CHIP_PIN_PG8_ETH_PPS_OUT            CHIP_PIN_ID(CHIP_PORT_G, 8, 11)
#define CHIP_PIN_PG8_EXMC_SDCLK             CHIP_PIN_ID(CHIP_PORT_G, 8, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 8, 13) */
#define CHIP_PIN_PG8_LCD_G7                 CHIP_PIN_ID(CHIP_PORT_G, 8, 14)
#define CHIP_PIN_PG8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 8, 15)


/*----- Пин PG9  ------------------------------------------------------------- */
#define CHIP_PIN_PG9_GPIO                   CHIP_PIN_ID(CHIP_PORT_G, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 9, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 9, 1) */
#if CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PG9_FDCAN3_TX              CHIP_PIN_ID(CHIP_PORT_G, 9, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 9, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 9, 4) */
#define CHIP_PIN_PG9_SPI1_MISO              CHIP_PIN_ID(CHIP_PORT_G, 9, 5)
#define CHIP_PIN_PG9_I2S1_SDI               CHIP_PIN_ID(CHIP_PORT_G, 9, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 9, 6) */
#define CHIP_PIN_PG9_USART6_RX              CHIP_PIN_ID(CHIP_PORT_G, 9, 7)
#define CHIP_PIN_PG9_SPDIFRX1_IN4           CHIP_PIN_ID(CHIP_PORT_G, 9, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG9_OCTOSPIM_P1_IO6        CHIP_PIN_ID(CHIP_PORT_G, 9, 9)
#else
#define CHIP_PIN_PG9_QUADSPI_BK2_IO2        CHIP_PIN_ID(CHIP_PORT_G, 9, 9)
#endif
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PG9_SAI2_FS_B              CHIP_PIN_ID(CHIP_PORT_G, 9, 10)
#else
#define CHIP_PIN_PG9_SAI4_FS_B              CHIP_PIN_ID(CHIP_PORT_G, 9, 10)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PG9_SDMMC2_D0              CHIP_PIN_ID(CHIP_PORT_G, 9, 11)
#endif
#define CHIP_PIN_PG9_EXMC_NE2               CHIP_PIN_ID(CHIP_PORT_G, 9, 12)
#define CHIP_PIN_PG9_EXMC_NCE               CHIP_PIN_ID(CHIP_PORT_G, 9, 12)
#define CHIP_PIN_PG9_DVP_VSYNC              CHIP_PIN_ID(CHIP_PORT_G, 9, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PG9_PSSI_RDY               CHIP_PIN_ID(CHIP_PORT_G, 9, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 9, 14) */
#define CHIP_PIN_PG9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_G, 9, 15)


/*----- Пин PG10 ------------------------------------------------------------- */
#define CHIP_PIN_PG10_GPIO                  CHIP_PIN_ID(CHIP_PORT_G, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 10, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 10, 1) */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PG10_HRTIM_FLT5            CHIP_PIN_ID(CHIP_PORT_G, 10, 2)
#endif
#if CHIP_DEV_SUPPORT_FDCAN3
#define CHIP_PIN_PG10_FDCAN3_RX             CHIP_PIN_ID(CHIP_PORT_G, 10, 2)
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG10_OCTOSPIM_P2_IO6       CHIP_PIN_ID(CHIP_PORT_G, 10, 3)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 10, 4) */
#define CHIP_PIN_PG10_SPI1_NSS              CHIP_PIN_ID(CHIP_PORT_G, 10, 5)
#define CHIP_PIN_PG10_I2S1_WS               CHIP_PIN_ID(CHIP_PORT_G, 10, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 10, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 10, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 10, 8) */
#define CHIP_PIN_PG10_LCD_G3                CHIP_PIN_ID(CHIP_PORT_G, 10, 9)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_PIN_PG10_SAI2_SD_B             CHIP_PIN_ID(CHIP_PORT_G, 10, 10)
#else
#define CHIP_PIN_PG10_SAI4_SD_B             CHIP_PIN_ID(CHIP_PORT_G, 10, 10)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PG10_SDMMC2_D1             CHIP_PIN_ID(CHIP_PORT_G, 10, 11)
#endif
#define CHIP_PIN_PG10_EXMC_NE3              CHIP_PIN_ID(CHIP_PORT_G, 10, 12)
#define CHIP_PIN_PG10_DVP_D2                CHIP_PIN_ID(CHIP_PORT_G, 10, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PG10_PSSI_D2               CHIP_PIN_ID(CHIP_PORT_G, 10, 13)
#endif
#define CHIP_PIN_PG10_LCD_B2                CHIP_PIN_ID(CHIP_PORT_G, 10, 14)
#define CHIP_PIN_PG10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_G, 10, 15)


/*----- Пин PG11 ------------------------------------------------------------- */
#define CHIP_PIN_PG11_GPIO                  CHIP_PIN_ID(CHIP_PORT_G, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 11, 0) */
#if !CHIP_REV_Y_COMPAT_EN
#define CHIP_PIN_PG11_LPTIM1_IN2            CHIP_PIN_ID(CHIP_PORT_G, 11, 1)
#endif
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PG11_HRTIM_EEV4            CHIP_PIN_ID(CHIP_PORT_G, 11, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 11, 3) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PG11_USART10_RX            CHIP_PIN_ID(CHIP_PORT_G, 11, 4)
#endif
#define CHIP_PIN_PG11_SPI1_SCK              CHIP_PIN_ID(CHIP_PORT_G, 11, 5)
#define CHIP_PIN_PG11_I2S1_CK               CHIP_PIN_ID(CHIP_PORT_G, 11, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 11, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 11, 7) */
#define CHIP_PIN_PG11_SPDIFRX1_IN1          CHIP_PIN_ID(CHIP_PORT_G, 11, 8)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG11_OCTOSPIM_P2_IO7       CHIP_PIN_ID(CHIP_PORT_G, 11, 9)
#endif
#define CHIP_PIN_PG11_SDMMC2_D2             CHIP_PIN_ID(CHIP_PORT_G, 11, 10)
#define CHIP_PIN_PG11_ETH_MII_TX_EN         CHIP_PIN_ID(CHIP_PORT_G, 11, 11)
#define CHIP_PIN_PG11_ETH_RMII_TX_EN        CHIP_PIN_ID(CHIP_PORT_G, 11, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 11, 12) */
#define CHIP_PIN_PG11_DVP_D3                CHIP_PIN_ID(CHIP_PORT_G, 11, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PG11_PSSI_D3               CHIP_PIN_ID(CHIP_PORT_G, 11, 13)
#endif
#define CHIP_PIN_PG11_LCD_B3                CHIP_PIN_ID(CHIP_PORT_G, 11, 14)
#define CHIP_PIN_PG11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_G, 11, 15)


/*----- Пин PG12 ------------------------------------------------------------- */
#define CHIP_PIN_PG12_GPIO                  CHIP_PIN_ID(CHIP_PORT_G, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 12, 0) */
#define CHIP_PIN_PG12_LPTIM1_IN1            CHIP_PIN_ID(CHIP_PORT_G, 12, 1)
#define CHIP_PIN_PG12_HRTIM_EEV5            CHIP_PIN_ID(CHIP_PORT_G, 12, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 12, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 12, 4) */
#define CHIP_PIN_PG12_SPI6_MISO             CHIP_PIN_ID(CHIP_PORT_G, 12, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 12, 6) */
#define CHIP_PIN_PG12_USART6_RTS            CHIP_PIN_ID(CHIP_PORT_G, 12, 7)
#define CHIP_PIN_PG12_USART6_DE             CHIP_PIN_ID(CHIP_PORT_G, 12, 7)
#define CHIP_PIN_PG12_SPDIFRX1_IN2          CHIP_PIN_ID(CHIP_PORT_G, 12, 8)
#define CHIP_PIN_PG12_LCD_B4                CHIP_PIN_ID(CHIP_PORT_G, 12, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 12, 10) */
#define CHIP_PIN_PG12_ETH_MII_TXD1          CHIP_PIN_ID(CHIP_PORT_G, 12, 11)
#define CHIP_PIN_PG12_ETH_RMII_TXD1         CHIP_PIN_ID(CHIP_PORT_G, 12, 11)
#define CHIP_PIN_PG12_EXMC_NE4              CHIP_PIN_ID(CHIP_PORT_G, 12, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 12, 13) */
#define CHIP_PIN_PG12_LCD_B1                CHIP_PIN_ID(CHIP_PORT_G, 12, 14)
#define CHIP_PIN_PG12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_G, 12, 15)


/*----- Пин PG13 ------------------------------------------------------------- */
#define CHIP_PIN_PG13_GPIO                  CHIP_PIN_ID(CHIP_PORT_G, 13, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PG13_TRACED0               CHIP_PIN_ID(CHIP_PORT_G, 13, 0)
#define CHIP_PIN_PG13_LPTIM1_OUT            CHIP_PIN_ID(CHIP_PORT_G, 13, 1)
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_PIN_PG13_HRTIM_EEV10           CHIP_PIN_ID(CHIP_PORT_G, 13, 2)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 13, 3) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PG13_USART10_CTS           CHIP_PIN_ID(CHIP_PORT_G, 13, 4)
#define CHIP_PIN_PG13_USART10_NSS           CHIP_PIN_ID(CHIP_PORT_G, 13, 4)
#endif
#define CHIP_PIN_PG13_SPI6_SCK              CHIP_PIN_ID(CHIP_PORT_G, 13, 5)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PG13_I2S6_CK               CHIP_PIN_ID(CHIP_PORT_G, 13, 5)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 13, 6) */
#define CHIP_PIN_PG13_USART6_CTS            CHIP_PIN_ID(CHIP_PORT_G, 13, 7)
#define CHIP_PIN_PG13_USART6_NSS            CHIP_PIN_ID(CHIP_PORT_G, 13, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 13, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 13, 9) */
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PG13_SDMMC2_D6             CHIP_PIN_ID(CHIP_PORT_G, 13, 10)
#endif
#define CHIP_PIN_PG13_ETH_MII_TXD0          CHIP_PIN_ID(CHIP_PORT_G, 13, 11)
#define CHIP_PIN_PG13_ETH_RMII_TXD0         CHIP_PIN_ID(CHIP_PORT_G, 13, 11)
#define CHIP_PIN_PG13_EXMC_A24              CHIP_PIN_ID(CHIP_PORT_G, 13, 12)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PG13_TIM23_CH2             CHIP_PIN_ID(CHIP_PORT_G, 13, 13)
#endif
#define CHIP_PIN_PG13_LCD_R0                CHIP_PIN_ID(CHIP_PORT_G, 13, 14)
#define CHIP_PIN_PG13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_G, 13, 15)


/*----- Пин PG14 ------------------------------------------------------------- */
#define CHIP_PIN_PG14_GPIO                  CHIP_PIN_ID(CHIP_PORT_G, 14, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PG14_TRACED1               CHIP_PIN_ID(CHIP_PORT_G, 14, 0)
#define CHIP_PIN_PG14_LPTIM1_ETR            CHIP_PIN_ID(CHIP_PORT_G, 14, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 14, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 14, 3) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PG14_USART10_RTS           CHIP_PIN_ID(CHIP_PORT_G, 14, 4)
#define CHIP_PIN_PG14_USART10_DE            CHIP_PIN_ID(CHIP_PORT_G, 14, 4)
#endif
#define CHIP_PIN_PG14_SPI6_MOSI             CHIP_PIN_ID(CHIP_PORT_G, 14, 5)
#if CHIP_DEV_SUPPORT_I2S6
#define CHIP_PIN_PG14_I2S6_SDO              CHIP_PIN_ID(CHIP_PORT_G, 14, 5)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 14, 6) */
#define CHIP_PIN_PG14_USART6_TX             CHIP_PIN_ID(CHIP_PORT_G, 14, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 14, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG14_OCTOSPIM_P1_IO7       CHIP_PIN_ID(CHIP_PORT_G, 14, 9)
#else
#define CHIP_PIN_PG14_QUADSPI_BK2_IO3       CHIP_PIN_ID(CHIP_PORT_G, 14, 9)
#endif
#if defined CHIP_DEV_STM32H723
#define CHIP_PIN_PG14_SDMMC2_D7             CHIP_PIN_ID(CHIP_PORT_G, 14, 10)
#endif
#define CHIP_PIN_PG14_ETH_MII_TXD1          CHIP_PIN_ID(CHIP_PORT_G, 14, 11)
#define CHIP_PIN_PG14_ETH_RMII_TXD1         CHIP_PIN_ID(CHIP_PORT_G, 14, 11)
#define CHIP_PIN_PG14_EXMC_A25              CHIP_PIN_ID(CHIP_PORT_G, 14, 12)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_PIN_PG14_TIM23_CH3             CHIP_PIN_ID(CHIP_PORT_G, 14, 13)
#endif
#define CHIP_PIN_PG14_LCD_B0                CHIP_PIN_ID(CHIP_PORT_G, 14, 14)
#define CHIP_PIN_PG14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_G, 14, 15)


/*----- Пин PG15 ------------------------------------------------------------- */
#define CHIP_PIN_PG15_GPIO                  CHIP_PIN_ID(CHIP_PORT_G, 15, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 6) */
#define CHIP_PIN_PG15_USART6_CTS            CHIP_PIN_ID(CHIP_PORT_G, 15, 7)
#define CHIP_PIN_PG15_USART6_NSS            CHIP_PIN_ID(CHIP_PORT_G, 15, 7)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 8) */
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_PIN_PG15_OCTOSPIM_P2_DQS       CHIP_PIN_ID(CHIP_PORT_G, 15, 9)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 10) */
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_PIN_PG15_USART10_CK            CHIP_PIN_ID(CHIP_PORT_G, 15, 11)
#endif
#define CHIP_PIN_PG15_EXMC_SDNCAS           CHIP_PIN_ID(CHIP_PORT_G, 15, 12)
#define CHIP_PIN_PG15_DVP_D13               CHIP_PIN_ID(CHIP_PORT_G, 15, 13)
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_PIN_PG15_PSSI_D13              CHIP_PIN_ID(CHIP_PORT_G, 15, 13)
#endif
/* -----                                    CHIP_PIN_ID(CHIP_PORT_G, 15, 14) */
#define CHIP_PIN_PG15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_G, 15, 15)



/*********************************** Порт H ************************************/
/*----- Пин PH0  ------------------------------------------------------------- */
#define CHIP_PIN_PH0_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 0, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PH0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 0, 15)

/*----- Пин PH1  ------------------------------------------------------------- */
#define CHIP_PIN_PH1_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 1, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PH1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 1, 15)

/*----- Пин PH2  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH2_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 2, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 0) */
#define CHIP_PIN_PH2_LPTIM1_IN2             CHIP_PIN_ID(CHIP_PORT_H, 2, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 8) */
#define CHIP_PIN_PH2_QUADSPI_BK2_IO0        CHIP_PIN_ID(CHIP_PORT_H, 2, 9)
#define CHIP_PIN_PH2_SAI2_SCK_B             CHIP_PIN_ID(CHIP_PORT_H, 2, 10)
#define CHIP_PIN_PH2_ETH_MII_CRS            CHIP_PIN_ID(CHIP_PORT_H, 2, 11)
#define CHIP_PIN_PH2_EXMC_SDCKE0            CHIP_PIN_ID(CHIP_PORT_H, 2, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 2, 13) */
#define CHIP_PIN_PH2_LCD_R0                 CHIP_PIN_ID(CHIP_PORT_H, 2, 14)
#define CHIP_PIN_PH2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 2, 15)
#define CHIP_PIN_PH2_ADC3_INP13             CHIP_PIN_ID(CHIP_PORT_H, 2, CHIP_PIN_FUNC_ANALOG)
#endif

/*----- Пин PH3  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH3_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 8) */
#define CHIP_PIN_PH3_QUADSPI_BK2_IO1        CHIP_PIN_ID(CHIP_PORT_H, 3, 9)
#define CHIP_PIN_PH3_SAI2_MCLK_B            CHIP_PIN_ID(CHIP_PORT_H, 3, 10)
#define CHIP_PIN_PH3_ETH_MII_COL            CHIP_PIN_ID(CHIP_PORT_H, 3, 11)
#define CHIP_PIN_PH3_EXMC_SDNE0             CHIP_PIN_ID(CHIP_PORT_H, 3, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 3, 13) */
#define CHIP_PIN_PH3_LCD_R1                 CHIP_PIN_ID(CHIP_PORT_H, 3, 14)
#define CHIP_PIN_PH3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 3, 15)
#define CHIP_PIN_PH3_ADC3_INN13             CHIP_PIN_ID(CHIP_PORT_H, 3, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PH3_ADC3_INP14             CHIP_PIN_ID(CHIP_PORT_H, 3, CHIP_PIN_FUNC_ANALOG)
#endif

/*----- Пин PH4  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH4_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 4, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 3) */
#define CHIP_PIN_PH4_I2C2_SCL               CHIP_PIN_ID(CHIP_PORT_H, 4, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 8) */
#define CHIP_PIN_PH4_LCD_G5                 CHIP_PIN_ID(CHIP_PORT_H, 4, 9)
#define CHIP_PIN_PH4_OTG_HS_ULPI_NXT        CHIP_PIN_ID(CHIP_PORT_H, 4, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 4, 13) */
#define CHIP_PIN_PH4_LCD_G4                 CHIP_PIN_ID(CHIP_PORT_H, 4, 14)
#define CHIP_PIN_PH4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 4, 15)
#define CHIP_PIN_PH4_ADC3_INN14             CHIP_PIN_ID(CHIP_PORT_H, 4, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PH4_ADC3_INP15             CHIP_PIN_ID(CHIP_PORT_H, 4, CHIP_PIN_FUNC_ANALOG)
#endif

/*----- Пин PH5  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH5_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 3) */
#define CHIP_PIN_PH5_I2C2_SDA               CHIP_PIN_ID(CHIP_PORT_H, 5, 4)
#define CHIP_PIN_PH5_SPI5_NSS               CHIP_PIN_ID(CHIP_PORT_H, 5, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 11) */
#define CHIP_PIN_PH5_EXMC_SDNWE             CHIP_PIN_ID(CHIP_PORT_H, 5, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 5, 14) */
#define CHIP_PIN_PH5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 5, 15)
#define CHIP_PIN_PH5_ADC3_INN15             CHIP_PIN_ID(CHIP_PORT_H, 5, CHIP_PIN_FUNC_ANALOG)
#define CHIP_PIN_PH5_ADC3_INP16             CHIP_PIN_ID(CHIP_PORT_H, 5, CHIP_PIN_FUNC_ANALOG)
#endif

/*----- Пин PH6  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH6_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 1) */
#define CHIP_PIN_PH6_TIM12_CH1              CHIP_PIN_ID(CHIP_PORT_H, 6, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 3) */
#define CHIP_PIN_PH6_I2C2_SMBA              CHIP_PIN_ID(CHIP_PORT_H, 6, 4)
#define CHIP_PIN_PH6_SPI5_SCK               CHIP_PIN_ID(CHIP_PORT_H, 6, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 10) */
#define CHIP_PIN_PH6_ETH_MII_RXD2           CHIP_PIN_ID(CHIP_PORT_H, 6, 11)
#define CHIP_PIN_PH6_EXMC_SDNE1             CHIP_PIN_ID(CHIP_PORT_H, 6, 12)
#define CHIP_PIN_PH6_DVP_D8                 CHIP_PIN_ID(CHIP_PORT_H, 6, 13)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 6, 14) */
#define CHIP_PIN_PH6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 6, 15)
#endif

/*----- Пин PH7  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH7_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 3) */
#define CHIP_PIN_PH7_I2C3_SCL               CHIP_PIN_ID(CHIP_PORT_H, 7, 4)
#define CHIP_PIN_PH7_SPI5_MISO              CHIP_PIN_ID(CHIP_PORT_H, 7, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 10) */
#define CHIP_PIN_PH7_ETH_MII_RXD3           CHIP_PIN_ID(CHIP_PORT_H, 7, 11)
#define CHIP_PIN_PH7_EXMC_SDCKE1            CHIP_PIN_ID(CHIP_PORT_H, 7, 12)
#define CHIP_PIN_PH7_DVP_D9                 CHIP_PIN_ID(CHIP_PORT_H, 7, 13)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 7, 14) */
#define CHIP_PIN_PH7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 7, 15)
#endif


/*----- Пин PH8  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH8_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 8, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 1) */
#define CHIP_PIN_PH8_TIM5_ETR               CHIP_PIN_ID(CHIP_PORT_H, 8, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 3) */
#define CHIP_PIN_PH8_I2C3_SDA               CHIP_PIN_ID(CHIP_PORT_H, 8, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 8, 11) */
#define CHIP_PIN_PH8_EXMC_D16               CHIP_PIN_ID(CHIP_PORT_H, 8, 12)
#define CHIP_PIN_PH8_DVP_HSYNC              CHIP_PIN_ID(CHIP_PORT_H, 8, 13)
#define CHIP_PIN_PH8_LCD_R2                 CHIP_PIN_ID(CHIP_PORT_H, 8, 14)
#define CHIP_PIN_PH8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 8, 15)
#endif


/*----- Пин PH9  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH9_GPIO                   CHIP_PIN_ID(CHIP_PORT_H, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 1) */
#define CHIP_PIN_PH9_TIM12_CH2              CHIP_PIN_ID(CHIP_PORT_H, 9, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 3) */
#define CHIP_PIN_PH9_I2C3_SMBA              CHIP_PIN_ID(CHIP_PORT_H, 9, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 9, 11) */
#define CHIP_PIN_PH9_EXMC_D17               CHIP_PIN_ID(CHIP_PORT_H, 9, 12)
#define CHIP_PIN_PH9_DVP_D0                 CHIP_PIN_ID(CHIP_PORT_H, 9, 13)
#define CHIP_PIN_PH9_LCD_R3                 CHIP_PIN_ID(CHIP_PORT_H, 9, 14)
#define CHIP_PIN_PH9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_H, 9, 15)
#endif


/*----- Пин PH10 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH10_GPIO                  CHIP_PIN_ID(CHIP_PORT_H, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 1) */
#define CHIP_PIN_PH10_TIM5_CH1              CHIP_PIN_ID(CHIP_PORT_H, 10, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 3) */
#define CHIP_PIN_PH10_I2C4_SMBA             CHIP_PIN_ID(CHIP_PORT_H, 10, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 10, 11) */
#define CHIP_PIN_PH10_EXMC_D18              CHIP_PIN_ID(CHIP_PORT_H, 10, 12)
#define CHIP_PIN_PH10_DVP_D1                CHIP_PIN_ID(CHIP_PORT_H, 10, 13)
#define CHIP_PIN_PH10_LCD_R4                CHIP_PIN_ID(CHIP_PORT_H, 10, 14)
#define CHIP_PIN_PH10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_H, 10, 15)
#endif


/*----- Пин PH11 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH11_GPIO                  CHIP_PIN_ID(CHIP_PORT_H, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 1) */
#define CHIP_PIN_PH11_TIM5_CH2              CHIP_PIN_ID(CHIP_PORT_H, 11, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 3) */
#define CHIP_PIN_PH11_I2C4_SCL              CHIP_PIN_ID(CHIP_PORT_H, 11, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 11, 11) */
#define CHIP_PIN_PH11_EXMC_D19              CHIP_PIN_ID(CHIP_PORT_H, 11, 12)
#define CHIP_PIN_PH11_DVP_D2                CHIP_PIN_ID(CHIP_PORT_H, 11, 13)
#define CHIP_PIN_PH11_LCD_R5                CHIP_PIN_ID(CHIP_PORT_H, 11, 14)
#define CHIP_PIN_PH11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_H, 11, 15)
#endif


/*----- Пин PH12 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH12_GPIO                  CHIP_PIN_ID(CHIP_PORT_H, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 1) */
#define CHIP_PIN_PH12_TIM5_CH3              CHIP_PIN_ID(CHIP_PORT_H, 12, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 3) */
#define CHIP_PIN_PH12_I2C4_SDA              CHIP_PIN_ID(CHIP_PORT_H, 12, 4)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 12, 11) */
#define CHIP_PIN_PH12_EXMC_D20              CHIP_PIN_ID(CHIP_PORT_H, 12, 12)
#define CHIP_PIN_PH12_DVP_D3                CHIP_PIN_ID(CHIP_PORT_H, 12, 13)
#define CHIP_PIN_PH12_LCD_R6                CHIP_PIN_ID(CHIP_PORT_H, 12, 14)
#define CHIP_PIN_PH12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_H, 12, 15)
#endif

/*----- Пин PH13 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH13_GPIO                  CHIP_PIN_ID(CHIP_PORT_H, 13, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 2) */
#define CHIP_PIN_PH13_TIM8_CH1N             CHIP_PIN_ID(CHIP_PORT_H, 13, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 7) */
#define CHIP_PIN_PH13_UART4_TX              CHIP_PIN_ID(CHIP_PORT_H, 13, 8)
#define CHIP_PIN_PH13_FDCAN1_TX             CHIP_PIN_ID(CHIP_PORT_H, 13, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 11) */
#define CHIP_PIN_PH13_EXMC_D21              CHIP_PIN_ID(CHIP_PORT_H, 13, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 13, 13) */
#define CHIP_PIN_PH13_LCD_G2                CHIP_PIN_ID(CHIP_PORT_H, 13, 14)
#define CHIP_PIN_PH13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_H, 13, 15)
#endif


/*----- Пин PH14 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH14_GPIO                  CHIP_PIN_ID(CHIP_PORT_H, 14, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 2) */
#define CHIP_PIN_PH14_TIM8_CH2N             CHIP_PIN_ID(CHIP_PORT_H, 14, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 7) */
#define CHIP_PIN_PH14_UART4_RX              CHIP_PIN_ID(CHIP_PORT_H, 14, 8)
#define CHIP_PIN_PH14_FDCAN1_RX             CHIP_PIN_ID(CHIP_PORT_H, 14, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 14, 11) */
#define CHIP_PIN_PH14_EXMC_D22              CHIP_PIN_ID(CHIP_PORT_H, 14, 12)
#define CHIP_PIN_PH14_DVP_D4                CHIP_PIN_ID(CHIP_PORT_H, 14, 13)
#define CHIP_PIN_PH14_LCD_G3                CHIP_PIN_ID(CHIP_PORT_H, 14, 14)
#define CHIP_PIN_PH14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_H, 14, 15)
#endif


/*----- Пин PH15 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PH15_GPIO                  CHIP_PIN_ID(CHIP_PORT_H, 15, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 2) */
#define CHIP_PIN_PH15_TIM8_CH3N             CHIP_PIN_ID(CHIP_PORT_H, 15, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 8) */
#define CHIP_PIN_PH15_FDCAN1_TXFD_MODE      CHIP_PIN_ID(CHIP_PORT_H, 15, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_H, 15, 11) */
#define CHIP_PIN_PH15_EXMC_D23              CHIP_PIN_ID(CHIP_PORT_H, 15, 12)
#define CHIP_PIN_PH15_DVP_D11               CHIP_PIN_ID(CHIP_PORT_H, 15, 13)
#define CHIP_PIN_PH15_LCD_G4                CHIP_PIN_ID(CHIP_PORT_H, 15, 14)
#define CHIP_PIN_PH15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_H, 15, 15)
#endif


/*********************************** Порт I ************************************/
/*----- Пин PI0  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI0_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 1) */
#define CHIP_PIN_PI0_TIM5_CH4               CHIP_PIN_ID(CHIP_PORT_I, 0, 2)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 4) */
#define CHIP_PIN_PI0_SPI2_NSS               CHIP_PIN_ID(CHIP_PORT_I, 0, 5)
#define CHIP_PIN_PI0_I2S2_WS                CHIP_PIN_ID(CHIP_PORT_I, 0, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 8) */
#define CHIP_PIN_PI0_FDCAN1_RXFD_MODE       CHIP_PIN_ID(CHIP_PORT_I, 0, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 0, 11) */
#define CHIP_PIN_PI0_EXMC_D24               CHIP_PIN_ID(CHIP_PORT_I, 0, 12)
#define CHIP_PIN_PI0_DVP_D13                CHIP_PIN_ID(CHIP_PORT_I, 0, 13)
#define CHIP_PIN_PI0_LCD_G5                 CHIP_PIN_ID(CHIP_PORT_I, 0, 14)
#define CHIP_PIN_PI0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 0, 15)
#endif


/*----- Пин PI1  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI1_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 2) */
#define CHIP_PIN_PI1_TIM8_BKIN2             CHIP_PIN_ID(CHIP_PORT_I, 1, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 4) */
#define CHIP_PIN_PI1_SPI2_SCK               CHIP_PIN_ID(CHIP_PORT_I, 1, 5)
#define CHIP_PIN_PI1_I2S2_CK                CHIP_PIN_ID(CHIP_PORT_I, 1, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 1, 10) */
#define CHIP_PIN_PI1_TIM8_BKIN2_COMP12      CHIP_PIN_ID(CHIP_PORT_I, 1, 11)
#define CHIP_PIN_PI1_EXMC_D25               CHIP_PIN_ID(CHIP_PORT_I, 1, 12)
#define CHIP_PIN_PI1_DVP_D8                 CHIP_PIN_ID(CHIP_PORT_I, 1, 13)
#define CHIP_PIN_PI1_LCD_G6                 CHIP_PIN_ID(CHIP_PORT_I, 1, 14)
#define CHIP_PIN_PI1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 1, 15)
#endif

/*----- Пин PI2  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI2_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 2, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 2) */
#define CHIP_PIN_PI2_TIM8_CH4               CHIP_PIN_ID(CHIP_PORT_I, 2, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 4) */
#define CHIP_PIN_PI2_SPI2_MISO              CHIP_PIN_ID(CHIP_PORT_I, 2, 5)
#define CHIP_PIN_PI2_I2S2_SDI               CHIP_PIN_ID(CHIP_PORT_I, 2, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 2, 11) */
#define CHIP_PIN_PI2_EXMC_D26               CHIP_PIN_ID(CHIP_PORT_I, 2, 12)
#define CHIP_PIN_PI2_DVP_D9                 CHIP_PIN_ID(CHIP_PORT_I, 2, 13)
#define CHIP_PIN_PI2_LCD_G7                 CHIP_PIN_ID(CHIP_PORT_I, 2, 14)
#define CHIP_PIN_PI2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 2, 15)
#endif

/*----- Пин PI3  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI3_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 2) */
#define CHIP_PIN_PI3_TIM8_ETR               CHIP_PIN_ID(CHIP_PORT_I, 3, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 4) */
#define CHIP_PIN_PI3_SPI2_MOSI              CHIP_PIN_ID(CHIP_PORT_I, 3, 5)
#define CHIP_PIN_PI3_I2S2_SDO               CHIP_PIN_ID(CHIP_PORT_I, 3, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 11) */
#define CHIP_PIN_PI3_EXMC_D27               CHIP_PIN_ID(CHIP_PORT_I, 3, 12)
#define CHIP_PIN_PI3_DVP_D10                CHIP_PIN_ID(CHIP_PORT_I, 3, 13)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 3, 14) */
#define CHIP_PIN_PI3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 3, 15)
#endif


/*----- Пин PI4  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI4_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 4, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 2) */
#define CHIP_PIN_PI4_TIM8_BKIN              CHIP_PIN_ID(CHIP_PORT_I, 4, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 4, 9) */
#define CHIP_PIN_PI4_SAI2_MCLK_A            CHIP_PIN_ID(CHIP_PORT_I, 4, 10)
#define CHIP_PIN_PI4_TIM8_BKIN_COMP12       CHIP_PIN_ID(CHIP_PORT_I, 4, 11)
#define CHIP_PIN_PI4_EXMC_NBL2              CHIP_PIN_ID(CHIP_PORT_I, 4, 12)
#define CHIP_PIN_PI4_DVP_D5                 CHIP_PIN_ID(CHIP_PORT_I, 4, 13)
#define CHIP_PIN_PI4_LCD_B4                 CHIP_PIN_ID(CHIP_PORT_I, 4, 14)
#define CHIP_PIN_PI4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 4, 15)
#endif


/*----- Пин PI5  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI5_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 2) */
#define CHIP_PIN_PI5_TIM8_CH1               CHIP_PIN_ID(CHIP_PORT_I, 5, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 9) */
#define CHIP_PIN_PI5_SAI2_SCK_A             CHIP_PIN_ID(CHIP_PORT_I, 5, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 5, 11) */
#define CHIP_PIN_PI5_EXMC_NBL3              CHIP_PIN_ID(CHIP_PORT_I, 5, 12)
#define CHIP_PIN_PI5_DVP_VSYNC              CHIP_PIN_ID(CHIP_PORT_I, 5, 13)
#define CHIP_PIN_PI5_LCD_B5                 CHIP_PIN_ID(CHIP_PORT_I, 5, 14)
#define CHIP_PIN_PI5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 5, 15)
#endif


/*----- Пин PI6  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI6_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 2) */
#define CHIP_PIN_PI6_TIM8_CH2               CHIP_PIN_ID(CHIP_PORT_I, 6, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 9) */
#define CHIP_PIN_PI6_SAI2_SD_A              CHIP_PIN_ID(CHIP_PORT_I, 6, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 6, 11) */
#define CHIP_PIN_PI6_EXMC_D28               CHIP_PIN_ID(CHIP_PORT_I, 6, 12)
#define CHIP_PIN_PI6_DVP_D6                 CHIP_PIN_ID(CHIP_PORT_I, 6, 13)
#define CHIP_PIN_PI6_LCD_B6                 CHIP_PIN_ID(CHIP_PORT_I, 6, 14)
#define CHIP_PIN_PI6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 6, 15)
#endif


/*----- Пин PI7  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI7_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 2) */
#define CHIP_PIN_PI7_TIM8_CH3               CHIP_PIN_ID(CHIP_PORT_I, 7, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 9) */
#define CHIP_PIN_PI7_SAI2_FS_A              CHIP_PIN_ID(CHIP_PORT_I, 7, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 7, 11) */
#define CHIP_PIN_PI7_EXMC_D29               CHIP_PIN_ID(CHIP_PORT_I, 7, 12)
#define CHIP_PIN_PI7_DVP_D7                 CHIP_PIN_ID(CHIP_PORT_I, 7, 13)
#define CHIP_PIN_PI7_LCD_B7                 CHIP_PIN_ID(CHIP_PORT_I, 7, 14)
#define CHIP_PIN_PI7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 7, 15)
#endif


/*----- Пин PI8  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI8_GPIO                  CHIP_PIN_ID(CHIP_PORT_I, 8, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PI8_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_I, 8, 15)
#endif


/*----- Пин PI9  ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI9_GPIO                   CHIP_PIN_ID(CHIP_PORT_I, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 7) */
#define CHIP_PIN_PI9_UART4_RX               CHIP_PIN_ID(CHIP_PORT_I, 9, 8)
#define CHIP_PIN_PI9_FDCAN1_RX              CHIP_PIN_ID(CHIP_PORT_I, 9, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 11) */
#define CHIP_PIN_PI9_EXMC_D30               CHIP_PIN_ID(CHIP_PORT_I, 9, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 9, 13) */
#define CHIP_PIN_PI9_LCD_VSYNC              CHIP_PIN_ID(CHIP_PORT_I, 9, 14)
#define CHIP_PIN_PI9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_I, 9, 15)
#endif


/*----- Пин PI10 ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI10_GPIO                  CHIP_PIN_ID(CHIP_PORT_I, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 8) */
#define CHIP_PIN_PI10_FDCAN1_RXFD_MODE      CHIP_PIN_ID(CHIP_PORT_I, 10, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 10) */
#define CHIP_PIN_PI10_ETH_MII_RX_ER         CHIP_PIN_ID(CHIP_PORT_I, 10, 11)
#define CHIP_PIN_PI10_EXMC_D31              CHIP_PIN_ID(CHIP_PORT_I, 10, 12)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 10, 13) */
#define CHIP_PIN_PI10_LCD_HSYNC             CHIP_PIN_ID(CHIP_PORT_I, 10, 14)
#define CHIP_PIN_PI10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_I, 10, 15)
#endif


/*----- Пин PI11 ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI11_GPIO                  CHIP_PIN_ID(CHIP_PORT_I, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 8) */
#define CHIP_PIN_PI11_LCD_G6                CHIP_PIN_ID(CHIP_PORT_I, 11, 9)
#define CHIP_PIN_PI11_OTG_HS_ULPI_DIR       CHIP_PIN_ID(CHIP_PORT_I, 11, 10)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 13) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 11, 14) */
#define CHIP_PIN_PI11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_I, 11, 15)
#endif


/*----- Пин PI12 ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI12_GPIO                  CHIP_PIN_ID(CHIP_PORT_I, 12, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 12, 13) */
#define CHIP_PIN_PI12_LCD_HSYNC             CHIP_PIN_ID(CHIP_PORT_I, 12, 14)
#define CHIP_PIN_PI12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_I, 12, 15)
#endif


/*----- Пин PI13 ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI13_GPIO                  CHIP_PIN_ID(CHIP_PORT_I, 13, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 13, 13) */
#define CHIP_PIN_PI13_LCD_VSYC              CHIP_PIN_ID(CHIP_PORT_I, 13, 14)
#define CHIP_PIN_PI13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_I, 13, 15)
#endif


/*----- Пин PI14 ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI14_GPIO                  CHIP_PIN_ID(CHIP_PORT_I, 14, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 14, 13) */
#define CHIP_PIN_PI14_LCD_CLK               CHIP_PIN_ID(CHIP_PORT_I, 14, 14)
#define CHIP_PIN_PI14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_I, 14, 15)
#endif


/*----- Пин PI15 ------------------------------------------------------------- */
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_PIN_PI15_GPIO                  CHIP_PIN_ID(CHIP_PORT_I, 15, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 8) */
#define CHIP_PIN_PI15_LCD_G2                CHIP_PIN_ID(CHIP_PORT_I, 15, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_I, 15, 13) */
#define CHIP_PIN_PI15_LCD_R0                CHIP_PIN_ID(CHIP_PORT_I, 15, 14)
#define CHIP_PIN_PI15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_I, 15, 15)
#endif



/*********************************** Порт J ************************************/
/*----- Пин PJ0  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ0_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 8) */
#define CHIP_PIN_PJ0_LCD_R7                 CHIP_PIN_ID(CHIP_PORT_J, 0, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 0, 13) */
#define CHIP_PIN_PJ0_LCD_R1                 CHIP_PIN_ID(CHIP_PORT_J, 0, 14)
#define CHIP_PIN_PJ0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 0, 15)
#endif


/*----- Пин PJ1  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ1_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 1, 13) */
#define CHIP_PIN_PJ1_LCD_R2                 CHIP_PIN_ID(CHIP_PORT_J, 1, 14)
#define CHIP_PIN_PJ1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 1, 15)
#endif


/*----- Пин PJ2  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ2_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 2, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 2, 13) */
#define CHIP_PIN_PJ2_LCD_R3                 CHIP_PIN_ID(CHIP_PORT_J, 2, 14)
#define CHIP_PIN_PJ2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 2, 15)
#endif


/*----- Пин PJ3  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ3_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 3, 13) */
#define CHIP_PIN_PJ3_LCD_R4                 CHIP_PIN_ID(CHIP_PORT_J, 3, 14)
#define CHIP_PIN_PJ3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 3, 15)
#endif


/*----- Пин PJ4  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ4_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 4, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 4, 13) */
#define CHIP_PIN_PJ4_LCD_R5                 CHIP_PIN_ID(CHIP_PORT_J, 4, 14)
#define CHIP_PIN_PJ4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 4, 15)
#endif


/*----- Пин PJ5  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ5_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 5, 13) */
#define CHIP_PIN_PJ5_LCD_R6                 CHIP_PIN_ID(CHIP_PORT_J, 5, 14)
#define CHIP_PIN_PJ5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 5, 15)
#endif
\

/*----- Пин PJ6  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ6_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 2) */
#define CHIP_PIN_PJ6_TIM8_CH2               CHIP_PIN_ID(CHIP_PORT_J, 6, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 6, 13) */
#define CHIP_PIN_PJ6_LCD_R7                 CHIP_PIN_ID(CHIP_PORT_J, 6, 14)
#define CHIP_PIN_PJ6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 6, 15)
#endif


/*----- Пин PJ7  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ7_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 7, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PJ7_TRGIN                  CHIP_PIN_ID(CHIP_PORT_J, 7, 0)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 2) */
#define CHIP_PIN_PJ7_TIM8_CH2N              CHIP_PIN_ID(CHIP_PORT_J, 7, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 7, 13) */
#define CHIP_PIN_PJ7_LCD_G0                 CHIP_PIN_ID(CHIP_PORT_J, 7, 14)
#define CHIP_PIN_PJ7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 7, 15)
#endif


/*----- Пин PJ8  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ8_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 8, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 0) */
#define CHIP_PIN_PJ8_TIM1_CH3N              CHIP_PIN_ID(CHIP_PORT_J, 8, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 2) */
#define CHIP_PIN_PJ8_TIM8_CH1               CHIP_PIN_ID(CHIP_PORT_J, 8, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 7) */
#define CHIP_PIN_PJ8_UART8_TX               CHIP_PIN_ID(CHIP_PORT_J, 8, 8)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 8, 13) */
#define CHIP_PIN_PJ8_LCD_G1                 CHIP_PIN_ID(CHIP_PORT_J, 8, 14)
#define CHIP_PIN_PJ8_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 8, 15)
#endif


/*----- Пин PJ9  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ9_GPIO                   CHIP_PIN_ID(CHIP_PORT_J, 9, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 0) */
#define CHIP_PIN_PJ9_TIM1_CH3               CHIP_PIN_ID(CHIP_PORT_J, 9, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 2) */
#define CHIP_PIN_PJ9_TIM8_CH1N              CHIP_PIN_ID(CHIP_PORT_J, 9, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 7) */
#define CHIP_PIN_PJ9_UART8_RX               CHIP_PIN_ID(CHIP_PORT_J, 9, 8)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 9, 13) */
#define CHIP_PIN_PJ9_LCD_G2                 CHIP_PIN_ID(CHIP_PORT_J, 9, 14)
#define CHIP_PIN_PJ9_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_J, 9, 15)
#endif


/*----- Пин PJ10 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ10_GPIO                  CHIP_PIN_ID(CHIP_PORT_J, 10, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 0) */
#define CHIP_PIN_PJ10_TIM1_CH2N             CHIP_PIN_ID(CHIP_PORT_J, 10, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 2) */
#define CHIP_PIN_PJ10_TIM8_CH2              CHIP_PIN_ID(CHIP_PORT_J, 10, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 4) */
#define CHIP_PIN_PJ10_SPI5_MOSI             CHIP_PIN_ID(CHIP_PORT_J, 10, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 10, 13) */
#define CHIP_PIN_PJ10_LCD_G3                CHIP_PIN_ID(CHIP_PORT_J, 10, 14)
#define CHIP_PIN_PJ10_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_J, 10, 15)
#endif


/*----- Пин PJ11 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ11_GPIO                  CHIP_PIN_ID(CHIP_PORT_J, 11, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 0) */
#define CHIP_PIN_PJ11_TIM1_CH2              CHIP_PIN_ID(CHIP_PORT_J, 11, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 2) */
#define CHIP_PIN_PJ11_TIM8_CH2N             CHIP_PIN_ID(CHIP_PORT_J, 11, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 4) */
#define CHIP_PIN_PJ11_SPI5_MISO             CHIP_PIN_ID(CHIP_PORT_J, 11, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 11, 13) */
#define CHIP_PIN_PJ11_LCD_G4                CHIP_PIN_ID(CHIP_PORT_J, 11, 14)
#define CHIP_PIN_PJ11_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_J, 11, 15)
#endif


/*----- Пин PJ12 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ12_GPIO                  CHIP_PIN_ID(CHIP_PORT_J, 12, CHIP_PIN_FUNC_GPIO)
#define CHIP_PIN_PJ12_TRGOUT                CHIP_PIN_ID(CHIP_PORT_J, 12, 0)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 8) */
#define CHIP_PIN_PJ12_LCD_G3                CHIP_PIN_ID(CHIP_PORT_J, 12, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 12, 13) */
#define CHIP_PIN_PJ12_LCD_B0                CHIP_PIN_ID(CHIP_PORT_J, 12, 14)
#define CHIP_PIN_PJ12_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_J, 12, 15)
#endif


/*----- Пин PJ113 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ13_GPIO                  CHIP_PIN_ID(CHIP_PORT_J, 13, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 8) */
#define CHIP_PIN_PJ13_LCD_B4                CHIP_PIN_ID(CHIP_PORT_J, 13, 9)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 13, 13) */
#define CHIP_PIN_PJ13_LCD_B1                CHIP_PIN_ID(CHIP_PORT_J, 13, 14)
#define CHIP_PIN_PJ13_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_J, 13, 15)
#endif


/*----- Пин PJ14 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ14_GPIO                  CHIP_PIN_ID(CHIP_PORT_J, 14, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 14, 13) */
#define CHIP_PIN_PJ14_LCD_B2                CHIP_PIN_ID(CHIP_PORT_J, 14, 14)
#define CHIP_PIN_PJ14_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_J, 14, 15)
#endif


/*----- Пин PJ15 ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PJ15_GPIO                  CHIP_PIN_ID(CHIP_PORT_J, 15, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_J, 15, 13) */
#define CHIP_PIN_PJ15_LCD_B3                CHIP_PIN_ID(CHIP_PORT_J, 15, 14)
#define CHIP_PIN_PJ15_EVENTOUT              CHIP_PIN_ID(CHIP_PORT_J, 15, 15)
#endif


/********************************** Порт K *************************************/
/*----- Пин PK0  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK0_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 0, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 0) */
#define CHIP_PIN_PK0_TIM1_CH1N              CHIP_PIN_ID(CHIP_PORT_K, 0, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 2) */
#define CHIP_PIN_PK0_TIM8_CH3               CHIP_PIN_ID(CHIP_PORT_K, 0, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 4) */
#define CHIP_PIN_PK0_SPI5_SCK               CHIP_PIN_ID(CHIP_PORT_K, 0, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 0, 13) */
#define CHIP_PIN_PK0_LCD_G5                 CHIP_PIN_ID(CHIP_PORT_K, 0, 14)
#define CHIP_PIN_PK0_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 0, 15)
#endif


/*----- Пин PK1  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK1_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 1, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 0) */
#define CHIP_PIN_PK1_TIM1_CH1               CHIP_PIN_ID(CHIP_PORT_K, 1, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 2) */
#define CHIP_PIN_PK1_TIM8_CH3N              CHIP_PIN_ID(CHIP_PORT_K, 1, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 4) */
#define CHIP_PIN_PK1_SPI5_NSS               CHIP_PIN_ID(CHIP_PORT_K, 1, 5)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 1, 13) */
#define CHIP_PIN_PK1_LCD_G6                 CHIP_PIN_ID(CHIP_PORT_K, 1, 14)
#define CHIP_PIN_PK1_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 1, 15)
#endif


/*----- Пин PK2  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK2_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 2, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 0) */
#define CHIP_PIN_PK2_TIM1_BKIN              CHIP_PIN_ID(CHIP_PORT_K, 2, 1)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 2) */
#define CHIP_PIN_PK2_TIM8_BKIN              CHIP_PIN_ID(CHIP_PORT_K, 2, 3)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 9) */
#define CHIP_PIN_PK2_TIM8_BKIN_COMP12       CHIP_PIN_ID(CHIP_PORT_K, 2, 10)
#define CHIP_PIN_PK2_TIM1_BKIN_COMP12       CHIP_PIN_ID(CHIP_PORT_K, 2, 11)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 2, 13) */
#define CHIP_PIN_PK2_LCD_G7                 CHIP_PIN_ID(CHIP_PORT_K, 2, 14)
#define CHIP_PIN_PK2_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 2, 15)
#endif


/*----- Пин PK3  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK3_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 3, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 3, 13) */
#define CHIP_PIN_PK3_LCD_B4                 CHIP_PIN_ID(CHIP_PORT_K, 3, 14)
#define CHIP_PIN_PK3_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 3, 15)
#endif


/*----- Пин PK4  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK4_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 4, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 4, 13) */
#define CHIP_PIN_PK4_LCD_B5                 CHIP_PIN_ID(CHIP_PORT_K, 4, 14)
#define CHIP_PIN_PK4_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 4, 15)
#endif


/*----- Пин PK5  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK5_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 5, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 5, 13) */
#define CHIP_PIN_PK5_LCD_B6                 CHIP_PIN_ID(CHIP_PORT_K, 5, 14)
#define CHIP_PIN_PK5_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 5, 15)
#endif


/*----- Пин PK6  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK6_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 6, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 6, 13) */
#define CHIP_PIN_PK6_LCD_B7                 CHIP_PIN_ID(CHIP_PORT_K, 6, 14)
#define CHIP_PIN_PK6_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 6, 15)
#endif


/*----- Пин PK7  ------------------------------------------------------------- */
#if !defined CHIP_DEV_STM32H723
#define CHIP_PIN_PK7_GPIO                   CHIP_PIN_ID(CHIP_PORT_K, 7, CHIP_PIN_FUNC_GPIO)
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 0) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 1) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 2) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 3) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 4) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 5) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 6) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 7) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 8) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 9) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 10) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 11) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 12) */
/* -----                                    CHIP_PIN_ID(CHIP_PORT_K, 7, 13) */
#define CHIP_PIN_PK7_LCD_DE                 CHIP_PIN_ID(CHIP_PORT_K, 7, 14)
#define CHIP_PIN_PK7_EVENTOUT               CHIP_PIN_ID(CHIP_PORT_K, 7, 15)
#endif


#endif // CHIP_PINS_H

