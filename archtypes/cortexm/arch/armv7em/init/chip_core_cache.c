#include "chip.h"



void chip_icache_enable() {
#if CHIP_CORE_SUPPORT_ICACHE
    if (!(CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_IC)) {
        chip_icache_full_invalidate();
        CHIP_REGS_SCB->CCR |=  CHIP_REGFLD_SCB_CCR_IC;
        chip_dsb();
        chip_isb();
    }
#endif
}

void chip_icache_disable() {
#if CHIP_CORE_SUPPORT_ICACHE
    if (CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_IC) {
        chip_dsb();
        chip_isb();
        SCB->CCR &= ~CHIP_REGFLD_SCB_CCR_IC;
        chip_dsb();
        chip_isb();
    }
#endif
}


void chip_dcache_enable() {
#if CHIP_CORE_SUPPORT_DCACHE
    if (!(CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_DC)) {
        chip_dcache_full_invalidate();
        CHIP_REGS_SCB->CCR |=  CHIP_REGFLD_SCB_CCR_DC;
        chip_dsb();
        chip_isb();
    }
#endif
}

void chip_dcache_disable() {
#if CHIP_CORE_SUPPORT_DCACHE
    if (CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_DC) {
        chip_dcache_full_flush();
        CHIP_REGS_SCB->CCR &= ~CHIP_REGFLD_SCB_CCR_DC;
        chip_dsb();
        chip_isb();
    }
#endif
}


#if CHIP_CACHE_SETUP_EN
void chip_cache_init(void) {
#if CHIP_CORE_SUPPORT_ICACHE
#if CHIP_ICACHE_EN
    chip_icache_enable();
#else
    chip_icache_disable();
#endif
#endif

#if CHIP_CORE_SUPPORT_DCACHE
#if CHIP_DCACHE_EN
    chip_dcache_enable();
#else
    chip_dcache_disable();
#endif
#endif
}
#endif


