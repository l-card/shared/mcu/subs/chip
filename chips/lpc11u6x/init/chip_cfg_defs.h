#ifndef CHIP_CFG_DEFS_H
#define CHIP_CFG_DEFS_H

#include "chip_config.h"

#ifdef CHIP_CFG_CLK_SYSOSC_MODE
    #define CHIP_CLK_SYSOSC_MODE    CHIP_CFG_CLK_SYSOSC_MODE
#else
    #define CHIP_CLK_SYSOSC_MODE    CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_IRC_EN
    #define CHIP_CLK_IRC_EN         CHIP_CFG_CLK_IRC_EN
#else
    #define CHIP_CLK_IRC_EN         1
#endif

#ifdef CHIP_CFG_CLK_RTCOSC_EN
    #define CHIP_CLK_RTCOSC_EN      CHIP_CFG_CLK_RTCOSC_EN
#else
    #define CHIP_CLK_RTCOSC_EN      0
#endif

#ifdef CHIP_CFG_CLK_SYSPLL_EN
    #define CHIP_CLK_SYSPLL_EN          CHIP_CFG_CLK_SYSPLL_EN
#else
    #define CHIP_CLK_SYSPLL_EN      0
#endif

#if CHIP_CLK_SYSPLL_EN
    #define CHIP_CLK_SYSPLLIN_EN    1
#elif defined CHIP_CFG_CLK_SYSPLLIN_EN
    #define CHIP_CLK_SYSPLLIN_EN    CHIP_CFG_CLK_SYSPLLIN_EN
#else
    #define CHIP_CLK_SYSPLLIN_EN    0
#endif

#ifdef CHIP_CFG_CLK_SYSPLL_SRC
    #define CHIP_CLK_SYSPLL_SRC     CHIP_CFG_CLK_SYSPLL_SRC
#else
    #define CHIP_CLK_SYSPLL_SRC     CHIP_CLK_IRC
#endif

#ifdef CHIP_CFG_CLK_SYSPLL_MUL
    #define CHIP_CLK_SYSPLL_MUL     CHIP_CFG_CLK_SYSPLL_MUL
#else
    #define CHIP_CLK_SYSPLL_MUL     1
#endif

#ifdef CHIP_CFG_CLK_USB_SETUP_EN
    #define CHIP_CLK_USB_SETUP_EN   CHIP_CFG_CLK_USB_SETUP_EN
#else
    #define CHIP_CLK_USB_SETUP_EN   0
#endif

#ifdef CHIP_CFG_CLK_USBPLL_EN
    #define CHIP_CLK_USBPLL_EN      CHIP_CFG_CLK_USBPLL_EN
#else
    #define CHIP_CLK_USBPLL_EN      0
#endif

#ifdef CHIP_CFG_CLK_USBPLL_SRC
    #define CHIP_CLK_USBPLL_SRC     CHIP_CFG_CLK_USBPLL_SRC
#else
    #define CHIP_CLK_USBPLL_SRC     CHIP_CLK_IRC
#endif

#ifdef CHIP_CFG_CLK_USBPLL_MUL
    #define CHIP_CLK_USBPLL_MUL     CHIP_CFG_CLK_USBPLL_MUL
#else
    #define CHIP_CLK_USBPLL_MUL     1
#endif

#ifdef CHIP_CFG_CLK_USB_SRC
    #define CHIP_CLK_USB_SRC        CHIP_CFG_CLK_USB_SRC
#else
    #define CHIP_CLK_USB_SRC        CHIP_CLK_USBPLL
#endif

#ifdef CHIP_CFG_CLK_USB_DIV
    #define CHIP_CLK_USB_DIV        CHIP_CFG_CLK_USB_DIV
#else
    #define CHIP_CLK_USB_DIV        1
#endif


#ifdef CHIP_CFG_CLK_MAIN_SRC
    #define CHIP_CLK_MAIN_SRC       CHIP_CFG_CLK_MAIN_SRC
#else
    #define CHIP_CLK_MAIN_SRC       CHIP_CLK_IRC
#endif

#ifdef CHIP_CFG_CLK_SYS_DIV
    #define CHIP_CLK_SYS_DIV        CHIP_CFG_CLK_SYS_DIV
#else
    #define CHIP_CLK_SYS_DIV        1
#endif

#ifdef CHIP_CFG_CLK_CLKOUT_EN
    #define CHIP_CLK_CLKOUT_EN      CHIP_CFG_CLK_CLKOUT_EN
#else
    #define CHIP_CLK_CLKOUT_EN      0
#endif

#ifdef CHIP_CFG_CLK_CLKOUT_SRC
    #define CHIP_CLK_CLKOUT_SRC     CHIP_CFG_CLK_CLKOUT_SRC
#else
    #define CHIP_CLK_CLKOUT_SRC     CHIP_CLK_IRC
#endif

#ifdef CHIP_CFG_CLK_CLKOUT_DIV
    #define CHIP_CLK_CLKOUT_DIV     CHIP_CFG_CLK_CLKOUT_DIV
#else
    #define CHIP_CLK_CLKOUT_DIV     1
#endif

#ifdef CHIP_CFG_CLK_WDTOSC_SETUP_EN
    #define CHIP_CLK_WDTOSC_SETUP_EN    CHIP_CFG_CLK_WDTOSC_SETUP_EN
#else
    #define CHIP_CLK_WDTOSC_SETUP_EN    0
#endif

#ifdef CHIP_CFG_CLK_WDTOSC_EN
    #define CHIP_CLK_WDTOSC_EN          CHIP_CFG_CLK_WDTOSC_EN
#else
    #define CHIP_CLK_WDTOSC_EN          0
#endif

#ifdef CHIP_CFG_CLK_WDTOSC_SRC_FREQ
    #define CHIP_CLK_WDTOSC_SRC_FREQ    CHIP_CFG_CLK_WDTOSC_SRC_FREQ
#else
    #define CHIP_CLK_WDTOSC_SRC_FREQ    CHIP_KHZ(600)
#endif

#ifdef CHIP_CFG_CLK_WDTOSC_DIV
    #define CHIP_CLK_WDTOSC_DIV         CHIP_CFG_CLK_WDTOSC_DIV
#else
    #define CHIP_CLK_WDTOSC_DIV         2
#endif



#ifdef CHIP_CFG_CLK_USART0_DIV
    #define CHIP_CLK_USART0_DIV     CHIP_CFG_CLK_USART0_DIV
#else
    #define CHIP_CLK_USART0_DIV     0
#endif

#ifdef CHIP_CFG_CLK_SSP0_DIV
    #define CHIP_CLK_SSP0_DIV       CHIP_CFG_CLK_SSP0_DIV
#else
    #define CHIP_CLK_SSP0_DIV       0
#endif

#ifdef CHIP_CFG_CLK_SSP0_DIV
    #define CHIP_CLK_SSP1_DIV       CHIP_CFG_CLK_SSP1_DIV
#else
    #define CHIP_CLK_SSP1_DIV       0
#endif

#ifdef CHIP_CFG_CLK_FRG_DIV
    #define CHIP_CLK_FRG_DIV        CHIP_CFG_CLK_FRG_DIV
#else
    #define CHIP_CLK_FRG_DIV        0
#endif

#ifdef CHIP_CFG_CLK_FRG_FRACT_MUL
    #define CHIP_CLK_FRG_FRACT_MUL  CHIP_CFG_CLK_FRG_FRACT_MUL
#else
    #define CHIP_CLK_FRG_FRACT_MUL  0
#endif

#ifdef CHIP_CFG_WDT_SETUP_EN
    #define CHIP_WDT_SETUP_EN       CHIP_CFG_WDT_SETUP_EN
#else
    #define CHIP_WDT_SETUP_EN       0
#endif

#ifdef CHIP_CFG_WDT_CLK_SRC
    #define CHIP_WDT_CLK_SRC        CHIP_CFG_WDT_CLK_SRC
#else
    #define CHIP_WDT_CLK_SRC        CHIP_CLK_IRC
#endif

#ifdef CHIP_CFG_WDT_CLK_LOCK
    #define CHIP_WDT_CLK_LOCK       CHIP_CFG_WDT_CLK_LOCK
#else
    #define CHIP_WDT_CLK_LOCK       0
#endif

#ifdef CHIP_CFG_WDT_TC_PROTECT
    #define CHIP_WDT_TC_PROTECT     CHIP_CFG_WDT_TC_PROTECT
#else
    #define CHIP_WDT_TC_PROTECT     0
#endif

#ifdef  CHIP_CFG_WDT_TIMEOUT_VAL
    #define CHIP_WDT_TIMEOUT_VAL    CHIP_CFG_WDT_TIMEOUT_VAL
#elif CHIP_CFG_WDT_SETUP_EN
    #error WDT tout value is not specified
#endif

#ifdef CHIP_CFG_WDT_WARNTIME_VAL
    #define CHIP_WDT_WARNTIME_VAL   CHIP_CFG_WDT_WARNTIME_VAL
#else
    #define CHIP_WDT_WARNTIME_VAL   0
#endif

#ifdef CHIP_CFG_WDT_WINDOW_VAL
    #define CHIP_WDT_WINDOW_VAL     CHIP_CFG_WDT_WINDOW_VAL
#else
    #define CHIP_WDT_WINDOW_VAL     CHIP_WDT_WINDOW_MAX_VAL
#endif


#ifdef CHIP_CFG_CLK_ROM_EN
    #define CHIP_CLK_ROM_EN         CHIP_CFG_CLK_ROM_EN
#else
    #define CHIP_CLK_ROM_EN         1
#endif
#ifdef CHIP_CFG_CLK_SRAM0_EN
    #define CHIP_CLK_SRAM0_EN       CHIP_CFG_CLK_SRAM0_EN
#else
    #define CHIP_CLK_SRAM0_EN       1
#endif
#ifdef CHIP_CFG_CLK_FLASHREG_EN
    #define CHIP_CLK_FLASHREG_EN    CHIP_CFG_CLK_FLASHREG_EN
#else
    #define CHIP_CLK_FLASHREG_EN    1
#endif
#ifdef CHIP_CFG_CLK_FLASH_EN
    #define CHIP_CLK_FLASH_EN       CHIP_CFG_CLK_FLASH_EN
#else
    #define CHIP_CLK_FLASH_EN       1
#endif
#ifdef CHIP_CFG_CLK_I2C0_EN
    #define CHIP_CLK_I2C0_EN        CHIP_CFG_CLK_I2C0_EN
#else
    #define CHIP_CLK_I2C0_EN        0
#endif
#ifdef CHIP_CFG_CLK_GPIO_EN
    #define CHIP_CLK_GPIO_EN        CHIP_CFG_CLK_GPIO_EN
#else
    #define CHIP_CLK_GPIO_EN        1
#endif
#ifdef CHIP_CFG_CLK_CT16B0_EN
    #define CHIP_CLK_CT16B0_EN      CHIP_CFG_CLK_CT16B0_EN
#else
    #define CHIP_CLK_CT16B0_EN      0
#endif
#ifdef CHIP_CFG_CLK_CT16B1_EN
    #define CHIP_CLK_CT16B1_EN      CHIP_CFG_CLK_CT16B1_EN
#else
    #define CHIP_CLK_CT16B1_EN      0
#endif
#ifdef CHIP_CFG_CLK_CT32B0_EN
    #define CHIP_CLK_CT32B0_EN      CHIP_CFG_CLK_CT32B0_EN
#else
    #define CHIP_CLK_CT32B0_EN      0
#endif
#ifdef CHIP_CFG_CLK_CT32B1_EN
    #define CHIP_CLK_CT32B1_EN      CHIP_CFG_CLK_CT32B1_EN
#else
    #define CHIP_CLK_CT32B1_EN      0
#endif
#ifdef CHIP_CFG_CLK_SSP0_EN
    #define CHIP_CLK_SSP0_EN        CHIP_CFG_CLK_SSP0_EN
#else
    #define CHIP_CLK_SSP0_EN        0
#endif
#ifdef CHIP_CFG_CLK_USART0_EN
    #define CHIP_CLK_USART0_EN      CHIP_CFG_CLK_USART0_EN
#else
    #define CHIP_CLK_USART0_EN      0
#endif
#ifdef CHIP_CFG_CLK_ADC_EN
    #define CHIP_CLK_ADC_EN         CHIP_CFG_CLK_ADC_EN
#else
    #define CHIP_CLK_ADC_EN         0
#endif
#ifdef CHIP_CFG_CLK_USB_EN
    #define CHIP_CLK_USB_EN         CHIP_CFG_CLK_USB_EN
#else
    #define CHIP_CLK_USB_EN         0
#endif
#if CHIP_CFG_WDT_SETUP_EN
    #define CHIP_CLK_WWDT_EN        1
#elif defined CHIP_CFG_CLK_WWDT_EN
    #define CHIP_CLK_WWDT_EN        CHIP_CFG_CLK_WWDT_EN
#else
    #define CHIP_CLK_WWDT_EN        0
#endif
#ifdef CHIP_CFG_CLK_IOCON_EN
    #define CHIP_CLK_IOCON_EN       CHIP_CFG_CLK_IOCON_EN
#else
    #define CHIP_CLK_IOCON_EN       0
#endif
#ifdef CHIP_CFG_CLK_SSP1_EN
    #define CHIP_CLK_SSP1_EN        CHIP_CFG_CLK_SSP1_EN
#else
    #define CHIP_CLK_SSP1_EN        0
#endif
#ifdef CHIP_CFG_CLK_PINT_EN
    #define CHIP_CLK_PINT_EN        CHIP_CFG_CLK_PINT_EN
#else
    #define CHIP_CLK_PINT_EN        0
#endif
#ifdef CHIP_CFG_CLK_USART1_EN
    #define CHIP_CLK_USART1_EN      CHIP_CFG_CLK_USART1_EN
#else
    #define CHIP_CLK_USART1_EN      0
#endif
#ifdef CHIP_CFG_CLK_USART2_EN
    #define CHIP_CLK_USART2_EN      CHIP_CFG_CLK_USART2_EN
#else
    #define CHIP_CLK_USART2_EN      0
#endif
#ifdef CHIP_CFG_CLK_USART3_EN
    #define CHIP_CLK_USART3_EN      CHIP_CFG_CLK_USART3_EN
#else
    #define CHIP_CLK_USART3_EN      0
#endif
#ifdef CHIP_CFG_CLK_USART4_EN
    #define CHIP_CLK_USART4_EN      CHIP_CFG_CLK_USART4_EN
#else
    #define CHIP_CLK_USART4_EN      0
#endif
#define CHIP_CLK_USART3_4_EN        (CHIP_CLK_USART3_EN || CHIP_CLK_USART4_EN)
#ifdef CHIP_CFG_CLK_GROUP0INT_EN
    #define CHIP_CLK_GROUP0INT_EN   CHIP_CFG_CLK_GROUP0INT_EN
#else
    #define CHIP_CLK_GROUP0INT_EN   0
#endif
#ifdef CHIP_CFG_CLK_GROUP1INT_EN
    #define CHIP_CLK_GROUP1INT_EN   CHIP_CFG_CLK_GROUP1INT_EN
#else
    #define CHIP_CLK_GROUP1INT_EN   0
#endif
#ifdef CHIP_CFG_CLK_I2C1_EN
    #define CHIP_CLK_I2C1_EN        CHIP_CFG_CLK_I2C1_EN
#else
    #define CHIP_CLK_I2C1_EN        0
#endif
#ifdef CHIP_CFG_CLK_SRAM1_EN
    #define CHIP_CLK_SRAM1_EN       CHIP_CFG_CLK_SRAM1_EN
#else
    #define CHIP_CLK_SRAM1_EN       0
#endif
#ifdef CHIP_CFG_CLK_USBSRAM_EN
    #define CHIP_CLK_USBSRAM_EN     CHIP_CFG_CLK_USBSRAM_EN
#else
    #define CHIP_CLK_USBSRAM_EN     0
#endif
#ifdef CHIP_CFG_CLK_CRC_EN
    #define CHIP_CLK_CRC_EN         CHIP_CFG_CLK_CRC_EN
#else
    #define CHIP_CLK_CRC_EN         0
#endif
#ifdef CHIP_CFG_CLK_DMA_EN
    #define CHIP_CLK_DMA_EN         CHIP_CFG_CLK_DMA_EN
#else
    #define CHIP_CLK_DMA_EN         0
#endif
#ifdef CHIP_CFG_CLK_RTC_EN
    #define CHIP_CLK_RTC_EN         CHIP_CFG_CLK_RTC_EN
#else
    #define CHIP_CLK_RTC_EN         0
#endif
#ifdef CHIP_CFG_CLK_SCT0_EN
    #define CHIP_CLK_SCT0_EN        CHIP_CFG_CLK_SCT0_EN
#else
    #define CHIP_CLK_SCT0_EN        0
#endif
#ifdef CHIP_CFG_CLK_SCT1_EN
    #define CHIP_CLK_SCT1_EN        CHIP_CFG_CLK_SCT1_EN
#else
    #define CHIP_CLK_SCT1_EN        0
#endif
#define CHIP_CLK_SCT0_1_EN          (CHIP_CLK_SCT0_EN || CHIP_CLK_SCT1_EN)

#ifdef CHIP_CFG_CRP_MODE
    #define CHIP_CRP_MODE            CHIP_CFG_CRP_MODE
#else
    #define CHIP_CRP_MODE            CHIP_CRP_DISABLED
#endif

#ifdef CHIP_CFG_CLK_FRG_EN
    #define CHIP_CLK_FRG_EN   CHIP_CFG_CLK_FRG_EN
#else
    #define CHIP_CLK_FRG_EN   0
#endif



#endif // CHIP_CFG_DEFS_H
