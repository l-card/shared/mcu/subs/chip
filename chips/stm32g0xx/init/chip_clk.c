#include "chip.h"
#include "chip_clk_defs.h"
#include "chip_clk_constraints.h"
#if CHIP_CFG_LTIMER_EN
    #include "ltimer.h"
#endif


#define CHIP_CLK_BD_CFG_SETUP_EN (CHIP_CFG_CLK_LSE_SETUP_EN | \
    CHIP_CFG_CLK_RTC_SETUP_EN | \
    CHIP_CFG_CLK_LSCO_SETUP_EN)


#if CHIP_CFG_CLK_HSE_MODE == CHIP_CLK_EXTMODE_CLOCK
    #if CHIP_CFG_CLK_HSE_FREQ > CHIP_CLK_HSE_ECLK_FREQ_MAX
        #error HSE external clock frequency is too high
    #endif
#elif CHIP_CFG_CLK_HSE_MODE == CHIP_CLK_EXTMODE_OSC
    #if CHIP_CFG_CLK_HSE_FREQ < CHIP_CLK_HSE_OSC_FREQ_MIN
        #error HSE external oscillator frequency is too low
    #endif
    #if CHIP_CFG_CLK_HSE_FREQ > CHIP_CLK_HSE_OSC_FREQ_MAX
        #error HSE external oscillator frequency is too high
    #endif
#endif


#if CHIP_CFG_CLK_LSE_SETUP_EN
    #if (CHIP_CFG_CLK_LSE_DRV < CHIP_CLK_LSE_DRV_LOWEST) || \
        (CHIP_CFG_CLK_LSE_DRV > CHIP_CLK_LSE_DRV_HIGHEST)
        #error Invalid LSE oscillator driving capability
    #endif

    #define CHIP_RVAL_RCC_BDCR_LSEDRV CHIP_CFG_CLK_LSE_DRV
#endif

#define CHIP_RCC_CR_CLK_WTRDY_MSK ((CHIP_CFG_CLK_HSI16_EN ? CHIP_REGFLD_RCC_CR_HSIRDY : 0) \
                              | (CHIP_CLK_HSI48_EN ? CHIP_REGFLD_RCC_CR_HSI48RDY : 0) \
                              | (CHIP_CLK_HSE_EN ? CHIP_REGFLD_RCC_CR_HSERDY : 0))


#if CHIP_CFG_CLK_HSI16_EN
    #ifndef CHIP_CFG_CLK_HSISYS_DIV
        #error HSISYS_DIV must be defined
    #else
        #if CHIP_CFG_CLK_HSISYS_DIV == 1
            #define CHIP_RVAL_RCC_CR_HSIDIV 0
        #elif CHIP_CFG_CLK_HSISYS_DIV == 2
            #define CHIP_RVAL_RCC_CR_HSIDIV 1
        #elif CHIP_CFG_CLK_HSISYS_DIV == 4
            #define CHIP_RVAL_RCC_CR_HSIDIV 2
        #elif CHIP_CFG_CLK_HSISYS_DIV == 8
            #define CHIP_RVAL_RCC_CR_HSIDIV 3
        #elif CHIP_CFG_CLK_HSISYS_DIV == 16
            #define CHIP_RVAL_RCC_CR_HSIDIV 4
        #elif CHIP_CFG_CLK_HSISYS_DIV == 32
            #define CHIP_RVAL_RCC_CR_HSIDIV 5
        #elif CHIP_CFG_CLK_HSISYS_DIV == 64
            #define CHIP_RVAL_RCC_CR_HSIDIV 6
        #elif CHIP_CFG_CLK_HSISYS_DIV == 128
            #define CHIP_RVAL_RCC_CR_HSIDIV 7
        #else
            #error invalid HSISYS_DIV value
        #endif
    #endif
#endif



/* --------------------------- проверка параметров PLL -----------------------*/
#if !CHIP_CFG_CLK_PLL_EN
    #define CHIP_CLK_PLL_EN         0
    #define CHIP_RVAL_RCC_PLLCFGR_PLLSRC    0
#else
    #define CHIP_CLK_PLL_EN 1

    /* проверка источника частоты PLL */
    #if CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_PLLCFGR_PLLSRC 2
    #elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_PLLCFGR_PLLSRC 3
    #else
        #error invalid PLL clock source (must be HSI16 or HSE)
    #endif

    #if (!CHIP_CLK_ID_EN(CHIP_CLK_PLL_SRC))
        #error PLL input frequency source must be enabled
    #endif



    #if (CHIP_CFG_CLK_PLL_DIVM < CHIP_CLK_PLL_DIVM_MIN) || (CHIP_CFG_CLK_PLL_DIVM > CHIP_CLK_PLL_DIVM_MAX)
        #error invalid PLL DIVM value
    #endif

    #if (CHIP_CLK_PLL_IN_FREQ < CHIP_CLK_PLL_IN_FREQ_MIN) || (CHIP_CLK_PLL_IN_FREQ > CHIP_CLK_PLL_IN_FREQ_MAX)
        #error invalid PLL input frequency
    #endif


    #if (CHIP_CFG_CLK_PLL_DIVN < CHIP_CLK_PLL_DIVN_MIN)  || (CHIP_CFG_CLK_PLL_DIVN > CHIP_CLK_PLL_DIVN_MAX)
        #error invalid PLL DIVN value
    #endif

    #if (CHIP_CLK_PLL_VCO_FREQ < CHIP_CLK_PLL_VCO_FREQ_MIN) || (CHIP_CLK_PLL_VCO_FREQ > CHIP_CLK_PLL_VCO_FREQ_MAX)
        #error PLL VCO Frequency out of range
    #endif

    #define CHIP_RVAL_RCC_PLLCFGR_PLLM (CHIP_CFG_CLK_PLL_DIVM - 1)
    #define CHIP_RVAL_RCC_PLLCFGR_PLLN (CHIP_CFG_CLK_PLL_DIVN)



    #if CHIP_CFG_CLK_PLL_DIVP_EN
        #if  (CHIP_CFG_CLK_PLL_DIVP < CHIP_CLK_PLL_DIVP_MIN) || \
             (CHIP_CFG_CLK_PLL_DIVP > CHIP_CLK_PLL_DIVP_MAX)
            #error invalid PLL DIVP value
        #endif

        #if (CHIP_CLK_PLLP_FREQ < CHIP_CLK_PLL_OUT_P_FREQ_MIN) || \
            (CHIP_CLK_PLLP_FREQ > CHIP_CLK_PLL_OUT_P_FREQ_MAX)
            #error invalid PLL P output freqence
        #endif

        #define CHIP_RVAL_RCC_PLLCFGR_PLLP  (CHIP_CFG_CLK_PLL_DIVP - 1)
    #else
        #define CHIP_RVAL_RCC_PLLCFGR_PLLP 0
    #endif

    #if CHIP_CFG_CLK_PLL_DIVQ_EN
        #if  (CHIP_CFG_CLK_PLL_DIVQ < CHIP_CLK_PLL_DIVQ_MIN) || \
             (CHIP_CFG_CLK_PLL_DIVQ > CHIP_CLK_PLL_DIVQ_MAX)
            #error invalid PLL DIVQ value
        #endif

        #if (CHIP_CLK_PLLQ_FREQ < CHIP_CLK_PLL_OUT_Q_FREQ_MIN) || \
            (CHIP_CLK_PLLQ_FREQ > CHIP_CLK_PLL_OUT_Q_FREQ_MAX)
            #error invalid PLL Q output freqence
        #endif

        #define CHIP_RVAL_RCC_PLLCFGR_PLLQ  (CHIP_CFG_CLK_PLL_DIVQ - 1)
    #else
        #define CHIP_RVAL_RCC_PLLCFGR_PLLQ 0
    #endif

    #if CHIP_CFG_CLK_PLL_DIVR_EN
        #if  (CHIP_CFG_CLK_PLL_DIVR < CHIP_CLK_PLL_DIVR_MIN) || \
             (CHIP_CFG_CLK_PLL_DIVR > CHIP_CLK_PLL_DIVR_MAX)
            #error invalid PLL DIVR value
        #endif

        #if (CHIP_CLK_PLLR_FREQ < CHIP_CLK_PLL_OUT_R_FREQ_MIN) || \
            (CHIP_CLK_PLLR_FREQ > CHIP_CLK_PLL_OUT_R_FREQ_MAX)
            #error invalid PLL R output freqence
        #endif

        #define CHIP_RVAL_RCC_PLLCFGR_PLLR (CHIP_CFG_CLK_PLL_DIVR - 1)
    #else
        #define CHIP_RVAL_RCC_PLLCFGR_PLLR 0
    #endif
#endif

/* --------------------- Проверка параметров SystemClock -------------------- */
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_LSE_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_LSE
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_LSI_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_LSI
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_HSE
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLLR_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_PLLR
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSISYS_ID
    #define CHIP_RVAL_RCC_CFGR_SW   CHIP_REGFLDVAL_RCC_CFGR_SW_HSISYS
#else
    #error Invalid SYSCLK clock source
#endif

#if !CHIP_CLK_SYS_EN
    #error System clock source is not enabled
#endif


/* --------------------- Проверка частоты AHB --------------------------------*/
#ifndef CHIP_CFG_CLK_AHB_DIV
    #error AHB_DIV must be defined
#else
    #if CHIP_CFG_CLK_AHB_DIV == 1
        #define CHIP_RVAL_RCC_CFGR_HPRE 0x0
    #elif CHIP_CFG_CLK_AHB_DIV == 2
        #define CHIP_RVAL_RCC_CFGR_HPRE 0x8
    #elif CHIP_CFG_CLK_AHB_DIV == 4
        #define CHIP_RVAL_RCC_CFGR_HPRE 0x9
    #elif CHIP_CFG_CLK_AHB_DIV == 8
        #define CHIP_RVAL_RCC_CFGR_HPRE 0xA
    #elif CHIP_CFG_CLK_AHB_DIV == 16
        #define CHIP_RVAL_RCC_CFGR_HPRE 0xB
    #elif CHIP_CFG_CLK_AHB_DIV == 64
        #define CHIP_RVAL_RCC_CFGR_HPRE 0xC
    #elif CHIP_CFG_CLK_AHB_DIV == 128
        #define CHIP_RVAL_RCC_CFGR_HPRE 0xD
    #elif CHIP_CFG_CLK_AHB_DIV == 256
        #define CHIP_RVAL_RCC_CFGR_HPRE 0xE
    #elif CHIP_CFG_CLK_AHB_DIV == 512
        #define CHIP_RVAL_RCC_CFGR_HPRE 0xF
    #else
        #error invalid AHB_DIV value
    #endif

    #if (CHIP_CLK_AHB_FREQ > CHIP_CLK_HCLK_FREQ_MAX)
        #error invalid AHB frequence
    #endif
#endif



/* --------------------- Проверка частоты APB --------------------------------*/
#ifndef CHIP_CFG_CLK_APB_DIV
    #error APB_DIV must be defined
#else
    #if CHIP_CFG_CLK_APB_DIV == 1
        #define CHIP_RVAL_RCC_CFGR_PPRE 0x0
    #elif CHIP_CFG_CLK_APB_DIV == 2
        #define CHIP_RVAL_RCC_CFGR_PPRE 0x4
    #elif CHIP_CFG_CLK_APB_DIV == 4
        #define CHIP_RVAL_RCC_CFGR_PPRE 0x5
    #elif CHIP_CFG_CLK_APB_DIV == 8
        #define CHIP_RVAL_RCC_CFGR_PPRE 0x6
    #elif CHIP_CFG_CLK_APB_DIV == 16
        #define CHIP_RVAL_RCC_CFGR_PPRE 0x7
    #else
        #error invalid APB_DIV value
    #endif

    #if (CHIP_CLK_APB_FREQ > CHIP_CLK_PCLK_FREQ_MAX)
        #error invalid APB frequence
    #endif
#endif


/* ------------------- Параметры клока RTC -----------------------------------*/
#if CHIP_CFG_CLK_RTC_SETUP_EN
    #if CHIP_CLK_RTC_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_BDCR_RTCSEL 0
    #elif CHIP_CLK_RTC_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_BDCR_RTCSEL 1
    #elif CHIP_CLK_RTC_SRC_ID == CHIP_CLK_HSE_DIV32_ID
        #define CHIP_RVAL_RCC_BDCR_RTCSEL 2
    #else
        #error invalid RTC clock source
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_RTC_SRC)
        #error source clock for RTC is not enabled
    #endif
#else
    #define CHIP_RVAL_RCC_BDCR_RTCSEL 0
#endif


#if CHIP_CFG_CLK_LSCO_SETUP_EN
    #if CHIP_CLK_LSCO_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_BDCR_LSCOSEL 0
    #elif CHIP_CLK_LSCO_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_BDCR_LSCOSEL 1
    #else
        #error invalid LSCO clock source
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_LSCO_SRC)
        #error source clock for LSCO is not enabled
    #endif
#else
    #define CHIP_RVAL_RCC_BDCR_RTCSEL 0
#endif

/* ------------------------- Настройки MCO -----------------------------------*/
#if CHIP_CFG_CLK_MCO_EN
    #if CHIP_CFG_CLK_MCO_DIV == 1
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 0
    #elif  CHIP_CFG_CLK_MCO_DIV == 2
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 1
    #elif  CHIP_CFG_CLK_MCO_DIV == 4
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 2
    #elif  CHIP_CFG_CLK_MCO_DIV == 8
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 3
    #elif  CHIP_CFG_CLK_MCO_DIV == 16
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 4
    #elif  CHIP_CFG_CLK_MCO_DIV == 32
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 5
    #elif  CHIP_CFG_CLK_MCO_DIV == 64
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 6
    #elif  CHIP_CFG_CLK_MCO_DIV == 128
        #define CHIP_RVAL_RCC_CFGR_MCOPRE 7
    #elif  CHIP_CFG_CLK_MCO_DIV == 256
        #if CHIP_DEV_SUPPORT_MCO_CFG_EX
            #define CHIP_RVAL_RCC_CFGR_MCOPRE 8
        #else
            #error unsupported MCO divider for specified device
        #endif
    #elif  CHIP_CFG_CLK_MCO_DIV == 512
        #if CHIP_DEV_SUPPORT_MCO_CFG_EX
            #define CHIP_RVAL_RCC_CFGR_MCOPRE 9
        #else
            #error unsupported MCO divider for specified device
        #endif
    #elif  CHIP_CFG_CLK_MCO_DIV == 1024
        #if CHIP_DEV_SUPPORT_MCO_CFG_EX
            #define CHIP_RVAL_RCC_CFGR_MCOPRE 10
        #else
            #error unsupported MCO divider for specified device
        #endif
    #else
        #error Invalid MCO divider
    #endif


    #define CHIP_CLK_MCO_SRC_ID CHIP_CLK_ID_VAL(CHIP_CFG_CLK_MCO_SRC)
    #if CHIP_CLK_MCO_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CFGR_MCOSEL 1
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSI48_ID
        #if CHIP_DEV_SUPPORT_HSI48
            #define CHIP_RVAL_RCC_CFGR_MCOSEL 2
        #else
            #error HSI48 is not supported on specified devive
        #endif
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CFGR_MCOSEL 3
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_CFGR_MCOSEL 4
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_PLLR_ID
        #define CHIP_RVAL_RCC_CFGR_MCOSEL 5
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_CFGR_MCOSEL 6
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CFGR_MCOSEL 7
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_PLLP_ID
        #if CHIP_DEV_SUPPORT_MCO_CFG_EX
            #define CHIP_RVAL_RCC_CFGR_MCOSEL 8
        #else
            #error unsupported MCO source for specified device
        #endif
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_PLLQ_ID
        #if CHIP_DEV_SUPPORT_MCO_CFG_EX
            #define CHIP_RVAL_RCC_CFGR_MCOSEL 9
        #else
            #error unsupported MCO source for specified device
        #endif
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_RTC_ID
        #if CHIP_DEV_SUPPORT_MCO_CFG_EX
            #define CHIP_RVAL_RCC_CFGR_MCOSEL 10
        #else
            #error unsupported MCO source for specified device
        #endif
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_RTC_WAKEUP_ID
        #if CHIP_DEV_SUPPORT_MCO_CFG_EX
            #define CHIP_RVAL_RCC_CFGR_MCOSEL 11
        #else
            #error unsupported MCO source for specified device
        #endif
    #else
        #error Invalid MCO clock source
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_MCO_SRC)
        #error Source clock for MCO is not enabled
    #endif
#else
    #define CHIP_RVAL_RCC_CFGR_MCOPRE 0
    #define CHIP_RVAL_RCC_CFGR_MCOSEL 0
#endif

#if CHIP_DEV_SUPPORT_MC02
    #if CHIP_CFG_CLK_MCO2_EN
        #if CHIP_CFG_CLK_MCO2_DIV == 1
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 0
        #elif  CHIP_CFG_CLK_MCO2_DIV == 2
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 1
        #elif  CHIP_CFG_CLK_MCO2_DIV == 4
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 2
        #elif  CHIP_CFG_CLK_MCO2_DIV == 8
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 3
        #elif  CHIP_CFG_CLK_MCO2_DIV == 16
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 4
        #elif  CHIP_CFG_CLK_MCO2_DIV == 32
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 5
        #elif  CHIP_CFG_CLK_MCO2_DIV == 64
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 6
        #elif  CHIP_CFG_CLK_MCO2_DIV == 128
            #define CHIP_RVAL_RCC_CFGR_MCO2PRE 7
        #elif  CHIP_CFG_CLK_MCO2_DIV == 256
            #if CHIP_DEV_SUPPORT_MCO_CFG_EX
                #define CHIP_RVAL_RCC_CFGR_MCO2PRE 8
            #else
                #error unsupported MCO divider for specified device
            #endif
        #elif  CHIP_CFG_CLK_MCO2_DIV == 512
            #if CHIP_DEV_SUPPORT_MCO_CFG_EX
                #define CHIP_RVAL_RCC_CFGR_MCO2PRE 9
            #else
                #error unsupported MCO divider for specified device
            #endif
        #elif  CHIP_CFG_CLK_MCO2_DIV == 1024
            #if CHIP_DEV_SUPPORT_MCO_CFG_EX
                #define CHIP_RVAL_RCC_CFGR_MCO2PRE 10
            #else
                #error unsupported MCO divider for specified device
            #endif
        #else
            #error Invalid MCO2 divider
        #endif


        #define CHIP_CLK_MCO2_SRC_ID CHIP_CLK_ID_VAL(CHIP_CFG_CLK_MCO2_SRC)
        #if CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_SYS_ID
            #define CHIP_RVAL_RCC_CFGR_MCO2SEL 1
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_HSI48_ID
            #if CHIP_DEV_SUPPORT_HSI48
                #define CHIP_RVAL_RCC_CFGR_MCO2SEL 2
            #else
                #error HSI48 is not supported on specified devive
            #endif
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_HSI16_ID
            #define CHIP_RVAL_RCC_CFGR_MCO2SEL 3
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_HSE_ID
            #define CHIP_RVAL_RCC_CFGR_MCO2SEL 4
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_PLLR_ID
            #define CHIP_RVAL_RCC_CFGR_MCO2SEL 5
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_LSI_ID
            #define CHIP_RVAL_RCC_CFGR_MCO2SEL 6
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_LSE_ID
            #define CHIP_RVAL_RCC_CFGR_MCO2SEL 7
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_PLLP_ID
            #if CHIP_DEV_SUPPORT_MCO_CFG_EX
                #define CHIP_RVAL_RCC_CFGR_MCO2SEL 8
            #else
                #error unsupported MCO2 source for specified device
            #endif
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_PLLQ_ID
            #if CHIP_DEV_SUPPORT_MCO_CFG_EX
                #define CHIP_RVAL_RCC_CFGR_MCO2SEL 9
            #else
                #error unsupported MCO2 source for specified device
            #endif
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_RTC_ID
            #if CHIP_DEV_SUPPORT_MCO_CFG_EX
                #define CHIP_RVAL_RCC_CFGR_MCO2SEL 10
            #else
                #error unsupported MCO2 source for specified device
            #endif
        #elif CHIP_CLK_MCO2_SRC_ID == CHIP_CLK_RTC_WAKEUP_ID
            #if CHIP_DEV_SUPPORT_MCO_CFG_EX
                #define CHIP_RVAL_RCC_CFGR_MCO2SEL 11
            #else
                #error unsupported MCO2 source for specified device
            #endif
        #else
            #error Invalid MCO2 clock source
        #endif

        #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_MCO2_SRC)
            #error Source clock for MCO2 is not enabled
        #endif
    #else
        #define CHIP_RVAL_RCC_CFGR_MCO2PRE 0
        #define CHIP_RVAL_RCC_CFGR_MCO2SEL 0
    #endif
#else
    #if defined CHIP_CFG_CLK_MCO2_EN && CHIP_CFG_CLK_MCO2_EN
        #error MCO2 is not supported on specified device
    #endif
#endif



/* ----------------- Настройки источников частот периферии -------------------*/
#if CHIP_CLK_USART1_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART1_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_USART1_KER_SRC)
    #if CHIP_CLK_USART1_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_USART1SEL   0
    #elif CHIP_CLK_USART1_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_USART1SEL   1
    #elif CHIP_CLK_USART1_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_USART1SEL   2
    #elif CHIP_CLK_USART1_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_USART1SEL 3
    #else
        #error Invalid USART1 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_USART1_KER_SRC)
        #error Source clock for USART1 is not enabled
    #endif
#endif

#if CHIP_CLK_USART2_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART2_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_USART2_KER_SRC)
    #if CHIP_CLK_USART2_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_USART2SEL   0
    #elif CHIP_CLK_USART2_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_USART2SEL   1
    #elif CHIP_CLK_USART2_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_USART2SEL   2
    #elif CHIP_CLK_USART2_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_USART2SEL 3
    #else
        #error Invalid USART2 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_USART2_KER_SRC)
        #error Source clock for USART2 is not enabled
    #endif
#endif


#if CHIP_CLK_USART3_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART3_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_USART3_KER_SRC)
    #if CHIP_CLK_USART3_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_USART3SEL   0
    #elif CHIP_CLK_USART3_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_USART3SEL   1
    #elif CHIP_CLK_USART3_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_USART3SEL   2
    #elif CHIP_CLK_USART3_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_USART3SEL 3
    #else
        #error Invalid USART3 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_USART3_KER_SRC)
        #error Source clock for USART3 is not enabled
    #endif
#endif


#if CHIP_CLK_CEC_KER_SRC_SETUP_EN
    #define CHIP_CLK_CEC_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_CEC_KER_SRC)
    #if CHIP_CLK_CEC_KER_SRC_ID == CHIP_CLK_HSI16_DIV488_ID
        #define CHIP_RVAL_RCC_CCIPR_CECSEL   0
    #elif CHIP_CLK_CEC_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_CECSEL   1
    #else
        #error Invalid CEC clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_CEC_KER_SRC)
        #error Source clock for CEC is not enabled
    #endif
#endif


#if CHIP_CLK_LPUART1_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPUART1_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_LPUART1_KER_SRC)
    #if CHIP_CLK_LPUART1_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART1SEL   0
    #elif CHIP_CLK_LPUART1_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART1SEL   1
    #elif CHIP_CLK_LPUART1_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART1SEL   2
    #elif CHIP_CLK_LPUART1_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART1SEL 3
    #else
        #error Invalid LPUART1 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_LPUART1_KER_SRC)
        #error Source clock for LPUART1 is not enabled
    #endif
#endif


#if CHIP_CLK_LPUART2_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPUART2_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_LPUART2_KER_SRC)
    #if CHIP_CLK_LPUART2_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART2SEL   0
    #elif CHIP_CLK_LPUART2_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART2SEL   1
    #elif CHIP_CLK_LPUART2_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART2SEL   2
    #elif CHIP_CLK_LPUART2_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_LPUART2SEL 3
    #else
        #error Invalid LPUART2 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_LPUART2_KER_SRC)
        #error Source clock for LPUART2 is not enabled
    #endif
#endif



#if CHIP_CLK_I2C1_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C1_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_I2C1_KER_SRC)
    #if CHIP_CLK_I2C1_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_I2C1SEL   0
    #elif CHIP_CLK_I2C1_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_I2C1SEL   1
    #elif CHIP_CLK_I2C1_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_I2C1SEL   2
    #else
        #error Invalid I2C1 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_I2C1_KER_SRC)
        #error Source clock for I2C1 is not enabled
    #endif
#endif

#if CHIP_CLK_I2C2_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C2_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_I2C2_KER_SRC)
    #if CHIP_CLK_I2C2_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_I2C2SEL   0
    #elif CHIP_CLK_I2C2_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_I2C2SEL   1
    #elif CHIP_CLK_I2C2_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_I2C2SEL   2
    #else
        #error Invalid I2C2 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_I2C2_KER_SRC)
        #error Source clock for I2C2 is not enabled
    #endif
#endif

#if CHIP_CLK_LPTIM1_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM1_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_LPTIM1_KER_SRC)
    #if CHIP_CLK_LPTIM1_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM1SEL   0
    #elif CHIP_CLK_LPTIM1_KER_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM1SEL   1
    #elif CHIP_CLK_LPTIM1_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM1SEL   2
    #elif CHIP_CLK_LPTIM1_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM1SEL   3
    #else
        #error Invalid LPTIM1 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_LPTIM1_KER_SRC)
        #error Source clock for LPTIM1 is not enabled
    #endif
#endif

#if CHIP_CLK_LPTIM2_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM2_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_LPTIM2_KER_SRC)
    #if CHIP_CLK_LPTIM2_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM2SEL   0
    #elif CHIP_CLK_LPTIM2_KER_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM2SEL   1
    #elif CHIP_CLK_LPTIM2_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM2SEL   2
    #elif CHIP_CLK_LPTIM2_KER_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CCIPR_LPTIM2SEL   3
    #else
        #error Invalid LPTIM2 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_LPTIM2_KER_SRC)
        #error Source clock for LPTIM2 is not enabled
    #endif
#endif

#if CHIP_CLK_TIM1_KER_SRC_SETUP_EN
    #define CHIP_CLK_TIM1_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_TIM1_KER_SRC)
    #if CHIP_CLK_TIM1_KER_SRC_ID == CHIP_CLK_TIMP_ID
        #define CHIP_RVAL_RCC_CCIPR_TIM1SEL   0
    #elif CHIP_CLK_TIM1_KER_SRC_ID == CHIP_CLK_PLLQ_ID
        #define CHIP_RVAL_RCC_CCIPR_TIM1SEL   1
    #else
        #error Invalid TIM1 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_TIM1_KER_SRC)
        #error Source clock for TIM1 is not enabled
    #endif
#endif

#if CHIP_CLK_TIM15_KER_SRC_SETUP_EN
    #define CHIP_CLK_TIM15_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_TIM15_KER_SRC)
    #if CHIP_CLK_TIM15_KER_SRC_ID == CHIP_CLK_TIMP_ID
        #define CHIP_RVAL_RCC_CCIPR_TIM15SEL   0
    #elif CHIP_CLK_TIM15_KER_SRC_ID == CHIP_CLK_PLLQ_ID
        #define CHIP_RVAL_RCC_CCIPR_TIM15SEL   1
    #else
        #error Invalid TIM15 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_TIM15_KER_SRC)
        #error Source clock for TIM15 is not enabled
    #endif
#endif


#if CHIP_CLK_RNG_KER_SRC_SETUP_EN
    #define CHIP_CLK_RNG_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_RNG_KER_SRC)
    #if CHIP_CLK_RNG_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_RNGSEL   1
    #elif CHIP_CLK_RNG_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_RNGSEL   2
    #elif CHIP_CLK_RNG_KER_SRC_ID == CHIP_CLK_PLLQ_ID
        #define CHIP_RVAL_RCC_CCIPR_RNGSEL   3
    #else
        #error Invalid RNG clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_RNG_KER_SRC)
        #error Source clock for RNG is not enabled
    #endif

    #if !defined CHIP_CFG_CLK_RNG_DIV
        #error RNG clock divider is not defined
    #else
        #if CHIP_CFG_CLK_RNG_DIV == 1
            #define CHIP_RVAL_RCC_CCIPR_RNGDIV   0
        #elif CHIP_CFG_CLK_RNG_DIV == 2
            #define CHIP_RVAL_RCC_CCIPR_RNGDIV   1
        #elif CHIP_CFG_CLK_RNG_DIV == 4
            #define CHIP_RVAL_RCC_CCIPR_RNGDIV   2
        #elif CHIP_CFG_CLK_RNG_DIV == 8
            #define CHIP_RVAL_RCC_CCIPR_RNGDIV   3
        #else
            #error Invalid RNG clock divider value
        #endif
    #endif
#endif


#if CHIP_CLK_ADC_KER_SRC_SETUP_EN
    #define CHIP_CLK_ADC_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_ADC_KER_SRC)
    #if CHIP_CLK_ADC_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_ADCSEL   0
    #elif CHIP_CLK_ADC_KER_SRC_ID == CHIP_CLK_PLLP_ID
        #define CHIP_RVAL_RCC_CCIPR_ADCSEL   1
    #elif CHIP_CLK_ADC_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_ADCSEL   2
    #else
        #error Invalid ADC clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_ADC_KER_SRC)
        #error Source clock for ADC is not enabled
    #endif
#endif


#if CHIP_CLK_I2S1_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2S1_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_I2S1_KER_SRC)
    #if CHIP_CLK_I2S1_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S1SEL   0
    #elif CHIP_CLK_I2S1_KER_SRC_ID == CHIP_CLK_PLLP_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S1SEL   1
    #elif CHIP_CLK_I2S1_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S1SEL   2
    #elif CHIP_CLK_I2S1_KER_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S1SEL   3
    #else
        #error Invalid I2S1 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_I2S1_KER_SRC)
        #error Source clock for I2S1 is not enabled
    #endif
#endif

#if CHIP_CLK_I2S2_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2S2_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_I2S2_KER_SRC)
    #if CHIP_CLK_I2S2_KER_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S2SEL   0
    #elif CHIP_CLK_I2S2_KER_SRC_ID == CHIP_CLK_PLLP_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S2SEL   1
    #elif CHIP_CLK_I2S2_KER_SRC_ID == CHIP_CLK_HSI16_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S2SEL   2
    #elif CHIP_CLK_I2S2_KER_SRC_ID == CHIP_CLK_I2S_CKIN_ID
        #define CHIP_RVAL_RCC_CCIPR_I2S2SEL   3
    #else
        #error Invalid I2S2 clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_I2S2_KER_SRC)
        #error Source clock for I2S2 is not enabled
    #endif
#endif

#if CHIP_CLK_FDCAN_KER_SRC_SETUP_EN
    #define CHIP_CLK_FDCAN_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_FDCAN_KER_SRC)
    #if CHIP_CLK_FDCAN_KER_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CCIPR_FDCANSEL   0
    #elif CHIP_CLK_FDCAN_KER_SRC_ID == CHIP_CLK_PLLQ_ID
        #define CHIP_RVAL_RCC_CCIPR_FDCANSEL   1
    #elif CHIP_CLK_FDCAN_KER_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_CCIPR_FDCANSEL   2
    #else
        #error Invalid FDCAN clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_FDCAN_KER_SRC)
        #error Source clock for FDCAN is not enabled
    #endif
#endif

#if CHIP_CLK_USB_KER_SRC_SETUP_EN
    #define CHIP_CLK_USB_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_USB_KER_SRC)
    #if CHIP_CLK_USB_KER_SRC_ID == CHIP_CLK_HSI48_ID
        #define CHIP_RVAL_RCC_CCIPR_USBSEL   0
    #elif CHIP_CLK_USB_KER_SRC_ID == CHIP_CLK_PLLQ_ID
        #define CHIP_RVAL_RCC_CCIPR_USBSEL   1
    #elif CHIP_CLK_USB_KER_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_CCIPR_USBSEL   2
    #else
        #error Invalid USB clock souce
    #endif

    #if !CHIP_CLK_ID_EN(CHIP_CFG_CLK_USB_KER_SRC)
        #error Source clock for USB is not enabled
    #endif
#endif








void chip_clk_init(void) {
    /* включение LSI либо если разрешен явно, либо если требуется настройка IWDG.
     * Т.к. IWDG следует разрешить как можно раньше, включаем этот клок до
     * настройки остального */
#if CHIP_CLK_LSI_EN
    CHIP_REGS_RCC->CSR |= CHIP_REGFLD_RCC_CSR_LSION;
#endif

#if CHIP_WDT_SETUP_EN
    /* wdt настраиваем как можно раньше, но нужно убедиться, что его клок уже
     * готов */
    while ((CHIP_REGS_RCC->CSR & CHIP_REGFLD_RCC_CSR_LSIRDY) != CHIP_REGFLD_RCC_CSR_LSIRDY) {
        continue;
    }
    chip_wdt_init();
#endif


    /* Если процессор работает не от HSI, то переходим в режим по умолчанию:
     * включаем HSI и ждем готовности, переводим процессор на работу от него */
    if (LBITFIELD_GET(CHIP_REGS_RCC->CFGR,  CHIP_REGFLD_RCC_CFGR_SWS) != CHIP_REGFLDVAL_RCC_CFGR_SW_HSISYS) {
        LBITFIELD_UPD(CHIP_REGS_RCC->CR, CHIP_REGFLD_RCC_CR_HSIDIV, CHIP_RVAL_RCC_CR_HSIDIV);
        CHIP_REGS_RCC->CR |= CHIP_REGFLD_RCC_CR_HSION;

        while ((CHIP_REGS_RCC->CR & CHIP_REGFLD_RCC_CR_HSIRDY) != CHIP_REGFLD_RCC_CR_HSIRDY)
            continue;

        LBITFIELD_UPD(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_SW, CHIP_REGFLDVAL_RCC_CFGR_SW_HSISYS);
    }


    chip_flash_init_freq();


    CHIP_REGS_RCC->CR = (CHIP_REGS_RCC->CR & 0xFC30C1FF)
            | CHIP_REGFLD_RCC_CR_HSION
        #if CHIP_CFG_CLK_HSI16_ON_STOP_EN
            | CHIP_REGFLD_RCC_CR_HSIKERON
        #endif
        #if CHIP_CFG_CLK_HSI16_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CR_HSIDIV, CHIP_RVAL_RCC_CR_HSIDIV)
        #endif
        #if CHIP_CLK_HSE_EN
            | CHIP_REGFLD_RCC_CR_HSEON
        #if CHIP_CFG_CLK_HSE_MODE == CHIP_CLK_EXTMODE_CLOCK
            | CHIP_REGFLD_RCC_CR_HSEBYP
        #endif
        #if CHIP_CFG_CLK_HSE_CSS_EN
            | CHIP_REGFLD_RCC_CR_CSSON
        #endif
        #endif
        #if CHIP_CLK_HSI48_EN
            | CHIP_REGFLD_RCC_CR_HSI48_EN
        #endif
            ;


#if CHIP_CLK_BD_CFG_SETUP_EN
    /** настройка RTC и LSE */
    const uint32_t bd_msk = 0
        #if CHIP_CFG_CLK_RTC_SETUP_EN
            | CHIP_REGFLD_RCC_BDCR_RTCEN
            | CHIP_REGFLD_RCC_BDCR_RTCSEL
        #endif
        #if CHIP_CFG_CLK_LSE_SETUP_EN
            | CHIP_REGFLD_RCC_BDCR_LSEON
            | CHIP_REGFLD_RCC_BDCR_LSEBYP
            | CHIP_REGFLD_RCC_BDCR_LSECSSON
            | CHIP_REGFLD_RCC_BDCR_LSEDRV
        #endif
        #if CHIP_CFG_CLK_LSCO_SETUP_EN
            | CHIP_REGFLD_RCC_BDCR_LSCOEN
            | CHIP_REGFLD_RCC_BDCR_LSCOSEL
        #endif
            ;

    const uint32_t bd_cfg = 0
        #if CHIP_CFG_CLK_RTC_SETUP_EN
            #if CHIP_CFG_CLK_RTC_EN
                | CHIP_REGFLD_RCC_BDCR_RTCEN
            #endif
                | LBITFIELD_SET(CHIP_REGFLD_RCC_BDCR_RTCSEL, CHIP_RVAL_RCC_BDCR_RTCSEL)
        #endif
        #if CHIP_CFG_CLK_LSE_SETUP_EN
            #if CHIP_CLK_LSE_EN
                | CHIP_REGFLD_RCC_BDCR_LSEON
                #if CHIP_CFG_CLK_LSE_MODE == CHIP_CLK_EXTMODE_CLOCK
                    | CHIP_REGFLD_RCC_BDCR_LSEBYP
                #endif
                #if CHIP_CFG_CLK_LSE_CSS_EN
                    | CHIP_REGFLD_RCC_BDCR_LSECSSON
                #endif
            #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_BDCR_LSEDRV, CHIP_RVAL_RCC_BDCR_LSEDRV)
        #endif
        #if CHIP_CFG_CLK_LSCO_SETUP_EN
            #if CHIP_CFG_CLK_LSCO_EN
                | CHIP_REGFLD_RCC_BDCR_LSCOEN
            #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_BDCR_LSCOSEL, CHIP_RVAL_RCC_BDCR_LSCOSEL)
        #endif
            ;

    if ((CHIP_REGS_RCC->BDCR & bd_msk) != bd_cfg) {

        chip_pwr_bd_cfg_write_enable();
        CHIP_REGS_RCC->BDCR = (CHIP_REGS_RCC->BDCR & ~bd_msk) | bd_cfg;
        chip_pwr_bd_cfg_write_disable();
    }
#endif

    /* ожидание установки всех базовых клоков */
    if (CHIP_RCC_CR_CLK_WTRDY_MSK != 0)  {
        while ((CHIP_REGS_RCC->CR & CHIP_RCC_CR_CLK_WTRDY_MSK) != CHIP_RCC_CR_CLK_WTRDY_MSK) {
            continue;
        }
    }


#if CHIP_CFG_CLK_LSE_SETUP_EN && CHIP_CLK_LSE_EN
#if CHIP_CFG_CLK_LSE_RDY_TOUT_MS >= 0
    int cntr = 0;
#endif
    while ((CHIP_REGS_RCC->BDCR & CHIP_REGFLD_RCC_BDCR_LSERDY) != CHIP_REGFLD_RCC_BDCR_LSERDY) {
#if CHIP_CFG_CLK_LSE_RDY_TOUT_MS >= 0
        if (cntr >= CHIP_CFG_CLK_LSE_RDY_TOUT_MS)
            break;
        chip_delay_clk(CHIP_CLK_HSI16_FREQ/1000);
        cntr++;
#endif
        continue;
    }
#endif


#if CHIP_CFG_CLK_PLL_EN
    /* настраиваем параметры PLL с запретом выходов P/Q/R */
    CHIP_REGS_RCC->PLLCFGR = (CHIP_REGS_RCC->PLLCFGR & 0x00C0808C)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLLSRC, CHIP_RVAL_RCC_PLLCFGR_PLLSRC)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLLM, CHIP_RVAL_RCC_PLLCFGR_PLLM)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLLN, CHIP_RVAL_RCC_PLLCFGR_PLLN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLLP, CHIP_RVAL_RCC_PLLCFGR_PLLP)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLLQ, CHIP_RVAL_RCC_PLLCFGR_PLLQ)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_PLLCFGR_PLLR, CHIP_RVAL_RCC_PLLCFGR_PLLR)
            ;
    /* разрешаем PLL в CR */
    CHIP_REGS_RCC->CR |= CHIP_REGFLD_RCC_CR_PLLON;
    /* ожидание готовности PLL */
    while (!(CHIP_REGS_RCC->CR & CHIP_REGFLD_RCC_CR_PLLRDY)) {
        continue;
    }
    /* разрешение выходов PLL */
    CHIP_REGS_RCC->PLLCFGR |= 0
        #if CHIP_CFG_CLK_PLL_DIVP_EN
            | CHIP_REGFLD_RCC_PLLCFGR_PLLPEN
        #endif
        #if CHIP_CFG_CLK_PLL_DIVQ_EN
            | CHIP_REGFLD_RCC_PLLCFGR_PLLQEN
        #endif
        #if CHIP_CFG_CLK_PLL_DIVR_EN
            | CHIP_REGFLD_RCC_PLLCFGR_PLLREN
        #endif
            ;
#endif

    CHIP_REGS_RCC->CFGR = (CHIP_REGS_RCC->CFGR & 0x000000C0)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_SW, CHIP_RVAL_RCC_CFGR_SW)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_HPRE, CHIP_RVAL_RCC_CFGR_HPRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PPRE, CHIP_RVAL_RCC_CFGR_PPRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCOPRE, CHIP_RVAL_RCC_CFGR_MCOPRE)
#if CHIP_DEV_SUPPORT_MC02
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO2PRE, CHIP_RVAL_RCC_CFGR_MCO2PRE)
#endif
            ;



    /* Если HSI не требуется, то его отключение можно выполнить как только
     * будет перенастроена частота CPU и шин, которые работают от HSI по
     * умолчанию, на нужный источник частоты */
#if !CHIP_CFG_CLK_HSI16_EN
    CHIP_REGS_RCC->CR &= ~CHIP_REGFLD_RCC_CR_HSION;
#endif

    /* выбор источников клоков для периферии */
    CHIP_REGS_RCC->CCIPR = (CHIP_REGS_RCC->CCIPR & 0x02830080)
        #if CHIP_CLK_USART1_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_USART1SEL,  CHIP_RVAL_RCC_CCIPR_USART1SEL)
        #endif
        #if CHIP_CLK_USART2_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_USART2SEL,  CHIP_RVAL_RCC_CCIPR_USART2SEL)
        #endif
        #if CHIP_CLK_USART3_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_USART3SEL,  CHIP_RVAL_RCC_CCIPR_USART3SEL)
        #endif
        #if CHIP_CLK_CEC_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_CECSEL,     CHIP_RVAL_RCC_CCIPR_CECSEL)
        #endif
        #if CHIP_CLK_LPUART1_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_LPUART1SEL, CHIP_RVAL_RCC_CCIPR_LPUART1SEL)
        #endif
        #if CHIP_CLK_LPUART2_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_LPUART2SEL, CHIP_RVAL_RCC_CCIPR_LPUART2SEL)
        #endif
        #if CHIP_CLK_I2C1_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_I2C1SEL, CHIP_RVAL_RCC_CCIPR_I2C1SEL)
        #endif
        /* если устройство поддерживает настройку клока I2C, то данное поле отвечает за нее,
         * а клок I2S1 настраивается в регистре CCIPR2, иначе это поле отвечает за клок I2S1 */
        #if CHIP_DEV_SUPPORT_I2C2_ICLKSEL
            #if CHIP_CLK_I2C2_KER_SRC_SETUP_EN
                | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_I2C2SEL, CHIP_RVAL_RCC_CCIPR_I2C2SEL)
            #endif
        #else
            #if CHIP_CLK_I2S1_KER_SRC_SETUP_EN
                | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_I2S1SEL, CHIP_RVAL_RCC_CCIPR_I2S1SEL)
            #endif
        #endif
        #if CHIP_CLK_LPTIM1_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_LPTIM1SEL, CHIP_RVAL_RCC_CCIPR_LPTIM1SEL)
        #endif
        #if CHIP_CLK_LPTIM2_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_LPTIM2SEL, CHIP_RVAL_RCC_CCIPR_LPTIM2SEL)
        #endif
        #if CHIP_CLK_TIM1_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_TIM1SEL, CHIP_RVAL_RCC_CCIPR_TIM1SEL)
        #endif
        #if CHIP_CLK_TIM15_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_TIM15SEL, CHIP_RVAL_RCC_CCIPR_TIM15SEL)
        #endif
        #if CHIP_CLK_RNG_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_RNGSEL, CHIP_RVAL_RCC_CCIPR_RNGSEL)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_RNGDIV, CHIP_RVAL_RCC_CCIPR_RNGDIV)
        #endif
        #if CHIP_CLK_ADC_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR_ADCSEL, CHIP_RVAL_RCC_CCIPR_ADCSEL)
        #endif

            ;

    /* регистр CCIPR2 реализован не на всех устройствах, поэтому если нет поддержки, то к нему не обращаемся */
#if CHIP_DEV_SUPPORT_I2C2_ICLKSEL || CHIP_DEV_SUPPORT_I2S2 || CHIP_DEV_SUPPORT_FDCAN || CHIP_DEV_SUPPORT_USB
    CHIP_REGS_RCC->CCIPR2 = (CHIP_REGS_RCC->CCIPR2 & 0xFFFFCCF0)
        #if CHIP_DEV_SUPPORT_I2C2_ICLKSEL
            #if CHIP_CLK_I2S1_KER_SRC_SETUP_EN
                | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR2_I2S1SEL, CHIP_RVAL_RCC_CCIPR_I2S1SEL)
            #endif
        #endif
        #if CHIP_CLK_I2S2_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR2_I2S2SEL, CHIP_RVAL_RCC_CCIPR_I2S2SEL)
        #endif
        #if CHIP_CLK_FDCAN_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR2_FDCANSEL, CHIP_RVAL_RCC_CCIPR_FDCANSEL)
        #endif
        #if CHIP_CLK_USB_KER_SRC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CCIPR2_USBSEL, CHIP_RVAL_RCC_CCIPR_USBSEL)
        #endif
            ;
#endif


    /* включение клоков периферии */
    CHIP_REGS_RCC->IOPENR = (CHIP_REGS_RCC->IOPENR & 0xFFFFFFC0)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_IOPENR_GPIOAEN,     !!CHIP_CFG_CLK_GPIOA_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_IOPENR_GPIOBEN,     !!CHIP_CFG_CLK_GPIOB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_IOPENR_GPIOCEN,     !!CHIP_CFG_CLK_GPIOC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_IOPENR_GPIODEN,     !!CHIP_CFG_CLK_GPIOD_EN)
        #if CHIP_DEV_SUPPORT_PORTE
            | LBITFIELD_SET(CHIP_REGFLD_RCC_IOPENR_GPIOEEN,     !!CHIP_CFG_CLK_GPIOE_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_IOPENR_GPIOFEN,     !!CHIP_CFG_CLK_GPIOF_EN)
            ;

    CHIP_REGS_RCC->AHBENR = (CHIP_REGS_RCC->AHBENR & 0xFFFAEEFC)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_DMA1EN,      !!CHIP_CFG_CLK_DMA1_EN)
        #if CHIP_DEV_SUPPORT_DMA2
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_DMA2EN,      !!CHIP_CFG_CLK_DMA2_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_FLASHEN,     !!CHIP_CFG_CLK_FLASH_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_CRCEN,       !!CHIP_CFG_CLK_CRC_EN)
        #if CHIP_DEV_SUPPORT_AES
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_AESEN,       !!CHIP_CFG_CLK_AES_EN)
        #endif
        #if CHIP_DEV_SUPPORT_RNG
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_RNGEN,       !!CHIP_CFG_CLK_RNG_EN)
        #endif
            ;

    CHIP_REGS_RCC->APBENR1 = (CHIP_REGS_RCC->APBENR1 & 0x00000048)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_TIM2EN,     !!CHIP_CFG_CLK_TIM2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_TIM3EN,     !!CHIP_CFG_CLK_TIM3_EN)
        #if CHIP_DEV_SUPPORT_TIM4
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_TIM4EN,     !!CHIP_CFG_CLK_TIM4_EN)
        #endif
        #if CHIP_DEV_SUPPORT_TIM6
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_TIM6EN,     !!CHIP_CFG_CLK_TIM6_EN)
        #endif
        #if CHIP_DEV_SUPPORT_TIM7
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_TIM7EN,     !!CHIP_CFG_CLK_TIM7_EN)
        #endif
        #if CHIP_DEV_SUPPORT_LPUART2
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_LPUART2EN,  !!CHIP_CFG_CLK_LPUART2_EN)
        #endif
        #if CHIP_DEV_SUPPORT_USART5
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_USART5EN,   !!CHIP_CFG_CLK_USART5_EN)
        #endif
        #if CHIP_DEV_SUPPORT_USART6
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_USART6EN,   !!CHIP_CFG_CLK_USART6_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_RTCAPBEN,   !!CHIP_CFG_CLK_RTCAPB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_WWDGEN,     !!CHIP_CFG_CLK_WWDG_EN)
        #if CHIP_DEV_SUPPORT_FDCAN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_FDCANEN,    !!CHIP_CFG_CLK_FDCAN_EN)
        #endif
        #if CHIP_DEV_SUPPORT_USB
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_USBEN,      !!CHIP_CFG_CLK_USB_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_SPI2EN,     !!CHIP_CFG_CLK_SPI2_EN)
        #if CHIP_DEV_SUPPORT_SPI3
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_SPI3EN,     !!CHIP_CFG_CLK_SPI3_EN)
        #endif
        #if CHIP_DEV_SUPPORT_CRS
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_CRSEN,      !!CHIP_CFG_CLK_CRS_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_USART2EN,   !!CHIP_CFG_CLK_USART2_EN)
        #if CHIP_DEV_SUPPORT_USART3
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_USART3EN,   !!CHIP_CFG_CLK_USART3_EN)
        #endif
        #if CHIP_DEV_SUPPORT_USART4
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_USART4EN,   !!CHIP_CFG_CLK_USART4_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_LPUART1EN,  !!CHIP_CFG_CLK_LPUART1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_I2C1EN,     !!CHIP_CFG_CLK_I2C1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_I2C2EN,     !!CHIP_CFG_CLK_I2C2_EN)
        #if CHIP_DEV_SUPPORT_I2C3
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_I2C3EN,     !!CHIP_CFG_CLK_I2C3_EN)
        #endif
        #if CHIP_DEV_SUPPORT_CEC
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_CECEN,      !!CHIP_CFG_CLK_CEC_EN)
        #endif
        #if CHIP_DEV_SUPPORT_UCPD
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_UCPD1EN,    !!CHIP_CFG_CLK_UCPD1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_UCPD2EN,    !!CHIP_CFG_CLK_UCPD2_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_DBGEN,      !!CHIP_CFG_CLK_DBG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_PWREN,      !!CHIP_CFG_CLK_PWR_EN)
        #if CHIP_DEV_SUPPORT_DAC
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_DAC1EN,     !!CHIP_CFG_CLK_DAC1_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_LPTIM2EN,   !!CHIP_CFG_CLK_LPTIM2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR1_LPTIM1EN,   !!CHIP_CFG_CLK_LPTIM1_EN)
            ;

    CHIP_REGS_RCC->APBENR2 = (CHIP_REGS_RCC->APBENR2 & 0xFFE827FE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_SYSCFGEN,   !!CHIP_CFG_CLK_SYSCFG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_TIM1EN,     !!CHIP_CFG_CLK_TIM1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_SPI1EN,     !!CHIP_CFG_CLK_SPI1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_USART1EN,   !!CHIP_CFG_CLK_USART1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_TIM14EN,    !!CHIP_CFG_CLK_TIM14_EN)
        #if CHIP_DEV_SUPPORT_TIM15
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_TIM15EN,    !!CHIP_CFG_CLK_TIM15_EN)
        #endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_TIM16EN,    !!CHIP_CFG_CLK_TIM16_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_TIM17EN,    !!CHIP_CFG_CLK_TIM17_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APBENR2_ADCEN,      !!CHIP_CFG_CLK_ADC_EN)
            ;







#if CHIP_CFG_CLK_MCO_EN
    LBITFIELD_UPD(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_MCOSEL, CHIP_RVAL_RCC_CFGR_MCOSEL);
#ifdef CHIP_CFG_CLK_MCO_PIN
    CHIP_PIN_CONFIG(CHIP_CFG_CLK_MCO_PIN);
#endif
#endif

#if CHIP_DEV_SUPPORT_MC02
    #if CHIP_CFG_CLK_MCO2_EN
        LBITFIELD_UPD(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_MCO2SEL, CHIP_RVAL_RCC_CFGR_MCO2SEL);
        #ifdef CHIP_CFG_CLK_MCO2_PIN
            CHIP_PIN_CONFIG(CHIP_CFG_CLK_MCO2_PIN);
        #endif
    #endif
#endif

#if CHIP_CFG_LTIMER_EN
    /* разрешение ltimer */
    lclock_init();
#endif
}
