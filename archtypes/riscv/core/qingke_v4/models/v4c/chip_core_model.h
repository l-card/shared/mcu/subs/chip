#ifndef CHIP_CORE_MODEL_H
#define CHIP_CORE_MODEL_H

#define CHIP_CORE_MODEL                             CHIP_CORE_MODEL_QINGKE_V4C

#define CHIP_CORE_MODEL_SUPPORT_FPU                 0 /* support hard floating point */

#define CHIP_CORE_MODEL_SUPPORT_INT_PREEMPT         0 /* int preemption config support */
#define CHIP_CORE_MODEL_INT_HPE_LVL_CNT             2 /* hardware stack levels */
#define CHIP_CORE_MODEL_INT_NEST_LVL_CNT            2 /* int nesting levels */
#define CHIP_CORE_MODEL_INT_VTF_CH_CNT              4 /* table free int cnt */
#define CHIP_CORE_MODEL_PMP_REG_CNT                 4 /* protection memory regions */


#endif // CHIP_CORE_MODEL_H
