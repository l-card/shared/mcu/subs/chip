#ifndef CHIP_CMSIS_CFG_H
#define CHIP_CMSIS_CFG_H

#define __MPU_PRESENT             1       /*!< HC32F4A0 provides MPU                            */
#define __VTOR_PRESENT            1       /*!< HC32F4A0 supported vector table registers            */
#define __NVIC_PRIO_BITS          4       /*!< HC32F4A0 uses 4 Bits for the Priority Levels         */
#define __Vendor_SysTickConfig    0       /*!< Set to 1 if different SysTick Config is used              */
#define __FPU_PRESENT             1       /*!< FPU present                                               */                     */


#endif /*CHIP_CMSIS_CFG_H*/
