#ifndef CHIP_WDT_H
#define CHIP_WDT_H

#define CHIP_WDT_REGS       CHIP_REGS_IWDG
#define CHIP_WDT_CLK_FREQ   CHIP_CLK_IWDG_FREQ

#include "stm32_iwdg.h"


#endif // CHIP_WDT_H
