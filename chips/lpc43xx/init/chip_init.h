/*================================================================================================*
 * I/O registers and chip configuration for LPC43xx
 *================================================================================================*/

#ifndef LPC_H_
#define LPC_H_

#include "lpc_config.h"

/*================================================================================================*/
/* Селекторы тактовых генераторов */
#define LPC_CLKSEL_RTC      0               /* 32 kHz oscillator */
#define LPC_CLKSEL_IRC      1               /* 12 MHz internal RC oscillator */
#define LPC_CLKSEL_ENET_RX  2               /* Ethernet RX clock */
#define LPC_CLKSEL_ENET_TX  3               /* Ethernet TX clock */
#define LPC_CLKSEL_GPCLKIN  4               /* General purpose CLKIN */
#define LPC_CLKSEL_OSC      6               /* Crystal oscillator */
#define LPC_CLKSEL_PLL0USB  7               /* PLL0USB */
#define LPC_CLKSEL_PLL0AUD  8               /* PLL0AUDIO */
#define LPC_CLKSEL_PLL1     9               /* PLL1 (main PLL) */
#define LPC_CLKSEL_IDIVA    0x0C            /* IDIVA (div 1..4) */
#define LPC_CLKSEL_IDIVB    0x0D            /* IDIVB (div 1..16) */
#define LPC_CLKSEL_IDIVC    0x0E            /* IDIVC (div 1..16) */
#define LPC_CLKSEL_IDIVD    0x0F            /* IDIVD (div 1..16) */
#define LPC_CLKSEL_IDIVE    0x10            /* IDIVE (div 1..256) */
/* Правила соединения генераторов между собой:
 * - без петель, в т.ч. PLL0AUDIO и PLL0USB не могут быть источниками друг для друга;
 * - IDIVA может быть источником для IDIVB..E, другие комбинации с IDIV недопустимы;
 * - PLL0USB не может быть источником для IDIVB..E.
 * Правила соединения генераторов с узлами:
 * - BASE_SAFE_CLK всегда от IRC;
 * - BASE_USB0_CLK всегда от PLL0USB;
 * - от PLL0USB могут тактироваться только BASE_USBx_CLK, BASE_OUT_CLK, BASE_CGU_OUTx_CLK
 * - все остальное разрешено, по умолчанию все от IRC.
 */ 

/* регистр конфигурации IDIVx_CTRL, BASE_xxx_CTRL (для BASE div = enable = 0 или 1) */
#define LPC_CLKCFG(src, div, autoblock) \
    (((src) << 24) | (!!(autoblock) << 11) | ((((div) - 1) & (!(div) - 1) & 0xFF) << 2) | !(div))
/* регистр BASE_xxx_CTRL (без поля power-down) */
#define LPC_BASECFG(src, autoblock) \
    (((src) << 24) | (!!(autoblock) << 11))
/* выделение полей источника и делителя из конфигурации IDIVx_CTRL */
#define LPC_CLKCFG_SRC(cfg)    (((cfg) >> 24) & 0x1F)
#define LPC_CLKCFG_DIV(cfg)    ((((cfg) >> 2) & 0xFF) + 1)
/*================================================================================================*/

/*================================================================================================*
 * ---- Здесь включаются настройки, специфичные для конкретного прибора ----
 *================================================================================================*/

#include "lpc_config.h"

/*================================================================================================*
 * ---- Вычисление тактовых частот разных источников ----
 *================================================================================================*/

#if LPC_USE_PLL1
#if     LPC_PLL1_SRC ==     LPC_CLKSEL_RTC
#define LPC_CLKSRCF_PLL1    LPC_CLKF_RTC
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_IRC
#define LPC_CLKSRCF_PLL1    LPC_CLKF_IRC
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_ENET_TX
#define LPC_CLKSRCF_PLL1    LPC_CLKF_ENET_TX
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_ENET_RX
#define LPC_CLKSRCF_PLL1    LPC_CLKF_ENET_RX
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_GPCLKIN
#define LPC_CLKSRCF_PLL1    LPC_CLKF_GPCLKIN
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_OSC
#define LPC_CLKSRCF_PLL1    LPC_CLKF_OSC
//#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_PLL0USB
//#define LPC_CLKSRCF_PLL1    LPC_CLKF_PLL0USB
//#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_PLL0AUD
//#define LPC_CLKSRCF_PLL1    LPC_CLKF_PLL0AUD
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_IDIVA
#define LPC_CLKSRCF_PLL1    LPC_CLKF_IDIVA
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_IDIVB
#define LPC_CLKSRCF_PLL1    LPC_CLKF_IDIVB
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_IDIVC
#define LPC_CLKSRCF_PLL1    LPC_CLKF_IDIVC
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_IDIVD
#define LPC_CLKSRCF_PLL1    LPC_CLKF_IDIVD
#elif   LPC_PLL1_SRC ==     LPC_CLKSEL_IDIVE
#define LPC_CLKSRCF_PLL1    LPC_CLKF_IDIVE
#else
#error "Invalid clock source for PLL1"
#endif
#else
#define LPC_CLKSRCF_PLL1    0
#endif /* LPC_USE_PLL1 */

#if LPC_USE_PLL0USB
#if     LPC_PLL0USB_SRC ==  LPC_CLKSEL_RTC
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_RTC
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_IRC
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_IRC
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_ENET_TX
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_ENET_TX
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_ENET_RX
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_ENET_RX
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_GPCLKIN
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_GPCLKIN
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_OSC
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_OSC
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_IDIVA
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_IDIVA
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_IDIVB
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_IDIVB
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_IDIVC
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_IDIVC
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_IDIVD
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_IDIVD
#elif   LPC_PLL0USB_SRC ==  LPC_CLKSEL_IDIVE
#define LPC_CLKSRCF_PLL0USB LPC_CLKF_IDIVE
#else
#error "Invalid clock source for PLL0USB"
#endif
#else
#define LPC_CLKSRCF_PLL0USB 0
#endif /* LPC_USE_PLL0USB */

#if     LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_RTC
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_RTC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_IRC
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_IRC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_ENET_TX
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_ENET_TX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_ENET_RX
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_ENET_RX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_GPCLKIN
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_GPCLKIN
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_OSC
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_OSC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_PLL0USB
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_PLL0USB
//#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_PLL0AUD
//#define LPC_CLKSRCF_IDIVA   LPC_CLKF_PLL0AUD
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVA_CFG) == LPC_CLKSEL_PLL1
#define LPC_CLKSRCF_IDIVA   LPC_CLKF_PLL1
#else
#error "Invalid clock source for IDIVA"
#endif

#if     LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_RTC
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_RTC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_IRC
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_IRC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_ENET_TX
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_ENET_TX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_ENET_RX
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_ENET_RX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_GPCLKIN
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_GPCLKIN
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_OSC
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_OSC
//#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_PLL0AUD
//#define LPC_CLKSRCF_IDIVB   LPC_CLKF_PLL0AUD
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_PLL1
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_PLL1
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVB_CFG) == LPC_CLKSEL_IDIVA
#define LPC_CLKSRCF_IDIVB   LPC_CLKF_IDIVA
#else
#error "Invalid clock source for IDIVB"
#endif

#if     LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_RTC
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_RTC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_IRC
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_IRC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_ENET_TX
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_ENET_TX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_ENET_RX
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_ENET_RX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_GPCLKIN
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_GPCLKIN
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_OSC
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_OSC
//#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_PLL0AUD
//#define LPC_CLKSRCF_IDIVC   LPC_CLKF_PLL0AUD
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_PLL1
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_PLL1
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVC_CFG) == LPC_CLKSEL_IDIVA
#define LPC_CLKSRCF_IDIVC   LPC_CLKF_IDIVA
#else
#error "Invalid clock source for IDIVC"
#endif

#if     LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_RTC
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_RTC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_IRC
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_IRC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_ENET_TX
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_ENET_TX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_ENET_RX
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_ENET_RX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_GPCLKIN
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_GPCLKIN
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_OSC
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_OSC
//#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_PLL0AUD
//#define LPC_CLKSRCF_IDIVD   LPC_CLKF_PLL0AUD
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_PLL1
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_PLL1
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVD_CFG) == LPC_CLKSEL_IDIVA
#define LPC_CLKSRCF_IDIVD   LPC_CLKF_IDIVA
#else
#error "Invalid clock source for IDIVD"
#endif

#if     LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_RTC
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_RTC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_IRC
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_IRC
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_ENET_TX
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_ENET_TX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_ENET_RX
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_ENET_RX
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_GPCLKIN
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_GPCLKIN
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_OSC
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_OSC
//#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_PLL0AUD
//#define LPC_CLKSRCF_IDIVE   LPC_CLKF_PLL0AUD
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_PLL1
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_PLL1
#elif   LPC_CLKCFG_SRC(LPC_CLKDIVE_CFG) == LPC_CLKSEL_IDIVA
#define LPC_CLKSRCF_IDIVE   LPC_CLKF_IDIVA
#else
#error "Invalid clock source for IDIVE"
#endif

    /* фиксированные частоты */
#define LPC_CLKF_RTC        32768UL         /* Частота осциллятора RTC, Гц */
#define LPC_CLKF_IRC        12000000UL      /* Частота внутреннего RC-осциллятора, Гц */
#define LPC_CLKF_PLL0USB    480000000UL     /* Фиксированная частота PLL0USB */
    /* Ethernet: 25 MHz (MII) or 50 MHz (RMII) */
#define LPC_CLKF_ENET_RX    25000000UL
#define LPC_CLKF_ENET_TX    25000000UL
    /* PLL0 audio: NOT IMPLEMENTED YET */
#define LPC_CLKF_PLL0AUD    0UL
    /* частота PLL1 */
#define LPC_PLL1_FCCO       (LPC_CLKSRCF_PLL1 * LPC_PLL1_M / LPC_PLL1_N)
#define LPC_CLKF_PLL1       (LPC_PLL1_FCCO / LPC_PLL1_P)
    /* выходы делителей */
#define LPC_CLKF_IDIVA      (LPC_CLKSRCF_IDIVA / LPC_CLKCFG_DIV(LPC_CLKDIVA_CFG))
#define LPC_CLKF_IDIVB      (LPC_CLKSRCF_IDIVB / LPC_CLKCFG_DIV(LPC_CLKDIVB_CFG))
#define LPC_CLKF_IDIVC      (LPC_CLKSRCF_IDIVC / LPC_CLKCFG_DIV(LPC_CLKDIVC_CFG))
#define LPC_CLKF_IDIVD      (LPC_CLKSRCF_IDIVD / LPC_CLKCFG_DIV(LPC_CLKDIVD_CFG))
#define LPC_CLKF_IDIVE      (LPC_CLKSRCF_IDIVE / LPC_CLKCFG_DIV(LPC_CLKDIVE_CFG))

/*================================================================================================*
 * ---- Вычисление тактовых частот процессора и периферии ----
 *================================================================================================*/
#define LPC_CLKF_BASE(cfg)  ( \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_RTC) * LPC_CLKF_RTC + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_IRC) * LPC_CLKF_IRC + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_ENET_TX) * LPC_CLKF_ENET_TX + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_ENET_RX) * LPC_CLKF_ENET_RX + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_GPCLKIN) * LPC_CLKF_GPCLKIN + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_OSC) * LPC_CLKF_OSC + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_PLL0USB) * LPC_CLKF_PLL0USB + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_PLL0AUD) * LPC_CLKF_PLL0AUD + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_PLL1) * LPC_CLKF_PLL1 + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_IDIVA) * LPC_CLKF_IDIVA + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_IDIVB) * LPC_CLKF_IDIVB + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_IDIVC) * LPC_CLKF_IDIVC + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_IDIVD) * LPC_CLKF_IDIVD + \
    (LPC_CLKCFG_SRC(cfg) == LPC_CLKSEL_IDIVE) * LPC_CLKF_IDIVE \
    )

#define LPC_SYSCLK          LPC_CLKF_BASE(LPC_BASE_M4_CFG)

#ifdef LPC_CLK_EMC_DIV2
    #define LPC_EMCCLK      LPC_SYSCLK/2
#else
    #define LPC_EMCCLK      LPC_SYSCLK
#endif




#define LPC_CLK_ADC0        (LPC_CLKF_BASE(LPC_BASE_APB3_CFG) / !!LPC_CLKEN_ADC0)      
#define LPC_CLK_ADC1        (LPC_CLKF_BASE(LPC_BASE_APB3_CFG) / !!LPC_CLKEN_ADC1)      
#define LPC_CLK_ADCHS       (LPC_CLKF_BASE(LPC_BASE_ADCHS_CFG) / !!LPC_CLKEN_ADCHS)     
#define LPC_CLK_CAN0        (LPC_CLKF_BASE(LPC_BASE_APB3_CFG) / !!LPC_CLKEN_CAN0)      
#define LPC_CLK_CAN1        (LPC_CLKF_BASE(LPC_BASE_APB1_CFG) / !!LPC_CLKEN_CAN1)      
#define LPC_CLK_CGOUT0      (LPC_CLKF_BASE(LPC_BASE_CGOUT0_CFG) / !!LPC_CLKEN_CGOUT0)    
#define LPC_CLK_CGOUT1      (LPC_CLKF_BASE(LPC_BASE_CGOUT1_CFG) / !!LPC_CLKEN_CGOUT1)    
#define LPC_CLK_DAC         (LPC_CLKF_BASE(LPC_BASE_APB3_CFG) / !!LPC_CLKEN_DAC)       
#define LPC_CLK_DMA         (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_DMA)       
#define LPC_CLK_EEPROM      (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_EEPROM)    
#define LPC_CLK_EMC         (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_EMC)       
//#define LPC_CLK_ENET        (LPC_CLKF_BASE(LPC_BASE_PHY_RX_CFG) / !!LPC_CLKEN_ENET)      
#define LPC_CLK_I2C0        (LPC_CLKF_BASE(LPC_BASE_APB1_CFG) / !!LPC_CLKEN_I2C0)      
#define LPC_CLK_I2C1        (LPC_CLKF_BASE(LPC_BASE_APB3_CFG) / !!LPC_CLKEN_I2C1)      
#define LPC_CLK_I2S         (LPC_CLKF_BASE(LPC_BASE_AUDIO_CFG) / !!LPC_CLKEN_I2S)       
#define LPC_CLK_LCD         (LPC_CLKF_BASE(LPC_BASE_LCD_CFG) / !!LPC_CLKEN_LCD)       
#define LPC_CLK_M0APP       (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_M0APP)     
#define LPC_CLK_MCPWM       (LPC_CLKF_BASE(LPC_BASE_APB1_CFG) / !!LPC_CLKEN_MCPWM)     
#define LPC_CLK_OUT         (LPC_CLKF_BASE(LPC_BASE_OUT_CFG) / !!LPC_CLKEN_OUT)       
#define LPC_CLK_PERIPH      (LPC_CLKF_BASE(LPC_BASE_PERIPH_CFG) / !!LPC_CLKEN_PERIPH)    
#define LPC_CLK_QEI         (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_QEI)       
#define LPC_CLK_RITIMER     (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_RITIMER)   
#define LPC_CLK_SCT         (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_SCT)       
#define LPC_CLK_SSP0        (LPC_CLKF_BASE(LPC_BASE_SSP0_CFG) / !!LPC_CLKEN_SSP0)      
#define LPC_CLK_SSP1        (LPC_CLKF_BASE(LPC_BASE_SSP1_CFG) / !!LPC_CLKEN_SSP1)      
#define LPC_CLK_SPI         (LPC_CLKF_BASE(LPC_BASE_SPI_CFG) / !!LPC_CLKEN_SPI)       
#define LPC_CLK_SPIFI       (LPC_CLKF_BASE(LPC_BASE_SPIFI_CFG) / !!LPC_CLKEN_SPIFI)     
#define LPC_CLK_SDIO        (LPC_CLKF_BASE(LPC_BASE_SDIO_CFG) / !!LPC_CLKEN_SDIO)      
#define LPC_CLK_TIMER0      (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_TIMER0)    
#define LPC_CLK_TIMER1      (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_TIMER1)    
#define LPC_CLK_TIMER2      (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_TIMER2)    
#define LPC_CLK_TIMER3      (LPC_CLKF_BASE(LPC_BASE_M4_CFG) / !!LPC_CLKEN_TIMER3)    
#define LPC_CLK_UART0       (LPC_CLKF_BASE(LPC_BASE_UART0_CFG) / !!LPC_CLKEN_UART0)     
#define LPC_CLK_UART1       (LPC_CLKF_BASE(LPC_BASE_UART1_CFG) / !!LPC_CLKEN_UART1)     
#define LPC_CLK_UART2       (LPC_CLKF_BASE(LPC_BASE_UART2_CFG) / !!LPC_CLKEN_UART2)     
#define LPC_CLK_UART3       (LPC_CLKF_BASE(LPC_BASE_UART3_CFG) / !!LPC_CLKEN_UART3)     
#define LPC_CLK_USB0        (LPC_CLKF_BASE(LPC_BASE_USB0_CFG) / !!LPC_CLKEN_USB0)      
#define LPC_CLK_USB1        (LPC_CLKF_BASE(LPC_BASE_USB1_CFG) / !!LPC_CLKEN_USB1)      
#define LPC_CLK_WDT         (LPC_CLKF_IRC / 4 / !!LPC_CLKEN_WDT)


#define LPC_WDT_TICKS_MS(ms)    ((LPC_CLK_WDT * (ms) + 999) / 1000)

#define LPC_CCU_CFG(id, on) do { \
    CCU_CFGSTAT_T* pcf; \
    if ((id) < CLK_CCU2_START) { \
        pcf = &LPC_CCU1->CLKCCU[id]; \
    } else { \
        unsigned arr_idx = (id) - CLK_CCU2_START; \
        pcf = &LPC_CCU2->CLKCCU[arr_idx]; \
    } \
    pcf->CFG = (pcf->CFG & ~LPC_CCU_CFG_RUN) | BF_SET(LPC_CCU_CFG_RUN, (on)); \
    } while (0)


#define chip_wdt_reset() do { \
        unsigned var; \
        __irq_save(var); \
        __irq_disable(); \
        LPC_WWDT->FEED = 0xAA; \
        LPC_WWDT->FEED = 0x55; \
        __irq_restore(var); \
    } while(0)



/*================================================================================================*/

/*================================================================================================*/


/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void chip_delay_clk(unsigned int clk_count);
/*------------------------------------------------------------------------------------------------*/
void chip_init(void);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
#endif /* LPC_H_ */
