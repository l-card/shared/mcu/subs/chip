#ifndef REGS_MISC_H
#define REGS_MISC_H

#include "chip_devtype_spec_features.h"

#ifdef CHIP_LGT8FX8P
#define CHIP_REG_IVBASE     _MMIO_BYTE(0x39) /* Interrupt Vector Base Address */
#endif

/******************* External Interrupts **************************************/

#define CHIP_REG_EICRA      _MMIO_BYTE(0x69) /* External Interrupt Control Register A */
#define CHIP_REG_EIMSK      _MMIO_BYTE(0x3D) /* External Interrupt Mask Register */
#define CHIP_REG_EIFR       _MMIO_BYTE(0x3C) /* External Interrupt Flag Register */


/* ---------------- EICRA – External Interrupt Control Register A ------------*/
#define CHIP_REGFLD_EICRA_ISC1              (0x03U << 2) /* (RW) INT1 pin interrupt trigger mode */
#define CHIP_REGFLD_EICRA_ISC0              (0x03U << 0) /* (RW) INT0 pin interrupt trigger mode */
#define CHIP_REGFLD_EICRA_ISC(n)            (0x03U << 2*(n))

#define CHIP_REGFLDVAL_EICRA_ISC_LOW_LVL    0
#define CHIP_REGFLDVAL_EICRA_ISC_ANY_EDGE   1
#define CHIP_REGFLDVAL_EICRA_ISC_FALL       2
#define CHIP_REGFLDVAL_EICRA_ISC_RISE       3

/* ---------------- EIMSK – External Interrupt Mask Register -----------------*/
#define CHIP_REGFLD_EIMSK_INT1              (0x01U << 1) /* (RW) External pin 1 interrupt enable */
#define CHIP_REGFLD_EIMSK_INT0              (0x01U << 0) /* (RW) External pin 0 interrupt enable */
#define CHIP_REGFLD_EIMSK_INT(n)            (0x01U << (n))

/* ---------------- EIFR – External Interrupt Flag Register ------------------*/
#define CHIP_REGFLD_EIFR_INTF1              (0x01U << 1) /* (RW1C) External pin 1 interrupt flag */
#define CHIP_REGFLD_EIFR_INTF0              (0x01U << 0) /* (RW1C) External pin 0 interrupt flag */
#define CHIP_REGFLD_EIFR_INTF(n)            (0x01U << (n))



/******************* Pin Change Interrupt *************************************/
#define CHIP_REG_PCICR      _MMIO_BYTE(0x68) /* Pin Change Interrupt Control Register */
#define CHIP_REG_PCIFR      _MMIO_BYTE(0x3B) /* Pin Change Interrupt Flag Register */
#define CHIP_REG_PCMSK0     _MMIO_BYTE(0x6B) /* Pin Change Interrupt Mask Register 0 */
#define CHIP_REG_PCMSK1     _MMIO_BYTE(0x6C) /* Pin Change Interrupt Mask Register 1 */
#define CHIP_REG_PCMSK2     _MMIO_BYTE(0x6D) /* Pin Change Interrupt Mask Register 2 */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_PCMSK3     _MMIO_BYTE(0x73) /* Pin Change Interrupt Mask Register 3 */
#define CHIP_REG_PCMSK4     _MMIO_BYTE(0x74) /* Pin Change Interrupt Mask Register 4 */
#endif


#endif // REGS_MISC_H
