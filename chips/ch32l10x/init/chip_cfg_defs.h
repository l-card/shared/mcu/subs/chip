#ifndef CHIP_CONFIG_DEFS_H
#define CHIP_CONFIG_DEFS_H

#include "chip_ioremap_defs.h"
#include "chip_config.h"

#ifdef CHIP_CFG_CLK_HSE_MODE
    #define CHIP_CLK_HSE_MODE   CHIP_CFG_CLK_HSE_MODE
#else
    #define CHIP_CLK_HSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_LSE_MODE
    #define CHIP_CLK_LSE_MODE   CHIP_CFG_CLK_LSE_MODE
#else
    #define CHIP_CLK_LSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_HSI_EN
    #define CHIP_CLK_HSI_EN     CHIP_CFG_CLK_HSI_EN
#else
    #define CHIP_CLK_HSI_EN     1
#endif

#ifdef CHIP_CFG_CLK_CSS_SETUP_EN
    #define CHIP_CLK_CSS_SETUP_EN   CHIP_CFG_CLK_CSS_SETUP_EN
#else
    #define CHIP_CLK_CSS_SETUP_EN   0
#endif


#ifdef CHIP_CFG_CLK_PLL1_EN
    #define CHIP_CLK_PLL1_EN        CHIP_CFG_CLK_PLL1_EN
#else
    #define CHIP_CLK_PLL1_EN        0
#endif

#ifdef CHIP_CFG_CLK_PLL1_EN
    #ifdef CHIP_CFG_CLK_HSE_PLLIN_DIV
        #define CHIP_CLK_HSE_PLLIN_DIV CHIP_CFG_CLK_HSE_PLLIN_DIV
    #else
        #define CHIP_CLK_HSE_PLLIN_DIV 1
    #endif

    #ifdef CHIP_CFG_CLK_HSI_PLLIN_DIV
        #define CHIP_CLK_HSI_PLLIN_DIV CHIP_CFG_CLK_HSI_PLLIN_DIV
    #else
        #define CHIP_CLK_HSI_PLLIN_DIV 1
    #endif

    #ifndef CHIP_CFG_CLK_PLL1_MUL
        #error "PLL1 multiplier is not specified"
    #else
        #define CHIP_CLK_PLL1_MUL   CHIP_CFG_CLK_PLL1_MUL
    #endif

    #ifdef CHIP_CFG_CLK_PLL1_SRC
    #define CHIP_CLK_PLL1_SRC   CHIP_CFG_CLK_PLL1_SRC
    #else
    #define CHIP_CLK_PLL1_SRC   CHIP_CLK_HSI
    #endif
#else
    #define CHIP_CLK_HSE_PLLIN_DIV 0
    #define CHIP_CLK_PLL1_MUL    0
#endif


#ifdef CHIP_CFG_CLK_SYS_SRC
    #define CHIP_CLK_SYS_SRC CHIP_CFG_CLK_SYS_SRC
#else
    #define CHIP_CLK_SYS_SRC CHIP_CLK_HSI
#endif

#ifdef CHIP_CFG_CLK_AHB_DIV
    #define CHIP_CLK_AHB_DIV        CHIP_CFG_CLK_AHB_DIV
#else
    #define CHIP_CLK_AHB_DIV        1
#endif

#ifdef CHIP_CFG_CLK_APB1_DIV
    #define CHIP_CLK_APB1_DIV       CHIP_CFG_CLK_APB1_DIV
#else
    #define CHIP_CLK_APB1_DIV       1
#endif

#ifdef CHIP_CFG_CLK_APB2_DIV
    #define CHIP_CLK_APB2_DIV       CHIP_CFG_CLK_APB2_DIV
#else
    #define CHIP_CLK_APB2_DIV       1
#endif


#ifdef CHIP_CFG_CLK_ADC_SRC
    #define CHIP_CLK_ADC_SRC       CHIP_CFG_CLK_ADC_SRC
#else
    #define CHIP_CLK_ADC_SRC       CHIP_CLK_APB2
#endif

#ifdef CHIP_CFG_CLK_ADC_APB2_DIV
    #define CHIP_CLK_ADC_APB2_DIV  CHIP_CFG_CLK_ADC_APB2_DIV
#else
    #define CHIP_CLK_ADC_APB2_DIV  2
#endif

#ifdef CHIP_CFG_CLK_ADC_DUTY_CHG
    #define CHIP_CLK_ADC_DUTY_CHG   CHIP_CFG_CLK_ADC_DUTY_CHG
#else
    #define CHIP_CLK_ADC_DUTY_CHG   0
#endif

#ifdef CHIP_CFG_CLK_MCO_EN
    #define CHIP_CLK_MCO_EN     CHIP_CFG_CLK_MCO_EN
#else
    #define CHIP_CLK_MCO_EN     0
#endif

#if CHIP_CLK_MCO_EN
    #ifndef CHIP_CFG_CLK_MCO_SRC
        #error MCO source clock is not specified
    #else
        #define CHIP_CLK_MCO_SRC    CHIP_CFG_CLK_MCO_SRC
    #endif
#endif


#ifdef CHIP_CFG_WDT_SETUP_EN
    #define CHIP_WDT_SETUP_EN CHIP_CFG_WDT_SETUP_EN
#else
    #define CHIP_WDT_SETUP_EN 0
#endif
#if CHIP_WDT_SETUP_EN
    #define CHIP_CLK_LSI_EN 1 /* LSI должен быть включен при использовании IWDT */
#elif defined CHIP_CFG_CLK_LSI_EN
    #define CHIP_CLK_LSI_EN CHIP_CFG_CLK_LSI_EN
#else
    #define CHIP_CLK_LSI_EN 0
#endif



#ifdef CHIP_CFG_LTIMER_EN
    #define CHIP_LTIMER_EN   CHIP_CFG_LTIMER_EN
#else
    #define CHIP_LTIMER_EN   1
#endif

#ifdef CHIP_CFG_CLK_DMA1_EN
    #define CHIP_CLK_DMA1_EN    CHIP_CFG_CLK_DMA1_EN
#else
    #define CHIP_CLK_DMA1_EN    0
#endif

#ifdef CHIP_CFG_CLK_SRAM_EN
    #define CHIP_CLK_SRAM_EN    CHIP_CFG_CLK_SRAM_EN
#else
    #define CHIP_CLK_SRAM_EN    1
#endif

#ifdef CHIP_CFG_CLK_CRC_EN
    #define CHIP_CLK_CRC_EN     CHIP_CFG_CLK_CRC_EN
#else
    #define CHIP_CLK_CRC_EN     0
#endif

#ifdef CHIP_CFG_CLK_USBFS_EN
    #define CHIP_CLK_USBFS_EN   CHIP_CFG_CLK_USBFS_EN
#else
    #define CHIP_CLK_USBFS_EN   0
#endif

#if CHIP_CFG_CLK_USBFS_EN
    #define CHIP_CLK_USBFS_SETUP_EN     1
#elif defined CHIP_CFG_CLK_USBFS_SETUP_EN
    #define CHIP_CLK_USBFS_SETUP_EN     CHIP_CFG_CLK_USBFS_SETUP_EN
#else
    #define CHIP_CLK_USBFS_SETUP_EN     0
#endif

#ifdef CHIP_CFG_CLK_USBPD_EN
    #define CHIP_CLK_USBPD_EN   CHIP_CFG_CLK_USBPD_EN
#else
    #define CHIP_CLK_USBPD_EN   0
#endif

#ifdef CHIP_CFG_CLK_AFIO_EN
    #define CHIP_CLK_AFIO_EN    CHIP_CFG_CLK_AFIO_EN
#else
    #define CHIP_CLK_AFIO_EN    1
#endif
#ifdef CHIP_CFG_CLK_GPIOA_EN
    #define CHIP_CLK_GPIOA_EN   CHIP_CFG_CLK_GPIOA_EN
#else
    #define CHIP_CLK_GPIOA_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOB_EN
    #define CHIP_CLK_GPIOB_EN   CHIP_CFG_CLK_GPIOB_EN
#else
    #define CHIP_CLK_GPIOB_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOC_EN
    #define CHIP_CLK_GPIOC_EN   CHIP_CFG_CLK_GPIOC_EN
#else
    #define CHIP_CLK_GPIOC_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOD_EN
    #define CHIP_CLK_GPIOD_EN   CHIP_CFG_CLK_GPIOD_EN
#else
    #define CHIP_CLK_GPIOD_EN   0
#endif
#ifdef CHIP_CFG_CLK_ADC1_EN
    #define CHIP_CLK_ADC1_EN    CHIP_CFG_CLK_ADC1_EN
#else
    #define CHIP_CLK_ADC1_EN    0
#endif
#ifdef CHIP_CFG_CLK_USART1_EN
    #define CHIP_CLK_USART1_EN  CHIP_CFG_CLK_USART1_EN
#else
    #define CHIP_CLK_USART1_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART2_EN
    #define CHIP_CLK_USART2_EN  CHIP_CFG_CLK_USART2_EN
#else
    #define CHIP_CLK_USART2_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART3_EN
    #define CHIP_CLK_USART3_EN  CHIP_CFG_CLK_USART3_EN
#else
    #define CHIP_CLK_USART3_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART4_EN
    #define CHIP_CLK_USART4_EN  CHIP_CFG_CLK_USART4_EN
#else
    #define CHIP_CLK_USART4_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI1_EN
    #define CHIP_CLK_SPI1_EN    CHIP_CFG_CLK_SPI1_EN
#else
    #define CHIP_CLK_SPI1_EN    0
#endif
#ifdef CHIP_CFG_CLK_SPI2_EN
    #define CHIP_CLK_SPI2_EN    CHIP_CFG_CLK_SPI2_EN
#else
    #define CHIP_CLK_SPI2_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C1_EN
    #define CHIP_CLK_I2C1_EN    CHIP_CFG_CLK_I2C1_EN
#else
    #define CHIP_CLK_I2C1_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C2_EN
    #define CHIP_CLK_I2C2_EN    CHIP_CFG_CLK_I2C2_EN
#else
    #define CHIP_CLK_I2C2_EN    0
#endif
#ifdef CHIP_CFG_CLK_CAN1_EN
    #define CHIP_CLK_CAN1_EN    CHIP_CFG_CLK_CAN1_EN
#else
    #define CHIP_CLK_CAN1_EN    0
#endif
#ifdef CHIP_CFG_CLK_BKP_EN
    #define CHIP_CLK_BKP_EN     CHIP_CFG_CLK_BKP_EN
#else
    #define CHIP_CLK_BKP_EN     0
#endif
#ifdef CHIP_CFG_CLK_PWR_EN
    #define CHIP_CLK_PWR_EN     CHIP_CFG_CLK_PWR_EN
#else
    #define CHIP_CLK_PWR_EN     0
#endif

#ifdef CHIP_CFG_CLK_TIM1_EN
    #define CHIP_CLK_TIM1_EN    CHIP_CFG_CLK_TIM1_EN
#else
    #define CHIP_CLK_TIM1_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM2_EN
    #define CHIP_CLK_TIM2_EN    CHIP_CFG_CLK_TIM2_EN
#else
    #define CHIP_CLK_TIM2_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM3_EN
    #define CHIP_CLK_TIM3_EN    CHIP_CFG_CLK_TIM3_EN
#else
    #define CHIP_CLK_TIM3_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM4_EN
    #define CHIP_CLK_TIM4_EN    CHIP_CFG_CLK_TIM4_EN
#else
    #define CHIP_CLK_TIM4_EN    0
#endif

#ifdef CHIP_CFG_CLK_WWDG_EN
    #define CHIP_CLK_WWDG_EN    CHIP_CFG_CLK_WWDG_EN
#else
    #define CHIP_CLK_WWDG_EN    0
#endif
#ifdef CHIP_CFG_CLK_LPTIM1_EN
    #define CHIP_CLK_LPTIM1_EN    CHIP_CFG_CLK_LPTIM1_EN
#else
    #define CHIP_CLK_LPTIM1_EN    0
#endif





#ifdef CHIP_CFG_IOREMAP_SDI_EN
    #define CHIP_IOREMAP_SDI_EN             CHIP_CFG_IOREMAP_SDI_EN
#else
    #define CHIP_IOREMAP_SDI_EN             0
#endif

#ifdef CHIP_CFG_IOREMAP_PD01_OSC_EN
    #define CHIP_IOREMAP_PD01_OSC_EN        CHIP_CFG_IOREMAP_PD01_OSC_EN
#else
    #define CHIP_IOREMAP_PD01_OSC_EN        1
#endif


#ifdef CHIP_CFG_IOREMAP_USART1
    #define CHIP_IOREMAP_USART1     CHIP_CFG_IOREMAP_USART1
#else
    #define CHIP_IOREMAP_USART1     CHIP_IOREMAP_USART1_A9_10_A8_A11_12
#endif



#ifdef CHIP_CFG_IOREMAP_USART2
    #define CHIP_IOREMAP_USART2     CHIP_CFG_IOREMAP_USART2
#else
    #define CHIP_IOREMAP_USART2     CHIP_IOREMAP_USART2_A2_4_A0_1
#endif

#ifdef CHIP_CFG_IOREMAP_USART3
    #define CHIP_IOREMAP_USART3     CHIP_CFG_IOREMAP_USART3
#else
    #define CHIP_IOREMAP_USART3     CHIP_IOREMAP_USART3_B10_14
#endif

#ifdef CHIP_CFG_IOREMAP_USART4
    #define CHIP_IOREMAP_USART4     CHIP_CFG_IOREMAP_USART4
#else
    #define CHIP_IOREMAP_USART4     CHIP_IOREMAP_USART4_B0_4
#endif

#ifdef CHIP_CFG_IOREMAP_SPI1
    #define CHIP_IOREMAP_SPI1       CHIP_CFG_IOREMAP_SPI1
#else
    #define CHIP_IOREMAP_SPI1       CHIP_IOREMAP_SPI1_A4_7
#endif

#ifdef CHIP_CFG_IOREMAP_I2C1
    #define CHIP_IOREMAP_I2C1       CHIP_CFG_IOREMAP_I2C1
#else
    #define CHIP_IOREMAP_I2C1       CHIP_IOREMAP_I2C1_B6_7
#endif

#ifdef CHIP_CFG_IOREMAP_TIM1
    #define CHIP_IOREMAP_TIM1       CHIP_CFG_IOREMAP_TIM1
#else
    #define CHIP_IOREMAP_TIM1       CHIP_IOREMAP_TIM1_A12_A8_11_B12_15
#endif

#ifdef CHIP_CFG_IOREMAP_TIM2
    #define CHIP_IOREMAP_TIM2       CHIP_CFG_IOREMAP_TIM2
#else
    #define CHIP_IOREMAP_TIM2       CHIP_IOREMAP_TIM2_A0_3
#endif

#ifdef CHIP_CFG_IOREMAP_TIM3
    #define CHIP_IOREMAP_TIM3       CHIP_CFG_IOREMAP_TIM3
#else
    #define CHIP_IOREMAP_TIM3       CHIP_IOREMAP_TIM3_A6_7_B0_1
#endif

#ifdef CHIP_CFG_IOREMAP_TIM4
    #define CHIP_IOREMAP_TIM4       CHIP_CFG_IOREMAP_TIM4
#else
    #define CHIP_IOREMAP_TIM4       CHIP_IOREMAP_TIM4_B6_9
#endif


#ifdef CHIP_CFG_IOREMAP_LPTIM1
    #define CHIP_IOREMAP_LPTIM1     CHIP_CFG_IOREMAP_LPTIM1
#else
    #define CHIP_IOREMAP_LPTIM1     CHIP_IOREMAP_LPTIM1_B12_15
#endif



#ifdef CHIP_CFG_IOREMAP_CAN1
    #define CHIP_IOREMAP_CAN1       CHIP_CFG_IOREMAP_CAN1
#else
    #define CHIP_IOREMAP_CAN1       CHIP_IOREMAP_CAN1_A11_12
#endif

#endif // CHIP_CONFIG_DEFS_H
