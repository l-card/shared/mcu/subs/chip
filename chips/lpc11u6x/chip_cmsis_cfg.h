#ifndef CHIP_CMSIS_N32G03X_H
#define CHIP_CMSIS_N32G03X_H

/**
/* Configuration of the Cortex-M0+ Processor and Core Peripherals */
#define __CM0PLUS_REV             0x0000    /*!< Cortex-M0 Core Revision                          */
#define __MPU_PRESENT             0         /*!< MPU present or not                               */
#define __NVIC_PRIO_BITS          2         /*!< Number of Bits used for Priority Levels          */
#define __Vendor_SysTickConfig    0         /*!< Set to 1 if different SysTick Config is used     */
#define __VTOR_PRESENT            1


#endif // CHIP_CMSIS_N32G03X_H
