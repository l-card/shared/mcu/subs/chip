#ifndef CHIP_TARGET_H
#define CHIP_TARGET_H

#include "chip_std_defs.h"


#include "chip_gic.h"
#include "chip_irq_nums.h"

#include "chip_gtim.h"


#include "chip_ioreg_defs.h"
#include "chip_mmap.h"
#include "chip_otp_map.h"

#include "regs/regs_src.h"
#include "regs/regs_iomuxc.h"
#include "regs/regs_sysctr.h"
#include "regs/regs_ccm.h"
#include "regs/regs_ccm_ana.h"
#include "regs/regs_gpc.h"
#include "regs/regs_xtalosc.h"
#include "regs/regs_rdc.h"
#include "regs/regs_gpio.h"
#include "regs/regs_tmu.h"
#include "regs/regs_gpt.h"
#include "regs/regs_uart.h"
#include "regs/regs_i2c.h"
#include "regs/regs_qspi.h"
#include "regs/regs_ecspi.h"
#include "regs/regs_ocotp.h"
#include "regs/regs_enet.h"
#include "regs/regs_audio_blk_ctrl.h"
#include "regs/regs_hsio_blk_ctrl.h"
#include "regs/regs_aipstz.h"


#include "chip_pins.h"

#include "init/chip_clk.h"
#include "init/chip_clk_constraints.h"
#include "init/chip_domain_defs.h"
#include "init/chip_per_ctl.h"

#include "boot/chip_boot_modes.h"
#include "boot/chip_boot_ivt.h"
#include "boot/chip_boot_qspi.h"

#endif // CHIP_TARGET_H
