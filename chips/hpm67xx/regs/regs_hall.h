#ifndef REG_HALL_H
#define REG_HALL_H

#include "chip_ioreg_defs.h"


typedef struct {
    __I  uint32_t W;                       /* 0x30: W counter */
    __I  uint32_t V;                       /* 0x34: V counter */
    __I  uint32_t U;                       /* 0x38: U counter */
    __I  uint32_t TMR;                     /* 0x3C: Timer counter */
} CHIP_REGS_HALL_COUNT_T;



typedef struct {
    __IO uint32_t CR;                          /* 0x0: Control Register */
    __IO uint32_t PHCFG;                       /* 0x4: Phase configure register */
    __IO uint32_t WDGCFG;                      /* 0x8: Watchdog configure register */
    __IO uint32_t UVWCFG;                      /* 0xC: U,V,W configure register */
    __IO uint32_t TRGOEN;                      /* 0x10: Trigger output enable register */
    __IO uint32_t READEN;                      /* 0x14: Read event enable register */
    __I  uint8_t  RESERVED0[12];               /* 0x18 - 0x23: Reserved */
    __IO uint32_t DMAEN;                       /* 0x24: DMA enable register */
    __IO uint32_t SR;                          /* 0x28: Status register */
    __IO uint32_t IRQEN;                       /* 0x2C: Interrupt request enable register */
    struct {
        CHIP_REGS_HALL_COUNT_T CURRENT;        /* 0x30 - 0x3C: Current counters */
        CHIP_REGS_HALL_COUNT_T READ;           /* 0x40 - 0x4C: Read counters */
        CHIP_REGS_HALL_COUNT_T SNAP[2];        /* 0x50 - 0x6C: Snapshot counters */
    } COUNT;
    struct {
        __I  uint32_t U[2];                    /* 0x70 - 0x74: U phase log register 0/1 */
        __I  uint32_t V[2];                    /* 0x78 - 0x7C: V phase log register 0/1 */
        __I  uint32_t W[2];                    /* 0x80 - 0x84: W phase log register 0/1 */
    } HIS;
} CHIP_REGS_HALL_T;


#define CHIP_REGS_HALL0           ((CHIP_REGS_HALL_T *)CHIP_MEMRGN_ADDR_HALL0)
#define CHIP_REGS_HALL1           ((CHIP_REGS_HALL_T *)CHIP_MEMRGN_ADDR_HALL1)
#define CHIP_REGS_HALL2           ((CHIP_REGS_HALL_T *)CHIP_MEMRGN_ADDR_HALL2)
#define CHIP_REGS_HALL3           ((CHIP_REGS_HALL_T *)CHIP_MEMRGN_ADDR_HALL3)


/*********** HALL_CR - Control Register ***************************************/
#define CHIP_REGFLD_HALL_CR_READ                        (0x00000001UL << 31U) /* (W1) Generate software read event */
#define CHIP_REGFLD_HALL_CR_SNAPEN                      (0x00000001UL << 11U) /* (RW) Enable snapshot function */
#define CHIP_REGFLD_HALL_CR_RSTCNT                      (0x00000001UL <<  4U) /* (RW) Reset all counters and corresponding snapshot registers */
#define CHIP_REGMSK_HALL_CR_RESERVED                    (0x7FFFF7EFUL)

/*********** HALL_PHCFG - Phase configure register ****************************/
#define CHIP_REGFLD_HALL_PHCFG_DLYSEL                   (0x00000001UL << 31U) /* (RW) Starting point of the delay */
#define CHIP_REGFLD_HALL_PHCFG_DLYCNT                   (0x00FFFFFFUL <<  0U) /* (RW) The duration of the delay in clock cycles */
#define CHIP_REGMSK_HALL_PHCFG_RESERVED                 (0x7F000000UL)


#define CHIP_REGFLDVAL_HALL_PHCFG_DLYSEL_PHPREF         1 /* Delay after early trigger event (SR[PHPREF] set to 1) */
#define CHIP_REGFLDVAL_HALL_PHCFG_DLYSEL_PHUPT          0 /* Delay after U, V, W signal toggle (SR[PHUPT] is set to 1) */


/*********** HALL_WDGCFG - watchdog configure register ************************/
#define CHIP_REGFLD_HALL_PHCFG_WDGEN                    (0x00000001UL << 31U) /* (RW) Enable watchdog */
#define CHIP_REGFLD_HALL_PHCFG_WDGTO                    (0x7FFFFFFFUL <<  0U) /* (RW) Watchdog counter timeout value */


/*********** HALL_UVWCFG - U,V,W configure register ***************************/
#define CHIP_REGFLD_HALL_UVWCFG_PRECNT                  (0x00FFFFFFUL <<  0U) /* (RW) The early amount of the early even in clock cycle */
#define CHIP_REGMSK_HALL_UVWCFG_RESERVED                (0xFF000000UL)


/*********** HALL_TRGOEN - trigger output enable register *********************/
#define CHIP_REGFLD_HALL_TRGOEN_WDGEN                   (0x00000001UL << 31U) /* (RW) WDGF flag TRGO output trigger enable */
#define CHIP_REGFLD_HALL_TRGOEN_PHUPTEN                 (0x00000001UL << 30U) /* (RW) PHUPTF flag TRGO output trigger enable */
#define CHIP_REGFLD_HALL_TRGOEN_PHPREEN                 (0x00000001UL << 29U) /* (RW) PHPREF flag TRGO output trigger enable */
#define CHIP_REGFLD_HALL_TRGOEN_PHDLYEN                 (0x00000001UL << 28U) /* (RW) PHDLYF flag TRGO output trigger enable */
#define CHIP_REGFLD_HALL_TRGOEN_UFEN                    (0x00000001UL << 23U) /* (RW) UF flag TRGO output trigger enable */
#define CHIP_REGFLD_HALL_TRGOEN_VFEN                    (0x00000001UL << 22U) /* (RW) VF flag TRGO output trigger enable */
#define CHIP_REGFLD_HALL_TRGOEN_WFEN                    (0x00000001UL << 21U) /* (RW) WF flag TRGO output trigger enable */
#define CHIP_REGMSK_HALL_TRGOEN_RESERVED                (0x0F1FFFFFUL)


/*********** HALL_READEN - read event enable register *************************/
#define CHIP_REGFLD_HALL_READEN_WDGEN                   (0x00000001UL << 31U) /* (RW) WDGF flag read event enable */
#define CHIP_REGFLD_HALL_READEN_PHUPTEN                 (0x00000001UL << 30U) /* (RW) PHUPTF flag read event enable */
#define CHIP_REGFLD_HALL_READEN_PHPREEN                 (0x00000001UL << 29U) /* (RW) PHPREF flag read event enable */
#define CHIP_REGFLD_HALL_READEN_PHDLYEN                 (0x00000001UL << 28U) /* (RW) PHDLYF flag read event enable */
#define CHIP_REGFLD_HALL_READEN_UFEN                    (0x00000001UL << 23U) /* (RW) UF flag read event enable */
#define CHIP_REGFLD_HALL_READEN_VFEN                    (0x00000001UL << 22U) /* (RW) VF flag read event enable */
#define CHIP_REGFLD_HALL_READEN_WFEN                    (0x00000001UL << 21U) /* (RW) WF flag read event enable */
#define CHIP_REGMSK_HALL_READEN_RESERVED                (0x0F1FFFFFUL)


/*********** HALL_DMAEN - DMA enable register *********************************/
#define CHIP_REGFLD_HALL_DMAEN_WDGEN                    (0x00000001UL << 31U) /* (RW) WDGF flag DMA request enable */
#define CHIP_REGFLD_HALL_DMAEN_PHUPTEN                  (0x00000001UL << 30U) /* (RW) PHUPTF flag DMA request enable */
#define CHIP_REGFLD_HALL_DMAEN_PHPREEN                  (0x00000001UL << 29U) /* (RW) PHPREF flag DMA request enable */
#define CHIP_REGFLD_HALL_DMAEN_PHDLYEN                  (0x00000001UL << 28U) /* (RW) PHDLYF flag DMA request enable */
#define CHIP_REGFLD_HALL_DMAEN_UFEN                     (0x00000001UL << 23U) /* (RW) UF flag DMA request enable */
#define CHIP_REGFLD_HALL_DMAEN_VFEN                     (0x00000001UL << 22U) /* (RW) VF flag DMA request enable */
#define CHIP_REGFLD_HALL_DMAEN_WFEN                     (0x00000001UL << 21U) /* (RW) WF flag DMA request enable */
#define CHIP_REGMSK_HALL_DMAEN_RESERVED                 (0x0F1FFFFFUL)


/*********** HALL_SR - status register ****************************************/
#define CHIP_REGFLD_HALL_SR_WDGF                        (0x00000001UL << 31U) /* (RW) Watchdog timeout flag */
#define CHIP_REGFLD_HALL_SR_PHUPTF                      (0x00000001UL << 30U) /* (RW) Phase update flag, set to 1 when any signal of U phase, V phase or W phase toggles */
#define CHIP_REGFLD_HALL_SR_PHPREF                      (0x00000001UL << 29U) /* (RW) The early event flag */
#define CHIP_REGFLD_HALL_SR_PHDLYEN                     (0x00000001UL << 28U) /* (RW) The delayed event flag */
#define CHIP_REGFLD_HALL_SR_UFEN                        (0x00000001UL << 23U) /* (RW) U phase flag, set to 1 when U phase signal toggles */
#define CHIP_REGFLD_HALL_SR_VFEN                        (0x00000001UL << 22U) /* (RW) V phase flag, set to 1 when V phase signal toggles */
#define CHIP_REGFLD_HALL_SR_WFEN                        (0x00000001UL << 21U) /* (RW) W phase flag, set to 1 when W phase signal toggles */
#define CHIP_REGMSK_HALL_SR_RESERVED                    (0x0F1FFFFFUL)


/*********** HALL_IRQEN - interrupt request enable register *******************/
#define CHIP_REGFLD_HALL_IRQEN_WDGEN                    (0x00000001UL << 31U) /* (RW) WDGF flag interrupt request enable */
#define CHIP_REGFLD_HALL_IRQEN_PHUPTEN                  (0x00000001UL << 30U) /* (RW) PHUPTF flag interrupt request enable */
#define CHIP_REGFLD_HALL_IRQEN_PHPREEN                  (0x00000001UL << 29U) /* (RW) PHPREF flag interrupt request enable */
#define CHIP_REGFLD_HALL_IRQEN_PHDLYEN                  (0x00000001UL << 28U) /* (RW) PHDLYF flag interrupt request enable */
#define CHIP_REGFLD_HALL_IRQEN_UFEN                     (0x00000001UL << 23U) /* (RW) UF flag interrupt request enable */
#define CHIP_REGFLD_HALL_IRQEN_VFEN                     (0x00000001UL << 22U) /* (RW) VF flag interrupt request enable */
#define CHIP_REGFLD_HALL_IRQEN_WFEN                     (0x00000001UL << 21U) /* (RW) WF flag interrupt request enable */
#define CHIP_REGMSK_HALL_IRQEN_RESERVED                 (0x0F1FFFFFUL)


/*********** HALL_COUNTW - W phase counter ************************************/
#define CHIP_REGFLD_HALL_COUNTW_WCNT                    (0x0FFFFFFFUL <<  0U) /* (RO) W phase counter */
#define CHIP_REGMSK_HALL_COUNTW_RESERVED                (0xF0000000UL << 28U)


/*********** HALL_COUNTV - V phase counter ************************************/
#define CHIP_REGFLD_HALL_COUNTV_VCNT                    (0x0FFFFFFFUL <<  0U) /* (RO) V phase counter */
#define CHIP_REGMSK_HALL_COUNTV_RESERVED                (0xF0000000UL << 28U)


/*********** HALL_COUNTU - U phase counter ************************************/
#define CHIP_REGFLD_HALL_COUNTU_DIR                     (0x00000001UL << 31U) /* (RO) Rotation reverse */
#define CHIP_REGFLD_HALL_COUNTU_USTAT                   (0x00000001UL << 30U) /* (RO) U phase input signal status */
#define CHIP_REGFLD_HALL_COUNTU_VSTAT                   (0x00000001UL << 29U) /* (RO) V phase input signal status */
#define CHIP_REGFLD_HALL_COUNTU_WSTAT                   (0x00000001UL << 28U) /* (RO) W phase input signal status */
#define CHIP_REGFLD_HALL_COUNTU_UCNT                    (0x0FFFFFFFUL <<  0U) /* (RO) U phase counter */


#endif // REG_HALL_H
