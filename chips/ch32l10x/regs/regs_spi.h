#ifndef REGS_SPI_H
#define REGS_SPI_H


#include "chip_devtype_spec_features.h"

#define CHIP_SUPPORT_SPI_NSSP               0
#define CHIP_SUPPORT_SPI_FRAME_FMT          0
#define CHIP_SUPPORT_SPI_CUSTOM_DATA_SIZE   0
#define CHIP_SUPPORT_SPI_FIFO               0
#define CHIP_SUPPORT_SPI_LAST_DMA           0
#define CHIP_SUPPORT_SPI_FRAME_ERR          0
#define CHIP_SUPPORT_SPI_I2S                0
#define CHIP_SUPPORT_I2S_ASYNC_START        0
#define CHIP_SUPPORT_SPI_HS_CTL             1

#include "stm32_spi_v1_regs.h"

#define CHIP_REGS_SPI1                  ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI1)
#define CHIP_REGS_SPI2                  ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI2)

#endif // REGS_SPI_H
