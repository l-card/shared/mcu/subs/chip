#include "archtypes/cortexm/init/chip_cortexm_isr.h"
#include <stddef.h>

CHIP_ISR_DUMMY_DECLARE()

CHIP_ISR_DECLARE(NMI_Handler);
CHIP_ISR_DECLARE(HardFault_Handler);
CHIP_ISR_DECLARE(SVC_Handler);
CHIP_ISR_DECLARE(PendSV_Handler);
CHIP_ISR_DECLARE(SysTick_IRQHandler);
CHIP_ISR_DECLARE(WWDG_IRQHandler);
CHIP_ISR_DECLARE(PVD_IRQHandler);
CHIP_ISR_DECLARE(RTC_IRQHandler);
CHIP_ISR_DECLARE(MMU_IRQHandler);
CHIP_ISR_DECLARE(FLASH_IRQHandler);
CHIP_ISR_DECLARE(RCC_IRQHandler);
CHIP_ISR_DECLARE(EXTI0_1_IRQHandler);
CHIP_ISR_DECLARE(EXTI2_3_IRQHandler);
CHIP_ISR_DECLARE(EXTI4_15_IRQHandler);
CHIP_ISR_DECLARE(TSC_IRQHandler);
CHIP_ISR_DECLARE(DMA_Channel1_2_IRQHandler);
CHIP_ISR_DECLARE(DMA_Channel1_2_3_4_IRQHandler);
CHIP_ISR_DECLARE(DMA_Channel5_IRQHandler);
CHIP_ISR_DECLARE(TIM1_BRK_UP_TRG_COM_IRQHandler);
CHIP_ISR_DECLARE(TIM1_CC_IRQHandler);
CHIP_ISR_DECLARE(DMA_Channel3_4_IRQHandler);
CHIP_ISR_DECLARE(TIM3_IRQHandler);
CHIP_ISR_DECLARE(USART2_IRQHandler);
CHIP_ISR_DECLARE(TIM8_BRK_UP_TRG_COM_IRQHandler);
CHIP_ISR_DECLARE(TIM8_CC_IRQHandler);
CHIP_ISR_DECLARE(LPTIM_TIM6_IRQHandler);
CHIP_ISR_DECLARE(ADC_IRQHandler);
CHIP_ISR_DECLARE(SPI2_IRQHandler);
CHIP_ISR_DECLARE(I2C1_IRQHandler);
CHIP_ISR_DECLARE(I2C2_IRQHandler);
CHIP_ISR_DECLARE(SPI1_IRQHandler);
CHIP_ISR_DECLARE(HDIV_SQRT_IRQHandler);
CHIP_ISR_DECLARE(RAMC_PERR_IRQHandler);
CHIP_ISR_DECLARE(USART1_2_IRQHandler);
CHIP_ISR_DECLARE(LPUART_IRQHandler);
CHIP_ISR_DECLARE(USART1_IRQHandler);
CHIP_ISR_DECLARE(COMP_IRQHandler);


CHIP_ISR_TABLE() = {
    CHIP_ISR_TABLE_ENTRY_STACKPTR,
    CHIP_ISR_TABLE_ENTRY_RESETPTR,
    NMI_Handler,
    HardFault_Handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    SVC_Handler,
    NULL,
    NULL,
    PendSV_Handler,
    SysTick_IRQHandler,
    WWDG_IRQHandler,                   /* Window WatchDog              */
    PVD_IRQHandler,                    /* PPVD through EXTI Line 16 detect */
    RTC_IRQHandler,                    /* RTC through the EXTI line    */
    MMU_IRQHandler,                    /* MMU */
    FLASH_IRQHandler,                  /* FLASH                        */
    RCC_IRQHandler,                    /* RCC                          */
    EXTI0_1_IRQHandler,                /* EXTI Line 0 and 1            */
    EXTI2_3_IRQHandler,                /* EXTI Line 2 and 3            */
    EXTI4_15_IRQHandler,               /* EXTI Line 4 to 15            */
    NULL,
    DMA_Channel1_2_IRQHandler,         /* DMA Channel 1,2              */
    DMA_Channel1_2_3_4_IRQHandler,     /* DMA Channel 1,2,3,4          */
    DMA_Channel5_IRQHandler,           /* DMA Channel 5 */
    TIM1_BRK_UP_TRG_COM_IRQHandler,    /* TIM1 Break, Update, Trigger and Commutation */
    TIM1_CC_IRQHandler,                /* TIM1 Capture Compare         */
    DMA_Channel3_4_IRQHandler,         /* DMA Channel 3,4              */
    TIM3_IRQHandler,                   /* TIM3                         */
    USART2_IRQHandler,                 /* USART2                       */
    TIM8_BRK_UP_TRG_COM_IRQHandler,    /* TIM8 Break, Update, Trigger and Commutation */
    TIM8_CC_IRQHandler,                /* TIM8 Capture Compare         */
    LPTIM_TIM6_IRQHandler,             /* LPTIM/TIM6                   */
    ADC_IRQHandler,                    /* ADC                          */
    SPI2_IRQHandler,                   /* SPI2                         */
    I2C1_IRQHandler,                   /* I2C1                         */
    I2C2_IRQHandler,                   /* I2C2                         */
    SPI1_IRQHandler,                   /* SPI1                         */
    HDIV_SQRT_IRQHandler,              /* HDIV/SQRT                    */
    RAMC_PERR_IRQHandler,              /* RAMC PARITY ERR              */
    USART1_2_IRQHandler,               /* USART1/USART2                */
    LPUART_IRQHandler,                 /* LPUART                       */
    USART1_IRQHandler,                 /* USART1                       */
    COMP_IRQHandler                    /* COMP */
};
