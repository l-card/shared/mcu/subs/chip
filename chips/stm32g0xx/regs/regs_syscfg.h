#ifndef CHIP_STM32G0XX_REGS_SYSCFG_H
#define CHIP_STM32G0XX_REGS_SYSCFG_H

typedef struct {
  __IO uint32_t CFGR1;          /*!< SYSCFG configuration register 1,                   Address offset: 0x00 */
       uint32_t RESERVED0[5];   /*!< Reserved,                                                   0x04 --0x14 */
  __IO uint32_t CFGR2;          /*!< SYSCFG configuration register 2,                   Address offset: 0x18 */
       uint32_t RESERVED1[25];  /*!< Reserved                                                           0x1C */
  __IO uint32_t ITLINE[32];     /*!< SYSCFG configuration ITLINE register,             Address offset: 0x80 */
} CHIP_REGS_SYSCFG_T;

#define CHIP_REGS_SYSCFG          ((CHIP_REGS_SYSCFG_T *) CHIP_MEMRGN_ADDR_PERIPH_SYSCFG)



/*****************  Bit definition for SYSCFG_CFGR1 register  ****************/
#define CHIP_REGFLD_SYSCFG_CFGR1_MEM_MODE               (0x00000003UL <<  0U)  /* Memory Remap Config */
#define CHIP_REGFLD_SYSCFG_CFGR1_PA11_RMP               (0x00000001UL <<  3U)  /* PA11 Remap */
#define CHIP_REGFLD_SYSCFG_CFGR1_PA12_RMP               (0x00000001UL <<  4U)  /* PA12 Remap */
#define CHIP_REGFLD_SYSCFG_CFGR1_IR_POL                 (0x00000001UL <<  5U)  /* IROut Polarity Selection */
#define CHIP_REGFLD_SYSCFG_CFGR1_IR_MOD                 (0x00000003UL <<  6U)  /* IRDA Modulation Envelope signal source selection */
#define CHIP_REGFLD_SYSCFG_CFGR1_BOOSTEN                (0x00000001UL <<  8U)  /* I/O analog switch voltage booster enable */
#define CHIP_REGFLD_SYSCFG_CFGR1_UCPD1_STROBE           (0x00000001UL <<  9U)  /* Strobe signal bit for UCPD1 */
#define CHIP_REGFLD_SYSCFG_CFGR1_UCPD2_STROBE           (0x00000001UL << 10U)  /* Strobe signal bit for UCPD2 */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB6_FMP            (0x00000001UL << 16U)  /* I2C PB6 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB7_FMP            (0x00000001UL << 17U)  /* I2C PB7 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB8_FMP            (0x00000001UL << 18U)  /* I2C PB8 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PB9_FMP            (0x00000001UL << 19U)  /* I2C PB9 Fast mode plus */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C1_FMP               (0x00000001UL << 20U)  /* Enable Fast Mode Plus on PB10, PB11, PF6 and PF7  */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C2_FMP               (0x00000001UL << 21U)  /* Enable I2C2 Fast mode plus  */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PA9_FMP            (0x00000001UL << 22U)  /* Enable Fast Mode Plus on PA9  */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C_PA10_FMP           (0x00000001UL << 23U)  /* Enable Fast Mode Plus on PA10 */
#define CHIP_REGFLD_SYSCFG_CFGR1_I2C3_FMP               (0x00000001UL << 24U)  /* Enable I2C3 Fast mode plus  */

/******************  Bit definition for SYSCFG_CFGR2 register  ****************/
#define CHIP_REGFLD_SYSCFG_CFGR2_CLL                    (0x00000001UL <<  0U)  /* Enables and locks the LOCKUP (Hardfault) output of CortexM0 with Break Input of TIMER1 */
#define CHIP_REGFLD_SYSCFG_CFGR2_SPL                    (0x00000001UL <<  1U)  /* Enables and locks the SRAM_PARITY error signal with Break Input of TIMER1 */
#define CHIP_REGFLD_SYSCFG_CFGR2_PVDL                   (0x00000001UL <<  2U)  /* Enables and locks the PVD connection with Timer1 Break Input and also the PVD_EN and PVDSEL[2:0] bits of the Power Control Interface */
#define CHIP_REGFLD_SYSCFG_CFGR2_ECCL                   (0x00000001UL <<  3U)  /* ECCL */
#define CHIP_REGFLD_SYSCFG_CFGR2_SRAM_PEF               (0x00000001UL <<  8U)  /* SRAM Parity error flag  */
#define CHIP_REGFLD_SYSCFG_CFGR2_PA1_CDEN               (0x00000001UL << 16U)  /* PA[1] Clamping Diode Enable */
#define CHIP_REGFLD_SYSCFG_CFGR2_PA3_CDEN               (0x00000001UL << 17U)  /* PA[3] Clamping Diode Enable */
#define CHIP_REGFLD_SYSCFG_CFGR2_PA5_CDEN               (0x00000001UL << 18U)  /* PA[5] Clamping Diode Enable */
#define CHIP_REGFLD_SYSCFG_CFGR2_PA6_CDEN               (0x00000001UL << 19U)  /* PA[6] Clamping Diode Enable */
#define CHIP_REGFLD_SYSCFG_CFGR2_PA13_CDEN              (0x00000001UL << 20U)  /* PA[13] Clamping Diode Enable */
#define CHIP_REGFLD_SYSCFG_CFGR2_PB0_CDEN               (0x00000001UL << 21U)  /* PB[0] Clamping Diode Enable */
#define CHIP_REGFLD_SYSCFG_CFGR2_PB1_CDEN               (0x00000001UL << 22U)  /* PB[1] Clamping Diode Enable */
#define CHIP_REGFLD_SYSCFG_CFGR2_PB2_CDEN               (0x00000001UL << 23U)  /* PB[2] Clamping Diode Enable */

/*****************  Bit definition for SYSCFG_ITLINEx ISR Wrapper register  ****************/
#define CHIP_REGFLD_SYSCFG_ITLINE0_WWDG                 (0x00000001UL <<  0U)  /* EWDG interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE1_PVDOUT               (0x00000001UL <<  0U)  /* Power voltage detection -> exti[16] Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE1_PVMOUT               (0x00000001UL <<  1U)  /*  VDDUSB Power voltage monitor -> exti[34] Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE2_TAMP                 (0x00000001UL <<  0U)  /* TAMPER -> exti[21] interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE2_RTC                  (0x00000001UL <<  1U)  /* RTC -> exti[19] interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE3_FLASH_ITF            (0x00000001UL <<  0U)  /* FLASH ITF interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE3_FLASH_ECC            (0x00000001UL <<  1U)  /* Flash ECC interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE4_RCC                  (0x00000001UL <<  0U)  /* RCC interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE4_CRS                  (0x00000001UL <<  1U)  /* CRS interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE5_EXTI0                (0x00000001UL <<  0U)  /* External Interrupt 0 */
#define CHIP_REGFLD_SYSCFG_ITLINE5_EXTI1                (0x00000001UL <<  1U)  /* External Interrupt 1 */

#define CHIP_REGFLD_SYSCFG_ITLINE6_EXTI2                (0x00000001UL <<  0U)  /* External Interrupt 2 */
#define CHIP_REGFLD_SYSCFG_ITLINE6_EXTI3                (0x00000001UL <<  1U)  /* External Interrupt 3 */

#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI4                (0x00000001UL <<  0U)  /* External Interrupt 4 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI5                (0x00000001UL <<  1U)  /* External Interrupt 5 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI6                (0x00000001UL <<  2U)  /* External Interrupt 6 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI7                (0x00000001UL <<  3U)  /* External Interrupt 7 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI8                (0x00000001UL <<  4U)  /* External Interrupt 8 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI9                (0x00000001UL <<  5U)  /* External Interrupt 9 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI10               (0x00000001UL <<  6U)  /* External Interrupt 10 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI11               (0x00000001UL <<  7U)  /* External Interrupt 11 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI12               (0x00000001UL <<  8U)  /* External Interrupt 12 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI13               (0x00000001UL <<  9U)  /* External Interrupt 13 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI14               (0x00000001UL << 10U)  /* External Interrupt 14 */
#define CHIP_REGFLD_SYSCFG_ITLINE7_EXTI15               (0x00000001UL << 11U)  /* External Interrupt 15 */

#define CHIP_REGFLD_SYSCFG_ITLINE8_UCPD1                (0x00000001UL <<  0U)  /* UCPD1 -> exti[32] Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE8_UCPD2                (0x00000001UL <<  1U)  /* UCPD2 -> exti[33] Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE8_USB                  (0x00000001UL <<  2U)  /* USB Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE9_DMA1_CH1             (0x00000001UL <<  0U)  /* DMA1 Channel 1 Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE10_DMA1_CH2            (0x00000001UL <<  0U)  /* DMA1 Channel 2 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE10_DMA1_CH3            (0x00000001UL <<  1U)  /* DMA2 Channel 3 Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE11_DMAMUX1             (0x00000001UL <<  0U)  /* DMAMUX Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH4            (0x00000001UL <<  1U)  /* DMA1 Channel 4 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH5            (0x00000001UL <<  2U)  /* DMA1 Channel 5 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH6            (0x00000001UL <<  3U)  /* DMA1 Channel 6 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA1_CH7            (0x00000001UL <<  4U)  /* DMA1 Channel 7 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH1            (0x00000001UL <<  5U)  /* DMA2 Channel 1 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH2            (0x00000001UL <<  6U)  /* DMA2 Channel 2 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH3            (0x00000001UL <<  7U)  /* DMA2 Channel 3 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH4            (0x00000001UL <<  8U)  /* DMA2 Channel 4 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE11_DMA2_CH5            (0x00000001UL <<  9U)  /* DMA2 Channel 5 Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE12_ADC                 (0x00000001UL <<  0U)  /* ADC Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE12_COMP1               (0x00000001UL <<  1U)  /* COMP1 Interrupt -> exti[17] */
#define CHIP_REGFLD_SYSCFG_ITLINE12_COMP2               (0x00000001UL <<  2U)  /* COMP2 Interrupt -> exti[18] */
#define CHIP_REGFLD_SYSCFG_ITLINE12_COMP3               (0x00000001UL <<  3U)  /* COMP3 Interrupt -> exti[20] */

#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_CCU            (0x00000001UL <<  0U)  /* TIM1 CCU Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_TRG            (0x00000001UL <<  1U)  /* TIM1 TRG Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_UPD            (0x00000001UL <<  2U)  /* TIM1 UPD Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE13_TIM1_BRK            (0x00000001UL <<  3U)  /* TIM1 BRK Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE14_TIM1_CC             (0x00000001UL <<  0U)  /* TIM1 CC Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE15_TIM2                (0x00000001UL <<  0U)  /* TIM2 GLB Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE16_TIM3                (0x00000001UL <<  0U)  /* TIM3 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE16_TIM4                (0x00000001UL <<  1U)  /* TIM4 GLB Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE17_TIM6                (0x00000001UL <<  0U)  /* TIM6 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE17_DAC                 (0x00000001UL <<  1U)  /* DAC Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE17_LPTIM1              (0x00000001UL <<  2U)  /* LPTIM1 -> exti[29] Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE18_TIM7                (0x00000001UL <<  0U)  /* TIM7 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE18_LPTIM2              (0x00000001UL <<  1U)  /* LPTIM2 -> exti[30] Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE19_TIM14               (0x00000001UL <<  0U)  /* TIM14 GLB Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE20_TIM15               (0x00000001UL <<  0U)  /* TIM15 GLB Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE21_TIM16               (0x00000001UL <<  0U)  /* TIM16 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE21_FDCAN1_IT0          (0x00000001UL <<  1U)  /* FDCAN1 IT0 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE21_FDCAN2_IT0          (0x00000001UL <<  2U)  /* FDCAN2 IT0 Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE22_TIM17               (0x00000001UL <<  0U)  /* TIM17 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE22_FDCAN1_IT1          (0x00000001UL <<  1U)  /* FDCAN1 IT1 Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE22_FDCAN2_IT1          (0x00000001UL <<  2U)  /* FDCAN2 IT1 Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE23_I2C1                (0x00000001UL <<  0U)  /* I2C1 GLB Interrupt -> exti[23] */

#define CHIP_REGFLD_SYSCFG_ITLINE24_I2C2                (0x00000001UL <<  0U)  /* I2C2 GLB Interrupt  -> exti[22]*/
#define CHIP_REGFLD_SYSCFG_ITLINE24_I2C3                (0x00000001UL <<  1U)  /* I2C3 GLB Interrupt  -> exti[24]*/

#define CHIP_REGFLD_SYSCFG_ITLINE25_SPI1                (0x00000001UL <<  0U)  /* SPI1 Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE26_SPI2                (0x00000001UL <<  0U)  /* SPI2  Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE26_SPI3                (0x00000001UL <<  1U)  /* SPI3  Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE27_USART1              (0x00000001UL <<  0U) /* USART1 GLB Interrupt -> exti[25] */

#define CHIP_REGFLD_SYSCFG_ITLINE28_USART2              (0x00000001UL <<  0U) /* USART2 GLB Interrupt -> exti[26] */
#define CHIP_REGFLD_SYSCFG_ITLINE28_LPUART2             (0x00000001UL <<  1U) /* LPUART2 GLB Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE29_USART3              (0x00000001UL <<  0U) /* USART3 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART4              (0x00000001UL <<  1U) /* USART4 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE29_LPUART1             (0x00000001UL <<  2U) /* LPUART1 GLB Interrupt -> exti[28] */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART5              (0x00000001UL <<  3U) /* USART5 GLB Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE29_USART6              (0x00000001UL <<  4U) /* USART6 GLB Interrupt */

#define CHIP_REGFLD_SYSCFG_ITLINE30_CEC                 (0x00000001UL <<  0U) /* CEC Interrupt-> exti[27] */

#define CHIP_REGFLD_SYSCFG_ITLINE31_RNG                 (0x00000001UL <<  0U) /* RNG Interrupt */
#define CHIP_REGFLD_SYSCFG_ITLINE31_AES                 (0x00000001UL <<  1U) /* AES Interrupt */

#endif // CHIP_STM32G0XX_REGS_SYSCFG_H
