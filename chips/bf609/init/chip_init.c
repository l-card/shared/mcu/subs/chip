#include "chip.h"


#define CHIP_T_NS(val)  val

#ifndef MIN
    #define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef MAX
    #define MAX(a, b) (((a) < (b)) ? (b) : (a))
#endif

#define TIME_NS_CLOCKS(fclk, ns)    (((((fclk) / 1000000)*(CHIP_T_NS(ns)))+999)/1000)
#define TIME_CLK_PARAM(fclk, param, adj) MAX(MAX(TIME_NS_CLOCKS(fclk, param##_NS), param##_CYCLES) - (adj), 0)
#define TIME_DMC_DVAL(param)       TIME_CLK_PARAM(CHIP_CLKF_DCLK, param, 0)
#define TIME_SMC_DVAL(param)       TIME_CLK_PARAM(CHIP_CLKF_SCLK0, param, 0)
#define TIME_DMC_PARAM(param)      ((int)TIME_DMC_DVAL(param))
#define TIME_SMC_PARAM(param)      ((int)TIME_SMC_DVAL(param))


#define BF_SET_OV(mask, val) BF_SET(mask, MIN(val, ((mask) / BF_LSB(mask))))


#if CHIP_DMC_INIT
    #if !defined CHIP_DMC_TMRD_CYCLES && !defined CHIP_DMC_TMRD_NS
        #error CHIP_DMC_TMRD_CYCLES or CHIP_DMC_TMRD_NS must be specified
    #elif !defined CHIP_DMC_TMRD_CYCLES
        #define CHIP_DMC_TMRD_CYCLES 0
    #elif !defined CHIP_DMC_TMRD_NS
        #define CHIP_DMC_TMRD_NS 0
    #endif

    #if !defined CHIP_DMC_TRC_CYCLES && !defined CHIP_DMC_TRC_NS
        #error CHIP_DMC_TRC_CYCLES or CHIP_DMC_TRC_NS must be specified
    #elif !defined CHIP_DMC_TRC_CYCLES
        #define CHIP_DMC_TRC_CYCLES 0
    #elif !defined CHIP_DMC_TRC_NS
        #define CHIP_DMC_TRC_NS 0
    #endif

    #if !defined CHIP_DMC_TRAS_CYCLES && !defined CHIP_DMC_TRAS_NS
        #error CHIP_DMC_TRAS_CYCLES or CHIP_DMC_TRAS_NS must be specified
    #elif !defined CHIP_DMC_TRAS_CYCLES
        #define CHIP_DMC_TRAS_CYCLES 0
    #elif !defined CHIP_DMC_TRAS_NS
        #define CHIP_DMC_TRAS_NS 0
    #endif

    #if !defined CHIP_DMC_TRP_CYCLES && !defined CHIP_DMC_TRP_NS
        #error CHIP_DMC_TRP_CYCLES or CHIP_DMC_TRP_NS must be specified
    #elif !defined CHIP_DMC_TRP_CYCLES
        #define CHIP_DMC_TRP_CYCLES 0
    #elif !defined CHIP_DMC_TRP_NS
        #define CHIP_DMC_TRP_NS 0
    #endif

    #if !defined CHIP_DMC_TWTR_CYCLES && !defined CHIP_DMC_TWTR_NS
        #error CHIP_DMC_TWTR_CYCLES or CHIP_DMC_TWTR_NS must be specified
    #elif !defined CHIP_DMC_TWTR_CYCLES
        #define CHIP_DMC_TWTR_CYCLES 0
    #elif !defined CHIP_DMC_TWTR_NS
        #define CHIP_DMC_TWTR_NS 0
    #endif

    #if !defined CHIP_DMC_TRCD_CYCLES && !defined CHIP_DMC_TRCD_NS
        #error CHIP_DMC_TRCD_CYCLES or CHIP_DMC_TRCD_NS must be specified
    #elif !defined CHIP_DMC_TRCD_CYCLES
        #define CHIP_DMC_TRCD_CYCLES 0
    #elif !defined CHIP_DMC_TRCD_NS
        #define CHIP_DMC_TRCD_NS 0
    #endif

    #if !defined CHIP_DMC_TRRD_CYCLES && !defined CHIP_DMC_TRRD_NS
        #error CHIP_DMC_TRRD_CYCLES or CHIP_DMC_TRRD_NS must be specified
    #elif !defined CHIP_DMC_TRRD_CYCLES
        #define CHIP_DMC_TRRD_CYCLES 0
    #elif !defined CHIP_DMC_TRRD_NS
        #define CHIP_DMC_TRRD_NS 0
    #endif

    #if !defined CHIP_DMC_TRFC_CYCLES && !defined CHIP_DMC_TRFC_NS
        #error CHIP_DMC_TRFC_CYCLES or CHIP_DMC_TRFC_NS must be specified
    #elif !defined CHIP_DMC_TRFC_CYCLES
        #define CHIP_DMC_TRFC_CYCLES 0
    #elif !defined CHIP_DMC_TRFC_NS
        #define CHIP_DMC_TRFC_NS 0
    #endif

    #if !defined CHIP_DMC_TREF_CYCLES && !defined CHIP_DMC_TREF_NS
        #error CHIP_DMC_TREF_CYCLES or CHIP_DMC_TREF_NS must be specified
    #elif !defined CHIP_DMC_TREF_CYCLES
        #define CHIP_DMC_TREF_CYCLES 0
    #elif !defined CHIP_DMC_TREF_NS
        #define CHIP_DMC_TREF_NS 0
    #endif

    #if !defined CHIP_DMC_TCKE_CYCLES && !defined CHIP_DMC_TCKE_NS
        #error CHIP_DMC_TCKE_CYCLES or CHIP_DMC_TCKE_NS must be specified
    #elif !defined CHIP_DMC_TCKE_CYCLES
        #define CHIP_DMC_TCKE_CYCLES 0
    #elif !defined CHIP_DMC_TCKE_NS
        #define CHIP_DMC_TCKE_NS 0
    #endif

    #if !defined CHIP_DMC_TXP_CYCLES && !defined CHIP_DMC_TXP_NS
        #error CHIP_DMC_TXP_CYCLES or CHIP_DMC_TXP_NS must be specified
    #elif !defined CHIP_DMC_TXP_CYCLES
        #define CHIP_DMC_TXP_CYCLES 0
    #elif !defined CHIP_DMC_TXP_NS
        #define CHIP_DMC_TXP_NS 0
    #endif

    #if !defined CHIP_DMC_TWR_CYCLES && !defined CHIP_DMC_TWR_NS
        #error CHIP_DMC_TWR_CYCLES or CHIP_DMC_TWR_NS must be specified
    #elif !defined CHIP_DMC_TWR_CYCLES
        #define CHIP_DMC_TWR_CYCLES 0
    #elif !defined CHIP_DMC_TWR_NS
        #define CHIP_DMC_TWR_NS 0
    #endif

    #if !defined CHIP_DMC_TRTP_CYCLES && !defined CHIP_DMC_TRTP_NS
        #error CHIP_DMC_TRTP_CYCLES or CHIP_DMC_TRTP_NS must be specified
    #elif !defined CHIP_DMC_TRTP_CYCLES
        #define CHIP_DMC_TRTP_CYCLES CHIP_DMC_TRTP_CYCLES_MIN
    #elif !defined CHIP_DMC_TRTP_NS
        #define CHIP_DMC_TRTP_NS 0        
    #endif

    #if CHIP_DMC_TRTP_CYCLES < CHIP_DMC_TRTP_CYCLES_MIN
        #error CHIP_DMC_TRTP_CYCLES must be minimum CHIP_DMC_TRTP_CYCLES_MIN cycles
    #endif

    #if !defined CHIP_DMC_TFAW_CYCLES && !defined CHIP_DMC_TFAW_NS
        #error CHIP_DMC_TFAW_CYCLES or CHIP_DMC_TFAW_NS must be specified
    #elif !defined CHIP_DMC_TFAW_CYCLES
        #define CHIP_DMC_TFAW_CYCLES 0
    #elif !defined CHIP_DMC_TFAW_NS
        #define CHIP_DMC_TFAW_NS 0
    #endif




    #define DMC_WAIT_COND(cond, dmc_err) do { \
            int done = 0; \
            for (size_t i = 0; (i < CHIP_CLKF_CCLK/10) && !done; i++) { \
                if (cond) \
                    done = 1;\
            } \
            if (!done) \
                dmc_err = 1; \
        } while(0)

    #if CHIP_DMC_SDRAM_SIZE == 8
        #define DMC_SDRSIZE_CODE  ENUM_DMC_CFG_SDRSIZE64
    #elif CHIP_DMC_SDRAM_SIZE == 16
        #define DMC_SDRSIZE_CODE  ENUM_DMC_CFG_SDRSIZE128
    #elif CHIP_DMC_SDRAM_SIZE == 32
        #define DMC_SDRSIZE_CODE  ENUM_DMC_CFG_SDRSIZE256
    #elif CHIP_DMC_SDRAM_SIZE == 64
        #define DMC_SDRSIZE_CODE  ENUM_DMC_CFG_SDRSIZE512
    #elif CHIP_DMC_SDRAM_SIZE == 128
        #define DMC_SDRSIZE_CODE  ENUM_DMC_CFG_SDRSIZE1G
    #elif CHIP_DMC_SDRAM_SIZE == 256
        #define DMC_SDRSIZE_CODE  ENUM_DMC_CFG_SDRSIZE2G
    #else
        #error invalid DMC_SDRSIZE_CODE specified
    #endif

    #if (((CHIP_DMC_SDRAM_SIZE == 8) || (CHIP_DMC_SDRAM_SIZE == 16)) && !CHIP_DMC_LPDDR)
        #error 64Mbit and 128Mbit SDRAM supported only in LPDDR mode
    #endif

    #if CHIP_DMC_BURST_LEN == 4
        #define DMC_BLEN_CODE  ENUM_DMC_MR_BLEN4
    #elif CHIP_DMC_BURST_LEN == 8
        #define DMC_BLEN_CODE  ENUM_DMC_MR_BLEN8
    #else
        #error invalid CHIP_DMC_BURST_LEN value (must be 4 or 8)
    #endif

    #if CHIP_DMC_RTT  == 0
        #define DMC_RTT_CODE 0
    #elif CHIP_DMC_RTT == 50
        #define DMC_RTT_CODE (BITM_DMC_EMR1_RTT1 | BITM_DMC_EMR1_RTT0)
    #elif CHIP_DMC_RTT == 75
        #define DMC_RTT_CODE (BITM_DMC_EMR1_RTT0)
    #elif CHIP_DMC_RTT == 150
        #define DMC_RTT_CODE (BITM_DMC_EMR1_RTT1)
    #else
        #error invalid CHIP_DMC_RTT value. must be 0, 50, 75 or 150
    #endif


#endif



#if CHIP_SMC_INIT
    #ifndef CHIP_SMC_ADDR_WIDTH
        #define CHIP_SMC_ADDR_WIDTH 26
    #endif
    #if CHIP_SMC_ADDR_WIDTH > 26
        #error invalid CHIP_SMC_ADDR_WIDTH value
    #endif
    #ifndef CHIP_SMC_B0_EN
        #define CHIP_SMC_B0_EN 0
    #endif
    #ifndef CHIP_SMC_B1_EN
        #define CHIP_SMC_B1_EN 0
    #endif
    #ifndef CHIP_SMC_B2_EN
        #define CHIP_SMC_B2_EN 0
    #endif
    #ifndef CHIP_SMC_B3_EN
        #define CHIP_SMC_B3_EN 0
    #endif

    #if CHIP_SMC_BYTE_OP_EN && (CHIP_SMC_B2_EN  || CHIP_SMC_B3_EN)
        #error cannot enable byte ops and banks 2 or 3 simultaneously
    #endif

    #ifndef CHIP_SMC_RAT_NS
        #define CHIP_SMC_RAT_NS 0
    #endif
    #ifndef CHIP_SMC_RAT_CYCLES
        #define CHIP_SMC_RAT_CYCLES 0
    #endif
    #ifndef CHIP_SMC_RHT_NS
        #define CHIP_SMC_RHT_NS 0
    #endif
    #ifndef CHIP_SMC_RHT_CYCLES
        #define CHIP_SMC_RHT_CYCLES 0
    #endif
    #ifndef CHIP_SMC_RST_NS
        #define CHIP_SMC_RST_NS 0
    #endif
    #ifndef CHIP_SMC_RST_CYCLES
        #define CHIP_SMC_RST_CYCLES 0
    #endif
    #ifndef CHIP_SMC_WAT_NS
        #define CHIP_SMC_WAT_NS 0
    #endif
    #ifndef CHIP_SMC_WAT_CYCLES
        #define CHIP_SMC_WAT_CYCLES 0
    #endif
    #ifndef CHIP_SMC_WHT_NS
        #define CHIP_SMC_WHT_NS 0
    #endif
    #ifndef CHIP_SMC_WHT_CYCLES
        #define CHIP_SMC_WHT_CYCLES 0
    #endif
    #ifndef CHIP_SMC_WST_NS
        #define CHIP_SMC_WST_NS 0
    #endif
    #ifndef CHIP_SMC_WST_CYCLES
        #define CHIP_SMC_WST_CYCLES 0
    #endif
    #ifndef CHIP_SMC_PGWS_NS
        #define CHIP_SMC_PGWS_NS 0
    #endif
    #ifndef CHIP_SMC_PGWS_CYCLES
        #define CHIP_SMC_PGWS_CYCLES 0
    #endif
    #ifndef CHIP_SMC_IT_NS
        #define CHIP_SMC_IT_NS 0
    #endif
    #ifndef CHIP_SMC_IT_CYCLES
        #define CHIP_SMC_IT_CYCLES 0
    #endif
    #ifndef CHIP_SMC_TT_NS
        #define CHIP_SMC_TT_NS 0
    #endif
    #ifndef CHIP_SMC_TT_CYCLES
        #define CHIP_SMC_TT_CYCLES 0
    #endif
    #ifndef CHIP_SMC_PREAT_NS
        #define CHIP_SMC_PREAT_NS 0
    #endif
    #ifndef CHIP_SMC_PREAT_CYCLES
        #define CHIP_SMC_PREAT_CYCLES 0
    #endif
    #ifndef CHIP_SMC_PREST_NS
        #define CHIP_SMC_PREST_NS 0
    #endif
    #ifndef CHIP_SMC_PREST_CYCLES
        #define CHIP_SMC_PREST_CYCLES 0
    #endif




    #if CHIP_SMC_B0_EN
        #ifndef CHIP_SMC_B0_BURST_TYPE_SEQ
            #define CHIP_SMC_B0_BURST_TYPE_SEQ CHIP_SMC_BURST_TYPE_SEQ
        #endif
        #ifndef CHIP_SMC_B0_BCLK_DIV
            #define CHIP_SMC_B0_BCLK_DIV CHIP_SMC_BCLK_DIV
        #endif
        #ifndef CHIP_SMC_B0_PAGE_SIZE
            #define CHIP_SMC_B0_PAGE_SIZE CHIP_SMC_PAGE_SIZE
        #endif
        #ifndef CHIP_SMC_B0_RDY_EN
            #define CHIP_SMC_B0_RDY_EN CHIP_SMC_RDY_EN
        #endif
        #ifndef CHIP_SMC_B0_RDY_POL
            #define CHIP_SMC_B0_RDY_POL CHIP_SMC_RDY_POL
        #endif
        #ifndef CHIP_SMC_B0_SELCTRL
            #define CHIP_SMC_B0_SELCTRL CHIP_SMC_SELCTRL
        #endif
        #ifndef CHIP_SMC_B0_MODE
            #define CHIP_SMC_B0_MODE CHIP_SMC_MODE
        #endif

        #if CHIP_SMC_B0_PAGE_SIZE  == 4
            #define CHIP_SMC_B0_PAGE_SIZE_CODE  0
        #elif CHIP_SMC_B0_PAGE_SIZE == 8
            #define CHIP_SMC_B0_PAGE_SIZE_CODE  1
        #elif CHIP_SMC_B0_PAGE_SIZE == 16
            #define CHIP_SMC_B0_PAGE_SIZE_CODE  2
        #else
            #error invalid CHIP_SMC_B0_PAGE_SIZE_CODE (must be 4, 8 or 16)
        #endif

        #if (CHIP_SMC_B0_SELCTRL != SMC_SELCTRL_AMS_ONLY) && \
            (CHIP_SMC_B0_SELCTRL != SMC_SELCTRL_AMS_OR_ARE) && \
            (CHIP_SMC_B0_SELCTRL != SMC_SELCTRL_AMS_OR_AOE) && \
            (CHIP_SMC_B0_SELCTRL != SMC_SELCTRL_AMS_OR_AWE)
            #error invalid CHIP_SMC_B0_SELCTRL value
        #endif

        #if (CHIP_SMC_B0_MODE != SMC_MODE_ASYNC_SRAM) && \
            (CHIP_SMC_B0_MODE != SMC_MODE_ASYNC_FLASH) && \
            (CHIP_SMC_B0_MODE != SMC_MODE_ASYNC_FLASH_PAGE) && \
            (CHIP_SMC_B0_MODE != SMC_MODE_SYNC_BURST_FLASH)
            #error invalid CHIP_SMC_B0_MODE value
        #endif

        #if !defined CHP_SMC_NOORCLK_EN && (CHIP_SMC_B0_MODE == SMC_MODE_SYNC_BURST_FLASH)
            #define CHP_SMC_NOORCLK_EN 1
        #endif


        #ifndef CHIP_SMC_B0_RAT_NS
            #define CHIP_SMC_B0_RAT_NS CHIP_SMC_RAT_NS
        #endif
        #ifndef CHIP_SMC_B0_RAT_CYCLES
            #define CHIP_SMC_B0_RAT_CYCLES CHIP_SMC_RAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_RHT_NS
            #define CHIP_SMC_B0_RHT_NS CHIP_SMC_RHT_NS
        #endif
        #ifndef CHIP_SMC_B0_RHT_CYCLES
            #define CHIP_SMC_B0_RHT_CYCLES CHIP_SMC_RHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_RST_NS
            #define CHIP_SMC_B0_RST_NS CHIP_SMC_RST_NS
        #endif
        #ifndef CHIP_SMC_B0_RST_CYCLES
            #define CHIP_SMC_B0_RST_CYCLES CHIP_SMC_RST_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_WAT_NS
            #define CHIP_SMC_B0_WAT_NS CHIP_SMC_WAT_NS
        #endif
        #ifndef CHIP_SMC_B0_WAT_CYCLES
            #define CHIP_SMC_B0_WAT_CYCLES CHIP_SMC_WAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_WHT_NS
            #define CHIP_SMC_B0_WHT_NS CHIP_SMC_WHT_NS
        #endif
        #ifndef CHIP_SMC_B0_WHT_CYCLES
            #define CHIP_SMC_B0_WHT_CYCLES CHIP_SMC_WHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_WST_NS
            #define CHIP_SMC_B0_WST_NS CHIP_SMC_WST_NS
        #endif
        #ifndef CHIP_SMC_B0_WST_CYCLES
            #define CHIP_SMC_B0_WST_CYCLES CHIP_SMC_WST_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_PGWS_NS
            #define CHIP_SMC_B0_PGWS_NS CHIP_SMC_PGWS_NS
        #endif
        #ifndef CHIP_SMC_B0_PGWS_CYCLES
            #define CHIP_SMC_B0_PGWS_CYCLES CHIP_SMC_PGWS_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_IT_NS
            #define CHIP_SMC_B0_IT_NS CHIP_SMC_IT_NS
        #endif
        #ifndef CHIP_SMC_B0_IT_CYCLES
            #define CHIP_SMC_B0_IT_CYCLES CHIP_SMC_IT_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_TT_NS
            #define CHIP_SMC_B0_TT_NS CHIP_SMC_TT_NS
        #endif
        #ifndef CHIP_SMC_B0_TT_CYCLES
            #define CHIP_SMC_B0_TT_CYCLES CHIP_SMC_TT_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_PREAT_NS
            #define CHIP_SMC_B0_PREAT_NS CHIP_SMC_PREAT_NS
        #endif
        #ifndef CHIP_SMC_B0_PREAT_CYCLES
            #define CHIP_SMC_B0_PREAT_CYCLES CHIP_SMC_PREAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B0_PREST_NS
            #define CHIP_SMC_B0_PREST_NS CHIP_SMC_PREST_NS
        #endif
        #ifndef CHIP_SMC_B0_PREST_CYCLES
            #define CHIP_SMC_B0_PREST_CYCLES CHIP_SMC_PREST_CYCLES
        #endif        
    #endif

    #if CHIP_SMC_B1_EN
        #ifndef CHIP_SMC_B1_BURST_TYPE_SEQ
            #define CHIP_SMC_B1_BURST_TYPE_SEQ CHIP_SMC_BURST_TYPE_SEQ
        #endif
        #ifndef CHIP_SMC_B1_BCLK_DIV
            #define CHIP_SMC_B1_BCLK_DIV CHIP_SMC_BCLK_DIV
        #endif
        #ifndef CHIP_SMC_B1_PAGE_SIZE
            #define CHIP_SMC_B1_PAGE_SIZE CHIP_SMC_PAGE_SIZE
        #endif
        #ifndef CHIP_SMC_B1_RDY_EN
            #define CHIP_SMC_B1_RDY_EN CHIP_SMC_RDY_EN
        #endif
        #ifndef CHIP_SMC_B1_RDY_POL
            #define CHIP_SMC_B1_RDY_POL CHIP_SMC_RDY_POL
        #endif
        #ifndef CHIP_SMC_B1_SELCTRL
            #define CHIP_SMC_B1_SELCTRL CHIP_SMC_SELCTRL
        #endif
        #ifndef CHIP_SMC_B1_MODE
            #define CHIP_SMC_B1_MODE CHIP_SMC_MODE
        #endif

        #if CHIP_SMC_B1_PAGE_SIZE  == 4
            #define CHIP_SMC_B1_PAGE_SIZE_CODE  0
        #elif CHIP_SMC_B1_PAGE_SIZE == 8
            #define CHIP_SMC_B1_PAGE_SIZE_CODE  1
        #elif CHIP_SMC_B1_PAGE_SIZE == 16
            #define CHIP_SMC_B1_PAGE_SIZE_CODE  2
        #else
            #error invalid CHIP_SMC_B1_PAGE_SIZE_CODE (must be 4, 8 or 16)
        #endif

        #if (CHIP_SMC_B1_SELCTRL != SMC_SELCTRL_AMS_ONLY) && \
            (CHIP_SMC_B1_SELCTRL != SMC_SELCTRL_AMS_OR_ARE) && \
            (CHIP_SMC_B1_SELCTRL != SMC_SELCTRL_AMS_OR_AOE) && \
            (CHIP_SMC_B1_SELCTRL != SMC_SELCTRL_AMS_OR_AWE)
            #error invalid CHIP_SMC_B1_SELCTRL value
        #endif

        #if (CHIP_SMC_B1_MODE != SMC_MODE_ASYNC_SRAM) && \
            (CHIP_SMC_B1_MODE != SMC_MODE_ASYNC_FLASH) && \
            (CHIP_SMC_B1_MODE != SMC_MODE_ASYNC_FLASH_PAGE) && \
            (CHIP_SMC_B1_MODE != SMC_MODE_SYNC_BURST_FLASH)
            #error invalid CHIP_SMC_B1_MODE value
        #endif

        #if !defined CHP_SMC_NOORCLK_EN && (CHIP_SMC_B1_MODE == SMC_MODE_SYNC_BURST_FLASH)
            #define CHP_SMC_NOORCLK_EN 1
        #endif


        #ifndef CHIP_SMC_B1_RAT_NS
            #define CHIP_SMC_B1_RAT_NS CHIP_SMC_RAT_NS
        #endif
        #ifndef CHIP_SMC_B1_RAT_CYCLES
            #define CHIP_SMC_B1_RAT_CYCLES CHIP_SMC_RAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_RHT_NS
            #define CHIP_SMC_B1_RHT_NS CHIP_SMC_RHT_NS
        #endif
        #ifndef CHIP_SMC_B1_RHT_CYCLES
            #define CHIP_SMC_B1_RHT_CYCLES CHIP_SMC_RHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_RST_NS
            #define CHIP_SMC_B1_RST_NS CHIP_SMC_RST_NS
        #endif
        #ifndef CHIP_SMC_B1_RST_CYCLES
            #define CHIP_SMC_B1_RST_CYCLES CHIP_SMC_RST_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_WAT_NS
            #define CHIP_SMC_B1_WAT_NS CHIP_SMC_WAT_NS
        #endif
        #ifndef CHIP_SMC_B1_WAT_CYCLES
            #define CHIP_SMC_B1_WAT_CYCLES CHIP_SMC_WAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_WHT_NS
            #define CHIP_SMC_B1_WHT_NS CHIP_SMC_WHT_NS
        #endif
        #ifndef CHIP_SMC_B1_WHT_CYCLES
            #define CHIP_SMC_B1_WHT_CYCLES CHIP_SMC_WHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_WST_NS
            #define CHIP_SMC_B1_WST_NS CHIP_SMC_WST_NS
        #endif
        #ifndef CHIP_SMC_B1_WST_CYCLES
            #define CHIP_SMC_B1_WST_CYCLES CHIP_SMC_WST_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_PGWS_NS
            #define CHIP_SMC_B1_PGWS_NS CHIP_SMC_PGWS_NS
        #endif
        #ifndef CHIP_SMC_B1_PGWS_CYCLES
            #define CHIP_SMC_B1_PGWS_CYCLES CHIP_SMC_PGWS_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_IT_NS
            #define CHIP_SMC_B1_IT_NS CHIP_SMC_IT_NS
        #endif
        #ifndef CHIP_SMC_B1_IT_CYCLES
            #define CHIP_SMC_B1_IT_CYCLES CHIP_SMC_IT_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_TT_NS
            #define CHIP_SMC_B1_TT_NS CHIP_SMC_TT_NS
        #endif
        #ifndef CHIP_SMC_B1_TT_CYCLES
            #define CHIP_SMC_B1_TT_CYCLES CHIP_SMC_TT_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_PREAT_NS
            #define CHIP_SMC_B1_PREAT_NS CHIP_SMC_PREAT_NS
        #endif
        #ifndef CHIP_SMC_B1_PREAT_CYCLES
            #define CHIP_SMC_B1_PREAT_CYCLES CHIP_SMC_PREAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B1_PREST_NS
            #define CHIP_SMC_B1_PREST_NS CHIP_SMC_PREST_NS
        #endif
        #ifndef CHIP_SMC_B1_PREST_CYCLES
            #define CHIP_SMC_B1_PREST_CYCLES CHIP_SMC_PREST_CYCLES
        #endif
    #endif

    #if CHIP_SMC_B2_EN
        #ifndef CHIP_SMC_B2_BURST_TYPE_SEQ
            #define CHIP_SMC_B2_BURST_TYPE_SEQ CHIP_SMC_BURST_TYPE_SEQ
        #endif
        #ifndef CHIP_SMC_B2_BCLK_DIV
            #define CHIP_SMC_B2_BCLK_DIV CHIP_SMC_BCLK_DIV
        #endif
        #ifndef CHIP_SMC_B2_PAGE_SIZE
            #define CHIP_SMC_B2_PAGE_SIZE CHIP_SMC_PAGE_SIZE
        #endif
        #ifndef CHIP_SMC_B2_RDY_EN
            #define CHIP_SMC_B2_RDY_EN CHIP_SMC_RDY_EN
        #endif
        #ifndef CHIP_SMC_B2_RDY_POL
            #define CHIP_SMC_B2_RDY_POL CHIP_SMC_RDY_POL
        #endif
        #ifndef CHIP_SMC_B2_SELCTRL
            #define CHIP_SMC_B2_SELCTRL CHIP_SMC_SELCTRL
        #endif
        #ifndef CHIP_SMC_B2_MODE
            #define CHIP_SMC_B2_MODE CHIP_SMC_MODE
        #endif

        #if CHIP_SMC_B2_PAGE_SIZE  == 4
            #define CHIP_SMC_B2_PAGE_SIZE_CODE  0
        #elif CHIP_SMC_B2_PAGE_SIZE == 8
            #define CHIP_SMC_B2_PAGE_SIZE_CODE  1
        #elif CHIP_SMC_B2_PAGE_SIZE == 16
            #define CHIP_SMC_B2_PAGE_SIZE_CODE  2
        #else
            #error invalid CHIP_SMC_B2_PAGE_SIZE_CODE (must be 4, 8 or 16)
        #endif

        #if (CHIP_SMC_B2_SELCTRL != SMC_SELCTRL_AMS_ONLY) && \
            (CHIP_SMC_B2_SELCTRL != SMC_SELCTRL_AMS_OR_ARE) && \
            (CHIP_SMC_B2_SELCTRL != SMC_SELCTRL_AMS_OR_AOE) && \
            (CHIP_SMC_B2_SELCTRL != SMC_SELCTRL_AMS_OR_AWE)
            #error invalid CHIP_SMC_B2_SELCTRL value
        #endif

        #if (CHIP_SMC_B2_MODE != SMC_MODE_ASYNC_SRAM) && \
            (CHIP_SMC_B2_MODE != SMC_MODE_ASYNC_FLASH) && \
            (CHIP_SMC_B2_MODE != SMC_MODE_ASYNC_FLASH_PAGE) && \
            (CHIP_SMC_B2_MODE != SMC_MODE_SYNC_BURST_FLASH)
            #error invalid CHIP_SMC_B2_MODE value
        #endif

        #if !defined CHP_SMC_NOORCLK_EN && (CHIP_SMC_B2_MODE == SMC_MODE_SYNC_BURST_FLASH)
            #define CHP_SMC_NOORCLK_EN 1
        #endif


        #ifndef CHIP_SMC_B2_RAT_NS
            #define CHIP_SMC_B2_RAT_NS CHIP_SMC_RAT_NS
        #endif
        #ifndef CHIP_SMC_B2_RAT_CYCLES
            #define CHIP_SMC_B2_RAT_CYCLES CHIP_SMC_RAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_RHT_NS
            #define CHIP_SMC_B2_RHT_NS CHIP_SMC_RHT_NS
        #endif
        #ifndef CHIP_SMC_B2_RHT_CYCLES
            #define CHIP_SMC_B2_RHT_CYCLES CHIP_SMC_RHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_RST_NS
            #define CHIP_SMC_B2_RST_NS CHIP_SMC_RST_NS
        #endif
        #ifndef CHIP_SMC_B2_RST_CYCLES
            #define CHIP_SMC_B2_RST_CYCLES CHIP_SMC_RST_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_WAT_NS
            #define CHIP_SMC_B2_WAT_NS CHIP_SMC_WAT_NS
        #endif
        #ifndef CHIP_SMC_B2_WAT_CYCLES
            #define CHIP_SMC_B2_WAT_CYCLES CHIP_SMC_WAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_WHT_NS
            #define CHIP_SMC_B2_WHT_NS CHIP_SMC_WHT_NS
        #endif
        #ifndef CHIP_SMC_B2_WHT_CYCLES
            #define CHIP_SMC_B2_WHT_CYCLES CHIP_SMC_WHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_WST_NS
            #define CHIP_SMC_B2_WST_NS CHIP_SMC_WST_NS
        #endif
        #ifndef CHIP_SMC_B2_WST_CYCLES
            #define CHIP_SMC_B2_WST_CYCLES CHIP_SMC_WST_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_PGWS_NS
            #define CHIP_SMC_B2_PGWS_NS CHIP_SMC_PGWS_NS
        #endif
        #ifndef CHIP_SMC_B2_PGWS_CYCLES
            #define CHIP_SMC_B2_PGWS_CYCLES CHIP_SMC_PGWS_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_IT_NS
            #define CHIP_SMC_B2_IT_NS CHIP_SMC_IT_NS
        #endif
        #ifndef CHIP_SMC_B2_IT_CYCLES
            #define CHIP_SMC_B2_IT_CYCLES CHIP_SMC_IT_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_TT_NS
            #define CHIP_SMC_B2_TT_NS CHIP_SMC_TT_NS
        #endif
        #ifndef CHIP_SMC_B2_TT_CYCLES
            #define CHIP_SMC_B2_TT_CYCLES CHIP_SMC_TT_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_PREAT_NS
            #define CHIP_SMC_B2_PREAT_NS CHIP_SMC_PREAT_NS
        #endif
        #ifndef CHIP_SMC_B2_PREAT_CYCLES
            #define CHIP_SMC_B2_PREAT_CYCLES CHIP_SMC_PREAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B2_PREST_NS
            #define CHIP_SMC_B2_PREST_NS CHIP_SMC_PREST_NS
        #endif
        #ifndef CHIP_SMC_B2_PREST_CYCLES
            #define CHIP_SMC_B2_PREST_CYCLES CHIP_SMC_PREST_CYCLES
        #endif
    #endif

    #if CHIP_SMC_B3_EN
        #ifndef CHIP_SMC_B3_BURST_TYPE_SEQ
            #define CHIP_SMC_B3_BURST_TYPE_SEQ CHIP_SMC_BURST_TYPE_SEQ
        #endif
        #ifndef CHIP_SMC_B3_BCLK_DIV
            #define CHIP_SMC_B3_BCLK_DIV CHIP_SMC_BCLK_DIV
        #endif
        #ifndef CHIP_SMC_B3_PAGE_SIZE
            #define CHIP_SMC_B3_PAGE_SIZE CHIP_SMC_PAGE_SIZE
        #endif
        #ifndef CHIP_SMC_B3_RDY_EN
            #define CHIP_SMC_B3_RDY_EN CHIP_SMC_RDY_EN
        #endif
        #ifndef CHIP_SMC_B3_RDY_POL
            #define CHIP_SMC_B3_RDY_POL CHIP_SMC_RDY_POL
        #endif
        #ifndef CHIP_SMC_B3_SELCTRL
            #define CHIP_SMC_B3_SELCTRL CHIP_SMC_SELCTRL
        #endif
        #ifndef CHIP_SMC_B3_MODE
            #define CHIP_SMC_B3_MODE CHIP_SMC_MODE
        #endif

        #if CHIP_SMC_B3_PAGE_SIZE  == 4
            #define CHIP_SMC_B3_PAGE_SIZE_CODE  0
        #elif CHIP_SMC_B3_PAGE_SIZE == 8
            #define CHIP_SMC_B3_PAGE_SIZE_CODE  1
        #elif CHIP_SMC_B3_PAGE_SIZE == 16
            #define CHIP_SMC_B3_PAGE_SIZE_CODE  2
        #else
            #error invalid CHIP_SMC_B3_PAGE_SIZE_CODE (must be 4, 8 or 16)
        #endif

        #if (CHIP_SMC_B3_SELCTRL != SMC_SELCTRL_AMS_ONLY) && \
            (CHIP_SMC_B3_SELCTRL != SMC_SELCTRL_AMS_OR_ARE) && \
            (CHIP_SMC_B3_SELCTRL != SMC_SELCTRL_AMS_OR_AOE) && \
            (CHIP_SMC_B3_SELCTRL != SMC_SELCTRL_AMS_OR_AWE)
            #error invalid CHIP_SMC_B3_SELCTRL value
        #endif

        #if (CHIP_SMC_B3_MODE != SMC_MODE_ASYNC_SRAM) && \
            (CHIP_SMC_B3_MODE != SMC_MODE_ASYNC_FLASH) && \
            (CHIP_SMC_B3_MODE != SMC_MODE_ASYNC_FLASH_PAGE) && \
            (CHIP_SMC_B3_MODE != SMC_MODE_SYNC_BURST_FLASH)
            #error invalid CHIP_SMC_B3_MODE value
        #endif

        #if !defined CHP_SMC_NOORCLK_EN && (CHIP_SMC_B3_MODE == SMC_MODE_SYNC_BURST_FLASH)
            #define CHP_SMC_NOORCLK_EN 1
        #endif


        #ifndef CHIP_SMC_B3_RAT_NS
            #define CHIP_SMC_B3_RAT_NS CHIP_SMC_RAT_NS
        #endif
        #ifndef CHIP_SMC_B3_RAT_CYCLES
            #define CHIP_SMC_B3_RAT_CYCLES CHIP_SMC_RAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_RHT_NS
            #define CHIP_SMC_B3_RHT_NS CHIP_SMC_RHT_NS
        #endif
        #ifndef CHIP_SMC_B3_RHT_CYCLES
            #define CHIP_SMC_B3_RHT_CYCLES CHIP_SMC_RHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_RST_NS
            #define CHIP_SMC_B3_RST_NS CHIP_SMC_RST_NS
        #endif
        #ifndef CHIP_SMC_B3_RST_CYCLES
            #define CHIP_SMC_B3_RST_CYCLES CHIP_SMC_RST_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_WAT_NS
            #define CHIP_SMC_B3_WAT_NS CHIP_SMC_WAT_NS
        #endif
        #ifndef CHIP_SMC_B3_WAT_CYCLES
            #define CHIP_SMC_B3_WAT_CYCLES CHIP_SMC_WAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_WHT_NS
            #define CHIP_SMC_B3_WHT_NS CHIP_SMC_WHT_NS
        #endif
        #ifndef CHIP_SMC_B3_WHT_CYCLES
            #define CHIP_SMC_B3_WHT_CYCLES CHIP_SMC_WHT_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_WST_NS
            #define CHIP_SMC_B3_WST_NS CHIP_SMC_WST_NS
        #endif
        #ifndef CHIP_SMC_B3_WST_CYCLES
            #define CHIP_SMC_B3_WST_CYCLES CHIP_SMC_WST_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_PGWS_NS
            #define CHIP_SMC_B3_PGWS_NS CHIP_SMC_PGWS_NS
        #endif
        #ifndef CHIP_SMC_B3_PGWS_CYCLES
            #define CHIP_SMC_B3_PGWS_CYCLES CHIP_SMC_PGWS_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_IT_NS
            #define CHIP_SMC_B3_IT_NS CHIP_SMC_IT_NS
        #endif
        #ifndef CHIP_SMC_B3_IT_CYCLES
            #define CHIP_SMC_B3_IT_CYCLES CHIP_SMC_IT_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_TT_NS
            #define CHIP_SMC_B3_TT_NS CHIP_SMC_TT_NS
        #endif
        #ifndef CHIP_SMC_B3_TT_CYCLES
            #define CHIP_SMC_B3_TT_CYCLES CHIP_SMC_TT_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_PREAT_NS
            #define CHIP_SMC_B3_PREAT_NS CHIP_SMC_PREAT_NS
        #endif
        #ifndef CHIP_SMC_B3_PREAT_CYCLES
            #define CHIP_SMC_B3_PREAT_CYCLES CHIP_SMC_PREAT_CYCLES
        #endif
        #ifndef CHIP_SMC_B3_PREST_NS
            #define CHIP_SMC_B3_PREST_NS CHIP_SMC_PREST_NS
        #endif
        #ifndef CHIP_SMC_B3_PREST_CYCLES
            #define CHIP_SMC_B3_PREST_CYCLES CHIP_SMC_PREST_CYCLES
        #endif
    #endif
    #define SMC_BANK_CONFIG(num) do { \
        *pREG_SMC0_B##num##CTL = \
            BF_SET(BITM_SMC_B##num##CTL_BTYPE,     CHIP_SMC_B##num##_BURST_TYPE_SEQ) | \
            BF_SET(BITM_SMC_B##num##CTL_BCLK,      CHIP_SMC_B##num##_BCLK_DIV-1) | \
            BF_SET(BITM_SMC_B##num##CTL_PGSZ,      CHIP_SMC_B##num##_PAGE_SIZE_CODE) | \
            BF_SET(BITM_SMC_B##num##CTL_RDYABTEN,  CHIP_SMC_B##num##_RDY_EN) | \
            BF_SET(BITM_SMC_B##num##CTL_RDYPOL,    CHIP_SMC_B##num##_RDY_POL) | \
            BF_SET(BITM_SMC_B##num##CTL_RDYEN,     CHIP_SMC_B##num##_RDY_EN) | \
            BF_SET(BITM_SMC_B##num##CTL_SELCTRL,   CHIP_SMC_B##num##_SELCTRL) | \
            BF_SET(BITM_SMC_B##num##CTL_MODE,      CHIP_SMC_B##num##_MODE) | \
            BF_SET(BITM_SMC_B##num##CTL_EN,        CHIP_SMC_B##num##_EN); \
        *pREG_SMC0_B##num##TIM = \
            BF_SET_OV(BITM_SMC_B##num##TIM_RAT, MAX(1, TIME_SMC_PARAM(CHIP_SMC_B##num##_RAT))) | \
            BF_SET_OV(BITM_SMC_B##num##TIM_RHT, TIME_SMC_PARAM(CHIP_SMC_B##num##_RHT)) | \
            BF_SET(BITM_SMC_B##num##TIM_RST, TIME_SMC_PARAM(CHIP_SMC_B##num##_RST) >= 8 \
                    ? 0 : MAX(1, TIME_SMC_PARAM(CHIP_SMC_B##num##_RST))) | \
            BF_SET_OV(BITM_SMC_B##num##TIM_WAT, MAX(1, TIME_SMC_PARAM(CHIP_SMC_B##num##_WAT))) | \
            BF_SET_OV(BITM_SMC_B##num##TIM_WHT, TIME_SMC_PARAM(CHIP_SMC_B##num##_WHT)) | \
            BF_SET(BITM_SMC_B##num##TIM_WST, TIME_SMC_PARAM(CHIP_SMC_B##num##_WST) >= 8 \
                    ? 0 : MAX(1, TIME_SMC_PARAM(CHIP_SMC_B##num##_WST))); \
        *pREG_SMC0_B##num##ETIM = \
            BF_SET_OV(BITM_SMC_B##num##ETIM_PGWS, MAX(1, TIME_SMC_PARAM(CHIP_SMC_B##num##_PGWS))) | \
            BF_SET_OV(BITM_SMC_B##num##ETIM_IT,   TIME_SMC_PARAM(CHIP_SMC_B##num##_IT)) | \
            BF_SET_OV(BITM_SMC_B##num##ETIM_TT,   MAX(1, TIME_SMC_PARAM(CHIP_SMC_B##num##_TT))) | \
            BF_SET_OV(BITM_SMC_B##num##ETIM_PREAT, TIME_SMC_PARAM(CHIP_SMC_B##num##_PREAT)) | \
            BF_SET_OV(BITM_SMC_B##num##ETIM_PREST, TIME_SMC_PARAM(CHIP_SMC_B##num##_PREST)); \
        } while(0)

#endif







#ifdef CHIP_INITCODE
    /* если файл используется в initcode, то в файле должна быть только одна
       функция (в противном случае неизвестно, какая будет вызвана),
       соответственно все остальные функции должны быть inline */
    __attribute__((always_inline)) static inline
#endif
void chip_init(void) {

#if CHIP_DMC_INIT
    int dmc_err = 0;    
    /* если DMC был иницилизирован, нужно перевести DDR в selfrefresh */
    if (*pREG_DMC0_STAT & BITM_DMC_STAT_MEMINITDONE) {
        *pREG_DMC0_CTL |= BITM_DMC_CTL_SRREQ;
        DMC_WAIT_COND(*pREG_DMC0_STAT & BITM_DMC_STAT_SRACK, dmc_err);
    }
#endif

#if CHIP_WDT_EN
    *pREG_WDOG0_CTL = BF_SET(BITM_WDOG_CTL_WDEN, 0xAD);
#endif

#if CHIP_PLL_INIT
    while (*pREG_CGU0_STAT & BITM_CGU_STAT_CLKSALGN) {}
    *pREG_CGU0_DIV =  BF_SET(BITM_CGU_DIV_OSEL,  CHIP_CGU_DIV_OSEL_VAL)  |
                      BF_SET(BITM_CGU_DIV_DSEL,  CHIP_CGU_DIV_DSEL_VAL)  |
                      BF_SET(BITM_CGU_DIV_S1SEL, CHIP_CGU_DIV_S1SEL_VAL) |
                      BF_SET(BITM_CGU_DIV_SYSSEL,CHIP_CGU_DIV_SYSSEL_VAL) |
                      BF_SET(BITM_CGU_DIV_S0SEL, CHIP_CGU_DIV_S0SEL_VAL) |
                      BF_SET(BITM_CGU_DIV_CSEL,  CHIP_CGU_DIV_CSEL_VAL);


    *pREG_CGU0_CTL = BF_SET(BITM_CGU_CTL_DF, CHIP_PLL_DF_VAL) |
                     BF_SET(BITM_CGU_CTL_MSEL, CHIP_PLL_MSEL_VAL);

    while ((*pREG_CGU0_STAT & BITM_CGU_STAT_CLKSALGN) ||
           (*pREG_CGU0_STAT & BITM_CGU_STAT_PLLBP) ||
           !(*pREG_CGU0_STAT & BITM_CGU_STAT_PLOCK)) {}
#endif

#if CHIP_INTERRUPT_RESET
    chip_interrupt_reset();
#endif
#if CHIP_WDT_EN
    /* для WDT необходимо настроить SEC на генерацию FAULT */
    *pREG_SEC0_FCTL  = BITM_SEC_FCTL_EN | BITM_SEC_FCTL_SREN;
    *pREG_SEC0_SCTL2 = BITM_SEC_SCTL_SEN | BITM_SEC_SCTL_FEN;

    *pREG_WDOG0_CNT = CHIP_WDT_CNT_VAL;
    *pREG_WDOG0_CTL = 0;
#endif

#if CHIP_DMC_INIT
    /* вывод из self-refresh, если был введен */
    if ((*pREG_DMC0_CTL & BITM_DMC_CTL_SRREQ) && !dmc_err) {
        *pREG_DMC0_CTL &= ~BITM_DMC_CTL_SRREQ;
        DMC_WAIT_COND(*pREG_DMC0_STAT & BITM_DMC_STAT_SRACK, dmc_err);
    }

    if (!dmc_err) {
        *pREG_DMC0_CTL = BF_SET(BITM_DMC_CTL_PPREF, CHIP_DMC_POSTPONE_REF) |
                         BF_SET(BITM_DMC_CTL_RDTOWR, CHIP_DMC_RDTOWR) |
                         BF_SET(BITM_DMC_CTL_ADDRMODE, CHIP_DMC_ADDRMODE_PAGE) |
                         BF_SET(BITM_DMC_CTL_PREC, CHIP_DMC_PRECHARGE) |
                         BF_SET(BITM_DMC_CTL_LPDDR, CHIP_DMC_LPDDR);
        *pREG_DMC0_CFG  = ENUM_DMC_CFG_EXTBANK1 | DMC_SDRSIZE_CODE |
                          ENUM_DMC_CFG_SDRWID16 | ENUM_DMC_CFG_IFWID16;
        *pREG_DMC0_TR0  = BF_SET_OV(BITM_DMC_TR0_TMRD, TIME_DMC_PARAM(CHIP_DMC_TMRD)) |
                          BF_SET_OV(BITM_DMC_TR0_TRC,  TIME_DMC_PARAM(CHIP_DMC_TRC)) |
                          BF_SET_OV(BITM_DMC_TR0_TRAS, TIME_DMC_PARAM(CHIP_DMC_TRAS)) |
                          BF_SET_OV(BITM_DMC_TR0_TRP,  TIME_DMC_PARAM(CHIP_DMC_TRP)) |
                          BF_SET_OV(BITM_DMC_TR0_TWTR, TIME_DMC_PARAM(CHIP_DMC_TWTR)) |
                          BF_SET_OV(BITM_DMC_TR0_TRCD, TIME_DMC_PARAM(CHIP_DMC_TRCD));
        *pREG_DMC0_TR1  = BF_SET_OV(BITM_DMC_TR1_TRRD, TIME_DMC_PARAM(CHIP_DMC_TRRD)) |
                          BF_SET_OV(BITM_DMC_TR1_TRFC, TIME_DMC_PARAM(CHIP_DMC_TRFC)) |
                          BF_SET_OV(BITM_DMC_TR1_TREF, TIME_DMC_PARAM(CHIP_DMC_TREF));
        *pREG_DMC0_TR2  = BF_SET_OV(BITM_DMC_TR2_TCKE, TIME_DMC_PARAM(CHIP_DMC_TCKE)) |
                          BF_SET_OV(BITM_DMC_TR2_TXP,  TIME_DMC_PARAM(CHIP_DMC_TXP)) |
#if CHIP_DMC_LPDDR
                          BF_SET_OV(BITM_DMC_TR2_TWR,  TIME_DMC_PARAM(CHIP_DMC_TWR)) |
#endif
                          BF_SET_OV(BITM_DMC_TR2_TRTP, TIME_DMC_PARAM(CHIP_DMC_TRTP)) |
                          BF_SET_OV(BITM_DMC_TR2_TFAW, TIME_DMC_PARAM(CHIP_DMC_TFAW));
        *pREG_DMC0_MR   = BF_SET(BITM_DMC_MR_PD, CHIP_DMC_PD) |                
#if !CHIP_DMC_LPDDR
                          BF_SET(BITM_DMC_MR_WRRECOV, TIME_DMC_PARAM(CHIP_DMC_TWR)) |
#endif
                          BF_SET(BITM_DMC_MR_CL, CHIP_DMC_CL) |
                          BF_SET(BITM_DMC_MR_BLEN, DMC_BLEN_CODE);
        *pREG_DMC0_EMR1 = BF_SET(BITM_DMC_EMR1_QOFF, CHIP_DMC_OUT_BUF_OFF) |
                          BF_SET(BITM_DMC_EMR1_DQS,  CHIP_DMC_DQS_OFF) |
                          BF_SET(BITM_DMC_EMR1_AL,   CHIP_DMC_ADD_LATENCY) |
                          BF_SET(BITM_DMC_EMR1_DIC,  CHIP_DMC_DIC_RED) |
                          BF_SET(BITM_DMC_EMR1_DLLEN, CHIP_DMC_DLL_EN) |
                          DMC_RTT_CODE;
        *pREG_DMC0_EMR2   = 0; /** @todo */
        *pREG_DMC0_EMR3   = 0; /* reserved */
        //*pREG_DMC0_PADCTL; /** @todo */
        *pREG_DMC0_PHY_CTL1 = 0;
        *pREG_DMC0_PHY_CTL3 = BITM_DMC_PHY_CTL3_OFST0 | BITM_DMC_PHY_CTL3_OFST1 |
                              BITM_DMC_PHY_CTL3_TMG0  | BITM_DMC_PHY_CTL3_TMG1;


        *pREG_DMC0_PRIO =   BF_SET(BITM_DMC_PRIO_ID1, CHIP_DMC_PRIO_ID1) |
                            BF_SET(BITM_DMC_PRIO_ID2, CHIP_DMC_PRIO_ID2);
        *pREG_DMC0_PRIOMSK = BF_SET(BITM_DMC_PRIOMSK_ID1MSK, CHIP_DMC_PRIO_ID1_MSK) |
                             BF_SET(BITM_DMC_PRIOMSK_ID2MSK, CHIP_DMC_PRIO_ID2_MSK);
        *pREG_DMC0_EFFCTL = BF_SET(BITM_DMC_EFFCTL_IDLECYC, CHIP_DMC_REF_IDLECYC) |
                            BF_SET(BITM_DMC_EFFCTL_NUMREF,  CHIP_DMC_NUMREF) |
                            BF_SET(0xFF << 8, CHIP_DMC_PRECHARGE_BANKS) |
                            BF_SET(BITM_DMC_EFFCTL_WAITWRDATA, CHIP_DMC_WAIT_WRDATA) |
                            BF_SET(BITM_DMC_EFFCTL_FULLWRDATA, CHIP_DMC_FULL_WRDATA);

        *pREG_DMC0_CTL |= BITM_DMC_CTL_INIT;
        DMC_WAIT_COND(*pREG_DMC0_STAT & BITM_DMC_STAT_MEMINITDONE, dmc_err);
    }

    if (!dmc_err) {
        DMC_WAIT_COND(*pREG_DMC0_STAT & BITM_DMC_STAT_DLLCALDONE, dmc_err);
    }

    if (!dmc_err) {
        *pREG_DMC0_DLLCTL = (*pREG_DMC0_DLLCTL & ~BITM_DMC_DLLCTL_DATACYC) |
                            BF_SET(BITM_DMC_DLLCTL_DATACYC, BF_GET(*pREG_DMC0_STAT, BITM_DMC_STAT_PHYRDPHASE));
    }

    if (dmc_err) {
        *pREG_DMC0_CTL |= BITM_DMC_CTL_SRREQ;
    }
#endif


#if CHIP_SMC_INIT
    *pREG_SMC0_GCTL = BF_SET(BITM_SMC_GCTL_BGDIS, !CHIP_SMC_BUS_GRANT_EN);

#if CHIP_SMC_B0_EN
    SMC_BANK_CONFIG(0);
#endif
#if CHIP_SMC_B1_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PB01_SMC0_AMS1);
    SMC_BANK_CONFIG(1);
#endif
#if CHIP_SMC_B2_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PB04_SMC0_AMS2);
    SMC_BANK_CONFIG(2);
#endif
#if CHIP_SMC_B3_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PB05_SMC0_AMS3);
    SMC_BANK_CONFIG(3);
#endif


#if CHIP_SMC_BUS_GRANT_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PB09_SMC0_BGH);
    CHIP_PIN_CONFIG(CHIP_PIN_PB12_SMC0_BG);
#endif

#if CHIP_SMC_BYTE_OP_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PB04_SMC0_ABE0);
    CHIP_PIN_CONFIG(CHIP_PIN_PB05_SMC0_ABE1);
#endif

#if CHIP_SMC_ADDR_WIDTH > 3
    CHIP_PIN_CONFIG(CHIP_PIN_PA00_SMC0_A03);
#endif
#if CHIP_SMC_ADDR_WIDTH > 4
    CHIP_PIN_CONFIG(CHIP_PIN_PA01_SMC0_A04);
#endif
#if CHIP_SMC_ADDR_WIDTH > 5
    CHIP_PIN_CONFIG(CHIP_PIN_PA02_SMC0_A05);
#endif
#if CHIP_SMC_ADDR_WIDTH > 6
    CHIP_PIN_CONFIG(CHIP_PIN_PA03_SMC0_A06);
#endif
#if CHIP_SMC_ADDR_WIDTH > 7
    CHIP_PIN_CONFIG(CHIP_PIN_PA04_SMC0_A07);
#endif
#if CHIP_SMC_ADDR_WIDTH > 8
    CHIP_PIN_CONFIG(CHIP_PIN_PA05_SMC0_A08);
#endif
#if CHIP_SMC_ADDR_WIDTH > 9
    CHIP_PIN_CONFIG(CHIP_PIN_PA06_SMC0_A09);
#endif
#if CHIP_SMC_ADDR_WIDTH > 10
    CHIP_PIN_CONFIG(CHIP_PIN_PA07_SMC0_A10);
#endif
#if CHIP_SMC_ADDR_WIDTH > 11
    CHIP_PIN_CONFIG(CHIP_PIN_PA08_SMC0_A11);
#endif
#if CHIP_SMC_ADDR_WIDTH > 12
    CHIP_PIN_CONFIG(CHIP_PIN_PA09_SMC0_A12);
#endif
#if CHIP_SMC_ADDR_WIDTH > 13
    CHIP_PIN_CONFIG(CHIP_PIN_PB02_SMC0_A13);
#endif
#if CHIP_SMC_ADDR_WIDTH > 14
    CHIP_PIN_CONFIG(CHIP_PIN_PA10_SMC0_A14);
#endif
#if CHIP_SMC_ADDR_WIDTH > 15
    CHIP_PIN_CONFIG(CHIP_PIN_PA11_SMC0_A15);
#endif
#if CHIP_SMC_ADDR_WIDTH > 16
    CHIP_PIN_CONFIG(CHIP_PIN_PB03_SMC0_A16);
#endif
#if CHIP_SMC_ADDR_WIDTH > 17
    CHIP_PIN_CONFIG(CHIP_PIN_PA12_SMC0_A17);
#endif
#if CHIP_SMC_ADDR_WIDTH > 18
    CHIP_PIN_CONFIG(CHIP_PIN_PA13_SMC0_A18);
#endif
#if CHIP_SMC_ADDR_WIDTH > 19
    CHIP_PIN_CONFIG(CHIP_PIN_PA14_SMC0_A19);
#endif
#if CHIP_SMC_ADDR_WIDTH > 20
    CHIP_PIN_CONFIG(CHIP_PIN_PA15_SMC0_A20);
#endif
#if CHIP_SMC_ADDR_WIDTH > 21
    CHIP_PIN_CONFIG(CHIP_PIN_PB06_SMC0_A21);
#endif
#if CHIP_SMC_ADDR_WIDTH > 22
    CHIP_PIN_CONFIG(CHIP_PIN_PB07_SMC0_A22);
#endif
#if CHIP_SMC_ADDR_WIDTH > 23
    CHIP_PIN_CONFIG(CHIP_PIN_PB08_SMC0_A23);
#endif
#if CHIP_SMC_ADDR_WIDTH > 24
    CHIP_PIN_CONFIG(CHIP_PIN_PB10_SMC0_A24);
#endif
#if CHIP_SMC_ADDR_WIDTH > 25
    CHIP_PIN_CONFIG(CHIP_PIN_PB11_SMC0_A25);
#endif

#if defined(CHP_SMC_NOORCLK_EN) && CHP_SMC_NOORCLK_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PB00_SMC0_NORCLK);
#endif

#endif
}
