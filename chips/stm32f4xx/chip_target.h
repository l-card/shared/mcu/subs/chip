#ifndef CHIP_TARGET_STM32G0XX_H_
#define CHIP_TARGET_STM32G0XX_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "chip_devtype_spec_features.h"

#include "chip_cmsis_cfg.h"

#include "chip_mmap.h"
//#include "regs/regs_pwr.h"
#include "regs/regs_rcc.h"
//#include "regs/regs_syscfg.h"
//#include "regs/regs_iwdg.h"
#include "regs/regs_gpio.h"
//#include "regs/regs_exti.h"
//#include "regs/regs_flash.h"
#include "regs/regs_usart.h"
//#include "regs/regs_tim.h"
//#include "regs/regs_spi.h"


#include "chip_config.h"
//#include "chip_per_ids.h"

#include "init/chip_clk.h"
//#include "init/chip_pwr.h"
//#include "init/chip_per_ctl.h"
//#include "init/chip_flash.h"
//#include "init/chip_wdt.h"


#include "chip_pins.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CHIP_TARGET_STM32G0XX_H_ */
