#ifndef STM32G0XX_CHIP_PIN_H
#define STM32G0XX_CHIP_PIN_H



#define CHIP_PIN_USE_OSPEED_VLOW
#define CHIP_PIN_USE_OSPEED_VHIGH
#include "chips/shared/stm32/chip_pin_defs.h"
#include "chip_dev_packages.h"
#include "chip_config.h"

#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4
#define CHIP_PORT_F 5

#ifdef CHIP_CFG_PACKAGE
    #define CHIP_PACKAGE CHIP_CFG_PACKAGE
#else
    #define CHIP_PACKAGE CHIP_PACKAGE_ANY
#endif

#include "chip_dev_pins.h"



#endif // STM32G0XX_CHIP_PIN_H
