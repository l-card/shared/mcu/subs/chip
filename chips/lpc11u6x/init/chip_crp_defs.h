#ifndef CHIP_CRP_DEFS_H
#define CHIP_CRP_DEFS_H

/* Code read protection (в секции .crp_sect) */
#define CHIP_CRP_DISABLED        0xFFFFFFFF
#define CHIP_CRP_NO_ISP          0x4E697370
#define CHIP_CRP1                0x12345678
#define CHIP_CRP2                0x87654321
#define CHIP_CRP3                0x43218765

#endif // CHIP_CRP_DEFS_H
