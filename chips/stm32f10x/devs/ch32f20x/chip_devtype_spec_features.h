#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H


#define CHIP_DEV_CH32XXXX
#define CHIP_DEV_CH32FXXX
#define CHIP_DEV_CH32F2XX
#define CHIP_DEV_CH32F203

#include "chip_dev_spec_features.h"

#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(144)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
