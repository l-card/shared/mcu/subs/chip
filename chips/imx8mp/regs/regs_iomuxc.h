#ifndef REGS_IOMUXC_H
#define REGS_IOMUXC_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t GPR[25]; /* General Purpose Register 0 */
} CHIP_REGS_IOMUXC_GPR_T;

typedef struct {
    uint32_t RESERVED[5];
    __IO uint32_t MUXCTL[143]; /**< MUX Control Register */
    __IO uint32_t PADCTL[156];
    __IO uint32_t INSEL[94];
} CHIP_REGS_IOMUXC_T;

#define CHIP_REGS_IOMUXC_GPR   ((CHIP_REGS_IOMUXC_GPR_T *) CHIP_MEMRGN_ADDR_PERIPH_IOMUXC_GPR)
#define CHIP_REGS_IOMUXC       ((CHIP_REGS_IOMUXC_T *) CHIP_MEMRGN_ADDR_PERIPH_IOMUXC)


/*********** IOMUXC MUX Control Register (MUXCTL)  ****************************/
#define CHIP_REGFLD_IOMUXC_MUXCTL_SION          (0x00000001UL <<  4U)  /* (RW) Software Input On Field (Force enable input path) */
#define CHIP_REGFLD_IOMUXC_MUXCTL_MUXMODE       (0x00000007UL <<  0U)  /* (RW) MUX Mode Select Field */


/*********** IOMUXC PAD Control Register (PADCTL)  ****************************/
#define CHIP_REGFLD_IOMUXC_PADCTL_PE            (0x00000001UL <<  8U)  /* (RW) Pull Enable */
#define CHIP_REGFLD_IOMUXC_PADCTL_HYS           (0x00000001UL <<  7U)  /* (RW) CMOS(0)/Schmitt(1) */
#define CHIP_REGFLD_IOMUXC_PADCTL_PUE           (0x00000001UL <<  6U)  /* (RW) Pull down(0)/Pull up(1) */
#define CHIP_REGFLD_IOMUXC_PADCTL_ODE           (0x00000001UL <<  5U)  /* (RW) Open drain enable */
#define CHIP_REGFLD_IOMUXC_PADCTL_FSEL          (0x00000001UL <<  4U)  /* (RW) Slew Rate Select - Slow(0)/Fast(1) */
#define CHIP_REGFLD_IOMUXC_PADCTL_DSE           (0x00000003UL <<  1U)  /* (RW) Drive Strength */

#define CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X1     0
#define CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X2     2
#define CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X4     1
#define CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X6     3



/*********** General Purpose Register 1 (IOMUXC_GPR_GPR1) *********************/
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_DBG_ACK_A53_MASK(n)         (0x0000000FUL << (28U + (n)))  /* (RW) Mask debug ack from each CA53 core */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_DBG_ACK_M7_MASK             (0x00000001UL << 27U)  /* (RW) Mask debug ack from each CM7 */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_TZASC1_SECURE_BOOT_LOCK     (0x00000001UL << 23U)  /* (RW) secure_boot_lock for TZASC */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ENET1_RGMII_EN              (0x00000001UL << 22U)  /* (RW) ENET1 TX clock direction select for RGMII (1 - output) or MII (0 - input) */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ENET_QOS_RGMII_EN           (0x00000001UL << 21U)  /* (RW) ENET_QOS TX clock direction select for RGMII (1 - output) or MII (0 - input) */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ENET_QOS_CLK_TX_CLK_SEL     (0x00000001UL << 20U)  /* (RW) ENET QOS RMII clock comes from external PHY or OSC (0) or from ccm->pad->loopback (1, SOI bit for the pad iomuxc_sw_input_on_pad_enet_td2 should also be set) */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ENET_QOS_CLK_GEN_EN         (0x00000001UL << 19U)  /* (RW) Enable clk generate module for ENET QoS */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ENET_QOS_INTF_SEL           (0x00000007UL << 16U)  /* (RW) Select ENET QOS working mode */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ANAMIX_IPT_MODE             (0x00000001UL << 15U)  /* (RW) Mask ANAMIX ipt_mode */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ENET_QOS_DIS_CRC_CHK        (0x00000001UL << 14U)  /* (RW) Disable CRC check feature */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_ENET1_TX_CLK_SEL            (0x00000001UL << 13U)  /* (RW) ENET1 RMII clock comes from external PHY or OSC (0) or from ccm->pad->loopback (1, SOI bit for the pad iomuxc_sw_input_on_pad_enet_td2 should also be set) */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_GPR_IRQ                     (0x00000001UL << 12U)  /* (RW) Generate IRQ on IRQ0 */
#define CHIP_REGFLD_IOMUXC_GPR_GPR1_CAPIN2_SEL                  (0x00000001UL <<  3U)  /* (RW) Selector for GPT1 Capture in Channel 2 (IOMUX/ENET_QOS_TIMIER1_EVENT) */
#define CHIP_REGFLD_IOMUXC_GPR_GPT1_CAPIN1_SEL                  (0x00000001UL <<  2U)  /* (RW) Selector for GPT1 Capture in Channel 1 (IOMUX/ENET1_TIMIER1_EVENT) */
#define CHIP_REGFLD_IOMUXC_GPR_GPT1_ENET_QOS_EVENT0IN_SEL       (0x00000001UL <<  1U)  /* (RW) Selector for ENET QoS EVENT0 IN (IOMUX/GPT1_CMPOUT2) */
#define CHIP_REGFLD_IOMUXC_GPR_GPT1_ENET1_EVENT0IN_SEL          (0x00000001UL <<  0U)  /* (RW) Selector for ENET1 EVENT0 IN (IOMUX/GPT1_CMPOUT2) */
#define CHIP_REGMSK_IOMUXC_GPR_GPT1_RESERVED                    (0x07000FF0UL)


#define CHIP_REGFLDVAL_IOMUXC_GPR_GPR1_ENET_QOS_INTF_SEL_MII    0
#define CHIP_REGFLDVAL_IOMUXC_GPR_GPR1_ENET_QOS_INTF_SEL_RGMII  1
#define CHIP_REGFLDVAL_IOMUXC_GPR_GPR1_ENET_QOS_INTF_SEL_RMII   4

#define CHIP_REGFLDVAL_IOMUXC_GPR_GPR1_CAPIN2_SEL_IOMUX                     0
#define CHIP_REGFLDVAL_IOMUXC_GPR_GPR1_CAPIN2_SEL_ENET_QOS_TIMIER1_EVENT    1

#define CHIP_REGFLDVAL_IOMUXC_GPR_GPR1_CAPIN1_SEL_IOMUX                     0
#define CHIP_REGFLDVAL_IOMUXC_GPR_GPR1_CAPIN1_SEL_ENET1_TIMIER1_EVENT       1

#define CHIP_REGFLDVAL_IOMUXC_GPR_GPT1_ENET_QOS_EVENT0IN_SEL_IOMUX          0
#define CHIP_REGFLDVAL_IOMUXC_GPR_GPT1_ENET_QOS_EVENT0IN_SEL_GPT1_CMPOUT2   1

#define CHIP_REGFLDVAL_IOMUXC_GPR_GPT1_ENET1_EVENT0IN_SEL_IOMUX             0
#define CHIP_REGFLDVAL_IOMUXC_GPR_GPT1_ENET1_EVENT0IN_SEL_GPT1_CMPOUT2      1


/*********** General Purpose Register 2 (IOMUXC_GPR_GPR2) *********************/
#define CHIP_REGFLD_IOMUXC_GPR_GPR2_CORESIGHT_GPR_CTM_SEL       (0x00000003UL <<  0U)  /* (RW) Select for Coresight master */
#define CHIP_REGMSK_IOMUXC_GPR_GPT2_RESERVED                    (0xFFFFFFFCUL)



#endif // REGS_IOMUXC_H
