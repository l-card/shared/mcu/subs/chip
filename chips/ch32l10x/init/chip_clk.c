#include "chip.h"
#if CHIP_LTIMER_EN
#include "lclock.h"
#endif

#define CHIP_RVAL_RCC_CR_WTRDY_MSK    (CHIP_CLK_HSE_EN ? CHIP_REGFLD_RCC_CR_HSE_RDY : 0)


#if CHIP_CLK_PLL1_EN
    #if ((CHIP_CLK_HSE_PLLIN_DIV >= 1) && (CHIP_CLK_HSE_PLLIN_DIV <= 2))
        #define CHIP_RVAL_RCC_CFG_PLL1XTPRE     (CHIP_CLK_HSE_PLLIN_DIV - 1)
    #else
        #error "Invalid PLL HSE devider value"
    #endif

    #if (CHIP_CLK_HSI_PLLIN_DIV == 2)
        #define CHIP_RVAL_EXTEN_CTR_HSI_PRE 0
    #elif (CHIP_CLK_HSI_PLLIN_DIV == 1)
        #define CHIP_RVAL_EXTEN_CTR_HSI_PRE 1
    #else
        #error "Invalid PLL HSI devider value"
    #endif

    #if (CHIP_CLK_PLL1_MUL >= 2) && (CHIP_CLK_PLL1_MUL <= 16)
        #define CHIP_RVAL_RCC_CFGR_PLL1MUL (CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_2 + CHIP_CLK_PLL1_MUL - 2)
    #elif CHIP_CLK_PLL1_MUL == 18
        #define CHIP_RVAL_RCC_CFGR_PLL1MUL  CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_18
    #else
        #error "Invalid PLL1 multiplier value"
    #endif

    #if !CHIP_CLK_PLL1IN_EN
        #error PLL clock source is not enabled
    #endif

    #if CHIP_CLK_PLL1_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_CFGR_PLL1SRC      CHIP_REGFLDVAL_RCC_CFGR_PLL1_SRC_HSI
    #elif CHIP_CLK_PLL1_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_CFGR_PLL1SRC      CHIP_REGFLDVAL_RCC_CFGR_PLL1_SRC_HSE
    #else
        #error Invalid PLL1 clock source
    #endif

    #if (CHIP_CLK_PLL1IN_FREQ < CHIP_CLK_PLL1_IN_FREQ_MIN) || (CHIP_CLK_PLL1IN_FREQ > CHIP_CLK_PLL1_IN_FREQ_MAX)
        #error PLL1 input frequency is out of range
    #endif

    #if ((CHIP_CLK_PLL1_FREQ < CHIP_CLK_PLL1_OUT_FREQ_MIN) || (CHIP_CLK_PLL1_FREQ > CHIP_CLK_PLL1_OUT_FREQ_MAX))
        #error PLL1 output freq is out of range
    #endif
#endif



/* --------------------- Проверка параметров SystemClock -------------------- */
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_RVAL_RCC_CFGR_SW    CHIP_REGFLDVAL_RCC_CFGR_SW_HSI
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_RVAL_RCC_CFGR_SW    CHIP_REGFLDVAL_RCC_CFGR_SW_HSE
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLL1_ID
    #define CHIP_RVAL_RCC_CFGR_SW    CHIP_REGFLDVAL_RCC_CFGR_SW_PLL1
#else
    #error Invalid System Clock Source (must be from HSI, HSE, PLL1)
#endif

#if !CHIP_CLK_SYS_EN
    #error System clock source is not enabled
#endif



/* -------------------- Проверка частот AHB ----------------------------------*/
#if CHIP_CLK_AHB_DIV == 1
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_1
#elif CHIP_CLK_AHB_DIV == 2
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_2
#elif CHIP_CLK_AHB_DIV == 4
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_4
#elif CHIP_CLK_AHB_DIV == 8
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_8
#elif CHIP_CLK_AHB_DIV == 16
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_16
#elif CHIP_CLK_AHB_DIV == 64
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_64
#elif CHIP_CLK_AHB_DIV == 128
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_128
#elif CHIP_CLK_AHB_DIV == 256
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_256
#elif CHIP_CLK_AHB_DIV == 512
    #define CHIP_RVAL_RCC_CFGR_HPRE             CHIP_REGFLDVAL_RCC_CFGR_HPRE_512
#else
    #error invalid AHB clock divider
#endif

#if CHIP_CLK_AHB_FREQ > CHIP_CLK_AHB_FREQ_MAX
    #error AHB freq is out of range
#endif

/* -------------------- Проверка частот APB1/APB2 ----------------------------*/
#if CHIP_CLK_APB1_DIV == 1
    #define CHIP_RVAL_RCC_CFGR_PPRE1            CHIP_REGFLDVAL_RCC_CFGR_PPRE_1
#elif CHIP_CLK_APB1_DIV == 2
    #define CHIP_RVAL_RCC_CFGR_PPRE1            CHIP_REGFLDVAL_RCC_CFGR_PPRE_2
#elif CHIP_CLK_APB1_DIV == 4
    #define CHIP_RVAL_RCC_CFGR_PPRE1            CHIP_REGFLDVAL_RCC_CFGR_PPRE_4
#elif CHIP_CLK_APB1_DIV == 8
    #define CHIP_RVAL_RCC_CFGR_PPRE1            CHIP_REGFLDVAL_RCC_CFGR_PPRE_8
#elif CHIP_CLK_APB1_DIV == 16
    #define CHIP_RVAL_RCC_CFGR_PPRE1            CHIP_REGFLDVAL_RCC_CFGR_PPRE_16
#else
    #error invalid APB1 clock divider
#endif

#if CHIP_CLK_APB1_FREQ > CHIP_CLK_APB1_FREQ_MAX
    #error APB1 freq is out of range
#endif

#if CHIP_CLK_APB2_DIV == 1
    #define CHIP_RVAL_RCC_CFGR_PPRE2            CHIP_REGFLDVAL_RCC_CFGR_PPRE_1
#elif CHIP_CLK_APB2_DIV == 2
    #define CHIP_RVAL_RCC_CFGR_PPRE2            CHIP_REGFLDVAL_RCC_CFGR_PPRE_2
#elif CHIP_CLK_APB2_DIV == 4
    #define CHIP_RVAL_RCC_CFGR_PPRE2            CHIP_REGFLDVAL_RCC_CFGR_PPRE_4
#elif CHIP_CLK_APB2_DIV == 8
    #define CHIP_RVAL_RCC_CFGR_PPRE2            CHIP_REGFLDVAL_RCC_CFGR_PPRE_8
#elif CHIP_CLK_APB2_DIV == 16
    #define CHIP_RVAL_RCC_CFGR_PPRE2            CHIP_REGFLDVAL_RCC_CFGR_PPRE_16
#else
    #error invalid APB2 clock divider
#endif

#if CHIP_CLK_APB2_FREQ > CHIP_CLK_APB2_FREQ_MAX
    #error APB2 freq is out of range
#endif



/* ----------------------------- определение MCO ----------------------------*/
#if CHIP_CLK_MCO_EN
    #if CHIP_CLK_MCO_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_SYSCLK
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_HSE
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_PLL1_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL1_DIV2
    #else
        #error "Invalid MCO source (must be HSI14, LSI, LSE, SYS, HSI, HSE, PLL or HSI48)"
    #endif

    #if !CHIP_CLK_MCO_SRC_EN
        #error MCO source is not eneabled
    #endif
#else
    #define CHIP_RVAL_RCC_CFGR_MCO              CHIP_REGFLDVAL_RCC_CFGR_MCO_DIS
#endif


/* ----------------------------- определение ADC ----------------------------*/
#if CHIP_CLK_ADC_SRC_ID == CHIP_CLK_APB2_ID
    #define CHIP_RVAL_RCC_CFGR_ADC_PRE_ADJ      0
#elif CHIP_CLK_ADC_SRC_ID == CHIP_CLK_AHB_ID
    #define CHIP_RVAL_RCC_CFGR_ADC_PRE_ADJ      1
#else
    #error invalid ADC clock source
#endif

#if (CHIP_CLK_ADC_DUTY_CHG >= 0) && (CHIP_CLK_ADC_DUTY_CHG <= 7)
    #define CHIP_RVAL_RCC_CFGR_ADC_DUTY_CHG  CHIP_CLK_ADC_DUTY_CHG
#else
    #error invalid ADC duty cycle change
#endif

#if CHIP_CLK_ADC_APB2_DIV == 2
    #define CHIP_RVAL_RCC_CFGR_ADCPRE           0
#elif CHIP_CLK_ADC_APB2_DIV == 4
    #define CHIP_RVAL_RCC_CFGR_ADCPRE           1
#elif CHIP_CLK_ADC_APB2_DIV == 6
    #define CHIP_RVAL_RCC_CFGR_ADCPRE           2
#elif CHIP_CLK_ADC_APB2_DIV == 8
    #define CHIP_RVAL_RCC_CFGR_ADCPRE           3
#else
    #error invalid ADC src freq prescaler
#endif

#if CHIP_CLK_ADC_SRC_FREQ > CHIP_CLK_ADC_SRC_MAX
    #error ADC frequency is out of range
#endif

#if CHIP_CLK_USBFS_SETUP_EN
    #if !CHIP_CLK_PLL1_EN
        #error PLL1 must be enabled for USBFS operation
    #else
        #if CHIP_CLK_PLL1_FREQ == CHIP_MHZ(48)
            #define CHIP_RVAL_RCC_CFGR_USB_PRE           CHIP_REGFLDVAL_RCC_CFGR_USB_PRE_2
        #elif CHIP_CLK_PLL1_FREQ == CHIP_MHZ(72)
            #define CHIP_RVAL_RCC_CFGR_USB_PRE           CHIP_REGFLDVAL_RCC_CFGR_USB_PRE_1_5
        #elif CHIP_CLK_PLL1_FREQ == CHIP_MHZ(96)
            #define CHIP_RVAL_RCC_CFGR_USB_PRE           CHIP_REGFLDVAL_RCC_CFGR_USB_PRE_1
        #else
            #error PLL1 frequency must be 48,72 or 96 MHz for USB FS operation
        #endif
    #endif
#endif


void chip_clk_init(void) {

    /* включение LSI либо если разрешен явно, либо если требуется настройка IWDG.
     * Т.к. IWDG следует разрешить как можно раньше, включаем этот клок до
     * настройки остального */
#if CHIP_CLK_LSI_EN
    CHIP_REGS_RCC->CSR |= CHIP_REGFLD_RCC_CSR_LSI_ON;
#endif

#if CHIP_WDT_SETUP_EN
    /* wdt настраиваем как можно раньше, но нужно убедиться, что его клок уже готов */
    while ((CHIP_REGS_RCC->CSR & CHIP_REGFLD_RCC_CSR_LSI_RDY) != CHIP_REGFLD_RCC_CSR_LSI_RDY) {
        continue;
    }
    chip_wdt_init();
#endif


    /* Если процессор работает не от HSI, то переходим в режим по умолчанию:
     * включаем HSI и ждем готовности, переводим процессор на работу от него */
    if (LBITFIELD_GET(CHIP_REGS_RCC->CFGR,  CHIP_REGFLD_RCC_CFGR_SWS) != CHIP_REGFLDVAL_RCC_CFGR_SW_HSI) {
        CHIP_REGS_RCC->CR |= CHIP_REGFLD_RCC_CR_HSI_ON;


        CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_REGFLD_RCC_CR_HSI_RDY) == CHIP_REGFLD_RCC_CR_HSI_RDY,
                       CHIP_INIT_FAULT_WAIT_HSI_RDY, 10000);

        LBITFIELD_UPD(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_SW, CHIP_REGFLDVAL_RCC_CFGR_SW_HSI);
    }

    CHIP_REGS_RCC->CR = (CHIP_REGS_RCC->CR & (CHIP_REGMSK_RCC_CR_RESERVED | CHIP_REGFLD_RCC_CR_HSI_TRIM))
                        | CHIP_REGFLD_RCC_CR_HSI_ON
#if CHIP_CLK_HSE_EN
                        | CHIP_REGFLD_RCC_CR_HSE_ON
#if CHIP_CLK_HSE_MODE == CHIP_CLK_EXTMODE_CLOCK
                        | CHIP_REGFLD_RCC_CR_HSE_BYP
#endif
#if CHIP_CLK_HSE_CSS_EN
                        | CHIP_REGFLD_RCC_CR_CSS_ON
#endif
#endif
        ;
    /* ожидание установки всех базовых клоков */
#if CHIP_RVAL_RCC_CR_WTRDY_MSK != 0
    CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_RVAL_RCC_CR_WTRDY_MSK) == CHIP_RVAL_RCC_CR_WTRDY_MSK,
                   CHIP_INIT_FAULT_WAIT_HSE_RDY, 10000);
#endif



    /* настройка PLL если требуется */
#if CHIP_CLK_PLL1_EN
    CHIP_REGS_EXTEN->CTR = (CHIP_REGS_EXTEN->CTR & ~CHIP_REGFLD_EXTEN_CTR_HSI_PRE) |
                           LBITFIELD_SET(CHIP_REGFLD_EXTEN_CTR_HSI_PRE, CHIP_RVAL_EXTEN_CTR_HSI_PRE);

    CHIP_REGS_RCC->CFGR = (CHIP_REGS_RCC->CFGR & ~(CHIP_REGFLD_RCC_CFGR_PLL1_MUL
                                                   | CHIP_REGFLD_RCC_CFGR_PLL1_SRC
                                                   | CHIP_REGFLD_RCC_CFGR_PLL1_XTPRE))
                          | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL1_MUL, CHIP_RVAL_RCC_CFGR_PLL1MUL)
                          | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL1_SRC, CHIP_RVAL_RCC_CFGR_PLL1SRC)
                          | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL1_XTPRE, CHIP_RVAL_RCC_CFG_PLL1XTPRE)
        ;


    /* разрешение PLL и ожидание установки */
    LBITFIELD_UPD(CHIP_REGS_RCC->CR, CHIP_REGFLD_RCC_CR_PLL1_ON, 1);



    CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_REGFLD_RCC_CR_PLL1_RDY) == CHIP_REGFLD_RCC_CR_PLL1_RDY,
                   CHIP_INIT_FAULT_WAIT_PLL1_RDY, 10000);
#endif



    /* перед настройкой SYS_CLK устанавливаем нужные параметры LATENCY для flash */
    chip_flash_init();

    /* выбор источника системного клока и делителей шин */
    CHIP_REGS_RCC->CFGR = (CHIP_REGS_RCC->CFGR & ~(CHIP_REGFLD_RCC_CFGR_SW
                                                   | CHIP_REGFLD_RCC_CFGR_HPRE
                                                   | CHIP_REGFLD_RCC_CFGR_PPRE1
                                                   | CHIP_REGFLD_RCC_CFGR_PPRE2
                                                   | CHIP_REGFLD_RCC_CFGR_MCO
                                                   | CHIP_REGFLD_RCC_CFGR_ADC_PRE
                                                   | CHIP_REGFLD_RCC_CFGR_ADC_DUTY_CHG
                                                   | CHIP_REGFLD_RCC_CFGR_ADC_PRE_ADJ
                                                   | CHIP_REGFLD_RCC_CFGR_USB_PRE
                                                   ))
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_SW,            CHIP_RVAL_RCC_CFGR_SW)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_HPRE,          CHIP_RVAL_RCC_CFGR_HPRE)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PPRE1,         CHIP_RVAL_RCC_CFGR_PPRE1)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PPRE2,         CHIP_RVAL_RCC_CFGR_PPRE2)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO,           CHIP_RVAL_RCC_CFGR_MCO)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_ADC_PRE,       CHIP_RVAL_RCC_CFGR_ADCPRE)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_ADC_DUTY_CHG,  CHIP_RVAL_RCC_CFGR_ADC_DUTY_CHG)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_ADC_PRE_ADJ,   CHIP_RVAL_RCC_CFGR_ADC_PRE_ADJ)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_USB_PRE,       CHIP_RVAL_RCC_CFGR_USB_PRE)
        ;

    CHIP_REGS_RCC->AHBENR = 0
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_DMA1_EN,     CHIP_CLK_DMA1_EN)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_SRAM_EN,     CHIP_CLK_SRAM_EN)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_CRC_EN,      CHIP_CLK_CRC_EN)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_USBFS_EN,    CHIP_CLK_USBFS_EN)
                            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_USBPD_EN,    CHIP_CLK_USBPD_EN)
        ;

    CHIP_REGS_RCC->APB2ENR = 0
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_AFIO_EN,   CHIP_CLK_AFIO_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_IOPA_EN,   CHIP_CLK_GPIOA_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_IOPB_EN,   CHIP_CLK_GPIOB_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_IOPC_EN,   CHIP_CLK_GPIOC_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_IOPD_EN,   CHIP_CLK_GPIOD_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_ADC1_EN,   CHIP_CLK_ADC1_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM1_EN,   CHIP_CLK_TIM1_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SPI1_EN,   CHIP_CLK_SPI1_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART1_EN, CHIP_CLK_USART1_EN)
        ;

    CHIP_REGS_RCC->APB1ENR = 0
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM2_EN,   CHIP_CLK_TIM2_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM3_EN,   CHIP_CLK_TIM3_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM4_EN,   CHIP_CLK_TIM4_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_WWDG_EN,   CHIP_CLK_WWDG_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_SPI2_EN,   CHIP_CLK_SPI2_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USART2_EN, CHIP_CLK_USART2_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USART3_EN, CHIP_CLK_USART3_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USART4_EN, CHIP_CLK_USART4_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_I2C1_EN,   CHIP_CLK_I2C1_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_I2C2_EN,   CHIP_CLK_I2C2_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_CAN1_EN,   CHIP_CLK_CAN1_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_BKP_EN,    CHIP_CLK_BKP_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_PWR_EN,    CHIP_CLK_PWR_EN)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_LPTIM1_EN, CHIP_CLK_LPTIM1_EN)
        ;

#if CHIP_CLK_MCO_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PA8_MCO);
#endif
#if CHIP_LTIMER_EN
    /* разрешение ltimer */
    lclock_init();
#endif
}
