﻿#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H


/* ------------------ Внешний высокочастотный источник частоты HSE -----------*/
#define CHIP_CFG_CLK_HSE_FREQ       CHIP_MHZ(8)             /* значение внешней частоты HSE */
#define CHIP_CFG_CLK_HSE_MODE       CHIP_CLK_EXTMODE_OSC    /* режим внешнего источника HSE (OSC/CLOCK/DIS)  */
#define CHIP_CFG_CLK_HSE_CFD_EN     1                       /* разрешение контроля внешней частоты HSE */
#define CHIP_CFG_CLK_HSE_PLLIN_DIV  2                       /* далитель частоты HSE перед подачей на PLL (1,2,3,4,5) */

/* ------------------ Внутренний высокочастотный источник частоты HSI --------*/
#define CHIP_CFG_CLK_HSI_EN         1                       /* разрешение источника частоты HSI */
#define CHIP_CFG_CLK_HSISYS_DIV_EN  0                       /* разрешение деления частоты HSI в 48 МГц на 6 для получения частоты 8 МГц (только при использовании ее как системной) */


/* ------------------ Настройки PLL ------------------------------------------*/
#define CHIP_CFG_CLK_PLL_EN         1                       /* разрешение PLL */
#define CHIP_CFG_CLK_PLL_SRC        CHIP_CLK_HSE_PLLIN      /* источник входной частоты для PLL: HSE_PLLIN (HSE_FREQ/CHIP_CFG_CLK_HSE_PLLIN_DIV) или HSI_PLLIN (HSI_FREQ/12) */
#define CHIP_CFG_CLK_PLL_MUL        42                      /* множитель для результирующей частоты (2..64) */

/* ------------------ Системная частота --------------------------------------*/
#define CHIP_CFG_CLK_SYS_SRC        CHIP_CLK_PLL            /* источник частоты для системного клока: HSI_SYSIN, HSE или PLL */


/* ------------------ Делители частот внетренних шин и периферии -------------*/
#define CHIP_CFG_CLK_AHB_DIV        1 /* делитель частоты шыны AHB из CLK_SYS (1,2,4,8,16,64,128,256,512) */
#define CHIP_CFG_CLK_APB1_DIV       2 /* делитель частоты APB1 из AHB (1,2,4,8,16) */
#define CHIP_CFG_CLK_APB2_DIV       2 /* делитель частоты APB2 из AHB (1,2,4,8,16) */

#define CHIP_CFG_CLK_ADC_SETUP_EN   1 /* разрешение настройки частоты АЦП */
#define CHIP_CFG_CLK_ADC_DIV        4 /* делитель частоты АЦП (2,4,5,6,12,16) */

#define CHIP_CFG_CLK_USB_SETUP_EN   1 /* разрешение настройки частоты USB */
#define CHIP_CFG_CLK_USB_MUL2_DIV   7 /* делитель частоты USB относительно удвоенной частоты PLL (2,3,4,5,6,7,8). результирующая частота должна быть 48 МГц */


/* ------------------- Разрешение подачи частоты для периферии при старте ----*/
#define CHIP_CFG_CLK_DMA1_EN        0
#define CHIP_CFG_CLK_DMA2_EN        0
#define CHIP_CFG_CLK_SRAM_EN        1
#define CHIP_CFG_CLK_FLASH_EN       1
#define CHIP_CFG_CLK_CRC_EN         0
#define CHIP_CFG_CLK_XMC_EN         0

#define CHIP_CFG_CLK_AFIO_EN        1
#define CHIP_CFG_CLK_GPIOA_EN       1
#define CHIP_CFG_CLK_GPIOB_EN       1
#define CHIP_CFG_CLK_GPIOC_EN       0
#define CHIP_CFG_CLK_GPIOD_EN       0
#define CHIP_CFG_CLK_GPIOE_EN       0
#define CHIP_CFG_CLK_PWR_EN         1
#define CHIP_CFG_CLK_BKP_EN         0

#define CHIP_CFG_CLK_WWDG_EN        0
#define CHIP_CFG_CLK_TMR1_EN        0
#define CHIP_CFG_CLK_TMR2_EN        0
#define CHIP_CFG_CLK_TMR3_EN        0
#define CHIP_CFG_CLK_TMR4_EN        0
#define CHIP_CFG_CLK_TMR5_EN        0
#define CHIP_CFG_CLK_TMR6_EN        0
#define CHIP_CFG_CLK_TMR7_EN        0
#define CHIP_CFG_CLK_TMR8_EN        0
#define CHIP_CFG_CLK_TMR9_EN        0
#define CHIP_CFG_CLK_TMR10_EN       0
#define CHIP_CFG_CLK_TMR11_EN       0
#define CHIP_CFG_CLK_TMR12_EN       0
#define CHIP_CFG_CLK_TMR13_EN       0
#define CHIP_CFG_CLK_TMR14_EN       0

#define CHIP_CFG_CLK_USART1_EN      0
#define CHIP_CFG_CLK_USART2_EN      0
#define CHIP_CFG_CLK_USART3_EN      0
#define CHIP_CFG_CLK_UART4_EN       0
#define CHIP_CFG_CLK_UART5_EN       0
#define CHIP_CFG_CLK_USART6_EN      0
#define CHIP_CFG_CLK_UART7_EN       0
#define CHIP_CFG_CLK_UART8_EN       0

#define CHIP_CFG_CLK_SPI1_EN        0
#define CHIP_CFG_CLK_SPI2_EN        0
#define CHIP_CFG_CLK_SPI3_EN        0
#define CHIP_CFG_CLK_SPI4_EN        0

#define CHIP_CFG_CLK_I2C1_EN        0
#define CHIP_CFG_CLK_I2C2_EN        0
#define CHIP_CFG_CLK_I2C3_EN        0

#define CHIP_CFG_CLK_USB_EN         0
#define CHIP_CFG_CLK_CAN_EN         0

#define CHIP_CFG_CLK_ADC_EN         0

#endif // CHIP_CONFIG_H

