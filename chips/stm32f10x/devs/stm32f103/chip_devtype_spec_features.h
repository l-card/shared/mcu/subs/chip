#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H


//#define CHIP_DEV_STM32F10X
//#define CHIP_DEV_STM32F103

#include "chip_dev_spec_features.h"


#ifndef CHIP_CLK_CPU_FREQ_MAX
    #define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(72)
#endif

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
