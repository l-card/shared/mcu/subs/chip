#ifndef CHIP_PER_IDS_H
#define CHIP_PER_IDS_H


#define CHIP_PER_ID(clken, rst)     ((clken & 0xFF) | ((rst & 0xFF) << 8))
#define CHIP_PER_ID_GET_CLKEN(id)   ((id) & 0xFF)
#define CHIP_PER_ID_GET_RST(id)     (((id) >> 8) & 0xFF)


#define CHIP_PER_ID_ROM         CHIP_PER_ID( 1, -1)
#define CHIP_PER_ID_SDRAM0      CHIP_PER_ID( 2, -1)
#define CHIP_PER_ID_FLASHREG    CHIP_PER_ID( 3, -1)
#define CHIP_PER_ID_FLASHARRAY  CHIP_PER_ID( 4, -1)
#define CHIP_PER_ID_I2C0        CHIP_PER_ID( 5,  1)
#define CHIP_PER_ID_GPIO        CHIP_PER_ID( 6, -1)
#define CHIP_PER_ID_CT16B0      CHIP_PER_ID( 7, -1)
#define CHIP_PER_ID_CT16B1      CHIP_PER_ID( 8, -1)
#define CHIP_PER_ID_CT32B0      CHIP_PER_ID( 9, -1)
#define CHIP_PER_ID_CT32B1      CHIP_PER_ID(10, -1)
#define CHIP_PER_ID_SSP0        CHIP_PER_ID(11,  0)
#define CHIP_PER_ID_USART0      CHIP_PER_ID(12, -1)
#define CHIP_PER_ID_ADC         CHIP_PER_ID(13, -1)
#define CHIP_PER_ID_USB         CHIP_PER_ID(14, -1)
#define CHIP_PER_ID_WWDT        CHIP_PER_ID(15, -1)
#define CHIP_PER_ID_IOCON       CHIP_PER_ID(16, -1)
#define CHIP_PER_ID_SSP1        CHIP_PER_ID(18,  2)
#define CHIP_PER_ID_PINT        CHIP_PER_ID(19, -1)
#define CHIP_PER_ID_USART1      CHIP_PER_ID(20,  5)
#define CHIP_PER_ID_USART2      CHIP_PER_ID(21,  6)
#define CHIP_PER_ID_USART3      CHIP_PER_ID(22,  7)
#define CHIP_PER_ID_USART4      CHIP_PER_ID(22,  8)
#define CHIP_PER_ID_GROUP0INT   CHIP_PER_ID(23, -1)
#define CHIP_PER_ID_GROUP1INT   CHIP_PER_ID(24, -1)
#define CHIP_PER_ID_I2C1        CHIP_PER_ID(25,  3)
#define CHIP_PER_ID_RAM1        CHIP_PER_ID(26, -1)
#define CHIP_PER_ID_USBRAM      CHIP_PER_ID(27, -1)
#define CHIP_PER_ID_CRC         CHIP_PER_ID(28, -1)
#define CHIP_PER_ID_DMA         CHIP_PER_ID(29, -1)
#define CHIP_PER_ID_RTC         CHIP_PER_ID(30, -1)
#define CHIP_PER_ID_SCT0        CHIP_PER_ID(31,  9)
#define CHIP_PER_ID_SCT1        CHIP_PER_ID(31, 10)
#define CHIP_PER_ID_FRG         CHIP_PER_ID(-1,  4)


#endif // CHIP_PER_IDS_H
