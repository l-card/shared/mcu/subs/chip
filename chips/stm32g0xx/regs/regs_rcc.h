#ifndef CHIP_STM32G0XX_REGS_RCC_H
#define CHIP_STM32G0XX_REGS_RCC_H

#include "chip_dev_spec_features.h"

typedef struct {
  __IO uint32_t CR;          /*!< RCC Clock Sources Control Register,                                     Address offset: 0x00 */
  __IO uint32_t ICSCR;       /*!< RCC Internal Clock Sources Calibration Register,                        Address offset: 0x04 */
  __IO uint32_t CFGR;        /*!< RCC Regulated Domain Clocks Configuration Register,                     Address offset: 0x08 */
  __IO uint32_t PLLCFGR;     /*!< RCC System PLL configuration Register,                                  Address offset: 0x0C */
  __IO uint32_t RESERVED0;   /*!< Reserved,                                                               Address offset: 0x10 */
  __IO uint32_t CRRCR;       /*!< RCC Clock Configuration Register,                                       Address offset: 0x14 */
  __IO uint32_t CIER;        /*!< RCC Clock Interrupt Enable Register,                                    Address offset: 0x18 */
  __IO uint32_t CIFR;        /*!< RCC Clock Interrupt Flag Register,                                      Address offset: 0x1C */
  __IO uint32_t CICR;        /*!< RCC Clock Interrupt Clear Register,                                     Address offset: 0x20 */
  __IO uint32_t IOPRSTR;     /*!< RCC IO port reset register,                                             Address offset: 0x24 */
  __IO uint32_t AHBRSTR;     /*!< RCC AHB peripherals reset register,                                     Address offset: 0x28 */
  __IO uint32_t APBRSTR1;    /*!< RCC APB peripherals reset register 1,                                   Address offset: 0x2C */
  __IO uint32_t APBRSTR2;    /*!< RCC APB peripherals reset register 2,                                   Address offset: 0x30 */
  __IO uint32_t IOPENR;      /*!< RCC IO port enable register,                                            Address offset: 0x34 */
  __IO uint32_t AHBENR;      /*!< RCC AHB peripherals clock enable register,                              Address offset: 0x38 */
  __IO uint32_t APBENR1;     /*!< RCC APB peripherals clock enable register1,                             Address offset: 0x3C */
  __IO uint32_t APBENR2;     /*!< RCC APB peripherals clock enable register2,                             Address offset: 0x40 */
  __IO uint32_t IOPSMENR;    /*!< RCC IO port clocks enable in sleep mode register,                       Address offset: 0x44 */
  __IO uint32_t AHBSMENR;    /*!< RCC AHB peripheral clocks enable in sleep mode register,                Address offset: 0x48 */
  __IO uint32_t APBSMENR1;   /*!< RCC APB peripheral clocks enable in sleep mode register1,               Address offset: 0x4C */
  __IO uint32_t APBSMENR2;   /*!< RCC APB peripheral clocks enable in sleep mode register2,               Address offset: 0x50 */
  __IO uint32_t CCIPR;       /*!< RCC Peripherals Independent Clocks Configuration Register,              Address offset: 0x54 */
  __IO uint32_t CCIPR2;      /*!< RCC Peripherals Independent Clocks Configuration Register2,             Address offset: 0x58 */
  __IO uint32_t BDCR;        /*!< RCC Backup Domain Control Register,                                     Address offset: 0x5C */
  __IO uint32_t CSR;         /*!< RCC Unregulated Domain Clock Control and Status Register,               Address offset: 0x60 */
} CHIP_REGS_RCC_T;


#define CHIP_REGS_RCC          ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)

/********************  Bit definition for RCC_CR register  *****************/
#define CHIP_REGFLD_RCC_CR_HSION                    (0x000000001UL <<  8U)   /* Internal High Speed clock enable */
#define CHIP_REGFLD_RCC_CR_HSIKERON                 (0x000000001UL <<  9U)   /* Internal High Speed clock enable for some IPs Kernel */
#define CHIP_REGFLD_RCC_CR_HSIRDY                   (0x000000001UL << 10U)   /* Internal High Speed clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSIDIV                   (0x000000007UL << 11U)   /* HSIDIV[13:11] Internal High Speed clock division factor */
#define CHIP_REGFLD_RCC_CR_HSEON                    (0x000000001UL << 16U)   /* External High Speed clock enable */
#define CHIP_REGFLD_RCC_CR_HSERDY                   (0x000000001UL << 17U)   /* External High Speed clock ready */
#define CHIP_REGFLD_RCC_CR_HSEBYP                   (0x000000001UL << 18U)   /* External High Speed clock Bypass */
#define CHIP_REGFLD_RCC_CR_CSSON                    (0x000000001UL << 19U)   /* HSE Clock Security System enable */
#define CHIP_REGFLD_RCC_CR_HSI48ON                  (0x000000001UL << 22U)   /* RC48 clock enable */
#define CHIP_REGFLD_RCC_CR_HSI48RDY                 (0x000000001UL << 23U)   /* RC48 clock ready */
#define CHIP_REGFLD_RCC_CR_PLLON                    (0x000000001UL << 24U)   /* System PLL clock enable */
#define CHIP_REGFLD_RCC_CR_PLLRDY                   (0x000000001UL << 25U)   /* System PLL clock ready */

/********************  Bit definition for RCC_ICSCR register  ***************/
#define CHIP_REGFLD_RCC_ICSCR_HSICAL                (0x0000000FFUL <<  0U)  /* HSICAL[7:0] bits */
#define CHIP_REGFLD_RCC_ICSCR_HSITRIM               (0x00000007FUL <<  8U)  /* HSITRIM[14:8] bits */

/********************  Bit definition for RCC_CFGR register  ***************/
#define CHIP_REGFLD_RCC_CFGR_SW                     (0x00000007UL <<  0U)   /* SW[2:0] bits (System clock Switch) */
#define CHIP_REGFLD_RCC_CFGR_SWS                    (0x00000007UL <<  3U)   /* SWS[2:0] bits (System Clock Switch Status) */
#define CHIP_REGFLD_RCC_CFGR_HPRE                   (0x0000000FUL <<  8U)   /* HPRE[3:0] bits (AHB prescaler) */
#define CHIP_REGFLD_RCC_CFGR_PPRE                   (0x00000007UL << 12U)   /* PRE1[2:0] bits (APB prescaler) */
#define CHIP_REGFLD_RCC_CFGR_MCO2SEL                (0x0000000FUL << 16U)   /* MCO2SEL [3:0] bits (Clock output selection) */
#define CHIP_REGFLD_RCC_CFGR_MCO2PRE                (0x0000000FUL << 20U)   /* MCO2 prescaler [3:0] */
#define CHIP_REGFLD_RCC_CFGR_MCOSEL                 (0x0000000FUL << 24U)   /* MCOSEL [2:0] bits (Clock output selection) */
#define CHIP_REGFLD_RCC_CFGR_MCOPRE                 (0x0000000FUL << 28U)   /* MCO prescaler [2:0] */


#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSISYS           (0x00000000UL)
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSE              (0x00000001UL)
#define CHIP_REGFLDVAL_RCC_CFGR_SW_PLLR             (0x00000002UL)
#define CHIP_REGFLDVAL_RCC_CFGR_SW_LSI              (0x00000003UL)
#define CHIP_REGFLDVAL_RCC_CFGR_SW_LSE              (0x00000004UL)

/********************  Bit definition for RCC_PLLCFGR register  ***************/
#define CHIP_REGFLD_RCC_PLLCFGR_PLLSRC              (0x00000003UL <<  0U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLM                (0x00000007UL <<  4U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLN                (0x0000007FUL <<  8U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLPEN              (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLP                (0x0000001FUL << 17U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLQEN              (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLQ                (0x00000007UL << 25U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLREN              (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_PLLCFGR_PLLR                (0x00000007UL << 29U)

/********************  Bit definition for RCC_CRRCR register  ******************/
#define CHIP_REGFLD_RCC_CRRCR_HSI48CAL              (0x000001FFUL <<  0U) /* RC48CAL[8:0] bits */

/********************  Bit definition for RCC_CIER register  ******************/
#define CHIP_REGFLD_RCC_CIER_LSIRDYIE               (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_CIER_LSERDYIE               (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_CIER_HSI48RDYIE             (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_CIER_HSIRDYIE               (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_CIER_HSERDYIE               (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_CIER_PLLRDYIE               (0x00000001UL <<  5U)

/********************  Bit definition for RCC_CIFR register  ******************/
#define CHIP_REGFLD_RCC_CIFR_LSIRDYF                (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_CIFR_LSERDYF                (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_CIFR_HSI48RDYF              (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_CIFR_HSIRDYF                (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_CIFR_HSERDYF                (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_CIFR_PLLRDYF                (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_CIFR_CSSF                   (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_CIFR_LSECSSF                (0x00000001UL <<  9U)

/********************  Bit definition for RCC_CICR register  ******************/
#define CHIP_REGFLD_RCC_CICR_LSIRDYC                (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_CICR_LSERDYC                (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_CICR_HSI48RDYC              (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_CICR_HSIRDYC                (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_CICR_HSERDYC                (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_CICR_PLLRDYC                (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_CICR_CSSC                   (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_CICR_LSECSSC                (0x00000001UL <<  9U)

/********************  Bit definition for RCC_IOPRSTR register  ****************/
#define CHIP_REGFLD_RCC_IOPRSTR_GPIOARST            (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_IOPRSTR_GPIOBRST            (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_IOPRSTR_GPIOCRST            (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_IOPRSTR_GPIODRST            (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_IOPRSTR_GPIOERST            (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_IOPRSTR_GPIOFRST            (0x00000001UL <<  5U)

/********************  Bit definition for RCC_AHBRSTR register  ***************/
#define CHIP_REGFLD_RCC_AHBRSTR_DMA1RST             (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHBRSTR_DMA2RST             (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHBRSTR_FLASHRST            (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_AHBRSTR_CRCRST              (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_AHBRSTR_AESRST              (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_AHBRSTR_RNGRST              (0x00000001UL << 18U)

/********************  Bit definition for RCC_APBRSTR1 register  **************/
#define CHIP_REGFLD_RCC_APBRSTR1_TIM2RST            (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APBRSTR1_TIM3RST            (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APBRSTR1_TIM4RST            (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APBRSTR1_TIM6RST            (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APBRSTR1_TIM7RST            (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APBRSTR1_LPUART2RST         (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APBRSTR1_USART5RST          (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_APBRSTR1_USART6RST          (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_APBRSTR1_FDCANRST           (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APBRSTR1_USBRST             (0x00000001UL << 13U)
#define CHIP_REGFLD_RCC_APBRSTR1_SPI2RST            (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APBRSTR1_SPI3RST            (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APBRSTR1_CRSRST             (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APBRSTR1_USART2RST          (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APBRSTR1_USART3RST          (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APBRSTR1_USART4RST          (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_APBRSTR1_LPUART1RST         (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APBRSTR1_I2C1RST            (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_APBRSTR1_I2C2RST            (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_APBRSTR1_I2C3RST            (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_APBRSTR1_CECRST             (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_APBRSTR1_UCPD1RST           (0x00000001UL << 25U)
#define CHIP_REGFLD_RCC_APBRSTR1_UCPD2RST           (0x00000001UL << 26U)
#define CHIP_REGFLD_RCC_APBRSTR1_DBGRST             (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_APBRSTR1_PWRRST             (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_APBRSTR1_DAC1RST            (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_APBRSTR1_LPTIM2RST          (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_APBRSTR1_LPTIM1RST          (0x00000001UL << 31U)

/********************  Bit definition for RCC_APBRSTR2 register  **************/
#define CHIP_REGFLD_RCC_APBRSTR2_SYSCFGRST          (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APBRSTR2_TIM1RST            (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APBRSTR2_SPI1RST            (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APBRSTR2_USART1RST          (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APBRSTR2_TIM14RST           (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APBRSTR2_TIM15RST           (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APBRSTR2_TIM16RST           (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APBRSTR2_TIM17RST           (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APBRSTR2_ADCRST             (0x00000001UL << 20U)

/********************  Bit definition for RCC_IOPENR register  ****************/
#define CHIP_REGFLD_RCC_IOPENR_GPIOAEN              (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_IOPENR_GPIOBEN              (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_IOPENR_GPIOCEN              (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_IOPENR_GPIODEN              (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_IOPENR_GPIOEEN              (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_IOPENR_GPIOFEN              (0x00000001UL <<  5U)

/********************  Bit definition for RCC_AHBENR register  ****************/
#define CHIP_REGFLD_RCC_AHBENR_DMA1EN               (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHBENR_DMA2EN               (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHBENR_FLASHEN              (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_AHBENR_CRCEN                (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_AHBENR_AESEN                (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_AHBENR_RNGEN                (0x00000001UL << 18U)

/********************  Bit definition for RCC_APBENR1 register  ***************/
#define CHIP_REGFLD_RCC_APBENR1_TIM2EN              (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APBENR1_TIM3EN              (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APBENR1_TIM4EN              (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APBENR1_TIM6EN              (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APBENR1_TIM7EN              (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APBENR1_LPUART2EN           (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APBENR1_USART5EN            (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_APBENR1_USART6EN            (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_APBENR1_RTCAPBEN            (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_APBENR1_WWDGEN              (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APBENR1_FDCANEN             (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APBENR1_USBEN               (0x00000001UL << 13U)
#define CHIP_REGFLD_RCC_APBENR1_SPI2EN              (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APBENR1_SPI3EN              (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APBENR1_CRSEN               (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APBENR1_USART2EN            (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APBENR1_USART3EN            (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APBENR1_USART4EN            (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_APBENR1_LPUART1EN           (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APBENR1_I2C1EN              (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_APBENR1_I2C2EN              (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_APBENR1_I2C3EN              (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_APBENR1_CECEN               (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_APBENR1_UCPD1EN             (0x00000001UL << 25U)
#define CHIP_REGFLD_RCC_APBENR1_UCPD2EN             (0x00000001UL << 26U)
#define CHIP_REGFLD_RCC_APBENR1_DBGEN               (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_APBENR1_PWREN               (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_APBENR1_DAC1EN              (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_APBENR1_LPTIM2EN            (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_APBENR1_LPTIM1EN            (0x00000001UL << 31U)

/********************  Bit definition for RCC_APBENR2 register  **************/
#define CHIP_REGFLD_RCC_APBENR2_SYSCFGEN            (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APBENR2_TIM1EN              (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APBENR2_SPI1EN              (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APBENR2_USART1EN            (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APBENR2_TIM14EN             (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APBENR2_TIM15EN             (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APBENR2_TIM16EN             (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APBENR2_TIM17EN             (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APBENR2_ADCEN               (0x00000001UL << 20U)

/********************  Bit definition for RCC_IOPSMENR register  *************/
#define CHIP_REGFLD_RCC_IOPSMENR_GPIOASMEN          (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_IOPSMENR_GPIOBSMEN          (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_IOPSMENR_GPIOCSMEN          (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_IOPSMENR_GPIODSMEN          (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_IOPSMENR_GPIOESMEN          (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_IOPSMENR_GPIOFSMEN          (0x00000001UL <<  5U)

/********************  Bit definition for RCC_AHBSMENR register  *************/
#define CHIP_REGFLD_RCC_AHBSMENR_DMA1SMEN           (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHBSMENR_DMA2SMEN           (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHBSMENR_FLASHSMEN          (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_AHBSMENR_SRAMSMEN           (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_AHBSMENR_CRCSMEN            (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_AHBSMENR_AESSMEN            (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_AHBSMENR_RNGSMEN            (0x00000001UL << 18U)

/********************  Bit definition for RCC_APBSMENR1 register  *************/
#define CHIP_REGFLD_RCC_APBSMENR1_TIM2SMEN          (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APBSMENR1_TIM3SMEN          (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APBSMENR1_TIM4SMEN          (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APBSMENR1_TIM6SMEN          (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APBSMENR1_TIM7SMEN          (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APBSMENR1_LPUART2SMEN       (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APBSMENR1_USART5SMEN        (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_APBSMENR1_USART6SMEN        (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_APBSMENR1_RTCAPBSMEN        (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_APBSMENR1_WWDGSMEN          (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APBSMENR1_FDCANSMEN         (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APBSMENR1_USBSMEN           (0x00000001UL << 13U)
#define CHIP_REGFLD_RCC_APBSMENR1_SPI2SMEN          (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APBSMENR1_SPI3SMEN          (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APBSMENR1_CRSSMEN           (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APBSMENR1_USART2SMEN        (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APBSMENR1_USART3SMEN        (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APBSMENR1_USART4SMEN        (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_APBSMENR1_LPUART1SMEN       (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APBSMENR1_I2C1SMEN          (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_APBSMENR1_I2C2SMEN          (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_APBSMENR1_I2C3SMEN          (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_APBSMENR1_CECSMEN           (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_APBSMENR1_UCPD1SMEN         (0x00000001UL << 25U)
#define CHIP_REGFLD_RCC_APBSMENR1_UCPD2SMEN         (0x00000001UL << 26U)
#define CHIP_REGFLD_RCC_APBSMENR1_DBGSMEN           (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_APBSMENR1_PWRSMEN           (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_APBSMENR1_DAC1SMEN          (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_APBSMENR1_LPTIM2SMEN        (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_APBSMENR1_LPTIM1SMEN        (0x00000001UL << 31U)

/********************  Bit definition for RCC_APBSMENR2 register  *************/
#define CHIP_REGFLD_RCC_APBSMENR2_SYSCFGSMEN        (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APBSMENR2_TIM1SMEN          (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APBSMENR2_SPI1SMEN          (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APBSMENR2_USART1SMEN        (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APBSMENR2_TIM14SMEN         (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APBSMENR2_TIM15SMEN         (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APBSMENR2_TIM16SMEN         (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APBSMENR2_TIM17SMEN         (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APBSMENR2_ADCSMEN           (0x00000001UL << 20U)

/********************  Bit definition for RCC_CCIPR register  ******************/
#define CHIP_REGFLD_RCC_CCIPR_USART1SEL             (0x00000003UL <<  0U)
#define CHIP_REGFLD_RCC_CCIPR_USART2SEL             (0x00000003UL <<  2U)
#define CHIP_REGFLD_RCC_CCIPR_USART3SEL             (0x00000003UL <<  4U)
#define CHIP_REGFLD_RCC_CCIPR_CECSEL                (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_CCIPR_LPUART2SEL            (0x00000003UL <<  8U)
#define CHIP_REGFLD_RCC_CCIPR_LPUART1SEL            (0x00000003UL << 10U)
#define CHIP_REGFLD_RCC_CCIPR_I2C1SEL               (0x00000003UL << 12U)
#if CHIP_DEV_SUPPORT_I2C2_ICLKSEL
    #define CHIP_REGFLD_RCC_CCIPR_I2C2SEL           (0x00000003UL << 14U)
#else
    #define CHIP_REGFLD_RCC_CCIPR_I2S1SEL           (0x00000003UL << 14U)
#endif
#define CHIP_REGFLD_RCC_CCIPR_LPTIM1SEL             (0x00000003UL << 18U)
#define CHIP_REGFLD_RCC_CCIPR_LPTIM2SEL             (0x00000003UL << 20U)
#define CHIP_REGFLD_RCC_CCIPR_TIM1SEL               (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_CCIPR_TIM15SEL              (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_CCIPR_RNGSEL                (0x00000003UL << 26U)
#define CHIP_REGFLD_RCC_CCIPR_RNGDIV                (0x00000003UL << 28U)
#define CHIP_REGFLD_RCC_CCIPR_ADCSEL                (0x00000003UL << 30U)

/********************  Bit definition for RCC_CCIPR2 register  ****************/
#define CHIP_REGFLD_RCC_CCIPR2_I2S1SEL              (0x00000003UL <<  0U)
#define CHIP_REGFLD_RCC_CCIPR2_I2S2SEL              (0x00000003UL <<  2U)
#define CHIP_REGFLD_RCC_CCIPR2_FDCANSEL             (0x00000003UL <<  8U)
#define CHIP_REGFLD_RCC_CCIPR2_USBSEL               (0x00000003UL << 12U)

/********************  Bit definition for RCC_BDCR register  ******************/
#define CHIP_REGFLD_RCC_BDCR_LSEON                  (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_BDCR_LSERDY                 (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_BDCR_LSEBYP                 (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_BDCR_LSEDRV                 (0x00000003UL <<  3U)
#define CHIP_REGFLD_RCC_BDCR_LSECSSON               (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_BDCR_LSECSSD                (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_BDCR_RTCSEL                 (0x00000003UL <<  8U)
#define CHIP_REGFLD_RCC_BDCR_RTCEN                  (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_BDCR_BDRST                  (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_BDCR_LSCOEN                 (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_BDCR_LSCOSEL                (0x00000001UL << 25U)

/********************  Bit definition for RCC_CSR register  *******************/
#define CHIP_REGFLD_RCC_CSR_LSION                   (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_CSR_LSIRDY                  (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_CSR_RMVF                    (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_CSR_OBLRSTF                 (0x00000001UL << 25U)
#define CHIP_REGFLD_RCC_CSR_PINRSTF                 (0x00000001UL << 26U)
#define CHIP_REGFLD_RCC_CSR_PWRRSTF                 (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_CSR_SFTRSTF                 (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_CSR_IWDGRSTF                (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_CSR_WWDGRSTF                (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_CSR_LPWRRSTF                (0x00000001UL << 31U)

#endif // CHIP_STM32G0XX_REGS_RCC_H
