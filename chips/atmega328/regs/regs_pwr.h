#ifndef REGS_PWR_H
#define REGS_PWR_H

#include "chip_devtype_spec_features.h"

#define CHIP_REG_SMCR       _MMIO_BYTE(0x53) /* Sleep Mode Control Register */
#define CHIP_REG_MCUCR      _MMIO_BYTE(0x55) /* MCU Control Register */
#define CHIP_REG_PRR        _MMIO_BYTE(0x64) /* Power Save Control Register */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_PRR1       _MMIO_BYTE(0x65) /* Power Save Control Register 1 */
#define CHIP_REG_IOCWK      _MMIO_BYTE(0xAE) /* PD Port Wake-on-Change Control Register */
#define CHIP_REG_DPS2R      _MMIO_BYTE(0xAF) /* DPS2 Mode Control Register */
#endif



/* ---------------- SMCR – Sleep Mode Control Register -----------------------*/
#define CHIP_REGFLD_SMCR_SE                 (0x01U << 0) /* (RW) Sleep mode enable control bit */
#define CHIP_REGFLD_SMCR_SM                 (0x07U << 1) /* (RW) Sleep mode selection */

#define CHIP_REGFLDVAL_SMCR_SM_IDLE         0 /* IDLE mode */
#define CHIP_REGFLDVAL_SMCR_SM_ADC_NRJ      1 /* ADC Noise Rejection Mode */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLDVAL_SMCR_SM_SAVE         2 /* Save Mode */
#define CHIP_REGFLDVAL_SMCR_SM_DPS1         3
#define CHIP_REGFLDVAL_SMCR_SM_DPS0         6
#define CHIP_REGFLDVAL_SMCR_SM_DPS2         7
#else
#define CHIP_REGFLDVAL_SMCR_SM_PWR_DOWN     2 /* Power Down */
#define CHIP_REGFLDVAL_SMCR_SM_PWR_SAVE     3 /* Power Down */
#define CHIP_REGFLDVAL_SMCR_SM_STBY         6 /* Standby */
#define CHIP_REGFLDVAL_SMCR_SM_EXT_STBY     7 /* Extended Standby */
#endif

/* ---------------- MCUCR – MCU Control Register -----------------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_MCUCR_WCE               (0x01U << 0) /* (W1SC) MCUCR update enable */
#else
#define CHIP_REGFLD_MCUCR_IVCE              (0x01U << 0) /* (W1SC) Interrupt Vector Change Enable */
#endif
#define CHIP_REGFLD_MCUCR_IVSEL             (0x01U << 1) /* (RW) Interrupt vector selection */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_MCUCR_IFAIL             (0x01U << 2) /* (RO) System configuration load failure flag */
#define CHIP_REGFLD_MCUCR_IRLD              (0x01U << 3) /* (WO) Writing 1 will reload system configuration information */
#endif
#define CHIP_REGFLD_MCUCR_PUD               (0x01U << 4) /* (RW) Global pull-up disable */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_MCUCR_EXRFD             (0x01U << 5) /* (RW) External reset filter disable */
#define CHIP_REGFLD_MCUCR_FPDEN             (0x01U << 6) /* (RW) Flash Power down enable control */
#define CHIP_REGFLD_MCUCR_FWKEN             (0x01U << 7) /* (RW) Fast wake-up mode enable control, only valid for Power Off mode (32us rather 260us) */
#else
#define CHIP_REGFLD_MCUCR_BODSE             (0x01U << 5) /* (RW) BOD Sleep Enable */
#define CHIP_REGFLD_MCUCR_BODS              (0x01U << 6) /* (RW) BOD Sleep */
#endif



/* ---------------- PRR – Power Save Control Register ------------------------*/
#define CHIP_REGFLD_PRR_PRADC               (0x01U << 0) /* (RW) turn off ADC controller clock */
#define CHIP_REGFLD_PRR_PRUART0             (0x01U << 1) /* (RW) turn off USART0 module clock */
#define CHIP_REGFLD_PRR_PRSPI               (0x01U << 2) /* (RW) turn off SPI module clock */
#define CHIP_REGFLD_PRR_PRTIM1              (0x01U << 3) /* (RW) turn off timer/counter 1 clock */
#define CHIP_REGFLD_PRR_PRTIM0              (0x01U << 5) /* (RW) turn off timer/counter 0 clock */
#define CHIP_REGFLD_PRR_PRTIM2              (0x01U << 6) /* (RW) turn off timer/counter 2 clock */
#define CHIP_REGFLD_PRR_PRTWI               (0x01U << 7) /* (RW) turn off the clock to the TWI module clock */

#ifdef CHIP_LGT8FX8P
/* ---------------- PRR1 – Power Save Control Register 1 ---------------------*/
#define CHIP_REGFLD_PRR1_PRPCI              (0x01U << 1) /* (RW) turn off external pin changes and external interrupt module clocks */
#define CHIP_REGFLD_PRR1_PREFL              (0x01U << 2) /* (RW) turn off  the FLASH controller interface clock */
#define CHIP_REGFLD_PRR1_PRTIM3             (0x01U << 3) /* (RW) turn off timer/counter 3 clock */
#define CHIP_REGFLD_PRR1_PRWDT              (0x01U << 5) /* (RW) turn off the WDT counter clock */

/* ---------------- DPS2R – DPS2 Mode Control Register -----------------------*/
#define CHIP_REGFLD_DPS2R_TOS               (0x03U << 0) /* (RW) LPRC timed wake-up settings */
#define CHIP_REGFLD_DPS2R_LPRCE             (0x01U << 2) /* (RW) LPRC enable */
#define CHIP_REGFLD_DPS2R_DPS2E             (0x01U << 3) /* (RW) DPS2 mode enable */


#define CHIP_REGFLDVAL_DPS2R_TOS_128MS      0
#define CHIP_REGFLDVAL_DPS2R_TOS_256MS      1
#define CHIP_REGFLDVAL_DPS2R_TOS_512MS      2
#define CHIP_REGFLDVAL_DPS2R_TOS_1S         3

#endif

#endif // REGS_PWR_H
