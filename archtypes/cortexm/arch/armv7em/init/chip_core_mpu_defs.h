#ifndef CHIP_MPU_DEFS_H
#define CHIP_MPU_DEFS_H

#include "regs/regs_core_mpu.h"

#define CHIP_MPU_RGN_ATTR_MASK(tex, c, b, s)    (LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_TEX,(tex)) \
                                                    | LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_C,(c)) \
                                                    | LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_B,(b)) \
                                                    | LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_S,(s)))

/* Cache policy for memory attribute encoding  CHIP_MPU_RGN_ATTR_NORMAL_INOUT_CP */
#define CHIP_MPU_CACHE_POLICY_NONCACHE          0 /* Non-cacheable */
#define CHIP_MPU_CACHE_POLICY_WBWA              1 /* Write back, write and read allocate */
#define CHIP_MPU_CACHE_POLICY_WT                2 /* Write through, no write allocate */
#define CHIP_MPU_CACHE_POLICY_WB                3 /* Write back, no write allocate */

/* MPU Region Memory access attributes */
#define CHIP_MPU_RGN_ATTR_STRNGLY_ORDERED_SHARE             CHIP_MPU_RGN_ATTR_MASK(0,0,0,0)
#define CHIP_MPU_RGN_ATTR_DEVICE_SHARE                      CHIP_MPU_RGN_ATTR_MASK(0,0,1,0)
#define CHIP_MPU_RGN_ATTR_DEVICE_NOTSHARE                   CHIP_MPU_RGN_ATTR_MASK(2,0,0,0)
#define CHIP_MPU_RGN_ATTR_NORMAL_NONCACHE_NOTSHARE          CHIP_MPU_RGN_ATTR_MASK(1,0,0,0)
#define CHIP_MPU_RGN_ATTR_NORMAL_NONCACHE_SHARE             CHIP_MPU_RGN_ATTR_MASK(1,0,0,1)
#define CHIP_MPU_RGN_ATTR_NORMAL_WT_NOTSHARE                CHIP_MPU_RGN_ATTR_MASK(0,1,0,0)
#define CHIP_MPU_RGN_ATTR_NORMAL_WT_SHARE                   CHIP_MPU_RGN_ATTR_MASK(0,1,0,1)
#define CHIP_MPU_RGN_ATTR_NORMAL_WB_NOTSHARE                CHIP_MPU_RGN_ATTR_MASK(0,1,1,0)
#define CHIP_MPU_RGN_ATTR_NORMAL_WB_SHARE                   CHIP_MPU_RGN_ATTR_MASK(0,1,1,1)
#define CHIP_MPU_RGN_ATTR_NORMAL_WBWA_NOTSHARE              CHIP_MPU_RGN_ATTR_MASK(1,1,1,0)
#define CHIP_MPU_RGN_ATTR_NORMAL_WBWA_SHARE                 CHIP_MPU_RGN_ATTR_MASK(1,1,1,1)
#define CHIP_MPU_RGN_ATTR_NORMAL_INOUT_CP_NOTSHARE(in,out)  CHIP_MPU_RGN_ATTR_MASK(4 + ((out) & 3), ((in) >> 1) & 1, (in) & 1, 0)
#define CHIP_MPU_RGN_ATTR_NORMAL_INOUT_CP_SHARE(in,out)     CHIP_MPU_RGN_ATTR_MASK(4 + ((out) & 3), ((in) >> 1) & 1, (in) & 1, 1)

/* MPU Region access permissions for privileged and unprivileged software */
#define CHIP_MPU_RGN_ACC_NA_NA                  CHIP_REGFLDVAL_MPU_RASR_AP_NA_NA /* All accesses generate a permission fault */
#define CHIP_MPU_RGN_ACC_RW_NA                  CHIP_REGFLDVAL_MPU_RASR_AP_RW_NA /* Access from privileged software only */
#define CHIP_MPU_RGN_ACC_RW_RO                  CHIP_REGFLDVAL_MPU_RASR_AP_RW_RO /* Writes by unprivileged software generate a permission fault */
#define CHIP_MPU_RGN_ACC_RW_RW                  CHIP_REGFLDVAL_MPU_RASR_AP_RW_RW /* Full access */
#define CHIP_MPU_RGN_ACC_RO_NA                  CHIP_REGFLDVAL_MPU_RASR_AP_RO_NA /* Reads by privileged software only */
#define CHIP_MPU_RGN_ACC_RO_RO                  CHIP_REGFLDVAL_MPU_RASR_AP_RO_RO /* Read only, by privileged or unprivileged software */

#endif // CHIP_MPU_DEFS_H
