#ifndef CHIP_PINS_H
#define CHIP_PINS_H

#include "chip_dev_spec_features.h"
#include "regs/regs_gpio.h"
#include "chip_per_ids.h"
#include "chip_config.h"


#include "lbitfield.h"

typedef unsigned t_chip_pin_id;

#define CHIP_PIN_MODE_INPUT     0x0
#define CHIP_PIN_MODE_OUTPUT    0x1
#define CHIP_PIN_MODE_AF        0x2
#define CHIP_PIN_MODE_ANALOG    0x3
#define CHIP_PIN_MODE_UNKNOWN   0xF

#define CHIP_PIN_FUNC_ANALOG    0xFE
#define CHIP_PIN_FUNC_GPIO      0xFF


/* -------------- различные параметры конфигурации пинов -------------------- */
/* тип выхода (push-pull или open-drain) */
#define CHIP_PIN_CFG_OTYPE              (0x1UL << 0)
#define CHIP_PIN_CFG_OTYPE_VAL(v)       (((v) & 0x1UL) << 0)
#define CHIP_PIN_CFG_OTYPE_PP           CHIP_PIN_CFG_OTYPE_VAL(0) /* push-pull */
#define CHIP_PIN_CFG_OTYPE_OD           CHIP_PIN_CFG_OTYPE_VAL(1) /* open-drain */

/* скорость переключения (slew rate) */
#define CHIP_PIN_CFG_OSPEED             (0x1UL << 1)
#define CHIP_PIN_CFG_OSPEED_VAL(v)      (((v) & 0x1UL) << 1)
#define CHIP_PIN_CFG_OSPEED_LOW         CHIP_PIN_CFG_OSPEED_VAL(0)
#define CHIP_PIN_CFG_OSPEED_HIGH        CHIP_PIN_CFG_OSPEED_VAL(1)


/* нагр. спасобность (мА)  */
#define CHIP_PIN_CFG_OSTRENGTH          (0x3UL << 2)
#define CHIP_PIN_CFG_OSTRENGTH_VAL(v)   (((v) & 0x3UL) << 2)
#define CHIP_PIN_CFG_OSTRENGTH_LOW      CHIP_PIN_CFG_OSTRENGTH_VAL(0)
#define CHIP_PIN_CFG_OSTRENGTH_MED      CHIP_PIN_CFG_OSTRENGTH_VAL(1)
#define CHIP_PIN_CFG_OSTRENGTH_HIGH     CHIP_PIN_CFG_OSTRENGTH_VAL(2)
#define CHIP_PIN_CFG_OSTRENGTH_VHIGH    CHIP_PIN_CFG_OSTRENGTH_VAL(3)

/* включение подтяжек (только для входа) */
#define CHIP_PIN_CFG_PUPD               (0x3UL << 4)
#define CHIP_PIN_CFG_PUPD_VAL(v)        (((v) & 0x3UL) << 4)
#define CHIP_PIN_CFG_NOPULL             CHIP_PIN_CFG_PUPD_VAL(0)
#define CHIP_PIN_CFG_PULLUP             CHIP_PIN_CFG_PUPD_VAL(1)
#define CHIP_PIN_CFG_PULLDOWN           CHIP_PIN_CFG_PUPD_VAL(2)


/* конфигурации по умолчанию */
#ifndef CHIP_PIN_CFG_STD
    #define CHIP_PIN_CFG_STD      (CHIP_PIN_CFG_OSPEED_HIGH | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_PP)
#endif

#ifndef CHIP_PIN_CFG_STD_OD
    #define CHIP_PIN_CFG_STD_OD   (CHIP_PIN_CFG_OSPEED_HIGH | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_OD)
#endif


#define CHIP_PIN_REMAP_NA           0
#define CHIP_PIN_REMAP1(n)          (1UL << (n))
#define CHIP_PIN_REMAP2(n1, n2)     ((1UL << (n1)) | (1UL << (n2)))
#define CHIP_PIN_REMAP_DEFAULT      CHIP_PIN_REMAP1(0)


#define CHIP_PIN_ID_EX(port, pin, func, remap_msk, mode, cfg) ((((port)       &  0xF) << 28) | \
                                                                (((pin)       &  0xF) << 24) | \
                                                                (((func)      & 0xFF) << 16) | \
                                                                (((remap_msk) &  0xF) << 12) | \
                                                                (((mode)      &  0xF) <<  8) | \
                                                                (((cfg)       & 0xFF) <<  0))

#define CHIP_PIN_ID(port, pin, func, remap_msk, mode) CHIP_PIN_ID_EX(port, pin, func, remap_msk, mode, CHIP_PIN_CFG_STD)
#define CHIP_PIN_ID_GPIO(port, pin)   CHIP_PIN_ID(port, pin,  CHIP_PIN_FUNC_GPIO,   CHIP_PIN_REMAP_NA, CHIP_PIN_MODE_UNKNOWN)
#define CHIP_PIN_ID_ANALOG(port, pin) CHIP_PIN_ID(port, pin,  CHIP_PIN_FUNC_ANALOG, CHIP_PIN_REMAP_NA, CHIP_PIN_MODE_ANALOG)



#define CHIP_PIN_ID_PORT(id)        (((id) >> 28) & 0xF)
#define CHIP_PIN_ID_PIN(id)         (((id) >> 24) & 0xF)
#define CHIP_PIN_ID_FUNC(id)        (((id) >> 16) & 0xFF)
#define CHIP_PIN_ID_REMAP_MSK(id)   (((id) >> 12) & 0xF)
#define CHIP_PIN_ID_MODE(id)        (((id) >>  8) & 0xF)
#define CHIP_PIN_ID_CFG(id)         (((id) >>  0) & 0xFF)

#define CHIP_PIN_ID_INVALID         0xFFFFFFFF

#define CHIP_PIN_GPIOREG(reg, id)    CHIP_REGS_GPIO(CHIP_PIN_ID_PORT(id))->reg
#define CHIP_PIN_GPIOREG_CTRL(id)    CHIP_PIN_GPIOREG(CTRL[CHIP_REGNUM_GPIO_CTRL(CHIP_PIN_ID_PIN(id))],id)
#define CHIP_PIN_BITMSK(id)          (1UL << CHIP_PIN_ID_PIN(id))
#define CHIP_PIN_CTL_MSK(id)         (CHIP_REGFLD_GPIO_CTRL_CONF(CHIP_PIN_ID_PIN(id)) | CHIP_REGFLD_GPIO_CTRL_MDE(CHIP_PIN_ID_PIN(id)))


#define CHIP_PIN_REGVAL_CTRL_CONF(id, cfg, mode) (((mode) == CHIP_PIN_MODE_OUTPUT) ? \
                                              ((((cfg) & CHIP_PIN_CFG_OTYPE) == CHIP_PIN_CFG_OTYPE_PP) ? \
                                                 CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_GP_PP : CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_GP_OD) : \
                                           ((mode) == CHIP_PIN_MODE_AF) ? \
                                              ((((cfg) & CHIP_PIN_CFG_OTYPE) == CHIP_PIN_CFG_OTYPE_PP) ? \
                                                 CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_ALT_PP : CHIP_REGFLDVAL_GPIO_CTRL_CONF_OUT_ALT_OD) : \
                                           ((mode) == CHIP_PIN_MODE_ANALOG) ? CHIP_REGFLDVAL_GPIO_CTRL_CONF_IN_ANALOG : \
                                           ((mode) == CHIP_PIN_MODE_INPUT) ? \
                                              ((((cfg) & CHIP_PIN_CFG_PUPD) == CHIP_PIN_CFG_NOPULL) ? \
                                                CHIP_REGFLDVAL_GPIO_CTRL_CONF_IN_FLOATING : CHIP_REGFLDVAL_GPIO_CTRL_CONF_IN_PUPD) : 0)

#define CHIP_PIN_REGVAL_CTRL_MDE(id, cfg, mode) ((((mode) == CHIP_PIN_MODE_OUTPUT) || ((mode) == CHIP_PIN_MODE_AF)) ? \
                                            ((((cfg) & CHIP_PIN_CFG_OSTRENGTH) == CHIP_PIN_CFG_OSTRENGTH_LOW) ? CHIP_REGFLDVAL_GPIO_CTRL_MDE_OUT_4MA : \
                                             (((cfg) & CHIP_PIN_CFG_OSTRENGTH) == CHIP_PIN_CFG_OSTRENGTH_MED) ? CHIP_REGFLDVAL_GPIO_CTRL_MDE_OUT_6MA :\
                                                                                                                               CHIP_REGFLDVAL_GPIO_CTRL_MDE_OUT_15MA) : \
                                                                                                                               CHIP_REGFLDVAL_GPIO_CTRL_MDE_IN)

/* функция, настраивающая пин по ID, с явным заданием специальных режимов через объединение по или CHIP_PIN_CFG */
#define CHIP_PIN_CONFIG_EX_MODE(id, cfg, mode) do { \
        if ((mode) != CHIP_PIN_MODE_UNKNOWN) { \
            CHIP_PIN_GPIOREG_CTRL(id) = (CHIP_PIN_GPIOREG_CTRL(id) & ~CHIP_PIN_CTL_MSK(id)) \
                | LBITFIELD_SET(CHIP_REGFLD_GPIO_CTRL_CONF(CHIP_PIN_ID_PIN(id)), CHIP_PIN_REGVAL_CTRL_CONF(id, cfg, mode)) \
                | LBITFIELD_SET(CHIP_REGFLD_GPIO_CTRL_MDE(CHIP_PIN_ID_PIN(id)), CHIP_PIN_REGVAL_CTRL_MDE(id, cfg, mode)); \
            if ((mode) == CHIP_PIN_MODE_INPUT) { \
                CHIP_PIN_OUT(id, ((cfg) & CHIP_PIN_CFG_PUPD) == CHIP_PIN_CFG_PULLUP); \
            } \
            LBITFIELD_UPD(CHIP_PIN_GPIOREG(HDRV,id), CHIP_PIN_BITMSK(id), ((cfg) & CHIP_PIN_CFG_OSTRENGTH) == CHIP_PIN_CFG_OSTRENGTH_VHIGH); \
            LBITFIELD_UPD(CHIP_PIN_GPIOREG(SRCTR,id), CHIP_PIN_BITMSK(id), ((cfg) & CHIP_PIN_CFG_OSPEED) == CHIP_PIN_CFG_OSPEED_HIGH); \
        } \
        if ((CHIP_PIN_ID_FUNC(id) != CHIP_PIN_FUNC_GPIO) && (CHIP_PIN_ID_FUNC(id) != CHIP_PIN_FUNC_ANALOG)) { \
        }  \
    } while (0)


/* функция, настраивающая пин по ID, использующая указанные настройки */
#define CHIP_PIN_CONFIG_EX(id, cfg) CHIP_PIN_CONFIG_EX_MODE(id, cfg, CHIP_PIN_ID_MODE(id))


/* функция, настраивающая пин по ID, использующая спец. настройки по умолчанию.
   Используется для альтернативных функций и АЦП. Для GPIO используются CHIP_PIN_CONFIG_OUT и CHIP_PIN_CONFIG_IN */
#define CHIP_PIN_CONFIG(id) do { \
        CHIP_PIN_CONFIG_EX(id, CHIP_PIN_ID_CFG(id)); \
    } while(0)


/* настройка пина на оптимальный режим, если не используется (аналоговый режим для отключения входа/выхода) */
#define CHIP_PIN_CONFIG_DIS(id) do { \
    CHIP_PIN_CONFIG(CHIP_PIN_ID(CHIP_PIN_ID_PORT(id), CHIP_PIN_ID_PIN(id), CHIP_PIN_FUNC_ANALOG)); \
} while(0)


/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val)        CHIP_PIN_GPIOREG(BSRE,id) = (val ? CHIP_REGFLD_GPIO_BSRE_BST(CHIP_PIN_ID_PIN(id)) : CHIP_REGFLD_GPIO_BSRE_BRE(CHIP_PIN_ID_PIN(id)))
/* чтение уровня на ножке (0 или 1) */
#define CHIP_PIN_IN(id)              (!!(CHIP_PIN_GPIOREG(IPTDT,id) & CHIP_PIN_BITMSK(id)))
/* состояние регистра выхода (независимо от физического уровня) */
#define CHIP_PIN_GET_OUT_VAL(id)     (!!(CHIP_PIN_GPIOREG(OPTDT,id) & CHIP_PIN_BITMSK(id)))
/* атомарное инвертирование уровня на ножке */
#define CHIP_PIN_TOGGLE(id)          CHIP_PIN_OUT(id, (CHIP_PIN_GET_OUT_VAL(id) ? 0 : 1))


/* Конфигурация пина как General Purpose Output с указанной доп. конфигурацией и начальным значением */
#define CHIP_PIN_CONFIG_OUT_EX(id, cfg, val) do { \
                                        CHIP_PIN_OUT(id, val); \
                                        CHIP_PIN_CONFIG_EX_MODE(id, cfg, CHIP_PIN_MODE_OUTPUT); \
                                    } while(0)

/* Конфигурация пина как General Purpose Input с указанной доп. конфигурацией */
#define CHIP_PIN_CONFIG_IN_EX(id, cfg) CHIP_PIN_CONFIG_EX_MODE(id, cfg, CHIP_PIN_MODE_INPUT);

/* Конфигурация пина как General Purpose Output с конфигурацией по умолчанию и начальным значением */
#define CHIP_PIN_CONFIG_OUT(id, val) CHIP_PIN_CONFIG_OUT_EX(id, CHIP_PIN_ID_CFG(id), val)
/* вспомогательный макрос для настройки пина и конфигурирования его на вход */
#define CHIP_PIN_CONFIG_IN(id) CHIP_PIN_CONFIG_IN_EX(id, CHIP_PIN_ID_CFG(id))



#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4



/* ------------------------ Функции пинов ------------------------------------*/

/********************************** Порт A ************************************/
/*----- Пин PA0  ------------------------------------------------------------ */
#define CHIP_PIN_PA0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 0)
#define CHIP_PIN_PA0_ADC1_IN0           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 0)
#define CHIP_PIN_PA0_WKUP1              CHIP_PIN_ID         (CHIP_PORT_A, 0, CHIP_PER_ID_WKUP,   CHIP_PIN_REMAP_NA,      CHIP_PIN_MODE_IN) /* ? */
#define CHIP_PIN_PA0_USART2_CTS         CHIP_PIN_ID         (CHIP_PORT_A, 0, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating/Input pull-up */
#define CHIP_PIN_PA0_TMR2_CH1_IN        CHIP_PIN_ID         (CHIP_PORT_A, 0, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA0_TMR2_CH1_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 0, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA0_TMR2_ETR           CHIP_PIN_ID         (CHIP_PORT_A, 0, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA0_UART4_TX           CHIP_PIN_ID         (CHIP_PORT_A, 0, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_AF) /* Alternate function push-pull */


/*----- Пин PA1  ------------------------------------------------------------ */
#define CHIP_PIN_PA1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_ADC1_IN1           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_WKUP2              CHIP_PIN_ID         (CHIP_PORT_A, 1, CHIP_PER_ID_WKUP,   CHIP_PIN_REMAP_NA,      CHIP_PIN_MODE_IN) /* ? */
#define CHIP_PIN_PA1_USART2_RTS         CHIP_PIN_ID         (CHIP_PORT_A, 1, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA1_TMR2_CH2_IN        CHIP_PIN_ID         (CHIP_PORT_A, 1, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA1_TMR2_CH2_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 1, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA1_TMR5_CH2_IN        CHIP_PIN_ID         (CHIP_PORT_A, 1, CHIP_PER_ID_TMR5,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA1_TMR5_CH2_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 1, CHIP_PER_ID_TMR5,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA1_UART4_RX           CHIP_PIN_ID         (CHIP_PORT_A, 1, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_IN) /* Input floating/Input pull-up */

/*----- Пин PA2  ------------------------------------------------------------ */
#define CHIP_PIN_PA2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 2)
#define CHIP_PIN_PA2_ADC1_IN2           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 2)
#define CHIP_PIN_PA2_USART2_TX          CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA2_TMR2_CH3_IN        CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA2_TMR2_CH3_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA2_TMR5_CH3_IN        CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_TMR5,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA2_TMR5_CH3_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_TMR5,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA2_TMR9_CH1_IN        CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_TMR9,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA2_TMR9_CH1_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_TMR9,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA2_XMC_D4             CHIP_PIN_ID         (CHIP_PORT_A, 2, CHIP_PER_ID_XMC,    CHIP_PIN_REMAP2(1,2),   CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA3  ------------------------------------------------------------ */
#define CHIP_PIN_PA3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 3)
#define CHIP_PIN_PA3_ADC1_IN3           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 3)
#define CHIP_PIN_PA3_USART2_RX          CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating/Input pull-up */
#define CHIP_PIN_PA3_TMR2_CH4_IN        CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA3_TMR2_CH4_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_TMR2,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA3_TMR5_CH4_IN        CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_TMR5,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA3_TMR5_CH4_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_TMR5,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA3_TMR9_CH2_IN        CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_TMR9,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA3_TMR9_CH2_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_TMR9,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA3_I2S2_MCK           CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_AF) /* Alternate function push-pull (master only) */
#define CHIP_PIN_PA3_XMC_D5             CHIP_PIN_ID         (CHIP_PORT_A, 3, CHIP_PER_ID_XMC,    CHIP_PIN_REMAP2(1,2),   CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA4  ------------------------------------------------------------ */
#define CHIP_PIN_PA4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_ADC1_IN4           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_USART2_CK          CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA4_SPI1_NSS_MST       CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* (Master) Alternate function push-pull */
#define CHIP_PIN_PA4_SPI1_NSS_SLV       CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* (Slave)  Input floating/pull-up/pull-down */
#define CHIP_PIN_PA4_I2S1_WS_MST        CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* (Master) Alternate function push-pull */
#define CHIP_PIN_PA4_I2S1_WS_SLV        CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* (Slave)  Input floating */
#define CHIP_PIN_PA4_USART6_TX          CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_USART6, CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA4_SPI3_NSS_MST       CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP2(1,3),   CHIP_PIN_MODE_AF) /* (Master) Alternate function push-pull */
#define CHIP_PIN_PA4_SPI3_NSS_SLV       CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP2(1,3),   CHIP_PIN_MODE_IN) /* (Slave)  Input floating/pull-up/pull-down */
#define CHIP_PIN_PA4_I2S3_WS_MST        CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP2(1,3),   CHIP_PIN_MODE_AF) /* (Master) Alternate function push-pull */
#define CHIP_PIN_PA4_I2S3_WS_SLV        CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP2(1,3),   CHIP_PIN_MODE_IN) /* (Slave)  Input floating */
#define CHIP_PIN_PA4_XMC_D6             CHIP_PIN_ID         (CHIP_PORT_A, 4, CHIP_PER_ID_XMC,    CHIP_PIN_REMAP2(1,2),   CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA5 ------------------------------------------------------------- */
#define CHIP_PIN_PA5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_ADC1_IN5           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_SPI1_SCK_MST       CHIP_PIN_ID         (CHIP_PORT_A, 5, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* (Master) Alternate function push-pull */
#define CHIP_PIN_PA5_SPI1_SCK_SLV       CHIP_PIN_ID         (CHIP_PORT_A, 5, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* (Slave)  Input floating */
#define CHIP_PIN_PA5_I2S1_CK_MST        CHIP_PIN_ID         (CHIP_PORT_A, 5, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* (Master) Alternate function push-pull */
#define CHIP_PIN_PA5_I2S1_CK_SLV        CHIP_PIN_ID         (CHIP_PORT_A, 5, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* (Slave)  Input floating */
#define CHIP_PIN_PA5_USART6_RX          CHIP_PIN_ID         (CHIP_PORT_A, 5, CHIP_PER_ID_USART6, CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_IN) /* Input floating/Input pull-up */
#define CHIP_PIN_PA5_XMC_D7             CHIP_PIN_ID         (CHIP_PORT_A, 5, CHIP_PER_ID_XMC,    CHIP_PIN_REMAP2(1,2),   CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA6  ------------------------------------------------------------ */
#define CHIP_PIN_PA6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 6)
#define CHIP_PIN_PA6_ADC1_IN6           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 6)
#define CHIP_PIN_PA6_SPI1_MISO_MST      CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* (Master) Input floating/Input pull-up */
#define CHIP_PIN_PA6_SPI1_MISO_SLV      CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* (Slave)  Alternate function push-pull */
#define CHIP_PIN_PA6_TMR3_CH1_IN        CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_TMR3,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA6_TMR3_CH1_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_TMR3,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA6_TMR8_BKIN          CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_TMR8,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA6_TMR13_CH1_IN       CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_TMR13,  CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA6_TMR13_CH1_OUT      CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_TMR13,  CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA6_I2S2_MCK           CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(2),     CHIP_PIN_MODE_AF) /* Alternate function push-pull (master only) */
#define CHIP_PIN_PA6_TMR1_BKIN          CHIP_PIN_ID         (CHIP_PORT_A, 6, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_IN) /* Input floating */

/*----- Пин PA7  ------------------------------------------------------------ */
#define CHIP_PIN_PA7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 7)
#define CHIP_PIN_PA7_ADC1_IN7           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 7)
#define CHIP_PIN_PA7_SPI1_MOSI_MST      CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* (Master) Alternate function push-pull */
#define CHIP_PIN_PA7_SPI1_MOSI_SLV      CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* (Slave) Input floating/Input pull-up */
#define CHIP_PIN_PA7_I2S1_SD_TR         CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* (Transiver) Alternate function push-pull */
#define CHIP_PIN_PA7_I2S1_SD_RV         CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* (Receiver) Input floating/pull-up/pull-down */
#define CHIP_PIN_PA7_TMR3_CH2_IN        CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_TMR3,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA7_TMR3_CH2_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_TMR3,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA7_TMR3_CH1N          CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_TMR3,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA7_TMR14_CH1_IN       CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_TMR14,  CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA7_TMR14_CH1_OUT      CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_TMR14,  CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA7_TMR1_CH1N          CHIP_PIN_ID         (CHIP_PORT_A, 7, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA8  ------------------------------------------------------------ */
#define CHIP_PIN_PA8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 8)
#define CHIP_PIN_PA8_CLKOUT             CHIP_PIN_ID         (CHIP_PORT_A, 8, CHIP_PER_ID_CLKOUT, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA8_USART1_CK          CHIP_PIN_ID         (CHIP_PORT_A, 8, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA8_I2C3_SCL           CHIP_PIN_ID_EX      (CHIP_PORT_A, 8, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF, CHIP_PIN_CFG_STD_OD) /* Alternate function open drain */
#define CHIP_PIN_PA8_USB_SOF            CHIP_PIN_ID         (CHIP_PORT_A, 8, CHIP_PER_ID_USB,    CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA8_TMR1_CH1_IN        CHIP_PIN_ID         (CHIP_PORT_A, 8, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA8_TMR1_CH1_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 8, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA9  ------------------------------------------------------------ */
#define CHIP_PIN_PA9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 9)
#define CHIP_PIN_PA9_USART1_TX          CHIP_PIN_ID         (CHIP_PORT_A, 9, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA9_I2C3_SMBA          CHIP_PIN_ID_EX      (CHIP_PORT_A, 9, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF, CHIP_PIN_CFG_STD_OD) /* Alternate function open drain */
#define CHIP_PIN_PA9_TMR1_CH2_IN        CHIP_PIN_ID         (CHIP_PORT_A, 9, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP2(0,1),   CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA9_TMR1_CH2_OUT       CHIP_PIN_ID         (CHIP_PORT_A, 9, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP2(0,1),   CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA10 ------------------------------------------------------------ */
#define CHIP_PIN_PA10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 10)
#define CHIP_PIN_PA10_USART1_RX         CHIP_PIN_ID         (CHIP_PORT_A, 10, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating/Input pull-up */
#define CHIP_PIN_PA10_TMR1_CH3_IN       CHIP_PIN_ID         (CHIP_PORT_A, 10, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_IN) /* Input floating */
#define CHIP_PIN_PA10_TMR1_CH3_OUT      CHIP_PIN_ID         (CHIP_PORT_A, 10, CHIP_PER_ID_TMR1,   CHIP_PIN_REMAP_DEFAULT, CHIP_PIN_MODE_AF) /* Alternate function push-pull */
#define CHIP_PIN_PA10_I2S4_MCLK         CHIP_PIN_ID         (CHIP_PORT_A, 10, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP1(1),     CHIP_PIN_MODE_AF) /* Alternate function push-pull */

/*----- Пин PA11 ------------------------------------------------------------ */
#define CHIP_PIN_PA11_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_A, 11)


/*----- Пин PA12 ------------------------------------------------------------ */
#define CHIP_PIN_PA12_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_A, 12)


/*----- Пин PA13 ------------------------------------------------------------ */
#define CHIP_PIN_PA13_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_A, 13)


/*----- Пин PA14 ------------------------------------------------------------ */
#define CHIP_PIN_PA14_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_A, 14)


/*----- Пин PA15 ------------------------------------------------------------ */
#define CHIP_PIN_PA15_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_A, 15)


/*********************************** Порт B ***********************************/
/*----- Пин PB0  ------------------------------------------------------------ */
#define CHIP_PIN_PB0_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 0)



/*----- Пин PB1  ------------------------------------------------------------ */
#define CHIP_PIN_PB1_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 1)


/*----- Пин PB2  ------------------------------------------------------------ */
#define CHIP_PIN_PB2_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 2)


/*----- Пин PB3  ------------------------------------------------------------ */
#define CHIP_PIN_PB3_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 3)


/*----- Пин PB4  ------------------------------------------------------------ */
#define CHIP_PIN_PB4_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 4)


/*----- Пин PB5  ------------------------------------------------------------ */
#define CHIP_PIN_PB5_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 5)


/*----- Пин PB6  ------------------------------------------------------------ */
#define CHIP_PIN_PB6_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 6)


/*----- Пин PB7  ------------------------------------------------------------ */
#define CHIP_PIN_PB7_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 7)


/*----- Пин PB8  ------------------------------------------------------------ */
#define CHIP_PIN_PB8_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 8)


/*----- Пин PB9  ------------------------------------------------------------ */
#define CHIP_PIN_PB9_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_B, 9)


/*----- Пин PB10 ------------------------------------------------------------ */
#define CHIP_PIN_PB10_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_B, 10)


/*----- Пин PB11 ------------------------------------------------------------ */
#define CHIP_PIN_PB11_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_B, 11)


/*----- Пин PB12 ------------------------------------------------------------ */
#define CHIP_PIN_PB12_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_B, 12)


/*----- Пин PB13 ------------------------------------------------------------ */
#define CHIP_PIN_PB13_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_B, 13)


/*----- Пин PB14 ------------------------------------------------------------ */
#define CHIP_PIN_PB14_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_B, 14)


/*----- Пин PB15 ------------------------------------------------------------ */
#define CHIP_PIN_PB15_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_B, 15)


/************************************ Порт C **********************************/
/*----- Пин PC0 ------------------------------------------------------------- */
#define CHIP_PIN_PC0_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 0)


/*----- Пин PC1 ------------------------------------------------------------- */
#define CHIP_PIN_PC1_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 1)


/*----- Пин PC2 ------------------------------------------------------------- */
#define CHIP_PIN_PC2_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 2)


/*----- Пин PC3 ------------------------------------------------------------- */
#define CHIP_PIN_PC3_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 3)


/*----- Пин PC4 ------------------------------------------------------------- */
#define CHIP_PIN_PC4_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 4)


/*----- Пин PC5 ------------------------------------------------------------- */
#define CHIP_PIN_PC5_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 5)


/*----- Пин PC6 ------------------------------------------------------------- */
#define CHIP_PIN_PC6_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 6)


/*----- Пин PC7 ------------------------------------------------------------- */
#define CHIP_PIN_PC7_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 7)


/*----- Пин PC8 ------------------------------------------------------------- */
#define CHIP_PIN_PC8_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 8)


/*----- Пин PC9 ------------------------------------------------------------- */
#define CHIP_PIN_PC9_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_C, 9)


/*----- Пин PC10 ------------------------------------------------------------- */
#define CHIP_PIN_PC10_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_C, 10)


/*----- Пин PC11 ------------------------------------------------------------- */
#define CHIP_PIN_PC11_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_C, 11)


/*----- Пин PC12 ------------------------------------------------------------- */
#define CHIP_PIN_PC12_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_C, 12)


/*----- Пин PC13 ------------------------------------------------------------- */
#define CHIP_PIN_PC13_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_C, 13)


/*----- Пин PC14 ------------------------------------------------------------- */
#define CHIP_PIN_PC14_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_C, 14)


/*----- Пин PC15 ------------------------------------------------------------- */
#define CHIP_PIN_PC15_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_C, 15)


/*********************************** Порт D ***********************************/
/*----- Пин PD0  ------------------------------------------------------------- */
#define CHIP_PIN_PD0_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 0)


/*----- Пин PD1  ------------------------------------------------------------- */
#define CHIP_PIN_PD1_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 1)


/*----- Пин PD2  ------------------------------------------------------------- */
#define CHIP_PIN_PD2_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 2)


/*----- Пин PD3  ------------------------------------------------------------- */
#define CHIP_PIN_PD3_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 3)


/*----- Пин PD4  ------------------------------------------------------------- */
#define CHIP_PIN_PD4_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 4)


/*----- Пин PD5  ------------------------------------------------------------- */
#define CHIP_PIN_PD5_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 5)


/*----- Пин PD6  ------------------------------------------------------------- */
#define CHIP_PIN_PD6_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 6)


/*----- Пин PD7  ------------------------------------------------------------- */
#define CHIP_PIN_PD7_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 7)


/*----- Пин PD8  ------------------------------------------------------------- */
#define CHIP_PIN_PD8_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 8)


/*----- Пин PD9  ------------------------------------------------------------- */
#define CHIP_PIN_PD9_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_D, 9)


/*----- Пин PD10  ------------------------------------------------------------- */
#define CHIP_PIN_PD10_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_D, 10)


/*----- Пин PD11 ------------------------------------------------------------- */
#define CHIP_PIN_PD11_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_D, 11)


/*----- Пин PD12 ------------------------------------------------------------- */
#define CHIP_PIN_PD12_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_D, 12)


/*----- Пин PD13 ------------------------------------------------------------- */
#define CHIP_PIN_PD13_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_D, 13)


/*----- Пин PD14 ------------------------------------------------------------- */
#define CHIP_PIN_PD14_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_D, 14)


/*----- Пин PD15 ------------------------------------------------------------- */
#define CHIP_PIN_PD15_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_D, 15)


/*********************************** Порт E ************************************/
/*----- Пин PE0  ------------------------------------------------------------- */
#define CHIP_PIN_PE0_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 0)


/*----- Пин PE1  ------------------------------------------------------------- */
#define CHIP_PIN_PE1_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 1)


/*----- Пин PE2  ------------------------------------------------------------- */
#define CHIP_PIN_PE2_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 2)


/*----- Пин PE3  ------------------------------------------------------------- */
#define CHIP_PIN_PE3_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 3)


/*----- Пин PE4  ------------------------------------------------------------- */
#define CHIP_PIN_PE4_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 4)


/*----- Пин PE5  ------------------------------------------------------------- */
#define CHIP_PIN_PE5_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 5)


/*----- Пин PE6  ------------------------------------------------------------- */
#define CHIP_PIN_PE6_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 6)


/*----- Пин PE7  ------------------------------------------------------------- */
#define CHIP_PIN_PE7_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 7)


/*----- Пин PE8  ------------------------------------------------------------- */
#define CHIP_PIN_PE8_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 8)


/*----- Пин PE9  ------------------------------------------------------------- */
#define CHIP_PIN_PE9_GPIO                   CHIP_PIN_ID_GPIO(CHIP_PORT_E, 9)


/*----- Пин PE10 ------------------------------------------------------------- */
#define CHIP_PIN_PE10_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_E, 10)


/*----- Пин PE11 ------------------------------------------------------------- */
#define CHIP_PIN_PE11_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_E, 11)


/*----- Пин PE12 ------------------------------------------------------------- */
#define CHIP_PIN_PE12_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_E, 12)


/*----- Пин PE13 ------------------------------------------------------------- */
#define CHIP_PIN_PE13_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_E, 13)


/*----- Пин PE14 ------------------------------------------------------------- */
#define CHIP_PIN_PE14_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_E, 14)


/*----- Пин PE15 ------------------------------------------------------------- */
#define CHIP_PIN_PE15_GPIO                  CHIP_PIN_ID_GPIO(CHIP_PORT_E, 15)



#endif // CHIP_PINS_H

