#ifndef CHIP_TARGET_STM32H7XX_H_
#define CHIP_TARGET_STM32H7XX_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "chip_cmsis.h"

#include "regs/regs_mmap.h"
#include "regs/regs_pwr.h"
#include "regs/regs_rcc.h"
#include "regs/regs_gpio.h"
#include "regs/regs_afio.h"
#include "regs/regs_usart.h"

#include "chip_config.h"

#include "init/chip_init.h"

#include "init/chip_clk.h"
#include "init/chip_per_ctl.h"

#include "chip_pins.h"



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CHIP_TARGET_STM32H7XX_H_ */
