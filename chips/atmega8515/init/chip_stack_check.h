#ifndef CHIP_STACK_CHECK_H
#define CHIP_STACK_CHECK_H

#include "chip.h"

uint16_t chip_stack_get_unused_words (void);

#endif // CHIP_STACK_CHECK_H
