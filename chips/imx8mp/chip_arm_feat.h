#ifndef CHIP_ARM_FEAT_H
#define CHIP_ARM_FEAT_H


#define CHIP_AARCH_FEAT_AES                 1
#define CHIP_AARCH_FEAT_PMULL               1
#define CHIP_AARCH_FEAT_SHA1                1
#define CHIP_AARCH_FEAT_SHA256              1
#define CHIP_AARCH_FEAT_SHA512              0
#define CHIP_AARCH_FEAT_CRC32               1
#define CHIP_AARCH_FEAT_LSE                 0

#define CHIP_AARCH_FEAT_RNG                 0
#define CHIP_AARCH_FEAT_FEAT_TLBIOS         0
#define CHIP_AARCH_FEAT_FEAT_TLBIRANGE      0

#define CHIP_AARCH_FEAT_RAS                 0
#define CHIP_AARCH_FEAT_RASv1p1             0
#define CHIP_AARCH_FEAT_DoubleFault         0
#define CHIP_AARCH_FEAT_SVE                 0
#define CHIP_AARCH_FEAT_SEL2                0 /* Secure EL2. Permits EL2 to be implemented in Secure state */
#define CHIP_AARCH_FEAT_MPAM                0
#define CHIP_AARCH_FEAT_AMUv1               0
#define CHIP_AARCH_FEAT_AMUv1p1             0 /* Armv8.6 AMU Extensions v1.1 */
#define CHIP_AARCH_FEAT_DIT                 0
#define CHIP_AARCH_FEAT_RME                 0
#define CHIP_AARCH_FEAT_CSV2                0
#define CHIP_AARCH_FEAT_CSV2_2              0
#define CHIP_AARCH_FEAT_CSV2_3              0
#define CHIP_AARCH_FEAT_CSV3                0
#define CHIP_AARCH_FEAT_NMI                 0
#define CHIP_AARCH_FEAT_RNG_TRAP            0
#define CHIP_AARCH_FEAT_FEAT_SME            0
#define CHIP_AARCH_FEAT_FEAT_SME2           0
#define CHIP_AARCH_FEAT_MTE                 0
#define CHIP_AARCH_FEAT_MTE2                0
#define CHIP_AARCH_FEAT_MTE3                0
#define CHIP_AARCH_FEAT_FEAT_SSBS           0
#define CHIP_AARCH_FEAT_FEAT_SSBS2          0
#define CHIP_AARCH_FEAT_BTI                 0

#define CHIP_AARCH_FEAT_ECV                 0 /* Armv8.6 Enhanced Counter Virtualization */
#define CHIP_AARCH_FEAT_FGT                 0 /* Armv8.6 Fine-grained Traps */
#define CHIP_AARCH_FEAT_MTPMU               0 /* Armv8.6 Multi-threaded PMU Extensions */
#define CHIP_AARCH_FEAT_VHE                 0


#define CHIP_AARCH_FEAT_TIDCP1              0
#define CHIP_AARCH_FEAT_LSE2                0

#define CHIP_AARCH_FEAT_NV                  0
#define CHIP_AARCH_FEAT_NV2                 0
#define CHIP_AARCH_FEAT_HPDS                0
#define CHIP_AARCH_FEAT_HPDS2               0
#define CHIP_AARCH_FEAT_HAFDBS              0
#define CHIP_AARCH_FEAT_BBM                 0
#define CHIP_AARCH_FEAT_LPA2                0
#define CHIP_AARCH_FEAT_PAUTH               0
#define CHIP_AARCH_FEAT_TTCNP               0

#define CHIP_AARCH_FEAT_GTG                 0 /* Guest translation granule size */



#define CHIP_AARCH_FEAT_CMOW                0 /* */
#define CHIP_AARCH_FEAT_TIDCP1              0 /* */
#define CHIP_AARCH_FEAT_nTLBPA              0
#define CHIP_AARCH_FEAT_AFP                 0
#define CHIP_AARCH_FEAT_HCX                 0
#define CHIP_AARCH_FEAT_ETS2                0
#define CHIP_AARCH_FEAT_TWED                0 /* Armv8.6 Delayed Trapping of WFE */
#define CHIP_AARCH_FEAT_XNX                 0
#define CHIP_AARCH_FEAT_PAN                 0
#define CHIP_AARCH_FEAT_PAN2                0
#define CHIP_AARCH_FEAT_PAN3                0

#define CHIP_AARCH_FEAT_PARANGE_BITS        40 /* Physical Address range 40 bits, 1TB */
#define CHIP_AARCH_FEAT_ASID_BITS           16 /* Number of ASID bits: 16 */
/* A translation granule of 4KB or 64KB (?) */




#endif // CHIP_ARM_FEAT_H
