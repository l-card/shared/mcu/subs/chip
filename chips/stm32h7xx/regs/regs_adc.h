#ifndef CHIP_STM32H7XX_REGS_ADC_H_
#define CHIP_STM32H7XX_REGS_ADC_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t ISR;              /* ADC Interrupt and Status Register,                          Address offset: 0x00 */
    __IO uint32_t IER;              /* ADC Interrupt Enable Register,                              Address offset: 0x04 */
    __IO uint32_t CR;               /* ADC control register,                                       Address offset: 0x08 */
    __IO uint32_t CFGR;             /* ADC Configuration register,                                 Address offset: 0x0C */
    __IO uint32_t CFGR2;            /* ADC Configuration register 2,                               Address offset: 0x10 */
    union {
        struct {
            __IO uint32_t SMPR1;            /* ADC sample time register 1,                         Address offset: 0x14 */
            __IO uint32_t SMPR2;            /* ADC sample time register 2,                         Address offset: 0x18 */
        };
        __IO uint32_t SMPR[2];
    };
    __IO uint32_t PCSEL;            /* ADC pre-channel selection,                                  Address offset: 0x1C */
    __IO uint32_t LTR1;             /* ADC watchdog Lower threshold register 1,                    Address offset: 0x20 */
    __IO uint32_t HTR1;             /* ADC watchdog higher threshold register 1,                   Address offset: 0x24 */
    uint32_t      RESERVED1;        /* Reserved, 0x028                                                                  */
    uint32_t      RESERVED2;        /* Reserved, 0x02C                                                                  */
    union {
        struct {
            __IO uint32_t SQR1;             /* ADC regular sequence register 1,                    Address offset: 0x30 */
            __IO uint32_t SQR2;             /* ADC regular sequence register 2,                    Address offset: 0x34 */
            __IO uint32_t SQR3;             /* ADC regular sequence register 3,                    Address offset: 0x38 */
            __IO uint32_t SQR4;             /* ADC regular sequence register 4,                    Address offset: 0x3C */
        };
        __IO uint32_t SQR[4];
    };
    __IO uint32_t DR;               /* ADC regular data register,                                  Address offset: 0x40 */
    uint32_t      RESERVED3;        /* Reserved, 0x044                                                                  */
    uint32_t      RESERVED4;        /* Reserved, 0x048                                                                  */
    __IO uint32_t JSQR;             /* ADC injected sequence register,                             Address offset: 0x4C */
    uint32_t      RESERVED5[4];     /* Reserved, 0x050 - 0x05C                                                          */
    union {
        struct {
            __IO uint32_t OFR1;             /* ADC offset register 1,                              Address offset: 0x60 */
            __IO uint32_t OFR2;             /* ADC offset register 2,                              Address offset: 0x64 */
            __IO uint32_t OFR3;             /* ADC offset register 3,                              Address offset: 0x68 */
            __IO uint32_t OFR4;             /* ADC offset register 4,                              Address offset: 0x6C */
        };
        __IO uint32_t OFR[4];
    };
    uint32_t      RESERVED6[4];     /* Reserved, 0x070 - 0x07C                                                          */
    union {
        struct {
            __IO uint32_t JDR1;             /* ADC injected data register 1,                       Address offset: 0x80 */
            __IO uint32_t JDR2;             /* ADC injected data register 2,                       Address offset: 0x84 */
            __IO uint32_t JDR3;             /* ADC injected data register 3,                       Address offset: 0x88 */
            __IO uint32_t JDR4;             /* ADC injected data register 4,                       Address offset: 0x8C */
        };
        __IO uint32_t JDR[4];
    };
    uint32_t      RESERVED7[4];     /* Reserved, 0x090 - 0x09C                                                          */
    __IO uint32_t AWD2CR;           /* ADC  Analog Watchdog 2 Configuration Register,              Address offset: 0xA0 */
    __IO uint32_t AWD3CR;           /* ADC  Analog Watchdog 3 Configuration Register,              Address offset: 0xA4 */
    uint32_t      RESERVED8;        /* Reserved, 0x0A8                                                                  */
    uint32_t      RESERVED9;        /* Reserved, 0x0AC                                                                  */
    __IO uint32_t LTR2;             /* ADC watchdog Lower threshold register 2,                    Address offset: 0xB0 */
    __IO uint32_t HTR2;             /* ADC watchdog Higher threshold register 2,                   Address offset: 0xB4 */
    __IO uint32_t LTR3;             /* ADC watchdog Lower threshold register 3,                    Address offset: 0xB8 */
    __IO uint32_t HTR3;             /* ADC watchdog Higher threshold register 3,                   Address offset: 0xBC */
    __IO uint32_t DIFSEL;           /* ADC  Differential Mode Selection Register,                  Address offset: 0xC0 */
    __IO uint32_t CALFACT;          /* ADC  Calibration Factors,                                   Address offset: 0xC4 */
    __IO uint32_t CALFACT2;         /* ADC  Linearity Calibration Factors,                         Address offset: 0xC8 */
} CHIP_REGS_ADC_T;


typedef struct {
    __IO uint32_t CSR; /* ADC Common status register, Address offset: ADC1/3 base address + 0x300 */
    uint32_t RESERVED; /* Reserved, ADC1/3 base address + 0x304 */
    __IO uint32_t CCR; /* ADC common control register, Address offset: ADC1/3 base address + 0x308 */
    __IO uint32_t CDR; /* ADC common regular data register for dual Address offset: ADC1/3 base address + 0x30C */
    __IO uint32_t CDR2; /* ADC common regular data register for 32-bit dual mode Address offset: ADC1/3 base address + 0x310 */
} CHIP_REGS_ADC_COMMON_T;


#define CHIP_MEMRGN_ADDR_PERIPH_ADC1              (CHIP_MEMRGN_ADDR_PERIPH_ADC1_2)
#define CHIP_MEMRGN_ADDR_PERIPH_ADC2              (CHIP_MEMRGN_ADDR_PERIPH_ADC1_2 + 0x100)
#define CHIP_MEMRGN_ADDR_PERIPH_ADC1_2_COM        (CHIP_MEMRGN_ADDR_PERIPH_ADC1_2 + 0x300)
#define CHIP_MEMRGN_ADDR_PERIPH_ADC3_COM          (CHIP_MEMRGN_ADDR_PERIPH_ADC3 + 0x300)


#define CHIP_REGS_ADC1                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC1)
#define CHIP_REGS_ADC2                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC2)
#define CHIP_REGS_ADC3                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC3)

#define CHIP_REGS_ADC1_2_COM                    ((CHIP_REGS_ADC_COMMON_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC1_2_COM)
#define CHIP_REGS_ADC3_COM                      ((CHIP_REGS_ADC_COMMON_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC3_COM)



#define CHIP_ADC_TS_CAL1_CBRVAL                 *((__IO uint16_t *)(0x1FF1E820UL))
#define CHIP_ADC_TS_CAL2_CBRVAL                 *((__IO uint16_t *)(0x1FF1E840UL))
#define CHIP_ADC_TS_CAL1_TMPVAL                 30
#define CHIP_ADC_TS_CAL2_TMPVAL                 110

#define CHIP_ADC_VREFINT_CAL_CBRVAL             *((__IO uint16_t *)(0x1FF1E860UL))
#define CHIP_ADC_VREFINT_CAL_VOLTAGE            (3.3F)

#define CHIP_ADC_CH_CNT                         20 /* количество каналов АЦП */
#define CHIP_ADC_REG_SEQ_MAX_LEN                16 /* максимальная длина регулярной последовательности опроса каналов */
#define CHIP_ADC_INJ_SEQ_MAX_LEN                 4
#define CHIP_ADC_LINCAL_WRD_CNT                  6 /* количество слов в коэффициенте линейной калибровки */

/* специальные входы для ADC3 */
#define CHIP_ADC3_IN_NUM_VBAT                   17
#define CHIP_ADC3_IN_NUM_VSENSE                 18
#define CHIP_ADC3_IN_NUM_VREFINT                19

/* External triggers for regular channels */
#define CHIP_ADC_EXTTRG_TIM1_OC1                0
#define CHIP_ADC_EXTTRG_TIM1_OC2                1
#define CHIP_ADC_EXTTRG_TIM1_OC3                2
#define CHIP_ADC_EXTTRG_TIM2_OC2                3
#define CHIP_ADC_EXTTRG_TIM3_TRGO               4
#define CHIP_ADC_EXTTRG_TIM4_OC4                5
#define CHIP_ADC_EXTTRG_EXTI11                  6
#define CHIP_ADC_EXTTRG_TIM8_TRGO               7
#define CHIP_ADC_EXTTRG_TIM8_TRGO2              8
#define CHIP_ADC_EXTTRG_TIM1_TRGO               9
#define CHIP_ADC_EXTTRG_TIM1_TRGO2              10
#define CHIP_ADC_EXTTRG_TIM2_TRGO               11
#define CHIP_ADC_EXTTRG_TIM4_TRGO               12
#define CHIP_ADC_EXTTRG_TIM6_TRGO               13
#define CHIP_ADC_EXTTRG_TIM15_TRGO              14
#define CHIP_ADC_EXTTRG_TIM3_OC4                15
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_ADC_EXTTRG_HRTIM1_ADCTRG1          16
#define CHIP_ADC_EXTTRG_HRTIM1_ADCTRG3          17
#endif
#define CHIP_ADC_EXTTRG_LPTIM1_OUT              18
#define CHIP_ADC_EXTTRG_LPTIM2_OUT              19
#define CHIP_ADC_EXTTRG_LPTIM3_OUT              20
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_ADC_EXTTRG_TIM23_TRGO              21
#define CHIP_ADC_EXTTRG_TIM24_TRGO              22
#endif

/* External triggers for injected channels */
#define CHIP_ADC_JEXTTRG_TIM1_TRGO              0
#define CHIP_ADC_JEXTTRG_TIM1_OC4               1
#define CHIP_ADC_JEXTTRG_TIM2_TRGO              2
#define CHIP_ADC_JEXTTRG_TIM2_OC1               3
#define CHIP_ADC_JEXTTRG_TIM3_OC4               4
#define CHIP_ADC_JEXTTRG_TIM4_TRGO              5
#define CHIP_ADC_JEXTTRG_EXTI15                 6
#define CHIP_ADC_JEXTTRG_TIM8_OC4               7
#define CHIP_ADC_JEXTTRG_TIM1_TRGO2             8
#define CHIP_ADC_JEXTTRG_TIM8_TRGO              9
#define CHIP_ADC_JEXTTRG_TIM8_TRGO2             10
#define CHIP_ADC_JEXTTRG_TIM3_OC3               11
#define CHIP_ADC_JEXTTRG_TIM3_TRGO              12
#define CHIP_ADC_JEXTTRG_TIM3_OC1               13
#define CHIP_ADC_JEXTTRG_TIM6_TRGO              14
#define CHIP_ADC_JEXTTRG_TIM15_TRGO             15
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_ADC_JEXTTRG_HRTIM_ADCTRG2          16
#define CHIP_ADC_JEXTTRG_HRTIM_ADCTRG4          17
#endif
#define CHIP_ADC_JEXTTRG_LPTIM1_OUT             18
#define CHIP_ADC_JEXTTRG_LPTIM2_OUT             19
#define CHIP_ADC_JEXTTRG_LPTIM3_OUT             20
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_ADC_JEXTTRG_TIM23_TRGO              21
#define CHIP_ADC_JEXTTRG_TIM24_TRGO              22
#endif

/*********** SAI global configuration register (SAI_GCR)  *********************/
#define CHIP_REGFLD_SAI_GCR_SYNCIN              (0x00000003UL <<  0U) /* Synchronization inputs */

/*********** ADC interrupt and status register (ADC_ISR) **********************/
#define CHIP_REGFLD_ADC_ISR_ADRDY               (0x00000001UL <<  0U) /* (RW1C) ADC Ready (ADRDY) flag  */
#define CHIP_REGFLD_ADC_ISR_EOSMP               (0x00000001UL <<  1U) /* (RW1C) ADC End of Sampling flag */
#define CHIP_REGFLD_ADC_ISR_EOC                 (0x00000001UL <<  2U) /* (RW1C) ADC End of Regular Conversion flag */
#define CHIP_REGFLD_ADC_ISR_EOS                 (0x00000001UL <<  3U) /* (RW1C) ADC End of Regular sequence of Conversions flag */
#define CHIP_REGFLD_ADC_ISR_OVR                 (0x00000001UL <<  4U) /* (RW1C) ADC overrun flag */
#define CHIP_REGFLD_ADC_ISR_JEOC                (0x00000001UL <<  5U) /* (RW1C) ADC End of Injected Conversion flag */
#define CHIP_REGFLD_ADC_ISR_JEOS                (0x00000001UL <<  6U) /* (RW1C) ADC End of Injected sequence of Conversions flag */
#define CHIP_REGFLD_ADC_ISR_AWD1                (0x00000001UL <<  7U) /* (RW1C) ADC Analog watchdog 1 flag */
#define CHIP_REGFLD_ADC_ISR_AWD2                (0x00000001UL <<  8U) /* (RW1C) ADC Analog watchdog 2 flag */
#define CHIP_REGFLD_ADC_ISR_AWD3                (0x00000001UL <<  9U) /* (RW1C) ADC Analog watchdog 3 flag */
#define CHIP_REGFLD_ADC_ISR_JQOVF               (0x00000001UL << 10U) /* (RW1C) ADC Injected Context Queue Overflow flag */
#define CHIP_REGFLD_ADC_ISR_LDORDY              (0x00000001UL << 12U) /* (RO) ADC LDO output voltage ready */
#define CHIP_REGMSK_ADC_ISR_ALL_FLAGS           (0x000007FFUL)        /* (RW1C) все биты флагов прерываний */
#define CHIP_REGMSK_ADC_ISR_RESERVED            (0xFFFFE800UL)

/*********** ADC interrupt enable register (ADC_IER) **************************/
#define CHIP_REGFLD_ADC_IER_ADRDYIE             (0x00000001UL <<  0U) /* ADC Ready (ADRDY) interrupt source */
#define CHIP_REGFLD_ADC_IER_EOSMPIE             (0x00000001UL <<  1U) /* ADC End of Sampling interrupt source */
#define CHIP_REGFLD_ADC_IER_EOCIE               (0x00000001UL <<  2U) /* ADC End of Regular Conversion interrupt source */
#define CHIP_REGFLD_ADC_IER_EOSIE               (0x00000001UL <<  3U) /* ADC End of Regular sequence of Conversions interrupt source */
#define CHIP_REGFLD_ADC_IER_OVRIE               (0x00000001UL <<  4U) /* ADC overrun interrupt source */
#define CHIP_REGFLD_ADC_IER_JEOCIE              (0x00000001UL <<  5U) /* ADC End of Injected Conversion interrupt source */
#define CHIP_REGFLD_ADC_IER_JEOSIE              (0x00000001UL <<  6U) /* ADC End of Injected sequence of Conversions interrupt source */
#define CHIP_REGFLD_ADC_IER_AWD1IE              (0x00000001UL <<  7U) /* ADC Analog watchdog 1 interrupt source */
#define CHIP_REGFLD_ADC_IER_AWD2IE              (0x00000001UL <<  8U) /* ADC Analog watchdog 2 interrupt source */
#define CHIP_REGFLD_ADC_IER_AWD3IE              (0x00000001UL <<  9U) /* ADC Analog watchdog 3 interrupt source */
#define CHIP_REGFLD_ADC_IER_JQOVFIE             (0x00000001UL << 10U) /* ADC Injected Context Queue Overflow interrupt source */
#define CHIP_REGMSK_ADC_IER_RESERVED            (0xFFFFF800UL)

/*********** ADC control register (ADC_CR)*************************************/
#define CHIP_REGFLD_ADC_CR_ADEN                 (0x00000001UL <<  0U) /* ADC Enable control */
#define CHIP_REGFLD_ADC_CR_ADDIS                (0x00000001UL <<  1U) /* ADC Disable command */
#define CHIP_REGFLD_ADC_CR_ADSTART              (0x00000001UL <<  2U) /* ADC Start of Regular conversion */
#define CHIP_REGFLD_ADC_CR_JADSTART             (0x00000001UL <<  3U) /* ADC Start of injected conversion */
#define CHIP_REGFLD_ADC_CR_ADSTP                (0x00000001UL <<  4U) /* ADC Stop of Regular conversion */
#define CHIP_REGFLD_ADC_CR_JADSTP               (0x00000001UL <<  5U) /* ADC Stop of injected conversion */
#define CHIP_REGFLD_ADC_CR_BOOST                (0x00000003UL <<  8U) /* ADC Boost Mode configuration */
#define CHIP_REGFLD_ADC_CR_ADCALLIN             (0x00000001UL << 16U) /* ADC Linearity calibration */
#define CHIP_REGFLD_ADC_CR_LINCALRDYW1          (0x00000001UL << 22U) /* ADC Linearity calibration ready Word 1 */
#define CHIP_REGFLD_ADC_CR_LINCALRDYW2          (0x00000001UL << 23U) /* ADC Linearity calibration ready Word 2 */
#define CHIP_REGFLD_ADC_CR_LINCALRDYW3          (0x00000001UL << 24U) /* ADC Linearity calibration ready Word 3 */
#define CHIP_REGFLD_ADC_CR_LINCALRDYW4          (0x00000001UL << 25U) /* ADC Linearity calibration ready Word 4 */
#define CHIP_REGFLD_ADC_CR_LINCALRDYW5          (0x00000001UL << 26U) /* ADC Linearity calibration ready Word 5 */
#define CHIP_REGFLD_ADC_CR_LINCALRDYW6          (0x00000001UL << 27U) /* ADC Linearity calibration ready Word 6 */
#define CHIP_REGFLD_ADC_CR_LINCALRDYW(i)        (0x00000001UL << (22U + i)) /* ADC Linearity calibration ready Word i+1 */
#define CHIP_REGFLD_ADC_CR_ADVREGEN             (0x00000001UL << 28U) /* ADC Voltage regulator Enable */
#define CHIP_REGFLD_ADC_CR_DEEPPWD              (0x00000001UL << 29U) /* ADC Deep power down Enable */
#define CHIP_REGFLD_ADC_CR_ADCALDIF             (0x00000001UL << 30U) /* ADC Differential Mode for calibration */
#define CHIP_REGFLD_ADC_CR_ADCAL                (0x00000001UL << 31U) /* ADC Calibration */
#define CHIP_REGMSK_ADC_CR_RESERVED             (0x003EFCC0UL)

#define CHIP_REGFLDVAL_ADC_CR_BOOST_6_25_MHZ    0 /* ADC clock ≤ 6.25 MHz */
#define CHIP_REGFLDVAL_ADC_CR_BOOST_12_5_MHZ    1 /* 6.25 MHz < ADC clock frequency ≤ 12.5 MHz */
#define CHIP_REGFLDVAL_ADC_CR_BOOST_25_MHZ      2 /* 12.5 MHz < ADC clock ≤ 25.0 MHz */
#define CHIP_REGFLDVAL_ADC_CR_BOOST_50_MHZ      3 /* 25.0 MHz < ADC clock ≤ 50.0 MHz */


/*********** ADC configuration register (ADC_CFGR) ****************************/
#define CHIP_REGFLD_ADC_CFGR_DMNGT              (0x00000003UL <<  0U) /* ADC Data Management configuration */
#define CHIP_REGFLD_ADC_CFGR_RES                (0x00000007UL <<  2U) /* ADC Data resolution */
#define CHIP_REGFLD_ADC_CFGR_EXTSEL             (0x0000001FUL <<  5U) /* ADC External trigger selection for regular group */
#define CHIP_REGFLD_ADC_CFGR_EXTEN              (0x00000003UL << 10U) /* ADC External trigger enable and polarity selection for regular channels */
#define CHIP_REGFLD_ADC_CFGR_OVRMOD             (0x00000001UL << 12U) /* ADC overrun mode */
#define CHIP_REGFLD_ADC_CFGR_CONT               (0x00000001UL << 13U) /* ADC Single/continuous conversion mode for regular conversion */
#define CHIP_REGFLD_ADC_CFGR_AUTDLY             (0x00000001UL << 14U) /* ADC Delayed conversion mode */
#define CHIP_REGFLD_ADC_CFGR_DISCEN             (0x00000001UL << 16U) /* ADC Discontinuous mode for regular channels */
#define CHIP_REGFLD_ADC_CFGR_DISCNUM            (0x00000007UL << 17U) /* ADC Discontinuous mode channel count */
#define CHIP_REGFLD_ADC_CFGR_JDISCEN            (0x00000001UL << 20U) /* ADC Discontinuous mode on injected channels */
#define CHIP_REGFLD_ADC_CFGR_JQM                (0x00000001UL << 21U) /* ADC JSQR Queue mode */
#define CHIP_REGFLD_ADC_CFGR_AWD1SGL            (0x00000001UL << 22U) /* Enable the watchdog 1 on a single channel or on all channels */
#define CHIP_REGFLD_ADC_CFGR_AWD1EN             (0x00000001UL << 23U) /* ADC Analog watchdog 1 enable on regular Channels */
#define CHIP_REGFLD_ADC_CFGR_JAWD1EN            (0x00000001UL << 24U) /* ADC Analog watchdog 1 enable on injected Channels */
#define CHIP_REGFLD_ADC_CFGR_JAUTO              (0x00000001UL << 25U) /* ADC Automatic injected group conversion */
#define CHIP_REGFLD_ADC_CFGR_AWD1CH             (0x0000001FUL << 26U) /* ADC Analog watchdog 1 Channel selection */
#define CHIP_REGFLD_ADC_CFGR_JQDIS              (0x00000001UL << 31U) /* ADC Injected queue disable */
#define CHIP_REGMSK_ADC_CFGR_RESERVED           (0x00008000UL)

#define CHIP_REGFLDVAL_ADC_CFGR_DMNGT_DR         0 /* Regular conversion data stored in DR only */
#define CHIP_REGFLDVAL_ADC_CFGR_DMNGT_DMA_SINGLE 1 /* DMA One Shot Mode selected */
#define CHIP_REGFLDVAL_ADC_CFGR_DMNGT_DFSDM      2 /* DFSDM mode selected */
#define CHIP_REGFLDVAL_ADC_CFGR_DMNGT_DMA_CIRC   3 /* DMA Circular Mode selecte */

#define CHIP_REGFLDVAL_ADC_CFGR_RES_16          0
#define CHIP_REGFLDVAL_ADC_CFGR_RES_14_LEGACY   1 /* 14 bits in legacy mode (not optimized power consumption) */
#define CHIP_REGFLDVAL_ADC_CFGR_RES_12_LEGACY   2 /* 12 bits in legacy mode (not optimized power consumption) */
#define CHIP_REGFLDVAL_ADC_CFGR_RES_14          5
#define CHIP_REGFLDVAL_ADC_CFGR_RES_12          6
#define CHIP_REGFLDVAL_ADC_CFGR_RES_10          3
#define CHIP_REGFLDVAL_ADC_CFGR_RES_8           7

#define CHIP_REGFLDVAL_ADC_CFGR_EXTEN_DIS       0 /* Hardware trigger detection disabled */
#define CHIP_REGFLDVAL_ADC_CFGR_EXTEN_RISE      1 /* Hardware trigger detection on the rising edge */
#define CHIP_REGFLDVAL_ADC_CFGR_EXTEN_FALL      2 /* Hardware trigger detection on the falling edge */
#define CHIP_REGFLDVAL_ADC_CFGR_EXTEN_BOTH      3 /* Hardware trigger detection on both the rising and falling edges */

#define CHIP_REGFLDVAL_ADC_CFGR_OVRMOD_OLD_DR   0 /* ADC_DR register is preserved with the old data when an overrun is detected */
#define CHIP_REGFLDVAL_ADC_CFGR_OVRMOD_LAST_DR  1 /* ADC_DR register is overwritten with the last conversion result when an overrun is detected. */


/*********** ADC configuration register 2 (ADC_CFGR2) *************************/
#define CHIP_REGFLD_ADC_CFGR2_ROVSE             (0x00000001UL <<  0U) /* ADC Regular group oversampler enable */
#define CHIP_REGFLD_ADC_CFGR2_JOVSE             (0x00000001UL <<  1U) /* ADC Injected group oversampler enable */
#define CHIP_REGFLD_ADC_CFGR2_OVSS              (0x0000000FUL <<  5U) /* ADC Regular Oversampling shift */
#define CHIP_REGFLD_ADC_CFGR2_TROVS             (0x00000001UL <<  9U) /* ADC Triggered regular Oversampling */
#define CHIP_REGFLD_ADC_CFGR2_ROVSM             (0x00000001UL << 10U) /* ADC Regular oversampling mode */
#define CHIP_REGFLD_ADC_CFGR2_RSHIFT1           (0x00000001UL << 11U) /* ADC Right-shift data after Offset 1 correction */
#define CHIP_REGFLD_ADC_CFGR2_RSHIFT2           (0x00000001UL << 12U) /* ADC Right-shift data after Offset 2 correction */
#define CHIP_REGFLD_ADC_CFGR2_RSHIFT3           (0x00000001UL << 13U) /* ADC Right-shift data after Offset 3 correction */
#define CHIP_REGFLD_ADC_CFGR2_RSHIFT4           (0x00000001UL << 14U) /* ADC Right-shift data after Offset 4 correction */
#define CHIP_REGFLD_ADC_CFGR2_RSHIFT(x)         (0x00000001UL << (11U + (x)))
#define CHIP_REGFLD_ADC_CFGR2_OVSR              (0x000003FFUL << 16U) /* ADC oversampling Ratio */
#define CHIP_REGFLD_ADC_CFGR2_LSHIFT            (0x0000000FUL << 28U) /* ADC Left shift factor */
#define CHIP_REGMSK_ADC_CFGR2_RESERVED          (0x0C00801CUL)

/*********** ADC sample time register 1/2 (ADC_SMPR1/2) ***********************/
#define CHIP_REGFLD_ADC_SMPR_SMP(x)             (0x00000007UL << (3*((x) % 10)))  /* ADC Channel x Sampling time selection  */
#define CHIP_REGNUM_ADC_SMPR_SMP(x)             ((x) / 10)
#define CHIP_REGMSK_ADC_SMPR_RESERVED           (0xC0000000UL)

#define CHIP_REGFLDVAL_ADC_SMPR_SMP_1_5         0 /* 1.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_2_5         1 /* 2.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_8_5         2 /* 8.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_16_5        3 /* 16.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_32_5        4 /* 32.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_64_5        5 /* 64.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_387_5       6 /* 387.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_810_5       7 /* 810.5 ADC clock cycles */

/*********** ADC channel preselection register (ADC_PCSEL) ********************/
#define CHIP_REGFLD_ADC_PCSEL_PCSEL(x)          (0x00000001UL << (0U + (x))) /* ADC channel x pre channel selection */

/*********** ADC watchdog threshold register 1, 2, 3 (ADC_LTR1/2/3) ***********/
#define CHIP_REGFLD_ADC_LTR_LT                  (0x03FFFFFFUL <<  0U) /* ADC Analog watchdog 1, 2 and 3 lower threshold */

/*********** ADC watchdog threshold register 1, 2, 3 (ADC_HTR1/2/3) ***********/
#define CHIP_REGFLD_ADC_HTR_HT                  (0x03FFFFFFUL <<  0U) /* ADC Analog watchdog 1,2 and 3 higher threshold */

/*********** ADC regular sequence register n (ADC_SQRn)************************/
#define CHIP_REGFLD_ADC_SQR1_L                  (0x0000000FUL <<  0U) /* ADC regular channel sequence length */
#define CHIP_REGFLD_ADC_SQR_SQ(x)               (0x0000001FUL <<  6U * (((x) + 1) % 5)) /* ADC x-st conversion in regular sequence */
#define CHIP_REGNUM_ADC_SQR_SQ(x)               (((x) + 1) / 5)


/*********** ADC regular Data Register (ADC_DR) *******************************/
#define CHIP_REGFLD_ADC_DR_RDATA                (0xFFFFFFFFUL <<  0U) /* ADC regular Data converted */

/*********** ADC injected sequence register (ADC_JSQR) ************************/
#define CHIP_REGFLD_ADC_JSQR_JL                 (0x00000003UL <<  0U) /* ADC injected channel sequence length */
#define CHIP_REGFLD_ADC_JSQR_JEXTSEL            (0x0000001FUL <<  2U) /* ADC external trigger selection for injected group */
#define CHIP_REGFLD_ADC_JSQR_JEXTEN             (0x00000003UL <<  7U) /* ADC external trigger enable and polarity selection for injected channels */
#define CHIP_REGFLD_ADC_JSQR_JSQ(x)             (0x0000001FUL <<  (9U + 6 * (x)))  /* ADC x-st conversion in injected sequence */
#define CHIP_REGMSK_ADC_JSQR_RESERVED           (0x04104000UL)

#define CHIP_REGFLDVAL_ADC_JSQR_JEXTEN_DIS      0 /* Hardware trigger detection disabled */
#define CHIP_REGFLDVAL_ADC_JSQR_JEXTEN_RISE     1 /* Hardware trigger detection on the rising edge */
#define CHIP_REGFLDVAL_ADC_JSQR_JEXTEN_FALL     2 /* Hardware trigger detection on the falling edge */
#define CHIP_REGFLDVAL_ADC_JSQR_JEXTEN_BOTH     3 /* Hardware trigger detection on both the rising and falling edges */

/*********** ADC injected channel y offset register (ADC_OFRy) ****************/
#define CHIP_REGFLD_ADC_OFR_OFFSET              (0x03FFFFFFUL <<  0U) /* ADC data offset y for channel programmed into bits OFFSET1_CH[4:0] */
#define CHIP_REGFLD_ADC_OFR_OFFSET_CH           (0x0000001FUL << 26U) /* ADC Channel selection for the data offset y */
#define CHIP_REGFLD_ADC_OFR_SSATE               (0x00000001UL << 31U) /* ADC Signed saturation Enable */

/*********** ADC injected channel y data register (ADC_JDRy) ******************/
#define CHIP_REGFLD_ADC_JDR_JDATA               (0xFFFFFFFFUL <<  0U) /* ADC Injected DATA */

/*********** ADC analog watchdog 2 configuration register (ADC_AWD2CR) ********/
#define CHIP_REGFLD_ADC_AWD2CR_AWD2CH(x)        (0x00000001UL <<  (0U + (x)))  /* ADC Analog watchdog 2 channel x selection */
#define CHIP_REGMSK_ADC_AWD2CR_RESERVED         (0xFFF00000UL)

/*********** ADC analog watchdog 3 configuration register (ADC_AWD3CR) ********/
#define CHIP_REGFLD_ADC_AWD3CR_AWD3CH(x)        (0x00000001UL <<  (0U + (x)))  /* ADC Analog watchdog 3 channel x selection */
#define CHIP_REGMSK_ADC_AWD3CR_RESERVED         (0xFFF00000UL)

/*********** ADC differential mode selection register (ADC_DIFSEL) ************/
#define CHIP_REGFLD_ADC_DIFSEL_DIFSEL(x)        (0x00000001UL <<  (0U + (x))) /* ADC differential modes for channels 0 to 19 */
#define CHIP_REGMSK_ADC_DIFSEL_RESERVED         (0xFFF00000UL)

/*********** ADC calibration factors register (ADC_CALFACT) *******************/
#define CHIP_REGFLD_ADC_CALFACT_CALFACT_S       (0x000007FFUL <<  0U) /* ADC calibration factors in single-ended mode */
#define CHIP_REGFLD_ADC_CALFACT_CALFACT_D       (0x000007FFUL << 16U) /* ADC calibration factors in differential mode */
#define CHIP_REGMSK_ADC_CALFACT_RESERVED        (0xF800F800UL)

/*********** ADC calibration factor register 2 (ADC_CALFACT2) *****************/
#define CHIP_REGFLD_ADC_CALFACT2_LINCALFACT     (0x3FFFFFFFUL <<  0U) /* ADC Linearity calibration factors */
#define CHIP_REGMSK_ADC_CALFACT2_RESERVED       (0xC0000000UL)

/*------------------------  ADC Common registers -----------------------------*/
/*********** ADC x common status register (ADCx_CSR) (x=1/2 or 3) *************/
#define CHIP_REGFLD_ADC_CSR_ADRDY_MST           (0x00000001UL <<  0U) /* Master ADC ready */
#define CHIP_REGFLD_ADC_CSR_EOSMP_MST           (0x00000001UL <<  1U) /* End of sampling phase flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_EOC_MST             (0x00000001UL <<  2U) /* End of regular conversion of the master ADC */
#define CHIP_REGFLD_ADC_CSR_EOS_MST             (0x00000001UL <<  3U) /* End of regular sequence flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_OVR_MST             (0x00000001UL <<  4U) /* Overrun flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_JEOC_MST            (0x00000001UL <<  5U) /* End of injected conversion of the master ADC */
#define CHIP_REGFLD_ADC_CSR_JEOS_MST            (0x00000001UL <<  6U) /* End of injected sequence flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_AWD1_MST            (0x00000001UL <<  7U) /* Analog watchdog 1 flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_AWD2_MST            (0x00000001UL <<  8U) /* Analog watchdog 2 flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_AWD3_MST            (0x00000001UL <<  9U) /* Analog watchdog 3 flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_JQOVF_MST           (0x00000001UL << 10U) /* Injected context queue overflow flag of the master ADC */
#define CHIP_REGFLD_ADC_CSR_ADRDY_SLV           (0x00000001UL << 16U) /* Slave ADC ready */
#define CHIP_REGFLD_ADC_CSR_EOSMP_SLV           (0x00000001UL << 17U) /* End of sampling phase flag of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_EOC_SLV             (0x00000001UL << 18U) /* End of regular conversion of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_EOS_SLV             (0x00000001UL << 19U) /* End of regular sequence flag of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_OVR_SLV             (0x00000001UL << 20U) /* Overrun flag of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_JEOC_SLV            (0x00000001UL << 21U) /* End of injected conversion of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_JEOS_SLV            (0x00000001UL << 22U) /* End of injected sequence flag of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_AWD1_SLV            (0x00000001UL << 23U) /* Analog watchdog 1 flag of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_AWD2_SLV            (0x00000001UL << 24U) /* Analog watchdog 2 flag of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_AWD3_SLV            (0x00000001UL << 25U) /* Analog watchdog 3 flag of the slave ADC */
#define CHIP_REGFLD_ADC_CSR_JQOVF_SLV           (0x00000001UL << 26U) /* Injected context queue overflow flag of the slave ADC */
#define CHIP_REGMSK_ADC_CSR_RESERVED            (0xF800F800UL)

/*********** ADC x common control register (ADCx_CCR) (x=1/2 or 3)*************/
#define CHIP_REGFLD_ADC_CCR_DUAL                (0x0000001FUL <<  0U) /* Dual ADC mode selection */
#define CHIP_REGFLD_ADC_CCR_DELAY               (0x0000000FUL <<  8U) /* Delay between 2 sampling phases */
#define CHIP_REGFLD_ADC_CCR_DAMDF               (0x00000003UL << 14U) /* Dual ADC mode Data format */
#define CHIP_REGFLD_ADC_CCR_CKMODE              (0x00000003UL << 16U) /* ADC clock mode */
#define CHIP_REGFLD_ADC_CCR_PRESC               (0x0000000FUL << 18U) /* ADC prescaler */
#define CHIP_REGFLD_ADC_CCR_VREFEN              (0x00000001UL << 22U) /* VREFINT enable */
#define CHIP_REGFLD_ADC_CCR_TSEN                (0x00000001UL << 23U) /* Temperature sensor enable */
#define CHIP_REGFLD_ADC_CCR_VBATEN              (0x00000001UL << 24U) /* VBAT enable */
#define CHIP_REGMSK_ADC_CCR_RESERVED            (0xFE0030E0UL)


#define CHIP_REGFLDVAL_ADC_CCR_DUAL_INDEP           0 /* Independent mode */
#define CHIP_REGFLDVAL_ADC_CCR_DUAL_COMB_REG_INJ    1 /* Combined regular simultaneous + injected simultaneous mode */
#define CHIP_REGFLDVAL_ADC_CCR_DUAL_COMB_REG_ALTTRG 2 /* Combined regular simultaneous + alternate trigger mode */
#define CHIP_REGFLDVAL_ADC_CCR_DUAL_COMB_INTERL_INJ 3 /* Combined Interleaved mode + injected simultaneous mode */
#define CHIP_REGFLDVAL_ADC_CCR_DUAL_INJ             5 /* Injected simultaneous mode only */
#define CHIP_REGFLDVAL_ADC_CCR_DUAL_REG             6 /* Regular simultaneous mode only */
#define CHIP_REGFLDVAL_ADC_CCR_DUAL_INTERL          7 /* Interleaved mode only */
#define CHIP_REGFLDVAL_ADC_CCR_DUAL_ALTTRG          9 /* Alternate trigger mode only */

#define CHIP_REGFLDVAL_ADC_CCR_DAMDF_NO_PACK    0 /* Dual ADC mode without data packing (ADCx_CDR and ADCx_CDR2 registers not used). */
#define CHIP_REGFLDVAL_ADC_CCR_DAMDF_10_32      2 /* Data formatting mode for 32 down to 10-bit resolution */
#define CHIP_REGFLDVAL_ADC_CCR_DAMDF_8          3 /* Data formatting mode for 8-bit resolution */

#define CHIP_REGFLDVAL_ADC_CCR_CKMODE_KER       0
#define CHIP_REGFLDVAL_ADC_CCR_CKMODE_SCLK      1
#define CHIP_REGFLDVAL_ADC_CCR_CKMODE_SCLK_2    2
#define CHIP_REGFLDVAL_ADC_CCR_CKMODE_SCLK_4    4

#define CHIP_REGFLDVAL_ADC_CCR_PRESC_1          0
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_2          1
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_4          2
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_6          3
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_8          4
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_10         5
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_12         6
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_16         7
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_32         8
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_64         9
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_128        10
#define CHIP_REGFLDVAL_ADC_CCR_PRESC_256        11


/*** ADC x common regular data register for dual mode (ADCx_CDR) (x=1/2 or 3) */
#define CHIP_REGFLD_ADC_CDR_RDATA_MST          (0x0000FFFFUL <<  0U) /* ADC multimode master group regular conversion data */
#define CHIP_REGFLD_ADC_CDR_RDATA_SLV          (0x0000FFFFUL << 16U) /* ADC multimode slave group regular conversion data */

/* ADC x common regular data register for 32-bit dual mode (ADCx_CDR2) (x=1/2 or 3) */
#define CHIP_REGFLD_ADC_CDR2_RDATA_ALT         (0xFFFFFFFFUL <<  0U) /* Regular data of the master/slave alternated ADCs */

#endif // CHIP_STM32H7XX_REGS_ADC_H_

