#include "archtypes/cortexm/init/chip_cortexm_isr.h"
#include <stddef.h>

CHIP_ISR_DUMMY_DECLARE()

CHIP_ISR_DECLARE(NMI_Handler);
CHIP_ISR_DECLARE(HardFault_Handler);
CHIP_ISR_DECLARE(MemManage_Handler);
CHIP_ISR_DECLARE(BusFault_Handler);
CHIP_ISR_DECLARE(UsageFault_Handler);
CHIP_ISR_DECLARE(SVC_Handler);
CHIP_ISR_DECLARE(DebugMon_Handler);
CHIP_ISR_DECLARE(PendSV_Handler);
CHIP_ISR_DECLARE(SysTick_IRQHandler);
CHIP_ISR_DECLARE(WWDG_IRQHandler);
CHIP_ISR_DECLARE(PVD_IRQHandler);
CHIP_ISR_DECLARE(TAMPER_IRQHandler);
CHIP_ISR_DECLARE(RTC_IRQHandler);
CHIP_ISR_DECLARE(FLASH_IRQHandler);
CHIP_ISR_DECLARE(RCC_IRQHandler);
CHIP_ISR_DECLARE(EXTI0_IRQHandler);
CHIP_ISR_DECLARE(EXTI1_IRQHandler);
CHIP_ISR_DECLARE(EXTI2_IRQHandler);
CHIP_ISR_DECLARE(EXTI3_IRQHandler);
CHIP_ISR_DECLARE(EXTI4_IRQHandler);
CHIP_ISR_DECLARE(TSC_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel1_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel2_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel3_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel4_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel5_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel6_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel7_IRQHandler);
CHIP_ISR_DECLARE(ADC1_2_IRQHandler);
CHIP_ISR_DECLARE(USB_HP_CAN1_TX_IRQHandler);
CHIP_ISR_DECLARE(USB_LP_CAN1_RX0_IRQHandler);
CHIP_ISR_DECLARE(CAN1_RX1_IRQHandler);
CHIP_ISR_DECLARE(CAN1_SCE_IRQHandler);
CHIP_ISR_DECLARE(EXTI9_5_IRQHandler);
CHIP_ISR_DECLARE(TIM1_BRK_IRQHandler);
CHIP_ISR_DECLARE(TIM1_UP_IRQHandler);
CHIP_ISR_DECLARE(TIM1_TRG_COM_IRQHandler);
CHIP_ISR_DECLARE(TIM1_CC_IRQHandler);
CHIP_ISR_DECLARE(TIM2_IRQHandler);
CHIP_ISR_DECLARE(TIM3_IRQHandler);
CHIP_ISR_DECLARE(TIM4_IRQHandler);
CHIP_ISR_DECLARE(I2C1_EV_IRQHandler);
CHIP_ISR_DECLARE(I2C1_ER_IRQHandler);
CHIP_ISR_DECLARE(I2C2_EV_IRQHandler);
CHIP_ISR_DECLARE(I2C2_ER_IRQHandler);
CHIP_ISR_DECLARE(SPI1_IRQHandler);
CHIP_ISR_DECLARE(SPI2_IRQHandler);
CHIP_ISR_DECLARE(USART1_IRQHandler);
CHIP_ISR_DECLARE(USART2_IRQHandler);
CHIP_ISR_DECLARE(USART3_IRQHandler);
CHIP_ISR_DECLARE(EXTI15_10_IRQHandler);
CHIP_ISR_DECLARE(RTC_Alarm_IRQHandler);
CHIP_ISR_DECLARE(USBWakeUp_IRQHandler);
#if defined CHIP_DEV_STM32F103_XE || defined CHIP_DEV_CH32F20X_D8 || defined CHIP_DEV_CH32F20X_D8C
CHIP_ISR_DECLARE(TIM8_BRK_IRQHandler);
CHIP_ISR_DECLARE(TIM8_UP_IRQHandler);
CHIP_ISR_DECLARE(TIM8_TRG_COM_IRQHandler);
CHIP_ISR_DECLARE(TIM8_CC_IRQHandler);
#if defined CHIP_DEV_STM32F103_XE
CHIP_ISR_DECLARE(ADC3_IRQHandler);
#elif defined CHIP_DEV_CH32F20X_D8  || defined CHIP_DEV_CH32F20X_D8C
CHIP_ISR_DECLARE(RNG_IRQHandler);
#endif
CHIP_ISR_DECLARE(FSMC_IRQHandler);
CHIP_ISR_DECLARE(SDIO_IRQHandler);
CHIP_ISR_DECLARE(TIM5_IRQHandler);
CHIP_ISR_DECLARE(SPI3_IRQHandler);
CHIP_ISR_DECLARE(UART4_IRQHandler);
CHIP_ISR_DECLARE(UART5_IRQHandler);
CHIP_ISR_DECLARE(TIM6_IRQHandler);
CHIP_ISR_DECLARE(TIM7_IRQHandler);
CHIP_ISR_DECLARE(DMA2_Channel1_IRQHandler);
CHIP_ISR_DECLARE(DMA2_Channel2_IRQHandler);
CHIP_ISR_DECLARE(DMA2_Channel3_IRQHandler);
#if defined CHIP_DEV_STM32F103_XE
CHIP_ISR_DECLARE(DMA2_Channel4_5_IRQHandler);
#elif defined CHIP_DEV_CH32F20X_D8 || defined CHIP_DEV_CH32F20X_D8C
CHIP_ISR_DECLARE(DMA2_Channel5_IRQHandler)
#if defined CHIP_DEV_CH32F20X_D8C
CHIP_ISR_DECLARE(ETH_IRQHandler)
CHIP_ISR_DECLARE(ETH_WKUP_IRQHandler)
CHIP_ISR_DECLARE(CAN2_TX_IRQHandler)
CHIP_ISR_DECLARE(CAN2_RX0_IRQHandler)
CHIP_ISR_DECLARE(CAN2_RX1_IRQHandler)
CHIP_ISR_DECLARE(CAN2_SCE_IRQHandler)
CHIP_ISR_DECLARE(OTG_FS_IRQHandler)
CHIP_ISR_DECLARE(USBHSWakeup_IRQHandler)
CHIP_ISR_DECLARE(USBHS_IRQHandler)
CHIP_ISR_DECLARE(DVP_IRQHandler)
#endif
CHIP_ISR_DECLARE(UART6_IRQHandler)
CHIP_ISR_DECLARE(UART7_IRQHandler)
CHIP_ISR_DECLARE(UART8_IRQHandler)
CHIP_ISR_DECLARE(TIM9_BRK_IRQHandler)
CHIP_ISR_DECLARE(TIM9_UP_IRQHandler)
CHIP_ISR_DECLARE(TIM9_TRG_COM_IRQHandler)
CHIP_ISR_DECLARE(TIM9_CC_IRQHandler)
CHIP_ISR_DECLARE(TIM10_BRK_IRQHandler)
CHIP_ISR_DECLARE(TIM10_UP_IRQHandler)
CHIP_ISR_DECLARE(TIM10_TRG_COM_IRQHandler)
CHIP_ISR_DECLARE(TIM10_CC_IRQHandler)
CHIP_ISR_DECLARE(DMA2_Channel6_IRQHandler)
CHIP_ISR_DECLARE(DMA2_Channel7_IRQHandler)
CHIP_ISR_DECLARE(DMA2_Channel8_IRQHandler)
CHIP_ISR_DECLARE(DMA2_Channel9_IRQHandler)
CHIP_ISR_DECLARE(DMA2_Channel10_IRQHandler)
CHIP_ISR_DECLARE(DMA2_Channel11_IRQHandler)
#endif
#endif
#if defined CHIP_DEV_CH32F20X_D6 || defined CHIP_DEV_CH32F20X_D8W
CHIP_ISR_DECLARE(USBHD_IRQHandler);
CHIP_ISR_DECLARE(USBHDWakeUp_IRQHandler);
#if defined CHIP_DEV_CH32F20X_D6
CHIP_ISR_DECLARE(UART4_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel8_IRQHandler);
#elif defined CHIP_DEV_CH32F20X_D8W
CHIP_ISR_DECLARE(ETH_IRQHandler)
CHIP_ISR_DECLARE(ETHWakeUp_IRQHandler)
CHIP_ISR_DECLARE(BLEC_IRQHandler)
CHIP_ISR_DECLARE(BLES_IRQHandler)
CHIP_ISR_DECLARE(TIM5_IRQHandler)
CHIP_ISR_DECLARE(UART4_IRQHandler)
CHIP_ISR_DECLARE(DMA1_Channel8_IRQHandler)
CHIP_ISR_DECLARE(OSC32KCal_IRQHandler)
CHIP_ISR_DECLARE(OSCWakeUp_IRQHandler)
#endif
#endif

#if 0
CHIP_ISR_TABLE() = {
    CHIP_ISR_TABLE_ENTRY_STACKPTR,
    CHIP_ISR_TABLE_ENTRY_RESETPTR,
    NMI_Handler,
    HardFault_Handler,
    MemManage_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    NULL,
    NULL,
    NULL,
    NULL,
    SVC_Handler,
    DebugMon_Handler,
    NULL,
    PendSV_Handler,
    SysTick_IRQHandler,
    WWDG_IRQHandler,                   /* Window WatchDog              */
    PVD_IRQHandler,                    /* PVD                          */
    TAMPER_IRQHandler,                 /* Tamper                       */
    RTC_IRQHandler,                    /* RTC through the EXTI line    */
    FLASH_IRQHandler,                  /* FLASH                        */
    RCC_IRQHandler,                    /* RCC                          */
    EXTI0_IRQHandler,                  /* EXTI Line 0                  */
    EXTI1_IRQHandler,                  /* EXTI Line 1                  */
    EXTI2_IRQHandler,                  /* EXTI Line 1                  */
    EXTI3_IRQHandler,                  /* EXTI Line 1                  */
    EXTI4_IRQHandler,                  /* EXTI Line 1                  */
    DMA1_Channel1_IRQHandler,          /* DMA1 Channel 1               */
    DMA1_Channel2_IRQHandler,          /* DMA1 Channel 2               */
    DMA1_Channel3_IRQHandler,          /* DMA1 Channel 3               */
    DMA1_Channel4_IRQHandler,          /* DMA1 Channel 4               */
    DMA1_Channel5_IRQHandler,          /* DMA1 Channel 5               */
    DMA1_Channel6_IRQHandler,          /* DMA1 Channel 6               */
    DMA1_Channel7_IRQHandler,          /* DMA1 Channel 7               */
    ADC1_2_IRQHandler,                 /* ADC1,ADC2                    */
    USB_HP_CAN1_TX_IRQHandler,
    USB_LP_CAN1_RX0_IRQHandler,
    CAN1_RX1_IRQHandler,
    CAN1_SCE_IRQHandler,
    EXTI9_5_IRQHandler,
    TIM1_BRK_IRQHandler,
    TIM1_UP_IRQHandler,
    TIM1_TRG_COM_IRQHandler,
    TIM1_CC_IRQHandler,                /* TIM1 Capture Compare         */
    TIM2_IRQHandler,                   /* TIM2                         */
    TIM3_IRQHandler,                   /* TIM3                         */
    TIM4_IRQHandler,                   /* TIM4                         */
    I2C1_EV_IRQHandler,
    I2C1_ER_IRQHandler,
    I2C2_EV_IRQHandler,
    I2C2_ER_IRQHandler,
    SPI1_IRQHandler,                   /* SPI1                         */
    SPI2_IRQHandler,                   /* SPI2                         */
    USART1_IRQHandler,                 /* USART1                       */
    USART2_IRQHandler,                 /* USART2                       */
    USART3_IRQHandler,                 /* USART3                       */
    EXTI15_10_IRQHandler,
    RTC_Alarm_IRQHandler,
    USBWakeUp_IRQHandler,
#if defined CHIP_DEV_STM32F103_XE || defined CHIP_DEV_CH32F20X_D8 || defined CHIP_DEV_CH32F20X_D8C
    TIM8_BRK_IRQHandler,
    TIM8_UP_IRQHandler,
    TIM8_TRG_COM_IRQHandler,
    TIM8_CC_IRQHandler,
#if defined CHIP_DEV_STM32F103_XE
    ADC3_IRQHandler,
#elif defined CHIP_DEV_CH32F20X_D8  || defined CHIP_DEV_CH32F20X_D8C
    RNG_IRQHandler
#endif
    FSMC_IRQHandler,
    SDIO_IRQHandler,
    TIM5_IRQHandler,
    SPI3_IRQHandler,
    UART4_IRQHandler,
    UART5_IRQHandler,
    TIM6_IRQHandler,
    TIM7_IRQHandler,
    DMA2_Channel1_IRQHandler,
    DMA2_Channel2_IRQHandler,
    DMA2_Channel3_IRQHandler,
#if defined CHIP_DEV_STM32F103_XE
    DMA2_Channel4_5_IRQHandler
#elif defined CHIP_DEV_CH32F20X_D8 || defined CHIP_DEV_CH32F20X_D8C
    DMA2_Channel5_IRQHandler,
#if defined CHIP_DEV_CH32F20X_D8C
    ETH_IRQHandler,
    ETH_WKUP_IRQHandler,
    CAN2_TX_IRQHandler,
    CAN2_RX0_IRQHandler,
    CAN2_RX1_IRQHandler,
    CAN2_SCE_IRQHandler,
    OTG_FS_IRQHandler,
    USBHSWakeup_IRQHandler,
    USBHS_IRQHandler,
    DVP_IRQHandler,
#else
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#endif
    UART6_IRQHandler,
    UART7_IRQHandler,
    UART8_IRQHandler,
    TIM9_BRK_IRQHandler,
    TIM9_UP_IRQHandler,
    TIM9_TRG_COM_IRQHandler,
    TIM9_CC_IRQHandler,
    TIM10_BRK_IRQHandler,
    TIM10_UP_IRQHandler,
    TIM10_TRG_COM_IRQHandler,
    TIM10_CC_IRQHandler,
    DMA2_Channel6_IRQHandler,
    DMA2_Channel7_IRQHandler,
    DMA2_Channel8_IRQHandler,
    DMA2_Channel9_IRQHandler,
    DMA2_Channel10_IRQHandler,
    DMA2_Channel11_IRQHandler,
#endif
#endif
#if defined CHIP_DEV_CH32F20X_D6 || defined CHIP_DEV_CH32F20X_D8W
    USBHD_IRQHandler,
    USBHDWakeUp_IRQHandler,
#if defined CHIP_DEV_CH32F20X_D6
    UART4_IRQHandler,
    DMA1_Channel8_IRQHandler,
#elif defined CHIP_DEV_CH32F20X_D8W
    ETH_IRQHandler,
    ETHWakeUp_IRQHandler,
    BLEC_IRQHandler,
    BLES_IRQHandler,
    TIM5_IRQHandler,
    UART4_IRQHandler,
    DMA1_Channel8_IRQHandler,
    OSC32KCal_IRQHandler,
    OSCWakeUp_IRQHandler,
#endif
#endif
    };
#endif
