#ifndef CHIP_V84XXX_REGS_PWR_H_
#define CHIP_V84XXX_REGS_PWR_H_

#include <stdint.h>
#include "chip_dev_spec_features.h"

typedef struct {
    __IO uint32_t CTRL;      /* PWR power control register,              Address offset: 0x00 */
    __IO uint32_t CTRLSTS;   /* PWR power control status register,       Address offset: 0x04 */
} CHIP_REGS_PWR_T;

#define CHIP_REGS_PWR          ((CHIP_REGS_PWR_T *) CHIP_MEMRGN_ADDR_PERIPH_PWR)

/*********** PWR control register (PWR_CTRL) **********************************/
#define CHIP_REGFLD_PWR_CTRL_LPDS               (0x00000001UL <<  0U)  /* (RW) Low Power consumption in Deepsleep mode when PDDS = 0 */
#define CHIP_REGFLD_PWR_CTRL_LDDS               (0x00000001UL <<  1U)  /* (RW) Power down Deep-Sleep */
#define CHIP_REGFLD_PWR_CTRL_CL_WUF             (0x00000001UL <<  2U)  /* (RCW1) Clear wakeup flag */
#define CHIP_REGFLD_PWR_CTRL_CL_SBF             (0x00000001UL <<  3U)  /* (RCW1) Clear standby flag */
#define CHIP_REGFLD_PWR_CTRL_PVDEN              (0x00000001UL <<  4U)  /* (RW) Power voltage detector enable. */
#define CHIP_REGFLD_PWR_CTRL_PVDS               (0x00000007UL <<  5U)  /* (RW) PVD level selection */
#define CHIP_REGFLD_PWR_CTRL_DBP                (0x00000001UL <<  8U)  /* (RW) Disable Back-up domain Protection */
#define CHIP_REGMSK_PWR_CTRL_RESERVED           (0xFFFFFE00UL)

#define CHIP_REGFLDVAL_PWR_CTRL_PVDS_2_3V       1
#define CHIP_REGFLDVAL_PWR_CTRL_PVDS_2_4V       2
#define CHIP_REGFLDVAL_PWR_CTRL_PVDS_2_5V       3
#define CHIP_REGFLDVAL_PWR_CTRL_PVDS_2_6V       4
#define CHIP_REGFLDVAL_PWR_CTRL_PVDS_2_7V       5
#define CHIP_REGFLDVAL_PWR_CTRL_PVDS_2_8V       6
#define CHIP_REGFLDVAL_PWR_CTRL_PVDS_2_9V       7


/*********** PWR control status register 1 (PWR_CTRLSTS) **********************/
#define CHIP_REGFLD_PWR_CTRLSTS_WUF                (0x00000001UL <<  0U) /* (RO) Wakeup flag */
#define CHIP_REGFLD_PWR_CTRLSTS_SBF                (0x00000001UL <<  1U) /* (RO) Standby flag */
#define CHIP_REGFLD_PWR_CTRLSTS_PVD                (0x00000001UL <<  2U) /* (RO) PVD output */
#define CHIP_REGFLD_PWR_CTRLSTS_WUP_EN(n)          (0x00000001UL <<  (8U + (n))) /* (RW) Enable WKUPF 1-4 pin */
#define CHIP_REGMSK_PWR_CTRLSTS_RESERVED           (0xFFFFF0F8UL)


#endif // CHIP_V84XXX_REGS_PWR_H_
