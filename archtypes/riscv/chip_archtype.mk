#CHIP_STARTUP_SRC += $(CHIP_ARCHTYPE_DIR)/init/startup_gcc.c
CHIP_INC_DIRS += $(CHIP_ARCHTYPE_DIR)
CHIP_DEFS += CHIP_ARCHTYPE_RISCV


ifneq (,$(CHIP_TARGET_CORETYPE))
    CHIP_CORETYPE_DIR := $(CHIP_ARCHTYPE_DIR)/core/$(CHIP_TARGET_CORETYPE)
    CHIP_INC_DIRS += $(CHIP_CORETYPE_DIR)
    include $(CHIP_CORETYPE_DIR)/chip_coretype_$(CHIP_TARGET_CORETYPE).mk
endif


ifneq (,$(CHIP_TARGET_ARCH))
    CHIP_ARCH_DIR := $(CHIP_ARCHTYPE_DIR)/arch/$(CHIP_TARGET_ARCH)
    include $(CHIP_ARCH_DIR)/chip_arch.mk
endif

LTIMER_PORT ?= riscv_mchtmr

CHIP_GEN_SRC += $(CHIP_ARCHTYPE_DIR)/chip_riscv_delay.c
