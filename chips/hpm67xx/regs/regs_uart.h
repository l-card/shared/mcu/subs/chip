#ifndef REGS_UART_H
#define REGS_UART_H

#include "chip_ioreg_defs.h"

typedef struct {
    uint32_t  RESERVED0;                       /* 0x0 - 0x3: Reserved */
    __IO uint32_t IDLE_CFG;                    /* 0x4: Idle Configuration Register */
    __IO uint32_t ADDR_CFG;                    /* 0x8: address match config register */
    __IO uint32_t IIR2;                        /* 0xC: Interrupt Identification Register2 */
    __IO uint32_t CFG;                         /* 0x10: Configuration query register */
    __IO uint32_t OSCR;                        /* 0x14: Oversampling control register */
    __IO uint32_t FCRR;                        /* 0x18: FIFO Control Register config */
    __IO uint32_t MOTO_CFG;                    /* 0x1C: moto system control register */
    union {
        __I  uint32_t RBR;                     /* 0x20: Receiver Buffer Register (when DLAB = 0) */
        __O  uint32_t THR;                     /* 0x20: Transmitter Holding Register (when DLAB = 0) */
        __IO uint32_t DLL;                     /* 0x20: Divisor Latch LSB (when DLAB = 1) */
    };
    union {
        __IO uint32_t IER;                     /* 0x24: Interrupt Enable Register (when DLAB = 0) */
        __IO uint32_t DLM;                     /* 0x24: Divisor Latch MSB (when DLAB = 1) */
    };
    union {
        __I  uint32_t IIR;                     /* 0x28: Interrupt Identification Register */
        __O  uint32_t FCR;                     /* 0x28: FIFO Control Register */
    };
    __IO uint32_t LCR;                         /* 0x2C: Line Control Register */
    __IO uint32_t MCR;                         /* 0x30: Modem Control Register */
    __I  uint32_t LSR;                         /* 0x34: Line Status Register */
    __I  uint32_t MSR;                         /* 0x38: Modem Status Register */
    __IO uint32_t GPR;                         /* 0x3C: GPR Register */
} CHIP_REGS_UART_T;

#define CHIP_REGS_UART0         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART0)
#define CHIP_REGS_UART1         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART1)
#define CHIP_REGS_UART2         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART2)
#define CHIP_REGS_UART3         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART3)
#define CHIP_REGS_UART4         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART4)
#define CHIP_REGS_UART5         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART5)
#define CHIP_REGS_UART6         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART6)
#define CHIP_REGS_UART7         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART7)
#define CHIP_REGS_UART8         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART8)
#define CHIP_REGS_UART9         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART9)
#define CHIP_REGS_UART10        ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART10)
#define CHIP_REGS_UART11        ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART11)
#define CHIP_REGS_UART12        ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART12)
#define CHIP_REGS_UART13        ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART13)
#define CHIP_REGS_UART14        ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART14)
#define CHIP_REGS_UART15        ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_UART15)
#define CHIP_REGS_PUART         ((CHIP_REGS_UART_T *)CHIP_MEMRGN_ADDR_PUART)


/*********** CFG - Configuration query register *******************************/
#define CHIP_REGFLD_UART_CFG_FIFOSIZE                   (0x00000003UL <<  0U) /* (RO) RXFIFO and TXFIFO depth */

#define CHIP_REGFLDVAL_UART_CFG_FIFOSIZE_16             0 /* 16 bytes */
#define CHIP_REGFLDVAL_UART_CFG_FIFOSIZE_32             1 /* 32 bytes */
#define CHIP_REGFLDVAL_UART_CFG_FIFOSIZE_64             2 /* 64 bytes */
#define CHIP_REGFLDVAL_UART_CFG_FIFOSIZE_128            3 /* 128 bytes */

/*********** OSCR - Oversampling control register *****************************/
#define CHIP_REGFLD_UART_OSCR_OSC                       (0x0000001FUL <<  0U) /* (RW) Oversampling rate settings, 8-30 (even) + 0 = 32 */

/*********** IER - Interrupt Enable Register (when DLAB = 0) ******************/
#define CHIP_REGFLD_UART_IER_EMSI                       (0x00000001UL <<  3U) /* (RW) Flow control interrupt enable */
#define CHIP_REGFLD_UART_IER_ELSI                       (0x00000001UL <<  2U) /* (RW) Receive status interrupt enable */
#define CHIP_REGFLD_UART_IER_ETHEI                      (0x00000001UL <<  1U) /* (RW) Transmit status interrupt enable */
#define CHIP_REGFLD_UART_IER_ERBI                       (0x00000001UL <<  0U) /* (RW) Receive data valid & character timeout interrupt enable  */

/*********** IIR - Interrupt Identification Register **************************/
#define CHIP_REGFLD_UART_IIR_FIFOED                     (0x00000003UL <<  6U) /* (RO) When FIFO is enabled (FIFOE bit is 1), the value of FIFOED is 0x3 */
#define CHIP_REGFLD_UART_IIR_INTRID                     (0x0000000FUL <<  0U) /* (RO) Interrupt ID */


/*********** FCR - Interrupt Identification Register **************************/
#define CHIP_REGFLD_UART_FCR_RFIFOT                     (0x00000003UL <<  6U) /* (WO) Receive FIFO trigger level */
#define CHIP_REGFLD_UART_FCR_TFIFOT                     (0x00000003UL <<  4U) /* (WO) Transmit FIFO trigger level */
#define CHIP_REGFLD_UART_FCR_DMAE                       (0x00000001UL <<  3U) /* (WO) DMA enable */
#define CHIP_REGFLD_UART_FCR_TFIFORST                   (0x00000001UL <<  2U) /* (WO) Transmit FIFO reset */
#define CHIP_REGFLD_UART_FCR_RFIFORST                   (0x00000001UL <<  1U) /* (WO) Receive FIFO reset */
#define CHIP_REGFLD_UART_FCR_FIFOE                      (0x00000001UL <<  0U) /* (WO) FIFO enabled */

/*********** LCR -  Line Control Register *************************************/
#define CHIP_REGFLD_UART_LCR_DLAB                       (0x00000001UL <<  7U) /* (RW) Frequency division parameter access control */
#define CHIP_REGFLD_UART_LCR_BC                         (0x00000001UL <<  6U) /* (RW) Break control */
#define CHIP_REGFLD_UART_LCR_SPS                        (0x00000001UL <<  5U) /* (RW) Sticky parity bit */
#define CHIP_REGFLD_UART_LCR_EPS                        (0x00000001UL <<  4U) /* (RW) Parity selection */
#define CHIP_REGFLD_UART_LCR_PEN                        (0x00000001UL <<  3U) /* (RW) Parity check is enabled */
#define CHIP_REGFLD_UART_LCR_STB                        (0x00000001UL <<  2U) /* (RW) Number of STOP bits (0 - 1, 1 & WLS=0 - 1.5, 1 & WLS!=0 - 2) */
#define CHIP_REGFLD_UART_LCR_WLS                        (0x00000003UL <<  0U) /* (RW) Word length setting */


#define CHIP_REGFLDVAL_UART_LCR_EPS_EVEN                1
#define CHIP_REGFLDVAL_UART_LCR_EPS_ODD                 0

#define CHIP_REGFLDVAL_UART_LCR_WLS_5                   0
#define CHIP_REGFLDVAL_UART_LCR_WLS_6                   1
#define CHIP_REGFLDVAL_UART_LCR_WLS_7                   2
#define CHIP_REGFLDVAL_UART_LCR_WLS_8                   3

/*********** MCR -  Modem Control Register ************************************/
#define CHIP_REGFLD_UART_MCR_AFE                        (0x00000001UL <<  5U) /* (RW) Automatic flow control enable */
#define CHIP_REGFLD_UART_MCR_LOOP                       (0x00000001UL <<  4U) /* (RW) Loopback enable */
#define CHIP_REGFLD_UART_MCR_RTS                        (0x00000001UL <<  1U) /* (RW) RTS control (1 - RTS = '0') */
#define CHIP_REGMSK_UART_MCR_RESERVED                   (0xFFFFFFCCUL)


/*********** LSR - Line Status Register ***************************************/
#define CHIP_REGFLD_UART_LSR_ERRF                       (0x00000001UL <<  7U) /* (RO) Receive FIFO error */
#define CHIP_REGFLD_UART_LSR_TEMPT                      (0x00000001UL <<  6U) /* (RO) Transmit FIFO and shift regster are both emptyr */
#define CHIP_REGFLD_UART_LSR_THRE                       (0x00000001UL <<  5U) /* (RO) Transmit FIFO empty */
#define CHIP_REGFLD_UART_LSR_LBREAK                     (0x00000001UL <<  4U) /* (RO) Line break */
#define CHIP_REGFLD_UART_LSR_FE                         (0x00000001UL <<  3U) /* (RO) Frame error */
#define CHIP_REGFLD_UART_LSR_PE                         (0x00000001UL <<  2U) /* (RO) Parity error */
#define CHIP_REGFLD_UART_LSR_OE                         (0x00000001UL <<  1U) /* (RO) Received data overrun */
#define CHIP_REGFLD_UART_LSR_DR                         (0x00000001UL <<  0U) /* (RO) Received data ready */


/*********** MSR - Modem Status Register **************************************/
#define CHIP_REGFLD_UART_MSR_CTS                        (0x00000001UL <<  4U) /* (RO) CTS signal status (0 - CTS high) */
#define CHIP_REGFLD_UART_MSR_DCTS                       (0x00000001UL <<  0U) /* (RO) CTS input signal has toggled since the last time this bit was read */



#endif // REGS_UART_H
