#ifndef CHIP_STM32G0XX_CMSIS_H_
#define CHIP_STM32G0XX_CMSIS_H_

#define __CM0PLUS_REV             0U /*!< Core Revision r0p0                            */
#define __MPU_PRESENT             1U /*!< STM32G0xx  provides an MPU                    */
#define __VTOR_PRESENT            1U /*!< Vector  Table  Register supported             */
#define __NVIC_PRIO_BITS          2U /*!< STM32G0xx uses 2 Bits for the Priority Levels */
#define __Vendor_SysTickConfig    0U /*!< Set to 1 if different SysTick Config is used  */


#endif /*CHIP_STM32G0XX_CMSIS_H_*/
