#ifndef CHIP_TARGET_BF609_H
#define CHIP_TARGET_BF609_H

#ifdef __GNUC__
    /* gcc.h в bfin-elf-gcc может определять макросы csync() и ssync(), которые
       конфликтуют с inline-функциями, которые объявлены в builtin.h.
       Для избежания этого всегда сперва включаем gcc.h и, если csync() и
       ssync() были определены в нем, то определяем __DEFINED_CSYNC и
       __DEFINED_SSYNC, которые указывают builtin.h, что не нужно эти
       функции пыиаится определять заново */
    #include <gcc.h>
    #ifdef csync
        #define __DEFINED_CSYNC
    #endif
    #ifdef ssync
        #define __DEFINED_SSYNC
    #endif
#endif

#include <cdefBF609.h>
#include <ccblkfn.h>

#define CHIP_CUR_CORE()   (*pDSPID & 1)




#include "bfin_cdefs.h"
#include "chip_pins.h"
#include "chip_cache.h"
#include "chip_interrupt.h"
#include "init/chip_init.h"


#endif // CHIP_TARGET_BF609_H

