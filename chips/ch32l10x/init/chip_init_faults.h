#ifndef CHIP_INIT_FAULTS_H
#define CHIP_INIT_FAULTS_H

#include "chip_init_wait.h"

#define CHIP_INIT_FAULT_WAIT_HSI_RDY        1
#define CHIP_INIT_FAULT_WAIT_HSE_RDY        2
#define CHIP_INIT_FAULT_WAIT_PLL1_RDY       3
#define CHIP_INIT_FAULT_WAIT_PLL2_RDY       4
#define CHIP_INIT_FAULT_WAIT_PLL3_RDY       5


#endif // CHIP_INIT_FAULTS_H
