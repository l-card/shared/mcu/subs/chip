#ifndef CHIP_PIN_DEFS_H
#define CHIP_PIN_DEFS_H


#include "lbitfield.h"
#include <stdint.h>

typedef uint32_t t_chip_pin_id;
/* gp_port  - 3 бита, номер порта GPIO (если у пина есть такая функция)
 * gp_pin   - 5 бит,  номер разряда порта GPIO (если у пина есть такая функция)
 * muxreg   - 8 бит,  номер регистра настройки мультиплексора MUXCCTL (явно не входит, т.к. если есть, то фиксированное смещение относительно padreg)
 * muxnum   - 4 бита, значение регистра мультиплексора
 * padreg   - 8 бит,  номер регистра настройки пина
 * insreg   - 7 бит,  номер регистра insreg (если применим, иначе - -1)
 * insnum   - 4 бита, значение регистра insreg
 */

#define CHIP_PIN_ID(gp_port, gp_pin, muxreg, muxnum, padreg, insreg, insnum) (\
        (((padreg)   & 0xFF) << 24) | \
        (((gp_port)  & 0x07) << 21) | \
        (((gp_pin)   & 0x1F) << 16) | \
        (((muxnum)   & 0x0F) << 12) | \
        (((insreg)   & 0x7F) <<  4) | \
        (((insnum)   & 0x0F) <<  0))


#define CHIP_PIN_ID_PAD_REGNUM(id)      (((id) >> 24) & 0xFF)
#define CHIP_PIN_ID_PORT(id)            (((id) >> 21) & 0x07)
#define CHIP_PIN_ID_HAS_PORT(id)        (CHIP_PIN_ID_PORT(id) != 0x07)
#define CHIP_PIN_ID_PIN(id)             (((id) >> 16) & 0x1F)
#define CHIP_PIN_ID_MUX_VAL(id)         (((id) >> 12) & 0x0F)
#define CHIP_PIN_ID_HAS_MUX(id)         (CHIP_PIN_ID_MUX_VAL(id) != 0x0F)
#define CHIP_PIN_ID_MUX_REGNUM(id)      (CHIP_PIN_ID_HAS_MUX(id) ? CHIP_PIN_ID_PAD_REGNUM(id) - 9 : -1)
#define CHIP_PIN_ID_INSEL_REGNUM(id)    (((id) >>  4) & 0x7F)
#define CHIP_PIN_ID_HAS_INSEL(id)       (CHIP_PIN_ID_INSEL_REGNUM(id) != 0x7F)
#define CHIP_PIN_ID_INSEL_VAL(id)       (((id) >>  0) & 0x0F)

#define CHIP_PIN_ID_INVALID             0xFFFFFFFF

/* настройка мультиплексора пина для функции GPIO. Для первых 16 пинов первого порта функция 0, для остальных - 5 */
#define CHIP_PIN_ID_GPIO_MUX_VAL(id)    (((CHIP_PIN_ID_PORT(id) == 0) && (CHIP_PIN_ID_PIN(id) < 15)) ? 0 : 5)


#define CHIP_PIN_GPIO_REGS(id)          (CHIP_REGS_GPIO(CHIP_PIN_ID_PORT(id)))
#define CHIP_PIN_GPIO_BITMSK(id)        (1UL << CHIP_PIN_ID_PIN(id))

#define CHIP_PIN_IOMUX_REG(id)          (CHIP_REGS_IOMUXC->MUXCTL[CHIP_PIN_ID_MUX_REGNUM(id)])
#define CHIP_PIN_IOPAD_REG(id)          (CHIP_REGS_IOMUXC->PADCTL[CHIP_PIN_ID_PAD_REGNUM(id)])
#define CHIP_PIN_INSEL_REG(id)          (CHIP_REGS_IOMUXC->INSEL[CHIP_PIN_ID_INSEL_REGNUM(id)])
#define CHIP_PIN_ICR_REG(id)            (CHIP_PIN_GPIO_REGS(id)->ICR[CHIP_REGFL_GPIO_ICR_REGNUM(CHIP_PIN_ID_PIN(id))])
#define CHIP_PIN_IMR_REG(id)            (CHIP_PIN_GPIO_REGS(id)->IMR)
#define CHIP_PIN_ISR_REG(id)            (CHIP_PIN_GPIO_REGS(id)->ISR)


#define CHIP_PIN_CFG_OTYPE_PP           LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_ODE, 0)  /* push-pull */
#define CHIP_PIN_CFG_OTYPE_OD           LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_ODE, 1)  /* open-drain */

#define CHIP_PIN_CFG_NOPULL             (LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_PE, 0) | LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_PUE, 0))
#define CHIP_PIN_CFG_PULLUP             (LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_PE, 1) | LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_PUE, 1))
#define CHIP_PIN_CFG_PULLDOWN           (LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_PE, 1) | LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_PUE, 0))


#define CHIP_PIN_CFG_OSPEED_LOW         LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_FSEL, 0)
#define CHIP_PIN_CFG_OSPEED_HIGH        LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_FSEL, 1)

#define CHIP_PIN_CFG_OSTRENGTH_LOW      LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_DSE, CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X1)
#define CHIP_PIN_CFG_OSTRENGTH_MED      LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_DSE, CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X2)
#define CHIP_PIN_CFG_OSTRENGTH_HIGH     LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_DSE, CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X4)
#define CHIP_PIN_CFG_OSTRENGTH_VHIGH    LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_DSE, CHIP_REGFLDVAL_IOMUXC_PADCTL_DSE_X6)

#define CHIP_PIN_CFG_ITYPE_CMOS         LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_HYS, 0)
#define CHIP_PIN_CFG_ITYPE_SCHMITT      LBITFIELD_SET(CHIP_REGFLD_IOMUXC_PADCTL_HYS, 1)

/*---- дополнительные флаги конфигурации, не относящиеся к регистру PADCTL ---*/
#define CHIP_PIN_CFG_SION               (0x00000001UL << 31) /* принудительное разрешение разрешения на вход сигнала пина (обязательно для I2C) */
/* общая маска таких флагов, для их исключения при записи в PADCTL */
#define CHIP_PIN_CFG_EX_MSK             CHIP_PIN_CFG_SION

#ifndef CHIP_PIN_SPEED_STD
    #define CHIP_PIN_SPEED_STD          CHIP_PIN_CFG_OSPEED_LOW
#endif
#ifndef CHIP_PIN_OSTRENGTH_STD
    #define CHIP_PIN_OSTRENGTH_STD      CHIP_PIN_CFG_OSTRENGTH_VHIGH
#endif
#define CHIP_PIN_CFG_STD                (CHIP_PIN_SPEED_STD | CHIP_PIN_OSTRENGTH_STD | CHIP_PIN_CFG_NOPULL)



/* функция, настраивающая пин по ID, с явным заданием специальных режимов через объединение по или CHIP_PIN_CFG */
#define CHIP_PIN_CONFIG_EX(id, cfg) do { \
    CHIP_PIN_IOPAD_REG(id) = (cfg) & ~CHIP_PIN_CFG_EX_MSK; \
    if (CHIP_PIN_ID_HAS_MUX(id)) { \
        CHIP_PIN_IOMUX_REG(id) = LBITFIELD_SET(CHIP_REGFLD_IOMUXC_MUXCTL_MUXMODE, CHIP_PIN_ID_MUX_VAL(id)) \
            | LBITFIELD_SET(CHIP_REGFLD_IOMUXC_MUXCTL_SION, !!((cfg) & CHIP_PIN_CFG_SION));\
    } \
    if (CHIP_PIN_ID_HAS_INSEL(id)) { \
        CHIP_PIN_INSEL_REG(id) = CHIP_PIN_ID_INSEL_VAL(id); \
    } \
} while (0)

/* функция, настраивающая пин по ID, использующая спец. настройки по умолчанию */
#define CHIP_PIN_CONFIG(id) do { \
    CHIP_PIN_CONFIG_EX(id, CHIP_PIN_CFG_STD); \
} while(0)

/* настройка пина на оптимальный режим, если не используется (аналоговый режим для отключения входа/выхода) */
#define CHIP_PIN_CONFIG_DIS(id) do { \
    CHIP_PIN_IOPAD_REG(id) = CHIP_PIN_CFG_PULLDOWN; \
    if (CHIP_PIN_ID_HAS_PORT(id)) { \
        CHIP_PIN_SET_DIR_IN(id); \
        CHIP_PIN_IOMUX_REG(id) = LBITFIELD_SET(CHIP_REGFLD_IOMUXC_MUXCTL_MUXMODE, CHIP_PIN_ID_GPIO_MUX_VAL(id));\
    } \
} while(0)

/* управление направлением */
#define CHIP_PIN_DIR_GET(id)        (!!(CHIP_PIN_GPIO_REGS(id)->GDIR & CHIP_PIN_GPIO_BITMSK(id)))
#define CHIP_PIN_SET_DIR_IN(id)     (CHIP_PIN_GPIO_REGS(id)->GDIR &= ~CHIP_PIN_GPIO_BITMSK(id))
#define CHIP_PIN_SET_DIR_OUT(id)    (CHIP_PIN_GPIO_REGS(id)->GDIR |= CHIP_PIN_GPIO_BITMSK(id))
/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val)       do {\
        if (val) { \
            CHIP_PIN_GPIO_REGS(id)->DR |= CHIP_PIN_GPIO_BITMSK(id);\
        } else { \
            CHIP_PIN_GPIO_REGS(id)->DR &= ~CHIP_PIN_GPIO_BITMSK(id);\
        } \
    } while(0)
/* чтение уровня на ножке (0 или 1) */
#define CHIP_PIN_IN(id)             ((CHIP_PIN_GPIO_REGS(id)->PSR & CHIP_PIN_GPIO_BITMSK(id)) ? 1 : 0)
/* атомарное инвертирование уровня на ножке */
#define CHIP_PIN_TOGGLE(id)         (CHIP_PIN_GPIO_REGS(id)->DR ^= CHIP_PIN_GPIO_BITMSK(id))
/* состояние регистра выхода (независимо от физического уровня) */
#define CHIP_PIN_GET_OUT_VAL(id)    (!!(CHIP_PIN_GPIO_REGS(id)->DR & CHIP_PIN_GPIO_BITMSK(id)))


/* вспомогательный макрос, который конфигурирует порт на вывод с установленным
   начальным значением */
#define CHIP_PIN_CONFIG_OUT(id, val) do { \
    CHIP_PIN_OUT(id, val); \
    CHIP_PIN_SET_DIR_OUT(id); \
    CHIP_PIN_CONFIG(id); \
} while(0)
/* вспомогательный макрос для настройки пина и конфигурирования его на вход */
#define CHIP_PIN_CONFIG_IN(id) do { \
    CHIP_PIN_SET_DIR_IN(id); \
    CHIP_PIN_CONFIG(id); \
} while(0)

#define CHIP_PIN_CONFIG_OUT_EX(id, cfg, val) do { \
    CHIP_PIN_OUT(id, val); \
    CHIP_PIN_SET_DIR_OUT(id); \
    CHIP_PIN_CONFIG_EX(id, cfg); \
} while(0)

#define CHIP_PIN_CONFIG_IN_EX(id, cfg) do { \
    CHIP_PIN_SET_DIR_IN(id); \
    CHIP_PIN_CONFIG_EX(id, cfg); \
} while(0)


/* Макросы для работы с прерываниями */

#define CHIP_PIN_EXTINT_COND_LOW                CHIP_REGFLDVAL_GPIO_ICR_LOW
#define CHIP_PIN_EXTINT_COND_HIGH               CHIP_REGFLDVAL_GPIO_ICR_HIGH
#define CHIP_PIN_EXTINT_COND_RISE               CHIP_REGFLDVAL_GPIO_ICR_RISE
#define CHIP_PIN_EXTINT_COND_FALL               CHIP_REGFLDVAL_GPIO_ICR_FALL

#define CHIP_PIN_EXTINT_CONFIG(id, cond) do { \
    LBITFIELD_UPD(CHIP_PIN_ICR_REG(id), CHIP_REGFL_GPIO_ICR_FLD(CHIP_PIN_ID_PIN(id)), cond); \
} while(0)

#define CHIP_PIN_EXTINT_EN(id)      (CHIP_PIN_IMR_REG(id) |= CHIP_PIN_GPIO_BITMSK(id))
#define CHIP_PIN_EXTINT_DIS(id)     (CHIP_PIN_IMR_REG(id) &= ~CHIP_PIN_GPIO_BITMSK(id))
#define CHIP_PIN_EXTINT_STATUS(id)  !!(CHIP_PIN_ISR_REG(id) & CHIP_PIN_GPIO_BITMSK(id))
#define CHIP_PIN_EXTINT_CLR(id)     (CHIP_PIN_ISR_REG(id) = CHIP_PIN_GPIO_BITMSK(id))


#endif // CHIP_PIN_DEFS_H
