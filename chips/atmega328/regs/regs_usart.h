#ifndef REGS_USART_H
#define REGS_USART_H

#include "chip_devtype_spec_features.h"

#define CHIP_REG_UCSRA      _MMIO_BYTE(0xC0) /* USART Control and Status Register A */
#define CHIP_REG_UCSRB      _MMIO_BYTE(0xC1) /* USART Control and Status Register B */
#define CHIP_REG_UCSRC      _MMIO_BYTE(0xC2) /* USART Control and Status Register C */
#define CHIP_REG_UBRRL      _MMIO_BYTE(0xC4) /* USART Baud Rate Register Low Byte */
#define CHIP_REG_UBRRH      _MMIO_BYTE(0xC5) /* USART Baud Rate Register High Byte */
#define CHIP_REG_UDR        _MMIO_BYTE(0xC6) /* USART Data Register */


/* ---------------- UCSRA – USART Control and Status Register A --------------*/
#define CHIP_REGFLD_UCSRA_RXC               (0x01U << 7) /* (RO) Receive complete flag */
#define CHIP_REGFLD_UCSRA_TXC               (0x01U << 6) /* (RW1C) Transmit complete flag */
#define CHIP_REGFLD_UCSRA_UDRE              (0x01U << 5) /* (RO) Data register empty flag */
#define CHIP_REGFLD_UCSRA_FE                (0x01U << 4) /* (RO) Framing error flag */
#define CHIP_REGFLD_UCSRA_DOR               (0x01U << 3) /* (RO) Data overflow flag */
#define CHIP_REGFLD_UCSRA_PE                (0x01U << 2) /* (RO) Parity error flag */
#define CHIP_REGFLD_UCSRA_U2X               (0x01U << 1) /* (RW) Double-speed transmission enable */
#define CHIP_REGFLD_UCSRA_MPCM              (0x01U << 1) /* (RW) Multiprocessor communication mode enable */

/* ---------------- UCSRB – USART Control and Status Register B --------------*/
#define CHIP_REGFLD_UCSRB_RXC_IE            (0x01U << 7) /* (RW) Receive complete interrupt enable */
#define CHIP_REGFLD_UCSRB_TXC_IE            (0x01U << 6) /* (RW) Transmit complete interrupt enable */
#define CHIP_REGFLD_UCSRB_UDR_IE            (0x01U << 5) /* (RW) Data register empty interrupt enable */
#define CHIP_REGFLD_UCSRB_RX_EN             (0x01U << 4) /* (RW) Receive enable */
#define CHIP_REGFLD_UCSRB_TX_EN             (0x01U << 3) /* (RW) Transmit enable */
#define CHIP_REGFLD_UCSRB_UCSZ_2            (0x01U << 2) /* (RW) Character length control bit 2 */
#define CHIP_REGFLD_UCSRB_RXB8              (0x01U << 1) /* (RO) Received the 8th bit of data */
#define CHIP_REGFLD_UCSRB_TXB8              (0x01U << 0) /* (RW) 8th bit of data to transmit */

/* ---------------- UCSRC – USART Control and Status Register C --------------*/
#define CHIP_REGFLD_UCSRC_UMSEL             (0x03U << 6) /* (RW) USART mode selection */
#define CHIP_REGFLD_UCSRC_UPM               (0x03U << 4) /* (RW) Parity mode selection */
#define CHIP_REGFLD_UCSRC_USBS              (0x01U << 3) /* (RW) Stop bit selection */
#define CHIP_REGFLD_UCSRC_UCSZ_10           (0x03U << 1) /* (RW) Character length control bits 1:0 */
#define CHIP_REGFLD_UCSRC_UCPOL             (0x01U << 0) /* (RW) Clock polarity selection */
/* USPI mode bits */
#define CHIP_REGFLD_UCSRC_DORD              (0x01U << 2) /* (RW) Data transfer order selection */
#define CHIP_REGFLD_UCSRC_UCPHA             (0x01U << 1) /* (RW) Clock phase selection */
#define CHIP_REGFLD_UCSRC_UCPOL             (0x01U << 0) /* (RW) Clock polarity selection */



#define CHIP_REGFLDVAL_UCSRC_UMSEL_ASYNC    0
#define CHIP_REGFLDVAL_UCSRC_UMSEL_SYNC     1
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLDVAL_UCSRC_UMSEL_SPI_SLV  2
#endif
#define CHIP_REGFLDVAL_UCSRC_UMSEL_SPI_MST  3

#define CHIP_REGFLDVAL_UCSRC_UPM_DIS        0
#define CHIP_REGFLDVAL_UCSRC_UPM_EVEN       2
#define CHIP_REGFLDVAL_UCSRC_UPM_ODD        3

#define CHIP_REGFLDVAL_UCSRC_USBS_1         0 /* 1 stop bit */
#define CHIP_REGFLDVAL_UCSRC_USBS_2         1 /* 2 stop bit */

#define CHIP_REGFLDVAL_UCSR_UCSZ_5          0 /* 5 data bit */
#define CHIP_REGFLDVAL_UCSR_UCSZ_6          1 /* 6 data bit */
#define CHIP_REGFLDVAL_UCSR_UCSZ_7          2 /* 7 data bit */
#define CHIP_REGFLDVAL_UCSR_UCSZ_8          3 /* 8 data bit */
#define CHIP_REGFLDVAL_UCSR_UCSZ_9          7 /* 9 data bit */


#define CHIP_REGFLDVAL_UCSRC_DORD_MSB_FIRST 0
#define CHIP_REGFLDVAL_UCSRC_DORD_LSB_FIRST 1

#endif // REGS_USART_H
