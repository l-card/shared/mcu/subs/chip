#include "archtypes/cortexm/init/chip_cortexm_isr.h"
#include <stddef.h>

CHIP_ISR_DUMMY_DECLARE()

CHIP_ISR_DECLARE(NMI_Handler);
CHIP_ISR_DECLARE(HardFault_Handler);
CHIP_ISR_DECLARE(SVC_Handler);
CHIP_ISR_DECLARE(PendSV_Handler);
CHIP_ISR_DECLARE(SysTick_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT0_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT1_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT2_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT3_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT4_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT5_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT6_IRQHandler);
CHIP_ISR_DECLARE(PIN_INT7_IRQHandler);
CHIP_ISR_DECLARE(GINT0_IRQHandler);
CHIP_ISR_DECLARE(GINT1_IRQHandler);
CHIP_ISR_DECLARE(I2C1_IRQHandler);
CHIP_ISR_DECLARE(USART1_4_IRQHandler);
CHIP_ISR_DECLARE(USART2_3_IRQHandler);
CHIP_ISR_DECLARE(SCT0_1_IRQHandler);
CHIP_ISR_DECLARE(SSP1_IRQHandler);
CHIP_ISR_DECLARE(I2C0_IRQHandler);
CHIP_ISR_DECLARE(CT16B0_IRQHandler);
CHIP_ISR_DECLARE(CT16B1_IRQHandler);
CHIP_ISR_DECLARE(CT32B0_IRQHandler);
CHIP_ISR_DECLARE(CT32B1_IRQHandler);
CHIP_ISR_DECLARE(SSP0_IRQHandler);
CHIP_ISR_DECLARE(USART0_IRQHandler);
CHIP_ISR_DECLARE(USB_IRQHandler);
CHIP_ISR_DECLARE(USB_FIQHandler);
CHIP_ISR_DECLARE(ADC_A_IRQHandler);
CHIP_ISR_DECLARE(RTC_IRQHandler);
CHIP_ISR_DECLARE(BOD_WDT_IRQHandler);
CHIP_ISR_DECLARE(FLASH_IRQHandler);
CHIP_ISR_DECLARE(DMA_IRQHandler);
CHIP_ISR_DECLARE(ADC_B_IRQHandler);
CHIP_ISR_DECLARE(USBWakeup_IRQHandler);


CHIP_ISR_TABLE() = {
    CHIP_ISR_TABLE_ENTRY_STACKPTR,
    CHIP_ISR_TABLE_ENTRY_RESETPTR,
    NMI_Handler,
    HardFault_Handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    SVC_Handler,
    NULL,
    NULL,
    PendSV_Handler,
    SysTick_IRQHandler,
    PIN_INT0_IRQHandler,               /* 0 - GPIO pin interrupt 0 (PSTAT - pin interrupt status) */
    PIN_INT1_IRQHandler,               /* 1 - GPIO pin interrupt 1 (PSTAT - pin interrupt status) */
    PIN_INT2_IRQHandler,               /* 2 - GPIO pin interrupt 2 (PSTAT - pin interrupt status) */
    PIN_INT3_IRQHandler,               /* 3 - GPIO pin interrupt 3 (PSTAT - pin interrupt status) */
    PIN_INT4_IRQHandler,               /* 4 - GPIO pin interrupt 4 (PSTAT - pin interrupt status) */
    PIN_INT5_IRQHandler,               /* 5 - GPIO pin interrupt 5 (PSTAT - pin interrupt status) */
    PIN_INT6_IRQHandler,               /* 6 - GPIO pin interrupt 6 (PSTAT - pin interrupt status) */
    PIN_INT7_IRQHandler,               /* 7 - GPIO pin interrupt 7 (PSTAT - pin interrupt status) */
    GINT0_IRQHandler,                  /* 8 - GPIO GROUP0 interrupt (INT - group interrupt status) */
    GINT1_IRQHandler,                  /* 9 - GPIO GROUP1 interrupt (INT - group interrupt status) */
    I2C1_IRQHandler,                   /* 10 - I2C1 interrupt (SI (state change)) */
    USART1_4_IRQHandler,               /* 11 - Combined USART1 andUSART4 interrupts */
    USART2_3_IRQHandler,               /* 12 - Combined USART2 andUSART3 interrupts */
    SCT0_1_IRQHandler,                 /* 13 - Combined SCT0 and SCT1 interrupts */
    SSP1_IRQHandler,                   /* 14 - SSP1 interrupt */
    I2C0_IRQHandler,                   /* 15 - I2C0 interrupt */
    CT16B0_IRQHandler,                 /* 16 - CT16B0 interrupt */
    CT16B1_IRQHandler,                 /* 17 - CT16B1 interrupt*/
    CT32B0_IRQHandler,                 /* 18 - CT32B0 interrupt */
    CT32B1_IRQHandler,                 /* 19 - CT32B1 interrupt */
    SSP0_IRQHandler,                   /* 20 - SSP0 interrupt */
    USART0_IRQHandler,                 /* 21 - USART interrupt */
    USB_IRQHandler,                    /* 22 - USB_IRQ interrupt */
    USB_FIQHandler,                    /* 23 - USB_FIQ interrupt */
    ADC_A_IRQHandler,                  /* 24 - ADC interrupt A */
    RTC_IRQHandler,                    /* 25 - RTC interrupt */
    BOD_WDT_IRQHandler,                /* 26 - Combined BOD and WWDT interrupt*/
    FLASH_IRQHandler,                  /* 27 - Flash/EEPROM interrupt*/
    DMA_IRQHandler,                    /* 28 - DMA interrupt */
    ADC_B_IRQHandler,                  /* 29 - ADC interrupt B */
    USBWakeup_IRQHandler,              /* 30 - USB_WAKEUP interrupt */
    NULL
};
