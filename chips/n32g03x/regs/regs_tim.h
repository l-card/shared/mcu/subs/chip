#ifndef CHIP_N32G03X_REGS_TIM_H
#define CHIP_N32G03X_REGS_TIM_H

#define CHIP_SUPPORT_TIM_UIF_REMAP              0
#define CHIP_SUPPORT_TIM_OUT_5_6                1
#define CHIP_SUPPORT_TIM_EXT_SYNC_MODE          0
#define CHIP_SUPPORT_TIM_EXT_BREAK              0
#define CHIP_SUPPORT_TIM_EXT_OUT_MODE           0
#define CHIP_SUPPORT_TIM_CH_GROUP               0
#define CHIP_SUPPORT_TIM_ALTFUNC                0

#include "chip_devtype_spec_features.h"
#include "stm32_tim_gen_regs.h"

#define CHIP_REGS_TIM1              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM1)
#define CHIP_REGS_TIM3              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM3)
#define CHIP_REGS_TIM6              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM6)
#define CHIP_REGS_TIM8              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM8)


/* Internal triggers mapping */
#define CHIP_TIM1_ITR_TIM3          CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2  /* TIM3 -> TIM1 */
#define CHIP_TIM8_ITR_TIM1          CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1 -> TIM8 */
#define CHIP_TIM3_ITR_TIM1          CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0  /* TIM1 -> TIM3 */


/*********** TIMx control register 1 (TIMx_CR1)  ******************************/
#define CHIP_REGFLD_TIM_CR1_PBKP_EN                 (0x00000001UL << 17U)     /* (RW) Enable PVD as BRK source */
#define CHIP_REGFLD_TIM_CR1_LOCKUP_EN               (0x00000001UL << 16U)     /* (RW) Enable LockUp (Cortex®-M0 Hardfault) as BRK source */
#define CHIP_REGFLD_TIM_CR1_CLR_SEL                 (0x00000001UL << 15U)     /* (RW) OCxRef selection */
#define CHIP_REGFLD_TIM_CR1_C1_SEL                  (0x00000001UL << 11U)     /* (RW) Channel 1 selection */
#define CHIP_REGFLD_TIM_CR1_IOMBKP_EN               (0x00000001UL << 10U)     /* (RW) Enable IOM as BKP Enable */

#define CHIP_REGFLDVAL_TIM_CR1_CLR_SEL_ETR          0 /* external ETR signal */
#define CHIP_REGFLDVAL_TIM_CR1_CLR_SEL_COMP         1 /* internal COMP signal */

#define CHIP_REGFLDVAL_TIM_CR1_C1_SEL_IOM           0 /* external IOM signal */
#define CHIP_REGFLDVAL_TIM_CR1_C1_SEL_COMP          1 /* internal COMP signal */


#endif // CHIP_N32G03X_REGS_TIM_H
