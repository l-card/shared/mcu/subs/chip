#ifndef CHIP_ARMV7EM_REGS_CORE_ACCCTL_H
#define CHIP_ARMV7EM_REGS_CORE_ACCCTL_H

#include <stdint.h>

typedef struct {
    __IOM uint32_t ITCMCR;                 /* Offset: 0x290 (R/W)  Instruction Tightly-Coupled Memory Control Register */
    __IOM uint32_t DTCMCR;                 /* Offset: 0x294 (R/W)  Data Tightly-Coupled Memory Control Registers */
    __IOM uint32_t AHBPCR;                 /* Offset: 0x298 (R/W)  AHBP Control Register */
    __IOM uint32_t CACR;                   /* Offset: 0x29C (R/W)  L1 Cache Control Register */
    __IOM uint32_t AHBSCR;                 /* Offset: 0x2A0 (R/W)  AHB Slave Control Register */
    uint32_t RESERVED8[1U];
    __IOM uint32_t ABFSR;                  /* Offset: 0x2A8 (R/W)  Auxiliary Bus Fault Status Register */
} CHIP_REGS_ARMV7EM_ACCCTL_T;


#define CHIP_REGS_ARM_ACCCTL                    ((CHIP_REGS_ARMV7EM_ACCCTL_T *) CHIP_MEMRGN_ADDR_ARMV7EM_ACCCTL)

/* Instruction and data tightly-coupled memory control registers (ARM_ACCCTL_ITCMR/DTCMR) ****/
#define CHIP_REGFLD_ARM_ACCCTL_TCMCR_EN         (0x00000001UL <<  0U) /* (RW) TCM enable. When a TCM is disabled all accesses are made to the AXI master. */
#define CHIP_REGFLD_ARM_ACCCTL_TCMCR_RMW        (0x00000001UL <<  1U) /* (RW) Read-Modify-Write (RMW) enable. Indicates that all sub-chunk writes to a given TCM use a RMW sequence */
#define CHIP_REGFLD_ARM_ACCCTL_TCMCR_RETEN      (0x00000001UL <<  2U) /* (RW) Retry phase enable. When enabled the processor guarantees to honor the retry output on the corresponding TCM interface */
#define CHIP_REGFLD_ARM_ACCCTL_TCMCR_SZ         (0x0000000FUL <<  3U) /* (RO) TCM size */

#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_NOT_IMPL 0
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_4KB      3
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_8KB      4
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_16KB     5
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_32KB     6
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_64KB     7
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_128KB    8
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_256KB    9
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_512KB    10
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_1MB      11
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_2MB      12
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_4MB      13
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_8MB      14
#define CHIP_REGFLDVAL_ARM_ACCCTL_TCMCR_SZ_16MB     15


/*********** AHBP control register (ARM_ACCCTL_AHBP) **************************/
#define CHIP_REGFLD_ARM_ACCCTL_AHBPCR_EN        (0x00000001UL <<  0U) /* (RW) AHBP enable */
#define CHIP_REGFLD_ARM_ACCCTL_AHBPCR_SZ        (0x00000007UL <<  1U) /* (RO) AHBP size */

#define CHIP_REGFLDVAL_ARM_ACCCTL_AHBPCR_SZ_64MB    1
#define CHIP_REGFLDVAL_ARM_ACCCTL_AHBPCR_SZ_128MB   2
#define CHIP_REGFLDVAL_ARM_ACCCTL_AHBPCR_SZ_256MB   3
#define CHIP_REGFLDVAL_ARM_ACCCTL_AHBPCR_SZ_512MB   4


/*********** Auxiliary cache control register (ARM_ACCCTL_CACR) ***************/
#define CHIP_REGFLD_ARM_ACCCTL_CACR_SIWT        (0x00000001UL <<  0U) /* (RW) Enables cache coherency usage */
#define CHIP_REGFLD_ARM_ACCCTL_CACR_ECCEN       (0x00000001UL <<  1U) /* (RW) Enables ECC in the instruction and data cache */
#define CHIP_REGFLD_ARM_ACCCTL_CACR_FORCEWT     (0x00000001UL <<  2U) /* (RW) Enables Force Write-through in the data cache */

/*********** AHB slave control register (ARM_ACCCTL_AHBSCR) *******************/
#define CHIP_REGFLD_ARM_ACCCTL_AHBSCR_CTL       (0x00000003UL <<  0U) /* AHBS prioritization control */
#define CHIP_REGFLD_ARM_ACCCTL_AHBSCR_TPRI      (0x000001FFUL <<  2U) /* Threshold execution priority for AHBS traffic demotion. */
#define CHIP_REGFLD_ARM_ACCCTL_AHBSCR_INITCOUNT (0x0000001FUL << 11U) /* Fairness counter initialization value */

/*********** Auxiliary bus fault status register (ARM_ACCCTL_ABFSR) ***********/
#define CHIP_REGFLD_ARM_ACCCTL_ABFSR_ITCM       (0x00000001UL <<  0U) /* Asynchronous fault on ITCM interface */
#define CHIP_REGFLD_ARM_ACCCTL_ABFSR_DTCM       (0x00000001UL <<  1U) /* Asynchronous fault on DTCM interface */
#define CHIP_REGFLD_ARM_ACCCTL_ABFSR_AHBP       (0x00000001UL <<  2U) /* Asynchronous fault on AHBP interface */
#define CHIP_REGFLD_ARM_ACCCTL_ABFSR_AXIM       (0x00000001UL <<  3U) /* Asynchronous fault on AXIM interface */
#define CHIP_REGFLD_ARM_ACCCTL_ABFSR_EPPB       (0x00000001UL <<  4U) /* Asynchronous fault on EPPB interface */
#define CHIP_REGFLD_ARM_ACCCTL_ABFSR_AXIMTYPE   (0x00000003UL <<  8U) /* Indicates the type of fault on the AXIM interface */

#endif // CHIP_ARMV7EM_REGS_CORE_ACCCTL_H
