#ifndef CHIP_STM32F10X_REGS_ADC_H
#define CHIP_STM32F10X_REGS_ADC_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t SR;               /* ADC Status Register,                                        Address offset: 0x00 */
    __IO uint32_t CR1;              /* ADC control register 1,                                     Address offset: 0x04 */
    __IO uint32_t CR2;              /* ADC control register 2,                                     Address offset: 0x08 */
    union {
        struct {
            __IO uint32_t SMPR1;            /* ADC sample time register 1,                         Address offset: 0x0C */
            __IO uint32_t SMPR2;            /* ADC sample time register 2,                         Address offset: 0x10 */
        };
        __IO uint32_t SMPR[2];
    };
#if CHIP_SUPPORT_ADC_INJECTED
    union {
        struct {
            __IO uint32_t JOFR1;             /* ADC injected channel data offset register 1,       Address offset: 0x14 */
            __IO uint32_t JOFR2;             /* ADC injected channel data offset register 2,       Address offset: 0x18 */
            __IO uint32_t JOFR3;             /* ADC injected channel data offset register 3,       Address offset: 0x1C */
            __IO uint32_t JOFR4;             /* ADC injected channel data offset register 4,       Address offset: 0x20 */
        };
        __IO uint32_t JOFR[4];
    };
#else
    uint32_t RESERVED1[4];
#endif
    __IO uint32_t HTR;              /* ADC watchdog higher threshold register,                     Address offset: 0x24 */
    __IO uint32_t LTR;              /* ADC watchdog Lower threshold register,                      Address offset: 0x28 */

    union {
        struct {
            __IO uint32_t SQR1;             /* ADC regular sequence register 1,                    Address offset: 0x2C */
            __IO uint32_t SQR2;             /* ADC regular sequence register 2,                    Address offset: 0x30 */
            __IO uint32_t SQR3;             /* ADC regular sequence register 3,                    Address offset: 0x34 */
        };
        __IO uint32_t SQR[3];
    };
#if CHIP_SUPPORT_ADC_INJECTED
    __IO uint32_t JSQR;             /* ADC injected sequence register,                             Address offset: 0x38 */
    union {
        struct {
            __IO uint32_t JDR1;             /* ADC injected data register 1,                       Address offset: 0x3C */
            __IO uint32_t JDR2;             /* ADC injected data register 2,                       Address offset: 0x40 */
            __IO uint32_t JDR3;             /* ADC injected data register 3,                       Address offset: 0x44 */
            __IO uint32_t JDR4;             /* ADC injected data register 4,                       Address offset: 0x48 */
        };
        __IO uint32_t JDR[4];
    };
#else
    uint32_t RESERVED2[5];
#endif
    __IO uint32_t DR;               /* ADC regular data register,                                  Address offset: 0x4C */
} CHIP_REGS_ADC_T;


#define CHIP_REGS_ADC1                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC1)
#if CHIP_DEV_ADC_CNT >= 2
#define CHIP_REGS_ADC2                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC2)
#endif
#if CHIP_DEV_ADC_CNT >= 3
#define CHIP_REGS_ADC3                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC3)
#endif


#define CHIP_ADC_POWERUP_TIME_US            1

/*********** ADC status register (ADC_SR) *************************************/
#define CHIP_REGFLD_ADC_SR_AWD                  (0x00000001UL <<  0U) /* (RCW0) Analog watchdog flag  */
#define CHIP_REGFLD_ADC_SR_EOC                  (0x00000001UL <<  1U) /* (RCW0) End of conversion  */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_SR_JEOC                 (0x00000001UL <<  2U) /* (RCW0) Injected channel end of conversion  */
#define CHIP_REGFLD_ADC_SR_JSTRT                (0x00000001UL <<  3U) /* (RCW0) Injected channel start flag  */
#endif
#define CHIP_REGFLD_ADC_SR_STRT                 (0x00000001UL <<  4U) /* (RCW0) Regular channel start flag  */

/*********** ADC control register 1 (ADC_CR1)**********************************/
#define CHIP_REGFLD_ADC_CR1_AWD_CH              (0x0000001FUL <<  0U) /* (RW) Analog watchdog channel select bits */
#define CHIP_REGFLD_ADC_CR1_EOC_IE              (0x00000001UL <<  5U) /* (RW) Interrupt enable for EOC */
#define CHIP_REGFLD_ADC_CR1_AWD_IE              (0x00000001UL <<  6U) /* (RW) Analog watchdog interrupt enable */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JEOC_IE             (0x00000001UL <<  7U) /* (RW) Interrupt enable for injected channels */
#endif
#define CHIP_REGFLD_ADC_CR1_SCAN                (0x00000001UL <<  8U) /* (RW) Scan mode */
#define CHIP_REGFLD_ADC_CR1_AWD_SGL             (0x00000001UL <<  9U) /* (RW) Enable the watchdog on a single channel in scan mode */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JAUTO               (0x00000001UL << 10U) /* (RW) Automatic Injected Group conversion */
#endif
#define CHIP_REGFLD_ADC_CR1_DISC_EN             (0x00000001UL << 11U) /* (RW) Discontinuous mode on regular channels */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JDISC_EN            (0x00000001UL << 12U) /* (RW) Discontinuous mode on injected channels */
#endif
#define CHIP_REGFLD_ADC_CR1_DISC_NUM            (0x00000007UL << 13U) /* (RW) Discontinuous mode channel count */
#define CHIP_REGFLD_ADC_CR1_DUAL_MOD            (0x0000000FUL << 16U) /* (RW) Dual mode selection */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JAWD_EN             (0x00000001UL << 22U) /* (RW) Analog watchdog enable on injected channels */
#endif
#define CHIP_REGFLD_ADC_CR1_AWD_EN              (0x00000001UL << 23U) /* (RW) Analog watchdog enable on regular channels */


#define CHIP_REGFLDVAL_ADC_CR1_DUAL_MOD_INDEP    0
#define CHIP_REGFLDVAL_ADC_CR1_DUAL_MOD_REG_INJ  1
#define CHIP_REGFLDVAL_ADC_CR1_DUAL_MOD_REG_ALTT 1


/*********** ADC control register 2 (ADC_CR2)**********************************/
#define CHIP_REGFLD_ADC_CR2_AD_ON               (0x00000001UL <<  0U) /* (RW) A/D converter ON / OFF */
#define CHIP_REGFLD_ADC_CR2_CONT                (0x00000001UL <<  1U) /* (RW) Continuous conversion */
#define CHIP_REGFLD_ADC_CR2_CAL                 (0x00000001UL <<  2U) /* (RWSC) A/D Calibration */
#define CHIP_REGFLD_ADC_CR2_RST_CAL             (0x00000001UL <<  3U) /* (RW) Reset calibration */
#define CHIP_REGFLD_ADC_CR2_DMA                 (0x00000001UL <<  8U) /* (RW) Direct memory access mode enable */
#define CHIP_REGFLD_ADC_CR2_ALIGN               (0x00000001UL << 11U) /* (RW) Data alignment  */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR2_JEXT_SEL            (0x00000007UL << 12U) /* (RW) External event select for injected group */
#define CHIP_REGFLD_ADC_CR2_JEXT_TRIG           (0x00000001UL << 15U) /* (RW) Injected conversion on external event enable */
#endif
#define CHIP_REGFLD_ADC_CR2_EXT_SEL             (0x00000007UL << 17U) /* (RW) External event select for regular group */
#define CHIP_REGFLD_ADC_CR2_EXT_TRIG            (0x00000001UL << 20U) /* (RW) Regular conversion on external event enable */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR2_JSW_START           (0x00000001UL << 21U) /* (RW) Start conversion of injected channels */
#endif
#define CHIP_REGFLD_ADC_CR2_SW_START            (0x00000001UL << 22U) /* (RW) Start conversion of regular channels */
#define CHIP_REGFLD_ADC_CR2_TS_VREF_E           (0x00000001UL << 23U) /* (RW) Temperature sensor and VREFINT enable */



#define CHIP_REGFLDVAL_ADC_CR2_ALIGN_RIGHT              0
#define CHIP_REGFLDVAL_ADC_CR2_ALIGN_LEFT               1

#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM1_CC1         0
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM1_CC2         1
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM1_CC3         2
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM2_CC2         3
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM3_TRGO        4
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM4_CC4         5
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_EXTI11_TIM8_TRGO 6
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_SW_START         7

#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_TIM3_CC1        0
#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_TIM2_CC3        1
#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_TIM1_CC3        2
#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_TIM8_CC1        3
#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_TIM8_TRGO       4
#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_TIM5_CC1        5
#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_TIM5_CC3        6
#define CHIP_REGFLDVAL_ADC3_CR2_EXT_SEL_SW_START        7

#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM1_TRGO       0
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM1_CC4        1
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM2_TRGO       2
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM2_CC1        3
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM3_CC4        4
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM4_TRGO       5
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_EXTI15_TIM8_CC4 6
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_JSW_START       7

#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_TIM1_TRGO      0
#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_TIM1_CC4       1
#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_TIM4_CC3       2
#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_TIM8_CC2       3
#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_TIM8_CC4       4
#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_TIM5_TRGO      5
#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_TIM5_CC4       6
#define CHIP_REGFLDVAL_ADC3_CR2_JEXT_SEL_JSW_START      7




/*********** ADC sample time register 1/2 (ADC_SMPR1/2) ***********************/
#define CHIP_REGFLD_ADC_SMPR_SMP(x)             (0x00000007UL << (3*((x) % 10)))  /* (RW) ADC Channel x Sampling time selection  */
#define CHIP_REGNUM_ADC_SMPR_SMP(x)             (1 - (x) / 10)
#define CHIP_REGMSK_ADC_SMPR_RESERVED           (0xC0000000UL)

#define CHIP_REGFLDVAL_ADC_SMPR_SMP_1_5         0 /* 1.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_7_5         1 /* 7.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_13_5        2 /* 13.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_28_5        3 /* 28.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_41_5        4 /* 41.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_55_5        5 /* 55.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_71_5        6 /* 71.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_239_5       7 /* 239.5 ADC clock cycles */


/*********** ADC injected channel data offset register x (ADC_JOFRx) (x=1..4) */
#define CHIP_REGFLD_ADC_JOFR_JOFFSET            (0x00000FFFUL <<  0U) /* (RW) Data offset for injected channel x */

/*********** ADC watchdog threshold register (ADC_LTR) ************************/
#define CHIP_REGFLD_ADC_LTR_LT                  (0x00000FFFUL <<  0U) /* (RW) Analog watchdog low threshold */

/*********** ADC watchdog threshold register (ADC_HTR) ************************/
#define CHIP_REGFLD_ADC_HTR_HT                  (0x00000FFFUL <<  0U) /* (RW) ADC Analog watchdog 1,2 and 3 higher threshold */

/*********** ADC regular sequence register n (ADC_SQRn)************************/
#define CHIP_REGFLD_ADC_SQR1_L                  (0x0000000FUL << 20U) /* (RW) ADC regular channel sequence length */
#define CHIP_REGFLD_ADC_SQR_SQ(x)               (0x0000001FUL <<  5U * ((x) % 6)) /* (RW) ADC x-st conversion in regular sequence */
#define CHIP_REGNUM_ADC_SQR_SQ(x)               (2 - ((x) / 6))


/*********** ADC injected sequence register (ADC_JSQR) ************************/
#define CHIP_REGFLD_ADC_JSQR_JL                 (0x00000003UL << 20U) /* (RW) ADC injected channel sequence length */
#define CHIP_REGFLD_ADC_JSQR_JSQ(x)             (0x0000001FUL <<  (5 * (x)))  /* (RW) ADC x-st conversion in injected sequence */

/*********** ADC injected channel y data register (ADC_JDRy) ******************/
#define CHIP_REGFLD_ADC_JDR_JDATA               (0x0000FFFFUL <<  0U) /* (RW) ADC Injected DATA */

/*********** ADC regular data register (ADC_DR) *******************************/
#define CHIP_REGFLD_ADC_DR_DATA                 (0x0000FFFFUL <<  0U) /* (RW) ADC regular DATA */
#define CHIP_REGFLD_ADC_DR_ADC2_DATA            (0x0000FFFFUL << 16U) /* (RW) ADC1: In dual mode, these bits contain the regular data of ADC2. */


#endif // REGS_ADC_H

