/*
 * @brief LPC11u6x USART0 chip driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef CHIP_REGS_LPC11U6X_UART0_H
#define CHIP_REGS_LPC11U6X_UART0_H

#include <stdint.h>
/**
 * @brief USART 0 register block structure
 */
typedef struct {                    /*!< USARTn Structure       */

    union {
        __IO uint32_t  DLL;            /*!< Divisor Latch LSB. Least significant byte of the baud rate divisor value. The full divisor is used to generate a baud rate from the fractional rate divider (DLAB = 1). */
        __O  uint32_t  THR;            /*!< Transmit Holding Register. The next character to be transmitted is written here (DLAB = 0). */
        __I  uint32_t  RBR;            /*!< Receiver Buffer Register. Contains the next received character to be read (DLAB = 0). */
    };

    union {
        __IO uint32_t IER;            /*!< Interrupt Enable Register. Contains individual interrupt enable bits for the 7 potential UART interrupts (DLAB = 0). */
        __IO uint32_t DLM;            /*!< Divisor Latch MSB. Most significant byte of the baud rate divisor value. The full divisor is used to generate a baud rate from the fractional rate divider (DLAB = 1). */
    };

    union {
        __O  uint32_t FCR;            /*!< FIFO Control Register. Controls UART FIFO usage and modes. */
        __I  uint32_t IIR;            /*!< Interrupt ID Register. Identifies which interrupt(s) are pending. */
    };

    __IO uint32_t LCR;                /*!< Line Control Register. Contains controls for frame formatting and break generation. */
    __IO uint32_t MCR;                /*!< Modem Control Register. Only present on USART ports with full modem support. */
    __I  uint32_t LSR;                /*!< Line Status Register. Contains flags for transmit and receive status, including line errors. */
    __I  uint32_t MSR;                /*!< Modem Status Register. Only present on USART ports with full modem support. */
    __IO uint32_t SCR;                /*!< Scratch Pad Register. Eight-bit temporary storage for software. */
    __IO uint32_t ACR;                /*!< Auto-baud Control Register. Contains controls for the auto-baud feature. */
    __IO uint32_t ICR;                /*!< IrDA control register (not all UARTS) */
    __IO uint32_t FDR;                /*!< Fractional Divider Register. Generates a clock input for the baud rate divider. */
    __IO uint32_t OSR;                /*!< Oversampling Register. Controls the degree of oversampling during each bit time. Only on some UARTS. */
    __IO uint32_t TER;                /*!< Transmit Enable Register. Turns off USART transmitter for use with software flow control. */
    __I  uint32_t RESERVED0[3];
    __IO uint32_t HDEN;                /*!< Half-duplex enable Register- only on some UARTs */
    __I  uint32_t RESERVED1[1];
    __IO uint32_t SCICTRL;            /*!< Smart card interface control register- only on some UARTs */
    __IO uint32_t RS485CTRL;        /*!< RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes. */
    __IO uint32_t RS485ADRMATCH;    /*!< RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode. */
    __IO uint32_t RS485DLY;            /*!< RS-485/EIA-485 direction control delay. */
    __IO uint32_t SYNCCTRL;            /*!< Synchronous mode control register. Only on USARTs. */
} CHIP_REGS_USART0_T;

#define CHIP_REGS_USART0            ((CHIP_REGS_USART0_T *) CHIP_MEMRGN_ADDR_PERIPH_USART0)

#define CHIP_UART0_TX_FIFO_SIZE     (16)

/*********** USART0 Receiver Buffer Register (USART_RBR, DLAB = 0, Read) ******/
#define CHIP_REGFLD_USART0_RBR_VAL                  (0x000000FFUL <<  0U)   /* (RO) Oldest received byte  */

/*********** USART0 Transmitter Holding Register (USART_THR, DLAB = 0, Write) */
#define CHIP_REGFLD_USART0_THR_VAL                  (0x000000FFUL <<  0U)   /* (WO) Sent byte  */

/*********** USART0 Divisor Latch LSB Register (USART_DLL, DLAB = 1) **********/
#define CHIP_REGFLD_USART0_DLL_SB                   (0x000000FFUL <<  0U)   /* (RW)  */

/*********** USART0 Divisor Latch MSB Register (USART_DLM, DLAB = 1) **********/
#define CHIP_REGFLD_USART0_DLM_SB                   (0x000000FFUL <<  0U)   /* (RW)  */

/*********** USART0 Interrupt Enable Register (USART_IER, DLAB = 0) ***********/
#define CHIP_REGFLD_USART0_IER_RBR                  (0x00000001UL <<  0U)   /* (RW) RBR Interrupt Enable */
#define CHIP_REGFLD_USART0_IER_THRE                 (0x00000001UL <<  1U)   /* (RW) THRE Interrupt Enable */
#define CHIP_REGFLD_USART0_IER_RLS                  (0x00000001UL <<  2U)   /* (RW) Receive Line Status Interrupt Enable */
#define CHIP_REGFLD_USART0_IER_MSI                  (0x00000001UL <<  3U)   /* (RW) Modem Status Interrupt Enable */
#define CHIP_REGFLD_USART0_IER_ABEO                 (0x00000001UL <<  8U)   /* (RW) End of auto-baud Interrupt Enable */
#define CHIP_REGFLD_USART0_IER_ABTO                 (0x00000001UL <<  9U)   /* (RW) Auto-baud time-out Interrupt Enable */
#define CHIP_REGMSK_USART0_IER_RESERVED             (0xFFFFFCF0UL)

/*********** USART0 Interrupt Identification Register (USART_IIR, Read) *******/
#define CHIP_REGFLD_USART0_IIR_INT_STATUS           (0x00000001UL <<  0U)   /* (RO) Interrupt status (active low) */
#define CHIP_REGFLD_USART0_IIR_INT_ID               (0x00000007UL <<  1U)   /* (RO) Interrupt identification */
#define CHIP_REGFLD_USART0_IIR_FIFO_EN              (0x00000003UL <<  6U)   /* (RO) FIFO enble status */
#define CHIP_REGFLD_USART0_IIR_ABEO_INT             (0x00000001UL <<  8U)   /* (RO) End of auto-baud interrupt */
#define CHIP_REGFLD_USART0_IIR_ABTO_INT             (0x00000001UL <<  9U)   /* (RO) End of time-out interrupt */

/***********  USART0 FIFO Control Register (USART_FCR, Write) *****************/
#define CHIP_REGFLD_USART0_FCR_FIFO_EN              (0x00000001UL <<  0U)   /* (WO) FIFO enable */
#define CHIP_REGFLD_USART0_FCR_RX_FIFO_RES          (0x00000001UL <<  1U)   /* (W1SC) Rx FIFO reset */
#define CHIP_REGFLD_USART0_FCR_TX_FIFO_RES          (0x00000001UL <<  2U)   /* (W1SC) Tx FIFO reset */
#define CHIP_REGFLD_USART0_FCR_DMA_MODE             (0x00000001UL <<  3U)   /* (WO) DMA Mode Select */
#define CHIP_REGFLD_USART0_FCR_RX_TL                (0x00000003UL <<  6U)   /* (WO) RX Trigger Level */
#define CHIP_REGMSK_USART0_FCR_RESERVED             (0xFFFFFF30UL)


#define CHIP_REGFLDVAL_USART0_FCR_RX_TL_1           0x0 /* 1 character */
#define CHIP_REGFLDVAL_USART0_FCR_RX_TL_4           0x1 /* 4 characters */
#define CHIP_REGFLDVAL_USART0_FCR_RX_TL_8           0x2 /* 8 characters */
#define CHIP_REGFLDVAL_USART0_FCR_RX_TL_14          0x3 /* 14 characters */


/***********  USART0 Line Control Register (USART_LCR) ************************/
#define CHIP_REGFLD_USART0_LCR_WLS                  (0x00000003UL <<  0U)   /* (RW) Word Length Select */
#define CHIP_REGFLD_USART0_LCR_SBS                  (0x00000001UL <<  2U)   /* (RW) Stop bit Select */
#define CHIP_REGFLD_USART0_LCR_PE                   (0x00000001UL <<  3U)   /* (RW) Parity Enable */
#define CHIP_REGFLD_USART0_LCR_PS                   (0x00000003UL <<  4U)   /* (RW) Parity Select */
#define CHIP_REGFLD_USART0_LCR_BC                   (0x00000001UL <<  6U)   /* (RW) Break Control */
#define CHIP_REGFLD_USART0_LCR_DLAB                 (0x00000001UL <<  7U)   /* (RW) Divisor Latch Access Bit */
#define CHIP_REGMSK_USART0_LCR_ReSERVED             (0xFFFFFF00UL)

#define CHIP_REGFLDVAL_USART0_LCR_WLS_5             0 /*  5-bit character length */
#define CHIP_REGFLDVAL_USART0_LCR_WLS_6             1 /*  6-bit character length */
#define CHIP_REGFLDVAL_USART0_LCR_WLS_7             2 /*  7-bit character length */
#define CHIP_REGFLDVAL_USART0_LCR_WLS_8             3 /*  8-bit character length */

#define CHIP_REGFLDVAL_USART0_LCR_SBS_1             0 /* 1 stop bit */
#define CHIP_REGFLDVAL_USART0_LCR_SBS_2             1 /* 2 stop bit (1.5 if LCR[1:0]=00) */

#define CHIP_REGFLDVAL_USART0_LCR_PS_ODD            0 /* Odd Parity */
#define CHIP_REGFLDVAL_USART0_LCR_PS_EVEN           1 /* Even Parity */
#define CHIP_REGFLDVAL_USART0_LCR_PS_FORCED_1       2 /* Forced 1 */
#define CHIP_REGFLDVAL_USART0_LCR_PS_FORCED_0       3 /* Forced 0 */


/*********** USART0 Modem Control Register (USART_MCR) ************************/
#define CHIP_REGFLD_USART0_MCR_DTR_CTRL             (0x00000001UL <<  0U)   /* (RW) Source for modem output pin DTR */
#define CHIP_REGFLD_USART0_MCR_RTS_CTRL             (0x00000001UL <<  1U)   /* (RW) Source for modem output pin RTS */
#define CHIP_REGFLD_USART0_MCR_LMS                  (0x00000001UL <<  4U)   /* (RW) Loopback mode select */
#define CHIP_REGFLD_USART0_MCR_RTS_EN               (0x00000001UL <<  6U)   /* (RW) RTS enable */
#define CHIP_REGFLD_USART0_MCR_CTS_EN               (0x00000001UL <<  7U)   /* (RW) CTS enable */
#define CHIP_REGMSK_USART0_MCR_RESERVED             (0xFFFFFF2CUL)


/*********** USART0 Line Status Register (USART_LSR) **************************/
#define CHIP_REGFLD_USART0_LSR_RDR                  (0x00000001UL <<  0U)   /* (RO) Receive Data Ready */
#define CHIP_REGFLD_USART0_LSR_OE                   (0x00000001UL <<  1U)   /* (RO) Overrun Error */
#define CHIP_REGFLD_USART0_LSR_PE                   (0x00000001UL <<  2U)   /* (RO) Parity Error */
#define CHIP_REGFLD_USART0_LSR_FE                   (0x00000001UL <<  3U)   /* (RO) Frame Error */
#define CHIP_REGFLD_USART0_LSR_BI                   (0x00000001UL <<  4U)   /* (RO) Break Interrupt */
#define CHIP_REGFLD_USART0_LSR_THRE                 (0x00000001UL <<  5U)   /* (RO) Transmitter Holding Register Empty */
#define CHIP_REGFLD_USART0_LSR_TEMT                 (0x00000001UL <<  6U)   /* (RO) Transmitter Empty */
#define CHIP_REGFLD_USART0_LSR_RXFE                 (0x00000001UL <<  7U)   /* (RO) Error in RX FIFO */
#define CHIP_REGFLD_USART0_LSR_TXERR                (0x00000001UL <<  8U)   /* (RO) Tx Error */
#define CHIP_REGMSK_USART0_LSR_RESERVED             (0xFFFFFE00UL)


/*********** USART0 Modem Status Register (USART_MSR) *************************/
#define CHIP_REGFLD_USART0_MSR_DCTS                 (0x00000001UL <<  0U)   /* (RC) Delta CTS */
#define CHIP_REGFLD_USART0_MSR_DDSR                 (0x00000001UL <<  1U)   /* (RC) Delta DSR */
#define CHIP_REGFLD_USART0_MSR_TERI                 (0x00000001UL <<  2U)   /* (RC) Trailing Edge RI */
#define CHIP_REGFLD_USART0_MSR_DDCD                 (0x00000001UL <<  3U)   /* (RC) Delta DCD */
#define CHIP_REGFLD_USART0_MSR_CTS                  (0x00000001UL <<  4U)   /* (RO) Clear To Send State */
#define CHIP_REGFLD_USART0_MSR_DSR                  (0x00000001UL <<  5U)   /* (RO) Data Set Ready State */
#define CHIP_REGFLD_USART0_MSR_RI                   (0x00000001UL <<  6U)   /* (RO) Ring Indicator State */
#define CHIP_REGFLD_USART0_MSR_DCD                  (0x00000001UL <<  7U)   /* (RO) Data Carrier Detect State */
#define CHIP_REGMSK_USART0_MSR_RESERVED             (0xFFFFFF00UL)


/*********** USART0 Auto-baud Control Register (USART_ACR) ********************/
#define CHIP_REGFLD_USART0_ACR_START                (0x00000001UL <<  0U)   /* (RWSC) Start auto-baud */
#define CHIP_REGFLD_USART0_ACR_MODE                 (0x00000001UL <<  1U)   /* (RW)   Auto-baud mode select */
#define CHIP_REGFLD_USART0_ACR_AUTO_RESTART         (0x00000001UL <<  2U)   /* (RW)   Time-out restart */
#define CHIP_REGFLD_USART0_ACR_AUTO_RESTART         (0x00000001UL <<  3U)   /* (RW)   Time-out restart */
#define CHIP_REGFLD_USART0_ACR_ABEO_INT_CLR         (0x00000001UL <<  8U)   /* (W1)   End of auto-baud interrupt clear bit */
#define CHIP_REGFLD_USART0_ACR_ABTO_INT_CLR         (0x00000001UL <<  9U)   /* (W1)   Auto-baud time-out interrupt clear bit */
#define CHIP_REGMSK_USART0_ACR_RESERVED             (0xFFFFFCF0UL)


/*********** USART0 IrDA Control Register (USART_ICR) *************************/
#define CHIP_REGFLD_USART0_ICR_IRDA_EN              (0x00000001UL <<  0U)   /* (RW) IrDA mode enable */
#define CHIP_REGFLD_USART0_ICR_IRDA_INV             (0x00000001UL <<  1U)   /* (RW) Serial input inverter */
#define CHIP_REGFLD_USART0_ICR_FIX_PULSE_EN         (0x00000001UL <<  2U)   /* (RW) IrDA fixed pulse width mode */
#define CHIP_REGFLD_USART0_ICR_PULSE_DIV            (0x00000007UL <<  3U)   /* (RW) Configures the pulse width when FixPulseEn = 1 */
#define CHIP_REGMSK_USART0_ICR_RESERVED             (0xFFFFFFC0UL)


/*********** USART0 Fractional Divider Register (USART_FDR) *******************/
#define CHIP_REGFLD_USART0_FDR_DIVADD_VAL           (0x0000000FUL <<  0U)   /* (RW) Baud rate pre-scaler divisor value */
#define CHIP_REGFLD_USART0_FDR_MUL_VAL              (0x0000000FUL <<  4U)   /* (RW) Baud rate pre-scaler multiplier value. */
#define CHIP_REGMSK_USART0_FDR_RESERVED             (0xFFFFFF00UL)

/*********** USART0 Oversampling Register (USART_OSR) *************************/
#define CHIP_REGFLD_USART0_OSR_OSFRAC               (0x00000007UL <<  1U)   /* (RW) Fraction part of the oversampling ratio */
#define CHIP_REGFLD_USART0_OSR_OSINT                (0x0000000FUL <<  4U)   /* (RW) Integer part of the oversampling ratio, minus 1 */
#define CHIP_REGFLD_USART0_OSR_FDINT                (0x0000007FUL <<  8U)   /* (RW) OSint extension in Smart Card mode */
#define CHIP_REGMSK_USART0_OSR_RESERVED             (0xFFFF8001UL)

/*********** USART0 Transmit Enable Register (USART_TER) **********************/
#define CHIP_REGFLD_USART0_TER_TXEN                 (0x00000001UL <<  7U)   /* (RW) Transmit enable */
#define CHIP_REGMSK_USART0_TER_RESERVED             (0xFFFFFF7FUL)

/*********** UART0 Half-duplex enable register (USART_HDEN) *******************/
#define CHIP_REGFLD_USART0_HDEN_HDEN                (0x00000001UL <<  0U)   /* (RW) Half-duplex enable */
#define CHIP_REGMSK_USART0_HDEN_RESERVED            (0xFFFFFFFEUL)

/*********** UART0 Smart Card Interface Control register (USART_SCICTRL) ******/
#define CHIP_REGFLD_USART0_SCICTRL_SCI_EN           (0x00000001UL <<  0U)   /* (RW) Smart Card Interface Enable. */
#define CHIP_REGFLD_USART0_SCICTRL_NACK_DIS         (0x00000001UL <<  1U)   /* (RW) NACK response disable. Only applicable in T=0. */
#define CHIP_REGFLD_USART0_SCICTRL_PROT_SEL         (0x00000001UL <<  2U)   /* (RW) Protocol selection as defined in the ISO7816-3 standard (T=0 or 1) */
#define CHIP_REGFLD_USART0_SCICTRL_TX_RETRY         (0x00000007UL <<  5U)   /* (RW) Maximum number of retransmissions on NACK (for T=0) */
#define CHIP_REGFLD_USART0_SCICTRL_XTRA_GUARD       (0x000000FFUL <<  8U)   /* (RW) Guard time after a character transmitted (for T=0) */
#define CHIP_REGMSK_USART0_SCICTRL_RESERVED         (0xFFFF0018UL)

/*********** UART0 RS485 Control register (USART_RS485CTRL) *******************/
#define CHIP_REGFLD_USART0_RS485CTRL_NMM_EN         (0x00000001UL <<  0U)   /* (RW) Normal Multidrop Mode Enable */
#define CHIP_REGFLD_USART0_RS485CTRL_RXDIS          (0x00000001UL <<  1U)   /* (RW) Receiver disable */
#define CHIP_REGFLD_USART0_RS485CTRL_AAD_EN         (0x00000001UL <<  2U)   /* (RW) Auto Address Detect Enable */
#define CHIP_REGFLD_USART0_RS485CTRL_SEL            (0x00000001UL <<  3U)   /* (RW) Select direction control pin */
#define CHIP_REGFLD_USART0_RS485CTRL_DCTRL          (0x00000001UL <<  4U)   /* (RW) Auto direction control enable */
#define CHIP_REGFLD_USART0_RS485CTRL_OINV           (0x00000001UL <<  5U)   /* (RW) Polarity control */
#define CHIP_REGMSK_USART0_RS485CTRL_RESERVED       (0xFFFFFFC0UL)

#define CHIP_REGFLDVAL_USART0_RS485CTRL_SEL_RTS     0
#define CHIP_REGFLDVAL_USART0_RS485CTRL_SEL_DTR     1

/*********** UART0 RS-485 Address Match register (USART_RS485ADRMATCH) ********/
#define CHIP_REGFLD_USART0_RS485ADRMATCH_VAL        (0x000000FFUL <<  0U)   /* (RW) Address match value */

/*********** USART0 RS-485 Delay value register (USART_RS485DLY) **************/
#define CHIP_REGFLD_USART0_RS485DLY_VAL             (0x000000FFUL <<  0U)   /* (RW) Direction control (RTS or DTR) delay value */

/*********** USART0 Synchronous mode control register (USART_SYNCCTRL) ********/
#define CHIP_REGFLD_USART0_SYNCCTRL_SYNC            (0x00000001UL <<  0U)   /* (RW) Enables synchronous mode */
#define CHIP_REGFLD_USART0_SYNCCTRL_CSRC            (0x00000001UL <<  1U)   /* (RW) Clock source select (master/slave) */
#define CHIP_REGFLD_USART0_SYNCCTRL_FES             (0x00000001UL <<  2U)   /* (RW) Falling edge sampling */
#define CHIP_REGFLD_USART0_SYNCCTRL_TSBYPASS        (0x00000001UL <<  3U)   /* (RW) Transmit synchronization bypass in synchronous slave mode */
#define CHIP_REGFLD_USART0_SYNCCTRL_CSC_EN          (0x00000001UL <<  4U)   /* (RW) Continuous master clock enable (used only when CSRC is 1) */
#define CHIP_REGFLD_USART0_SYNCCTRL_SS_DIS          (0x00000001UL <<  5U)   /* (RW) Start/stop bits send disable */
#define CHIP_REGFLD_USART0_SYNCCTRL_CC_CLR          (0x00000001UL <<  6U)   /* (RW) Continuous clock clear */
#define CHIP_REGMSK_USART0_SYNCCTRL_RESERVED        (0xFFFFFF80UL)

#define CHIP_REGFLDVAL_USART0_SYNCCTRL_CSRC_SLAVE   0
#define CHIP_REGFLDVAL_USART0_SYNCCTRL_CSRC_MASTER  1


#endif /* CHIP_REGS_LPC11U6X_UART0_H */
