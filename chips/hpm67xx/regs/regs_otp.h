#ifndef REGS_OTP_H
#define REGS_OTP_H

#include "chip_ioreg_defs.h"

#define CHIP_OTP_WORD_CNT                           128
#define CHIP_OTP_REGION_CNT                         4


typedef struct {
    __IO uint32_t SHADOW[CHIP_OTP_WORD_CNT];           /* Fuse load register,                  Address offset: 0x0000..0x01FC */
    __IO uint32_t SHADOW_LOCK[CHIP_OTP_WORD_CNT/16];   /* Load register lock,                  Address offset: 0x0200..0x021C */
    uint32_t RESERVED1[CHIP_OTP_WORD_CNT - CHIP_OTP_WORD_CNT/16];
    __IO uint32_t FUSE[CHIP_OTP_WORD_CNT];             /* Fuse register,                       Address offset: 0x0400..0x05FC */
    __IO uint32_t FUSE_LOCK[CHIP_OTP_WORD_CNT/16];     /* Fuse register lock,                  Address offset: 0x0600..0x061C */
    uint32_t RESERVED2[CHIP_OTP_WORD_CNT - CHIP_OTP_WORD_CNT/16];
    __IO uint32_t UNLOCK;                              /* Unlock fuse write,                   Address offset: 0x0800 */
    __IO uint32_t DATA;                                /* Word data,                           Address offset: 0x0804 */
    __IO uint32_t ADDR;                                /* Word address,                        Address offset: 0x0808 */
    __IO uint32_t CMD;                                 /* Fuse command,                        Address offset: 0x080C */
    uint32_t RESERVED3[124];
    __IO uint32_t LOAD_REQ;                            /* LOAD request,                        Address offset: 0x0A00 */
    __IO uint32_t LOAD_COMP;                           /* LOAD complete,                       Address offset: 0x0A04 */
    uint32_t RESERVED4[6];
    __IO uint32_t REGION[CHIP_OTP_REGION_CNT];         /* LOAD region,                         Address offset: 0x0A20 - 0xA2C */
    uint32_t RESERVED5[116];
    __IO uint32_t INT_FLAG;                            /* interrupt flag,                      Address offset: 0x0C00 */
    __IO uint32_t INT_EN;                              /* interrupt enable,                    Address offset: 0x0C04 */
} CHIP_REGS_OTP_T;

#define CHIP_REGS_OTP                ((CHIP_REGS_OTP_T *)CHIP_MEMRGN_ADDR_OTP)
#define CHIP_REGS_OTPSHW             ((CHIP_REGS_OTP_T *)CHIP_MEMRGN_ADDR_OTPSHW)



#define CHIP_REGFLDVAL_OTP_UNLOCK_OPEN                  0x4F50454E
#define CHIP_REGFLDVAL_OTP_CMD_BLOW                     0x424C4F57
#define CHIP_REGFLDVAL_OTP_CMD_READ                     0x52454144

/*********** LOAD region - OTP_REGION  ****************************************/
#define CHIP_REGFLD_OTP_REGION_START                    (0x0000003FUL <<  0U) /* (RW) Start address of the loading area */
#define CHIP_REGFLD_OTP_REGION_END                      (0x0000003FUL <<  8U) /* (RW) End address of the loading area (not included) */

/*********** interrupt flag/enable - OTP_INT_FLAG/EN  *************************/
#define CHIP_REGFLD_OTP_INT_LOAD                        (0x00000001UL <<  0U) /* (RW) Fuse load completion */
#define CHIP_REGFLD_OTP_INT_READ                        (0x00000001UL <<  1U) /* (RW) Fuse read completion */
#define CHIP_REGFLD_OTP_INT_WRITE                       (0x00000001UL <<  2U) /* (RW) Fuse write completion */
#endif // REGS_OTP_H
