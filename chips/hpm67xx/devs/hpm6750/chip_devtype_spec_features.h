#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_DEV_UART_CNT           16
#define CHIP_DEV_UART_FIFO_SIZE     16

#define CHIP_DEV_SUPPORT_SYNT_TSTMP 0  /* HPM53xx only */
#define CHIP_DEV_SUPPORT_PWM_HR     0  /* HPM62xx pwm high resolution */

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
