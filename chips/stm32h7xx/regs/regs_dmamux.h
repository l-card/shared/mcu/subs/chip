#ifndef CHIP_STM32H7XX_REGS_DMAMUX_H_
#define CHIP_STM32H7XX_REGS_DMAMUX_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"
#include "regs_dma.h"

#define CHIP_DMAMUX_MAX_CHANNEL_CNT             16
#define CHIP_DMAMUX_MAX_REQGEN_CNT              8

/* определение номера DMAMUX по номеру DMA (для BDMA используется CHIP_DMA_BDMA_NUM) */
#define CHIP_DMAMUX_NUM(dma)                    (((dma) == CHIP_DMA_BDMA_NUM) ? 2 : 1)
#define CHIP_DMAMUX_CH_NUM(dma, ch)             (((dma) == CHIP_DMA_BDMA_NUM) ? (ch) : (((dma) - 1) * CHIP_DMA_STREAM_CNT + (ch)))


typedef struct {
    __IO uint32_t  CCR[CHIP_DMAMUX_MAX_CHANNEL_CNT];  /* DMA Multiplexer Channel x Control Register   */
    uint32_t RESERVED0[16];
    __IO uint32_t  CSR;      /* DMA Channel Status Register     */
    __IO uint32_t  CFR;      /* DMA Channel Clear Flag Register */
    uint32_t RESERVED1[30];
    __IO uint32_t  RGCR[CHIP_DMAMUX_MAX_REQGEN_CNT];  /* DMA Request Generator x Control Register   */
    uint32_t RESERVED2[16 - CHIP_DMAMUX_MAX_REQGEN_CNT];
    __IO uint32_t  RGSR;        /* DMA Request Generator Status Register       */
    __IO uint32_t  RGCFR;       /* DMA Request Generator Clear Flag Register   */
} CHIP_REGS_DMAMUX_T;

/* Number of DMAMUX output request channels */
#define CHIP_DMAMUX1_CHANNEL_CNT                16
#define CHIP_DMAMUX2_CHANNEL_CNT                8
/* Number of DMAMUX request generator channels */
#define CHIP_DMAMUX1_REQGEN_CNT                 8
#define CHIP_DMAMUX2_REQGEN_CNT                 8
/* Number of DMAMUX request trigger inputs */
#define CHIP_DMAMUX1_REQTRIG_CNT                8
#define CHIP_DMAMUX2_REQTRIG_CNT                32
/* Number of DMAMUX synchronization inputs */
#define CHIP_DMAMUX1_SYNCIN_CNT                 8
#define CHIP_DMAMUX2_SYNCIN_CNT                 16
/* Number of DMAMUX peripheral request inputs */
#ifdef CHIP_DEV_STM32H723
#define CHIP_DMAMUX1_REQ_CNT                   129
#else
#define CHIP_DMAMUX1_REQ_CNT                   107
#endif
#define CHIP_DMAMUX2_PREQ_CNT                   12


#define CHIP_REGS_DMAMUX1                       ((CHIP_REGS_DMAMUX_T *) CHIP_MEMRGN_ADDR_PERIPH_DMAMUX1)
#define CHIP_REGS_DMAMUX2                       ((CHIP_REGS_DMAMUX_T *) CHIP_MEMRGN_ADDR_PERIPH_DMAMUX2)

/* макросы для определения регистров по заданному номеру контроллера (от 1) и канала (от 0) */
#define CHIP_REGS_DMAMUX(num)                   CHIP_REGS_DMAMUX_(num)
#define CHIP_REGS_DMAMUX_(num)                  CHIP_REGS_DMAMUX ## num

#define CHIP_DMAMUX_CH_REG_CCR(num, ch)         CHIP_DMAMUX_CH_REG_CCR_(num, ch)
#define CHIP_DMAMUX_CH_REG_CCR_(num, ch)        (CHIP_REGS_DMAMUX(num)->CCR[ch])

#define CHIP_DMAMUX_REQGEN_REG_RGCR(num, ch)    CHIP_DMAMUX_REQGEN_REG_RGCR_(num, ch)
#define CHIP_DMAMUX_REQGEN_REG_RGCR_(num, ch)   (CHIP_REGS_DMAMUX(num)->RGCR[ch])

/* проверка корректности соответствия номера DMA контроллера и канала и номера DMAMUX блока и его канала */
#define CHIP_DMAMUX_CHECK_NUM_OK(dmanum, dmach, muxnum, muxch)  (((((dmanum) == 1) || ((dmanum) == 2)) \
                                                                 && ((muxnum) == 1) && ((dmach) >= 0) && ((dmach) < CHIP_DMA_STREAM_CNT) \
                                                                 && ((muxch) == (((dmanum) - 1) * CHIP_DMA_STREAM_CNT + (dmach)))) \
                                                                || (((dmanum) == CHIP_DMA_BDMA_NUM) && ((muxnum) == 2) \
                                                                 && ((muxch) == (dmach))))





#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN(i)     ((i) + 1)
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN0       1
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN1       2
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN2       3
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN3       4
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN4       5
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN5       6
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN6       7
#define CHIP_DMAMUX1_REQ_DMAMUX1_REQ_GEN7       8
#define CHIP_DMAMUX1_REQ_ADC1_DMA               9
#define CHIP_DMAMUX1_REQ_ADC2_DMA               10
#define CHIP_DMAMUX1_REQ_TIM1_CH1               11
#define CHIP_DMAMUX1_REQ_TIM1_CH2               12
#define CHIP_DMAMUX1_REQ_TIM1_CH3               13
#define CHIP_DMAMUX1_REQ_TIM1_CH4               14
#define CHIP_DMAMUX1_REQ_TIM1_UP                15
#define CHIP_DMAMUX1_REQ_TIM1_TRIG              16
#define CHIP_DMAMUX1_REQ_TIM1_COM               17
#define CHIP_DMAMUX1_REQ_TIM2_CH1               18
#define CHIP_DMAMUX1_REQ_TIM2_CH2               19
#define CHIP_DMAMUX1_REQ_TIM2_CH3               20
#define CHIP_DMAMUX1_REQ_TIM2_CH4               21
#define CHIP_DMAMUX1_REQ_TIM2_UP                22
#define CHIP_DMAMUX1_REQ_TIM3_CH1               23
#define CHIP_DMAMUX1_REQ_TIM3_CH2               24
#define CHIP_DMAMUX1_REQ_TIM3_CH3               25
#define CHIP_DMAMUX1_REQ_TIM3_CH4               26
#define CHIP_DMAMUX1_REQ_TIM3_UP                27
#define CHIP_DMAMUX1_REQ_TIM3_TRIG              28
#define CHIP_DMAMUX1_REQ_TIM4_CH1               29
#define CHIP_DMAMUX1_REQ_TIM4_CH2               30
#define CHIP_DMAMUX1_REQ_TIM4_CH3               31
#define CHIP_DMAMUX1_REQ_TIM4_UP                32
#define CHIP_DMAMUX1_REQ_I2C1_RX_DMA            33
#define CHIP_DMAMUX1_REQ_I2C1_TX_DMA            34
#define CHIP_DMAMUX1_REQ_I2C2_RX_DMA            35
#define CHIP_DMAMUX1_REQ_I2C2_TX_DMA            36
#define CHIP_DMAMUX1_REQ_SPI1_RX_DMA            37
#define CHIP_DMAMUX1_REQ_SPI1_TX_DMA            38
#define CHIP_DMAMUX1_REQ_SPI2_RX_DMA            39
#define CHIP_DMAMUX1_REQ_SPI2_TX_DMA            40
#define CHIP_DMAMUX1_REQ_USART1_RX_DMA          41
#define CHIP_DMAMUX1_REQ_USART1_TX_DMA          42
#define CHIP_DMAMUX1_REQ_USART2_RX_DMA          43
#define CHIP_DMAMUX1_REQ_USART2_TX_DMA          44
#define CHIP_DMAMUX1_REQ_USART3_RX_DMA          45
#define CHIP_DMAMUX1_REQ_USART3_TX_DMA          46
#define CHIP_DMAMUX1_REQ_TIM8_CH1               47
#define CHIP_DMAMUX1_REQ_TIM8_CH2               48
#define CHIP_DMAMUX1_REQ_TIM8_CH3               49
#define CHIP_DMAMUX1_REQ_TIM8_CH4               50
#define CHIP_DMAMUX1_REQ_TIM8_UP                51
#define CHIP_DMAMUX1_REQ_TIM8_TRIG              52
#define CHIP_DMAMUX1_REQ_TIM8_COM               53
#define CHIP_DMAMUX1_REQ_TIM5_CH1               55
#define CHIP_DMAMUX1_REQ_TIM5_CH2               56
#define CHIP_DMAMUX1_REQ_TIM5_CH3               57
#define CHIP_DMAMUX1_REQ_TIM5_CH4               58
#define CHIP_DMAMUX1_REQ_TIM5_UP                59
#define CHIP_DMAMUX1_REQ_TIM5_TRIG              60
#define CHIP_DMAMUX1_REQ_SPI3_RX_DMA            61
#define CHIP_DMAMUX1_REQ_SPI3_TX_DMA            62
#define CHIP_DMAMUX1_REQ_UART4_RX_DMA           63
#define CHIP_DMAMUX1_REQ_UART4_TX_DMA           64
#define CHIP_DMAMUX1_REQ_UART5_RX_DMA           65
#define CHIP_DMAMUX1_REQ_UART5_TX_DMA           66
#define CHIP_DMAMUX1_REQ_DAC_CH1_DMA            67
#define CHIP_DMAMUX1_REQ_DAC_CH2_DMA            68
#define CHIP_DMAMUX1_REQ_TIM6_UP                69
#define CHIP_DMAMUX1_REQ_TIM7_UP                70
#define CHIP_DMAMUX1_REQ_USART6_RX_DMA          71
#define CHIP_DMAMUX1_REQ_USART6_TX_DMA          72
#define CHIP_DMAMUX1_REQ_I2C3_RX_DMA            73
#define CHIP_DMAMUX1_REQ_I2C3_TX_DMA            74
#define CHIP_DMAMUX1_REQ_DVP_DMA                75
#define CHIP_DMAMUX1_REQ_CRYP_IN_DMA            76
#define CHIP_DMAMUX1_REQ_CRYP_OUT_DMA           77
#define CHIP_DMAMUX1_REQ_HASH_IN_DMA            78
#define CHIP_DMAMUX1_REQ_UART7_RX_DMA           79
#define CHIP_DMAMUX1_REQ_UART7_TX_DMA           80
#define CHIP_DMAMUX1_REQ_UART8_RX_DMA           81
#define CHIP_DMAMUX1_REQ_UART8_TX_DMA           82
#define CHIP_DMAMUX1_REQ_SPI4_RX_DMA            83
#define CHIP_DMAMUX1_REQ_SPI4_TX_DMA            84
#define CHIP_DMAMUX1_REQ_SPI5_RX_DMA            85
#define CHIP_DMAMUX1_REQ_SPI6_TX_DMA            86
#define CHIP_DMAMUX1_REQ_SAI1A_DMA              87
#define CHIP_DMAMUX1_REQ_SAI1B_DMA              88
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_DMAMUX1_REQ_SAI2A_DMA              89
#define CHIP_DMAMUX1_REQ_SAI2B_DMA              90
#endif
#define CHIP_DMAMUX1_REQ_SWPMI_RX_DMA           91
#define CHIP_DMAMUX1_REQ_SWPMI_TX_DMA           92
#define CHIP_DMAMUX1_REQ_SPDIFRX_DAT_DMA        93
#define CHIP_DMAMUX1_REQ_SPDIFRX_CTRL_DMA       94
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_DMAMUX1_REQ_HR_REQ1                95
#define CHIP_DMAMUX1_REQ_HR_REQ2                96
#define CHIP_DMAMUX1_REQ_HR_REQ3                97
#define CHIP_DMAMUX1_REQ_HR_REQ4                98
#define CHIP_DMAMUX1_REQ_HR_REQ5                99
#define CHIP_DMAMUX1_REQ_HR_REQ6                100
#endif
#define CHIP_DMAMUX1_REQ_DFSDM1_DMA0            101
#define CHIP_DMAMUX1_REQ_DFSDM1_DMA1            102
#define CHIP_DMAMUX1_REQ_DFSDM1_DMA2            103
#define CHIP_DMAMUX1_REQ_DFSDM1_DMA3            104
#define CHIP_DMAMUX1_REQ_TIM15_CH1              105
#define CHIP_DMAMUX1_REQ_TIM15_UP               106
#define CHIP_DMAMUX1_REQ_TIM15_TRIG             107
#define CHIP_DMAMUX1_REQ_TIM15_COM              108
#define CHIP_DMAMUX1_REQ_TIM16_CH1              109
#define CHIP_DMAMUX1_REQ_TIM16_UP               110
#define CHIP_DMAMUX1_REQ_TIM17_CH1              111
#define CHIP_DMAMUX1_REQ_TIM17_UP               112
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_DMAMUX1_REQ_SAI3A_DMA              113
#define CHIP_DMAMUX1_REQ_SAI3B_DMA              114
#endif
#define CHIP_DMAMUX1_REQ_ADC3_DMA               115
#if CHIP_DEV_SUPPORT_USART9_10U
#define CHIP_DMAMUX1_REQ_UART9_RX_DMA           116
#define CHIP_DMAMUX1_REQ_UART9_TX_DMA           117
#define CHIP_DMAMUX1_REQ_USART10_RX_DMA         118
#define CHIP_DMAMUX1_REQ_USART10_TX_DMA         119
#endif
#if CHIP_DEV_SUPPORT_FMAC
#define CHIP_DMAMUX1_REQ_FMAC_RD                120
#define CHIP_DMAMUX1_REQ_FMAC_WR                121
#endif
#if CHIP_DEV_SUPPORT_CORDIC
#define CHIP_DMAMUX1_REQ_CORDIC_RD              122
#define CHIP_DMAMUX1_REQ_CORDIC_WR              123
#endif
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_DMAMUX1_REQ_I2C5_RX_DMA            124
#define CHIP_DMAMUX1_REQ_I2C5_TX_DMA            125
#endif
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_DMAMUX1_REQ_TIM23_CH1              126
#define CHIP_DMAMUX1_REQ_TIM23_CH2              127
#define CHIP_DMAMUX1_REQ_TIM23_CH3              128
#define CHIP_DMAMUX1_REQ_TIM23_CH4              129
#define CHIP_DMAMUX1_REQ_TIM23_UP               130
#define CHIP_DMAMUX1_REQ_TIM23_TRIG             131
#define CHIP_DMAMUX1_REQ_TIM24_CH1              132
#define CHIP_DMAMUX1_REQ_TIM24_CH2              133
#define CHIP_DMAMUX1_REQ_TIM24_CH3              134
#define CHIP_DMAMUX1_REQ_TIM24_CH4              135
#define CHIP_DMAMUX1_REQ_TIM24_UP               136
#define CHIP_DMAMUX1_REQ_TIM24_TRIG             137
#endif

#define CHIP_DMAMUX1_TRIGIN_DMAMUX1_EVT(i)      (i)
#define CHIP_DMAMUX1_TRIGIN_DMAMUX1_EVT0        0
#define CHIP_DMAMUX1_TRIGIN_DMAMUX1_EVT1        1
#define CHIP_DMAMUX1_TRIGIN_DMAMUX1_EVT2        2
#define CHIP_DMAMUX1_TRIGIN_LPTIM1_OUT          3
#define CHIP_DMAMUX1_TRIGIN_LPTIM2_OUT          4
#define CHIP_DMAMUX1_TRIGIN_LPTIM3_OUT          5
#define CHIP_DMAMUX1_TRIGIN_EXTIT0              6
#define CHIP_DMAMUX1_TRIGIN_TIM12_TRGO          7

#define CHIP_DMAMUX1_SYNCIN_DMAMUX1_EVT(i)      (i)
#define CHIP_DMAMUX1_SYNCIN_DMAMUX1_EVT0        0
#define CHIP_DMAMUX1_SYNCIN_DMAMUX1_EVT1        1
#define CHIP_DMAMUX1_SYNCIN_DMAMUX1_EVT2        2
#define CHIP_DMAMUX1_SYNCIN_LPTIM1_OUT          3
#define CHIP_DMAMUX1_SYNCIN_LPTIM2_OUT          4
#define CHIP_DMAMUX1_SYNCIN_LPTIM3_OUT          5
#define CHIP_DMAMUX1_SYNCIN_EXTIT0              6
#define CHIP_DMAMUX1_SYNCIN_TIM12_TRGO          7



#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN(i)     ((i) + 1)
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN0       1
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN1       2
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN2       3
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN3       4
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN4       5
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN5       6
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN6       7
#define CHIP_DMAMUX2_REQ_DMAMUX2_REQ_GEN7       8
#define CHIP_DMAMUX2_REQ_LPUART1_RX_DMA         9
#define CHIP_DMAMUX2_REQ_LPUART1_TX_DMA         10
#define CHIP_DMAMUX2_REQ_SPI6_RX_DMA            11
#define CHIP_DMAMUX2_REQ_SPI6_TX_DMA            12
#define CHIP_DMAMUX2_REQ_I2C4_RX_DMA            13
#define CHIP_DMAMUX2_REQ_I2C4_TX_DMA            14
#define CHIP_DMAMUX2_REQ_SAI4_A_DMA             15
#define CHIP_DMAMUX2_REQ_SAI4_B_DMA             16
#define CHIP_DMAMUX2_REQ_ADC3_DMA               17


#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT(i)      (i)
#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT0        0
#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT1        1
#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT2        2
#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT3        3
#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT4        4
#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT5        5
#define CHIP_DMAMUX2_TRIGIN_DMAMUX2_EVT6        6
#define CHIP_DMAMUX2_TRIGIN_LPUART_RX_WKUP      7
#define CHIP_DMAMUX2_TRIGIN_LPUART_TX_WKUP      8
#define CHIP_DMAMUX2_TRIGIN_LPTIM2_WKUP         9
#define CHIP_DMAMUX2_TRIGIN_LPTIM2_OUT          10
#define CHIP_DMAMUX2_TRIGIN_LPTIM3_WKUP         11
#define CHIP_DMAMUX2_TRIGIN_LPTIM3_OUT          12
#define CHIP_DMAMUX2_TRIGIN_LPTIM4_AIT          13
#define CHIP_DMAMUX2_TRIGIN_LPTIM5_AIT          14
#define CHIP_DMAMUX2_TRIGIN_I2C4_WKUP           15
#define CHIP_DMAMUX2_TRIGIN_SPI6_WKUP           16
#define CHIP_DMAMUX2_TRIGIN_COMP1_OUT           17
#define CHIP_DMAMUX2_TRIGIN_COMP2_OUT           18
#define CHIP_DMAMUX2_TRIGIN_RTC_WKUP            19
#define CHIP_DMAMUX2_TRIGIN_SYSCFG_EXTI0_MUX    20
#define CHIP_DMAMUX2_TRIGIN_SYSCFG_EXTI2_MUX    21
#define CHIP_DMAMUX2_TRIGIN_I2C4_EVENT_IT       22
#define CHIP_DMAMUX2_TRIGIN_SPI6_IT             23
#define CHIP_DMAMUX2_TRIGIN_LPUART_IT_T         24
#define CHIP_DMAMUX2_TRIGIN_LPUART_IT_R         25
#define CHIP_DMAMUX2_TRIGIN_ADC3_IT             26
#define CHIP_DMAMUX2_TRIGIN_ADC3_AWD1           27
#define CHIP_DMAMUX2_TRIGIN_BDMA_CH0_IT         28
#define CHIP_DMAMUX2_TRIGIN_BDMA_CH1_IT         29

#define CHIP_DMAMUX2_SYNCIN_DMAMUX2_EVT(i)      (i)
#define CHIP_DMAMUX2_SYNCIN_DMAMUX2_EVT0        0
#define CHIP_DMAMUX2_SYNCIN_DMAMUX2_EVT1        1
#define CHIP_DMAMUX2_SYNCIN_DMAMUX2_EVT2        2
#define CHIP_DMAMUX2_SYNCIN_DMAMUX2_EVT3        3
#define CHIP_DMAMUX2_SYNCIN_DMAMUX2_EVT4        4
#define CHIP_DMAMUX2_SYNCIN_DMAMUX2_EVT5        5
#define CHIP_DMAMUX2_SYNCIN_LPUART1_RX_WKUP     6
#define CHIP_DMAMUX2_SYNCIN_LPUART1_TX_WKUP     7
#define CHIP_DMAMUX2_SYNCIN_LPTIM2_OUT          8
#define CHIP_DMAMUX2_SYNCIN_LPTIM3_OUT          9
#define CHIP_DMAMUX2_SYNCIN_I2C4_WKUP           10
#define CHIP_DMAMUX2_SYNCIN_SPI6_WKUP           11
#define CHIP_DMAMUX2_SYNCIN_COMP1_OUT           12
#define CHIP_DMAMUX2_SYNCIN_RTC_WKUP            13
#define CHIP_DMAMUX2_SYNCIN_SYSCFG_EXTI0_MUX    14
#define CHIP_DMAMUX2_SYNCIN_SYSCFG_EXTI2_MUX    15



/* DMAMUX request line multiplexer channel x configuration register (DMAMUX_CxCR) */
#define CHIP_REGFLD_DMAMUX_CCR_DMAREQ_ID        (0x000000FFUL <<  0U) /* DMA request identification */
#define CHIP_REGFLD_DMAMUX_CCR_SOIE             (0x00000001UL <<  8U) /* Synchronization overrun interrupt enable */
#define CHIP_REGFLD_DMAMUX_CCR_EGE              (0x00000001UL <<  9U) /* Event generation enable */
#define CHIP_REGFLD_DMAMUX_CCR_SE               (0x00000001UL << 16U) /* Synchronization enable */
#define CHIP_REGFLD_DMAMUX_CCR_SPOL             (0x00000003UL << 17U) /* Synchronization polarity */
#define CHIP_REGFLD_DMAMUX_CCR_NBREQ            (0x0000001FUL << 19U) /* Number of DMA requests minus 1 to forward */
#define CHIP_REGFLD_DMAMUX_CCR_SYNC_ID          (0x0000001FUL << 24U) /* Synchronization identification */
#define CHIP_REGMSK_DMAMUX_CCR_RESERVED         (0xE000FC00UL)

#define CHIP_REGFLDVAL_DMAMUX_CCR_SPOL_NO       0 /* no event, i.e. no synchronization nor detection */
#define CHIP_REGFLDVAL_DMAMUX_CCR_SPOL_RISE     1 /* rising edge */
#define CHIP_REGFLDVAL_DMAMUX_CCR_SPOL_FALL     2 /* falling edge */
#define CHIP_REGFLDVAL_DMAMUX_CCR_SPOL_BOTH     3 /* rising and falling edge */

#define CHIP_REGFLDVAL_DMAMUX_CCR_NBREQ(n)      ((n)-1)

/* DMAMUX request line multiplexer interrupt channel status register (DMAMUX_CSR) */
#define CHIP_REGFLD_DMAMUX_CSR_SOF(x)           (0x00000001UL <<  (x)) /* Channel x Synchronization overrun event flag */

/* DMAMUX request line multiplexer interrupt clear flag register (DMAMUX_CFR) */
#define CHIP_REGFLD_DMAMUX_CFR_CSOF(x)          (0x00000001UL <<  (x)) /* Channel x Clear synchronization overrun event flag */

/* DMAMUX request generator channel x configuration register (DMAMUX_RGxCR) ***/
#define CHIP_REGFLD_DMAMUX_RGCR_SIG_ID          (0x0000001FUL <<  0U) /* Signal identification */
#define CHIP_REGFLD_DMAMUX_RGCR_OIE             (0x00000001UL <<  8U) /* Trigger overrun interrupt enable */
#define CHIP_REGFLD_DMAMUX_RGCR_GE              (0x00000001UL << 16U) /* DMA request generator enable */
#define CHIP_REGFLD_DMAMUX_RGCR_GPOL            (0x00000003UL << 17U) /* DMA request generator trigger polarity */
#define CHIP_REGFLD_DMAMUX_RGCR_GNBREQ          (0x0000001FUL << 19U) /* Number of DMA requests to be generated */
#define CHIP_REGMSK_DMAMUX_RGCR_RESERVED        (0xFF00FEE0UL)


#define CHIP_REGFLDVAL_DMAMUX_RGCR_GPOL_NO      0 /* no event, i.e. no synchronization nor detection */
#define CHIP_REGFLDVAL_DMAMUX_RGCR_GPOL_RISE    1 /* rising edge */
#define CHIP_REGFLDVAL_DMAMUX_RGCR_GPOL_FALL    2 /* falling edge */
#define CHIP_REGFLDVAL_DMAMUX_RGCR_GPOL_BOTH    3 /* rising and falling edge */

#define CHIP_REGFLDVAL_DMAMUX_RGCR_GNBREQ(n)    ((n)-1)

/* DMAMUX request generator interrupt status register (DMAMUX_RGSR) ***********/
#define CHIP_REGFLD_DMAMUX_RGSR_OF(x)           (0x00000001UL <<  (x)) /* Request generator channel x Trigger overrun event flag */

/* DMAMUX request generator interrupt clear flag register (DMAMUX_RGCFR) ******/
#define CHIP_REGFLD_DMAMUX_RGCFR_COF(x)         (0x00000001UL <<  (x))           /* Request generator channel 0 Clear trigger overrun event flag */


#endif // CHIP_STM32H7XX_REGS_DMAMUX_H_
