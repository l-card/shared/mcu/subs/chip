#ifndef REGS_RCU_H
#define REGS_RCU_H

#include "chip_devtype_spec_features.h"

#ifdef CHIP_LGT8FX8P
#define CHIP_REG_VDTCR      _MMIO_BYTE(0x62) /* Low Voltage Detection (LVD) Control Register */
#endif
#define CHIP_REG_MCUSR      _MMIO_BYTE(0x54) /* MCU Status Register */
#define CHIP_REG_WDTCSR     _MMIO_BYTE(0x60) /* Watchdog Control Status Register */


/* ---------------- VDTCR - Low Voltage Detection (LVD) Control Register -----*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_VDTCR_WCE               (0x01U << 7) /* (W1SC) VDTCR Value Change Enable */
#define CHIP_REGFLD_VDTCR_SWR               (0x01U << 6) /* (RW) Software Reset */
#define CHIP_REGFLD_VDTCR_VDTS              (0x07U << 2) /* (RW) Low Voltage Detection Threshold */
#define CHIP_REGFLD_VDTCR_VDR_EN            (0x01U << 1) /* (RW) Low voltage reset function enable */
#define CHIP_REGFLD_VDTCR_VDT_EN            (0x01U << 0) /* (RW) Low-voltage detection module enable */

#define CHIP_REGFLDVAL_VDTCR_VDTS_1_8V      0
#define CHIP_REGFLDVAL_VDTCR_VDTS_2_2V      1
#define CHIP_REGFLDVAL_VDTCR_VDTS_2_5V      2
#define CHIP_REGFLDVAL_VDTCR_VDTS_2_9V      3
#define CHIP_REGFLDVAL_VDTCR_VDTS_3_2V      4
#define CHIP_REGFLDVAL_VDTCR_VDTS_3_6V      5
#define CHIP_REGFLDVAL_VDTCR_VDTS_4_0V      6
#define CHIP_REGFLDVAL_VDTCR_VDTS_4_4V      7
#endif

/* ---------------- MCUSR - MCU Status Register ------------------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_MCUSR_SDDD              (0x01U << 7) /* (RW) SWD interface disable */
#define CHIP_REGFLD_MCUSR_PDRF              (0x01U << 5) /* (RW) Wake-up flag from Power-off mode */
#define CHIP_REGFLD_MCUSR_OCDRF             (0x01U << 4) /* (RW) OCD debugger reset flag */
#endif
#define CHIP_REGFLD_MCUSR_WDRF              (0x01U << 3) /* (RW) Watchdog reset flag */
#define CHIP_REGFLD_MCUSR_BORF              (0x01U << 2) /* (RW) Low voltage detection reset */
#define CHIP_REGFLD_MCUSR_EXTRF             (0x01U << 1) /* (RW) External reset flag */
#define CHIP_REGFLD_MCUSR_PORF              (0x01U << 0) /* (RW) Power-on reset flag */


/* ---------------- WDTCSR - Watchdog Control Status Register ----------------*/
#define CHIP_REGFLD_WDTCSR_WDIF             (0x01U << 7) /* (RW) WDT interrupt flag */
#define CHIP_REGFLD_WDTCSR_WDIE             (0x01U << 6) /* (RW) WDT interrupt enable */
#define CHIP_REGFLD_WDTCSR_WDP_3            (0x01U << 5) /* (RW) The WDT prescaler selection bit 3 */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_WDTCSR_WDTOE            (0x01U << 4) /* (RW) WDT shutdown enable */
#else
#define CHIP_REGFLD_WDTCSR_WDCE             (0x01U << 4) /* (RW) WDT change enable */
#endif
#define CHIP_REGFLD_WDTCSR_WDE              (0x01U << 3) /* (RW) WDT enable */
#define CHIP_REGFLD_WDTCSR_WDP              (0x07U << 0) /* (RW) The WDT prescaler selection bits 2..0 */


#define CHIP_REGFLDVAL_WDTCSR_WDP_2K        0
#define CHIP_REGFLDVAL_WDTCSR_WDP_4K        1
#define CHIP_REGFLDVAL_WDTCSR_WDP_8K        2
#define CHIP_REGFLDVAL_WDTCSR_WDP_16K       3
#define CHIP_REGFLDVAL_WDTCSR_WDP_32K       4
#define CHIP_REGFLDVAL_WDTCSR_WDP_64K       5
#define CHIP_REGFLDVAL_WDTCSR_WDP_128K      6
#define CHIP_REGFLDVAL_WDTCSR_WDP_256K      7
#define CHIP_REGFLDVAL_WDTCSR_WDP_512K      8
#define CHIP_REGFLDVAL_WDTCSR_WDP_1024K     9

#endif // REGS_RCU_H
