#ifndef CHIP_PER_CTL_H
#define CHIP_PER_CTL_H



#include "chip_clk.h"

/* Идентификаторы клоков периферийный устройств, которые могут быть разрешены
 * или запрещены при работе с помощью chip_per_clk_en() и chip_per_clk_dis()
 * соответственно, а также блок, для которого предназначен клок в большинстве
 * случаев может быть сброшен с помощью chip_per_rst() (за исключением
 * помеченных надписью clock only). */
typedef  enum {
    CHIP_PER_ID_FLASH,
    CHIP_PER_ID_PWR,
    CHIP_PER_ID_SYSCFG,
    CHIP_PER_ID_DBG,

    CHIP_PER_ID_GPIOA,
    CHIP_PER_ID_GPIOB,
    CHIP_PER_ID_GPIOC,
    CHIP_PER_ID_GPIOD,
#if CHIP_DEV_SUPPORT_PORTE
    CHIP_PER_ID_GPIOE,
#endif
    CHIP_PER_ID_GPIOF,

    CHIP_PER_ID_DMA1,
#if CHIP_DEV_SUPPORT_DMA2
    CHIP_PER_ID_DMA2,
#endif

    CHIP_PER_ID_TIM1,
    CHIP_PER_ID_TIM2,
    CHIP_PER_ID_TIM3,
#if CHIP_DEV_SUPPORT_TIM4
    CHIP_PER_ID_TIM4,
#endif
#if CHIP_DEV_SUPPORT_TIM6
    CHIP_PER_ID_TIM6,
#endif
#if CHIP_DEV_SUPPORT_TIM7
    CHIP_PER_ID_TIM7,
#endif
    CHIP_PER_ID_TIM14,
#if CHIP_DEV_SUPPORT_TIM15
    CHIP_PER_ID_TIM15,
#endif
    CHIP_PER_ID_TIM16,
    CHIP_PER_ID_TIM17,
    CHIP_PER_ID_LPTIM1,
    CHIP_PER_ID_LPTIM2,

    CHIP_PER_ID_USART1,
    CHIP_PER_ID_USART2,
#if CHIP_DEV_SUPPORT_USART3
    CHIP_PER_ID_USART3,
#endif
#if CHIP_DEV_SUPPORT_USART4
    CHIP_PER_ID_USART4,
#endif
#if CHIP_DEV_SUPPORT_USART5
    CHIP_PER_ID_USART5,
#endif
#if CHIP_DEV_SUPPORT_USART6
    CHIP_PER_ID_USART6,
#endif
    CHIP_PER_ID_LPUART1,
#if CHIP_DEV_SUPPORT_LPUART2
    CHIP_PER_ID_LPUART2,
#endif
    CHIP_PER_ID_SPI1,
    CHIP_PER_ID_SPI2,
#if CHIP_DEV_SUPPORT_SPI3
    CHIP_PER_ID_SPI3,
#endif
    CHIP_PER_ID_I2C1,
    CHIP_PER_ID_I2C2,
#if CHIP_DEV_SUPPORT_I2C3
    CHIP_PER_ID_I2C3,
#endif
    CHIP_PER_ID_ADC,
#if CHIP_DEV_SUPPORT_DAC
    CHIP_PER_ID_DAC1,
#endif

    CHIP_PER_ID_CRC,
#if CHIP_DEV_SUPPORT_AES
    CHIP_PER_ID_AES,
#endif
#if CHIP_DEV_SUPPORT_RNG
    CHIP_PER_ID_RNG,
#endif
    CHIP_PER_ID_RTCAPB,
    CHIP_PER_ID_WWDG,
#if CHIP_DEV_SUPPORT_FDCAN
    CHIP_PER_ID_FDCAN,
#endif
#if CHIP_DEV_SUPPORT_USB
    CHIP_PER_ID_USB,
#endif
#if CHIP_DEV_SUPPORT_CRS
    CHIP_PER_ID_CRS,
#endif
#if CHIP_DEV_SUPPORT_CEC
    CHIP_PER_ID_CEC,
#endif
#if CHIP_DEV_SUPPORT_UCPD
    CHIP_PER_ID_UCPD1,
    CHIP_PER_ID_UCPD2,
#endif
}  t_chip_per_id;

void chip_per_rst(t_chip_per_id per_id);
void chip_per_clk_en(t_chip_per_id per_id);
void chip_per_clk_dis(t_chip_per_id per_id);
int  chip_per_clk_is_en(t_chip_per_id per_id);

#endif // CHIP_PER_CTL_H
