#ifndef REGS_GPIO_H
#define REGS_GPIO_H

#include "chip_devtype_spec_features.h"

#define CHIP_REG_PINB       _MMIO_BYTE(0x23) /* Port B Input Data Register */
#define CHIP_REG_DDRB       _MMIO_BYTE(0x24) /* Port B Direction Register */
#define CHIP_REG_PORTB      _MMIO_BYTE(0x25) /* Port B Output Data Register */
#define CHIP_REG_PINC       _MMIO_BYTE(0x26) /* Port C Input Data Register */
#define CHIP_REG_DDRC       _MMIO_BYTE(0x27) /* Port C Direction Register */
#define CHIP_REG_PORTC      _MMIO_BYTE(0x28) /* Port C Output Data Register */
#define CHIP_REG_PIND       _MMIO_BYTE(0x29) /* Port D Input Data Register */
#define CHIP_REG_DDRD       _MMIO_BYTE(0x2A) /* Port D Direction Register */
#define CHIP_REG_PORTD      _MMIO_BYTE(0x2B) /* Port D Output Data Register */
#define CHIP_REG_PINE       _MMIO_BYTE(0x2C) /* Port E Input Data Register */
#define CHIP_REG_DDRE       _MMIO_BYTE(0x2D) /* Port E Direction Register */
#define CHIP_REG_PORTE      _MMIO_BYTE(0x2E) /* Port E Output Data Register */
#define CHIP_REG_PINF       _MMIO_BYTE(0x32) /* Port F Input Data Register */
#define CHIP_REG_DDRF       _MMIO_BYTE(0x33) /* Port F Direction Register */
#define CHIP_REG_PORTF      _MMIO_BYTE(0x34) /* Port F Output Data Register */


#ifdef CHIP_LGT8FX8P
#define CHIP_REG_HDR        _MMIO_BYTE(0xE0) /* Port Driver Control Register */
#define CHIP_REG_PMX0       _MMIO_BYTE(0xEE) /* Port Multiplexing Control Register 0 */
#define CHIP_REG_PMX1       _MMIO_BYTE(0xED) /* Port Multiplexing Control Register 1 */
#define CHIP_REG_PMX2       _MMIO_BYTE(0xF0) /* Port Multiplexing Control Register 2 */
#endif


#ifdef CHIP_LGT8FX8P
/* ---------------- HDR - Port Driver Control Register -----------------------*/
#define CHIP_REGFLD_HDR_PF5                 (0x01U << 5) /* (RW) PF5 output drive control; 1 = 80mA drive, 0 = 12mA drive */
#define CHIP_REGFLD_HDR_PF4                 (0x01U << 4) /* (RW) PF4 output drive control; 1 = 80mA drive, 0 = 12mA drive */
#define CHIP_REGFLD_HDR_PF2                 (0x01U << 3) /* (RW) PF2 output drive control; 1 = 80mA drive, 0 = 12mA drive */
#define CHIP_REGFLD_HDR_PF1                 (0x01U << 2) /* (RW) PF1 output drive control; 1 = 80mA drive, 0 = 12mA drive */
#define CHIP_REGFLD_HDR_PD6                 (0x01U << 1) /* (RW) PD6 output drive control; 1 = 80mA drive, 0 = 12mA drive */
#define CHIP_REGFLD_HDR_PD5                 (0x01U << 0) /* (RW) PD5 output drive control; 1 = 80mA drive, 0 = 12mA drive */

/* ---------------- PMX0 – Port Multiplexing Control Register 0 --------------*/
#define CHIP_REGFLD_PMX0_WCE                (0x01U << 7) /* (W1SC) PMX0/1 update enable control */
#define CHIP_REGFLD_PMX0_C1BF4              (0x01U << 6) /* (RW) OC1B Auxiliary Output Control (PB2 or PF4) */
#define CHIP_REGFLD_PMX0_C1AF5              (0x01U << 5) /* (RW) OC1A Auxiliary Output Control (PB1 or PF5) */
#define CHIP_REGFLD_PMX0_C0BF3              (0x01U << 4) /* (RW) OC0B Auxiliary output control (PD5 or PF3) */
#define CHIP_REGFLD_PMX0_C0AC0              (0x01U << 3) /* (RW) OC0A Auxiliary output control with TCCR0B.C0AS (PD6, PE4, PC0 or PF4 + PC0) */
#define CHIP_REGFLD_PMX0_SPSS               (0x01U << 2) /* (RW) SPSS Auxiliary Output Control (PB2 or PB1) */
#define CHIP_REGFLD_PMX0_TXD6               (0x01U << 1) /* (RW) Serial port TXD auxiliary output control (PD1 or PD6) */
#define CHIP_REGFLD_PMX0_TXD5               (0x01U << 0) /* (RW) Serial port RXD auxiliary output control (PD0 or PD5) */


/* ---------------- PMX1 – Port Multiplexing Control Register 1 --------------*/
#define CHIP_REGFLD_PMX1_C3AC               (0x01U << 2) /* (RW) OC3A Auxiliary Output Control (PF1 or QFP48/AC0P) */
#define CHIP_REGFLD_PMX1_C2BF7              (0x01U << 1) /* (RW) OC2B Auxiliary Output Control (PD3 or PF7) */
#define CHIP_REGFLD_PMX1_C2AF6              (0x01U << 0) /* (RW) OC2A Auxiliary Output Control (PB3 or PF6) */


/* ---------------- PMX2 – Port Multiplexing Control Register 2 --------------*/
#define CHIP_REGFLD_PMX2_WCE                (0x01U << 7) /* (W1SC) PMX2 update enable control */
#define CHIP_REGFLD_PMX2_STSC1              (0x01U << 6) /* (RW) High-speed crystal oscillator IO startup circuit control */
#define CHIP_REGFLD_PMX2_STSC0              (0x01U << 5) /* (RW) Low-speed crystal oscillator IO startup circuit control */
#define CHIP_REGFLD_PMX2_XIEN               (0x01U << 2) /* (RW) External clock input enable control */
#define CHIP_REGFLD_PMX2_E6EN               (0x01U << 1) /* (RW) Enable the general-purpose IO function of PE6; the default PE6 is the AVREF function */
#define CHIP_REGFLD_PMX2_C6EN               (0x01U << 0) /* (RW) Enable general-purpose IO function of PC6; default PC6 is external reset input */


#endif

#endif // REGS_GPIO_H
