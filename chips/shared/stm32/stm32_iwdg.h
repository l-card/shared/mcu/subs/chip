#ifndef STM32_IWDT_H
#define STM32_IWDT_H




#include "chip_config.h"

#ifndef CHIP_WDT_SUPPORT_REMCORE
    #define CHIP_WDT_SUPPORT_REMCORE 0
#endif

#ifdef CHIP_CFG_WDT_SETUP_EN
    #define CHIP_WDT_SETUP_EN         CHIP_CFG_WDT_SETUP_EN
#else
    #define CHIP_WDT_SETUP_EN         0
#endif

#ifdef CHIP_CFG_WDT_NV_SETUP_EN
    #define CHIP_WDT_NV_SETUP_EN      CHIP_CFG_WDT_NV_SETUP_EN
#else
    #define CHIP_WDT_NV_SETUP_EN      CHIP_FLASH_FUNC_EN
#endif

#if CHIP_WDT_SUPPORT_REMCORE
    #define CHIP_WDT_REMCORE_SETUP_EN CHIP_CFG_WDT_REMCORE_SETUP_EN
    #define CHIP_WDT_REMCORE_EN       CHIP_CFG_WDT_REMCORE_EN
#else
    #define CHIP_WDT_REMCORE_SETUP_EN 0
    #define CHIP_WDT_REMCORE_EN       0
#endif




#if CHIP_WDT_SETUP_EN
void chip_wdt_init(void);
#endif
void chip_wdt_reset(void);

void chip_wdt_set_interval(int ms);
#if CHIP_WDT_SUPPORT_REMCORE
    void chip_wdt_remcore_enable(void);
    void chip_wdt_remcore_set_interval(int ms);
#endif

#endif // STM32_IWDT_H
