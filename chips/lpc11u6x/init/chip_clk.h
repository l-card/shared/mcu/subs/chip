#ifndef CHIP_CLK_H
#define CHIP_CLK_H

#include "chip_cfg_defs.h"
#include "chip_std_defs.h"
#include "chip_clk_def.h"
#include "chip_clk_constraints.h"

#define CHIP_CLK_SYSOSC_EN  (CHIP_CLK_SYSOSC_MODE != CHIP_CLK_EXTMODE_DIS)
//#define CHIP_CLK_LSE_EN  (CHIP_CLK_LSE_MODE != CHIP_CLK_EXTMODE_DIS)

#if CHIP_CLK_SYSOSC_EN
    #ifndef CHIP_CFG_CLK_SYSOSC_FREQ
        #error SYSOSC clock frequency is not specified
    #endif
    #define CHIP_CLK_SYSOSC_FREQ    CHIP_CFG_CLK_SYSOSC_FREQ
#else
    #define CHIP_CLK_SYSOSC_FREQ    0
#endif




#if CHIP_CLK_SYSPLLIN_EN
    #define CHIP_CLK_SYSPLL_SRC_ID      CHIP_CLK_ID_VAL(CHIP_CLK_SYSPLL_SRC)
#if CHIP_CLK_SYSPLL_SRC_ID == CHIP_CLK_IRC_ID
    #define CHIP_CLK_SYSPLLIN_FREQ      CHIP_CLK_IRC_FREQ
    #define CHIP_CLK_SYSPLLIN_SRC_EN    CHIP_CLK_IRC_EN
#elif CHIP_CLK_SYSPLL_SRC_ID == CHIP_CLK_SYSOSC_ID
    #define CHIP_CLK_SYSPLLIN_FREQ      CHIP_CLK_SYSOSC_FREQ
    #define CHIP_CLK_SYSPLLIN_SRC_EN    CHIP_CLK_SYSOSC_EN
#elif CHIP_CLK_SYSPLL_SRC_ID == CHIP_CLK_RTCOSC_ID
    #define CHIP_CLK_SYSPLLIN_FREQ      CHIP_CLK_RTCOSC_FREQ
    #define CHIP_CLK_SYSPLLIN_SRC_EN    CHIP_CLK_RTCOSC_EN
#else
    #define CHIP_CLK_SYSPLLIN_FREQ      0
    #define CHIP_CLK_SYSPLLIN_SRC_EN    0
#endif
#else
    #define CHIP_CLK_SYSPLLIN_FREQ      0
    #define CHIP_CLK_SYSPLLIN_SRC_EN    0
#endif

#if CHIP_CLK_SYSPLL_EN
    #define CHIP_CLK_SYSPLL_FREQ        (CHIP_CLK_SYSPLLIN_FREQ * CHIP_CLK_SYSPLL_MUL)
#else
    #define CHIP_CLK_SYSPLL_FREQ        0
#endif


#if CHIP_CLK_USB_SETUP_EN
    #define CHIP_CLK_USBPLL_SRC_ID      CHIP_CLK_ID_VAL(CHIP_CLK_USBPLL_SRC)
    #if CHIP_CLK_USBPLL_SRC_ID == CHIP_CLK_IRC_ID
        #define CHIP_CLK_USBPLLIN_FREQ      CHIP_CLK_IRC_FREQ
        #define CHIP_CLK_USBPLLIN_EN        CHIP_CLK_IRC_EN
    #elif CHIP_CLK_USBPLL_SRC_ID == CHIP_CLK_SYSOSC_ID
        #define CHIP_CLK_USBPLLIN_FREQ      CHIP_CLK_SYSOSC_FREQ
        #define CHIP_CLK_USBPLLIN_EN        CHIP_CLK_SYSOSC_EN
    #else
        #define CHIP_CLK_USBPLLIN_FREQ      0
        #define CHIP_CLK_USBPLLIN_EN        0
    #endif
        #define CHIP_CLK_USBPLL_FREQ        (CHIP_CLK_USBPLLIN_FREQ * CHIP_CLK_USBPLL_MUL)

        #define CHIP_CLK_USB_SRC_ID         CHIP_CLK_ID_VAL(CHIP_CLK_USB_SRC)
    #if CHIP_CLK_USB_SRC_ID == CHIP_CLK_USBPLL_ID
        #define CHIP_CLK_USBIN_FREQ         CHIP_CLK_USBPLL_FREQ
        #define CHIP_CLK_USBIN_EN           CHIP_CLK_USBPLL_EN
    #elif CHIP_CLK_USB_SRC_ID == CHIP_CLK_MAIN_ID
        #define CHIP_CLK_USBIN_FREQ         CHIP_CLK_MAIN_FREQ
        #define CHIP_CLK_USBIN_EN           CHIP_CLK_MAIN_EN
    #endif
    #define CHIP_CLK_USB_FREQ           (CHIP_CLK_USBIN_FREQ/CHIP_CLK_USB_DIV)
#else
    #define CHIP_CLK_USBPLLIN_FREQ      0
    #define CHIP_CLK_USBPLLIN_EN        0
    #define CHIP_CLK_USBPLL_FREQ        0
#endif


#define CHIP_CLK_MAIN_SRC_ID            CHIP_CLK_ID_VAL(CHIP_CLK_MAIN_SRC)
#if CHIP_CLK_MAIN_SRC_ID == CHIP_CLK_IRC_ID
    #define CHIP_CLK_MAIN_FREQ          CHIP_CLK_IRC_FREQ
    #define CHIP_CLK_MAIN_EN            CHIP_CLK_IRC_EN
#elif CHIP_CLK_MAIN_SRC_ID == CHIP_CLK_SYSPLLIN_ID
    #define CHIP_CLK_MAIN_FREQ          CHIP_CLK_SYSPLLIN_FREQ
    #define CHIP_CLK_MAIN_EN            CHIP_CLK_SYSPLLIN_EN
#elif CHIP_CLK_MAIN_SRC_ID == CHIP_CLK_WDTOSC_ID
    #define CHIP_CLK_MAIN_FREQ          CHIP_CLK_WDTOSC_FREQ
    #define CHIP_CLK_MAIN_EN            CHIP_CLK_WDTOSC_EN
#elif CHIP_CLK_MAIN_SRC_ID == CHIP_CLK_SYSPLL_ID
    #define CHIP_CLK_MAIN_FREQ          CHIP_CLK_SYSPLL_FREQ
    #define CHIP_CLK_MAIN_EN            CHIP_CLK_SYSPLL_EN
#else
    #define CHIP_CLK_MAIN_FREQ          0
    #define CHIP_CLK_MAIN_EN            0
#endif

#define CHIP_CLK_SYS_FREQ               (CHIP_CLK_MAIN_FREQ/CHIP_CLK_SYS_DIV)
#define CHIP_CLK_SYS_EN                 CHIP_CLK_MAIN_EN

#define CHIP_CLK_CPU_FREQ               CHIP_CLK_SYS_FREQ
#define CHIP_CLK_SYSTICK_FREQ           CHIP_CLK_SYS_FREQ

#if CHIP_CLK_CLKOUT_EN
#define CHIP_CLK_CLKOUT_SRC_ID          CHIP_CLK_ID_VAL(CHIP_CLK_CLKOUT_SRC)
#if CHIP_CLK_CLKOUT_SRC_ID == CHIP_CLK_IRC_ID
    #define CHIP_CLK_CLKOUTSRC_FREQ     CHIP_CLK_IRC_FREQ
    #define CHIP_CLK_CLKOUTSRC_EN       CHIP_CLK_IRC_EN
#elif CHIP_CLK_CLKOUT_SRC_ID == CHIP_CLK_SYSOSC_ID
    #define CHIP_CLK_CLKOUTSRC_FREQ     CHIP_CLK_SYSOSC_FREQ
    #define CHIP_CLK_CLKOUTSRC_EN       CHIP_CLK_SYSOSC_EN
#elif CHIP_CLK_CLKOUT_SRC_ID == CHIP_CLK_WDTOSC_ID
    #define CHIP_CLK_CLKOUTSRC_FREQ     CHIP_CLK_WDTOSC_FREQ
    #define CHIP_CLK_CLKOUTSRC_EN       CHIP_CLK_WDTOSC_EN
#elif CHIP_CLK_CLKOUT_SRC_ID == CHIP_CLK_MAIN_ID
    #define CHIP_CLK_CLKOUTSRC_FREQ     CHIP_CLK_MAIN_FREQ
    #define CHIP_CLK_CLKOUTSRC_EN       CHIP_CLK_MAIN_EN
#else
    #define CHIP_CLK_CLKOUTSRC_FREQ     0
    #define CHIP_CLK_CLKOUTSRC_EN       0
#endif
#define CHIP_CLK_CLKOUT_FREQ            (CHIP_CLK_CLKOUTSRC_FREQ/CHIP_CLK_CLKOUT_DIV)
#endif



#if CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(850)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(600)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(1225)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(1050)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(1575)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(1400)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(1925)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(1750)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(2250)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(2100)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(2550)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(2400)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(2850)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(2700)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(3125)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(3000)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(3375)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(3250)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(3625)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(3500)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(3775)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(3750)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(4100)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(4000)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(4300)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(4200)
#elif CHIP_CLK_WDTOSC_SRC_FREQ <= CHIP_KHZ(4500)
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(4400)
#else
    #define CHIP_CLK_WDTANA_FREQ        CHIP_KHZ(4600)
#endif
#define CHIP_CLK_WDTOSC_FREQ            (CHIP_CLK_WDTANA_FREQ/CHIP_CLK_WDTOSC_DIV)


#define CHIP_CLK_WDT_SRC_ID         CHIP_CLK_ID_VAL(CHIP_WDT_CLK_SRC)
#if CHIP_CLK_WDT_SRC_ID == CHIP_CLK_IRC_ID
    #define CHIP_CLK_WDT_FREQ       (CHIP_CLK_IRC_FREQ/4)
    #define CHIP_CLK_WDT_EN         CHIP_CLK_IRC_EN
#elif CHIP_CLK_WDT_SRC_ID == CHIP_CLK_WDTOSC_ID
    #define CHIP_CLK_WDT_FREQ       (CHIP_CLK_WDTOSC_FREQ/4)
    #define CHIP_CLK_WDT_EN         CHIP_CLK_WDTOSC_EN
#else
    #define CHIP_CLK_WDT_FREQ       0
    #define CHIP_CLK_WDT_EN         0
#endif

#define CHIP_WDT_TICKS_MS(ms)       ((CHIP_CLK_WDT_FREQ * (ms) + 999) / 1000 - 1)






#define CHIP_CLK_USART0_FREQ            (CHIP_CLK_MAIN_FREQ/CHIP_CLK_USART0_DIV)
#define CHIP_CLK_SSP0_FREQ              (CHIP_CLK_MAIN_FREQ/CHIP_CLK_SSP0_DIV)
#define CHIP_CLK_SSP1_FREQ              (CHIP_CLK_MAIN_FREQ/CHIP_CLK_SSP1_DIV)

#define CHIP_CLK_FRGIN_FREQ             (CHIP_CLK_MAIN_FREQ/CHIP_CLK_FRG_DIV)
#define CHIP_CLK_FRGOUT_FREQ            (CHIP_CLK_FRGIN_FREQ/(1. + (double)CHIP_CLK_FRG_FRACT_MUL/256))




#if CHIP_CLK_FRG_DIV > 0
/* Настройка дробного делителя для USART1,2,3,4.
 * Функция использует настройку CHIP_CFG_CLK_FRG_DIV для пределителя и вычисляет
 * параметры так, чтобы получить частоту uart наиболее близко к заданной.
 * Функция настраивает и разрешает FRG и возвращает уже значение
 * постделителя для деления уже в самом uart (запись в BRG значения uart_brgdiv - 1).
 * Также функция вовзращает опционально результирующую найденную частоту uart */
void chip_frg_configure(unsigned uart_baud_rate, unsigned *uart_brgdiv, unsigned *uart_res_freq);
#endif


#endif // CHIP_CLK_H
