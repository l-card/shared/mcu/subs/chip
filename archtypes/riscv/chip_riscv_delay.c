

__attribute__ ((__noinline__)) void chip_delay_clk(unsigned int clk_count) {
#define CYCLES_OVERHEAD     7
    clk_count = clk_count > CYCLES_OVERHEAD ? clk_count - CYCLES_OVERHEAD : 1;

    /* Задержка примерно на clk_count тактов, min = 11 тактов */
    asm volatile (
        "1:"
        "addi %[reg], %[reg], -1\n"
        "bne %[reg], zero, 1b\n"
        : [reg] "+r" (clk_count)
        :
        : "cc"
        );
#undef CYCLES_OVERHEAD
}
