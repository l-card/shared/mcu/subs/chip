#ifndef CHIP_N32G03X_REGS_RCC_H
#define CHIP_N32G03X_REGS_RCC_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR;           /*!< RCC clock control register,                                  Address offset: 0x00 */
    __IO uint32_t CFGR;         /*!< RCC clock configuration register,                            Address offset: 0x04 */
    __IO uint32_t CIR;          /*!< RCC clock interrupt register,                                Address offset: 0x08 */
    __IO uint32_t APB2RSTR;     /*!< RCC APB2 peripheral reset register,                          Address offset: 0x0C */
    __IO uint32_t APB1RSTR;     /*!< RCC APB1 peripheral reset register,                          Address offset: 0x10 */
    __IO uint32_t AHBENR;       /*!< RCC AHB peripheral clock register,                           Address offset: 0x14 */
    __IO uint32_t APB2ENR;      /*!< RCC APB2 peripheral clock enable register,                   Address offset: 0x18 */
    __IO uint32_t APB1ENR;      /*!< RCC APB1 peripheral clock enable register,                   Address offset: 0x1C */
    __IO uint32_t BDCR;         /*!< RCC Backup domain control register,                          Address offset: 0x20 */
    __IO uint32_t CSR;          /*!< RCC clock control & status register,                         Address offset: 0x24 */
    __IO uint32_t AHBRSTR;      /*!< RCC AHB peripheral reset register,                           Address offset: 0x28 */
    __IO uint32_t CFGR2;        /*!< RCC clock configuration register 2,                          Address offset: 0x2C */
    __IO uint32_t EMCCTRL;      /*!< EMC Control Register                                         Address offset: 0x30 */
} CHIP_REGS_RCC_T;

#define CHIP_REGS_RCC          ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)

/*********** Clock control register (RCC_CR) **********************************/
#define CHIP_REGFLD_RCC_CR_HSI_ON                   (0x00000001UL <<  0U)   /* (RW) HSI clock enable */
#define CHIP_REGFLD_RCC_CR_HSI_RDY                  (0x00000001UL <<  1U)   /* (RO) HSI clock ready flag */
#if CHIP_DEV_SUPPORT_HSI_TRIM
#define CHIP_REGFLD_RCC_CR_HSI_TRIM                 (0x0000001FUL <<  3U)   /* (RW) HSI clock trimming */
#endif
#define CHIP_REGFLD_RCC_CR_HSE_ON                   (0x00000001UL << 16U)   /* (RW) HSE clock enable */
#define CHIP_REGFLD_RCC_CR_HSE_RDY                  (0x00000001UL << 17U)   /* (RO) HSE clock ready flag */
#if !CHIP_DEV_SUPPORT_HSE_IOSEL
#define CHIP_REGFLD_RCC_CR_HSE_BYP                  (0x00000001UL << 18U)   /* (RW) HSE crystal oscillator bypass */
#endif
#define CHIP_REGFLD_RCC_CR_CSS_ON                   (0x00000001UL << 19U)   /* (RW) Clock security system enable */
#define CHIP_REGFLD_RCC_CR_PLL_BP                   (0x00000001UL << 22U)   /* (RW) PLL bypass mode */
#define CHIP_REGFLD_RCC_CR_PLL_OUT_EN               (0x00000001UL << 23U)   /* (RW) PLL clock output enable bit */
#define CHIP_REGFLD_RCC_CR_PLL_EN                   (0x00000001UL << 24U)   /* (RW) PLL enable */
#define CHIP_REGFLD_RCC_CR_PLL_RDY                  (0x00000001UL << 25U)   /* (RO) PLL clock ready flag */
#if CHIP_DEV_SUPPORT_HSE_IOSEL
#define CHIP_REGFLD_RCC_CR_HSE_IOSEL                (0x00000003UL << 26U)   /* (RO) PLL clock ready flag */
#endif
#define CHIP_REGMSK_RCC_CR_RESERVED                 (0xF030FF04UL)

#if CHIP_DEV_SUPPORT_HSE_IOSEL
#define CHIP_REGFLDVAL_RCC_CR_HSE_IOSEL_PA0_DIG     0 /* PA0 digital input as HSE OSC_IN, HSE should be configured as external clock mode; */
#define CHIP_REGFLDVAL_RCC_CR_HSE_IOSEL_PF0_DIG     1 /* PF0 digital input as HSE OSC_IN, HSE should be configured as external clock mode; */
#define CHIP_REGFLDVAL_RCC_CR_HSE_IOSEL_PF0_ANA     2 /* PF0 analog input as HSE OSC_IN, HSE should be configured as external crystal mode; */
#endif

/*********** Clock configuration register (RCC_CFGR) **************************/
#define CHIP_REGFLD_RCC_CFGR_SW                     (0x00000007UL <<  0U)   /* (RW) System clock switch */
#define CHIP_REGFLD_RCC_CFGR_SWS1                   (0x00000001UL <<  3U)   /* (RO) System clock switch status */
#define CHIP_REGFLD_RCC_CFGR_AHB_PRE                (0x0000000FUL <<  4U)   /* (RW) HCLK prescaler */
#define CHIP_REGFLD_RCC_CFGR_APB1_PRE               (0x00000007UL <<  8U)   /* (RW) APB1 prescaler */
#define CHIP_REGFLD_RCC_CFGR_APB2_PRE               (0x00000007UL << 11U)   /* (RW) APB2 prescaler */
#define CHIP_REGFLD_RCC_CFGR_SWS2                   (0x00000003UL << 14U)   /* (RW) */
#define CHIP_REGFLD_RCC_CFGR_PLL_MUL                (0x0000000FUL << 16U)   /* (RW) PLL multiplication factor */
#define CHIP_REGFLD_RCC_CFGR_PLL_PRE                (0x00000003UL << 20U)   /* (RW) PLL prescaler */
#define CHIP_REGFLD_RCC_CFGR_PLL_OUTDIV             (0x00000003UL << 22U)   /* (RW) PLL output clock divider */
#define CHIP_REGFLD_RCC_CFGR_PLL_SRC                (0x00000001UL << 24U)   /* (RW) PLL input clock source */
#define CHIP_REGFLD_RCC_CFGR_MCO                    (0x00000007UL << 25U)   /* (RW) Microcontroller clock output */
#define CHIP_REGFLD_RCC_CFGR_MCO_PRE                (0x0000000FUL << 28U)   /* (RW) Microcontroller Clock Output Prescaler */
#define CHIP_REGMSK_RCC_CFGR_RESERVED               (0x00000000UL)


#define CHIP_REGFLDGET_RCC_CFGR_SWS(wrd)            (LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_SWS1) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_SWS2)) << 1))

#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSI              0
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSE              1
#define CHIP_REGFLDVAL_RCC_CFGR_SW_PLL              2
#define CHIP_REGFLDVAL_RCC_CFGR_SW_LSE              3
#define CHIP_REGFLDVAL_RCC_CFGR_SW_LSI              4


#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_1           0
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_2           8
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_4           9
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_8           10
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_16          11
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_64          12
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_128         13
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_256         14
#define CHIP_REGFLDVAL_RCC_CFGR_AHB_PRE_512         15


#define CHIP_REGFLDVAL_RCC_CFGR_APB_PRE_1           0
#define CHIP_REGFLDVAL_RCC_CFGR_APB_PRE_2           4
#define CHIP_REGFLDVAL_RCC_CFGR_APB_PRE_4           5
#define CHIP_REGFLDVAL_RCC_CFGR_APB_PRE_8           6
#define CHIP_REGFLDVAL_RCC_CFGR_APB_PRE_16          7


#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_3           0
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_4           1
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_5           2
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_6           3
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_7           4
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_8           5
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_9           6
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_10          7
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_11          8
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_12          9
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_13          10
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_14          11
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_15          12
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_16          13
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_17          14
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_18          15


#define CHIP_REGFLDVAL_RCC_CFGR_PLL_PRE_1           0
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_PRE_2           1
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_PRE_3           2
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_PRE_4           3

#define CHIP_REGFLDVAL_RCC_CFGR_PLL_OUTDIV_1        0
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_OUTDIV_2        1
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_OUTDIV_3        2
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_OUTDIV_4        3

#define CHIP_REGFLDVAL_RCC_CFGR_PLL_SRC_HSI         0
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_SRC_HSE         1

#define CHIP_REGFLDVAL_RCC_CFGR_MCO_DIS             0 /* MCO output disabled, no clock on MCO */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_LSI             1 /* Internal low speed (LSI) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_LSE             2 /* External low speed (LSE) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_SYSCLK          3 /* System clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI             4 /* Internal RC 8 MHz (HSI) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSE             5 /* External 4-32 MHz (HSE) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL             6 /* PLL clock selected (divided by PLL_PRE) */

#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_MIN         2
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_MAX         15

/*********** Clock interrupt register (RCC_CIR) *******************************/
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_F               (0x00000001UL <<  0U)   /* (RO) LSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_F               (0x00000001UL <<  1U)   /* (RO) LSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_F               (0x00000001UL <<  2U)   /* (RO) HSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_F               (0x00000001UL <<  3U)   /* (RO) HSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_F               (0x00000001UL <<  4U)   /* (RO) PLL ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_RAMCP_F                 (0x00000001UL <<  5U)   /* (RO) RAMC Parity interrupt status */
#define CHIP_REGFLD_RCC_CIR_CSS_F                   (0x00000001UL <<  7U)   /* (RO) Clock security system interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_IE              (0x00000001UL <<  8U)   /* (RW) LSI ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_IE              (0x00000001UL <<  9U)   /* (RW) LSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_IE              (0x00000001UL << 10U)   /* (RW) HSI ready interrupt enable  */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_IE              (0x00000001UL << 11U)   /* (RW) HSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_IE              (0x00000001UL << 12U)   /* (RW) PLL ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_RAMC_ERR_IE             (0x00000001UL << 13U)   /* (RW) RAMC parity error interrupt enable */
#define CHIP_REGFLD_RCC_CIR_RAMC_ERR_RST            (0x00000001UL << 14U)   /* (RW) RAMC parity error reset enable */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_C               (0x00000001UL << 16U)   /* (W1C) LSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_C               (0x00000001UL << 17U)   /* (W1C) LSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_C               (0x00000001UL << 18U)   /* (W1C) HSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_C               (0x00000001UL << 19U)   /* (W1C) HSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_C               (0x00000001UL << 20U)   /* (W1C) PLL ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_PERR_C                  (0x00000001UL << 21U)   /* (W1C) PERR interrupt clear. */
#define CHIP_REGFLD_RCC_CIR_CSS_C                   (0x00000001UL << 23U)   /* (W1C) Clock security system interrupt clear */
#define CHIP_REGMSK_RCC_CIR_RESERVED                (0xFF408040UL)


/*********** APB peripheral reset register 2 (RCC_APB2RSTR) *******************/
#define CHIP_REGFLD_RCC_APB2RSTR_AFIO_RST           (0x00000001UL <<  0U)   /* (RW) Auxiliary function IO reset */
#define CHIP_REGFLD_RCC_APB2RSTR_IOPA_RST           (0x00000001UL <<  2U)   /* (RW) GPIO port A reset */
#define CHIP_REGFLD_RCC_APB2RSTR_IOPB_RST           (0x00000001UL <<  3U)   /* (RW) GPIO port B reset */
#define CHIP_REGFLD_RCC_APB2RSTR_IOPC_RST           (0x00000001UL <<  4U)   /* (RW) GPIO port C reset */
#if CHIP_DEV_SUPPORT_PORTD
#define CHIP_REGFLD_RCC_APB2RSTR_IOPD_RST           (0x00000001UL <<  5U)   /* (RW) GPIO port D reset */
#endif
#define CHIP_REGFLD_RCC_APB2RSTR_IOPF_RST           (0x00000001UL <<  7U)   /* (RW) GPIO port F reset */
#define CHIP_REGFLD_RCC_APB2RSTR_SPI1_RST           (0x00000001UL <<  9U)   /* (RW) SPI1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_SPI2_RST           (0x00000001UL << 10U)   /* (RW) SPI1 reset */
#if CHIP_DEV_SPI_CNT >= 3
#define CHIP_REGFLD_RCC_APB2RSTR_SPI3_RST           (0x00000001UL << 11U)   /* (RW) SPI3 reset */
#endif
#define CHIP_REGFLD_RCC_APB2RSTR_TIM1_RST           (0x00000001UL << 12U)   /* (RW) TIM1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM8_RST           (0x00000001UL << 13U)   /* (RW) TIM8 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART1_RST         (0x00000001UL << 14U)   /* (RW) USART1 reset */
#if CHIP_DEV_SUPPORT_UART6
#define CHIP_REGFLD_RCC_APB2RSTR_UART6_RST          (0x00000001UL << 17U)   /* (RW) UART6 reset */
#endif
#define CHIP_REGMSK_RCC_APB2RSTR_RESERVED           (0xFFFD8142UL)




/*********** APB peripheral reset register 1 (RCC_APB1RSTR) *******************/
#define CHIP_REGFLD_RCC_APB1RSTR_TIM3_RST           (0x00000001UL <<  1U)   /* (RW) TIM3 timer reset */
#if CHIP_DEV_SUPPORT_TIM4
#define CHIP_REGFLD_RCC_APB1RSTR_TIM4_RST           (0x00000001UL <<  2U)   /* (RW) TIM4 timer reset */
#endif
#define CHIP_REGFLD_RCC_APB1RSTR_LPTIM_RST          (0x00000001UL <<  3U)   /* (RW) LPTIM timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM6_RST           (0x00000001UL <<  4U)   /* (RW) TIM6 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_BEEP1_RST          (0x00000001UL <<  5U)   /* (RW) Beeper1 reset */
#if CHIP_DEV_BEEPER_CNT >= 2
#define CHIP_REGFLD_RCC_APB1RSTR_BEEP2_RST          (0x00000001UL <<  6U)   /* (RW) Beeper2 reset */
#endif
#define CHIP_REGFLD_RCC_APB1RSTR_WWDG_RST           (0x00000001UL << 11U)   /* (RW) Window watchdog reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART2_RST         (0x00000001UL << 17U)   /* (RW) USART2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_LPUART1_RST        (0x00000001UL << 18U)   /* (RW) LPUART1 reset */
#if CHIP_DEV_LPUART_CNT >= 2
#define CHIP_REGFLD_RCC_APB1RSTR_LPUART2_RST        (0x00000001UL << 19U)   /* (RW) LPUART2 reset */
#endif
#if CHIP_DEV_SUPPORT_UART5
#define CHIP_REGFLD_RCC_APB1RSTR_UART5_RST          (0x00000001UL << 20U)   /* (RW) USART5 reset */
#endif
#define CHIP_REGFLD_RCC_APB1RSTR_I2C1_RST           (0x00000001UL << 21U)   /* (RW) I2C1 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C2_RST           (0x00000001UL << 22U)   /* (RW) I2C2 reset */
#if CHIP_DEV_CAN_CNT >= 1
#define CHIP_REGFLD_RCC_APB1RSTR_CAN1_RST           (0x00000001UL << 26U)   /* (RW) CAN interface reset */
#endif
#define CHIP_REGFLD_RCC_APB1RSTR_PWR_RST            (0x00000001UL << 28U)   /* (RW) Power interface reset */
#define CHIP_REGMSK_RCC_APB1RSTR_RESERVED           (0xEB81F781UL)

/*********** AHB peripheral clock enable register (RCC_AHBENR) ****************/
#define CHIP_REGFLD_RCC_AHBENR_DMA_EN               (0x00000001UL <<  0U)   /* (RW) DMA1 clock enable */
#define CHIP_REGFLD_RCC_AHBENR_SRAM_EN              (0x00000001UL <<  2U)   /* (RW) SRAM interface clock enable */
#define CHIP_REGFLD_RCC_AHBENR_FLITF_EN             (0x00000001UL <<  4U)   /* (RW) FLITF clock enable */
#define CHIP_REGFLD_RCC_AHBENR_HSQRT_EN             (0x00000001UL <<  5U)   /* (RW) HSQRT clock enable */
#define CHIP_REGFLD_RCC_AHBENR_CRC_EN               (0x00000001UL <<  6U)   /* (RW) CRC clock enable */
#define CHIP_REGFLD_RCC_AHBENR_HDIV_EN              (0x00000001UL <<  7U)   /* (RW) HDIV clock enable */
#if CHIP_DEV_SUPPORT_RNG
#define CHIP_REGFLD_RCC_AHBENR_RNG_EN               (0x00000001UL <<  9U)   /* (RW) RNG clock enable */
#endif
#if CHIP_DEV_SUPPORT_SAC
#define CHIP_REGFLD_RCC_AHBENR_SAC_EN               (0x00000001UL << 11U)   /* (RW) SAC clock enable */
#endif
#define CHIP_REGFLD_RCC_AHBENR_ADC_EN               (0x00000001UL << 12U)   /* (RW) ADC clock enable */
#define CHIP_REGMSK_RCC_AHBENR_RESERVED             (0xFFFFE50AUL)


/*********** APB peripheral clock enable register 2 (RCC_APB2ENR) ****************/
#define CHIP_REGFLD_RCC_APB2ENR_AFIO_EN             (0x00000001UL <<  0U)   /* (RW) Auxiliary function IO clock enable. */
#define CHIP_REGFLD_RCC_APB2ENR_IOPA_EN             (0x00000001UL <<  2U)   /* (RW) GPIO port A clock enable. */
#define CHIP_REGFLD_RCC_APB2ENR_IOPB_EN             (0x00000001UL <<  3U)   /* (RW) GPIO port B clock enable. */
#define CHIP_REGFLD_RCC_APB2ENR_IOPC_EN             (0x00000001UL <<  4U)   /* (RW) GPIO port C clock enable. */
#if CHIP_DEV_SUPPORT_PORTD
#define CHIP_REGFLD_RCC_APB2ENR_IOPD_EN             (0x00000001UL <<  5U)   /* (RW) GPIO port D clock enable. */
#endif
#define CHIP_REGFLD_RCC_APB2ENR_IOPF_EN             (0x00000001UL <<  7U)   /* (RW) GPIO port F clock enable. */
#define CHIP_REGFLD_RCC_APB2ENR_SPI1_EN             (0x00000001UL <<  9U)   /* (RW) SPI1 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_SPI2_EN             (0x00000001UL << 10U)   /* (RW) SPI2 clock enable */
#if CHIP_DEV_SPI_CNT >= 3
#define CHIP_REGFLD_RCC_APB2ENR_SPI3_EN             (0x00000001UL << 11U)   /* (RW) SPI3 clock enable */
#endif
#define CHIP_REGFLD_RCC_APB2ENR_TIM1_EN             (0x00000001UL << 12U)   /* (RW) TIM1 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM8_EN             (0x00000001UL << 13U)   /* (RW) TIM8 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART1_EN           (0x00000001UL << 14U)   /* (RW) USART1 clock enable */
#if CHIP_DEV_SUPPORT_UART6
#define CHIP_REGFLD_RCC_APB2ENR_UART6_EN            (0x00000001UL << 17U)   /* (RW) UART6 clock enable */
#endif
#define CHIP_REGMSK_RCC_APB2ENR_RESERVED            (0xFFFD8142UL)


/*********** APB peripheral clock enable register 1 (RCC_APB1ENR) ****************/
#define CHIP_REGFLD_RCC_APB1ENR_TIM3_EN             (0x00000001UL <<  1U)   /* (RW) TIM3 timer clock enable */
#if CHIP_DEV_SUPPORT_TIM4
#define CHIP_REGFLD_RCC_APB1ENR_TIM4_EN             (0x00000001UL <<  2U)   /* (RW) TIM4 timer clock enable */
#endif
#define CHIP_REGFLD_RCC_APB1ENR_LPTIM_EN            (0x00000001UL <<  3U)   /* (RW) LPTIM timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM6_EN             (0x00000001UL <<  4U)   /* (RW) TIM6 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_BEEP1_EN            (0x00000001UL <<  5U)   /* (RW) Beeper1 clock enable */
#if CHIP_DEV_BEEPER_CNT >= 2
#define CHIP_REGFLD_RCC_APB1ENR_BEEP2_EN            (0x00000001UL <<  6U)   /* (RW) Beeper2 clock enable */
#endif
#define CHIP_REGFLD_RCC_APB1ENR_COMP_EN             (0x00000001UL <<  8U)   /* (RW) Comparator Clock Enable */
#define CHIP_REGFLD_RCC_APB1ENR_COMPFILT_EN         (0x00000001UL <<  9U)   /* (RW) Comparator Filter Clock Enable. */
#if CHIP_DEV_SUPPORT_TSC
#define CHIP_REGFLD_RCC_APB1ENR_TSC_EN              (0x00000001UL << 10U)   /* (RW) TSC clock enable.. */
#endif
#define CHIP_REGFLD_RCC_APB1ENR_WWDG_EN             (0x00000001UL << 11U)   /* (RW) Window watchdog clock enable.. */
#define CHIP_REGFLD_RCC_APB1ENR_USART2_EN           (0x00000001UL << 17U)   /* (RW) USART2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_LPUART1_EN          (0x00000001UL << 18U)   /* (RW) LPUART1 clock enable */
#if CHIP_DEV_LPUART_CNT >= 2
#define CHIP_REGFLD_RCC_APB1ENR_LPUART2_EN          (0x00000001UL << 19U)   /* (RW) LPUART2 clock enable */
#endif
#if CHIP_DEV_SUPPORT_UART5
#define CHIP_REGFLD_RCC_APB1ENR_UART5_EN            (0x00000001UL << 20U)   /* (RW) USART5 clock enable */
#endif
#define CHIP_REGFLD_RCC_APB1ENR_I2C1_EN             (0x00000001UL << 21U)   /* (RW) I2C1 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C2_EN             (0x00000001UL << 22U)   /* (RW) I2C2 clock enable */
#if CHIP_DEV_CAN_CNT >= 1
#define CHIP_REGFLD_RCC_APB1ENR_CAN1_EN             (0x00000001UL << 26U)   /* (RW) CAN interface clock enable */
#endif
#define CHIP_REGFLD_RCC_APB1ENR_PWR_EN              (0x00000001UL << 28U)   /* (RW) Power interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_OPA_EN              (0x00000001UL << 31U)   /* (RW) OPA clock enable */
#define CHIP_REGMSK_RCC_APB1ENR_RESERVED            (0x6B81F081UL)


/*********** RTC domain control register (RCC_BDCR) ***************************/
#define CHIP_REGFLD_RCC_BDCR_LSI_EN                 (0x00000001UL <<  0U)   /* (RW) LSI oscillator enable */
#define CHIP_REGFLD_RCC_BDCR_LSI_RDY                (0x00000001UL <<  1U)   /* (RO) LSI oscillator ready */
#define CHIP_REGFLD_RCC_BDCR_LSE_EN                 (0x00000001UL <<  2U)   /* (RW) LSE oscillator enable */
#define CHIP_REGFLD_RCC_BDCR_LSE_RDY                (0x00000001UL <<  3U)   /* (RW) LSE oscillator ready */
#define CHIP_REGFLD_RCC_BDCR_LSE_BYP                (0x00000001UL <<  4U)   /* (RW) LSE oscillator bypass */
#define CHIP_REGFLD_RCC_BDCR_RTC_SEL                (0x00000003UL <<  5U)   /* (RW) RTC clock source selection */
#define CHIP_REGFLD_RCC_BDCR_RTC_EN                 (0x00000001UL <<  7U)   /* (RW) RTC clock enable */
#define CHIP_REGFLD_RCC_BDCR_RTC_RST                (0x00000001UL <<  8U)   /* (RW) RTC software reset */
#define CHIP_REGFLD_RCC_BDCR_LPRUN_SEL              (0x00000001UL <<  9U)   /* (RW) LPRUN clock selection */
#if CHIP_DEV_SUPPORT_TSC
#define CHIP_REGFLD_RCC_BDCR_TSC_SEL                (0x00000001UL << 10U)   /* (RW) TSC clock selection */
#endif
#define CHIP_REGMSK_RCC_BDCR_RESERVED               (0xFFFFF800UL)


#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_DIS         0
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSE         1
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSI         2
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_HSE         3

#define CHIP_REGFLDVAL_RCC_BDCR_LRUN_SEL_LSI        0
#define CHIP_REGFLDVAL_RCC_BDCR_LRUN_SEL_LSE        1

#if CHIP_DEV_TSC_CNT >= 1
#define CHIP_REGFLDVAL_RCC_BDCR_TSC_SEL_LSI         0
#define CHIP_REGFLDVAL_RCC_BDCR_TSC_SEL_LSE         1
#endif



/*********** Control/status register (RCC_CSR) ********************************/
#define CHIP_REGFLD_RCC_CSR_RM_RSTF                 (0x00000001UL <<  0U)   /* (W1SC) Remove Reset Flags */
#define CHIP_REGFLD_RCC_CSR_RAM_RSTF                (0x00000001UL <<  1U)   /* (RO) RAM reset flag */
#define CHIP_REGFLD_RCC_CSR_MMU_RSTF                (0x00000001UL <<  2U)   /* (RO) MMU reset flag */
#define CHIP_REGFLD_RCC_CSR_PIN_RSTF                (0x00000001UL <<  3U)   /* (RO) PIN reset flag */
#define CHIP_REGFLD_RCC_CSR_POR_RSTF                (0x00000001UL <<  4U)   /* (RO) POR reset flag */
#define CHIP_REGFLD_RCC_CSR_SFT_RSTF                (0x00000001UL <<  5U)   /* (RO) Software reset flag */
#define CHIP_REGFLD_RCC_CSR_IWDG_RSTF               (0x00000001UL <<  6U)   /* (RO) Independent watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_WWDG_RSTF               (0x00000001UL <<  7U)   /* (RO) Window watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_LPWR_RSTF               (0x00000001UL <<  8U)   /* (RO) Low power reset flag */
#define CHIP_REGFLD_RCC_CSR_EMC_GBN_RSTF            (0x00000001UL <<  9U)   /* (RO) EMC GBN reset flag */
#define CHIP_REGFLD_RCC_CSR_EMC_GB_RSTF             (0x00000001UL <<  10U)  /* (RO) EMC GB reset flag */
#define CHIP_REGFLD_RCC_CSR_EMC_CLAMP_RSTF          (0x00000001UL <<  11U)  /* (RO) EMC Clamp reset flag */
#define CHIP_REGMSK_RCC_CSR_RESERVED                (0xFFFFF000UL)


/*********** AHB peripheral reset register (RCC_AHBRSTR) **********************/
#define CHIP_REGFLD_RCC_AHBRSTR_HSQRT_RST           (0x00000001UL <<  5U)   /* (RW) HSQRT reset */
#define CHIP_REGFLD_RCC_AHBRSTR_HDIV_RST            (0x00000001UL <<  7U)   /* (RW) HDIV reset */
#if CHIP_DEV_SUPPORT_RNG
#define CHIP_REGFLD_RCC_AHBRSTR_RNG_RST             (0x00000001UL <<  9U)   /* (RW) RNG reset */
#endif
#if CHIP_DEV_SUPPORT_SAC
#define CHIP_REGFLD_RCC_AHBRSTR_SAC_RST             (0x00000001UL << 11U)   /* (RW) SAC reset */
#endif
#define CHIP_REGFLD_RCC_AHBRSTR_ADC_RST             (0x00000001UL << 12U)   /* (RW) ADC reset */
#define CHIP_REGMSK_RCC_AHBRSTR_RESERVED            (0xFFFFE55FUL)

/*********** Clock configuration register 2 (RCC_CFGR2) ***********************/
#define CHIP_REGFLD_RCC_CFGR2_ADC_HPRE              (0x0000000FUL <<  0U)   /* (RW) ADC HCLK prescaler.  */
#define CHIP_REGFLD_RCC_CFGR2_ADC_PLL_PRE           (0x0000001FUL <<  4U)   /* (RW) ADC PLL prescaler.  */
#define CHIP_REGFLD_RCC_CFGR2_ADC_1M_SEL            (0x00000001UL << 10U)   /* (RW) ADC 1M clock source selection. */
#define CHIP_REGFLD_RCC_CFGR2_ADC_1M_PRE            (0x0000001FUL << 11U)   /* (RW) ADC 1M clock prescaler */
#if CHIP_DEV_SUPPORT_RNG
#define CHIP_REGFLD_RCC_CFGR2_RNG_PRE               (0x0000001FUL << 16U)   /* (RW) RNG prescaler */
#endif
#define CHIP_REGFLD_RCC_CFGR2_LPTIM_SEL             (0x00000007UL << 21U)   /* (RW) LPTIM timer clock source selection */
#if CHIP_DEV_SUPPORT_RNG
#define CHIP_REGFLD_RCC_CFGR2_RNG_AN_EN             (0x00000001UL << 24U)   /* (RW) RNG analog interface clock enable */
#endif
#define CHIP_REGFLD_RCC_CFGR2_LPUART1_SEL           (0x00000007UL << 25U)   /* (RW) LPUART1 timer clock source selection */
#if CHIP_DEV_LPUART_CNT >= 2
#define CHIP_REGFLD_RCC_CFGR2_LPUART2_SEL           (0x00000007UL << 28U)   /* (RW) LPUART2 timer clock source selection */
#endif
#define CHIP_REGFLD_RCC_CFGR2_TIM_SEL               (0x00000001UL << 31U)   /* (RW) TIMCLK: Timer1/8 source selection */


#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_1         0
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_2         1
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_3         2
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_4         3
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_6         4
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_8         5
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_10        6
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_12        7
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_16        8
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_HPRE_32        9

#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_DIS    0x00
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_1      0x10
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_2      0x11
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_3      0x12
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_4      0x13
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_6      0x14
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_8      0x15
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_10     0x16
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_12     0x17
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_16     0x18
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_32     0x19
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_64     0x1A
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_128    0x1B
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_PLL_PRE_256    0x1C

#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_1M_SEL_HSI     0
#define CHIP_REGFLDVAL_RCC_CFGR2_ADC_1M_SEL_HSE     1

#define CHIP_REGFLDVAL_RCC_CFGR2_LPTIM_SEL_APB1     0
#define CHIP_REGFLDVAL_RCC_CFGR2_LPTIM_SEL_HSI      1
#define CHIP_REGFLDVAL_RCC_CFGR2_LPTIM_SEL_HSE      2
#define CHIP_REGFLDVAL_RCC_CFGR2_LPTIM_SEL_LSI      3
#define CHIP_REGFLDVAL_RCC_CFGR2_LPTIM_SEL_LSE      4
#define CHIP_REGFLDVAL_RCC_CFGR2_LPTIM_SEL_COMP     5

#define CHIP_REGFLDVAL_RCC_CFGR2_LPUART_SEL_APB1    0
#define CHIP_REGFLDVAL_RCC_CFGR2_LPUART_SEL_SYSCLK  1
#define CHIP_REGFLDVAL_RCC_CFGR2_LPUART_SEL_HSI     2
#define CHIP_REGFLDVAL_RCC_CFGR2_LPUART_SEL_HSE     3
#define CHIP_REGFLDVAL_RCC_CFGR2_LPUART_SEL_LSI     4
#define CHIP_REGFLDVAL_RCC_CFGR2_LPUART_SEL_LSE     5

#define CHIP_REGFLDVAL_RCC_CFGR2_TIM_SEL_TIMCLK     0
#define CHIP_REGFLDVAL_RCC_CFGR2_TIM_SEL_SYSCLK     1


/*********** EMC Control Register (RCC_EMCCTRL) *******************************/
#define CHIP_REGFLD_RCC_EMCCTRL_CLP_DET(n)          (0x00000001UL <<  ((n) + 0U))  /* (RW) EMC Clamp n detection enable (n=0..3)*/
#define CHIP_REGFLD_RCC_EMCCTRL_GBN_DET(n)          (0x00000001UL <<  ((n) + 4U))  /* (RW) GBN n detection enable (n=0..3)*/
#define CHIP_REGFLD_RCC_EMCCTRL_GB_DET(n)           (0x00000001UL <<  ((n) + 8U))  /* (RW) GB n detection enable (n=0..3)*/
#define CHIP_REGFLD_RCC_EMCCTRL_CLP_RST(n)          (0x00000001UL <<  ((n) + 12U)) /* (RW) EMC Clamp n reset enable (n=0..3)*/
#define CHIP_REGFLD_RCC_EMCCTRL_GBN_RST(n)          (0x00000001UL <<  ((n) + 16U)) /* (RW) GBN n reset enable (n=0..3)*/
#define CHIP_REGFLD_RCC_EMCCTRL_GB_RST(n)           (0x00000001UL <<  ((n) + 20U)) /* (RW) GB n reset enable (n=0..3)*/


#endif // CHIP_N32G03X_REGS_RCC_H

