#ifndef CHIP_CFG_DEFS_H
#define CHIP_CFG_DEFS_H

#include "chip_config.h"

#ifdef CHIP_CFG_PLIC_VECTOR_MODE
    #define CHIP_USE_PLIC_ANDES_VECTOR_MODE     CHIP_CFG_PLIC_VECTOR_MODE
#else
    #define CHIP_USE_PLIC_ANDES_VECTOR_MODE     1
#endif

#ifdef CHIP_CFG_PLIC_PREEMTION
    #define CHIP_USE_PLIC_ANDES_PREEMTION       CHIP_CFG_PLIC_PREEMTION
#else
    #define CHIP_USE_PLIC_ANDES_PREEMTION       1
#endif

#ifdef CHIP_CFG_CYCLE_CSR_ACCESS
#define CHIP_CYCLE_CSR_ACCESS                   CHIP_CFG_CYCLE_CSR_ACCESS
#else
#define CHIP_CYCLE_CSR_ACCESS                   1
#endif

#ifdef CHIP_CFG_STARTUP_CLEANUP
    #define CHIP_USE_STARTUP_CLEANUP            CHIP_CFG_STARTUP_CLEANUP
#else
    #define CHIP_USE_STARTUP_CLEANUP            1
#endif

#endif // CHIP_CFG_DEFS_H
