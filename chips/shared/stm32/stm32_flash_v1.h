#ifndef STM32_FLASH_V1_H
#define STM32_FLASH_V1_H

#include "chip_ioreg_defs.h"

/* параметры, которые должны быть определены до включения данного файла в зависимости от возможностей чипа
   CHIP_SUPPORT_FLASH_BANK2             - поддержка второго банка памяти
   CHIP_CLK_FLASH_LATENCYn_MAX          - максимальная частота для использования n циклов ожидания при обращении к flash
                                          (по определению на каждое возможное значение, кроме старшего)
   CHIP_SUPPORT_FLASH_FORCE_OB_LOAD     - поддержка бита OBL_LAUNCH регистра CR
   CHIP_SUPPORT_FLASH_OPTION_NBOOT0     - поддержка опции NBOOT0 (stm32f04x, stm32f09x)
   CHIP_SUPPORT_FLASH_OPTION_BOOT_SEL   - поддержка опции BOOT_SEL (stm32f04x, stm32f09x)
   CHIP_SUPPORT_FLASH_REG_WS_EN         - поддержка регистра разрешения wait-state  (gd)
   CHIP_SUPPORT_FLASH_REG_PID           - поддержка регистра разрешения product id  (gd)
   CHIP_SUPPORT_FLASH_ECC               - поддержка ECC (nationstech)
   CHIP_SUPPORT_FLASH_STD_PROG          - поддержка обычной записи
   CHIP_SUPPORT_FLASH_QUICK_PROG        - поддержка быстрого стирания/записи (wch)
   CHIP_SUPPORT_FLASH_WAKE_CTL          - управление wake (wch)
   CHIP_SUPPORT_FLASH_BUF_CTL           - управление буфером (wch)
   CHIP_SUPPORT_FLASH_BLOCK_ERASE       - стариние блоков (wch)
   CHIP_FLASH_CUSTOM_OPTIONS            - свое определение options bytes

   */


#define CHIP_FLASH_KEY1     0x45670123
#define CHIP_FLASH_KEY2     0xCDEF89AB




typedef struct  {
    __IO uint32_t ACR;          /* FLASH access control register,                 Address offset: 0x00 */
    __IO uint32_t KEYR;         /* FLASH key register,                            Address offset: 0x04 */
    __IO uint32_t OPTKEYR;      /* FLASH OPT key register,                        Address offset: 0x08 */
    __IO uint32_t SR;           /* FLASH status register,                         Address offset: 0x0C */
    __IO uint32_t CR;           /* FLASH control register,                        Address offset: 0x10 */
    __IO uint32_t AR;           /* FLASH address register,                        Address offset: 0x14 */
    __IO uint32_t RESERVED;     /*  Reserved,                                                     0x18 */
    __IO uint32_t OBR;          /* FLASH option bytes register,                   Address offset: 0x1C */
    __IO uint32_t WRPR;         /* FLASH write protection register,               Address offset: 0x20 */
#if CHIP_SUPPORT_FLASH_QUICK_PROG
    __IO uint32_t MODEKEYR;     /* Extension Key Register,                        Address offset: 0x24 */
#elif CHIP_SUPPORT_FLASH_ECC
    __IO uint32_t ECC;
#else
    uint32_t Reserved0;
#endif
    uint32_t Reserved1[7];
#if CHIP_SUPPORT_FLASH_BANK2
    __IO uint32_t KEYR2;        /*!<FLASH key register 2,                          Address offset: 0x44 */
    uint32_t Reserved2;
    __IO uint32_t SR2;          /*!<FLASH status register 2,                       Address offset: 0x4C */
    __IO uint32_t CR2;          /*!<FLASH control register 2,                      Address offset: 0x50 */
    __IO uint32_t AR2;          /*!<FLASH address register 2,                      Address offset: 0x54 */
#else
    uint32_t Reserved2[5];
#endif
    uint32_t Reserved3[41];
#if CHIP_SUPPORT_FLASH_REG_WS_EN
    __IO uint32_t WS_EN;      /*!<FMC wait state enable register,               Address offset: 0xFC */
#else
    uint32_t Reserved4;
#endif
#if CHIP_SUPPORT_FLASH_REG_PID
    __I  uint32_t PID;        /*!<Product ID register,                          Address offset: 0x100 */
#else
    uint32_t Reserved5;
#endif
} CHIP_REGS_FLASH_T;

#define CHIP_REGS_FLASH          ((CHIP_REGS_FLASH_T *) CHIP_MEMRGN_ADDR_PERIPH_FLASH)

/*********** Flash access control register (FLASH_ACR) ************************/
#define CHIP_REGFLD_FLASH_ACR_LATENCY               (0x00000007UL <<  0U)   /* (RW) Latency */
#define CHIP_REGFLD_FLASH_ACR_HLF_CYA               (0x00000001UL <<  3U)   /* (RW) (!wch) Flash half cycle access enable */
#define CHIP_REGFLD_FLASH_ACR_PRFTB_E               (0x00000001UL <<  4U)   /* (RW) (!wch) Prefetch buffer enable */
#define CHIP_REGFLD_FLASH_ACR_PRFTB_S               (0x00000001UL <<  5U)   /* (RO) (!wch) Prefetch buffer status */


/*********** Flash status register (FLASH_SR) *********************************/
#define CHIP_REGFLD_FLASH_SR_BSY                    (0x00000001UL <<  0U)   /* (RO)   Busy */
#if CHIP_SUPPORT_FLASH_STD_PROG
#define CHIP_REGFLD_FLASH_SR_PG_ERR                 (0x00000001UL <<  2U)   /* (RW1C) (!wch) Programming error */
#endif
#define CHIP_REGFLD_FLASH_SR_WRPRT_ERR              (0x00000001UL <<  4U)   /* (RW1C) Write protection error */
#define CHIP_REGFLD_FLASH_SR_EOP                    (0x00000001UL <<  5U)   /* (RW1C) End of operation */
#if CHIP_SUPPORT_FLASH_WAKE_CTL
#define CHIP_REGFLD_FLASH_SR_FWAKE_FLAG             (0x00000001UL <<  6U)   /* (RW0C) (wch) FLASH wake-up flag, write 0 to clear  */
#define CHIP_REGFLD_FLASH_SR_TURBO                  (0x00000001UL <<  7U)   /* (RO)   (wch) accelerate wake-up, increase current */
#endif


/*********** Flash control register (FLASH_CR) ********************************/
#if CHIP_SUPPORT_FLASH_STD_PROG
#define CHIP_REGFLD_FLASH_CR_PG                     (0x00000001UL <<  0U)   /* (RW) (!wch) Programming */
#endif
#define CHIP_REGFLD_FLASH_CR_PER                    (0x00000001UL <<  1U)   /* (RW) Page/sector erase */
#define CHIP_REGFLD_FLASH_CR_MER                    (0x00000001UL <<  2U)   /* (RW) Mass erase */
#define CHIP_REGFLD_FLASH_CR_OPT_PG                 (0x00000001UL <<  4U)   /* (RW) Option byte programming */
#define CHIP_REGFLD_FLASH_CR_OPT_ER                 (0x00000001UL <<  5U)   /* (RW) Option byte erase */
#define CHIP_REGFLD_FLASH_CR_STRT                   (0x00000001UL <<  6U)   /* (RW1SC) Start erase operation, hw clear after complete */
#define CHIP_REGFLD_FLASH_CR_LOCK                   (0x00000001UL <<  7U)   /* (RW1) Lock FPEC and FLASH_CR (unlock sequence required to clear to 0) */
#define CHIP_REGFLD_FLASH_CR_OPT_WRE                (0x00000001UL <<  9U)   /* (RW0) Option byte write enable (set by sequence to OPTKEYR) */
#define CHIP_REGFLD_FLASH_CR_ERR_IE                 (0x00000001UL << 10U)   /* (RW) Error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_EOP_IE                 (0x00000001UL << 12U)   /* (RW) End of operation interrupt enable */
#if CHIP_SUPPORT_FLASH_FORCE_OB_LOAD
#define CHIP_REGFLD_FLASH_CR_OBL_LAUNCH             (0x00000001UL << 13U)   /* (W1) Force option byte loading */
#elif CHIP_SUPPORT_FLASH_ECC
#define CHIP_REGFLD_FLASH_CR_ECC_ERR_IE             (0x00000001UL << 13U)   /* (RW) ECC error interrupt */
#elif CHIP_SUPPORT_FLASH_WAKE_CTL
#define CHIP_REGFLD_FLASH_CR_FWAKE_IE               (0x00000001UL << 13U)   /* (RW) (wch) FLASH Wake-up interrupt enable */
#endif
#if CHIP_SUPPORT_FLASH_QUICK_PROG
#define CHIP_REGFLD_FLASH_CR_FLOCK                  (0x00000001UL << 15U)   /* (RW1) (wch) Quick programming lock (set to 0 by unlock sequence to MODEKEYR) */
#define CHIP_REGFLD_FLASH_CR_FT_PG                  (0x00000001UL << 16U)   /* (RW1) (wch) Performs quick page programming */
#define CHIP_REGFLD_FLASH_CR_FT_ER                  (0x00000001UL << 17U)   /* (RW1) (wch) Performs a fast page (256Byte) erase operation. */
#endif
#if CHIP_SUPPORT_FLASH_BUF_CTL
#define CHIP_REGFLD_FLASH_CR_BUF_LOAD               (0x00000001UL << 18U)   /* (RW)  (wch) Cache data into BUF. */
#define CHIP_REGFLD_FLASH_CR_BUF_RST                (0x00000001UL << 19U)   /* (RW)  (wch) BUF reset operation. */
#endif
#if CHIP_SUPPORT_FLASH_BLOCK_ERASE
#define CHIP_REGFLD_FLASH_CR_BER32                  (0x00000001UL << 23U)   /* (RW)  (wch) Perform block erase 32KB. */
#endif


#if !CHIP_FLASH_CUSTOM_OPTIONS
/*********** FFlash Option byte register (FLASH_OBR) **************************/
#define CHIP_REGFLD_FLASH_OBR_OPT_ERR               (0x00000001UL <<  0U)   /* (RO) Option byte error */
#define CHIP_REGFLD_FLASH_OBR_RD_PTR                (0x00000003UL <<  1U)   /* (RO) Read protection level status */
#define CHIP_REGFLD_FLASH_OBR_WDG_SW                (0x00000001UL <<  8U)   /* (RO) */
#define CHIP_REGFLD_FLASH_OBR_NRST_STOP             (0x00000001UL <<  9U)   /* (RO) */
#define CHIP_REGFLD_FLASH_OBR_NRST_STDBY            (0x00000001UL << 10U)   /* (RO) */
#if CHIP_SUPPORT_FLASH_OPTION_NBOOT0
#define CHIP_REGFLD_FLASH_OBR_NBOOT0                (0x00000001UL << 11U)   /* (RO) */
#endif
#define CHIP_REGFLD_FLASH_OBR_NBOOT1                (0x00000001UL << 12U)   /* (RO) */
#define CHIP_REGFLD_FLASH_OBR_VDDA_MONITOR          (0x00000001UL << 13U)   /* (RO) */
#define CHIP_REGFLD_FLASH_OBR_RAM_PARITY_CHECK      (0x00000001UL << 14U)   /* (RO) */
#if CHIP_SUPPORT_FLASH_OPTION_BOOT_SEL
#define CHIP_REGFLD_FLASH_OBR_BOOT_SEL              (0x00000001UL << 15U)   /* (RO) BOOT_SEL option bit */
#endif
#define CHIP_REGFLD_FLASH_OBR_DATA0                 (0x000000FFUL << 16U)   /* (RO) DATA0 */
#define CHIP_REGFLD_FLASH_OBR_DATA1                 (0x000000FFUL << 24U)   /* (RO) DATA1 */
#define CHIP_REGMSK_FLASH_OBR_RESERVED              (0x000000F8UL)
#endif


void chip_flash_init(void);

#endif // STM32_FLASH_V1_H
