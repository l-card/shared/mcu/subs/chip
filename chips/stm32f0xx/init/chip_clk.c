#include "chip.h"
#include "chip_clk_constraints.h"
#include "chip_init_faults.h"
#include "chip_crs.h"
#if CHIP_LTIMER_EN
    #include "lclock.h"
#endif





#define CHIP_RVAL_RCC_CR_WTRDY_MSK    (CHIP_CLK_HSE_EN ? CHIP_REGFLD_RCC_CR_HSE_RDY : 0)
#define CHIP_RVAL_RCC_CR2_WTRDY_MSK   ((CHIP_CLK_HSI48_EN ? CHIP_REGFLD_RCC_CR2_HSI48_RDY : 0) \
                                         | (CHIP_CLK_HSI14_EN ? CHIP_REGFLD_RCC_CR2_HSI14_RDY : 0))

#if CHIP_CLK_PLL_EN
    #if ((CHIP_CLK_PLL_PREDIV >= 1) && (CHIP_CLK_PLL_PREDIV <= 16))
        #define CHIP_RVAL_RCC_CFGR2_PREDIV     (CHIP_CLK_PLL_PREDIV - 1)
    #else
        #error "Invalid PLL predivider value"
    #endif

    #if ((CHIP_CLK_PLL_MUL >= 2) && (CHIP_CLK_PLL_MUL <= 16))
        #define CHIP_RVAL_RCC_CFGR_PLLMUL  (CHIP_CLK_PLL_MUL - 2)
    #else
        #error "Invalid PLL multiplier value"
    #endif


    #if !CHIP_CLK_PLLIN_EN
        #error PLL clock source is not enabled
    #endif
    #if CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_CFGR_PLLSRC    1
        #if (!CHIP_DEV_SUPPORT_HSI_PLL_PREDIV && (CHIP_CLK_PLL_PREDIV != 2))
            #error Only predivider 2 is supported on this device type
        #endif
    #elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_CFGR_PLLSRC    2
    #elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI48_ID
        #if !CHIP_DEV_SUPPORT_HSI48
            #error HSI48 is not supported on this device`
        #else
            #define CHIP_RVAL_RCC_CFGR_PLLSRC    3
        #endif
    #else
         #error Invalid PLL clock source
    #endif

    #if ((CHIP_CLK_PLL_FREQ < CHIP_CLK_PLL_FREQ_MIN) || (CHIP_CLK_PLL_FREQ > CHIP_CLK_PLL_FREQ_MAX))
        #error PLL output freq is out of range
    #endif
#endif


/* --------------------- Проверка параметров SystemClock -------------------- */
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_RVAL_RCC_CFGR_SW    CHIP_REGFLDVAL_RCC_CFGR_SW_HSI
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_RVAL_RCC_CFGR_SW    CHIP_REGFLDVAL_RCC_CFGR_SW_HSE
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLL_ID
    #define CHIP_RVAL_RCC_CFGR_SW    CHIP_REGFLDVAL_RCC_CFGR_SW_PLL
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSI48_ID
    #define CHIP_RVAL_RCC_CFGR_SW    CHIP_REGFLDVAL_RCC_CFGR_SW_HSI48
    #if !CHIP_DEV_SUPPORT_HSI48
        #error HSI48 is not supported on this device
    #endif
#else
    #error Invalid System Clock Source (must be from HSI, HSE, PLL, HSI48)
#endif

#if !CHIP_CLK_SYS_EN
    #error System clock source is not enabled
#endif



/* -------------------- Проверка частот AHB ----------------------------------*/
#if CHIP_CLK_AHB_DIV == 1
    #define CHIP_RVAL_RCC_CFGR_HPRE             0
#elif CHIP_CLK_AHB_DIV == 2
    #define CHIP_RVAL_RCC_CFGR_HPRE             8
#elif CHIP_CLK_AHB_DIV == 4
    #define CHIP_RVAL_RCC_CFGR_HPRE             9
#elif CHIP_CLK_AHB_DIV == 8
    #define CHIP_RVAL_RCC_CFGR_HPRE             10
#elif CHIP_CLK_AHB_DIV == 16
    #define CHIP_RVAL_RCC_CFGR_HPRE             11
#elif CHIP_CLK_AHB_DIV == 64
    #define CHIP_RVAL_RCC_CFGR_HPRE             12
#elif CHIP_CLK_AHB_DIV == 128
    #define CHIP_RVAL_RCC_CFGR_HPRE             13
#elif CHIP_CLK_AHB_DIV == 256
    #define CHIP_RVAL_RCC_CFGR_HPRE             14
#elif CHIP_CLK_AHB_DIV == 512
    #define CHIP_RVAL_RCC_CFGR_HPRE             15
#else
    #error invalid AHB clock divider
#endif

#if CHIP_CLK_AHB_FREQ > CHIP_CLK_AHB_FREQ_MAX
    #error AHB freq is out of range
#endif

/* -------------------- Проверка частот APB1/APB2 ----------------------------*/
#if CHIP_CLK_APB_DIV == 1
    #define CHIP_RVAL_RCC_CFGR_PPRE             0
#elif CHIP_CLK_APB_DIV == 2
    #define CHIP_RVAL_RCC_CFGR_PPRE             4
#elif CHIP_CLK_APB_DIV == 4
    #define CHIP_RVAL_RCC_CFGR_PPRE             5
#elif CHIP_CLK_APB_DIV == 8
    #define CHIP_RVAL_RCC_CFGR_PPRE             6
#elif CHIP_CLK_APB_DIV == 16
    #define CHIP_RVAL_RCC_CFGR_PPRE             7
#else
    #error invalid APB clock divider
#endif

#if CHIP_CLK_APB_FREQ > CHIP_CLK_APB_FREQ_MAX
    #error APB freq is out of range
#endif




/* ----------------------------- определение MCO ----------------------------*/
#if CHIP_CLK_MCO_EN
    #if CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSI14_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI14
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_LSI_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_LSI
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_LSE
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_SYSCLK
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSE_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_HSE
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_PLL_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL
    #elif CHIP_CLK_MCO_SRC_ID == CHIP_CLK_HSI48_ID
        #define CHIP_RVAL_RCC_CFGR_MCO   CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI48
    #else
        #error "Invalid MCO source (must be HSI14, LSI, LSE, SYS, HSI, HSE, PLL or HSI48)"
    #endif


    #if !CHIP_CLK_MCO_SRC_EN
        #error MCO source is not eneabled
    #endif

    #if CHIP_DEV_SUPPORT_MCO_PRE
        #if (CHIP_CLK_MCO_DIV == 1)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_1
        #elif (CHIP_CLK_MCO_DIV == 2)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_2
        #elif (CHIP_CLK_MCO_DIV == 4)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_4
        #elif (CHIP_CLK_MCO_DIV == 8)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_8
        #elif (CHIP_CLK_MCO_DIV == 16)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_16
        #elif (CHIP_CLK_MCO_DIV == 32)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_32
        #elif (CHIP_CLK_MCO_DIV == 64)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_64
        #elif (CHIP_CLK_MCO_DIV == 128)
            #define CHIP_RVAL_RCC_CFGR_MCO_PRE  CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_128
        #else
            #error "Invalid PLL divider value"
        #endif

        #if CHIP_CLK_MCO_PLL_DIV == 1
            #define CHIP_RVAL_RCC_CFGR_MCO_PLLNODIV    1
        #elif CHIP_CLK_MCO_PLL_DIV == 2
            #define CHIP_RVAL_RCC_CFGR_MCO_PLLNODIV    0
        #else
            #error invalid PLL MCO devider
        #endif
    #else
        #if (CHIP_CLK_MCO_DIV != 1)
            #error MCO divider is not supported on this chip
        #endif
        #if (CHIP_CLK_MCO_PLL_DIV != 1)
            #error MCO PLL divider is not supported on this chip
        #endif
    #endif
#else
    #define CHIP_RVAL_RCC_CFGR_MCO              CHIP_REGFLDVAL_RCC_CFGR_MCO_DIS
    #define CHIP_RVAL_RCC_CFGR_MCO_PRE          0
    #define CHIP_RVAL_RCC_CFGR_MCO_PLLNODIV     0
#endif




/* -------------------- Проверка источников периферии ------------------------*/
#if CHIP_CLK_I2C1_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_RVAL_RCC_CFGR3_I2C1SW        CHIP_REGFLDVAL_RCC_CFGR3_I2C1_SW_HSI
#elif CHIP_CLK_I2C1_SRC_ID == CHIP_CLK_SYS_ID
    #define CHIP_RVAL_RCC_CFGR3_I2C1SW        CHIP_REGFLDVAL_RCC_CFGR3_I2C1_SW_SYSCLK
#else
    #error "Invalid I2C1 clock source (HSI, SYS)"
#endif
#if CHIP_CLK_I2C1_EN && !CHIP_CLK_I2C1_SRC_EN
    #error I2C1 source clock is not enabled
#endif

#if CHIP_CLK_USART1_SRC_ID == CHIP_CLK_APB_ID
    #define CHIP_RVAL_RCC_CFGR3_USART1SW        CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_PCLK
#elif CHIP_CLK_USART1_SRC_ID == CHIP_CLK_SYS_ID
    #define CHIP_RVAL_RCC_CFGR3_USART1SW        CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_SYSCLK
#elif CHIP_CLK_USART1_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_RVAL_RCC_CFGR3_USART1SW        CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_HSI
#elif CHIP_CLK_USART1_SRC_ID == CHIP_CLK_LSE_ID
    #define CHIP_RVAL_RCC_CFGR3_USART1SW        CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_LSE
#else
    #error Invalid USART1 Source (must be from APB, SYS, HSI, LSE)
#endif
#if CHIP_CLK_USART1_EN && !CHIP_CLK_USART1_SRC_EN
    #error USART1 source clock is not enabled
#endif

#if CHIP_DEV_SUPPORT_USART2_CLKSEL
    #if CHIP_CLK_USART2_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CFGR3_USART2SW        CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_PCLK
    #elif CHIP_CLK_USART2_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CFGR3_USART2SW        CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_SYSCLK
    #elif CHIP_CLK_USART2_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_CFGR3_USART2SW        CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_HSI
    #elif CHIP_CLK_USART2_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CFGR3_USART2SW        CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_LSE
    #else
        #error Invalid USART2 Source (must be from APB, SYS, HSI, LSE)
    #endif
    #if CHIP_CLK_USART2_EN && !CHIP_CLK_USART2_SRC_EN
        #error USART2 source clock is not enabled
    #endif
#else
    #if CHIP_CLK_USART2_SRC_ID != CHIP_CLK_APB_ID
        #error Devise doesntt support clock selection for USART2
    #endif
#endif

#if CHIP_DEV_SUPPORT_USART3_CLKSEL
    #if CHIP_CLK_USART3_SRC_ID == CHIP_CLK_APB_ID
        #define CHIP_RVAL_RCC_CFGR3_USART3SW        CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_PCLK
    #elif CHIP_CLK_USART3_SRC_ID == CHIP_CLK_SYS_ID
        #define CHIP_RVAL_RCC_CFGR3_USART3SW        CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_SYSCLK
    #elif CHIP_CLK_USART3_SRC_ID == CHIP_CLK_HSI_ID
        #define CHIP_RVAL_RCC_CFGR3_USART3SW        CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_HSI
    #elif CHIP_CLK_USART3_SRC_ID == CHIP_CLK_LSE_ID
        #define CHIP_RVAL_RCC_CFGR3_USART3SW        CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_LSE
    #else
        #error Invalid USART3 Source (must be from APB, SYS, HSI, LSE)
    #endif
    #if CHIP_CLK_USART3_EN && !CHIP_CLK_USART3_SRC_EN
        #error USART3 source clock is not enabled
    #endif
#else
    #if CHIP_CLK_USART3_SRC_ID != CHIP_CLK_APB_ID
        #error Devise doesntt support clock selection for USART3
    #endif
#endif


#if CHIP_CLK_CEC_SRC_ID == CHIP_CLK_HSI_DIV244_ID
    #define CHIP_RVAL_RCC_CFGR3_CECSW        CHIP_REGFLDVAL_RCC_CFGR3_CEC_SW_HSI_DIV244
#elif CHIP_CLK_CEC_SRC_ID == CHIP_CLK_LSE_ID
    #define CHIP_RVAL_RCC_CFGR3_CECSW        CHIP_REGFLDVAL_RCC_CFGR3_CEC_SW_LSE
#else
    #error "Invalid CEC clock source (HSI_DIV244, LSE)"
#endif
#if CHIP_CLK_CEC_EN && !CHIP_CLK_CEC_SRC_EN
    #error CEC source clock is not enabled
#endif

#if CHIP_CLK_USB_SRC_ID == CHIP_CLK_HSI48_ID
    #define CHIP_RVAL_RCC_CFGR3_USBSW        CHIP_REGFLDVAL_RCC_CFGR3_USB_SW_HSI48
#elif CHIP_CLK_USB_SRC_ID == CHIP_CLK_PLL_ID
    #define CHIP_RVAL_RCC_CFGR3_USBSW        CHIP_REGFLDVAL_RCC_CFGR3_USB_SW_PLL
#else
    #error "Invalid USB clock source (HSI48, PLL)"
#endif
#if CHIP_CLK_USB_EN && !CHIP_CLK_USB_SRC_EN
    #error USB source clock is not enabled
#endif



void chip_clk_init(void) {
    /* включение LSI либо если разрешен явно, либо если требуется настройка IWDG.
     * Т.к. IWDG следует разрешить как можно раньше, включаем этот клок до
     * настройки остального */
#if CHIP_CLK_LSI_EN
    CHIP_REGS_RCC->CSR |= CHIP_REGFLD_RCC_CSR_LSI_ON;
#endif

#if CHIP_WDT_SETUP_EN
    /* wdt настраиваем как можно раньше, но нужно убедиться, что его клок уже готов */
    while ((CHIP_REGS_RCC->CSR & CHIP_REGFLD_RCC_CSR_LSI_RDY) != CHIP_REGFLD_RCC_CSR_LSI_RDY) {
        continue;
    }
    chip_wdt_init();
#endif


    /* Если процессор работает не от HSI, то переходим в режим по умолчанию:
     * включаем HSI и ждем готовности, переводим процессор на работу от него */
    if (LBITFIELD_GET(CHIP_REGS_RCC->CFGR,  CHIP_REGFLD_RCC_CFGR_SWS) != CHIP_REGFLDVAL_RCC_CFGR_SW_HSI) {
        CHIP_REGS_RCC->CR |= CHIP_REGFLD_RCC_CR_HSI_ON;


        CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_REGFLD_RCC_CR_HSI_RDY) == CHIP_REGFLD_RCC_CR_HSI_RDY,
                           CHIP_INIT_FAULT_WAIT_HSI_RDY, 10000);

        LBITFIELD_UPD(CHIP_REGS_RCC->CFGR, CHIP_REGFLD_RCC_CFGR_SW, CHIP_REGFLDVAL_RCC_CFGR_SW_HSI);
    }


    CHIP_REGS_RCC->CR = (CHIP_REGS_RCC->CR & (CHIP_REGMSK_RCC_CR_RESERVED | CHIP_REGFLD_RCC_CR_HSI_TRIM))
            | CHIP_REGFLD_RCC_CR_HSI_ON
        #if CHIP_CLK_HSE_EN
            | CHIP_REGFLD_RCC_CR_HSE_ON
        #if CHIP_CLK_HSE_MODE == CHIP_CLK_EXTMODE_CLOCK
            | CHIP_REGFLD_RCC_CR_HSE_BYP
        #endif
        #if CHIP_CLK_HSE_CSS_EN
            | CHIP_REGFLD_RCC_CR_CSS_ON
        #endif
        #endif
            ;

    CHIP_REGS_RCC->CR2 = (CHIP_REGS_RCC->CR2 & (CHIP_REGMSK_RCC_CR2_RESERVED | CHIP_REGFLD_RCC_CR2_HSI14_TRIM))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CR2_HSI14_ON, CHIP_CLK_HSI14_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CR2_HSI48_ON, CHIP_CLK_HSI48_EN)
            ;

    /* ожидание установки всех базовых клоков */
#if CHIP_RVAL_RCC_CR_WTRDY_MSK != 0
    CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_RVAL_RCC_CR_WTRDY_MSK) == CHIP_RVAL_RCC_CR_WTRDY_MSK,
                       CHIP_INIT_FAULT_WAIT_HSE_RDY, 10000);
#endif
#if CHIP_RVAL_RCC_CR2_WTRDY_MSK != 0
    CHIP_INIT_WAIT((CHIP_REGS_RCC->CR2 & CHIP_RVAL_RCC_CR2_WTRDY_MSK) == CHIP_RVAL_RCC_CR2_WTRDY_MSK,
                       CHIP_INIT_FAULT_WAIT_HSI8_48_RDY, 10000);
#endif


    /* настройка PLL если требуется */
#if CHIP_CLK_PLL_EN
    LBITFIELD_UPD(CHIP_REGS_RCC->CFGR2, CHIP_REGFLD_RCC_CFGR2_PREDIV, CHIP_RVAL_RCC_CFGR2_PREDIV);

    CHIP_REGS_RCC->CFGR = (CHIP_REGS_RCC->CFGR & ~(CHIP_REGFLD_RCC_CFGR_PLL_MUL
                               | CHIP_REGFLD_RCC_CFGR_PLL_SRC))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL_MUL, CHIP_RVAL_RCC_CFGR_PLLMUL)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL_SRC, CHIP_RVAL_RCC_CFGR_PLLSRC)
            ;

    /* разрешение PLL и ожидание установки */
    LBITFIELD_UPD(CHIP_REGS_RCC->CR, CHIP_REGFLD_RCC_CR_PLL_ON, 1);

    CHIP_INIT_WAIT((CHIP_REGS_RCC->CR & CHIP_REGFLD_RCC_CR_PLL_RDY) == CHIP_REGFLD_RCC_CR_PLL_RDY,
                   CHIP_INIT_FAULT_WAIT_PLL_RDY, 10000);
#endif

    /* перед настройкой SYS_CLK устанавливаем нужные параметры LATENCY для flash */
    chip_flash_init();

    /* выбор источника системного клока и делителей шин */
    CHIP_REGS_RCC->CFGR = (CHIP_REGS_RCC->CFGR & ~(CHIP_REGFLD_RCC_CFGR_SW
                               | CHIP_REGFLD_RCC_CFGR_HPRE
                               | CHIP_REGFLD_RCC_CFGR_PPRE
                               | CHIP_REGFLD_RCC_CFGR_MCO
#if CHIP_DEV_SUPPORT_MCO_PRE
                               | CHIP_REGFLD_RCC_CFGR_MCO_PRE
                               | CHIP_REGFLD_RCC_CFGR_PLL_NODIV
#endif
                                                   ))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_SW,        CHIP_RVAL_RCC_CFGR_SW)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_HPRE,      CHIP_RVAL_RCC_CFGR_HPRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PPRE,      CHIP_RVAL_RCC_CFGR_PPRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO,       CHIP_RVAL_RCC_CFGR_MCO)
#if CHIP_DEV_SUPPORT_MCO_PRE
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_MCO_PRE,   CHIP_RVAL_RCC_CFGR_MCO_PRE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL_NODIV, CHIP_RVAL_RCC_CFGR_MCO_PLLNODIV)
#endif
            ;

    /* настройка источников для периферии */
    CHIP_REGS_RCC->CFGR3 = (CHIP_REGS_RCC->CFGR3 & CHIP_REGMSK_RCC_CFGR3_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR3_USART1_SW,    CHIP_RVAL_RCC_CFGR3_USART1SW)
#if CHIP_DEV_SUPPORT_USART2_CLKSEL
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR3_USART2_SW,    CHIP_RVAL_RCC_CFGR3_USART2SW)
#endif
#if CHIP_DEV_SUPPORT_USART3_CLKSEL
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR3_USART3_SW,    CHIP_RVAL_RCC_CFGR3_USART3SW)
#endif
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR3_I2C1_SW,      CHIP_RVAL_RCC_CFGR3_I2C1SW)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR3_CEC_SW,       CHIP_RVAL_RCC_CFGR3_CECSW)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR3_USB_SW,       CHIP_RVAL_RCC_CFGR3_USBSW)
            ;

    CHIP_REGS_RCC->AHBENR = (CHIP_REGS_RCC->AHBENR & CHIP_REGMSK_RCC_AHBENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_DMA1_EN,         CHIP_CLK_DMA1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_DMA2_EN,         CHIP_CLK_DMA2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_SRAM_EN,         CHIP_CLK_SRAM_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_FLITF_EN,        CHIP_CLK_FLASH_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_CRC_EN,          CHIP_CLK_CRC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_IOPA_EN,         CHIP_CLK_GPIOA_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_IOPB_EN,         CHIP_CLK_GPIOB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_IOPC_EN,         CHIP_CLK_GPIOC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_IOPD_EN,         CHIP_CLK_GPIOD_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_IOPE_EN,         CHIP_CLK_GPIOE_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_IOPF_EN,         CHIP_CLK_GPIOF_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBENR_TSC_EN,          CHIP_CLK_TSC_EN)
            ;

    CHIP_REGS_RCC->APB2ENR = (CHIP_REGS_RCC->APB2ENR & CHIP_REGMSK_RCC_APB2ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SYSCG_COMP_EN,  CHIP_CLK_SYSCFG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART6_EN,      CHIP_CLK_USART6_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART7_EN,      CHIP_CLK_USART7_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART8_EN,      CHIP_CLK_USART8_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_ADC_EN,         CHIP_CLK_ADC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM1_EN,        CHIP_CLK_TIM1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_SPI1_EN,        CHIP_CLK_SPI1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_USART1_EN,      CHIP_CLK_USART1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM15_EN,       CHIP_CLK_TIM15_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM16_EN,       CHIP_CLK_TIM16_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_TIM17_EN,       CHIP_CLK_TIM17_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_DBGMCU_EN,      CHIP_CLK_DBG_EN)
            ;

    CHIP_REGS_RCC->APB1ENR = (CHIP_REGS_RCC->APB1ENR & CHIP_REGMSK_RCC_APB1ENR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM2_EN,        CHIP_CLK_TIM2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM3_EN,        CHIP_CLK_TIM3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM6_EN,        CHIP_CLK_TIM6_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM7_EN,        CHIP_CLK_TIM7_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_TIM14_EN,       CHIP_CLK_TIM14_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_WWDG_EN,        CHIP_CLK_WWDG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_SPI2_EN,        CHIP_CLK_SPI2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USART2_EN,      CHIP_CLK_USART2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USART3_EN,      CHIP_CLK_USART3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USART4_EN,      CHIP_CLK_USART4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USART5_EN,      CHIP_CLK_USART5_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_I2C1_EN,        CHIP_CLK_I2C1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_I2C2_EN,        CHIP_CLK_I2C2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_USB_EN,         CHIP_CLK_USB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_CAN_EN,         CHIP_CLK_CAN_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_CRS_EN,         CHIP_CLK_CRS_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_PWR_EN,         CHIP_CLK_PWR_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_DAC_EN,         CHIP_CLK_DAC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1ENR_CEC_EN,         CHIP_CLK_CEC_EN)
            ;

#if CHIP_CLK_CRS_SETUP_EN
    chip_crs_init();
#endif

#if CHIP_CLK_MCO_EN
    CHIP_PIN_CONFIG(CHIP_PIN_PA8_MCO);
#endif

#if CHIP_LTIMER_EN
    /* разрешение ltimer */
    lclock_init();
#endif
}
