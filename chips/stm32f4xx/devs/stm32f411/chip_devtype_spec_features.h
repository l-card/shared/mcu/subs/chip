#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_DEV_STM32F411
#define CHIP_DEV_STM32F41X


#define CHIP_DEV_SUPPORT_LCD            0
#define CHIP_DEV_SUPPORT_EXMC           (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 90)
#define CHIP_DEV_SUPPORT_SDRAM          0
#define CHIP_DEV_SUPPORT_EXMC_D32       0
#define CHIP_DEV_SUPPORT_SAI            0
#define CHIP_DEV_SUPPORT_DVP            0
#define CHIP_DEV_SUPPORT_ETH            0
#define CHIP_DEV_SUPPORT_USBHS          0
#define CHIP_DEV_SUPPORT_USBHS_PHY      0
#define CHIP_DEV_SUPPORT_CRS            0
#define CHIP_DEV_SUPPORT_QSPI           0
#define CHIP_DEV_SUPPORT_I2C_TXFRAME    0
#define CHIP_DEV_SUPPORT_TRACE          1

#define CHIP_DEV_SPI_CNT                5
#define CHIP_DEV_I2S_CNT                5
#define CHIP_DEV_I2S_FD_CNT             2
#define CHIP_DEV_I2C_CNT                3
#define CHIP_DEV_UART_CNT              3
#define CHIP_DEV_TIM_GP_CNT             7
#define CHIP_DEV_TIM_ADV_CNT            1
#define CHIP_DEV_TIM_BASIC_CNT          0

#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(100)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
