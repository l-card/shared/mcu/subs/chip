#ifndef REGS_EXTEND_H
#define REGS_EXTEND_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t CTR; /* Extended Control Register  */
} CHIP_REGS_EXTEN_T;

#define CHIP_REGS_EXTEN                 ((CHIP_REGS_EXTEN_T *) CHIP_MEMRGN_ADDR_PERIPH_EXTEN)

/*********** Extended Control Register (EXTEN_CTR) ****************************/
#define CHIP_REGFLD_EXTEN_CTR_LDO_TRIM              (0x00000003UL << 12U) /* (RW) Adjust digital core voltage value, LDO voltage value (default 2) */
#define CHIP_REGFLD_EXTEN_CTR_ULLDO_TRIM            (0x00000007UL <<  8U) /* (RW) Adjust ULLDO voltage value in low-power mode (default 4) */
#define CHIP_REGFLD_EXTEN_CTR_LKUP_RST              (0x00000001UL <<  7U) /* (RW1C) LOCKUP occurs and causes system reset */
#define CHIP_REGFLD_EXTEN_CTR_HSI_PRE               (0x00000001UL <<  4U) /* (RW) HSI (1) or HSI/2 (0) clock selected as PLL input clock */
#define CHIP_REGMSK_EXTEN_CTR_RESERVED              (0xFFFFC82FUL)

#endif // REGS_EXTEND_H
