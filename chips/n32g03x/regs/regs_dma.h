#ifndef CHIP_N32G03X_REGS_DMA_H
#define CHIP_N32G03X_REGS_DMA_H

#include "chip_devtype_spec_features.h"

#define CHIP_DMA_CHANNEL_CNT            CHIP_DEV_DMA_CH_CNT
#define CHIP_SUPPORT_DMA_CHSEL          1

#include "stm32_dma_v1_regs.h"

#define CHIP_REGS_DMA                   ((CHIP_REGS_DMA_T *) CHIP_MEMRGN_ADDR_PERIPH_DMA)


#define CHIP_DMA_REQ_ADC                0
#define CHIP_DMA_REQ_USART1_TX          1
#define CHIP_DMA_REQ_USART1_RX          2
#define CHIP_DMA_REQ_USART2_TX          3
#define CHIP_DMA_REQ_USART2_RX          4
#define CHIP_DMA_REQ_LPUART1_TX         5
#define CHIP_DMA_REQ_LPUART1_RX         6
#if CHIP_DEV_LPUART_CNT >= 2
#define CHIP_DMA_REQ_LPUART2_TX         7
#define CHIP_DMA_REQ_LPUART2_RX         8
#endif
#if CHIP_DEV_UART_CNT >= 3
#define CHIP_DMA_REQ_UART5_TX           9
#define CHIP_DMA_REQ_UART5_RX           10
#endif
#if CHIP_DEV_UART_CNT >= 4
#define CHIP_DMA_REQ_UART6_TX           11
#define CHIP_DMA_REQ_UART6_RX           12
#endif
#define CHIP_DMA_REQ_SPI1_TX            13
#define CHIP_DMA_REQ_SPI1_RX            14
#define CHIP_DMA_REQ_SPI2_TX            15
#define CHIP_DMA_REQ_SPI2_RX            16
#if CHIP_DEV_SPI_CNT >= 3
#define CHIP_DMA_REQ_SPI3_TX            17
#define CHIP_DMA_REQ_SPI3_RX            18
#endif
#define CHIP_DMA_REQ_I2C1_TX            19
#define CHIP_DMA_REQ_I2C1_RX            20
#define CHIP_DMA_REQ_I2C2_TX            21
#define CHIP_DMA_REQ_I2C3_RX            22
#define CHIP_DMA_REQ_TIM1_CH1           23
#define CHIP_DMA_REQ_TIM1_CH2           24
#define CHIP_DMA_REQ_TIM1_CH3           25
#define CHIP_DMA_REQ_TIM1_CH4           26
#define CHIP_DMA_REQ_TIM1_COM           27
#define CHIP_DMA_REQ_TIM1_UP            28
#define CHIP_DMA_REQ_TIM1_TRIG          29
#define CHIP_DMA_REQ_TIM8_CH1           30
#define CHIP_DMA_REQ_TIM8_CH2           31
#define CHIP_DMA_REQ_TIM8_CH3           32
#define CHIP_DMA_REQ_TIM8_CH4           33
#define CHIP_DMA_REQ_TIM8_COM           34
#define CHIP_DMA_REQ_TIM8_UP            35
#define CHIP_DMA_REQ_TIM8_TRIG          36
#define CHIP_DMA_REQ_TIM3_CH1           37
#define CHIP_DMA_REQ_TIM3_CH3           38
#define CHIP_DMA_REQ_TIM3_CH4           39
#define CHIP_DMA_REQ_TIM3_UP            40
#define CHIP_DMA_REQ_TIM3_TRIG          41
#if CHIP_DEV_TIM_GP_CNT >= 2
#define CHIP_DMA_REQ_TIM4_CH1           42
#define CHIP_DMA_REQ_TIM4_CH2           43
#define CHIP_DMA_REQ_TIM4_CH3           44
#define CHIP_DMA_REQ_TIM4_UP            45
#endif
#define CHIP_DMA_REQ_TIM6               46


#endif // CHIP_N32G03X_REGS_DMA_H
