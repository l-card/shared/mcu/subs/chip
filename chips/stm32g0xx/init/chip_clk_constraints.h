#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#include "chip_pwr_defs.h"
/* для определения зависимого от настроенного VOS параметра частоты.
 * необходимо определить каждого VOS (N = 1,2) константы CHIP_CLK_<CLKNAME>_VOS<N>_<PARAM>
 * после чего определить CHIP_CLK_<CLKNAME>_<PARAM> = CHIP_CLK_VOS_PARAM(<CLKNAME>, <PARAM>)
 * в результате чего последняя константа будет соответствовать одной из первых четырех в зависимости от VOS */
#define CHIP_CLK_VOS_PARAM__(clkname, param, vosnum)  (CHIP_CLK_## clkname ## _VOS ## vosnum ## _ ## param)
#define CHIP_CLK_VOS_PARAM_(clkname, param, vosnum)   CHIP_CLK_VOS_PARAM__(clkname, param, vosnum)
#define CHIP_CLK_VOS_PARAM(clkname, param)            CHIP_CLK_VOS_PARAM_(clkname, param, CHIP_CFG_PWR_VOS)

#define CHIP_CLK_HSE_ECLK_VOS1_FREQ_MAX     CHIP_MHZ(48)
#define CHIP_CLK_HSE_ECLK_VOS2_FREQ_MAX     CHIP_MHZ(26)
#define CHIP_CLK_HSE_ECLK_FREQ_MAX          CHIP_CLK_VOS_PARAM(HSE_ECLK, FREQ_MAX)

#define CHIP_CLK_HSE_OSC_FREQ_MIN           CHIP_MHZ(4)
#define CHIP_CLK_HSE_OSC_VOS1_FREQ_MAX      CHIP_MHZ(48)
#define CHIP_CLK_HSE_OSC_VOS2_FREQ_MAX      CHIP_MHZ(26)
#define CHIP_CLK_HSE_OSC_FREQ_MAX           CHIP_CLK_VOS_PARAM(HSE_OSC, FREQ_MAX)

/* граничные условия для внешней частоты LSE */
#define CHIP_CLK_LSE_ECLK_FREQ_MAX          CHIP_MHZ(1)     /* максимальня частота LSE в режиме CHIP_CLK_EXTMODE_CLOCK */




#define CHIP_CLK_HCLK_FREQ_MAX              CHIP_MHZ(64)
#define CHIP_CLK_PCLK_FREQ_MAX              CHIP_MHZ(64)


/* пределы входной частоты для PLL */
#define CHIP_CLK_PLL_IN_FREQ_MIN            CHIP_KHZ(2660)
#define CHIP_CLK_PLL_IN_FREQ_MAX            CHIP_MHZ(16)


#define CHIP_CLK_PLL_OUT_P_VOS1_FREQ_MAX    CHIP_MHZ(122)
#define CHIP_CLK_PLL_OUT_P_VOS2_FREQ_MAX    CHIP_MHZ(40)
#define CHIP_CLK_PLL_OUT_P_FREQ_MAX         CHIP_CLK_VOS_PARAM(PLL_OUT_P, FREQ_MAX)
#define CHIP_CLK_PLL_OUT_P_FREQ_MIN         CHIP_KHZ(30900)


#define CHIP_CLK_PLL_OUT_Q_VOS1_FREQ_MAX    CHIP_MHZ(128)
#define CHIP_CLK_PLL_OUT_Q_VOS2_FREQ_MAX    CHIP_MHZ(32)
#define CHIP_CLK_PLL_OUT_Q_FREQ_MAX         CHIP_CLK_VOS_PARAM(PLL_OUT_Q, FREQ_MAX)
#define CHIP_CLK_PLL_OUT_Q_FREQ_MIN         CHIP_MHZ(12)


#define CHIP_CLK_PLL_OUT_R_VOS1_FREQ_MAX    CHIP_MHZ(64)
#define CHIP_CLK_PLL_OUT_R_VOS2_FREQ_MAX    CHIP_MHZ(16)
#define CHIP_CLK_PLL_OUT_R_FREQ_MAX         CHIP_CLK_VOS_PARAM(PLL_OUT_R, FREQ_MAX)
#define CHIP_CLK_PLL_OUT_R_FREQ_MIN         CHIP_MHZ(12)


#define CHIP_CLK_PLL_VCO_VOS1_FREQ_MAX      CHIP_MHZ(344)
#define CHIP_CLK_PLL_VCO_VOS2_FREQ_MAX      CHIP_MHZ(128)
#define CHIP_CLK_PLL_VCO_FREQ_MAX           CHIP_CLK_VOS_PARAM(PLL_VCO, FREQ_MAX)
#define CHIP_CLK_PLL_VCO_FREQ_MIN           CHIP_KHZ(96)



#define CHIP_CLK_ADC_VOS1_FREQ_MAX           CHIP_MHZ(35)
#define CHIP_CLK_ADC_VOS2_FREQ_MAX           CHIP_MHZ(16)

#define CHIP_CLK_SPI_VOS1_FREQ_MAX           CHIP_MHZ(32)
#define CHIP_CLK_SPI_VOS2_FREQ_MAX           CHIP_MHZ(8)




#define CHIP_CLK_PLL_DIVM_MIN       1
#define CHIP_CLK_PLL_DIVM_MAX       8
#define CHIP_CLK_PLL_DIVN_MIN       8
#define CHIP_CLK_PLL_DIVN_MAX       86
#define CHIP_CLK_PLL_DIVP_MIN       2
#define CHIP_CLK_PLL_DIVP_MAX       32
#define CHIP_CLK_PLL_DIVQ_MIN       2
#define CHIP_CLK_PLL_DIVQ_MAX       8
#define CHIP_CLK_PLL_DIVR_MIN       2
#define CHIP_CLK_PLL_DIVR_MAX       8


#endif // CHIP_CLK_CONSTRAINTS_H
