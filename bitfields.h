#ifndef CHIP_BITFIELDS_H_
#define CHIP_BITFIELDS_H_

#include "lbitfield.h"

#define BF_LSB(mask)                LBITFIELD_LSB(mask)
#define BF_SET(mask, val)           LBITFIELD_SET(mask, val)
#define BF_GET(word, mask)          LBITFIELD_GET(word, mask)
#define BF_UPD(word, mask,val)      LBITFIELD_UPD(word,mask, val)

#endif
