#ifndef CHIP_STM32F10X_REGS_PWR_H
#define CHIP_STM32F10X_REGS_PWR_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR;   /* Power control register */
    __IO uint32_t CSR;  /* Power control/status register  */
} CHIP_REGS_PWR_T;

#define CHIP_REGS_PWR                   ((CHIP_REGS_PWR_T *) CHIP_MEMRGN_ADDR_PERIPH_PWR)

/*********** Power control register (PWR_CR) **********************************/
#define CHIP_REGFLD_PWR_CR_LPDS                     (0x00000001UL <<  0U)   /* (RW)   Low-power deepsleep */
#define CHIP_REGFLD_PWR_CR_PDDS                     (0x00000001UL <<  1U)   /* (RW)   Power down deepsleep */
#define CHIP_REGFLD_PWR_CR_CWUF                     (0x00000001UL <<  2U)   /* (RCW1) Clear wakeup flag */
#define CHIP_REGFLD_PWR_CR_CSBF                     (0x00000001UL <<  3U)   /* (RCW1) Clear standby flag */
#define CHIP_REGFLD_PWR_CR_PVDE                     (0x00000001UL <<  4U)   /* (RW)   Programmable voltage detector (PVD) enable.*/
#define CHIP_REGFLD_PWR_CR_PLS                      (0x00000007UL <<  5U)   /* (RW)   PVD level selection */
#define CHIP_REGFLD_PWR_CR_DBP                      (0x00000001UL <<  8U)   /* (RW)   Disable backup domain write protection*/
#define CHIP_REGMSK_PWR_CR_RESERVED                 (0xFFFFFE00UL)

#if defined CHIP_DEV_CH32F1XX || CHIP_DEV_CH32V1XX
/* WCH with 5V power */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_2_65_F_2_5V     0 /* 2.65 rise, 2.5  fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_2_87_F_2_7V     1 /* 2.87 rise, 2.7  fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_3_07_F_2_89V    2 /* 3.07 rise, 2.89 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_3_27_F_3_08V    3 /* 3.27 rise, 3.08 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_3_46_F_3_27V    4 /* 3.46 rise, 3.27 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_3_76_F_3_55V    5 /* 3.76 rise, 3.55 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_4_07_F_3_84V    6 /* 4.07 rise, 3.84 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_4_43_F_4_13V    7 /* 4.43 rise, 4.13 fall */
#elif defined CHIP_DEV_CH32XXXX
/* WCH with 3.3V power */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_2_37_F_2_39V    0 /* 2.37 rise, 2.29 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_2_55_F_2_46V    1 /* 2.55 rise, 2.46 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_2_63_F_2_55V    2 /* 2.63 rise, 2.55 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_2_76_F_2_67V    3 /* 2.76 rise, 2.67 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_2_87_F_2_78V    4 /* 2.87 rise, 2.78 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_3_03_F_2_93V    5 /* 3.03 rise, 2.93 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_3_18_F_3_06V    6 /* 3.18 rise, 3.06 fall */
#define CHIP_REGFLDVAL_PWR_CR_PLS_R_3_29_F_3_19V    7 /* 3.29 rise, 3.19 fall */
#else
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_2V              0
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_3V              1
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_4V              2
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_5V              3
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_6V              4
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_7V              5
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_8V              6
#define CHIP_REGFLDVAL_PWR_CR_PLS_2_9V              7
#endif


/*********** Power control/status register (PWR_CSR) **************************/
#define CHIP_REGFLD_PWR_CSR_WUF                     (0x00000001UL <<  0U)   /* (RO) Wakeup flag */
#define CHIP_REGFLD_PWR_CSR_SBF                     (0x00000001UL <<  1U)   /* (RO) Standby flag */
#define CHIP_REGFLD_PWR_CSR_PVDO                    (0x00000001UL <<  2U)   /* (RO) PVD output */
#define CHIP_REGFLD_PWR_CSR_EWUP                    (0x00000001UL <<  8U)   /* (RO) Enable WKUP pin */
#define CHIP_REGMSK_PWR_CSR_RESERVED                (0xFFFFFEF8UL)



#endif // CHIP_STM32F10X_REGS_PWR_H
