#ifndef CHIP_PWR_INIT_H
#define CHIP_PWR_INIT_H

#include "chip_cfg_defs.h"
#include <stdbool.h>

#if defined CHIP_DEV_CH32F1XX || CHIP_DEV_CH32V1XX
/* WCH with 5V power */
#define CHIP_PWR_PVD_LVL0_VALUE       2500 /* 2.65 rise, 2.5  fall */
#define CHIP_PWR_PVD_LVL1_VALUE       2700 /* 2.87 rise, 2.7  fall */
#define CHIP_PWR_PVD_LVL2_VALUE       2890 /* 3.07 rise, 2.89 fall */
#define CHIP_PWR_PVD_LVL3_VALUE       3080 /* 3.27 rise, 3.08 fall */
#define CHIP_PWR_PVD_LVL4_VALUE       3270 /* 3.46 rise, 3.27 fall */
#define CHIP_PWR_PVD_LVL5_VALUE       3550 /* 3.76 rise, 3.55 fall */
#define CHIP_PWR_PVD_LVL6_VALUE       3840 /* 4.07 rise, 3.84 fall */
#define CHIP_PWR_PVD_LVL7_VALUE       4130 /* 4.43 rise, 4.13 fall */
#elif defined CHIP_DEV_CH32XXXX
/* WCH with 3.3V power */
#define CHIP_PWR_PVD_LVL0_VALUE       2290 /* 2.37 rise, 2.29 fall */
#define CHIP_PWR_PVD_LVL1_VALUE       2460 /* 2.55 rise, 2.46 fall */
#define CHIP_PWR_PVD_LVL2_VALUE       2550 /* 2.63 rise, 2.55 fall */
#define CHIP_PWR_PVD_LVL3_VALUE       2670 /* 2.76 rise, 2.67 fall */
#define CHIP_PWR_PVD_LVL4_VALUE       2780 /* 2.87 rise, 2.78 fall */
#define CHIP_PWR_PVD_LVL5_VALUE       2930 /* 3.03 rise, 2.93 fall */
#define CHIP_PWR_PVD_LVL6_VALUE       3060 /* 3.18 rise, 3.06 fall */
#define CHIP_PWR_PVD_LVL7_VALUE       3190 /* 3.29 rise, 3.19 fall */
#else
/* default */
#define CHIP_PWR_PVD_LVL0_VALUE       2200
#define CHIP_PWR_PVD_LVL1_VALUE       2300
#define CHIP_PWR_PVD_LVL2_VALUE       2400
#define CHIP_PWR_PVD_LVL3_VALUE       2500
#define CHIP_PWR_PVD_LVL4_VALUE       2600
#define CHIP_PWR_PVD_LVL5_VALUE       2700
#define CHIP_PWR_PVD_LVL6_VALUE       2800
#define CHIP_PWR_PVD_LVL7_VALUE       2900
#endif


#if CHIP_PWR_PVD_EN
    #if CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL0_VALUE
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL0_VALUE
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL1_VALUE
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL1_VALUE
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL2_VALUE
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL2_VALUE
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL3_VALUE
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL3_VALUE
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL4_VALUE
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL4_VALUE
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL5_VALUE
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL5_VALUE
    #elif CHIP_PWR_PVD_LVL <= CHIP_PWR_PVD_LVL6_VALUE
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL6_VALUE
    #else
        #define CHIP_PWR_PVD_VALUE    CHIP_PWR_PVD_LVL7_VALUE
    #endif
#endif

bool chip_pwr_pvd_fail(void);
void chip_pwr_bd_cfg_write_enable(void);
void chip_pwr_bd_cfg_write_disable(void);


#endif // CHIP_PWR_INIT_H
