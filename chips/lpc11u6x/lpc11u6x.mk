CHIP_TARGET_ARCHTYPE := cortexm
CHIP_TARGET_CORETYPE ?= cm0p

CHIP_TARGET_DEVTYPE_DIR := $(CHIP_TARGET_DIR)/devs/$(CHIP_TARGET_DEVTYPE)
CHIP_TARGET_DEV_DIR := $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEV)
ifeq (,$(CHIP_TARGET_DEVTYPE))
    $(error CHIP_TARGET_DEVTYPE variable must be set to target cpu device type)
else ifeq (,$(wildcard $(CHIP_TARGET_DEVTYPE_DIR)/chip_devtype_spec_features.h))
    $(error $(CHIP_TARGET_DEVTYPE) is not supported device type)
endif
CHIP_STARTUP_SRC += $(CHIP_TARGET_DIR)/chip_isr_table.c \
                    $(CHIP_TARGET_DIR)/init/chip_clk.c

CHIP_INC_DIRS += $(CHIP_TARGET_DEVTYPE_DIR) ${CHIP_TARGET_DEV_DIR}
