#ifndef CHIP_PIN_H
#define CHIP_PIN_H


typedef uint8_t t_chip_pin_id;

#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4



#define CHIP_PORT_REG_ADDRS(port) (((port) == CHIP_PORT_A) ? 0x19 : \
                                   ((port) == CHIP_PORT_B) ? 0x16 : \
                                   ((port) == CHIP_PORT_C) ? 0x13 : \
                                   ((port) == CHIP_PORT_D) ? 0x10 : \
                                   ((port) == CHIP_PORT_E) ? 0x05 : 0)

#define CHIP_PORT_PINREG(port)  (_SFR_IO8(CHIP_PORT_REG_ADDRS(port)))
#define CHIP_PORT_DDRREG(port)  (_SFR_IO8(CHIP_PORT_REG_ADDRS(port) + 1))
#define CHIP_PORT_PORTREG(port) (_SFR_IO8(CHIP_PORT_REG_ADDRS(port) + 2))


#define CHIP_PIN_ID(port, pin, func)  ((((port) & 0x7) << 5) | (((pin) & 0x7) << 2) | ((func) & 3))

#define CHIP_PIN_ID_PORT(id) (((id) >> 5) & 0x7)
#define CHIP_PIN_ID_PIN(id)  (((id) >> 2) & 0x7)
#define CHIP_PIN_ID_FUNC(id) (((id) >> 0) & 0x3)


#define CHIP_PIN_SET(id) CHIP_PORT_PORTREG(CHIP_PIN_ID_PORT(id)) |= (1 << CHIP_PIN_ID_PIN(id))
#define CHIP_PIN_CLR(id) CHIP_PORT_PORTREG(CHIP_PIN_ID_PORT(id)) &= ~(1 << CHIP_PIN_ID_PIN(id))

/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val)      do { \
        if (val) { \
            CHIP_PIN_SET(id); \
        } else { \
            CHIP_PIN_CLR(id); \
        } \
    } while(0)
/* чтение уровня на ножке (0 или 1) */
#define CHIP_PIN_IN(id)           (!!(CHIP_PORT_PINREG(CHIP_PIN_ID_PORT(id)) & (1 << CHIP_PIN_ID_PIN(id))))
/* состояние регистра выхода (независимо от физического уровня) */
#define CHIP_PIN_GET_OUT_VAL(id)  (!!(CHIP_PORT_PORTREG(CHIP_PIN_ID_PORT(id)) & (1 << CHIP_PIN_ID_PIN(id))))

#define CHIP_PIN_TOGGLE(id)  (CHIP_PORT_PORTREG(CHIP_PIN_ID_PORT(id)) ^= (1 << CHIP_PIN_ID_PIN(id)))



#define CHIP_PIN_SET_DIR_IN(id)    (CHIP_PORT_DDRREG(CHIP_PIN_ID_PORT(id)) &= ~(1 << CHIP_PIN_ID_PIN(id)))
#define CHIP_PIN_SET_DIR_OUT(id)   (CHIP_PORT_DDRREG(CHIP_PIN_ID_PORT(id)) |= (1 << CHIP_PIN_ID_PIN(id)))




#define CHIP_PIN_CONFIG_OUT(id, val) do { \
        CHIP_PIN_OUT(id, val); \
        CHIP_PIN_SET_DIR_OUT(id); \
    } while(0)

#define CHIP_PIN_CONFIG_IN_PU(id, pu) do { \
        CHIP_PIN_SET_DIR_IN(id); \
        CHIP_PIN_OUT(id, pu); \
    } while(0)

#define CHIP_PIN_CONFIG_IN(id) CHIP_PIN_CONFIG_IN_PU(id, 0)



/* ------------------------ Порт PA ---------------------*/
#define CHIP_PIN_PA0_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 0, 0)
#define CHIP_PIN_PA0_AD0     CHIP_PIN_ID(CHIP_PORT_A, 0, 1) /* External memory interface address and data bit 0 */

#define CHIP_PIN_PA1_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 1, 0)
#define CHIP_PIN_PA1_AD1     CHIP_PIN_ID(CHIP_PORT_A, 1, 1) /* External memory interface address and data bit 1 */

#define CHIP_PIN_PA2_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 2, 0)
#define CHIP_PIN_PA2_AD2     CHIP_PIN_ID(CHIP_PORT_A, 2, 1) /* External memory interface address and data bit 2 */

#define CHIP_PIN_PA3_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 3, 0)
#define CHIP_PIN_PA3_AD3     CHIP_PIN_ID(CHIP_PORT_A, 3, 1) /* External memory interface address and data bit 3 */

#define CHIP_PIN_PA4_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 4, 0)
#define CHIP_PIN_PA4_AD4     CHIP_PIN_ID(CHIP_PORT_A, 4, 1) /* External memory interface address and data bit 4 */

#define CHIP_PIN_PA5_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 5, 0)
#define CHIP_PIN_PA5_AD5     CHIP_PIN_ID(CHIP_PORT_A, 5, 1) /* External memory interface address and data bit 5 */

#define CHIP_PIN_PA6_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 6, 0)
#define CHIP_PIN_PA6_AD6     CHIP_PIN_ID(CHIP_PORT_A, 6, 1) /* External memory interface address and data bit 6 */

#define CHIP_PIN_PA7_GPIO    CHIP_PIN_ID(CHIP_PORT_A, 7, 0)
#define CHIP_PIN_PA7_AD7     CHIP_PIN_ID(CHIP_PORT_A, 7, 1) /* External memory interface address and data bit 7 */


/* ------------------------ Порт PB ---------------------*/
#define CHIP_PIN_PB0_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 0, 0)
#define CHIP_PIN_PB0_T0      CHIP_PIN_ID(CHIP_PORT_B, 0, 1) /* Timer/Counter0 External Counter Input */
#define CHIP_PIN_PB0_OC0     CHIP_PIN_ID(CHIP_PORT_B, 0, 2) /* Timer/Counter0 Output Compare Match Output */

#define CHIP_PIN_PB1_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 1, 0)
#define CHIP_PIN_PB1_T1      CHIP_PIN_ID(CHIP_PORT_B, 1, 1) /* Timer/Counter1 External Counter Input */

#define CHIP_PIN_PB2_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 2, 0)
#define CHIP_PIN_PB2_AIN0    CHIP_PIN_ID(CHIP_PORT_B, 2, 1) /* Analog Comparator Positive Input */

#define CHIP_PIN_PB3_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 3, 0)
#define CHIP_PIN_PB3_AIN1    CHIP_PIN_ID(CHIP_PORT_B, 3, 1) /* Analog Comparator Negative Input */

#define CHIP_PIN_PB4_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 4, 0)
#define CHIP_PIN_PB4_SS      CHIP_PIN_ID(CHIP_PORT_B, 4, 1) /* SPI Slave Select Input */

#define CHIP_PIN_PB5_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 5, 0)
#define CHIP_PIN_PB5_MOSI    CHIP_PIN_ID(CHIP_PORT_B, 5, 1) /* SPI Bus Master Output/Slave Input */

#define CHIP_PIN_PB6_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 6, 0)
#define CHIP_PIN_PB6_MISO    CHIP_PIN_ID(CHIP_PORT_B, 6, 1) /* SPI Bus Master Input/Slave Output */

#define CHIP_PIN_PB7_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 7, 0)
#define CHIP_PIN_PB7_SCK     CHIP_PIN_ID(CHIP_PORT_B, 7, 1) /* SPI Bus Serial Clock */

/* ------------------------ Порт PC ---------------------*/
#define CHIP_PIN_PC0_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 0, 0)
#define CHIP_PIN_PC0_A8      CHIP_PIN_ID(CHIP_PORT_C, 0, 1)  /* External memory interface address bit 8 */

#define CHIP_PIN_PC1_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 1, 0)
#define CHIP_PIN_PC1_A9      CHIP_PIN_ID(CHIP_PORT_C, 1, 1)  /* External memory interface address bit 9 */

#define CHIP_PIN_PC2_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 2, 0)
#define CHIP_PIN_PC2_A10     CHIP_PIN_ID(CHIP_PORT_C, 2, 1)  /* External memory interface address bit 10 */

#define CHIP_PIN_PC3_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 3, 0)
#define CHIP_PIN_PC3_A11     CHIP_PIN_ID(CHIP_PORT_C, 3, 1)  /* External memory interface address bit 11 */

#define CHIP_PIN_PC4_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 4, 0)
#define CHIP_PIN_PC4_A12     CHIP_PIN_ID(CHIP_PORT_C, 4, 1)  /* External memory interface address bit 12 */

#define CHIP_PIN_PC5_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 5, 0)
#define CHIP_PIN_PC5_A13     CHIP_PIN_ID(CHIP_PORT_C, 5, 1)  /* External memory interface address bit 13 */

#define CHIP_PIN_PC6_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 6, 0)
#define CHIP_PIN_PC6_A14     CHIP_PIN_ID(CHIP_PORT_C, 6, 1)  /* External memory interface address bit 14 */

#define CHIP_PIN_PC7_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 7, 0)
#define CHIP_PIN_PC7_A15     CHIP_PIN_ID(CHIP_PORT_C, 7, 1)  /* External memory interface address bit 15 */


/* ------------------------ Порт PD ---------------------*/
#define CHIP_PIN_PD0_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 0, 0)
#define CHIP_PIN_PD0_RXD     CHIP_PIN_ID(CHIP_PORT_D, 0, 1) /* USART Input Pin */

#define CHIP_PIN_PD1_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 1, 0)
#define CHIP_PIN_PD1_TXD     CHIP_PIN_ID(CHIP_PORT_D, 1, 1) /* USART Output Pin */

#define CHIP_PIN_PD2_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 2, 0)
#define CHIP_PIN_PD2_INT0    CHIP_PIN_ID(CHIP_PORT_D, 2, 1) /* External Interrupt 0 Input */

#define CHIP_PIN_PD3_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 3, 0)
#define CHIP_PIN_PD3_INT1    CHIP_PIN_ID(CHIP_PORT_D, 3, 1) /* External Interrupt 1 Input */

#define CHIP_PIN_PD4_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 4, 0)
#define CHIP_PIN_PD4_XCK     CHIP_PIN_ID(CHIP_PORT_D, 4, 1) /* USART External Clock Input/Output */

#define CHIP_PIN_PD5_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 5, 0)
#define CHIP_PIN_PD5_OC1A    CHIP_PIN_ID(CHIP_PORT_D, 5, 1) /* Timer/Counter1 Output Compare A Match Output */

#define CHIP_PIN_PD6_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 6, 0)
#define CHIP_PIN_PD6_WR      CHIP_PIN_ID(CHIP_PORT_D, 6, 1) /* Write Strobe to External Memory */

#define CHIP_PIN_PD7_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 7, 0)
#define CHIP_PIN_PD7_RD      CHIP_PIN_ID(CHIP_PORT_D, 7, 1) /* Read Strobe to External Memory */

/* ------------------------ Порт PE ---------------------*/
#define CHIP_PIN_PE0_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 0, 0)
#define CHIP_PIN_PE0_INT2    CHIP_PIN_ID(CHIP_PORT_E, 0, 1) /* External Interrupt 2 Input */
#define CHIP_PIN_PE0_ICP     CHIP_PIN_ID(CHIP_PORT_E, 0, 2) /* Timer/Counter1 Input Capture Pin */

#define CHIP_PIN_PE1_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 1, 0)
#define CHIP_PIN_PE1_ALE     CHIP_PIN_ID(CHIP_PORT_E, 1, 1) /* Address Latch Enable to External Memory */

#define CHIP_PIN_PE2_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 2, 0)
#define CHIP_PIN_PE2_OC1B    CHIP_PIN_ID(CHIP_PORT_E, 2, 1) /* Timer/Counter1 Output Compare B Match Output */

#endif // CHIP_PIN_H

