#include "chip.h"
#if CHIP_PININT_ISR_PROC_EN

#define CHIP_PININT_IRQ_BITMSK(intnum)  CHIP_EXTI_EVTIN_BITMSK(CHIP_EXTI_EVTIN_EXTI(intnum))

static t_chip_pinint_isr f_isr_cbs[CHIP_PININT_CNT];
static uint16_t f_isr9_5_en_msk;
static uint16_t f_isr15_10_en_msk;



static inline void f_extint_isr_single(uint8_t intnum) {
    CHIP_EXTI_EVTIN_PEND_CLR(CHIP_EXTI_EVTIN_EXTI(intnum));
    if (f_isr_cbs[intnum]) {
        f_isr_cbs[intnum]();
    }
}

static inline void f_extint_isr_shared(uint8_t from, uint8_t to) {
    const uint32_t msk = ((1 << (from - to + 1)) - 1) << from;
    const uint32_t pendreg = CHIP_REGS_EXTI->CORE[CHIP_EXTI_CUR_CORE_NUM][0].PR & msk;
    CHIP_REGS_EXTI->CORE[CHIP_EXTI_CUR_CORE_NUM][0].PR = pendreg;

    for (uint8_t intnum = from; intnum < to; ++intnum) {
        if (pendreg & CHIP_PININT_IRQ_BITMSK(intnum)) {
            if (f_isr_cbs[intnum]) {
                f_isr_cbs[intnum]();
            }
        }
    }
}


void EXTI0_IRQHandler(void) {
    f_extint_isr_single(0);
}

void EXTI1_IRQHandler(void) {
    f_extint_isr_single(1);
}

void EXTI2_IRQHandler(void) {
    f_extint_isr_single(2);
}

void EXTI3_IRQHandler(void) {
    f_extint_isr_single(3);
}

void EXTI4_IRQHandler(void) {
    f_extint_isr_single(4);
}

void EXTI9_5_IRQHandler(void) {
    f_extint_isr_shared(5, 9);
}

void EXTI15_10_IRQHandler(void) {
    f_extint_isr_shared(10, 15);
}


void chip_pinint_nvic_set_isr(uint8_t intnum, t_chip_pinint_isr isr) {
    f_isr_cbs[intnum] = isr;
}

void chip_pinint_nvic_set_irq_en(uint8_t intnum, IRQn_Type irqnum, bool en) {
    bool changed = false;
    if (irqnum == EXTI9_5_IRQn) {
        const bool was_en = f_isr9_5_en_msk != 0;
        if (en) {
            f_isr9_5_en_msk |= CHIP_PININT_IRQ_BITMSK(intnum);
        } else {
            f_isr9_5_en_msk &= ~CHIP_PININT_IRQ_BITMSK(intnum);
        }
        changed = was_en != (f_isr9_5_en_msk != 0);
    } else if (irqnum == EXTI15_10_IRQn) {
        const bool was_en = f_isr15_10_en_msk != 0;
        if (en) {
            f_isr15_10_en_msk |= CHIP_PININT_IRQ_BITMSK(intnum);
        } else {
            f_isr15_10_en_msk &= ~CHIP_PININT_IRQ_BITMSK(intnum);
        }
        changed = was_en != (f_isr15_10_en_msk != 0);
    } else {
        changed = true;
    }

    if (changed) {
        if (en) {
            chip_interrupt_enable(irqnum);
        } else {
            chip_interrupt_disable(irqnum);
        }
    }
}
#endif
