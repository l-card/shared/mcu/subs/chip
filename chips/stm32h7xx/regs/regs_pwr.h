#ifndef CHIP_STM32H7XX_REGS_PWR_H
#define CHIP_STM32H7XX_REGS_PWR_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR1;       /* PWR power control register 1,            Address offset: 0x00 */
    __IO uint32_t CSR1;      /* PWR power control status register 1,     Address offset: 0x04 */
    __IO uint32_t CR2;       /* PWR power control register 2,            Address offset: 0x08 */
    __IO uint32_t CR3;       /* PWR power control register 3,            Address offset: 0x0C */
    __IO uint32_t CPUCR;     /* PWR CPU control register,                Address offset: 0x10 */
#if CHIP_DEV_SUPPORT_CORE_CM4
    __IO uint32_t CPU2CR;    /* PWR CPU2 control register,               Address offset: 0x14 */
#else
    uint32_t RESERVED0;      /* Reserved,                                Address offset: 0x14 */
#endif
    __IO uint32_t D3CR;      /* PWR D3 domain control register,          Address offset: 0x18 */
    uint32_t RESERVED1;      /* Reserved,                                Address offset: 0x1C */
    __IO uint32_t WKUPCR;    /* PWR wakeup clear register,               Address offset: 0x20 */
    __IO uint32_t WKUPFR;    /* PWR wakeup flag register,                Address offset: 0x24 */
    __IO uint32_t WKUPEPR;   /* PWR wakeup enable and polarity register, Address offset: 0x28 */
} CHIP_REGS_PWR_T;

#define CHIP_REGS_PWR          ((CHIP_REGS_PWR_T *) CHIP_MEMRGN_ADDR_PERIPH_PWR)

/*********** PWR control register 1 (PWR_CR1)  ********************************/
#define CHIP_REGFLD_PWR_CR1_LPDS                (0x00000001UL <<  0U)  /* Low Power Deepsleep with SVOS3 */
#define CHIP_REGFLD_PWR_CR1_PVDEN               (0x00000001UL <<  4U)  /* Programmable Voltage detector enable. */
#define CHIP_REGFLD_PWR_CR1_PLS                 (0x00000007UL <<  5U)  /* Programmable Voltage Detector level selection */
#define CHIP_REGFLD_PWR_CR1_DBP                 (0x00000001UL <<  8U)  /* Disable Back-up domain Protection */
#define CHIP_REGFLD_PWR_CR1_FLPS                (0x00000001UL <<  9U)  /* Flash low power mode in DSTOP */
#define CHIP_REGFLD_PWR_CR1_SVOS                (0x00000003UL << 14U)  /* System STOP mode Voltage Scaling selection. */
#define CHIP_REGFLD_PWR_CR1_AVDEN               (0x00000001UL << 16U)  /* Analog Voltage Detector Enable */
#define CHIP_REGFLD_PWR_CR1_ALS                 (0x00000003UL << 17U)  /* Analog Voltage Detector level selection */

#define CHIP_REGFLDVAL_PWR_CR1_ALS_1_7V         0
#define CHIP_REGFLDVAL_PWR_CR1_ALS_2_1V         1
#define CHIP_REGFLDVAL_PWR_CR1_ALS_2_5V         2
#define CHIP_REGFLDVAL_PWR_CR1_ALS_2_8V         3

#define CHIP_REGFLDVAL_PWR_CR1_SVOS_5           1
#define CHIP_REGFLDVAL_PWR_CR1_SVOS_4           2
#define CHIP_REGFLDVAL_PWR_CR1_SVOS_3           3

#define CHIP_REGFLDVAL_PWR_CR1_PLS_1_95V        0
#define CHIP_REGFLDVAL_PWR_CR1_PLS_2_1V         1
#define CHIP_REGFLDVAL_PWR_CR1_PLS_2_25V        2
#define CHIP_REGFLDVAL_PWR_CR1_PLS_2_4V         3
#define CHIP_REGFLDVAL_PWR_CR1_PLS_2_55V        4
#define CHIP_REGFLDVAL_PWR_CR1_PLS_2_7V         5
#define CHIP_REGFLDVAL_PWR_CR1_PLS_2_85V        6
#define CHIP_REGFLDVAL_PWR_CR1_PLS_EXTPIN       7

/*********** PWR control status register 1 (PWR_CSR1)  ************************/
#define CHIP_REGFLD_PWR_CSR1_PVDO                  (0x00000001UL <<  4U) /* Programmable Voltage Detect Output */
#define CHIP_REGFLD_PWR_CSR1_ACTVOSRDY             (0x00000001UL << 13U) /* Ready bit for current actual used VOS for VDD11 Voltage Scaling  */
#define CHIP_REGFLD_PWR_CSR1_ACTVOS                (0x00000003UL << 14U) /* Current actual used VOS for VDD11 Voltage Scaling */
#define CHIP_REGFLD_PWR_CSR1_AVDO                  (0x00000001UL << 16U) /* Analog Voltage Detect Output */

/*********** PWR control register 2 (PWR_CR2) *********************************/
#define CHIP_REGFLD_PWR_CR2_BREN                   (0x00000001UL <<  0U) /* Backup regulator enable */
#define CHIP_REGFLD_PWR_CR2_MONEN                  (0x00000001UL <<  4U) /* VBAT and temperature monitoring enable */
#define CHIP_REGFLD_PWR_CR2_BRRDY                  (0x00000001UL << 16U) /* Backup regulator ready */
#define CHIP_REGFLD_PWR_CR2_VBATL                  (0x00000001UL << 20U) /* Monitored VBAT level above low threshold */
#define CHIP_REGFLD_PWR_CR2_VBATH                  (0x00000001UL << 21U) /* Monitored VBAT level above high threshold */
#define CHIP_REGFLD_PWR_CR2_TEMPL                  (0x00000001UL << 22U) /* Monitored temperature level above low threshold */
#define CHIP_REGFLD_PWR_CR2_TEMPH                  (0x00000001UL << 23U) /* Monitored temperature level above high threshold */

/*********** PWR control register 3 (PWR_CR3) *********************************/
#define CHIP_REGFLD_PWR_CR3_BYPASS                 (0x00000001UL <<  0U) /* Power Management Unit bypass */
#define CHIP_REGFLD_PWR_CR3_LDOEN                  (0x00000001UL <<  1U) /* Low Drop Output regulator enable */
#if CHIP_DEV_SUPPORT_SMPS
#define CHIP_REGFLD_PWR_CR3_SDEN                   (0x00000001UL <<  2U) /* SMPS step-down converter enable */
#define CHIP_REGFLD_PWR_CR3_SDEXTHP                (0x00000001UL <<  3U) /* SMPS step-down converter forced ON and in High Power MR mode */
#define CHIP_REGFLD_PWR_CR3_SDLEVEL                (0x00000003UL <<  4U) /* SMPS step-down converter voltage output level selection */
#else
#define CHIP_REGFLD_PWR_CR3_SCUEN                  (0x00000001UL <<  2U) /* Supply configuration update enable */
#endif
#define CHIP_REGFLD_PWR_CR3_VBE                    (0x00000001UL <<  8U) /* VBAT charging enable */
#define CHIP_REGFLD_PWR_CR3_VBRS                   (0x00000001UL <<  9U) /* VBAT charging resistor selection */
#if CHIP_DEV_SUPPORT_SMPS
#define CHIP_REGFLD_PWR_CR3_SDEXTRDY               (0x00000001UL << 16U) /* SMPS step-down converter external supply ready */
#endif
#define CHIP_REGFLD_PWR_CR3_USB33DEN               (0x00000001UL << 24U) /* VDD33_USB voltage level detector enable */
#define CHIP_REGFLD_PWR_CR3_USBREGEN               (0x00000001UL << 25U) /* USB regulator enable */
#define CHIP_REGFLD_PWR_CR3_USB33RDY               (0x00000001UL << 26U) /* USB supply ready */

#if CHIP_DEV_SUPPORT_SMPS
#define CHIP_REGFLDVAL_PWR_CR3_SDLEVEL_RST         0
#define CHIP_REGFLDVAL_PWR_CR3_SDLEVEL_1_8V        1
#define CHIP_REGFLDVAL_PWR_CR3_SDLEVEL_2_5V        2
#endif

/*****************  PWR CPU1 control register (PWR_CPU1CR)  *******************/
#define CHIP_REGFLD_PWR_CPUCR_PDDS_D1              (0x00000001UL <<  0U) /* D1 domain Power Down Deepsleep selection */
#define CHIP_REGFLD_PWR_CPUCR_PDDS_D2              (0x00000001UL <<  1U) /* D2 domain Power Down Deepsleep */
#define CHIP_REGFLD_PWR_CPUCR_PDDS_D3              (0x00000001UL <<  2U) /* System D3 domain Power Down Deepsleep */
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_PWR_CPUCR_HOLD2F               (0x00000001UL <<  4U) /* CPU2 on hold wakeup flag */
#endif
#define CHIP_REGFLD_PWR_CPUCR_STOPF                (0x00000001UL <<  5U) /* STOP Flag */
#define CHIP_REGFLD_PWR_CPUCR_SBF                  (0x00000001UL <<  6U) /* System STANDBY Flag */
#define CHIP_REGFLD_PWR_CPUCR_SBF_D1               (0x00000001UL <<  7U) /* D1 domain DSTANDBY Flag */
#define CHIP_REGFLD_PWR_CPUCR_SBF_D2               (0x00000001UL <<  8U) /* D2 domain DSTANDBY Flag */
#define CHIP_REGFLD_PWR_CPUCR_CSSF                 (0x00000001UL <<  9U) /* Clear D1 domain CPU1 STANDBY, STOP and HOLD flags */
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_PWR_CPUCR_HOLD2                (0x00000001UL << 10U) /* Hold the CPU2 and allocated peripherals when exiting from Stop mode. */
#endif
#define CHIP_REGFLD_PWR_CPUCR_RUN_D3               (0x00000001UL << 11U) /* Keep system D3 domain in RUN mode regardless of the CPU sub-systems modes */

#if CHIP_DEV_SUPPORT_CORE_CM4
/*****************  PWR CPU2 control register (PWR_CPU2CR) *******************/
#define CHIP_REGFLD_PWR_CPU2CR_PDDS_D1             (0x00000001UL <<  0U) /* D1 domain Power Down Deepsleep selection */
#define CHIP_REGFLD_PWR_CPU2CR_PDDS_D2             (0x00000001UL <<  1U) /* D2 domain Power Down Deepsleep */
#define CHIP_REGFLD_PWR_CPU2CR_PDDS_D3             (0x00000001UL <<  2U) /* System D3 domain Power Down Deepsleep */
#define CHIP_REGFLD_PWR_CPU2CR_HOLD1F              (0x00000001UL <<  4U) /* CPU1 on hold wakeup flag */
#define CHIP_REGFLD_PWR_CPU2CR_STOPF               (0x00000001UL <<  5U) /* STOP Flag */
#define CHIP_REGFLD_PWR_CPU2CR_SBF                 (0x00000001UL <<  6U) /* System STANDBY Flag */
#define CHIP_REGFLD_PWR_CPU2CR_SBF_D1              (0x00000001UL <<  7U) /* D1 domain DSTANDBY Flag */
#define CHIP_REGFLD_PWR_CPU2CR_SBF_D2              (0x00000001UL <<  8U) /* D2 domain DSTANDBY Flag */
#define CHIP_REGFLD_PWR_CPU2CR_CSSF                (0x00000001UL <<  9U) /* Clear D1 domain CPU1 STANDBY, STOP and HOLD flags */
#define CHIP_REGFLD_PWR_CPU2CR_HOLD1               (0x00000001UL << 10U) /* Hold the CPU1 and allocated peripherals when exiting from Stop mode */
#define CHIP_REGFLD_PWR_CPU2CR_RUN_D3              (0x00000001UL << 11U) /* Keep D3 domain in Run mode regardless of the other CPU subsystems modes. */
#endif

/********************  PWR D3 domain control register (PWR_D3CR)  *************/
#define CHIP_REGFLD_PWR_D3CR_VOSRDY                (0x00000001UL << 13U) /* VOS Ready bit for VDD11 Voltage Scaling output selection */
#define CHIP_REGFLD_PWR_D3CR_VOS                   (0x00000003UL << 14U) /* Voltage Scaling selection according performance */

#define CHIP_REGFLDVAL_PWR_D3CR_VOS_3               1
#define CHIP_REGFLDVAL_PWR_D3CR_VOS_2               2
#define CHIP_REGFLDVAL_PWR_D3CR_VOS_1               3

/********************  PWR wakeup clear register (PWR_WKUPCR) *****************/
#define CHIP_REGFLD_PWR_WKUPCR_WKUPC1              (0x00000001UL <<  (0U + (n))) /* Clear Wakeup Pin Flag n (n=0-5) */

/********************  PWR wakeup flag register (PWR_WKUPFR)*******************/
#define CHIP_REGFLD_PWR_WKUPFR_WKUPF(n)            (0x00000001UL <<  (0U + (n))) /* Wakeup Pin Flag n (n=0-5) */

/*******  PWR wakeup enable and polarity register (PWR_WKUPEPR)  **************/
#define CHIP_REGFLD_PWR_WKUPEPR_WKUPEN(n)          (0x00000001UL <<  (0U + (n)))  /* Enable Wakeup Pin WKUPn (n=0-5) */
#define CHIP_REGFLD_PWR_WKUPEPR_WKUPP(n)           (0x00000001UL <<  (8U + (n)))  /* Wakeup Pin Polarity for WKUPn (n=0-5) */
#define CHIP_REGFLD_PWR_WKUPEPR_WKUPPUPD(n)        (0x00000003UL <<  (16U + 2*(n)) /* Wakeup Pin pull configuration for WKUPn (n=0-5) */

#define CHIP_REGFLDVAL_PWR_WKUPEPR_WKUPPUPD_NO     0
#define CHIP_REGFLDVAL_PWR_WKUPEPR_WKUPPUPD_PU     1
#define CHIP_REGFLDVAL_PWR_WKUPEPR_WKUPPUPD_PD     2

#endif // CHIP_STM32H7XX_REGS_PWR_H
