#ifndef CHIP_DEV_SPEC_FEATURES_H
#define CHIP_DEV_SPEC_FEATURES_H

#include "chip_pkg_types.h"


#define CHIP_DEV_CH32F2_V30X_D8
#define CHIP_DEV_CH32F20X_D8


#define CHIP_DEV_PKG                        CHIP_PKG_LQFP48
#define CHIP_FLASH_MEM_SIZE                 (128*1024)
#define CHIP_RAM_MEM_SIZE                   ( 32*1024)

#define CHIP_DEV_SUPPORT_EXMC           0
#define CHIP_DEV_SUPPORT_TIM9           0
#define CHIP_DEV_SUPPORT_TIM10          0
#define CHIP_DEV_SUPPORT_TIM11          0
#define CHIP_DEV_SUPPORT_TIM12          0
#define CHIP_DEV_SUPPORT_TIM13          0
#define CHIP_DEV_SUPPORT_TIM14          0
#define CHIP_DEV_SUPPORT_TRACE          0
#define CHIP_DEV_SUPPORT_ETH_RGMII      0
#define CHIP_DEV_SUPPORT_RNG            0

#define CHIP_DEV_TIM_GP_CNT             3
#define CHIP_DEV_TIM_GP32_CNT           0
#define CHIP_DEV_TIM_ADV_CNT            1
#define CHIP_DEV_TIM_BASIC_CNT          0
#define CHIP_DEV_SPI_CNT                2
#define CHIP_DEV_I2S_CNT                0
#define CHIP_DEV_I2S_FD_CNT             0
#define CHIP_DEV_I2C_CNT                2
#define CHIP_DEV_UART_CNT               3
#define CHIP_DEV_CAN_CNT                1
#define CHIP_DEV_USB_CNT                1
#define CHIP_DEV_USBHS_CNT              0
#define CHIP_DEV_SDIO_CNT               0
#define CHIP_DEV_ETH_CNT                0
#define CHIP_DEV_ADC_CNT                2
#define CHIP_DEV_DAC_CNT                2
#define CHIP_DEV_SPIM_CNT               0
#define CHIP_DEV_QSPI_CNT               0
#define CHIP_DEV_DVP_CNT                0
#define CHIP_DEV_OPA_CNT                4
#define CHIP_DEV_BLES_CNT               0


#define CHIP_DEV_PLL_CNT                1 /* 3 - CH32F20x_D8C and CH32V30x_D8C */

#endif
