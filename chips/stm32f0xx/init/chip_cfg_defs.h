#ifndef CHIP_CFG_DEFS_H
#define CHIP_CFG_DEFS_H

#include "chip_config.h"

#ifdef CHIP_CFG_CLK_HSE_MODE
    #define CHIP_CLK_HSE_MODE   CHIP_CFG_CLK_HSE_MODE
#else
    #define CHIP_CLK_HSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_LSE_MODE
    #define CHIP_CLK_LSE_MODE   CHIP_CFG_CLK_LSE_MODE
#else
    #define CHIP_CLK_LSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_HSI_EN
    #define CHIP_CLK_HSI_EN     CHIP_CFG_CLK_HSI_EN
#else
    #define CHIP_CLK_HSI_EN     1
#endif

#ifdef CHIP_CFG_CLK_HSI48_EN
    #define CHIP_CLK_HSI48_EN   CHIP_CFG_CLK_HSI48_EN
#else
    #define CHIP_CLK_HSI48_EN   0
#endif

#ifdef CHIP_CFG_CLK_HSI14_EN
    #define CHIP_CLK_HSI14_EN   CHIP_CFG_CLK_HSI14_EN
#else
    #define CHIP_CLK_HSI14_EN   0
#endif


#ifdef CHIP_CFG_CLK_CRS_SETUP_EN
    #define CHIP_CLK_CRS_SETUP_EN CHIP_CFG_CLK_CRS_SETUP_EN
#else
    #define CHIP_CLK_CRS_SETUP_EN 0
#endif

#ifdef CHIP_CFG_WDT_SETUP_EN
    #define CHIP_WDT_SETUP_EN CHIP_CFG_WDT_SETUP_EN
#else
    #define CHIP_WDT_SETUP_EN 0
#endif
#if CHIP_WDT_SETUP_EN
    #define CHIP_CLK_LSI_EN 1 /* LSI должен быть включен при использовании IWDT */
#elif defined CHIP_CFG_CLK_LSI_EN
    #define CHIP_CLK_LSI_EN CHIP_CFG_CLK_LSI_EN
#else
    #define CHIP_CLK_LSI_EN 0
#endif


#ifdef CHIP_CFG_CLK_PLL_EN
    #define CHIP_CLK_PLL_EN CHIP_CFG_CLK_PLL_EN
    #ifdef CHIP_CFG_CLK_PLL_PREDIV
        #define CHIP_CLK_PLL_PREDIV CHIP_CFG_CLK_PLL_PREDIV
    #else
        #define CHIP_CLK_PLL_PREDIV 1
    #endif

    #ifndef CHIP_CFG_CLK_PLL_MUL
        #error "PLL multiplier is not specified"
    #else
        #define CHIP_CLK_PLL_MUL CHIP_CFG_CLK_PLL_MUL
    #endif

    #ifdef CHIP_CFG_CLK_PLL_SRC
        #define CHIP_CLK_PLL_SRC CHIP_CFG_CLK_PLL_SRC
    #else
        #define CHIP_CLK_PLL_SRC CHIP_CLK_HSI
    #endif
#else
    #define CHIP_CLK_PLL_EN     0
    #define CHIP_CLK_PLL_PREDIV 0
    #define CHIP_CLK_PLL_MUL    0
#endif


#ifdef CHIP_CFG_CLK_SYS_SRC
    #define CHIP_CLK_SYS_SRC CHIP_CFG_CLK_SYS_SRC
#else
    #define CHIP_CLK_SYS_SRC CHIP_CLK_HSI
#endif



#ifdef CHIP_CFG_CLK_AHB_DIV
    #define CHIP_CLK_AHB_DIV    CHIP_CFG_CLK_AHB_DIV
#else
    #define CHIP_CLK_AHB_DIV    1
#endif

#ifdef CHIP_CFG_CLK_APB_DIV
    #define CHIP_CLK_APB_DIV    CHIP_CFG_CLK_APB_DIV
#else
    #define CHIP_CLK_APB_DIV    1
#endif

#ifdef CHIP_CFG_CLK_I2C1_SRC
    #define CHIP_CLK_I2C1_SRC CHIP_CFG_CLK_I2C1_SRC
#else
    #define CHIP_CLK_I2C1_SRC CHIP_CLK_HSI
#endif

#ifdef CHIP_CFG_CLK_USART1_SRC
    #define CHIP_CLK_USART1_SRC CHIP_CFG_CLK_USART1_SRC
#else
    #define CHIP_CLK_USART1_SRC CHIP_CLK_APB
#endif

#ifdef CHIP_CFG_CLK_USART2_SRC
    #define CHIP_CLK_USART2_SRC CHIP_CFG_CLK_USART2_SRC
#else
    #define CHIP_CLK_USART2_SRC CHIP_CLK_APB
#endif

#ifdef CHIP_CFG_CLK_USART3_SRC
    #define CHIP_CLK_USART3_SRC CHIP_CFG_CLK_USART3_SRC
#else
    #define CHIP_CLK_USART3_SRC CHIP_CLK_APB
#endif

#ifdef CHIP_CFG_CLK_CEC_SRC
    #define CHIP_CLK_CEC_SRC    CHIP_CFG_CLK_CEC_SRC
#else
    #define CHIP_CLK_CEC_SRC    CHIP_CLK_HSI_DIV244
#endif

#ifdef CHIP_CFG_CLK_USB_SRC
    #define CHIP_CLK_USB_SRC    CHIP_CFG_CLK_USB_SRC
#else
    #define CHIP_CLK_USB_SRC    CHIP_CLK_HSI48
#endif



#ifdef CHIP_CFG_CLK_DMA1_EN
    #define CHIP_CLK_DMA1_EN    CHIP_CFG_CLK_DMA1_EN
#else
    #define CHIP_CLK_DMA1_EN    0
#endif
#ifdef CHIP_CFG_CLK_DMA2_EN
    #define CHIP_CLK_DMA2_EN    CHIP_CFG_CLK_DMA2_EN
#else
    #define CHIP_CLK_DMA2_EN    0
#endif
#ifdef CHIP_CFG_CLK_SRAM_EN
    #define CHIP_CLK_SRAM_EN    CHIP_CFG_CLK_SRAM_EN
#else
    #define CHIP_CLK_SRAM_EN    1
#endif
#ifdef CHIP_CFG_CLK_FLASH_EN
    #define CHIP_CLK_FLASH_EN   CHIP_CFG_CLK_FLASH_EN
#else
    #define CHIP_CLK_FLASH_EN   1
#endif
#ifdef CHIP_CFG_CLK_CRC_EN
    #define CHIP_CLK_CRC_EN     CHIP_CFG_CLK_CRC_EN
#else
    #define CHIP_CLK_CRC_EN     0
#endif
#ifdef CHIP_CFG_CLK_GPIOA_EN
    #define CHIP_CLK_GPIOA_EN   CHIP_CFG_CLK_GPIOA_EN
#else
    #define CHIP_CLK_GPIOA_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOB_EN
    #define CHIP_CLK_GPIOB_EN   CHIP_CFG_CLK_GPIOB_EN
#else
    #define CHIP_CLK_GPIOB_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOC_EN
    #define CHIP_CLK_GPIOC_EN   CHIP_CFG_CLK_GPIOC_EN
#else
    #define CHIP_CLK_GPIOC_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOD_EN
    #define CHIP_CLK_GPIOD_EN   CHIP_CFG_CLK_GPIOD_EN
#else
    #define CHIP_CLK_GPIOD_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOE_EN
    #define CHIP_CLK_GPIOE_EN   CHIP_CFG_CLK_GPIOE_EN
#else
    #define CHIP_CLK_GPIOE_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOF_EN
    #define CHIP_CLK_GPIOF_EN   CHIP_CFG_CLK_GPIOF_EN
#else
    #define CHIP_CLK_GPIOF_EN   0
#endif

#ifdef CHIP_CFG_CLK_SYSCFG_EN
    #define CHIP_CLK_SYSCFG_EN  CHIP_CFG_CLK_SYSCFG_EN
#else
    #define CHIP_CLK_SYSCFG_EN  1
#endif
#ifdef CHIP_CFG_CLK_PWR_EN
    #define CHIP_CLK_PWR_EN     CHIP_CFG_CLK_PWR_EN
#else
    #define CHIP_CLK_PWR_EN     1
#endif
#ifdef CHIP_CFG_CLK_CRS_EN
    #define CHIP_CLK_CRS_EN     CHIP_CFG_CLK_CRS_EN
#else
    #define CHIP_CLK_CRS_EN     0
#endif
#ifdef CHIP_CFG_CLK_DBG_EN
    #define CHIP_CLK_DBG_EN     CHIP_CFG_CLK_DBG_EN
#else
    #define CHIP_CLK_DBG_EN     1
#endif
#ifdef CHIP_CFG_CLK_WWDG_EN
    #define CHIP_CLK_WWDG_EN    CHIP_CFG_CLK_WWDG_EN
#else
    #define CHIP_CLK_WWDG_EN    0
#endif
#ifdef CHIP_CFG_CLK_WWDG_EN
    #define CHIP_CLK_WWDG_EN    CHIP_CFG_CLK_WWDG_EN
#else
    #define CHIP_CLK_WWDG_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM1_EN
    #define CHIP_CLK_TIM1_EN    CHIP_CFG_CLK_TIM1_EN
#else
    #define CHIP_CLK_TIM1_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM2_EN
    #define CHIP_CLK_TIM2_EN    CHIP_CFG_CLK_TIM2_EN
#else
    #define CHIP_CLK_TIM2_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM3_EN
    #define CHIP_CLK_TIM3_EN    CHIP_CFG_CLK_TIM3_EN
#else
    #define CHIP_CLK_TIM3_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM6_EN
    #define CHIP_CLK_TIM6_EN    CHIP_CFG_CLK_TIM6_EN
#else
    #define CHIP_CLK_TIM6_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM7_EN
    #define CHIP_CLK_TIM7_EN    CHIP_CFG_CLK_TIM7_EN
#else
    #define CHIP_CLK_TIM7_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM14_EN
    #define CHIP_CLK_TIM14_EN   CHIP_CFG_CLK_TIM14_EN
#else
    #define CHIP_CLK_TIM14_EN   0
#endif
#ifdef CHIP_CFG_CLK_TIM15_EN
    #define CHIP_CLK_TIM15_EN   CHIP_CFG_CLK_TIM15_EN
#else
    #define CHIP_CLK_TIM15_EN   0
#endif
#ifdef CHIP_CFG_CLK_TIM16_EN
    #define CHIP_CLK_TIM16_EN   CHIP_CFG_CLK_TIM16_EN
#else
    #define CHIP_CLK_TIM16_EN   0
#endif
#ifdef CHIP_CFG_CLK_TIM17_EN
    #define CHIP_CLK_TIM17_EN   CHIP_CFG_CLK_TIM17_EN
#else
    #define CHIP_CLK_TIM17_EN   0
#endif
#ifdef CHIP_CFG_CLK_USART1_EN
    #define CHIP_CLK_USART1_EN  CHIP_CFG_CLK_USART1_EN
#else
    #define CHIP_CLK_USART1_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART2_EN
    #define CHIP_CLK_USART2_EN  CHIP_CFG_CLK_USART2_EN
#else
    #define CHIP_CLK_USART2_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART3_EN
    #define CHIP_CLK_USART3_EN  CHIP_CFG_CLK_USART3_EN
#else
    #define CHIP_CLK_USART3_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART4_EN
    #define CHIP_CLK_USART4_EN  CHIP_CFG_CLK_USART4_EN
#else
    #define CHIP_CLK_USART4_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART5_EN
    #define CHIP_CLK_USART5_EN  CHIP_CFG_CLK_USART5_EN
#else
    #define CHIP_CLK_USART5_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART6_EN
    #define CHIP_CLK_USART6_EN  CHIP_CFG_CLK_USART6_EN
#else
    #define CHIP_CLK_USART6_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART7_EN
    #define CHIP_CLK_USART7_EN  CHIP_CFG_CLK_USART7_EN
#else
    #define CHIP_CLK_USART7_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART8_EN
    #define CHIP_CLK_USART8_EN  CHIP_CFG_CLK_USART8_EN
#else
    #define CHIP_CLK_USART8_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI1_EN
    #define CHIP_CLK_SPI1_EN    CHIP_CFG_CLK_SPI1_EN
#else
    #define CHIP_CLK_SPI1_EN    0
#endif
#ifdef CHIP_CFG_CLK_SPI2_EN
    #define CHIP_CLK_SPI2_EN    CHIP_CFG_CLK_SPI2_EN
#else
    #define CHIP_CLK_SPI2_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C1_EN
    #define CHIP_CLK_I2C1_EN    CHIP_CFG_CLK_I2C1_EN
#else
    #define CHIP_CLK_I2C1_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C2_EN
    #define CHIP_CLK_I2C2_EN    CHIP_CFG_CLK_I2C2_EN
#else
    #define CHIP_CLK_I2C2_EN    0
#endif
#ifdef CHIP_CFG_CLK_USB_EN
    #define CHIP_CLK_USB_EN     CHIP_CFG_CLK_USB_EN
#else
    #define CHIP_CLK_USB_EN     0
#endif
#ifdef CHIP_CFG_CLK_CAN_EN
    #define CHIP_CLK_CAN_EN     CHIP_CFG_CLK_CAN_EN
#else
    #define CHIP_CLK_CAN_EN     0
#endif
#ifdef CHIP_CFG_CLK_ADC_EN
    #define CHIP_CLK_ADC_EN     CHIP_CFG_CLK_ADC_EN
#else
    #define CHIP_CLK_ADC_EN     0
#endif
#ifdef CHIP_CFG_CLK_DAC_EN
    #define CHIP_CLK_DAC_EN     CHIP_CFG_CLK_DAC_EN
#else
    #define CHIP_CLK_DAC_EN     0
#endif
#ifdef CHIP_CFG_CLK_DAC_EN
    #define CHIP_CLK_DAC_EN     CHIP_CFG_CLK_DAC_EN
#else
    #define CHIP_CLK_DAC_EN     0
#endif
#ifdef CHIP_CFG_CLK_CEC_EN
    #define CHIP_CLK_CEC_EN     CHIP_CFG_CLK_CEC_EN
#else
    #define CHIP_CLK_CEC_EN     0
#endif
#ifdef CHIP_CFG_CLK_TSC_EN
    #define CHIP_CLK_TSC_EN    CHIP_CFG_CLK_TSC_EN
#else
    #define CHIP_CLK_TSC_EN  0
#endif



#ifdef CHIP_CFG_CLK_MCO_EN
    #define CHIP_CLK_MCO_EN     CHIP_CFG_CLK_MCO_EN
#else
    #define CHIP_CLK_MCO_EN     0
#endif

#if CHIP_CLK_MCO_EN
    #ifndef CHIP_CFG_CLK_MCO_SRC
        #error MCO source clock is not specified
    #else
        #define CHIP_CLK_MCO_SRC    CHIP_CFG_CLK_MCO_SRC
    #endif

    #ifdef CHIP_CFG_CLK_MCO_DIV
        #define CHIP_CLK_MCO_DIV    CHIP_CFG_CLK_MCO_DIV
    #else
        #define CHIP_CLK_MCO_DIV    1
    #endif

    #ifdef CHIP_CFG_CLK_MCO_PLL_DIV
        #define CHIP_CLK_MCO_PLL_DIV    CHIP_CFG_CLK_MCO_PLL_DIV
    #else
        #define CHIP_CLK_MCO_PLL_DIV    1
    #endif
#endif

#ifdef CHIP_CFG_LTIMER_EN
    #define CHIP_LTIMER_EN   CHIP_CFG_LTIMER_EN
#else
    #define CHIP_LTIMER_EN   1
#endif


#endif // CHIP_CFG_DEFS_H
