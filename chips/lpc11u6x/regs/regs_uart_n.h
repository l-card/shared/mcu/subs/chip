#ifndef CHIP_REGS_LPC11U6X_UARTN_H
#define CHIP_REGS_LPC11U6X_UARTN_H

#include <stdint.h>

/**
 * @brief UART register block structure
 */
typedef struct {
    __IO uint32_t  CFG;                 /*!< Configuration register */
    __IO uint32_t  CTRL;                /*!< Control register */
    __IO uint32_t  STAT;                /*!< Status register */
    __IO uint32_t  INTENSET;            /*!< Interrupt Enable read and set register */
    __O  uint32_t  INTENCLR;            /*!< Interrupt Enable clear register */
    __I  uint32_t  RXDAT;               /*!< Receive Data register */
    __I  uint32_t  RXDATSTAT;           /*!< Receive Data with status register */
    __IO uint32_t  TXDAT;               /*!< Transmit data register */
    __IO uint32_t  BRG;                 /*!< Baud Rate Generator register */
    __IO uint32_t  INTSTAT;             /*!< Interrupt status register */
    __IO uint32_t  OSR;                 /*!< Oversample selection register for asynchronous communication */
    __IO uint32_t  ADDR;                /*!< Address register for automatic address matching */
} CHIP_REGS_USART_T;


#define CHIP_REGS_USART1            ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART1)
#define CHIP_REGS_USART2            ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART2)
#define CHIP_REGS_USART3            ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART3)
#define CHIP_REGS_USART4            ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART4)

/*********** USART Configuration register (USART_CFG) *************************/
#define CHIP_REGFLD_USART_CFG_ENABLE                (0x00000001UL <<  0U)   /* (RW) USART Enable */
#define CHIP_REGFLD_USART_CFG_DATA_LEN              (0x00000003UL <<  2U)   /* (RW) Selects the data size for the USART */
#define CHIP_REGFLD_USART_CFG_PARITY_SEL            (0x00000003UL <<  4U)   /* (RW) Selects the type of parity is used by the USART */
#define CHIP_REGFLD_USART_CFG_STOP_LEN              (0x00000001UL <<  6U)   /* (RW) Number of stop bits appended to transmitted data */
#define CHIP_REGFLD_USART_CFG_MODE_32K              (0x00000001UL <<  7U)   /* (RW) Selects standard or 32 kHz clocking mode */
#define CHIP_REGFLD_USART_CFG_CTS_EN                (0x00000001UL <<  9U)   /* (RW) CTS Enable */
#define CHIP_REGFLD_USART_CFG_SYNC_EN               (0x00000001UL << 11U)   /* (RW) Selects synchronous or asynchronous operation */
#define CHIP_REGFLD_USART_CFG_CLK_POL               (0x00000001UL << 12U)   /* (RW) Selects the clock polarity and sampling edge of received data in synchronous mode */
#define CHIP_REGFLD_USART_CFG_SYNC_MST              (0x00000001UL << 14U)   /* (RW) Synchronous mode Master select */
#define CHIP_REGFLD_USART_CFG_LOOP                  (0x00000001UL << 15U)   /* (RW) Selects data loopback mode */
#define CHIP_REGFLD_USART_CFG_OETA                  (0x00000001UL << 18U)   /* (RW) Output Enable Turnaround time enable for RS-485 operation */
#define CHIP_REGFLD_USART_CFG_AUTO_ADDR             (0x00000001UL << 19U)   /* (RW) Automatic Address matching enable */
#define CHIP_REGFLD_USART_CFG_OSEL                  (0x00000001UL << 20U)   /* (RW) Output Enable Select (1 - RTS as RS-485 output enable) */
#define CHIP_REGFLD_USART_CFG_OEPOL                 (0x00000001UL << 21U)   /* (RW) Output Enable Polarity (0 - active low, 1 - high) */
#define CHIP_REGFLD_USART_CFG_RX_POL                (0x00000001UL << 22U)   /* (RW) Receive data polarity */
#define CHIP_REGFLD_USART_CFG_TX_POL                (0x00000001UL << 23U)   /* (RW) Transive data polarity */
#define CHIP_REGMSK_USART_CFG_RESERVED              (0xFF032502UL)

#define CHIP_REGFLDVAL_USART_CFG_DATA_LEN_7         0
#define CHIP_REGFLDVAL_USART_CFG_DATA_LEN_8         1
#define CHIP_REGFLDVAL_USART_CFG_DATA_LEN_9         2

#define CHIP_REGFLDVAL_USART_CFG_PARITY_SEL_DIS     0
#define CHIP_REGFLDVAL_USART_CFG_PARITY_SEL_EVEN    2
#define CHIP_REGFLDVAL_USART_CFG_PARITY_SEL_ODD     3

#define CHIP_REGFLDVAL_USART_CFG_STOP_LEN_1         0
#define CHIP_REGFLDVAL_USART_CFG_STOP_LEN_2         1


/*********** USART Control register (USART_CTL) *******************************/
#define CHIP_REGFLD_USART_CTK_TX_BRK_EN             (0x00000001UL <<  1U)   /* (RW) Break Enable */
#define CHIP_REGFLD_USART_CTK_ADDR_DET              (0x00000001UL <<  2U)   /* (RW) Enable address detect mode */
#define CHIP_REGFLD_USART_CTK_TX_DIS                (0x00000001UL <<  6U)   /* (RW) Transmit Disable */
#define CHIP_REGFLD_USART_CTK_CC                    (0x00000001UL <<  8U)   /* (RW) Continuous Clock generation */
#define CHIP_REGFLD_USART_CTK_CLR_CC_ON_RX          (0x00000001UL <<  9U)   /* (RW) Clear Continuous Clock */
#define CHIP_REGFLD_USART_CTK_AUTOBAUD              (0x00000001UL << 16U)   /* (RW) Autobaud enable */
#define CHIP_REGMSK_USART_CTK_RESERVED              (0xFFFEFCB9)


/*********** USART Status register (USART_STAT) *******************************/
#define CHIP_REGFLD_USART_STAT_RX_RDY               (0x00000001UL <<  0U)   /* (RO) Receiver Ready flag */
#define CHIP_REGFLD_USART_STAT_RX_IDLE              (0x00000001UL <<  1U)   /* (RO) Receiver Idle */
#define CHIP_REGFLD_USART_STAT_TX_RDY               (0x00000001UL <<  2U)   /* (RO) Transmitter Ready flag */
#define CHIP_REGFLD_USART_STAT_TX_IDLE              (0x00000001UL <<  3U)   /* (RO) Transmitter Idle */
#define CHIP_REGFLD_USART_STAT_CTS                  (0x00000001UL <<  4U)   /* (RO) State of the CTS signal */
#define CHIP_REGFLD_USART_STAT_DELTA_CTS            (0x00000001UL <<  5U)   /* (RW1C) Change in the state is detected for the CTS  */
#define CHIP_REGFLD_USART_STAT_TX_DIS_STAT          (0x00000001UL <<  6U)   /* (RO)   Transmitter Disabled Interrupt flag */
#define CHIP_REGFLD_USART_STAT_OVERRUN_INT          (0x00000001UL <<  8U)   /* (RW1C) Overrun Error interrupt flag. */
#define CHIP_REGFLD_USART_STAT_RX_BRK               (0x00000001UL << 10U)   /* (RO)   Overrun Error interrupt flag. */
#define CHIP_REGFLD_USART_STAT_DELTA_RX_BRK         (0x00000001UL << 11U)   /* (RW1C) Change in the state of receiver break detection */
#define CHIP_REGFLD_USART_STAT_START                (0x00000001UL << 12U)   /* (RW1C) Start is detected on the receiver input */
#define CHIP_REGFLD_USART_STAT_FRAME_ERR_INT        (0x00000001UL << 13U)   /* (RW1C) Framing Error interrupt flag  */
#define CHIP_REGFLD_USART_STAT_PARITY_ERR_INT       (0x00000001UL << 14U)   /* (RW1C) Parity Error interrupt flag */
#define CHIP_REGFLD_USART_STAT_RX_NOISE_INT         (0x00000001UL << 15U)   /* (RW1C) Received Noise interrupt flag */
#define CHIP_REGFLD_USART_STAT_AB_ERR               (0x00000001UL << 16U)   /* (RW1C) Autobaud Error */
#define CHIP_REGMSK_USART_STAT_RESERVED             (0xFFFE0280UL)

/* USART Interrupt Enable read and set/clear/status register (USART_INTENSET/INTENCLEAR/INTSTAT) */
#define CHIP_REGFLD_USART_INT_RX_RDY                (0x00000001UL <<  0U)   /* (RW) RX_RDY interrupt enable */
#define CHIP_REGFLD_USART_INT_TX_RDY                (0x00000001UL <<  2U)   /* (RW) TX_RDY interrupt enable */
#define CHIP_REGFLD_USART_INT_TX_IDLE               (0x00000001UL <<  3U)   /* (RW) TX_IDLE interrupt enable */
#define CHIP_REGFLD_USART_INT_DELTA_CTS             (0x00000001UL <<  5U)   /* (RW) DELTA_CTS interrupt enable */
#define CHIP_REGFLD_USART_INT_TX_DIS                (0x00000001UL <<  6U)   /* (RW) TX_DIS interrupt enable */
#define CHIP_REGFLD_USART_INT_OVERRUN               (0x00000001UL <<  8U)   /* (RW) overrun interrupt enable */
#define CHIP_REGFLD_USART_INT_DELTA_RX_BRK          (0x00000001UL << 11U)   /* (RW) DELTA_RX_BRK interrupt enable */
#define CHIP_REGFLD_USART_INT_START                 (0x00000001UL << 12U)   /* (RW) START interrupt enable */
#define CHIP_REGFLD_USART_INT_FRAME_ERR             (0x00000001UL << 13U)   /* (RW) FRAME_ERR interrupt enable */
#define CHIP_REGFLD_USART_INT_PARITY_ERR            (0x00000001UL << 14U)   /* (RW) PARITY_ERR interrupt enable */
#define CHIP_REGFLD_USART_INT_RX_NOISE              (0x00000001UL << 15U)   /* (RW) RX_NOISE interrupt enable */
#define CHIP_REGFLD_USART_INT_AB_ERR                (0x00000001UL << 16U)   /* (RW) AB_ERR interrupt enable */
#define CHIP_REGMSK_USART_INT_RESERVED              (0xFFFE0692UL)

/*********** USART Receiver Data with Status register (USART_RXDATSTAT) *******/
#define CHIP_REGFLD_USART_RXDATSTAT_RXDAT           (0x000001FFUL <<  0U)   /* (RO) Receiver Data */
#define CHIP_REGFLD_USART_RXDATSTAT_FRAME_ERR       (0x00000001UL << 13U)   /* (RO) Framing Error status flag */
#define CHIP_REGFLD_USART_RXDATSTAT_PARITY_ERR      (0x00000001UL << 14U)   /* (RO) Parity Error status flag */
#define CHIP_REGFLD_USART_RXDATSTAT_RX_NOISE        (0x00000001UL << 15U)   /* (RO) Received Noise flag */
#define CHIP_REGMSK_USART_RXDATSTAT_RESERVED        (0xFFFF1E00UL)

/*********** USART Baud Rate Generator register (USART_BRG) *******************/
#define CHIP_REGFLD_USART_BRG_BRGVAL                (0x0000FFFFUL <<  0U)   /* (RW) USART input clock divider to determine the baud rate, */
#define CHIP_REGMSK_USART_BRG_RESERVED              (0xFFFF0000UL)

/*********** USART Oversample selection register (USART_OSR) ******************/
#define CHIP_REGFLD_USART_OSR_OSRVAL                (0x000000FFUL <<  0U)   /* (RW) Oversample Selection Value, */
#define CHIP_REGMSK_USART_OSR_RESERVED              (0xFFFFFF00UL)

/*********** USART Address register (USART_ADDR) ******************************/
#define CHIP_REGFLD_USART_ADDR_ADDRESS              (0x000000FFUL <<  0U)   /* (RW)  */
#define CHIP_REGMSK_USART_ADDR_RESERVED             (0xFFFFFF00UL)

#endif /* CHIP_REGS_LPC11U6X_UARTN_H */
