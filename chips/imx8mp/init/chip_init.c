void chip_init_mmu(void);
void chip_clk_init(void);

void chip_init(void) {
    chip_init_mmu();
    chip_clk_init();
}

