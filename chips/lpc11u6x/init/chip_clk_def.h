#ifndef CHIP_CLK_DEF_H
#define CHIP_CLK_DEF_H

#define CHIP_CLK_IRC_FREQ           CHIP_MHZ(12)
#define CHIP_CLK_RTCOSC_FREQ        (32768UL)


#define CHIP_CLK_CPU_INITIAL_FREQ   CHIP_CLK_IRC_FREQ


#define CHIP_CLK_IRC_ID             1
#define CHIP_CLK_SYSOSC_ID          2
#define CHIP_CLK_RTCOSC_ID          3
#define CHIP_CLK_WDTOSC_ID          4
#define CHIP_CLK_SYSPLLIN_ID        5
#define CHIP_CLK_SYSPLL_ID          6
#define CHIP_CLK_USBPLL_ID          7
#define CHIP_CLK_MAIN_ID            8


#endif // CHIP_CLK_DEF_H
