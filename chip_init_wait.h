#ifndef CHIP_INIT_WAIT_H
#define CHIP_INIT_WAIT_H

extern void chip_user_init_fault(int code);


#define CHIP_INIT_WAIT(check_func, err_code, tout_us) do { \
        unsigned rem_us = tout_us; \
        while (!(check_func) && (rem_us > 0)) { \
            chip_delay_clk(CHIP_CLK_CPU_INITIAL_FREQ/CHIP_MHZ(1)); \
            --rem_us; \
        } \
        if (!(check_func)) { \
            chip_user_init_fault(err_code); \
        } \
    } while(0)

#endif // CHIP_INIT_WAIT_H
