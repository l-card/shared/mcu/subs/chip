#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#include "chip_devtype_spec_features.h"

/* граничные условия для внешней частоты HSE */
#define CHIP_CLK_HSE_OSC_FREQ_MIN           CHIP_MHZ(4)  /* минимальная  частота HSE при подключении кварцевого резанатора */
#define CHIP_CLK_HSE_OSC_FREQ_MAX           CHIP_MHZ(16) /* максимальная частота HSE при подключении кварцевого резанатора */
#define CHIP_CLK_HSE_CLOCK_FREQ_MAX         CHIP_MHZ(25) /* максимальная частота HSE при подключении внешнего клока  */

#define CHIP_CLK_ADC_FREQ_MAX               CHIP_MHZ(14)

#define CHIP_CLK_MCO_FREQ_MAX               CHIP_MHZ(50) /* stm10x */

#define CHIP_CLK_AHB_FREQ_MAX               CHIP_CLK_CPU_FREQ_MAX
#ifdef CHIP_DEV_AT32F40X
#define CHIP_CLK_APB1_FREQ_MAX              CHIP_MHZ(120)
#define CHIP_CLK_APB2_FREQ_MAX              CHIP_MHZ(120)
#elif defined CHIP_DEV_CH32V3XX
#define CHIP_CLK_APB1_FREQ_MAX              CHIP_CLK_CPU_FREQ_MAX
#define CHIP_CLK_APB2_FREQ_MAX              CHIP_CLK_CPU_FREQ_MAX
#else
#define CHIP_CLK_APB1_FREQ_MAX              (CHIP_CLK_CPU_FREQ_MAX/2)
#define CHIP_CLK_APB2_FREQ_MAX              CHIP_CLK_CPU_FREQ_MAX
#endif

#ifndef CHIP_DEV_GD32F10X
#define CHIP_CLK_FLASH_LATENCY0_MAX         CHIP_MHZ(24)
#define CHIP_CLK_FLASH_LATENCY1_MAX         CHIP_MHZ(48)
#endif




#endif // CHIP_CLK_CONSTRAINTS_H
