#ifndef REGS_PCFG_H
#define REGS_PCFG_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t BANDGAP;                     /* 0x0: BANGGAP control */
    __IO uint32_t LDO1P1;                      /* 0x4: 1V LDO config */
    __IO uint32_t LDO2P5;                      /* 0x8: 2.5V LDO config */
    __I  uint8_t  RESERVED0[4];                /* 0xC - 0xF: Reserved */
    __IO uint32_t DCDC_MODE;                   /* 0x10: DCDC mode select */
    __IO uint32_t DCDC_LPMODE;                 /* 0x14: DCDC low power mode */
    __IO uint32_t DCDC_PROT;                   /* 0x18: DCDC protection */
    __IO uint32_t DCDC_CURRENT;                /* 0x1C: DCDC current estimation */
    __IO uint32_t DCDC_ADVMODE;                /* 0x20: DCDC advance settings */
    __IO uint32_t DCDC_ADVPARAM;               /* 0x24: DCDC advance parameters */
    __IO uint32_t DCDC_MISC;                   /* 0x28: DCDC misc parameters */
    __IO uint32_t DCDC_DEBUG;                  /* 0x2C: DCDC Debug */
    __IO uint32_t DCDC_START_TIME;             /* 0x30: DCDC ramp time */
    __IO uint32_t DCDC_RESUME_TIME;            /* 0x34: DCDC resume time */
    __I  uint8_t  RESERVED1[8];                /* 0x38 - 0x3F: Reserved */
    __IO uint32_t POWER_TRAP;                  /* 0x40: SOC power trap */
    __IO uint32_t WAKE_CAUSE;                  /* 0x44: Wake up source */
    __IO uint32_t WAKE_MASK;                   /* 0x48: Wake up mask */
    __IO uint32_t SCG_CTRL;                    /* 0x4C: Clock gate control in PMIC */
    __IO uint32_t DEBUG_STOP;                  /* 0x50: Debug stop config */
    __I  uint8_t  RESERVED2[12];               /* 0x54 - 0x5F: Reserved */
    __IO uint32_t RC24M;                       /* 0x60: RC 24M config */
    __IO uint32_t RC24M_TRACK;                 /* 0x64: RC 24M track mode */
    __IO uint32_t TRACK_TARGET;                /* 0x68: RC 24M track target */
    __I  uint32_t STATUS;                      /* 0x6C: RC 24M track status */
} CHIP_REGS_PCFG_T;

#define CHIP_REGS_PCFG            ((CHIP_REGS_PCFG_T *)CHIP_MEMRGN_ADDR_PCFG)


/*********** BANGGAP control - BANDGAP ****************************************/
#define CHIP_REGFLD_PCFG_BANDGAP_VBG_TRIMMED            (0x00000001UL << 31U) /* (RW) Reference soucre is calibrated */
#define CHIP_REGFLD_PCFG_BANDGAP_LOWPOWER_MODE          (0x00000001UL << 25U) /* (RW) Reference source low power mode */
#define CHIP_REGFLD_PCFG_BANDGAP_POWER_SAVE             (0x00000001UL << 24U) /* (RW) Reference source power saving mode */
#define CHIP_REGFLD_PCFG_BANDGAP_VBG_1P0_TRIM           (0x0000001FUL << 16U) /* (RW) 1.0V reference calibration value */
#define CHIP_REGFLD_PCFG_BANDGAP_VBG_P65_TRIM           (0x0000001FUL <<  8U) /* (RW) 0.65V reference calibration value */
#define CHIP_REGFLD_PCFG_BANDGAP_VBG_P50_TRIM           (0x0000001FUL <<  0U) /* (RW) 0.5V reference calibration value */
#define CHIP_REGMSK_PCFG_BANDGAP_RESERVED               (0x7CE0E0E0UL)

/*********** 1V LDO config - LDO1P1 *******************************************/
#define CHIP_REGFLD_PCFG_LDO1P1_ENABLE                  (0x00000001UL << 16U) /* (RW) LDO enable */
#define CHIP_REGFLD_PCFG_LDO1P1_VOLT                    (0x00000FFFUL <<  0U) /* (RW) Output voltage in millivolts (range 700 mV – 1320 mV, step 20 mV). */
#define CHIP_REGMSK_PCFG_LDO1P1_RESERVED                (0xFFFEF000UL)

/*********** 2.5V LDO config - LDO2P5 *****************************************/
#define CHIP_REGFLD_PCFG_LDO2P5_READY                   (0x00000001UL << 28U) /* (RO) LDO ready */
#define CHIP_REGFLD_PCFG_LDO2P5_ENABLE                  (0x00000001UL << 16U) /* (RW) LDO enable */
#define CHIP_REGFLD_PCFG_LDO2P5_VOLT                    (0x00000FFFUL <<  0U) /* (RW) Output voltage in millivolts (range 2125 mV - 2900 mV, step 25 mV). */
#define CHIP_REGMSK_PCFG_LDO2P5_RESERVED                (0xEFFEF000UL)

/*********** DCDC mode select - DCDC_MODE *************************************/
#define CHIP_REGFLD_PCFG_DCDC_MODE_READY                (0x00000001UL << 28U) /* (RO) DCDC ready */
#define CHIP_REGFLD_PCFG_DCDC_MODE_MODE                 (0x00000007UL << 16U) /* (RW) DCDC working mode */
#define CHIP_REGFLD_PCFG_DCDC_MODE_VOLT                 (0x00000FFFUL <<  0U) /* (RW) Output voltage in millivolts (range 600 mV - 1375 mV, step 25 mV). */
#define CHIP_REGMSK_PCFG_DCDC_MODE_RESERVED             (0xEFFEF000UL)

#define CHIP_REGFLDVAL_PCFG_DCDC_MODE_MODE_SHUTDOWN     0
#define CHIP_REGFLDVAL_PCFG_DCDC_MODE_MODE_BASIC        1
#define CHIP_REGFLDVAL_PCFG_DCDC_MODE_MODE_GENERAL      3
#define CHIP_REGFLDVAL_PCFG_DCDC_MODE_MODE_EXPERT       7

/*********** DCDC low power mode - DCDC_LPMODE ********************************/
#define CHIP_REGFLD_PCFG_DCDC_LPMODE_VOLT               (0x00000FFFUL <<  0U) /* (RW) ow power mode output voltage in millivolts (range 600 mV - 1375 mV, step 25 mV). */
#define CHIP_REGMSK_PCFG_DCDC_LPMODE_RESERVED           (0xFFFFF000UL)

/*********** DCDC protection - DCDC_PROT **************************************/
#define CHIP_REGFLD_PCFG_DCDC_PROT_ILIMIT_LP            (0x00000001UL << 28U) /* (RW) Low power mode load current */
#define CHIP_REGFLD_PCFG_DCDC_PROT_OVERLOAD_LP          (0x00000001UL << 24U) /* (RW) Overcurrent occurs in low power mode */
#define CHIP_REGFLD_PCFG_DCDC_PROT_DISABLE_POWER_LOSS   (0x00000001UL << 23U) /* (RW) Disable power loss protection */
#define CHIP_REGFLD_PCFG_DCDC_PROT_POWER_LOSS_FLAG      (0x00000001UL << 16U) /* (RO) Power loss flag */
#define CHIP_REGFLD_PCFG_DCDC_PROT_DISABLE_OVERVOLTAGE  (0x00000001UL << 15U) /* (RW) Disable output overvoltage protection */
#define CHIP_REGFLD_PCFG_DCDC_PROT_OVERVOLT_FLAG        (0x00000001UL <<  8U) /* (RO) Output overvoltage flag */
#define CHIP_REGFLD_PCFG_DCDC_PROT_DISABLE_SHORT        (0x00000001UL <<  7U) /* (RW) Disable output short circuit protection */
#define CHIP_REGFLD_PCFG_DCDC_PROT_SHORT_CURRENT        (0x00000001UL <<  6U) /* (RW) Short circuit current */
#define CHIP_REGFLD_PCFG_DCDC_PROT_SHORT_FLAG           (0x00000001UL <<  0U) /* (RO) Short circuit flag */
#define CHIP_REGMSK_PCFG_DCDC_PROT_RESERVED             (0xEE7E7E6EUL)

#define CHIP_REGFLDVAL_PCFG_DCDC_PROT_ILIMIT_LP_250MA       0
#define CHIP_REGFLDVAL_PCFG_DCDC_PROT_ILIMIT_LP_200MA       1

#define CHIP_REGFLDVAL_PCFG_DCDC_PROT_SHORT_CURRENT_2_0A    0
#define CHIP_REGFLDVAL_PCFG_DCDC_PROT_SHORT_CURRENT_1_3A    1

/*********** DCDC current estimation - DCDC_CURRENT ***************************/
#define CHIP_REGFLD_PCFG_DCDC_CURRENT_ESTI_EN           (0x00000001UL << 15U) /* (RW) Enable current estimation */
#define CHIP_REGFLD_PCFG_DCDC_CURRENT_VALID             (0x00000001UL <<  8U) /* (RO) Current estimate is valid */
#define CHIP_REGFLD_PCFG_DCDC_CURRENT_LEVEL             (0x0000001FUL <<  0U) /* (RO) DCDC estimated current, current value is LEVEL * 50mA */
#define CHIP_REGMSK_PCFG_DCDC_CURRENT_RESERVED          (0xFFFF7EE0UL)

/*********** DCDC advance settings - DCDC_ADVMODE *****************************/
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_RCSCALE        (0x00000007UL << 24U) /* (RW) Enable RC scale */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_DC_C              (0x00000003UL << 20U) /* (RW) Loop C number */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_DC_R              (0x0000000FUL << 16U) /* (RW) Loop R number */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_FF_DET         (0x00000001UL <<  6U) /* (RW) Enable feed forward detect */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_FF_LOOP        (0x00000001UL <<  5U) /* (RW) Enable feed forward loop */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_AUTOLP         (0x00000001UL <<  4U) /* (RW) Enable auto enter low power mode */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_DCM_EXIT       (0x00000001UL <<  3U) /* (RW) Avoid overvoltage */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_SKIP           (0x00000001UL <<  2U) /* (RW) Enable skip on narrow pulse */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_IDLE           (0x00000001UL <<  1U) /* (RW) Enable skip when voltage is higher than threshold */
#define CHIP_REGFLD_PCFG_DCDC_ADVMODE_EN_DCM            (0x00000001UL <<  0U) /* (RW) DCM mode */
#define CHIP_REGMSK_PCFG_DCDC_ADVMODE_RESERVED          (0xF8C0FF80UL)

/*********** DCDC advance parameters- DCDC_ADVPARAM ***************************/
#define CHIP_REGFLD_PCFG_DCDC_ADVPARAM_MIN_DUT          (0x0000007FUL <<  8U) /* (RW) Minimum duty cycle */
#define CHIP_REGFLD_PCFG_DCDC_ADVPARAM_MAX_DUT          (0x0000007FUL <<  0U) /* (RW) Maximum duty cycle */
#define CHIP_REGMSK_PCFG_DCDC_ADVPARAM_RESERVED         (0xFFFF8080UL)


/*********** DCDC misc parameters- DCDC_MISC **********************************/
#define CHIP_REGFLD_PCFG_DCDC_MISC_EN_HYST              (0x00000001UL << 28U) /* (RW) Hysteresis enable */
#define CHIP_REGFLD_PCFG_DCDC_MISC_HYST_SIGN            (0x00000001UL << 25U) /* (RW) Hysteresis sign */
#define CHIP_REGFLD_PCFG_DCDC_MISC_HYST_THRS            (0x00000001UL << 24U) /* (RW) Hysteresis threshold */
#define CHIP_REGFLD_PCFG_DCDC_MISC_RC_SCALE             (0x00000001UL << 20U) /* (RW) Loop RC scale threshold */
#define CHIP_REGFLD_PCFG_DCDC_MISC_DC_FF                (0x00000007UL << 16U) /* (RW) Loop feed forward number */
#define CHIP_REGFLD_PCFG_DCDC_MISC_OL_THRE              (0x00000003UL <<  8U) /* (RW) Overload for threshold for lod power mode */
#define CHIP_REGFLD_PCFG_DCDC_MISC_OL_HYST              (0x00000001UL <<  4U) /* (RW) Current hysteresis range */
#define CHIP_REGFLD_PCFG_DCDC_MISC_DELAY                (0x00000001UL <<  2U) /* (RW) Enable delay */
#define CHIP_REGFLD_PCFG_DCDC_MISC_CLK_SEL              (0x00000001UL <<  1U) /* (RW) Clock selection */
#define CHIP_REGFLD_PCFG_DCDC_MISC_EN_STEP              (0x00000001UL <<  0U) /* (RW) Enable stepping in voltage change */
#define CHIP_REGMSK_PCFG_DCDC_MISC_RESERVED             (0xECE8FCE8UL)

#define CHIP_REGFLDVAL_PCFG_DCDC_MISC_OL_HYST_12_5MV    0
#define CHIP_REGFLDVAL_PCFG_DCDC_MISC_OL_HYST_25_0MV    1

#define CHIP_REGFLDVAL_PCFG_DCDC_MISC_CLK_SEL_DCDC      0 /* Select DCDC internal oscillator */
#define CHIP_REGFLDVAL_PCFG_DCDC_MISC_CLK_SEL_RC24M     1 /* Select  oscillator */


/*********** DCDC Debug- DCDC_DEBUG *******************************************/
#define CHIP_REGFLD_PCFG_DCDC_DEBUG_UPDATE_TIME         (0x000FFFFFUL <<  0U) /* (RW) DCDC voltage adjustment time, counted in 24M clock cycles, default value 1mS */
#define CHIP_REGMSK_PCFG_DCDC_DEBUG_RESERVED            (0xFFF00000UL)

/*********** DCDC ramp time - DCDC_START_TIME *********************************/
#define CHIP_REGFLD_PCFG_DCDC_START_TIME_VAL            (0x000FFFFFUL <<  0U) /* (RW) DCDC voltage startup time, counted in 24M clock cycles, default value 3mS */
#define CHIP_REGMSK_PCFG_DCDC_START_TIME_RESERVED       (0xFFF00000UL)

/*********** DCDC resume time - DCDC_RESUME_TIME *********************************/
#define CHIP_REGFLD_PCFG_DCDC_RESUME_TIME_VAL           (0x000FFFFFUL <<  0U) /* (RW) DCDC low power recovery time, counted in 24M clock cycles, default value 1.5mS */
#define CHIP_REGMSK_PCFG_DCDC_RESUME_TIME_RESERVED      (0xFFF00000UL)

/*********** SOC power trap - POWER_TRAP **************************************/
#define CHIP_REGFLD_PCFG_POWER_TRAP_TRIGGERED           (0x00000001UL << 31U) /* (RW1C) DCDC was entered in low power status. */
#define CHIP_REGFLD_PCFG_POWER_TRAP_RETENTION           (0x00000001UL << 16U) /* (RW) When DCDC enters low power mode, it reduces the voltage to maintain the contents of SOCSRAM. */
#define CHIP_REGFLD_PCFG_POWER_TRAP_TRAP                (0x00000001UL <<  0U) /* (RW) Allows DCDC to shut down or reduce voltage in low power mode. */
#define CHIP_REGMSK_PCFG_POWER_TRAP_RESERVED            (0x7FFEFFFEUL)

/*********** Wake up source / Wake up mask - WAKE_CAUSE / WAKE_MASK ***********/
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_CLOCK               (0x00000001UL << 19U) /* (RW1C/RW) Clock interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_BUTN                (0x00000001UL << 18U) /* (RW1C/RW) Button interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_BGPIO               (0x00000001UL << 17U) /* (RW1C/RW) BGPIO interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_BAT_SEC_VIOLATION   (0x00000001UL << 16U) /* (RW1C/RW) Battery domain security violation */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_SEC_STATE           (0x00000001UL << 12U) /* (RW1C/RW) Security state interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_SEC_SENS            (0x00000001UL << 11U) /* (RW1C/RW) Security sensor interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_PGPIO               (0x00000001UL << 10U) /* (RW1C/RW) PGPIO interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_WDG                 (0x00000001UL <<  9U) /* (RW1C/RW) WDG interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_TMR                 (0x00000001UL <<  8U) /* (RW1C/RW) TMR interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_UART                (0x00000001UL <<  7U) /* (RW1C/RW) UART interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_FUSE                (0x00000001UL <<  4U) /* (RW1C/RW) Fuse interrupt */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_DEBUG               (0x00000001UL <<  1U) /* (RW1C/RW) Debug wakeup */
#define CHIP_REGFLD_PCFG_WAKE_CAUSE_SOC_REQ             (0x00000001UL <<  0U) /* (RW1C/RW) Soc request */


/*********** Clock gate control in PMIC - SCG_CTRL ****************************/
#define CHIP_REGFLD_PCFG_SCG_CTRL_DEBUG_PORT            (0x00000003UL << 16U) /* (RW) Debug port */
#define CHIP_REGFLD_PCFG_SCG_CTRL_UART                  (0x00000003UL << 14U) /* (RW) UART */
#define CHIP_REGFLD_PCFG_SCG_CTRL_WDG                   (0x00000003UL << 12U) /* (RW) WDG */
#define CHIP_REGFLD_PCFG_SCG_CTRL_TMR                   (0x00000003UL << 10U) /* (RW) TMR */
#define CHIP_REGFLD_PCFG_SCG_CTRL_IOC                   (0x00000003UL <<  8U) /* (RW) IO controller */
#define CHIP_REGFLD_PCFG_SCG_CTRL_GPIO                  (0x00000003UL <<  6U) /* (RW) GPIO */
#define CHIP_REGFLD_PCFG_SCG_CTRL_VAD                   (0x00000003UL <<  4U) /* (RW) Voice detection */
#define CHIP_REGFLD_PCFG_SCG_CTRL_SRAM                  (0x00000003UL <<  2U) /* (RW) SRAM */
#define CHIP_REGFLD_PCFG_SCG_CTRL_FUSE                  (0x00000003UL <<  0U) /* (RW) Fuse */

#define CHIP_REGFLDVAL_PCFG_SCG_CTRL_OFF                0x2
#define CHIP_REGFLDVAL_PCFG_SCG_CTRL_RUN                0x3

/***********  Debug stop config - DEBUG_STOP **********************************/
#define CHIP_REGFLD_PCFG_DEBUG_STOP_CPU1                (0x00000001UL <<  1U) /* (RW) Notify peripheral settings during CPU1 debugging */
#define CHIP_REGFLD_PCFG_DEBUG_STOP_CPU0                (0x00000001UL <<  0U) /* (RW) Notify peripheral settings during CPU0 debugging */

/***********  RC 24M config - RC24M *******************************************/
#define CHIP_REGFLD_PCFG_RC24M_RC_TRIMMED               (0x00000001UL << 31U) /* (RW) RC24M calibration flag */
#define CHIP_REGFLD_PCFG_RC24M_TRIM_C                   (0x00000007UL <<  8U) /* (RW) RC24M coarse adjustment */
#define CHIP_REGFLD_PCFG_RC24M_TRIM_F                   (0x0000001FUL <<  0U) /* (RW) RC24M fine adjustment */
#define CHIP_REGMSK_PCFG_RC24M_RESERVED                 (0x7FFFF8E0UL)

/***********  RC 24M track mode - RC24M_TRACK *********************************/
#define CHIP_REGFLD_PCFG_RC24M_TRACK_SEL24M             (0x00000001UL << 16U) /* (RW) Select external reference (0 - 32K OSC, 1 - 24M OSC) */
#define CHIP_REGFLD_PCFG_RC24M_TRACK_RETURN             (0x00000001UL <<  4U) /* (RW) Behavior when external clock source is stopped (0 - keep last, 1 - set default) */
#define CHIP_REGFLD_PCFG_RC24M_TRACK_TRACK              (0x00000001UL <<  0U) /* (RW) Automatic tracking (0 - free running, 1 - track external source) */
#define CHIP_REGMSK_PCFG_RC24M_TRACK_RESERVED           (0xFFFEFFEEUL)

/***********  RC 24M track target - TRACK_TARGET ******************************/
#define CHIP_REGFLD_PCFG_TRACK_TARGET_PRE_DIV           (0x0000FFFFUL << 16U) /* (RW) Reference frequency prescaler number */
#define CHIP_REGFLD_PCFG_TRACK_TARGET_TARGET            (0x0000FFFFUL <<  0U) /* (RW) Target frequency, multiplier of prescaled reference frequency */

/***********  RC 24M track status - STATUS ************************************/
#define CHIP_REGFLD_PCFG_STATUS_SEL32K                  (0x00000001UL << 20U) /* (RO) Reference is OSC 32K */
#define CHIP_REGFLD_PCFG_STATUS_SEL24M                  (0x00000001UL << 16U) /* (RO) Reference is OSC 24M */
#define CHIP_REGFLD_PCFG_STATUS_EN_TRIM                 (0x00000001UL << 15U) /* (RO) Default values available */
#define CHIP_REGFLD_PCFG_STATUS_TRIM_C                  (0x00000007UL <<  8U) /* (RO) Default coarse adjustment value */
#define CHIP_REGFLD_PCFG_STATUS_TRIM_F                  (0x0000001FUL <<  0U) /* (RO) Default fine adjustment value */




#endif // REGS_PCFG_H
