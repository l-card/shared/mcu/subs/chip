#ifndef CHIP_CMSIS_CFG_H
#define CHIP_CMSIS_CFG_H

#define __MPU_PRESENT             0 /* Other CH32 devices does not provide an MPU */
#define __NVIC_PRIO_BITS          4 /* CH32 uses 4 Bits for the Priority Levels */
#define __Vendor_SysTickConfig    0 /* Set to 1 if different SysTick Config is used */


#endif // CHIP_CMSIS_CFG_H
