#ifndef ATMEGA8515_EXTINT_H
#define ATMEGA8515_EXTINT_H

/******************************************************************************
   Файл содержит макросы для настройки и использования внешних прерываний.
   В качестве номера прерывания (intnum) должно использоваться одно из значений:
     - CHIP_EXTINT0 - INT0
     - CHIP_EXTINT1 - INT1
     - CHIP_EXTINT2 - INT2

   Доступны следующие макросы:

    - CHIP_EXTINT_ISR(intnum)       - объявление функции обработки прерывания intnum
                                      (после него должен идти код обработчика в {})
    - CHIP_EXTINT_CONFIG(intnum, mode) - настройка прерывания на заданный режим срабатывания:
        - CHIP_EXTINT_MODE_LOW_LVL      - по низкому уровню (только INT0, INT1)
        - CHIP_EXTINT_MODE_ANY_EDGE     - по любомоу перепаду (только INT0, INT1)
        - CHIP_EXTINT_MODE_FALL         - по спаду
        - CHIP_EXTINT_MODE_RISE         - по фронту
    - CHIP_EXTINT_EN(intnum)        - разрешение прерывания
    - CHIP_EXTINT_DIS(intnum)       - запрет прерывания
    - CHIP_EXTINT_IS_EN(intnum)     - проверка, разрешено ли прерывание
    - CHIP_EXTINT_CLR(intnum)       - сброс флага прерывания
    - CHIP_EXTINT_IS_SET(intnum)    - проверка, установлен ли флаг прерывания
*******************************************************************************/




/* номера внешних прерываний */
#define CHIP_EXTINT0 0 /* INT0 */
#define CHIP_EXTINT1 1 /* INT1 */
#define CHIP_EXTINT2 2 /* INT2 */

/* настройки срабатывания внешних прерываний */
#define CHIP_EXTINT_MODE_LOW_LVL    0 /* по низкому уровню (только INT0, INT1) */
#define CHIP_EXTINT_MODE_ANY_EDGE   1 /* по любомоу перепаду (только INT0, INT1) */
#define CHIP_EXTINT_MODE_FALL       2 /* по спаду */
#define CHIP_EXTINT_MODE_RISE       3 /* по фронту */


/* макросы настройки каждого прерывания. как правило используются не напрямую, а через CHIP_EXTINT_CONFIG */
#define CHIP_EXTINT0_CONFIG(mode)        LBITFIELD_UPD(MCUCR, _BV(ISC00) | _BV(ISC01), mode)
#define CHIP_EXTINT1_CONFIG(mode)        LBITFIELD_UPD(MCUCR, _BV(ISC10) | _BV(ISC11), mode)

/* маппинг стандартных кодов режимов INT0 и INT1 на режимы INT2 */
#define CHIP_EXTINT2_MODE2_         0
#define CHIP_EXTINT2_MODE3_         1
/* при изменении настроек INT2 может быть ложное срабатывание прерывания.
 * поэтому явно его запрещаем перед обновлением, а после сбрасываем флаг и при
 * необходимости разрешаем снова */
#define CHIP_EXTINT2_CONFIG_(mode)       do { \
                                            uint8_t is_en = CHIP_EXTINT_IS_EN(CHIP_EXTINT2); \
                                            CHIP_EXTINT_DIS(CHIP_EXTINT2); \
                                            LBITFIELD_UPD(EMCUCR, _BV(ISC2), CHIP_EXTINT2_MODE##mode##_); \
                                            CHIP_EXTINT_CLR(CHIP_EXTINT2); \
                                            if (is_en) \
                                                CHIP_EXTINT_EN(CHIP_EXTINT2); \
                                         } while(0)
#define CHIP_EXTINT2_CONFIG(mode)        CHIP_EXTINT2_CONFIG_(mode)


/* настройка указанного прерывания  CHIP_EXTINTx на заданный режим из CHIP_EXTINT_MODE_xxx*/
#define CHIP_EXTINT_CONFIG(intnum, mode)    CHIP_EXTINT_CONFIG_(intnum, mode)
#define CHIP_EXTINT_CONFIG_(intnum, mode)   CHIP_EXTINT##intnum##_CONFIG(mode)


/* название бита GICR для разрешения или запрещения прерывания CHIP_EXTINTx */
#define CHIP_EXTINT_EN_BIT(intnum)       CHIP_EXTINT_EN_BIT_(intnum)
#define CHIP_EXTINT_EN_BIT_(intnum)      _BV(INT##intnum)

/* разрешение указанного прерывания  CHIP_EXTINTx */
#define CHIP_EXTINT_EN(intnum)          (GICR |= CHIP_EXTINT_EN_BIT(intnum))

/* разрешение указанного прерывания  CHIP_EXTINTx */
#define CHIP_EXTINT_DIS(intnum)          (GICR &= ~CHIP_EXTINT_EN_BIT(intnum))

/* проверка, разрешено ли прерывание  CHIP_EXTINTx  (без учета глобального флага всех прерываний) */
#define CHIP_EXTINT_IS_EN(intnum)           CHIP_EXTINT_IS_SET_(intnum)
#define CHIP_EXTINT_IS_EN_(intnum)          !!(GICR & _BV(INT##intnum))

/* сброс флага возникновения прерывания CHIP_EXTINTx  */
#define CHIP_EXTINT_CLR(intnum)           CHIP_EXTINT_CLR_(intnum)
#define CHIP_EXTINT_CLR_(intnum)          (GIFR = _BV(INTF##intnum))

/* проверка, установлен ли флаг возникновения прерывания CHIP_EXTINTx  */
#define CHIP_EXTINT_IS_SET(intnum)           CHIP_EXTINT_IS_SET_(intnum)
#define CHIP_EXTINT_IS_SET_(intnum)          !!(GIFR & _BV(INTF##intnum))


/* название вектора обработчика прерывания для CHIP_EXTINTx */
#define CHIP_EXTINT_VECT(intnum)              CHIP_EXTINT_VECT_(intnum)
#define CHIP_EXTINT_VECT_(intnum)             INT##intnum##_vect

/* определение обработчика прерывания для CHIP_EXTINTx */
#define CHIP_EXTINT_ISR(intnum)              ISR(CHIP_EXTINT_VECT(intnum))


#endif // ATMEGA8515_EXTINT_H

