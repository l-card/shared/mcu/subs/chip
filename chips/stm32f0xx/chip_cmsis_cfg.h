#ifndef CHIP_STM32G0XX_CMSIS_H_
#define CHIP_STM32G0XX_CMSIS_H_

/**
 * @brief Configuration of the Cortex-M0 Processor and Core Peripherals
 */
#define __CM0_REV                 0 /*!< Core Revision r0p0                            */
#define __MPU_PRESENT             0 /*!< STM32F0xx do not provide MPU                  */
#define __NVIC_PRIO_BITS          2 /*!< STM32F0xx uses 2 Bits for the Priority Levels */
#define __Vendor_SysTickConfig    0     /*!< Set to 1 if different SysTick Config is used */
#ifdef CHIP_CORETYPE_CM0P
#define __VTOR_PRESENT            1 /** Set to 1 if different Vector Table Offset is used */
#endif


#endif /*CHIP_STM32G0XX_CMSIS_H_*/
