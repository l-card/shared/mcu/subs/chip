#ifndef CHIP_CORTEXM_INIT
#define CHIP_CORTEXM_INIT

void chip_delay_clk(unsigned int clk_count);
unsigned int chip_stack_get_unused_words (void);

#endif
