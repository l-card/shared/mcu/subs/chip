#ifndef CHIP_INIT_H_
#define CHIP_INIT_H_

#include "init/chip_cortexm_init.h"

void chip_init(void);

#endif /* CHIP_INIT_H_ */
