#ifndef CHIP_BOOT_QSPI_H
#define CHIP_BOOT_QSPI_H

#include <stdint.h>

#define CHIP_BOOT_QSPI_CFGBLOCK_OFFSET      0x400

#define CHIP_BOOT_QSPI_NOR_IMAGE_OFFSET     4096


#define CHIP_BOOT_QSPI_CFGBLOCK_TAGVAL      0x42464346
#define CHIP_BOOT_QSPI_CFGBLOCK_VERSION     0x56010000

/* controllerMiscOption */
#define CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_DIFF_CLK_EN     (0x00000001UL << 0U) /* differential clock enable */
#define CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_CK2_EN          (0x00000001UL << 1U) /* CK2 enable, must set to 0 in this silicon */
#define CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_PARA_EN         (0x00000001UL << 2U) /* ParallelModeEnable, must set to 0 for this silicon */
#define CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_WRD_ADDR_EN     (0x00000001UL << 3U) /* wordAddressableEnable */
#define CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_SAFE_CFG_FREQ   (0x00000001UL << 4U) /* Safe Configuration Frequency enable set to 1 for the devices that support DDR Read instructions */
#define CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_PAD_CFG_OVD     (0x00000001UL << 5U) /* Pad Setting Override Enable */
#define CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_DDR_EN          (0x00000001UL << 6U) /* DDR Mode Enable, set to 1 for device supports DDR read command */
/* deviceType */
#define CHIP_BOOT_QSPI_CFGBLOCK_DEVTYPE_NOR                 1
#define CHIP_BOOT_QSPI_CFGBLOCK_DEVTYPE_NAND                2
/* sflashPadType */
#define CHIP_BOOT_QSPI_CFGBLOCK_PADTYPE_SINGLE              1
#define CHIP_BOOT_QSPI_CFGBLOCK_PADTYPE_DOUBLE              2
#define CHIP_BOOT_QSPI_CFGBLOCK_PADTYPE_QUAD                4
#define CHIP_BOOT_QSPI_CFGBLOCK_PADTYPE_OCTAL               8
/* serialClkFreq */
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_30MHZ             1
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_50MHZ             2
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_60MHZ             3
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_75MHZ             4
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_80MHZ             5
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_100MHZ            6
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_133MHZ            7
#define CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_166MHZ            8

/* LUT sequence definition for Serial NOR */
#define CHIP_BOOT_QSPI_CFGBLOCK_LUTSEQID_READ               0 /* Read command Sequence */
#define CHIP_BOOT_QSPI_CFGBLOCK_LUTSEQID_READ_STATUS        1 /* Read Status command */
#define CHIP_BOOT_QSPI_CFGBLOCK_LUTSEQID_WRITE_ENABLE       3 /* Write Enable command sequence */
#define CHIP_BOOT_QSPI_CFGBLOCK_LUTSEQID_ERASE_SECTOR       5 /* Erase Sector Command */
#define CHIP_BOOT_QSPI_CFGBLOCK_LUTSEQID_PAGE_PROG          9 /* Page Program Command */
#define CHIP_BOOT_QSPI_CFGBLOCK_LUTSEQID_ERASE_CHIP        11 /* Full Chip Erase */
#define CHIP_BOOT_QSPI_CFGBLOCK_LUTSEQID_DUMMY             15 /* Dummy Command as needed */


/* Общий заголовок параметров кнофигурации при загрузке с FlexSPI. Должно быть расположено в flash по адресу CHIP_BOOT_QSPI_CFGBLOCK_OFFSET (0x400) */
typedef struct {
    uint32_t tag;                   /* = CHIP_BOOT_QSPI_CFGBLOCK_TAGVAL */
    uint32_t version;               /* = CHIP_BOOT_QSPI_CFGBLOCK_VERSION */
    uint32_t reserved0;
    uint8_t  readSampleClkSrc;      /* значения из QSPI_MCR0_RX_CLK_SRC */
    uint8_t  dataHoldTime;          /* Serial Flash CS Hold Time (recommend 0x03) */
    uint8_t  dataSetupTime;         /* Serial Flash CS Hold Time (recommend 0x03) */
    uint8_t  columnAdressWidth;     /* 3 – For HyperFlash, 12/13 – Serial NAND, 0 – Other devices */
    uint8_t  deviceModeCfgEnable;   /* Device Mode Configuration Enable feature (0/1) */
    uint8_t  reserved1[3];
    uint32_t deviceModeSeq;         /* Sequence parameter for device mode configuration */
    uint32_t deviceModeArg;         /* Device Mode argument, effective only when deviceModeCfgEnable = 1 */
    uint8_t  configCmdEnable;       /* Config Command Enable feature (0/1) */
    uint8_t  reserved2[3];
    uint32_t configCmdSeqs[4];      /* Sequences for Config Command, allow 4 separate configuration command sequences. */
    uint32_t cfgCmdArgs[4];         /* Arguments for each separate configuration command sequences. */
    uint32_t controllerMiscOption;  /* flags from CHIP_BOOT_QSPI_CFGBLOCK_CNTRMISCOPT_xxx */
    uint8_t deviceType;             /* NOR/NAND - значение из CHIP_BOOT_QSPI_CFGBLOCK_DEVTYPE_xxx*/
    uint8_t sflashPadType;          /* data pads count (1,2,4,8) */
    uint8_t serialClkFreq;          /* serial clock freq (значения из CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_xxx) */
    uint8_t lutCustomSeqEnable;     /* 0 – Use pre-defined LUT sequence index and number,  1 - Use LUT sequence parameters provided in this block */
    uint8_t  reserved3[8];
    uint32_t sflashA1Size;          /* NOR - actual size, NAND - actual size * 2 */
    uint32_t sflashA2Size;
    uint32_t sflashB1Size;
    uint32_t sflashB2Size;
    uint32_t csPadSettingOverride;  /* Set to 0 if it is not supported */
    uint32_t sclkPadSettingOverride; /* Set to 0 if it is not supported */
    uint32_t dataPadSettingOverride; /* Set to 0 if it is not supported */
    uint32_t dqsPadSettingOverride;  /* Set to 0 if it is not supported */
    uint32_t timeoutInMs;            /* Maximum wait time during read busy status (0 - disable) */
    uint32_t commandInterval;        /* ns, only for high freq NAND */
    uint32_t dataValidTime;         /* Time from clock edge to data valid edge (for freq < 100 MHZ, clk - DQS), 31-16 - DLLB, 15-0 - DLLA, in 0.1 ns */
    uint16_t busyOffset;            /* busy bit offset, valid range : 0-31  */
    uint16_t busyBitPolarity;       /* 0 – busy bit is 1 if device is busy1 – busy bit is 0 if device is busy */
    uint32_t lookupTable[4][16];    /* Lookup table */
    uint32_t lutCustomSeq[12];      /* Customized LUT sequence */
    uint8_t  reserved4[16];
} t_chip_boot_qspi_cfgblock;

/* Полная конфигурация для загрузки из NOR (deviceType == CHIP_BOOT_QSPI_CFGBLOCK_DEVTYPE_NOR) */
typedef struct {
    t_chip_boot_qspi_cfgblock memCfg; /* The common memory configuration block  */
    uint32_t pageSize;                /* Page size in terms of bytes, not used by ROM */
    uint32_t sectorSize;              /* Sector size in terms of bytes, not used by ROM */
    uint32_t ipCmdSerialClkFreq;      /* 0 - no change from serialClkFreq, или CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_xxx */
    uint8_t  reserved0[52];
}t_chip_boot_qspi_cfgblock_nor;

/* Полная конфигурация для загрузки из NAND (deviceType == CHIP_BOOT_QSPI_CFGBLOCK_DEVTYPE_NAND) */
typedef struct {
    t_chip_boot_qspi_cfgblock memCfg; /* The common memory configuration block  */
    uint32_t pageDataSize;            /* Page size in terms of bytes, usually, it is 2048 or 4096 */
    uint32_t pageTotalSize;           /* It equals to 2 ^ width of column adddress */
    uint32_t pagesPerBlock;           /* Pages in one block */
    uint8_t  bypassReadStatus;        /* 0 – Read Status Register, 1 - Bypass Read status register */
    uint8_t  bypassEccRead;           /* 0 – Perform ECC read, 1 – Bypass ECC read */
    uint8_t  hasMultiPlanes;          /* 0 – Only 1 plane, 1 – Has two planes */
    uint8_t  skippOddBlocks;          /* 0 – Read Odd blocks, 1 – Skip Odd blocks */
    uint8_t  eccCheckCustomEnable;    /* 0 – Use the common ECC check command and ECC related masks,  1 - Use ECC check related masks provided in this configuration block */
    uint8_t  ipCmdSerialClkFreq;      /* 0 - no change from serialClkFreq, или CHIP_BOOT_QSPI_CFGBLOCK_SCLK_FREQ_xxx */
    uint16_t readPageTimeUs;          /* Wait time during page read, this field will take effect on if the bypassReadStatus is set to 1 */
    uint32_t eccStatusMask;           /* ECC Status Mask */
    uint32_t eccFailureMask;          /* ECC Check Failure mask */
    uint32_t blocksPerDevice;         /* Blocks in a Serial NAND */
    uint8_t  reserved0[32];
}t_chip_boot_qspi_cfgblock_nand;


#endif // CHIP_BOOT_QSPI_H
