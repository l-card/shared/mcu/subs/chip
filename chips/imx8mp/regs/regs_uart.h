#ifndef REGS_UART_H
#define REGS_UART_H

#include "chip_ioreg_defs.h"

typedef struct {
    __I uint32_t URXD;              /* UART Receiver Register,                  Address offset: 0x00 */
    uint32_t RESERVED0[15];
    __O uint32_t UTXD;              /* UART Transmitter Register,               Address offset: 0x40 */
    uint32_t RESERVED1[15];
    __IO uint32_t UCR1;             /* UART Control Register 1,                 Address offset: 0x80 */
    __IO uint32_t UCR2;             /* UART Control Register 2,                 Address offset: 0x84 */
    __IO uint32_t UCR3;             /* UART Control Register 3,                 Address offset: 0x88 */
    __IO uint32_t UCR4;             /* UART Control Register 4,                 Address offset: 0x8C */
    __IO uint32_t UFCR;             /* UART FIFO Control Register,              Address offset: 0x90 */
    __IO uint32_t USR1;             /* UART Status Register 1,                  Address offset: 0x94 */
    __IO uint32_t USR2;             /* UART Status Register 2,                  Address offset: 0x98 */
    __IO uint32_t UESC;             /* UART Escape Character Register,          Address offset: 0x9C */
    __IO uint32_t UTIM;             /* UART Escape Timer Register,              Address offset: 0xA0 */
    __IO uint32_t UBIR;             /* UART BRM Incremental Register,           Address offset: 0xA4 */
    __IO uint32_t UBMR;             /* UART BRM Modulator Register,             Address offset: 0xA8 */
    __IO uint32_t UBRC;             /* UART Baud Rate Count Register,           Address offset: 0xAC */
    __IO uint32_t ONEMS;            /* UART One Millisecond Register,           Address offset: 0xB0 */
    __IO uint32_t UTS;              /* UART Test Register,                      Address offset: 0xB4 */
    __IO uint32_t UMCR;             /* UART RS-485 Mode Control Register,       Address offset: 0xB8 */
} CHIP_REGS_UART_T;


#define CHIP_REGS_UART1             ((CHIP_REGS_UART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART1)
#define CHIP_REGS_UART2             ((CHIP_REGS_UART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART2)
#define CHIP_REGS_UART3             ((CHIP_REGS_UART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART3)
#define CHIP_REGS_UART4             ((CHIP_REGS_UART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART4)


/*********** UART Receiver Register (UART_URXD) *******************************/
#define CHIP_REGFLD_UART_URXD_RX_DATA               (0x000000FFUL <<  0U)   /* (RO) Receive data */
#define CHIP_REGFLD_UART_URXD_PR_ERR                (0x00000001UL << 10U)   /* (RO) 9-bit/parity error flag */
#define CHIP_REGFLD_UART_URXD_BRK                   (0x00000001UL << 11U)   /* (RO) BREAK detect */
#define CHIP_REGFLD_UART_URXD_FRM_ERR               (0x00000001UL << 12U)   /* (RO) Frame error */
#define CHIP_REGFLD_UART_URXD_OVRRUN                (0x00000001UL << 13U)   /* (RO) Receive overrun */
#define CHIP_REGFLD_UART_URXD_ERR                   (0x00000001UL << 14U)   /* (RO) Error detect */
#define CHIP_REGFLD_UART_URXD_CHAR_RDY              (0x00000001UL << 15U)   /* (RO) Character ready */

/*********** UART Transmitter Register (UART_UTXD) ****************************/
#define CHIP_REGFLD_UART_UTXD_TX_DATA               (0x000000FFUL <<  0U)   /* (WO) Transmit data */

/*********** UART Control Register 1 (UART_UCR1) ******************************/
#define CHIP_REGFLD_UART_UCR1_UART_EN               (0x00000001UL <<  0U)   /* (RW) UART enable */
#define CHIP_REGFLD_UART_UCR1_DOZE                  (0x00000001UL <<  1U)   /* (RW) UART disable in DOZE state */
#define CHIP_REGFLD_UART_UCR1_AT_DMA_EN             (0x00000001UL <<  2U)   /* (RW) Aging DMA timer enable */
#define CHIP_REGFLD_UART_UCR1_TX_DMA_EN             (0x00000001UL <<  3U)   /* (RW) Transmitter Ready DMA Enable */
#define CHIP_REGFLD_UART_UCR1_SND_BRK               (0x00000001UL <<  4U)   /* (RW) Send BREAK */
#define CHIP_REGFLD_UART_UCR1_RTSD_IEN              (0x00000001UL <<  5U)   /* (RW) RTS Delta Interrupt Enable */
#define CHIP_REGFLD_UART_UCR1_TXMPTY_IEN            (0x00000001UL <<  6U)   /* (RW) Transmitter Empty Interrupt Enable */
#define CHIP_REGFLD_UART_UCR1_IR_EN                 (0x00000001UL <<  7U)   /* (RW) Infrared Interface Enable */
#define CHIP_REGFLD_UART_UCR1_RX_DMA_EN             (0x00000001UL <<  8U)   /* (RW) Receive Ready DMA Enable */
#define CHIP_REGFLD_UART_UCR1_RRDY_IEN              (0x00000001UL <<  9U)   /* (RW) Receiver Ready Interrupt Enable */
#define CHIP_REGFLD_UART_UCR1_ICD                   (0x00000003UL << 10U)   /* (RW) Idle Condition Detect */
#define CHIP_REGFLD_UART_UCR1_ID_IEN                (0x00000001UL << 12U)   /* (RW) Idle Condition Detected Interrupt Enable */
#define CHIP_REGFLD_UART_UCR1_TRDY_IEN              (0x00000001UL << 13U)   /* (RW) Transmitter Ready Interrupt Enable */
#define CHIP_REGFLD_UART_UCR1_ADBR                  (0x00000001UL << 14U)   /* (RW) Automatic Detection of Baud Rate */
#define CHIP_REGFLD_UART_UCR1_AD_IEN                (0x00000001UL << 15U)   /* (RW) Automatic Baud Rate Detection Interrupt Enable */

#define CHIP_REGFLDVAK_UART_UCR1_ICD_4              0 /* Idle for more than 4 frames */
#define CHIP_REGFLDVAK_UART_UCR1_ICD_8              1 /* Idle for more than 8 frames */
#define CHIP_REGFLDVAK_UART_UCR1_ICD_16             2 /* Idle for more than 16 frames */
#define CHIP_REGFLDVAK_UART_UCR1_ICD_32             3 /* Idle for more than 32 frames */

/*********** UART Control Register 2 (UART_UCR2) ******************************/
#define CHIP_REGFLD_UART_UCR2_SRST                  (0x00000001UL <<  0U)   /* (WO) Software reset (reset write 0! must be 1 for other writes!) */
#define CHIP_REGFLD_UART_UCR2_RX_EN                 (0x00000001UL <<  1U)   /* (RW) Receiver enable */
#define CHIP_REGFLD_UART_UCR2_TX_EN                 (0x00000001UL <<  2U)   /* (RW) Transmitter Enable */
#define CHIP_REGFLD_UART_UCR2_AT_IEN                (0x00000001UL <<  3U)   /* (RW) Aging Timer Interrupt Enable */
#define CHIP_REGFLD_UART_UCR2_RTS_IEN               (0x00000001UL <<  4U)   /* (RW) Request to Send Interrupt Enable. */
#define CHIP_REGFLD_UART_UCR2_WS                    (0x00000001UL <<  5U)   /* (RW) Word size */
#define CHIP_REGFLD_UART_UCR2_STPB                  (0x00000001UL <<  6U)   /* (RW) Stop bit count */
#define CHIP_REGFLD_UART_UCR2_PR_OE                 (0x00000001UL <<  7U)   /* (RW) Parity Odd/Even. */
#define CHIP_REGFLD_UART_UCR2_PR_EN                 (0x00000001UL <<  8U)   /* (RW) Parity Enable */
#define CHIP_REGFLD_UART_UCR2_RTS_EC                (0x00000003UL <<  9U)   /* (RW) Request to Send Edge Control. */
#define CHIP_REGFLD_UART_UCR2_ESC_EN                (0x00000001UL << 11U)   /* (RW) Escape sequence detection enable  */
#define CHIP_REGFLD_UART_UCR2_CTS                   (0x00000001UL << 12U)   /* (RW) Clear To Send output value (if CTSC = 0) (0 - high (inactive), 1 - low (active)) */
#define CHIP_REGFLD_UART_UCR2_CTSC                  (0x00000001UL << 13U)   /* (RW) Clear To Send Control (1 - Receiver Control, 0 - CTS bit) */
#define CHIP_REGFLD_UART_UCR2_IRTS                  (0x00000001UL << 14U)   /* (RW) Ignore RTS pins */
#define CHIP_REGFLD_UART_UCR2_ESC_IEN               (0x00000001UL << 15U)   /* (RW) Escape Sequence Interrupt Enable */


#define CHIP_REGFLDVAL_UART_UCR2_WS_7               0 /* 7-bit transmit and receive character length (not including START, STOP or PARITY bits) */
#define CHIP_REGFLDVAL_UART_UCR2_WS_8               1 /* 8-bit transmit and receive character length (not including START, STOP or PARITY bits) */

#define CHIP_REGFLDVAL_UART_UCR2_STPB_1             0 /* The transmitter sends 1 stop bit. The receiver expects 1 or more stop bits. */
#define CHIP_REGFLDVAL_UART_UCR2_STPB_2             1 /* The transmitter sends 2 stop bit. The receiver expects 2 or more stop bits. */

#define CHIP_REGFLDVAL_UART_UCR2_PR_OE_EVEN         0 /* Even parity */
#define CHIP_REGFLDVAL_UART_UCR2_PR_OE_ODD          1 /* Odd parity */

#define CHIP_REGFLDVAL_UART_UCR2_RTS_EC_RISE        0 /* Trigger interrupt on a rising edge */
#define CHIP_REGFLDVAL_UART_UCR2_RTS_EC_FALL        1 /* Trigger interrupt on a falling edge */
#define CHIP_REGFLDVAL_UART_UCR2_RTS_EC_ANY         2 /* Trigger interrupt on a any edge */


/*********** UART Control Register 3 (UART_UCR3) ******************************/
#define CHIP_REGFLD_UART_UCR3_AC_IEN                (0x00000001UL <<  0U)   /* (RW) Autobaud Counter Interrupt Enable */
#define CHIP_REGFLD_UART_UCR3_INVT                  (0x00000001UL <<  1U)   /* (RW) Invert TXD output */
#define CHIP_REGFLD_UART_UCR3_RXD_MUXSEL            (0x00000001UL <<  2U)   /* (RW) RXD Muxed Input Selected (must be set for imx8mp!!) */
#define CHIP_REGFLD_UART_UCR3_DTRD_EN               (0x00000001UL <<  3U)   /* (-) not used for imx8mp */
#define CHIP_REGFLD_UART_UCR3_AWAKE_IEN             (0x00000001UL <<  4U)   /* (RW) Asynchronous WAKE Interrupt Enable */
#define CHIP_REGFLD_UART_UCR3_AIR_IEN               (0x00000001UL <<  5U)   /* (RW) Asynchronous IR WAKE Interrupt Enable */
#define CHIP_REGFLD_UART_UCR3_RXDS_IEN              (0x00000001UL <<  6U)   /* (RW) Receive Status Interrupt Enable */
#define CHIP_REGFLD_UART_UCR3_AD_NIMP               (0x00000001UL <<  7U)   /* (RW) Autobaud Detection Not Improved */
#define CHIP_REGFLD_UART_UCR3_RI                    (0x00000001UL <<  8U)   /* (-) not used for imx8mp (default 1) */
#define CHIP_REGFLD_UART_UCR3_DCD                   (0x00000001UL <<  9U)   /* (-) not used for imx8mp (default 1) */
#define CHIP_REGFLD_UART_UCR3_DSR                   (0x00000001UL << 10U)   /* (-) not used for imx8mp (default 1) */
#define CHIP_REGFLD_UART_UCR3_FRA_ERR_IEN           (0x00000001UL << 11U)   /* (RW) Frame Error Interrupt Enable */
#define CHIP_REGFLD_UART_UCR3_PAR_ERR_IEN           (0x00000001UL << 12U)   /* (RW) Parity Error Interrupt Enable */
#define CHIP_REGFLD_UART_UCR3_DTR_EN                (0x00000001UL << 13U)   /* (-) not used for imx8mp */
#define CHIP_REGFLD_UART_UCR3_DPEC                  (0x00000003UL << 14U)   /* (-) not used for imx8mp */

/*********** UART Control Register 4 (UART_UCR4) ******************************/
#define CHIP_REGFLD_UART_UCR4_DR_IEN                (0x00000001UL <<  0U)   /* (RW) Receive Data Ready Interrupt Enable */
#define CHIP_REGFLD_UART_UCR4_OR_IEN                (0x00000001UL <<  1U)   /* (RW) Receiver Overrun Interrupt Enable */
#define CHIP_REGFLD_UART_UCR4_BK_IEN                (0x00000001UL <<  2U)   /* (RW) BREAK Condition Detected Interrupt Enable */
#define CHIP_REGFLD_UART_UCR4_TC_IEN                (0x00000001UL <<  3U)   /* (RW) TransmitComplete Interrupt Enable */
#define CHIP_REGFLD_UART_UCR4_LP_BYP                (0x00000001UL <<  4U)   /* (RW) Low Power Bypass */
#define CHIP_REGFLD_UART_UCR4_IR_SC                 (0x00000001UL <<  5U)   /* (RW) IR Special Case */
#define CHIP_REGFLD_UART_UCR4_ID_DMA_IEN            (0x00000001UL <<  6U)   /* (RW) DMA IDLE Condition Detected Interrupt Enable */
#define CHIP_REGFLD_UART_UCR4_WK_IEN                (0x00000001UL <<  7U)   /* (RW) WAKE Interrupt Enable */
#define CHIP_REGFLD_UART_UCR4_IR_IEN                (0x00000001UL <<  8U)   /* (RW) Serial Infrared Interrupt Enable. */
#define CHIP_REGFLD_UART_UCR4_INVR                  (0x00000001UL <<  9U)   /* (RW) Invert RXD input. */
#define CHIP_REGFLD_UART_UCR4_CTS_TL                (0x0000003FUL << 10U)   /* (RW) CTS Trigger Level. */

/*********** UART FIFO Control Register (UART_UFCR) ***************************/
#define CHIP_REGFLD_UART_UFCR_RX_TL                 (0x0000003FUL <<  0U)   /* (RW) Receiver Trigger Level (0-32) */
#define CHIP_REGFLD_UART_UFCR_DTE                   (0x00000001UL <<  6U)   /* (RW) DCE(0)/DTE(1) mode select */
#define CHIP_REGFLD_UART_UFCR_RF_DIV                (0x00000007UL <<  7U)   /* (RW) Reference Frequency Divider (1 - 7) */
#define CHIP_REGFLD_UART_UFCR_TX_TL                 (0x0000003FUL << 10U)   /* (RW) Transmitter Trigger Level (0-32) */


#define CHIP_REGFLDVAL_UART_UFCR_RF_DIV_6           0 /* Divide input clock by 6 */
#define CHIP_REGFLDVAL_UART_UFCR_RF_DIV_5           1 /* Divide input clock by 5 */
#define CHIP_REGFLDVAL_UART_UFCR_RF_DIV_4           2 /* Divide input clock by 4 */
#define CHIP_REGFLDVAL_UART_UFCR_RF_DIV_3           3 /* Divide input clock by 3 */
#define CHIP_REGFLDVAL_UART_UFCR_RF_DIV_2           4 /* Divide input clock by 2 */
#define CHIP_REGFLDVAL_UART_UFCR_RF_DIV_1           5 /* Divide input clock by 1 */
#define CHIP_REGFLDVAL_UART_UFCR_RF_DIV_7           6 /* Divide input clock by 7 */

/*********** UART Status Register 1 (UART_USR1) *******************************/
#define CHIP_REGFLD_UART_USR1_SAD                   (0x00000001UL <<  3U)   /* (RW1C) RS-485 Slave Address Detected Interrupt Flag. */
#define CHIP_REGFLD_UART_USR1_AWAKE                 (0x00000001UL <<  4U)   /* (RW1C) Asynchronous WAKE Interrupt Flag. */
#define CHIP_REGFLD_UART_USR1_AIR_INT               (0x00000001UL <<  5U)   /* (RW1C) Asynchronous IR WAKE Interrupt Flag. */
#define CHIP_REGFLD_UART_USR1_RXDS                  (0x00000001UL <<  6U)   /* (RO)   Receiver IDLE Interrupt Flag. */
#define CHIP_REGFLD_UART_USR1_DTRD                  (0x00000001UL <<  7U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_USR1_AGTIM                 (0x00000001UL <<  8U)   /* (RW1C) Ageing Timer Interrupt Flag. */
#define CHIP_REGFLD_UART_USR1_RRDY                  (0x00000001UL <<  9U)   /* (RO)   Receiver Ready Interrupt / DMA Flag (RxFIFO data level is above threshold) */
#define CHIP_REGFLD_UART_USR1_FRAME_ERR             (0x00000001UL << 10U)   /* (RW1C) Frame Error Interrupt Flag */
#define CHIP_REGFLD_UART_USR1_ESCF                  (0x00000001UL << 11U)   /* (RW1C) Escape Sequence Interrupt Flag */
#define CHIP_REGFLD_UART_USR1_RTSD                  (0x00000001UL << 12U)   /* (RW1C) RTS Delta. Indicates whether the RTS_B pin changed state */
#define CHIP_REGFLD_UART_USR1_TRDY                  (0x00000001UL << 13U)   /* (RO)   Transmitter Ready Interrupt / DMA Flag  (TxFIFO emptied below threshold) */
#define CHIP_REGFLD_UART_USR1_RTSS                  (0x00000001UL << 14U)   /* (RO)   RTS_B Pin Status. */
#define CHIP_REGFLD_UART_USR1_PARITY_ERR            (0x00000001UL << 15U)   /* (RW1C) Parity Error Interrupt Flag. */

/*********** UART Status Register 2 (UART_USR2) *******************************/
#define CHIP_REGFLD_UART_USR2_RDR                   (0x00000001UL <<  0U)   /* (RO)   Receive Data Ready (at least 1 character is received). */
#define CHIP_REGFLD_UART_USR2_ORE                   (0x00000001UL <<  1U)   /* (RW1C) Overrun Error */
#define CHIP_REGFLD_UART_USR2_BRCD                  (0x00000001UL <<  2U)   /* (RW1C) BREAK Condition Detected */
#define CHIP_REGFLD_UART_USR2_TXDC                  (0x00000001UL <<  3U)   /* (RO)   Transmitter Complete (TxFIFO and Shift Register is empty) */
#define CHIP_REGFLD_UART_USR2_RTSF                  (0x00000001UL <<  4U)   /* (RW1C) RTS Edge Triggered Interrupt Flag */
#define CHIP_REGFLD_UART_USR2_DCD_IN                (0x00000001UL <<  5U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_USR2_DCD_DELT              (0x00000001UL <<  6U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_USR2_WAKE                  (0x00000001UL <<  7U)   /* (RW1C) Wake (start bit is detected). */
#define CHIP_REGFLD_UART_USR2_IR_INT                (0x00000001UL <<  8U)   /* (RW1C) Serial Infrared Interrupt Flag */
#define CHIP_REGFLD_UART_USR2_RI_IN                 (0x00000001UL <<  9U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_USR2_RI_DELT               (0x00000001UL << 10U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_USR2_AC_ST                 (0x00000001UL << 11U)   /* (RW1C) Autobaud Counter Stopped */
#define CHIP_REGFLD_UART_USR2_IDLE                  (0x00000001UL << 12U)   /* (RW1C) Idle Condition */
#define CHIP_REGFLD_UART_USR2_DTRF                  (0x00000001UL << 13U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_USR2_TXFE                  (0x00000001UL << 14U)   /* (RO)   Transmit Buffer FIFO Empty */
#define CHIP_REGFLD_UART_USR2_ADET                  (0x00000001UL << 15U)   /* (RW1C) Automatic Baud Rate Detect Complete */

/*********** UART Escape Character Register (UART_UESC) ***********************/
#define CHIP_REGFLD_UART_UESC_ESC_CHAR              (0x000000FFUL <<  0U)   /* (RW) UART Escape Character. */

/*********** UART Escape Timer Register (UART_UTIM) ***************************/
#define CHIP_REGFLD_UART_UTIM_TIM                   (0x00000FFFUL <<  0U)   /* (RW) UART Escape Timer. */

/*********** UART BRM Incremental Register (UART_UBIR) ************************/
#define CHIP_REGFLD_UART_UBIR_INC                   (0x0000FFFFUL <<  0U)   /* (RW) Incremental Numerator. */

/*********** UART BRM Modulator Register (UART_UBMR) **************************/
#define CHIP_REGFLD_UART_UBMR_MOD                   (0x0000FFFFUL <<  0U)   /* (RW) Modulator Denominator. */

/*********** UART Baud Rate Count Register (UART_UBRC) ************************/
#define CHIP_REGFLD_UART_UBRC_BCNT                  (0x0000FFFFUL <<  0U)   /* (RW) Baud Rate Count Register. */

/*********** UART One Millisecond Register (UART_ONEMS) ***********************/
#define CHIP_REGFLD_UART_ONEMS_ONEMS                (0x00FFFFFFUL <<  0U)   /* (RW) One Millisecond Register (number of UART BRM internal clock cycles present in one ms) */

/*********** UART Test Register (UART_UTS) ************************************/
#define CHIP_REGFLD_UART_UTS_SOFT_RST               (0x00000001UL <<  0U)   /* (RO) Status of the software reset. */
#define CHIP_REGFLD_UART_UTS_RX_FULL                (0x00000001UL <<  3U)   /* (RO) RxFIFO is full. */
#define CHIP_REGFLD_UART_UTS_TX_FULL                (0x00000001UL <<  4U)   /* (RO) TxFIFO is full. */
#define CHIP_REGFLD_UART_UTS_RX_EMPTY               (0x00000001UL <<  5U)   /* (RO) RxFIFO is empty. */
#define CHIP_REGFLD_UART_UTS_TX_EMPTY               (0x00000001UL <<  6U)   /* (RO) TxFIFO is empty. */
#define CHIP_REGFLD_UART_UTS_RX_DBG                 (0x00000001UL <<  9U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_UTS_LOOP_IR                (0x00000001UL << 10U)   /* (RW) Loop TX and RX for IR Test   */
#define CHIP_REGFLD_UART_UTS_DBG_EN                 (0x00000001UL << 11U)   /* (-) not used for imx8mp  */
#define CHIP_REGFLD_UART_UTS_LOOP                   (0x00000001UL << 12U)   /* (RW) Loop TX and RX for Test  */
#define CHIP_REGFLD_UART_UTS_FRC_PERR               (0x00000001UL << 13U)   /* (RW) Force Parity Error */

/*********** UART RS-485 Mode Control Register (UART_UMCR) ********************/
#define CHIP_REGFLD_UART_UMCR_MD_EN                 (0x00000001UL <<  0U)   /* (RW) 9-bit data or Multidrop Mode (RS-485) Enable. */
#define CHIP_REGFLD_UART_UMCR_SLAM                  (0x00000001UL <<  1U)   /* (RW) RS-485 Slave Address Detect Mode Selection (normal/automatic). */
#define CHIP_REGFLD_UART_UMCR_TXB8                  (0x00000001UL <<  2U)   /* (RW) Transmit RS-485 bit 8 (the ninth bit or 9th bit). */
#define CHIP_REGFLD_UART_UMCR_SAD_IEN               (0x00000001UL <<  3U)   /* (RW) RS-485 Slave Address Detected Interrupt Enable. */
#define CHIP_REGFLD_UART_UMCR_SL_ADDR               (0x000000FFUL <<  8U)   /* (RW) RS-485 Slave Address Character. */


#endif // REGS_UART_H
