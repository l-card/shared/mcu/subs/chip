/************************************************************************
 *
 * defBF609_rom.h
 *
 * File Version 1.1.0.1
 * (c) Copyright 2010-2014 Analog Devices, Inc.  All rights reserved.
 *
 ************************************************************************/

#ifndef _DEF_BF609_ROM_H
#define _DEF_BF609_ROM_H

/*!
 * @defgroup ADSPBF609_PUBLIC_BOOT_HEADERS ADSP-BF609 Public Boot Headers
 * @{
 */
#if defined(_MISRA_2004_RULES) || defined(_MISRA_2012_RULES)
#pragma diag(push)
#ifdef _MISRA_2012_RULES
#pragma diag(suppress:misra2012_rule_1_3)
#pragma diag(suppress:misra2012_rule_5_3:"ADI Header re-uses identifiers")
#pragma diag(suppress:misra2012_rule_5_4:"ADI Header re-uses identifiers")
#pragma diag(suppress:misra2012_rule_5_6:"ADI Header re-uses identifiers")
#pragma diag(suppress:misra2012_rule_5_7:"ADI Header re-uses identifiers")
#pragma diag(suppress:misra2012_rule_6_1)
#pragma diag(suppress:misra2012_rule_8_2)
#pragma diag(suppress:misra2012_rule_11_3:"ADI Header contains casts between integrals and pointers")
#pragma diag(suppress:misra2012_rule_11_4:"ADI Header contains casts between integrals and pointers")
#pragma diag(suppress:misra2012_rule_19_2)
#pragma diag(suppress:misra2012_rule_20_10)
#else
#pragma diag(suppress:misra_rule_5_2)
#pragma diag(suppress:misra_rule_5_6)
#pragma diag(suppress:misra_rule_5_7)
#pragma diag(suppress:misra_rule_6_3)
#pragma diag(suppress:misra_rule_6_4)
#pragma diag(suppress:misra_rule_8_1)
#pragma diag(suppress:misra_rule_8_5)
#pragma diag(suppress:misra_rule_11_3)
#pragma diag(suppress:misra_rule_16_5)
#pragma diag(suppress:misra_rule_18_1)
#pragma diag(suppress:misra_rule_18_4)
#pragma diag(suppress:misra_rule_19_4:"ADI headers contain unbraced identifers")
#pragma diag(suppress:misra_rule_19_13:"ADI header uses ## preprocessor operator")
#pragma diag(suppress:misra_rule_19_7:"ADI header uses function-like macro for bitmasks")
#endif
#endif


/* do not add casts to literal constants in assembly code */
#if defined(_LANGUAGE_ASM) || defined(__ASSEMBLER__)
#define _ADI_MSK( mask, type ) (mask) /*!< Make a bitmask */
#else
#include <stdint.h>
#define _ADI_MSK( mask, type ) ((type)(mask)) /*!< Make a bitmask */
#endif



/* ==================================================
        Boot ROM Constants and Revision Information Registers
   ================================================== */
/*!
 * @addtogroup BOOT_CONST Boot ROM constants and Revision Information
 * @brief Boot ROM Constants and Revision Information
 * @details
 * @{
 */

/* =========================
        CONST
   ========================= */

#define REG_ROM_REVISION        		0xC8000040	/*!< ROM Revision */
#define REG_ROM_CHKSUM          		0xC8000044  /*!< ROM 32-bit XOR Checksum */
#define REG_ROM_ZEROS           		0xC8000048  /*!< All Zeros */
#define REG_ROM_ONES            		0xC800004C  /*!< All Ones */
#define REG_ROM_DATECODE        		0xC8000050  /*!< ROM Date Code */
#define REG_ROM_TIMECODE        		0xC8000054  /*!< ROM Time Code */
#define REG_ROM_SBST_REVISION   		0xC8000060  /*!< ROM SBST Revision */

/*!
 * @addtogroup BOOT_CONST_REVISION Boot ROM Revision
 * @{
 */
/*!
 * @addtogroup BOOT_CONST_REVISION_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BK_REVISION			0   /*!< ROM Project Revision*/
#define BITP_ROM_BK_REVISION_ID			24  /*!< ROM Project Revision Global ID */
#define BITP_ROM_BK_REVISION_PROJECT	16  /*!< ROM Project Revision Project ID */
#define BITP_ROM_BK_REVISION_VERSION	8   /*!< ROM Project Revision Version ID */
#define BITP_ROM_BK_REVISION_UPDATE		0   /*!< ROM Project Revision Update ID */

#define BITM_ROM_BK_REVISION            (_ADI_MSK(0xFFFFFFFF,uint32_t)) /*!< ROM Project Revision*/
#define BITM_ROM_BK_REVISION_ID         (_ADI_MSK(0xFF000000,uint32_t)) /*!< ROM Project Revision Global ID */
#define BITM_ROM_BK_REVISION_PROJECT    (_ADI_MSK(0x00FF0000,uint32_t)) /*!< ROM Project Revision Project ID */
#define BITM_ROM_BK_REVISION_VERSION    (_ADI_MSK(0x0000FF00,uint32_t)) /*!< ROM Project Revision Version ID */
#define BITM_ROM_BK_REVISION_UPDATE     (_ADI_MSK(0x000000FF,uint32_t)) /*!< ROM Project Revision Update ID */

#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define ENUM_ROM_BK_REVISION            (_ADI_MSK(0xAD101002,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision*/
#define ENUM_ROM_BK_REVISION_ID         (_ADI_MSK(0xAD000000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Global ID */
#define ENUM_ROM_BK_REVISION_PROJECT    (_ADI_MSK(0x00100000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Project ID */
#define ENUM_ROM_BK_REVISION_VERSION    (_ADI_MSK(0x00001000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Version ID */
#define ENUM_ROM_BK_REVISION_UPDATE     (_ADI_MSK(0x00000002,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Update ID */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define ENUM_ROM_BK_REVISION            (_ADI_MSK(0xAD101002,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision*/
#define ENUM_ROM_BK_REVISION_ID         (_ADI_MSK(0xAD000000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Global ID */
#define ENUM_ROM_BK_REVISION_PROJECT    (_ADI_MSK(0x00100000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Project ID */
#define ENUM_ROM_BK_REVISION_VERSION    (_ADI_MSK(0x00001000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Version ID */
#define ENUM_ROM_BK_REVISION_UPDATE     (_ADI_MSK(0x00000002,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Project Revision Update ID */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#define ENUM_ROM_BK_REVISION            (_ADI_MSK(0xAD101001,uint32_t)) /*!< Silicon Revision 0.0 ROM Project Revision*/
#define ENUM_ROM_BK_REVISION_ID         (_ADI_MSK(0xAD000000,uint32_t)) /*!< Silicon Revision 0.0 ROM Project Revision Global ID */
#define ENUM_ROM_BK_REVISION_PROJECT    (_ADI_MSK(0x00100000,uint32_t)) /*!< Silicon Revision 0.0 ROM Project Revision Project ID */
#define ENUM_ROM_BK_REVISION_VERSION    (_ADI_MSK(0x00001000,uint32_t)) /*!< Silicon Revision 0.0 ROM Project Revision Version ID */
#define ENUM_ROM_BK_REVISION_UPDATE     (_ADI_MSK(0x00000001,uint32_t)) /*!< Silicon Revision 0.0 ROM Project Revision Update ID */
#else
# error "Unknown silicon revision"
#endif
/*! @} */
/*! @} */

/*!
 * @addtogroup BOOT_CONST_CHKSUM Boot ROM Checksum
 * @{
 */
/*!
 * @addtogroup BOOT_CONST_CHKSUM_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_CHKSUM		    0    /*!< ROM 32-bit XOR Checksum */
#define BITM_ROM_CHKSUM         (_ADI_MSK(0xFFFFFFFF,uint32_t)) /*!< ROM 32-bit XOR Checksum */

#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define ENUM_ROM_CHKSUM         (_ADI_MSK(0x215C5DCE,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM 32-bit XOR Checksum */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define ENUM_ROM_CHKSUM         (_ADI_MSK(0x215C5DCE,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM 32-bit XOR Checksum */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#define ENUM_ROM_CHKSUM         (_ADI_MSK(0x0D66794A,uint32_t)) /*!< Silicon Revision 0.0 ROM 32-bit XOR Checksum */
#else
# error "Unknown silicon revision"
#endif
/*! @} */
/*! @} */

/*!
 * @addtogroup BOOT_CONST_ZEROS Boot ROM Zeros
 * @{
 */
/*!
 * @addtogroup BOOT_CONST_ZEROS_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_ZEROS		    0 /*!< All Zeros */
#define BITM_ROM_ZEROS         (_ADI_MSK(0xFFFFFFFF,uint32_t)) /*!< All Zeros */
#define ENUM_ROM_ZEROS         (_ADI_MSK(0x00000000,uint32_t)) /*!< All Zeros */
/*! @} */
/*! @} */


/*!
 * @addtogroup BOOT_CONST_ONES Boot ROM Ones
 * @{
 */
/*!
 * @addtogroup BOOT_CONST_ONES_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_ONES		    0 /*!< All Ones */
#define BITM_ROM_ONES         (_ADI_MSK(0xFFFFFFFF,uint32_t)) /*!< All Ones */
#define ENUM_ROM_ONES         (_ADI_MSK(0xFFFFFFFF,uint32_t)) /*!< All Ones */
/*! @} */
/*! @} */

/*!
 * @addtogroup BOOT_CONST_DATECODE Boot ROM date code
 * @{
 */
/*!
 * @addtogroup BOOT_CONST_DATECODE_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BK_DATECODE            0  /*!< ROM Date Code */
#define BITP_ROM_BK_DATECODE_YEAR       16 /*!< ROM Date Code Year */
#define BITP_ROM_BK_DATECODE_MONTH      8  /*!< ROM Date Code Month */
#define BITP_ROM_BK_DATECODE_DAY        0  /*!< ROM Date Code Day */
#define BITM_ROM_BK_DATECODE            (_ADI_MSK(0xFFFFFFFF,uint32_t)) /*!< ROM Date Code */
#define BITM_ROM_BK_DATECODE_YEAR       (_ADI_MSK(0xFFFF0000,uint32_t)) /*!< ROM Date Code Year */
#define BITM_ROM_BK_DATECODE_MONTH      (_ADI_MSK(0x0000FF00,uint32_t)) /*!< ROM Date Code Month */
#define BITM_ROM_BK_DATECODE_DAY        (_ADI_MSK(0x000000FF,uint32_t)) /*!< ROM Date Code Day */

#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define ENUM_ROM_BK_DATECODE            (_ADI_MSK(0x20120822,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code */
#define ENUM_ROM_BK_DATECODE_YEAR       (_ADI_MSK(0x20120000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code Year */
#define ENUM_ROM_BK_DATECODE_MONTH      (_ADI_MSK(0x00000800,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code Month */
#define ENUM_ROM_BK_DATECODE_DAY        (_ADI_MSK(0x00000022,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code Day */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define ENUM_ROM_BK_DATECODE            (_ADI_MSK(0x20120822,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code */
#define ENUM_ROM_BK_DATECODE_YEAR       (_ADI_MSK(0x20120000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code Year */
#define ENUM_ROM_BK_DATECODE_MONTH      (_ADI_MSK(0x00000800,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code Month */
#define ENUM_ROM_BK_DATECODE_DAY        (_ADI_MSK(0x00000022,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Date Code Day */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#define ENUM_ROM_BK_DATECODE            (_ADI_MSK(0x20110505,uint32_t)) /*!< Silicon Revision 0.0 ROM Date Code */
#define ENUM_ROM_BK_DATECODE_YEAR       (_ADI_MSK(0x20110000,uint32_t)) /*!< Silicon Revision 0.0 ROM Date Code Year */
#define ENUM_ROM_BK_DATECODE_MONTH      (_ADI_MSK(0x00000500,uint32_t)) /*!< Silicon Revision 0.0 ROM Date Code Month */
#define ENUM_ROM_BK_DATECODE_DAY        (_ADI_MSK(0x00000005,uint32_t)) /*!< Silicon Revision 0.0 ROM Date Code Day */
#else
# error "Unknown silicon revision"
#endif
/*! @} */
/*! @} */

/*!
 * @addtogroup BOOT_CONST_TIMECODE Boot ROM time code
 * @{
 */
/*!
 * @addtogroup BOOT_CONST_TIMECODE_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BK_TIMECODE            0  /*!< ROM Time Code */
#define BITP_ROM_BK_TIMECODE_HOURS      16 /*!< ROM Time Code Hours */
#define BITP_ROM_BK_TIMECODE_MINS       8  /*!< ROM Time Code Minutes */
#define BITP_ROM_BK_TIMECODE_SECS       0  /*!< ROM Time Code Seconds */
#define BITM_ROM_BK_TIMECODE            (_ADI_MSK(0x00FFFFFF,uint32_t)) /*!< ROM Time Code */
#define BITM_ROM_BK_TIMECODE_HOURS      (_ADI_MSK(0x00FF0000,uint32_t)) /*!< ROM Time Code Hours */
#define BITM_ROM_BK_TIMECODE_MINS       (_ADI_MSK(0x0000FF00,uint32_t)) /*!< ROM Time Code Minutes */
#define BITM_ROM_BK_TIMECODE_SECS       (_ADI_MSK(0x000000FF,uint32_t)) /*!< ROM Time Code Seconds */
#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define ENUM_ROM_BK_TIMECODE            (_ADI_MSK(0x00145802,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code */
#define ENUM_ROM_BK_TIMECODE_HOURS      (_ADI_MSK(0x00140000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code Hours */
#define ENUM_ROM_BK_TIMECODE_MINS       (_ADI_MSK(0x00005800,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code Minutes */
#define ENUM_ROM_BK_TIMECODE_SECS       (_ADI_MSK(0x00000002,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code Seconds */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define ENUM_ROM_BK_TIMECODE            (_ADI_MSK(0x00145802,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code */
#define ENUM_ROM_BK_TIMECODE_HOURS      (_ADI_MSK(0x00140000,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code Hours */
#define ENUM_ROM_BK_TIMECODE_MINS       (_ADI_MSK(0x00005800,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code Minutes */
#define ENUM_ROM_BK_TIMECODE_SECS       (_ADI_MSK(0x00000001,uint32_t)) /*!< Silicon Revision 0.1 / 0.2 ROM Time Code Seconds */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#define ENUM_ROM_BK_TIMECODE            (_ADI_MSK(0x00121350,uint32_t)) /*!< Silicon Revision 0.0 ROM Time Code */
#define ENUM_ROM_BK_TIMECODE_HOURS      (_ADI_MSK(0x00120000,uint32_t)) /*!< Silicon Revision 0.0 ROM Time Code Hours */
#define ENUM_ROM_BK_TIMECODE_MINS       (_ADI_MSK(0x00001300,uint32_t)) /*!< Silicon Revision 0.0 ROM Time Code Minutes */
#define ENUM_ROM_BK_TIMECODE_SECS       (_ADI_MSK(0x00000050,uint32_t)) /*!< Silicon Revision 0.0 ROM Time Code Seconds */
#else
# error "Unknown silicon revision"
#endif
/*! @} */
/*! @} */

/*!
 * @addtogroup BOOT_CONST_SBST_REVISION Boot ROM SBST revision
 * @{
 */
/*!
 * @addtogroup BOOT_CONST_SBST_REVISION_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_SBST_REVISION		    0 /*!< Software Builtin Self Test Revision */
#define BITM_ROM_SBST_REVISION         (_ADI_MSK(0xFFFFFFFF,uint32_t)) /*!< Software Builtin Self Test Revision */
#define ENUM_ROM_SBST_REVISION         (_ADI_MSK(0x0308112C,uint32_t)) /*!< Software Builtin Self Test Revision */
/*! @} */
/*! @} */
/*! @} */

/*********************************************************************************************/
/*                                                                                           */
/*   Jump Table Entries                                                                      */
/*                                                                                           */
/*********************************************************************************************/
/*!
 * @addtogroup BOOT_JUMPTABLE Boot ROM Jumptable
 * @brief The bootrom Jumptable entries
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_JUMPTABLE_ADDRESSES API entry points
 * @{
 */
#define FUNC_ROM_C0MAIN                 0xC8000000 /*!< Core 0 entry point to the boot rom */
#define FUNC_ROM_GETADDR                0xC8000004 /*!< Entry point to find various lookup table addresses */
#define FUNC_ROM_BOOTKERNEL             0xC8000008 /*!< Entry point to call just the boot kernel */
#define FUNC_ROM_BOOT                   0xC800000C /*!< Entry point to call a boot mode */
#define FUNC_ROM_C1MAIN                 0xC8000010 /*!< Core 1 entry point to the boot rom */
#define FUNC_ROM_CRCINIT                0xC8000014 /*!< CRC Initialization Routine */
#define FUNC_ROM_FWDCFG                 0xC8000018 /*!< Boot forward configuration routine */
#define FUNC_ROM_EXEC                   0xC800001C /*!< Routine to release a core from reset and vector to a defined location */
#define FUNC_ROM_MEMCPY                 0xC8000024 /*!< DMA memory copy routine */
#define FUNC_ROM_MEMCRC                 0xC8000028 /*!< Memory to memory DMA with CRC verification */
#define FUNC_ROM_CRCLUT                 0xC800002C /*!< CRC Look up table initialization */
#define FUNC_ROM_MEMFILL                0xC8000034 /*!< Memory fill routine using DMA */
#define FUNC_ROM_MEMCMP                 0xC8000038 /*!< Memory compare routine using the memory DMA and the CRC peripheral */
#define FUNC_ROM_SYSCONTROL             0xC8000080 /*!< Syscontrol for CGU and DMC restoration from wakeup */
#define FUNC_ROM_SBST                   0xC8000084 /*!< Software Builtin Self Test */
/*! @} */
/*! @} */



/* ==================================================
        Boot ROM Block Header
   ================================================== */
/*!
 * @addtogroup BOOT_KERNEL Boot ROM Boot Kernel
 * @brief Boot ROM boot kernel processes the boot stream and distributes the data throughout the device
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_KERNEL_HDR Block Header
 * @brief Boot Stream Block Header
 * @details The boot block header consists of 4 fields.
 * The block code contains all the flags for the boot block that instructs the kernel how to
 * process the block. The target address is used to either store the destination address of
 * the code or data to be loader or for certain block types it may contain the address of a
 * function that is to be called. The byte count contains the number of bytes that are to be
 * processed for the given block type and the argument field is used to pass parameters
 * to functions for specific block types.
 * @{
 */

/*!
 * @addtogroup BOOT_KERNEL_HDR_BCODE Block Code
 * @{
 */
/*!
 * @addtogroup BOOT_KERNEL_HDR_BCODE_BITS bitmasks, positions and enumerations
 * @{
 */
/*********************************************************************************************/
/*                                                                                           */
/*   Boot ROM Block Header                                                                   */
/*                                                                                           */
/*********************************************************************************************/
#define BITP_ROM_HDR_SGN              24 /*!< The signature of the boot block */
#define BITP_ROM_HDR_CHK              16 /*!< 8-bit XOR checksum of the 16-byte block header */
#define BITP_ROM_HDR_FINAL            15 /*!< final block in stream */
#define BITP_ROM_HDR_FIRST            14 /*!< first block in stream */
#define BITP_ROM_HDR_INDIRECT         13 /*!< load data via intermediate buffer */
#define BITP_ROM_HDR_IGNORE           12 /*!< ignore block payload */
#define BITP_ROM_HDR_INIT             11 /*!< call initcode routine */
#define BITP_ROM_HDR_CALLBACK         10 /*!< call callback routine */
#define BITP_ROM_HDR_QUICKBOOT        9 /*!< boot block only when BFLAG_WAKEUP=0 */
#define BITP_ROM_HDR_FILL             8 /*!< fill memory with 32-bit argument value */
#define BITP_ROM_HDR_FORWARD          7 /*!< forward block */
#define BITP_ROM_HDR_AUX              5 /*!< load auxiliary header -- reserved */
#define BITP_ROM_HDR_SAVE             4 /*!< save block on power down -- reserved */
#define BITP_ROM_HDR_BFLAG            4 /*!< First bit position of the block code flags */
#define BITP_ROM_HDR_CODE             0 /*!< The code used for device auto-detection */

#define BITM_ROM_HDR_SGN              (_ADI_MSK(0xFF000000,uint32_t)) /*!< The signature of the boot block */
#define BITM_ROM_HDR_CHK              (_ADI_MSK(0x00FF0000,uint32_t)) /*!< 8-bit XOR checksum of the 16-byte block header */
#define BITM_ROM_HDR_FINAL            (_ADI_MSK(0x00008000,uint32_t)) /*!< final block in stream */
#define BITM_ROM_HDR_FIRST            (_ADI_MSK(0x00004000,uint32_t)) /*!< first block in stream */
#define BITM_ROM_HDR_INDIRECT         (_ADI_MSK(0x00002000,uint32_t)) /*!< load data via intermediate buffer */
#define BITM_ROM_HDR_IGNORE           (_ADI_MSK(0x00001000,uint32_t)) /*!< ignore block payload */
#define BITM_ROM_HDR_INIT             (_ADI_MSK(0x00000800,uint32_t)) /*!< call initcode routine */
#define BITM_ROM_HDR_CALLBACK         (_ADI_MSK(0x00000400,uint32_t)) /*!< call callback routine */
#define BITM_ROM_HDR_QUICKBOOT        (_ADI_MSK(0x00000200,uint32_t)) /*!< boot block only when BFLAG_WAKEUP=0 */
#define BITM_ROM_HDR_FILL             (_ADI_MSK(0x00000100,uint32_t)) /*!< fill memory with 32-bit argument value */
#define BITM_ROM_HDR_FORWARD          (_ADI_MSK(0x00000080,uint32_t)) /*!< forward block */
#define BITM_ROM_HDR_AUX              (_ADI_MSK(0x00000020,uint32_t)) /*!< load auxiliary header -- reserved */
#define BITM_ROM_HDR_SAVE             (_ADI_MSK(0x00000010,uint32_t)) /*!< save block on power down -- reserved */
#define BITM_ROM_HDR_BFLAG            (_ADI_MSK(0x0000FFF0,uint32_t)) /*!< First bit position of the block code flags */
#define BITM_ROM_HDR_CODE             (_ADI_MSK(0x0000000F,uint32_t)) /*!< The code used for device auto-detection */

/*! @} */
/*! @} */

/*!
 * @addtogroup BOOT_KERNEL_HDR_TARGETADDRESS Target Address
 * @{
 */

/*! @} */

/*!
 * @addtogroup BOOT_KERNEL_HDR_BYTECOUNT Byte Count
 * @{
 */

/*! @} */
/*!
 * @addtogroup BOOT_KERNEL_HDR_ARGUMENT Argument
 * @{
 */

/*! @} */
/*! @} */

/*********************************************************************************************/
/*                                                                                           */
/*   Boot Flags (part of block header's block code field)                                    */
/*                                                                                           */
/*********************************************************************************************/
/* ==================================================
        Boot Kernel Block Flags
   ================================================== */
/*!
 * @addtogroup BOOT_KERNEL_FLAGS Boot Kernel Block Processing Flags
 * @brief Instructs the bootkernel on how to process the block
 * @details The boot kernel flags are a combination of some global flags internal to the
 * boot kernel and the block specific flags as defined in the block headers block code.
 * @{
 */
/*!
 * @addtogroup BOOT_KERNEL_FLAGS_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BFLAG_FINAL         15   /*!< final block in stream */
#define BITP_ROM_BFLAG_FIRST         14   /*!< first block in stream */
#define BITP_ROM_BFLAG_INDIRECT      13   /*!< load data via intermediate buffer */
#define BITP_ROM_BFLAG_IGNORE        12   /*!< ignore block payload */
#define BITP_ROM_BFLAG_INIT          11   /*!< call initcode routine */
#define BITP_ROM_BFLAG_CALLBACK      10   /*!< call callback routine */
#define BITP_ROM_BFLAG_QUICKBOOT     9   /*!< boot block only when BFLAG_WAKEUP=0 */
#define BITP_ROM_BFLAG_FILL          8   /*!< fill memory with 32-bit argument value */
#define BITP_ROM_BFLAG_FORWARD       7   /*!< forward block */
#define BITP_ROM_BFLAG_AUX           5   /*!< load auxiliary header -- reserved */
#define BITP_ROM_BFLAG_SAVE          4   /*!< save block on power down -- reserved */

#define BITM_ROM_BFLAG_FINAL         (_ADI_MSK(0x00008000,uint32_t))   /*!< final block in stream */
#define BITM_ROM_BFLAG_FIRST         (_ADI_MSK(0x00004000,uint32_t))   /*!< first block in stream */
#define BITM_ROM_BFLAG_INDIRECT      (_ADI_MSK(0x00002000,uint32_t))   /*!< load data via intermediate buffer */
#define BITM_ROM_BFLAG_IGNORE        (_ADI_MSK(0x00001000,uint32_t))   /*!< ignore block payload */
#define BITM_ROM_BFLAG_INIT          (_ADI_MSK(0x00000800,uint32_t))   /*!< call initcode routine */
#define BITM_ROM_BFLAG_CALLBACK      (_ADI_MSK(0x00000400,uint32_t))   /*!< call callback routine */
#define BITM_ROM_BFLAG_QUICKBOOT     (_ADI_MSK(0x00000200,uint32_t))   /*!< boot block only when BFLAG_WAKEUP=0 */
#define BITM_ROM_BFLAG_FILL          (_ADI_MSK(0x00000100,uint32_t))   /*!< fill memory with 32-bit argument value */
#define BITM_ROM_BFLAG_FORWARD       (_ADI_MSK(0x00000080,uint32_t))   /*!< forward block */
#define BITM_ROM_BFLAG_AUX           (_ADI_MSK(0x00000020,uint32_t))   /*!< load auxiliary header -- reserved */
#define BITM_ROM_BFLAG_SAVE          (_ADI_MSK(0x00000010,uint32_t))   /*!< save block on power down -- reserved */


/*********************************************************************************************/
/*                                                                                           */
/*   Boot Flags (global flags for pFlag word)                                                */
/*                                                                                           */
/*********************************************************************************************/
#define BITP_ROM_BFLAG_NORESTORE     31   /*!< do not restore MMR register when done */
#define BITP_ROM_BFLAG_NORESET       30   /*!< issue system reset when done */
#define BITP_ROM_BFLAG_RETURN        29   /*!< issue RTS instead of jumping to EVT1 vector */
#define BITP_ROM_BFLAG_NEXTDXE       28   /*!< parse stream via Next DXE pointer */
#define BITP_ROM_BFLAG_WAKEUP        27   /*!< WURESET bit was a '1', enable quickboot */
#define BITP_ROM_BFLAG_SLAVE         26   /*!< boot mode is a slave mode */
#define BITP_ROM_BFLAG_PERIPHERAL    25   /*!< boot mode is a peripheral mode */
#define BITP_ROM_BFLAG_DATAREAD      22   /*!< don't follow boot stream format */
#define BITP_ROM_BFLAG_HEADER        21   /*!< pLoadFunction call for block header */
#define BITP_ROM_BFLAG_NOFIRSTHEADER 20   /*!< don't load first header */
#define BITP_ROM_BFLAG_PAGEMODE      19   /*!< page mode, replaced HDRINDIRECT */
#define BITP_ROM_BFLAG_HOOK          18   /*!< call hook routine after initialization */
#define BITP_ROM_BFLAG_TEST          17   /*!< factory testing */

#define BITM_ROM_BFLAG_NORESTORE     (_ADI_MSK(0x80000000,uint32_t))   /*!< do not restore MMR register when done */
#define BITM_ROM_BFLAG_NORESET       (_ADI_MSK(0x40000000,uint32_t))   /*!< issue system reset when done */
#define BITM_ROM_BFLAG_RETURN        (_ADI_MSK(0x20000000,uint32_t))   /*!< issue RTS instead of jumping to EVT1 vector */
#define BITM_ROM_BFLAG_NEXTDXE       (_ADI_MSK(0x10000000,uint32_t))   /*!< parse stream via Next DXE pointer */
#define BITM_ROM_BFLAG_WAKEUP        (_ADI_MSK(0x08000000,uint32_t))   /*!< WURESET bit was a '1', enable quickboot */
#define BITM_ROM_BFLAG_SLAVE         (_ADI_MSK(0x04000000,uint32_t))   /*!< boot mode is a slave mode */
#define BITM_ROM_BFLAG_PERIPHERAL    (_ADI_MSK(0x02000000,uint32_t))   /*!< boot mode is a peripheral mode */
#define BITM_ROM_BFLAG_DATAREAD      (_ADI_MSK(0x00400000,uint32_t))   /*!< don't follow boot stream format */
#define BITM_ROM_BFLAG_HEADER        (_ADI_MSK(0x00200000,uint32_t))   /*!< pLoadFunction call for block header */
#define BITM_ROM_BFLAG_NOFIRSTHEADER (_ADI_MSK(0x00100000,uint32_t))   /*!< don't load first header */
#define BITM_ROM_BFLAG_PAGEMODE      (_ADI_MSK(0x00080000,uint32_t))   /*!< page mode, replaced HDRINDIRECT */
#define BITM_ROM_BFLAG_HOOK          (_ADI_MSK(0x00040000,uint32_t))   /*!< call hook routine after initialization */
#define BITM_ROM_BFLAG_TEST          (_ADI_MSK(0x00020000,uint32_t))   /*!< factory testing */
/*! @} */
/*! @} */

/*********************************************************************************************/
/*                                                                                           */
/*   Boot Commands                                                                           */
/*                                                                                           */
/*********************************************************************************************/
/*!
 * @addtogroup BOOT_KERNEL_BCMD Boot Kernel Boot Command
 * @brief Provides instruction to the bootkernel on what peripheral to boot from and the initial configuration to use.
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_KERNEL_BCMD_BITS bitmasks, positions and enumerations
 * @brief Provides detials on all the bitmasks, bitposition and the enumerations of the generic boot command.
 * @details The generic boot command only defines the generic information common to most of the boot modes.
 * Each individual boot mode uses it's own dedicated boot command that contains configuration information specific
 * to that particular boot peripheral.
 * @{
 */
#define BITP_ROM_BCMD_BMODECFG    12          /*!< Re-purposed field specific to each boot mode */
#define BITP_ROM_BCMD_DEVENUM     8           /*!< enumeration/instance of boot source device, SPI0, SPI1, .. */
#define BITP_ROM_BCMD_NOAUTO      6           /*!< skip automatic device detection */
#define BITP_ROM_BCMD_NOCFG       5           /*!< skip device configuration and pinmuxing */
#define BITP_ROM_BCMD_HOST        4           /*!< slave mode enable */
#define BITP_ROM_BCMD_DEVICE      0           /*!< boot source device SPI, Flash, ...*/

#define BITM_ROM_BCMD_BMODECFG         (_ADI_MSK(0xFFFFF000,uint32_t))  /*!< bmode specific configuration */
#define BITM_ROM_BCMD_DEVENUM          (_ADI_MSK(0x00000F00,uint32_t))  /*!< enumeration/instance of boot source device, SPI0, SPI1, .. */
#define ENUM_ROM_BCMD_DEVENUM_0        0x00000000 /*!< device enumeration 0 */
#define ENUM_ROM_BCMD_DEVENUM_1        0x00000100 /*!< device enumeration 1 */
#define ENUM_ROM_BCMD_DEVENUM_2        0x00000200 /*!< device enumeration 2 */
#define ENUM_ROM_BCMD_DEVENUM_3        0x00000300 /*!< device enumeration 3 */
#define ENUM_ROM_BCMD_DEVENUM_4        0x00000400 /*!< device enumeration 4 */
#define ENUM_ROM_BCMD_DEVENUM_5        0x00000500 /*!< device enumeration 5 */
#define ENUM_ROM_BCMD_DEVENUM_6        0x00000600 /*!< device enumeration 6 */
#define ENUM_ROM_BCMD_DEVENUM_7        0x00000700 /*!< device enumeration 7 */
#define ENUM_ROM_BCMD_DEVENUM_8        0x00000800 /*!< device enumeration 8 */
#define ENUM_ROM_BCMD_DEVENUM_9        0x00000900 /*!< device enumeration 9 */
#define ENUM_ROM_BCMD_DEVENUM_10       0x00000A00 /*!< device enumeration 10 */
#define ENUM_ROM_BCMD_DEVENUM_11       0x00000B00 /*!< device enumeration 11 */
#define ENUM_ROM_BCMD_DEVENUM_12       0x00000C00 /*!< device enumeration 12 */
#define ENUM_ROM_BCMD_DEVENUM_13       0x00000D00 /*!< device enumeration 13 */
#define ENUM_ROM_BCMD_DEVENUM_14       0x00000E00 /*!< device enumeration 14 */
#define ENUM_ROM_BCMD_DEVENUM_15       0x00000F00 /*!< device enumeration 15 */

#define BITM_ROM_BCMD_NOAUTO           (_ADI_MSK(0x00000040,uint32_t))  /*!< skip automatic device detection */
#define BITM_ROM_BCMD_NOCFG            (_ADI_MSK(0x00000020,uint32_t))  /*!< skip default configuration */
#define BITM_ROM_BCMD_HOST             (_ADI_MSK(0x00000010,uint32_t))  /*!< slave mode enable*/

#define BITM_ROM_BCMD_DEVICE           (_ADI_MSK(0x0000000F,uint32_t))  /*!< boot source device SPI, Flash, ...*/
#define ENUM_ROM_BCMD_DEVICE_NONE      0x00000000 /*!< NoBoot mode */
#define ENUM_ROM_BCMD_DEVICE_MEMORY    0x00000001 /*!< Boot from memory via the SMC */
#define ENUM_ROM_BCMD_DEVICE_SPI       0x00000002 /*!< SPI Master/Slave boot mode*/
#define ENUM_ROM_BCMD_DEVICE_UART      0x00000003 /*!< UART boot mode*/
#define ENUM_ROM_BCMD_DEVICE_LPT       0x00000004 /*!< Linkport boot mode*/
#define ENUM_ROM_BCMD_DEVICE_RSI       0x00000005 /*!< RSI boot mode*/
/*! @} */
/*! @} */

/*********************************************************************************************/
/*                                                                                           */
/*   Boot Forward                                                                            */
/*                                                                                           */
/*********************************************************************************************/
/*!
 * @addtogroup BOOT_FORWARD Boot Forward
 * @brief Functionality to forwards parts of a boot stream to another device via the supported boot forward peripherals
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_FORWARD_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BFWD_FWDCFG          12  /*!< Peripheral mode specific forward configuration */
#define BITP_ROM_BFWD_DEVENUM         8   /*!< Device enumeration to forward to */
#define BITP_ROM_BFWD_CLEANUP         7   /*!< Boot forward cleanup enable */
#define BITP_ROM_BFWD_DEVICE          0   /*!< The peripheral to use for the forward operation */

#define BITM_ROM_BFWD_FWDCFG               (_ADI_MSK(0x0000F000,uint32_t)) /*!< Peripheral mode specific forward configuration */

#define BITM_ROM_BFWD_DEVENUM              (_ADI_MSK(0x00000F00,uint32_t)) /*!< Device enumeration to forward to */
#define ENUM_ROM_BFWD_DEVENUM_0            0x00000000 /*!< device enumeration 0 */
#define ENUM_ROM_BFWD_DEVENUM_1            0x00000100 /*!< device enumeration 1 */
#define ENUM_ROM_BFWD_DEVENUM_2            0x00000200 /*!< device enumeration 2 */
#define ENUM_ROM_BFWD_DEVENUM_3            0x00000300 /*!< device enumeration 3 */
#define ENUM_ROM_BFWD_DEVENUM_4            0x00000400 /*!< device enumeration 4 */
#define ENUM_ROM_BFWD_DEVENUM_5            0x00000500 /*!< device enumeration 5 */
#define ENUM_ROM_BFWD_DEVENUM_6            0x00000600 /*!< device enumeration 6 */
#define ENUM_ROM_BFWD_DEVENUM_7            0x00000700 /*!< device enumeration 7 */
#define ENUM_ROM_BFWD_DEVENUM_8            0x00000800 /*!< device enumeration 8 */
#define ENUM_ROM_BFWD_DEVENUM_9            0x00000900 /*!< device enumeration 9 */
#define ENUM_ROM_BFWD_DEVENUM_10           0x00000A00 /*!< device enumeration 10 */
#define ENUM_ROM_BFWD_DEVENUM_11           0x00000B00 /*!< device enumeration 11 */
#define ENUM_ROM_BFWD_DEVENUM_12           0x00000C00 /*!< device enumeration 12 */
#define ENUM_ROM_BFWD_DEVENUM_13           0x00000D00 /*!< device enumeration 13 */
#define ENUM_ROM_BFWD_DEVENUM_14           0x00000E00 /*!< device enumeration 14 */
#define ENUM_ROM_BFWD_DEVENUM_15           0x00000F00 /*!< device enumeration 15 */

#define BITM_ROM_BFWD_CLEANUP              (_ADI_MSK(0x00000080,uint32_t)) /*!< Boot forward cleanup enable */

#define BITM_ROM_BFWD_DEVICE               (_ADI_MSK(0x0000000F,uint32_t)) /*!< The peripheral to use for the forward operation */
#define ENUM_ROM_BFWD_DEVICE_NONE          0x00000000 /*!< Do not forward the block */
#define ENUM_ROM_BFWD_DEVICE_SPI           0x00000002 /*!< forward to the SPI ports */
#define ENUM_ROM_BFWD_DEVICE_LPT           0x00000004  /*!< forward to the Linkports */
/*! @} */
/*! @} */

/*********************************************************************************************/
/*                                                                                           */
/*   Callback Flags                                                                          */
/*                                                                                           */
/*********************************************************************************************/
/*!
 * @addtogroup BOOT_CALLBACK Boot Kernel Callback
 * @brief Functionality related to the boot kernel callback scheme
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_CALLBACK_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_CBFLAG_FORWARD      4	/*!< Callback is the result of a forward block */
#define BITP_ROM_CBFLAG_FINAL        3  /*!< The last callback of the boot stream due to the final block */
#define BITP_ROM_CBFLAG_FIRST        2  /*!< The first callback request */
#define BITP_ROM_CBFLAG_PAGESTART    1  /*!< Callback was as the result of a page fill operation to the intermediate temporary buffer */
#define BITP_ROM_CBFLAG_DIRECT       0  /*!< The intermediate buffer was not used for the boot block */

#define BITM_ROM_CBFLAG_FORWARD      (_ADI_MSK(0x00000010,uint32_t)) /*!< Callback is the result of a forward block */
#define BITM_ROM_CBFLAG_FINAL        (_ADI_MSK(0x00000008,uint32_t)) /*!< The last callback request for the current boot block */
#define BITM_ROM_CBFLAG_FIRST        (_ADI_MSK(0x00000004,uint32_t)) /*!< The first callback request for the current boot block */
#define BITM_ROM_CBFLAG_PAGESTART    (_ADI_MSK(0x00000002,uint32_t)) /*!< Callback was as the result of a page fill operation to the intermediate temporary buffer */
#define BITM_ROM_CBFLAG_DIRECT       (_ADI_MSK(0x00000001,uint32_t)) /*!< The intermediate buffer was not used for the boot block */
/*! @} */
/*! @} */
/*! @} */





/***********************************************************/
/*                                                         */
/* dBootCommand Memory                                     */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_MODE_MEMORY Memory Boot Mode
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_MEMORY_BCMD Boot Command
 * @brief The boot command used to boot the processor via the SMC peripheral
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_MEMORY_BCMD_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BCMD_MEM_CODE        	16 /*!< Boot code used for device configuration */
#define BITP_ROM_BCMD_MEM_SMCBANK       12 /*!< SMC memory bank to boot from */
#define BITP_ROM_BCMD_MEM_NOAUTO        6  /*!< skip automatic device detection */
#define BITP_ROM_BCMD_MEM_NOCFG         5  /*!< skip device configuration and pinmuxing */
#define BITP_ROM_BCMD_MEM_HOST          4  /*!< slave mode enable */
#define BITP_ROM_BCMD_MEM_DEVICE        0  /*!< boot source device SPI, Flash, ...*/

#define BITM_ROM_BCMD_MEM_SMCBANK       (_ADI_MSK(0x00003000,uint32_t)) /*!< SMC  memory bank to boot from */
#define ENUM_ROM_BCMD_MEM_SMCBANK_0     0x00000000 /*!< Boot from SMC memory bank 0 */
#define ENUM_ROM_BCMD_MEM_SMCBANK_1     0x00001000 /*!< Boot from SMC memory bank 1 */
#define ENUM_ROM_BCMD_MEM_SMCBANK_2     0x00002000 /*!< Boot from SMC memory bank 2 */
#define ENUM_ROM_BCMD_MEM_SMCBANK_3     0x00003000 /*!< Boot from SMC memory bank 3 */


#define BITM_ROM_BCMD_MEM_CODE        	(_ADI_MSK(0x000F0000,uint32_t)) /*!< Boot code used for device configuration */
#define ENUM_ROM_BCMD_MEM_CODE_0      	0x00000000 /*!< Use configuration option 0 */
#define ENUM_ROM_BCMD_MEM_CODE_1      	0x00010000 /*!< Use configuration option 1 */
#define ENUM_ROM_BCMD_MEM_CODE_2      	0x00020000 /*!< Use configuration option 2 */
#define ENUM_ROM_BCMD_MEM_CODE_3      	0x00030000 /*!< Use configuration option 3 */
#define ENUM_ROM_BCMD_MEM_CODE_4      	0x00040000 /*!< Use configuration option 4 */
#define ENUM_ROM_BCMD_MEM_CODE_5      	0x00050000 /*!< Use configuration option 5 */
#define ENUM_ROM_BCMD_MEM_CODE_6      	0x00060000 /*!< Use configuration option 6 */
#define ENUM_ROM_BCMD_MEM_CODE_7      	0x00070000 /*!< Use configuration option 7 */
#define ENUM_ROM_BCMD_MEM_CODE_8      	0x00080000 /*!< Use configuration option 8 */
#define ENUM_ROM_BCMD_MEM_CODE_9      	0x00090000 /*!< Use configuration option 9 */
#define ENUM_ROM_BCMD_MEM_CODE_10     	0x000A0000 /*!< Use configuration option 10 */
#define ENUM_ROM_BCMD_MEM_CODE_11      	0x000B0000 /*!< Use configuration option 11 */
#define ENUM_ROM_BCMD_MEM_CODE_12      	0x000C0000 /*!< Use configuration option 12 */
#define ENUM_ROM_BCMD_MEM_CODE_13      	0x000D0000 /*!< Use configuration option 13 */
#define ENUM_ROM_BCMD_MEM_CODE_14      	0x000E0000 /*!< Use configuration option 14 */
#define ENUM_ROM_BCMD_MEM_CODE_15      	0x000F0000 /*!< Use configuration option 15 */

#define BITM_ROM_BCMD_MEM_NOAUTO        (_ADI_MSK(0x00000040,uint32_t))  /*!< skip automatic device detection */
#define BITM_ROM_BCMD_MEM_NOCFG         (_ADI_MSK(0x00000020,uint32_t))  /*!< skip default configuration */
#define BITM_ROM_BCMD_MEM_HOST          (_ADI_MSK(0x00000010,uint32_t))  /*!< slave mode enable*/
/*! @} */
/*! @} */
/*! @} */


/***********************************************************/
/*                                                         */
/* dBootCommand RSI                                        */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_MODE_RSI RSI Boot Mode
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_RSI_BCMD Boot Command
 * @brief Boots the processor via the RSI peripheral
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_RSI_BCMD_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BCMD_RSI_HSCLK       	28 /*!< RSI clock divider to use in high speed mode */
#define BITP_ROM_BCMD_RSI_CLK           24 /*!< RSI Clock divider to use in normal mode */
#define BITP_ROM_BCMD_RSI_ACCESS        20 /*!< When set indicates a high capacity card requiring sector accesses */
#define BITP_ROM_BCMD_RSI_CODE          16 /*!< Boot code used for device configuration */
#define BITP_ROM_BCMD_RSI_TYPE          12 /*!< The card or embedded device type */
#define BITP_ROM_BCMD_RSI_NOAUTO        6  /*!< skip automatic device detection */
#define BITP_ROM_BCMD_RSI_NOCFG         5  /*!< skip device configuration and pinmuxing */
#define BITP_ROM_BCMD_RSI_HOST          4  /*!< slave mode enable */
#define BITP_ROM_BCMD_RSI_DEVICE        0  /*!< boot source device SPI, Flash, ...*/

#define BITM_ROM_BCMD_RSI_TYPE          (_ADI_MSK(0x0000F000,uint32_t)) /*!< The card or embedded device type */
#define ENUM_ROM_BCMD_RSI_TYPE_MMC      0x00000000 /*!< MMC device */
#define ENUM_ROM_BCMD_RSI_TYPE_MMCBBM   0x00001000 /*!< eMMC device accessing the main memory array */
#define ENUM_ROM_BCMD_RSI_TYPE_MMCBB1   0x00002000 /*!< eMMC device accessing boot block sector 1 */
#define ENUM_ROM_BCMD_RSI_TYPE_MMCBB2   0x00003000 /*!< eMMC device accessing boot block sector 2 */
#define ENUM_ROM_BCMD_RSI_TYPE_SD       0x00004000 /*!< SD device */

#define BITM_ROM_BCMD_RSI_CODE          (_ADI_MSK(0x000F0000,uint32_t)) /*!< Boot code used for device configuration */
#define ENUM_ROM_BCMD_RSI_CODE_0        0x00000000 /*!< Use configuration option 0 */
#define ENUM_ROM_BCMD_RSI_CODE_1        0x00010000 /*!< Use configuration option 1 */
#define ENUM_ROM_BCMD_RSI_CODE_2        0x00020000 /*!< Use configuration option 2 */
#define ENUM_ROM_BCMD_RSI_CODE_3        0x00030000 /*!< Use configuration option 3 */
#define ENUM_ROM_BCMD_RSI_CODE_4        0x00040000 /*!< Use configuration option 4 */
#define ENUM_ROM_BCMD_RSI_CODE_5        0x00050000 /*!< Use configuration option 5 */
#define ENUM_ROM_BCMD_RSI_CODE_6        0x00060000 /*!< Use configuration option 6 */
#define ENUM_ROM_BCMD_RSI_CODE_7        0x00070000 /*!< Use configuration option 7 */
#define ENUM_ROM_BCMD_RSI_CODE_8        0x00080000 /*!< Use configuration option 8 */
#define ENUM_ROM_BCMD_RSI_CODE_9        0x00090000 /*!< Use configuration option 9 */
#define ENUM_ROM_BCMD_RSI_CODE_10       0x000A0000 /*!< Use configuration option 10 */
#define ENUM_ROM_BCMD_RSI_CODE_11       0x000B0000 /*!< Use configuration option 11 */
#define ENUM_ROM_BCMD_RSI_CODE_12       0x000C0000 /*!< Use configuration option 12 */
#define ENUM_ROM_BCMD_RSI_CODE_13       0x000D0000 /*!< Use configuration option 13 */
#define ENUM_ROM_BCMD_RSI_CODE_14       0x000E0000 /*!< Use configuration option 14 */
#define ENUM_ROM_BCMD_RSI_CODE_15       0x000F0000 /*!< Use configuration option 15 */

#define BITM_ROM_BCMD_RSI_ACCESS        (_ADI_MSK(0x00100000,uint32_t)) /*!< When set indicates a high capacity card requiring sector accesses */
#define ENUM_ROM_BCMD_RSI_ACCESS_BYTE   (_ADI_MSK(0x00000000,uint32_t)) /*!< Low capacity byte addressable device */
#define ENUM_ROM_BCMD_RSI_ACCESS_SECTOR (_ADI_MSK(0x00100000,uint32_t)) /*!< High capacity sector addressable device */

#define BITM_ROM_BCMD_RSI_CLK           0x0F000000 /*!< RSI Clock divider to use in normal mode */
#define BITM_ROM_BCMD_RSI_HSCLK         0xF0000000 /*!< RSI clock divider to use in high speed mode */

#define BITM_ROM_BCMD_RSI_NOAUTO        (_ADI_MSK(0x00000040,uint32_t))  /*!< skip automatic device detection */
#define BITM_ROM_BCMD_RSI_NOCFG         (_ADI_MSK(0x00000020,uint32_t))  /*!< skip default configuration */
#define BITM_ROM_BCMD_RSI_HOST          (_ADI_MSK(0x00000010,uint32_t))  /*!< slave mode enable*/
/*! @} */
/*! @} */
/*! @} */

/***********************************************************/
/*                                                         */
/* dBootCommand SPI Master                                 */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_MODE_SPIM SPI Master Boot Mode
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_SPIM_BCMD Boot Command
 * @brief Boots the processor via the SPI peripheral
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_SPIM_BCMD_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BCMD_SPI_SPEED         24 /*!< SPI clock divider value */
#define BITP_ROM_BCMD_SPI_DUMMY_BYTES   22 /*!< Number of dummy bytes in the SPI command cycle */
#define BITP_ROM_BCMD_SPI_ADDR_BYTES    20 /*!< Number of address bytes in the SPI command cycle */
#define BITP_ROM_BCMD_SPI_MCODE         16 /*!< Boot code used for device configuration */
#define BITP_ROM_BCMD_SPI_CHANNEL       12 /*!< Slave select signal to use to select the SPI device */
#define BITP_ROM_BCMD_SPI_NOAUTO        6  /*!< skip automatic device detection */
#define BITP_ROM_BCMD_SPI_NOCFG         5  /*!< skip device configuration and pinmuxing */
#define BITP_ROM_BCMD_SPI_HOST          4  /*!< slave mode enable */
#define BITP_ROM_BCMD_SPI_DEVICE        0  /*!< boot source device SPI, Flash, ...*/

#define BITM_ROM_BCMD_SPI_SPEED         (_ADI_MSK(0xFF000000,uint32_t)) /*!< SPI clock divider value */
#define BITM_ROM_BCMD_SPI_DUMMY_BYTES   (_ADI_MSK(0x00C00000,uint32_t)) /*!< Number of dummy bytes required in the SPI command cycle */
#define ENUM_ROM_BCMD_SPI_DUMMY0		0x00000000 /*!< No dummy byte issued after the address */
#define ENUM_ROM_BCMD_SPI_DUMMY1        0x00400000 /*!< One dummy byte sent after the address */
#define ENUM_ROM_BCMD_SPI_DUMMY2        0x00800000 /*!< Two dummy byte sent after the address */
#define ENUM_ROM_BCMD_SPI_DUMMY3        0x00C00000 /*!< Three dummy byte sent after the address */

#define BITM_ROM_BCMD_SPI_ADDR_BYTES    (_ADI_MSK(0x00300000,uint32_t)) /*!< Number of address bytes in the SPI command cycle */
#define ENUM_ROM_BCMD_SPI_ADDRESS1		0x00000000 /*!< A single address byte is required */
#define ENUM_ROM_BCMD_SPI_ADDRESS2      0x00100000 /*!< Two address bytes required */
#define ENUM_ROM_BCMD_SPI_ADDRESS3      0x00200000 /*!< Three address bytes required */
#define ENUM_ROM_BCMD_SPI_ADDRESS4      0x00300000 /*!< Four address bytes required */

#define BITM_ROM_BCMD_SPI_MCODE         (_ADI_MSK(0x000F0000,uint32_t)) /*!< Boot code used for device configuration */
#define ENUM_ROM_BCMD_SPI_MCODE_0     	0x00000000 /*!< Use configuration option 0 */
#define ENUM_ROM_BCMD_SPI_MCODE_1     	0x00010000 /*!< Use configuration option 1 */
#define ENUM_ROM_BCMD_SPI_MCODE_2     	0x00020000 /*!< Use configuration option 2 */
#define ENUM_ROM_BCMD_SPI_MCODE_3     	0x00030000 /*!< Use configuration option 3 */
#define ENUM_ROM_BCMD_SPI_MCODE_4     	0x00040000 /*!< Use configuration option 4 */
#define ENUM_ROM_BCMD_SPI_MCODE_5     	0x00050000 /*!< Use configuration option 5 */
#define ENUM_ROM_BCMD_SPI_MCODE_6     	0x00060000 /*!< Use configuration option 6 */
#define ENUM_ROM_BCMD_SPI_MCODE_7     	0x00070000 /*!< Use configuration option 7 */
#define ENUM_ROM_BCMD_SPI_MCODE_8     	0x00080000 /*!< Use configuration option 8 */
#define ENUM_ROM_BCMD_SPI_MCODE_9     	0x00090000 /*!< Use configuration option 9 */
#define ENUM_ROM_BCMD_SPI_MCODE_A     	0x000A0000 /*!< Use configuration option 10 */
#define ENUM_ROM_BCMD_SPI_MCODE_B     	0x000B0000 /*!< Use configuration option 11 */
#define ENUM_ROM_BCMD_SPI_MCODE_C     	0x000C0000 /*!< Use configuration option 12 */
#define ENUM_ROM_BCMD_SPI_MCODE_D     	0x000D0000 /*!< Use configuration option 13 */
#define ENUM_ROM_BCMD_SPI_MCODE_E     	0x000E0000 /*!< Use configuration option 14 */
#define ENUM_ROM_BCMD_SPI_MCODE_F     	0x000F0000 /*!< Use configuration option 15 */

#define BITM_ROM_BCMD_SPI_CHANNEL       (_ADI_MSK(0x0000F000,uint32_t)) /*!< Slave select signal to use to select the SPI device */
#define ENUM_ROM_BCMD_SPI_CHANNEL_0   	0x00000000 /*!< Use slave select 1 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_1   	0x00001000 /*!< Use slave select 2 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_2   	0x00002000 /*!< Use slave select 3 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_3   	0x00003000 /*!< Use slave select 4 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_4   	0x00004000 /*!< Use slave select 5 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_5   	0x00005000 /*!< Use slave select 6 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_6   	0x00006000 /*!< Use slave select 7 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_7   	0x00007000 /*!< Use slave select 8 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_8   	0x00008000 /*!< Use slave select 9 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_9   	0x00009000 /*!< Use slave select 10 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_10  	0x0000A000 /*!< Use slave select 11 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_11  	0x0000B000 /*!< Use slave select 12 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_12  	0x0000C000 /*!< Use slave select 13 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_13  	0x0000D000 /*!< Use slave select 14 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_14  	0x0000E000 /*!< Use slave select 15 */
#define ENUM_ROM_BCMD_SPI_CHANNEL_15  	0x0000F000 /*!< Use slave select 16 */

#define BITM_ROM_BCMD_SPI_NOAUTO        (_ADI_MSK(0x00000040,uint32_t))  /*!< skip automatic device detection */
#define BITM_ROM_BCMD_SPI_NOCFG         (_ADI_MSK(0x00000020,uint32_t))  /*!< skip default configuration */
#define BITM_ROM_BCMD_SPI_HOST          (_ADI_MSK(0x00000010,uint32_t))  /*!< slave mode enable*/
/*! @} */
/*! @} */
/*! @} */

/***********************************************************/
/*                                                         */
/* dBootCommand SPI Slave                                  */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_MODE_SPIS SPI Slave Boot Mode
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_SPIS_BCMD Boot Command
 * @brief Boots the processor via the SPI peripheral configured as a slave device
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_SPIS_BCMD_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BCMD_SPI_SCODE   		 16 /*!< Boot code used for device configuration */
#define BITP_ROM_BCMD_SPIS_CHANNEL       12 /*!< Slave select signal to use to select the SPI device */
#define BITP_ROM_BCMD_SPIS_NOAUTO        6  /*!< skip automatic device detection */
#define BITP_ROM_BCMD_SPIS_NOCFG         5  /*!< skip device configuration and pinmuxing */
#define BITP_ROM_BCMD_SPIS_HOST          4  /*!< slave mode enable */
#define BITP_ROM_BCMD_SPIS_DEVICE        0  /*!< boot source device SPI, Flash, ...*/

#define BITM_ROM_BCMD_SPI_SCODE        	(_ADI_MSK(0x000F0000,uint32_t)) /*!< Boot code used for device configuration */
#define ENUM_ROM_BCMD_SPI_SCODE_0 		0x00000000 /*!< Use configuration option 0 */
#define ENUM_ROM_BCMD_SPI_SCODE_1 		0x00010000 /*!< Use configuration option 1 */
#define ENUM_ROM_BCMD_SPI_SCODE_2 		0x00020000 /*!< Use configuration option 2 */
#define ENUM_ROM_BCMD_SPI_SCODE_3 		0x00030000 /*!< Use configuration option 3 */
#define ENUM_ROM_BCMD_SPI_SCODE_4 		0x00040000 /*!< Use configuration option 4 */
#define ENUM_ROM_BCMD_SPI_SCODE_5 		0x00050000 /*!< Use configuration option 5 */
#define ENUM_ROM_BCMD_SPI_SCODE_6 		0x00060000 /*!< Use configuration option 6 */
#define ENUM_ROM_BCMD_SPI_SCODE_7 		0x00070000 /*!< Use configuration option 7 */
#define ENUM_ROM_BCMD_SPI_SCODE_8 		0x00080000 /*!< Use configuration option 8 */
#define ENUM_ROM_BCMD_SPI_SCODE_9 		0x00090000 /*!< Use configuration option 9 */
#define ENUM_ROM_BCMD_SPI_SCODE_10 		0x000A0000 /*!< Use configuration option 10 */
#define ENUM_ROM_BCMD_SPI_SCODE_11 		0x000B0000 /*!< Use configuration option 11 */
#define ENUM_ROM_BCMD_SPI_SCODE_12 		0x000C0000 /*!< Use configuration option 12 */
#define ENUM_ROM_BCMD_SPI_SCODE_13 		0x000D0000 /*!< Use configuration option 13 */
#define ENUM_ROM_BCMD_SPI_SCODE_14 		0x000E0000 /*!< Use configuration option 14 */
#define ENUM_ROM_BCMD_SPI_SCODE_15 		0x000F0000 /*!< Use configuration option 15 */

#define BITM_ROM_BCMD_SPIS_NOAUTO        (_ADI_MSK(0x00000040,uint32_t))  /*!< skip automatic device detection */
#define BITM_ROM_BCMD_SPIS_NOCFG         (_ADI_MSK(0x00000020,uint32_t))  /*!< skip default configuration */
#define BITM_ROM_BCMD_SPIS_HOST          (_ADI_MSK(0x00000010,uint32_t))  /*!< slave mode enable*/
/*! @} */
/*! @} */

/***********************************************************/
/*                                                         */
/* SPI_SCMD For SPI slave Boot                             */
/*                                                         */
/***********************************************************/
#define BITP_ROM_SPI_SCMD         		0 /*!< Boot code used for device configuration shifter down do bit 0 */

#define BITM_ROM_SPI_SCMD              	(_ADI_MSK(0x0000000F,uint32_t)) /*!< Boot code used for device configuration shifter down to bit 0 */
#define ENUM_ROM_SPI_SCMD_LSBM     		0x1 					/*!< legacy single-bit mode */
#define ENUM_ROM_SPI_SCMD_SBM     		0x3 					/*!< single-bit mode */
#define ENUM_ROM_SPI_SCMD_DBM     		0x7 					/*!< dual-bit mode */
#define ENUM_ROM_SPI_SCMD_QBM     		0xB 					/*!< quad-bit mode */
/*! @} */

/***********************************************************/
/*                                                         */
/* dBootCommand UART Slave                                 */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_MODE_UART UART Boot Mode
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_UART_BCMD Boot Command
 * @brief Boots the processor via the UART peripheral
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_UART_BCMD_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BCMD_UART_CLK        	16 /*!< UART clock divisor */
#define BITP_ROM_BCMD_UART_EDBO         15 /*!< Enable EDBO feature */

#define BITM_ROM_BCMD_UART_CLK          (_ADI_MSK(0xFFFF0000,uint32_t)) /*!< UART clock divisor */
#define BITM_ROM_BCMD_UART_EDBO         (_ADI_MSK(0x00008000,uint32_t)) /*!< Enable EDBO feature of UART */
/*! @} */
/*! @} */
/*! @} */


/***********************************************************/
/*                                                         */
/* dBootForward Linkport                                   */
/*                                                         */
/***********************************************************/
/*!
 * @ingroup BOOT_FORWARD
 * @{
 */
/*!
 * @addtogroup BOOT_FORWARD_LPT Linkport Boot Forward Command
 * @brief Configures the linkport transmitter for forwarding data to the linkport
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_FORWARD_LPT_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BFWD_LPT_CLOCK       	24 /*!< Linkport clock divisor */
#define BITM_ROM_BFWD_LPT_CLOCK         (_ADI_MSK(0xFF000000,uint32_t)) /*!< Linkport clock divisor */
/*! @} */
/*! @} */
/*! @} */

/***********************************************************/
/*                                                         */
/* dBootForward SPI                                        */
/*                                                         */
/***********************************************************/
/*!
 * @ingroup BOOT_FORWARD
 * @{
 */
/*!
 * @addtogroup BOOT_FORWARD_SPI SPI Boot Forward Command
 * @brief Configures the SPI transmitter for forwarding data to the SPI
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_FORWARD_SPI_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_BFWD_SPI_CLOCK       	24 /*!< SPI clock divisor */
#define BITP_ROM_BFWD_SPI_CPOL        	23 /*!< SPI clock polarity */
#define BITP_ROM_BFWD_SPI_CPHA        	22 /*!< SPI clock phase */
#define BITP_ROM_BFWD_SPI_SIZE        	20 /*!< SPI word transfer size */
#define BITP_ROM_BFWD_SPI_FCPL        	19 /*!< SPI flow control polarity */
#define BITP_ROM_BFWD_SPI_MCODE       	16 /*!< SPI mode detection */
#define BITP_ROM_BFWD_SPI_CHANNEL     	12 /*!< SPI slave select signal */

#define BITM_ROM_BFWD_SPI_CLOCK         (_ADI_MSK(0xFF000000,uint32_t)) /*!< SPI clock divisor */
#define BITM_ROM_BFWD_SPI_CPOL          (_ADI_MSK(0x00800000,uint32_t)) /*!< SPI clock polarity */
#define ENUM_ROM_BFWD_SPI_CLKACTHIGH    0x00000000 /*!< SPI clock active high */
#define ENUM_ROM_BFWD_SPI_CLKACTLOW     0x00800000 /*!< SPI clock active low */

#define BITM_ROM_BFWD_SPI_CPHA          (_ADI_MSK(0x00400000,uint32_t)) /*!< SPI clock phase */
#define ENUM_ROM_BFWD_SPI_CLKTGLMID    0x00000000 /*!< SPI clock toggles from the middle of the first data bit */
#define ENUM_ROM_BFWD_SPI_CLKTGLSTART  0x00400000 /*!< SPI clock toggles from the start of the first data bit */

#define BITM_ROM_BFWD_SPI_SIZE          (_ADI_MSK(0x00300000,uint32_t)) /*!< SPI word transfer size */
#define ENUM_ROM_BFWD_SPI_SIZE_0      	0x00000000 /*!< 8-bit word transfer size */
#define ENUM_ROM_BFWD_SPI_SIZE_1      	0x00100000 /*!< 16-bit word transfer size */
#define ENUM_ROM_BFWD_SPI_SIZE_2      	0x00200000 /*!< 32-bit word transfer size */

#define BITM_ROM_BFWD_SPI_FCPL          (_ADI_MSK(0x00080000,uint32_t)) /*!< SPI flow control polarity */

#define BITM_ROM_BFWD_SPI_MCODE         (_ADI_MSK(0x000F0000,uint32_t)) /*!< SPI mode detection */
#define ENUM_ROM_BFWD_SPI_MCODE_0     	0x00000000 /*!< Automode detection command sent */
#define ENUM_ROM_BFWD_SPI_MCODE_1     	0x00010000 /*!< DIOM automode detection command sent */
#define ENUM_ROM_BFWD_SPI_MCODE_2     	0x00020000 /*!< QSPI automode detection command sent */
#define ENUM_ROM_BFWD_SPI_MCODE_3     	0x00030000 /*!< Single bit mode only (no autodetection command sent) */

#define BITM_ROM_BFWD_SPI_CHANNEL       (_ADI_MSK(0x0000F000,uint32_t)) /*!< SPI slave select signal */
#define ENUM_ROM_BFWD_SPI_CHANNEL_0     0x00000000 /*!< Use slave select 1 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_1     0x00001000 /*!< Use slave select 2 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_2     0x00002000 /*!< Use slave select 3 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_3     0x00003000 /*!< Use slave select 4 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_4     0x00004000 /*!< Use slave select 5 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_5     0x00005000 /*!< Use slave select 6 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_6     0x00006000 /*!< Use slave select 7 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_7     0x00007000 /*!< Use slave select 8 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_8     0x00008000 /*!< Use slave select 9 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_9     0x00009000 /*!< Use slave select 10 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_10    0x0000A000 /*!< Use slave select 11 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_11    0x0000B000 /*!< Use slave select 12 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_12    0x0000C000 /*!< Use slave select 13 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_13    0x0000D000 /*!< Use slave select 14 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_14    0x0000E000 /*!< Use slave select 15 */
#define ENUM_ROM_BFWD_SPI_CHANNEL_15    0x0000F000 /*!< Use slave select 16 */

/*! @} */
/*! @} */
/*! @} */



/***********************************************************/
/*                                                         */
/* RSI Boot mode specific structure                        */
/*                                                         */
/***********************************************************/
/*!
 * @ingroup BOOT_MODE_RSI
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_RSI_STRUCTURES RSI Specific Structures
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_RSI_STRUCTURES_BITS bitmasks, positions and enumerations
 * @{
 */
/*!
 * @addtogroup BOOT_MODE_RSI_STRUCTURES_BITS_FLAGS uwFlags
 * @{
 */
#define BITP_ROM_BOOT_RSI_FLAGS_TYPE    0x00000000 /*!< Device type detected */
#define BITP_ROM_BOOT_RSI_FLAGS_ACCESS  0x00000004 /*!< Access type for standard and high capacity card support */
#define BITP_ROM_BOOT_RSI_FLAGS_SPEED   0x00000005 /*!< Standard or high speed protocol */
#define BITP_ROM_BOOT_RSI_FLAGS_BBEN    0x00000006 /*!< Enable the booting from a dedicated boot block */
#define BITP_ROM_BOOT_RSI_FLAGS_STOPEN  0x00000007 /*!< Stop command feature enable */
#define BITP_ROM_BOOT_RSI_FLAGS_BB      0x00000008 /*!< Boot block to boot from */
#define BITP_ROM_BOOT_RSI_FLAGS_MAXEN   0x0000000F /*!< Set RSI clock to maximum for the entire boot process */

#define BITM_ROM_BOOT_RSI_FLAGS_TYPE    (_ADI_MSK(0x0000000F,uint32_t)) /*!< Device type detected */
#define ENUM_ROM_BOOT_RSI_FLAGS_EMPTY   0x00000000 /*!< No device detected */
#define ENUM_ROM_BOOT_RSI_FLAGS_MMC     0x00000001 /*!< MMC device detected */
#define ENUM_ROM_BOOT_RSI_FLAGS_SD      0x00000002 /*!< SD device detected */
#define ENUM_ROM_BOOT_RSI_FLAGS_INVALID 0x00000004 /*!< Non compatible device detected */

#define BITM_ROM_BOOT_RSI_FLAGS_ACCESS  (_ADI_MSK(0x00000010,uint32_t)) /*!< Access type for standard and high capacity card support */
#define ENUM_ROM_BOOT_RSI_FLAGS_BYTE    0x00000000 /*!< Byte access for standard capacity devices */
#define ENUM_ROM_BOOT_RSI_FLAGS_SECTOR  0x00000010 /*!< Sector access for high capacity devices */

#define BITM_ROM_BOOT_RSI_FLAGS_SPEED   (_ADI_MSK(0x00000020,uint32_t)) /*!< Standard or high speed protocol */
#define ENUM_ROM_BOOT_RSI_FLAGS_LOSPEED 0x00000000 /*!< Standard speed protocol */
#define ENUM_ROM_BOOT_RSI_FLAGS_HISPEED 0x00000020 /*!< High speed protocol */

#define BITM_ROM_BOOT_RSI_FLAGS_BBEN    (_ADI_MSK(0x00000040,uint32_t)) /*!< Enable the booting from a dedicated boot block */


#define BITM_ROM_BOOT_RSI_FLAGS_STOPEN  (_ADI_MSK(0x00000080,uint32_t)) /*!< Stop command feature enable */

#define BITM_ROM_BOOT_RSI_FLAGS_BB      (_ADI_MSK(0x00000700,uint32_t)) /*!< Boot block to boot from */
#define ENUM_ROM_BOOT_RSI_FLAGS_BBMAIN  0x00000000 /*!< Boot from main area array */
#define ENUM_ROM_BOOT_RSI_FLAGS_BB1     0x00000100 /*!< Boot from boot block sector 1 */
#define ENUM_ROM_BOOT_RSI_FLAGS_BB2     0x00000200 /*!< Boot from boot block sector 2 */

#define BITM_ROM_BOOT_RSI_FLAGS_MAXEN   (_ADI_MSK(0x00008000,uint32_t)) /*!< Set RSI clock to maximum for the entire boot process */


/*! @} */
/*! @} */
/*! @} */
/*! @} */




/***********************************************************/
/*                                                         */
/* Wakeup Actions for DPM_RESTORE                          */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_WAKEUP Wakeup From Hibernate Actions
 * @{
 * @brief Provides instructions to the boot ROM on specific actions to be taken in the event of a wakeup from hibernate.
 * @details The wakeup actions are required to be stored in the DPM0_RESTORE0 register prior to entering hibernate
 * so they are preserved during the hibernate sequence and can be read back upon wakeup.
 * A majority of the wakeup actions are performed by the syscontrol routine and relate to restoring the CGU and DMC
 * configuration. For the actions that are to performed by Syscontrol, the preboot sequence processes the wakeup action flags
 * then sets the required Syscontrol flags to perform the required actions prior to calling the Syscontrol routine.
 */
/*!
 * @addtogroup BOOT_WAKEUP_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_WUA_CHKHDR             24 /*!< The signature field that must be set in order for wakeup actions to be performed */

#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define BITP_ROM_WUA_CCBF1DIS           12 /*!< Disable Core Clock Buffer 1 in the DPM */
#define BITP_ROM_WUA_SCBF3DIS           11 /*!< Disable SCLK Clock Buffer 3 in the DPM */
#define BITP_ROM_WUA_SCBF2DIS           10 /*!< Disable SCLK Clock Buffer 2 in the DPM */
#define BITP_ROM_WUA_SCBF1DIS           9  /*!< Disable SCLK Clock Buffer 1 in the DPM */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define BITP_ROM_WUA_CCBF1DIS           12 /*!< Disable Core Clock Buffer 1 in the DPM */
#define BITP_ROM_WUA_SCBF3DIS           11 /*!< Disable SCLK Clock Buffer 3 in the DPM */
#define BITP_ROM_WUA_SCBF2DIS           10 /*!< Disable SCLK Clock Buffer 2 in the DPM */
#define BITP_ROM_WUA_SCBF1DIS           9  /*!< Disable SCLK Clock Buffer 1 in the DPM */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#else
#error "Unknown silicon revision"
#endif

#define BITP_ROM_WUA_DDRLOCK            7 /*!< Extend the DDR Lock wait time */
#define BITP_ROM_WUA_DDRDLLEN           6 /*!< Restore the DMC_DLLCTL and the DMC_PADCTL registers */
#define BITP_ROM_WUA_DDR                5 /*!< Initialize DMC upon wakeup */
#define BITP_ROM_WUA_CGU                4 /*!< Initialize CGU upon wakeup */
#define BITP_ROM_WUA_BCODE	            2 /*!< Restore RCU0_BCODE register upon wakeup */
#define BITP_ROM_WUA_MEMBOOT            1 /*!< Boot from memory instead of original boot source on wakeup */
#define BITP_ROM_WUA_EN                 0 /*!< Wakeup actions enable */

#define BITM_ROM_WUA_CHKHDR             (_ADI_MSK(0xFF000000,uint32_t)) /*!< The signature field that must be set in order for wakeup actions to be performed */
#define ENUM_ROM_WUA_CHKHDR_AD			0xAD000000 /*!< The signature value to enable wakeup actions */

#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define BITM_ROM_WUA_CCBF1DIS           (_ADI_MSK(0x00001000,uint32_t)) /*!< Disable Core Clock Buffer 1 in the DPM */
#define BITM_ROM_WUA_SCBF3DIS           (_ADI_MSK(0x00000800,uint32_t)) /*!< Disable SCLK Clock Buffer 3 in the DPM */
#define BITM_ROM_WUA_SCBF2DIS           (_ADI_MSK(0x00000400,uint32_t)) /*!< Disable SCLK Clock Buffer 2 in the DPM */
#define BITM_ROM_WUA_SCBF1DIS           (_ADI_MSK(0x00000200,uint32_t)) /*!< Disable SCLK Clock Buffer 1 in the DPM */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define BITM_ROM_WUA_CCBF1DIS           (_ADI_MSK(0x00001000,uint32_t)) /*!< Disable Core Clock Buffer 1 in the DPM */
#define BITM_ROM_WUA_SCBF3DIS           (_ADI_MSK(0x00000800,uint32_t)) /*!< Disable SCLK Clock Buffer 3 in the DPM */
#define BITM_ROM_WUA_SCBF2DIS           (_ADI_MSK(0x00000400,uint32_t)) /*!< Disable SCLK Clock Buffer 2 in the DPM */
#define BITM_ROM_WUA_SCBF1DIS           (_ADI_MSK(0x00000200,uint32_t)) /*!< Disable SCLK Clock Buffer 1 in the DPM */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#else
# error "Unknown silicon revision"
#endif

#define BITM_ROM_WUA_DDRLOCK            (_ADI_MSK(0x00000080,uint32_t)) /*!< Extend the DDR Lock wait time */
#define BITM_ROM_WUA_DDRDLLEN           (_ADI_MSK(0x00000040,uint32_t)) /*!< Restore the DMC_DLLCTL and the DMC_PADCTL registers */
#define BITM_ROM_WUA_DDR                (_ADI_MSK(0x00000020,uint32_t)) /*!< Initialize DMC upon wakeup */
#define BITM_ROM_WUA_CGU                (_ADI_MSK(0x00000010,uint32_t)) /*!< Initialize CGU upon wakeup */

#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define BITM_ROM_WUA_BCODE              (_ADI_MSK(0x00000004,uint32_t)) /*!< Restore RCU0_BCODE register upon wakeup */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define BITM_ROM_WUA_BCODE              (_ADI_MSK(0x00000004,uint32_t)) /*!< Restore RCU0_BCODE register upon wakeup */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#else
# error "Unknown silicon revision"
#endif

#define BITM_ROM_WUA_MEMBOOT            (_ADI_MSK(0x00000002,uint32_t)) /*!< Boot from memory instead of original boot source on wakeup */
#define BITM_ROM_WUA_EN                	(_ADI_MSK(0x00000001,uint32_t)) /*!< Wakeup actions enable */
/*! @} */
/*! @} */

/***********************************************************/
/*                                                         */
/* Syscontrol                                              */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_SYSCTRL Syscontrol
 * @{
 * @brief Provides instructions to the boot ROM on specific actions to be taken in the event of a wakeup from hibernate.
 * @details The wakeup actions are required to be stored in the DPM0_RESTORE0 register prior to entering hibernate
 * so they are preserved during the hibernate sequence and can be read back upon wakeup.
 * A majority of the wakeup actions are performed by the syscontrol routine and relate to restoring the CGU and DMC
 * configuration. For the actions that are to performed by Syscontrol, the preboot sequence processes the wakeup action flags
 * then sets the required Syscontrol flags to perform the required actions prior to calling the Syscontrol routine.
 */
/*!
 * @addtogroup BOOT_SYSCTRL_ACTIONS Syscontrol Actions
 * @{
 */
/*!
 * @addtogroup BOOT_SYSCTRL_ACTIONS_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_SYSCTRL_CGU_LOCKINGEN	28    /*!< Locks the register after the write access */
#define BITP_ROM_SYSCTRL_WUA_OVERRIDE   24    /*!< Save MMR instead of structure member */
#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define BITP_ROM_SYSCTRL_WUA_BCODE      21    /*!< Save the RCU0_BCODE register */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define BITP_ROM_SYSCTRL_WUA_BCODE      21    /*!< Save the RCU0_BCODE register */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#else
# error "Unknown silicon revision"
#endif


#define BITP_ROM_SYSCTRL_WUA_DDRDLLEN   20    /*!< Save and restore the DMC DLLCTL and PADS registers */
#define BITP_ROM_SYSCTRL_WUA_DDR        19    /*!< Save and restore the DMC registers to the DPM registers */
#define BITP_ROM_SYSCTRL_WUA_CGU        18    /*!< Saves the CGU registers into DPM registers */
#define BITP_ROM_SYSCTRL_WUA_DPMWRITE   17    /*!< Saves the Syscontrol structure structure contents into DPM registers */
#define BITP_ROM_SYSCTRL_WUA_EN         16    /*!< reads current PLL and DDR configuration into structure */
#define BITP_ROM_SYSCTRL_DDR_WRITE      13    /*!< writes the DDR registers from Syscontrol structure for wakeup initialization of DDR */
#define BITP_ROM_SYSCTRL_DDR_READ       12    /*!< Read the DDR registers into the Syscontrol structure for storing prior to hibernate */
#define BITP_ROM_SYSCTRL_CGU_AUTODIS    11    /*!< Disables auto handling of UPDT and ALGN fields */
#define BITP_ROM_SYSCTRL_CGU_CLKOUTSEL  7    /*!< access CGU_CLKOUTSEL register */
#define BITP_ROM_SYSCTRL_CGU_DIV        6    /*!< access CGU_DIV register */
#define BITP_ROM_SYSCTRL_CGU_STAT       5    /*!< access CGU_STAT register */
#define BITP_ROM_SYSCTRL_CGU_CTL        4    /*!< access CGU_CTL register */
#define BITP_ROM_SYSCTRL_CGU_RTNSTAT    2    /*!< Update structure STAT field upon error */
#define BITP_ROM_SYSCTRL_CGU_WRITE      1    /*!< write CGU registers */
#define BITP_ROM_SYSCTRL_CGU_READ       0    /*!< read CGU registers */
#define BITP_ROM_SYSCTRL_WRITE          1    /*!< write CGU registers legacy bit name  */
#define BITP_ROM_SYSCTRL_READ           0    /*!< read CGU registers legacy bit name */


#define BITM_ROM_SYSCTRL_CGU_READ     	(_ADI_MSK(0x00000001,uint32_t))    /*!< Read CGU registers */
#define BITM_ROM_SYSCTRL_CGU_WRITE      (_ADI_MSK(0x00000002,uint32_t))    /*!< Write registers */
#define BITM_ROM_SYSCTRL_CGU_RTNSTAT    (_ADI_MSK(0x00000004,uint32_t))    /*!< Update structure STAT field upon error or after a write operation */
#define BITM_ROM_SYSCTRL_CGU_CTL        (_ADI_MSK(0x00000010,uint32_t))    /*!< Access CGU_CTL register */
#define BITM_ROM_SYSCTRL_CGU_STAT       (_ADI_MSK(0x00000020,uint32_t))    /*!< Access CGU_STAT register */
#define BITM_ROM_SYSCTRL_CGU_DIV        (_ADI_MSK(0x00000040,uint32_t))    /*!< Access CGU_DIV register */
#define BITM_ROM_SYSCTRL_CGU_CLKOUTSEL  (_ADI_MSK(0x00000080,uint32_t))    /*!< Access CGU_CLKOUTSEL register */
#define BITM_ROM_SYSCTRL_CGU_AUTODIS    (_ADI_MSK(0x00000800,uint32_t))    /*!< Disables auto handling of UPDT and ALGN fields */
#define BITM_ROM_SYSCTRL_DDR_READ       (_ADI_MSK(0x00001000,uint32_t))    /*!< Reads the contents of the DDR registers and stores them into the structure */
#define BITM_ROM_SYSCTRL_DDR_WRITE      (_ADI_MSK(0x00002000,uint32_t))    /*!< Writes the DDR registers from the structure, only really intented for wakeup functionality and not for full DDR configuration */
#define BITM_ROM_SYSCTRL_WUA_EN         (_ADI_MSK(0x00010000,uint32_t))    /*!< Wakeup entry or exit opertation enable */
#define BITM_ROM_SYSCTRL_WUA_DPMWRITE   (_ADI_MSK(0x00020000,uint32_t))    /*!< When set indicates a restore of the PLL and DDR is to be performed otherwise a save is required */
#define BITM_ROM_SYSCTRL_WUA_CGU        (_ADI_MSK(0x00040000,uint32_t))    /*!< Only applicable for a PLL and DDR save operation to the DPM, saves the current settings if cleared or the contents of the structure if set */
#define BITM_ROM_SYSCTRL_WUA_DDR        (_ADI_MSK(0x00080000,uint32_t))    /*!< Only applicable for a PLL and DDR save operation to the DPM, saves the current settings if cleared or the contents of the structure if set */
#define BITM_ROM_SYSCTRL_WUA_DDRDLLEN   (_ADI_MSK(0x00100000,uint32_t))    /*!< Enables saving/restoring of the DDR DLLCTL register */
#if !defined(__SILICON_REVISION__) || (defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0xFFFF))
#define BITM_ROM_SYSCTRL_WUA_BCODE      (_ADI_MSK(0x00200000,uint32_t))    /*!< Save the RCU0_BCODE register */
#elif defined(__SILICON_REVISION__) && ( (__SILICON_REVISION__ == 1) || (__SILICON_REVISION__ == 2) )
#define BITM_ROM_SYSCTRL_WUA_BCODE      (_ADI_MSK(0x00200000,uint32_t))    /*!< Save the RCU0_BCODE register */
#elif defined(__SILICON_REVISION__) && (__SILICON_REVISION__ == 0)
#else
# error "Unknown silicon revision"
#endif
#define BITM_ROM_SYSCTRL_WUA_OVERRIDE   (_ADI_MSK(0x01000000,uint32_t))    /*!< Save MMR instead of structure member */
#define BITM_ROM_SYSCTRL_CGU_LOCKINGEN  (_ADI_MSK(0x10000000,uint32_t))    /*!< Locks the register after the write access */
/*! @} */
/*! @} */

/*!
 * @addtogroup BOOT_SYSCTRL_RESULT Syscontrol Return Results
 * @{
 */
/*!
 * @addtogroup BOOT_SYSCTRL_RESULT_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_ROM_SYSCTRL_CGU_STATERR    8    /*!< Error during a CGU0_STATUS polling operation */
#define BITP_ROM_SYSCTRL_CGU_WFIERR     6    /*!< Error due to WFI being set in write value */
#define BITP_ROM_SYSCTRL_CGU_CONFIGERR  5    /*!< Configuration Error */
#define BITP_ROM_SYSCTRL_CGU_CTLINVERR  4	 /*!< Not implemented in boot code */
#define BITP_ROM_SYSCTRL_WRITEERR       1    /*!< PLL in bypass mode, cannot update configuration */
#define BITP_ROM_SYSCTRL_READERR        0    /*!< Error in polling CGU STAT for CGU configuration update */

#define BITM_ROM_SYSCTRL_READERR        (_ADI_MSK(0x00000001,uint32_t))    /*!< Error in polling CGU STAT for CGU configuration update */
#define BITM_ROM_SYSCTRL_WRITEERR       (_ADI_MSK(0x00000002,uint32_t))    /*!< PLL in bypass mode, cannot update configuration */
#define BITM_ROM_SYSCTRL_CGU_CTLINVERR  (_ADI_MSK(0x00000010,uint32_t))    /*!< Not implemented in boot code */
#define BITM_ROM_SYSCTRL_CGU_CONFIGERR  (_ADI_MSK(0x00000020,uint32_t))    /*!< Configuration Error */
#define BITM_ROM_SYSCTRL_CGU_WFIERR     (_ADI_MSK(0x00000040,uint32_t))    /*!< Error due to WFI being set in write value */
#define BITM_ROM_SYSCTRL_CGU_STATERR    (_ADI_MSK(0x00000100,uint32_t))    /*!< Error during a CGU0_STATUS polling operation */
/*! @} */
/*! @} */
/*! @} */

/*********************************************************************************************/
/*                                                                                           */
/*  RCU_BCODE MMR Register                                                                   */
/*                                                                                           */
/*********************************************************************************************/
/*!
 * @addtogroup BOOT_RCUBCODE RCU0_BCODE Register
 * @brief Functionality related to the RCU0_BCODE register
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_RCUBCODE_BITS bitmasks, positions and enumerations
 * @{
 */
#define BITP_RCU_BCODE_NOCORE1      17 /*!< Skip all Core 1 operations */
#define BITP_RCU_BCODE_NOHOOK       10 /*!< Do not execute the hook routine */
#define BITP_RCU_BCODE_NOPREBOOT    9  /*!< Do not execute the preboot sequence */
#define BITP_RCU_BCODE_NOFAULTS     6  /*!< Do not execute the fault management and configuration */
#define BITP_RCU_BCODE_NOCACHE      5  /*!< Do not initialize the cache */
#define BITP_RCU_BCODE_NOMEMINIT    4  /*!< Do not perform memory initialization */
#define BITP_RCU_BCODE_HBTOVW       3  /*!< Execute the wakeup from hibernate sequence and set the global wakeup flag for the boot stream */
#define BITP_RCU_BCODE_HALT         2  /*!< Execute the No Boot mode */
#define BITP_RCU_BCODE_NOVECTINIT   1  /*!< Do not initialize the soft reset vector registers */
#define BITP_RCU_BCODE_NOKERNEL     0  /*!< Do not call the boot kernel */

#define BITM_RCU_BCODE_NOCORE1      (_ADI_MSK(0x00020000,uint32_t)) /*!< Skip all Core 1 operations */
#define BITM_RCU_BCODE_NOHOOK       (_ADI_MSK(0x00000400,uint32_t)) /*!< Do not execute the hook routine */
#define BITM_RCU_BCODE_NOPREBOOT    (_ADI_MSK(0x00000200,uint32_t)) /*!< Do not execute the preboot sequence */
#define BITM_RCU_BCODE_NOFAULTS     (_ADI_MSK(0x00000040,uint32_t)) /*!< Do not execute the fault management and configuration */
#define BITM_RCU_BCODE_NOCACHE      (_ADI_MSK(0x00000020,uint32_t)) /*!< Do not initialize the cache */
#define BITM_RCU_BCODE_NOMEMINIT    (_ADI_MSK(0x00000010,uint32_t)) /*!< Do not perform memory initialization */
#define BITM_RCU_BCODE_HBTOVW       (_ADI_MSK(0x00000008,uint32_t)) /*!< Execute the wakeup from hibernate sequence and set the global wakeup flag for the boot stream */
#define BITM_RCU_BCODE_HALT         (_ADI_MSK(0x00000004,uint32_t)) /*!< Execute the No Boot mode */
#define BITM_RCU_BCODE_NOVECTINIT   (_ADI_MSK(0x00000002,uint32_t)) /*!< Do not initialize the soft reset vector registers */
#define BITM_RCU_BCODE_NOKERNEL     (_ADI_MSK(0x00000001,uint32_t)) /*!< Do not call the boot kernel */
/*! @} */
/*! @} */

/***********************************************************/
/*                                                         */
/* GetAddress Enumerations                                 */
/*                                                         */
/***********************************************************/
/*!
 * @addtogroup BOOT_GETADDRESS GetAddress
 * @brief GetAddress provides access to the various lookup tables in the boot rom.
 * @details None
 * @{
 */
/*!
 * @addtogroup BOOT_GETADDRESS_BITS bitmasks, positions and enumerations
 * @{
 */
#define ENUM_GETADDR_CONSTANTS      0 /*!< Boot ROM Constants and Revision Information */
#define ENUM_GETADDR_BOOTDEF        1 /*!< All Boot Mode default Boot Commands */
#define ENUM_GETADDR_ECCSYN         2 /*!< ECC Data */
#define ENUM_GETADDR_LUT            3 /*!< Lookup table contain all the entries accessible via GetAddress */
#define ENUM_GETADDR_MDMACODE       4 /*!< Memory boot mode BCODE values */
#define ENUM_GETADDR_SPIMCODE       5 /*!< SPI Master boot mode BCODE values */
#define ENUM_GETADDR_RSICODE        6 /*!< RSI boot mode BCODE values */
#define ENUM_GETADDR_SBSTCHKSUM    10 /*!< SBST ROM checksum result */
/*! @} */
/*! @} */

#if defined(_MISRA_2004_RULES) || defined(_MISRA_2012_RULES)
#pragma diag(pop)
#endif /* _MISRA_2004_RULES */

/*! @} */

#endif /* _DEF_BF609_ROM_H */
