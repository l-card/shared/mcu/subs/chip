#ifndef CHIP_ISR_CMSIS_H
#define CHIP_ISR_CMSIS_H

#include "chip_dev_spec_features.h"

typedef enum {
    /******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
    NonMaskableInt_IRQn         = -14,    /*!< 2 Non Maskable Interrupt                             */
    HardFault_IRQn              = -13,    /*!< 3 Cortex-M3 Hard Fault Interrupt                     */
    MemoryManagement_IRQn       = -12,    /*!< 4 Cortex-M3 Memory Management Interrupt              */
    BusFault_IRQn               = -11,    /*!< 5 Cortex-M3 Bus Fault Interrupt                      */
    UsageFault_IRQn             = -10,    /*!< 6 Cortex-M3 Usage Fault Interrupt                    */
    SVCall_IRQn                 = -5,     /*!< 11 Cortex-M3 SV Call Interrupt                       */
    DebugMonitor_IRQn           = -4,     /*!< 12 Cortex-M3 Debug Monitor Interrupt                 */
    PendSV_IRQn                 = -2,     /*!< 14 Cortex-M3 Pend SV Interrupt                       */
    SysTick_IRQn                = -1,     /*!< 15 Cortex-M3 System Tick Interrupt                   */

    /******  STM32 specific Interrupt Numbers *********************************************************/
    WWDG_IRQn                   = 0,      /*!< Window WatchDog Interrupt                            */
    PVD_IRQn                    = 1,      /*!< PVD through EXTI Line detection Interrupt            */
    TAMPER_IRQn                 = 2,      /*!< Tamper Interrupt                                     */
    RTC_IRQn                    = 3,      /*!< RTC global Interrupt                                 */
    FLASH_IRQn                  = 4,      /*!< FLASH global Interrupt                               */
    RCC_IRQn                    = 5,      /*!< RCC global Interrupt                                 */
    EXTI0_IRQn                  = 6,      /*!< EXTI Line0 Interrupt                                 */
    EXTI1_IRQn                  = 7,      /*!< EXTI Line1 Interrupt                                 */
    EXTI2_IRQn                  = 8,      /*!< EXTI Line2 Interrupt                                 */
    EXTI3_IRQn                  = 9,      /*!< EXTI Line3 Interrupt                                 */
    EXTI4_IRQn                  = 10,     /*!< EXTI Line4 Interrupt                                 */
    DMA1_Channel1_IRQn          = 11,     /*!< DMA1 Channel 1 global Interrupt                      */
    DMA1_Channel2_IRQn          = 12,     /*!< DMA1 Channel 2 global Interrupt                      */
    DMA1_Channel3_IRQn          = 13,     /*!< DMA1 Channel 3 global Interrupt                      */
    DMA1_Channel4_IRQn          = 14,     /*!< DMA1 Channel 4 global Interrupt                      */
    DMA1_Channel5_IRQn          = 15,     /*!< DMA1 Channel 5 global Interrupt                      */
    DMA1_Channel6_IRQn          = 16,     /*!< DMA1 Channel 6 global Interrupt                      */
    DMA1_Channel7_IRQn          = 17,     /*!< DMA1 Channel 7 global Interrupt                      */
    ADC1_2_IRQn                 = 18,     /*!< ADC1 and ADC2 global Interrupt                       */
    USB_HP_CAN1_TX_IRQn         = 19,     /*!< USB Device High Priority or CAN1 TX Interrupts       */
    USB_LP_CAN1_RX0_IRQn        = 20,     /*!< USB Device Low Priority or CAN1 RX0 Interrupts       */
    CAN1_RX1_IRQn               = 21,     /*!< CAN1 RX1 Interrupt                                   */
    CAN1_SCE_IRQn               = 22,     /*!< CAN1 SCE Interrupt                                   */
    EXTI9_5_IRQn                = 23,     /*!< External Line[9:5] Interrupts                        */
    TIM1_BRK_IRQn               = 24,     /*!< TIM1 Break Interrupt                                 */
    TIM1_UP_IRQn                = 25,     /*!< TIM1 Update Interrupt                                */
    TIM1_TRG_COM_IRQn           = 26,     /*!< TIM1 Trigger and Commutation Interrupt               */
    TIM1_CC_IRQn                = 27,     /*!< TIM1 Capture Compare Interrupt                       */
    TIM2_IRQn                   = 28,     /*!< TIM2 global Interrupt                                */
    TIM3_IRQn                   = 29,     /*!< TIM3 global Interrupt                                */
    TIM4_IRQn                   = 30,     /*!< TIM4 global Interrupt                                */
    I2C1_EV_IRQn                = 31,     /*!< I2C1 Event Interrupt                                 */
    I2C1_ER_IRQn                = 32,     /*!< I2C1 Error Interrupt                                 */
    I2C2_EV_IRQn                = 33,     /*!< I2C2 Event Interrupt                                 */
    I2C2_ER_IRQn                = 34,     /*!< I2C2 Error Interrupt                                 */
    SPI1_IRQn                   = 35,     /*!< SPI1 global Interrupt                                */
    SPI2_IRQn                   = 36,     /*!< SPI2 global Interrupt                                */
    USART1_IRQn                 = 37,     /*!< USART1 global Interrupt                              */
    USART2_IRQn                 = 38,     /*!< USART2 global Interrupt                              */
    USART3_IRQn                 = 39,     /*!< USART3 global Interrupt                              */
    EXTI15_10_IRQn              = 40,     /*!< External Line[15:10] Interrupts                      */
    RTC_Alarm_IRQn              = 41,     /*!< RTC Alarm through EXTI Line Interrupt                */
    USBWakeUp_IRQn              = 42,     /*!< USB Device WakeUp from suspend through EXTI Line Interrupt */
#if defined CHIP_DEV_STM32F103_XE || defined CHIP_DEV_CH32F20X_D8 || defined CHIP_DEV_CH32F20X_D8C
    TIM8_BRK_IRQn               = 43,     /*!< TIM8 Break Interrupt                                 */
    TIM8_UP_IRQn                = 44,     /*!< TIM8 Update Interrupt                                */
    TIM8_TRG_COM_IRQn           = 45,     /*!< TIM8 Trigger and Commutation Interrupt               */
    TIM8_CC_IRQn                = 46,     /*!< TIM8 Capture Compare Interrupt                       */
#if defined CHIP_DEV_STM32F103_XE
    ADC3_IRQn                   = 47,     /*!< ADC3 global Interrupt                                */
#elif defined CHIP_DEV_CH32F20X_D8  || defined CHIP_DEV_CH32F20X_D8C
    RNG_IRQn                    = 47,     /*!< RNG global Interrupt                                 */
#endif
    FSMC_IRQn                   = 48,     /*!< FSMC global Interrupt                                */
    SDIO_IRQn                   = 49,     /*!< SDIO global Interrupt                                */
    TIM5_IRQn                   = 50,     /*!< TIM5 global Interrupt                                */
    SPI3_IRQn                   = 51,     /*!< SPI3 global Interrupt                                */
    UART4_IRQn                  = 52,     /*!< UART4 global Interrupt                               */
    UART5_IRQn                  = 53,     /*!< UART5 global Interrupt                               */
    TIM6_IRQn                   = 54,     /*!< TIM6 global Interrupt                                */
    TIM7_IRQn                   = 55,     /*!< TIM7 global Interrupt                                */
    DMA2_Channel1_IRQn          = 56,     /*!< DMA2 Channel 1 global Interrupt                      */
    DMA2_Channel2_IRQn          = 57,     /*!< DMA2 Channel 2 global Interrupt                      */
    DMA2_Channel3_IRQn          = 58,     /*!< DMA2 Channel 3 global Interrupt                      */
#if defined CHIP_DEV_STM32F103_XE
    DMA2_Channel4_5_IRQn        = 59,     /*!< DMA2 Channel 4 and Channel 5 global Interrupt        */
#elif defined CHIP_DEV_CH32F20X_D8 || defined CHIP_DEV_CH32F20X_D8C
    DMA2_Channel4_IRQn          = 59,      /* DMA2 Channel 4 global Interrupt                      */
    DMA2_Channel5_IRQn          = 60,      /* DMA2 Channel 5 global Interrupt                      */
#if defined CHIP_DEV_CH32F20X_D8C
    ETH_IRQn                    = 61,      /* ETH global Interrupt                                 */
    ETH_WKUP_IRQn               = 62,      /* ETH WakeUp Interrupt                                 */
    CAN2_TX_IRQn                = 63,      /* CAN2 TX Interrupts                                   */
    CAN2_RX0_IRQn               = 64,      /* CAN2 RX0 Interrupts                                  */
    CAN2_RX1_IRQn               = 65,      /* CAN2 RX1 Interrupt                                   */
    CAN2_SCE_IRQn               = 66,      /* CAN2 SCE Interrupt                                   */
    OTG_FS_IRQn                 = 67,      /* OTGFS global Interrupt                               */
    USBHSWakeup_IRQn            = 68,      /* USBHS WakeUp Interrupt                               */
    USBHS_IRQn                  = 69,      /* USBHS global Interrupt                               */
    DVP_IRQn                    = 70,      /* DVP global Interrupt                                 */
#endif
    UART6_IRQn                  = 71,      /* UART6 global Interrupt                               */
    UART7_IRQn                  = 72,      /* UART7 global Interrupt                               */
    UART8_IRQn                  = 73,      /* UART8 global Interrupt                               */
    TIM9_BRK_IRQn               = 74,      /* TIM9 Break Interrupt                                 */
    TIM9_UP_IRQn                = 75,      /* TIM9 Update Interrupt                                */
    TIM9_TRG_COM_IRQn           = 76,      /* TIM9 Trigger and Commutation Interrupt               */
    TIM9_CC_IRQn                = 77,      /* TIM9 Capture Compare Interrupt                       */
    TIM10_BRK_IRQn              = 78,      /* TIM10 Break Interrupt                                */
    TIM10_UP_IRQn               = 79,      /* TIM10 Update Interrupt                               */
    TIM10_TRG_COM_IRQn          = 80,      /* TIM10 Trigger and Commutation Interrupt              */
    TIM10_CC_IRQn               = 81,      /* TIM10 Capture Compare Interrupt                      */
    DMA2_Channel6_IRQn          = 82,      /* DMA2 Channel 6 global Interrupt                      */
    DMA2_Channel7_IRQn          = 83,      /* DMA2 Channel 7 global Interrupt                      */
    DMA2_Channel8_IRQn          = 84,      /* DMA2 Channel 8 global Interrupt                      */
    DMA2_Channel9_IRQn          = 85,      /* DMA2 Channel 9 global Interrupt                      */
    DMA2_Channel10_IRQn         = 86,      /* DMA2 Channel 10 global Interrupt                     */
    DMA2_Channel11_IRQn         = 87,      /* DMA2 Channel 11 global Interrupt                     */
#endif
#endif
#if defined CHIP_DEV_CH32F20X_D6 || defined CHIP_DEV_CH32F20X_D8W
    USBHD_IRQn                  = 43,      /* USBHD global Interrupt                               */
    USBHDWakeUp_IRQn            = 44,      /* USB Host/Device WakeUp Interrupt                     */
#if defined CHIP_DEV_CH32F20X_D6
    UART4_IRQn                  = 45,      /* UART4 global Interrupt                               */
    DMA1_Channel8_IRQn          = 46,      /* DMA1 Channel 8 global Interrupt                      */
#elif defined CHIP_DEV_CH32F20X_D8W
    ETH_IRQn                    = 45,      /* ETH global Interrupt                                 */
    ETHWakeUp_IRQn              = 46,      /* ETH WakeUp Interrupt                                 */
    BLEC_IRQn                   = 47,      /* BLEC global Interrupt                                */
    BLES_IRQn                   = 48,      /* BLES global Interrupt                                */
    TIM5_IRQn                   = 49,      /* TIM5 global Interrupt                                */
    UART4_IRQn                  = 50,      /* UART4 global Interrupt                               */
    DMA1_Channel8_IRQn          = 51,      /* DMA1 Channel 8 global Interrupt                      */
    OSC32KCal_IRQn              = 52,      /* OSC32K global Interrupt                              */
    OSCWakeUp_IRQn              = 53,      /* OSC32K WakeUp Interrupt                              */
#endif
#endif



} IRQn_Type;

#endif // CHIP_ISR_CMSIS_H
