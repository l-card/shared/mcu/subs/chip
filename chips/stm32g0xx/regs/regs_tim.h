#ifndef CHIP_STM32G0XX_REGS_TIM_H
#define CHIP_STM32G0XX_REGS_TIM_H

#define CHIP_SUPPORT_TIM_UIF_REMAP              1
#define CHIP_SUPPORT_TIM_OUT_5_6                1
#define CHIP_SUPPORT_TIM_EXT_SYNC_MODE          1
#define CHIP_SUPPORT_TIM_EXT_BREAK              1
#define CHIP_SUPPORT_TIM_EXT_OUT_MODE           1

#include "chip_dev_spec_features.h"
#include "stm32_tim_gen_regs.h"


#define CHIP_REGS_TIM1          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM1)
#define CHIP_REGS_TIM2          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM2)
#define CHIP_REGS_TIM3          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM3)
#if CHIP_DEV_SUPPORT_TIM4
    #define CHIP_REGS_TIM4          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM4)
#endif
#if CHIP_DEV_SUPPORT_TIM6
    #define CHIP_REGS_TIM6          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM6)
#endif
#if CHIP_DEV_SUPPORT_TIM7
    #define CHIP_REGS_TIM7          ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM7)
#endif
#define CHIP_REGS_TIM14         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM14)
#if CHIP_DEV_SUPPORT_TIM5
    #define CHIP_REGS_TIM15         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM15)
#endif
#define CHIP_REGS_TIM16         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM16)
#define CHIP_REGS_TIM17         ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM17)



/*******************  Bit definition for TIM1_OR1 register  *******************/
#define CHIP_REGFLD_TIM1_OR1_OCREF_CLR        (0x00000003UL <<  0U) /* OCREF_CLR input selection */

#define CHIP_REGFLDVAL_TIM1_OR1_OCREF_CLR_COMP1    0
#define CHIP_REGFLDVAL_TIM1_OR1_OCREF_CLR_COMP2    1
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM1_OR1_OCREF_CLR_COMP3    2
#endif


/*******************  Bit definition for TIM1_AF1 register  *******************/
#define CHIP_REGFLD_TIM1_AF1_BKINE            (0x00000001UL <<  0U)  /* BRK BKIN input enable */
#define CHIP_REGFLD_TIM1_AF1_BKCMP1E          (0x00000001UL <<  1U)  /* BRK COMP1 enable */
#define CHIP_REGFLD_TIM1_AF1_BKCMP2E          (0x00000001UL <<  2U)  /* BRK COMP2 enable */
#define CHIP_REGFLD_TIM1_AF1_BKCMP3E          (0x00000001UL <<  3U)  /* BRK COMP3 enable */
#define CHIP_REGFLD_TIM1_AF1_BKINP            (0x00000001UL <<  9U)  /* BRK BKIN input polarity */
#define CHIP_REGFLD_TIM1_AF1_BKCMP1P          (0x00000001UL << 10U)  /* BRK COMP1 input polarity */
#define CHIP_REGFLD_TIM1_AF1_BKCMP2P          (0x00000001UL << 11U)  /* BRK COMP2 input polarity */
#define CHIP_REGFLD_TIM1_AF1_BKCMP3P          (0x00000001UL << 12U)  /* BRK COMP3 input polarity */
#define CHIP_REGFLD_TIM1_AF1_ETRSEL           (0x0000000FUL << 14U)  /* TIM1 ETR source selection */

#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_LEGACY    0 /* ETR legacy mode */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_COMP1     1 /* COMP1 output */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_COMP2     2 /* COMP2 output */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC1_AWD1 3 /* ADC1 AWD1 */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC1_AWD2 4 /* ADC1 AWD2 */
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_ADC1_AWD3 5 /* ADC1 AWD3 */
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM1_AF1_ETRSEL_COMP3     6 /* COMP3 output */
#endif


/*******************  Bit definition for TIM1_AF2 register  *******************/
#define CHIP_REGFLD_TIM1_AF2_BK2INE           (0x00000001UL <<  0U) /* BRK2 BKIN2 input enable */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP1E         (0x00000001UL <<  1U) /* BRK2 COMP1 enable */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP2E         (0x00000001UL <<  2U) /* BRK2 COMP2 enable */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP3E         (0x00000001UL <<  3U) /* BRK2 COMP3 enable */
#define CHIP_REGFLD_TIM1_AF2_BK2INP           (0x00000001UL <<  9U) /* BRK2 BKIN2 input polarity */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP1P         (0x00000001UL << 10U) /* BRK2 COMP1 input polarity */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP2P         (0x00000001UL << 11U) /* BRK2 COMP2 input polarity */
#define CHIP_REGFLD_TIM1_AF2_BK2CMP3P         (0x00000001UL << 12U) /* BRK2 COMP3 input polarity */

/*******************  Bit definition for TIM2_OR1 register  *******************/
#define CHIP_REGFLD_TIM2_OR1_OCREF_CLR        (0x00000003UL <<  0U) /* OCREF_CLR input selection */

#define CHIP_REGFLDVAL_TIM2_OR1_OCREF_CLR_COMP1    0
#define CHIP_REGFLDVAL_TIM2_OR1_OCREF_CLR_COMP2    1
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM2_OR1_OCREF_CLR_COMP3    2
#endif

/*******************  Bit definition for TIM2_AF1 register  *******************/
#define CHIP_REGFLD_TIM2_AF1_ETRSEL           (0x0000000FUL << 14U)  /* TIM2 ETR source selection */

#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_LEGACY    0 /* ETR legacy mode */
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_COMP1     1 /* COMP1 output */
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_COMP2     2 /* COMP2 output */
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_LSE       3 /* LSE */
#if CHIP_DEV_SUPPORT_MCO_CFG_EX
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_MCO       4 /* MCO */
#endif
#if CHIP_DEV_SUPPORT_MC02
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_MCO2      5 /* MCO2 */
#endif
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM2_AF1_ETRSEL_COMP3     6 /* COMP3 output */
#endif

/*******************  Bit definition for TIM3_OR1 register  *******************/
#define CHIP_REGFLD_TIM3_OR1_OCREF_CLR        (0x00000003UL <<  0U) /* OCREF_CLR input selection */

#define CHIP_REGFLDVAL_TIM3_OR1_OCREF_CLR_COMP1    0
#define CHIP_REGFLDVAL_TIM3_OR1_OCREF_CLR_COMP2    1
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM3_OR1_OCREF_CLR_COMP3    2
#endif

/*******************  Bit definition for TIM3_AF1 register  *******************/
#define CHIP_REGFLD_TIM3_AF1_ETRSEL           (0x0000000FUL << 14U)  /* TIM3 ETR source selection */

#define CHIP_REGFLDVAL_TIM3_AF1_ETRSEL_LEGACY    0 /* ETR legacy mode */
#define CHIP_REGFLDVAL_TIM3_AF1_ETRSEL_COMP1     1 /* COMP1 output */
#define CHIP_REGFLDVAL_TIM3_AF1_ETRSEL_COMP2     2 /* COMP2 output */
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM3_AF1_ETRSEL_COMP3     6 /* COMP3 output */
#endif

/*******************  Bit definition for TIM4_OR1 register  *******************/
#define CHIP_REGFLD_TIM4_OR1_OCREF_CLR        (0x00000003UL <<  0U) /* OCREF_CLR input selection */

#define CHIP_REGFLDVAL_TIM4_OR1_OCREF_CLR_COMP1    0
#define CHIP_REGFLDVAL_TIM4_OR1_OCREF_CLR_COMP2    1
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM4_OR1_OCREF_CLR_COMP3    2
#endif

/*******************  Bit definition for TIM4_AF1 register  *******************/
#define CHIP_REGFLD_TIM4_AF1_ETRSEL           (0x0000000FUL << 14U)  /* TIM4 ETR source selection */

#define CHIP_REGFLDVAL_TIM4_AF1_ETRSEL_LEGACY    0 /* ETR legacy mode */
#define CHIP_REGFLDVAL_TIM4_AF1_ETRSEL_COMP1     1 /* COMP1 output */
#define CHIP_REGFLDVAL_TIM4_AF1_ETRSEL_COMP2     2 /* COMP2 output */
#if CHIP_DEV_SUPPORT_COMP3
#define CHIP_REGFLDVAL_TIM4_AF1_ETRSEL_COMP3     6 /* COMP3 output */
#endif

/*******************  Bit definition for TIM15_AF1 register  ******************/
#define CHIP_REGFLD_TIM15_AF1_BKINE          (0x00000001UL <<  0U) /* BRK BKIN input enable */
#define CHIP_REGFLD_TIM15_AF1_BKCMP1E        (0x00000001UL <<  1U) /* BRK COMP1 enable */
#define CHIP_REGFLD_TIM15_AF1_BKCMP2E        (0x00000001UL <<  2U) /* BRK COMP2 enable */
#define CHIP_REGFLD_TIM15_AF1_BKCMP3E        (0x00000001UL <<  3U) /* BRK COMP3 enable */
#define CHIP_REGFLD_TIM15_AF1_BKINP          (0x00000001UL <<  9U) /* BRK BKIN input polarity */
#define CHIP_REGFLD_TIM15_AF1_BKCMP1P        (0x00000001UL << 10U) /* BRK COMP1 input polarity */
#define CHIP_REGFLD_TIM15_AF1_BKCMP2P        (0x00000001UL << 11U) /* BRK COMP2 input polarity */
#define CHIP_REGFLD_TIM15_AF1_BKCMP3P        (0x00000001UL << 12U) /* BRK COMP3 input polarity */

/*******************  Bit definition for TIM16_AF1 register  ******************/
#define CHIP_REGFLD_TIM16_AF1_BKINE          (0x00000001UL <<  0U) /* BRK BKIN input enable */
#define CHIP_REGFLD_TIM16_AF1_BKCMP1E        (0x00000001UL <<  1U) /* BRK COMP1 enable */
#define CHIP_REGFLD_TIM16_AF1_BKCMP2E        (0x00000001UL <<  2U) /* BRK COMP2 enable */
#define CHIP_REGFLD_TIM16_AF1_BKCMP3E        (0x00000001UL <<  3U) /* BRK COMP3 enable */
#define CHIP_REGFLD_TIM16_AF1_BKINP          (0x00000001UL <<  9U) /* BRK BKIN input polarity */
#define CHIP_REGFLD_TIM16_AF1_BKCMP1P        (0x00000001UL << 10U) /* BRK COMP1 input polarity */
#define CHIP_REGFLD_TIM16_AF1_BKCMP2P        (0x00000001UL << 11U) /* BRK COMP2 input polarity */
#define CHIP_REGFLD_TIM16_AF1_BKCMP3P        (0x00000001UL << 12U) /* BRK COMP3 input polarity */

/*******************  Bit definition for TIM17_AF1 register  ******************/
#define CHIP_REGFLD_TIM17_AF1_BKINE          (0x00000001UL <<  0U) /* BRK BKIN input enable */
#define CHIP_REGFLD_TIM17_AF1_BKCMP1E        (0x00000001UL <<  1U) /* BRK COMP1 enable */
#define CHIP_REGFLD_TIM17_AF1_BKCMP2E        (0x00000001UL <<  2U) /* BRK COMP2 enable */
#define CHIP_REGFLD_TIM17_AF1_BKCMP3E        (0x00000001UL <<  3U) /* BRK COMP3 enable */
#define CHIP_REGFLD_TIM17_AF1_BKINP          (0x00000001UL <<  9U) /* BRK BKIN input polarity */
#define CHIP_REGFLD_TIM17_AF1_BKCMP1P        (0x00000001UL << 10U) /* BRK COMP1 input polarity */
#define CHIP_REGFLD_TIM17_AF1_BKCMP2P        (0x00000001UL << 11U) /* BRK COMP2 input polarity */
#define CHIP_REGFLD_TIM17_AF1_BKCMP3P        (0x00000001UL << 12U) /* BRK COMP3 input polarity */



#endif // CHIP_STM32G0XX_REGS_TIM_H

