/*  Файл определяет набор макросов для выполнения операций, зависящий от
    компилятора (VisualDSP или GCC) */

#ifndef CHIP_BFIN_CDEFS_H
#define CHIP_BFIN_CDEFS_H

/** Макрос для помещения переменной или функции в определенную секцию */
#ifdef __GNUC__
    #define SECTION(sect, member)  member __attribute__((section(sect)))
#else
    #define SECTION(sect, member)  section(sect) member
#endif

/** Марос для помещения переменной в неинициализируемую область SDRAM */
#ifdef __GNUC__
    #define MEM_SDRAM_NOINIT(variable) SECTION(".sdram_noinit", variable)
#else
    #define MEM_SDRAM_NOINIT(variable) section("sdram_noinit", NO_INIT) variable
#endif

/** Макрос для описания обработчика прерываний */
#ifdef __GNUC__
    #define ISR(handler) __attribute__((interrupt_handler,nesting)) void handler(void)
#else
    #define ISR(handler) EX_INTERRUPT_HANDLER(handler)
#endif

/** Макрос для регистрации обработчика прерываний */
#ifdef __GNUC__
    #define REGISTER_ISR(ivg, isr) do { \
        int i=0; \
        ssync(); \
        *pEVT##ivg = isr; \
        ssync(); \
        __asm__ volatile ("cli %0; bitset (%0, %1); sti %0; csync;": "+d"(i) : "i"(ivg)); \
        } while(0)
#else
    #define REGISTER_ISR(ivg, isr) register_handler(ik_ivg##ivg, isr)
#endif


#ifdef __GNUC__
    /* запрет прерываний с сохранением в var текущего состояния */
    #define irq_disable_save(var) \
        __asm__ volatile ("cli %[reg]" : [reg] "=d" (var) :: "memory", "cc")
    /* восстановление состояния прерываний */
    #define irq_restore(var) \
        __asm__ volatile ("sti %[reg]" : : [reg] "d" (var): "memory", "cc")

    /* выполнение операции, защищенной от ложного спекулятивного
       выполнения, когда лишнее повторное операции недопустимо */
    #define GUARDED_OP(_op) do { \
        int _guard_cntr_irq = 0; \
        irq_disable_save(_guard_cntr_irq); \
        __asm__ volatile ("nop; nop; nop;"); \
        _op; \
        irq_restore(_guard_cntr_irq); \
    } while(0)
#endif



#endif // CHIP_BFIN_CDEFS_H
