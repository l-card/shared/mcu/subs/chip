#ifndef CHIP_SPEC_CSR_H
#define CHIP_SPEC_CSR_H


/* NON-STANDARD CRS address definition */
#define CHIP_CSR_MILMB              (0x7C0) /* Instruction local memory base register */
#define CHIP_CSR_MDLMB              (0x7C1) /* Data local memory base register */
#define CHIP_CSR_MECC_CODE          (0x7C2) /* ECC code register */
#define CHIP_CSR_MNVEC              (0x7C3) /* NMI vector base address register */
#define CHIP_CSR_MXSTATUS           (0x7C4) /* Machine mode extended status */
#define CHIP_CSR_MPFT_CTL           (0x7C5) /* Performance throttling control register */
#define CHIP_CSR_MHSP_CTL           (0x7C6) /* Machine mode hardware stack protection control */
#define CHIP_CSR_MSP_BOUND          (0x7C7) /* Machine mode SP binding register */
#define CHIP_CSR_MSP_BASE           (0x7C8) /* Machine mode SP base register */
#define CHIP_CSR_MDCAUSE            (0x7C9) /* Machine mode detailed trap reason */
#define CHIP_CSR_MCACHE_CTL         (0x7CA) /* Cache control register */
#define CHIP_CSR_MCCTLBEGINADDR     (0x7CB) /* Machine mode CCTL starting address */
#define CHIP_CSR_MCCTLCOMMAND       (0x7CC) /* Machine mode CCTL commands */
#define CHIP_CSR_MCCTLDATA          (0x7CD) /* Machine mode CCTL data */
#define CHIP_CSR_MCOUNTERWEN        (0x7CE) /* Machine counter write enable */
#define CHIP_CSR_MCOUNTERINTEN      (0x7CF) /* Machine counter interrupt enable */
#define CHIP_CSR_MMISC_CTL          (0x7D0) /* Machine mode miscellaneous control register */
#define CHIP_CSR_MCOUNTERMASK_M     (0x7D1) /* Machine counter mask for machine mode */
#define CHIP_CSR_MCOUNTERMASK_S     (0x7D2) /* Machine counter mask for supervisor mode */
#define CHIP_CSR_MCOUNTERMASK_U     (0x7D3) /* Machine counter mask for user mode */
#define CHIP_CSR_MCOUNTEROVF        (0x7D4) /* Machine counter overflow status */
#define CHIP_CSR_MSLIDELEG          (0x7D5) /* Machine supervisor mode local interrupt delegation */
#define CHIP_CSR_MCLK_CTL           (0x7DF) /* Clock control register */
#define CHIP_CSR_DEXC2DBG           (0x7E0) /* Exception redirection register */
#define CHIP_CSR_DDCAUSE            (0x7E1) /* Debug exception detailed reason */
#define CHIP_CSR_UITB               (0x800) /* Instruction table base address register */
#define CHIP_CSR_UCODE              (0x801) /* Code register */
#define CHIP_CSR_UDCAUSE            (0x809) /* User detailed trap reason */
#define CHIP_CSR_UCCTLBEGINADDR     (0x80B) /* User CCTL starting address */
#define CHIP_CSR_UCCTLCOMMAND       (0x80C) /* User CCTL command */
#define CHIP_CSR_SLIE               (0x9C4) /* Supervisor local interrupt enable */
#define CHIP_CSR_SLIP               (0x9C5) /* Supervisor local interrupt pending */
#define CHIP_CSR_SDCAUSE            (0x9C9) /* Supervisor detailed trap reasons */
#define CHIP_CSR_SCCTLDATA          (0x9CD) /* Supervisor CCTL data */
#define CHIP_CSR_SCOUNTERINTEN      (0x9CF) /* Supervisor counter interrupt enable */
#define CHIP_CSR_SCOUNTERMASK_M     (0x9D1) /* Supervisor counter mask for machine mode */
#define CHIP_CSR_SCOUNTERMASK_S     (0x9D2) /* Supervisor counter mask for supervisor mode */
#define CHIP_CSR_SCOUNTERMASK_U     (0x9D3) /* Supervisor counter mask for user mode */
#define CHIP_CSR_SCOUNTEROVF        (0x9D4) /* Supervisor counter overflow status */
#define CHIP_CSR_SCOUNTINHIBIT      (0x9E0) /* Supervisor counter prohibition */
#define CHIP_CSR_SHPMEVENT3         (0x9E3) /* Supervisor performance monitoring event selector */
#define CHIP_CSR_SHPMEVENT4         (0x9E4) /* Supervisor performance monitoring event selector */
#define CHIP_CSR_SHPMEVENT5         (0x9E5) /* Supervisor performance monitoring event selector */
#define CHIP_CSR_SHPMEVENT6         (0x9E6) /* Supervisor performance monitoring event selector */
#define CHIP_CSR_MICM_CFG           (0xFC0)
#define CHIP_CSR_MDCM_CFG           (0xFC1)
#define CHIP_CSR_MMSC_CFG           (0xFC2)
#define CHIP_CSR_MMSC_CFG2          (0xFC3)


/* HPM specific mcase interrupt/exception codes */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_WDG           5  /* Watchdog timer interrupt  */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_M_ECC_ERR     16 /* Imprecise ECC error interrupt (M‑mode) */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_M_BUS_ERR     17 /* Bus read/write transaction error interrupt (M‑mode) */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_M_PMON_OVF    18 /* erformance monitor overflow interrupt (M‑mode) */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_S_ECC_ERR     (256 + 16) /* Imprecise ECC error interrupt (S‑mode) */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_S_BUS_ERR     (256 + 17) /* Bus read/write transaction error interrupt (S‑mode) */
#define CHIP_REGFLDVAL_CSR_MCAUSE_INTCODE_S_PMON_OVF    (256 + 18) /* erformance monitor overflow interrupt (S‑mode) */

#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_STACK_OVF     32 /* Stack overflow excaption */
#define CHIP_REGFLDVAL_CSR_MCAUSE_EXCCODE_STACK_UNF     33 /* Stack underflow excaption */





/*********** Instruction local memory base register - MILMB  (0x7C0) **********/
#define CHIP_REGFLD_CSR_MILMB_IBPA                      (0x003FFFFFUL << 10U) /* (RO) Base physical address of the DLM */
#define CHIP_REGFLD_CSR_MILMB_RWECC                     (0x00000001UL <<  3U) /* (RW) Enable diagnostic access for ECC codes */
#define CHIP_REGFLD_CSR_MILMB_ECCEN                     (0x00000003UL <<  1U) /* (RW) Parity/ECC enable control */
#define CHIP_REGFLD_CSR_MILMB_IEN                       (0x00000001UL <<  0U) /* (RO) DLM enable control*/

#define CHIP_REGFLDVAL_CSR_MILMB_ECCEN_DIS              0 /* Disable parity/ECC */
#define CHIP_REGFLDVAL_CSR_MILMB_ECCEN_UNCOR            2 /* Generate exception only for uncorrectable parity/ECC errors */
#define CHIP_REGFLDVAL_CSR_MILMB_ECCEN_ALL              3 /* Generate exception for any kind of parity/ECC error */

/*********** Data local memory base register - MDLMB  (0x7C1) *****************/
#define CHIP_REGFLD_CSR_MDLMB_DBPA                      (0x003FFFFFUL << 10U) /* (RO) Base physical address of the DLM */
#define CHIP_REGFLD_CSR_MDLMB_RWECC                     (0x00000001UL <<  3U) /* (RW) Enable diagnostic access for ECC codes */
#define CHIP_REGFLD_CSR_MDLMB_ECCEN                     (0x00000003UL <<  1U) /* (RW) Parity/ECC enable control */
#define CHIP_REGFLD_CSR_MDLMB_DEN                       (0x00000001UL <<  0U) /* (RO) DLM enable control*/

#define CHIP_REGFLDVAL_CSR_MDLMB_ECCEN_DIS              0 /* Disable parity/ECC */
#define CHIP_REGFLDVAL_CSR_MDLMB_ECCEN_UNCOR            2 /* Generate exception only for uncorrectable parity/ECC errors */
#define CHIP_REGFLDVAL_CSR_MDLMB_ECCEN_ALL              3 /* Generate exception for any kind of parity/ECC error */

/*********** ECC code register - MECC_CODE  (0x7C2) ***************************/
#define CHIP_REGFLD_CSR_MECC_CODE_INSN                  (0x00000001UL << 22U) /* (RO) Indicates whether the parity/ECC error was caused by an instruction fetch or a data access. */
#define CHIP_REGFLD_CSR_MECC_CODE_RAMID                 (0x0000000FUL << 18U) /* (RO) ID of the RAM causing the parity/ECC error */
#define CHIP_REGFLD_CSR_MECC_CODE_P                     (0x00000001UL << 17U) /* (RO) Precise error */
#define CHIP_REGFLD_CSR_MECC_CODE_C                     (0x00000001UL << 16U) /* (RO) Errors is correctable */
#define CHIP_REGFLD_CSR_MECC_CODE_CODE                  (0x0000007FUL <<  0U) /* (RW) This field records the ECC value of the ECC error exception */


#define CHIP_REGFLDVAL_CSR_MECC_CODE_INSN_DATA          0 /* Data access */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_INSN_INSTR         1 /* Instruction acquisition */

#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_ITAG         2 /* Tag RAM of I-Cache */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_IDATA        3 /* Data RAM of I-Cache */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_DTAG         4 /* Tag RAM of D-Cache */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_DDATA        5 /* Data RAM of D-Cache */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_TLB_TAG      6 /* Tag RAM of TLB */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_TLB_DATA     7 /* Data RAM of TLB */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_ILM          8 /* ILM */
#define CHIP_REGFLDVAL_CSR_MECC_CODE_RAMID_DLM          9 /* DLM */


/*********** Machine mode extended statu - MXSTATUS (0x7C4) *******************/
#define CHIP_REGFLD_CSR_MXSTATUS_PDME                   (0x00000001UL <<  5U) /* (RW) Used to save the previous DME state when entering trap. */
#define CHIP_REGFLD_CSR_MXSTATUS_DME                    (0x00000001UL <<  4U) /* (RW) Machine data error flag */
#define CHIP_REGFLD_CSR_MXSTATUS_PPFT_EN                (0x00000001UL <<  1U) /* (RW)  */
#define CHIP_REGFLD_CSR_MXSTATUS_PFT_EN                 (0x00000001UL <<  0U) /* (RW) Enable performance throttling */


/*********** Performance throttling control register - MPFT_CTL (0x7C5) *******/
#define CHIP_REGFLD_CSR_MPFT_CTL_FAST_INT               (0x00000001UL <<  8U) /* (RW) Fast interrupt response (clear mxstatus.PFT_EN on interrupts)  */
#define CHIP_REGFLD_CSR_MPFT_CTL_T_LEVEL                (0x0000000FUL <<  4U) /* (RW) Throttle level (15 - highest, 0 - lowest performance) */


/*********** Machine mode hardware stack protection control - MHSP_CTL (0x7C6) */
#define CHIP_REGFLD_CSR_MHSP_CTL_M                      (0x00000001UL <<  5U) /* (RW) Enable SP protection and logging in machine mode */
#define CHIP_REGFLD_CSR_MHSP_CTL_S                      (0x00000001UL <<  4U) /* (RW) Enable SP protection and logging in supervisor mode */
#define CHIP_REGFLD_CSR_MHSP_CTL_U                      (0x00000001UL <<  3U) /* (RW) Enable SP protection and logging in user mode */
#define CHIP_REGFLD_CSR_MHSP_CTL_SCHM                   (0x00000001UL <<  2U) /* (RW) Operation scheme of stack protection and logging mechanism */
#define CHIP_REGFLD_CSR_MHSP_CTL_UDF_EN                 (0x00000001UL <<  1U) /* (RW) Stack underflow protection mechanism enable */
#define CHIP_REGFLD_CSR_MHSP_CTL_OVF_EN                 (0x00000001UL <<  0U) /* (RW) Stack overflow protection and logging mechanism enable  */

#define CHIP_REGFLDVAL_CSR_MHSP_CTL_SCHM_OVUF           0 /* Stack overflow/underflow detection */
#define CHIP_REGFLDVAL_CSR_MHSP_CTL_SCHM_TOP            1 /* Top record on stack */



/*********** Cache control register - MCACHE_CTL (0x7CA) **********************/
#define CHIP_REGFLD_CSR_MCACHE_CTL_DC_WAROUND           (0x00000003UL << 13U) /* (RW) D-cache write‑back threshold */
#define CHIP_REGFLD_CSR_MCACHE_CTL_DC_FIRST_WORD        (0x00000001UL << 12U) /* (RO) D‑cache miss allocation fill policy */
#define CHIP_REGFLD_CSR_MCACHE_CTL_IC_FIRST_WORD        (0x00000001UL << 11U) /* (RO) I‑cache miss allocation fill policy */
#define CHIP_REGFLD_CSR_MCACHE_CTL_DPREF_EN             (0x00000001UL << 10U) /* (RW) Enable D-cache hardware prefetching on load/store memory accesses */
#define CHIP_REGFLD_CSR_MCACHE_CTL_IPREF_EN             (0x00000001UL <<  9U) /* (RW) Enable I-cache hardware prefetching on load/store memory accesses */
#define CHIP_REGFLD_CSR_MCACHE_CTL_CCTL_SUEN            (0x00000001UL <<  8U) /* (RW) Enable ucctlbeginaddr and ucctlcommand access in S/U‑mode */
#define CHIP_REGFLD_CSR_MCACHE_CTL_DC_RWECC             (0x00000001UL <<  7U) /* (RW) Enable D-cache diagnostic access for ECC codes */
#define CHIP_REGFLD_CSR_MCACHE_CTL_IC_RWECC             (0x00000001UL <<  6U) /* (RW) Enable I-cache diagnostic access for ECC codes */
#define CHIP_REGFLD_CSR_MCACHE_CTL_DC_ECCEN             (0x00000003UL <<  4U) /* (RW) Enable control for D-cache parity/ECC error checking */
#define CHIP_REGFLD_CSR_MCACHE_CTL_IC_ECCEN             (0x00000003UL <<  2U) /* (RW) Enable control for I-cache parity/ECC error checking */
#define CHIP_REGFLD_CSR_MCACHE_CTL_DC_EN                (0x00000001UL <<  1U) /* (RW) D‑Cache is enable */
#define CHIP_REGFLD_CSR_MCACHE_CTL_IC_EN                (0x00000001UL <<  0U) /* (RW) I‑Cache is enable */


#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_DC_WAROUND_DIS    0 /* Disable streaming. All cacheable write misses are assigned a cache line based on the PMA settings. */
#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_DC_WAROUND_4      1 /* Stop allocating D‑Cache entries after 4 consecutive cache lines, regardless of PMA settings. */
#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_DC_WAROUND_64     2 /* Stop allocating D‑Cache entries after 64 consecutive cache lines, regardless of PMA settings. */
#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_DC_WAROUND_128    3 /* Stop allocating D‑Cache entries after 128 consecutive cache lines, regardless of PMA settings. */

#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_FIRST_WORD_KEY    0 /* Cache line data returns the key (double) word first */
#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_FIRST_WORD_LA     1 /* Cache line data returns the lowest address (double) word first */

#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_ECCEN_DIS         0 /* Disable parity/ECC */
#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_ECCEN_UNCOR       2 /* Generate exception only for uncorrectable parity/ECC errors */
#define CHIP_REGFLDVAL_CSR_MCACHE_CTL_ECCEN_ALL         3 /* Generate exception for any kind of parity/ECC error */


/*********** Machine mode detailed trap reason - MDCAUSE (0x7C9) **************/
#define CHIP_REGFLD_CSR_MDCAUSE_PM                      (0x00000003UL << 5U) /* (RW) Privilege mode of the instruction that caused the imprecise exception  */
#define CHIP_REGFLD_CSR_MDCAUSE_MDCAUSE                 (0x00000007UL << 0U) /* (RW) Details the cause of the trap recorded in the mcause register. */

#define CHIP_REGFLDVAL_CSR_MDCAUSE_PM_U                 0 /* User mode */
#define CHIP_REGFLDVAL_CSR_MDCAUSE_PM_S                 1 /* Supervisor mode */
#define CHIP_REGFLDVAL_CSR_MDCAUSE_PM_M                 3 /* Machine mode */


/*********** Machine mode miscellaneous control register - MMISC_CTL (0x7D0) **/
#define CHIP_REGFLD_CSR_MMISC_CTL_NBLD_EN               (0x00000001UL <<  8U) /* (RW) Non-blocking memory load enable */
#define CHIP_REGFLD_CSR_MMISC_CTL_MSA_UNA               (0x00000001UL <<  6U) /* (RW) Enable misaligned memory access */
#define CHIP_REGFLD_CSR_MMISC_CTL_BRPE                  (0x00000001UL <<  3U) /* (RW) Branch predication enable */
#define CHIP_REGFLD_CSR_MMISC_CTL_RVCOMPM               (0x00000001UL <<  2U) /* (RW) RISC-V compatibility mode enable */
#define CHIP_REGFLD_CSR_MMISC_CTL_VEC_PLIC              (0x00000001UL <<  1U) /* (RW) Vector mode of PLIC */


/*********** Machine supervisor mode local interrupt delegation - MSLIDELEG (0x7D5) */
#define CHIP_REGFLD_CSR_MSLIDELEG_PMOVI                 (0x00000001UL << 18U) /* (RW) Delegate S‑mode performance monitor overflow local interrupts to S‑mode */
#define CHIP_REGFLD_CSR_MSLIDELEG_BWEI                  (0x00000001UL << 17U) /* (RW) Delegate S‑mode bus read/write transaction error local interrupt to S‑mode */
#define CHIP_REGFLD_CSR_MSLIDELEG_IMECCI                (0x00000001UL << 16U) /* (RW) Delegate S‑mode slave port ECC error local interrupt to S‑mode */


/*********** Clock control register  - MCLK_CTL (0x7DF) ***********************/
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_LS               (0x00000001UL << 24U) /* (RW) Level 2 clock gating enable: load/store unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_FPDIV            (0x00000001UL << 23U) /* (RW) Level 2 clock gating enable: floating point division unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_FPMISC           (0x00000001UL << 22U) /* (RW) Level 2 clock gating enable: floating point miscellaneous unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_FPMAC            (0x00000001UL << 21U) /* (RW) Level 2 clock gating enable: floating point multiply-accumulate unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_INTMAC           (0x00000001UL << 20U) /* (RW) Level 2 clock gating enable: integer multiplication and addition unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_INTDIV           (0x00000001UL << 19U) /* (RW) Level 2 clock gating enable: integer division unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_INTMSK           (0x00000001UL << 18U) /* (RW) Level 2 clock gating enable: integer mask unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_INTSUBS          (0x00000001UL << 17U) /* (RW) Level 2 clock gating enable: integer substitution unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_FUNIT_INTOP            (0x00000001UL << 16U) /* (RW) Level 2 clock gating enable: integer operation unit */
#define CHIP_REGFLD_CSR_MCLK_CTL_VI                     (0x00000001UL << 15U) /* (RW) Level 1 clock gating enable: vector/floating point issue queues. */
#define CHIP_REGFLD_CSR_MCLK_CTL_VR                     (0x00000001UL << 14U) /* (RW) Level 1 clock gating enable: vector/floating point register files. */
#define CHIP_REGFLD_CSR_MCLK_CTL_AQ                     (0x00000001UL << 13U) /* (RW) Level 1 clock gating enable: ACE load/store queue. */
#define CHIP_REGFLD_CSR_MCLK_CTL_DQ                     (0x00000001UL << 12U) /* (RW) Level 1 clock gating enable: data cache load/store queues. */
#define CHIP_REGFLD_CSR_MCLK_CTL_UQ                     (0x00000001UL << 11U) /* (RW) Level 1 clock gating enable: uncached queues. */
#define CHIP_REGFLD_CSR_MCLK_CTL_FP                     (0x00000001UL << 10U) /* (RW) Level 1 clock gating enable: scalar floating point issue units and queues. */
#define CHIP_REGFLD_CSR_MCLK_CTL_CLKGATE_L3_VPU         (0x00000001UL <<  2U) /* (RW) Hot clock gating level enable: Level 3 VPU level clock gating. */
#define CHIP_REGFLD_CSR_MCLK_CTL_CLKGATE_L2_UNIT        (0x00000001UL <<  1U) /* (RW) Hot clock gating level enable: Level 2 unit level clock gating. */
#define CHIP_REGFLD_CSR_MCLK_CTL_CLKGATE_L1_MODULE      (0x00000001UL <<  0U) /* (RW) Hot clock gating level enable: Level 1 module clock gating. */


/*********** Exception redirection register  - DEXC2DBG (0x7E0) ***************/
#define CHIP_REGFLD_CSR_DEXC2DBG_PMOV                   (0x00000001UL << 19U) /* (RW) Redirect the performance counter overflow interrupt into debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_SPF                    (0x00000001UL << 18U) /* (RW) Redirect storage page fault exceptions into debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_LPF                    (0x00000001UL << 17U) /* (RW) Redirect loading failure exception into debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_IPF                    (0x00000001UL << 16U) /* (RW) Redirect the instruction page fault exception into debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_BWE                    (0x00000001UL << 15U) /* (RW) Redirect the Bus-write Transaction Error local interrupt to enter debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_SLPECC                 (0x00000001UL << 14U) /* (RW) Redirect local memory slave port ECC Error local interrupt to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_ACE                    (0x00000001UL << 13U) /* (RW) Redirect ACE related exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_HSP                    (0x00000001UL << 12U) /* (RW) Redirect stack protection exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_MEC                    (0x00000001UL << 11U) /* (RW) Redirect M‑mode environment call exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_SEC                    (0x00000001UL <<  9U) /* (RW) Redirect S‑mode environment call exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_UEC                    (0x00000001UL <<  8U) /* (RW) Redirect U‑mode environment call exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_SAF                    (0x00000001UL <<  7U) /* (RW) Redirect Store Access Fault exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_SAM                    (0x00000001UL <<  6U) /* (RW) Redirect Store Access Misaligned exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_LAF                    (0x00000001UL <<  5U) /* (RW) Redirect load access failure exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_LAV                    (0x00000001UL <<  4U) /* (RW) Redirect Load Access Misaligned exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_NMI                    (0x00000001UL <<  3U) /* (RW) Redirect a non-maskable interrupt to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_II                     (0x00000001UL <<  2U) /* (RW) Redirect illegal instruction exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_IAF                    (0x00000001UL <<  1U) /* (RW) Redirect the instruction access error exceptions to debug mode */
#define CHIP_REGFLD_CSR_DEXC2DBG_IAM                    (0x00000001UL <<  0U) /* (RW) Redirect the instruction access misaligned exceptions to debug mode */


/*********** Debug exception detailed reason  - DDCAUSE (0x7E1) ***************/
#define CHIP_REGFLD_CSR_DDCAUSE_SUBTYPE                 (0x000000FFUL <<  8U) /* (RO) A subtype of the main type. */
#define CHIP_REGFLD_CSR_DDCAUSE_MAINTYPE                (0x000000FFUL <<  0U) /* (RO) Reason for redirecting to debug mode. */


/*********** Instruction table base address register  - UITB (0x800) **********/
#define CHIP_REGFLD_CSR_UITB_ADDR                       (0x3FFFFFFFUL <<  2U) /* (RW) The base address of the CoDense instruction table. */
#define CHIP_REGFLD_CSR_UITB_HW                         (0x00000001UL <<  0U) /* (RO) CoDense instruction list is hardwired */


/*********** Code register  - UCODE (0x801) ***********************************/
#define CHIP_REGFLD_CSR_UCODE_OV                        (0x00000001UL <<  0U) /* (RW) Overflow flag. Set by DSP instructions, results in saturation. */


/*********** User detailed trap reason  - UDCAUSE (0x809) *********************/
#define CHIP_REGFLD_CSR_UDCAUSE_UDCAUSE                 (0x00000007UL <<  0U) /* (RW) Details the cause of the trap recorded in the ucause register. */


/*********** Supervisor local interrupt enable  - SLIE (0x9C4) ****************/
#define CHIP_REGFLD_CSR_SLIE_PMOVI                      (0x00000001UL << 18U) /* (RW) Enables the S‑Mode Performance Monitor overflow local interrupt */
#define CHIP_REGFLD_CSR_SLIE_BWEI                       (0x00000001UL << 17U) /* (RW) Enables the S‑Mode bus read/write transaction error local interrupt. The processor may receive bus errors for load/store instructions or cache writebacks. */
#define CHIP_REGFLD_CSR_SLIE_IMECCI                     (0x00000001UL << 16U) /* (RW) Enables S‑Mode slave port ECC error local interrupt. */


/*********** Supervisor local interrupt pending  - SLIP (0x9C5) ****************/
#define CHIP_REGFLD_CSR_SLIP_PMOVI                      (0x00000001UL << 18U) /* (RW) S‑Mode Performance Monitor overflow local interrupt pending status and control*/
#define CHIP_REGFLD_CSR_SLIP_BWEI                       (0x00000001UL << 17U) /* (RW) S‑Mode bus read/write transaction error local interrupt pending status and control. The processor may receive bus errors for load/store instructions or cache writebacks. */
#define CHIP_REGFLD_CSR_SLIP_IMECCI                     (0x00000001UL << 16U) /* (RW) S‑Mode slave port ECC error local interrupt pending status and control. */


/*********** Supervisor detailed trap reason - SDCAUSE (0x9C9) ****************/
#define CHIP_REGFLD_CSR_SDCAUSE_PM                      (0x00000003UL << 5U) /* (RW) Privilege mode of the instruction that caused the imprecise exception  */
#define CHIP_REGFLD_CSR_SDCAUSE_MDCAUSE                 (0x00000007UL << 0U) /* (RW) Details the cause of the trap recorded in the scause register. */

#define CHIP_REGFLDVAL_CSR_SDCAUSE_PM_U                 0 /* User mode */
#define CHIP_REGFLDVAL_CSR_SDCAUSE_PM_S                 1 /* Supervisor mode */
#define CHIP_REGFLDVAL_CSR_SDCAUSE_PM_M                 3 /* Machine mode */







#endif // CHIP_SPEC_CSR_H
