#ifndef REGS_TWI_H
#define REGS_TWI_H

#include "chip_devtype_spec_features.h"

#define CHIP_REG_TWBR       _MMIO_BYTE(0xB8) /* TWI bit rate register */
#define CHIP_REG_TWSR       _MMIO_BYTE(0xB9) /* TWI Status Register */
#define CHIP_REG_TWAR       _MMIO_BYTE(0xBA) /* TWI Address Register */
#define CHIP_REG_TWDR       _MMIO_BYTE(0xBB) /* TWI Data Register */
#define CHIP_REG_TWCR       _MMIO_BYTE(0xBC) /* TWI Control Register */
#define CHIP_REG_TWAMR      _MMIO_BYTE(0xBD) /* TWI Address Mask Register */

/* ---------------- TWSR – TWI Status Register -------------------------------*/
#define CHIP_REGFLD_TWSR_TWS                (0x1FU << 3) /* (RO) TWI status flag */
#define CHIP_REGFLD_TWSR_TWPS               (0x03U << 0) /* (RW) TWI prescaler */

#define CHIP_REGFLDVAL_TWSR_TWPS_1          0
#define CHIP_REGFLDVAL_TWSR_TWPS_4          1
#define CHIP_REGFLDVAL_TWSR_TWPS_16         2
#define CHIP_REGFLDVAL_TWSR_TWPS_64         3

/* ---------------- TWAR – TWI address register ------------------------------*/
#define CHIP_REGFLD_TWAR_TWA                (0x7FU << 1) /* (RW) TWI slave address */
#define CHIP_REGFLD_TWAR_TWGCE              (0x01U << 0) /* (RW) TWI General Call (Broadcast) Recognition Enable */

/* ---------------- TWCR – TWI Control Register ------------------------------*/
#define CHIP_REGFLD_TWCR_TWINT              (0x01U << 7) /* (RW1C) TWI interrupt flag */
#define CHIP_REGFLD_TWCR_TWEA               (0x01U << 6) /* (RW) TWI enable acknowledge */
#define CHIP_REGFLD_TWCR_TWSTA              (0x01U << 5) /* (RW) TWI start condition control */
#define CHIP_REGFLD_TWCR_TWSTO              (0x01U << 4) /* (RW) TWI stop condition control */
#define CHIP_REGFLD_TWCR_TWWC               (0x01U << 3) /* (RW) TWI write conflict flag */
#define CHIP_REGFLD_TWCR_TWEN               (0x01U << 2) /* (RW) TWI enable */
#define CHIP_REGFLD_TWCR_TWIE               (0x01U << 0) /* (RW) TWI interrupt enable */

/* ---------------- TWAMR – TWI Address Mask Register ------------------------*/
#define CHIP_REGFLD_TWAMR_TWAM              (0x7FU << 1) /* (RW) TWI address mask control  */

#endif // REGS_TWI_H
