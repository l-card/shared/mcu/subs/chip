#ifndef CHIP_TARGET_ATMEGA8515_H
#define CHIP_TARGET_ATMEGA8515_H

#include <avr/io.h>
#ifndef __ASSEMBLER__
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/boot.h>

#include "chip_pins.h"
#endif
#include "extint.h"
#include "init/chip_xmem.h"
#ifndef __ASSEMBLER__
#include "init/chip_stack_check.h"
#endif

#endif // CHIP_TARGET_ATMEGA8515_H
