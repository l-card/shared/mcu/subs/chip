#include "chip.h"




/* достаточно 33 битов, чтобы покрыть все адресное пространство imx8mp */
#define MMU_VA_BITSIZE         33
/* изпользуем 33 битный VA, на уровне 0 идет разрешение битов 32:30, т.е. достаточно 8 записей */
#define MMU_LVL1_TT_SIZE       8
#define MMU_LVL2_TT_SIZE       CHIP_AARCH_TT_SIZE_TG_4KB
#define MMU_LVL3_TT_SIZE       CHIP_AARCH_TT_SIZE_TG_4KB

#define MMU_LVL3_BLOCK_SIZE    CHIP_AARCH_LVL3_BLOCK_SIZE_TG_4K
#define MMU_LVL2_BLOCK_SIZE    CHIP_AARCH_LVL2_BLOCK_SIZE_TG_4K
#define MMU_PAGE_SIZE          MMU_LVL3_BLOCK_SIZE

/* таблицы помещаем в отдельную секцию (не bss), чтобы MMU можно было настроить до
 * обнуления bss и это обнуление не перетерло бы таблицы
 * volatile используем, чтобы запись в таблицы не оптимизировал компилятор. Т.к. запись таблиц
 * идет до включения MMU, то вся память еще считается DEVICE и не допускает unaligned access, а
 * компилятор по умолчанию может оптимизировать несколько записей в одну инструкцию, даже если
 * объединенная запись будет невыровнена. */
#define MMU_TLB_MEM(var)   volatile var __attribute__ ((section (".mmu_tlb"))) __attribute__ ((__aligned__ (MMU_PAGE_SIZE)))

 //0x00000000_00EE4400
/* Варианты используемых аттрибутов (может быть до 8) */
#define MMU_ATTR_IDX_DEV           0
#define MMU_ATTR_VAL_DEV           CHIP_REGFLDVAL_AARCH_MAIR_ATTR_DEV(CHIP_AARCH_MEMATTR_DEV_nGnRnE) /* Device Memory, nGnRnE. Для регистров периферии */

#define MMU_ATTR_IDX_MEM_NOCACHE   1
#define MMU_ATTR_VAL_MEM_NOCACHE   CHIP_REGFLDVAL_AARCH_MAIR_ATTR_NORM(CHIP_AARCH_MEMATTR_NORM_NO_CACHE, CHIP_AARCH_MEMATTR_NORM_NO_CACHE)

#define MMU_ATTR_IDX_MEM_RO_CACHED 2
#define MMU_ATTR_VAL_MEM_RO_CACHED CHIP_REGFLDVAL_AARCH_MAIR_ATTR_NORM(CHIP_AARCH_MEMATTR_NORM_WBNT_ROAL, CHIP_AARCH_MEMATTR_NORM_WBNT_ROAL)

#define MMU_ATTR_IDX_MEM_CACHED    3
#define MMU_ATTR_VAL_MEM_CACHED    CHIP_REGFLDVAL_AARCH_MAIR_ATTR_NORM(CHIP_AARCH_MEMATTR_NORM_WBNT_RWAL, CHIP_AARCH_MEMATTR_NORM_WBNT_RWAL)



#define MMU_RGN_ATTR_IDX_PERIPH_REGS    MMU_ATTR_IDX_DEV
#define MMU_RGN_APVAL_PERIPH_REGS       CHIP_REGFLDVAL_AARCH_TT_AP_RW_PRIV
#define MMU_RGN_ATTR_IDX_BOOTROM        MMU_ATTR_IDX_MEM_RO_CACHED
#define MMU_RGN_APVAL_BOOTROM           CHIP_REGFLDVAL_AARCH_TT_AP_RO_PRIV
#define MMU_RGN_ATTR_IDX_BOOTROM_PROT   MMU_ATTR_IDX_MEM_RO_CACHED
#define MMU_RGN_APVAL_BOOTROM_PROT      CHIP_REGFLDVAL_AARCH_TT_AP_RO_PRIV
#define MMU_RGN_ATTR_IDX_OCRAM          MMU_ATTR_IDX_MEM_NOCACHE
#define MMU_RGN_APVAL_OCRAM             CHIP_REGFLDVAL_AARCH_TT_AP_RW_PRIV
#define MMU_RGN_ATTR_IDX_ITCM           MMU_ATTR_IDX_MEM_NOCACHE
#define MMU_RGN_APVAL_ITCM              CHIP_REGFLDVAL_AARCH_TT_AP_RW_PRIV
#define MMU_RGN_ATTR_IDX_DTCM           MMU_ATTR_IDX_MEM_NOCACHE
#define MMU_RGN_APVAL_DTCM              CHIP_REGFLDVAL_AARCH_TT_AP_RW_PRIV



#define MMU_DESCR_NEXT_LVL(tbl_addr) ((uint64_t)(tbl_addr) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_VALID, 1) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_NEXT_LVL, 1) \
            )


#define MMU_DESCR_BLOCK(addr, aatridx, access) ((uint64_t)(addr) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_VALID, 1) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_NEXT_LVL, 0) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_ATTRINDX, aatridx) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_NS, 0) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_AP, access) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_SH, CHIP_REGFLDVAL_AARCH_TCR_SH_OUTER) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_AF, 1) \
            )

#define MMU_DESCR_PAGE(addr, aatridx, access) (\
                MMU_DESCR_BLOCK(addr, aatridx, access) \
                | LBITFIELD_SET(CHIP_REGFLD_AARCH_TT_NEXT_LVL, 1) \
            )

#define MMU_ADDR_IN_RGN(addr, rgn_addr, rgn_size) (((addr) >= (rgn_addr)) && ((addr) < ((rgn_addr) + (rgn_size))))


MMU_TLB_MEM(uint64_t mmu_tbl_lvl1[MMU_LVL1_TT_SIZE]);
MMU_TLB_MEM(uint64_t mmu_tbl_lvl2_int[MMU_LVL2_TT_SIZE]);
MMU_TLB_MEM(uint64_t mmu_tbl_lvl3_boot[MMU_LVL3_TT_SIZE]);
MMU_TLB_MEM(uint64_t mmu_tbl_lvl3_itcm[MMU_LVL3_TT_SIZE]);
MMU_TLB_MEM(uint64_t mmu_tbl_lvl3_ocram[MMU_LVL3_TT_SIZE]);


static uint64_t f_fill_l3_table(volatile uint64_t *mmu_tbl_lvl3, uint64_t block_start) {
    for (int l3_idx = 0; l3_idx < MMU_LVL2_TT_SIZE; ++l3_idx) {
        const uint64_t page_start =  block_start + MMU_LVL3_BLOCK_SIZE * l3_idx;
        uint64_t page_tbl_descr = 0;
        if (page_start < CHIP_MEMRGN_SIZE_BOOT) {
            page_tbl_descr = MMU_DESCR_PAGE(page_start, MMU_RGN_ATTR_IDX_BOOTROM, MMU_RGN_APVAL_BOOTROM);
        } else if (MMU_ADDR_IN_RGN(page_start, CHIP_MEMRGN_ADDR_BOOT_PROT, CHIP_MEMRGN_SIZE_BOOT_PROT)) {
            page_tbl_descr = MMU_DESCR_PAGE(page_start, MMU_RGN_ATTR_IDX_BOOTROM_PROT, MMU_RGN_APVAL_BOOTROM_PROT);
        } else if (MMU_ADDR_IN_RGN(page_start, CHIP_MEMRGN_ADDR_CAAM, CHIP_MEMRGN_SIZE_CAAM)) {
            page_tbl_descr = MMU_DESCR_PAGE(page_start, MMU_RGN_ATTR_IDX_PERIPH_REGS, MMU_RGN_APVAL_PERIPH_REGS);
        } else if (MMU_ADDR_IN_RGN(page_start, CHIP_MEMRGN_ADDR_OCRAM_S, CHIP_MEMRGN_SIZE_OCRAM_S)) {
            page_tbl_descr = MMU_DESCR_PAGE(page_start, MMU_RGN_ATTR_IDX_OCRAM, MMU_RGN_APVAL_OCRAM);
        } else if (MMU_ADDR_IN_RGN(page_start, CHIP_MEMRGN_ADDR_OCRAM, CHIP_MEMRGN_SIZE_OCRAM)) {
            page_tbl_descr = MMU_DESCR_PAGE(page_start, MMU_RGN_ATTR_IDX_OCRAM, MMU_RGN_APVAL_OCRAM);
        } else if (MMU_ADDR_IN_RGN(page_start, CHIP_MEMRGN_ADDR_ITCM, CHIP_MEMRGN_SIZE_ITCM)) {
            page_tbl_descr = MMU_DESCR_PAGE(page_start, MMU_RGN_ATTR_IDX_ITCM, MMU_RGN_APVAL_ITCM);
        } else if (MMU_ADDR_IN_RGN(page_start, CHIP_MEMRGN_ADDR_DTCM, CHIP_MEMRGN_SIZE_DTCM)) {
            page_tbl_descr = MMU_DESCR_PAGE(page_start, MMU_RGN_ATTR_IDX_DTCM, MMU_RGN_APVAL_DTCM);
        }
        mmu_tbl_lvl3[l3_idx] = page_tbl_descr;
    }
    return MMU_DESCR_NEXT_LVL(mmu_tbl_lvl3);
}


void chip_init_mmu(void) {
    /* Заполнение наборов атрибутов, на которые будут ссылаться страницы по индексу */
    chip_sysreg_wr("MAIR_EL3", 0
                   | LBITFIELD_SET(CHIP_REGFLD_AARCH_MAIR_ATTR(MMU_ATTR_IDX_DEV),           MMU_ATTR_VAL_DEV)
                   | LBITFIELD_SET(CHIP_REGFLD_AARCH_MAIR_ATTR(MMU_ATTR_IDX_MEM_NOCACHE),   MMU_ATTR_VAL_MEM_NOCACHE)
                   | LBITFIELD_SET(CHIP_REGFLD_AARCH_MAIR_ATTR(MMU_ATTR_IDX_MEM_RO_CACHED), MMU_ATTR_VAL_MEM_RO_CACHED)
                   | LBITFIELD_SET(CHIP_REGFLD_AARCH_MAIR_ATTR(MMU_ATTR_IDX_MEM_CACHED),    MMU_ATTR_VAL_MEM_CACHED)
                   );

    /** @todo включить настройки cachability и Shareability для регионов RAM и в настройках TLB */
    /* Общие настройки преобразования страниц */
    chip_sysreg_wr("TCR_EL3", CHIP_REGMSK_AARCH_TCR_RES1
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_TCR_PS, CHIP_REGFLDVAL_AARCH_TCR_PS_36) /* Размер Output Address (PA) - 36 бит (ближайшее большее 33) */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_TCR_T0SZ, 64-33) /* Размер Input Address (VA) - 33 бита */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_TCR_TG0, CHIP_REGFLDVAL_AARCH_TCR_TG_4KB) /* Размер страницы (granule) - 4K */
                                  /* Атрибуты должны совпадать с атрибутами соответствующего участка памяти */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_TCR_IRGN0, CHIP_REGFLDVAL_AARCH_TCR_RGN_NO /*CHIP_REGFLDVAL_AARCH_TCR_RGN_WB_RWAL*/) /* Inner cacheability */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_TCR_ORGN0, CHIP_REGFLDVAL_AARCH_TCR_RGN_NO /*CHIP_REGFLDVAL_AARCH_TCR_RGN_WB_RWAL*/) /* Outer cacheability */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_TCR_SH0, CHIP_REGFLDVAL_AARCH_TCR_SH_INNER)  /* Shareability */
                   );

    /* Заполнение таблицы первого уровня по 1GB */
    for (int l1_idx = 0; l1_idx < MMU_LVL1_TT_SIZE; ++l1_idx) {
        uint64_t l1_tbl_descr = 0;
        if (l1_idx == 0) {
            /* первый блок относится к внутренней памяти */
            l1_tbl_descr = MMU_DESCR_NEXT_LVL(&mmu_tbl_lvl2_int[0]);
        }
        /** @todo добавить заполнение блоков для DDR по настройкам размера DDR */
        mmu_tbl_lvl1[l1_idx] = l1_tbl_descr;
    }
    for (int l2_idx = 0; l2_idx < MMU_LVL2_TT_SIZE; ++l2_idx) {
        const uint64_t block_start = MMU_LVL2_BLOCK_SIZE * l2_idx;
        uint64_t l2_tbl_descr = 0;
        if (MMU_ADDR_IN_RGN(CHIP_MEMRGN_ADDR_BOOT, block_start, MMU_LVL2_BLOCK_SIZE)) {
            l2_tbl_descr = f_fill_l3_table(mmu_tbl_lvl3_boot, block_start);
        } else if (MMU_ADDR_IN_RGN(CHIP_MEMRGN_ADDR_ITCM, block_start, MMU_LVL2_BLOCK_SIZE)) {
            l2_tbl_descr = f_fill_l3_table(mmu_tbl_lvl3_itcm, block_start);
        } else if (MMU_ADDR_IN_RGN(CHIP_MEMRGN_ADDR_OCRAM, block_start, MMU_LVL2_BLOCK_SIZE)) {
            l2_tbl_descr = f_fill_l3_table(mmu_tbl_lvl3_ocram, block_start);
        } else {
            /** @todo сейчас всю память кроме отдельных случаев заполняем как Device.
             *  Сделать выдаление оставшихся регионов периферии, а также QSPI/PCIE/VPU,
             *  и заполнить явно, а для недействительных сделать нулевой tbl_descr */
            l2_tbl_descr = MMU_DESCR_BLOCK(block_start, MMU_RGN_ATTR_IDX_PERIPH_REGS, MMU_RGN_APVAL_PERIPH_REGS);
        }
        mmu_tbl_lvl2_int[l2_idx] = l2_tbl_descr;
    }

    chip_sysreg_wr("TTBR0_EL3", &mmu_tbl_lvl1[0]);

    __asm__ volatile ("DSB ISHST" ::: "memory"); // ensure write has completed
    __asm__ volatile ("TLBI ALLE3" ::: "memory"); // invalidate the TLB entries
    __asm__ volatile ("DSB ISH" ::: "memory");  // ensure TLB invalidation is complete
    __asm__ volatile ("ISB" ::: "memory"); // synchronize context on this processor




    /* Включение MMU */
    chip_sysreg_update("SCTLR_EL3", CHIP_REGFLD_AARCH_SCTLR_M, CHIP_REGFLD_AARCH_SCTLR_M);
    chip_isb();
}
