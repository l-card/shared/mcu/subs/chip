CHIP_TARGET_ARCHTYPE := cortexm
CHIP_TARGET_CORE ?= cm7

CHIP_DEFS = CHIP_STM32H7XX
CHIP_GEN_SRC += $(CHIP_TARGET_DIR)/init/chip_clk.c \
                $(CHIP_TARGET_DIR)/init/chip_gpv.c \
                $(CHIP_TARGET_DIR)/init/chip_pwr.c \
                $(CHIP_TARGET_DIR)/init/chip_flash.c \
                $(CHIP_TARGET_DIR)/init/chip_pinint_isr.c \
                $(CHIP_TARGET_DIR)/init/chip_per_ctl.c \
                $(CHIP_TARGET_DIR)/init/chip_exmc.c \
                $(CHIP_TARGET_DIR)/init/chip_id.c \
                $(CHIP_TARGET_DIR)/init/chip_mdma.c \
                $(CHIP_SHARED_DIR)/stm32/stm32_iwdg.c

CHIP_STARTUP_SRC += $(CHIP_TARGET_DIR)/chip_isr_table.c


CHIP_TARGET_DEVTYPE_DIR := $(CHIP_TARGET_DIR)/devs/$(CHIP_TARGET_DEVTYPE)
CHIP_TARGET_DEV_DIR := $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEV)

ifeq (,$(CHIP_TARGET_DEVTYPE))
    $(error CHIP_TARGET_DEV variable must be set to target cpu device)
else ifeq (,$(wildcard $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEVTYPE).mk))
    $(error $(CHIP_TARGET_DEVTYPE) is not supported device type)
else
    include $(CHIP_TARGET_DEVTYPE_DIR)/$(CHIP_TARGET_DEVTYPE).mk
endif
CHIP_INC_DIRS += $(CHIP_TARGET_DEVTYPE_DIR) $(CHIP_TARGET_DEV_DIR) $(CHIP_SHARED_DIR)/stm32


ifeq ($(CHIP_TARGET_CORE), cm7)
    CHIP_TARGET_CORETYPE := cm7
else ifeq ($(CHIP_TARGET_CORE), cm4)
    ifeq ($(CHIP_DEV_SUPPORT_CORE_CM4), 1)
        CHIP_TARGET_CORETYPE := cm4
    else
        $(error chip err: cortex-m4 core is not specified for selected device)
    endif
else
    $(error chip err: invalid core is specified)
endif

LPRINTF_TARGET := stm32_uart_v2

