#ifndef REGS_TIM_H
#define REGS_TIM_H

#define CHIP_SUPPORT_TIM_UIF_REMAP              0
#define CHIP_SUPPORT_TIM_OUT_5_6                0
#define CHIP_SUPPORT_TIM_EXT_SYNC_MODE          0
#define CHIP_SUPPORT_TIM_EXT_BREAK              0
#define CHIP_SUPPORT_TIM_EXT_OUT_MODE           0
#define CHIP_SUPPORT_TIM_CH_GROUP               0
#define CHIP_SUPPORT_TIM_ALTFUNC                0

#include "chip_devtype_spec_features.h"
#include "stm32_tim_gen_regs.h"

#define CHIP_REGS_TIM1              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM1)
#define CHIP_REGS_TIM2              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM2)
#define CHIP_REGS_TIM3              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM3)
#define CHIP_REGS_TIM4              ((CHIP_REGS_TIM_T *) CHIP_MEMRGN_ADDR_PERIPH_TIM4)

/* специфичные биты для WCH */
/*********** TIMx control register 1 (TIMx_CR1)  ******************************/
#define CHIP_REGFLD_TIM_CR1_TMR_CAP_LVL_EN          (0x00000001UL << 15U)     /* (RW) Capture level indication enable in duouble edge capture mode */
#define CHIP_REGFLD_TIM_CR1_TMR_CAP_OV_EN           (0x00000001UL << 14U)     /* (RW) Capture value mode configuration (1 - 0xFFFF when counter overflows before capture */
#define CHIP_REGFLD_TIM_CR1_BKSEL                   (0x00000001UL << 12U)     /* (RW) Brake select (TIM1 only) */

#define CHIP_REGFLD_TIM_CR1_BKSEL_IO_OPA            /* Brake comes from IO or OPA */
#define CHIP_REGFLD_TIM_CR1_BKSEL_CMP               /* Brake signal comes from the comparator */

/*********** TIMx capture/compare register n (TIMx_CCRb) **********************/
#define CHIP_REGFLD_TIM_CCR_LEVEL                   (0x00000001UL << 16U)  /* The level indicator bit corresponding to the capture value */

#endif // REGS_TIM_H
