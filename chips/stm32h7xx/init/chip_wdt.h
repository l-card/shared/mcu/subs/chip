#ifndef CHIP_WDT_H
#define CHIP_WDT_H

#include "chip_devtype_spec_features.h"

#if CHIP_DEV_SUPPORT_CORE_CM4
    #define CHIP_WDT_SUPPORT_REMCORE        1
#ifdef CHIP_CORETYPE_CM4
    #define CHIP_WDT_REGS                   CHIP_REGS_IWDG2
    #define CHIP_WDT_CLK_FREQ               CHIP_CLK_IWDG2_FREQ
    #define CHIP_WDT_OPTBIT_SW_MODE         CHIP_REGFLD_FLASH_OPTSR_IWDG2_SW

    #define CHIP_WDT_REMCORE_REGS           CHIP_REGS_IWDG1
    #define CHIP_WDT_REMCORE_CLK_FREQ       CHIP_CLK_IWDG1_FREQ
    #define CHIP_WDT_OPTBIT_REMCORE_SW_MODE CHIP_REGFLD_FLASH_OPTSR_IWDG1_SW
#else
    #define CHIP_WDT_REGS                   CHIP_REGS_IWDG1
    #define CHIP_WDT_CLK_FREQ               CHIP_CLK_IWDG1_FREQ
    #define CHIP_WDT_OPTBIT_SW_MODE         CHIP_REGFLD_FLASH_OPTSR_IWDG1_SW
    #define CHIP_WDT_REMCORE_REGS           CHIP_REGS_IWDG2
    #define CHIP_WDT_REMCORE_CLK_FREQ       CHIP_CLK_IWDG2_FREQ
    #define CHIP_WDT_OPTBIT_REMCORE_SW_MODE CHIP_REGFLD_FLASH_OPTSR_IWDG2_SW
#endif
#else
    #define CHIP_WDT_REGS                   CHIP_REGS_IWDG1
    #define CHIP_WDT_CLK_FREQ               CHIP_CLK_IWDG1_FREQ
    #define CHIP_WDT_OPTBIT_SW_MODE         CHIP_REGFLD_FLASH_OPTSR_IWDG1_SW
#endif

#define CHIP_WDT_OPTBIT_ON_STOP_EN          CHIP_REGFLD_FLASH_OPTSR_FZ_IWDG_STOP
#define CHIP_WDT_OPTBIT_ON_STANDBY_EN       CHIP_REGFLD_FLASH_OPTSR_FZ_IWDG_SDBY


#define CHIP_WDT_GET_OPTBITS()              (CHIP_REGS_FLASH->OPTSR_CUR)
#define CHIP_WDT_SET_OPTBITS(val)           (CHIP_REGS_FLASH->OPTSR_PRG = val)


#include "stm32_iwdg.h"

#endif // CHIP_WDT_H
