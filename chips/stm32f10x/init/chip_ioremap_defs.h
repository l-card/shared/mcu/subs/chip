#ifndef CHIP_IOREMAP_DEFS_H
#define CHIP_IOREMAP_DEFS_H

#include "chip_devtype_spec_features.h"

#define CHIP_IOREMAP_JTAG_FULL              0
#define CHIP_IOREMAP_JTAG_NO_NJTRST         1
#define CHIP_IOREMAP_JTAG_SWD               2
#define CHIP_IOREMAP_JTAG_DIS               4


#define CHIP_IOREMAP_USART1_A9_10_A8_A11_12 0 /* TX - PA9,  RX - PA10, CK = PA8,  CTS - PA11, RTS - PA12 */
#define CHIP_IOREMAP_USART1_B6_7_A8_A11_12  1 /* TX - PB6,  RX - PB7,  CK = PA8,  CTS - PA11, RTS - PA12 */
#define CHIP_IOREMAP_USART1_A5_A8_10_B15    2 /* TX - PB15, RX - PA8,  CK = PA10, CTS - PA5,  RTS - PA9  */
#define CHIP_IOREMAP_USART1_A5_7_C4_5       3 /* TX - PA6,  RX - PA7,  CK = PA5,  CTS - PC4,  RTS - PC5  */

#define CHIP_IOREMAP_USART2_A2_4_A0_1       0 /* TX - PA2,  RX - PA3,  CK - PA4,  CTS - PA0,  RTS - PA1 */
#define CHIP_IOREMAP_USART2_D5_7_D3_4       1 /* TX - PD5,  RX - PD6,  CK - PD7,  CTS - PD3,  RTS - PD4 */

#define CHIP_IOREMAP_USART3_B10_14          0 /* TX - PB10, RX - PB11, CK - PB12, CTS - PB13, RTS - PB14 */
#define CHIP_IOREMAP_USART3_C10_12_B13_14   1 /* TX - PC10, RX - PC11, CK - PC12, CTS - PB13, RTS - PB14 */
#ifdef CHIP_DEV_CH32V3XX
#define CHIP_IOREMAP_USART3_A13_14_D10_12   2 /* TX - PA13,  RX - PA14,  CK - PD10, CTS - PD11, RTS - PD12 */
#endif
#define CHIP_IOREMAP_USART3_D8_12           3 /* TX - PD8,   RX - PD9,   CK - PD10, CTS - PD11, RTS - PD12 */

#define CHIP_IOREMAP_SPI1_A4_7              0 /* NSS - PA4,  SCK - PA5, MISO - PA6,  MOSI - PA7 */
#define CHIP_IOREMAP_SPI1_A15_B3_5          1 /* NSS - PA15, SCK - PB3, MISO - PB4,  MOSI - PB5 */

#define CHIP_IOREMAP_I2C1_B6_7              0 /* SCL - PB6,  SDA - PB7 */
#define CHIP_IOREMAP_I2C1_B8_9              1 /* SCL - PB8,  SDA - PB9 */

#define CHIP_IOREMAP_TIM1_A12_A8_11_B12_15      0 /* ETR - PA12, CH1 - PA8,  CH2 - PA9,  CH3 - PA10, CH4 - PA11, BKIN - PB12, CH1N - PB13, CH2N - PB14, CH3N - PB15 */
#define CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1   1 /* ETR - PA12, CH1 - PA8,  CH2 - PA9,  CH3 - PA10, CH4 - PA11, BKIN - PA6,  CH1N - PA7,  CH2N - PB0,  CH3N - PB1  */
#define CHIP_IOREMAP_TIM1_E7_15                 3 /* ETR - PE7,  CH1 - PE9,  CH2 - PE11, CH3 - PE13, CH4 - PE14, BKIN - PE15, CH1N - PE8,  CH2N - PE10, CH3N - PE12 */

#define CHIP_IOREMAP_TIM2_A0_3              0 /* CH1/ETR - PA0,  CH2 - PA1,  CH3 - PA2,  CH4 - PA3  */
#define CHIP_IOREMAP_TIM2_A15_B3_A2_3       1 /* CH1/ETR - PA15, CH2 - PB3,  CH3 - PA2,  CH4 - PA3  */
#define CHIP_IOREMAP_TIM2_A0_1_B10_11       2 /* CH1/ETR - PA0,  CH2 - PA1,  CH3 - PB10, CH4 - PB11 */
#define CHIP_IOREMAP_TIM2_A15_B3_B10_11     3 /* CH1/ETR - PA15, CH2 - PB3,  CH3 - PB10, CH4 - PB11  */

#define CHIP_IOREMAP_TIM3_A6_7_B0_1         0 /* CH1 - PA6,  CH2 - PA7,  CH3 - PB0,  CH4 - PB1  */
#define CHIP_IOREMAP_TIM3_B4_5_B0_1         2 /* CH1 - PB4,  CH2 - PB5,  CH3 - PB0,  CH4 - PB1  */
#define CHIP_IOREMAP_TIM3_C6_9              3 /* CH1 - PC6,  CH2 - PC7,  CH3 - PC8,  CH4 - PC9  */

#define CHIP_IOREMAP_TIM4_B6_9              0 /* CH1 - PB6,  CH2 - PB7,  CH3 - PB8,  CH4 - PB9  */
#define CHIP_IOREMAP_TIM4_D12_15            1 /* CH1 - PD12, CH2 - PD13, CH3 - PD14, CH4 - PD15 */

#define CHIP_IOREMAP_SPI3_B3_5_A15          0 /* SCK - PB3,  MISO - PB4,  MOSI - PB5,   NSS - PA15 */
#define CHIP_IOREMAP_SPI3_C10_12_A4         1 /* SCK - PC10, MISO - PC11, MOSI - PC12,  NSS - PA4 */

#define CHIP_IOREMAP_CAN1_A11_12            0 /* RX - PA11, TX - PA12 */
#define CHIP_IOREMAP_CAN1_B8_9              2 /* RX - PB8,  TX - PB9  */
#define CHIP_IOREMAP_CAN1_D0_1              3 /* RX - PD0,  TX - PD1  */

#define CHIP_IOREMAP_CAN2_B12_13            0 /* RX - PB12, TX - PB13 */
#define CHIP_IOREMAP_CAN2_B5_6              1 /* RX - PB5,  TX - PB6  */

#define CHIP_IOREMAP_ETH_A7_C4_5_B0_1       0 /* RX_DV/CRS - PA7,  RXD0 - PC4,  RXD1 - PC5,  RXD2 - PB0,  RXD3 - PB1  */
#define CHIP_IOREMAP_ETH_D8_12              1 /* RX_DV/CRS - PD8,  RXD0 - PD9,  RXD1 - PD10, RXD2 - PD11, RXD3 - PD12 */


#define CHIP_IOREMAP_TIM2_ITR1_PPS          0 /* Internally connect TIM2_ITR1 to Ethernet PTP output */
#define CHIP_IOREMAP_TIM2_ITR1_USB_OTG_SOF  1 /* Internally connect TIM2_ITR1 to SOF output of full-speed USB OTG */

#define CHIP_IOREMAP_ETH_PPS_DIS            0 /* PPS disabled */
#define CHIP_IOREMAP_ETH_PPS_PB5            1 /* PPS - PB5 */

/** @todo PD0, dis EXMC_NADV, PPS, remap TIM9-14 */
#if defined CHIP_DEV_CH32XXXX
#define CHIP_IOREMAP_USART8_PC4_5           0 /* RX - PC5,  TX - PC4  */
#define CHIP_IOREMAP_USART8_PA14_15         1 /* RX - PA15, TX - PA14 */
#define CHIP_IOREMAP_USART8_PE14_15         2 /* RX - PE15, TX - PE14 */

#define CHIP_IOREMAP_USART7_PC2_3           0 /* RX - PC3,  TX - PC2  */
#define CHIP_IOREMAP_USART7_PA6_7           1 /* RX - PA7,  TX - PA6  */
#define CHIP_IOREMAP_USART7_PE12_13         2 /* RX - PE13, TX - PE12 */

#define CHIP_IOREMAP_USART6_PC0_1           0 /* RX - PC1,  TX - PC0  */
#define CHIP_IOREMAP_USART6_PB8_9           1 /* RX - PB9,  TX - PB8  */
#define CHIP_IOREMAP_USART6_PE10_11         2 /* RX - PE11, TX - PE10 */

#define CHIP_IOREMAP_UART5_PC12_D2          0 /* RX - PD2,  TX - PC12  */
#define CHIP_IOREMAP_UART5_PB4_5            1 /* RX - PB5,  TX - PB4  */
#define CHIP_IOREMAP_UART5_PE8_9            2 /* RX - PE9,  TX - PE8  */

#define CHIP_IOREMAP_UART4_PC10_11          0 /* RX - PC11,  TX - PC10  */
#define CHIP_IOREMAP_UART4_PB0_1            1 /* RX - PB1,   TX - PB0  */
#define CHIP_IOREMAP_UART4_PE0_1            2 /* RX - PE1,   TX - PE0  */

#define CHIP_IOREMAP_EXMC_NADV_PB7          0 /* PB7 */
#define CHIP_IOREMAP_EXMC_NADV_DIS          1 /* disabled */

#define CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12   0 /* ETR - PC10, CH1 - PB8,  CH2 - PB9,  CH3 - PC3, CH4 - PC11, BKIN - PC12, CH1N - PA12, CH2N - PA13, CH3N - PA14 */
#define CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15        1 /* ETR - PB11, CH1 - PB3,  CH2 - PB4,  CH3 - PB5, CH4 - PC15, BKIN - PB10, CH1N - PA5,  CH2N - PA6,  CH3N - PA7  */
#define CHIP_IOREMAP_TIM10_PD0_5_7_E3_5             2 /* ETR - PD0,  CH1 - PD1,  CH2 - PD2,  CH3 - PD5, CH4 - PD7,  BKIN - PE2,  CH1N - PE3,  CH2N - PE4,  CH3N - PE5  */

#define CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5   0 /* ETR - PA2,  CH1 - PA2,  CH2 - PA3,  CH3 - PA4,  CH4 - PC4,  BKIN - PC5,  CH1N - PC0,  CH2N - PC1,  CH3N - PC2  */
#define CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14    1 /* ETR - PA2,  CH1 - PA2,  CH2 - PA3,  CH3 - PA4,  CH4 - PC14, BKIN - PA1,  CH1N - PB0,  CH2N - PB1,  CH3N - PB2  */
#define CHIP_IOREMAP_TIM9_PD8_15            2 /* ETR - PD9,  CH1 - PD9,  CH2 - PD11, CH3 - PD13, CH4 - PD15, BKIN - PD14, CH1N - PD8,  CH2N - PD10, CH3N - PD12 */



#endif

#endif // CHIP_IOREMAP_DEFS_H
