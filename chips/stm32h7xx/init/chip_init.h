#ifndef CHIP_INIT_H_
#define CHIP_INIT_H_

#include "init/chip_cortexm_init.h"
#include "chip_config.h"

#ifdef CHIP_CFG_FLASH_FUNC_EN
    #define CHIP_FLASH_FUNC_EN  CHIP_CFG_FLASH_FUNC_EN
#else
    #define CHIP_FLASH_FUNC_EN  0
#endif





#ifdef CHIP_CFG_MDMA_FUNC_EN
    #define CHIP_MDMA_FUNC_EN  CHIP_CFG_MDMA_FUNC_EN
#else
    #define CHIP_MDMA_FUNC_EN  0
#endif

#ifdef CHIP_CFG_SHARED_NO_INIT
    #define CHIP_SHARED_NO_INIT CHIP_CFG_SHARED_NO_INIT
#else
    #define CHIP_SHARED_NO_INIT 0
#endif

void chip_init(void);
void chip_main_prestart(void);



#endif /* CHIP_INIT_H_ */
