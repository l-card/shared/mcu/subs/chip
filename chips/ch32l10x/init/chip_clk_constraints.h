#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#include "chip_std_defs.h"


/* граничные условия для внешней частоты HSE */
#define CHIP_CLK_HSE_FREQ_MIN               CHIP_MHZ(3)
#define CHIP_CLK_HSE_FREQ_MAX               CHIP_MHZ(25)

#define CHIP_CLK_PLL1_IN_FREQ_MIN           CHIP_MHZ(3)
#define CHIP_CLK_PLL1_IN_FREQ_MAX           CHIP_MHZ(25)

#define CHIP_CLK_PLL1_OUT_FREQ_MIN          CHIP_MHZ(18)
#define CHIP_CLK_PLL1_OUT_FREQ_MAX          CHIP_MHZ(96)


#define CHIP_CLK_CPU_FREQ_MAX               CHIP_MHZ(96)
#define CHIP_CLK_AHB_FREQ_MAX               CHIP_MHZ(96)
#define CHIP_CLK_APB1_FREQ_MAX              CHIP_MHZ(96)
#define CHIP_CLK_APB2_FREQ_MAX              CHIP_MHZ(96)


#define CHIP_CLK_ADC_SRC_MAX                CHIP_MHZ(48)

#define CHIP_CLK_FLASH_LATENCY0_MAX         CHIP_MHZ(40)
#define CHIP_CLK_FLASH_LATENCY1_MAX         CHIP_MHZ(72)



#endif // CHIP_CLK_CONSTRAINTS_H
