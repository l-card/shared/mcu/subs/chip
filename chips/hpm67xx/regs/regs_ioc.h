#ifndef REGS_IOC_H
#define REGS_IOC_H

#include "chip_ioreg_defs.h"

typedef struct {
    struct {
        __IO uint32_t FUNC_CTL;                /* 0x0: Peripheral multiplexer function selection */
        __IO uint32_t PAD_CTL;                 /* 0x4: PAD performance configuration */
    } PAD[492];
} CHIP_REGS_IOC_T;

#define CHIP_REGS_IOC             ((CHIP_REGS_IOC_T *)CHIP_MEMRGN_ADDR_IOC)
#define CHIP_REGS_PIOC            ((CHIP_REGS_IOC_T *)CHIP_MEMRGN_ADDR_PIOC)
#define CHIP_REGS_BIOC            ((CHIP_REGS_IOC_T *)CHIP_MEMRGN_ADDR_BIOC)

/*********** FUNC_CTL - Peripheral multiplexer function selection *************/
#define CHIP_REGFLD_IOC_FUNC_CTL_LOOP_BACK              (0x00000001UL << 16U) /* (RW) Output loopback */
#define CHIP_REGFLD_IOC_FUNC_CTL_ANALOG                 (0x00000001UL <<  8U) /* (RW) Analog function */
#define CHIP_REGFLD_IOC_FUNC_CTL_ALT_SELECT             (0x0000001FUL <<  0U) /* (RW) Peripheral multiplexing function mapping selection */
#define CHIP_REGMSK_IOC_FUNC_CTL_RESERVED               (0xFFFEFEE0UL)

/*********** PAD_CTL - PAD performance configuration **************************/
#define CHIP_REGFLD_IOC_PAD_CTL_MS                      (0x00000001UL << 14U) /* (RW) Pin supply voltage */
#define CHIP_REGFLD_IOC_PAD_CTL_OD                      (0x00000001UL << 13U) /* (RW) Open drain output enable */
#define CHIP_REGFLD_IOC_PAD_CTL_SMT                     (0x00000001UL << 12U) /* (RW) Input Schmitt trigger enable */
#define CHIP_REGFLD_IOC_PAD_CTL_PS                      (0x00000001UL << 11U) /* (RW) Internal pull-up or pull-down resistor selection */
#define CHIP_REGFLD_IOC_PAD_CTL_PE                      (0x00000001UL <<  4U) /* (RW) Internal pull-up/down function enable */
#define CHIP_REGFLD_IOC_PAD_CTL_DS                      (0x00000007UL <<  0U) /* (RW) Drive strength selection */

#define CHIP_REGFLDVAL_IOC_PAD_CTL_PS_PD                0 /* Pull-down */
#define CHIP_REGFLDVAL_IOC_PAD_CTL_PS_PU                1 /* Pull-up */


#endif // REGS_IOC_H
