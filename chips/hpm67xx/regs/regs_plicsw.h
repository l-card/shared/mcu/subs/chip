#ifndef REGS_PLICSW_H
#define REGS_PLICSW_H

#include "chip_ioreg_defs.h"

typedef struct {
    uint32_t RESERVED1[1024];
    __IO uint32_t PENDING;              /* Software interrupt trigger register,                 Address offset: 0x1000 */
    uint32_t RESERVED2[1023];
    __IO uint32_t INTEN;                /* Software interrupt enable,                           Address offset: 0x2080 */
    uint32_t RESERVED3[2088963];
    __IO uint32_t CLAIM;                /* Software interrupt response and completion,          Address offset: 0x200004 */
} CHIP_REGS_PLICSW_T;

#define CHIP_REGS_PLICSW          ((CHIP_REGS_PLICSW_T *)CHIP_MEMRGN_ADDR_PLICSW)


/*********** Software interrupt trigger register - PLIC_PENDING ***************/
#define CHIP_REGFLD_PLICSW_PENDING_INT                  (0x00000001UL <<  1U) /* (RW) Writing a 1 to this register bit triggers a software interrupt */



#endif // REGS_PLICSW_H
