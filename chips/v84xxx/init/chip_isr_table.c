#include "init/chip_cortexm_isr.h"
#include "chip_dev_spec_features.h"
#include <stddef.h>

CHIP_ISR_DUMMY_DECLARE()


/* Standard Cortex-M interrupts */
CHIP_ISR_DECLARE(NMI_ExHandler);
CHIP_ISR_DECLARE(HardFault_ExHandler);
CHIP_ISR_DECLARE(MMUFault_ExHandler);
CHIP_ISR_DECLARE(BusFault_ExHandler);
CHIP_ISR_DECLARE(UsageFault_ExHandler);
CHIP_ISR_DECLARE(SVCall_ExHandler);
CHIP_ISR_DECLARE(Debug_ExHandler);
CHIP_ISR_DECLARE(PendSV_ExHandler);
CHIP_ISR_DECLARE(SysTick_IRQHandler);
/* Chip specific interrupts */
CHIP_ISR_DECLARE(WWDG_IRQHandler);                   /* Window WatchDog                 */
CHIP_ISR_DECLARE(PVD_IRQHandler);                    /* PVD/AVD through EXTI Line detection */
CHIP_ISR_DECLARE(TAMP_IRQHandler);                   /* Tamper and TimeStamps through the EXTI line */
CHIP_ISR_DECLARE(RTC_IRQHandler);                    /* RTC Wakeup through the EXTI line */
CHIP_ISR_DECLARE(FLASH_IRQHandler);                  /* FLASH                           */
CHIP_ISR_DECLARE(RCC_IRQHandler);                    /* RCC                             */
CHIP_ISR_DECLARE(EXTI0_IRQHandler);                  /* EXTI Line0                      */
CHIP_ISR_DECLARE(EXTI1_IRQHandler);                  /* EXTI Line1                      */
CHIP_ISR_DECLARE(EXTI2_IRQHandler);                  /* EXTI Line2                      */
CHIP_ISR_DECLARE(EXTI3_IRQHandler);                  /* EXTI Line3                      */
CHIP_ISR_DECLARE(EXTI4_IRQHandler);                  /* EXTI Line4                      */
CHIP_ISR_DECLARE(DMA1_Channel1_IRQHandler);          /* DMA1 Channel 1                   */
CHIP_ISR_DECLARE(DMA1_Channel2_IRQHandler);          /* DMA1 Channel 2                   */
CHIP_ISR_DECLARE(DMA1_Channel3_IRQHandler);          /* DMA1 Channel 3                   */
CHIP_ISR_DECLARE(DMA1_Channel4_IRQHandler);          /* DMA1 Channel 4                   */
CHIP_ISR_DECLARE(DMA1_Channel5_IRQHandler);          /* DMA1 Channel 5                   */
CHIP_ISR_DECLARE(DMA1_Channel6_IRQHandler);          /* DMA1 Channel 6                   */
CHIP_ISR_DECLARE(DMA1_Channel7_IRQHandler);          /* DMA1 Channel 7                   */
CHIP_ISR_DECLARE(ADC_IRQHandler);                    /* ADC                              */
CHIP_ISR_DECLARE(USB_HP_CAN1_TX_IRQHandler);         /* USB High Priority or CAN1 TX Interrupts   */
CHIP_ISR_DECLARE(USB_LP_CAN1_RX0_IRQHandler);        /* USB Low Priority or CAN1 RX0 Interrupts   */
CHIP_ISR_DECLARE(CAN_RX1_IRQHandler);                /* CAN RX1 Interrupt    */
CHIP_ISR_DECLARE(CAN_SCE_IRQHandler);                /* CAN SCE Interrupt */
CHIP_ISR_DECLARE(EXTI9_5_IRQHandler);                /* External Line[9:5] Interrupts */
CHIP_ISR_DECLARE(TMR1_BRK_TMR9_IRQHandler);          /* TMR1 break interrupt and TMR9 global interrupt */
CHIP_ISR_DECLARE(TMR1_OV_TMR10_IRQHandler);          /* TMR1 update interrupt and TMR10 global interrupt */
CHIP_ISR_DECLARE(TMR1_TRG_COM_TMR11_IRQHandler);     /* TMR1 trigger and communication interrupt and TMR11 global interrupt */
CHIP_ISR_DECLARE(TMR1_CC_IRQHandler);                /* TMR1 Capture Compare Interrupt */
CHIP_ISR_DECLARE(TMR2_IRQHandler);                   /* TMR2 global Interrupt */
CHIP_ISR_DECLARE(TMR3_IRQHandler);                   /* TMR3 global Interrupt */
CHIP_ISR_DECLARE(TMR4_IRQHandler);                   /* TMR3 global Interrupt */
CHIP_ISR_DECLARE(I2C1_EV_IRQHandler);                /* I2C1 Event Interrupt */
CHIP_ISR_DECLARE(I2C1_ER_IRQHandler);                /* I2C1 Error Interrupt */
CHIP_ISR_DECLARE(I2C2_EV_IRQHandler);                /* I2C2 Event Interrupt */
CHIP_ISR_DECLARE(I2C2_ER_IRQHandler);                /* I2C2 Error Interrupt */
CHIP_ISR_DECLARE(SPI1_IRQHandler);                   /* SPI1 global Interrupt */
CHIP_ISR_DECLARE(SPI2_I2S2EXT_IRQHandler);           /* SPI2 and I2S2EXT global Interrupt */
CHIP_ISR_DECLARE(USART1_IRQHandler);                 /* USART1 global Interrupt */
CHIP_ISR_DECLARE(USART2_IRQHandler);                 /* USART2 global Interrupt */
CHIP_ISR_DECLARE(USART3_IRQHandler);                 /* USART3 global Interrupt */
CHIP_ISR_DECLARE(EXTI15_10_IRQHandler);              /* External Line[15:10] Interrupts */
CHIP_ISR_DECLARE(RTC_Alarm_IRQHandler);              /* RTC Alarm through EXTI Line Interrupt */
CHIP_ISR_DECLARE(USBWakeUp_IRQHandler);              /* USB Device WakeUp from suspend through EXTI Line Interrupt */
CHIP_ISR_DECLARE(TMR8_BRK_TMR12_IRQHandler);         /* TMR8 Break Interrupt and TMR12 global interrupt */
CHIP_ISR_DECLARE(TMR8_OV_TMR13_IRQHandler);          /* TMR8 Update Interrupt and TMR13 global interrupt */
CHIP_ISR_DECLARE(TMR8_TRG_COM_TMR14_IRQHandler);     /* TMR8 Trigger and Commutation Interrupt and TMR14 global interrupt */
CHIP_ISR_DECLARE(TMR8_CC_IRQHandler);                /* TMR8 Capture Compare global interrupt */
CHIP_ISR_DECLARE(ADC3_IRQHandler);                   /* ADC3 global interrupt */
CHIP_ISR_DECLARE(XMC_IRQHandler);                    /* XMC global Interrupt */
CHIP_ISR_DECLARE(TMR5_IRQHandler);                   /* TMR5 global Interrupt */
CHIP_ISR_DECLARE(SPI3_I2S3EXT_IRQHandler);           /* SPI3 and I2S3EXT global Interrupt */
CHIP_ISR_DECLARE(UART4_IRQHandler);                  /* UART4 global Interrupt */
CHIP_ISR_DECLARE(UART5_IRQHandler);                  /* UART5 global Interrupt */
CHIP_ISR_DECLARE(TMR6_IRQHandler);                   /* TMR6 global Interrupt */
CHIP_ISR_DECLARE(TMR7_IRQHandler);                   /* TMR7 global Interrupt */
CHIP_ISR_DECLARE(DMA2_Channel1_IRQHandler);          /* DMA2 Channel 1 global Interrupt */
CHIP_ISR_DECLARE(DMA2_Channel2_IRQHandler);          /* DMA2 Channel 2 global Interrupt */
CHIP_ISR_DECLARE(DMA2_Channel3_IRQHandler);          /* DMA2 Channel 3 global Interrupt */
CHIP_ISR_DECLARE(DMA2_Channel4_5_IRQHandler);        /* DMA2 Channel 4 and 5 global Interrupt */
CHIP_ISR_DECLARE(I2C3_EV_IRQHandler);                /* I2C3 Event Interrupt */
CHIP_ISR_DECLARE(I2C3_ER_IRQHandler);                /* I2C3 Error Interrupt */
CHIP_ISR_DECLARE(SPI4_IRQHandler);                   /* SPI4 global Interrupt */
CHIP_ISR_DECLARE(USB_HP_IRQHandler);                 /* USB High Priority interrupt */
CHIP_ISR_DECLARE(USB_LP_IRQHandler);                 /* USB Low Priority interrupt */
CHIP_ISR_DECLARE(DMA2_Channel6_7_IRQHandler);        /* DMA2 Channel 6 and 7 global Interrupt */
CHIP_ISR_DECLARE(USART6_IRQHandler);                 /* USART6 global Interrupt */
CHIP_ISR_DECLARE(UART7_IRQHandler);                  /* UART7 global Interrupt */
CHIP_ISR_DECLARE(UART8_IRQHandler);                  /* UART8 global Interrupt */


CHIP_ISR_TABLE() = {
    CHIP_ISR_TABLE_ENTRY_STACKPTR,
    CHIP_ISR_TABLE_ENTRY_RESETPTR,
    NMI_ExHandler,
    HardFault_ExHandler,
    MMUFault_ExHandler,
    BusFault_ExHandler,
    UsageFault_ExHandler,
    NULL,
    NULL,
    NULL,
    NULL,
    SVCall_ExHandler,
    Debug_ExHandler,
    NULL,
    PendSV_ExHandler,
    SysTick_IRQHandler,

    /* External Interrupts */
    WWDG_IRQHandler,                   /* Window WatchDog              */
    PVD_IRQHandler,                    /* PVD/AVD through EXTI Line detection */
    TAMP_IRQHandler,                   /* Tamper and TimeStamps through the EXTI line */
    RTC_IRQHandler,                    /* RTC Wakeup through the EXTI line */
    FLASH_IRQHandler,                  /* FLASH                        */
    RCC_IRQHandler,                    /* RCC                          */
    EXTI0_IRQHandler,                  /* EXTI Line0                   */
    EXTI1_IRQHandler,                  /* EXTI Line1                   */
    EXTI2_IRQHandler,                  /* EXTI Line2                   */
    EXTI3_IRQHandler,                  /* EXTI Line3                   */
    EXTI4_IRQHandler,                  /* EXTI Line4                   */
    DMA1_Channel1_IRQHandler,           /* DMA1 Channel 0                */
    DMA1_Channel2_IRQHandler,           /* DMA1 Channel 1                */
    DMA1_Channel3_IRQHandler,           /* DMA1 Channel 2                */
    DMA1_Channel4_IRQHandler,           /* DMA1 Channel 3                */
    DMA1_Channel5_IRQHandler,           /* DMA1 Channel 4                */
    DMA1_Channel6_IRQHandler,           /* DMA1 Channel 5                */
    DMA1_Channel7_IRQHandler,           /* DMA1 Channel 6                */
    ADC_IRQHandler,                     /* ADC                           */
    USB_HP_CAN1_TX_IRQHandler,          /* USB High Priority or CAN1 TX Interrupts   */
    USB_LP_CAN1_RX0_IRQHandler,         /* USB Low Priority or CAN1 RX0 Interrupts   */
    CAN_RX1_IRQHandler,                 /* CAN RX1 Interrupt */
    CAN_SCE_IRQHandler,                 /* CAN SCE Interrupt */
    EXTI9_5_IRQHandler,                 /* External Line[9:5] Interrupts */
    TMR1_BRK_TMR9_IRQHandler,           /* TMR1 break interrupt and TMR9 global interrupt */
    TMR1_OV_TMR10_IRQHandler,           /* TMR1 update interrupt and TMR10 global interrupt */
    TMR1_TRG_COM_TMR11_IRQHandler,      /* TMR1 trigger and communication interrupt and TMR11 global interrupt */
    TMR1_CC_IRQHandler,                 /* TMR1 Capture Compare Interrupt */
    TMR2_IRQHandler,                    /* TMR2 global Interrupt */
    TMR3_IRQHandler,                    /* TMR3 global Interrupt */
    TMR4_IRQHandler,                    /* TMR4 global Interrupt */
    I2C1_EV_IRQHandler,                 /* I2C1 Event Interrupt */
    I2C1_ER_IRQHandler,                 /* I2C1 Error Interrupt */
    I2C2_EV_IRQHandler,                 /* I2C2 Event Interrupt */
    I2C2_ER_IRQHandler,                 /* I2C2 Error Interrupt */
    SPI1_IRQHandler,                    /* SPI1 global Interrupt */
    SPI2_I2S2EXT_IRQHandler,            /* SPI2 and I2S2EXT global Interrupt */
    USART1_IRQHandler,                  /* USART1 global Interrupt */
    USART2_IRQHandler,                  /* USART2 global Interrupt */
    USART3_IRQHandler,                  /* USART3 global Interrupt */
    EXTI15_10_IRQHandler,               /* External Line[15:10] Interrupts */
    RTC_Alarm_IRQHandler,               /* RTC Alarm through EXTI Line Interrupt */
    USBWakeUp_IRQHandler,               /* USB Device WakeUp from suspend through EXTI Line Interrupt */
    TMR8_BRK_TMR12_IRQHandler,          /* TMR8 Break Interrupt and TMR12 global interrupt */
    TMR8_OV_TMR13_IRQHandler,           /* TMR8 Update Interrupt and TMR13 global interrupt */
    TMR8_TRG_COM_TMR14_IRQHandler,      /* TMR8 Trigger and Commutation Interrupt and TMR14 global interrupt */
    TMR8_CC_IRQHandler,                 /* TMR8 Capture Compare global interrupt */
    ADC3_IRQHandler,                    /* ADC3 global interrupt */
    XMC_IRQHandler,                     /* XMC global Interrupt */
    NULL,                               /* 49 - Reserved */
    TMR5_IRQHandler,                    /* TMR5 global Interrupt */
    SPI3_I2S3EXT_IRQHandler,            /* SPI3 and I2S3EXT global Interrupt */
    UART4_IRQHandler,                   /* UART4 global Interrupt */
    UART5_IRQHandler,                   /* UART5 global Interrupt */
    TMR6_IRQHandler,                    /* TMR6 global Interrupt */
    TMR7_IRQHandler,                    /* TMR7 global Interrupt */
    DMA2_Channel1_IRQHandler,           /* DMA2 Channel 1 global Interrupt */
    DMA2_Channel2_IRQHandler,           /* DMA2 Channel 2 global Interrupt */
    DMA2_Channel3_IRQHandler,           /* DMA2 Channel 3 global Interrupt */
    DMA2_Channel4_5_IRQHandler,         /* DMA2 Channel 4 and 5 global Interrupt */
    NULL,                               /* 60 ?? */
    I2C3_EV_IRQHandler,                 /* I2C3 Event Interrupt */
    I2C3_ER_IRQHandler,                 /* I2C3 Error Interrupt */
    SPI4_IRQHandler,                    /* SPI4 global Interrupt */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    USB_HP_IRQHandler,                  /* USB High Priority interrupt */
    USB_LP_IRQHandler,                  /* USB Low Priority interrupt */
    DMA2_Channel6_7_IRQHandler,         /* DMA2 Channel 6 and 7 global Interrupt */
    USART6_IRQHandler,                  /* USART6 global Interrupt */
    UART7_IRQHandler,                   /* UART7 global Interrupt */
    UART8_IRQHandler,                   /* UART8 global Interrupt */
};


