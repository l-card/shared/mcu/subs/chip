#ifndef CHIP_CFG_DEFS_H
#define CHIP_CFG_DEFS_H

#include "chip_config.h"
#include "chip_devtype_spec_features.h"

#ifdef CHIP_CFG_CLK_HSE_MODE
    #define CHIP_CLK_HSE_MODE   CHIP_CFG_CLK_HSE_MODE
#else
    #define CHIP_CLK_HSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_LSE_MODE
    #define CHIP_CLK_LSE_MODE   CHIP_CFG_CLK_LSE_MODE
#else
    #define CHIP_CLK_LSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_HSI_EN
    #define CHIP_CLK_HSI_EN     CHIP_CFG_CLK_HSI_EN
#else
    #define CHIP_CLK_HSI_EN     1
#endif

#ifdef CHIP_CFG_CLK_CSS_SETUP_EN
    #define CHIP_CLK_CRS_SETUP_EN   CHIP_CFG_CLK_CRS_SETUP_EN
#else
    #define CHIP_CLK_CRS_SETUP_EN   0
#endif

#ifdef CHIP_CFG_CLK_PLL1_EN
    #define CHIP_CLK_PLL1_EN        CHIP_CFG_CLK_PLL1_EN
#else
    #define CHIP_CLK_PLL1_EN        0
#endif


#ifdef CHIP_CFG_CLK_PLL1_EN
    #ifdef CHIP_CFG_CLK_HSE_PLLIN_DIV
        #define CHIP_CLK_HSE_PLLIN_DIV CHIP_CFG_CLK_HSE_PLLIN_DIV
    #else
        #define CHIP_CLK_HSE_PLLIN_DIV 1
    #endif


    #ifndef CHIP_CFG_CLK_PLL1_MUL
        #error "PLL1 multiplier is not specified"
    #else
        #define CHIP_CLK_PLL1_MUL   CHIP_CFG_CLK_PLL1_MUL
    #endif

    #ifdef CHIP_CFG_CLK_PLL1_SRC
        #define CHIP_CLK_PLL1_SRC   CHIP_CFG_CLK_PLL1_SRC
    #else
        #define CHIP_CLK_PLL1_SRC   CHIP_CLK_HSI
    #endif
#else
    #define CHIP_CLK_HSE_PLLIN_DIV 0
    #define CHIP_CLK_PLL1_MUL    0
#endif

#ifdef CHIP_CFG_CLK_SYS_SRC
    #define CHIP_CLK_SYS_SRC CHIP_CFG_CLK_SYS_SRC
#else
    #define CHIP_CLK_SYS_SRC CHIP_CLK_HSI
#endif




#ifdef CHIP_CFG_CLK_AHB_DIV
    #define CHIP_CLK_AHB_DIV        CHIP_CFG_CLK_AHB_DIV
#else
    #define CHIP_CLK_AHB_DIV        1
#endif

#ifdef CHIP_CFG_CLK_APB1_DIV
    #define CHIP_CLK_APB1_DIV       CHIP_CFG_CLK_APB1_DIV
#else
    #define CHIP_CLK_APB1_DIV       1
#endif

#ifdef CHIP_CFG_CLK_APB2_DIV
    #define CHIP_CLK_APB2_DIV       CHIP_CFG_CLK_APB2_DIV
#else
    #define CHIP_CLK_APB2_DIV       1
#endif

#ifdef CHIP_CFG_CLK_ADC_DIV
    #define CHIP_CLK_ADC_DIV        CHIP_CFG_CLK_ADC_DIV
#else
    #define CHIP_CLK_ADC_DIV        8
#endif


#ifdef CHIP_CFG_WDT_SETUP_EN
    #define CHIP_WDT_SETUP_EN CHIP_CFG_WDT_SETUP_EN
#else
    #define CHIP_WDT_SETUP_EN 0
#endif
#if CHIP_WDT_SETUP_EN
    #define CHIP_CLK_LSI_EN 1 /* LSI должен быть включен при использовании IWDT */
#elif defined CHIP_CFG_CLK_LSI_EN
    #define CHIP_CLK_LSI_EN CHIP_CFG_CLK_LSI_EN
#else
    #define CHIP_CLK_LSI_EN 0
#endif


#ifdef CHIP_CFG_CODE_EFFICIENCY
    #define CHIP_CODE_EFFICIENCY    CHIP_CFG_CODE_EFFICIENCY
#else
    #define CHIP_CODE_EFFICIENCY    0
#endif

#ifdef CHIP_CFG_PWR_PVD_EN
    #define CHIP_PWR_PVD_EN         CHIP_CFG_PWR_PVD_EN
#else
    #define CHIP_PWR_PVD_EN         0
#endif
#if CHIP_PWR_PVD_EN
    #if defined CHIP_CFG_PWR_PVD_LVL
        #define CHIP_PWR_PVD_LVL    CHIP_CFG_PWR_PVD_LVL
    #else
        #error Power PVD is enabled but PDV level config parameter is not defined
    #endif
#endif

#ifdef CHIP_CFG_LTIMER_EN
    #define CHIP_LTIMER_EN   CHIP_CFG_LTIMER_EN
#else
    #define CHIP_LTIMER_EN   1
#endif


#ifdef CHIP_CFG_CLK_DMA1_EN
    #define CHIP_CLK_DMA1_EN    CHIP_CFG_CLK_DMA1_EN
#else
    #define CHIP_CLK_DMA1_EN    0
#endif
#ifdef CHIP_CFG_CLK_DMA2_EN
    #define CHIP_CLK_DMA2_EN    CHIP_CFG_CLK_DMA2_EN
#else
    #define CHIP_CLK_DMA2_EN    0
#endif
#ifdef CHIP_CFG_CLK_SRAM_EN
    #define CHIP_CLK_SRAM_EN    CHIP_CFG_CLK_SRAM_EN
#else
    #define CHIP_CLK_SRAM_EN    1
#endif
#ifdef CHIP_CFG_CLK_FLASH_EN
    #define CHIP_CLK_FLASH_EN   CHIP_CFG_CLK_FLASH_EN
#else
    #define CHIP_CLK_FLASH_EN   1
#endif
#ifdef CHIP_CFG_CLK_CRC_EN
    #define CHIP_CLK_CRC_EN     CHIP_CFG_CLK_CRC_EN
#else
    #define CHIP_CLK_CRC_EN     0
#endif
#if CHIP_DEV_SUPPORT_EXMC && defined CHIP_CFG_CLK_EXMC_EN
    #define CHIP_CLK_EXMC_EN    CHIP_CFG_CLK_EXMC_EN
#else
    #define CHIP_CLK_EXMC_EN    0
#endif
#ifdef CHIP_CFG_CLK_AFIO_EN
    #define CHIP_CLK_AFIO_EN    CHIP_CFG_CLK_AFIO_EN
#else
    #define CHIP_CLK_AFIO_EN    1
#endif
#ifdef CHIP_CFG_CLK_GPIOA_EN
    #define CHIP_CLK_GPIOA_EN   CHIP_CFG_CLK_GPIOA_EN
#else
    #define CHIP_CLK_GPIOA_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOB_EN
    #define CHIP_CLK_GPIOB_EN   CHIP_CFG_CLK_GPIOB_EN
#else
    #define CHIP_CLK_GPIOB_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOC_EN
    #define CHIP_CLK_GPIOC_EN   CHIP_CFG_CLK_GPIOC_EN
#else
    #define CHIP_CLK_GPIOC_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOD_EN
    #define CHIP_CLK_GPIOD_EN   CHIP_CFG_CLK_GPIOD_EN
#else
    #define CHIP_CLK_GPIOD_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOE_EN
    #define CHIP_CLK_GPIOE_EN   CHIP_CFG_CLK_GPIOE_EN
#else
    #define CHIP_CLK_GPIOE_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOF_EN
    #define CHIP_CLK_GPIOF_EN   CHIP_CFG_CLK_GPIOF_EN
#else
    #define CHIP_CLK_GPIOF_EN   0
#endif
#ifdef CHIP_CFG_CLK_GPIOG_EN
    #define CHIP_CLK_GPIOG_EN   CHIP_CFG_CLK_GPIOG_EN
#else
    #define CHIP_CLK_GPIOG_EN   0
#endif
#ifdef CHIP_CFG_CLK_PWR_EN
    #define CHIP_CLK_PWR_EN     CHIP_CFG_CLK_PWR_EN
#else
    #define CHIP_CLK_PWR_EN     0
#endif
#ifdef CHIP_CFG_CLK_BKP_EN
    #define CHIP_CLK_BKP_EN     CHIP_CFG_CLK_BKP_EN
#else
    #define CHIP_CLK_BKP_EN     0
#endif
#ifdef CHIP_CFG_CLK_WWDG_EN
    #define CHIP_CLK_WWDG_EN    CHIP_CFG_CLK_WWDG_EN
#else
    #define CHIP_CLK_WWDG_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM1_EN
    #define CHIP_CLK_TIM1_EN    CHIP_CFG_CLK_TIM1_EN
#else
    #define CHIP_CLK_TIM1_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM2_EN
    #define CHIP_CLK_TIM2_EN    CHIP_CFG_CLK_TIM2_EN
#else
    #define CHIP_CLK_TIM2_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM3_EN
    #define CHIP_CLK_TIM3_EN    CHIP_CFG_CLK_TIM3_EN
#else
    #define CHIP_CLK_TIM3_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM4_EN
    #define CHIP_CLK_TIM4_EN    CHIP_CFG_CLK_TIM4_EN
#else
    #define CHIP_CLK_TIM4_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM5_EN
    #define CHIP_CLK_TIM5_EN    CHIP_CFG_CLK_TIM5_EN
#else
    #define CHIP_CLK_TIM5_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM6_EN
    #define CHIP_CLK_TIM6_EN    CHIP_CFG_CLK_TIM6_EN
#else
    #define CHIP_CLK_TIM6_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM7_EN
    #define CHIP_CLK_TIM7_EN    CHIP_CFG_CLK_TIM7_EN
#else
    #define CHIP_CLK_TIM7_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM8_EN
    #define CHIP_CLK_TIM8_EN    CHIP_CFG_CLK_TIM8_EN
#else
    #define CHIP_CLK_TIM8_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM9_EN
    #define CHIP_CLK_TIM9_EN    CHIP_CFG_CLK_TIM9_EN
#else
    #define CHIP_CLK_TIM9_EN    0
#endif
#ifdef CHIP_CFG_CLK_TIM10_EN
    #define CHIP_CLK_TIM10_EN   CHIP_CFG_CLK_TIM10_EN
#else
    #define CHIP_CLK_TIM10_EN   0
#endif
#ifdef CHIP_CFG_CLK_TIM11_EN
    #define CHIP_CLK_TIM11_EN   CHIP_CFG_CLK_TIM11_EN
#else
    #define CHIP_CLK_TIM11_EN   0
#endif
#if CHIP_DEV_SUPPORT_TIM12 && defined CHIP_CFG_CLK_TIM12_EN
    #define CHIP_CLK_TIM12_EN   CHIP_CFG_CLK_TIM12_EN
#else
    #define CHIP_CLK_TIM12_EN   0
#endif
#if CHIP_DEV_SUPPORT_TIM13 && defined CHIP_CFG_CLK_TIM13_EN
    #define CHIP_CLK_TIM13_EN   CHIP_CFG_CLK_TIM13_EN
#else
    #define CHIP_CLK_TIM13_EN   0
#endif
#if CHIP_DEV_SUPPORT_TIM14 && defined CHIP_CFG_CLK_TIM14_EN
    #define CHIP_CLK_TIM14_EN   CHIP_CFG_CLK_TIM14_EN
#else
    #define CHIP_CLK_TIM14_EN   0
#endif
#ifdef CHIP_CFG_CLK_USART1_EN
    #define CHIP_CLK_USART1_EN  CHIP_CFG_CLK_USART1_EN
#else
    #define CHIP_CLK_USART1_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART2_EN
    #define CHIP_CLK_USART2_EN  CHIP_CFG_CLK_USART2_EN
#else
    #define CHIP_CLK_USART2_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART3_EN
    #define CHIP_CLK_USART3_EN  CHIP_CFG_CLK_USART3_EN
#else
    #define CHIP_CLK_USART3_EN  0
#endif
#if (CHIP_DEV_UART_CNT >= 4) && defined CHIP_CFG_CLK_UART4_EN
    #define CHIP_CLK_UART4_EN   CHIP_CFG_CLK_UART4_EN
#else
    #define CHIP_CLK_UART4_EN   0
#endif
#if (CHIP_DEV_UART_CNT >= 5) && defined CHIP_CFG_CLK_UART5_EN
    #define CHIP_CLK_UART5_EN   CHIP_CFG_CLK_UART5_EN
#else
    #define CHIP_CLK_UART5_EN   0
#endif
#if (CHIP_DEV_UART_CNT >= 6) && defined CHIP_CFG_CLK_UART6_EN
    #define CHIP_CLK_UART6_EN   CHIP_CFG_CLK_UART6_EN
#else
    #define CHIP_CLK_UART6_EN   0
#endif
#if (CHIP_DEV_UART_CNT >= 7) && defined CHIP_CFG_CLK_UART7_EN
    #define CHIP_CLK_UART7_EN   CHIP_CFG_CLK_UART7_EN
#else
    #define CHIP_CLK_UART7_EN   0
#endif
#if (CHIP_DEV_UART_CNT >= 8) && defined CHIP_CFG_CLK_UART8_EN
    #define CHIP_CLK_UART8_EN   CHIP_CFG_CLK_UART8_EN
#else
    #define CHIP_CLK_UART8_EN   0
#endif
#ifdef CHIP_CFG_CLK_SPI1_EN
    #define CHIP_CLK_SPI1_EN    CHIP_CFG_CLK_SPI1_EN
#else
    #define CHIP_CLK_SPI1_EN    0
#endif
#ifdef CHIP_CFG_CLK_SPI2_EN
    #define CHIP_CLK_SPI2_EN    CHIP_CFG_CLK_SPI2_EN
#else
    #define CHIP_CLK_SPI2_EN    0
#endif
#if (CHIP_DEV_SPI_CNT >= 3) && defined CHIP_CFG_CLK_SPI3_EN
    #define CHIP_CLK_SPI3_EN    CHIP_CFG_CLK_SPI3_EN
#else
    #define CHIP_CLK_SPI3_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C1_EN
    #define CHIP_CLK_I2C1_EN    CHIP_CFG_CLK_I2C1_EN
#else
    #define CHIP_CLK_I2C1_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C2_EN
    #define CHIP_CLK_I2C2_EN    CHIP_CFG_CLK_I2C2_EN
#else
    #define CHIP_CLK_I2C2_EN    0
#endif
#ifdef CHIP_CFG_CLK_USBD_EN
    #define CHIP_CLK_USBD_EN    CHIP_CFG_CLK_USBD_EN
#else
    #define CHIP_CLK_USBD_EN    0
#endif
#ifdef CHIP_CFG_CLK_USBHD_EN
    #define CHIP_CLK_USBHD_EN   CHIP_CFG_CLK_USBHD_EN
#else
    #define CHIP_CLK_USBHD_EN   0
#endif
#if (CHIP_DEV_USBHS_CNT >= 1) && defined CHIP_CFG_CLK_USBHS_EN
    #define CHIP_CLK_USBHS_EN   CHIP_CFG_CLK_USBHS_EN
#else
    #define CHIP_CLK_USBHS_EN   0
#endif
#if (CHIP_DEV_CAN_CNT >= 1) && defined CHIP_CFG_CLK_CAN1_EN
    #define CHIP_CLK_CAN1_EN    CHIP_CFG_CLK_CAN1_EN
#else
    #define CHIP_CLK_CAN1_EN    0
#endif
#if (CHIP_DEV_CAN_CNT >= 2) && defined CHIP_CFG_CLK_CAN2_EN
    #define CHIP_CLK_CAN2_EN    CHIP_CFG_CLK_CAN2_EN
#else
    #define CHIP_CLK_CAN2_EN    0
#endif
#if (CHIP_DEV_CAN_CNT >= 2) && defined CHIP_CFG_CLK_CAN2_EN
    #define CHIP_CLK_CAN2_EN    CHIP_CFG_CLK_CAN2_EN
#else
    #define CHIP_CLK_CAN2_EN    0
#endif
#if (CHIP_DEV_DVP_CNT >= 1) && defined CHIP_CFG_CLK_DVP_EN
    #define CHIP_CLK_DVP_EN     CHIP_CFG_CLK_DVP_EN
#else
    #define CHIP_CLK_DVP_EN     0
#endif
#if (CHIP_DEV_SDIO_CNT >= 1) && defined CHIP_CFG_CLK_SDIO1_EN
    #define CHIP_CLK_SDIO1_EN   CHIP_CFG_CLK_SDIO1_EN
#else
    #define CHIP_CLK_SDIO1_EN   0
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && defined CHIP_CFG_CLK_ETH_EN
    #define CHIP_CLK_ETH_EN     CHIP_CFG_CLK_ETH_EN
#else
    #define CHIP_CLK_ETH_EN     0
#endif
#if (CHIP_DEV_BLES_CNT >= 1) && defined CHIP_CFG_CLK_BLES_EN
    #define CHIP_CLK_BLES_EN    CHIP_CFG_CLK_BLES_EN
#else
    #define CHIP_CLK_BLES_EN    0
#endif
#ifdef CHIP_CFG_CLK_ADC1_EN
    #define CHIP_CLK_ADC1_EN    CHIP_CFG_CLK_ADC1_EN
#else
    #define CHIP_CLK_ADC1_EN    0
#endif
#if (CHIP_DEV_ADC_CNT >= 2) && defined CHIP_CFG_CLK_ADC2_EN
    #define CHIP_CLK_ADC2_EN    CHIP_CFG_CLK_ADC2_EN
#else
    #define CHIP_CLK_ADC2_EN    0
#endif
#if (CHIP_DEV_ADC_CNT >= 3) && defined CHIP_CFG_CLK_ADC3_EN
    #define CHIP_CLK_ADC3_EN    CHIP_CFG_CLK_ADC3_EN
#else
    #define CHIP_CLK_ADC3_EN    0
#endif
#ifdef CHIP_CFG_CLK_DAC_EN
    #define CHIP_CLK_DAC_EN     CHIP_CFG_CLK_DAC_EN
#else
    #define CHIP_CLK_DAC_EN     0
#endif
#if CHIP_DEV_SUPPORT_RNG && defined CHIP_CFG_CLK_RNG_EN
    #define CHIP_CLK_RNG_EN     CHIP_CFG_CLK_RNG_EN
#else
    #define CHIP_CLK_RNG_EN     0
#endif

#ifdef CHIP_CFG_CLK_MCO_EN
    #define CHIP_CLK_MCO_EN     CHIP_CFG_CLK_MCO_EN
#else
    #define CHIP_CLK_MCO_EN     0
#endif

#if CHIP_CLK_MCO_EN
    #ifndef CHIP_CFG_CLK_MCO_SRC
        #error MCO source clock is not specified
    #else
        #define CHIP_CLK_MCO_SRC    CHIP_CFG_CLK_MCO_SRC
    #endif
#endif

#ifdef CHIP_CFG_IOREMAP_JTAG
    #define CHIP_IOREMAP_JTAG        CHIP_CFG_IOREMAP_JTAG
#else
    #define CHIP_IOREMAP_JTAG        CHIP_IOREMAP_JTAG_FULL
#endif


#ifdef CHIP_CFG_IOREMAP_USART1
    #define CHIP_IOREMAP_USART1     CHIP_CFG_IOREMAP_USART1
#else
    #define CHIP_IOREMAP_USART1     CHIP_IOREMAP_USART1_A9_10_A8_A11_12
#endif

#ifdef CHIP_CFG_IOREMAP_USART2
    #define CHIP_IOREMAP_USART2     CHIP_CFG_IOREMAP_USART2
#else
    #define CHIP_IOREMAP_USART2     CHIP_IOREMAP_USART2_A2_4_A0_1
#endif

#ifdef CHIP_CFG_IOREMAP_USART3
    #define CHIP_IOREMAP_USART3     CHIP_CFG_IOREMAP_USART3
#else
    #define CHIP_IOREMAP_USART3     CHIP_IOREMAP_USART3_B10_14
#endif

#ifdef CHIP_CFG_IOREMAP_I2C1
    #define CHIP_IOREMAP_I2C1       CHIP_CFG_IOREMAP_I2C1
#else
    #define CHIP_IOREMAP_I2C1       CHIP_IOREMAP_I2C1_B6_7
#endif

#ifdef CHIP_CFG_IOREMAP_SPI1
    #define CHIP_IOREMAP_SPI1       CHIP_CFG_IOREMAP_SPI1
#else
    #define CHIP_IOREMAP_SPI1       CHIP_IOREMAP_SPI1_A4_7
#endif


#ifdef CHIP_CFG_IOREMAP_TIM1
    #define CHIP_IOREMAP_TIM1       CHIP_CFG_IOREMAP_TIM1
#else
    #define CHIP_IOREMAP_TIM1       CHIP_IOREMAP_TIM1_A12_A8_11_B12_15
#endif

#ifdef CHIP_CFG_IOREMAP_TIM2
    #define CHIP_IOREMAP_TIM2       CHIP_CFG_IOREMAP_TIM2
#else
    #define CHIP_IOREMAP_TIM2       CHIP_IOREMAP_TIM2_A0_3
#endif


#ifdef CHIP_CFG_IOREMAP_TIM3
    #define CHIP_IOREMAP_TIM3       CHIP_CFG_IOREMAP_TIM3
#else
    #define CHIP_IOREMAP_TIM3       CHIP_IOREMAP_TIM3_A6_7_B0_1
#endif

#ifdef CHIP_CFG_IOREMAP_TIM4
    #define CHIP_IOREMAP_TIM4       CHIP_CFG_IOREMAP_TIM4
#else
    #define CHIP_IOREMAP_TIM4       CHIP_IOREMAP_TIM4_B6_9
#endif

#ifdef CHIP_CFG_IOREMAP_TIM10
    #define CHIP_IOREMAP_TIM10      CHIP_CFG_IOREMAP_TIM10
#endif


#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_SPI_CNT >= 3)
    #ifdef CHIP_CFG_IOREMAP_SPI3
        #define CHIP_IOREMAP_SPI3   CHIP_CFG_IOREMAP_SPI3
    #else
        #define CHIP_IOREMAP_SPI3   0
    #endif
#else
    #define CHIP_IOREMAP_SPI3       0
#endif


#ifdef CHIP_CFG_IOREMAP_CAN1
    #define CHIP_IOREMAP_CAN1       CHIP_CFG_IOREMAP_CAN1
#else
    #define CHIP_IOREMAP_CAN1       CHIP_IOREMAP_CAN1_A11_12
#endif

#ifdef CHIP_CFG_IOREMAP_CAN2
    #define CHIP_IOREMAP_CAN2       CHIP_CFG_IOREMAP_CAN2
#else
    #define CHIP_IOREMAP_CAN2       0
#endif

#ifdef CHIP_CFG_IOREMAP_ETH
    #define CHIP_IOREMAP_ETH        CHIP_CFG_IOREMAP_ETH
#else
    #define CHIP_IOREMAP_ETH        0
#endif

#ifdef CHIP_CFG_IOREMAP_TIM2_ITR1
    #define CHIP_IOREMAP_TIM2_ITR1  CHIP_CFG_IOREMAP_TIM2_ITR1
#else
    #define CHIP_IOREMAP_TIM2_ITR1  CHIP_IOREMAP_TIM2_ITR1_PPS
#endif


#ifdef CHIP_CFG_IOREMAP_ETH_PPS
    #define  CHIP_IOREMAP_ETH_PPS   CHIP_CFG_IOREMAP_ETH_PPS
#else
    #define  CHIP_IOREMAP_ETH_PPS   CHIP_IOREMAP_ETH_PPS_DIS
#endif

#if defined CHIP_DEV_CH32XXXX
#ifdef CHIP_CFG_IOREMAP_USART8
    #define CHIP_IOREMAP_USART8     CHIP_CFG_IOREMAP_USART8
#endif

#ifdef CHIP_CFG_IOREMAP_USART7
    #define CHIP_IOREMAP_USART7     CHIP_CFG_IOREMAP_USART7
#endif

#ifdef CHIP_CFG_IOREMAP_USART6
    #define CHIP_IOREMAP_USART6     CHIP_CFG_IOREMAP_USART6
#endif

#ifdef CHIP_CFG_IOREMAP_UART5
    #define CHIP_IOREMAP_UART5      CHIP_CFG_IOREMAP_UART5
#endif

#ifdef CHIP_CFG_IOREMAP_UART4
    #define CHIP_IOREMAP_UART4      CHIP_CFG_IOREMAP_UART4
#endif

#ifdef CHIP_CFG_IOREMAP_EXMC_NADV
    #define CHIP_IOREMAP_EXMC_NADV  CHIP_CFG_IOREMAP_EXMC_NADV
#endif

#endif

#endif // CHIP_CFG_DEFS_H
