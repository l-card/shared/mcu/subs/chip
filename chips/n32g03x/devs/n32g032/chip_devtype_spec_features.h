#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#define CHIP_DEV_N32G03X
#define CHIP_DEV_N32G032

#include "chip_dev_spec_features.h"

#define CHIP_DEV_TIM_GP_CNT             2
#define CHIP_DEV_TIM_GP32_CNT           0
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          1
#define CHIP_DEV_LPTIM_CNT              1
#define CHIP_DEV_SPI_CNT                ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48) ? 3 : (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32) ? 2 : 1)
#define CHIP_DEV_I2S_CNT                1
#define CHIP_DEV_I2S_FD_CNT             0
#define CHIP_DEV_I2C_CNT                2
#define CHIP_DEV_UART_CNT               ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25) ? 3 : 4)
#define CHIP_DEV_LPUART_CNT             2
#define CHIP_DEV_CAN_CNT                1
#define CHIP_DEV_USB_CNT                0
#define CHIP_DEV_ADC_CNT                1
#define CHIP_DEV_DAC_CNT                0
#define CHIP_DEV_OPA_CNT                1
#define CHIP_DEV_COMP_CNT               (((CHIP_DEV_PKG == CHIP_PKG_UFQFPN20) || (CHIP_DEV_PKG == CHIP_PKG_WLCSP25)) ? 2 : 3)
#define CHIP_DEV_BEEPER_CNT             ((CHIP_DEV_PKG == CHIP_PKG_TSSOP20) ? 1 : 2)

#define CHIP_DEV_DMA_CH_CNT             8


#define CHIP_DEV_SUPPORT_TSC            1
#define CHIP_DEV_SUPPORT_RNG            1
#define CHIP_DEV_SUPPORT_SAC            1
#define CHIP_DEV_SUPPORT_HSI_TRIM       0
#define CHIP_DEV_SUPPORT_HSE_IOSEL      1
#define CHIP_DEV_SUPPORT_PORTD          1
#define CHIP_DEV_SUPPORT_TIM4           (CHIP_DEV_TIM_GP_CNT >= 2)
#define CHIP_DEV_SUPPORT_UART5          (CHIP_DEV_UART_CNT >= 4)
#define CHIP_DEV_SUPPORT_UART6          (CHIP_DEV_UART_CNT >= 4)

#define CHIP_SUPPORT_ADC_INJECTED       1
#define CHIP_SUPPORT_ADC_EOC_RD_CLR     0 /* признак, сбрасывается ли флаг EOC при чтении данных автоматом (stm), или нужно вручную (gd) */

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
