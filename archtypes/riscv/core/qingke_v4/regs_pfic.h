#ifndef REGS_PFIC_H
#define REGS_PFIC_H

#include "chip_ioreg_defs.h"

#define CHIP_IRQNUM_NMI                     2   /* Non-maskable interrupt */
#define CHIP_IRQNUM_EXC                     3   /* Exception interrupt */
#define CHIP_IRQNUM_ECALL_M                 5   /* Machine mode callback interrupt */
#define CHIP_IRQNUM_ECALL_U                 8   /* User mode callback interrupt */
#define CHIP_IRQNUM_BREAKPOINT              9   /* Breakpoint callback interrupt */
#define CHIP_IRQNUM_SYSTICK                 12  /* System timer interrupt */
#define CHIP_IRQNUM_SWI                     14  /* Software interrupt */
#define CHIP_IRQNUM_EXTINT(x)               (16 + (x)) /* External interrupt 16-255 */


typedef struct{
    __I  uint32_t ISR[8];       /* (RO) Interrupt enable status,                    Address offset: 0x0000-0x001C */
    __I  uint32_t IPR[8];       /* (RO) Interrupt pending status,                   Address offset: 0x0020-0x003C */
    __IO uint32_t ITHRESDR;     /* (RW) Interrupt priority threshold configuration, Address offset: 0x0040 */
    __IO uint32_t RESERVED;
    __IO uint32_t CFGR;         /* (RW) Interrupt configuration,                    Address offset: 0x0048 */
    __I  uint32_t GISR;         /* (RO) Interrupt global status,                    Address offset: 0x004C */
    __IO uint8_t VTFIDR[4];     /* (RW) Number of VTF x,                            Address offset: 0x0050-0x0053 */
    uint8_t RESERVED0[12];
    __IO uint32_t VTFADDR[4];   /* (RW) VTF x offset address,                       Address offset: 0x0060-0x006C */
    uint8_t RESERVED1[0x90];
    __O  uint32_t IENR[8];      /* (WO) Interrupt enable set,                       Address offset: 0x0100-0x011C */
    uint8_t RESERVED2[0x60];
    __O  uint32_t IRER[8];      /* (WO) Interrupt enable clear,                     Address offset: 0x0180-0x019C */
    uint8_t RESERVED3[0x60];
    __O  uint32_t IPSR[8];      /* (WO) Interrupt pending set,                      Address offset: 0x0200-0x021C */
    uint8_t RESERVED4[0x60];
    __O  uint32_t IPRR[8];      /* (WO) Interrupt pending clear,                    Address offset: 0x0280-0x029C */
    uint8_t RESERVED5[0x60];
    __IO uint32_t IACTR[8];     /* (RO) Interrupt activation status,                Address offset: 0x0300-0x031C */
    uint8_t RESERVED6[0xE0];
    __IO uint8_t IPRIOR[256];   /* (RW) Interrupt priority configuration,           Address offset: 0x0400-0x033C */
    uint8_t RESERVED7[0x810];
    __IO uint32_t SCTLR;        /* (RW) System control,                             Address offset: 0x0D10 */
} CHIP_REGS_PFIC_T;

#define CHIP_REGS_PFIC                   ((CHIP_REGS_PFIC_T *) (CHIP_MEMRGN_ADDR_CORE_INTERNAL + 0xE000))


#define CHIP_PFIC_INT_REGNUM(irq_num)  ((irq_num) >> 5)
#define CHIP_PFIC_INT_REGFLD(irq_num)  (0x00000001UL << ((irq_num) & 0x1F))


/*********** PFIC_CFGR - Interrupt Configuration Register *********************/
#define CHIP_REGFLD_PFIC_CFGR_KEYCODE                   (0x0000FFFFUL << 16U)   /* (WO) Specific key must be written to modify bits  */
#define CHIP_REGFLD_PFIC_CFGR_SYS_RESET                 (0x00000001UL <<  7U)   /* (W1) System reset (KEY3) */


#define CHIP_REGFLDVAL_PFIC_CFGR_KEYCODE_KEY1           0xFA05
#define CHIP_REGFLDVAL_PFIC_CFGR_KEYCODE_KEY2           0xBCAF
#define CHIP_REGFLDVAL_PFIC_CFGR_KEYCODE_KEY3           0xBEEF

/*********** PFIC_GISR - Interrupt Global Status Register *********************/
#define CHIP_REGFLD_PFIC_GISR_GPEND_STA                 (0x00000001UL <<  9U)   /* (RO) Whether an interrupt is currently pending */
#define CHIP_REGFLD_PFIC_GISR_GACT_STA                  (0x00000001UL <<  8U)   /* (RO) Whether an interrupt is currently being executed */
#define CHIP_REGFLD_PFIC_GISR_NEST_STA                  (0x000000FFUL <<  0U)   /* (RO) Current interrupt nesting status */

#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_8             0xFF
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_7             0x7F
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_6             0x3F
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_5             0x1F
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_4             0x0F
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_3             0x07
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_2             0x03
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_1             0x01
#define CHIP_REGFLDVAL_PFIC_GISR_NEST_STA_0             0x00


/*********** PFIC_GISR - Interrupt Global Status Register *********************/
#define CHIP_REGFLD_PFIC_VTFADDRR_EN                    (0x00000001UL <<  0U)   /* (RW) Enable VTF x channel */
#define CHIP_REGFLD_PFIC_VTFADDRR_ADDR                  (0x7FFFFFFFUL <<  1U)   /* (RW) VTF 0 address, two-byte alignmentl */


/*********** PFIC_SCTLR - System Control Register *********************/
#define CHIP_REGFLD_PFIC_SCTLR_SYS_RESET                (0x00000001UL << 31U)   /* (W1SC) System reset */
#define CHIP_REGFLD_PFIC_SCTLR_SET_EVENT                (0x00000001UL <<  5U)   /* (W1SC) Set the event to wake up the WFE case */
#define CHIP_REGFLD_PFIC_SCTLR_SEV_ON_PEND              (0x00000001UL <<  4U)   /* (RW) Disabled pending interrupts can wake up the system */
#define CHIP_REGFLD_PFIC_SCTLR_WFI_TO_WFE               (0x00000001UL <<  3U)   /* (RW) Treat the subsequent WFI instruction as a WFE instruction */
#define CHIP_REGFLD_PFIC_SCTLR_SLEEP_DEEP               (0x00000001UL <<  2U)   /* (RW) Low power mode of the control system is deepslep (1) or sleep (0) */
#define CHIP_REGFLD_PFIC_SCTLR_SLEEP_ON_EXIT            (0x00000001UL <<  1U)   /* (RW) System enters low power mode after control leaves the interrupt service program. */
#define CHIP_REGMSK_PFIC_SCTLR_RESERVED                 (0x7FFFFFC1UL)


#define CHIP_ISR(name)  __attribute__((interrupt("WCH-Interrupt-fast"))) void name (void)


#define chip_interrupts_enable()           chip_csr_set(CHIP_CSR_GINTENR, CHIP_REGFLD_CSR_GINTENR_MPIE | CHIP_REGFLD_CSR_GINTENR_MIE)
#define chip_interrupts_disable()          chip_csr_clr(CHIP_CSR_GINTENR, CHIP_REGFLD_CSR_GINTENR_MPIE | CHIP_REGFLD_CSR_GINTENR_MIE)

#define chip_interrupt_enable(irq_num)  (CHIP_REGS_PFIC->IENR[CHIP_PFIC_INT_REGNUM(irq_num)] = CHIP_PFIC_INT_REGFLD(irq_num))
#define chip_interrupt_disable(irq_num) (CHIP_REGS_PFIC->IRER[CHIP_PFIC_INT_REGNUM(irq_num)] = CHIP_PFIC_INT_REGFLD(irq_num))

#define chip_interrupt_set_priority(irq_num, prio) (CHIP_REGS_PFIC->IPRIOR[irq_num] = prio)  /* действительные биты: 7 - preemption priority, 6:5 sub priority */

#define chip_interrupt_set_vtf(irq_num, addr) do { \
    int fnd  = 0; \
    for (int i = 0; (i < CHIP_CORE_MODEL_INT_VTF_CH_CNT) && !fnd; ++i) { \
        if ((CHIP_REGS_PFIC->VTFADDR[i] & CHIP_REGFLD_PFIC_VTFADDRR_EN) == 0) { \
            fnd = 1; \
            CHIP_REGS_PFIC->VTFADDR[i] = (intptr_t)(addr) | CHIP_REGFLD_PFIC_VTFADDRR_EN; \
            CHIP_REGS_PFIC->VTFIDR[i] = irq_num; \
        } \
    } \
} while (0)


#endif // REGS_PFIC_H
