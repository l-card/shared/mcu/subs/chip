#ifndef CHIP_FLASH_H
#define CHIP_FLASH_H

#include <stdint.h>
#include "chip_init.h"

typedef uint8_t t_chip_flash_sec_num;

typedef enum {
    CHIP_FLASH_BANK1 = 0,
    CHIP_FLASH_BANK2 = 1
} t_chip_flash_bank;

/* readout protection level - уровень защиты всей flash на чтение */
typedef  enum {
    CHIP_FLASH_RDP_LEVEL0, /* нет защиты */
    CHIP_FLASH_RDP_LEVEL1, /* защита на чтение, от отладки и загрузки из RAM. понижение со стиранием */
    CHIP_FLASH_RDP_LEVEL2, /* полная защита. нет возможности отменить */
} t_chip_flash_rdp_level;

typedef enum {
    CHIP_FLASH_ERR_OK                   = 0,
    CHIP_FLASH_ERR_INVALID_ADDR         = -2000,
    CHIP_FLASH_ERR_DATA_ALIGN           = -2001,
    CHIP_FLASH_ERR_DATA_SIZE            = -2002,
    CHIP_FLASH_ERR_WAIT_OPRDY_TOUT      = -2010, /* ошибка ожидания признака завершения операции */
    CHIP_FLASH_ERR_LOCKED               = -2011, /* попытка выполнить запись/стирание без chip_flash_unlock() */
    CHIP_FLASH_ERR_STAT_WRITE_PROTECT   = -2012, /* попытка записи/стирания защищенной области */
    CHIP_FLASH_ERR_STAT_PROG_SEQ        = -2013, /* неверная последовательность команд */
    CHIP_FLASH_ERR_STAT_STROBE          = -2014, /* запись в нестертую ячейку */
    CHIP_FLASH_ERR_STAT_INCONSIST       = -2015, /* запись по непоследовательным адресам или новая запись до окончания старой */
    CHIP_FLASH_ERR_STAT_OPERATION       = -2016, /* ошибка операции из-за неверного поведения памяти */
    CHIP_FLASH_ERR_STAT_RDP             = -2017, /* ошибка чтения из защищенной для чтения области */
    CHIP_FLASH_ERR_STAT_READ_SECURE     = -2018, /* оишбка чтения из secure-области */
    CHIP_FLASH_ERR_STAT_ECC_SINGLE      = -2019, /* одиночная ошибка ECC */
    CHIP_FLASH_ERR_STAT_ECC_DOUBLE      = -2020, /* двойная ошибка ECC */
    CHIP_FLASH_ERR_OPTIONS_CHANGE       = -2021, /* ошибка смены опций */
    CHIP_FLASH_ERR_NOT_RDY              = -2022, /* память занята (идет незаконченная операция) */
} t_chip_flash_err;


/* Инициализация временных задержек flash-памяти в соответствии с настройками из
 * частот chip_config.h. Вызывается при инициализации до или после смены частоты */
void chip_flash_init_freq(void);

#if CHIP_FLASH_FUNC_EN
/* Инициализация общих параметров flash-памяти. Вызывается из кода инициализации */
void chip_flash_init(void);

/* разблокировка банка для записи/стирания.
 * Перед набором вызов chip_flash_erase/chip_flash_write необходимо
 * вызвать данную функцию, а после - chip_flash_lock() */
void chip_flash_unlock(t_chip_flash_bank bank);
void chip_flash_lock(t_chip_flash_bank bank);

/* получение адреса в едином пространстве контроллера, соответствующего
 * заданному смещению от начала заданного банка FLASH-памяти.
 * Может использоваться для чтения данных из FLASH-памяти */
const void *chip_flash_rd_ptr(t_chip_flash_bank bank, uint32_t addr);

/* Стирание заданной области falsh-памяти.
 * Стирание происходит по 128К секторам. Если адрес или размер не выравнены на границу
 * сектора, то будет стерты все сектора, в которые попадает указанная область.
 * Flash-память должна быть разблокирована с помощью chip_flash_unlock() и
 * стираемые сектора не должны быть защищены от записи */
t_chip_flash_err chip_flash_erase(t_chip_flash_bank bank, uint32_t start_addr, uint32_t size, void (*cb_func)(void));

/* Запись блока данных во flash-память.
 * Адрес start_addr должан быть выравнен на 32 байта (слово записи во Flash).
 * Size может быть не кратен 32-м байтам, однака оставшееся место до кратного
 * размера нельзя будет перезаписывать без стирания.
 * Flash-память должна быть разблокирована с помощью chip_flash_unlock(), стерта и
 * перезаписываемые сектора не должны быть защищены от записи */
t_chip_flash_err chip_flash_write(t_chip_flash_bank bank, uint32_t start_addr, const void *data, uint32_t size, void (*cb_func)(void));

/* Получение кода ошибки, соответствующего текущим статусным битам flash-памяти
 * с их сбросом. Функции записи автоматически проверяют результат их выполнения.
 * Данная функция может использоваться при чтении блока памяти, чтобу убедиться
 * в отсутствии ошибок */
t_chip_flash_err chip_flash_check_err(t_chip_flash_bank bank);

/* Установка и снятие защиты от записи во flash-памяти по указанным адресам.
 * Защита выполняется по 128К секторам. Если адрес или размер не выравнены на границу
 * сектора, то будет изменена защита записи во всех секторах, в которые попадает указанная
 * область */
t_chip_flash_err chip_flash_wr_protect(t_chip_flash_bank bank, uint32_t start_addr, uint32_t size, void (*cb_func)(void));
t_chip_flash_err chip_flash_wr_unprotect(t_chip_flash_bank bank, uint32_t start_addr, uint32_t size, void (*cb_func)(void));

/* Функции аналогичны chip_flash_wr_protect()/chip_flash_wr_unprotect(), но указывается
 * битовая маска секторов, для которых должен быть изменен признак защиты записи*/
t_chip_flash_err chip_flash_sectors_wr_protect(t_chip_flash_bank bank, uint32_t sector_mask, void (*cb_func)(void));
t_chip_flash_err chip_flash_sectors_wr_unprotect(t_chip_flash_bank bank, uint32_t sector_mask, void (*cb_func)(void));
/* Получение маски секторов, указывающих, какие сектара сейчас защищены */
uint32_t chip_flash_get_sectors_wr_protection(t_chip_flash_bank bank);

/* получить текущий уровень защиты всей памяти от постороннего чтения */
t_chip_flash_rdp_level chip_flash_opts_get_rdp_level(void);
/* установить уровень защиты всей памяти от постороннего чтения */
t_chip_flash_err chip_flash_opts_set_rdp_level(t_chip_flash_rdp_level lvl, void (*cb_func)(void));

/* ------- низкоуровневые функции ---------------------------------------------*/

/* возвращение номеров секторов, которые занимает указанная область */
t_chip_flash_err chip_flash_get_sector_nums(uint32_t start_addr, uint32_t size, t_chip_flash_sec_num *pstart_sec, t_chip_flash_sec_num *pend_sec);
/* проверка, что операции с flash завершены. если нет - возвращает CHIP_FLASH_ERR_NOT_RDY */
t_chip_flash_err chip_flash_check_rdy(t_chip_flash_bank bank);
/* ожидание завершения операций с flash-памятью */
t_chip_flash_err chip_flash_wait_rdy(t_chip_flash_bank bank, unsigned tout_ms, void (*cb_func)(void));
/* запуск стирания выбранного сектора. завершение необходимо проверить с помощью
 * chip_flash_check_rdy() и затем проверить статус с помощью chip_flash_check_err().
 * по завершению стирания всех секторов, вызвать chip_flash_erase_finish() */
t_chip_flash_err chip_flash_erase_sector_start(t_chip_flash_bank bank, uint8_t sector);
/* функция должна вызываться после вызовов chip_flash_erase_sector_start() для
 * запрета разрешения стирания */
void chip_flash_erase_finish(t_chip_flash_bank bank);


/* Функции для работы с опциями чипа во flash-памяти. Для смены опций необходимо:
 * 1. Разблокировать доступ с помощью chip_flash_options_unlock()
 * 2. Изменить значения опций в нужных *_PRG регистрах
 * 3. Записать изменения с помощью chip_flash_options_write();
 * 4. Заблокировать доступ с помощью chip_flash_options_lock() */
void chip_flash_opts_unlock(void);
void chip_flash_opts_lock(void);
t_chip_flash_err chip_flash_opts_write(void (*cb_func)(void));




#endif

#endif // CHIP_FLASH_H
