#ifndef REGS_USART_H
#define REGS_USART_H

#include "chip_devtype_spec_features.h"

#define CHIP_DEV_UART_CNT             4

#define CHIP_SUPPORT_USART_OVER8      0
#define CHIP_SUPPORT_USART_ONEBIT     0

#include "stm32_usart_v1_regs.h"

#define CHIP_REGS_USART1              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART1)
#define CHIP_REGS_USART2              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART2)
#define CHIP_REGS_USART3              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART3)
#define CHIP_REGS_USART4              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART4)
#define CHIP_USART_SUPPORT_SYNC(n)    1

#endif // REGS_USART_H
