#ifndef CHIP_STM32G0XX_REGS_SPI_H
#define CHIP_STM32G0XX_REGS_SPI_H

#include "chip_devtype_spec_features.h"

#define CHIP_SUPPORT_SPI_NSSP               1
#define CHIP_SUPPORT_SPI_FRAME_FMT          1
#define CHIP_SUPPORT_SPI_CUSTOM_DATA_SIZE   1
#define CHIP_SUPPORT_SPI_FIFO               1
#define CHIP_SUPPORT_SPI_LAST_DMA           1
#define CHIP_SUPPORT_SPI_FRAME_ERR          1
#define CHIP_SUPPORT_SPI_I2S                1
#define CHIP_SUPPORT_SPI_I2S_ASYNC_START    1
#define CHIP_SUPPORT_SPI_HS_CTL             0

#include "stm32_spi_v1_regs.h"

#define CHIP_REGS_SPI1         ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI1)
#define CHIP_REGS_SPI2         ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI2)
#if CHIP_DEV_SUPPORT_SPI3
#define CHIP_REGS_SPI3         ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI3)
#endif

#endif // CHIP_STM32G0XX_REGS_SPI_H
