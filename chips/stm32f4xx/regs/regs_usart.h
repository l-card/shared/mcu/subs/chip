#ifndef CHIP_STM32F4XX_REGS_USART_H
#define CHIP_STM32F4XX_REGS_USART_H


#include <stdint.h>
#include "chip_dev_spec_features.h"

#define CHIP_SUPPORT_USART_OVER8      1
#define CHIP_SUPPORT_USART_ONEBIT     1

#include "stm32_usart_v1_regs.h"

#define CHIP_REGS_USART1              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART1)
#define CHIP_REGS_USART2              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART2)
#define CHIP_REGS_USART3              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART3)
#define CHIP_REGS_UART4               ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART4)
#define CHIP_REGS_UART5               ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART5)
#define CHIP_REGS_USART6              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART6)

#define CHIP_USART_SUPPORT_SYNC(n)    (((n) == 1) || ((n) == 2) || ((n) == 3) || ((n) == 6))

#endif // REGS_USART_H
