#include "init/chip_cortexm_isr.h"
#include "chip_devtype_spec_features.h"
#include <stddef.h>

CHIP_ISR_DUMMY_DECLARE()


/* Standard Cortex-M interrupts */
CHIP_ISR_DECLARE(NMI_ExHandler);
CHIP_ISR_DECLARE(HardFault_ExHandler);
CHIP_ISR_DECLARE(MMUFault_ExHandler);
CHIP_ISR_DECLARE(BusFault_ExHandler);
CHIP_ISR_DECLARE(UsageFault_ExHandler);
CHIP_ISR_DECLARE(SVCall_ExHandler);
CHIP_ISR_DECLARE(Debug_ExHandler);
CHIP_ISR_DECLARE(PendSV_ExHandler);
CHIP_ISR_DECLARE(SysTick_IRQHandler);
/* Chip specific interrupts */
CHIP_ISR_DECLARE(WWDG_IRQHandler);                   /* Window WatchDog                 */
CHIP_ISR_DECLARE(PVD_AVD_IRQHandler);                /* PVD/AVD through EXTI Line detection */
CHIP_ISR_DECLARE(TAMP_STAMP_IRQHandler);             /* Tamper and TimeStamps through the EXTI line */
CHIP_ISR_DECLARE(RTC_WKUP_IRQHandler);               /* RTC Wakeup through the EXTI line */
CHIP_ISR_DECLARE(FLASH_IRQHandler);                  /* FLASH                           */
CHIP_ISR_DECLARE(RCC_IRQHandler);                    /* RCC                             */
CHIP_ISR_DECLARE(EXTI0_IRQHandler);                  /* EXTI Line0                      */
CHIP_ISR_DECLARE(EXTI1_IRQHandler);                  /* EXTI Line1                      */
CHIP_ISR_DECLARE(EXTI2_IRQHandler);                  /* EXTI Line2                      */
CHIP_ISR_DECLARE(EXTI3_IRQHandler);                  /* EXTI Line3                      */
CHIP_ISR_DECLARE(EXTI4_IRQHandler);                  /* EXTI Line4                      */
CHIP_ISR_DECLARE(DMA1_Stream0_IRQHandler);           /* DMA1 Stream 0                   */
CHIP_ISR_DECLARE(DMA1_Stream1_IRQHandler);           /* DMA1 Stream 1                   */
CHIP_ISR_DECLARE(DMA1_Stream2_IRQHandler);           /* DMA1 Stream 2                   */
CHIP_ISR_DECLARE(DMA1_Stream3_IRQHandler);           /* DMA1 Stream 3                   */
CHIP_ISR_DECLARE(DMA1_Stream4_IRQHandler);           /* DMA1 Stream 4                   */
CHIP_ISR_DECLARE(DMA1_Stream5_IRQHandler);           /* DMA1 Stream 5                   */
CHIP_ISR_DECLARE(DMA1_Stream6_IRQHandler);           /* DMA1 Stream 6                   */
CHIP_ISR_DECLARE(ADC_IRQHandler);                    /* ADC1 and ADC2                   */
CHIP_ISR_DECLARE(FDCAN1_IT0_IRQHandler);             /* FDCAN1 interrupt line 0         */
CHIP_ISR_DECLARE(FDCAN2_IT0_IRQHandler);             /* FDCAN2 interrupt line 0         */
CHIP_ISR_DECLARE(FDCAN1_IT1_IRQHandler);             /* FDCAN1 interrupt line 1         */
CHIP_ISR_DECLARE(FDCAN2_IT1_IRQHandler);             /* FDCAN2 interrupt line 1         */
CHIP_ISR_DECLARE(EXTI9_5_IRQHandler);                /* External Line[9:5]s             */
CHIP_ISR_DECLARE(TIM1_BRK_IRQHandler);               /* TIM1 Break interrupt            */
CHIP_ISR_DECLARE(TIM1_UP_IRQHandler);                /* TIM1 Update interrupt           */
CHIP_ISR_DECLARE(TIM1_TRG_COM_IRQHandler);           /* TIM1 Trigger and Commutation interrupt */
CHIP_ISR_DECLARE(TIM1_CC_IRQHandler);                /* TIM1 Capture Compare            */
CHIP_ISR_DECLARE(TIM2_IRQHandler);                   /* TIM2                            */
CHIP_ISR_DECLARE(TIM3_IRQHandler);                   /* TIM3                            */
CHIP_ISR_DECLARE(TIM4_IRQHandler);                   /* TIM4                            */
CHIP_ISR_DECLARE(I2C1_EV_IRQHandler);                /* I2C1 Event                      */
CHIP_ISR_DECLARE(I2C1_ER_IRQHandler);                /* I2C1 Error                      */
CHIP_ISR_DECLARE(I2C2_EV_IRQHandler);                /* I2C2 Event                      */
CHIP_ISR_DECLARE(I2C2_ER_IRQHandler);                /* I2C2 Error                      */
CHIP_ISR_DECLARE(SPI1_IRQHandler);                   /* SPI1                            */
CHIP_ISR_DECLARE(SPI2_IRQHandler);                   /* SPI2                            */
CHIP_ISR_DECLARE(USART1_IRQHandler);                 /* USART1                          */
CHIP_ISR_DECLARE(USART2_IRQHandler);                 /* USART2                          */
CHIP_ISR_DECLARE(USART3_IRQHandler);                 /* USART3                          */
CHIP_ISR_DECLARE(EXTI15_10_IRQHandler);              /* External Line[15:10]s           */
CHIP_ISR_DECLARE(RTC_Alarm_IRQHandler);              /* RTC Alarm (A and B) through EXTI Line */
CHIP_ISR_DECLARE(TIM8_BRK_TIM12_IRQHandler);         /* TIM8 Break and TIM12            */
CHIP_ISR_DECLARE(TIM8_UP_TIM13_IRQHandler);          /* TIM8 Update and TIM13           */
CHIP_ISR_DECLARE(TIM8_TRG_COM_TIM14_IRQHandler);     /* TIM8 Trigger and Commutation and TIM14 */
CHIP_ISR_DECLARE(TIM8_CC_IRQHandler);                /* TIM8 Capture Compare            */
CHIP_ISR_DECLARE(DMA1_Stream7_IRQHandler);           /* DMA1 Stream7                    */
CHIP_ISR_DECLARE(EXMC_IRQHandler);                   /* EXMC                             */
CHIP_ISR_DECLARE(SDMMC1_IRQHandler);                 /* SDMMC1                          */
CHIP_ISR_DECLARE(TIM5_IRQHandler);                   /* TIM5                            */
CHIP_ISR_DECLARE(SPI3_IRQHandler);                   /* SPI3                            */
CHIP_ISR_DECLARE(UART4_IRQHandler);                  /* UART4                           */
CHIP_ISR_DECLARE(UART5_IRQHandler);                  /* UART5                           */
CHIP_ISR_DECLARE(TIM6_DAC_IRQHandler);               /* TIM6 and DAC1&2 underrun errors */
CHIP_ISR_DECLARE(TIM7_IRQHandler);                   /* TIM7                            */
CHIP_ISR_DECLARE(DMA2_Stream0_IRQHandler);           /* DMA2 Stream 0                   */
CHIP_ISR_DECLARE(DMA2_Stream1_IRQHandler);           /* DMA2 Stream 1                   */
CHIP_ISR_DECLARE(DMA2_Stream2_IRQHandler);           /* DMA2 Stream 2                   */
CHIP_ISR_DECLARE(DMA2_Stream3_IRQHandler);           /* DMA2 Stream 3                   */
CHIP_ISR_DECLARE(DMA2_Stream4_IRQHandler);           /* DMA2 Stream 4                   */
CHIP_ISR_DECLARE(ETH_IRQHandler);                    /* Ethernet                        */
CHIP_ISR_DECLARE(ETH_WKUP_IRQHandler);               /* Ethernet Wakeup through EXTI line */
CHIP_ISR_DECLARE(FDCAN_CAL_IRQHandler);              /* FDCAN calibration unit interrupt*/
#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
CHIP_ISR_DECLARE(CM7_SEV_IRQHandler);                /* CM7 Send event interrupt for CM4*/
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM7
CHIP_ISR_DECLARE(CM4_SEV_IRQHandler);                /* CM4 Send event interrupt for CM7*/
#endif
CHIP_ISR_DECLARE(DMA2_Stream5_IRQHandler);           /* DMA2 Stream 5                   */
CHIP_ISR_DECLARE(DMA2_Stream6_IRQHandler);           /* DMA2 Stream 6                   */
CHIP_ISR_DECLARE(DMA2_Stream7_IRQHandler);           /* DMA2 Stream 7                   */
CHIP_ISR_DECLARE(USART6_IRQHandler);                 /* USART6                          */
CHIP_ISR_DECLARE(I2C3_EV_IRQHandler);                /* I2C3 event                      */
CHIP_ISR_DECLARE(I2C3_ER_IRQHandler);                /* I2C3 error                      */
CHIP_ISR_DECLARE(OTG_HS_EP1_OUT_IRQHandler);         /* USB OTG HS End Point 1 Out      */
CHIP_ISR_DECLARE(OTG_HS_EP1_IN_IRQHandler);          /* USB OTG HS End Point 1 In       */
CHIP_ISR_DECLARE(OTG_HS_WKUP_IRQHandler);            /* USB OTG HS Wakeup through EXTI  */
CHIP_ISR_DECLARE(OTG_HS_IRQHandler);                 /* USB OTG HS                      */
#if CHIP_DEV_SUPPORT_PSSI
CHIP_ISR_DECLARE(DVP_PSSI_IRQHandler);               /* DVP, PSSI                   */
#else
CHIP_ISR_DECLARE(DVP_IRQHandler);                    /* DVP                            */
#endif
CHIP_ISR_DECLARE(CRYPT_IRQHandler);                  /* CRYP                            */
CHIP_ISR_DECLARE(RNG_IRQHandler);                    /* Rng                             */
CHIP_ISR_DECLARE(FPU_IRQHandler);                    /* FPU                             */
CHIP_ISR_DECLARE(UART7_IRQHandler);                  /* UART7                           */
CHIP_ISR_DECLARE(UART8_IRQHandler);                  /* UART8                           */
CHIP_ISR_DECLARE(SPI4_IRQHandler);                   /* SPI4                            */
CHIP_ISR_DECLARE(SPI5_IRQHandler);                   /* SPI5                            */
CHIP_ISR_DECLARE(SPI6_IRQHandler);                   /* SPI6                            */
CHIP_ISR_DECLARE(SAI1_IRQHandler);                   /* SAI1                            */
CHIP_ISR_DECLARE(LTDC_IRQHandler);                   /* LTDC                            */
CHIP_ISR_DECLARE(LTDC_ER_IRQHandler);                /* LTDC error                      */
CHIP_ISR_DECLARE(DMA2D_IRQHandler);                  /* DMA2D                           */
#if CHIP_DEV_SUPPORT_SAI2_3
CHIP_ISR_DECLARE(SAI2_IRQHandler);                   /* SAI2                            */
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
CHIP_ISR_DECLARE(OCTOSPI1_IRQHandler)                /* OCTOSPI1                        */
#else
CHIP_ISR_DECLARE(QUADSPI_IRQHandler);                /* QUADSPI                         */
#endif
CHIP_ISR_DECLARE(LPTIM1_IRQHandler);                 /* LPTIM1                          */
CHIP_ISR_DECLARE(CEC_IRQHandler);                    /* HDMI_CEC                        */
CHIP_ISR_DECLARE(I2C4_EV_IRQHandler);                /* I2C4 Event                      */
CHIP_ISR_DECLARE(I2C4_ER_IRQHandler);                /* I2C4 Error                      */
CHIP_ISR_DECLARE(SPDIF_RX_IRQHandler);               /* SPDIF_RX                        */
#if CHIP_DEV_SUPPORT_USB2
CHIP_ISR_DECLARE(OTG_FS_EP1_OUT_IRQHandler);         /* USB OTG FS End Point 1 Out      */
CHIP_ISR_DECLARE(OTG_FS_EP1_IN_IRQHandler);          /* USB OTG FS End Point 1 In       */
CHIP_ISR_DECLARE(OTG_FS_WKUP_IRQHandler);            /* USB OTG FS Wakeup through EXTI  */
CHIP_ISR_DECLARE(OTG_FS_IRQHandler);                 /* USB OTG FS                      */
#endif
CHIP_ISR_DECLARE(DMAMUX1_OVR_IRQHandler);            /* DMAMUX1 Overrun interrupt       */
#if CHIP_DEV_SUPPORT_HRTIM
CHIP_ISR_DECLARE(HRTIM1_Master_IRQHandler);          /* HRTIM Master Timer global Interrupt */
CHIP_ISR_DECLARE(HRTIM1_TIMA_IRQHandler);            /* HRTIM Timer A global Interrupt */
CHIP_ISR_DECLARE(HRTIM1_TIMB_IRQHandler);            /* HRTIM Timer B global Interrupt */
CHIP_ISR_DECLARE(HRTIM1_TIMC_IRQHandler);            /* HRTIM Timer C global Interrupt */
CHIP_ISR_DECLARE(HRTIM1_TIMD_IRQHandler);            /* HRTIM Timer D global Interrupt */
CHIP_ISR_DECLARE(HRTIM1_TIME_IRQHandler);            /* HRTIM Timer E global Interrupt */
CHIP_ISR_DECLARE(HRTIM1_FLT_IRQHandler);             /* HRTIM Fault global Interrupt   */
#endif
CHIP_ISR_DECLARE(DFSDM1_FLT0_IRQHandler);            /* DFSDM Filter0 Interrupt        */
CHIP_ISR_DECLARE(DFSDM1_FLT1_IRQHandler);            /* DFSDM Filter1 Interrupt        */
CHIP_ISR_DECLARE(DFSDM1_FLT2_IRQHandler);            /* DFSDM Filter2 Interrupt        */
CHIP_ISR_DECLARE(DFSDM1_FLT3_IRQHandler);            /* DFSDM Filter3 Interrupt        */
#if CHIP_DEV_SUPPORT_SAI2_3
CHIP_ISR_DECLARE(SAI3_IRQHandler);                   /* SAI3 global Interrupt          */
#endif
CHIP_ISR_DECLARE(SWPMI1_IRQHandler);                 /* Serial Wire Interface 1 global interrupt */
CHIP_ISR_DECLARE(TIM15_IRQHandler);                  /* TIM15 global Interrupt          */
CHIP_ISR_DECLARE(TIM16_IRQHandler);                  /* TIM16 global Interrupt          */
CHIP_ISR_DECLARE(TIM17_IRQHandler);                  /* TIM17 global Interrupt          */
CHIP_ISR_DECLARE(MDIOS_WKUP_IRQHandler);             /* MDIOS Wakeup  Interrupt         */
CHIP_ISR_DECLARE(MDIOS_IRQHandler);                  /* MDIOS global Interrupt          */
#if CHIP_DEV_SUPPORT_JPGDEC
CHIP_ISR_DECLARE(JPEG_IRQHandler);                   /* JPEG global Interrupt           */
#endif
CHIP_ISR_DECLARE(MDMA_IRQHandler);                   /* MDMA global Interrupt           */
#if CHIP_DEV_SUPPORT_DSI
CHIP_ISR_DECLARE(DSI_IRQHandler);                    /* DSI global Interrupt            */
#endif
CHIP_ISR_DECLARE(SDMMC2_IRQHandler);                 /* SDMMC2 global Interrupt         */
#if defined CHIP_CORETYPE_CM7
CHIP_ISR_DECLARE(HSEM1_IRQHandler);                  /* HSEM1 global Interrupt          */
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
CHIP_ISR_DECLARE(HSEM2_IRQHandler);                  /* HSEM2 global Interrupt          */
#endif
CHIP_ISR_DECLARE(ADC3_IRQHandler);                   /* ADC3 global Interrupt           */
CHIP_ISR_DECLARE(DMAMUX2_OVR_IRQHandler);            /* DMAMUX Overrun interrupt        */
CHIP_ISR_DECLARE(BDMA_Channel0_IRQHandler);          /* BDMA Channel 0 global Interrupt */
CHIP_ISR_DECLARE(BDMA_Channel1_IRQHandler);          /* BDMA Channel 1 global Interrupt */
CHIP_ISR_DECLARE(BDMA_Channel2_IRQHandler);          /* BDMA Channel 2 global Interrupt */
CHIP_ISR_DECLARE(BDMA_Channel3_IRQHandler);          /* BDMA Channel 3 global Interrupt */
CHIP_ISR_DECLARE(BDMA_Channel4_IRQHandler);          /* BDMA Channel 4 global Interrupt */
CHIP_ISR_DECLARE(BDMA_Channel5_IRQHandler);          /* BDMA Channel 5 global Interrupt */
CHIP_ISR_DECLARE(BDMA_Channel6_IRQHandler);          /* BDMA Channel 6 global Interrupt */
CHIP_ISR_DECLARE(BDMA_Channel7_IRQHandler);          /* BDMA Channel 7 global Interrupt */
CHIP_ISR_DECLARE(COMP1_IRQHandler);                  /* COMP1 global Interrupt          */
CHIP_ISR_DECLARE(LPTIM2_IRQHandler);                 /* LP TIM2 global interrupt        */
CHIP_ISR_DECLARE(LPTIM3_IRQHandler);                 /* LP TIM3 global interrupt        */
CHIP_ISR_DECLARE(LPTIM4_IRQHandler);                 /* LP TIM4 global interrupt        */
CHIP_ISR_DECLARE(LPTIM5_IRQHandler);                 /* LP TIM5 global interrupt        */
CHIP_ISR_DECLARE(LPUART1_IRQHandler);                /* LP UART1 interrupt              */
#if CHIP_DEV_SUPPORT_CORE_CM4
CHIP_ISR_DECLARE(WWDG_RST_IRQHandler);               /* Window Watchdog reset interrupt (exti_d2_wwdg_it, exti_d1_wwdg_it) */
#endif
CHIP_ISR_DECLARE(CRS_IRQHandler);                    /* Clock Recovery Global Interrupt */
CHIP_ISR_DECLARE(ECC_IRQHandler);                    /* ECC diagnostic Global Interrupt */
CHIP_ISR_DECLARE(SAI4_IRQHandler);                   /* SAI4 global interrupt           */
#if CHIP_DEV_SUPPORT_DTS
CHIP_ISR_DECLARE(DTS_IRQHandler);                    /* Digital Temperature Sensor  interrupt */
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4
CHIP_ISR_DECLARE(HOLD_CORE_IRQHandler);              /* Hold core interrupt             */
#endif
CHIP_ISR_DECLARE(WAKEUP_PIN_IRQHandler);             /* Interrupt for all 6 wake-up pins */
#if CHIP_DEV_SUPPORT_OCTOSPI
CHIP_ISR_DECLARE(OCTOSPI2_IRQHandler);               /* OCTOSPI2 Interrupt              */
CHIP_ISR_DECLARE(OTFDEC1_IRQHandler);
CHIP_ISR_DECLARE(OTFDEC2_IRQHandler);
#endif
#if CHIP_DEV_SUPPORT_FMAC
CHIP_ISR_DECLARE(FMAC_IRQHandler);                   /* FMAC Interrupt                  */
#endif
#if CHIP_DEV_SUPPORT_CORDIC
CHIP_ISR_DECLARE(CORDIC_IRQHandler);                 /* CORDIC Interrupt                */
#endif
#if CHIP_DEV_SUPPORT_USART9_10
CHIP_ISR_DECLARE(UART9_IRQHandler);                  /* UART9 Interrupt                 */
CHIP_ISR_DECLARE(USART10_IRQHandler);                /* USART10 Interrupt               */
#endif
#if CHIP_DEV_SUPPORT_I2C5
CHIP_ISR_DECLARE(I2C5_EV_IRQHandler);                /* I2C5 Event Interrupt            */
CHIP_ISR_DECLARE(I2C5_ER_IRQHandler);                /* I2C5 Error Interrupt            */
#endif
#if CHIP_DEV_SUPPORT_FDCAN3
CHIP_ISR_DECLARE(FDCAN3_IT0_IRQHandler);             /* FDCAN3 interrupt line 0         */
CHIP_ISR_DECLARE(FDCAN3_IT1_IRQHandler);             /* FDCAN3 interrupt line 1         */
#endif
#if CHIP_DEV_SUPPORT_TIM23_24
CHIP_ISR_DECLARE(TIM23_IRQHandler);                  /* TIM23 global interrupt          */
CHIP_ISR_DECLARE(TIM24_IRQHandler);                  /* TIM24 global interrupt          */
#endif



CHIP_ISR_TABLE() = {
    CHIP_ISR_TABLE_ENTRY_STACKPTR,
    CHIP_ISR_TABLE_ENTRY_RESETPTR,
    NMI_ExHandler,
    HardFault_ExHandler,
    MMUFault_ExHandler,
    BusFault_ExHandler,
    UsageFault_ExHandler,
    NULL,
    NULL,
    NULL,
    NULL,
    SVCall_ExHandler,
    Debug_ExHandler,
    NULL,
    PendSV_ExHandler,
    SysTick_IRQHandler,

    /* External Interrupts */
    WWDG_IRQHandler,                   /* Window WatchDog              */
    PVD_AVD_IRQHandler,                /* PVD/AVD through EXTI Line detection */
    TAMP_STAMP_IRQHandler,             /* Tamper and TimeStamps through the EXTI line */
    RTC_WKUP_IRQHandler,               /* RTC Wakeup through the EXTI line */
    FLASH_IRQHandler,                  /* FLASH                        */
    RCC_IRQHandler,                    /* RCC                          */
    EXTI0_IRQHandler,                  /* EXTI Line0                   */
    EXTI1_IRQHandler,                  /* EXTI Line1                   */
    EXTI2_IRQHandler,                  /* EXTI Line2                   */
    EXTI3_IRQHandler,                  /* EXTI Line3                   */
    EXTI4_IRQHandler,                  /* EXTI Line4                   */
    DMA1_Stream0_IRQHandler,           /* DMA1 Stream 0                */
    DMA1_Stream1_IRQHandler,           /* DMA1 Stream 1                */
    DMA1_Stream2_IRQHandler,           /* DMA1 Stream 2                */
    DMA1_Stream3_IRQHandler,           /* DMA1 Stream 3                */
    DMA1_Stream4_IRQHandler,           /* DMA1 Stream 4                */
    DMA1_Stream5_IRQHandler,           /* DMA1 Stream 5                */
    DMA1_Stream6_IRQHandler,           /* DMA1 Stream 6                */
    ADC_IRQHandler,                    /* ADC1 and ADC2                */
    FDCAN1_IT0_IRQHandler,             /* FDCAN1 interrupt line 0      */
    FDCAN2_IT0_IRQHandler,             /* FDCAN2 interrupt line 0      */
    FDCAN1_IT1_IRQHandler,             /* FDCAN1 interrupt line 1      */
    FDCAN2_IT1_IRQHandler,             /* FDCAN2 interrupt line 1      */
    EXTI9_5_IRQHandler,                /* External Line[9:5]s          */
    TIM1_BRK_IRQHandler,               /* TIM1 Break interrupt         */
    TIM1_UP_IRQHandler,                /* TIM1 Update interrupt        */
    TIM1_TRG_COM_IRQHandler,           /* TIM1 Trigger and Commutation interrupt */
    TIM1_CC_IRQHandler,                /* TIM1 Capture Compare         */
    TIM2_IRQHandler,                   /* TIM2                         */
    TIM3_IRQHandler,                   /* TIM3                         */
    TIM4_IRQHandler,                   /* TIM4                         */
    I2C1_EV_IRQHandler,                /* I2C1 Event                   */
    I2C1_ER_IRQHandler,                /* I2C1 Error                   */
    I2C2_EV_IRQHandler,                /* I2C2 Event                   */
    I2C2_ER_IRQHandler,                /* I2C2 Error                   */
    SPI1_IRQHandler,                   /* SPI1                         */
    SPI2_IRQHandler,                   /* SPI2                         */
    USART1_IRQHandler,                 /* USART1                       */
    USART2_IRQHandler,                 /* USART2                       */
    USART3_IRQHandler,                 /* USART3                       */
    EXTI15_10_IRQHandler,              /* External Line[15:10]s        */
    RTC_Alarm_IRQHandler,              /* RTC Alarm (A and B) through EXTI Line */
    NULL,                                 /* Reserved                     */
    TIM8_BRK_TIM12_IRQHandler,         /* TIM8 Break and TIM12         */
    TIM8_UP_TIM13_IRQHandler,          /* TIM8 Update and TIM13        */
    TIM8_TRG_COM_TIM14_IRQHandler,     /* TIM8 Trigger and Commutation and TIM14 */
    TIM8_CC_IRQHandler,                /* TIM8 Capture Compare         */
    DMA1_Stream7_IRQHandler,           /* DMA1 Stream7                 */
    EXMC_IRQHandler,                    /* EXMC                          */
    SDMMC1_IRQHandler,                 /* SDMMC1                       */
    TIM5_IRQHandler,                   /* TIM5                         */
    SPI3_IRQHandler,                   /* SPI3                         */
    UART4_IRQHandler,                  /* UART4                        */
    UART5_IRQHandler,                  /* UART5                        */
    TIM6_DAC_IRQHandler,               /* TIM6 and DAC1&2 underrun errors */
    TIM7_IRQHandler,                   /* TIM7                         */
    DMA2_Stream0_IRQHandler,           /* DMA2 Stream 0                */
    DMA2_Stream1_IRQHandler,           /* DMA2 Stream 1                */
    DMA2_Stream2_IRQHandler,           /* DMA2 Stream 2                */
    DMA2_Stream3_IRQHandler,           /* DMA2 Stream 3                */
    DMA2_Stream4_IRQHandler,           /* DMA2 Stream 4                */
    ETH_IRQHandler,                    /* Ethernet                     */
    ETH_WKUP_IRQHandler,               /* Ethernet Wakeup through EXTI line */
    FDCAN_CAL_IRQHandler,              /* FDCAN calibration unit interrupt*/
#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
    CM7_SEV_IRQHandler,                /* CM7 Send event interrupt for CM4 */
#else
    NULL,
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM7
    CM4_SEV_IRQHandler,                /* CM4 Send event interrupt for CM7 */
#else
    NULL,
#endif
    NULL,                              /* Reserved                     */
    NULL,                              /* Reserved                     */
    DMA2_Stream5_IRQHandler,           /* DMA2 Stream 5                */
    DMA2_Stream6_IRQHandler,           /* DMA2 Stream 6                */
    DMA2_Stream7_IRQHandler,           /* DMA2 Stream 7                */
    USART6_IRQHandler,                 /* USART6                       */
    I2C3_EV_IRQHandler,                /* I2C3 event                   */
    I2C3_ER_IRQHandler,                /* I2C3 error                   */
    OTG_HS_EP1_OUT_IRQHandler,         /* USB OTG HS End Point 1 Out   */
    OTG_HS_EP1_IN_IRQHandler,          /* USB OTG HS End Point 1 In    */
    OTG_HS_WKUP_IRQHandler,            /* USB OTG HS Wakeup through EXTI */
    OTG_HS_IRQHandler,                 /* USB OTG HS                   */
#if CHIP_DEV_SUPPORT_PSSI
    DVP_PSSI_IRQHandler               /* DVP, PSSI                   */
#else
    DVP_IRQHandler,                   /* DVP                         */
#endif
    CRYPT_IRQHandler,                  /* CRYP global interrupt        */
    RNG_IRQHandler,                    /* Rng                          */
    FPU_IRQHandler,                    /* FPU                          */
    UART7_IRQHandler,                  /* UART7                        */
    UART8_IRQHandler,                  /* UART8                        */
    SPI4_IRQHandler,                   /* SPI4                         */
    SPI5_IRQHandler,                   /* SPI5                         */
    SPI6_IRQHandler,                   /* SPI6                         */
    SAI1_IRQHandler,                   /* SAI1                         */
    LTDC_IRQHandler,                   /* LTDC                         */
    LTDC_ER_IRQHandler,                /* LTDC error                   */
    DMA2D_IRQHandler,                  /* DMA2D                        */
#if CHIP_DEV_SUPPORT_SAI2_3
    SAI2_IRQHandler,                   /* SAI2                         */
#else
    NULL,
#endif
#if CHIP_DEV_SUPPORT_OCTOSPI
    OCTOSPI1_IRQHandler,               /* OCTOSPI1                     */
#else
    QUADSPI_IRQHandler,                /* QUADSPI                      */
#endif
    LPTIM1_IRQHandler,                 /* LPTIM1                       */
    CEC_IRQHandler,                    /* HDMI_CEC                     */
    I2C4_EV_IRQHandler,                /* I2C4 Event                   */
    I2C4_ER_IRQHandler,                /* I2C4 Error                   */
    SPDIF_RX_IRQHandler,               /* SPDIF_RX                     */
#if CHIP_DEV_SUPPORT_USB2
    OTG_FS_EP1_OUT_IRQHandler,         /* USB OTG FS End Point 1 Out   */
    OTG_FS_EP1_IN_IRQHandler,          /* USB OTG FS End Point 1 In    */
    OTG_FS_WKUP_IRQHandler,            /* USB OTG FS Wakeup through EXTI */
    OTG_FS_IRQHandler,                 /* USB OTG FS                   */
#else
    NULL,
    NULL,
    NULL,
    NULL,
#endif
    DMAMUX1_OVR_IRQHandler,            /* DMAMUX1 Overrun interrupt    */
#if CHIP_DEV_SUPPORT_HRTIM
    HRTIM1_Master_IRQHandler,          /* HRTIM Master Timer global Interrupt */
    HRTIM1_TIMA_IRQHandler,            /* HRTIM Timer A global Interrupt */
    HRTIM1_TIMB_IRQHandler,            /* HRTIM Timer B global Interrupt */
    HRTIM1_TIMC_IRQHandler,            /* HRTIM Timer C global Interrupt */
    HRTIM1_TIMD_IRQHandler,            /* HRTIM Timer D global Interrupt */
    HRTIM1_TIME_IRQHandler,            /* HRTIM Timer E global Interrupt */
    HRTIM1_FLT_IRQHandler,             /* HRTIM Fault global Interrupt   */
#else
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#endif
    DFSDM1_FLT0_IRQHandler,            /* DFSDM Filter0 Interrupt        */
    DFSDM1_FLT1_IRQHandler,            /* DFSDM Filter1 Interrupt        */
    DFSDM1_FLT2_IRQHandler,            /* DFSDM Filter2 Interrupt        */
    DFSDM1_FLT3_IRQHandler,            /* DFSDM Filter3 Interrupt        */
#if CHIP_DEV_SUPPORT_SAI2_3
    SAI3_IRQHandler,                   /* SAI3 global Interrupt          */
#else
    NULL,
#endif
    SWPMI1_IRQHandler,                 /* Serial Wire Interface 1 global interrupt */
    TIM15_IRQHandler,                  /* TIM15 global Interrupt        */
    TIM16_IRQHandler,                  /* TIM16 global Interrupt        */
    TIM17_IRQHandler,                  /* TIM17 global Interrupt        */
    MDIOS_WKUP_IRQHandler,             /* MDIOS Wakeup  Interrupt       */
    MDIOS_IRQHandler,                  /* MDIOS global Interrupt        */
#if CHIP_DEV_SUPPORT_JPGDEC
    JPEG_IRQHandler,                   /* JPEG global Interrupt         */
#else
    NULL,
#endif
    MDMA_IRQHandler,                   /* MDMA global Interrupt         */
#if CHIP_DEV_SUPPORT_DSI
    DSI_IRQHandler,                    /* DSI global Interrupt            */
#else
    NULL,
#endif
    SDMMC2_IRQHandler,                 /* SDMMC2 global Interrupt     */
#if defined CHIP_CORETYPE_CM7
    HSEM1_IRQHandler,                  /* HSEM1 global Interrupt      */
#else
    NULL,
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
    HSEM2_IRQHandler,                  /* HSEM2 global Interrupt      */
#else
    NULL,
#endif
    ADC3_IRQHandler,                   /* ADC3 global Interrupt       */
    DMAMUX2_OVR_IRQHandler,            /* DMAMUX Overrun interrupt    */
    BDMA_Channel0_IRQHandler,          /* BDMA Channel 0 global Interrupt */
    BDMA_Channel1_IRQHandler,          /* BDMA Channel 1 global Interrupt */
    BDMA_Channel2_IRQHandler,          /* BDMA Channel 2 global Interrupt */
    BDMA_Channel3_IRQHandler,          /* BDMA Channel 3 global Interrupt */
    BDMA_Channel4_IRQHandler,          /* BDMA Channel 4 global Interrupt */
    BDMA_Channel5_IRQHandler,          /* BDMA Channel 5 global Interrupt */
    BDMA_Channel6_IRQHandler,          /* BDMA Channel 6 global Interrupt */
    BDMA_Channel7_IRQHandler,          /* BDMA Channel 7 global Interrupt */
    COMP1_IRQHandler,                  /* COMP1 global Interrupt     */
    LPTIM2_IRQHandler,                 /* LP TIM2 global interrupt   */
    LPTIM3_IRQHandler,                 /* LP TIM3 global interrupt   */
    LPTIM4_IRQHandler,                 /* LP TIM4 global interrupt   */
    LPTIM5_IRQHandler,                 /* LP TIM5 global interrupt   */
    LPUART1_IRQHandler,                /* LP UART1 interrupt         */
#if CHIP_DEV_SUPPORT_CORE_CM4
    WWDG_RST_IRQHandler,               /* Window Watchdog reset interrupt (exti_d2_wwdg_it, exti_d1_wwdg_it) */
#else
    NULL,
#endif
    CRS_IRQHandler,                    /* Clock Recovery Global Interrupt */
    ECC_IRQHandler,                    /* ECC diagnostic Global Interrupt */
    SAI4_IRQHandler,                   /* SAI4 global interrupt      */
#if CHIP_DEV_SUPPORT_DTS
    DTS_IRQHandler,                    /* Digital Temperature Sensor  interrupt */
#else
    NULL,                              /* Reserved                   */
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4
    HOLD_CORE_IRQHandler,              /* Hold core interrupt        */
#else
    NULL,
#endif
    WAKEUP_PIN_IRQHandler,             /* Interrupt for all 6 wake-up pins */
#if CHIP_DEV_SUPPORT_OCTOSPI
    OCTOSPI2_IRQHandler,               /* OCTOSPI2 Interrupt       */
    OTFDEC1_IRQHandler,
    OTFDEC2_IRQHandler,
#else
    NULL,
    NULL,
    NULL,
#endif
#if CHIP_DEV_SUPPORT_FMAC
    FMAC_IRQHandler,                   /* FMAC Interrupt           */
#else
    NULL,
#endif
#if CHIP_DEV_SUPPORT_CORDIC
    CORDIC_IRQHandler,                 /* CORDIC Interrupt         */
#else
    NULL,
#endif
#if CHIP_DEV_SUPPORT_USART9_10
    UART9_IRQHandler,                  /* UART9 Interrupt          */
    USART10_IRQHandler,                /* USART10 Interrupt         */
#else
    NULL,
    NULL,
#endif
#if CHIP_DEV_SUPPORT_I2C5
    I2C5_EV_IRQHandler,                /* I2C5 Event Interrupt     */
    I2C5_ER_IRQHandler,                /* I2C5 Error Interrupt     */
#else
    NULL,
    NULL,
#endif
#if CHIP_DEV_SUPPORT_FDCAN3
    FDCAN3_IT0_IRQHandler,             /* FDCAN3 interrupt line 0  */
    FDCAN3_IT1_IRQHandler,             /* FDCAN3 interrupt line 1  */
#else
    NULL,
    NULL,
#endif
#if CHIP_DEV_SUPPORT_TIM23_24
    TIM23_IRQHandler,                  /* TIM23 global interrupt   */
    TIM24_IRQHandler,                  /* TIM24 global interrupt   */
#else
    NULL,
    NULL,
#endif
};

