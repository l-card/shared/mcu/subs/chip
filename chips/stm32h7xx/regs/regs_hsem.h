#ifndef CHIP_REGS_HSEM_STM32H7XX_H_
#define CHIP_REGS_HSEM_STM32H7XX_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"


#define CHIP_HSEM_SEMAPHORE_CNT     32 /* количество семафоров */

/* COREID для AHB master, которые могут блокировать семафор */
#define CHIP_HSEM_COREID_CM7        3
#if CHIP_DEV_SUPPORT_CORE_CM4
    #define CHIP_HSEM_COREID_CM4    1
#endif

/* количество прерываний блока HSEM и номера прерываний */
#define CHIP_HSEM_INT_IDX_CM7       0
#if CHIP_DEV_SUPPORT_CORE_CM4
    #define CHIP_HSEM_INT_IDX_CM4   1
    #define CHIP_HSEM_INT_CNT       2
#else
    #define CHIP_HSEM_INT_CNT       1
#endif

#if CHIP_DEV_SUPPORT_CORE_CM4
    #if defined CHIP_CORETYPE_CM4
        #define CHIP_HSEM_CUR_COREID    CHIP_HSEM_COREID_CM4
        #define CHIP_HSEM_REM_COREID    CHIP_HSEM_COREID_CM7

        #define CHIP_HSEM_CUR_INT_IDX   CHIP_HSEM_INT_IDX_CM4
        #define CHIP_HSEM_REM_INT_IDX   CHIP_HSEM_INT_IDX_CM7
    #else
        #define CHIP_HSEM_CUR_COREID    CHIP_HSEM_COREID_CM7
        #define CHIP_HSEM_REM_COREID    CHIP_HSEM_COREID_CM4

        #define CHIP_HSEM_CUR_INT_IDX   CHIP_HSEM_INT_IDX_CM7
        #define CHIP_HSEM_REM_INT_IDX   CHIP_HSEM_INT_IDX_CM4
    #endif
#else
    #define CHIP_HSEM_CUR_COREID    CHIP_HSEM_COREID_CM7
    #define CHIP_HSEM_CUR_INT_IDX   CHIP_HSEM_INT_IDX_CM7
#endif



#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
    #define CHIP_HSEM_NVIC_IRQN              HSEM2_IRQn
    #define CHIP_HSEM_ISR_NAME               HSEM2_IRQHandler
#else
    #define CHIP_HSEM_NVIC_IRQN              HSEM1_IRQn
    #define CHIP_HSEM_ISR_NAME               HSEM1_IRQHandler
#endif

typedef struct {
    __IO uint32_t IER;        /*!< HSEM interrupt enable register ,                Address offset:   0h     */
    __IO uint32_t ICR;        /*!< HSEM interrupt clear register ,                 Address offset:   4h     */
    __IO uint32_t ISR;        /*!< HSEM interrupt status register ,                Address offset:   8h     */
    __IO uint32_t MISR;       /*!< HSEM masked interrupt status register ,         Address offset:   Ch     */
} CHIP_REGS_HSEM_INTERRUPT_T;

typedef struct {
    __IO uint32_t R[CHIP_HSEM_SEMAPHORE_CNT];      /*!< 2-step write lock and read back registers,     Address offset: 00h-7Ch  */
    __IO uint32_t RLR[CHIP_HSEM_SEMAPHORE_CNT];    /*!< 1-step read lock registers,                    Address offset: 80h-FCh  */
    union {
        /* настройки прерывания по названию ядра CM7 и CM4 */
        struct {
            CHIP_REGS_HSEM_INTERRUPT_T CM7;
#if CHIP_DEV_SUPPORT_CORE_CM4
            CHIP_REGS_HSEM_INTERRUPT_T CM4;
#else
            uint32_t RESERVED[4]; /* для обеспечения фиксированного размера на одно и двухядерном контроллере */
#endif
        };
        /* настройки прерывания текущего или удаленного ядра, в зависимости от
       * того, для какого собрана прошивка */
        struct {
#ifdef CHIP_CORETYPE_CM7
            CHIP_REGS_HSEM_INTERRUPT_T CUR_CORE;
#if CHIP_DEV_SUPPORT_CORE_CM4
            CHIP_REGS_HSEM_INTERRUPT_T REM_CORE;
#endif
#endif
#ifdef CHIP_CORETYPE_CM4
            CHIP_REGS_HSEM_INTERRUPT_T REM_CORE;
            CHIP_REGS_HSEM_INTERRUPT_T CUR_CORE;
#endif
        };
        /* настройки прерываний по идексу в массиве */
        CHIP_REGS_HSEM_INTERRUPT_T INT[CHIP_HSEM_INT_CNT];
    };
    uint32_t  Reserved[8];   /* Reserved                                         Address offset: 120h-13Ch*/
    __IO uint32_t CR;         /*!< HSEM Semaphore clear register ,                Address offset: 140h      */
    __IO uint32_t KEYR;       /*!< HSEM Semaphore clear key register ,            Address offset: 144h      */
} CHIP_REGS_HSEM_T;

#define CHIP_REGS_HSEM                          ((CHIP_REGS_HSEM_T *) CHIP_MEMRGN_ADDR_PERIPH_HSEM)


/*********** SAI global configuration register (SAI_GCR)  *********************/
#define CHIP_REGFLD_SAI_GCR_SYNCIN              (0x00000003UL <<  0U) /* Synchronization inputs */

/*********** HSEM register semaphore x (HSEM_Rx) ******************************/
#define CHIP_REGFLD_HSEM_R_PROCID               (0x000000FFUL <<  0U) /* Semaphore ProcessID */
#define CHIP_REGFLD_HSEM_R_COREID               (0x0000000FUL <<  8U) /* Semaphore CoreID. */
#define CHIP_REGFLD_HSEM_R_LOCK                 (0x00000001UL << 31U) /* Lock indication. */

/*********** HSEM read lock register semaphore x (HSEM_RLRx) ******************/
#define CHIP_REGFLD_HSEM_RLR_PROCID             (0x000000FFUL <<  0U) /* Semaphore ProcessID */
#define CHIP_REGFLD_HSEM_RLR_COREID             (0x0000000FUL <<  8U) /* Semaphore CoreID. */
#define CHIP_REGFLD_HSEM_RLR_LOCK               (0x00000001UL << 31U) /* Lock indication. */

/*********** HSEM clear register (HSEM_CR) ************************************/
#define CHIP_REGFLD_HSEM_CR_COREID              (0x0000000FUL <<  8U) /* CoreID of semaphores to be cleared. */
#define CHIP_REGFLD_HSEM_CR_KEY                 (0x0000FFFFUL << 16U) /* semaphores clear key. */

/*********** HSEM interrupt clear register (HSEM_KEYR) ************************/
#define CHIP_REGFLD_HSEM_KEYR_KEY               (0x0000FFFFUL << 16U) /* semaphores clear key. */

#endif // CHIP_REGS_HSEM_STM32H7XX_H_
