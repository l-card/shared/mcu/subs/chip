#ifndef CHIP_CLK_H
#define CHIP_CLK_H

#include "chip_clk_defs.h"
#include "chip_config.h"
#include "chip_dev_spec_features.h"

#ifdef CHIP_CFG_CLK_HSE_MODE
    #define CHIP_CLK_HSE_MODE CHIP_CFG_CLK_HSE_MODE
#else
    #define CHIP_CLK_HSE_MODE CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_LSE_MODE
    #define CHIP_CLK_LSE_MODE CHIP_CFG_CLK_LSE_MODE
#else
    #define CHIP_CLK_LSE_MODE CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_HSI_EN
    #define CHIP_CLK_HSI_EN CHIP_CFG_CLK_HSI_EN
#else
    #define CHIP_CLK_HSI_EN 1
#endif

#ifdef CHIP_CFG_CLK_HSISYS_DIV_EN
    #define CHIP_CLK_HSISYS_DIV_EN CHIP_CFG_CLK_HSISYS_DIV_EN
#else
    #define CHIP_CLK_HSISYS_DIV_EN 1
#endif

#ifdef CHIP_CFG_CLK_HSE_PLLIN_DIV
    #define CHIP_CLK_HSE_PLLIN_DIV CHIP_CFG_CLK_HSE_PLLIN_DIV
#else
    #define CHIP_CLK_HSE_PLLIN_DIV 1
#endif

#ifdef CHIP_CFG_CLK_PLL_EN
    #define CHIP_CLK_PLL_EN CHIP_CFG_CLK_PLL_EN
#else
    #define CHIP_CLK_PLL_EN 0
#endif


#ifdef CHIP_CFG_CLK_AHB_DIV
    #define CHIP_CLK_AHB_DIV  CHIP_CFG_CLK_AHB_DIV
#else
    #define CHIP_CLK_AHB_DIV 1
#endif

#ifdef CHIP_CFG_CLK_APB1_DIV
    #define CHIP_CLK_APB1_DIV CHIP_CFG_CLK_APB1_DIV
#else
    #define CHIP_CLK_APB1_DIV 1
#endif

#ifdef CHIP_CFG_CLK_APB2_DIV
    #define CHIP_CLK_APB2_DIV CHIP_CFG_CLK_APB2_DIV
#else
    #define CHIP_CLK_APB2_DIV 1
#endif


#ifdef CHIP_CFG_CLK_PLL_SRC
    #define CHIP_CLK_PLL_SRC CHIP_CFG_CLK_PLL_SRC
#else
    #define CHIP_CLK_PLL_SRC CHIP_CLK_HSI_PLLIN
#endif


#ifdef CHIP_CFG_CLK_SYS_SRC
    #define CHIP_CLK_SYS_SRC CHIP_CFG_CLK_SYS_SRC
#else
    #define CHIP_CLK_SYS_SRC CHIP_CLK_HSI_SYSIN
#endif

#ifdef CHIP_CFG_CLK_ADC_SETUP_EN
    #define CHIP_CLK_ADC_SETUP_EN CHIP_CFG_CLK_ADC_SETUP_EN
#else
    #define CHIP_CLK_ADC_SETUP_EN 0
#endif
#if CHIP_CLK_ADC_SETUP_EN
    #ifndef CHIP_CFG_CLK_ADC_DIV
        #error ADC clock divider is not specified
    #else
        #define CHIP_CLK_ADC_DIV CHIP_CFG_CLK_ADC_DIV
    #endif
#endif
#ifdef CHIP_CFG_CLK_USB_SETUP_EN
    #define CHIP_CLK_USB_SETUP_EN CHIP_CFG_CLK_USB_SETUP_EN
#else
    #define CHIP_CLK_USB_SETUP_EN 0
#endif
#if CHIP_CLK_USB_SETUP_EN
    #ifndef CHIP_CFG_CLK_USB_MUL2_DIV
        #error USB clock divider is not specified
    #else
        #define CHIP_CLK_USB_MUL2_DIV CHIP_CFG_CLK_USB_MUL2_DIV
    #endif
#endif



#ifdef CHIP_CFG_CLK_DMA1_EN
    #define CHIP_CLK_DMA1_EN  CHIP_CFG_CLK_DMA1_EN
#else
    #define CHIP_CLK_DMA1_EN  0
#endif
#ifdef CHIP_CFG_CLK_DMA2_EN
    #define CHIP_CLK_DMA2_EN  CHIP_CFG_CLK_DMA2_EN
#else
    #define CHIP_CLK_DMA2_EN  0
#endif
#ifdef CHIP_CFG_CLK_SRAM_EN
    #define CHIP_CLK_SRAM_EN  CHIP_CFG_CLK_SRAM_EN
#else
    #define CHIP_CLK_SRAM_EN  1
#endif
#ifdef CHIP_CFG_CLK_FLASH_EN
    #define CHIP_CLK_FLASH_EN  CHIP_CFG_CLK_FLASH_EN
#else
    #define CHIP_CLK_FLASH_EN  1
#endif
#ifdef CHIP_CFG_CLK_CRC_EN
    #define CHIP_CLK_CRC_EN  CHIP_CFG_CLK_CRC_EN
#else
    #define CHIP_CLK_CRC_EN  0
#endif
#ifdef CHIP_CFG_CLK_XMC_EN
    #define CHIP_CLK_XMC_EN  CHIP_CFG_CLK_XMC_EN
#else
    #define CHIP_CLK_XMC_EN  0
#endif
#ifdef CHIP_CFG_CLK_AFIO_EN
    #define CHIP_CLK_AFIO_EN  CHIP_CFG_CLK_AFIO_EN
#else
    #define CHIP_CLK_AFIO_EN  1
#endif
#ifdef CHIP_CFG_CLK_GPIOA_EN
    #define CHIP_CLK_GPIOA_EN  CHIP_CFG_CLK_GPIOA_EN
#else
    #define CHIP_CLK_GPIOA_EN  0
#endif
#ifdef CHIP_CFG_CLK_GPIOB_EN
    #define CHIP_CLK_GPIOB_EN  CHIP_CFG_CLK_GPIOB_EN
#else
    #define CHIP_CLK_GPIOB_EN  0
#endif
#ifdef CHIP_CFG_CLK_GPIOC_EN
    #define CHIP_CLK_GPIOC_EN  CHIP_CFG_CLK_GPIOC_EN
#else
    #define CHIP_CLK_GPIOC_EN  0
#endif
#ifdef CHIP_CFG_CLK_GPIOD_EN
    #define CHIP_CLK_GPIOD_EN  CHIP_CFG_CLK_GPIOD_EN
#else
    #define CHIP_CLK_GPIOD_EN  0
#endif
#ifdef CHIP_CFG_CLK_GPIOE_EN
    #define CHIP_CLK_GPIOE_EN  CHIP_CFG_CLK_GPIOE_EN
#else
    #define CHIP_CLK_GPIOE_EN  0
#endif
#ifdef CHIP_CFG_CLK_PWR_EN
    #define CHIP_CLK_PWR_EN  CHIP_CFG_CLK_PWR_EN
#else
    #define CHIP_CLK_PWR_EN  1
#endif
#ifdef CHIP_CFG_CLK_BKP_EN
    #define CHIP_CLK_BKP_EN  CHIP_CFG_CLK_BKP_EN
#else
    #define CHIP_CLK_BKP_EN  0
#endif
#ifdef CHIP_CFG_CLK_WWDG_EN
    #define CHIP_CLK_WWDG_EN  CHIP_CFG_CLK_WWDG_EN
#else
    #define CHIP_CLK_WWDG_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR1_EN
    #define CHIP_CLK_TMR1_EN  CHIP_CFG_CLK_TMR1_EN
#else
    #define CHIP_CLK_TMR1_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR2_EN
    #define CHIP_CLK_TMR2_EN  CHIP_CFG_CLK_TMR2_EN
#else
    #define CHIP_CLK_TMR2_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR3_EN
    #define CHIP_CLK_TMR3_EN  CHIP_CFG_CLK_TMR3_EN
#else
    #define CHIP_CLK_TMR3_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR4_EN
    #define CHIP_CLK_TMR4_EN  CHIP_CFG_CLK_TMR4_EN
#else
    #define CHIP_CLK_TMR4_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR5_EN
    #define CHIP_CLK_TMR5_EN  CHIP_CFG_CLK_TMR5_EN
#else
    #define CHIP_CLK_TMR5_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR6_EN
    #define CHIP_CLK_TMR6_EN  CHIP_CFG_CLK_TMR6_EN
#else
    #define CHIP_CLK_TMR6_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR7_EN
    #define CHIP_CLK_TMR7_EN  CHIP_CFG_CLK_TMR7_EN
#else
    #define CHIP_CLK_TMR7_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR8_EN
    #define CHIP_CLK_TMR8_EN  CHIP_CFG_CLK_TMR8_EN
#else
    #define CHIP_CLK_TMR8_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR9_EN
    #define CHIP_CLK_TMR9_EN  CHIP_CFG_CLK_TMR9_EN
#else
    #define CHIP_CLK_TMR9_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR10_EN
    #define CHIP_CLK_TMR10_EN  CHIP_CFG_CLK_TMR10_EN
#else
    #define CHIP_CLK_TMR10_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR11_EN
    #define CHIP_CLK_TMR11_EN  CHIP_CFG_CLK_TMR11_EN
#else
    #define CHIP_CLK_TMR11_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR12_EN
    #define CHIP_CLK_TMR12_EN  CHIP_CFG_CLK_TMR12_EN
#else
    #define CHIP_CLK_TMR12_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR13_EN
    #define CHIP_CLK_TMR13_EN  CHIP_CFG_CLK_TMR13_EN
#else
    #define CHIP_CLK_TMR13_EN  0
#endif
#ifdef CHIP_CFG_CLK_TMR14_EN
    #define CHIP_CLK_TMR14_EN  CHIP_CFG_CLK_TMR14_EN
#else
    #define CHIP_CLK_TMR14_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART1_EN
    #define CHIP_CLK_USART1_EN  CHIP_CFG_CLK_USART1_EN
#else
    #define CHIP_CLK_USART1_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART2_EN
    #define CHIP_CLK_USART2_EN  CHIP_CFG_CLK_USART2_EN
#else
    #define CHIP_CLK_USART2_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART3_EN
    #define CHIP_CLK_USART3_EN  CHIP_CFG_CLK_USART3_EN
#else
    #define CHIP_CLK_USART3_EN  0
#endif
#ifdef CHIP_CFG_CLK_UART4_EN
    #define CHIP_CLK_UART4_EN  CHIP_CFG_CLK_UART4_EN
#else
    #define CHIP_CLK_UART4_EN  0
#endif
#ifdef CHIP_CFG_CLK_UART5_EN
    #define CHIP_CLK_UART5_EN  CHIP_CFG_CLK_UART5_EN
#else
    #define CHIP_CLK_UART5_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART6_EN
    #define CHIP_CLK_USART6_EN  CHIP_CFG_CLK_USART6_EN
#else
    #define CHIP_CLK_USART6_EN  0
#endif
#ifdef CHIP_CFG_CLK_UART7_EN
    #define CHIP_CLK_UART7_EN  CHIP_CFG_CLK_UART7_EN
#else
    #define CHIP_CLK_UART7_EN  0
#endif
#ifdef CHIP_CFG_CLK_UART8_EN
    #define CHIP_CLK_UART8_EN  CHIP_CFG_CLK_UART8_EN
#else
    #define CHIP_CLK_UART8_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI1_EN
    #define CHIP_CLK_SPI1_EN  CHIP_CFG_CLK_SPI1_EN
#else
    #define CHIP_CLK_SPI1_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI2_EN
    #define CHIP_CLK_SPI2_EN  CHIP_CFG_CLK_SPI2_EN
#else
    #define CHIP_CLK_SPI2_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI3_EN
    #define CHIP_CLK_SPI3_EN  CHIP_CFG_CLK_SPI3_EN
#else
    #define CHIP_CLK_SPI3_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI4_EN
    #define CHIP_CLK_SPI4_EN  CHIP_CFG_CLK_SPI4_EN
#else
    #define CHIP_CLK_SPI4_EN  0
#endif
#ifdef CHIP_CFG_CLK_I2C1_EN
    #define CHIP_CLK_I2C1_EN  CHIP_CFG_CLK_I2C1_EN
#else
    #define CHIP_CLK_I2C1_EN  0
#endif
#ifdef CHIP_CFG_CLK_I2C2_EN
    #define CHIP_CLK_I2C2_EN  CHIP_CFG_CLK_I2C2_EN
#else
    #define CHIP_CLK_I2C2_EN  0
#endif
#ifdef CHIP_CFG_CLK_I2C3_EN
    #define CHIP_CLK_I2C3_EN  CHIP_CFG_CLK_I2C3_EN
#else
    #define CHIP_CLK_I2C3_EN  0
#endif
#ifdef CHIP_CFG_CLK_USB_EN
    #define CHIP_CLK_USB_EN  CHIP_CFG_CLK_USB_EN
#else
    #define CHIP_CLK_USB_EN  0
#endif
#ifdef CHIP_CFG_CLK_CAN_EN
    #define CHIP_CLK_CAN_EN  CHIP_CFG_CLK_CAN_EN
#else
    #define CHIP_CLK_CAN_EN  0
#endif
#ifdef CHIP_CFG_CLK_ADC_EN
    #define CHIP_CLK_ADC_EN  CHIP_CFG_CLK_ADC_EN
#else
    #define CHIP_CLK_ADC_EN  0
#endif



#ifdef CHIP_CFG_LTIMER_EN
    #define CHIP_LTIMER_EN   CHIP_CFG_LTIMER_EN
#else
    #define CHIP_LTIMER_EN   1
#endif

#define CHIP_CLK_HSE_EN  (CHIP_CLK_HSE_MODE != CHIP_CLK_EXTMODE_DIS)
#define CHIP_CLK_LSE_EN  (CHIP_CLK_LSE_MODE != CHIP_CLK_EXTMODE_DIS)


#if CHIP_CLK_HSE_EN
    #ifndef CHIP_CFG_CLK_HSE_FREQ
        #error HSE clock frequency is not specified
    #endif
    #define CHIP_CLK_HSE_FREQ       CHIP_CFG_CLK_HSE_FREQ

    #ifdef CHIP_CFG_CLK_HSE_CFD_EN
        #define CHIP_CLK_HSE_CFD_EN CHIP_CFG_CLK_HSE_CFD_EN
    #else
        #define CHIP_CLK_HSE_CFD_EN 0
    #endif
#else
    #define CHIP_CLK_HSE_FREQ       0
    #define CHIP_CLK_HSE_CFD_EN     0
#endif


#define CHIP_CLK_HSE_PLLIN_EN (CHIP_CLK_HSE_EN && CHIP_CLK_PLL_EN)
#if CHIP_CLK_HSE_PLLIN_EN
    #define CHIP_CLK_HSE_PLLIN_FREQ CHIP_CLK_HSE_FREQ/CHIP_CLK_HSE_PLLIN_DIV
#else
    #define CHIP_CLK_HSE_PLLIN_FREQ 0
#endif



#define CHIP_CLK_HSI_DIV_EN         CHIP_CLK_HSI_EN
#define CHIP_CLK_HSI_PLLIN_EN       CHIP_CLK_HSI_EN
#define CHIP_CLK_HSI_SYSIN_EN       CHIP_CLK_HSI_EN

#if CHIP_CLK_HSI_EN
    #if CHIP_CLK_HSISYS_DIV_EN
        #define CHIP_CLK_HSI_SYSIN_FREQ  CHIP_CLK_HSI_DIV_FREQ
    #else
        #define CHIP_CLK_HSI_SYSIN_FREQ  CHIP_CLK_HSI_SRC_FREQ
    #endif
#else
    #define CHIP_CLK_HSI_SYSIN_FREQ 0
#endif



#if CHIP_CLK_PLL_EN
    #define CHIP_CLK_PLL_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_PLL_SRC)
    #if CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI_PLLIN_ID
        #define CHIP_CLK_PLLIN_FREQ    CHIP_CLK_HSI_PLLIN_FREQ
        #define CHIP_CLK_PLLIN_EN      CHIP_CLK_HSI_PLLIN_EN
    #elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSE_PLLIN_ID
        #define CHIP_CLK_PLLIN_FREQ    CHIP_CLK_HSE_PLLIN_FREQ
        #define CHIP_CLK_PLLIN_EN      CHIP_CLK_HSE_PLLIN_EN
    #else
        #define CHIP_CLK_PLLIN_FREQ    0
        #define CHIP_CLK_PLLIN_EN      0
    #endif

    #define CHIP_CLK_PLL_FREQ          (CHIP_CLK_PLLIN_FREQ * CHIP_CFG_CLK_PLL_MUL)
#else
    #define CHIP_CLK_PLLIN_FREQ    0
    #define CHIP_CLK_PLLIN_EN      0
    #define CHIP_CLK_PLL_FREQ      0
#endif

#define CHIP_CLK_SYS_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CLK_SYS_SRC)
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSI_SYSIN_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_HSI_SYSIN_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_HSI_SYSIN_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_HSE_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_HSE_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLL_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_PLL_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_PLL_EN
#else
    #define CHIP_CLK_SYS_FREQ 0
    #define CHIP_CLK_SYS_EN   0
#endif




#define CHIP_CLK_LSE_OSC_FREQ           CHIP_HZ(32768) /* требуемая частота LSE в режиме CHIP_CLK_EXTMODE_OSC */



#define CHIP_CLK_AHB_FREQ       (CHIP_CLK_SYS_FREQ / CHIP_CLK_AHB_DIV)
#define CHIP_CLK_APB1_FREQ      (CHIP_CLK_AHB_FREQ / CHIP_CLK_APB1_DIV) /* частота шины APB1 */
#define CHIP_CLK_APB2_FREQ      (CHIP_CLK_AHB_FREQ / CHIP_CLK_APB2_DIV) /* частота шины APB2 */

/* частота таймеров в 2 раза больше частоты соответствующей шины APB, если только делитель
 * для ABP не равен 1 */
#if CHIP_CLK_APB1_DIV == 1
    #define CHIP_CLK_APB1_TMR_FREQ   CHIP_CLK_APB1_FREQ
#else
    #define CHIP_CLK_APB1_TMR_FREQ   (2*CHIP_CLK_APB1_FREQ)
#endif
#if CHIP_CLK_APB2_DIV == 1
    #define CHIP_CLK_APB2_TMR_FREQ   CHIP_CLK_APB2_FREQ
#else
    #define CHIP_CLK_APB2_TMR_FREQ   (2*CHIP_CLK_APB2_FREQ)
#endif


#define CHIP_CLK_CPU_FREQ        CHIP_CLK_AHB_FREQ
#define CHIP_CLK_SYSTICK_FREQ    CHIP_CLK_AHB_FREQ /* есть опция использовать AHB_FREQ/8, но в CMSIS она не используется */

#define CHIP_CLK_DMA1_FREQ       CHIP_CLK_AHB_FREQ
#define CHIP_CLK_DMA2_FREQ       CHIP_CLK_AHB_FREQ
#define CHIP_CLK_SRAM_FREQ       CHIP_CLK_AHB_FREQ
#define CHIP_CLK_FLASH_FREQ      CHIP_CLK_AHB_FREQ
#define CHIP_CLK_XMC_FREQ        CHIP_CLK_AHB_FREQ



#define CHIP_CLK_TMR1_FREQ      CHIP_CLK_APB2_TMR_FREQ
#define CHIP_CLK_TMR2_FREQ      CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR3_FREQ      CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR4_FREQ      CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR5_FREQ      CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR6_FREQ      CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR7_FREQ      CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR8_FREQ      CHIP_CLK_APB2_TMR_FREQ
#define CHIP_CLK_TMR9_FREQ      CHIP_CLK_APB2_TMR_FREQ
#define CHIP_CLK_TMR10_FREQ     CHIP_CLK_APB2_TMR_FREQ
#define CHIP_CLK_TMR11_FREQ     CHIP_CLK_APB2_TMR_FREQ
#define CHIP_CLK_TMR12_FREQ     CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR13_FREQ     CHIP_CLK_APB1_TMR_FREQ
#define CHIP_CLK_TMR14_FREQ     CHIP_CLK_APB1_TMR_FREQ


#define CHIP_CLK_USART1_FREQ    CHIP_CLK_APB2_FREQ
#define CHIP_CLK_USART2_FREQ    CHIP_CLK_APB1_FREQ
#define CHIP_CLK_USART3_FREQ    CHIP_CLK_APB1_FREQ
#define CHIP_CLK_UART4_FREQ     CHIP_CLK_APB1_FREQ
#define CHIP_CLK_UART5_FREQ     CHIP_CLK_APB1_FREQ
#define CHIP_CLK_USART6_FREQ    CHIP_CLK_APB2_FREQ
#define CHIP_CLK_UART7_FREQ     CHIP_CLK_APB2_FREQ
#define CHIP_CLK_UART8_FREQ     CHIP_CLK_APB2_FREQ

#define CHIP_CLK_SPI1_FREQ      CHIP_CLK_APB2_FREQ
#define CHIP_CLK_SPI2_FREQ      CHIP_CLK_APB1_FREQ
#define CHIP_CLK_SPI3_FREQ      CHIP_CLK_APB1_FREQ
#define CHIP_CLK_SPI4_FREQ      CHIP_CLK_APB1_FREQ

#define CHIP_CLK_I2S1_FREQ      CHIP_CLK_SYS_FREQ
#define CHIP_CLK_I2S2_FREQ      CHIP_CLK_SYS_FREQ
#define CHIP_CLK_I2S3_FREQ      CHIP_CLK_SYS_FREQ
#define CHIP_CLK_I2S4_FREQ      CHIP_CLK_SYS_FREQ

#define CHIP_CLK_I2C1_FREQ      CHIP_CLK_APB1_FREQ
#define CHIP_CLK_I2C2_FREQ      CHIP_CLK_APB1_FREQ
#define CHIP_CLK_I2C3_FREQ      CHIP_CLK_APB1_FREQ

#define CHIP_CLK_CAN_FREQ       CHIP_CLK_APB1_FREQ
#if CHIP_CLK_ADC_SETUP_EN
    #define CHIP_CLK_ADC_FREQ   (CHIP_CLK_APB2_FREQ/CHIP_CLK_ADC_DIV)
#endif
#if CHIP_CLK_USB_SETUP_EN
    #define CHIP_CLK_USB_FREQ   ((2*CHIP_CLK_PLL_FREQ)/CHIP_CLK_USB_MUL2_DIV)
#endif




#endif // CHIP_CLK_H
