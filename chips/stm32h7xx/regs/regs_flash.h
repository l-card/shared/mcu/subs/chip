#ifndef CHIP_STM32H7XX_REGS_FLASH_H_
#define CHIP_STM32H7XX_REGS_FLASH_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"


#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_FLASH_BANK_CNT  2
#else
#define CHIP_FLASH_BANK_CNT  1
#endif


/* FLASH Registers */
typedef struct {
  __IO uint32_t ACR;             /* FLASH access control register,                            Address offset: 0x00  */
  union {
      struct {
          __IO uint32_t KEYR1;           /* FLASH Key Register for bank1,                             Address offset: 0x04  */
          __IO uint32_t OPTKEYR;         /* FLASH Option Key Register,                                Address offset: 0x08  */
          __IO uint32_t CR1;             /* FLASH Control Register for bank1,                         Address offset: 0x0C  */
          __IO uint32_t SR1;             /* FLASH Status Register for bank1,                          Address offset: 0x10  */
          __IO uint32_t CCR1;            /* FLASH Control Register for bank1,                         Address offset: 0x14  */
          __IO uint32_t OPTCR;           /* FLASH Option Control Register,                            Address offset: 0x18  */
          __IO uint32_t OPTSR_CUR;       /* FLASH Option Status Current Register,                     Address offset: 0x1C  */
          __IO uint32_t OPTSR_PRG;       /* FLASH Option Status to Program Register,                  Address offset: 0x20  */
          __IO uint32_t OPTCCR;          /* FLASH Option Clear Control Register,                      Address offset: 0x24  */
          __IO uint32_t PRAR_CUR1;       /* FLASH Current Protection Address Register for bank1,      Address offset: 0x28  */
          __IO uint32_t PRAR_PRG1;       /* FLASH Protection Address to Program Register for bank1,   Address offset: 0x2C  */
          __IO uint32_t SCAR_CUR1;       /* FLASH Current Secure Address Register for bank1,          Address offset: 0x30  */
          __IO uint32_t SCAR_PRG1;       /* FLASH Secure Address to Program Register for bank1,       Address offset: 0x34  */
          __IO uint32_t WPSN_CUR1;       /* FLASH Current Write Protection Register on bank1,         Address offset: 0x38  */
          __IO uint32_t WPSN_PRG1;       /* FLASH Write Protection to Program Register on bank1,      Address offset: 0x3C  */
          __IO uint32_t BOOT7_CUR;       /* FLASH Current CM7 Boot Address for Pelican Core Register, Address offset: 0x40  */
          __IO uint32_t BOOT7_PRG;       /* FLASH CM7 Boot Address to Program for Pelican Core Register, Address offset: 0x44  */
#if CHIP_DEV_SUPPORT_CORE_CM4
          __IO uint32_t BOOT4_CUR;       /* FLASH Current CM4 Boot Address for Pelican Core Register,  Address offset: 0x48  */
          __IO uint32_t BOOT4_PRG;       /* FLASH CM4 Boot Address to Program for Pelican Core Register,  Address offset: 0x4C  */
#else
          uint32_t      RESERVED0[2];    /* Reserved, 0x48 to 0x4C                                                          */
#endif
          __IO uint32_t CRCCR1;          /* FLASH CRC Control register For Bank1 Register ,           Address offset: 0x50  */
          __IO uint32_t CRCSADD1;        /* FLASH CRC Start Address Register for Bank1 ,              Address offset: 0x54  */
          __IO uint32_t CRCEADD1;        /* FLASH CRC End Address Register for Bank1 ,                Address offset: 0x58  */
          __IO uint32_t CRCDATA;         /* FLASH CRC Data Register for Bank1 ,                       Address offset: 0x5C  */
          __IO uint32_t ECC_FA1;         /* FLASH ECC Fail Address For Bank1 Register ,               Address offset: 0x60  */
          uint32_t      RESERVED1_1[3];
#if CHIP_DEV_SUPPORT_ITCM_AXI_SHARE || CHIP_DEV_SUPPORT_CPUFREQ_BOOST
          __IO uint32_t OPTSR2_CUR;      /* FLASH Option Status 2 Current Register,                   Address offset: 0x70  */
          __IO uint32_t OPTSR2_PRG;      /* FLASH Option Status 2 to Program Register,                Address offset: 0x74  */
#else
          uint32_t      RESERVED1_2[2];
#endif
          uint32_t      RESERVED1_3[35];  /* Reserved, 0x78 to 0x100                                                        */
#if CHIP_DEV_SUPPORT_FLASH_BANK2
          __IO uint32_t KEYR2;           /* FLASH Key Register for bank2,                             Address offset: 0x104 */
          uint32_t      RESERVED2;       /* Reserved, 0x108                                                                 */
          __IO uint32_t CR2;             /* FLASH Control Register for bank2,                         Address offset: 0x10C */
          __IO uint32_t SR2;             /* FLASH Status Register for bank2,                          Address offset: 0x110 */
          __IO uint32_t CCR2;            /* FLASH Status Register for bank2,                          Address offset: 0x114 */
          uint32_t      RESERVED3[4];    /* Reserved, 0x118 to 0x124                                                        */
          __IO uint32_t PRAR_CUR2;       /* FLASH Current Protection Address Register for bank2,      Address offset: 0x128 */
          __IO uint32_t PRAR_PRG2;       /* FLASH Protection Address to Program Register for bank2,   Address offset: 0x12C */
          __IO uint32_t SCAR_CUR2;       /* FLASH Current Secure Address Register for bank2,          Address offset: 0x130 */
          __IO uint32_t SCAR_PRG2;       /* FLASH Secure Address Register for bank2,                  Address offset: 0x134 */
          __IO uint32_t WPSN_CUR2;       /* FLASH Current Write Protection Register on bank2,         Address offset: 0x138 */
          __IO uint32_t WPSN_PRG2;       /* FLASH Write Protection to Program Register on bank2,      Address offset: 0x13C */
          uint32_t      RESERVED4[4];    /* Reserved, 0x140 to 0x14C                                                        */
          __IO uint32_t CRCCR2;          /* FLASH CRC Control register For Bank2 Register ,           Address offset: 0x150 */
          __IO uint32_t CRCSADD2;        /* FLASH CRC Start Address Register for Bank2 ,              Address offset: 0x154 */
          __IO uint32_t CRCEADD2;        /* FLASH CRC End Address Register for Bank2 ,                Address offset: 0x158 */
          __IO uint32_t CRCDATA2;        /* FLASH CRC Data Register for Bank2 ,                       Address offset: 0x15C */
          __IO uint32_t ECC_FA2;         /* FLASH ECC Fail Address For Bank2 Register ,               Address offset: 0x160 */
#endif
      };
      struct {
          __IO uint32_t KEYR;
          uint32_t      RESERVED;
          __IO uint32_t CR;
          __IO uint32_t SR;
          __IO uint32_t CCR;
          uint32_t      RESERVED5[4];
          __IO uint32_t PRAR_CUR;
          __IO uint32_t PRAR_PRG;
          __IO uint32_t SCAR_CUR;
          __IO uint32_t SCAR_PRG;
          __IO uint32_t WPSN_CUR;
          __IO uint32_t WPSN_PRG;
          uint32_t      RESERVED6[4];
          __IO uint32_t CRCCR;
          __IO uint32_t CRCSADD;
          __IO uint32_t CRCEADD;
          __IO uint32_t CRCDATA;
          __IO uint32_t ECC_FA;
          uint32_t      RESERVED7[40];
      } BANK[CHIP_FLASH_BANK_CNT];
  };
} CHIP_REGS_FLASH_T;

#define CHIP_REGS_FLASH          ((CHIP_REGS_FLASH_T *) CHIP_MEMRGN_ADDR_PERIPH_FLASH)


#define CHIP_FLASH_BANK_SIZE     0x00100000   /* 1MB */
#define CHIP_FLASH_MEM_SIZE      (CHIP_FLASH_BANK_CNT * CHIP_FLASH_BANK_SIZE)
#define CHIP_FLASH_SECTOR_SIZE   0x00020000 /* 128 KB */

#define CHIP_FLASH_SECTOR_CNT    8
#define CHIP_FLASH_WRWORD_SIZE   32


/****** FLASH access control register (FLASH_ACR) *****************************/
#define CHIP_REGFLD_FLASH_ACR_LATENCY               (0x0000000FUL <<  0U) /* Read latency */
#define CHIP_REGFLD_FLASH_ACR_WRHIGHFREQ            (0x00000003UL <<  4U) /* Flash signal delay */

/****** FLASH bank control register (FLASH_CR) ********************************/
#define CHIP_REGFLD_FLASH_CR_LOCK                   (0x00000001UL <<  0U) /* configuration lock */
#define CHIP_REGFLD_FLASH_CR_PG                     (0x00000001UL <<  1U) /* Internal buffer control */
#define CHIP_REGFLD_FLASH_CR_SER                    (0x00000001UL <<  2U) /* Sector erase request */
#define CHIP_REGFLD_FLASH_CR_BER                    (0x00000001UL <<  3U) /* Erase request */
#define CHIP_REGFLD_FLASH_CR_PSIZE                  (0x00000003UL <<  4U) /* Program size */
#define CHIP_REGFLD_FLASH_CR_FW                     (0x00000001UL <<  6U) /* Write forcing control */
#define CHIP_REGFLD_FLASH_CR_START                  (0x00000001UL <<  7U) /* Erase start control */
#define CHIP_REGFLD_FLASH_CR_SNB                    (0x00000007UL <<  8U) /* Sector erase selection number */
#define CHIP_REGFLD_FLASH_CR_CRC_EN                 (0x00000001UL << 15U) /* CRC control */
#define CHIP_REGFLD_FLASH_CR_EOPIE                  (0x00000001UL << 16U) /* End-of-program interrupt control */
#define CHIP_REGFLD_FLASH_CR_WRPERRIE               (0x00000001UL << 17U) /* Write protection error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_PGSERRIE               (0x00000001UL << 18U) /* Programming sequence error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_STRBERRIE              (0x00000001UL << 19U) /* Strobe error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_INCERRIE               (0x00000001UL << 21U) /* Inconsistency error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_OPERRIE                (0x00000001UL << 22U) /* Write/erase error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_RDPERRIE               (0x00000001UL << 23U) /* Read protection error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_RDSERRIE               (0x00000001UL << 24U) /* Secure error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_SNECCERRIE             (0x00000001UL << 25U) /* ECC single correction error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_DBECCERRIE             (0x00000001UL << 26U) /* ECC double detection error interrupt enable */
#define CHIP_REGFLD_FLASH_CR_CRCENDIE               (0x00000001UL << 27U) /* CRC end of calculation interrupt enable */
#define CHIP_REGFLD_FLASH_CR_CRCRDERRIE             (0x00000001UL << 28U) /* CRC read error interrupt enable */

/****** FLASH bank status register (FLASH_SR) *********************************/
#define CHIP_REGFLD_FLASH_SR_BSY                    (0x00000001UL <<  0U) /* Busy flag */
#define CHIP_REGFLD_FLASH_SR_WBNE                   (0x00000001UL <<  1U) /* Write buffer not empty flag */
#define CHIP_REGFLD_FLASH_SR_QW                     (0x00000001UL <<  2U) /* Wait queue flag */
#define CHIP_REGFLD_FLASH_SR_CRC_BUSY               (0x00000001UL <<  3U) /* CRC busy flag */
#define CHIP_REGFLD_FLASH_SR_EOP                    (0x00000001UL << 16U) /* End-of-program flag */
#define CHIP_REGFLD_FLASH_SR_WRPERR                 (0x00000001UL << 17U) /* Write protection error flag */
#define CHIP_REGFLD_FLASH_SR_PGSERR                 (0x00000001UL << 18U) /* Programming sequence error flag */
#define CHIP_REGFLD_FLASH_SR_STRBERR                (0x00000001UL << 19U) /* Strobe error flag */
#define CHIP_REGFLD_FLASH_SR_INCERR                 (0x00000001UL << 21U) /* Inconsistency error flag */
#define CHIP_REGFLD_FLASH_SR_OPERR                  (0x00000001UL << 22U) /* Write/erase error flag */
#define CHIP_REGFLD_FLASH_SR_RDPERR                 (0x00000001UL << 23U) /* Read protection error flag */
#define CHIP_REGFLD_FLASH_SR_RDSERR                 (0x00000001UL << 24U) /* Secure error flag */
#define CHIP_REGFLD_FLASH_SR_SNECCERR               (0x00000001UL << 25U) /* Single correction error flag */
#define CHIP_REGFLD_FLASH_SR_DBECCERR               (0x00000001UL << 26U) /* ECC double detection error flag */
#define CHIP_REGFLD_FLASH_SR_CRCEND                 (0x00000001UL << 27U) /* CRC end of calculation flag */
#define CHIP_REGFLD_FLASH_SR_CRCRDERR               (0x00000001UL << 28U) /* CRC read error flag */

/****** FLASH bank clear control register (FLASH_CCR)  ************************/
#define CHIP_REGFLD_FLASH_CCR_CLR_EOP               (0x00000001UL << 16U)
#define CHIP_REGFLD_FLASH_CCR_CLR_WRPERR            (0x00000001UL << 17U)
#define CHIP_REGFLD_FLASH_CCR_CLR_PGSERR            (0x00000001UL << 18U)
#define CHIP_REGFLD_FLASH_CCR_CLR_STRBERR           (0x00000001UL << 19U)
#define CHIP_REGFLD_FLASH_CCR_CLR_INCERR            (0x00000001UL << 21U)
#define CHIP_REGFLD_FLASH_CCR_CLR_OPERR             (0x00000001UL << 22U)
#define CHIP_REGFLD_FLASH_CCR_CLR_RDPERR            (0x00000001UL << 23U)
#define CHIP_REGFLD_FLASH_CCR_CLR_RDSERR            (0x00000001UL << 24U)
#define CHIP_REGFLD_FLASH_CCR_CLR_SNECCERR          (0x00000001UL << 25U)
#define CHIP_REGFLD_FLASH_CCR_CLR_DBECCERR          (0x00000001UL << 26U)
#define CHIP_REGFLD_FLASH_CCR_CLR_CRCEND            (0x00000001UL << 27U)
#define CHIP_REGFLD_FLASH_CCR_CLR_CRCRDERR          (0x00000001UL << 28U)

/****** FLASH option control register (FLASH_OPTCR) ***************************/
#define CHIP_REGFLD_FLASH_OPTCR_OPTLOCK             (0x00000001UL <<  0U) /* Lock option configuration */
#define CHIP_REGFLD_FLASH_OPTCR_OPTSTART            (0x00000001UL <<  1U) /* Option byte start change option configuration */
#if CHIP_DEV_SUPPORT_FLASH_MER
#define CHIP_REGFLD_FLASH_OPTCR_MER                 (0x00000001UL <<  2U) /* Mass erase request */
#endif
#define CHIP_REGFLD_FLASH_OPTCR_OPTCHANGEERRIE      (0x00000001UL << 30U) /* Option byte change error interrupt enable */
#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_REGFLD_FLASH_OPTCR_SWAP_BANK           (0x00000001UL << 31U) /* Bank swapping option configuration */
#endif

/****** FLASH option status register (FLASH_OPTSR_CUR/FLASH_OPTSR_PRG) ************************/
#define CHIP_REGFLD_FLASH_OPTSR_OPT_BUSY            (0x00000001UL <<  0U) /* Option byte change ongoing flag  (_CUR only) */
#define CHIP_REGFLD_FLASH_OPTSR_BOR_LEV             (0x00000003UL <<  2U) /* Brownout level option status bit */
#define CHIP_REGFLD_FLASH_OPTSR_IWDG1_SW            (0x00000001UL <<  4U) /* IWDG1 control mode option status */
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_FLASH_OPTSR_IWDG2_SW            (0x00000001UL <<  5U) /* IWDG2 control mode option status */
#endif
#define CHIP_REGFLD_FLASH_OPTSR_NRST_STOP_D1        (0x00000001UL <<  6U) /* D1 domain DStop entry reset option status */
#define CHIP_REGFLD_FLASH_OPTSR_NRST_STBY_D1        (0x00000001UL <<  7U) /* D1 domain DStandby entry reset option status */
#define CHIP_REGFLD_FLASH_OPTSR_RDP                 (0x000000FFUL <<  8U) /* Readout protection level option status */
#define CHIP_REGFLD_FLASH_OPTSR_FZ_IWDG_STOP        (0x00000001UL << 17U) /* IWDG Stop mode freeze option status */
#define CHIP_REGFLD_FLASH_OPTSR_FZ_IWDG_SDBY        (0x00000001UL << 18U) /* IWDG Standby mode freeze option status */
#define CHIP_REGFLD_FLASH_OPTSR_ST_RAM_SIZE         (0x00000003UL << 19U) /* ST RAM size option status */
#define CHIP_REGFLD_FLASH_OPTSR_SECURITY            (0x00000001UL << 21U) /* Security enable option status */
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_FLASH_OPTSR_BOOT_CM4            (0x00000001UL << 22U) /* Cortex-M4 boot option status */
#define CHIP_REGFLD_FLASH_OPTSR_BOOT_CM7            (0x00000001UL << 23U) /* Cortex-M7 boot option status */
#endif
#if CHIP_DEV_SUPPORT_D2_RSTCTL
#define CHIP_REGFLD_FLASH_OPTSR_NRST_STOP_D2        (0x00000001UL << 24U) /* D2 domain DStop entry reset option status */
#define CHIP_REGFLD_FLASH_OPTSR_NRST_STBY_D2        (0x00000001UL << 25U) /* D2 domain DStandby entry reset option status */
#endif
#define CHIP_REGFLD_FLASH_OPTSR_IO_HSLV             (0x00000001UL << 29U) /* I/O high-speed at low-voltage status */
#define CHIP_REGFLD_FLASH_OPTSR_OPTCHANGEERR        (0x00000001UL << 30U) /* Option byte change error flag (_CUR only) */
#if CHIP_DEV_SUPPORT_FLASH_BANK2
#define CHIP_REGFLD_FLASH_OPTSR_SWAP_BANK_OPT       (0x00000001UL << 31U) /* Bank swapping option status */
#endif

/****** FLASH option clear control register (FLASH_OPTCCR) ********************/
#define CHIP_REGFLD_FLASH_OPTCCR_CLR_OPTCHANGEERR   (0x00000001UL << 30U) /* OPTCHANGEERR clear */

/****** FLASH protection address (FLASH_PRAR_CUR/FLASH_PRAR_PRG)  *************/
#define CHIP_REGFLD_FLASH_PRAR_PROT_AREA_START      (0x00000FFFUL <<  0U) /* PCROP area start */
#define CHIP_REGFLD_FLASH_PRAR_PROT_AREA_END        (0x00000FFFUL << 16U) /* PCROP area end */
#define CHIP_REGFLD_FLASH_PRAR_DMEP                 (0x00000001UL << 31U) /* PCROP protected erase enable option */

/****** FLASH bank secure address (FLASH_SCAR_CUR/FLASH_SCAR_PRG) *************/
#define CHIP_REGFLD_FLASH_SCAR_SEC_AREA_START       (0x00000FFFUL <<  0U) /* Secure-only area start */
#define CHIP_REGFLD_FLASH_SCAR_SEC_AREA_END         (0x00000FFFUL << 16U) /* Secure-only area end */
#define CHIP_REGFLD_FLASH_SCAR_DMES                 (0x00000001UL << 31U) /* Secure access protected erase enable option */

/****** FLASH bank write sector protection (FLASH_WPSN_CUR/FLASH_WPSN_PRG)*****/
#define CHIP_REGFLD_FLASH_WPSN_WRPSN                (0x000000FFUL <<  0U) /* Sector write protection option */

/****** FLASH register boot address for Cortex-M7 core (FLASH_BOOT7_CURR/PRG) */
#define CHIP_REGFLD_FLASH_BOOT7_ADD0                (0x0000FFFFUL <<  0U)
#define CHIP_REGFLD_FLASH_BOOT7_ADD1                (0x0000FFFFUL << 16U)

/****** FLASH register boot address for Cortex-M4 core (FLASH_BOOT4_CURR/PRG) */
#define CHIP_REGFLD_FLASH_BOOT4_ADD0                (0x0000FFFFUL <<  0U)
#define CHIP_REGFLD_FLASH_BOOT4_ADD1                (0x0000FFFFUL << 16U)

/****** FLASH bank CRC control register (FLASH_CRCCR) *************************/
#define CHIP_REGFLD_FLASH_CRCCR_CRC_SECT            (0x00000007UL <<  0U) /* CRC sector number */
#define CHIP_REGFLD_FLASH_CRCCR_CRC_BY_SECT         (0x00000001UL <<  8U) /* CRC sector mode select */
#define CHIP_REGFLD_FLASH_CRCCR_ADD_SECT            (0x00000001UL <<  9U) /* CRC sector select */
#define CHIP_REGFLD_FLASH_CRCCR_CLEAN_SECT          (0x00000001UL << 10U) /* CRC sector list clear */
#define CHIP_REGFLD_FLASH_CRCCR_START_CRC           (0x00000001UL << 16U) /* CRC start */
#define CHIP_REGFLD_FLASH_CRCCR_CLEAN_CRC           (0x00000001UL << 17U) /* CRC clear */
#define CHIP_REGFLD_FLASH_CRCCR_CRC_BURST           (0x00000003UL << 20U) /* CRC burst size */
#define CHIP_REGFLD_FLASH_CRCCR_ALL_BANK            (0x00000001UL << 22U) /* All bank CRC select bit */

/****** FLASH bank CRC start address register (FLASH_CRCSADD) *****************/
#define CHIP_REGFLD_FLASH_CRCSADD_CRC_START_ADDR    (0x000FFFFFUL <<  0U)

/******* FLASH bank CRC end address register (FLASH_CRCEADD) ******************/
#define CHIP_REGFLD_FLASH_CRCEADD_CRC_END_ADDR      (0x000FFFFFUL <<  0U)

/****** FLASH CRC data register (FLASH_CRCDATAR) ******************************/
#define CHIP_REGFLD_FLASH_CRCDATA_CRC_DATA          (0xFFFFFFFFUL <<  0U)

/****** FLASH bank ECC fail address (FLASH_ECC_FAR) ***************************/
#define CHIP_REGFLD_FLASH_ECC_FAR_FAIL_ECC_ADDR     (0x00007FFFUL <<  0U)

/****** FLASH option status register 2 (FLASH_OPTSR2_CUR/PGR) *********************/
#if CHIP_DEV_SUPPORT_ITCM_AXI_SHARE
#define CHIP_REGFLD_FLASH_OPTSR2_TCM_AXI_SHARED     (0x00000003UL <<  0U) /* TCM RAM sharing */
#endif
#if CHIP_DEV_SUPPORT_CPUFREQ_BOOST
#define CHIP_REGFLD_FLASH_OPTSR2_CPUFREQ_BOOST      (0x00000001UL <<  2U) /* CPU frequency boost */
#endif

#if CHIP_DEV_SUPPORT_ITCM_AXI_SHARE
#define CHIP_REGFLDVAL_FLASH_OPTSR2_TCM_AXI_SHARED_64_320    0 /* 64-Kbyte ITCM / 320 Kbyte system AXI */
#define CHIP_REGFLDVAL_FLASH_OPTSR2_TCM_AXI_SHARED_128_256   1 /* 128-Kbyte ITCM / 256-Kbyte system AXI */
#define CHIP_REGFLDVAL_FLASH_OPTSR2_TCM_AXI_SHARED_192_192   2 /* 192-Kbyte ITCM / 192-Kbyte system AXI */
#define CHIP_REGFLDVAL_FLASH_OPTSR2_TCM_AXI_SHARED_256_128   3 /* 256-Kbyte ITCM / 128-Kbyte system AXI */
#endif
#endif // CHIP_STM32H7XX_REGS_FLASH_H_
