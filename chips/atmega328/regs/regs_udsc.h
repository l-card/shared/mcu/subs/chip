#ifndef REGS_UDSC_H
#define REGS_UDSC_H

#include "chip_devtype_spec_features.h"

#ifdef CHIP_LGT8FX8P
#define CHIP_REG_DSCR       _MMIO_BYTE(0x20) /* uDSC Control and Status Register */
#define CHIP_REG_DSIR       _MMIO_BYTE(0x21) /* Operation instruction register */
#define CHIP_REG_DSSD       _MMIO_BYTE(0x22) /* 16-bit saturation result of accumulator DSA */
#define CHIP_REG_DSDX       _MMIO_BYTE(0x30) /* Operand DSDX, 16-bit read and write access */
#define CHIP_REG_DSDY       _MMIO_BYTE(0x31) /* Operand DSDY, 16-bit read and write access */
#define CHIP_REG_DSAL       _MMIO_BYTE(0x58) /* 32-bit accumulator DSA[15:0], 16-bit read and write access */
#define CHIP_REG_DSAH       _MMIO_BYTE(0x59) /* 32-bit accumulator DSA[31:16], 16-bit read and write access */

/* ---------------- DSCR – uDSC Control and Status Register ------------------*/
#define CHIP_REGFLD_DSCR_DSUEN              (0x01U << 7) /* (RW) uDSC module enable */
#define CHIP_REGFLD_DSCR_MM                 (0x01U << 6) /* (RW) uDSC register mapping mode */
#define CHIP_REGFLD_DSCR_D1                 (0x01U << 5) /* (RW) Divide operation complete */
#define CHIP_REGFLD_DSCR_D0                 (0x01U << 4) /* (RW) Divide by 0 flag */
#define CHIP_REGFLD_DSCR_N                  (0x01U << 2) /* (RW) The result of the operation is a negative number */
#define CHIP_REGFLD_DSCR_Z                  (0x01U << 1) /* (RW) The result of the operation is zero */
#define CHIP_REGFLD_DSCR_C                  (0x01U << 0) /* (RW) 32-bit addition carry/borrow  */

#define CHIP_REGFLDVAL_DSCR_MM_FAST         0
#define CHIP_REGFLDVAL_DSCR_MM_IO           1

#endif

#endif // REGS_UDSC_H
