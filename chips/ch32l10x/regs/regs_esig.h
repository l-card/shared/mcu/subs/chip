#ifndef REGS_ESIG_H
#define REGS_ESIG_H

#include "chip_ioreg_defs.h"

typedef struct {
    __I uint16_t FLASH_DENSITY;
    uint16_t Reserved1;
    uint32_t Reserved2;
    union {
        __I uint8_t  UIDB[12];
        __I uint32_t UIDW[3];
    };
} CHIP_REGS_ESIG_T;

#define CHIP_REGS_ESIG                  ((CHIP_REGS_ESIG_T *) CHIP_MEMRGN_ADDR_ESIG)


#endif // REGS_ESIG_H
