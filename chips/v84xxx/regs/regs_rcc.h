#ifndef CHIP_V84XXX_REGS_RCC_H_
#define CHIP_V84XXX_REGS_RCC_H_

#include <stdint.h>

/* Reset and Clock Control */
typedef struct {
    __IO uint32_t CTRL;             /* RCC clock control register,                      Address offset: 0x00  */
    __IO uint32_t CFG;              /* RCC clock configuration register,                Address offset: 0x04  */
    __IO uint32_t CLKINT;           /* RCC clock interrupt register,                    Address offset: 0x08  */
    __IO uint32_t APB2RST;          /* RCC APB2 peripheral reset register,              Address offset: 0x0C  */
    __IO uint32_t APB1RST;          /* RCC APB1 peripheral reset register,              Address offset: 0x10  */
    __IO uint32_t AHBEN;            /* RCC AHB peripheral clock enable register,        Address offset: 0x14  */
    __IO uint32_t APB2EN;           /* RCC APB2 peripheral clock enable register,       Address offset: 0x18  */
    __IO uint32_t APB1EN;           /* RCC APB1 peripheral clock enable register,       Address offset: 0x1C  */
    __IO uint32_t BDC;              /* RCC backup domain control  register,             Address offset: 0x20  */
    __IO uint32_t CTRLSTS;          /* RCC control/statuss register,                    Address offset: 0x24  */
    uint32_t RESERVED1[2];
    __IO uint32_t MISC;             /* RCC additional register,                         Address offset: 0x30  */
    uint32_t RESERVED2[7];
    __IO uint32_t TEST;             /* RCC test register,                               Address offset: 0x50  */
    __IO uint32_t MISC2;            /* RCC additional register 2                        Address offset: 0x54  */
    uint32_t RESERVED3;
    __IO uint32_t INTCTRL;          /* RCC interrupt control                            Address offset: 0x5C  */
} CHIP_REGS_RCC_T;


#define CHIP_REGS_RCC          ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)

/*********** RCC clock control register (RCC_CTRL) ****************************/
#define CHIP_REGFLD_RCC_CTRL_PLL_STBL               (0x00000001UL << 25U) /* (RO) PLL clock ready flag */
#define CHIP_REGFLD_RCC_CTRL_PLL_EN                 (0x00000001UL << 24U) /* (RW) PLL enable */
#define CHIP_REGFLD_RCC_CTRL_CFD_EN                 (0x00000001UL << 19U) /* (RW) Clock Failure Detection enable */
#define CHIP_REGFLD_RCC_CTRL_HSE_BYPS               (0x00000001UL << 18U) /* (RW) HSE clock bypass  */
#define CHIP_REGFLD_RCC_CTRL_HSE_STBL               (0x00000001UL << 17U) /* (RO) HSE clock ready flag  */
#define CHIP_REGFLD_RCC_CTRL_HSE_EN                 (0x00000001UL << 16U) /* (RW) HSE clock enable  */
#define CHIP_REGFLD_RCC_CTRL_HSI_CAL                (0x000000FFUL <<  8U) /* (RW) HSI clock calibration */
#define CHIP_REGFLD_RCC_CTRL_HSI_TWK                (0x0000003FUL <<  2U) /* (RW) HSI clock trimming */
#define CHIP_REGFLD_RCC_CTRL_HSI_STBL               (0x00000001UL <<  1U) /* (RW) HSI clock ready flag */
#define CHIP_REGFLD_RCC_CTRL_HSI_EN                 (0x00000001UL <<  1U) /* (RW) HSI clock enable */
#define CHIP_REGMSK_RCC_CTRL_RESERVED               (0xFCF00000UL)

/*********** RCC clock configuration register (RCC_CFG) ***********************/
#define CHIP_REGFLD_RCC_CFG_PLL_RANGE               (0x00000001UL << 31U) /* (RW) PLL clock output range  */
#define CHIP_REGFLD_RCC_CFG_PLLMUL_H                (0x00000003UL << 29U) /* (RW) PLL multiplication factor [5..4] */
#define CHIP_REGFLD_RCC_CFG_ADC_PSC_H               (0x00000001UL << 28U) /* (RW) ADC prescaler [2] */
#define CHIP_REGFLD_RCC_CFG_USB_PSC_H               (0x00000001UL << 27U) /* (RW) USB Prescaler [2] */
#define CHIP_REGFLD_RCC_CFG_CLKOUT_L                (0x00000007UL << 24U) /* (RW) CLKOUT[2..0] (CLKOUT3 - _RCC_MISC.16)  */
#define CHIP_REGFLD_RCC_CFG_USB_PSC_L               (0x00000003UL << 22U) /* (RW) USB Prescaler [1..0] */
#define CHIP_REGFLD_RCC_CFG_PLLMUL_L                (0x0000000FUL << 18U) /* (RW) PLL multiplication factor [3..0] */
#define CHIP_REGFLD_RCC_CFG_PLL_HSE_PSC             (0x00000001UL << 17U) /* (RW) HSE divider for PLL entry */
#define CHIP_REGFLD_RCC_CFG_PLL_RC                  (0x00000001UL << 16U) /* (RW) PLL entry clock source */
#define CHIP_REGFLD_RCC_CFG_ADC_PSC_L               (0x00000003UL << 14U) /* (RW) ADC prescaler [1..0] */
#define CHIP_REGFLD_RCC_CFG_APB2_PSC                (0x00000007UL << 11U) /* (RW) APB2 prescaler */
#define CHIP_REGFLD_RCC_CFG_APB1_PSC                (0x00000007UL <<  8U) /* (RW) APB1 prescaler */
#define CHIP_REGFLD_RCC_CFG_AHB_PSC                 (0x0000000FUL <<  4U) /* (RW) AHB prescaler */
#define CHIP_REGFLD_RCC_CFG_SYSCLK_STS              (0x00000003UL <<  2U) /* (RO) System clock status */
#define CHIP_REGFLD_RCC_CFG_SYSCLK_SEL              (0x00000003UL <<  0U) /* (RW) System clock select */


#define CHIP_REGFLDVAL_RCC_CFG_ADC_PSC_2            0
#define CHIP_REGFLDVAL_RCC_CFG_ADC_PSC_4            1
#define CHIP_REGFLDVAL_RCC_CFG_ADC_PSC_6            2
#define CHIP_REGFLDVAL_RCC_CFG_ADC_PSC_8            3
#define CHIP_REGFLDVAL_RCC_CFG_ADC_PSC_12           5
#define CHIP_REGFLDVAL_RCC_CFG_ADC_PSC_16           7

#define CHIP_REGFLDVAL_RCC_CFG_APB_PSC_1            0
#define CHIP_REGFLDVAL_RCC_CFG_APB_PSC_2            4
#define CHIP_REGFLDVAL_RCC_CFG_APB_PSC_4            5
#define CHIP_REGFLDVAL_RCC_CFG_APB_PSC_8            6
#define CHIP_REGFLDVAL_RCC_CFG_APB_PSC_16           7

#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_1            0
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_2            8
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_4            9
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_8            10
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_16           11
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_64           12
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_128          13
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_256          14
#define CHIP_REGFLDVAL_RCC_CFG_AHB_PSC_512          15


#define CHIP_REGFLDVAL_RCC_CFG_SYSCLK_HSI           0
#define CHIP_REGFLDVAL_RCC_CFG_SYSCLK_HSE           1
#define CHIP_REGFLDVAL_RCC_CFG_SYSCLK_PLLOUT        2
#define CHIP_REGFLDVAL_RCC_CFG_SYSCLK_DIS           3

/*********** RCC clock interrupt register (RCC_CLKINT) ************************/
#define CHIP_REGFLD_RCC_CLKINT_CFD_FC               (0x00000001UL << 23U) /* (W1C) Clock failure detection interrupt clear  */
#define CHIP_REGFLD_RCC_CLKINT_PLL_STBL_FC          (0x00000001UL << 20U) /* (W1C) PLL ready interrupt clear  */
#define CHIP_REGFLD_RCC_CLKINT_HSE_STBL_FC          (0x00000001UL << 19U) /* (W1C) HSE ready interrupt clear  */
#define CHIP_REGFLD_RCC_CLKINT_HSI_STBL_FC          (0x00000001UL << 18U) /* (W1C) HSI ready interrupt clear  */
#define CHIP_REGFLD_RCC_CLKINT_LSE_STBL_FC          (0x00000001UL << 17U) /* (W1C) LSE ready interrupt clear  */
#define CHIP_REGFLD_RCC_CLKINT_LSI_STBL_FC          (0x00000001UL << 16U) /* (W1C) LSI ready interrupt clear  */
#define CHIP_REGFLD_RCC_CLKINT_PLL_STBL_IE          (0x00000001UL << 12U) /* (RW)  PLL ready interrupt enable  */
#define CHIP_REGFLD_RCC_CLKINT_HSE_STBL_IE          (0x00000001UL << 11U) /* (RW)  HSE ready interrupt enable  */
#define CHIP_REGFLD_RCC_CLKINT_HSI_STBL_IE          (0x00000001UL << 10U) /* (RW)  HSI ready interrupt enable  */
#define CHIP_REGFLD_RCC_CLKINT_LSE_STBL_IE          (0x00000001UL <<  9U) /* (RW)  LSE ready interrupt enable  */
#define CHIP_REGFLD_RCC_CLKINT_LSI_STBL_IE          (0x00000001UL <<  8U) /* (RW)  LSI ready interrupt enable  */
#define CHIP_REGFLD_RCC_CLKINT_CFD_F                (0x00000001UL <<  7U) /* (RO)  Clock failure detection interrupt flag  */
#define CHIP_REGFLD_RCC_CLKINT_PLL_STBL_F           (0x00000001UL <<  4U) /* (RO)  PLL ready interrupt flag  */
#define CHIP_REGFLD_RCC_CLKINT_HSE_STBL_F           (0x00000001UL <<  3U) /* (RO)  HSE ready interrupt flag  */
#define CHIP_REGFLD_RCC_CLKINT_HSI_STBL_F           (0x00000001UL <<  2U) /* (RO)  HSI ready interrupt flag  */
#define CHIP_REGFLD_RCC_CLKINT_LSE_STBL_F           (0x00000001UL <<  1U) /* (RO)  LSE ready interrupt flag  */
#define CHIP_REGFLD_RCC_CLKINT_LSI_STBL_F           (0x00000001UL <<  0U) /* (RO)  LSI ready interrupt flag  */
#define CHIP_REGMSK_RCC_CLKINT_RESERVED             (0xFF601060)

#define CHIP_RCC_CLKINT_NUM_CFD                     7
#define CHIP_RCC_CLKINT_NUM_PLL_STB                 3
#define CHIP_RCC_CLKINT_NUM_HSE_STB                 3
#define CHIP_RCC_CLKINT_NUM_HSI_STB                 2
#define CHIP_RCC_CLKINT_NUM_LSE_STB                 1
#define CHIP_RCC_CLKINT_NUM_LSI_STB                 0

#define CHIP_REGFLD_RCC_CLKINT_F(intnum)            (0x00000001UL <<  (intnum))         /* (RO) interrupt (intnum) flag */
#define CHIP_REGFLD_RCC_CLKINT_IE(intnum)           (0x00000001UL <<  ((intnum) + 8))   /* (RW) interrupt (intnum) enable */
#define CHIP_REGFLD_RCC_CLKINT_FC(intnum)           (0x00000001UL <<  ((intnum) + 16))  /* (W1C) interrupt (intnum) clear */


/*********** RCC peripheral reset register (RCC_APB2RST) ***********************/
#define CHIP_REGFLD_RCC_APB2RST_UART8_RST           (0x00000001UL << 26U) /* (RW)  UART8 reset */
#define CHIP_REGFLD_RCC_APB2RST_UART7_RST           (0x00000001UL << 25U) /* (RW)  UART7 reset */
#define CHIP_REGFLD_RCC_APB2RST_USART6_RST          (0x00000001UL << 24U) /* (RW)  USART6 reset */
#define CHIP_REGFLD_RCC_APB2RST_TMR11_RST           (0x00000001UL << 21U) /* (RW)  TMR11 reset */
#define CHIP_REGFLD_RCC_APB2RST_TMR10_RST           (0x00000001UL << 20U) /* (RW)  TMR10 reset */
#define CHIP_REGFLD_RCC_APB2RST_TMR9_RST            (0x00000001UL << 19U) /* (RW)  TMR9 reset */
#define CHIP_REGFLD_RCC_APB2RST_USART1_RST          (0x00000001UL << 14U) /* (RW)  USART1 reset */
#define CHIP_REGFLD_RCC_APB2RST_TMR8_RST            (0x00000001UL << 13U) /* (RW)  TMR8 reset */
#define CHIP_REGFLD_RCC_APB2RST_SPI1_RST            (0x00000001UL << 12U) /* (RW)  SPI1 reset */
#define CHIP_REGFLD_RCC_APB2RST_TMR1_RST            (0x00000001UL << 11U) /* (RW)  TMR1 reset */
#define CHIP_REGFLD_RCC_APB2RST_ADC1_RST            (0x00000001UL <<  9U) /* (RW)  ADC1 reset */
#define CHIP_REGFLD_RCC_APB2RST_GPIOE_RST           (0x00000001UL <<  6U) /* (RW)  GPIOE reset */
#define CHIP_REGFLD_RCC_APB2RST_GPIOD_RST           (0x00000001UL <<  5U) /* (RW)  GPIOD reset */
#define CHIP_REGFLD_RCC_APB2RST_GPIOC_RST           (0x00000001UL <<  4U) /* (RW)  GPIOC reset */
#define CHIP_REGFLD_RCC_APB2RST_GPIOB_RST           (0x00000001UL <<  3U) /* (RW)  GPIOB reset */
#define CHIP_REGFLD_RCC_APB2RST_GPIOA_RST           (0x00000001UL <<  2U) /* (RW)  GPIOA reset */
#define CHIP_REGFLD_RCC_APB2RST_EXTI_RST            (0x00000001UL <<  1U) /* (RW)  EXTI reset */
#define CHIP_REGFLD_RCC_APB2RST_AFIO_RST            (0x00000001UL <<  0U) /* (RW)  Alternate function I/0 reset */
#define CHIP_REGMSK_RCC_APB2RST_RESERVED            (0xF8C78580UL)

/*********** RCC peripheral reset register (RCC_APB1RST) ***********************/
#define CHIP_REGFLD_RCC_APB1RST_PWR_RST             (0x00000001UL << 28U) /* (RW)  Power interface reset */
#define CHIP_REGFLD_RCC_APB1RST_BKP_RST             (0x00000001UL << 27U) /* (RW)  Backup interface reset */
#define CHIP_REGFLD_RCC_APB1RST_I2C3_RST            (0x00000001UL << 26U) /* (RW)  I2C3 reset */
#define CHIP_REGFLD_RCC_APB1RST_CAN_RST             (0x00000001UL << 25U) /* (RW)  CAN1 reset */
#define CHIP_REGFLD_RCC_APB1RST_USB_RST             (0x00000001UL << 23U) /* (RW)  USB reset */
#define CHIP_REGFLD_RCC_APB1RST_I2C2_RST            (0x00000001UL << 22U) /* (RW)  I2C2 reset */
#define CHIP_REGFLD_RCC_APB1RST_I2C1_RST            (0x00000001UL << 21U) /* (RW)  I2C1 reset */
#define CHIP_REGFLD_RCC_APB1RST_UART5_RST           (0x00000001UL << 20U) /* (RW)  UART5 reset */
#define CHIP_REGFLD_RCC_APB1RST_UART4_RST           (0x00000001UL << 19U) /* (RW)  UART4 reset */
#define CHIP_REGFLD_RCC_APB1RST_USART3_RST          (0x00000001UL << 18U) /* (RW)  USART3 reset */
#define CHIP_REGFLD_RCC_APB1RST_USART2_RST          (0x00000001UL << 17U) /* (RW)  USART2 reset */
#define CHIP_REGFLD_RCC_APB1RST_SPI4_RST            (0x00000001UL << 16U) /* (RW)  SPI4 reset */
#define CHIP_REGFLD_RCC_APB1RST_SPI3_RST            (0x00000001UL << 15U) /* (RW)  SPI3 reset */
#define CHIP_REGFLD_RCC_APB1RST_SPI2_RST            (0x00000001UL << 14U) /* (RW)  SPI2 reset */
#define CHIP_REGFLD_RCC_APB1RST_WWDG_RST            (0x00000001UL << 11U) /* (RW)  WWDG reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR14_RST           (0x00000001UL <<  8U) /* (RW)  TMR14 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR13_RST           (0x00000001UL <<  7U) /* (RW)  TMR13 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR12_RST           (0x00000001UL <<  6U) /* (RW)  TMR12 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR7_RST            (0x00000001UL <<  5U) /* (RW)  TMR7 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR6_RST            (0x00000001UL <<  4U) /* (RW)  TMR6 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR5_RST            (0x00000001UL <<  3U) /* (RW)  TMR5 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR4_RST            (0x00000001UL <<  2U) /* (RW)  TMR4 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR3_RST            (0x00000001UL <<  1U) /* (RW)  TMR3 reset */
#define CHIP_REGFLD_RCC_APB1RST_TMR2_RST            (0x00000001UL <<  0U) /* (RW)  TMR2 reset */
#define CHIP_REGMSK_RCC_APB1RST_RESERVED            (0xE1003600UL)

/*********** RCC AHB peripheral clock enable register (RCC_AHBEN) *************/
#define CHIP_REGFLD_RCC_AHBEN_XMC_EN                (0x00000001UL <<  8U) /* (RW) XMC clock enable */
#define CHIP_REGFLD_RCC_AHBEN_CRC_EN                (0x00000001UL <<  6U) /* (RW) CRC clock enable */
#define CHIP_REGFLD_RCC_AHBEN_FLASH_EN              (0x00000001UL <<  4U) /* (RW) FLASH clock enable */
#define CHIP_REGFLD_RCC_AHBEN_SRAM_EN               (0x00000001UL <<  2U) /* (RW) SRAM clock enable */
#define CHIP_REGFLD_RCC_AHBEN_DMA2_EN               (0x00000001UL <<  1U) /* (RW) DMA2 clock enable */
#define CHIP_REGFLD_RCC_AHBEN_DMA1_EN               (0x00000001UL <<  0U) /* (RW) DMA1 clock enable */
#define CHIP_REGMSK_RCC_AHBEN_RESERVED              (0xFFFFFEA8UL)

/*********** RCC peripheral clock enable  register (RCC_APB2EN) ***************/
#define CHIP_REGFLD_RCC_APB2EN_UART8_EN             (0x00000001UL << 26U) /* (RW)  UART8 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_UART7_EN             (0x00000001UL << 25U) /* (RW)  UART7 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_USART6_EN            (0x00000001UL << 24U) /* (RW)  USART6 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_TMR11_EN             (0x00000001UL << 21U) /* (RW)  TMR11 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_TMR10_EN             (0x00000001UL << 20U) /* (RW)  TMR10 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_TMR9_EN              (0x00000001UL << 19U) /* (RW)  TMR9 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_USART1_EN            (0x00000001UL << 14U) /* (RW)  USART1 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_TMR8_EN              (0x00000001UL << 13U) /* (RW)  TMR8 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_SPI1_EN              (0x00000001UL << 12U) /* (RW)  SPI1 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_TMR1_EN              (0x00000001UL << 11U) /* (RW)  TMR1 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_ADC1_EN              (0x00000001UL <<  9U) /* (RW)  ADC1 clock enable */
#define CHIP_REGFLD_RCC_APB2EN_GPIOE_EN             (0x00000001UL <<  6U) /* (RW)  GPIOE clock enable */
#define CHIP_REGFLD_RCC_APB2EN_GPIOD_EN             (0x00000001UL <<  5U) /* (RW)  GPIOD clock enable */
#define CHIP_REGFLD_RCC_APB2EN_GPIOC_EN             (0x00000001UL <<  4U) /* (RW)  GPIOC clock enable */
#define CHIP_REGFLD_RCC_APB2EN_GPIOB_EN             (0x00000001UL <<  3U) /* (RW)  GPIOB clock enable */
#define CHIP_REGFLD_RCC_APB2EN_GPIOA_EN             (0x00000001UL <<  2U) /* (RW)  GPIOA clock enable */
#define CHIP_REGFLD_RCC_APB2EN_AFIO_EN              (0x00000001UL <<  0U) /* (RW)  Alternate function I/0 clock enable */
#define CHIP_REGMSK_RCC_APB2EN_RESERVED             (0xF8C78582UL)

/*********** RCC peripheral reset register (RCC_APB1EN) ***********************/
#define CHIP_REGFLD_RCC_APB1EN_PWR_EN               (0x00000001UL << 28U) /* (RW)  Power interface clock enable */
#define CHIP_REGFLD_RCC_APB1EN_BKP_EN               (0x00000001UL << 27U) /* (RW)  Backup interface clock enable */
#define CHIP_REGFLD_RCC_APB1EN_I2C3_EN              (0x00000001UL << 26U) /* (RW)  I2C3 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_CAN_EN               (0x00000001UL << 25U) /* (RW)  CAN1 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_USB_EN               (0x00000001UL << 23U) /* (RW)  USB clock enable */
#define CHIP_REGFLD_RCC_APB1EN_I2C2_EN              (0x00000001UL << 22U) /* (RW)  I2C2 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_I2C1_EN              (0x00000001UL << 21U) /* (RW)  I2C1 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_UART5_EN             (0x00000001UL << 20U) /* (RW)  UART5 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_UART4_EN             (0x00000001UL << 19U) /* (RW)  UART4 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_USART3_EN            (0x00000001UL << 18U) /* (RW)  USART3 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_USART2_EN            (0x00000001UL << 17U) /* (RW)  USART2 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_SPI4_EN              (0x00000001UL << 16U) /* (RW)  SPI4 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_SPI3_EN              (0x00000001UL << 15U) /* (RW)  SPI3 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_SPI2_EN              (0x00000001UL << 14U) /* (RW)  SPI2 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_WWDG_EN              (0x00000001UL << 11U) /* (RW)  WWDG clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR14_EN             (0x00000001UL <<  8U) /* (RW)  TMR14 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR13_EN             (0x00000001UL <<  7U) /* (RW)  TMR13 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR12_EN             (0x00000001UL <<  6U) /* (RW)  TMR12 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR7_EN              (0x00000001UL <<  5U) /* (RW)  TMR7 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR6_EN              (0x00000001UL <<  4U) /* (RW)  TMR6 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR5_EN              (0x00000001UL <<  3U) /* (RW)  TMR5 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR4_EN              (0x00000001UL <<  2U) /* (RW)  TMR4 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR3_EN              (0x00000001UL <<  1U) /* (RW)  TMR3 clock enable */
#define CHIP_REGFLD_RCC_APB1EN_TMR2_EN              (0x00000001UL <<  0U) /* (RW)  TMR2 clock enable */
#define CHIP_REGMSK_RCC_APB1EN_RESERVED             (0xE1003600UL)

/*********** RCC backup domain control register (RCC_BDC) *********************/
#define CHIP_REGFLD_RCC_BDC_BD_RST                  (0x00000001UL << 16U) /* (RW)  Backup domain software reset */
#define CHIP_REGFLD_RCC_BDC_RTC_EN                  (0x00000001UL << 15U) /* (RW)  RTC clock enable */
#define CHIP_REGFLD_RCC_BDC_RTC_SEL                 (0x00000003UL <<  8U) /* (RW)  RTC clock source selection */
#define CHIP_REGFLD_RCC_BDC_LSE_BYPS                (0x00000001UL <<  2U) /* (RW)  LSE oscillator bypass */
#define CHIP_REGFLD_RCC_BDC_LSE_STBL                (0x00000001UL <<  1U) /* (RO)  LSE ready */
#define CHIP_REGFLD_RCC_BDC_LSE_EN                  (0x00000001UL <<  0U) /* (RW)  LSE enable */
#define CHIP_REGMSK_RCC_BDC_RESERVED                (0xFFFE7CF8UL)

/*********** RCC control/statuss register (RCC_CTRLSTS) ***********************/
#define CHIP_REGFLD_RCC_CTRLSTS_LP_RST_F            (0x00000001UL << 31U) /* (RW) Low-power reset flag */
#define CHIP_REGFLD_RCC_CTRLSTS_WWDG_RST_F          (0x00000001UL << 30U) /* (RW) WWDG reset flag */
#define CHIP_REGFLD_RCC_CTRLSTS_IWDG_RST_F          (0x00000001UL << 29U) /* (RW) IWDG reset flag */
#define CHIP_REGFLD_RCC_CTRLSTS_SW_RST_F            (0x00000001UL << 28U) /* (RW) software reset flag */
#define CHIP_REGFLD_RCC_CTRLSTS_POR_RST_F           (0x00000001UL << 27U) /* (RW) POR/PDR reset flag */
#define CHIP_REGFLD_RCC_CTRLSTS_PIN_RST_F           (0x00000001UL << 26U) /* (RW) NRST pin reset flag */
#define CHIP_REGFLD_RCC_CTRLSTS_RST_FC              (0x00000001UL << 24U) /* (RW) Reset flag clear */
#define CHIP_REGFLD_RCC_CTRLSTS_LSI_STBL            (0x00000001UL <<  1U) /* (RO) LSI ready */
#define CHIP_REGFLD_RCC_CTRLSTS_LSI_EN              (0x00000001UL <<  0U) /* (RW) LSI enable */
#define CHIP_REGMSK_RCC_CTRLSTS_RESERVED            (0x027FFFFCUL)

/*********** RCC additional register (RCC_MISC) *******************************/
#define CHIP_REGFLD_RCC_MISC_MCO_PRE                (0x0000000FUL << 28U) /* (RW) MCO orescaler */
#define CHIP_REGFLD_RCC_MISC_HSI_DIV_EN             (0x00000001UL << 25U) /* (RW) HSI 6 divider enable */
#define CHIP_REGFLD_RCC_MISC_USB_768_BUF            (0x00000001UL << 24U) /* (RW) USB buffer size 768 ~ 1280 enable */
#define CHIP_REGFLD_RCC_MISC_CLKOUT_H               (0x00000001UL << 16U) /* (RW) CLKOUT[3] */
#define CHIP_REGFLD_RCC_MISC_HSICAL_KEY             (0x000000FFUL <<  0U) /* (RW) HSI calibration written value */
#define CHIP_REGMSK_RCC_MISC_RESERVED               (0x0CFEFF00UL)

#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_1           0
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_2           8
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_4           9
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_8           10
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_16          11
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_64          12
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_128         13
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_256         14
#define CHIP_REGFLDVAL_RCC_MISC_MCO_PRE_512         15

#define CHIP_REGFLDVAL_RCC_MISC_HSICAL_KEY          0x5A

/*********** RCC test register (RCC_TEST) *************************************/
#define CHIP_REGFLD_RCC_TEST_MCO2TMR_EN             (0x00000001UL << 16U) /* (RW) CLKOUT to Timer 10 CH0 enable */
#define CHIP_REGMSK_RCC_TEST_RESERVED               (0xFFFEFFFFUL)

/*********** RCC additional register (RCC_MISC2) *******************************/
#define CHIP_REGFLD_RCC_MISC2_HSE_DIV_CTRL          (0x00000003UL << 12U) /* (RW) HSE clock divider control */
#define CHIP_REGFLD_RCC_MISC2_HSI_SYS_CTRL          (0x00000001UL <<  9U) /* (RW) HSI as system clock frequency select */
#define CHIP_REGFLD_RCC_MISC2_AUTO_STEP_EN          (0x00000003UL <<  4U) /* (RW) Auto step clock switch enable */
#define CHIP_REGMSK_RCC_MISC2_RESERVED              (0xFFFFCDCFUL)


#define CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_2     0
#define CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_3     1
#define CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_4     2
#define CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_5     3

#define CHIP_REGFLDVAL_RCC_MISC2_AUTO_STEP_DIS      0
#define CHIP_REGFLDVAL_RCC_MISC2_AUTO_STEP_EN       3

/*********** RCC interrupt control register (RCC_INTCTRL) *********************/
#define CHIP_REGFLD_RCC_INTCTRL_USB_INT_CTRL        (0x00000001UL <<  0U) /* (RW) USBDEV interrupt remapping control (from 19/20 to 73/74) */
#define CHIP_REGMSK_RCC_INTCTRL_RESERVED            (0xFFFFFFFEUL)

#endif // CHIP_V84XXX_REGS_RCC_H_
