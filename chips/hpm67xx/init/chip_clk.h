#ifndef CHIP_CLK_H
#define CHIP_CLK_H

#define CHIP_CLK_UART0_FREQ                 CHIP_MHZ(80)
#define CHIP_CLK_MCHTMR_FREQ                CHIP_MHZ(1)


#endif // CHIP_CLK_H
