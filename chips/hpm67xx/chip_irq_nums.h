#ifndef CHIP_IRQ_NUMS_H
#define CHIP_IRQ_NUMS_H

#define CHIP_IRQNUM_GPIO0_A             1   /* GPIO Controller 0 Port A Interrupt */
#define CHIP_IRQNUM_GPIO0_B             2   /* GPIO Controller 0 Port B Interrupt */
#define CHIP_IRQNUM_GPIO0_C             3   /* GPIO Controller 0 Port C Interrupt */
#define CHIP_IRQNUM_GPIO0_D             4   /* GPIO Controller 0 Port D Interrupt */
#define CHIP_IRQNUM_GPIO0_E             5   /* GPIO Controller 0 Port E Interrupt */
#define CHIP_IRQNUM_GPIO0_F             6   /* GPIO Controller 0 Port F Interrupt */
#define CHIP_IRQNUM_GPIO0_X             7   /* GPIO Controller 0 Port X Interrupt */
#define CHIP_IRQNUM_GPIO0_Y             8   /* GPIO Controller 0 Port Y Interrupt */
#define CHIP_IRQNUM_GPIO0_Z             9   /* GPIO Controller 0 Port Z Interrupt */
#define CHIP_IRQNUM_GPIO1_A             10  /* GPIO Controller 1 Port A Interrupt */
#define CHIP_IRQNUM_GPIO1_B             11  /* GPIO Controller 1 Port B Interrupt */
#define CHIP_IRQNUM_GPIO1_C             12  /* GPIO Controller 1 Port C Interrupt */
#define CHIP_IRQNUM_GPIO1_D             13  /* GPIO Controller 1 Port D Interrupt */
#define CHIP_IRQNUM_GPIO1_E             14  /* GPIO Controller 1 Port E Interrupt */
#define CHIP_IRQNUM_GPIO1_F             15  /* GPIO Controller 1 Port F Interrupt */
#define CHIP_IRQNUM_GPIO1_X             16  /* GPIO Controller 1 Port X Interrupt */
#define CHIP_IRQNUM_GPIO1_Y             17  /* GPIO Controller 1 Port Y Interrupt */
#define CHIP_IRQNUM_GPIO1_Z             18  /* GPIO Controller 1 Port Z Interrupt */
#define CHIP_IRQNUM_ADC0                19  /* Analog-to-digital converter ADC0 interrupt */
#define CHIP_IRQNUM_ADC1                20  /* Analog-to-digital converter ADC1 interrupt */
#define CHIP_IRQNUM_ADC2                21  /* Analog-to-digital converter ADC2 interrupt */
#define CHIP_IRQNUM_ADC3                22  /* Analog-to-digital converter ADC3 interrupt */
#define CHIP_IRQNUM_ACMP0               23  /* Comparator ACMP0 interrupt */
#define CHIP_IRQNUM_ACMP1               24  /* Comparator ACMP1 interrupt */
#define CHIP_IRQNUM_ACMP2               25  /* Comparator ACMP2 interrupt */
#define CHIP_IRQNUM_ACMP3               26  /* Comparator ACMP3 interrupt */
#define CHIP_IRQNUM_SPI0                27  /* Serial Peripheral Bus SPI0 Interrupt */
#define CHIP_IRQNUM_SPI1                28  /* Serial Peripheral Bus SPI1 Interrupt */
#define CHIP_IRQNUM_SPI2                29  /* Serial Peripheral Bus SPI2 Interrupt */
#define CHIP_IRQNUM_SPI3                30  /* Serial Peripheral Bus SPI3 Interrupt */
#define CHIP_IRQNUM_UART0               31  /* Universal Asynchronous Receiver-Transmitter UART0 Interrupt */
#define CHIP_IRQNUM_UART1               32  /* Universal Asynchronous Receiver-Transmitter UART1 Interrupt */
#define CHIP_IRQNUM_UART2               33  /* Universal Asynchronous Receiver-Transmitter UART2 Interrupt */
#define CHIP_IRQNUM_UART3               34  /* Universal Asynchronous Receiver-Transmitter UART3 Interrupt */
#define CHIP_IRQNUM_UART4               35  /* Universal Asynchronous Receiver-Transmitter UART4 Interrupt */
#define CHIP_IRQNUM_UART5               36  /* Universal Asynchronous Receiver-Transmitter UART5 Interrupt */
#define CHIP_IRQNUM_UART6               37  /* Universal Asynchronous Receiver-Transmitter UART6 Interrupt */
#define CHIP_IRQNUM_UART7               38  /* Universal Asynchronous Receiver-Transmitter UART7 Interrupt */
#define CHIP_IRQNUM_UART8               39  /* Universal Asynchronous Receiver-Transmitter UART8 Interrupt */
#define CHIP_IRQNUM_UART9               40  /* Universal Asynchronous Receiver-Transmitter UART9 Interrupt */
#define CHIP_IRQNUM_UART10              41  /* Universal Asynchronous Receiver-Transmitter UART10 Interrupt */
#define CHIP_IRQNUM_UART11              42  /* Universal Asynchronous Receiver-Transmitter UART11 Interrupt */
#define CHIP_IRQNUM_UART12              43  /* Universal Asynchronous Receiver-Transmitter UART12 Interrupt */
#define CHIP_IRQNUM_UART13              44  /* Universal Asynchronous Receiver-Transmitter UART13 Interrupt */
#define CHIP_IRQNUM_UART14              45  /* Universal Asynchronous Receiver-Transmitter UART14 Interrupt */
#define CHIP_IRQNUM_UART15              46  /* Universal Asynchronous Receiver-Transmitter UART15 Interrupt */
#define CHIP_IRQNUM_CAN0                47  /* Controller Area Network CAN0 Interrupt */
#define CHIP_IRQNUM_CAN1                48  /* Controller Area Network CAN1 Interrupt */
#define CHIP_IRQNUM_CAN2                49  /* Controller Area Network CAN2 Interrupt */
#define CHIP_IRQNUM_CAN3                50  /* Controller Area Network CAN3 Interrupt */
#define CHIP_IRQNUM_PTPC                51  /* High-precision time synchronization protocol controller PTPC interrupt */
#define CHIP_IRQNUM_WDG0                52  /* Watchdog WDOG0 interrupt */
#define CHIP_IRQNUM_WDG1                53  /* Watchdog WDOG1 interrupt */
#define CHIP_IRQNUM_WDG2                54  /* Watchdog WDOG2 interrupt */
#define CHIP_IRQNUM_WDG3                55  /* Watchdog WDOG3 interrupt */
#define CHIP_IRQNUM_MBX0A               56  /* Mailbox MBX0A Interrupt */
#define CHIP_IRQNUM_MBX0B               57  /* Mailbox MBX0B Interrupt */
#define CHIP_IRQNUM_MBX1A               58  /* Mailbox MBX1A Interrupt */
#define CHIP_IRQNUM_MBX1B               59  /* Mailbox MBX1B Interrupt */
#define CHIP_IRQNUM_GPTMR0              60  /* General purpose timer GTMR0 interrupt */
#define CHIP_IRQNUM_GPTMR1              61  /* General purpose timer GTMR1 interrupt */
#define CHIP_IRQNUM_GPTMR2              62  /* General purpose timer GTMR2 interrupt */
#define CHIP_IRQNUM_GPTMR3              63  /* General purpose timer GTMR3 interrupt */
#define CHIP_IRQNUM_GPTMR4              64  /* General purpose timer GTMR4 interrupt */
#define CHIP_IRQNUM_GPTMR5              65  /* General purpose timer GTMR5 interrupt */
#define CHIP_IRQNUM_GPTMR6              66  /* General purpose timer GTMR6 interrupt */
#define CHIP_IRQNUM_GPTMR7              67  /* General purpose timer GTMR7 interrupt */
#define CHIP_IRQNUM_I2C0                68  /* Integrated Circuit Bus I2C0 Interrupt */
#define CHIP_IRQNUM_I2C1                69  /* Integrated Circuit Bus I2C1 Interrupt */
#define CHIP_IRQNUM_I2C2                70  /* Integrated Circuit Bus I2C2 Interrupt */
#define CHIP_IRQNUM_I2C3                71  /* Integrated Circuit Bus I2C3 Interrupt */
#define CHIP_IRQNUM_PWM0                72  /* PWM timer PWM0 interrupt */
#define CHIP_IRQNUM_HALL0               73  /* Hall sensor interface HALL0 interrupt */
#define CHIP_IRQNUM_QEI0                74  /* Quadrature encoder interface QEI0 interrupt */
#define CHIP_IRQNUM_PWM1                75  /* PWM timer PWM1 interrupt */
#define CHIP_IRQNUM_HALL1               76  /* Hall sensor interface HALL1 interrupt */
#define CHIP_IRQNUM_QEI1                77  /* Quadrature encoder interface QEI1 interrupt */
#define CHIP_IRQNUM_PWM2                78  /* PWM timer PWM2 interrupt */
#define CHIP_IRQNUM_HALL2               79  /* Hall sensor interface HALL2 interrupt */
#define CHIP_IRQNUM_QEI2                80  /* Quadrature encoder interface QEI2 interrupt */
#define CHIP_IRQNUM_PWM3                81  /* PWM timer PWM3 interrupt */
#define CHIP_IRQNUM_HALL3               82  /* Hall sensor interface HALL3 interrupt */
#define CHIP_IRQNUM_QEI3                83  /* Quadrature encoder interface QEI3 interrupt */
#define CHIP_IRQNUM_SDP                 84  /* Secure Data Processor SDP Interrupt */
#define CHIP_IRQNUM_XPI0                85  /* Serial Bus Controller XPI0 Interrupt */
#define CHIP_IRQNUM_XPI1                86  /* Serial Bus Controller XPI1 Interrupt */
#define CHIP_IRQNUM_XDMA                87  /* DMA Controller XDMA Interrupt */
#define CHIP_IRQNUM_HDMA                88  /* DMA Controller HDMA Interrupt */
#define CHIP_IRQNUM_FEMC                89  /* FEMC Controller FEMC Interrupt */
#define CHIP_IRQNUM_RNG                 90  /* Random number generator RNG interrupt */
#define CHIP_IRQNUM_I2S0                91  /* Integrated circuit built-in audio bus I2S0 interrupt */
#define CHIP_IRQNUM_I2S1                92  /* Integrated circuit built-in audio bus I2S1 interrupt */
#define CHIP_IRQNUM_I2S2                93  /* Integrated circuit built-in audio bus I2S2 interrupt */
#define CHIP_IRQNUM_I2S3                94  /* Integrated circuit built-in audio bus I2S3 interrupt */
#define CHIP_IRQNUM_DAO                 95  /* Digital audio output DAO interrupt */
#define CHIP_IRQNUM_PDM                 96  /* Digital Microphone PDM Interrupt */
#define CHIP_IRQNUM_CAM0                97  /* Camera interface CAM0 interrupt */
#define CHIP_IRQNUM_CAM1                98  /* Camera interface CAM1 interrupt */
#define CHIP_IRQNUM_LCDC_D0             99  /* LCD Controller LCDC Interrupt 0 */
#define CHIP_IRQNUM_LCDC_D1             100 /* LCD Controller LCDC Interrupt 1 */
#define CHIP_IRQNUM_PDMA_D0             101 /* 2D graphics acceleration PDMA interrupt 0 */
#define CHIP_IRQNUM_PDMA_D1             102 /* 2D graphics acceleration PDMA interrupt 1 */
#define CHIP_IRQNUM_JPEG                103 /* JPEG codec interrupt */
#define CHIP_IRQNUM_NTMR0               104 /* Network timer NTMR0 interrupt */
#define CHIP_IRQNUM_NTMR1               105 /* Network timer NTMR1 interrupt */
#define CHIP_IRQNUM_USB0                106 /* Universal Serial Bus USB0 Interrupt */
#define CHIP_IRQNUM_USB1                107 /* Universal Serial Bus USB1 Interrupt */
#define CHIP_IRQNUM_ENET0               108 /* Ethernet controller ENET0 interrupt */
#define CHIP_IRQNUM_ENET1               109 /* Ethernet controller ENET1 interrupt */
#define CHIP_IRQNUM_SDXC0               110 /* SD/eMMC Controller SDXC 0 Interrupt */
#define CHIP_IRQNUM_SDXC1               111 /* SD/eMMC Controller SDXC 1 Interrupt */
#define CHIP_IRQNUM_PSEC                112 /* Power Domain Security Manager PSEC Interrupt */
#define CHIP_IRQNUM_PGPIO               113 /* GPIO controller PGPIO interrupt */
#define CHIP_IRQNUM_PWDG                114 /* Watchdog PWDG interrupt */
#define CHIP_IRQNUM_PTMR                115 /* General purpose timer PTMR interrupt */
#define CHIP_IRQNUM_PUART               116 /* Universal Asynchronous Receiver Transmitter PUART Interrupt */
#define CHIP_IRQNUM_VAD                 117 /* Voice print detection module VAD interrupt */
#define CHIP_IRQNUM_FUSE                118 /* OTP controller interrupt */
#define CHIP_IRQNUM_SECMON              119 /* Power Domain Monitor PMON Interrupt */
#define CHIP_IRQNUM_RTC                 120 /* Real time clock RTC interrupt */
#define CHIP_IRQNUM_BUTN                121 /* Battery domain button BUTN interrupt */
#define CHIP_IRQNUM_BGPIO               122 /* GPIO controller BGPIO interrupt */
#define CHIP_IRQNUM_BVIO                123 /* Battery domain security event interrupt, from intrusion detection TAMP, monotonic counter MONO, battery domain monitor BMON */
#define CHIP_IRQNUM_BROWNOUT            124 /* VPMC undervoltage Brownout interrupt */
#define CHIP_IRQNUM_SYSCTL              125 /* System control module SYSCTL clock monitor interrupt */
#define CHIP_IRQNUM_DEBUG0              126 /* Reserved */
#define CHIP_IRQNUM_DEBUG1              127 /* Reserved */

#endif // CHIP_IRQ_NUMS_H
