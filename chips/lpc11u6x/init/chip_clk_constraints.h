#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#define CHIP_CLK_PLL_IN_FREQ_MIN            CHIP_MHZ(10) /* минимальная  входная частота PLL */
#define CHIP_CLK_PLL_IN_FREQ_MAX            CHIP_MHZ(25) /* максимальная входная частота PLL */

#define CHIP_CLK_PLL_VCO_FREQ_MIN           CHIP_MHZ(156) /* минимальная  частота PLL VCO (PLLIN * M / N) */
#define CHIP_CLK_PLL_VCO_FREQ_MAX           CHIP_MHZ(320) /* максимальная частота PLL VCO (PLLIN * M / N) */

#define CHIP_CLK_PLL_OUT_FREQ_MAX           CHIP_MHZ(50)


#endif // CHIP_CLK_CONSTRAINTS_H
