#ifndef CHIP_ARCHTYPE_H
#define CHIP_ARCHTYPE_H

#include "chip_coretype.h"
#include "chip_riscv_core.h"
#include "chip_riscv_csr.h"
#include "chip_riscv_membarrier.h"
#include "chip_riscv_delay.h"



#endif // CHIP_ARCHTYPE_H
