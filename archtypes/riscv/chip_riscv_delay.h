#ifndef CHIP_RISCV_DELAY_H
#define CHIP_RISCV_DELAY_H

__attribute__ ((__noinline__)) void chip_delay_clk(unsigned int clk_count);

#endif // CHIP_RISCV_DELAY_H
