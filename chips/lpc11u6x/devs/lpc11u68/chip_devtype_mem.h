#ifndef CHIP_DEVTYPE_MEM_H
#define CHIP_DEVTYPE_MEM_H

#include "chip_std_defs.h"

#define CHIP_RAM0_MEM_SIZE                   CHIP_KB(32)
#define CHIP_RAM1_MEM_SIZE                   CHIP_KB(2)
#define CHIP_RAM2_MEM_SIZE                   CHIP_KB(2)
#define CHIP_FLASH_MEM_SIZE                  CHIP_KB(256)
#define CHIP_EEPROM_MEM_SIZE                 CHIP_KB(4)


#endif // CHIP_DEVTYPE_MEM_H
