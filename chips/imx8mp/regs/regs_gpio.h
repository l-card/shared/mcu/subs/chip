#ifndef REGS_GPIO_H
#define REGS_GPIO_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t DR;       /* GPIO data register */
    __IO uint32_t GDIR;     /* GPIO direction register */
    __I uint32_t  PSR;      /* GPIO pad status register */
    __IO uint32_t ICR[2];   /* GPIO interrupt configuration registers 1/2 */
    __IO uint32_t IMR;      /* GPIO interrupt mask register */
    __IO uint32_t ISR;      /* GPIO interrupt status register */
    __IO uint32_t EDGE_SEL; /* GPIO edge select register */
} CHIP_REGS_GPIO_T;

#define CHIP_REGS_GPIO(i)        ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIO(i))
#define CHIP_REGS_GPIO1          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIO1)
#define CHIP_REGS_GPIO2          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIO2)
#define CHIP_REGS_GPIO3          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIO3)
#define CHIP_REGS_GPIO4          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIO4)
#define CHIP_REGS_GPIO5          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIO5)


/*********** GPIO interrupt configuration register 1/2 (ICR1/ICR2)  ***********/
#define CHIP_REGFL_GPIO_ICR_FLD(pin)            (0x00000003UL <<  2*((pin) & 0xF))
#define CHIP_REGFL_GPIO_ICR_REGNUM(pin)         ((pin) / 16)

#define CHIP_REGFLDVAL_GPIO_ICR_LOW             0UL
#define CHIP_REGFLDVAL_GPIO_ICR_HIGH            1UL
#define CHIP_REGFLDVAL_GPIO_ICR_RISE            2UL
#define CHIP_REGFLDVAL_GPIO_ICR_FALL            3UL

#endif // REGS_GPIO_H

