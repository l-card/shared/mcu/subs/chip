#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#define CHIP_CLK_PLL_AUDIO_VCO_FREQ_MAX         CHIP_MHZ(1300)
#define CHIP_CLK_PLL_AUDIO_VCO_FREQ_MIN         CHIP_MHZ(650)
#define CHIP_CLK_PLL_AUDIO_OUT_FREQ_MAX         CHIP_MHZ(650)

#define CHIP_CLK_PLL_VIDEO_VCO_FREQ_MAX         CHIP_MHZ(1190)
#define CHIP_CLK_PLL_VIDEO_VCO_FREQ_MIN         CHIP_MHZ(650)
#define CHIP_CLK_PLL_VIDEO_OUT_FREQ_MAX         CHIP_MHZ(1190)

#define CHIP_CLK_PLL_AV_VCO_FREQ_MAX            CHIP_MHZ(1368)
#define CHIP_CLK_PLL_AV_VCO_FREQ_MIN            CHIP_MHZ(552)

#define CHIP_CLK_PLL_SAI_VCO_FREQ_MAX           CHIP_MHZ(100)
#define CHIP_CLK_PLL_SAI_VCO_FREQ_MIN           CHIP_MHZ(25)

#define CHIP_CLK_PLL_HSIO_VCO_FREQ_MAX          CHIP_MHZ(100)

#define CHIP_CLK_PLL_SYS1_VCO_FREQ_MAX          CHIP_MHZ(800)
#define CHIP_CLK_PLL_SYS1_OUT_FREQ_MAX          CHIP_MHZ(800)

#define CHIP_CLK_PLL_SYS2_VCO_FREQ_MAX          CHIP_MHZ(1000)
#define CHIP_CLK_PLL_SYS2_OUT_FREQ_MAX          CHIP_MHZ(1000)

#define CHIP_CLK_PLL_SYS3_VCO_FREQ_MAX          CHIP_MHZ(1000)
#define CHIP_CLK_PLL_SYS3_VCO_FREQ_MIN          CHIP_MHZ(600)
#define CHIP_CLK_PLL_SYS3_OUT_FREQ_MAX          CHIP_MHZ(1000)

#define CHIP_CLK_PLL_ARM_VCO_FREQ_MAX           CHIP_MHZ(1800)
#define CHIP_CLK_PLL_ARM_VCO_FREQ_MIN           CHIP_MHZ(800)
#define CHIP_CLK_PLL_ARM_OUT_FREQ_MAX           CHIP_MHZ(1000)

#define CHIP_CLK_PLL_DRAM_VCO_FREQ_MAX          CHIP_MHZ(1067)
#define CHIP_CLK_PLL_DRAM_VCO_FREQ_MIN          CHIP_MHZ(400)
#define CHIP_CLK_PLL_DRAM_OUT_FREQ_MAX          CHIP_MHZ(1067)

#define CHIP_CLK_PLL_GPU_VCO_FREQ_MAX           CHIP_MHZ(1000)
#define CHIP_CLK_PLL_GPU_VCO_FREQ_MIN           CHIP_MHZ(800)
#define CHIP_CLK_PLL_GPU_OUT_FREQ_MAX           CHIP_MHZ(1000)

#define CHIP_CLK_PLL_VPU_VCO_FREQ_MAX           CHIP_MHZ(800)
#define CHIP_CLK_PLL_VPU_VCO_FREQ_MIN           CHIP_MHZ(400)
#define CHIP_CLK_PLL_VPU_OUT_FREQ_MAX           CHIP_MHZ(800)

#define CHIP_CLK_EXT_FREQ_MAX                   CHIP_MHZ(134)



#define CHIP_CLK_ROOT_ARM_A53_FREQ_MAX          CHIP_MHZ(1000)
#define CHIP_CLK_ROOT_ARM_M7_FREQ_MAX           CHIP_MHZ(800)
#define CHIP_CLK_ROOT_ML_FREQ_MAX               CHIP_MHZ(1000)
#define CHIP_CLK_ROOT_GPU3D_CORE_FREQ_MAX       CHIP_MHZ(1000)
#define CHIP_CLK_ROOT_GPU3D_SHADER_FREQ_MAX     CHIP_MHZ(1000)
#define CHIP_CLK_ROOT_GPU2D_FREQ_MAX            CHIP_MHZ(1000)
#define CHIP_CLK_ROOT_AUDIO_AXI_FREQ_MAX        CHIP_MHZ(800)
#define CHIP_CLK_ROOT_HSIO_AXI_FREQ_MAX         CHIP_MHZ(500)
#define CHIP_CLK_ROOT_MEDIA_ISP_FREQ_MAX        CHIP_MHZ(500)
#define CHIP_CLK_ROOT_MAIN_AXI_FREQ_MAX         CHIP_MHZ(400)
#define CHIP_CLK_ROOT_ENET_AXI_FREQ_MAX         CHIP_MHZ(267)
#define CHIP_CLK_ROOT_NAND_USDHC_BUS_FREQ_MAX   CHIP_MHZ(267)
#define CHIP_CLK_ROOT_VPU_BUS_FREQ_MAX          CHIP_MHZ(800)
#define CHIP_CLK_ROOT_MEDIA_AXI_FREQ_MAX        CHIP_MHZ(500)
#define CHIP_CLK_ROOT_MEDIA_APB_FREQ_MAX        CHIP_MHZ(200)
#define CHIP_CLK_ROOT_HDMI_APB_FREQ_MAX         CHIP_MHZ(200)
#define CHIP_CLK_ROOT_HDMI_AXI_FREQ_MAX         CHIP_MHZ(500)
#define CHIP_CLK_ROOT_GPU_AXI_FREQ_MAX          CHIP_MHZ(800)
#define CHIP_CLK_ROOT_GPU_AHB_FREQ_MAX          CHIP_MHZ(400)
#define CHIP_CLK_ROOT_NOC_FREQ_MAX              CHIP_MHZ(1000)
#define CHIP_CLK_ROOT_NOC_IO_FREQ_MAX           CHIP_MHZ(800)
#define CHIP_CLK_ROOT_ML_AXI_FREQ_MAX           CHIP_MHZ(800)
#define CHIP_CLK_ROOT_ML_AHB_FREQ_MAX           CHIP_MHZ(400)
#define CHIP_CLK_ROOT_AHB_FREQ_MAX              CHIP_MHZ(134)
#define CHIP_CLK_ROOT_IPG_FREQ_MAX              CHIP_MHZ(134)
#define CHIP_CLK_ROOT_AUDIO_AHB_FREQ_MAX        CHIP_MHZ(400)
#define CHIP_CLK_ROOT_MEDIA_DISP2_FREQ_MAX      CHIP_MHZ(160)
#define CHIP_CLK_ROOT_DRAM_SEL_FREQ_MAX         CHIP_MHZ(800)
#define CHIP_CLK_ROOT_ARM_A53_SEL_FREQ_MAX      CHIP_MHZ(1200)
#define CHIP_CLK_ROOT_DRAM_ALT_FREQ_MAX         CHIP_MHZ(800)
#define CHIP_CLK_ROOT_DRAM_APB_FREQ_MAX         CHIP_MHZ(200)
#define CHIP_CLK_ROOT_VPU_G1_FREQ_MAX           CHIP_MHZ(800)
#define CHIP_CLK_ROOT_VPU_G2_FREQ_MAX           CHIP_MHZ(700)
#define CHIP_CLK_ROOT_CAN_FREQ_MAX              CHIP_MHZ(80)
#define CHIP_CLK_ROOT_MEMREPAIR_FREQ_MAX        CHIP_MHZ(50)
#define CHIP_CLK_ROOT_PCIE_PHY_FREQ_MAX         CHIP_MHZ(250)
#define CHIP_CLK_ROOT_PCIE_AUX_FREQ_MAX         CHIP_MHZ(10)
#define CHIP_CLK_ROOT_I2C_FREQ_MAX              CHIP_MHZ(67)
#define CHIP_CLK_ROOT_SAI_FREQ_MAX              CHIP_MHZ(67)
#define CHIP_CLK_ROOT_ENET_QOS_FREQ_MAX         CHIP_MHZ(125)
#define CHIP_CLK_ROOT_ENET_QOS_TIMER_FREQ_MAX   CHIP_MHZ(200)
#define CHIP_CLK_ROOT_ENET_REF_FREQ_MAX         CHIP_MHZ(125)
#define CHIP_CLK_ROOT_ENET_TIMER_FREQ_MAX       CHIP_MHZ(125)
#define CHIP_CLK_ROOT_ENET_PHY_REF_FREQ_MAX     CHIP_MHZ(125)
#define CHIP_CLK_ROOT_NAND_FREQ_MAX             CHIP_MHZ(500)
#define CHIP_CLK_ROOT_QSPI_FREQ_MAX             CHIP_MHZ(400)
#define CHIP_CLK_ROOT_USDHC_FREQ_MAX            CHIP_MHZ(400)
#define CHIP_CLK_ROOT_UART_FREQ_MAX             CHIP_MHZ(80)
#define CHIP_CLK_ROOT_GIC_FREQ_MAX              CHIP_MHZ(500)
#define CHIP_CLK_ROOT_ECSPI_FREQ_MAX            CHIP_MHZ(80)
#define CHIP_CLK_ROOT_PWM_FREQ_MAX              CHIP_MHZ(67)
#define CHIP_CLK_ROOT_GPT_FREQ_MAX              CHIP_MHZ(100)
#define CHIP_CLK_ROOT_TRACE_FREQ_MAX            CHIP_MHZ(134)
#define CHIP_CLK_ROOT_WDOG_FREQ_MAX             CHIP_MHZ(67)
#define CHIP_CLK_ROOT_WRCLK_FREQ_MAX            CHIP_MHZ(40)
#define CHIP_CLK_ROOT_IPP_DO_FREQ_MAX           CHIP_MHZ(267)
#define CHIP_CLK_ROOT_HDMI_FDCC_TST_FREQ_MAX    CHIP_MHZ(600)
#define CHIP_CLK_ROOT_HDMI_REF_266M_FREQ_MAX    CHIP_MHZ(267)
#define CHIP_CLK_ROOT_MEDIA_CAM1_PIX_FREQ_MAX   CHIP_MHZ(500)
#define CHIP_CLK_ROOT_MEDIA_MIPI_PHY1_REF_FREQ_MAX CHIP_MHZ(125)
#define CHIP_CLK_ROOT_MEDIA_DISP1_PIX_FREQ_MAX  CHIP_MHZ(250)
#define CHIP_CLK_ROOT_MEDIA_CAM2_PIX_FREQ_MAX   CHIP_MHZ(267)
#define CHIP_CLK_ROOT_MEDIA_LDB_FREQ_MAX        CHIP_MHZ(560)
#define CHIP_CLK_ROOT_MEDIA_MIPI_TEST_BYTE_FREQ_MAX CHIP_MHZ(200)
#define CHIP_CLK_ROOT_PDM_FREQ_MAX              CHIP_MHZ(200)
#define CHIP_CLK_ROOT_VPU_VC8000E_FREQ_MAX      CHIP_MHZ(800)




#endif // CHIP_CLK_CONSTRAINTS_H
