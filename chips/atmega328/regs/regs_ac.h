#ifndef REGS_AC_H
#define REGS_AC_H


#include "chip_devtype_spec_features.h"

#define CHIP_REG_C0SR       _MMIO_BYTE(0x50) /* AC0 Control and Status Register */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_C0XR       _MMIO_BYTE(0x51) /* AC0 Auxiliary Control Register */
#define CHIP_REG_C0TR       _MMIO_BYTE(0x52) /* AC0 Trimming register */

#define CHIP_REG_C1SR       _MMIO_BYTE(0x2F) /* AC1 Control and Status Register */
#define CHIP_REG_C1XR       _MMIO_BYTE(0x3A) /* AC1 Auxiliary Control Register */
#define CHIP_REG_C1TR       _MMIO_BYTE(0x5B) /* AC1 Trimming register */
#endif


/* ---------------- C0SR – AC0 Control and Status Register -------------------*/
#define CHIP_REGFLD_C0SR_C0D                (0x01U << 7) /* (RW) Analog comparator disable bit */
#define CHIP_REGFLD_C0SR_C0BG               (0x01U << 6) /* (RW) Analog Comparator 0 Positive Input Source Select high bit */
#define CHIP_REGFLD_C0SR_C0O                (0x01U << 5) /* (RO) Analog comparator output status */
#define CHIP_REGFLD_C0SR_C0I                (0x01U << 4) /* (RW1C) The interrupt flag bit for the analog comparator */
#define CHIP_REGFLD_C0SR_C0IE               (0x01U << 3) /* (RW) Interrupt enable bit for the analog comparator */
#define CHIP_REGFLD_C0SR_C0IC               (0x01U << 2) /* (RW) Analog Comparator input capture enable  */
#define CHIP_REGFLD_C0SR_C0IS               (0x03U << 0) /* (RW) Analog Comparator interrupt mode control */

#define CHIP_REGFLDVAL_C0SR_C0I_ANY_EDGE    0 /* Trigger on ACO rising and falling edge */
#define CHIP_REGFLDVAL_C0SR_C0I_FALL        2 /* Trigger on ACO falling edge */
#define CHIP_REGFLDVAL_C0SR_C0I_RISE        3 /* Trigger on ACO rising edge */

#ifdef CHIP_LGT8FX8P
/* ---------------- C0XR – AC0 Auxiliary Control Register --------------------*/
#define CHIP_REGFLD_C0XR_C0OE              (0x01U << 6) /* (RW) AC0 Comparator output to external port PD2 enable */
#define CHIP_REGFLD_C0XR_C0HYSE            (0x01U << 5) /* (RW) AC0 output hysteresis function enable */
#define CHIP_REGFLD_C0XR_C0PS0             (0x01U << 4) /* (RW) AC0 output hysteresis function enable */
#define CHIP_REGFLD_C0XR_COWKE             (0x01U << 3) /* (RW) AC0 usage as wake-up from sleep  source enable */
#define CHIP_REGFLD_C0XR_C0FEN             (0x01U << 2) /* (RW) AC0 digital filter enable */
#define CHIP_REGFLD_C0XR_C0FS              (0x03U << 0) /* (RW) AC0 digital filter width setting */


/* combined with CHIP_REGFLD_C0SR_C0BG */
#define CHIP_REGFLDVAL_C0XR_C0PS_AC0P      0 /* AC0P as positive input */
#define CHIP_REGFLDVAL_C0XR_C0PS_ACXP      1 /* ACXP as positive input */
#define CHIP_REGFLDVAL_C0XR_C0PS_DACO      2 /* output of internal DAC as positive input */
#define CHIP_REGFLDVAL_C0XR_C0PS_DIS       3 /* Turn off the positive input source of AC0 */

#define CHIP_REGFLDVAL_C0XR_C0FS_OFF       0
#define CHIP_REGFLDVAL_C0XR_C0FS_32_US     1
#define CHIP_REGFLDVAL_C0XR_C0FS_64_US     2
#define CHIP_REGFLDVAL_C0XR_C0FS_96_US     3




/* ---------------- C1SR – AC1 Control and Status Register -------------------*/
#define CHIP_REGFLD_C1SR_C1D                (0x01U << 7) /* (RW) Analog comparator disable bit */
#define CHIP_REGFLD_C1SR_C1BG               (0x01U << 6) /* (RW) Analog Comparator 1 Positive Input Source Select high bit */
#define CHIP_REGFLD_C1SR_C1O                (0x01U << 5) /* (RO) Analog comparator output status */
#define CHIP_REGFLD_C1SR_C1I                (0x01U << 4) /* (RW1C) The interrupt flag bit for the analog comparator */
#define CHIP_REGFLD_C1SR_C1IE               (0x01U << 3) /* (RW) Interrupt enable bit for the analog comparator */
#define CHIP_REGFLD_C1SR_C1IC               (0x01U << 2) /* (RW) Analog Comparator input capture enable  */
#define CHIP_REGFLD_C1SR_C1IS               (0x03U << 0) /* (RW) Analog Comparator interrupt mode control */

#define CHIP_REGFLDVAL_C1SR_C1I_ANY_EDGE    0 /* Trigger on ACO rising and falling edge */
#define CHIP_REGFLDVAL_C1SR_C1I_FALL        2 /* Trigger on ACO falling edge */
#define CHIP_REGFLDVAL_C1SR_C1I_RISE        3 /* Trigger on ACO rising edge */


/* ---------------- C1XR – AC1 Auxiliary Control Register --------------------*/
#define CHIP_REGFLD_C1XR_C1OE              (0x01U << 6) /* (RW) AC1 Comparator output to external port PD2 enable */
#define CHIP_REGFLD_C1XR_C1HYSE            (0x01U << 5) /* (RW) AC1 output hysteresis function enable */
#define CHIP_REGFLD_C1XR_C1PS0             (0x01U << 4) /* (RW) AC1 output hysteresis function enable */
#define CHIP_REGFLD_C1XR_C1WKE             (0x01U << 3) /* (RW) AC1 usage as wake-up from sleep  source enable */
#define CHIP_REGFLD_C1XR_C1FEN             (0x01U << 2) /* (RW) AC1 digital filter enable */
#define CHIP_REGFLD_C1XR_C1FS              (0x03U << 0) /* (RW) AC1 digital filter width setting */

/* combined with CHIP_REGFLD_C0SR_C0BG */
#define CHIP_REGFLDVAL_C1XR_C1PS_AC1P      0 /* AC1P as positive input */
#define CHIP_REGFLDVAL_C1XR_C1PS_ACXP      1 /* ACXP as positive input */
#define CHIP_REGFLDVAL_C1XR_C1PS_DACO      2 /* output of internal DAC as positive input */
#define CHIP_REGFLDVAL_C1XR_C1PS_DIS       3 /* Turn off the positive input source of AC1 */

#define CHIP_REGFLDVAL_C1XR_C1FS_OFF       0
#define CHIP_REGFLDVAL_C1XR_C1FS_32_US     1
#define CHIP_REGFLDVAL_C1XR_C1FS_64_US     2
#define CHIP_REGFLDVAL_C1XR_C1FS_96_US     3


#endif

#endif // REGS_AC_H
