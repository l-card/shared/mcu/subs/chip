#ifndef STM32_TIM_GEN_REGS_H
#define STM32_TIM_GEN_REGS_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t CR1;         /* TIM control register 1,                   Address offset: 0x00 */
    __IO uint32_t CR2;         /* TIM control register 2,                   Address offset: 0x04 */
    __IO uint32_t SMCR;        /* TIM slave mode control register,          Address offset: 0x08 */
    __IO uint32_t DIER;        /* TIM DMA/interrupt enable register,        Address offset: 0x0C */
    __IO uint32_t SR;          /* TIM status register,                      Address offset: 0x10 */
    __IO uint32_t EGR;         /* TIM event generation register,            Address offset: 0x14 */
    __IO uint32_t CCMR1;       /* TIM capture/compare mode register 1,      Address offset: 0x18 */
    __IO uint32_t CCMR2;       /* TIM capture/compare mode register 2,      Address offset: 0x1C */
    __IO uint32_t CCER;        /* TIM capture/compare enable register,      Address offset: 0x20 */
    __IO uint32_t CNT;         /* TIM counter register,                     Address offset: 0x24 */
    __IO uint32_t PSC;         /* TIM prescaler register,                   Address offset: 0x28 */
    __IO uint32_t ARR;         /* TIM auto-reload register,                 Address offset: 0x2C */
    __IO uint32_t RCR;         /* TIM repetition counter register,          Address offset: 0x30 */
    __IO uint32_t CCR1;        /* TIM capture/compare register 1,           Address offset: 0x34 */
    __IO uint32_t CCR2;        /* TIM capture/compare register 2,           Address offset: 0x38 */
    __IO uint32_t CCR3;        /* TIM capture/compare register 3,           Address offset: 0x3C */
    __IO uint32_t CCR4;        /* TIM capture/compare register 4,           Address offset: 0x40 */
    __IO uint32_t BDTR;        /* TIM break and dead-time register,         Address offset: 0x44 */
    __IO uint32_t DCR;         /* TIM DMA control register,                 Address offset: 0x48 */
    __IO uint32_t DMAR;        /* TIM DMA address for full transfer,        Address offset: 0x4C */
    __IO uint32_t OR1;         /* TIM option register,                      Address offset: 0x50 */
#if CHIP_SUPPORT_TIM_OUT_5_6
    __IO uint32_t CCMR3;       /* TIM capture/compare mode register 3,      Address offset: 0x54 */
    __IO uint32_t CCR5;        /* TIM capture/compare register5,            Address offset: 0x58 */
    __IO uint32_t CCR6;        /* TIM capture/compare register6,            Address offset: 0x5C */
#else
    uint32_t RESERVED1[3];
#endif
#if CHIP_SUPPORT_TIM_ALTFUNC
    __IO uint32_t AF1;         /* TIM alternate function register 1,        Address offset: 0x60 */
    __IO uint32_t AF2;         /* TIM alternate function register 2,        Address offset: 0x64 */
    __IO uint32_t TISEL;       /* TIM Input Selection register,             Address offset: 0x68 */
#else
    uint32_t RESERVED2[3];
#endif
} CHIP_REGS_TIM_T;


/*********** TIMx control register 1 (TIMx_CR1)  ******************************/
#define CHIP_REGFLD_TIM_CR1_CEN                     (0x00000001UL <<  0U)     /* (RW) Counter enable */
#define CHIP_REGFLD_TIM_CR1_UDIS                    (0x00000001UL <<  1U)     /* (RW) Update disable */
#define CHIP_REGFLD_TIM_CR1_URS                     (0x00000001UL <<  2U)     /* (RW) Update request source */
#define CHIP_REGFLD_TIM_CR1_OPM                     (0x00000001UL <<  3U)     /* (RW) One pulse mode */
#define CHIP_REGFLD_TIM_CR1_DIR                     (0x00000001UL <<  4U)     /* (RW) Direction */
#define CHIP_REGFLD_TIM_CR1_CMS                     (0x00000003UL <<  5U)     /* (RW) Center-aligned mode selection */
#define CHIP_REGFLD_TIM_CR1_ARPE                    (0x00000001UL <<  7U)     /* (RW) Auto-reload preload enable */
#define CHIP_REGFLD_TIM_CR1_CKD                     (0x00000003UL <<  8U)     /* (RW) CKD[1:0] bits (clock division) */
#if CHIP_SUPPORT_TIM_UIF_REMAP
#define CHIP_REGFLD_TIM_CR1_UIF_REMAP               (0x00000001UL << 11U)     /* (RW) Update interrupt flag remap */
#endif
#define CHIP_REGMSK_TIM_CR1_RESERVED                (0x0000F400UL)

#define CHIP_REGFLDVAL_TIM_CR1_DIR_UP               0
#define CHIP_REGFLDVAL_TIM_CR1_DIR_DOWN             1

#define CHIP_REGFLDVAL_TIM_CR1_CMS_EDGE             0 /* Edge Aligned */
#define CHIP_REGFLDVAL_TIM_CR1_CMS_CENTER_OC_DOWN   1 /* Center Aligned, Output Compare on counting down */
#define CHIP_REGFLDVAL_TIM_CR1_CMS_CENTER_OC_UP     2 /* Center Aligned, Output Compare on counting up */
#define CHIP_REGFLDVAL_TIM_CR1_CMS_CENTER_OC_BOTH   3 /* Center Aligned, Output Compare on counting up and down */

#define CHIP_REGFLDVAL_TIM_CR1_CKD_DIV1             0
#define CHIP_REGFLDVAL_TIM_CR1_CKD_DIV2             1
#define CHIP_REGFLDVAL_TIM_CR1_CKD_DIV4             2



/*******************  TIMx control register 2 (TIMx_CR2)  *********************/
#define CHIP_REGFLD_TIM_CR2_CCPC                    (0x00000001UL <<  0U)     /* Capture/Compare Preloaded Control */
#define CHIP_REGFLD_TIM_CR2_CCUS                    (0x00000001UL <<  2U)     /* Capture/Compare Control Update Selection */
#define CHIP_REGFLD_TIM_CR2_CCDS                    (0x00000001UL <<  3U)     /* Capture/Compare DMA Selection */
#define CHIP_REGFLD_TIM_CR2_MMS                     (0x00000007UL <<  4U)     /* Master Mode Selection (for slave timers) */
#define CHIP_REGFLD_TIM_CR2_TI1S                    (0x00000001UL <<  7U)     /* TI1 Selection (xor mode) */
#define CHIP_REGFLD_TIM_CR2_OIS1                    (0x00000001UL <<  8U)     /* Output Idle state 1 (OC1 output) */
#define CHIP_REGFLD_TIM_CR2_OIS1N                   (0x00000001UL <<  9U)     /* Output Idle state 1 (OC1N output) */
#define CHIP_REGFLD_TIM_CR2_OIS2                    (0x00000001UL << 10U)     /* Output Idle state 2 (OC2 output) */
#define CHIP_REGFLD_TIM_CR2_OIS2N                   (0x00000001UL << 11U)     /* Output Idle state 2 (OC2N output) */
#define CHIP_REGFLD_TIM_CR2_OIS3                    (0x00000001UL << 12U)     /* Output Idle state 3 (OC3 output) */
#define CHIP_REGFLD_TIM_CR2_OIS3N                   (0x00000001UL << 13U)     /* Output Idle state 3 (OC3N output) */
#define CHIP_REGFLD_TIM_CR2_OIS4                    (0x00000001UL << 14U)     /* Output Idle state 4 (OC4 output) */
#if CHIP_SUPPORT_TIM_OUT_5_6
#define CHIP_REGFLD_TIM_CR2_OIS5                    (0x00000001UL << 16U)     /* Output Idle state 5 (OC5 output) */
#define CHIP_REGFLD_TIM_CR2_OIS6                    (0x00000001UL << 18U)     /* Output Idle state 6 (OC6 output) */
#endif
#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLD_TIM_CR2_MMS2                    (0x0000000FUL << 20U)     /* Master Mode Selection 2 (TRGO2 for ADC) */
#endif

#define CHIP_REGFLD_TIM_CR2_MMS_RESET                   0 /* TRGO on UG bit */
#define CHIP_REGFLD_TIM_CR2_MMS_ENABLE                  1 /* TRGO on counter enable */
#define CHIP_REGFLD_TIM_CR2_MMS_UPDATE                  2 /* TRGO on update event */
#define CHIP_REGFLD_TIM_CR2_MMS_COMP_PULSE              3 /* TRGO pulse on a catpture or a compare match occured */
#define CHIP_REGFLD_TIM_CR2_MMS_COMP_OC1                4 /* OC1REFC as TRGO */
#define CHIP_REGFLD_TIM_CR2_MMS_COMP_OC2                5 /* OC2REFC as TRGO */
#define CHIP_REGFLD_TIM_CR2_MMS_COMP_OC3                6 /* OC3REFC as TRGO */
#define CHIP_REGFLD_TIM_CR2_MMS_COMP_OC4                7 /* OC4REFC as TRGO */
#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLD_TIM_CR2_MMS2_RESET                  0 /* TRGO2 on UG bit */
#define CHIP_REGFLD_TIM_CR2_MMS2_ENABLE                 1 /* TRGO2 on counter enable */
#define CHIP_REGFLD_TIM_CR2_MMS2_UPDATE                 2 /* TRGO2 on update event */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_PULSE             3 /* TRGO2 pulse on a catpture or a compare match occured */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_OC1               4 /* OC1REFC as TRGO2 */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_OC2               5 /* OC2REFC as TRGO2 */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_OC3               6 /* OC3REFC as TRGO2 */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_OC4               7 /* OC4REFC as TRGO2 */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_OC5               8 /* OC5REFC as TRGO2 */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_OC6               9 /* OC6REFC as TRGO2 */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_PULSE_OC4        10 /* TRGO2 on rising or falling edges of OC4REFC */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_PULSE_OC6        11 /* TRGO2 on rising or falling edges of OC6REFC */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_PULSE_OC4R_OC6R  12 /* TRGO2 on rising edges of OC4REFC or OC6REFC */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_PULSE_OC4R_OC6F  13 /* TRGO2 on rising edges of OC4REFC or falling edges OC6REFC */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_PULSE_OC5R_OC6R  14 /* TRGO2 on rising edges of OC5REFC or OC6REFC */
#define CHIP_REGFLD_TIM_CR2_MMS2_COMP_PULSE_OC5R_OC6F  15 /* TRGO2 on rising edges of OC5REFC or falling edges OC6REFC */
#endif


/*********** TIMx slave mode control register (TIMx_SMCR) *********************/
#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLD_TIM_SMCR_SMS_L                  (0x00000007UL <<  0U)  /* Slave mode selection */
#else
#define CHIP_REGFLD_TIM_SMCR_SMS                    (0x00000007UL <<  0U)  /* Slave mode selection */
#endif
#define CHIP_REGFLD_TIM_SMCR_OCCS                   (0x00000001UL <<  3U)  /* OCREF clear selection */
#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLD_TIM_SMCR_TS_L                   (0x00000007UL <<  4U)  /* Trigger selection */
#else
#define CHIP_REGFLD_TIM_SMCR_TS                     (0x00000007UL <<  4U)  /* Trigger selection */
#endif
#define CHIP_REGFLD_TIM_SMCR_MSM                    (0x00000001UL <<  7U)  /* Master/slave mode */
#define CHIP_REGFLD_TIM_SMCR_ETF                    (0x0000000FUL <<  8U)  /* External trigger filter */
#define CHIP_REGFLD_TIM_SMCR_ETPS                   (0x00000003UL << 12U)  /* External trigger prescaler */
#define CHIP_REGFLD_TIM_SMCR_ECE                    (0x00000001UL << 14U)  /* External clock enable */
#define CHIP_REGFLD_TIM_SMCR_ETP                    (0x00000001UL << 15U)  /* External trigger polarity */
#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLD_TIM_SMCR_SMS_H                  (0x00000001UL << 16U)
#define CHIP_REGFLD_TIM_SMCR_TS_H                   (0x00000003UL << 20U)
#endif

#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLDGET_TIM_SMCR_SMS(wrd)            (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_SMCR_SMS_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_SMCR_SMS_H)) << 3))
#define CHIP_REGFLDGET_TIM_SMCR_TS(wrd)             (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_SMCR_TS_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_SMCR_TS_H)) << 3))
#define CHIP_REGFLDSET_TIM_SMCR_SMS(val)            (LBITFIELD_SET(CHIP_REGFLD_TIM_SMCR_SMS_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_SMCR_SMS_H, ((val) >> 3)))))
#define CHIP_REGFLDSET_TIM_SMCR_TS(val)             (LBITFIELD_SET(CHIP_REGFLD_TIM_SMCR_TS_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_SMCR_TS_H, ((val) >> 3)))))
#else
#define CHIP_REGFLDGET_TIM_SMCR_SMS(wrd)            (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_SMCR_SMS))
#define CHIP_REGFLDGET_TIM_SMCR_TS(wrd)             (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_SMCR_TS))
#define CHIP_REGFLDSET_TIM_SMCR_SMS(val)            (LBITFIELD_SET(CHIP_REGFLD_TIM_SMCR_SMS, val))
#define CHIP_REGFLDSET_TIM_SMCR_TS(val)             (LBITFIELD_SET(CHIP_REGFLD_TIM_SMCR_TS, val))
#endif


#define CHIP_REGFLDVAL_TIM_SMCR_SMS_DIS             0 /* Slave mode disabled */
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_ENCODER_TI1     1 /* Encoder mode 1 - counts TI1FP1 on TI2FP2 level */
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_ENCODER_TI2     2 /* Encoder mode 2 - counts TI2FP2 on TI1FP1 level*/
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_ENCODER_TI1_2   3 /* Encoder mode 3 - counts both */
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_RESET           4 /* Reinit on TRGI rising edge */
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_GATED           5 /* Clock enabled when TRGI is high */
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_TRIG            6 /* Counter start on rising edge of TRGI */
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_EXT_CLOK        7 /* Rising edges of TRGI clock the counter */
#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLDVAL_TIM_SMCR_SMS_RESET_TRIG      8 /* Reset + Trigger */
#endif

#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR0             0 /* Internal Trigger 0 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR1             1 /* Internal Trigger 1 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR2             2 /* Internal Trigger 2 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR3             3 /* Internal Trigger 3 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_TI1F_ED          4 /* TI1 Edge Detector */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_TI1FP1           5 /* Filtered timer input 1 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_TI2FP2           6 /* Filtered timer input 2 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ETRF             7 /* External Trigger input */
#if CHIP_SUPPORT_TIM_EXT_SYNC_MODE
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR4             8  /* Internal Trigger 4 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR5             9  /* Internal Trigger 5 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR6             10 /* Internal Trigger 6 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR7             11 /* Internal Trigger 7 */
#define CHIP_REGFLDVAL_TIM_SMCR_TS_ITR8             12 /* Internal Trigger 8 */
#endif

#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS            0
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FCKINT_N2       1
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FCKINT_N4       2
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FCKINT_N8       3
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_2_N6       4
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_2_N8       5
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_4_N6       6
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_4_N8       7
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_8_N6       8
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_8_N8       9
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_16_N5      10
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_16_N6      11
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_16_N8      12
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_32_N5      13
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_32_N6      14
#define CHIP_REGFLDVAL_TIM_SMCR_ETF_FDTS_32_N8      15


#define CHIP_REGFLDVAL_TIM_SMCR_ETPS_OFF            0
#define CHIP_REGFLDVAL_TIM_SMCR_ETPS_2              1
#define CHIP_REGFLDVAL_TIM_SMCR_ETPS_4              2
#define CHIP_REGFLDVAL_TIM_SMCR_ETPS_8              3


/*********** TIMx DMA/interrupt enable register (TIMx_DIER) *******************/
#define CHIP_REGFLD_TIM_DIER_UIE                    (0x00000001UL <<  0U) /* Update interrupt enable */
#define CHIP_REGFLD_TIM_DIER_CC1IE                  (0x00000001UL <<  1U) /* Capture/Compare 1 interrupt enable */
#define CHIP_REGFLD_TIM_DIER_CC2IE                  (0x00000001UL <<  2U) /* Capture/Compare 2 interrupt enable */
#define CHIP_REGFLD_TIM_DIER_CC3IE                  (0x00000001UL <<  3U) /* Capture/Compare 3 interrupt enable */
#define CHIP_REGFLD_TIM_DIER_CC4IE                  (0x00000001UL <<  4U) /* Capture/Compare 4 interrupt enable */
#define CHIP_REGFLD_TIM_DIER_COMIE                  (0x00000001UL <<  5U) /* COM interrupt enable */
#define CHIP_REGFLD_TIM_DIER_TIE                    (0x00000001UL <<  6U) /* Trigger interrupt enable */
#define CHIP_REGFLD_TIM_DIER_BIE                    (0x00000001UL <<  7U) /* Break interrupt enable */
#define CHIP_REGFLD_TIM_DIER_UDE                    (0x00000001UL <<  8U) /* Update DMA request enable */
#define CHIP_REGFLD_TIM_DIER_CC1DE                  (0x00000001UL <<  9U) /* Capture/Compare 1 DMA request enable */
#define CHIP_REGFLD_TIM_DIER_CC2DE                  (0x00000001UL << 10U) /* Capture/Compare 2 DMA request enable */
#define CHIP_REGFLD_TIM_DIER_CC3DE                  (0x00000001UL << 11U) /* Capture/Compare 3 DMA request enable */
#define CHIP_REGFLD_TIM_DIER_CC4DE                  (0x00000001UL << 12U) /* Capture/Compare 4 DMA request enable */
#define CHIP_REGFLD_TIM_DIER_COMDE                  (0x00000001UL << 13U) /* COM DMA request enable */
#define CHIP_REGFLD_TIM_DIER_TDE                    (0x00000001UL << 14U) /* Trigger DMA request enable */

/*********** TIMx status register (TIMx_SR) ***********************************/
#define CHIP_REGFLD_TIM_SR_UIF                      (0x00000001UL <<  0U) /* Update interrupt Flag */
#define CHIP_REGFLD_TIM_SR_CC1IF                    (0x00000001UL <<  1U) /* Capture/Compare 1 interrupt Flag */
#define CHIP_REGFLD_TIM_SR_CC2IF                    (0x00000001UL <<  2U) /* Capture/Compare 2 interrupt Flag */
#define CHIP_REGFLD_TIM_SR_CC3IF                    (0x00000001UL <<  3U) /* Capture/Compare 3 interrupt Flag */
#define CHIP_REGFLD_TIM_SR_CC4IF                    (0x00000001UL <<  4U) /* Capture/Compare 4 interrupt Flag */
#define CHIP_REGFLD_TIM_SR_COMIF                    (0x00000001UL <<  5U) /* COM interrupt Flag */
#define CHIP_REGFLD_TIM_SR_TIF                      (0x00000001UL <<  6U) /* Trigger interrupt Flag */
#define CHIP_REGFLD_TIM_SR_BIF                      (0x00000001UL <<  7U) /* Break interrupt Flag */
#define CHIP_REGFLD_TIM_SR_B2IF                     (0x00000001UL <<  8U) /* Break 2 interrupt Flag */
#define CHIP_REGFLD_TIM_SR_CC1OF                    (0x00000001UL <<  9U) /* Capture/Compare 1 Overcapture Flag */
#define CHIP_REGFLD_TIM_SR_CC2OF                    (0x00000001UL << 10U) /* Capture/Compare 2 Overcapture Flag */
#define CHIP_REGFLD_TIM_SR_CC3OF                    (0x00000001UL << 11U) /* Capture/Compare 3 Overcapture Flag */
#define CHIP_REGFLD_TIM_SR_CC4OF                    (0x00000001UL << 12U) /* Capture/Compare 4 Overcapture Flag */
#define CHIP_REGFLD_TIM_SR_SBIF                     (0x00000001UL << 13U) /* System Break interrupt Flag */
#if CHIP_SUPPORT_TIM_OUT_5_6
#define CHIP_REGFLD_TIM_SR_CC5IF                    (0x00000001UL << 16U) /* Capture/Compare 5 interrupt Flag */
#define CHIP_REGFLD_TIM_SR_CC6IF                    (0x00000001UL << 17U) /* Capture/Compare 6 interrupt Flag */
#endif


/*********** TIMx event generation register (TIMx_EGR) ************************/
#define CHIP_REGFLD_TIM_EGR_UG                      (0x00000001UL <<  0U) /* Update Generation */
#define CHIP_REGFLD_TIM_EGR_CC1G                    (0x00000001UL <<  1U) /* Capture/Compare 1 Generation */
#define CHIP_REGFLD_TIM_EGR_CC2G                    (0x00000001UL <<  2U) /* Capture/Compare 2 Generation */
#define CHIP_REGFLD_TIM_EGR_CC3G                    (0x00000001UL <<  3U) /* Capture/Compare 3 Generation */
#define CHIP_REGFLD_TIM_EGR_CC4G                    (0x00000001UL <<  4U) /* Capture/Compare 4 Generation */
#define CHIP_REGFLD_TIM_EGR_COMG                    (0x00000001UL <<  5U) /* Capture/Compare Control Update Generation */
#define CHIP_REGFLD_TIM_EGR_TG                      (0x00000001UL <<  6U) /* Trigger Generation */
#define CHIP_REGFLD_TIM_EGR_BG                      (0x00000001UL <<  7U) /* Break Generation */
#if CHIP_SUPPORT_TIM_EXT_BREAK
#define CHIP_REGFLD_TIM_EGR_B2G                     (0x00000001UL <<  8U) /* Break 2 Generation */
#endif


/*********** TIMx capture/compare mode register 1 [alternate] (TIMx_CCMR1) ****/
#define CHIP_REGFLD_TIM_CCMR1_CC1S                  (0x00000003UL <<  0U) /* Capture/Compare 1 Selection */
#define CHIP_REGFLD_TIM_CCMR1_OC1FE                 (0x00000001UL <<  2U) /* Output Compare 1 Fast enable */
#define CHIP_REGFLD_TIM_CCMR1_OC1PE                 (0x00000001UL <<  3U) /* Output Compare 1 Preload enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR1_OC1M_L                (0x00000007UL <<  4U) /* OC1M[2:0] bits (Output Compare 1 Mode) */
#else
#define CHIP_REGFLD_TIM_CCMR1_OC1M                  (0x00000007UL <<  4U) /* OC1M Output Compare 1 Mode */
#endif
#define CHIP_REGFLD_TIM_CCMR1_OC1CE                 (0x00000001UL <<  7U) /* Output Compare 1 Clear Enable */

#define CHIP_REGFLD_TIM_CCMR1_CC2S                  (0x00000003UL <<  8U) /* Capture/Compare 2 Selection */
#define CHIP_REGFLD_TIM_CCMR1_OC2FE                 (0x00000001UL << 10U) /* Output Compare 2 Fast enable */
#define CHIP_REGFLD_TIM_CCMR1_OC2PE                 (0x00000001UL << 11U) /* Output Compare 2 Preload enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR1_OC2M_L                (0x00000007UL << 12U) /* OC2M[2:0] bits (Output Compare 2 Mode) */
#else
#define CHIP_REGFLD_TIM_CCMR1_OC2M                  (0x00000007UL << 12U) /* OC2M (Output Compare 2 Mode) */
#endif
#define CHIP_REGFLD_TIM_CCMR1_OC2CE                 (0x00000001UL << 15U) /* Output Compare 2 Clear Enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR1_OC1M_H                (0x00000001UL << 16U) /*OC1M[3] bits (Output Compare 1 Mode) */
#define CHIP_REGFLD_TIM_CCMR1_OC2M_H                (0x00000001UL << 24U) /*OC2M[3] bits (Output Compare 2 Mode) */
#endif

/*--------------------- Input Capture Mode    ------------------------------*/
#define CHIP_REGFLD_TIM_CCMR1_IC1PSC                (0x00000003UL <<  2U) /* Input Capture 1 Prescaler */
#define CHIP_REGFLD_TIM_CCMR1_IC1F                  (0x0000000FUL <<  4U) /* Input Capture 1 Filter */
#define CHIP_REGFLD_TIM_CCMR1_IC2PSC                (0x00000003UL << 10U) /* Input Capture 2 Prescaler */
#define CHIP_REGFLD_TIM_CCMR1_IC2F                  (0x0000000FUL << 12U) /* Input Capture 2 Filter */

#define CHIP_REGFLDVAL_TIM_CCMR_CCXS_OUT            0 /* Output */
#define CHIP_REGFLDVAL_TIM_CCMR_CCXS_IN_TRC         3 /* Input, IC1 = TRC */
#define CHIP_REGFLDVAL_TIM_CCMR1_CC1S_IN_TI1        1 /* Input, IC1 = TI1 */
#define CHIP_REGFLDVAL_TIM_CCMR1_CC1S_IN_TI2        2 /* Input, IC1 = TI2 */
#define CHIP_REGFLDVAL_TIM_CCMR1_CC2S_IN_TI2        1 /* Input, IC2 = TI2 */
#define CHIP_REGFLDVAL_TIM_CCMR1_CC2S_IN_TI1        2 /* Input, IC2 = TI1 */


#define CHIP_REGFLDVAL_TIM_CCMR_ICXPSC_DIV1         0
#define CHIP_REGFLDVAL_TIM_CCMR_ICXPSC_DIV2         1
#define CHIP_REGFLDVAL_TIM_CCMR_ICXPSC_DIV4         2
#define CHIP_REGFLDVAL_TIM_CCMR_ICXPSC_DIV8         3

#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_DIS           0 /* No filter, f_dts */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FCKINT_N2     1 /* Fck_int, N=2 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FCKINT_N4     2 /* Fck_int, N=4 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FCKINT_N6     3 /* Fck_int, N=8 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS2_N6      4 /* Fdts/2,  N=6 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS2_N8      5 /* Fdts/2,  N=8 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS4_N6      6 /* Fdts/4,  N=6 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS4_N8      7 /* Fdts/4,  N=8 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS8_N6      8 /* Fdts/8,  N=6 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS8_N8      9 /* Fdts/8,  N=8 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS16_N5    10 /* Fdts/16, N=5 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS16_N6    11 /* Fdts/16, N=6 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS16_N8    12 /* Fdts/16, N=8 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS32_N5    13 /* Fdts/32, N=5 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS32_N6    14 /* Fdts/32, N=6 */
#define CHIP_REGFLDVAL_TIM_CCMR_ICXF_FDTS32_N8    15 /* Fdts/32, N=8 */

#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_FROZEN        0 /* No effect on the outputs */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_SET           1 /* Set OCxREF to active level on match */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_CLEAR         2 /* Set OCxREF to inactive level on match */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_TOGGLE        3 /* Toggle OCxREF on match */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_FORCE_OFF     4 /* Force OCxREF inactive level */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_FORCE_ON      5 /* Force OCxREF active level */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_PWM1          6 /* Upcounting - active on CNT < CCRx */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_PWM2          7 /* Upcounting - inactive on CNT < CCRx */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_RETRIG_OPM1   8 /* Retrigerrable OPM mode 1 */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_RETRIG_OPM2   9 /* Retrigerrable OPM mode 2 */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_COMB_PWM1    12 /* Combined PWM1 */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_COMB_PWM2    13 /* Combined PWM2 */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_ASYM_PWM1    14 /* Asymmetric PWM1 */
#define CHIP_REGFLDVAL_TIM_CCMR_OCXM_ASYM_PWM2    15 /* Asymmetric PWM2 */
#endif

#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLDGET_TIM_CCMR1_OC1M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR1_OC1M_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR1_OC1M_H)) << 3))
#define CHIP_REGFLDGET_TIM_CCMR1_OC2M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR1_OC2M_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR1_OC2M_H)) << 3))
#define CHIP_REGFLDSET_TIM_CCMR1_OC1M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR1_OC1M_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR1_OC1M_H, ((val) >> 3)))))
#define CHIP_REGFLDSET_TIM_CCMR1_OC2M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR1_OC2M_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR1_OC2M_H, ((val) >> 3)))))
#else
#define CHIP_REGFLDGET_TIM_CCMR1_OC1M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR1_OC1M))
#define CHIP_REGFLDGET_TIM_CCMR1_OC2M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR1_OC2M))
#define CHIP_REGFLDSET_TIM_CCMR1_OC1M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR1_OC1M, val))
#define CHIP_REGFLDSET_TIM_CCMR1_OC2M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR1_OC2M, val))
#endif


/*********** TIMx capture/compare mode register 2 [alternate] (TIMx_CCMR2) ****/
#define CHIP_REGFLD_TIM_CCMR2_CC3S                  (0x00000003UL <<  0U) /* Capture/Compare 3 Selection */
#define CHIP_REGFLD_TIM_CCMR2_OC3FE                 (0x00000001UL <<  2U) /* Output Compare 3 Fast enable */
#define CHIP_REGFLD_TIM_CCMR2_OC3PE                 (0x00000001UL <<  3U) /* Output Compare 3 Preload enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR2_OC3M_L                (0x00000007UL <<  4U) /* OC3M[2:0] bits (Output Compare 3 Mode) */
#else
#define CHIP_REGFLD_TIM_CCMR2_OC3M                  (0x00000007UL <<  4U) /* OC3M (Output Compare 3 Mode) */
#endif
#define CHIP_REGFLD_TIM_CCMR2_OC3CE                 (0x00000001UL <<  7U) /* Output Compare 3 Clear Enable */
#define CHIP_REGFLD_TIM_CCMR2_CC4S                  (0x00000003UL <<  8U) /* Capture/Compare 4 Selection */
#define CHIP_REGFLD_TIM_CCMR2_OC4FE                 (0x00000001UL << 10U) /* Output Compare 4 Fast enable */
#define CHIP_REGFLD_TIM_CCMR2_OC4PE                 (0x00000001UL << 11U) /* Output Compare 4 Preload enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR2_OC4M_L                (0x00000007UL << 12U) /* OC4M[2:0] bits (Output Compare 4 Mode) */
#else
#define CHIP_REGFLD_TIM_CCMR2_OC4M                  (0x00000007UL << 12U) /* OC4M (Output Compare 4 Mode) */
#endif
#define CHIP_REGFLD_TIM_CCMR2_OC4CE                 (0x00000001UL << 15U) /* Output Compare 4 Clear Enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR2_OC3M_H                (0x00000001UL << 16U) /*OC3M[3] bits (Output Compare 3 Mode) */
#define CHIP_REGFLD_TIM_CCMR2_OC4M_H                (0x00000001UL << 24U) /*OC4M[3] bits (Output Compare 4 Mode) */
#endif
/*--------------------- Input Capture Mode    ------------------------------*/
#define CHIP_REGFLD_TIM_CCMR2_IC3PSC                (0x00000003UL <<  2U) /* Input Capture 3 Prescaler */
#define CHIP_REGFLD_TIM_CCMR2_IC3F                  (0x0000000FUL <<  4U) /* Input Capture 3 Filter */
#define CHIP_REGFLD_TIM_CCMR2_IC4PSC                (0x00000003UL << 10U) /* Input Capture 4 Prescaler */
#define CHIP_REGFLD_TIM_CCMR2_IC4F                  (0x0000000FUL << 12U) /* Input Capture 4 Filter */


#define CHIP_REGFLDVAL_TIM_CCMR2_CC3S_IN_TI3        1 /* Input, IC3 = TI3 */
#define CHIP_REGFLDVAL_TIM_CCMR2_CC3S_IN_TI4        2 /* Input, IC3 = TI4 */
#define CHIP_REGFLDVAL_TIM_CCMR2_CC4S_IN_TI4        1 /* Input, IC4 = TI4 */
#define CHIP_REGFLDVAL_TIM_CCMR2_CC4S_IN_TI3        2 /* Input, IC4 = TI3 */

#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLDGET_TIM_CCMR2_OC3M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR2_OC3M_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR2_OC3M_H)) << 3))
#define CHIP_REGFLDGET_TIM_CCMR2_OC4M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR2_OC4M_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR2_OC4M_H)) << 3))
#define CHIP_REGFLDSET_TIM_CCMR2_OC3M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR2_OC3M_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR2_OC3M_H, ((val) >> 3)))))
#define CHIP_REGFLDSET_TIM_CCMR2_OC4M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR2_OC4M_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR2_OC4M_H, ((val) >> 3)))))
#else
#define CHIP_REGFLDGET_TIM_CCMR2_OC3M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR2_OC3M))
#define CHIP_REGFLDGET_TIM_CCMR2_OC4M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR2_OC4M))
#define CHIP_REGFLDSET_TIM_CCMR2_OC3M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR2_OC3M, val))
#define CHIP_REGFLDSET_TIM_CCMR2_OC4M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR2_OC4M, val))
#endif

#if CHIP_SUPPORT_TIM_OUT_5_6
/*********** TIMx capture/compare mode register 3 (TIMx_CCMR3) ****************/
#define CHIP_REGFLD_TIM_CCMR3_OC5FE                 (0x00000001UL <<  2U)  /* Output Compare 5 Fast enable */
#define CHIP_REGFLD_TIM_CCMR3_OC5PE                 (0x00000001UL <<  3U)  /* Output Compare 5 Preload enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR3_OC5M_L                (0x00000007UL <<  4U)  /* OC5M[2:0] bits (Output Compare 5 Mode) */
#else
#define CHIP_REGFLD_TIM_CCMR3_OC5M                  (0x00000007UL <<  4U)  /* OC5M (Output Compare 5 Mode) */
#endif
#define CHIP_REGFLD_TIM_CCMR3_OC5CE                 (0x00000001UL <<  7U)  /* Output Compare 5 Clear Enable */
#define CHIP_REGFLD_TIM_CCMR3_OC6FE                 (0x00000001UL << 10U)  /* Output Compare 6 Fast enable */
#define CHIP_REGFLD_TIM_CCMR3_OC6PE                 (0x00000001UL << 11U)  /* Output Compare 6 Preload enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR3_OC6M_L                (0x00000007UL << 12U)  /* OC6M[2:0] bits (Output Compare 6 Mode) */
#else
#define CHIP_REGFLD_TIM_CCMR3_OC6M                  (0x00000007UL << 12U)  /* OC6M (Output Compare 6 Mode) */
#endif
#define CHIP_REGFLD_TIM_CCMR3_OC6CE                 (0x00000001UL << 15U)  /* Output Compare 6 Clear Enable */
#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLD_TIM_CCMR3_OC5M_H                (0x00000001UL << 16U)  /* OC5M[3] bits (Output Compare 5 Mode) */
#define CHIP_REGFLD_TIM_CCMR3_OC6M_H                (0x00000001UL << 24U)  /* OC6M[3] bits (Output Compare 6 Mode) */
#endif


#if CHIP_SUPPORT_TIM_EXT_OUT_MODE
#define CHIP_REGFLDGET_TIM_CCMR3_OC5M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR3_OC5M_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR3_OC5M_H)) << 3))
#define CHIP_REGFLDGET_TIM_CCMR3_OC6M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR3_OC6M_L) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR3_OC6M_H)) << 3))
#define CHIP_REGFLDSET_TIM_CCMR3_OC5M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR3_OC5M_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR3_OC5M_H, ((val) >> 3)))))
#define CHIP_REGFLDSET_TIM_CCMR3_OC6M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR3_OC6M_L, val) | ((LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR3_OC6M_H, ((val) >> 3)))))
#else
#define CHIP_REGFLDGET_TIM_CCMR3_OC5M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR3_OC5M))
#define CHIP_REGFLDGET_TIM_CCMR3_OC6M(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_TIM_CCMR3_OC6M))
#define CHIP_REGFLDSET_TIM_CCMR3_OC5M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR3_OC5M, val))
#define CHIP_REGFLDSET_TIM_CCMR3_OC6M(val)          (LBITFIELD_SET(CHIP_REGFLD_TIM_CCMR3_OC6M, val))
#endif
#endif



/****** TIMx capture/compare enable register (TIMx_CCER) **********************/
#define CHIP_REGFLD_TIM_CCER_CC1E                   (0x00000001UL <<  0U)  /* Capture/Compare 1 output enable */
#define CHIP_REGFLD_TIM_CCER_CC1P                   (0x00000001UL <<  1U)  /* Capture/Compare 1 output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC1NE                  (0x00000001UL <<  2U)  /* Capture/Compare 1 Complementary output enable */
#define CHIP_REGFLD_TIM_CCER_CC1NP                  (0x00000001UL <<  3U)  /* Capture/Compare 1 Complementary output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC2E                   (0x00000001UL <<  4U)  /* Capture/Compare 2 output enable */
#define CHIP_REGFLD_TIM_CCER_CC2P                   (0x00000001UL <<  5U)  /* Capture/Compare 2 output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC2NE                  (0x00000001UL <<  6U)  /* Capture/Compare 2 Complementary output enable */
#define CHIP_REGFLD_TIM_CCER_CC2NP                  (0x00000001UL <<  7U)  /* Capture/Compare 2 Complementary output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC3E                   (0x00000001UL <<  8U)  /* Capture/Compare 3 output enable */
#define CHIP_REGFLD_TIM_CCER_CC3P                   (0x00000001UL <<  9U)  /* Capture/Compare 3 output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC3NE                  (0x00000001UL << 10U)  /* Capture/Compare 3 Complementary output enable */
#define CHIP_REGFLD_TIM_CCER_CC3NP                  (0x00000001UL << 11U)  /* Capture/Compare 3 Complementary output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC4E                   (0x00000001UL << 12U)  /* Capture/Compare 4 output enable */
#define CHIP_REGFLD_TIM_CCER_CC4P                   (0x00000001UL << 13U)  /* Capture/Compare 4 output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC4NP                  (0x00000001UL << 15U)  /* Capture/Compare 4 Complementary output Polarity */
#if CHIP_SUPPORT_TIM_OUT_5_6
#define CHIP_REGFLD_TIM_CCER_CC5E                   (0x00000001UL << 16U)  /* Capture/Compare 5 output enable */
#define CHIP_REGFLD_TIM_CCER_CC5P                   (0x00000001UL << 17U)  /* Capture/Compare 5 output Polarity */
#define CHIP_REGFLD_TIM_CCER_CC6E                   (0x00000001UL << 20U)  /* Capture/Compare 6 output enable */
#define CHIP_REGFLD_TIM_CCER_CC6P                   (0x00000001UL << 21U)  /* Capture/Compare 6 output Polarity */
#endif

/*********** TIMx counter (TIMx_CNT) ******************************************/
#define CHIP_REGFLD_TIM_CNT_CNT                     (0xFFFFFFFFUL <<  0U)  /* Counter Value */
#define CHIP_REGFLD_TIM_CNT_UIFCPY                  (0x00000001UL << 31U)  /* Update interrupt flag copy (if UIFREMAP=1) */

/*********** TIMx prescaler (TIMx_PSC) ****************************************/
#define CHIP_REGFLD_TIM_PSC_PSC                     (0x0000FFFFUL <<  0U)  /* Prescaler Value */

/*********** TIMx auto-reload register (TIMx_ARR)********* ********************/
#define CHIP_REGFLD_TIM_ARR_ARR                     (0xFFFFFFFFUL <<  0U)  /* Actual auto-reload Value */

/*********** TIMx repetition counter register  (TIMx_RCR) *********************/
#define CHIP_REGFLD_TIM_RCR_REP                     (0x0000FFFFUL <<  0U)  /* Repetition Counter Value */

/*********** TIMx capture/compare register 1 (TIMx_CCR1) **********************/
#define CHIP_REGFLD_TIM_CCR1_CCR1                   (0x0000FFFFUL <<  0U)  /* Capture/Compare 1 Value */

/*********** TIMx capture/compare register 2 (TIMx_CCR2) **********************/
#define CHIP_REGFLD_TIM_CCR2_CCR2                   (0x0000FFFFUL <<  0U)  /* Capture/Compare 2 Value */

/*********** TIMx capture/compare register 3 (TIMx_CCR3) **********************/
#define CHIP_REGFLD_TIM_CCR3_CCR3                   (0x0000FFFFUL <<  0U)  /* Capture/Compare 3 Value */

/*********** TIMx capture/compare register 4 (TIMx_CCR4) **********************/
#define CHIP_REGFLD_TIM_CCR4_CCR4             (0x0000FFFFUL <<  0U)  /* Capture/Compare 4 Value */

/*********** TIMx capture/compare register 5 (TIMx_CCR5) **********************/
#define CHIP_REGFLD_TIM_CCR5_CCR5             (0xFFFFFFFFUL <<  0U)  /* Capture/Compare 5 Value */
#if CHIP_SUPPORT_TIM_CH_GROUP
#define CHIP_REGFLD_TIM_CCR5_GC5C1            (0x00000001UL << 29U)  /* Group Channel 5 and Channel 1 */
#define CHIP_REGFLD_TIM_CCR5_GC5C2            (0x00000001UL << 30U)  /* Group Channel 5 and Channel 2 */
#define CHIP_REGFLD_TIM_CCR5_GC5C3            (0x00000001UL << 31U)  /* Group Channel 5 and Channel 3 */
#endif

/*********** TIMx capture/compare register 6 (TIMx_CCR6) **********************/
#define CHIP_REGFLD_TIM_CCR6_CCR6             (0x0000FFFFUL <<  0U)  /* Capture/Compare 6 Value */

/*********** TIMx break and dead-time register (TIMx_BDTR) ********************/
#define CHIP_REGFLD_TIM_BDTR_DTG              (0x000000FFUL <<  0U)  /* Dead-Time Generator set-up */
#define CHIP_REGFLD_TIM_BDTR_LOCK             (0x00000003UL <<  8U)  /* Lock Configuration */
#define CHIP_REGFLD_TIM_BDTR_OSSI             (0x00000001UL << 10U)  /* Off-State Selection for Idle mode */
#define CHIP_REGFLD_TIM_BDTR_OSSR             (0x00000001UL << 11U)  /* Off-State Selection for Run mode */
#define CHIP_REGFLD_TIM_BDTR_BKE              (0x00000001UL << 12U)  /* Break enable for Break 1 */
#define CHIP_REGFLD_TIM_BDTR_BKP              (0x00000001UL << 13U)  /* Break Polarity for Break 1 */
#define CHIP_REGFLD_TIM_BDTR_AOE              (0x00000001UL << 14U)  /* Automatic Output enable */
#define CHIP_REGFLD_TIM_BDTR_MOE              (0x00000001UL << 15U)  /* Main Output enable */
#if CHIP_SUPPORT_TIM_EXT_BREAK
#define CHIP_REGFLD_TIM_BDTR_BKF              (0x0000000FUL << 16U)  /* Break Filter for Break 1 */
#define CHIP_REGFLD_TIM_BDTR_BK2F             (0x0000000FUL << 20U)  /* Break Filter for Break 2 */
#define CHIP_REGFLD_TIM_BDTR_BK2E             (0x00000001UL << 24U)  /* Break enable for Break 2 */
#define CHIP_REGFLD_TIM_BDTR_BK2P             (0x00000001UL << 25U)  /* Break Polarity for Break 2 */
#define CHIP_REGFLD_TIM_BDTR_BKDSRM           (0x00000001UL << 26U)  /* Break disarming/re-arming */
#define CHIP_REGFLD_TIM_BDTR_BK2DSRM          (0x00000001UL << 27U)  /* Break2 disarming/re-arming */
#define CHIP_REGFLD_TIM_BDTR_BKBID            (0x00000001UL << 28U)  /* Break BIDirectional */
#define CHIP_REGFLD_TIM_BDTR_BK2BID           (0x00000001UL << 29U)  /* Break2 BIDirectional */
#endif

#define CHIP_REGFLDVAL_TIM_BDTR_OSSR_DIS      0
#define CHIP_REGFLDVAL_TIM_BDTR_OSSR_INACT    1

/*********** TIMx DMA address for full transfer *******************************/
#define CHIP_REGFLD_TIM_DCR_DBA               (0x0000001FUL <<  0U)  /* DMA Base Address */
#define CHIP_REGFLD_TIM_DCR_DBL               (0x0000001FUL <<  8U)  /* DMA Burst Length */

/*********** TIMx DMA address for full transfer (TIMx_DMAR) *******************/
#define CHIP_REGFLD_TIM_DMAR_DMAB             (0xFFFFFFFFUL <<  0U)  /* DMA register for burst accesses */

/*********** TIM timer input selection register (TIMx_TISEL) ******************/
#if CHIP_SUPPORT_TIM_ALTFUNC
#define CHIP_REGFLD_TIM_TISEL_TI1SEL          (0x0000000FUL <<  0U) /* TIM TI1 SEL */
#define CHIP_REGFLD_TIM_TISEL_TI2SEL          (0x0000000FUL <<  8U) /* TIM TI2 SEL */
#define CHIP_REGFLD_TIM_TISEL_TI3SEL          (0x0000000FUL << 16U) /* TIM TI3 SEL */
#define CHIP_REGFLD_TIM_TISEL_TI4SEL          (0x0000000FUL << 24U) /* TIM TI4 SEL */
#endif


#endif // STM32_TIM_GEN_REGS_H
