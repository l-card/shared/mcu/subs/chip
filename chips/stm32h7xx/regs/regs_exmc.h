#ifndef CHIP_STM32H7XX_REGS_EXMC_H_
#define CHIP_STM32H7XX_REGS_EXMC_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t BTCR[8];      /* NOR/PSRAM chip-select control register(BCR) and chip-select timing register(BTR), Address offset: 0x00-1C */
    uint32_t RESERVED0[16];
    __IO uint32_t PCR2;         /* NAND Flash control register 2,                       Address offset: 0x60 */
    __IO uint32_t SR2;          /* NAND Flash FIFO status and interrupt register 2,     Address offset: 0x64 */
    __IO uint32_t PMEM2;        /* NAND Flash Common memory space timing register 2,    Address offset: 0x68 */
    __IO uint32_t PATT2;        /* NAND Flash Attribute memory space timing register 2, Address offset: 0x6C */
    uint32_t      RESERVED1;    /* Reserved, 0x70                                                            */
    __IO uint32_t ECCR2;        /* NAND Flash ECC result registers 2,                   Address offset: 0x74 */
    uint32_t      RESERVED2[2];
    __IO uint32_t PCR;          /* NAND Flash control register 3,                       Address offset: 0x80 */
    __IO uint32_t SR;           /* NAND Flash FIFO status and interrupt register 3,     Address offset: 0x84 */
    __IO uint32_t PMEM;         /* NAND Flash Common memory space timing register 3,    Address offset: 0x88 */
    __IO uint32_t PATT;         /* NAND Flash Attribute memory space timing register 3, Address offset: 0x8C */
    uint32_t      RESERVED3;    /* Reserved, 0x90                                                            */
    __IO uint32_t ECCR;         /* NAND Flash ECC result registers 3,                   Address offset: 0x94 */
    uint32_t      RESERVED4[27];
    __IO uint32_t BWTR[7];      /* NOR/PSRAM write timing registers, Address offset: 0x104-0x11C */
    uint32_t      RESERVED5[8];
    __IO uint32_t SDCR[2];      /* SDRAM Control registers ,      Address offset: 0x140-0x144  */
    __IO uint32_t SDTR[2];      /* SDRAM Timing registers ,       Address offset: 0x148-0x14C  */
    __IO uint32_t SDCMR;        /* SDRAM Command Mode register,    Address offset: 0x150  */
    __IO uint32_t SDRTR;        /* SDRAM Refresh Timer register,   Address offset: 0x154  */
    __IO uint32_t SDSR;         /* SDRAM Status register,          Address offset: 0x158  */
} CHIP_REGS_EXMC_T;

#define CHIP_REGS_EXMC          ((CHIP_REGS_EXMC_T *) CHIP_MEMRGN_ADDR_PERIPH_EXMC)

/* SRAM/NOR-Flash chip-select control registers for bank 1 special (EXMC_BCR1) */
#define CHIP_REGFLD_EXMC_BCR1_CCLKEN                 (0x00000001UL << 20U) /* Continous clock enable */
#define CHIP_REGFLD_EXMC_BCR1_WFDIS                  (0x00000001UL << 21U) /* Write FIFO Disable */
#define CHIP_REGFLD_EXMC_BCR1_BMAP                   (0x00000003UL << 24U) /* EXMC bank mapping */
#define CHIP_REGFLD_EXMC_BCR1_EN                     (0x00000001UL << 31U) /* EXMC controller Enable */

/* SRAM/NOR-Flash chip-select control registers for bank x (EXMC_BCRx), x=1..4 */
#define CHIP_REGFLD_EXMC_BCR_MBKEN                   (0x00000001UL <<  0U) /* Memory bank enable bit */
#define CHIP_REGFLD_EXMC_BCR_MUXEN                   (0x00000001UL <<  1U) /* Address/data multiplexing enable bit */
#define CHIP_REGFLD_EXMC_BCR_MTYP                    (0x00000003UL <<  2U) /* Memory type */
#define CHIP_REGFLD_EXMC_BCR_MWID                    (0x00000003UL <<  4U) /* Memory data bus width */
#define CHIP_REGFLD_EXMC_BCR_FACCEN                  (0x00000001UL <<  6U) /* Flash access enable */
#define CHIP_REGFLD_EXMC_BCR_BURSTEN                 (0x00000001UL <<  8U) /* Burst enable bit */
#define CHIP_REGFLD_EXMC_BCR_WAITPOL                 (0x00000001UL <<  9U) /* Wait signal polarity bit */
#define CHIP_REGFLD_EXMC_BCR_WAITCFG                 (0x00000001UL << 11U) /* Wait timing configuration */
#define CHIP_REGFLD_EXMC_BCR_WREN                    (0x00000001UL << 12U) /* Write enable bit */
#define CHIP_REGFLD_EXMC_BCR_WAITEN                  (0x00000001UL << 13U) /* Wait enable bit */
#define CHIP_REGFLD_EXMC_BCR_EXTMOD                  (0x00000001UL << 14U) /* Extended mode enable */
#define CHIP_REGFLD_EXMC_BCR_ASYNCWAIT               (0x00000001UL << 15U) /* Asynchronous wait */
#define CHIP_REGFLD_EXMC_BCR_CPSIZE                  (0x00000007UL << 16U) /* CRAM Page Size */
#define CHIP_REGFLD_EXMC_BCR_CBURSTRW                (0x00000001UL << 19U) /* Write burst enable */

/* SRAM/NOR-Flash chip-select timing registers for bank x (EXMC_BTRx), x=1..4 **/
#define CHIP_REGFLD_EXMC_BTR_ADDSET                  (0x0000000FUL <<  0U) /* Address setup phase duration */
#define CHIP_REGFLD_EXMC_BTR_ADDHLD                  (0x0000000FUL <<  4U) /* Address-hold phase duration */
#define CHIP_REGFLD_EXMC_BTR_DATAST                  (0x000000FFUL <<  8U) /* Data-phase duration */
#define CHIP_REGFLD_EXMC_BTR_BUSTURN                 (0x0000000FUL << 16U) /* Bus turnaround phase duration */
#define CHIP_REGFLD_EXMC_BTR_CLKDIV                  (0x0000000FUL << 20U) /* Clock divide ratio */
#define CHIP_REGFLD_EXMC_BTR_DATLAT                  (0x0000000FUL << 24U) /* Data latency */
#define CHIP_REGFLD_EXMC_BTR_ACCMOD                  (0x00000003UL << 28U) /* Access mode */

/* SRAM/NOR-Flash write timing registers for bank x (EXMC_BWTRx), x=1..4 *******/
#define CHIP_REGFLD_EXMC_BWTR_ADDSET                 (0x0000000FUL <<  0U) /* Address setup phase duration */
#define CHIP_REGFLD_EXMC_BWTR_ADDHLD                 (0x0000000FUL <<  4U) /* Address-hold phase duration */
#define CHIP_REGFLD_EXMC_BWTR_DATAST                 (0x000000FFUL <<  8U) /* Data-phase duration */
#define CHIP_REGFLD_EXMC_BWTR_BUSTURN                (0x0000000FUL << 16U) /* Bus turnaround phase duration */
#define CHIP_REGFLD_EXMC_BWTR_ACCMOD                 (0x0000000FUL << 28U) /* Access mode */



/****** NAND Flash control registers (EXMC_PCR) ********************************/
#define CHIP_REGFLD_EXMC_PCR_PWAITEN                 (0x00000001UL <<  1U) /* Wait feature enable bit */
#define CHIP_REGFLD_EXMC_PCR_PBKEN                   (0x00000001UL <<  2U) /* NAND Flash memory bank enable bit */
#define CHIP_REGFLD_EXMC_PCR_PWID                    (0x00000003UL <<  4U) /* NAND Flash databus width */
#define CHIP_REGFLD_EXMC_PCR_ECCEN                   (0x00000001UL <<  6U) /* ECC computation logic enable bit */
#define CHIP_REGFLD_EXMC_PCR_TCLR                    (0x0000000FUL <<  9U) /* CLE to RE delay */
#define CHIP_REGFLD_EXMC_PCR_TAR                     (0x0000000FUL << 13U) /* ALE to RE delay */
#define CHIP_REGFLD_EXMC_PCR_ECCPS                   (0x00000007UL << 17U) /* ECC page size */

/****** FIFO status and interrupt register (EXMC_SR) ***************************/
#define CHIP_REGFLD_EXMC_SR_IRS                      (0x00000001UL <<  0U) /* Interrupt Rising Edge status */
#define CHIP_REGFLD_EXMC_SR_ILS                      (0x00000001UL <<  1U) /* Interrupt Level status */
#define CHIP_REGFLD_EXMC_SR_IFS                      (0x00000001UL <<  2U) /* Interrupt Falling Edge status */
#define CHIP_REGFLD_EXMC_SR_IREN                     (0x00000001UL <<  3U) /* Interrupt Rising Edge detection Enable bit */
#define CHIP_REGFLD_EXMC_SR_ILEN                     (0x00000001UL <<  4U) /* Interrupt Level detection Enable bit */
#define CHIP_REGFLD_EXMC_SR_IFEN                     (0x00000001UL <<  5U) /* Interrupt Falling Edge detection Enable bit */
#define CHIP_REGFLD_EXMC_SR_FEMPT                    (0x00000001UL <<  6U) /* FIFO empty */

/****** Common memory space timing register (EXMC_PMEM) ************************/
#define CHIP_REGFLD_EXMC_PMEM_MEMSET                 (0x000000FFUL <<  0U) /* Common memory setup time */
#define CHIP_REGFLD_EXMC_PMEM_MEMWAIT                (0x000000FFUL <<  8U) /* Common memory wait time */
#define CHIP_REGFLD_EXMC_PMEM_MEMHOLD                (0x000000FFUL << 16U) /* Common memory hold time */
#define CHIP_REGFLD_EXMC_PMEM_MEMHIZ                 (0x00000001UL << 24U) /* Common memory databus HiZ time */

/****** Attribute memory space timing registers (EXMC_PATT) * ******************/
#define CHIP_REGFLD_EXMC_PATT_ATTSET                 (0x000000FFUL <<  0U) /* Attribute memory setup time */
#define CHIP_REGFLD_EXMC_PATT_ATTWAIT                (0x000000FFUL <<  8U) /* Attribute memory wait time */
#define CHIP_REGFLD_EXMC_PATT_ATTHOLD                (0x000000FFUL << 16U) /* Attribute memory hold time */
#define CHIP_REGFLD_EXMC_PATT_ATTHIZ                 (0x000000FFUL << 24U) /* Attribute memory databus HiZ time */

/****** ECC result registers (EXMC_ECCR) ***************************************/
#define CHIP_REGFLD_EXMC_ECCR3_ECC3                  (0xFFFFFFFFUL <<  0U) /* ECC result */



/****** SDRAM Control registers for SDRAM memory bank x (EXMC_SDCRx), x=1..2 ***/
#define CHIP_REGFLD_EXMC_SDCR_NC                     (0x00000003UL <<  0U) /* Number of column bits */
#define CHIP_REGFLD_EXMC_SDCR_NR                     (0x00000003UL <<  2U) /* Number of row bits */
#define CHIP_REGFLD_EXMC_SDCR_MWID                   (0x00000003UL <<  4U) /* Memory data bus width */
#define CHIP_REGFLD_EXMC_SDCR_NB                     (0x00000001UL <<  6U) /* Number of internal bank */
#define CHIP_REGFLD_EXMC_SDCR_CAS                    (0x00000003UL <<  7U) /* CAS latency */
#define CHIP_REGFLD_EXMC_SDCR_WP                     (0x00000001UL <<  9U) /* Write protection */
#define CHIP_REGFLD_EXMC_SDCR_SDCLK                  (0x00000003UL << 10U) /* SDRAM clock configuration */
#define CHIP_REGFLD_EXMC_SDCR_RBURST                 (0x00000001UL << 12U) /* Read burst */
#define CHIP_REGFLD_EXMC_SDCR_RPIPE                  (0x00000003UL << 13U) /* Write protection */

/****** SDRAM Timing registers for SDRAM memory bank x (EXMC_SDTRx), x=1..2 ****/
#define CHIP_REGFLD_EXMC_SDTR_TMRD                   (0x0000000FUL <<  0U) /* Load mode register to active */
#define CHIP_REGFLD_EXMC_SDTR_TXSR                   (0x0000000FUL <<  4U) /* Exit self refresh */
#define CHIP_REGFLD_EXMC_SDTR_TRAS                   (0x0000000FUL <<  8U) /* Self refresh time */
#define CHIP_REGFLD_EXMC_SDTR_TRC                    (0x0000000FUL << 12U) /* Row cycle delay */
#define CHIP_REGFLD_EXMC_SDTR_TWR                    (0x0000000FUL << 16U) /* Write recovery delay */
#define CHIP_REGFLD_EXMC_SDTR_TRP                    (0x0000000FUL << 20U) /* Row precharge delay */
#define CHIP_REGFLD_EXMC_SDTR_TRCD                   (0x0000000FUL << 24U) /* Row to column delay */

/****** SDRAM Command mode register (EXMC_SDCMR) *******************************/
#define CHIP_REGFLD_EXMC_SDCMR_MODE                  (0x00000007UL <<  0U) /* Command mode */
#define CHIP_REGFLD_EXMC_SDCMR_CTB2                  (0x00000001UL <<  3U) /* Command target 2 */
#define CHIP_REGFLD_EXMC_SDCMR_CTB1                  (0x00000001UL <<  4U) /* Command target 1 */
#define CHIP_REGFLD_EXMC_SDCMR_NRFS                  (0x0000000FUL <<  5U) /* Number of auto-refresh */
#define CHIP_REGFLD_EXMC_SDCMR_MRD                   (0x00003FFFUL <<  9U)              /*MRD[12:0] bits (Mode register definition) */

#define CHIP_REGFLDVAL_EXMC_SDCMR_MODE_NORMAL        0
#define CHIP_REGFLDVAL_EXMC_SDCMR_MODE_CLK_CFG_EN    1
#define CHIP_REGFLDVAL_EXMC_SDCMR_MODE_PALL          2
#define CHIP_REGFLDVAL_EXMC_SDCMR_MODE_AUTO_REF      3
#define CHIP_REGFLDVAL_EXMC_SDCMR_MODE_LOAD_MODE     4
#define CHIP_REGFLDVAL_EXMC_SDCMR_MODE_SELF_REF      5
#define CHIP_REGFLDVAL_EXMC_SDCMR_MODE_PWR_DOWN      6

/****** SDRAM refresh timer register (EXMC_SDRTR) ******************************/
#define CHIP_REGFLD_EXMC_SDRTR_CRE                   (0x00000001UL <<  0U) /* Clear refresh error flag */
#define CHIP_REGFLD_EXMC_SDRTR_COUNT                 (0x00001FFFUL <<  3U) /* Refresh timer count */
#define CHIP_REGFLD_EXMC_SDRTR_REIE                  (0x00000001UL << 14U) /* RES interupt enable */

/****** SDRAM Status register (EXMC_SDSR) **************************************/
#define CHIP_REGFLD_EXMC_SDSR_RE                     (0x00000001UL <<  0U) /* Refresh error flag */
#define CHIP_REGFLD_EXMC_SDSR_MODES1                 (0x00000003UL <<  1U) /* Status mode for bank 1 */
#define CHIP_REGFLD_EXMC_SDSR_MODES2                 (0x00000003UL <<  3U) /* Status mode for bank 2 */

#endif // CHIP_STM32H7XX_REGS_EXMC_H_
