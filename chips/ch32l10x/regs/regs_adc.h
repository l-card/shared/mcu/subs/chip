#ifndef REGS_ADC_H
#define REGS_ADC_H

#define CHIP_SUPPORT_ADC_INJECTED               1
#define CHIP_SUPPORT_ADC_EX_CFG                 1

#define CHIP_ADC_POWERUP_TIME_US                1

/* специальные входы для ADC */
#define CHIP_ADC_IN_NUM_TS                     16 /* Temperature Sensor */
#define CHIP_ADC_IN_NUM_VREFINT                17 /* The internal reference voltage VREFINT */
#define CHIP_ADC_IN_NUM_VDDA_DIV2              18 /* VDDA/2 */

#define CHIP_ADC_VREFIN_V                      1.2  /* ном. значение внутреннего опорного напряжения */
#define CHIP_ADC_TS_NOM_T                      25.  /* ном. значение температуры, для которого напряжение должно быть CHIP_ADC_TS_NOM_V */
#define CHIP_ADC_TS_NOM_V                      1.45 /* ном. значение напряжения в В соответствующее температуре CHIP_ADC_TS_NOM_T */
#define CHIP_ADC_TS_SLOPE                      4.2  /* коэф. шкалы для датчика темп. mV/℃ */


#include "stm32_adc_v1_regs.h"

#define CHIP_REGS_ADC1                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC1)

/* Sample time for ADC_LP=1 */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_7_5      0 /* 7.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_11_5     1 /* 11.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_17_5     2 /* 17.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_27_5     3 /* 27.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_47_5     4 /* 47.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_55_5     5 /* 55.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_71_5     6 /* 71.5 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_LP_239_5    7 /* 239.5 ADC clock cycles */

/*********** ADC control register 1 (ADC_CR1) (wch ext) ***********************/
#define CHIP_REGFLD_ADC_CR1_TK_EN               (0x00000001UL << 24U) /* (RW) TKEY (TKEY_F, TKEY_V) module enable */
#define CHIP_REGFLD_ADC_CR1_TK_TUNE             (0x00000001UL << 25U) /* (RW) TKEY module charging current configuration (0 - current is 35uA, 1 - current is reduced half) */
#define CHIP_REGFLD_ADC_CR1_BUF_EN              (0x00000001UL << 26U) /* (RW) Enable input Buffer. */
#define CHIP_REGFLD_ADC_CR1_PGA                 (0x00000003UL << 27U) /* (RW) ADC channel gain configuration (input buffer must be enabled). */

#define CHIP_REGFLDVAL_ADC_CR1_PGA_1            0
#define CHIP_REGFLDVAL_ADC_CR1_PGA_4            1
#define CHIP_REGFLDVAL_ADC_CR1_PGA_16           2
#define CHIP_REGFLDVAL_ADC_CR1_PGA_64           3


/*********** ADC Configuration Register (ADC_CFG) (wch ext) *******************/
#define CHIP_REGFLD_ADC_CFG_TKEY_DRV_OUT_EN(x)  (0x00000001UL << (9U + (x))) /* (RW) TOUCHKEY Multi-mask enable for channel x */
#define CHIP_REGFLD_ADC_CFG_TKEY_DRV_EN         (0x00000001UL <<  8U) /* (RW) TOUCHKEY Multi-mask enable */
#define CHIP_REGFLD_ADC_CFG_ADC_DUTY_EN         (0x00000001UL <<  7U) /* (RW) ADC clock duty cycle control (1 - extend the high level of the ADC clock by 4ns) */
#define CHIP_REGFLD_ADC_CFG_FIFO_EN             (0x00000001UL <<  6U) /* (RW) ADC FIFO enable */
#define CHIP_REGFLD_ADC_CFG_ADC_LP              (0x00000001UL <<  5U) /* (RW) ADC low-power mode control (0 - low power <= 1M, 1 - high pow) */
#define CHIP_REGFLD_ADC_CFG_AWD_RST_EN          (0x00000001UL <<  4U) /* (RW) Analog watchdog reset enable */
#define CHIP_REGFLD_ADC_CFG_ADC_BUF_TRIM        (0x0000000FUL <<  0U) /* (RW) ADCBUFFER calibration control (high bit - sign) */




#endif // REGS_ADC_H

