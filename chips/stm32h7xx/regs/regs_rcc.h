#ifndef CHIP_STM32H7XX_REGS_RCC_H_
#define CHIP_STM32H7XX_REGS_RCC_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

/* Reset and Clock Control */
typedef struct {
    __IO uint32_t CR;             /* RCC clock control register,                                              Address offset: 0x00  */
#if CHIP_DEV_SUPPORT_SEP_CSICFG
    __IO uint32_t HSICFGR;        /* HSI Clock Calibration Register,                                          Address offset: 0x04  */
#else
    __IO uint32_t  RCC_ICSCR;  /* Internal Clock Source Calibration Register */
#endif
    __IO uint32_t CRRCR;          /* Clock Recovery RC  Register,                                             Address offset: 0x08  */
#if CHIP_DEV_SUPPORT_SEP_CSICFG
    __IO uint32_t CSICFGR;        /* CSI Clock Calibration Register,                                          Address offset: 0x0C  */
#else
    uint32_t     RESERVED0;
#endif
    __IO uint32_t CFGR;           /* RCC clock configuration register,                                        Address offset: 0x10  */
    uint32_t     RESERVED1;       /* Reserved,                                                                Address offset: 0x14  */
    __IO uint32_t D1CFGR;         /* RCC Domain 1 configuration register,                                     Address offset: 0x18  */
    __IO uint32_t D2CFGR;         /* RCC Domain 2 configuration register,                                     Address offset: 0x1C  */
    __IO uint32_t D3CFGR;         /* RCC Domain 3 configuration register,                                     Address offset: 0x20  */
    uint32_t     RESERVED2;       /* Reserved,                                                                Address offset: 0x24  */
    __IO uint32_t PLLCKSELR;      /* RCC PLLs Clock Source Selection Register,                                Address offset: 0x28  */
    __IO uint32_t PLLCFGR;        /* RCC PLLs  Configuration Register,                                        Address offset: 0x2C  */
    __IO uint32_t PLL1DIVR;       /* RCC PLL1 Dividers Configuration Register,                                Address offset: 0x30  */
    __IO uint32_t PLL1FRACR;      /* RCC PLL1 Fractional Divider Configuration Register,                      Address offset: 0x34  */
    __IO uint32_t PLL2DIVR;       /* RCC PLL2 Dividers Configuration Register,                                Address offset: 0x38  */
    __IO uint32_t PLL2FRACR;      /* RCC PLL2 Fractional Divider Configuration Register,                      Address offset: 0x3C  */
    __IO uint32_t PLL3DIVR;       /* RCC PLL3 Dividers Configuration Register,                                Address offset: 0x40  */
    __IO uint32_t PLL3FRACR;      /* RCC PLL3 Fractional Divider Configuration Register,                      Address offset: 0x44  */
    uint32_t      RESERVED3;      /* Reserved,                                                                Address offset: 0x48  */
    __IO uint32_t  D1CCIPR;       /* RCC Domain 1 Kernel Clock Configuration Register                         Address offset: 0x4C  */
    __IO uint32_t  D2CCIP1R;      /* RCC Domain 2 Kernel Clock Configuration Register                         Address offset: 0x50  */
    __IO uint32_t  D2CCIP2R;      /* RCC Domain 2 Kernel Clock Configuration Register                         Address offset: 0x54  */
    __IO uint32_t  D3CCIPR;       /* RCC Domain 3 Kernel Clock Configuration Register                         Address offset: 0x58  */
    uint32_t      RESERVED4;      /* Reserved,                                                                Address offset: 0x5C  */
    __IO uint32_t  CIER;          /* RCC Clock Source Interrupt Enable Register                               Address offset: 0x60  */
    __IO uint32_t  CIFR;          /* RCC Clock Source Interrupt Flag Register                                 Address offset: 0x64  */
    __IO uint32_t  CICR;          /* RCC Clock Source Interrupt Clear Register                                Address offset: 0x68  */
    uint32_t     RESERVED5;       /* Reserved,                                                                Address offset: 0x6C  */
    __IO uint32_t  BDCR;          /* RCC Vswitch Backup Domain Control Register,                              Address offset: 0x70  */
    __IO uint32_t  CSR;           /* RCC clock control & status register,                                     Address offset: 0x74  */
    uint32_t     RESERVED6;       /* Reserved,                                                                Address offset: 0x78  */
    __IO uint32_t AHB3RSTR;       /* RCC AHB3 peripheral reset register,                                      Address offset: 0x7C  */
    __IO uint32_t AHB1RSTR;       /* RCC AHB1 peripheral reset register,                                      Address offset: 0x80  */
    __IO uint32_t AHB2RSTR;       /* RCC AHB2 peripheral reset register,                                      Address offset: 0x84  */
    __IO uint32_t AHB4RSTR;       /* RCC AHB4 peripheral reset register,                                      Address offset: 0x88  */
    __IO uint32_t APB3RSTR;       /* RCC APB3 peripheral reset register,                                      Address offset: 0x8C  */
    __IO uint32_t APB1LRSTR;      /* RCC APB1 peripheral reset Low Word register,                             Address offset: 0x90  */
    __IO uint32_t APB1HRSTR;      /* RCC APB1 peripheral reset High Word register,                            Address offset: 0x94  */
    __IO uint32_t APB2RSTR;       /* RCC APB2 peripheral reset register,                                      Address offset: 0x98  */
    __IO uint32_t APB4RSTR;       /* RCC APB4 peripheral reset register,                                      Address offset: 0x9C  */
    __IO uint32_t GCR;            /* RCC RCC Global Control  Register,                                        Address offset: 0xA0  */
    uint32_t     RESERVED8;       /* Reserved,                                                                Address offset: 0xA4  */
    __IO uint32_t D3AMR;          /* RCC Domain 3 Autonomous Mode Register,                                   Address offset: 0xA8  */
    uint32_t     RESERVED11[9];   /* Reserved, 0xAC-0xCC                                                      Address offset: 0xAC  */
    __IO uint32_t RSR;            /* RCC Reset status register,                                               Address offset: 0xD0  */
    __IO uint32_t AHB3ENR;        /* RCC AHB3 peripheral clock  register,                                     Address offset: 0xD4  */
    __IO uint32_t AHB1ENR;        /* RCC AHB1 peripheral clock  register,                                     Address offset: 0xD8  */
    __IO uint32_t AHB2ENR;        /* RCC AHB2 peripheral clock  register,                                     Address offset: 0xDC  */
    __IO uint32_t AHB4ENR;        /* RCC AHB4 peripheral clock  register,                                     Address offset: 0xE0  */
    __IO uint32_t APB3ENR;        /* RCC APB3 peripheral clock  register,                                     Address offset: 0xE4  */
    __IO uint32_t APB1LENR;       /* RCC APB1 peripheral clock  Low Word register,                            Address offset: 0xE8  */
    __IO uint32_t APB1HENR;       /* RCC APB1 peripheral clock  High Word register,                           Address offset: 0xEC  */
    __IO uint32_t APB2ENR;        /* RCC APB2 peripheral clock  register,                                     Address offset: 0xF0  */
    __IO uint32_t APB4ENR;        /* RCC APB4 peripheral clock  register,                                     Address offset: 0xF4  */
    uint32_t      RESERVED12;     /* Reserved,                                                                Address offset: 0xF8  */
    __IO uint32_t AHB3LPENR;      /* RCC AHB3 peripheral sleep clock  register,                               Address offset: 0xFC  */
    __IO uint32_t AHB1LPENR;      /* RCC AHB1 peripheral sleep clock  register,                               Address offset: 0x100 */
    __IO uint32_t AHB2LPENR;      /* RCC AHB2 peripheral sleep clock  register,                               Address offset: 0x104 */
    __IO uint32_t AHB4LPENR;      /* RCC AHB4 peripheral sleep clock  register,                               Address offset: 0x108 */
    __IO uint32_t APB3LPENR;      /* RCC APB3 peripheral sleep clock  register,                               Address offset: 0x10C */
    __IO uint32_t APB1LLPENR;     /* RCC APB1 peripheral sleep clock  Low Word register,                      Address offset: 0x110 */
    __IO uint32_t APB1HLPENR;     /* RCC APB1 peripheral sleep clock  High Word register,                     Address offset: 0x114 */
    __IO uint32_t APB2LPENR;      /* RCC APB2 peripheral sleep clock  register,                               Address offset: 0x118 */
    __IO uint32_t APB4LPENR;      /* RCC APB4 peripheral sleep clock  register,                               Address offset: 0x11C */
    uint32_t     RESERVED13[4];   /* Reserved, 0x120-0x12C                                                    Address offset: 0x120 */
} CHIP_REGS_RCC_T;


typedef struct {
    __IO uint32_t RSR;            /* RCC Reset status register,                                               Address offset: 0x00  */
    __IO uint32_t AHB3ENR;        /* RCC AHB3 peripheral clock  register,                                     Address offset: 0x04  */
    __IO uint32_t AHB1ENR;        /* RCC AHB1 peripheral clock  register,                                     Address offset: 0x08  */
    __IO uint32_t AHB2ENR;        /* RCC AHB2 peripheral clock  register,                                     Address offset: 0x0C  */
    __IO uint32_t AHB4ENR;        /* RCC AHB4 peripheral clock  register,                                     Address offset: 0x10  */
    __IO uint32_t APB3ENR;        /* RCC APB3 peripheral clock  register,                                     Address offset: 0x14  */
    __IO uint32_t APB1LENR;       /* RCC APB1 peripheral clock  Low Word register,                            Address offset: 0x18  */
    __IO uint32_t APB1HENR;       /* RCC APB1 peripheral clock  High Word register,                           Address offset: 0x1C  */
    __IO uint32_t APB2ENR;        /* RCC APB2 peripheral clock  register,                                     Address offset: 0x20  */
    __IO uint32_t APB4ENR;        /* RCC APB4 peripheral clock  register,                                     Address offset: 0x24  */
    uint32_t      RESERVED12;      /* Reserved,                                                               Address offset: 0x28  */
    __IO uint32_t AHB3LPENR;      /* RCC AHB3 peripheral sleep clock  register,                               Address offset: 0x2C  */
    __IO uint32_t AHB1LPENR;      /* RCC AHB1 peripheral sleep clock  register,                               Address offset: 0x30 */
    __IO uint32_t AHB2LPENR;      /* RCC AHB2 peripheral sleep clock  register,                               Address offset: 0x34 */
    __IO uint32_t AHB4LPENR;      /* RCC AHB4 peripheral sleep clock  register,                               Address offset: 0x38 */
    __IO uint32_t APB3LPENR;      /* RCC APB3 peripheral sleep clock  register,                               Address offset: 0x3C */
    __IO uint32_t APB1LLPENR;     /* RCC APB1 peripheral sleep clock  Low Word register,                      Address offset: 0x40 */
    __IO uint32_t APB1HLPENR;     /* RCC APB1 peripheral sleep clock  High Word register,                     Address offset: 0x44 */
    __IO uint32_t APB2LPENR;      /* RCC APB2 peripheral sleep clock  register,                               Address offset: 0x48 */
    __IO uint32_t APB4LPENR;      /* RCC APB4 peripheral sleep clock  register,                               Address offset: 0x4C */
    uint32_t     RESERVED13[4];   /* Reserved, 0x50-0x5C                                                      Address offset: 0x50 */
} CHIP_REGS_RCC_CORE_T;

#define CHIP_REGS_RCC          ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)
#define CHIP_REGS_RCC_C1       ((CHIP_REGS_RCC_CORE_T *) (CHIP_MEMRGN_ADDR_PERIPH_RCC + 0x130))
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGS_RCC_C2       ((CHIP_REGS_RCC_CORE_T *) (CHIP_MEMRGN_ADDR_PERIPH_RCC + 0x190))
#endif


/******************** RCC source control register (RCC_CR) ********************/
#define CHIP_REGFLD_RCC_CR_HSION                    (0x00000001UL <<  0U) /* Internal High Speed clock enable */
#define CHIP_REGFLD_RCC_CR_HSIKERON                 (0x00000001UL <<  1U) /* Internal High Speed clock enable for some IPs Kernel */
#define CHIP_REGFLD_RCC_CR_HSIRDY                   (0x00000001UL <<  2U) /* Internal High Speed clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSIDIV                   (0x00000003UL <<  3U) /* Internal High Speed clock divider selection */
#define CHIP_REGFLD_RCC_CR_HSIDIVF                  (0x00000001UL <<  5U) /* HSI Divider flag */
#define CHIP_REGFLD_RCC_CR_CSION                    (0x00000001UL <<  7U) /* The Internal RC 4MHz oscillator clock enable */
#define CHIP_REGFLD_RCC_CR_CSIRDY                   (0x00000001UL <<  8U) /* The Internal RC 4MHz oscillator clock ready */
#define CHIP_REGFLD_RCC_CR_CSIKERON                 (0x00000001UL <<  9U) /* Internal RC 4MHz oscillator clock enable for some IPs Kernel */
#define CHIP_REGFLD_RCC_CR_HSI48ON                  (0x00000001UL << 12U) /* HSI48 clock enable clock enable  */
#define CHIP_REGFLD_RCC_CR_HSI48RDY                 (0x00000001UL << 13U) /* HSI48 clock ready */
#define CHIP_REGFLD_RCC_CR_D1CKRDY                  (0x00000001UL << 14U) /* D1 domain clocks ready flag  */
#define CHIP_REGFLD_RCC_CR_D2CKRDY                  (0x00000001UL << 15U) /* D2 domain clocks ready flag */
#define CHIP_REGFLD_RCC_CR_HSEON                    (0x00000001UL << 16U) /* External High Speed clock enable */
#define CHIP_REGFLD_RCC_CR_HSERDY                   (0x00000001UL << 17U) /* External High Speed clock ready */
#define CHIP_REGFLD_RCC_CR_HSEBYP                   (0x00000001UL << 18U) /* External High Speed clock Bypass */
#define CHIP_REGFLD_RCC_CR_HSECSSON                 (0x00000001UL << 19U) /* HSE Clock security System enable */
#define CHIP_REGFLD_RCC_CR_PLL1ON                   (0x00000001UL << 24U) /* System PLL1 clock enable */
#define CHIP_REGFLD_RCC_CR_PLL1RDY                  (0x00000001UL << 25U) /* System PLL1 clock ready */
#define CHIP_REGFLD_RCC_CR_PLL2ON                   (0x00000001UL << 26U) /* System PLL2 clock enable */
#define CHIP_REGFLD_RCC_CR_PLL2RDY                  (0x00000001UL << 27U) /* System PLL2 clock ready */
#define CHIP_REGFLD_RCC_CR_PLL3ON                   (0x00000001UL << 28U) /* System PLL3 clock enable */
#define CHIP_REGFLD_RCC_CR_PLL3RDY                  (0x00000001UL << 29U) /* System PLL3 clock ready */

#define CHIP_REGFLDVAL_RCC_CR_HSIDIV_1              0
#define CHIP_REGFLDVAL_RCC_CR_HSIDIV_2              1
#define CHIP_REGFLDVAL_RCC_CR_HSIDIV_4              2
#define CHIP_REGFLDVAL_RCC_CR_HSIDIV_8              3


#if CHIP_DEV_SUPPORT_SEP_CSICFG
/*****************  RCC HSI configuration register (RCC_HSICFGR)  *************/
#define CHIP_REGFLD_RCC_HSICFGR_HSICAL              (0x00000FFFUL <<  0U) /* HSI clock calibration */
#define CHIP_REGFLD_RCC_HSICFGR_HSITRIM             (0x0000007FUL << 24U) /* HSI clock trimming */
#else
/********** RCC Internal Clock Source Calibration Register (RCC_ICSCR) ********/
#define CHIP_REGFLD_RCC_ICSCR_HSICAL                (0x00000FFFUL <<  0U) /* HSI clock calibration */
#define CHIP_REGFLD_RCC_ICSCR_HSITRIM               (0x0000003FUL << 12U) /* HSI clock trimming */
#define CHIP_REGFLD_RCC_ICSCR_CSICAL                (0x000000FFUL << 18U) /* HSI clock calibration */
#define CHIP_REGFLD_RCC_ICSCR_CSITRIM               (0x0000001FUL << 26U) /* CSI clock trimming */
#endif

/****************** RCC clock recovery RC register (RCC_CRRCR)*****************/
#define CHIP_REGFLD_RCC_CRRCR_HSI48CAL              (0x000003FFUL <<  0U) /*  Internal RC 48 MHz clock calibration */

#if CHIP_DEV_SUPPORT_SEP_CSICFG
/****************** RCC CSI configuration register (RCC_CSICFGR) **************/
#define CHIP_REGFLD_RCC_CSICFGR_CSICAL              (0x000003FFUL <<  0U) /* CSI clock calibration */
#define CHIP_REGFLD_RCC_CSICFGR_CSITRIM             (0x0000003FUL << 24U) /* CSI clock trimming */
#endif

/*****************  RCC clock configuration register (RCC_CFGR) ***************/
#define CHIP_REGFLD_RCC_CFGR_SW                     (0x00000007UL <<  0U) /* System clock Switch */
#define CHIP_REGFLD_RCC_CFGR_SWS                    (0x00000007UL <<  3U) /* System Clock Switch Status */
#define CHIP_REGFLD_RCC_CFGR_STOPWUCK               (0x00000001UL <<  6U) /* Wake Up from stop and CSS backup clock selection */
#define CHIP_REGFLD_RCC_CFGR_STOPKERWUCK            (0x00000001UL <<  7U) /* Kernel Clock Selection after a Wake Up from STOP */
#define CHIP_REGFLD_RCC_CFGR_RTCPRE                 (0x0000003FUL <<  8U) /* HSE division factor for RTC clock */
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_REGFLD_RCC_CFGR_HRTIMSEL               (0x00000001UL << 14U) /* High Resolution Timer clock prescaler selection */
#endif
#define CHIP_REGFLD_RCC_CFGR_TIMPRE                 (0x00000001UL << 15U) /* Timers clocks prescaler selection */
#define CHIP_REGFLD_RCC_CFGR_MCO1PRE                (0x0000000FUL << 18U) /* MCO1 prescaler */
#define CHIP_REGFLD_RCC_CFGR_MCO1SEL                (0x00000007UL << 22U) /* Micro-controller clock output 1 */
#define CHIP_REGFLD_RCC_CFGR_MCO2PRE                (0x0000000FUL << 25U) /* MCO2 prescaler */
#define CHIP_REGFLD_RCC_CFGR_MCO2SEL                (0x00000007UL << 29U) /* Micro-controller clock output 2 */

#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSI              0 /* HSI selection as system clock */
#define CHIP_REGFLDVAL_RCC_CFGR_SW_CSI              1 /* CSI selection as system clock */
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSE              2 /* HSE selection as system clock */
#define CHIP_REGFLDVAL_RCC_CFGR_SW_PLL1             3 /* PLL1 selection as system clock */

/********** RCC domain 1 clock configuration register (RCC_D1CFGR) ************/
#define CHIP_REGFLD_RCC_D1CFGR_HPRE                 (0x0000000FUL <<  0U) /* AHB3 prescaler */
#define CHIP_REGFLD_RCC_D1CFGR_D1PPRE               (0x00000007UL <<  4U) /* APB3 prescaler */
#define CHIP_REGFLD_RCC_D1CFGR_D1CPRE               (0x0000000FUL <<  8U) /* Domain 1 Core prescaler */

/********** RCC domain 2 clock configuration register (RCC_D2CFGR) ************/
#define CHIP_REGFLD_RCC_D2CFGR_D2PPRE1              (0x00000007UL <<  4U) /* APB1 prescaler */
#define CHIP_REGFLD_RCC_D2CFGR_D2PPRE2              (0x00000007UL <<  8U) /* APB2 prescaler */

/********** RCC domain 3 clock configuration register (RCC_D3CFGR)*************/
#define CHIP_REGFLD_RCC_D3CFGR_D3PPRE               (0x00000007UL <<  4U) /* APB4 prescaler */

/********** RCC PLLs clock source selection register (RCC_PLLCKSELR) **********/
#define CHIP_REGFLD_RCC_PLLCKSELR_PLLSRC            (0x00000003UL <<  0U) /* DIVMx and PLLs clock source selection */
#define CHIP_REGFLD_RCC_PLLCKSELR_DIVM1             (0x0000003FUL <<  4U) /* Prescaler for PLL1 */
#define CHIP_REGFLD_RCC_PLLCKSELR_DIVM2             (0x0000003FUL << 12U) /* Prescaler for PLL2 */
#define CHIP_REGFLD_RCC_PLLCKSELR_DIVM3             (0x0000003FUL << 20U) /* Prescaler for PLL3 */

#define CHIP_REGFLDVAL_RCC_PLLCKSELR_PLLSRC_HSI     0
#define CHIP_REGFLDVAL_RCC_PLLCKSELR_PLLSRC_CSI     1
#define CHIP_REGFLDVAL_RCC_PLLCKSELR_PLLSRC_HSE     2
#define CHIP_REGFLDVAL_RCC_PLLCKSELR_PLLSRC_DIS     3


/*********** RCC PLLs configuration register (RCC_PLLCFGR) ********************/
#define CHIP_REGFLD_RCC_PLLCFGR_PLL1FRACEN          (0x00000001UL <<  0U) /* PLL1 fractional latch enable */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL1VCOSEL          (0x00000001UL <<  1U) /* PLL1 VCO selection */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL1RGE             (0x00000003UL <<  2U) /* PLL1 input frequency range */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL2FRACEN          (0x00000001UL <<  4U) /* PLL2 fractional latch enable */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL2VCOSEL          (0x00000001UL <<  5U) /* PLL2 VCO selection */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL2RGE             (0x00000003UL <<  6U) /* PLL2 input frequency range */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL3FRACEN          (0x00000001UL <<  8U) /* PLL3 fractional latch enable */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL3VCOSEL          (0x00000001UL <<  9U) /* PLL3 VCO selection */
#define CHIP_REGFLD_RCC_PLLCFGR_PLL3RGE             (0x00000003UL << 10U) /* PLL3 input frequency range */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVP1EN             (0x00000001UL << 16U) /* PLL1 DIVP divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVQ1EN             (0x00000001UL << 17U) /* PLL1 DIVQ divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVR1EN             (0x00000001UL << 18U) /* PLL1 DIVR divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVP2EN             (0x00000001UL << 19U) /* PLL2 DIVP divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVQ2EN             (0x00000001UL << 20U) /* PLL2 DIVQ divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVR2EN             (0x00000001UL << 21U) /* PLL1 DIVR divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVP3EN             (0x00000001UL << 22U) /* PLL3 DIVP divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVQ3EN             (0x00000001UL << 23U) /* PLL1 DIVQ divider output enable */
#define CHIP_REGFLD_RCC_PLLCFGR_DIVR3EN             (0x00000001UL << 24U) /* PLL1 DIVR divider output enable */


/*********** RCC PLL1 dividers configuration register (RCC_PLL1DIVR) **********/
#define CHIP_REGFLD_RCC_PLL1DIVR_N1                 (0x000001FFUL <<  0U)
#define CHIP_REGFLD_RCC_PLL1DIVR_P1                 (0x0000007FUL <<  9U)
#define CHIP_REGFLD_RCC_PLL1DIVR_Q1                 (0x0000007FUL << 16U)
#define CHIP_REGFLD_RCC_PLL1DIVR_R1                 (0x0000007FUL << 24U)

/*********** RCC PLL1 fractional divider register (RCC_PLL1FRACR) *************/
#define CHIP_REGFLD_RCC_PLL1FRACR_FRACN1            (0x00001FFFUL <<  3U)

/*********** RCC PLL2 dividers configuration register (RCC_PLL2DIVR) **********/
#define CHIP_REGFLD_RCC_PLL2DIVR_N2                 (0x000001FFUL <<  0U)
#define CHIP_REGFLD_RCC_PLL2DIVR_P2                 (0x0000007FUL <<  9U)
#define CHIP_REGFLD_RCC_PLL2DIVR_Q2                 (0x0000007FUL << 16U)
#define CHIP_REGFLD_RCC_PLL2DIVR_R2                 (0x0000007FUL << 24U)

/*********** RCC PLL2 fractional divider register (RCC_PLL2FRACR) *************/
#define CHIP_REGFLD_RCC_PLL2FRACR_FRACN2            (0x00001FFFUL <<  3U)

/*********** RCC PLL3 dividers configuration register (RCC_PLL3DIVR) **********/
#define CHIP_REGFLD_RCC_PLL3DIVR_N3                 (0x000001FFUL <<  0U)
#define CHIP_REGFLD_RCC_PLL3DIVR_P3                 (0x0000007FUL <<  9U)
#define CHIP_REGFLD_RCC_PLL3DIVR_Q3                 (0x0000007FUL << 16U)
#define CHIP_REGFLD_RCC_PLL3DIVR_R3                 (0x0000007FUL << 24U)

/*********** RCC PLL3 fractional divider register (RCC_PLL3FRACR) *************/
#define CHIP_REGFLD_RCC_PLL3FRACR_FRACN3            (0x00001FFFUL <<  3U)

/*********** RCC domain 1 kernel clock configuration register (RCC_D1CCIPR) ***/
#define CHIP_REGFLD_RCC_D1CCIPR_EXMCSEL             (0x00000003UL <<  0U)
#define CHIP_REGFLD_RCC_D1CCIPR_FMCSEL              (0x00000003UL <<  0U)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_REGFLD_RCC_D1CCIPR_OCTOSPISEL          (0x00000003UL <<  4U)
#else
#define CHIP_REGFLD_RCC_D1CCIPR_QSPISEL             (0x00000003UL <<  4U)
#endif
#if CHIP_DEV_SUPPORT_DSI
#define CHIP_REGFLD_RCC_D1CCIPR_DSISEL              (0x00000001UL <<  8U)
#endif
#define CHIP_REGFLD_RCC_D1CCIPR_SDMMCSEL            (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_D1CCIPR_CKPERSEL            (0x00000003UL << 28U)

/*********** RCC domain 2 kernel clock configuration register (RCC_D2CCIP1R) **/
#define CHIP_REGFLD_RCC_D2CCIP1R_SAI1SEL            (0x00000007UL <<  0U)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_REGFLD_RCC_D2CCIP1R_SAI23SEL           (0x00000007UL <<  6U)
#endif
#define CHIP_REGFLD_RCC_D2CCIP1R_SPI123SEL          (0x00000007UL << 12U)
#define CHIP_REGFLD_RCC_D2CCIP1R_SPI45SEL           (0x00000007UL << 16U)
#define CHIP_REGFLD_RCC_D2CCIP1R_SPDIFSEL           (0x00000003UL << 20U)
#define CHIP_REGFLD_RCC_D2CCIP1R_DFSDM1SEL          (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_D2CCIP1R_FDCANSEL           (0x00000003UL << 28U)
#define CHIP_REGFLD_RCC_D2CCIP1R_SWPSEL             (0x00000001UL << 31U)

/*********** RCC domain 2 kernel clock configuration register (RCC_D2CCIP2R) **/
#define CHIP_REGFLD_RCC_D2CCIP2R_USART28SEL         (0x00000007UL <<  0U)
#define CHIP_REGFLD_RCC_D2CCIP2R_USART16SEL         (0x00000007UL <<  3U)
#define CHIP_REGFLD_RCC_D2CCIP2R_RNGSEL             (0x00000003UL <<  8U)
#define CHIP_REGFLD_RCC_D2CCIP2R_I2C123SEL          (0x00000003UL << 12U)
#define CHIP_REGFLD_RCC_D2CCIP2R_USBSEL             (0x00000003UL << 20U)
#define CHIP_REGFLD_RCC_D2CCIP2R_CECSEL             (0x00000003UL << 22U)
#define CHIP_REGFLD_RCC_D2CCIP2R_LPTIM1SEL          (0x00000007UL << 28U)

/*********** RCC Domain 3 Kernel Clock Configuration Register (RCC_D3CCIPR) ***/
#define CHIP_REGFLD_RCC_D3CCIPR_LPUART1SEL          (0x00000007UL <<  0U)
#define CHIP_REGFLD_RCC_D3CCIPR_I2C4SEL             (0x00000003UL <<  8U)
#define CHIP_REGFLD_RCC_D3CCIPR_LPTIM2SEL           (0x00000007UL << 10U)
#define CHIP_REGFLD_RCC_D3CCIPR_LPTIM345SEL         (0x00000007UL << 13U)
#define CHIP_REGFLD_RCC_D3CCIPR_ADCSEL              (0x00000003UL << 16U)
#define CHIP_REGFLD_RCC_D3CCIPR_SAI4ASEL            (0x00000007UL << 21U)
#define CHIP_REGFLD_RCC_D3CCIPR_SAI4BSEL            (0x00000007UL << 24U)
#define CHIP_REGFLD_RCC_D3CCIPR_SPI6SEL             (0x00000007UL << 28U)

/*********** RCC Clock Source Interrupt Enable Register (RCC_CIER) ************/
#define CHIP_REGFLD_RCC_CIER_LSIRDYIE               (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_CIER_LSERDYIE               (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_CIER_HSIRDYIE               (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_CIER_HSERDYIE               (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_CIER_CSIRDYIE               (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_CIER_HSI48RDYIE             (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_CIER_PLL1RDYIE              (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_CIER_PLL2RDYIE              (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_CIER_PLL3RDYIE              (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_CIER_LSECSSIE               (0x00000001UL <<  9U)

/*********** RCC clock source interrupt flag register (RCC_CIFR) **************/
#define CHIP_REGFLD_RCC_CIFR_LSIRDYF                (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_CIFR_LSERDYF                (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_CIFR_HSIRDYF                (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_CIFR_HSERDYF                (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_CIFR_CSIRDYF                (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_CIFR_HSI48RDYF              (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_CIFR_PLL1RDYF               (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_CIFR_PLL2RDYF               (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_CIFR_PLL3RDYF               (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_CIFR_LSECSSF                (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_CIFR_HSECSSF                (0x00000001UL << 10U)

/*********** RCC clock source interrupt clear register (RCC_CICR) *************/
#define CHIP_REGFLD_RCC_CICR_LSIRDYC                (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_CICR_LSERDYC                (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_CICR_HSIRDYC                (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_CICR_HSERDYC                (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_CICR_CSIRDYC                (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_CICR_HSI48RDYC              (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_CICR_PLLRDYC                (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_CICR_PLL2RDYC               (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_CICR_PLL3RDYC               (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_CICR_LSECSSC                (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_CICR_HSECSSC                (0x00000001UL << 10U)

/*********** RCC Backup domain control register (RCC_BDCR) ********************/
#define CHIP_REGFLD_RCC_BDCR_LSEON                  (0x00000001UL <<  0U) /* LSE oscillator enabled */
#define CHIP_REGFLD_RCC_BDCR_LSERDY                 (0x00000001UL <<  1U) /* LSE oscillator ready */
#define CHIP_REGFLD_RCC_BDCR_LSEBYP                 (0x00000001UL <<  2U) /* LSE oscillator bypass */
#define CHIP_REGFLD_RCC_BDCR_LSEDRV                 (0x00000003UL <<  3U) /* LSE oscillator driving capability */
#define CHIP_REGFLD_RCC_BDCR_LSECSSON               (0x00000001UL <<  5U) /* LSE clock security system enable */
#define CHIP_REGFLD_RCC_BDCR_LSECSSD                (0x00000001UL <<  6U) /* LSE clock security system failure detection */
#define CHIP_REGFLD_RCC_BDCR_RTCSEL                 (0x00000003UL <<  8U) /* RTC clock source selection */
#define CHIP_REGFLD_RCC_BDCR_RTCEN                  (0x00000001UL << 15U) /* RTC clock enable */
#define CHIP_REGFLD_RCC_BDCR_BDRST                  (0x00000001UL << 16U) /* Backup domain software reset */

/*********** RCC clock control and status register (RCC_CSR) ******************/
#define CHIP_REGFLD_RCC_CSR_LSION                   (0x00000001UL <<  0U) /* LSI oscillator enable */
#define CHIP_REGFLD_RCC_CSR_LSIRDY                  (0x00000001UL <<  1U) /* LSI oscillator ready */

/*********** RCC AHB3 reset register (RCC_AHB3RSTR) ***************************/
#define CHIP_REGFLD_RCC_AHB3RSTR_MDMARST            (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB3RSTR_DMA2DRST           (0x00000001UL <<  4U)
#if CHIP_DEV_SUPPORT_JPGDEC
#define CHIP_REGFLD_RCC_AHB3RSTR_JPGDECRST          (0x00000001UL <<  5U)
#endif
#define CHIP_REGFLD_RCC_AHB3RSTR_FMCRST             (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_AHB3RSTR_EXMCRST            (0x00000001UL << 12U)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_REGFLD_RCC_AHB3RSTR_OCTOSPI1RST        (0x00000001UL << 14U)
#else
#define CHIP_REGFLD_RCC_AHB3RSTR_QSPIRST            (0x00000001UL << 14U)
#endif
#define CHIP_REGFLD_RCC_AHB3RSTR_SDMMC1RST          (0x00000001UL << 16U)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_REGFLD_RCC_AHB3RSTR_OCTOSPI2RST        (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_AHB3RSTR_IOMNGRRST          (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_AHB3RSTR_OTFD1RST           (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_AHB3RSTR_OTFD2RST           (0x00000001UL << 23U)
#endif
#if CHIP_DEV_SUPPORT_CPURST
#define CHIP_REGFLD_RCC_AHB3RSTR_CPURST             (0x00000001UL << 31U)
#endif



/*********** RCC AHB1 peripheral reset register (RCC_AHB1RSTR) *****************/
#define CHIP_REGFLD_RCC_AHB1RSTR_DMA1RST            (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB1RSTR_DMA2RST            (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHB1RSTR_ADC12RST           (0x00000001UL <<  5U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_AHB1RSTR_ARTRST             (0x00000001UL << 14U)
#endif
#define CHIP_REGFLD_RCC_AHB1RSTR_ETH1MACRST         (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_AHB1RSTR_USB1OTGRST         (0x00000001UL << 25U)
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_REGFLD_RCC_AHB1RSTR_USB2OTGRST         (0x00000001UL << 27U)
#endif

/*********** RCC AHB2 peripheral reset register (RCC_AHB2RSTR)*****************/
#define CHIP_REGFLD_RCC_AHB2RSTR_CAMITFRST          (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB2RSTR_CRYPRST            (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_AHB2RSTR_HASHRST            (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_AHB2RSTR_RNGRST             (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_AHB2RSTR_SDMMC2RST          (0x00000001UL <<  9U)
#if CHIP_DEV_SUPPORT_FMAC
#define CHIP_REGFLD_RCC_AHB2RSTR_FMACRST            (0x00000001UL << 16U)
#endif
#if CHIP_DEV_SUPPORT_CORDIC
#define CHIP_REGFLD_RCC_AHB2RSTR_CORDICRST          (0x00000001UL << 17U)
#endif

/*********** RCC AHB4 peripheral reset register (RCC_AHB4RSTR) ****************/
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOARST           (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOBRST           (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOCRST           (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIODRST           (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOERST           (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOFRST           (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOGRST           (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOHRST           (0x00000001UL <<  7U)
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOIRST           (0x00000001UL <<  8U)
#endif
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOJRST           (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_AHB4RSTR_GPIOKRST           (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_AHB4RSTR_CRCRST             (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_AHB4RSTR_BDMARST            (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_AHB4RSTR_ADC3RST            (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_AHB4RSTR_HSEMRST            (0x00000001UL << 25U)

/*********** RCC APB3 peripheral reset register (RCC_APB3RSTR) ****************/
#define CHIP_REGFLD_RCC_APB3RSTR_LTDCRST            (0x00000001UL <<  3U)
#if CHIP_DEV_SUPPORT_DSI
#define CHIP_REGFLD_RCC_APB3RSTR_DSIRST             (0x00000001UL <<  4U)
#endif

/*********** RCC APB1 peripheral reset register (RCC_APB1LRSTR) ***************/
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM2RST           (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM3RST           (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM4RST           (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM5RST           (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM6RST           (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM7RST           (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM12RST          (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM13RST          (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APB1LRSTR_TIM14RST          (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_APB1LRSTR_LPTIM1RST         (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_APB1LRSTR_SPI2RST           (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APB1LRSTR_SPI3RST           (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APB1LRSTR_SPDIFRXRST        (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB1LRSTR_USART2RST         (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APB1LRSTR_USART3RST         (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APB1LRSTR_UART4RST          (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_APB1LRSTR_UART5RST          (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APB1LRSTR_I2C1RST           (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_APB1LRSTR_I2C2RST           (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_APB1LRSTR_I2C3RST           (0x00000001UL << 23U)
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_REGFLD_RCC_APB1LRSTR_I2C5RST           (0x00000001UL << 25U)
#endif
#define CHIP_REGFLD_RCC_APB1LRSTR_CECRST            (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_APB1LRSTR_DAC12RST          (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_APB1LRSTR_UART7RST          (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_APB1LRSTR_UART8RST          (0x00000001UL << 31U)

/*********** RCC APB1 peripheral reset register (RCC_APB1HRSTR) ***************/
#define CHIP_REGFLD_RCC_APB1HRSTR_CRSRST            (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB1HRSTR_SWPMIRST          (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APB1HRSTR_OPAMPRST          (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB1HRSTR_MDIOSRST          (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB1HRSTR_FDCANRST          (0x00000001UL <<  8U)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_REGFLD_RCC_APB1HRSTR_TIM23RST          (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_APB1HRSTR_TIM24RST          (0x00000001UL << 25U)
#endif

/*********** RCC APB2 peripheral reset register (RCC_APB2RSTR) ****************/
#define CHIP_REGFLD_RCC_APB2RSTR_TIM1RST            (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APB2RSTR_TIM8RST            (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB2RSTR_USART1RST          (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB2RSTR_USART6RST          (0x00000001UL <<  5U)
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_REGFLD_RCC_APB2RSTR_UART9RST           (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_APB2RSTR_USART10RST         (0x00000001UL <<  7U)
#endif
#define CHIP_REGFLD_RCC_APB2RSTR_SPI1RST            (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APB2RSTR_SPI4RST            (0x00000001UL << 13U)
#define CHIP_REGFLD_RCC_APB2RSTR_TIM15RST           (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB2RSTR_TIM16RST           (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APB2RSTR_TIM17RST           (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APB2RSTR_SPI5RST            (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APB2RSTR_SAI1RST            (0x00000001UL << 22U)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_REGFLD_RCC_APB2RSTR_SAI2RST            (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_APB2RSTR_SAI3RST            (0x00000001UL << 24U)
#endif
#if !CHIP_DEV_DFSDM1_ALTPOS
#define CHIP_REGFLD_RCC_APB2RSTR_DFSDM1RST          (0x00000001UL << 28U)
#endif
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_REGFLD_RCC_APB2RSTR_HRTIMRST           (0x00000001UL << 29U)
#endif
#if CHIP_DEV_DFSDM1_ALTPOS
#define CHIP_REGFLD_RCC_APB2RSTR_DFSDM1RST          (0x00000001UL << 30U)
#endif

/*********** RCC APB4 peripheral reset register (RCC_APB4RSTR) ****************/
#define CHIP_REGFLD_RCC_APB4RSTR_SYSCFGRST          (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB4RSTR_LPUART1RST         (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_APB4RSTR_SPI6RST            (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB4RSTR_I2C4RST            (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APB4RSTR_LPTIM2RST          (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_APB4RSTR_LPTIM3RST          (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_APB4RSTR_LPTIM4RST          (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APB4RSTR_LPTIM5RST          (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APB4RSTR_COMP12RST          (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APB4RSTR_VREFRST            (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APB4RSTR_SAI4RST            (0x00000001UL << 21U)
#if CHIP_DEV_SUPPORT_DTS
#define CHIP_REGFLD_RCC_APB4RSTR_DTSRST             (0x00000001UL << 26U)
#endif

/*********** RCC global control register (RCC_GCR) ****************************/
#define CHIP_REGFLD_RCC_GCR_WW1RSC                  (0x00000001UL <<  0U) /* WWDG1 reset scope control */
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_GCR_WW2RSC                  (0x00000001UL <<  1U) /* WWDG2 reset scope control */
#define CHIP_REGFLD_RCC_GCR_BOOT_C1                 (0x00000001UL <<  2U) /* Allows CPU1 to boot */
#define CHIP_REGFLD_RCC_GCR_BOOT_C2                 (0x00000001UL <<  3U) /* Allows CPU2 to boot */
#endif

/*********** RCC D3 Autonomous mode register (RCC_D3AMR) * ********************/
#define CHIP_REGFLD_RCC_D3AMR_BDMAAMEN              (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_D3AMR_LPUART1AMEN           (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_D3AMR_SPI6AMEN              (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_D3AMR_I2C4AMEN              (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_D3AMR_LPTIM2AMEN            (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_D3AMR_LPTIM3AMEN            (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_D3AMR_LPTIM4AMEN            (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_D3AMR_LPTIM5AMEN            (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_D3AMR_COMP12AMEN            (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_D3AMR_VREFAMEN              (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_D3AMR_RTCAMEN               (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_D3AMR_CRCAMEN               (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_D3AMR_SAI4AMEN              (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_D3AMR_ADC3AMEN              (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_D3AMR_BKPRAMAMEN            (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_D3AMR_SRAM4AMEN             (0x00000001UL << 29U)


/*********** RCC reset status register (RCC_RSR) ******************************/
#define CHIP_REGFLD_RCC_RSR_RMVF                    (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_RSR_C1RSTF                  (0x00000001UL << 17U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_RSR_C2RSTF                  (0x00000001UL << 18U)
#endif
#define CHIP_REGFLD_RCC_RSR_D1RSTF                  (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_RSR_D2RSTF                  (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_RSR_BORRSTF                 (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_RSR_PINRSTF                 (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_RSR_PORRSTF                 (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_RSR_SFT1RSTF                (0x00000001UL << 24U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_RSR_SFT2RSTF                (0x00000001UL << 25U)
#endif
#define CHIP_REGFLD_RCC_RSR_IWDG1RSTF               (0x00000001UL << 26U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_RSR_IWDG2RSTF               (0x00000001UL << 27U)
#endif
#define CHIP_REGFLD_RCC_RSR_WWDG1RSTF               (0x00000001UL << 28U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_RSR_WWDG2RSTF               (0x00000001UL << 29U)
#endif
#define CHIP_REGFLD_RCC_RSR_LPWR1RSTF               (0x00000001UL << 30U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_RSR_LPWR2RSTF               (0x00000001UL << 31U)
#endif

/* ********* RCC AHB3 clock register (RCC_AHB3ENR) ****************************/
#define CHIP_REGFLD_RCC_AHB3ENR_MDMAEN              (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB3ENR_DMA2DEN             (0x00000001UL <<  4U)
#if CHIP_DEV_SUPPORT_JPGDEC
#define CHIP_REGFLD_RCC_AHB3ENR_JPGDECEN            (0x00000001UL <<  5U)
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_AHB3ENR_FLITFEN             (0x00000001UL <<  8U)
#endif
#define CHIP_REGFLD_RCC_AHB3ENR_FMCEN               (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_AHB3ENR_EXMCEN              (0x00000001UL << 12U)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_REGFLD_RCC_AHB3ENR_OCTOSPI1EN          (0x00000001UL << 14U)
#else
#define CHIP_REGFLD_RCC_AHB3ENR_QSPIEN              (0x00000001UL << 14U)
#endif
#define CHIP_REGFLD_RCC_AHB3ENR_SDMMC1EN            (0x00000001UL << 16U)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_REGFLD_RCC_AHB3ENR_OCTOSPI2EN          (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_AHB3ENR_IOMNGREN            (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_AHB3ENR_OTFD1EN             (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_AHB3ENR_OTFD2EN             (0x00000001UL << 23U)
#endif
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_AHB3ENR_DTCM1EN             (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_AHB3ENR_DTCM2EN             (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_AHB3ENR_ITCMEN              (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_AHB3ENR_AXISRAMEN           (0x00000001UL << 31U)
#endif
#define CHIP_REGMSK_RCC_AHB3ENR_RESERVED            (0x0F16AECEUL)


/*********** RCC AHB1 clock register (RCC_AHB1ENR) *****************************/
#define CHIP_REGFLD_RCC_AHB1ENR_DMA1EN              (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB1ENR_DMA2EN              (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHB1ENR_ADC12EN             (0x00000001UL <<  5U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_AHB1ENR_ARTEN               (0x00000001UL << 14U)
#endif
#define CHIP_REGFLD_RCC_AHB1ENR_ETH1MACEN           (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_AHB1ENR_ETH1TXEN            (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_AHB1ENR_ETH1RXEN            (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_AHB1ENR_USB1OTGHSEN         (0x00000001UL << 25U)
#define CHIP_REGFLD_RCC_AHB1ENR_USB1OTGHSULPIEN     (0x00000001UL << 26U)
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_REGFLD_RCC_AHB1ENR_USB2OTGHSEN         (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_AHB1ENR_USB2OTGHSULPIEN     (0x00000001UL << 28U) /** @note В RM - 18, в Cube - 28 */
#endif
#define CHIP_REGMSK_RCC_AHB1ENR_RESERVED            (0xF1F83FDC)

/*********** RCC AHB2 clock register (RCC_AHB2ENR) ****************************/
#define CHIP_REGFLD_RCC_AHB2ENR_CAMITFEN            (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB2ENR_CRYPEN              (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_AHB2ENR_HASHEN              (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_AHB2ENR_RNGEN               (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_AHB2ENR_SDMMC2EN            (0x00000001UL <<  9U)
#if CHIP_DEV_SUPPORT_FMAC
#define CHIP_REGFLD_RCC_AHB2ENR_FMACEN              (0x00000001UL << 16U)
#endif
#if CHIP_DEV_SUPPORT_CORDIC
#define CHIP_REGFLD_RCC_AHB2ENR_CORDICEN            (0x00000001UL << 17U)
#endif
#define CHIP_REGFLD_RCC_AHB2ENR_SRAM1EN             (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_AHB2ENR_SRAM2EN             (0x00000001UL << 30U)
#if CHIP_DEV_SUPPORT_SRAM3
#define CHIP_REGFLD_RCC_AHB2ENR_SRAM3EN             (0x00000001UL << 31U)
#endif
#define CHIP_REGMSK_RCC_AHB2ENR_RESERVED            (0x1FFCFD8EUL)

/*********** RCC AHB4 clock register (RCC_AHB4ENR) ****************************/
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOAEN             (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOBEN             (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOCEN             (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIODEN             (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOEEN             (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOFEN             (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOGEN             (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOHEN             (0x00000001UL <<  7U)
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOIEN             (0x00000001UL <<  8U)
#endif
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOJEN             (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_AHB4ENR_GPIOKEN             (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_AHB4ENR_CRCEN               (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_AHB4ENR_BDMAEN              (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_AHB4ENR_ADC3EN              (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_AHB4ENR_HSEMEN              (0x00000001UL << 25U)
#define CHIP_REGFLD_RCC_AHB4ENR_BKPRAMEN            (0x00000001UL << 28U)
#define CHIP_REGMSK_RCC_AHB4ENR_RESERVED            (0xECD7F800UL)

/*********** RCC APB3 clock register (RCC_APB3ENR) ****************************/
#define CHIP_REGFLD_RCC_APB3ENR_LTDCEN              (0x00000001UL <<  3U)
#if CHIP_DEV_SUPPORT_DSI
#define CHIP_REGFLD_RCC_APB3ENR_DSIEN               (0x00000001UL <<  4U)
#endif
#define CHIP_REGFLD_RCC_APB3ENR_WWDG1EN             (0x00000001UL <<  6U)

/*********** RCC APB1 clock register (RCC_APB1LENR) ***************************/
#define CHIP_REGFLD_RCC_APB1LENR_TIM2EN             (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM3EN             (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM4EN             (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM5EN             (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM6EN             (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM7EN             (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM12EN            (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM13EN            (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APB1LENR_TIM14EN            (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_APB1LENR_LPTIM1EN           (0x00000001UL <<  9U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_APB1LENR_WWDG2EN            (0x00000001UL << 11U)
#endif
#define CHIP_REGFLD_RCC_APB1LENR_SPI2EN             (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APB1LENR_SPI3EN             (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APB1LENR_SPDIFRXEN          (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB1LENR_USART2EN           (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APB1LENR_USART3EN           (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APB1LENR_UART4EN            (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_APB1LENR_UART5EN            (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APB1LENR_I2C1EN             (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_APB1LENR_I2C2EN             (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_APB1LENR_I2C3EN             (0x00000001UL << 23U)
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_REGFLD_RCC_APB1LENR_I2C5EN             (0x00000001UL << 25U)
#endif
#define CHIP_REGFLD_RCC_APB1LENR_CECEN              (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_APB1LENR_DAC12EN            (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_APB1LENR_UART7EN            (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_APB1LENR_UART8EN            (0x00000001UL << 31U)
#define CHIP_REGMSK_RCC_APB1LENR_RESERVED           (0x15003400UL)

/*********** RCC APB1 clock register (RCC_APB1HENR) ***************************/
#define CHIP_REGFLD_RCC_APB1HENR_CRSEN              (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB1HENR_SWPMIEN            (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APB1HENR_OPAMPEN            (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB1HENR_MDIOSEN            (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB1HENR_FDCANEN            (0x00000001UL <<  8U)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_REGFLD_RCC_APB1HENR_TIM23EN            (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_APB1HENR_TIM24EN            (0x00000001UL << 25U)
#endif
#define CHIP_REGMSK_RCC_APB1HENR_RESERVED           (0xFCFFFEC9UL)

/*********** RCC APB2 clock register (RCC_APB2ENR) ****************************/
#define CHIP_REGFLD_RCC_APB2ENR_TIM1EN              (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APB2ENR_TIM8EN              (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB2ENR_USART1EN            (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB2ENR_USART6EN            (0x00000001UL <<  5U)
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_REGFLD_RCC_APB2ENR_UART9EN             (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_APB2ENR_USART10EN           (0x00000001UL <<  7U)
#endif
#define CHIP_REGFLD_RCC_APB2ENR_SPI1EN              (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APB2ENR_SPI4EN              (0x00000001UL << 13U)
#define CHIP_REGFLD_RCC_APB2ENR_TIM15EN             (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB2ENR_TIM16EN             (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APB2ENR_TIM17EN             (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APB2ENR_SPI5EN              (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APB2ENR_SAI1EN              (0x00000001UL << 22U)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_REGFLD_RCC_APB2ENR_SAI2EN              (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_APB2ENR_SAI3EN              (0x00000001UL << 24U)
#endif
#if !CHIP_DEV_DFSDM1_ALTPOS
#define CHIP_REGFLD_RCC_APB2ENR_DFSDM1EN            (0x00000001UL << 28U)
#endif
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_REGFLD_RCC_APB2ENR_HRTIMEN             (0x00000001UL << 29U)
#endif
#if CHIP_DEV_DFSDM1_ALTPOS
#define CHIP_REGFLD_RCC_APB2ENR_DFSDM1EN            (0x00000001UL << 30U)
#endif
#define CHIP_REGMSK_RCC_APB2ENR_RESERVED            (0x8E28CF0CUL)

/*********** RCC APB4 clock register (RCC_APB4ENR) ****************************/
#define CHIP_REGFLD_RCC_APB4ENR_SYSCFGEN            (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB4ENR_LPUART1EN           (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_APB4ENR_SPI6EN              (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB4ENR_I2C4EN              (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APB4ENR_LPTIM2EN            (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_APB4ENR_LPTIM3EN            (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_APB4ENR_LPTIM4EN            (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APB4ENR_LPTIM5EN            (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APB4ENR_COMP12EN            (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APB4ENR_VREFEN              (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APB4ENR_RTCAPBEN            (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB4ENR_SAI4EN              (0x00000001UL << 21U)
#if CHIP_DEV_SUPPORT_DTS
#define CHIP_REGFLD_RCC_APB4ENR_DTSEN               (0x00000001UL << 26U)
#endif
#define CHIP_REGMSK_RCC_APB4ENR_RESERVED            (0xFBDE2155UL)

/*********** RCC AHB3 Sleep clock register (RCC_AHB3LPENR) ********************/
#define CHIP_REGFLD_RCC_AHB3LPENR_MDMALPEN          (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB3LPENR_DMA2DLPEN         (0x00000001UL <<  4U)
#if CHIP_DEV_SUPPORT_JPGDEC
#define CHIP_REGFLD_RCC_AHB3LPENR_JPGDECLPEN        (0x00000001UL <<  5U)
#endif
#define CHIP_REGFLD_RCC_AHB3LPENR_FLASHLPEN         (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_AHB3LPENR_FMCLPEN           (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_AHB3LPENR_EXMCLPEN          (0x00000001UL << 12U)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_REGFLD_RCC_AHB3LPENR_OCTO1LPEN         (0x00000001UL << 14U)
#else
#define CHIP_REGFLD_RCC_AHB3LPENR_QSPILPEN          (0x00000001UL << 14U)
#endif
#define CHIP_REGFLD_RCC_AHB3LPENR_SDMMC1LPEN        (0x00000001UL << 16U)
#if CHIP_DEV_SUPPORT_OCTOSPI
#define CHIP_REGFLD_RCC_AHB3LPENR_OCTO2LPEN         (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_AHB3LPENR_IOMNGRLPEN        (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_AHB3LPENR_OTFD1LPEN         (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_AHB3LPENR_OTFD2LPEN         (0x00000001UL << 23U)
#endif
#define CHIP_REGFLD_RCC_AHB3LPENR_DTCM1LPEN         (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_AHB3LPENR_DTCM2LPEN         (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_AHB3LPENR_ITCMLPEN          (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_AHB3LPENR_AXISRAMLPEN       (0x00000001UL << 31U)

/*********** RCC AHB1 Sleep clock register (RCC_AHB1LPENR) ********************/
#define CHIP_REGFLD_RCC_AHB1LPENR_DMA1LPEN          (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB1LPENR_DMA2LPEN          (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHB1LPENR_ADC12LPEN         (0x00000001UL <<  5U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_AHB1LPENR_ARTLPEN           (0x00000001UL << 14U)
#endif
#define CHIP_REGFLD_RCC_AHB1LPENR_ETH1MACLPEN       (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_AHB1LPENR_ETH1TXLPEN        (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_AHB1LPENR_ETH1RXLPEN        (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_AHB1LPENR_USB1OTGHSLPEN     (0x00000001UL << 25U)
#define CHIP_REGFLD_RCC_AHB1LPENR_USB1OTGHSULPILPEN (0x00000001UL << 26U)
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_REGFLD_RCC_AHB1LPENR_USB2OTGFSLPEN     (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_AHB1LPENR_USB2OTGFSULPILPEN (0x00000001UL << 28U)
#endif

/*********** RCC AHB2 Sleep clock register (RCC_AHB2LPENR) ********************/
#define CHIP_REGFLD_RCC_AHB2LPENR_CAMITFLPEN        (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB2LPENR_CRYPLPEN          (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_AHB2LPENR_HASHLPEN          (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_AHB2LPENR_RNGLPEN           (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_AHB2LPENR_SDMMC2LPEN        (0x00000001UL <<  9U)
#if CHIP_DEV_SUPPORT_FMAC
#define CHIP_REGFLD_RCC_AHB2LPENR_FMACLPEN          (0x00000001UL << 16U)
#endif
#if CHIP_DEV_SUPPORT_CORDIC
#define CHIP_REGFLD_RCC_AHB2LPENR_CORDICLPEN        (0x00000001UL << 17U)
#endif
#define CHIP_REGFLD_RCC_AHB2LPENR_SRAM1LPEN         (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_AHB2LPENR_SRAM2LPEN         (0x00000001UL << 30U)
#if CHIP_DEV_SUPPORT_SRAM3
#define CHIP_REGFLD_RCC_AHB2LPENR_SRAM3LPEN         (0x00000001UL << 31U)
#endif

/*********** RCC AHB4 Sleep Clock Register (RCC_AHB4LPENR) ********************/
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOALPEN         (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOBLPEN         (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOCLPEN         (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIODLPEN         (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOELPEN         (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOFLPEN         (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOGLPEN         (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOHLPEN         (0x00000001UL <<  7U)
#if CHIP_DEV_SUPPORT_PORTI
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOILPEN         (0x00000001UL <<  8U)
#endif
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOJLPEN         (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_AHB4LPENR_GPIOKLPEN         (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_AHB4LPENR_CRCLPEN           (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_AHB4LPENR_BDMALPEN          (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_AHB4LPENR_ADC3LPEN          (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_AHB4LPENR_BKPRAMLPEN        (0x00000001UL << 28U)
#define CHIP_REGFLD_RCC_AHB4LPENR_SRAM4LPEN         (0x00000001UL << 29U)

/*********** RCC APB3 Sleep clock register (RCC_APB3LPENR) ********************/
#define CHIP_REGFLD_RCC_APB3LPENR_LTDCLPEN          (0x00000001UL <<  3U)
#if CHIP_DEV_SUPPORT_DSI
#define CHIP_REGFLD_RCC_APB3LPENR_DSILPEN           (0x00000001UL <<  4U)
#endif
#define CHIP_REGFLD_RCC_APB3LPENR_WWDG1LPEN         (0x00000001UL <<  6U)

/*********** RCC APB1 Low Sleep clock register (RCC_APB1LLPENR) ***************/
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM2LPEN         (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM3LPEN         (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM4LPEN         (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM5LPEN         (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM6LPEN         (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM7LPEN         (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM12LPEN        (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM13LPEN        (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APB1LLPENR_TIM14LPEN        (0x00000001UL <<  8U)
#define CHIP_REGFLD_RCC_APB1LLPENR_LPTIM1LPEN       (0x00000001UL <<  9U)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGFLD_RCC_APB1LLPENR_WWDG2LPEN        (0x00000001UL << 11U)
#endif
#define CHIP_REGFLD_RCC_APB1LLPENR_SPI2LPEN         (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APB1LLPENR_SPI3LPEN         (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APB1LLPENR_SPDIFRXLPEN      (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB1LLPENR_USART2LPEN       (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APB1LLPENR_USART3LPEN       (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APB1LLPENR_UART4LPEN        (0x00000001UL << 19U)
#define CHIP_REGFLD_RCC_APB1LLPENR_UART5LPEN        (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APB1LLPENR_I2C1LPEN         (0x00000001UL << 21U)
#define CHIP_REGFLD_RCC_APB1LLPENR_I2C2LPEN         (0x00000001UL << 22U)
#define CHIP_REGFLD_RCC_APB1LLPENR_I2C3LPEN         (0x00000001UL << 23U)
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_REGFLD_RCC_APB1LLPENR_I2C5LPEN         (0x00000001UL << 25U)
#endif
#define CHIP_REGFLD_RCC_APB1LLPENR_CECLPEN          (0x00000001UL << 27U)
#define CHIP_REGFLD_RCC_APB1LLPENR_DAC12LPEN        (0x00000001UL << 29U)
#define CHIP_REGFLD_RCC_APB1LLPENR_UART7LPEN        (0x00000001UL << 30U)
#define CHIP_REGFLD_RCC_APB1LLPENR_UART8LPEN        (0x00000001UL << 31U)

/*********** RCC APB1 High Sleep clock register (RCC_APB1HLPENR) **************/
#define CHIP_REGFLD_RCC_APB1HLPENR_CRSLPEN          (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB1HLPENR_SWPMILPEN        (0x00000001UL <<  2U)
#define CHIP_REGFLD_RCC_APB1HLPENR_OPAMPLPEN        (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB1HLPENR_MDIOSLPEN        (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB1HLPENR_FDCANLPEN        (0x00000001UL <<  8U)
#if CHIP_DEV_SUPPORT_TIM23_24
#define CHIP_REGFLD_RCC_APB1HLPENR_TIM23LPEN        (0x00000001UL << 24U)
#define CHIP_REGFLD_RCC_APB1HLPENR_TIM24LPEN        (0x00000001UL << 25U)
#endif

/*********** RCC APB2 Sleep clock register (RCC_APB2LPENR) ********************/
#define CHIP_REGFLD_RCC_APB2LPENR_TIM1LPEN          (0x00000001UL <<  0U)
#define CHIP_REGFLD_RCC_APB2LPENR_TIM8LPEN          (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB2LPENR_USART1LPEN        (0x00000001UL <<  4U)
#define CHIP_REGFLD_RCC_APB2LPENR_USART6LPEN        (0x00000001UL <<  5U)
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_REGFLD_RCC_APB2LPENR_UART9LPEN         (0x00000001UL <<  6U)
#define CHIP_REGFLD_RCC_APB2LPENR_USART10LPEN       (0x00000001UL <<  7U)
#endif
#define CHIP_REGFLD_RCC_APB2LPENR_SPI1LPEN          (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APB2LPENR_SPI4LPEN          (0x00000001UL << 13U)
#define CHIP_REGFLD_RCC_APB2LPENR_TIM15LPEN         (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB2LPENR_TIM16LPEN         (0x00000001UL << 17U)
#define CHIP_REGFLD_RCC_APB2LPENR_TIM17LPEN         (0x00000001UL << 18U)
#define CHIP_REGFLD_RCC_APB2LPENR_SPI5LPEN          (0x00000001UL << 20U)
#define CHIP_REGFLD_RCC_APB2LPENR_SAI1LPEN          (0x00000001UL << 22U)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_REGFLD_RCC_APB2LPENR_SAI2LPEN          (0x00000001UL << 23U)
#define CHIP_REGFLD_RCC_APB2LPENR_SAI3LPEN          (0x00000001UL << 24U)
#endif
#if !CHIP_DEV_DFSDM1_ALTPOS
#define CHIP_REGFLD_RCC_APB2LPENR_DFSDM1LPEN        (0x00000001UL << 28U)
#endif
#if CHIP_DEV_SUPPORT_HRTIM
#define CHIP_REGFLD_RCC_APB2LPENR_HRTIMLPEN         (0x00000001UL << 29U)
#endif
#if CHIP_DEV_DFSDM1_ALTPOS
#define CHIP_REGFLD_RCC_APB2LPENR_DFSDM1LPEN        (0x00000001UL << 30U)
#endif

/*********** RCC APB4 Sleep clock register (RCC_APB4LPENR) ********************/
#define CHIP_REGFLD_RCC_APB4LPENR_SYSCFGLPEN        (0x00000001UL <<  1U)
#define CHIP_REGFLD_RCC_APB4LPENR_LPUART1LPEN       (0x00000001UL <<  3U)
#define CHIP_REGFLD_RCC_APB4LPENR_SPI6LPEN          (0x00000001UL <<  5U)
#define CHIP_REGFLD_RCC_APB4LPENR_I2C4LPEN          (0x00000001UL <<  7U)
#define CHIP_REGFLD_RCC_APB4LPENR_LPTIM2LPEN        (0x00000001UL <<  9U)
#define CHIP_REGFLD_RCC_APB4LPENR_LPTIM3LPEN        (0x00000001UL << 10U)
#define CHIP_REGFLD_RCC_APB4LPENR_LPTIM4LPEN        (0x00000001UL << 11U)
#define CHIP_REGFLD_RCC_APB4LPENR_LPTIM5LPEN        (0x00000001UL << 12U)
#define CHIP_REGFLD_RCC_APB4LPENR_COMP12LPEN        (0x00000001UL << 14U)
#define CHIP_REGFLD_RCC_APB4LPENR_VREFLPEN          (0x00000001UL << 15U)
#define CHIP_REGFLD_RCC_APB4LPENR_RTCAPBLPEN        (0x00000001UL << 16U)
#define CHIP_REGFLD_RCC_APB4LPENR_SAI4LPEN          (0x00000001UL << 21U)
#if CHIP_DEV_SUPPORT_DTS
#define CHIP_REGFLD_RCC_APB4LPENR_DTSLPEN           (0x00000001UL << 26U)
#endif

#endif // CHIP_STM32H7XX_REGS_RCC_H_
