#ifndef CHIP_STM32H7XX_REGS_SPI_H_
#define CHIP_STM32H7XX_REGS_SPI_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

/* Serial Peripheral Interface */
typedef struct {
  __IO uint32_t CR1;           /* SPI/I2S Control register 1,                      Address offset: 0x00 */
  __IO uint32_t CR2;           /* SPI Control register 2,                          Address offset: 0x04 */
  __IO uint32_t CFG1;          /* SPI Configuration register 1,                    Address offset: 0x08 */
  __IO uint32_t CFG2;          /* SPI Configuration register 2,                    Address offset: 0x0C */
  __IO uint32_t IER;           /* SPI/I2S Interrupt Enable register,               Address offset: 0x10 */
  __IO uint32_t SR;            /* SPI/I2S Status register,                         Address offset: 0x14 */
  __IO uint32_t IFCR;          /* SPI/I2S Interrupt/Status flags clear register,   Address offset: 0x18 */
  uint32_t      RESERVED0;     /* Reserved, 0x1C                                                        */
  union {
    __IO uint32_t TXDR32;      /* SPI/I2S Transmit data register,                  Address offset: 0x20 */
    struct {
        __IO uint16_t TXDR16;
        uint16_t tpad16;
    };
    struct {
        __IO uint8_t TXDR8;
        uint8_t tpad8[3];
    };
  };

  uint32_t      RESERVED1[3];  /* Reserved, 0x24-0x2C                                                   */
  union {
    __IO uint32_t RXDR32;      /* SPI/I2S Receive data register,                   Address offset: 0x30 */
    struct {
        __IO uint16_t RXDR16;
        uint16_t rpad16;
    };
    struct {
        __IO uint8_t RXDR8;
        uint8_t rpad8[3];
    };
  };
  uint32_t      RESERVED2[3];  /* Reserved, 0x34-0x3C                                                   */
  __IO uint32_t CRCPOLY;       /* SPI CRC Polynomial register,                     Address offset: 0x40 */
  __IO uint32_t TXCRC;         /* SPI Transmitter CRC register,                    Address offset: 0x44 */
  __IO uint32_t RXCRC;         /* SPI Receiver CRC register,                       Address offset: 0x48 */
  __IO uint32_t UDRDR;         /* SPI Underrun data register,                      Address offset: 0x4C */
  __IO uint32_t I2SCFGR;       /* I2S Configuration register,                      Address offset: 0x50 */
} CHIP_REGS_SPI_T;


#define CHIP_REGS_SPI1                          ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI1)
#define CHIP_REGS_SPI2                          ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI2)
#define CHIP_REGS_SPI3                          ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI3)
#define CHIP_REGS_SPI4                          ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI4)
#define CHIP_REGS_SPI5                          ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI5)
#define CHIP_REGS_SPI6                          ((CHIP_REGS_SPI_T *) CHIP_MEMRGN_ADDR_PERIPH_SPI6)


#define CHIP_SPI1_FIFO_SIZE        16
#define CHIP_SPI2_FIFO_SIZE        16
#define CHIP_SPI3_FIFO_SIZE        16
#define CHIP_SPI4_FIFO_SIZE        8
#define CHIP_SPI5_FIFO_SIZE        8
#define CHIP_SPI6_FIFO_SIZE        8


#define CHIP_SPI1_WORD_MAX_SIZE    32
#define CHIP_SPI2_WORD_MAX_SIZE    32
#define CHIP_SPI3_WORD_MAX_SIZE    32
#define CHIP_SPI4_WORD_MAX_SIZE    16
#define CHIP_SPI5_WORD_MAX_SIZE    16
#define CHIP_SPI6_WORD_MAX_SIZE    16

/*********** SPI/I2S control register 1 (SPI2S_CR1) ***************************/
#define CHIP_REGFLD_SPI_CR1_SPE                 (0x00000001UL <<  0U) /* Serial Peripheral Enable */
#define CHIP_REGFLD_SPI_CR1_MASRX               (0x00000001UL <<  8U) /* Master automatic SUSP in Receive mode */
#define CHIP_REGFLD_SPI_CR1_CSTART              (0x00000001UL <<  9U) /* Master transfer start */
#define CHIP_REGFLD_SPI_CR1_CSUSP               (0x00000001UL << 10U) /* Master SUSPend request */
#define CHIP_REGFLD_SPI_CR1_HDDIR               (0x00000001UL << 11U) /* Rx/Tx direction at Half-duplex mode */
#define CHIP_REGFLD_SPI_CR1_SSI                 (0x00000001UL << 12U) /* Internal SS signal input level */
#define CHIP_REGFLD_SPI_CR1_CRC33_17            (0x00000001UL << 13U) /* 32-bit CRC polynomial configuration  */
#define CHIP_REGFLD_SPI_CR1_RCRCINI             (0x00000001UL << 14U) /* CRC init pattern control for receiver */
#define CHIP_REGFLD_SPI_CR1_TCRCINI             (0x00000001UL << 15U) /* CRC init pattern control for transmitter */
#define CHIP_REGFLD_SPI_CR1_IOLOCK              (0x00000001UL << 16U) /* Locking the AF configuration of associated IOs */
#define CHIP_REGMSK_SPI_CR1_RESERVED            (0xFFFE00FEUL)

/*********** SPI control register 2 (SPI_CR2) *********************************/
#define CHIP_REGFLD_SPI_CR2_TSIZE               (0x0000FFFFUL <<  0U) /* Number of data at current transfer */
#define CHIP_REGFLD_SPI_CR2_TSER                (0x00000001UL << 16U) /* Number of data transfer extension */


/*********** SPI configuration register 1 (SPI_CFG1) **************************/
#define CHIP_REGFLD_SPI_CFG1_DSIZE              (0x0000001FUL <<  0U) /* Bits number in single SPI data frame */
#define CHIP_REGFLD_SPI_CFG1_FTHLV              (0x0000000FUL <<  5U) /* FIFO threshold level */
#define CHIP_REGFLD_SPI_CFG1_UDRCFG             (0x00000003UL <<  9U) /* Behavior of transmitter at underrun */
#define CHIP_REGFLD_SPI_CFG1_UDRDET             (0x00000003UL << 11U) /* Detection of underrun condition */
#define CHIP_REGFLD_SPI_CFG1_RXDMAEN            (0x00000001UL << 14U) /* Rx DMA stream enable */
#define CHIP_REGFLD_SPI_CFG1_TXDMAEN            (0x00000001UL << 15U) /* Tx DMA stream enable */
#define CHIP_REGFLD_SPI_CFG1_CRCSIZE            (0x0000001FUL << 16U) /* Length of CRC frame*/
#define CHIP_REGFLD_SPI_CFG1_CRCEN              (0x00000001UL << 22U) /* Hardware CRC computation enable */
#define CHIP_REGFLD_SPI_CFG1_MBR                (0x00000007UL << 28U) /* Master baud rate */
#define CHIP_REGMSK_SPI_CFG1_RESERVED           (0x8FA02000UL)

#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV2        0
#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV4        1
#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV8        2
#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV16       3
#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV32       4
#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV64       5
#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV128      6
#define CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV256      7

/*********** SPI configuration register 2 (SPI_CFG2) **************************/
#define CHIP_REGFLD_SPI_CFG2_MSSI               (0x0000000FUL <<  0U) /* Master SS Idleness */
#define CHIP_REGFLD_SPI_CFG2_MIDI               (0x0000000FUL <<  4U) /* Master Inter-Data Idleness */
#define CHIP_REGFLD_SPI_CFG2_IOSWP              (0x00000001UL << 15U) /* Swap functionality of MISO and MOSI pins */
#define CHIP_REGFLD_SPI_CFG2_COMM               (0x00000003UL << 17U) /* SPI Communication Mode */
#define CHIP_REGFLD_SPI_CFG2_SP                 (0x00000007UL << 19U) /* Serial Protocol */
#define CHIP_REGFLD_SPI_CFG2_MASTER             (0x00000001UL << 22U) /* SPI Master */
#define CHIP_REGFLD_SPI_CFG2_LSBFRST            (0x00000001UL << 23U) /* Data frame format */
#define CHIP_REGFLD_SPI_CFG2_CPHA               (0x00000001UL << 24U) /* Clock Phase */
#define CHIP_REGFLD_SPI_CFG2_CPOL               (0x00000001UL << 25U) /* Clock Polarity */
#define CHIP_REGFLD_SPI_CFG2_SSM                (0x00000001UL << 26U) /* Software slave management */
#define CHIP_REGFLD_SPI_CFG2_SSIOP              (0x00000001UL << 28U) /* SS input/output polarity */
#define CHIP_REGFLD_SPI_CFG2_SSOE               (0x00000001UL << 29U) /* SS output enable */
#define CHIP_REGFLD_SPI_CFG2_SSOM               (0x00000001UL << 30U) /* SS output management in master mode */
#define CHIP_REGFLD_SPI_CFG2_AFCNTR             (0x00000001UL << 31U) /* Alternate function GPIOs control */
#define CHIP_REGMSK_SPI_CFG2_RESERVED           (0x08017F00UL)

#define CHIP_REGFLDVAL_SPI_CFG2_COMM_FD         0 /* full-duplex */
#define CHIP_REGFLDVAL_SPI_CFG2_COMM_TX         1 /* simplex transmitter */
#define CHIP_REGFLDVAL_SPI_CFG2_COMM_RX         2 /* simplex receiver */
#define CHIP_REGFLDVAL_SPI_CFG2_COMM_HD         3 /* half-duplex */

#define CHIP_REGFLDVAL_SPI_CFG2_SP_MOTOROLA     0
#define CHIP_REGFLDVAL_SPI_CFG2_SP_TI           1

/*********** SPI/I2S interrupt enable register (SPI2S_IER) ********************/
#define CHIP_REGFLD_SPI_IER_RXPIE               (0x00000001UL <<  0U) /* RXP Interrupt enable */
#define CHIP_REGFLD_SPI_IER_TXPIE               (0x00000001UL <<  1U) /* TXP interrupt enable */
#define CHIP_REGFLD_SPI_IER_DXPIE               (0x00000001UL <<  2U) /* DXP interrupt enable */
#define CHIP_REGFLD_SPI_IER_EOTIE               (0x00000001UL <<  3U) /* EOT/SUSP/TXC interrupt enable */
#define CHIP_REGFLD_SPI_IER_TXTFIE              (0x00000001UL <<  4U) /* TXTF interrupt enable */
#define CHIP_REGFLD_SPI_IER_UDRIE               (0x00000001UL <<  5U) /* UDR interrupt enable */
#define CHIP_REGFLD_SPI_IER_OVRIE               (0x00000001UL <<  6U) /* OVR interrupt enable */
#define CHIP_REGFLD_SPI_IER_CRCEIE              (0x00000001UL <<  7U) /* CRCE interrupt enable */
#define CHIP_REGFLD_SPI_IER_TIFREIE             (0x00000001UL <<  8U) /* TI Frame Error interrupt enable */
#define CHIP_REGFLD_SPI_IER_MODFIE              (0x00000001UL <<  9U) /* MODF interrupt enable */
#define CHIP_REGFLD_SPI_IER_TSERFIE             (0x00000001UL << 10U) /* TSERF interrupt enable */
#define CHIP_REGMSK_SPI_IER_RESERVED            (0xFFFFF800UL)

/*********** SPI/I2S status register (SPI2S_SR) *******************************/
#define CHIP_REGFLD_SPI_SR_RXP                  (0x00000001UL <<  0U) /* Rx-Packet available */
#define CHIP_REGFLD_SPI_SR_TXP                  (0x00000001UL <<  1U) /* Tx-Packet space available */
#define CHIP_REGFLD_SPI_SR_DXP                  (0x00000001UL <<  2U) /* Duplex Packet available */
#define CHIP_REGFLD_SPI_SR_EOT                  (0x00000001UL <<  3U) /* End of transfer */
#define CHIP_REGFLD_SPI_SR_TXTF                 (0x00000001UL <<  4U) /* Transmission Transfer Filled */
#define CHIP_REGFLD_SPI_SR_UDR                  (0x00000001UL <<  5U) /* Underrun at Slave transmission */
#define CHIP_REGFLD_SPI_SR_OVR                  (0x00000001UL <<  6U) /* Overrun */
#define CHIP_REGFLD_SPI_SR_CRCE                 (0x00000001UL <<  7U) /* CRC Error Detected */
#define CHIP_REGFLD_SPI_SR_TIFRE                (0x00000001UL <<  8U) /* TI frame format error Detected */
#define CHIP_REGFLD_SPI_SR_MODF                 (0x00000001UL <<  9U) /* Mode Fault Detected */
#define CHIP_REGFLD_SPI_SR_TSERF                (0x00000001UL << 10U) /* Additional number of SPI data to be transacted was reloaded */
#define CHIP_REGFLD_SPI_SR_SUSP                 (0x00000001UL << 11U) /* Suspended */
#define CHIP_REGFLD_SPI_SR_TXC                  (0x00000001UL << 12U) /* TxFIFO transmission complete */
#define CHIP_REGFLD_SPI_SR_RXPLVL               (0x00000003UL << 13U) /* RxFIFO Packing Level */
#define CHIP_REGFLD_SPI_SR_RXWNE                (0x00000001UL << 15U) /* Rx FIFO Word Not Empty */
#define CHIP_REGFLD_SPI_SR_CTSIZE               (0x0000FFFFUL << 16U) /* Number of data frames remaining in current TSIZE session */

/*********** SPI/I2S interrupt/status flags clear register (SPI2S_IFCR) *******/
#define CHIP_REGFLD_SPI_IFCR_EOTC               (0x00000001UL <<  3U) /* End Of Transfer flag clear */
#define CHIP_REGFLD_SPI_IFCR_TXTFC              (0x00000001UL <<  4U) /* Transmission Transfer Filled flag clear */
#define CHIP_REGFLD_SPI_IFCR_UDRC               (0x00000001UL <<  5U) /* Underrun flag clear */
#define CHIP_REGFLD_SPI_IFCR_OVRC               (0x00000001UL <<  6U) /* Overrun flag clear */
#define CHIP_REGFLD_SPI_IFCR_CRCEC              (0x00000001UL <<  7U) /* CRC Error flag clear */
#define CHIP_REGFLD_SPI_IFCR_TIFREC             (0x00000001UL <<  8U) /* TI frame format error flag clear */
#define CHIP_REGFLD_SPI_IFCR_MODFC              (0x00000001UL <<  9U) /* Mode Fault flag clear */
#define CHIP_REGFLD_SPI_IFCR_TSERFC             (0x00000001UL << 10U) /* TSERFC flag clear */
#define CHIP_REGFLD_SPI_IFCR_SUSPC              (0x00000001UL << 11U) /* SUSPend flag clear */
#define CHIP_REGMSK_SPI_IFCR_RESERVED           (0xFFFFF007UL)

/*********** SPI/I2S transmit data register (SPI2S_TXDR) **********************/
#define CHIP_REGFLD_SPI_TXDR_TXDR               (0xFFFFFFFFUL <<  0U) /* Transmit Data Register */

/*********** SPI/I2S receive data register (SPI2S_RXDR) ***********************/
#define CHIP_REGFLD_SPI_RXDR_RXDR               (0xFFFFFFFFUL <<  0U) /* Receive Data Register  */

/*********** SPI polynomial register (SPI_CRCPOLY) ****************************/
#define CHIP_REGFLD_SPI_CRCPOLY_CRCPOLY         (0xFFFFFFFFUL <<  0U) /* CRC Polynomial register  */

/*********** SPI transmitter CRC register (SPI_TXCRC) *************************/
#define CHIP_REGFLD_SPI_TXCRC_TXCRC             (0xFFFFFFFFUL <<  0U) /* CRCRegister for transmitter */

/********** SPI receiver CRC register (SPI_RXCRC) *****************************/
#define CHIP_REGFLD_SPI_RXCRC_RXCRC             (0xFFFFFFFFUL <<  0U) /* CRCRegister for receiver */

/*********** SPI underrun data register (SPI_UDRDR) ***************************/
#define CHIP_REGFLD_SPI_UDRDR_UDRDR             (0xFFFFFFFFUL <<  0U) /* Data at slave underrun condition */

/*********** SPI/I2S configuration register (SPI_I2SCFGR) *********************/
#define CHIP_REGFLD_SPI_I2SCFGR_I2SMOD          (0x00000001UL <<  0U) /* I2S mode selection */
#define CHIP_REGFLD_SPI_I2SCFGR_I2SCFG          (0x00000007UL <<  1U) /* I2S configuration mode */
#define CHIP_REGFLD_SPI_I2SCFGR_I2SSTD          (0x00000003UL <<  4U) /* I2S standard selection */
#define CHIP_REGFLD_SPI_I2SCFGR_PCMSYNC         (0x00000001UL <<  7U) /* PCM frame synchronization */
#define CHIP_REGFLD_SPI_I2SCFGR_DATLEN          (0x00000003UL <<  8U) /* Data length to be transferred */
#define CHIP_REGFLD_SPI_I2SCFGR_CHLEN           (0x00000001UL << 10U) /* Channel length (number of bits per audio channel) */
#define CHIP_REGFLD_SPI_I2SCFGR_CKPOL           (0x00000001UL << 11U) /* Steady state clock polarity */
#define CHIP_REGFLD_SPI_I2SCFGR_FIXCH           (0x00000001UL << 12U) /* Fixed channel length in SLAVE */
#define CHIP_REGFLD_SPI_I2SCFGR_WSINV           (0x00000001UL << 13U) /* Word select inversion */
#define CHIP_REGFLD_SPI_I2SCFGR_DATFMT          (0x00000001UL << 14U) /* Data format */
#define CHIP_REGFLD_SPI_I2SCFGR_I2SDIV          (0x000000FFUL << 16U) /* I2S Linear prescaler */
#define CHIP_REGFLD_SPI_I2SCFGR_ODD             (0x00000001UL << 24U) /* Odd factor for the prescaler */
#define CHIP_REGFLD_SPI_I2SCFGR_MCKOE           (0x00000001UL << 25U) /* Master Clock Output Enable */
#define CHIP_REGMSK_SPI_I2SCFGR_RESERVED        (0xFC008040UL)

#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SCFG_SLAVE_TX  0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SCFG_SLAVE_RX  1
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SCFG_MASTER_TX 2
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SCFG_MASTER_RX 3
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SCFG_SLAVE_FD  4
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SCFG_MASTER_FD 5

#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SSTD_PHILIPS   0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SSTD_LJ        1
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SSTD_RJ        2
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2SSTD_PCM       3

#define CHIP_REGFLDVAL_SPI_I2SCFGR_PCMSYNC_SHORT    0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_PCMSYNC_LONG     1

#define CHIP_REGFLDVAL_SPI_I2SCFGR_DATLEN_16BIT     0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_DATLEN_24BIT     1
#define CHIP_REGFLDVAL_SPI_I2SCFGR_DATLEN_32BIT     2

#define CHIP_REGFLDVAL_SPI_I2SCFGR_CHLEN_16BIT      0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_CHLEN_32BIT      1

#endif // CHIP_STM32H7XX_REGS_SPI_H_

