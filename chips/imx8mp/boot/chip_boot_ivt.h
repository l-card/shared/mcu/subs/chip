#ifndef CHIP_BOOT_IVT_H
#define CHIP_BOOT_IVT_H

#include <stdint.h>

#define CHIP_BOOT_IVT_HDR_TAG   0xD1
#define CHIP_BOOT_IVT_SIZE      32
#define CHIP_BOOT_IVT_VERSION   0x41


typedef struct {
    uint8_t tag;        /* A single byte field set to 0xD1 */
    uint8_t len_h;
    uint8_t len_l;      /* the length is fixed and must have a value of 32 bytes */
    uint8_t version;    /* A single byte field set to 0x40 or 0x41 */
} t_chip_boot_ivt_hdr;


typedef struct {
    uint32_t start;     /* Absolute address of the image (совпадает с self, если смещение ivt = 0) */
    uint32_t length;    /* Length in bytes */
    uint32_t plugin;
} t_chip_boot_ivt_boot_data;

/* с IVT должено располагаться по фиксированному смещению (как правило 0) в образе программы, запускаемой встроенным загрузчиком */
typedef struct {
    t_chip_boot_ivt_hdr hdr;
    uint32_t entry;      /* Абсолютный адрес начала приложения (адрес, с которого запускается выполнение) */
    uint32_t reserved1;
    uint32_t dcd_ptr;    /* Адрес DCD (0 для imx8mp) */
    uint32_t boot_data_ptr;  /* Абсолютный адрес структуры t_chip_boot_ivt_boot_data */
    uint32_t self;           /* Абсолютный адрес начала данного заголовка (после переноса в целевую память) */
    uint32_t csf;            /* Сертификат и подпись для защищенной загрузки (или 0) */
    uint32_t reserved2;
} t_chip_ivt;



#endif // CHIP_BOOT_IVT_H
