#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#include "chip_std_defs.h"
#include "chip_dev_spec_features.h"

/* граничные условия для внешней частоты HSE */
#define CHIP_CLK_HSE_FREQ_MIN               CHIP_MHZ(4)  /* минимальная внешняя частота HSE */
#define CHIP_CLK_HSE_FREQ_MAX               CHIP_MHZ(25) /* максимальная внешняя частота HSE */


#define CHIP_CLK_PLLIN_FREQ_MIN             CHIP_MHZ(2)  /* минимальная входная частота PLL */
#define CHIP_CLK_PLLIN_FREQ_MAX             CHIP_MHZ(16) /* максимальная входная частота PLL */

#define CHIP_CLK_PLL_FREQ_MIN               CHIP_MHZ(16)  /* минимальная выходная частота PLL */
#define CHIP_CLK_PLL_FREQ_MAX               CHIP_MHZ(180) /* максимальная выходная частота PLL */

#define CHIP_CLK_SYSCLK_FREQ_MAX            CHIP_MHZ(180)
#define CHIP_CLK_AHB_FREQ_MAX               CHIP_MHZ(180)
#define CHIP_CLK_APB1_FREQ_MAX              CHIP_MHZ(90)  /* на рисунке 120, в тексте 90 */
#define CHIP_CLK_APB2_FREQ_MAX              CHIP_MHZ(90)

#define CHIP_CLK_ADC_FREQ_MAX               CHIP_MHZ(28)
#define CHIP_CLK_USB_FREQ_REQ               CHIP_MHZ(48)


#endif // CHIP_CLK_CONSTRAINTS_H


