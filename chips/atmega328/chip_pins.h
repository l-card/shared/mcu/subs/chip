#ifndef CHIP_PIN_H
#define CHIP_PIN_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef uint8_t t_chip_pin_id;


#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4
#define CHIP_PORT_F 6



#define CHIP_PORT_REG_ADDRS(port) ((port) * 3)

#define CHIP_PORT_PINREG(port)  (_SFR_IO8(CHIP_PORT_REG_ADDRS(port)))
#define CHIP_PORT_DDRREG(port)  (_SFR_IO8(CHIP_PORT_REG_ADDRS(port) + 1))
#define CHIP_PORT_PORTREG(port) (_SFR_IO8(CHIP_PORT_REG_ADDRS(port) + 2))


#define CHIP_PIN_ID(port, pin, func)  ((((port) & 0x7) << 5) | (((pin) & 0x7) << 2) | ((func) & 3))

#define CHIP_PIN_ID_PORT(id) (((id) >> 5) & 0x7)
#define CHIP_PIN_ID_PIN(id)  (((id) >> 2) & 0x7)
#define CHIP_PIN_ID_FUNC(id) (((id) >> 0) & 0x3)


#define CHIP_PIN_SET(id) CHIP_PORT_PORTREG(CHIP_PIN_ID_PORT(id)) |= (1U << CHIP_PIN_ID_PIN(id))
#define CHIP_PIN_CLR(id) CHIP_PORT_PORTREG(CHIP_PIN_ID_PORT(id)) &= ~(1U << CHIP_PIN_ID_PIN(id))

/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val) do { \
        if (val) { \
            CHIP_PIN_SET(id); \
        } else { \
            CHIP_PIN_CLR(id); \
        } \
    } while(0)
/* чтение уровня на ножке (0 или 1) */
#define CHIP_PIN_IN(id)           (!!(CHIP_PORT_PINREG(CHIP_PIN_ID_PORT(id)) & (1 << CHIP_PIN_ID_PIN(id))))
/* состояние регистра выхода (независимо от физического уровня) */
#define CHIP_PIN_GET_OUT_VAL(id)  (!!(CHIP_PORT_PORTREG(CHIP_PIN_ID_PORT(id)) & (1 << CHIP_PIN_ID_PIN(id))))
/* toggle выполняется записью 1 в бит PIN */
#define CHIP_PIN_TOGGLE(id)  (CHIP_PORT_PINREG(CHIP_PIN_ID_PORT(id)) = (1 << CHIP_PIN_ID_PIN(id)))


#define CHIP_PIN_SET_DIR_IN(id)    (CHIP_PORT_DDRREG(CHIP_PIN_ID_PORT(id)) &= ~(1 << CHIP_PIN_ID_PIN(id)))
#define CHIP_PIN_SET_DIR_OUT(id)   (CHIP_PORT_DDRREG(CHIP_PIN_ID_PORT(id)) |= (1 << CHIP_PIN_ID_PIN(id)))


#define CHIP_PIN_STDMODE_UNK   0
#define CHIP_PIN_STDMODE_IN    1
#define CHIP_PIN_STDMODE_OUT   2



#define CHIP_PIN_CONFIG_OUT(id, val) do { \
        CHIP_PIN_OUT(id, val); \
        CHIP_PIN_SET_DIR_OUT(id); \
    } while(0)

#define CHIP_PIN_CONFIG_IN_PU(id, pu) do { \
        CHIP_PIN_SET_DIR_IN(id); \
        CHIP_PIN_OUT(id, pu); \
    } while(0)

#define CHIP_PIN_CONFIG_IN(id) CHIP_PIN_CONFIG_IN_PU(id, 0)



/* ------------------------------ Порт PB ------------------------------------*/
/* PB0 */
#define CHIP_PIN_PB0_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 0, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB0_ICP1    CHIP_PIN_ID(CHIP_PORT_B, 0, CHIP_PIN_STDMODE_IN)  /* Timer/Counter1 Input Capture Input */
#define CHIP_PIN_PB0_CLKO0   CHIP_PIN_ID(CHIP_PORT_B, 0, CHIP_PIN_STDMODE_UNK) /* Divided System Clock Output */
#define CHIP_PIN_PB0_PCINT0  CHIP_PIN_ID(CHIP_PORT_B, 0, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 0 */

/* PB1 */
#define CHIP_PIN_PB1_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 1, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB1_OC1A    CHIP_PIN_ID(CHIP_PORT_B, 1, CHIP_PIN_STDMODE_OUT) /* (+) Timer/Counter1 Output Compare Match A Output */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PB1_SPSS    CHIP_PIN_ID(CHIP_PORT_B, 1, CHIP_PIN_STDMODE_UNK) /* (*) SPI Bus Master Slave select */
#endif
#define CHIP_PIN_PB1_PCINT1  CHIP_PIN_ID(CHIP_PORT_B, 1, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 1 */

/* PB2 */
#define CHIP_PIN_PB2_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 2, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB2_SPSS    CHIP_PIN_ID(CHIP_PORT_B, 2, CHIP_PIN_STDMODE_UNK) /* (+) SPI Bus Master Slave select */
#define CHIP_PIN_PB2_OC1B    CHIP_PIN_ID(CHIP_PORT_B, 2, CHIP_PIN_STDMODE_OUT) /* (+) Timer/Counter1 Output Compare Match B Output */
#define CHIP_PIN_PB2_PCINT2  CHIP_PIN_ID(CHIP_PORT_B, 2, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 2 */

/* PB3 */
#define CHIP_PIN_PB3_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 3, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB3_MOSI    CHIP_PIN_ID(CHIP_PORT_B, 3, CHIP_PIN_STDMODE_UNK) /* SPI Bus Master Output/Slave Input */
#define CHIP_PIN_PB3_OC2A    CHIP_PIN_ID(CHIP_PORT_B, 3, CHIP_PIN_STDMODE_OUT) /* (+) Timer/Counter2 Output Compare Match A Output */
#define CHIP_PIN_PB3_PCINT3  CHIP_PIN_ID(CHIP_PORT_B, 3, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 3 */

/* PB4 */
#define CHIP_PIN_PB4_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 4, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB4_MISO    CHIP_PIN_ID(CHIP_PORT_B, 4, CHIP_PIN_STDMODE_UNK) /* SPI Bus Master Input/Slave Output */
#define CHIP_PIN_PB4_PCINT4  CHIP_PIN_ID(CHIP_PORT_B, 4, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 4 */

/* PB5 */
#define CHIP_PIN_PB5_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 5, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB5_SCK     CHIP_PIN_ID(CHIP_PORT_B, 5, CHIP_PIN_STDMODE_UNK) /* SPI Bus Master clock */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PB5_AC1P    CHIP_PIN_ID(CHIP_PORT_B, 5, CHIP_PIN_STDMODE_IN)  /* Analog Comparator 1 Positive Input (нет в списке?) */
#endif
#define CHIP_PIN_PB5_PCINT5  CHIP_PIN_ID(CHIP_PORT_B, 5, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 5 */

/* PB6 */
#define CHIP_PIN_PB6_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 6, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB6_XTALO   CHIP_PIN_ID(CHIP_PORT_B, 6, CHIP_PIN_STDMODE_UNK) /* External Main Clock Oscillator Output */
#define CHIP_PIN_PB6_TOSC1   CHIP_PIN_ID(CHIP_PORT_B, 6, CHIP_PIN_STDMODE_UNK) /* Timer Oscillator pin 1 */
#define CHIP_PIN_PB6_PCINT6  CHIP_PIN_ID(CHIP_PORT_B, 6, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 6 */

/* PB7 */
#define CHIP_PIN_PB7_GPIO    CHIP_PIN_ID(CHIP_PORT_B, 7, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PB7_XTALI   CHIP_PIN_ID(CHIP_PORT_B, 7, CHIP_PIN_STDMODE_UNK) /* External Main Clock Oscillator Input */
#define CHIP_PIN_PB7_TOSC2   CHIP_PIN_ID(CHIP_PORT_B, 7, CHIP_PIN_STDMODE_UNK) /* Timer Oscillator pin 2 */
#define CHIP_PIN_PB7_PCINT7  CHIP_PIN_ID(CHIP_PORT_B, 7, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 7 */


/* ------------------------------ Порт PC ------------------------------------*/
/* PC0 */
#define CHIP_PIN_PC0_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 0, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC0_ADC0    CHIP_PIN_ID(CHIP_PORT_C, 0, CHIP_PIN_STDMODE_IN)  /* ADC Input Channel 0 */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PC0_APP0    CHIP_PIN_ID(CHIP_PORT_C, 0, CHIP_PIN_STDMODE_IN)  /* Differential Amplifier Non-Inverting Input 0 */
#define CHIP_PIN_PC0_OC0A    CHIP_PIN_ID(CHIP_PORT_C, 0, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 0 Compare Match Output A */
#endif
#define CHIP_PIN_PC0_PCINT8  CHIP_PIN_ID(CHIP_PORT_C, 0, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 8 */

/* PC1 */
#define CHIP_PIN_PC1_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 1, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC1_ADC1    CHIP_PIN_ID(CHIP_PORT_C, 1, CHIP_PIN_STDMODE_IN)  /* ADC Input Channel 1 */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PC1_APP1    CHIP_PIN_ID(CHIP_PORT_C, 1, CHIP_PIN_STDMODE_IN)  /* Differential Amplifier Non-Inverting Input 1 */
#endif
#define CHIP_PIN_PC1_PCINT9  CHIP_PIN_ID(CHIP_PORT_C, 1, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 9 */

/* PC2 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32
#define CHIP_PIN_PC2_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC2_ADC2    CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_STDMODE_IN)  /* ADC Input Channel 2 */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PC2_APN0    CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_STDMODE_IN)  /* Differential Amplifier Inverting Input 0 */
#endif
#define CHIP_PIN_PC2_PCINT10 CHIP_PIN_ID(CHIP_PORT_C, 2, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 10 */
#endif

/* PC3 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32
#define CHIP_PIN_PC3_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC3_ADC3    CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_STDMODE_IN)  /* ADC Input Channel 3 */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PC3_APN1    CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_STDMODE_IN)  /* Differential Amplifier Inverting Input 1 */
#endif
#define CHIP_PIN_PC3_PCINT11 CHIP_PIN_ID(CHIP_PORT_C, 3, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 11 */
#endif

/* PC4 */
#define CHIP_PIN_PC4_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 4, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC4_ADC4    CHIP_PIN_ID(CHIP_PORT_C, 4, CHIP_PIN_STDMODE_IN)  /* ADC Input Channel 4 */
#define CHIP_PIN_PC4_SDA     CHIP_PIN_ID(CHIP_PORT_C, 4, CHIP_PIN_STDMODE_UNK)  /* TWI Serial Bus Data Input/Output Line */
#define CHIP_PIN_PC4_PCINT12 CHIP_PIN_ID(CHIP_PORT_C, 4, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 12 */

/* PC5 */
#define CHIP_PIN_PC5_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 5, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC5_ADC5    CHIP_PIN_ID(CHIP_PORT_C, 5, CHIP_PIN_STDMODE_IN)  /* ADC Input Channel 5 */
#define CHIP_PIN_PC5_SCL     CHIP_PIN_ID(CHIP_PORT_C, 5, CHIP_PIN_STDMODE_UNK) /* TWI Serial Bus Clock Line */
#define CHIP_PIN_PC5_PCINT13 CHIP_PIN_ID(CHIP_PORT_C, 5, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 13 */

/* PC6 */
#define CHIP_PIN_PC6_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 6, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC6_RESETN  CHIP_PIN_ID(CHIP_PORT_C, 6, CHIP_PIN_STDMODE_UNK) /* External reset input */
#define CHIP_PIN_PC6_PCINT14 CHIP_PIN_ID(CHIP_PORT_C, 6, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 14 */

/* PC7 */
#ifdef CHIP_LGT8FX8P
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC7_GPIO    CHIP_PIN_ID(CHIP_PORT_C, 7, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PC7_ADC8    CHIP_PIN_ID(CHIP_PORT_C, 7, CHIP_PIN_STDMODE_IN)  /* ADC Input Channel 8 */
#define CHIP_PIN_PC7_APN2    CHIP_PIN_ID(CHIP_PORT_C, 7, CHIP_PIN_STDMODE_IN)  /* Differential Amplifier Inverting Input 2 */
#define CHIP_PIN_PC7_PCINT15 CHIP_PIN_ID(CHIP_PORT_C, 7, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 15 */
#endif
#endif


/* ------------------------ Порт PD ---------------------*/
/* PD0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32
#define CHIP_PIN_PD0_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 0, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PD0_RXD     CHIP_PIN_ID(CHIP_PORT_D, 0, CHIP_PIN_STDMODE_UNK) /* (+) USART Input Pin */
#define CHIP_PIN_PD0_PCINT16 CHIP_PIN_ID(CHIP_PORT_D, 0, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 16 */
#endif

/* PD1 (QFP32: merged with PF1) */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32
#define CHIP_PIN_PD1_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 1, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PD1_TXD     CHIP_PIN_ID(CHIP_PORT_D, 1, CHIP_PIN_STDMODE_UNK) /* (+) USART Output Pin */
#define CHIP_PIN_PD1_PCINT17 CHIP_PIN_ID(CHIP_PORT_D, 1, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 17 */
#endif

/* PD2 (QFP32: merged with PF2) */
#define CHIP_PIN_PD2_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 2, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PD2_INT0    CHIP_PIN_ID(CHIP_PORT_D, 2, CHIP_PIN_STDMODE_IN)  /* External Interrupt 0 Input */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PD2_AC0O    CHIP_PIN_ID(CHIP_PORT_D, 2, CHIP_PIN_STDMODE_UNK) /* Comparator 0 output */
#endif
#define CHIP_PIN_PD2_PCINT18 CHIP_PIN_ID(CHIP_PORT_D, 2, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 18 */

/* PD3 */
#define CHIP_PIN_PD3_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 3, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PD3_INT1    CHIP_PIN_ID(CHIP_PORT_D, 3, CHIP_PIN_STDMODE_IN)  /* External Interrupt 1 Input */
#define CHIP_PIN_PD3_OC2B    CHIP_PIN_ID(CHIP_PORT_D, 3, CHIP_PIN_STDMODE_OUT) /* (+) Timer/Counter2 Output Compare Match B Output */
#define CHIP_PIN_PD3_PCINT19 CHIP_PIN_ID(CHIP_PORT_D, 3, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 19 */

/* PD4 */
#define CHIP_PIN_PD4_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 4, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PD4_XCK     CHIP_PIN_ID(CHIP_PORT_D, 4, CHIP_PIN_STDMODE_UNK) /* USART External Clock Input/Output */
#define CHIP_PIN_PD4_T0      CHIP_PIN_ID(CHIP_PORT_D, 4, CHIP_PIN_STDMODE_IN)  /* Timer/Counter 0 External Counter Input */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PD4_DAO     CHIP_PIN_ID(CHIP_PORT_D, 4, CHIP_PIN_STDMODE_UNK) /* Internal 8bit DAC analog output */
#endif
#define CHIP_PIN_PD4_PCINT20 CHIP_PIN_ID(CHIP_PORT_D, 4, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 20 */

/* PD5 */
#define CHIP_PIN_PD5_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 5, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PD5_T1      CHIP_PIN_ID(CHIP_PORT_D, 5, CHIP_PIN_STDMODE_IN)  /* Timer/Counter 1 External Counter Input */
#define CHIP_PIN_PD5_OC0B    CHIP_PIN_ID(CHIP_PORT_D, 5, CHIP_PIN_STDMODE_OUT) /* (+) Timer/Counter0 Output Compare Match B Output */
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PD5_RXD     CHIP_PIN_ID(CHIP_PORT_D, 5, CHIP_PIN_STDMODE_UNK) /* (*) USART Input Pin */
#endif
#define CHIP_PIN_PD5_PCINT21 CHIP_PIN_ID(CHIP_PORT_D, 5, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 21 */

/* PD6 */
#define CHIP_PIN_PD6_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_STDMODE_UNK)
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PD6_TXD     CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_STDMODE_UNK) /* (*) USART Output Pin */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) <= 32 /* для LQFP48 выделенный пин без GPIO на эти функции */
#define CHIP_PIN_PD6_AC0P    CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_STDMODE_IN)  /* Analog Comparator 0 Positive Input */
#define CHIP_PIN_PD6_OC3A    CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 3 Compare Match Output A */
#endif
#else
#define CHIP_PIN_PD6_AIN0    CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_STDMODE_IN)  /* Analog Comparator Positive Input */
#endif
#define CHIP_PIN_PD6_OC0A    CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_STDMODE_OUT) /* (+) Timer/Event Counter 0 Compare Match Output A */
#define CHIP_PIN_PD6_PCINT22 CHIP_PIN_ID(CHIP_PORT_D, 6, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 22 */

/* PD7 */
#define CHIP_PIN_PD7_GPIO    CHIP_PIN_ID(CHIP_PORT_D, 7, CHIP_PIN_STDMODE_UNK)
#ifdef CHIP_LGT8FX8P
#define CHIP_PIN_PD7_ACXN    CHIP_PIN_ID(CHIP_PORT_D, 7, CHIP_PIN_STDMODE_IN) /* Analog Comparator 0/1 Common Negative Input */
#else
#define CHIP_PIN_PD7_AIN1    CHIP_PIN_ID(CHIP_PORT_D, 7, CHIP_PIN_STDMODE_IN) /* Analog Comparator Negative Input */
#endif
#define CHIP_PIN_PD7_PCINT23 CHIP_PIN_ID(CHIP_PORT_D, 7, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 23 */



/* ------------------------ Порт PE ---------------------*/
#ifdef CHIP_LGT8FX8P
/* PE0 */
#define CHIP_PIN_PE0_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 0, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE0_SWC     CHIP_PIN_ID(CHIP_PORT_E, 0, CHIP_PIN_STDMODE_UNK) /* SWD debugger clock line */
#define CHIP_PIN_PE0_APN4    CHIP_PIN_ID(CHIP_PORT_E, 0, CHIP_PIN_STDMODE_IN)  /* Differential Amplifier Inverting Input 4 */
#define CHIP_PIN_PE0_PCINT24 CHIP_PIN_ID(CHIP_PORT_E, 0, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 24 */

/* PE1 */
#define CHIP_PIN_PE1_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 1, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE1_ADC6    CHIP_PIN_ID(CHIP_PORT_E, 1, CHIP_PIN_STDMODE_IN)  /* ADC input channel 6 (для ATmega - фиксирована данная функция) */
#define CHIP_PIN_PE1_ACXP    CHIP_PIN_ID(CHIP_PORT_E, 1, CHIP_PIN_STDMODE_IN)  /* Analog Comparator 0/1 Common Positive Input */
#define CHIP_PIN_PE1_PCINT25 CHIP_PIN_ID(CHIP_PORT_E, 1, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 25 */

/* PE2 */
#define CHIP_PIN_PE2_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 2, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE2_SWD     CHIP_PIN_ID(CHIP_PORT_E, 2, CHIP_PIN_STDMODE_UNK) /* SWD debugger data line */
#define CHIP_PIN_PE2_PCINT26 CHIP_PIN_ID(CHIP_PORT_E, 2, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 26 */

/* PE3 */
#define CHIP_PIN_PE3_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 3, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE3_ADC7    CHIP_PIN_ID(CHIP_PORT_E, 3, CHIP_PIN_STDMODE_IN)  /* ADC input channel 7  (для ATmega - фиксирована данная функция) */
#define CHIP_PIN_PE3_AC1N    CHIP_PIN_ID(CHIP_PORT_E, 3, CHIP_PIN_STDMODE_IN)  /* Analog Comparator 1 Negative Input */
#define CHIP_PIN_PE3_PCINT27 CHIP_PIN_ID(CHIP_PORT_E, 3, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 27 */

/* PE4 (QFP32: merged with PF4) */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32
#define CHIP_PIN_PE4_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 4, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE4_OC0A    CHIP_PIN_ID(CHIP_PORT_E, 4, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 0 Group A compare match output */
#define CHIP_PIN_PE4_PCINT28 CHIP_PIN_ID(CHIP_PORT_E, 4, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 28 */
#endif

/* PE5 (QFP32: merged with PF5) */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32
#define CHIP_PIN_PE5_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 5, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE5_CLKO1   CHIP_PIN_ID(CHIP_PORT_E, 5, CHIP_PIN_STDMODE_UNK) /* (*) Divided System Clock Output */
#define CHIP_PIN_PE5_AC1O    CHIP_PIN_ID(CHIP_PORT_E, 5, CHIP_PIN_STDMODE_UNK) /* Analog Comparator 1 Output */
#define CHIP_PIN_PE5_PCINT29 CHIP_PIN_ID(CHIP_PORT_E, 5, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 29 */
#endif

/* PE6 */
#define CHIP_PIN_PE6_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 6, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE6_AVREF   CHIP_PIN_ID(CHIP_PORT_E, 6, CHIP_PIN_STDMODE_IN)  /* ADC external reference power input (для ATmega - фиксирована данная функция) */
#define CHIP_PIN_PE6_ADC10   CHIP_PIN_ID(CHIP_PORT_E, 6, CHIP_PIN_STDMODE_IN)  /* ADC external input channel 10 */
#define CHIP_PIN_PE6_PCINT30 CHIP_PIN_ID(CHIP_PORT_E, 6, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 30 */

/* PE7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PE7_GPIO    CHIP_PIN_ID(CHIP_PORT_E, 7, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PE7_ADC11   CHIP_PIN_ID(CHIP_PORT_E, 7, CHIP_PIN_STDMODE_IN)  /* ADC external input channel 11 */
#define CHIP_PIN_PE7_PCINT31 CHIP_PIN_ID(CHIP_PORT_E, 7, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 31 */
#endif
#endif


/* ------------------------ Порт PF ---------------------*/
#ifdef CHIP_LGT8FX8P
/* PF0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PF0_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 0, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF0_ADC9    CHIP_PIN_ID(CHIP_PORT_F, 0, CHIP_PIN_STDMODE_IN)  /* ADC external mode input channel 9 */
#define CHIP_PIN_PF0_APN3    CHIP_PIN_ID(CHIP_PORT_F, 0, CHIP_PIN_STDMODE_IN)  /* Differential Amplifier Inverting Input 3 */
#define CHIP_PIN_PF0_PCINT32 CHIP_PIN_ID(CHIP_PORT_F, 0, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 32 */
#endif

/* PF1 (QFP32: merged with PD1) */
#define CHIP_PIN_PF1_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 1, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF1_OC3A    CHIP_PIN_ID(CHIP_PORT_F, 1, CHIP_PIN_STDMODE_OUT) /* (+) Group B compare match output of Timer/Event Counter 3 */
#define CHIP_PIN_PF1_PCINT33 CHIP_PIN_ID(CHIP_PORT_F, 1, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 33 */

/* PF2 (QFP32: merged with PD2) */
#define CHIP_PIN_PF2_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 2, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF2_OC3B    CHIP_PIN_ID(CHIP_PORT_F, 2, CHIP_PIN_STDMODE_OUT) /* Group B compare match output of Timer/Event Counter 3 */
#define CHIP_PIN_PF2_PCINT34 CHIP_PIN_ID(CHIP_PORT_F, 2, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 34 */

/* PF3 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PF3_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 3, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF3_OC0B    CHIP_PIN_ID(CHIP_PORT_F, 3, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 0 compare match output B */
#define CHIP_PIN_PF3_OC3C    CHIP_PIN_ID(CHIP_PORT_F, 3, CHIP_PIN_STDMODE_OUT) /* Timer/Event Counter 3 compare match output C */
#define CHIP_PIN_PF3_PCINT35 CHIP_PIN_ID(CHIP_PORT_F, 3, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 35 */
#endif

/* PF4 (QFP32: merged with PE4) */
#define CHIP_PIN_PF4_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 4, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF4_OC1B    CHIP_PIN_ID(CHIP_PORT_F, 4, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 1 compare match output B */
#define CHIP_PIN_PF4_ICP3    CHIP_PIN_ID(CHIP_PORT_F, 4, CHIP_PIN_STDMODE_IN)  /* Timer/Event Counter 3 External Capture Input */
#define CHIP_PIN_PF4_PCINT36 CHIP_PIN_ID(CHIP_PORT_F, 4, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 36 */

/* PF5 (QFP32: merged with PE5) */
#define CHIP_PIN_PF5_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 5, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF5_OC1A    CHIP_PIN_ID(CHIP_PORT_F, 5, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 1 compare match output A */
#define CHIP_PIN_PF5_PCINT37 CHIP_PIN_ID(CHIP_PORT_F, 5, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 37 */

/* PF6 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PF6_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 6, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF6_OC2A    CHIP_PIN_ID(CHIP_PORT_F, 6, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 2 compare match output A */
#define CHIP_PIN_PF6_T3      CHIP_PIN_ID(CHIP_PORT_F, 6, CHIP_PIN_STDMODE_IN)  /* Timer/Event Counter 3 external clock input. */
#define CHIP_PIN_PF6_PCINT38 CHIP_PIN_ID(CHIP_PORT_F, 6, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 38 */
#endif

/* PF7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) != 32
#define CHIP_PIN_PF7_GPIO    CHIP_PIN_ID(CHIP_PORT_F, 7, CHIP_PIN_STDMODE_UNK)
#define CHIP_PIN_PF7_OC2B    CHIP_PIN_ID(CHIP_PORT_F, 7, CHIP_PIN_STDMODE_OUT) /* (*) Timer/Event Counter 2 compare match output B */
#define CHIP_PIN_PF7_PCINT39 CHIP_PIN_ID(CHIP_PORT_F, 7, CHIP_PIN_STDMODE_IN)  /* Pin Change Interrupt 39 */
#endif
#endif


#endif // CHIP_PIN_H

