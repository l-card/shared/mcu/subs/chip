#ifndef CHIP_IOREMAP_DEFS_H
#define CHIP_IOREMAP_DEFS_H

#define CHIP_IOREMAP_DYNAMIC                -1

/* debug interface */
#define CHIP_IOREMAP_SDI_EN                 0
#define CHIP_IOREMAP_SDI_DIS                4





#define CHIP_IOREMAP_USART1_A9_10_A8_A11_12     0 /* TX - PA9,  RX - PA10, CK = PA8,  CTS - PA11, RTS - PA12 */
#define CHIP_IOREMAP_USART1_B6_7_A8_A11_12      1 /* TX - PB6,  RX - PB7,  CK = PA8,  CTS - PA11, RTS - PA12 */
#define CHIP_IOREMAP_USART1_A4_A5_A3_A2_A13     2 /* TX - PA4,  RX - PA5,  CK = PA3,  CTS - PA2,  RTS - PA13 */
#define CHIP_IOREMAP_USART1_A5_A4_A6_B7_8       3 /* TX - PA5,  RX - PA4,  CK = PA6,  CTS - PB7,  RTS - PB8  */
#define CHIP_IOREMAP_USART1_B11_B9_A6_A14_A13   4 /* TX - PB11, RX - PB9,  CK = PA6,  CTS - PA14, RTS - PA13 */
#define CHIP_IOREMAP_USART1_B12_A12_B6_8        5 /* TX - PB12, RX - PA12, CK = PB6,  CTS - PB7,  RTS - PB8 */


#define CHIP_IOREMAP_USART2_A2_4_A0_1           0 /* TX - PA2,  RX - PA3,  CK - PA4,  CTS - PA0,  RTS - PA1 */
#define CHIP_IOREMAP_USART2_A11_12_4_0_1        2 /* TX - PA11, RX - PA12, CK - PA4,  CTS - PA0,  RTS - PA1 */
#define CHIP_IOREMAP_USART2_A12_11_4_0_1        3 /* TX - PA12, RX - PA11, CK - PA4,  CTS - PA0,  RTS - PA1 */

#define CHIP_IOREMAP_USART3_B10_14              0 /* TX - PB10, RX - PB11, CK - PB12, CTS - PB13, RTS - PB14 */
#define CHIP_IOREMAP_USART3_D1_D0_B12_B14       2 /* TX - PD1,  RX - PD0,  CK - PB12, CTS - PB13, RTS - PB14 */
#define CHIP_IOREMAP_USART3_D0_D1_B12_B14       3 /* TX - PD0,  RX - PD1,  CK - PB12, CTS - PB13, RTS - PB14 */

#define CHIP_IOREMAP_USART4_B0_4                0 /* TX - PB0,  RX - PB1,  CK - PB2,  CTS - PB3,  RTS - PB4  */
#define CHIP_IOREMAP_USART4_A5_B5_A6_7_A15      1 /* TX - PA5,  RX - PB5,  CK - PA6,  CTS - PA7,  RTS - PA15 */

#define CHIP_IOREMAP_SPI1_A4_7                  0 /* NSS - PA4,  SCK - PA5, MISO - PA6,  MOSI - PA7 */
#define CHIP_IOREMAP_SPI1_A15_B3_5              1 /* NSS - PA15, SCK - PB3, MISO - PB4,  MOSI - PB5 */
#define CHIP_IOREMAP_SPI1_A12_B6_B8_B7          2 /* NSS - PA12, SCK - PB6, MISO - PB8,  MOSI - PB7 */
#define CHIP_IOREMAP_SPI1_B12_B6_B8_B7          3 /* NSS - PB12, SCK - PB6, MISO - PB8,  MOSI - PB7 */

#define CHIP_IOREMAP_I2C1_B6_7                  0 /* SCL - PB6,  SDA - PB7,  SMBA - PB5 */
#define CHIP_IOREMAP_I2C1_A13_A12               2 /* SCL - PA13, SDA - PA12, SMBA - PB5 */
#define CHIP_IOREMAP_I2C1_B9_B11                3 /* SCL - PB9,  SDA - PB11, SMBA - PB5 */

#define CHIP_IOREMAP_TIM1_A12_A8_11_B12_15              0 /* ETR - PA12, CH1 - PA8,  CH2 - PA9,  CH3 - PA10, CH4 - PA11, BKIN - PB12, CH1N - PB13, CH2N - PB14, CH3N - PB15 */
#define CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1           1 /* ETR - PA12, CH1 - PA8,  CH2 - PA9,  CH3 - PA10, CH4 - PA11, BKIN - PA6,  CH1N - PA7,  CH2N - PB0,  CH3N - PB1  */
#define CHIP_IOREMAP_TIM1_B6_A1_A7_A14_B1_A13_B11_B0_B9 2 /* ETR - PB6,  CH1 - PA1,  CH2 - PA7,  CH3 - PA14, CH4 - PB1,  BKIN - PA13, CH1N - PB11, CH2N - PB0,  CH3N - PB9  */
#define CHIP_IOREMAP_TIM1_A3_A1_A7_A14_B1_A13_B11_B0_B9 3 /* ETR - PA3,  CH1 - PA1,  CH2 - PA7,  CH3 - PA14, CH4 - PB1,  BKIN - PA13, CH1N - PB11, CH2N - PB0,  CH3N - PB9  */
#define CHIP_IOREMAP_TIM1_B6_8_B12_A2_A12_A14_B1_B9     4 /* ETR - PB6,  CH1 - PB7,  CH2 - PB8,  CH3 - PB12, CH4 - PA2,  BKIN - PA12, CH1N - PA14, CH2N - PB1,  CH3N - PB9  */
#define CHIP_IOREMAP_TIM1_A13_B1_B8_B6_A3_A12_A14_A1_B7 5 /* ETR - PA13, CH1 - PB1,  CH2 - PB8,  CH3 - PB6,  CH4 - PA3,  BKIN - PA12, CH1N - PA14, CH2N - PA1,  CH3N - PB7  */
#define CHIP_IOREMAP_TIM1_LSI                           7 /* Controls LSI inputs only for LSI calibration */

#define CHIP_IOREMAP_TIM2_A0_3                  0 /* CH1/ETR - PA0,  CH2 - PA1,  CH3 - PA2,  CH4 - PA3  */
#define CHIP_IOREMAP_TIM2_A15_B3_A2_3           1 /* CH1/ETR - PA15, CH2 - PB3,  CH3 - PA2,  CH4 - PA3  */
#define CHIP_IOREMAP_TIM2_A0_1_B10_11           2 /* CH1/ETR - PA0,  CH2 - PA1,  CH3 - PB10, CH4 - PB11 */
#define CHIP_IOREMAP_TIM2_A15_B3_B10_11         3 /* CH1/ETR - PA15, CH2 - PB3,  CH3 - PB10, CH4 - PB11  */
#define CHIP_IOREMAP_TIM2_A3_A2_B12_A6          4 /* CH1/ETR - PA3,  CH2 - PA2,  CH3 - PB12, CH4 - PA6  */
#define CHIP_IOREMAP_TIM2_A12_A2_B12_A6         5 /* CH1/ETR - PA12, CH2 - PA2,  CH3 - PB12, CH4 - PA6  */
#define CHIP_IOREMAP_TIM2_A12_B8_A5_A4          7 /* CH1/ETR - PA12, CH2 - PB8,  CH3 - PA5,  CH4 - PA4  */

#define CHIP_IOREMAP_TIM3_A6_7_B0_1             0 /* CH1 - PA6,  CH2 - PA7,  CH3 - PB0,  CH4 - PB1  */
#define CHIP_IOREMAP_TIM3_B4_5_B0_1             1 /* CH1 - PB4,  CH2 - PB5,  CH3 - PB0,  CH4 - PB1  */

#define CHIP_IOREMAP_TIM4_B6_9                  0 /* CH1 - PB6,  CH2 - PB7,  CH3 - PB8,  CH4 - PB9  */
#define CHIP_IOREMAP_TIM4_B10_11_B8_9           1 /* CH1 - PB10, CH2 - PB11, CH3 - PB8,  CH4 - PB9  */

#define CHIP_IOREMAP_LPTIM1_B12_15              0 /* CH1 - PB12, CH2 - PB13, ETR - PB14, OUT - PB15 */
#define CHIP_IOREMAP_LPTIM1_B5_B7_B6_B2         1 /* CH1 - PB5,  CH2 - PB7,  ETR - PB6,  OUT - PB2  */

#define CHIP_IOREMAP_CAN1_A11_12                0 /* RX - PA11, TX - PA12 */
#define CHIP_IOREMAP_CAN1_B8_9                  2 /* RX - PB8,  TX - PB9  */
#define CHIP_IOREMAP_CAN1_D0_1                  3 /* RX - PD0,  TX - PD1  */



#endif // CHIP_IOREMAP_DEFS_H
