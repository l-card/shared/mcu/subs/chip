#ifndef CHIP_PER_CTL_H
#define CHIP_PER_CTL_H

#include "chip_per_ids.h"
#include <stdbool.h>


void chip_per_rst(int per_id);
void chip_per_clk_en(int per_id);
void chip_per_clk_dis(int per_id);
bool chip_per_clk_is_en(int per_id);

#endif // CHIP_PER_CTL_H
