#ifndef REGS_CCM_H
#define REGS_CCM_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t VAL;
    __IO uint32_t SET;
    __IO uint32_t CLR;
    __IO uint32_t TOG;
} CHIP_REGS_CCM_GRP_T;

typedef struct {
    CHIP_REGS_CCM_GRP_T TARGET;             /* Target Register */
    CHIP_REGS_CCM_GRP_T MISC;               /* Miscellaneous Register */
    CHIP_REGS_CCM_GRP_T POST;               /* Post Divider Register */
    CHIP_REGS_CCM_GRP_T PRE;                /* Pre Divider Register */
    uint32_t Reserved[3 * 4];
    CHIP_REGS_CCM_GRP_T ACCESS_CTRL;        /* Access Control Register */
} CHIP_REGS_CCM_ROOT_GRP_T;

typedef struct {
    CHIP_REGS_CCM_GRP_T GPR0;              /* General Purpose Register,         Address offset: 0x0000 */
    uint32_t            RESERVED0[127*4];
    CHIP_REGS_CCM_GRP_T PLL_CTRL[39];      /* CCM PLL Control Register 0-38,    Address offset: 0x0800 - 0x0A60 */
    uint32_t            RESERVED1[857*4];
    CHIP_REGS_CCM_GRP_T CCGR[192];         /* CCM Clock Gating Register 0-191,  Address offset: 0x4000 - 0x4BF0 */
    uint32_t            RESERVED2[832*4];
    CHIP_REGS_CCM_ROOT_GRP_T ROOT[142];
} CHIP_REGS_CCM_T;

#define CHIP_REGS_CCM            ((CHIP_REGS_CCM_T *)CHIP_MEMRGN_ADDR_PERIPH_CCM)

/*********** General Purpose Register (CCM_GPR0) ******************************/
#define CHIP_REGFLD_CCM_GPR0_GP0                    (0xFFFFFFFFUL <<  0U)   /* (RW) Timeout cycle count of ipg_clk, when perform read and write */

/*********** CCM PLL Control Register (CCM_PLL_CTRLn) *************************/
#define CHIP_REGFLD_CCM_PLL_CTRL_SET(d)             (0x00000003UL <<  (0U + 4*(d)))   /* (RW) Clock gate control setting for domain d */

#define CHIP_REGFLDVAL_CCM_PLL_CTRL_SET_NONE        0 /* Domain clocks not needed */
#define CHIP_REGFLDVAL_CCM_PLL_CTRL_SET_RUN         1 /* Domain clocks needed when in RUN */
#define CHIP_REGFLDVAL_CCM_PLL_CTRL_SET_RUN_WAIT    2 /* Domain clocks needed when in RUN and WAIT */
#define CHIP_REGFLDVAL_CCM_PLL_CTRL_SET_ALL         3 /* Domain clocks needed all the time */

/*********** CCM PLL CCM Clock Gating Register (CCM_CCGRn) ********************/
#define CHIP_REGFLD_CCM_CCGR_SET(d)                 (0x00000003UL <<  (0U + 4*(d)))   /* (RW) Clock gate control setting for domain d */

#define CHIP_REGFLDVAL_CCM_CCGR_SETTING_NONE        0 /* Domain clocks not needed */
#define CHIP_REGFLDVAL_CCM_CCGR_SETTING_RUN         1 /* Domain clocks needed when in RUN */
#define CHIP_REGFLDVAL_CCM_CCGR_SETTING_RUN_WAIT    2 /* Domain clocks needed when in RUN and WAIT */
#define CHIP_REGFLDVAL_CCM_CCGR_SETTING_ALL         3 /* Domain clocks needed all the time */

/*********** Target Register (CCM_ROOTn_TARGET) *******************************/
#define CHIP_REGFLD_CCM_ROOT_TARGET_POST_PODF       (0x0000003FUL <<  0U)  /* (RW) Post divider divide number. CORE - 3 bit, IP - 1 bit, DRAM_PHYM - not apply. */
#define CHIP_REGFLD_CCM_ROOT_TARGET_PRE_PODF        (0x00000007UL << 16U)  /* (RW) Pre divider divide the number. CORE, DRAM, DRAM_PHYM - not apply. */
#define CHIP_REGFLD_CCM_ROOT_TARGET_MUX             (0x00000007UL << 24U)  /* (RW) Selection of clock sources. DRAM and CORE - 1 bit. */
#define CHIP_REGFLD_CCM_ROOT_TARGET_ENABLE          (0x00000001UL << 28U)  /* (RW) Enable this clock */

/*********** Miscellaneous Register (CCM_ROOTn_MISC) **************************/
#define CHIP_REGFLD_CCM_ROOT_MISC_AUTHEN_FAIL       (0x00000001UL <<  0U)  /* (RW1C) This sticky bit reflects access restricted by access control of this clock. */
#define CHIP_REGFLD_CCM_ROOT_MISC_TIMEOUT           (0x00000001UL <<  4U)  /* (RW1C) This sticky bit reflects time out happened during accessing this clock. */
#define CHIP_REGFLD_CCM_ROOT_MISC_VIOLATE           (0x00000001UL <<  8U)  /* (RW1C) This sticky bit reflects access violation in normal interface of this clock. */

/*********** Post Divider Register (CCM_ROOTn_POST) ***************************/
#define CHIP_REGFLD_CCM_ROOT_POST_POST_PODF         (0x0000003FUL <<  0U)  /* (RW) Post divider divide number. CORE - 3 bit, IP - 1 bit, DRAM_PHYM - not apply. */
#define CHIP_REGFLD_CCM_ROOT_POST_BUSY1             (0x00000001UL <<  7U)  /* (RO) Post divider is applying new set value */
#define CHIP_REGFLD_CCM_ROOT_POST_SELECT            (0x00000001UL << 28U)  /* (RW) Selection of post clock branches. Not applicable to IP */
#define CHIP_REGFLD_CCM_ROOT_POST_BUSY2             (0x00000001UL << 31U)  /* (RO) Clock switching multiplexer is applying new setting */

/*********** Pre Divider Register (CCM_ROOTn_PRE) ***************************/
#define CHIP_REGFLD_CCM_ROOT_PRE_PRE_PODF_B         (0x00000007UL <<  0U)  /* (RW) Pre divider divide number for branch B (CORE, IP,DRAM, DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_BUSY0              (0x00000001UL <<  4U)  /* (RO) Pre divider value for branch B is applying (CORE, IP,DRAM, DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_MUX_B              (0x00000007UL <<  8U)  /* (RW) Selection control of multiplexer of branch B (CORE, IP,DRAM, DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_EN_B               (0x00000001UL << 12U)  /* (RW) Branch B clock gate control (CORE, IP,DRAM, DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_BUSY1              (0x00000001UL << 15U)  /* (RO) EN_B is applied (CORE, IP,DRAM, DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_PRE_PODF_A         (0x00000007UL << 16U)  /* (RW) Pre divider divide number for branch A (CORE, DRAM, DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_BUSY3              (0x00000001UL << 19U)  /* (RO) Pre divider value for branch A is applied (DRAM and DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_MUX_A              (0x00000007UL << 24U)  /* (RW) Selection control of multiplexer of branch A (DRAM and DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_EN_A               (0x00000001UL << 28U)  /* (RW) Branch A clock gate control (DRAM and DRAM_PHYM) */
#define CHIP_REGFLD_CCM_ROOT_PRE_BUSY4              (0x00000001UL << 31U)  /* (RO) EN_A is applied (DRAM and DRAM_PHYM) */

/*********** Access Control Register (CCM_ROOTn_ACCESS_CTRL) ***************************/
#define CHIP_REGFLD_CCM_ROOT_ACCESS_CTRL_DMN_INFO(d)    (0x0000000FUL <<  (0U + 4*(d)))  /* (RW) Information from domain n to pass to others */
#define CHIP_REGFLD_CCM_ROOT_ACCESS_CTRL_OWNER_ID       (0x00000003UL <<  16U)  /* (RO) Current domain that owns semaphore  */
#define CHIP_REGFLD_CCM_ROOT_ACCESS_CTRL_MUTEX          (0x00000001UL <<  20U)  /* (RW) Semaphore to control access */
#define CHIP_REGFLD_CCM_ROOT_ACCESS_CTRL_DMN_WLIST(d)   (0x00000001UL <<  (24U + (d)))  /* (RW) White list of domains that can change setting of this clock root */
#define CHIP_REGFLD_CCM_ROOT_ACCESS_CTRL_SEMA_EN        (0x00000001UL <<  28U)  /* (RW) Enable internal semaphore  */
#define CHIP_REGFLD_CCM_ROOT_ACCESS_CTRL_LOCK           (0x00000001UL <<  31U)  /* (RW) Lock this clock root to use access control */




#endif // REGS_CCM_H
