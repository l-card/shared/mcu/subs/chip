#include "chip.h"


#if CHIP_MPU_SETUP_EN


#define MPU_RGN_SIZE_FLDVAL(size)  (\
    (size) == CHIP_B(32)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_32B : \
    (size) == CHIP_B(64)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_64B : \
    (size) == CHIP_B(128)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_128B : \
    (size) == CHIP_B(256)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_256B : \
    (size) == CHIP_B(512)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_512B : \
    (size) == CHIP_KB(1)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_1KB : \
    (size) == CHIP_KB(2)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_2KB : \
    (size) == CHIP_KB(4)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_4KB : \
    (size) == CHIP_KB(8)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_8KB : \
    (size) == CHIP_KB(16)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_16KB : \
    (size) == CHIP_KB(32)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_32KB : \
    (size) == CHIP_KB(16)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_64KB : \
    (size) == CHIP_KB(128)  ? CHIP_REGFLDVAL_MPU_RASR_SIZE_128KB : \
    (size) == CHIP_KB(256)  ? CHIP_REGFLDVAL_MPU_RASR_SIZE_256KB : \
    (size) == CHIP_KB(512)  ? CHIP_REGFLDVAL_MPU_RASR_SIZE_512KB : \
    (size) == CHIP_MB(1)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_1MB : \
    (size) == CHIP_MB(2)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_2MB : \
    (size) == CHIP_MB(4)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_4MB : \
    (size) == CHIP_MB(8)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_8MB : \
    (size) == CHIP_MB(16)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_16MB : \
    (size) == CHIP_MB(32)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_32MB : \
    (size) == CHIP_MB(16)   ? CHIP_REGFLDVAL_MPU_RASR_SIZE_64MB : \
    (size) == CHIP_MB(128)  ? CHIP_REGFLDVAL_MPU_RASR_SIZE_128MB : \
    (size) == CHIP_MB(256)  ? CHIP_REGFLDVAL_MPU_RASR_SIZE_256MB : \
    (size) == CHIP_MB(512)  ? CHIP_REGFLDVAL_MPU_RASR_SIZE_512MB : \
    (size) == CHIP_GB(1)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_1GB : \
    (size) == CHIP_GB(2)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_2GB : \
    (size) == CHIP_GB(4)    ? CHIP_REGFLDVAL_MPU_RASR_SIZE_4GB : \
    0)


#if defined CHIP_CFG_MPU_EN
    #define CHIP_MPU_EN     CHIP_CFG_MPU_EN
#else
    #define CHIP_MPU_EN     0
#endif

#if defined CHIP_CFG_MPU_DEF_RGN_EN
    #define CHIP_MPU_DEF_RGN_EN     CHIP_CFG_MPU_DEF_RGN_EN
#else
    #define CHIP_MPU_DEF_RGN_EN     0
#endif

#if defined CHIP_CFG_MPU_NMI_FLT_EN
    #define CHIP_MPU_NMI_FLT_EN     CHIP_CFG_MPU_NMI_FLT_EN
#else
    #define CHIP_MPU_NMI_FLT_EN     0
#endif


#if CHIP_CFG_MPU_EN
    #if CHIP_CFG_MPU_RGN_CNT < 1
        #error at least one MPU region must be enabled
    #endif

    #if CHIP_CFG_MPU_RGN_CNT > CHIP_CORE_MPU_REGIONS_CNT
        #error chip not support specified MPU region count
    #endif


    #if CHIP_CFG_MPU_RGN_CNT >= 1
        #ifndef CHIP_CFG_MPU_RGN1_SIZE
            #error MPU Region 1 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN1_SIZE) == 0
            #error invalid MPU Region 1 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN1_ADDR
            #error MPU Region 1 addr is not specified
        #elif (CHIP_CFG_MPU_RGN1_ADDR & (CHIP_CFG_MPU_RGN1_SIZE-1)) != 0
            #error MPU Region 1 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN1_ATTR
            #error MPU Region 1 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN1_ACC
            #define CHIP_CFG_MPU_RGN1_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN1_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN1_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN1_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN1_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN1_NOEXEC
            #define CHIP_CFG_MPU_RGN1_NOEXEC 0
        #endif
    #endif


    #if CHIP_CFG_MPU_RGN_CNT >= 2
        #ifndef CHIP_CFG_MPU_RGN2_SIZE
            #error MPU Region 2 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN2_SIZE) == 0
            #error invalid MPU Region 2 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN2_ADDR
            #error MPU Region 2 addr is not specified
        #elif (CHIP_CFG_MPU_RGN2_ADDR & (CHIP_CFG_MPU_RGN2_SIZE-1)) != 0
            #error MPU Region 2 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN2_ATTR
            #error MPU Region 2 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN2_ACC
            #define CHIP_CFG_MPU_RGN2_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN2_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN2_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN2_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN2_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN2_NOEXEC
            #define CHIP_CFG_MPU_RGN2_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 3
        #ifndef CHIP_CFG_MPU_RGN3_SIZE
            #error MPU Region 3 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN3_SIZE) == 0
            #error invalid MPU Region 3 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN3_ADDR
            #error MPU Region 3 addr is not specified
        #elif (CHIP_CFG_MPU_RGN3_ADDR & (CHIP_CFG_MPU_RGN3_SIZE-1)) != 0
            #error MPU Region 3 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN3_ATTR
            #error MPU Region 3 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN3_ACC
            #define CHIP_CFG_MPU_RGN3_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN3_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN3_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN3_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN3_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN3_NOEXEC
            #define CHIP_CFG_MPU_RGN3_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 4
        #ifndef CHIP_CFG_MPU_RGN4_SIZE
            #error MPU Region 4 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN4_SIZE) == 0
            #error invalid MPU Region 4 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN4_ADDR
            #error MPU Region 4 addr is not specified
        #elif (CHIP_CFG_MPU_RGN4_ADDR & (CHIP_CFG_MPU_RGN4_SIZE-1)) != 0
            #error MPU Region 4 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN4_ATTR
            #error MPU Region 4 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN4_ACC
            #define CHIP_CFG_MPU_RGN4_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN4_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN4_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN4_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN4_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN4_NOEXEC
            #define CHIP_CFG_MPU_RGN4_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 5
        #ifndef CHIP_CFG_MPU_RGN5_SIZE
            #error MPU Region 5 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN5_SIZE) == 0
            #error invalid MPU Region 5 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN5_ADDR
            #error MPU Region 5 addr is not specified
        #elif (CHIP_CFG_MPU_RGN5_ADDR & (CHIP_CFG_MPU_RGN5_SIZE-1)) != 0
            #error MPU Region 5 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN5_ATTR
            #error MPU Region 5 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN5_ACC
            #define CHIP_CFG_MPU_RGN5_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN5_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN5_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN5_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN5_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN5_NOEXEC
            #define CHIP_CFG_MPU_RGN5_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 6
        #ifndef CHIP_CFG_MPU_RGN6_SIZE
            #error MPU Region 6 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN6_SIZE) == 0
            #error invalid MPU Region 6 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN6_ADDR
            #error MPU Region 6 addr is not specified
        #elif (CHIP_CFG_MPU_RGN6_ADDR & (CHIP_CFG_MPU_RGN6_SIZE-1)) != 0
            #error MPU Region 6 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN6_ATTR
            #error MPU Region 6 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN6_ACC
            #define CHIP_CFG_MPU_RGN6_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN6_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN6_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN6_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN6_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN6_NOEXEC
            #define CHIP_CFG_MPU_RGN6_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 7
        #ifndef CHIP_CFG_MPU_RGN7_SIZE
            #error MPU Region 7 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN7_SIZE) == 0
            #error invalid MPU Region 7 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN7_ADDR
            #error MPU Region 7 addr is not specified
        #elif (CHIP_CFG_MPU_RGN7_ADDR & (CHIP_CFG_MPU_RGN7_SIZE-1)) != 0
            #error MPU Region 7 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN7_ATTR
            #error MPU Region 7 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN7_ACC
            #define CHIP_CFG_MPU_RGN7_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN7_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN7_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN7_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN7_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN7_NOEXEC
            #define CHIP_CFG_MPU_RGN7_NOEXEC 0
        #endif
    #endif


    #if CHIP_CFG_MPU_RGN_CNT >= 8
        #ifndef CHIP_CFG_MPU_RGN8_SIZE
            #error MPU Region 8 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN8_SIZE) == 0
            #error invalid MPU Region 8 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN8_ADDR
            #error MPU Region 8 addr is not specified
        #elif (CHIP_CFG_MPU_RGN8_ADDR & (CHIP_CFG_MPU_RGN8_SIZE-1)) != 0
            #error MPU Region 8 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN8_ATTR
            #error MPU Region 8 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN8_ACC
            #define CHIP_CFG_MPU_RGN8_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN8_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN8_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN8_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN8_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN8_NOEXEC
            #define CHIP_CFG_MPU_RGN8_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 9
        #ifndef CHIP_CFG_MPU_RGN9_SIZE
            #error MPU Region 9 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN9_SIZE) == 0
            #error invalid MPU Region 9 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN9_ADDR
            #error MPU Region 9 addr is not specified
        #elif (CHIP_CFG_MPU_RGN9_ADDR & (CHIP_CFG_MPU_RGN9_SIZE-1)) != 0
            #error MPU Region 9 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN9_ATTR
            #error MPU Region 9 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN9_ACC
            #define CHIP_CFG_MPU_RGN9_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN9_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN9_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN9_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN9_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN9_NOEXEC
            #define CHIP_CFG_MPU_RGN9_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 10
        #ifndef CHIP_CFG_MPU_RGN10_SIZE
            #error MPU Region 10 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN10_SIZE) == 0
            #error invalid MPU Region 10 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN10_ADDR
            #error MPU Region 10 addr is not specified
        #elif (CHIP_CFG_MPU_RGN10_ADDR & (CHIP_CFG_MPU_RGN10_SIZE-1)) != 0
            #error MPU Region 10 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN10_ATTR
            #error MPU Region 10 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN10_ACC
            #define CHIP_CFG_MPU_RGN10_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN10_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN10_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN10_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN10_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN10_NOEXEC
            #define CHIP_CFG_MPU_RGN10_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 11
        #ifndef CHIP_CFG_MPU_RGN11_SIZE
            #error MPU Region 11 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN11_SIZE) == 0
            #error invalid MPU Region 11 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN11_ADDR
            #error MPU Region 11 addr is not specified
        #elif (CHIP_CFG_MPU_RGN11_ADDR & (CHIP_CFG_MPU_RGN11_SIZE-1)) != 0
            #error MPU Region 11 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN11_ATTR
            #error MPU Region 11 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN11_ACC
            #define CHIP_CFG_MPU_RGN11_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN11_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN11_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN11_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN11_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN11_NOEXEC
            #define CHIP_CFG_MPU_RGN11_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 12
        #ifndef CHIP_CFG_MPU_RGN12_SIZE
            #error MPU Region 12 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN12_SIZE) == 0
            #error invalid MPU Region 12 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN12_ADDR
            #error MPU Region 12 addr is not specified
        #elif (CHIP_CFG_MPU_RGN12_ADDR & (CHIP_CFG_MPU_RGN12_SIZE-1)) != 0
            #error MPU Region 12 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN12_ATTR
            #error MPU Region 12 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN12_ACC
            #define CHIP_CFG_MPU_RGN12_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN12_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN12_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN12_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN12_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN12_NOEXEC
            #define CHIP_CFG_MPU_RGN12_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 13
        #ifndef CHIP_CFG_MPU_RGN13_SIZE
            #error MPU Region 13 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN13_SIZE) == 0
            #error invalid MPU Region 13 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN13_ADDR
            #error MPU Region 13 addr is not specified
        #elif (CHIP_CFG_MPU_RGN13_ADDR & (CHIP_CFG_MPU_RGN13_SIZE-1)) != 0
            #error MPU Region 13 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN13_ATTR
            #error MPU Region 13 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN13_ACC
            #define CHIP_CFG_MPU_RGN13_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN13_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN13_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN13_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN13_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN13_NOEXEC
            #define CHIP_CFG_MPU_RGN13_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 14
        #ifndef CHIP_CFG_MPU_RGN14_SIZE
            #error MPU Region 14 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN14_SIZE) == 0
            #error invalid MPU Region 14 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN14_ADDR
            #error MPU Region 14 addr is not specified
        #elif (CHIP_CFG_MPU_RGN14_ADDR & (CHIP_CFG_MPU_RGN14_SIZE-1)) != 0
            #error MPU Region 14 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN14_ATTR
            #error MPU Region 14 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN14_ACC
            #define CHIP_CFG_MPU_RGN14_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN14_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN14_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN14_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN14_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN14_NOEXEC
            #define CHIP_CFG_MPU_RGN14_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 15
        #ifndef CHIP_CFG_MPU_RGN15_SIZE
            #error MPU Region 15 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN15_SIZE) == 0
            #error invalid MPU Region 15 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN15_ADDR
            #error MPU Region 15 addr is not specified
        #elif (CHIP_CFG_MPU_RGN15_ADDR & (CHIP_CFG_MPU_RGN15_SIZE-1)) != 0
            #error MPU Region 15 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN15_ATTR
            #error MPU Region 15 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN15_ACC
            #define CHIP_CFG_MPU_RGN15_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN15_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN15_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN15_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN15_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN15_NOEXEC
            #define CHIP_CFG_MPU_RGN15_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 16
        #ifndef CHIP_CFG_MPU_RGN16_SIZE
            #error MPU Region 16 size is not specified
        #elif MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN16_SIZE) == 0
            #error invalid MPU Region 16 size
        #endif

        #ifndef CHIP_CFG_MPU_RGN16_ADDR
            #error MPU Region 16 addr is not specified
        #elif (CHIP_CFG_MPU_RGN16_ADDR & (CHIP_CFG_MPU_RGN16_SIZE-1)) != 0
            #error MPU Region 16 addr is not aligned
        #endif

        #ifndef CHIP_CFG_MPU_RGN16_ATTR
            #error MPU Region 16 memory access attributes are not specified
        #endif

        #ifndef CHIP_CFG_MPU_RGN16_ACC
            #define CHIP_CFG_MPU_RGN16_ACC CHIP_MPU_RGN_ACC_RW_RW
        #endif

        #ifndef CHIP_CFG_MPU_RGN16_SRGN_DISMSK
            #define CHIP_CFG_MPU_RGN16_SRGN_DISMSK 0
        #elif (CHIP_CFG_MPU_RGN16_SIZE < CHIP_B(256)) && (CHIP_CFG_MPU_RGN16_SRGN_DISMSK != 0)
            #error subregions supported only for regions of 256 bytes or more
        #endif

        #ifndef CHIP_CFG_MPU_RGN16_NOEXEC
            #define CHIP_CFG_MPU_RGN16_NOEXEC 0
        #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 17
        #error currently not implemented more than 16 MPU region
    #endif



#endif



#define MPU_RGN_CFG_(i) do { \
    CHIP_REGS_MPU->RBAR = CHIP_REGFLD_MPU_RBAR_VALID | LBITFIELD_SET(CHIP_REGFLD_MPU_RBAR_REGION, i-1) | (CHIP_CFG_MPU_RGN##i##_ADDR & 0xFFFFFFE0UL); \
    CHIP_REGS_MPU->RASR = CHIP_CFG_MPU_RGN##i##_ATTR \
                          | LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_AP, CHIP_CFG_MPU_RGN##i##_ACC) \
                          | LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_XN, CHIP_CFG_MPU_RGN##i##_NOEXEC) \
                          | LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_SRD, CHIP_CFG_MPU_RGN##i##_SRGN_DISMSK) \
                          | LBITFIELD_SET(CHIP_REGFLD_MPU_RASR_SIZE, MPU_RGN_SIZE_FLDVAL(CHIP_CFG_MPU_RGN##i##_SIZE)) \
                          | LBITFIELD_SET(CHIP_REGFLD_MPU_CTRL_ENABLE, 1); \
    } while(0);
#define MPU_RGN_CFG(i) MPU_RGN_CFG_(i)

#define MPU_RGN_DIS_(i) do { \
        CHIP_REGS_MPU->RNR = i-1; \
        CHIP_REGS_MPU->RASR &= ~CHIP_REGFLD_MPU_RASR_ENABLE; \
    } while(0)
#define MPU_RGN_DIS(i) MPU_RGN_DIS_(i)


void chip_mpu_init(void) {
#if CHIP_CORE_SUPPORT_DCACHE
    chip_dcache_disable();
#endif
#if CHIP_CORE_SUPPORT_ICACHE
    chip_icache_disable();
#endif
    chip_dmb();

#ifdef CHIP_REGFLD_SCB_SHCSR_MEMFAULTENA
    CHIP_REGS_SCB->SHCSR &= ~CHIP_REGFLD_SCB_SHCSR_MEMFAULTENA;
#endif

    CHIP_REGS_MPU->CTRL  &= ~CHIP_REGFLD_MPU_CTRL_ENABLE;

#if CHIP_MPU_EN

    #if CHIP_CORE_MPU_REGIONS_CNT >= 1
    #if CHIP_CFG_MPU_RGN_CNT >= 1
        MPU_RGN_CFG(1);
    #else
        MPU_RGN_DIS(1);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 2
    #if CHIP_CFG_MPU_RGN_CNT >= 2
        MPU_RGN_CFG(2);
    #else
        MPU_RGN_DIS(2);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 3
    #if CHIP_CFG_MPU_RGN_CNT >= 3
        MPU_RGN_CFG(3);
    #else
        MPU_RGN_DIS(3);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 4
    #if CHIP_CFG_MPU_RGN_CNT >= 4
        MPU_RGN_CFG(4);
    #else
        MPU_RGN_DIS(4);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 5
    #if CHIP_CFG_MPU_RGN_CNT >= 5
        MPU_RGN_CFG(5);
    #else
        MPU_RGN_DIS(5);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 6
    #if CHIP_CFG_MPU_RGN_CNT >= 6
        MPU_RGN_CFG(6);
    #else
        MPU_RGN_DIS(6);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 7
    #if CHIP_CFG_MPU_RGN_CNT >= 7
        MPU_RGN_CFG(7);
    #else
        MPU_RGN_DIS(7);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 8
    #if CHIP_CFG_MPU_RGN_CNT >= 8
        MPU_RGN_CFG(8);
    #else
        MPU_RGN_DIS(8);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 9
    #if CHIP_CFG_MPU_RGN_CNT >= 9
        MPU_RGN_CFG(9);
    #else
        MPU_RGN_DIS(9);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 10
    #if CHIP_CFG_MPU_RGN_CNT >= 10
        MPU_RGN_CFG(10);
    #else
        MPU_RGN_DIS(10);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 11
    #if CHIP_CFG_MPU_RGN_CNT >= 11
        MPU_RGN_CFG(11);
    #else
        MPU_RGN_DIS(11);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 12
    #if CHIP_CFG_MPU_RGN_CNT >= 12
        MPU_RGN_CFG(12);
    #else
        MPU_RGN_DIS(12);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 13
    #if CHIP_CFG_MPU_RGN_CNT >= 13
        MPU_RGN_CFG(13);
    #else
        MPU_RGN_DIS(13);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 14
    #if CHIP_CFG_MPU_RGN_CNT >= 14
        MPU_RGN_CFG(14);
    #else
        MPU_RGN_DIS(14);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 15
    #if CHIP_CFG_MPU_RGN_CNT >= 15
        MPU_RGN_CFG(15);
    #else
        MPU_RGN_DIS(15);
    #endif
    #endif

    #if CHIP_CORE_MPU_REGIONS_CNT >= 16
    #if CHIP_CFG_MPU_RGN_CNT >= 16
        MPU_RGN_CFG(16);
    #else
        MPU_RGN_DIS(16);
    #endif
    #endif

    #if CHIP_CFG_MPU_RGN_CNT >= 17
        #error not implemented more than 16 MPU region
    #endif


    CHIP_REGS_MPU->CTRL  = (CHIP_REGS_MPU->CTRL & CHIP_REGMSK_MPU_CTRL_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_MPU_CTRL_PRIVDEFENA, CHIP_MPU_DEF_RGN_EN)
            | LBITFIELD_SET(CHIP_REGFLD_MPU_CTRL_HFNMIENA, CHIP_MPU_NMI_FLT_EN)
            | LBITFIELD_SET(CHIP_REGFLD_MPU_CTRL_ENABLE, 1);
    chip_dsb();
    chip_isb();

#ifdef CHIP_REGFLD_SCB_SHCSR_MEMFAULTENA
    CHIP_REGS_SCB->SHCSR |= CHIP_REGFLD_SCB_SHCSR_MEMFAULTENA;
#endif
#endif
}
#endif
