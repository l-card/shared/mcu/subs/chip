CHIP_MAKEFILE = $(abspath $(lastword $(MAKEFILE_LIST)))
CHIP_DIR := $(dir $(CHIP_MAKEFILE))

# CHIP_TARGET_DEV должен быть определен и задавать полное название чипа
ifeq (,$(CHIP_TARGET_DEV))
    $(error CHIP_TARGET_DEV is not specified)
else
    # на каждый чип в директории chipdevs лежит свой .mk файл с соответствующим
    # именем, который включает определение CHIP_TARGET_FAMILY, задающее семейство
    # чипов, к которому он относится, а также возможно другие дополнительные определения
    CHIP_DEVMAP_FILE := $(CHIP_DIR)/chipdevs/$(CHIP_TARGET_DEV).mk
    ifeq (,$(wildcard $(CHIP_DEVMAP_FILE)))
        $(error $(CHIP_TARGET_DEV) is not supported cpu device)
    else
        include $(CHIP_DEVMAP_FILE)
    endif
endif

CHIP_INC_DIRS := $(CHIP_DIR)  $(CHIP_DIR)/lbitfield
CHIP_TARGET_DIR := $(CHIP_DIR)/chips/$(CHIP_TARGET_FAMILY)
CHIP_SHARED_DIR := $(CHIP_DIR)/chips/shared

# для каждого семейства чипов должен быть файл chip_init.c с кодом инииализации
CHIP_INIT_SRC = $(CHIP_TARGET_DIR)/init/chip_init.c
# для каждого семейства чипов должна быть директория в chips с соответствующим названием
# и с .mk файлом, включающим все файлы и определения для данного семейства
ifeq (,$(wildcard $(CHIP_TARGET_DIR)/$(CHIP_TARGET_FAMILY).mk))
    $(error $(CHIP_TARGET_FAMILY) is not supported cpu family ($(CHIP_TARGET_DIR)))
else
    TARGET_CPU := $(CHIP_TARGET_FAMILY)
    include $(CHIP_TARGET_DIR)/$(CHIP_TARGET_FAMILY).mk
    CHIP_INC_DIRS += $(CHIP_TARGET_DIR)
endif

CHIP_GEN_SRC += $(CHIP_INIT_SRC)

# если задана переменная CHIP_TARGET_ARCHTYPE, то включаем общие для заданной
# архитектуры определения и файлы путем включения chip_archtype.mk из
# соответствующей поддиректории в директории archtypes
ifneq (,$(CHIP_TARGET_ARCHTYPE))
    CHIP_ARCHTYPE_DIR := $(CHIP_DIR)/archtypes/$(CHIP_TARGET_ARCHTYPE)
    include $(CHIP_ARCHTYPE_DIR)/chip_archtype.mk
endif


CHIP_SRC := $(CHIP_GEN_SRC) $(CHIP_STARTUP_SRC)
CHIP_NOSTARTUP_SRC += $(CHIP_GEN_SRC)


CC   = $(CHIP_TOOLCHAIN_TARGET)gcc
CP   = $(CHIP_TOOLCHAIN_TARGET)objcopy
AS   = $(CHIP_TOOLCHAIN_TARGET)gcc -x assembler-with-cpp
SIZE = $(CHIP_TOOLCHAIN_TARGET)size
LDR  = $(CHIP_TOOLCHAIN_TARGET)ldr
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary
