#ifndef CHIP_LPC11U6X_REGS_GPIO_H
#define CHIP_LPC11U6X_REGS_GPIO_H

#include <stdint.h>

/**
 * @brief  GPIO port register block structure
 */
typedef struct {                /*!< GPIO_PORT Structure */
    __IO uint8_t B[128][32];    /*!< Offset 0x0000: Byte pin registers ports 0 to n; pins PIOn_0 to PIOn_31 */
    __IO uint32_t W[32][32];    /*!< Offset 0x1000: Word pin registers port 0 to n */
    __IO uint32_t DIR[32];      /*!< Offset 0x2000: Direction registers port n */
    __IO uint32_t MASK[32];     /*!< Offset 0x2080: Mask register port n */
    __IO uint32_t PIN[32];      /*!< Offset 0x2100: Portpin register port n */
    __IO uint32_t MPIN[32];     /*!< Offset 0x2180: Masked port register port n */
    __IO uint32_t SET[32];      /*!< Offset 0x2200: Write: Set register for port n Read: output bits for port n */
    __O  uint32_t CLR[32];      /*!< Offset 0x2280: Clear port n */
    __O  uint32_t NOT[32];      /*!< Offset 0x2300: Toggle port n */
} CHIP_REGS_GPIO_T;

#define CHIP_REGS_GPIO          ((CHIP_REGS_GPIO_T *) CHIP_MEMRGN_ADDR_PERIPH_GPIO)

#endif /* CHIP_LPC11U6X_REGS_GPIO_H */
