#ifndef CHIP_CLK_DEFS_H
#define CHIP_CLK_DEFS_H

#include "chip_std_defs.h"
#include "chip_dev_spec_features.h"


#define CHIP_CLK_LSE_DRV_LOWEST         0
#define CHIP_CLK_LSE_DRV_MEDIUM_LOW     1
#define CHIP_CLK_LSE_DRV_MEDIUM_HIGH    2
#define CHIP_CLK_LSE_DRV_HIGHEST        3












/* Номера идентификаторов различных клоков, которые могут использоваться в качестве
 * источников.
 * Номера используются исключительно для уникальной идентификации
 * для сравнения настроек, сами номера должны быть уникальные,
 * но не связаны с какими-либо значениеми самого контроллера.
 * В настройках источников сигнала используются значения без суффикса _ID,
 * а уже по этомим значениям с помощью макросов CHIP_CLK_ID_VAL получается
 * ID из таблицы. Это позволяет также по определению получить параметры выбранной
 * частоты с помощью CHIP_CLK_ID_EN() и CHIP_CLK_ID_FREQ() */
#define CHIP_CLK_HSE_ID             1
#define CHIP_CLK_LSE_ID             2
#define CHIP_CLK_HSI16_ID           3
#define CHIP_CLK_LSI_ID             4
#define CHIP_CLK_HSI48_ID           5
#define CHIP_CLK_HSISYS_ID          6
#define CHIP_CLK_I2S_CKIN_ID        7
#define CHIP_CLK_RTC_ID             8
#define CHIP_CLK_RTC_WAKEUP_ID      9
#define CHIP_CLK_HSI16_DIV8_ID      10 /* HSI16/8 для RNG */
#define CHIP_CLK_HSI16_DIV488_ID    11 /* HSI16/488 для CEC */
#define CHIP_CLK_HSE_DIV32_ID       12 /* HSE/32 для RTC */
#define CHIP_CLK_PLLR_ID            100
#define CHIP_CLK_PLLQ_ID            101
#define CHIP_CLK_PLLP_ID            102
#define CHIP_CLK_SYS_ID             103
#define CHIP_CLK_AHB_ID             104
#define CHIP_CLK_AHB_DIV8_ID        105 /* AHB/8 для system timer */
#define CHIP_CLK_APB_ID             106
#define CHIP_CLK_TIMP_ID            200





#endif // CHIP_CLK_DEFS_H

