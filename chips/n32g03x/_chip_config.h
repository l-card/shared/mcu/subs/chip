﻿#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H


#define CHIP_CFG_STACK_WORDS_LEN    512 /* размер стека */
#define CHIP_CFG_LTIMER_EN          1

/* ------------------ Внешний высокочастотный источник частоты HSE -----------*/
#define CHIP_CFG_CLK_HSE_MODE           CHIP_CLK_EXTMODE_OSC    /* режим внешнего источника HSE - внешний клок */
#define CHIP_CFG_CLK_HSE_FREQ           CHIP_MHZ(16)            /* значение внешней частоты HSE */
#define CHIP_CFG_CLK_HSE_CSS_EN         0                       /* разрешение контроля внешней частоты HSE */
#define CHIP_CFG_CLK_HSE_PIN_PA0        0                       /* использование PA0 вместо PF0 в качестве входа OSC_IN (только n32g032 и только для режима EXTMODE_CLOCK) */

/* ------------------ Внутренний высокочастотный источник частоты HSI --------*/
#define CHIP_CFG_CLK_HSI_EN         1           /* разрешение источника частоты HSI */


/* ------------------ Настройки PLL ------------------------------------------*/
#define CHIP_CFG_CLK_PLL_EN         0                     /* разрешение PLL */
#define CHIP_CFG_CLK_PLL_SRC        CHIP_CLK_HSE          /* источник входной частоты для PLL: HSI, HSE */
#define CHIP_CFG_CLK_PLL_PREDIV     1                     /* предделитель входной частоты PLL (1..4) */
#define CHIP_CFG_CLK_PLL_MUL        6                     /* множитель для результирующей частоты (3..18) */
#define CHIP_CFG_CLK_PLL_OUTDIV     3                     /* Делитель выходной частоты PLL (1..4) */
#define CHIP_CFG_CLK_PLL_BYPASS     0                     /* PLL Bypass - не используются MUL и OUTDIV */


/* ------------------ Системная частота --------------------------------------*/
#define CHIP_CFG_CLK_SYS_SRC        CHIP_CLK_HSE          /* источник частоты для системного клока: HSI, HSE, PLL, LSE, LSI */

#define CHIP_CFG_CLK_AHB_DIV        1 /* делитель частоты шыны AHB из CLK_SYS (1,2,4,8,16,64,128,256,512) */
#define CHIP_CFG_CLK_APB1_DIV       1 /* делитель частоты APB1 из AHB (1,2,4,8,16) */
#define CHIP_CFG_CLK_APB2_DIV       1 /* делитель частоты APB2 из AHB (1,2,4,8,16) */



/* ----------------------- Настройки MCO (выходы частоты) --------------------*/
#define CHIP_CFG_CLK_MCO_EN          1 /* разрешение генерации выходной частоты MCO1 */
#define CHIP_CFG_CLK_MCO_SRC         CHIP_CLK_HSE /* Источник частоты для MCO (LSI, LSE, SYS, HSI, HSE, PLL) */
#define CHIP_CFG_CLK_MCO_PLL_DIV     2 /* делитель частоты вывода Pот PLL (от 2 до 15) */
#define CHIP_CFG_CLK_MCO_PIN         CHIP_PIN_PA8_MCO



/* источники частот для периферии, где их можно выбрать явно */
#define CHIP_CFG_CLK_LPUART1_SRC     CHIP_CLK_APB1        /* источник частоты для LPUART1 (APB1, SYS, HSI, HSE, LSI, LSE) */
#define CHIP_CFG_CLK_LPUART2_SRC     CHIP_CLK_APB1        /* (n32g032) источник частоты для LPUART2 (APB1, SYS, HSI, HSE, LSI, LSE) */
#define CHIP_CFG_CLK_LPTIM_SRC       CHIP_CLK_APB1        /* источник частоты для LPTIM (APB1, HSI, HSE, LSI, LSE, COMP) */
#define CHIP_CFG_CLK_TIM1_8_SRC      CHIP_CLK_APB2_TIM    /* источник частоты для TIM1/TIM8 (APB2_TIM, SYS) */

#define CHIP_CFG_CLK_RNG_PREDIV      1                    /* (n32g032) делитель частоты генератора случайных чисел (1..32) */


/* ----------------------- настройки частоты АЦП -----------------------------*/
#define CHIP_CFG_CLK_ADC_1M_SRC     CHIP_CLK_HSI         /* источник частоты АЦП 1MHZ (HSE, HSI) */
#define CHIP_CFG_CLK_ADC_1M_DIV     8                    /* делитель частоты АЦП 1MHZ (1..32) */

#define CHIP_CFG_CLK_ADC_SRC        CHIP_CLK_ADC_HCLK    /* источник частоты АЦП (ADC_HCLK, ADC_PLL) (устанавливается только при CHIP_CFG_CLK_ADC_EN = 1) */
#define CHIP_CFG_CLK_ADC_PLL_DIV    1                    /* делитель частоты ADC_PLL (0(disabled),1,2,4,6,8,10,12,16,32,64,128,256) */
#define CHIP_CFG_CLK_ADC_HCLK_DIV   2                    /* делитель частоты ADC_HCLK (1,2,4,6,8,10,12,16,32)*/

/* разрешение клоков периферии при старте */
#define CHIP_CFG_CLK_DMA_EN         0 /* AHB */
#define CHIP_CFG_CLK_SRAM_EN        1 /* AHB */
#define CHIP_CFG_CLK_FLASH_EN       1 /* AHB */
#define CHIP_CFG_CLK_CRC_EN         0 /* AHB */
#define CHIP_CFG_CLK_AFIO_EN        1 /* APB2 */
#define CHIP_CFG_CLK_GPIOA_EN       1 /* APB2 */
#define CHIP_CFG_CLK_GPIOB_EN       1 /* APB2 */
#define CHIP_CFG_CLK_GPIOC_EN       1 /* APB2 */
#define CHIP_CFG_CLK_GPIOD_EN       1 /* APB2, n32g032 */
#define CHIP_CFG_CLK_GPIOF_EN       1 /* APB2 */

#define CHIP_CFG_CLK_TIM1_EN        0 /* APB2 */
#define CHIP_CFG_CLK_TIM3_EN        0 /* APB1 */
#define CHIP_CFG_CLK_TIM4_EN        0 /* APB1, n32g032 */
#define CHIP_CFG_CLK_TIM6_EN        0 /* APB1 */
#define CHIP_CFG_CLK_TIM8_EN        0 /* APB2 */
#define CHIP_CFG_CLK_LPTIM_EN       0 /* APB1 */

#define CHIP_CFG_CLK_USART1_EN      1 /* APB2 */
#define CHIP_CFG_CLK_USART2_EN      1 /* APB1 */
#define CHIP_CFG_CLK_UART5_EN       0 /* APB1, n32g032 */
#define CHIP_CFG_CLK_UART6_EN       0 /* APB2, n32g032 */
#define CHIP_CFG_CLK_LPUART1_EN     1 /* APB1 */
#define CHIP_CFG_CLK_LPUART2_EN     0 /* APB1, n32g032 */

#define CHIP_CFG_CLK_SPI1_EN        0 /* APB2 */
#define CHIP_CFG_CLK_SPI2_EN        0 /* APB2 */
#define CHIP_CFG_CLK_SPI3_EN        0 /* APB2, n32g032 */

#define CHIP_CFG_CLK_I2C1_EN        0 /* APB1 */
#define CHIP_CFG_CLK_I2C2_EN        0 /* APB1 */

#define CHIP_CFG_CLK_CAN1_EN        0 /* APB1, n32g032 */

#define CHIP_CFG_CLK_WWDG_EN        0 /* APB1 */
#define CHIP_CFG_CLK_PWR_EN         1 /* APB1 */

#define CHIP_CFG_CLK_ADC_EN         0 /* AHB */
#define CHIP_CFG_CLK_COMP_EN        0 /* APB1 */
#define CHIP_CFG_CLK_OPA_EN         0 /* APB1 */
#define CHIP_CFG_CLK_TSC_EN         0 /* APB1, n32g032 */

#define CHIP_CFG_CLK_HSQRT_EN       0 /* AHB */
#define CHIP_CFG_CLK_HDIV_EN        0 /* AHB */
#define CHIP_CFG_CLK_RNG_EN         0 /* AHB, n32g032 */
#define CHIP_CFG_CLK_SAC_EN         0 /* AHB, n32g032 */
#define CHIP_CFG_CLK_BEEP1_EN       0 /* APB1 */
#define CHIP_CFG_CLK_BEEP2_EN       0 /* APB1, n32g032 */

#endif // CHIP_CONFIG_H

