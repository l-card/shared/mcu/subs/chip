#ifndef CHIP_INIT_H
#define CHIP_INIT_H

#include "init/chip_cortexm_init.h"
#include "chip_config.h"

void chip_init(void);
void chip_main_prestart(void);



#endif // CHIP_INIT_H
