#ifndef CHIP_MMAP_H
#define CHIP_MMAP_H

#define CHIP_MEMRGN_ADDR_ILM          0x00000000  /* Instruction Local Memory, 256 KBytes */
#define CHIP_MEMRGN_ADDR_DLM          0x00080000  /* Data Local Memory, 256 KBytes */
#define CHIP_MEMRGN_ADDR_FGPIO        0x000C0000  /* Fast GPIO Controller, 192 Bytes */
#define CHIP_MEMRGN_ADDR_CPU0_ILM_SLV 0x01000000  /* CPU0 ILM Mirroring, 256 KBytes */
#define CHIP_MEMRGN_ADDR_CPU0_DLM_SLV 0x01040000  /* CPU0 DLM Mirroring, 256 KBytes */
#define CHIP_MEMRGN_ADDR_XRAM0        0x01080000  /* AXI SRAM0, 512 KBytes */
#define CHIP_MEMRGN_ADDR_XRAM1        0x01100000  /* AXI SRAM1, 512 KBytes */
#define CHIP_MEMRGN_ADDR_CPU1_ILM_SLV 0x01180000  /* CPU1 ILM Mirroring, 256 KBytes */
#define CHIP_MEMRGN_ADDR_CPU1_DLM_SLV 0x011C0000  /* CPU1 DLM Mirroring, 256 KBytes */
#define CHIP_MEMRGN_ADDR_ROM          0x20000000  /* Read-only Memory ROM, 128 KBytes */
#define CHIP_MEMRGN_ADDR_DM           0x30000000  /* Debugging Module DEBUG, 1 MBytes */
#define CHIP_MEMRGN_ADDR_FEMC_MEM     0x40000000  /* FEMC Memory Space, 256 MBytes */
#define CHIP_MEMRGN_ADDR_XPI0_MEM     0x80000000  /* Serial bus controller XPI0 memory space, 255 MBytes */
#define CHIP_MEMRGN_ADDR_XPI1_MEM     0x90000000  /* Serial bus controller XPI1 memory space, 255 MBytes */
#define CHIP_MEMRGN_ADDR_PLIC         0xE4000000  /* Platform Interrupt Controller PLIC, 4 MBytes */
#define CHIP_MEMRGN_ADDR_MCHTMR       0xE6000000  /* Machine timer, 1 MBytes */
#define CHIP_MEMRGN_ADDR_PLICSW       0xE6400000  /* Platform software interrupt controller, 4 MBytes */
#define CHIP_MEMRGN_ADDR_GPIO0        0xF0000000  /* GPIO Controller GPIO0 */
#define CHIP_MEMRGN_ADDR_GPIO1        0xF0004000  /* GPIO Controller GPIO1 */
#define CHIP_MEMRGN_ADDR_GPIOM        0xF0008000  /* GPIO Manager */
#define CHIP_MEMRGN_ADDR_ADC0         0xF0010000  /* Analog-to-digital converter ADC0 */
#define CHIP_MEMRGN_ADDR_ADC1         0xF0014000  /* Analog-to-digital converter ADC1 */
#define CHIP_MEMRGN_ADDR_ADC2         0xF0018000  /* Analog-to-digital converter ADC2 */
#define CHIP_MEMRGN_ADDR_ADC3         0xF001C000  /* Analog-to-digital converter ADC3 */
#define CHIP_MEMRGN_ADDR_ACMP         0xF0020000  /* Analog comparator */
#define CHIP_MEMRGN_ADDR_SPI0         0xF0030000  /* Serial Peripheral Bus SPI0 */
#define CHIP_MEMRGN_ADDR_SPI1         0xF0034000  /* Serial Peripheral Bus SPI1 */
#define CHIP_MEMRGN_ADDR_SPI2         0xF0038000  /* Serial Peripheral Bus SPI2 */
#define CHIP_MEMRGN_ADDR_SPI3         0xF003C000  /* Serial Peripheral Bus SPI3 */
#define CHIP_MEMRGN_ADDR_UART0        0xF0040000  /* Universal Asynchronous Receiver-Transmitter UART0 */
#define CHIP_MEMRGN_ADDR_UART1        0xF0044000  /* Universal Asynchronous Receiver-Transmitter UART1 */
#define CHIP_MEMRGN_ADDR_UART2        0xF0048000  /* Universal Asynchronous Receiver-Transmitter UART2 */
#define CHIP_MEMRGN_ADDR_UART3        0xF004C000  /* Universal Asynchronous Receiver-Transmitter UART3 */
#define CHIP_MEMRGN_ADDR_UART4        0xF0050000  /* Universal Asynchronous Receiver-Transmitter UART4 */
#define CHIP_MEMRGN_ADDR_UART5        0xF0054000  /* Universal Asynchronous Receiver-Transmitter UART5 */
#define CHIP_MEMRGN_ADDR_UART6        0xF0058000  /* Universal Asynchronous Receiver-Transmitter UART6 */
#define CHIP_MEMRGN_ADDR_UART7        0xF005C000  /* Universal Asynchronous Receiver-Transmitter UART7 */
#define CHIP_MEMRGN_ADDR_UART8        0xF0060000  /* Universal Asynchronous Receiver-Transmitter UART8 */
#define CHIP_MEMRGN_ADDR_UART9        0xF0064000  /* Universal Asynchronous Receiver-Transmitter UART9 */
#define CHIP_MEMRGN_ADDR_UART10       0xF0068000  /* Universal Asynchronous Receiver-Transmitter UART10 */
#define CHIP_MEMRGN_ADDR_UART11       0xF006C000  /* Universal Asynchronous Receiver-Transmitter UART11 */
#define CHIP_MEMRGN_ADDR_UART12       0xF0070000  /* Universal Asynchronous Receiver-Transmitter UART12 */
#define CHIP_MEMRGN_ADDR_UART13       0xF0074000  /* Universal Asynchronous Receiver-Transmitter UART13 */
#define CHIP_MEMRGN_ADDR_UART14       0xF0078000  /* Universal Asynchronous Receiver-Transmitter UART14 */
#define CHIP_MEMRGN_ADDR_UART15       0xF007C000  /* Universal Asynchronous Receiver-Transmitter UART15 */
#define CHIP_MEMRGN_ADDR_CAN0         0xF0080000  /* Controller Area Network CAN0 */
#define CHIP_MEMRGN_ADDR_CAN1         0xF0084000  /* Controller Area Network CAN1 */
#define CHIP_MEMRGN_ADDR_CAN2         0xF0088000  /* Controller Area Network CAN2 */
#define CHIP_MEMRGN_ADDR_CAN3         0xF008C000  /* Controller Area Network CAN3 */
#define CHIP_MEMRGN_ADDR_WDG0         0xF0090000  /* Watchdog WDG0 */
#define CHIP_MEMRGN_ADDR_WDG1         0xF0094000  /* Watchdog WDG1 */
#define CHIP_MEMRGN_ADDR_WDG2         0xF0098000  /* Watchdog WDG2 */
#define CHIP_MEMRGN_ADDR_WDG3         0xF009C000  /* Watchdog WDG3 */
#define CHIP_MEMRGN_ADDR_MBX0A        0xF00A0000  /* Mailbox MBX0A */
#define CHIP_MEMRGN_ADDR_MBX0B        0xF00A4000  /* Mailbox MBX0B */
#define CHIP_MEMRGN_ADDR_MBX1A        0xF00A8000  /* Mailbox MBX1A */
#define CHIP_MEMRGN_ADDR_MBX1B        0xF00AC000  /* Mailbox MBX1B */
#define CHIP_MEMRGN_ADDR_PTPC         0xF00B0000  /* High-precision time synchronization protocol controller PTPC */
#define CHIP_MEMRGN_ADDR_DMAMUX       0xF00C0000  /* DMA request router DMAMUX */
#define CHIP_MEMRGN_ADDR_HDMA         0xF00C4000  /* DMA controller HDMA */
#define CHIP_MEMRGN_ADDR_RNG          0xF00C8000  /* Random number generator RNG */
#define CHIP_MEMRGN_ADDR_KEYM         0xF00CC000  /* Key manager */
#define CHIP_MEMRGN_ADDR_I2S0         0xF0100000  /* Inter-IC Sound bus I2S0 */
#define CHIP_MEMRGN_ADDR_I2S1         0xF0104000  /* Inter-IC Sound bus I2S1 */
#define CHIP_MEMRGN_ADDR_I2S2         0xF0108000  /* Inter-IC Sound bus I2S2 */
#define CHIP_MEMRGN_ADDR_I2S3         0xF010C000  /* Inter-IC Sound bus I2S3 */
#define CHIP_MEMRGN_ADDR_DAO          0xF0110000  /* Digital audio output DAO */
#define CHIP_MEMRGN_ADDR_PDM          0xF0114000  /* Digital microphone PDM */
#define CHIP_MEMRGN_ADDR_PWM0         0xF0200000  /* PWM timer PWM0 */
#define CHIP_MEMRGN_ADDR_HALL0        0xF0204000  /* Hall sensor interface HALL0 */
#define CHIP_MEMRGN_ADDR_QEI0         0xF0208000  /* Quadrature encoder interface QEI0 */
#define CHIP_MEMRGN_ADDR_TRGM0        0xF020C000  /* Interconnect manager TRGM0 */
#define CHIP_MEMRGN_ADDR_PWM1         0xF0210000  /* PWM timer PWM1 */
#define CHIP_MEMRGN_ADDR_HALL1        0xF0214000  /* Hall sensor interface HALL1 */
#define CHIP_MEMRGN_ADDR_QEI1         0xF0218000  /* Quadrature encoder interface QEI1 */
#define CHIP_MEMRGN_ADDR_TRGM1        0xF021C000  /* Interconnect manager TRGM1 */
#define CHIP_MEMRGN_ADDR_PWM2         0xF0220000  /* PWM timer PWM2 */
#define CHIP_MEMRGN_ADDR_HALL2        0xF0224000  /* Hall sensor interface HALL2 */
#define CHIP_MEMRGN_ADDR_QEI2         0xF0228000  /* Quadrature encoder interface QEI2 */
#define CHIP_MEMRGN_ADDR_TRGM2        0xF022C000  /* Interconnect manager TRGM2 */
#define CHIP_MEMRGN_ADDR_PWM3         0xF0230000  /* PWM timer PWM3 */
#define CHIP_MEMRGN_ADDR_HALL3        0xF0234000  /* Hall sensor interface HALL3 */
#define CHIP_MEMRGN_ADDR_QEI3         0xF0238000  /* Quadrature encoder interface QEI3 */
#define CHIP_MEMRGN_ADDR_TRGM3        0xF023C000  /* Interconnect manager TRGM3 */
#define CHIP_MEMRGN_ADDR_SYNT         0xF0240000  /* Synchronization timer SYNT */
#define CHIP_MEMRGN_ADDR_HRAM         0xF0300000  /* General purpose memory AHB SRAM, 32 KBytes */
#define CHIP_MEMRGN_ADDR_LCDC         0xF1000000  /* LCD controller */
#define CHIP_MEMRGN_ADDR_CAM0         0xF1008000  /* Camera interface CAM0 */
#define CHIP_MEMRGN_ADDR_CAM1         0xF100C000  /* Camera interface CAM1 */
#define CHIP_MEMRGN_ADDR_PDMA         0xF1010000  /* 2D graphics acceleration PDMA */
#define CHIP_MEMRGN_ADDR_JPEG         0xF1014000  /* JPEG codec */
#define CHIP_MEMRGN_ADDR_ENET0        0xF2000000  /* Ethernet controller ENET0 */
#define CHIP_MEMRGN_ADDR_ENET1        0xF2004000  /* Ethernet controller ENET1 */
#define CHIP_MEMRGN_ADDR_NTMR0        0xF2010000  /* Ethernet timer NTMR0 */
#define CHIP_MEMRGN_ADDR_NTMR1        0xF2014000  /* Ethernet timer NTMR1 */
#define CHIP_MEMRGN_ADDR_USB0         0xF2020000  /* Universal Serial Bus USB0 */
#define CHIP_MEMRGN_ADDR_USB1         0xF2024000  /* Universal Serial Bus USB1 */
#define CHIP_MEMRGN_ADDR_SDXC0        0xF2030000  /* SD/EMMC controller SDXC0 */
#define CHIP_MEMRGN_ADDR_SDXC1        0xF2034000  /* SD/EMMC controller SDXC1 */
#define CHIP_MEMRGN_ADDR_CONCTL       0xF2040000  /* Communication system controller CONCTL */
#define CHIP_MEMRGN_ADDR_GPTMR0       0xF3000000  /* General timer GPTMR0 */
#define CHIP_MEMRGN_ADDR_GPTMR1       0xF3004000  /* General timer GPTMR1 */
#define CHIP_MEMRGN_ADDR_GPTMR2       0xF3008000  /* General timer GPTMR2 */
#define CHIP_MEMRGN_ADDR_GPTMR3       0xF300C000  /* General timer GPTMR3 */
#define CHIP_MEMRGN_ADDR_GPTMR4       0xF3010000  /* General timer GPTMR4 */
#define CHIP_MEMRGN_ADDR_GPTMR5       0xF3014000  /* General timer GPTMR5 */
#define CHIP_MEMRGN_ADDR_GPTMR6       0xF3018000  /* General timer GPTMR6 */
#define CHIP_MEMRGN_ADDR_GPTMR7       0xF301C000  /* General timer GPTMR7 */
#define CHIP_MEMRGN_ADDR_I2C0         0xF3020000  /* Inter-Integrated Circuit bus I2C0 */
#define CHIP_MEMRGN_ADDR_I2C1         0xF3024000  /* Inter-Integrated Circuit bus I2C1 */
#define CHIP_MEMRGN_ADDR_I2C2         0xF3028000  /* Inter-Integrated Circuit bus I2C2 */
#define CHIP_MEMRGN_ADDR_I2C3         0xF302C000  /* Inter-Integrated Circuit bus I2C3 */
#define CHIP_MEMRGN_ADDR_XPI0         0xF3040000  /* Serial bus controller XPI0 */
#define CHIP_MEMRGN_ADDR_XPI1         0xF3044000  /* Serial bus controller XPI1 */
#define CHIP_MEMRGN_ADDR_XDMA         0xF3048000  /* DMA controller XDMA */
#define CHIP_MEMRGN_ADDR_SDP          0xF304C000  /* Secure data processor SDP */
#define CHIP_MEMRGN_ADDR_FEMC         0xF3050000  /* FEMC controller */
#define CHIP_MEMRGN_ADDR_SYSCTL       0xF4000000  /* System control module */
#define CHIP_MEMRGN_ADDR_IOC          0xF4040000  /* IO controller IOC */
#define CHIP_MEMRGN_ADDR_OTPSHW       0xF4080000  /* OTP controller shadow registers */
#define CHIP_MEMRGN_ADDR_PPOR         0xF40C0000  /* Power management domain reset module */
#define CHIP_MEMRGN_ADDR_PCFG         0xF40C4000  /* Power management domain configuration module */
#define CHIP_MEMRGN_ADDR_OTP          0xF40C8000  /* OTP controller */
#define CHIP_MEMRGN_ADDR_PSEC         0xF40CC000  /* Power management domain security manager */
#define CHIP_MEMRGN_ADDR_PMON         0xF40D0000  /* Power management domain monitor */
#define CHIP_MEMRGN_ADDR_PGPR         0xF40D4000  /* Power management domain general registers */
#define CHIP_MEMRGN_ADDR_PIOC         0xF40D8000  /* Power management domain IO controller */
#define CHIP_MEMRGN_ADDR_PGPIO        0xF40DC000  /* Power management domain GPIO controller */
#define CHIP_MEMRGN_ADDR_PTMR         0xF40E0000  /* Power management domain general timer PTMR */
#define CHIP_MEMRGN_ADDR_PUART        0xF40E4000  /* Power management domain Universal Asynchronous Receiver Transmitter PUART */
#define CHIP_MEMRGN_ADDR_PWDG         0xF40E8000  /* Power management domain watchdog PWDG */
#define CHIP_MEMRGN_ADDR_VAD          0xF40EC000  /* Language detection module */
#define CHIP_MEMRGN_ADDR_PMIC_MEM     0xF40F0000  /* General purpose memory APB SRAM */
#define CHIP_MEMRGN_ADDR_PLLCTL       0xF4100000  /* PLL controller */
#define CHIP_MEMRGN_ADDR_BPOR         0xF5004000  /* Battery backup domain reset management module */
#define CHIP_MEMRGN_ADDR_BCFG         0xF5008000  /* Battery backup domain management module */
#define CHIP_MEMRGN_ADDR_BUTN         0xF500C000  /* Battery backup domain button module */
#define CHIP_MEMRGN_ADDR_BIOC         0xF5010000  /* Battery backup domain IO controller */
#define CHIP_MEMRGN_ADDR_BGPIO        0xF5014000  /* Battery backup domain GPIO controller */
#define CHIP_MEMRGN_ADDR_BGPR         0xF5018000  /* Battery backup domain general registers */
#define CHIP_MEMRGN_ADDR_RTCSHW       0xF501C000  /* RTC shadow registers */
#define CHIP_MEMRGN_ADDR_BSEC         0xF5040000  /* Battery backup domain security manager */
#define CHIP_MEMRGN_ADDR_RTC          0xF5044000  /* Real time clock */
#define CHIP_MEMRGN_ADDR_BKEY         0xF5048000  /* Battery backup domain key management */
#define CHIP_MEMRGN_ADDR_BMON         0xF504C000  /* Battery backup domain monitor */
#define CHIP_MEMRGN_ADDR_TAMP         0xF5050000  /* Intrusion detection module */
#define CHIP_MEMRGN_ADDR_MONO         0xF5054000  /* Monotonic counter */

#endif // CHIP_MMAP_H
