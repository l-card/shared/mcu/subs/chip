#ifndef CHIP_QINGKE_ISR_H
#define CHIP_QINGKE_ISR_H

#define CHIP_CSR_GINTENR            (0x800) /* (URW) Global interrupt enable register */
#define CHIP_CSR_INTSYSCR           (0x804) /* (URW) Interrupt system control register */
#define CHIP_CSR_CORECFGR           (0xBC0) /* (MRW) Microprocessor configuration register */
#define CHIP_CSR_CSTRCR             (0xBC2) /* (MRW) Cache policy configuration register */
#define CHIP_CSR_CPMPOCR            (0xBC3) /* (MRW) Cache policy overrides PMP control register */
#define CHIP_CSR_CMCR               (0xBD0) /* (MWO) Cache operation control register */
#define CHIP_CSR_CINFOR             (0xFC0) /* (MRO) Cache information register */


/*********** MTVEC extension *************************************************/
/* Старший бит mode (резерв по спецификации) выбирает, что содержится в таблице векторов, выполняемая инструкция или адрес обработчика */
#define CHIP_REGFLDVAL_CSR_MTVEC_MODE1_JUMP              0 /* Identification by jump instruction, limited range, support for non-jump */
#define CHIP_REGFLDVAL_CSR_MTVEC_MODE1_ABS_ADDR          2 /* Identify by absolute address, support full range, but must jump. */


/*********** Global interrupt enable register - GINTENR (0x800) ***************/
#define CHIP_REGFLD_CSR_GINTENR_MPIE                    (0x00000001UL <<  7U) /* (RW) MIE bit before trap */
#define CHIP_REGFLD_CSR_GINTENR_MIE                     (0x00000001UL <<  3U) /* (RW) M‑mode interrupt enable bit. */


/*********** Interrupt system control register - INTSYSCR (0x804) *************/
#if CHIP_CORE_MODEL_SUPPORT_INT_PREEMPT
#define CHIP_REGFLD_CSR_INTSYSCR_PMT_STA                (0x000000FFUL <<  8U) /* (RO) Preemption status indication (V4F only, bitmask for preemtion priority) */
#endif
#define CHIP_REGFLD_CSR_INTSYSCR_GIH_WSTK_EN            (0x00000001UL <<  5U) /* (RW1) Global interrupt and HPE off enable. Autoclear on interrupt return. Used for context switch */
#define CHIP_REGFLD_CSR_INTSYSCR_HWSTK_OV_EN            (0x00000001UL <<  4U) /* (RW) Interrupt enable after HPE overflow (if enabled then HPE must be disabled for higher level nesting) */
#if CHIP_CORE_MODEL_SUPPORT_INT_PREEMPT
#define CHIP_REGFLD_CSR_INTSYSCR_PMT_CFG                (0x00000003UL <<  2U) /* (RW) Interrupt nesting depth configuration (V4F only) */
#endif
#define CHIP_REGFLD_CSR_INTSYSCR_INEST_EN               (0x00000001UL <<  1U) /* (RW) Interrupt nesting enable */
#define CHIP_REGFLD_CSR_INTSYSCR_HWSTK_EN               (0x00000001UL <<  0U) /* (RW) HPE enable */

#define CHIP_REGFLDVAL_CSR_INTSYSCR_PMT_CFG_DIS         0 /* No nesting, the number of preemption bits is 0 */
#define CHIP_REGFLDVAL_CSR_INTSYSCR_PMT_CFG_2           1 /* 2 levels of nesting, with 1 preemption bit  */
#define CHIP_REGFLDVAL_CSR_INTSYSCR_PMT_CFG_4           2 /* 4 levels of nesting, with 2 preemption bits */
#define CHIP_REGFLDVAL_CSR_INTSYSCR_PMT_CFG_8           3 /* 8 levels of nesting, with 3 preemption bits */


/*********** Cache Policy Configuration Register - CSTRCR (0xBC2) *************/
#define CHIP_REGFLD_CSR_CSTRCR_IC_SRAM_STR_EN           (0x00000001UL << 25U) /* (RW) Enable caching of instructions in the SRAM area */
#define CHIP_REGFLD_CSR_CSTRCR_IC_CODE_STR_EN           (0x00000001UL << 24U) /* (RW) Enable caching of instructions in the CODE area */
#define CHIP_REGFLD_CSR_CSTRCR_IC_DISABLE               (0x00000001UL <<  1U) /* (RW) Command cache disabled */


/*********** Cache policy overrides PMP control register - CPMPOCR (0xBC3) ****/
#define CHIP_REGFLD_CSR_CPMPOCR_IC_PMP_CACH(n)          (0x00000001UL << (4U * (n))) /* (RW) Enable PMPn cache policy override (force to value from cstrcr) */

/*********** Cache Operation Control Register - CMCR (0xBD0) ******************/
#define CHIP_REGFLD_CSR_CMCR_VADDR                      (0x07FFFFFFUL <<  5U) /* (WO) Address or index information for the operation */
#define CHIP_REGFLD_CSR_CMCR_IDX_MODE                   (0x00000001UL <<  2U) /* (WO) Indexing mode (0 - Vaddr index, 1 - address) */
#define CHIP_REGFLD_CSR_CMCR_OPCODEE                    (0x00000003UL <<  0U) /* (WO) Operation code */

#define CHIP_REGFLDVAL_CSR_CMCR_OPCODE_DIS_IC           0 /* Disable instruction cache */

/*********** Cache Information Register - CINFOR (0xFC0) **********************/
#define CHIP_REGFLD_CSR_CINFOR_IC_WAY                   (0x00000003UL <<  5U) /* (RO) Instruction cache ways */
#define CHIP_REGFLD_CSR_CINFOR_IC_SIZE                  (0x00000007UL <<  2U) /* (RO) Instruction cache capacity information */
#define CHIP_REGFLD_CSR_CINFOR_IC_LINE_SIZE             (0x00000003UL <<  0U) /* (RO) Instruction cache line length */


#define CHIP_REGFLDVAL_CSR_CINFOR_IC_WAY_1              0
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_WAY_2              1
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_WAY_4              2
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_WAY_8              3

#define CHIP_REGFLDVAL_CSR_CINFOR_IC_SIZE_NO            0 /* No cache */
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_SIZE_4K            1
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_SIZE_8K            2
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_SIZE_16K           3
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_SIZE_32K           4
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_SIZE_64K           5

#define CHIP_REGFLDVAL_CSR_CINFOR_IC_LINE_SIZE_8        0 /* 8 bytes */
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_LINE_SIZE_16       1 /* 16 bytes */
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_LINE_SIZE_32       2 /* 32 bytes */
#define CHIP_REGFLDVAL_CSR_CINFOR_IC_LINE_SIZE_64       3 /* 64 bytes */


#endif // CHIP_QINGKE_ISR_H


