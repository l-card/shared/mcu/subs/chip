#ifndef CHIP_RISCV_MEMBARRIER_H
#define CHIP_RISCV_MEMBARRIER_H

/** @todo */
#define chip_isb() chip_fencei()   /* instruction sync barrier */
#define chip_dsb()    /* data sync barrier, e.g. after disable IRQ */
#define chip_dmb()    /* data memory barrier, e.g. before DMA */


#endif // CHIP_RISCV_MEMBARRIER_H
