#ifndef CHIP_ARCHTYPE_H
#define CHIP_ARCHTYPE_H

#include "chip_arm.h"
#include "chip_arm_feat_defs.h"
#include "chip_aarch64_sysregs.h"
#include "chip_aarch64_exceptions.h"
#include "chip_aarch64_mmu_regs.h"

#endif // CHIP_ARCHTYPE_H
