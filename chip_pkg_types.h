#ifndef CHIP_PKG_TYPES_H
#define CHIP_PKG_TYPES_H


#define CHIP_PKG_TYPE_SO            1
#define CHIP_PKG_TYPE_TSSOP         2
#define CHIP_PKG_TYPE_QSOP          3

#define CHIP_PKG_TYPE_QFN           10
#define CHIP_PKG_TYPE_UFQFPN        11

#define CHIP_PKG_TYPE_LQFP          20
#define CHIP_PKG_TYPE_TQFP          21

#define CHIP_PKG_TYPE_WLCSP         30
#define CHIP_PKG_TYPE_BGA           31
#define CHIP_PKG_TYPE_UFBGA         32
#define CHIP_PKG_TYPE_TFBGA         33
#define CHIP_PKG_TYPE_VFBGA         34




#define CHIP_PKG_ID(type, pincnt)  (((type) << 16) | ((pincnt) << 0))
#define CHIP_PKG_ID_EX(type, pincnt, subtype)  (((type) << 16) | ((subtype) << 12) | ((pincnt) << 0))

#define CHIP_PKG_ID_TYPE(pkgid)    (((pkgid) >> 16) & 0xFF)
#define CHIP_PKG_ID_SUBTYPE(pkgid) (((pkgid) >> 12) & 0xF)
#define CHIP_PKG_ID_PINCNT(pkgid)  ((pkgid) & 0x0FFF)



#define CHIP_PKG_SO8N              CHIP_PKG_ID(CHIP_PKG_TYPE_SO,        8)

#define CHIP_PKG_TSSOP20           CHIP_PKG_ID(CHIP_PKG_TYPE_TSSOP,     20)

#define CHIP_PKG_QSOP28            CHIP_PKG_ID(CHIP_PKG_TYPE_QSOP,      28)


#define CHIP_PKG_QFN20             CHIP_PKG_ID(CHIP_PKG_TYPE_QFN,       20)
#define CHIP_PKG_QFN28             CHIP_PKG_ID(CHIP_PKG_TYPE_QFN,       28)
#define CHIP_PKG_QFN32             CHIP_PKG_ID(CHIP_PKG_TYPE_QFN,       32)
#define CHIP_PKG_QFN48             CHIP_PKG_ID(CHIP_PKG_TYPE_QFN,       48)

#define CHIP_PKG_QFN32_4x4         CHIP_PKG_ID_EX(CHIP_PKG_TYPE_QFN,    32, 1)
#define CHIP_PKG_QFN32_5x5         CHIP_PKG_ID_EX(CHIP_PKG_TYPE_QFN,    32, 2)

#define CHIP_PKG_UFQFPN20          CHIP_PKG_ID(CHIP_PKG_TYPE_UFQFPN,    20)
#define CHIP_PKG_UFQFPN32          CHIP_PKG_ID(CHIP_PKG_TYPE_UFQFPN,    32)

#define CHIP_PKG_LQFP32            CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  32)
#define CHIP_PKG_LQFP48            CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  48)
#define CHIP_PKG_LQFP64            CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  64)
#define CHIP_PKG_LQFP80            CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  80)
#define CHIP_PKG_LQFP100           CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  100)
#define CHIP_PKG_LQFP128           CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  128)
#define CHIP_PKG_LQFP144           CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  144)
#define CHIP_PKG_LQFP176           CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  176)
#define CHIP_PKG_LQFP208           CHIP_PKG_ID(CHIP_PKG_TYPE_LQFP,  208)

#define CHIP_PKG_TQFP32            CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  32)
#define CHIP_PKG_TQFP48            CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  48)
#define CHIP_PKG_TQFP64            CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  64)
#define CHIP_PKG_TQFP80            CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  80)
#define CHIP_PKG_TQFP100           CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  100)
#define CHIP_PKG_TQFP128           CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  128)
#define CHIP_PKG_TQFP144           CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  144)
#define CHIP_PKG_TQFP176           CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  176)
#define CHIP_PKG_TQFP208           CHIP_PKG_ID(CHIP_PKG_TYPE_TQFP,  208)


#define CHIP_PKG_WLCSP18           CHIP_PKG_ID(CHIP_PKG_TYPE_WLCSP, 18)
#define CHIP_PKG_WLCSP25           CHIP_PKG_ID(CHIP_PKG_TYPE_WLCSP, 25)
#define CHIP_PKG_WLCSP49           CHIP_PKG_ID(CHIP_PKG_TYPE_WLCSP, 49)


#define CHIP_PKG_BGA196            CHIP_PKG_ID(CHIP_PKG_TYPE_BGA,   196)
#define CHIP_PKG_BGA289            CHIP_PKG_ID(CHIP_PKG_TYPE_BGA,   289)

#define CHIP_PKG_TFBGA100          CHIP_PKG_ID(CHIP_PKG_TYPE_TFBGA, 100)
#define CHIP_PKG_TFBGA208          CHIP_PKG_ID(CHIP_PKG_TYPE_TFBGA, 208)
#define CHIP_PKG_TFBGA240          CHIP_PKG_ID(CHIP_PKG_TYPE_TFBGA, 240)

#define CHIP_PKG_UFBGA64           CHIP_PKG_ID(CHIP_PKG_TYPE_UFBGA,  64)
#define CHIP_PKG_UFBGA100          CHIP_PKG_ID(CHIP_PKG_TYPE_UFBGA, 100)
#define CHIP_PKG_UFBGA169          CHIP_PKG_ID(CHIP_PKG_TYPE_UFBGA, 169)
#define CHIP_PKG_UFBGA176          CHIP_PKG_ID(CHIP_PKG_TYPE_UFBGA, 176)

#define CHIP_PKG_VFBGA176          CHIP_PKG_ID(CHIP_PKG_TYPE_VFBGA, 176)


#endif // CHIP_PKG_TYPES_H
