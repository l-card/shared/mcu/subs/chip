#ifndef CHIP_REGS_LPC11U6X_WWDT_H
#define CHIP_REGS_LPC11U6X_WWDT_H

#include <stdint.h>

typedef struct {                /*!< WWDT Structure         */
    __IO uint32_t  MOD;         /*!< Watchdog mode register. This register contains the basic mode and status of the Watchdog Timer. */
    __IO uint32_t  TC;          /*!< Watchdog timer constant register. This register determines the time-out value. */
    __O  uint32_t  FEED;        /*!< Watchdog feed sequence register. Writing 0xAA followed by 0x55 to this register reloads the Watchdog timer with the value contained in WDTC. */
    __I  uint32_t  TV;          /*!< Watchdog timer value register. This register reads out the current value of the Watchdog timer. */
    __IO uint32_t CLKSEL;       /*!< Watchdog clock select register. */
    __IO uint32_t  WARNINT;     /*!< Watchdog warning interrupt register. This register contains the Watchdog warning interrupt compare value. */
    __IO uint32_t  WINDOW;      /*!< Watchdog timer window register. This register contains the Watchdog window value. */
} CHIP_REGS_WWDT_T;

#define CHIP_REGS_WWDT              ((CHIP_REGS_WWDT_T *) CHIP_MEMRGN_ADDR_PERIPH_WWDT)


#define CHIP_WDT_TC_MAX_MIN                         0x000000FFUL
#define CHIP_WDT_TC_MAX_VAL                         0x00FFFFFFUL
#define CHIP_WDT_WINDOW_MAX_VAL                     0x00FFFFFFUL
#define CHIP_WDT_WARNTIME_MAX_VAL                   0x000003FFUL


/*********** Watchdog mode register(WWDT_MOD) ********************************/
#define CHIP_REGFLD_WWDT_MOD_WD_EN                  (0x00000001UL <<  0U)   /* (RW) Watchdog enable bit  */
#define CHIP_REGFLD_WWDT_MOD_WD_RESET               (0x00000001UL <<  1U)   /* (RW) Watchdog reset enable bit  */
#define CHIP_REGFLD_WWDT_MOD_WD_TOF                 (0x00000001UL <<  2U)   /* (RW) Watchdog time-out flag  */
#define CHIP_REGFLD_WWDT_MOD_WD_INT                 (0x00000001UL <<  3U)   /* (RW) Watchdog interrupt flag  */
#define CHIP_REGFLD_WWDT_MOD_WD_PROTECT             (0x00000001UL <<  4U)   /* (RW) Watchdog time-out value change protect */
#define CHIP_REGFLD_WWDT_MOD_LOCK                   (0x00000001UL <<  5U)   /* (RW) Disable switch to disabled clock source */
#define CHIP_REGMSK_WWDT_MOD_RESERBED               (0xFFFFFFC0UL)

/*********** Watchdog Timer Constant register (WWDT_TC) ***********************/
#define CHIP_REGFLD_WWDT_TC_COUNT                   (0x00FFFFFFUL <<  0U)   /* (RW) Watchdog time-out value  */
#define CHIP_REGMSK_WWDT_TC_RESERBED                (0xFF000000UL)


/*********** Watchdog Clock Select register (WWDT_CLKSEL) ********************************/
#define CHIP_REGFLD_WWDT_CLKSEL_VAL                 (0x00000001UL <<  0U)   /* (RW) Select source of WDT clock  */
#define CHIP_REGFLD_WWDT_CLKSEL_LOCK                (0x00000001UL << 31U)   /* (RW) Lock register changes  */
#define CHIP_REGMSK_WWDT_CLKSEL_RESERBED            (0x7FFFFFFEUL)

#define CHIP_REGFLDVAL_WWDT_CLKSEL_VAL_IRC          0
#define CHIP_REGFLDVAL_WWDT_CLKSEL_VAL_WDTOSC       1

#endif /* CHIP_REGS_LPC11U6X_WWDT_H */
