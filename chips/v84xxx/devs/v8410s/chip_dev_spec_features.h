#ifndef CHIP_DEV_SPEC_FEATURES_H
#define CHIP_DEV_SPEC_FEATURES_H

#define CHIP_DEV_PKG                             CHIP_PKG_QFN48
#define CHIP_DEV_SUPPORT_FLASH_BANK3             0
#define CHIP_DEV_SUPPORT_SPIM                    1


#endif // CHIP_DEV_SPEC_FEATURES_H
