#ifndef CHIP_ARMV7EM_REGS_CORE_SCB_H
#define CHIP_ARMV7EM_REGS_CORE_SCB_H

#include "stdint.h"

typedef struct {
    /* ICTR и ACTLR  вне области  CHIP_MEMRGN_ADDR_ARMV7EM_SCB => отсчитываем от начала SCS */
    uint32_t RESERVED0_0[1];
    __IM  uint32_t ICTR;                   /* Offset: 0x004 (R/ )  Interrupt Controller Type Register (нет в CM7 programming manual!) */
    __IOM uint32_t ACTLR;                  /* Offset: 0x008 (R/W)  Auxiliary Control Register */
    uint32_t RESERVED0_1[(CHIP_MEMRGN_ADDR_ARMV7EM_SCB - CHIP_MEMRGN_ADDR_ARMV7EM_SCS)/4 - 3];
    /* Область CHIP_MEMRGN_ADDR_ARMV7EM_SCB */
    __IM  uint32_t CPUID;                  /* Offset: 0x000 (R/ )  CPUID Base Register */
    __IOM uint32_t ICSR;                   /* Offset: 0x004 (R/W)  Interrupt Control and State Register */
    __IOM uint32_t VTOR;                   /* Offset: 0x008 (R/W)  Vector Table Offset Register */
    __IOM uint32_t AIRCR;                  /* Offset: 0x00C (R/W)  Application Interrupt and Reset Control Register */
    __IOM uint32_t SCR;                    /* Offset: 0x010 (R/W)  System Control Register */
    __IOM uint32_t CCR;                    /* Offset: 0x014 (R/W)  Configuration Control Register */
    __IOM uint8_t  SHPR[12U];              /* Offset: 0x018 (R/W)  System Handlers Priority Registers (4-7, 8-11, 12-15) */
    __IOM uint32_t SHCSR;                  /* Offset: 0x024 (R/W)  System Handler Control and State Register */
    union {
        __IOM uint32_t CFSR;               /* Offset: 0x028 (R/W)  Configurable Fault Status Register */
        struct {
            __IOM uint8_t MMFSR;           /* Offset: 0x028 (R/W)  MemManage fault address register */
            __IOM uint8_t BFSR;            /* Offset: 0x029 (R/W)  BusFault status registe */
            __IOM uint16_t UFSR;           /* Offset: 0x02A (R/W)  Auxiliary control register */
        };
    };
    __IOM uint32_t HFSR;                   /* Offset: 0x02C (R/W)  HardFault Status Register */
    __IOM uint32_t DFSR;                   /* Offset: 0x030 (R/W)  Debug Fault Status Register  (нет в CM7 programming manual!) */
    __IOM uint32_t MMFAR;                  /* Offset: 0x034 (R/W)  MemManage Fault Address Register */
    __IOM uint32_t BFAR;                   /* Offset: 0x038 (R/W)  BusFault Address Register */
    __IOM uint32_t AFSR;                   /* Offset: 0x03C (R/W)  Auxiliary Fault Status Register */
} CHIP_REGS_ARMV7EM_SCB_T;

#define CHIP_REGS_SCB                           ((CHIP_REGS_ARMV7EM_SCB_T *) CHIP_MEMRGN_ADDR_ARMV7EM_SCS)


/*********** Interrupt Controller Type register (SCB_ICTR) ********************/
#define CHIP_REGFLD_SCB_ICTR_INTLINESNUM        (0x0000000FUL <<  0U) /* Reserved? */

/*********** Auxiliary control register (SCB_ACTLR) ***************************/
#define CHIP_REGFLD_SCB_ACTLR_DISMCYCINT        (0x00000001UL <<  0U) /* Disables interrupt of multi-cycle instructions */
#define CHIP_REGFLD_SCB_ACTLR_DISDEFWBUF        (0x00000001UL <<  1U) /* Disables write buffer use during default memory map accesses (CM4 only) */
#define CHIP_REGFLD_SCB_ACTLR_DISFOLD           (0x00000001UL <<  2U) /* Disables dual-issue functionality */
#define CHIP_REGFLD_SCB_ACTLR_DISFPCA           (0x00000001UL <<  8U) /* Disables automatic update of CONTROL.FPCA (CM4 only?) */
#define CHIP_REGFLD_SCB_ACTLR_DISOOFP           (0x00000001UL <<  9U) /* Disables floating point instructions completing out of order with respect to integer instructions (CM4 only?) */
#define CHIP_REGFLD_SCB_ACTLR_FPEXCODIS         (0x00000001UL << 10U) /* Disables FPU exception outputs */
#define CHIP_REGFLD_SCB_ACTLR_DISRAMODE         (0x00000001UL << 11U) /* Disables dynamic read allocate mode for Write-Back Write-Allocate memory regions */
#define CHIP_REGFLD_SCB_ACTLR_DISITMATBFLUSH    (0x00000001UL << 12U) /* Disables ITM and DWT ATB flush */
#define CHIP_REGFLD_SCB_ACTLR_DISBTACREAD       (0x00000001UL << 13U) /* Reserved? */
#define CHIP_REGFLD_SCB_ACTLR_DISBTACALLOC      (0x00000001UL << 14U) /* Reserved? */
#define CHIP_REGFLD_SCB_ACTLR_DISCRITAXIRUR     (0x00000001UL << 15U) /* Reserved? */
#define CHIP_REGFLD_SCB_ACTLR_DISDI             (0x0000001FUL << 16U) /* Reserved? */
#define CHIP_REGFLD_SCB_ACTLR_DISISSCH1         (0x0000001FUL << 21U) /* Reserved? */
#define CHIP_REGFLD_SCB_ACTLR_DISDYNADD         (0x00000001UL << 26U) /* Reserved? */

/*********** CPUID base register (SCB_CPUID) **********************************/
#define CHIP_REGFLD_SCB_CPUID_REVISION          (0x0000000FUL <<  0U) /* (RO) Revision number, the p value in the rnpn product revision identifier */
#define CHIP_REGFLD_SCB_CPUID_PARTNO            (0x00000FFFUL <<  4U) /* (RO) Part number of the processor */
#define CHIP_REGFLD_SCB_CPUID_ARCHITECTURE      (0x0000000FUL << 16U) /* (RO) Constant. Reads as 0xF */
#define CHIP_REGFLD_SCB_CPUID_VARIANT_Msk       (0x0000000FUL << 20U) /* (RO) Variant number, the r value in the rnpn product revision identifier */
#define CHIP_REGFLD_SCB_CPUID_IMPLEMENTER       (0x000000FFUL << 24U) /* (RO) Implementer code */

#define CHIP_REGFLDVAL_SCB_CPUID_PARTNO_CM0P    0xC60 /* Cortex-M0+ */
#define CHIP_REGFLDVAL_SCB_CPUID_PARTNO_CM4     0xC24 /* Cortex-M4 */
#define CHIP_REGFLDVAL_SCB_CPUID_PARTNO_CM7     0xC27 /* Cortex-M7 */

#define CHIP_REGFLDVAL_SCB_CPUID_ARCH_AMRV6M    0xC

#define CHIP_REGFLDVAL_SCB_CPUID_IMPLEMENTER_ARM    0x41


/*********** Interrupt control and state register (SCB_ICSR) ******************/
#define CHIP_REGFLD_SCB_ICSR_VECTACTIVE_Msk     (0x000001FFUL <<  0U) /* (RO) Contains the active exception number */
#define CHIP_REGFLD_SCB_ICSR_RETTOBASE_Msk      (0x00000001UL << 11U) /* (RO) Indicates whether there are preempted active exceptions */
#define CHIP_REGFLD_SCB_ICSR_VECTPENDING_Msk    (0x000001FFUL << 12U) /* (RO) Indicates the exception number of the highest priority pending enabled */
#define CHIP_REGFLD_SCB_ICSR_ISRPENDING         (0x00000001UL << 22U) /* (RO) Interrupt pending flag, excluding NMI and Faults */
#define CHIP_REGFLD_SCB_ICSR_ISRPREEMPT         (0x00000001UL << 23U) /* (RO) This bit is reserved for Debug use */
#define CHIP_REGFLD_SCB_ICSR_PENDSTCLR          (0x00000001UL << 25U) /* (WO) SysTick exception clear-pending bit */
#define CHIP_REGFLD_SCB_ICSR_PENDSTSET          (0x00000001UL << 26U) /* (RW) SysTick exception set-pending bit */
#define CHIP_REGFLD_SCB_ICSR_PENDSVCLR          (0x00000001UL << 27U) /* (WO) PendSV clear-pending bit */
#define CHIP_REGFLD_SCB_ICSR_PENDSVSET          (0x00000001UL << 28U) /* (RW) PendSV set-pending bit */
#define CHIP_REGFLD_SCB_ICSR_NMIPENDSET         (0x00000001UL << 31U) /* (RW) NMI set-pending bit */

/*********** Application interrupt and reset control register (SCB_AIRCR) *****/
#define CHIP_REGFLD_SCB_AIRCR_VECTRESET         (0x00000001UL <<  0U) /* (WO) Reserved for Debug use */
#define CHIP_REGFLD_SCB_AIRCR_VECTCLRACTIVE     (0x00000001UL <<  1U) /* (WO) Reserved for Debug use */
#define CHIP_REGFLD_SCB_AIRCR_SYSRESETREQ       (0x00000001UL <<  2U) /* (WO) System reset request */
#define CHIP_REGFLD_SCB_AIRCR_PRIGROUP          (0x00000007UL <<  8U) /* (RW) Interrupt priority grouping field */
#define CHIP_REGFLD_SCB_AIRCR_ENDIANESS         (0x00000001UL << 15U) /* (RO) Data endianness bit: 0 - Little-endian */
#define CHIP_REGFLD_SCB_AIRCR_VECTKEY           (0x0000FFFFUL << 16U) /* (WO) On writes, write 0x5FA to VECTKEY, otherwise the write is ignored */
#define CHIP_REGFLD_SCB_AIRCR_VECTKEYSTAT       (0x0000FFFFUL << 16U) /* (RO) Reads as 0xFA05 */
#define CHIP_REGMSK_SCB_AIRCR_RESERVED          (0x000078F8UL)

#define CHIP_REGFLDVAL_SCB_AIRCR_VECTKEY        0x05FA
#define CHIP_REGFLDVAL_SCB_AIRCR_VECTKEYSTAT    0xFA05

/*********** System control register (SCB_SCR) ********************************/
#define CHIP_REGFLD_SCB_SCR_SLEEPONEXIT         (0x00000001UL <<  1U) /* Indicates sleep-on-exit when returning from Handler mode to Thread mode */
#define CHIP_REGFLD_SCB_SCR_SLEEPDEEP           (0x00000001UL <<  2U) /* Controls whether the processor uses sleep or deep sleep as its Low-power mode */
#define CHIP_REGFLD_SCB_SCR_SEVONPEND           (0x00000001UL <<  4U) /* Send Event on Pending */
#define CHIP_REGMSK_SCB_SCR_RESERVED            (0xFFFFFFE9UL)


/*********** Configuration and control register (SCB_CCR) *********************/
#define CHIP_REGFLD_SCB_CCR_NONBASETHRDENA      (0x00000001UL <<  0U) /* (RW) Indicates how the processor enters Thread mode */
#define CHIP_REGFLD_SCB_CCR_USERSETMPEND        (0x00000001UL <<  1U) /* (RW) Enables unprivileged software access to the STIR */
#define CHIP_REGFLD_SCB_CCR_UNALIGN_TRP         (0x00000001UL <<  3U) /* (RW) Enables unaligned access traps */
#define CHIP_REGFLD_SCB_CCR_DIV_0_TRP           (0x00000001UL <<  4U) /* (RW) Enables faulting or halting when the processor executes an SDIV or UDIV instruction with a divisor of 0 */
#define CHIP_REGFLD_SCB_CCR_BFHFNMIGN           (0x00000001UL <<  8U) /* (RW) Enables handlers with priority -1 or -2 to ignore data BusFaults caused by load and store instructions */
#define CHIP_REGFLD_SCB_CCR_STKALIGN            (0x00000001UL <<  9U) /* (RO) Always reads-as-one. It indicates stack alignment on exception entry is 8-byte aligned. */
#define CHIP_REGFLD_SCB_CCR_DC                  (0x00000001UL << 16U) /* (RW) Enables L1data cache */
#define CHIP_REGFLD_SCB_CCR_IC                  (0x00000001UL << 17U) /* (RW) Enables L1 instruction cache */
#define CHIP_REGFLD_SCB_CCR_BP                  (0x00000001UL << 18U) /* (RO) Always reads-as-one. It indicates branch prediction is enabled. */
#define CHIP_REGMSK_SCB_CCR_RESERVED            (0xFFF8FCE4UL)

/*********** System handler control and state register (SCB_SHCSR) ************/
#define CHIP_REGFLD_SCB_SHCSR_MEMFAULTACT       (0x00000001UL <<  0U) /* (RW) MemManage exception active bit, reads as 1 if exception is active */
#define CHIP_REGFLD_SCB_SHCSR_BUSFAULTACT       (0x00000001UL <<  1U) /* (RW) BusFault exception active bit, reads as 1 if exception is active */
#define CHIP_REGFLD_SCB_SHCSR_USGFAULTACT       (0x00000001UL <<  3U) /* (RW) UsageFault exception active bit, reads as 1 if exception is active */
#define CHIP_REGFLD_SCB_SHCSR_SVCALLACT         (0x00000001UL <<  7U) /* (RW) SVCall active bit, reads as 1 if SVC call is active */
#define CHIP_REGFLD_SCB_SHCSR_MONITORACT        (0x00000001UL <<  8U) /* (RW) Debug monitor active bit, reads as 1 if Debug monitor is active */
#define CHIP_REGFLD_SCB_SHCSR_PENDSVACT         (0x00000001UL << 10U) /* (RW) PendSV exception active bit, reads as 1 if exception is active */
#define CHIP_REGFLD_SCB_SHCSR_SYSTICKACT        (0x00000001UL << 11U) /* (RW) SysTick exception active bit, reads as 1 if exception is active */
#define CHIP_REGFLD_SCB_SHCSR_USGFAULTPENDED    (0x00000001UL << 12U) /* (RW) UsageFault exception pending bit, reads as 1 if exception is pending */
#define CHIP_REGFLD_SCB_SHCSR_MEMFAULTPENDED    (0x00000001UL << 13U) /* (RW) MemManage exception pending bit, reads as 1 if exception is pending */
#define CHIP_REGFLD_SCB_SHCSR_BUSFAULTPENDED    (0x00000001UL << 14U) /* (RW) BusFault exception pending bit, reads as 1 if exception is pending */
#define CHIP_REGFLD_SCB_SHCSR_SVCALLPENDED      (0x00000001UL << 15U) /* (RW) SVCall pending bit, reads as 1 if exception is pending */
#define CHIP_REGFLD_SCB_SHCSR_MEMFAULTENA       (0x00000001UL << 16U) /* (RW) MemManage enable bit, set to 1 to enable */
#define CHIP_REGFLD_SCB_SHCSR_BUSFAULTENA       (0x00000001UL << 17U) /* (RW) BusFault enable bit, set to 1 to enable */
#define CHIP_REGFLD_SCB_SHCSR_USGFAULTENA       (0x00000001UL << 18U) /* (RW) UsageFault enable bit, set to 1 to enable */
#define CHIP_REGMSK_SCB_SHCSR_RESERVED          (0xFFF80274UL)

/*********** Configurable fault status register (SCB_CFSR) ********************/
#define CHIP_REGFLD_SCB_CFSR_MEMFAULTSR         (0x000000FFUL <<  0U) /* MemManage fault status register */
#define CHIP_REGFLD_SCB_CFSR_BUSFAULTSR         (0x000000FFUL <<  8U) /* BusFault status register */
#define CHIP_REGFLD_SCB_CFSR_USGFAULTSR         (0x0000FFFFUL << 16U) /* UsageFault status register */

/*********** MemManage fault status register (SCB_MMFSR) **********************/
#define CHIP_REGFLD_SCB_MMFSR_IACCVIOL          (0x00000001UL <<  0U) /* (RW1C) Instruction access violation flag */
#define CHIP_REGFLD_SCB_MMFSR_DACCVIOL          (0x00000001UL <<  1U) /* (RW1C) Data access violation flag */
#define CHIP_REGFLD_SCB_MMFSR_MUNSTKERR         (0x00000001UL <<  3U) /* (RW1C) MemManage fault on unstacking for a return from exception */
#define CHIP_REGFLD_SCB_MMFSR_MSTKERR           (0x00000001UL <<  4U) /* (RW1C) MemManage fault on stacking for exception entry */
#define CHIP_REGFLD_SCB_MMFSR_MLSPERR           (0x00000001UL <<  5U) /* (RW1C) MemManage fault occurred during floating-point lazy state preservation */
#define CHIP_REGFLD_SCB_MMFSR_MMARVALID         (0x00000001UL <<  7U) /* (RW1C) MemManage Fault Address register (MMFAR) valid flag */
#define CHIP_REGMSK_SCB_MMFSR_RESERVED          (0x44UL)

/*********** BusFault status register (SCB_BFSR) ******************************/
#define CHIP_REGFLD_SCB_BFSR_IBUSERR            (0x00000001UL <<  0U) /* (RW1C) Instruction bus error */
#define CHIP_REGFLD_SCB_BFSR_PRECISERR          (0x00000001UL <<  1U) /* (RW1C) Precise data bus error */
#define CHIP_REGFLD_SCB_BFSR_IMPRECISERR        (0x00000001UL <<  2U) /* (RW1C) Imprecise data bus error */
#define CHIP_REGFLD_SCB_BFSR_UNSTKERR           (0x00000001UL <<  3U) /* (RW1C) BusFault on unstacking for a return from exception */
#define CHIP_REGFLD_SCB_BFSR_STKERR             (0x00000001UL <<  4U) /* (RW1C) BusFault on stacking for exception entry */
#define CHIP_REGFLD_SCB_BFSR_LSPERR             (0x00000001UL <<  5U) /* (RW1C) bus fault occurred during floating-point lazy state preservation */
#define CHIP_REGFLD_SCB_BFSR_BFARVALID          (0x00000001UL <<  7U) /* (RW1C) BusFault Address register (BFAR) valid flag */
#define CHIP_REGMSK_SCB_BFSR_RESERVED           (0x40UL)

/*********** UsageFault status register (SCB_UFSR) ****************************/
#define CHIP_REGFLD_SCB_UFSR_UNDEFINSTR         (0x00000001UL <<  0U) /* (RW1C) Undefined instruction UsageFault */
#define CHIP_REGFLD_SCB_UFSR_INVSTATE           (0x00000001UL <<  1U) /* (RW1C) Invalid state UsageFault */
#define CHIP_REGFLD_SCB_UFSR_INVPC              (0x00000001UL <<  2U) /* (RW1C) Invalid PC load UsageFault, caused by an invalid PC load by EXC_RETURN */
#define CHIP_REGFLD_SCB_UFSR_NOCP               (0x00000001UL <<  3U) /* (RW1C) No coprocessor UsageFault */
#define CHIP_REGFLD_SCB_UFSR_UNALIGNED          (0x00000001UL <<  8U) /* (RW1C) Unaligned access UsageFault */
#define CHIP_REGFLD_SCB_UFSR_DIVBYZERO          (0x00000001UL <<  9U) /* (RW1C) Divide by zero UsageFault */
#define CHIP_REGMSK_SCB_UFSR_RESERVED           (0xFCF0UL)


/*********** HardFault status register (SCB_HFSR) ****************************/
#define CHIP_REGFLD_SCB_HFSR_VECTTBL            (0x00000001UL <<  1U) /* (RW1C) Indicates a BusFault on a vector table read during exception processing */
#define CHIP_REGFLD_SCB_HFSR_FORCED             (0x00000001UL << 30U) /* (RW1C) Indicates a forced hard fault, generated by escalation of a fault with configurable priority that cannot be handled */
#define CHIP_REGFLD_SCB_HFSR_DEBUGEVT           (0x00000001UL << 30U) /* (RW1C) Reserved for Debug use. When writing to the register the user must write 1 to this bit! */
#define CHIP_REGMSK_SCB_HFSR_RESERVED           (0x3FFFFFFDUL)

/*********** Debug Fault Status register (SCB_DFSR) ***************************/
#define CHIP_REGFLD_SCB_DFSR_HALTED             (0x00000001UL <<  0U)
#define CHIP_REGFLD_SCB_DFSR_BKPT               (0x00000001UL <<  1U)
#define CHIP_REGFLD_SCB_DFSR_DWTTRAP            (0x00000001UL <<  2U)
#define CHIP_REGFLD_SCB_DFSR_VCATCH             (0x00000001UL <<  3U)
#define CHIP_REGFLD_SCB_DFSR_EXTERNAL           (0x00000001UL <<  4U)

#endif // CHIP_ARMV7EM_REGS_CORE_SCB_H
