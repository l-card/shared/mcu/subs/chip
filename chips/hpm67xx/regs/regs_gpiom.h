#ifndef REGS_GPIOM_H
#define REGS_GPIOM_H

#include "chip_ioreg_defs.h"

typedef struct {
    struct {
        __IO uint32_t PIN[32];                 /* 0x0 - 0x7C: GPIO mananger */
    } ASSIGN[16];
} CHIP_REGS_GPIOM_T;

#define CHIP_REGS_GPIOM             ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_GPIOM)

#define CHIP_GPIOCTL_NUM_GPIO0                          (0UL)
#define CHIP_GPIOCTL_NUM_GPIO1                          (1UL)
#define CHIP_GPIOCTL_NUM_FGPIO_CPU0                     (2UL)
#define CHIP_GPIOCTL_NUM_FGPIO_CPU1                     (3UL)

/*********** Indicator brightness - LED_INTENSE *******************************/
#define CHIP_REGFLD_GPIOM_ASSIGN_LOCK                   (0x00000001UL << 31U) /* (RW) lock register setting */
#define CHIP_REGFLD_GPIOM_ASSIGN_HIDE(x)                (0x00000001UL << (8U + (x))) /* (RW) GPIO controller x visibility */
#define CHIP_REGFLD_GPIOM_ASSIGN_SELECT                 (0x00000003UL << 0U) /* (RW) Select pin control source */
#define CHIP_REGMSK_GPIOM_ASSIGN_RESERVED               (0x7FFFF0FCUL)


#endif // REGS_GPIOM_H
