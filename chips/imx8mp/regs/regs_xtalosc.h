#ifndef REGS_OSC_H
#define REGS_OSC_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t CTL0; /**< OSC Normal Clock Generation Control Register 0 */
    __IO uint32_t CTL1; /**< OSC Normal Clock Generation Control Register 1 */
} CHIP_REGS_XTALOSC_T;

#define CHIP_REGS_XTALOSC       ((CHIP_REGS_XTALOSC_T *) CHIP_MEMRGN_ADDR_PERIPH_ANA_OSC)

/*********** OSC Normal Clock Generation Control Register 0 (XTALOSC_CTL0) ****/
#define CHIP_REGFLD_XTALOSC_CTL0_SF0                (0x00000001UL <<  0U)   /* (RW) Select Frequency0 */
#define CHIP_REGFLD_XTALOSC_CTL0_SF1                (0x00000001UL <<  1U)   /* (RW) Select Frequency1 */
#define CHIP_REGFLD_XTALOSC_CTL0_SP                 (0x00000001UL <<  2U)   /* (RW) Select Power */
#define CHIP_REGFLD_XTALOSC_CTL0_RTO                (0x00000001UL <<  4U)   /* (RW) Retention Enable */
#define CHIP_REGFLD_XTALOSC_CTL0_EN                 (0x00000001UL << 31U)   /* (RW) Enable Oscillator */

/*********** OSC Normal Clock Generation Control Register 1 (XTALOSC_CTL1) ****/
#define CHIP_REGFLD_XTALOSC_CTL1_CLK_CKE_OVRD       (0x00000001UL <<  1U)   /* (RW) Oscillator Clock Gating Enable Override */
#define CHIP_REGFLD_XTALOSC_CTL1_CLK_CKE            (0x00000001UL <<  2U)   /* (RW) Oscillator Clock Gating Enable */
#define CHIP_REGFLD_XTALOSC_CTL1_LOCK_COUNT         (0x000000FFUL <<  4U)   /* (RW) Lock Signal Gen Counter */


#endif // REGS_OSC_H
