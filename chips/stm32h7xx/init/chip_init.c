#include "chip.h"
#include "chip_init.h"

void chip_clk_init(void);
void chip_pwr_init(void);
void chip_gpv_init(void);


void chip_dummy_init_fault(int code) {
    (void)code;

    for (;;) {
        continue;
    }
}


void chip_init(void) {

#if !defined CHIP_CORETYPE_CM4
    chip_gpv_init();
#endif
#if !CHIP_SHARED_NO_INIT
    chip_pwr_init();
#endif
    chip_clk_init();
#if CHIP_CFG_EXMC_SETUP_EN && !CHIP_SHARED_NO_INIT
    chip_exmc_init();
#endif
#if !CHIP_SHARED_NO_INIT
    chip_hsem_init();
#endif
}




void chip_main_prestart() {
#if CHIP_MPU_SETUP_EN
    chip_mpu_init();
#endif
#if CHIP_CACHE_SETUP_EN
    chip_cache_init();
#endif
    chip_fpu_init();
#if CHIP_MDMA_FUNC_EN
    chip_mdma_init();
#endif
}
