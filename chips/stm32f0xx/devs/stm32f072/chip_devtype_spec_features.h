#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_DEV_STM32F072
#define CHIP_DEV_STM32F07X
#define CHIP_DEV_STM32F0X2


#define CHIP_DEV_SUPPORT_MCO_PRE        1 /* настройки делителя MCO (not available on STM32F05x devices) */
#define CHIP_DEV_SUPPORT_HSI48          1 /* поддержка HSI48 (available on STM32F04x, STM32F07x, STM32F09x) */
#define CHIP_DEV_SUPPORT_HSI_PLL_PREDIV 1 /* поддержка заданного делителя HSI для PLL (available on STM32F04x, STM32F07x, STM32F09x) */
#define CHIP_DEV_SUPPORT_USART2_CLKSEL  1 /* настройка частоты USART2 (available on STM32F07x and STM32F09x) */
#define CHIP_DEV_SUPPORT_USART3_CLKSEL  0 /* настройка частоты USART3 (available on STM32F09x) */
#define CHIP_DEV_SUPPORT_EXTERNAL_POR   0 /* POR внешним образом через NPOR пин (on STM32F0x8) */
#define CHIP_DEV_SUPPORT_CRS            1 /* Наличе Clock Recovery System (STM32F04x, STM32F07x and STM32F09x) */

#define CHIP_DEV_UART_CNT              4

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
