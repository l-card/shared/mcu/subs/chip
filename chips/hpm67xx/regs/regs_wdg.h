#ifndef REGS_WDG_H
#define REGS_WDG_H

#include "chip_ioreg_defs.h"

typedef struct {
    __I  uint32_t RESERVED0[4];                /* 0x0 - 0xF: Reserved */
    __IO uint32_t CTRL;                        /* 0x10: Control Register */
    __O  uint32_t RESTART;                     /* 0x14: Restart Register */
    __O  uint32_t WREN;                        /* 0x18: Write Protection Register */
    __O  uint32_t ST;                          /* 0x1C: Status Register */
} CHIP_REGS_WDG_T;

#define CHIP_REGS_WDG0              ((CHIP_REGS_WDG_T *)CHIP_MEMRGN_ADDR_WDG0)
#define CHIP_REGS_WDG1              ((CHIP_REGS_WDG_T *)CHIP_MEMRGN_ADDR_WDG1)
#define CHIP_REGS_WDG2              ((CHIP_REGS_WDG_T *)CHIP_MEMRGN_ADDR_WDG2)
#define CHIP_REGS_WDG3              ((CHIP_REGS_WDG_T *)CHIP_MEMRGN_ADDR_WDG3)
#define CHIP_REGS_PWDG              ((CHIP_REGS_WDG_T *)CHIP_MEMRGN_ADDR_PWDG)


#define CHIP_WDG_RESET_KEY          0xCAFE
#define CHIP_WDG_WREN_KEY           0x5AA5

/*********** WDG_CTRL - Control Register **************************************/
#define CHIP_REGFLD_WDG_CTRL_RSTTIME                    (0x00000007UL <<  8U) /* (RW) Time setting for triggering reset */
#define CHIP_REGFLD_WDG_CTRL_INTTIME                    (0x0000000FUL <<  4U) /* (RW) Time setting for triggering interrupt */
#define CHIP_REGFLD_WDG_CTRL_RSTEN                      (0x00000001UL <<  3U) /* (RW) Enable watchdog timeout reset */
#define CHIP_REGFLD_WDG_CTRL_INTEN                      (0x00000001UL <<  2U) /* (RW) Enable watchdog timeout interrupt */
#define CHIP_REGFLD_WDG_CTRL_CLKSEL                     (0x00000001UL <<  1U) /* (RW) Timing clock selection */
#define CHIP_REGFLD_WDG_CTRL_EN                         (0x00000001UL <<  0U) /* (RW) Enable watchdog timing */


#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_7             0 /* 2^7 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_8             1 /* 2^8 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_9             2 /* 2^9 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_10            3 /* 2^10 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_11            4 /* 2^11 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_12            5 /* 2^12 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_13            6 /* 2^13 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_RSTTIME_2_14            7 /* 2^14 timing clock cycles */


#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_6             0 /* 2^6 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_8             1 /* 2^8 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_10            2 /* 2^10 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_11            3 /* 2^11 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_12            4 /* 2^12 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_13            5 /* 2^13 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_14            6 /* 2^14 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_15            7 /* 2^15 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_17            8 /* 2^17 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_19            9 /* 2^19 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_21            10 /* 2^21 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_23            11 /* 2^23 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_25            12 /* 2^25 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_27            13 /* 2^27 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_29            14 /* 2^29 timing clock cycles */
#define CHIP_REGFLDVAL_WDG_CTRL_INTTIME_2_31            15 /* 2^31 timing clock cycles */

#define CHIP_REGFLDVAL_WDG_CTRL_CLKSEL_EXT              0  /* External clock */
#define CHIP_REGFLDVAL_WDG_CTRL_CLKSEL_BUS              1  /* Bus clock */

/*********** WDG_SR - Status Register *****************************************/
#define CHIP_REGFLD_WDG_SR_INTEXPIRED                   (0x00000001UL <<  0U) /* (RW1C) Watchdog interrupt timer status */

#endif // REGS_WDG_H
