#ifndef CHIP_ARM_ARCH_H
#define CHIP_ARM_ARCH_H

#include "chip_core_features.h"

#include "regs/regs_core_mmap.h"
#include "regs/regs_core_scb.h"
#include "regs/regs_core_mpu.h"
#include "regs/regs_core_cache.h"
#include "regs/regs_core_arm_features.h"
#include "regs/regs_core_fpu.h"
#include "regs/regs_core_arm_accctl.h"

#include "init/chip_core_cache.h"
#include "init/chip_core_mpu.h"
#include "init/chip_core_fpu.h"






#endif // CHIP_ARM_ARCH_H
