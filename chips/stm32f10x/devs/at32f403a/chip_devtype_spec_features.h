#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_DEV_AT32F40X
#define CHIP_DEV_AT32F403A

#define CHIP_DEV_SUPPORT_EXMC           (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_DEV_SUPPORT_TIM9           1
#define CHIP_DEV_SUPPORT_TIM10          1
#define CHIP_DEV_SUPPORT_TIM11          1
#define CHIP_DEV_SUPPORT_TIM12          1
#define CHIP_DEV_SUPPORT_TIM13          1
#define CHIP_DEV_SUPPORT_TIM14          1
#define CHIP_DEV_SUPPORT_TRACE          1
#define CHIP_DEV_SUPPORT_ETH_RGMII      0
#define CHIP_DEV_SUPPORT_USBHS          0
#define CHIP_DEV_SUPPORT_USBHS_PHY      0
#define CHIP_DEV_SUPPORT_RNG            0
#define CHIP_SUPPORT_ADC_INJECTED       1

#define CHIP_DEV_TIM_GP_CNT             8
#define CHIP_DEV_TIM_GP32_CNT           2
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          2
#define CHIP_DEV_SPI_CNT                4
#define CHIP_DEV_I2S_CNT                4
#define CHIP_DEV_I2S_FD_CNT             2
#define CHIP_DEV_I2C_CNT                3
#define CHIP_DEV_UART_CNT               7
#define CHIP_DEV_CAN_CNT                2
#define CHIP_DEV_USB_CNT                1
#define CHIP_DEV_SDIO_CNT               2
#define CHIP_DEV_ETH_CNT                0
#define CHIP_DEV_ADC_CNT                3
#define CHIP_DEV_DAC_CNT                2
#define CHIP_DEV_SPIM_CNT               1
#define CHIP_DEV_QSPI_CNT               0
#define CHIP_DEV_DVP_CNT                0
#define CHIP_DEV_USBHS_CNT              0
#define CHIP_DEV_BLES_CNT               0
#define CHIP_DEV_PLL_CNT                1 /** check */


#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(240)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
