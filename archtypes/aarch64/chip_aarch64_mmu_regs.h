#ifndef CHIP_AARCH64_MMU_REGS_H
#define CHIP_AARCH64_MMU_REGS_H

#include "chip_arm_feat_defs.h"


/* Размеры таблицы преобразования адресов одной стадии для разных размеров страницы */
#define CHIP_AARCH_TT_SIZE_TG_4KB               512   /* каждая стадия разрешает 9 бит, таблица содержит 512 записей */
#define CHIP_AARCH_TT_SIZE_TG_16KB              2048  /* каждая стадия разрешает 11 бит, таблица содержит 2048 записей */
#define CHIP_AARCH_TT_SIZE_TG_64KB              8192  /* каждая стадия разрешает 13 бит, таблица содержит 8192 записей */


#define CHIP_AARCH_LVL3_BLOCK_SIZE_TG_4K        0x00001000          /* 4KB */
#define CHIP_AARCH_LVL2_BLOCK_SIZE_TG_4K        0x00200000          /* 2MB */
#define CHIP_AARCH_LVL1_BLOCK_SIZE_TG_4K        0x40000000          /* 1GB */


/*********** SCTLR, System Control Register (EL1/EL2/EL3) *********************/
#define CHIP_REGFLD_AARCH_SCTLR_M               (0x0000000000000001ULL <<  0U) /* MMU enable */
#define CHIP_REGFLD_AARCH_SCTLR_A               (0x0000000000000001ULL <<  1U) /* Alignment check enable */
#define CHIP_REGFLD_AARCH_SCTLR_C               (0x0000000000000001ULL <<  2U) /* Data access Cacheability control */
#define CHIP_REGFLD_AARCH_SCTLR_SA              (0x0000000000000001ULL <<  3U) /* SP Alignment check enable */
#define CHIP_REGFLD_AARCH_SCTLR_SA0             (0x0000000000000001ULL <<  4U) /* EL1/2: SP Alignment check enable for EL0 */
#define CHIP_REGFLD_AARCH_SCTLR_CP15BEN         (0x0000000000000001ULL <<  5U) /* EL1/2: System instruction memory barrier enable. */
#if CHIP_AARCH_FEAT_LSE2
#define CHIP_REGFLD_AARCH_SCTLR_nAA             (0x0000000000000001ULL <<  6U) /* Non-aligned access check disable */
#endif
#define CHIP_REGFLD_AARCH_SCTLR_ITD             (0x0000000000000001ULL <<  7U) /* EL1/2: Disables some uses of IT instructions at EL0 using AArch32  */
#define CHIP_REGFLD_AARCH_SCTLR_SED             (0x0000000000000001ULL <<  8U) /* EL1/2: Disables SETEND instructions at EL0 using AArch32 */
#define CHIP_REGFLD_AARCH_SCTLR_UMA             (0x0000000000000001ULL <<  9U) /* EL1: User Mask Access */
#if CHIP_AARCH_FEAT_SPECRES
#define CHIP_REGFLD_AARCH_SCTLR_EnRCTX          (0x0000000000000001ULL << 10U) /* EL1/2: Enable EL0 access to the CFPRCTX, DVPRCTX and CPPRCTX System instructions.*/
#endif
#if CHIP_AARCH_FEAT_ExS
#define CHIP_REGFLD_AARCH_SCTLR_EOS             (0x0000000000000001ULL << 11U) /* FEAT_ExS: Exception Exit is Context Synchronizing */
#endif
#define CHIP_REGFLD_AARCH_SCTLR_I               (0x0000000000000001ULL << 12U) /* Instruction access Cacheability control */
#define CHIP_REGFLD_AARCH_SCTLR_ENDB            (0x0000000000000001ULL << 13U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_DZE             (0x0000000000000001ULL << 14U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_UCT             (0x0000000000000001ULL << 15U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_NTWI            (0x0000000000000001ULL << 16U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_NTWE            (0x0000000000000001ULL << 18U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_WXN             (0x0000000000000001ULL << 19U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_TSCXT           (0x0000000000000001ULL << 20U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_IESB            (0x0000000000000001ULL << 21U) /* */
#if CHIP_AARCH_FEAT_ExS
#define CHIP_REGFLD_AARCH_SCTLR_EIS             (0x0000000000000001ULL << 22U) /* FEAT_ExS: Exception Entry is Context Synchronizing. */
#endif
#define CHIP_REGFLD_AARCH_SCTLR_SPAN            (0x0000000000000001ULL << 23U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_E0E             (0x0000000000000001ULL << 24U) /* EL1/2:*/
#define CHIP_REGFLD_AARCH_SCTLR_EE              (0x0000000000000001ULL << 25U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_UCI             (0x0000000000000001ULL << 26U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_ENDA            (0x0000000000000001ULL << 27U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_NTLSMD          (0x0000000000000001ULL << 28U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_LSMAOE          (0x0000000000000001ULL << 29U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_ENIB            (0x0000000000000001ULL << 30U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_ENIA            (0x0000000000000001ULL << 31U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_CMOW            (0x0000000000000001ULL << 32U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_MSCEN           (0x0000000000000001ULL << 33U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_BT0             (0x0000000000000001ULL << 35U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_BT              (0x0000000000000001ULL << 36U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_ITFSB           (0x0000000000000001ULL << 37U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_TCF0            (0x0000000000000003ULL << 38U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_TCF             (0x0000000000000003ULL << 40U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_ATA0            (0x0000000000000001ULL << 42U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_ATA             (0x0000000000000001ULL << 43U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_DSSBS           (0x0000000000000001ULL << 44U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_TWEDEN          (0x0000000000000001ULL << 45U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_TWEDEL          (0x000000000000000FULL << 46U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_TMT0            (0x0000000000000001ULL << 50U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_TMT             (0x0000000000000001ULL << 51U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_TME0            (0x0000000000000001ULL << 52U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_TME             (0x0000000000000001ULL << 53U) /* */
#define CHIP_REGFLD_AARCH_SCTLR_ENASR           (0x0000000000000001ULL << 54U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_ENAS0           (0x0000000000000001ULL << 55U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_ENALS           (0x0000000000000001ULL << 56U) /* EL1/2: */
#define CHIP_REGFLD_AARCH_SCTLR_EPAN            (0x0000000000000001ULL << 57U) /* EL1/2: */
#if CHIP_AARCH_FEAT_SME
#define CHIP_REGFLD_AARCH_SCTLR_EnTP2           (0x0000000000000001ULL << 60U) /* EL1/2: (HCR.E2H == 1, HCR.TGE == 1). Traps instructions executed at EL0 that access TPIDR2_EL0 to EL2 */
#endif
#if CHIP_AARCH_FEAT_NMI
#define CHIP_REGFLD_AARCH_SCTLR_NMI             (0x0000000000000001ULL << 61U) /* Non-maskable Interrupt enable */
#define CHIP_REGFLD_AARCH_SCTLR_SPINTMASK       (0x0000000000000001ULL << 62U) /* (NMI == 1) SP Interrupt Mask enable (PSTATE.SP mask interrupt) */
#endif
#if CHIP_AARCH_FEAT_TIDCP1
#define CHIP_REGFLD_AARCH_SCTLR_TIDCP           (0x0000000000000001ULL << 63U) /* EL1/2: Trap IMPLEMENTATION DEFINED functionality */
#endif


/*********** TCR, Translation Control Register (EL2/EL3) ******************/
#define CHIP_REGFLD_AARCH_TCR_T0SZ              (0x000000000000003FULL <<  0U) /* The size offset of the memory region addressed by TTBR0. The region size is 2^(64-T0SZ) bytes*/
#define CHIP_REGFLD_AARCH_TCR_IRGN0             (0x0000000000000003ULL <<  8U) /* Inner cacheability attribute for memory associated with translation table walks using TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_ORGN0             (0x0000000000000003ULL << 10U) /* Outer cacheability attribute for memory associated with translation table walks using TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_SH0               (0x0000000000000003ULL << 12U) /* Shareability attribute for memory associated with translation table walks using TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_TG0               (0x0000000000000003ULL << 14U) /* Granule size for the TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_PS                (0x0000000000000007ULL << 16U) /* Physical Address Size */
#define CHIP_REGFLD_AARCH_TCR_TBI               (0x0000000000000001ULL << 20U) /* Top Byte Ignored: whether the top byte of an address is used for address match for the TTBR0 region, or ignored and used for tagged addresses */
#if CHIP_AARCH_FEAT_HAFDBS
#define CHIP_REGFLD_AARCH_TCR_HA                (0x0000000000000001ULL << 21U) /* FEAT_HAFDBS: */
#define CHIP_REGFLD_AARCH_TCR_HD                (0x0000000000000001ULL << 22U) /* FEAT_HAFDBS: */
#endif
#if CHIP_AARCH_FEAT_HPDS
#define CHIP_REGFLD_AARCH_TCR_HPD               (0x0000000000000001ULL << 24U) /* FEAT_HPDS: */
#endif
#if CHIP_AARCH_FEAT_HPDS2
#define CHIP_REGFLD_AARCH_TCR_HWU59             (0x0000000000000001ULL << 25U) /* FEAT_HPDS2: */
#define CHIP_REGFLD_AARCH_TCR_HWU60             (0x0000000000000001ULL << 26U) /* FEAT_HPDS2:  */
#define CHIP_REGFLD_AARCH_TCR_HWU61             (0x0000000000000001ULL << 27U) /* FEAT_HPDS2: */
#define CHIP_REGFLD_AARCH_TCR_HWU62             (0x0000000000000001ULL << 28U) /* FEAT_HPDS2:  */
#endif
#if CHIP_AARCH_FEAT_PAUTH
#define CHIP_REGFLD_AARCH_TCR_TBID              (0x0000000000000001ULL << 29U) /* FEAT_PAUTH:  */
#endif
#if CHIP_AARCH_FEAT_MTE2
#define CHIP_REGFLD_AARCH_TCR_TCMA              (0x0000000000000001ULL << 30U) /* FEAT_MTE2: All accesses are Unchecked when address [59:56] = 0b0000. */
#endif
#if CHIP_AARCH_FEAT_LPA2
#define CHIP_REGFLD_AARCH_TCR_DS                (0x0000000000000001ULL << 32U) /* FEAT_LPA2: Whether a 52-bit output address can be described by the translation tables of the 4KB or 16KB translation granules */
#endif
#define CHIP_REGMSK_AARCH_TCR_RES1              (0x0000000080800000ULL)

/*********** TCR, Translation Control Register (EL1) ******************/
#define CHIP_REGFLD_AARCH_TCR_EL1_T0SZ          (0x000000000000003FULL <<  0U) /* The size offset of the memory region addressed by TTBR0. The region size is 2^(64-T0SZ) bytes*/
#define CHIP_REGFLD_AARCH_TCR_EL1_EPD0          (0x0000000000000001ULL <<  7U) /* Translation table walk disable for translations using TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_EL1_IRGN0         (0x0000000000000003ULL <<  8U) /* Inner cacheability attribute for memory associated with translation table walks using TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_EL1_ORGN0         (0x0000000000000003ULL << 10U) /* Outer cacheability attribute for memory associated with translation table walks using TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_EL1_SH0           (0x0000000000000003ULL << 12U) /* Shareability attribute for memory associated with translation table walks using TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_EL1_TG0           (0x0000000000000003ULL << 14U) /* Granule size for the TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_EL1_T1SZ          (0x000000000000003FULL << 16U) /* The size offset of the memory region addressed by TTBR1. The region size is 2^(64-T1SZ) bytes */
#define CHIP_REGFLD_AARCH_TCR_EL1_A1            (0x0000000000000001ULL << 22U) /* Selects whether TTBR0_EL1 (0) or TTBR1_EL1 (1) defines the ASID */
#define CHIP_REGFLD_AARCH_TCR_EL1_EPD1          (0x0000000000000001ULL << 23U) /* Translation table walk disable for translations using TTBR1 */
#define CHIP_REGFLD_AARCH_TCR_EL1_IRGN1         (0x0000000000000003ULL << 24U) /* Inner cacheability attribute for memory associated with translation table walks using TTBR1 */
#define CHIP_REGFLD_AARCH_TCR_EL1_ORGN1         (0x0000000000000003ULL << 26U) /* Outer cacheability attribute for memory associated with translation table walks using TTBR1 */
#define CHIP_REGFLD_AARCH_TCR_EL1_SH1           (0x0000000000000003ULL << 28U) /* Shareability attribute for memory associated with translation table walks using TTBR1 */
#define CHIP_REGFLD_AARCH_TCR_EL1_TG1           (0x0000000000000003ULL << 30U) /* Granule size for the TTBR1 */
#define CHIP_REGFLD_AARCH_TCR_EL1_IPS           (0x0000000000000007ULL << 32U) /* (Intermediate) Physical Address Size */
#define CHIP_REGFLD_AARCH_TCR_EL1_AS            (0x0000000000000001ULL << 36U) /* ASID Size */
#define CHIP_REGFLD_AARCH_TCR_EL1_TBI0          (0x0000000000000001ULL << 37U) /* Top Byte ignored for TTBR0 */
#define CHIP_REGFLD_AARCH_TCR_EL1_TBI1          (0x0000000000000001ULL << 38U) /* Top Byte ignored for TTBR1 */
#define CHIP_REGFLD_AARCH_TCR_EL1_HA            (0x0000000000000001ULL << 39U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HD            (0x0000000000000001ULL << 40U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HPD0          (0x0000000000000001ULL << 41U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HPD1          (0x0000000000000001ULL << 42U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU059        (0x0000000000000001ULL << 43U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU060        (0x0000000000000001ULL << 44U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU061        (0x0000000000000001ULL << 45U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU062        (0x0000000000000001ULL << 46U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU159        (0x0000000000000001ULL << 47U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU160        (0x0000000000000001ULL << 48U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU161        (0x0000000000000001ULL << 49U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_HWU162        (0x0000000000000001ULL << 50U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_TBID0         (0x0000000000000001ULL << 51U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_TBID1         (0x0000000000000001ULL << 52U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_NFD0          (0x0000000000000001ULL << 53U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_NFD1          (0x0000000000000001ULL << 54U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_E0PD0         (0x0000000000000001ULL << 55U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_E0PD1         (0x0000000000000001ULL << 56U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_TCMA0         (0x0000000000000001ULL << 57U) /* */
#define CHIP_REGFLD_AARCH_TCR_EL1_TCMA1         (0x0000000000000001ULL << 58U) /* */
#if CHIP_AARCH_FEAT_LPA2
#define CHIP_REGFLD_AARCH_TCR_EL1_DS            (0x0000000000000001ULL << 59U) /* Whether a 52-bit output address can be described by the translation tables of the 4KB or 16KB translation granules */
#endif

#define CHIP_REGFLDVAL_AARCH_TCR_RGN_NO         0 /* Normal memory, Non-cacheable. */
#define CHIP_REGFLDVAL_AARCH_TCR_RGN_WB_RWAL    1 /* Normal memory, Write-Back Read-Allocate Write-Allocate Cacheable. */
#define CHIP_REGFLDVAL_AARCH_TCR_RGN_WT_ROAL    2 /* Normal memory, Write-Through Read-Allocate No Write-Allocate Cacheable. */
#define CHIP_REGFLDVAL_AARCH_TCR_RGN_WB_ROAL    3 /* Normal memory, Write-Back Read-Allocate No Write-Allocate Cacheable. */

#define CHIP_REGFLDVAL_AARCH_TCR_SH_NO          0 /* Non-shareable */
#define CHIP_REGFLDVAL_AARCH_TCR_SH_OUTER       2 /* Outer Shareable */
#define CHIP_REGFLDVAL_AARCH_TCR_SH_INNER       3 /* Inner Shareable */

#define CHIP_REGFLDVAL_AARCH_TCR_TG_4KB         0 /* 4KB */
#define CHIP_REGFLDVAL_AARCH_TCR_TG_64KB        1 /* 64KB */
#define CHIP_REGFLDVAL_AARCH_TCR_TG_16KB        2 /* 16KB */

#define CHIP_REGFLDVAL_AARCH_TCR_PS_32          0 /* 32 bits, 4GB. */
#define CHIP_REGFLDVAL_AARCH_TCR_PS_36          1 /* 36 bits, 64GB. */
#define CHIP_REGFLDVAL_AARCH_TCR_PS_40          2 /* 40 bits, 1TB. */
#define CHIP_REGFLDVAL_AARCH_TCR_PS_42          3 /* 42 bits, 4TB. */
#define CHIP_REGFLDVAL_AARCH_TCR_PS_44          4 /* 44 bits, 16TB. */
#define CHIP_REGFLDVAL_AARCH_TCR_PS_48          5 /* 48 bits, 256TB. */
#define CHIP_REGFLDVAL_AARCH_TCR_PS_52          6 /* 52 bits, 4PB. */

#define CHIP_REGFLDVAL_AARCH_TCR_AS_8           0 /* 8 bit ASID size*/
#define CHIP_REGFLDVAL_AARCH_TCR_AS_16          1 /* 16 bit ASID size */


/*** TTBRn, Translation Table Base Register n (0 - EL1/EL2/EL3, 1 - EL1/EL2) **/
#if CHIP_AARCH_FEAT_TTCNP
#define CHIP_REGFLD_AARCH_TTBR_CNP              (0x0000000000000001ULL <<  0U) /* Common not Private */
#endif
#define CHIP_REGFLD_AARCH_TTBR_BADDR            (0x00007FFFFFFFFFFFULL <<  1U) /* Translation table base address bits A[47:x] */
#define CHIP_REGFLD_AARCH_TTBR_ASID             (0x000000000000FFFFULL << 48U) /* (EL1/EL2) An ASID for the translation table base address */


/*********** MAIR_EL1, Memory Attribute Indirection Register (EL1) ************/
#define CHIP_REGFLD_AARCH_MAIR_ATTR(i)          (0x00000000000000FFULL <<  8*(i)) /* The memory attribute encoding for an AttrIndx[2:0] entry */

#define CHIP_REGFLDVAL_AARCH_MAIR_ATTR_DEV(d)   (((d) & 0x3) << 2)
#define CHIP_REGFLDVAL_AARCH_MAIR_ATTR_NORM(i,o)(((i) & 0xF) | (((o) & 0xF) << 4))

#define CHIP_AARCH_MEMATTR_DEV_nGnRnE           0
#define CHIP_AARCH_MEMATTR_DEV_nGnRE            1
#define CHIP_AARCH_MEMATTR_DEV_nGRE             2
#define CHIP_AARCH_MEMATTR_DEV_GRE              3

#define CHIP_AARCH_MEMATTR_NORM_WTT_RWAL        (0x3 | 0x0) /* Normal memory, Write-Through Transient, Read and Write Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WTT_ROAL        (0x2 | 0x0) /* Normal memory, Write-Through Transient, Read Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WTT_WOAL        (0x1 | 0x0) /* Normal memory, Write-Through Transient, Write Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_NO_CACHE        (0x0 | 0x4) /* Normal memory, Non-cacheable */
#define CHIP_AARCH_MEMATTR_NORM_WBT_RWAL        (0x3 | 0x4) /* Normal memory, Write-Back Transient, Read and Write Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WBT_ROAL        (0x2 | 0x4) /* Normal memory, Write-Back Transient, Read Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WBT_WOAL        (0x1 | 0x4) /* Normal memory, Write-Back Transient, Write Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WTNT_RWAL       (0x3 | 0x8) /* Normal memory, Write-Through Non-Transient, Read and Write Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WTNT_ROAL       (0x2 | 0x8) /* Normal memory, Write-Through Non-Transient, Read Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WTNT_WOAL       (0x1 | 0x8) /* Normal memory, Write-Through Non-Transient, Write Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WBNT_RWAL       (0x3 | 0xC) /* Normal memory, Write-Back Non-Transient, Read and Write Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WBNT_ROAL       (0x2 | 0xC) /* Normal memory, Write-Back Non-Transient, Read Allocate  */
#define CHIP_AARCH_MEMATTR_NORM_WBNT_WOAL       (0x1 | 0xC) /* Normal memory, Write-Back Non-Transient, Write Allocate  */


/*********** PAR, Physical Address Register (EL1) *****************************/
#define CHIP_REGFLD_AARCH_PAR_F                 (0x0000000000000001ULL <<  0U) /* Translation failed */
#define CHIP_REGFLD_AARCH_PAR_SH                (0x0000000000000003ULL <<  7U) /* (F==0) Shareability attribute, for the returned output address. */
#define CHIP_REGFLD_AARCH_PAR_NS                (0x0000000000000001ULL <<  9U) /* (F==0) Non-secure. The NS attribute for a translation table entry from a Secure translation regime */
#define CHIP_REGFLD_AARCH_PAR_NSE               (0x0000000000000001ULL << 11U) /* (F==0) NSE attribute for a translation table entry from the EL3 translation regime. */
#define CHIP_REGFLD_AARCH_PAR_PA                (0x000000FFFFFFFFFFULL << 12U) /* (F==0) The output address (OA) corresponding to the supplied input address */
#define CHIP_REGFLD_AARCH_PAR_ATTR              (0x00000000000000FFULL << 56U) /* (F==0) Memory attributes for the returned output address (==MAIR) */
#define CHIP_REGFLD_AARCH_PAR_FST               (0x000000000000003FULL <<  1U) /* (F==1) Fault status code */
#define CHIP_REGFLD_AARCH_PAR_PTW               (0x0000000000000001ULL <<  8U) /* (F==1) Translation aborted because of a stage 2 fault during a stage 1 translation table walk. */
#define CHIP_REGFLD_AARCH_PAR_S                 (0x0000000000000001ULL <<  9U) /* (F==1) Translation stage at which the translation aborted (0-1,1-2) */

#define CHIP_AARCH_TFS_ADDR_SIZE(lvl)           ((lvl) == -1 ? 0x29 : (0x00 + (lvl))) /* Address size fault, level 0..3 (-1 - FEAT_LPA2) of translation or translation table base register */
#define CHIP_AARCH_TFS_TRANSLATION(lvl)         ((lvl) == -1 ? 0x2B : (0x04 + (lvl))) /* Translation fault, level 0..3 (-1 - FEAT_LPA2) */
#define CHIP_AARCH_TFS_ACC_FLAG(lvl)            (0x08 + (lvl)) /* Translation fault, level 1..3 (0 - FEAT_LPA2)*/
#define CHIP_AARCH_TFS_PERMISSION(lvl)          (0x0C + (lvl)) /* Permission fault, level 1..3 (0 - FEAT_LPA2)*/
#define CHIP_AARCH_TFS_SYNC_EXT_ABORT(lvl)      (0x14 + (lvl)) /* Synchronous External abort on translation table walk or hardware update of translation table, level 0..3 (-1 - FEAT_LPA2)*/
#define CHIP_AARCH_TFS_SYNC_ECC(lvl)            (0x1C + (lvl)) /* Synchronous parity or ECC error on memory access on translation table walk or hardware update of translation table, level 0..3 (-1 - FEAT_LPA2)*/
#if CHIP_AARCH_FEAT_RME
#define CHIP_AARCH_TFS_GRAN_PROT(lvl)           (0x24 + (lvl)) /* Granule Protection Fault on translation table walk or hardware update of translation table, level 0..3 (-1 - FEAT_LPA2)*/
#define CHIP_AARCH_TFS_GRAN_PROT_NOT_WALK       (0x28)         /* Granule Protection Fault, not on translation table walk or hardware update of translation table. */
#endif
#define CHIP_AARCH_TFS_TLB_CONFLICT             (0x30) /* TLB conflict abort. */
#if CHIP_AARCH_FEAT_HAFDBS
#define CHIP_AARCH_TFS_TLB_UNSUP_ATOMIC_HWUP    (0x31) /* Unsupported atomic hardware update fault. */
#endif
#define CHIP_AARCH_TFS_TLB_AARCH32_SECT_DOMAIN  (0x3D) /* Section Domain fault, from an AArch32 stage 1 EL1&0 translation regime using Short-descriptor translation table format. */
#define CHIP_AARCH_TFS_TLB_AARCH32_PAGE_DOMAIN  (0x3E) /* Page Domain fault, from an AArch32 stage 1 EL1&0 translation regime using Short-descriptor translation table format. */


/****** Translation Table Format **********************************************/
#define CHIP_REGFLD_AARCH_TT_VALID              (0x0000000000000001ULL <<  0U) /* Valid */
#define CHIP_REGFLD_AARCH_TT_NEXT_LVL           (0x0000000000000001ULL <<  1U) /* Next level table (0 - block (lvl0..2), 1 - next level table (lvl0..2) or page (lvl3)) */
#define CHIP_REGMSK_AARCH_TT_ADDR               (0x0003FFFFFFFFF000ULL)
#define CHIP_REGFLD_AARCH_TT_ATTR_UPPER         (0x0000000000003FFFULL << 50U)
#define CHIP_REGFLD_AARCH_TT_ATTRINDX           (0x0000000000000007ULL <<  2U) /* Attribute index in MAIR register */
#define CHIP_REGFLD_AARCH_TT_NS                 (0x0000000000000001ULL <<  5U) /* Non-secure */
#define CHIP_REGFLD_AARCH_TT_AP                 (0x0000000000000003ULL <<  6U) /* Data Access Permission */
#define CHIP_REGFLD_AARCH_TT_SH                 (0x0000000000000003ULL <<  8U) /* Shareability */
#define CHIP_REGFLD_AARCH_TT_AF                 (0x0000000000000001ULL << 10U) /* Access flag */
#define CHIP_REGFLD_AARCH_TT_NSE                (0x0000000000000001ULL << 11U)
#if CHIP_AARCH_FEAT_BBM
#define CHIP_REGFLD_AARCH_TT_nT                 (0x0000000000000001ULL << 16U)
#endif
#if CHIP_AARCH_FEAT_BTI
#define CHIP_REGFLD_AARCH_TT_GP                 (0x0000000000000001ULL << 50U) /* Guarded page field */
#endif
#define CHIP_REGFLD_AARCH_TT_DBM                (0x0000000000000001ULL << 51U) /* Dirty Bit Modifier */
#define CHIP_REGFLD_AARCH_TT_CONT               (0x0000000000000001ULL << 52U) /* Contiguous */
#define CHIP_REGFLD_AARCH_TT_PXN                (0x0000000000000001ULL << 53U) /* Privileged execute never */
#define CHIP_REGFLD_AARCH_TT_UXN                (0x0000000000000001ULL << 54U) /* Unprivileged execute never */
#define CHIP_REGFLD_AARCH_TT_S2_NS              (0x0000000000000001ULL << 55U) /* (stage 2 only) */
#if CHIP_AARCH_FEAT_HPDS2
#define CHIP_REGFLD_AARCH_TT_PBHA               (0x000000000000000FULL << 59U) /* Page Based Hardware Attribute */
#endif

#define CHIP_REGFLDVAL_AARCH_TT_AP_RW_PRIV      0
#define CHIP_REGFLDVAL_AARCH_TT_AP_RW_ALL       1
#define CHIP_REGFLDVAL_AARCH_TT_AP_RO_PRIV      2
#define CHIP_REGFLDVAL_AARCH_TT_AP_RO_ALL       3


#endif // CHIP_AARCH64_MMU_REGS_H
