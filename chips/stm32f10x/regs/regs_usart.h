#ifndef REGS_USART_ST32F10X_H
#define REGS_USART_ST32F10X_H


#include "chip_devtype_spec_features.h"

#define CHIP_SUPPORT_USART_OVER8      0
#define CHIP_SUPPORT_USART_ONEBIT     0

#include "stm32_usart_v1_regs.h"

#define CHIP_REGS_USART1              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART1)
#define CHIP_REGS_USART2              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART2)
#define CHIP_REGS_USART3              ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_USART3)
#if CHIP_DEV_UART_CNT >= 4
#define CHIP_REGS_UART4               ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART4)
#endif
#if CHIP_DEV_UART_CNT >= 5
#define CHIP_REGS_UART5               ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART5)
#endif
#if CHIP_DEV_UART_CNT >= 6
#define CHIP_REGS_UART6               ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART6)
#endif
#if CHIP_DEV_UART_CNT >= 7
#define CHIP_REGS_UART7               ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART7)
#endif
#if CHIP_DEV_UART_CNT >= 8
#define CHIP_REGS_UART8               ((CHIP_REGS_USART_T *) CHIP_MEMRGN_ADDR_PERIPH_UART8)
#endif

#if defined CHIP_DEV_CH32F20X_D6 || defined CHIP_DEV_CH32V20X_D6
#define CHIP_USART_SUPPORT_SYNC(n)    (n <= 4)
#else
#define CHIP_USART_SUPPORT_SYNC(n)    (n <= 3)
#endif

#endif // REGS_USART_ST32F10X_H
