#ifndef REGS_BCFG_H
#define REGS_BCFG_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t VBG_CFG;                     /* 0x0: Bandgap config */
    __IO uint32_t LDO_CFG;                     /* 0x4: LDO config */
    __IO uint32_t IRC32K_CFG;                  /* 0x8: 32K internal clock config */
    __IO uint32_t XTAL32K_CFG;                 /* 0xC: XTAL 32K config */
    __IO uint32_t CLK_CFG;                     /* 0x10: Clock config */
} CHIP_REGS_BCFG_T;

#define CHIP_REGS_BCFG            ((CHIP_REGS_BCFG_T *)CHIP_MEMRGN_ADDR_BCFG)


/*********** Bandgap config - VBG_CFG  ****************************************/
#define CHIP_REGFLD_BCFG_VBG_CFG_TRIMMED                (0x00000001UL << 31U) /* (RW) Is calibrated */
#define CHIP_REGFLD_BCFG_VBG_CFG_LP_MODE                (0x00000001UL << 25U) /* (RW) Low power mode */
#define CHIP_REGFLD_BCFG_VBG_CFG_POWER_SAVE             (0x00000001UL << 24U) /* (RW) Power-saving mode */
#define CHIP_REGFLD_BCFG_VBG_CFG_VBG_1P0                (0x0000001FUL << 16U) /* (RW) 1.0V reference calibration value */
#define CHIP_REGFLD_BCFG_VBG_CFG_VBG_P65                (0x0000001FUL <<  8U) /* (RW) 0.65V reference calibration value */
#define CHIP_REGFLD_BCFG_VBG_CFG_VBG_P50                (0x0000001FUL <<  0U) /* (RW) 0.5V reference calibration value */
#define CHIP_REGMSK_BCFG_VBG_CFG_RESERVED               (0x7CE0E0E0UL)

/*********** LDO config - LDO_CFG  ********************************************/
#define CHIP_REGFLD_BCFG_LDO_CFG_RES_TRIM               (0x00000003UL << 24U) /* (RW) Resistance calibration */
#define CHIP_REGFLD_BCFG_LDO_CFG_CP_TRIM                (0x00000003UL << 20U) /* (RW) Capacitance calibration */
#define CHIP_REGFLD_BCFG_LDO_CFG_EN_SL                  (0x00000001UL << 18U) /* (RW) Turn on selfload */
#define CHIP_REGFLD_BCFG_LDO_CFG_DIS_PD                 (0x00000001UL << 17U) /* (RW) Turn off the discharge during overvoltage */
#define CHIP_REGFLD_BCFG_LDO_CFG_ENABLE                 (0x00000001UL << 16U) /* (RW) LDO enble */
#define CHIP_REGFLD_BCFG_LDO_CFG_VOLT                   (0x00000FFFUL <<  0U) /* (RW) Output voltage in millivolts (600 mV - 1100 mV, 20 mV step) */
#define CHIP_REGMSK_BCFG_LDO_CFG_RESERVED               (0xFCC8F000UL)

/*********** 32K internal clock config - IRC32K_CFG ***************************/
#define CHIP_REGFLD_BCFG_IRC32K_CFG_TRIMMED             (0x00000001UL << 31U) /* (RW) Is calibrated */
#define CHIP_REGFLD_BCFG_IRC32K_CFG_CAPEX7_TRIM         (0x00000001UL << 23U) /* (RW) 32K capacitor calibration value, redundant bit 7 */
#define CHIP_REGFLD_BCFG_IRC32K_CFG_CAPEX6_TRIM         (0x00000001UL << 22U) /* (RW) 32K capacitor calibration value, redundant bit 6 */
#define CHIP_REGFLD_BCFG_IRC32K_CFG_CAP_TRIM            (0x000001FFUL <<  0U) /* (RW) 32K capacitor calibration value */
#define CHIP_REGMSK_BCFG_IRC32K_CFG_RESERVED            (0x7F3FFE00UL)

/*********** 32K crystal oscillator - XTAL32K_CFG ***********************/
#define CHIP_REGFLD_BCFG_XTAL32K_CFG_HYST_EN            (0x00000001UL << 12U) /* (RW) 32K oscillator hysteresis enable */
#define CHIP_REGFLD_BCFG_XTAL32K_CFG_GMSEL              (0x00000003UL <<  8U) /* (RW) 32K oscillator trans-conductance */
#define CHIP_REGFLD_BCFG_XTAL32K_CFG_CFG                (0x00000001UL <<  4U) /* (RW) 32K oscillator configuration */
#define CHIP_REGFLD_BCFG_XTAL32K_CFG_AMP                (0x00000003UL <<  0U) /* (RW) 32K oscillator amplifier */
#define CHIP_REGMSK_BCFG_XTAL32K_RESERVED               (0xFFFFECECUL)

/*********** Clock config - CLK_CFG *******************************************/
#define CHIP_REGFLD_BCFG_CLK_CFG_XTAL_SEL               (0x00000001UL << 28U) /* (RO) Crystal selection status */
#define CHIP_REGFLD_BCFG_CLK_CFG_KEEP_IRC               (0x00000001UL << 16U) /* (RW) Force IRC32K run */
#define CHIP_REGFLD_BCFG_CLK_CFG_FORCE_XTAL             (0x00000001UL <<  4U) /* (RW) Force switch to crystal */
#define CHIP_REGMSK_BCFG_CLK_CFG_RESERVED               (0xEFFEFFEFUL)


#endif // REGS_BCFG_H
