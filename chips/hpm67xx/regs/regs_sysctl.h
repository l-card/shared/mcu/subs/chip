#ifndef REGS_SYSCTL_H
#define REGS_SYSCTL_H

#include "regs_common_types.h"


typedef struct {
    __IO uint32_t RESOURCE[350];               /* 0x0 - 0x574: Resource control register */
    __I  uint8_t  RESERVED0[648];              /* 0x578 - 0x7FF: Reserved */
    CHIP_REGS_BITSETCLR_T GROUP0[3];        /* 0x800 - 0x82C: Group0 setting */
    __I  uint8_t  RESERVED1[16];               /* 0x830 - 0x83F: Reserved */
    CHIP_REGS_BITSETCLR_T GROUP1[3];        /* 0x840 - 0x86C: Group1 setting */
    __I  uint8_t  RESERVED2[144];              /* 0x870 - 0x8FF: Reserved */
    CHIP_REGS_BITSETCLR_T AFFILIATE[2];     /* 0x900 - 0x91C: Affiliate CPU to Groups */
    CHIP_REGS_BITSETCLR_T RETENTION[2];     /* 0x920 - 0x93C: Resource Retention Contol */
    __I  uint8_t  RESERVED3[1728];             /* 0x940 - 0xFFF: Reserved */
    struct {
        __IO uint32_t STATUS;                  /* 0x1000: Power switch control */
        __IO uint32_t LF_WAIT;                 /* 0x1004: Power switch low fanout wait */
        __I  uint8_t  RESERVED0[4];            /* 0x1008 - 0x100B: Reserved */
        __IO uint32_t OFF_WAIT;                /* 0x100C: Power off wait */
    } POWER[4];
    __I  uint8_t  RESERVED4[960];              /* 0x1040 - 0x13FF: Reserved */
    struct {
        __IO uint32_t CONTROL;                 /* 0x1400: Reset control register */
        __IO uint32_t CONFIG;                  /* 0x1404: Reset configuration register */
        __I  uint8_t  RESERVED0[4];            /* 0x1408 - 0x140B: Reserved */
        __IO uint32_t COUNTER;                 /* 0x140C: Reset counter */
    } RESET[5];
    __I  uint8_t  RESERVED5[944];              /* 0x1450 - 0x17FF: Reserved */
    __IO uint32_t CLOCK[67];                   /* 0x1800 - 0x1908: Clock setting register */
    __I  uint8_t  RESERVED6[756];              /* 0x190C - 0x1BFF: Reserved */
    __IO uint32_t ADCCLK[4];                   /* 0x1C00 - 0x1C0C: ADC functional clock setting */
    __IO uint32_t I2SCLK[4];                   /* 0x1C10 - 0x1C1C: I2S functional clock setting */
    __I  uint8_t  RESERVED7[992];              /* 0x1C20 - 0x1FFF: Reserved */
    __IO uint32_t GLOBAL00;                    /* 0x2000: Clock senario */
    __I  uint8_t  RESERVED8[1020];             /* 0x2004 - 0x23FF: Reserved */
    struct {
        __IO uint32_t CONTROL;                 /* 0x2400: Clock measure and monitor control */
        __I  uint32_t CURRENT;                 /* 0x2404: Clock measure result */
        __IO uint32_t LOW_LIMIT;               /* 0x2408: Clock lower limit */
        __IO uint32_t HIGH_LIMIT;              /* 0x240C: Clock upper limit */
        __I  uint8_t  RESERVED0[16];           /* 0x2410 - 0x241F: Reserved */
    } MONITOR[4];
    __I  uint8_t  RESERVED9[896];              /* 0x2480 - 0x27FF: Reserved */
    struct {
        __IO uint32_t LP;                      /* 0x2800: CPU low power control */
        __IO uint32_t LOCK;                    /* 0x2804: CPU general registers lock control */
        __IO uint32_t GPR[14];                 /* 0x2808 - 0x283C: CPU general register x */
        __I  uint32_t WAKEUP_STATUS[8];        /* 0x2840 - 0x285C: Wake up the IRQ status of CPU */
        __I  uint8_t  RESERVED0[32];           /* 0x2860 - 0x287F: Reserved */
        __IO uint32_t WAKEUP_ENABLE[8];        /* 0x2880 - 0x289C: Wake up IRQ enable of CPU */
        __I  uint8_t  RESERVED1[864];          /* 0x28A0 - 0x2BFF: Reserved */
    } CPU[2];
} CHIP_REGS_SYSCTL_T;

#define CHIP_REGS_SYSCTL          ((CHIP_REGS_SYSCTL_T *)CHIP_MEMRGN_ADDR_SYSCTL)





/* RESOURCE register group index macro definition */
#define CHIP_SYSCTL_RESOURCE_NUM_CPU0_CORE              (0UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CPU0_SUBSYS            (1UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CPU1_CORE              (8UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CPU1_SUBSYS            (9UL)
#define CHIP_SYSCTL_RESOURCE_NUM_POW_CON                (21UL)
#define CHIP_SYSCTL_RESOURCE_NUM_POW_VIS                (22UL)
#define CHIP_SYSCTL_RESOURCE_NUM_POW_CPU0               (23UL)
#define CHIP_SYSCTL_RESOURCE_NUM_POW_CPU1               (24UL)
#define CHIP_SYSCTL_RESOURCE_NUM_RST_SOC                (25UL)
#define CHIP_SYSCTL_RESOURCE_NUM_RST_CON                (26UL)
#define CHIP_SYSCTL_RESOURCE_NUM_RST_VIS                (27UL)
#define CHIP_SYSCTL_RESOURCE_NUM_RST_CPU0               (28UL)
#define CHIP_SYSCTL_RESOURCE_NUM_RST_CPU1               (29UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_XTAL           (32UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL0           (33UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL0_CLK0      (34UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL1           (35UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL1_CLK0      (36UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL1_CLK1      (37UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL2           (38UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL2_CLK0      (39UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL2_CLK1      (40UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL3           (41UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL3_CLK0      (42UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL4           (43UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_SRC_PLL4_CLK0      (44UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CPU0           (64UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_MCHTMR0        (65UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CPU1           (66UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_MCHTMR1        (67UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_AXI            (68UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CONN           (69UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_VIS            (70UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_AHB            (71UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_FEMC           (72UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_XPI0           (73UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_XPI1           (74UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR0         (75UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR1         (76UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR2         (77UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR3         (78UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR4         (79UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR5         (80UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR6         (81UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_GPTMR7         (82UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART0          (83UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART1          (84UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART2          (85UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART3          (86UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART4          (87UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART5          (88UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART6          (89UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART7          (90UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART8          (91UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART9          (92UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART10         (93UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART11         (94UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART12         (95UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART13         (96UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART14         (97UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_UART15         (98UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2C0           (99UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2C1           (100UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2C2           (101UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2C3           (102UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_SPI0           (103UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_SPI1           (104UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_SPI2           (105UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_SPI3           (106UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CAN0           (107UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CAN1           (108UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CAN2           (109UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CAN3           (110UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_PTPC           (111UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ANA0           (112UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ANA1           (113UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ANA2           (114UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_AUD0           (115UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_AUD1           (116UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_AUD2           (117UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_LCDC           (118UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CAM0           (119UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_CAM1           (120UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ENET0          (121UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ENET1          (122UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_PTP0           (123UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_PTP1           (124UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_REF0           (125UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_REF1           (126UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_NTMR0          (127UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_NTMR1          (128UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_SDXC0          (129UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_SDXC1          (130UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ADC0           (192UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ADC1           (193UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ADC2           (194UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_ADC3           (195UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2S0           (196UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2S1           (197UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2S2           (198UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CLK_TOP_I2S3           (199UL)
#define CHIP_SYSCTL_RESOURCE_NUM_LINKABLE_START         (256UL)
#define CHIP_SYSCTL_RESOURCE_NUM_AHBAPB_BUS             (256UL)
#define CHIP_SYSCTL_RESOURCE_NUM_AXI_BUS                (257UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CONN_BUS               (258UL)
#define CHIP_SYSCTL_RESOURCE_NUM_VIS_BUS                (259UL)
#define CHIP_SYSCTL_RESOURCE_NUM_FEMC                   (260UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ROM                    (261UL)
#define CHIP_SYSCTL_RESOURCE_NUM_LMM0                   (262UL)
#define CHIP_SYSCTL_RESOURCE_NUM_LMM1                   (263UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MCHTMR0                (264UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MCHTMR1                (265UL)
#define CHIP_SYSCTL_RESOURCE_NUM_AXI_SRAM0              (266UL)
#define CHIP_SYSCTL_RESOURCE_NUM_AXI_SRAM1              (267UL)
#define CHIP_SYSCTL_RESOURCE_NUM_XPI0                   (268UL)
#define CHIP_SYSCTL_RESOURCE_NUM_XPI1                   (269UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SDP                    (270UL)
#define CHIP_SYSCTL_RESOURCE_NUM_RNG                    (271UL)
#define CHIP_SYSCTL_RESOURCE_NUM_KEYM                   (272UL)
#define CHIP_SYSCTL_RESOURCE_NUM_HDMA                   (273UL)
#define CHIP_SYSCTL_RESOURCE_NUM_XDMA                   (274UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPIO                   (275UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MBX0                   (276UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MBX1                   (277UL)
#define CHIP_SYSCTL_RESOURCE_NUM_WDG0                   (278UL)
#define CHIP_SYSCTL_RESOURCE_NUM_WDG1                   (279UL)
#define CHIP_SYSCTL_RESOURCE_NUM_WDG2                   (280UL)
#define CHIP_SYSCTL_RESOURCE_NUM_WDG3                   (281UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR0                 (282UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR1                 (283UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR2                 (284UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR3                 (285UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR4                 (286UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR5                 (287UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR6                 (288UL)
#define CHIP_SYSCTL_RESOURCE_NUM_GPTMR7                 (289UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART0                  (290UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART1                  (291UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART2                  (292UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART3                  (293UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART4                  (294UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART5                  (295UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART6                  (296UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART7                  (297UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART8                  (298UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART9                  (299UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART10                 (300UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART11                 (301UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART12                 (302UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART13                 (303UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART14                 (304UL)
#define CHIP_SYSCTL_RESOURCE_NUM_UART15                 (305UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2C0                   (306UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2C1                   (307UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2C2                   (308UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2C3                   (309UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SPI0                   (310UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SPI1                   (311UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SPI2                   (312UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SPI3                   (313UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CAN0                   (314UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CAN1                   (315UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CAN2                   (316UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CAN3                   (317UL)
#define CHIP_SYSCTL_RESOURCE_NUM_PTPC                   (318UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ADC0                   (319UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ADC1                   (320UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ADC2                   (321UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ADC3                   (322UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ACMP                   (323UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2S0                   (324UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2S1                   (325UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2S2                   (326UL)
#define CHIP_SYSCTL_RESOURCE_NUM_I2S3                   (327UL)
#define CHIP_SYSCTL_RESOURCE_NUM_PDM                    (328UL)
#define CHIP_SYSCTL_RESOURCE_NUM_DAO                    (329UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SYNT                   (330UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MOT0                   (331UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MOT1                   (332UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MOT2                   (333UL)
#define CHIP_SYSCTL_RESOURCE_NUM_MOT3                   (334UL)
#define CHIP_SYSCTL_RESOURCE_NUM_LCDC                   (335UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CAM0                   (336UL)
#define CHIP_SYSCTL_RESOURCE_NUM_CAM1                   (337UL)
#define CHIP_SYSCTL_RESOURCE_NUM_JPEG                   (338UL)
#define CHIP_SYSCTL_RESOURCE_NUM_PDMA                   (339UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ENET0                  (340UL)
#define CHIP_SYSCTL_RESOURCE_NUM_ENET1                  (341UL)
#define CHIP_SYSCTL_RESOURCE_NUM_NTMR0                  (342UL)
#define CHIP_SYSCTL_RESOURCE_NUM_NTMR1                  (343UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SDXC0                  (344UL)
#define CHIP_SYSCTL_RESOURCE_NUM_SDXC1                  (345UL)
#define CHIP_SYSCTL_RESOURCE_NUM_USB0                   (346UL)
#define CHIP_SYSCTL_RESOURCE_NUM_USB1                   (347UL)
#define CHIP_SYSCTL_RESOURCE_NUM_REF0                   (348UL)
#define CHIP_SYSCTL_RESOURCE_NUM_REF1                   (349UL)


/* CLOCK register group index macro definition */
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CPU0              (0UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_MCHTMR0           (1UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CPU1              (2UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_MCHTMR1           (3UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_AXI               (4UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CONN              (5UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_VIS               (6UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_AHB               (7UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_FEMC              (8UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_XPI0              (9UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_XPI1              (10UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR0            (11UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR1            (12UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR2            (13UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR3            (14UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR4            (15UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR5            (16UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR6            (17UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_GPTMR7            (18UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART0             (19UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART1             (20UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART2             (21UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART3             (22UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART4             (23UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART5             (24UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART6             (25UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART7             (26UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART8             (27UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART9             (28UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART10            (29UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART11            (30UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART12            (31UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART13            (32UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART14            (33UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_UART15            (34UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_I2C0              (35UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_I2C1              (36UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_I2C2              (37UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_I2C3              (38UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_SPI0              (39UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_SPI1              (40UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_SPI2              (41UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_SPI3              (42UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CAN0              (43UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CAN1              (44UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CAN2              (45UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CAN3              (46UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_PTPC              (47UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_ANA0              (48UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_ANA1              (49UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_ANA2              (50UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_AUD0              (51UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_AUD1              (52UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_AUD2              (53UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_LCDC              (54UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CAM0              (55UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_CAM1              (56UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_ENET0             (57UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_ENET1             (58UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_PTP0              (59UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_PTP1              (60UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_REF0              (61UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_REF1              (62UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_NTMR0             (63UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_NTMR1             (64UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_SDXC0             (65UL)
#define CHIP_SYSCTL_CLOCK_NUM_CLK_TOP_SDXC1             (66UL)


/* Node retention numbers */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_SOC_POW          0 /* SOC power and memory content */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_SOC_RST          1 /* SOC peripheral and register settings */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_CPU0_POW         2 /* CPU0 power and memory content */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_CPU0_RST         3 /* CPU0 peripheral and register settings */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_CPU1_POW         4 /* CPU1 power and memory content */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_CPU1_RST         5 /* CPU1 peripheral and register settings */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_CONN_POW         6 /* CONN power and memory content */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_CONN_RST         7 /* CONN peripheral and register settings */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_VIS_POW          8 /* VIS power and memory content */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_VIS_RST          9 /* VIS peripheral and register settings */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_XTAL24M         10 /* XTAL crystal */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_PLL0            11 /* PLL0 lock */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_PLL1            12 /* PLL1 lock */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_PLL2            13 /* PLL2 lock */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_PLL3            14 /* PLL3 lock */
#define CHIP_SYSCTL_NODE_RETENTION_NUM_PLL4            15 /* PLL4 lock */


/* Monitoring clock selection values */
#define CHIP_SYSCTL_MON_CLK_32K                         0
#define CHIP_SYSCTL_MON_CLK_RC24M                       1
#define CHIP_SYSCTL_MON_CLK_XTAL24M                     2
#define CHIP_SYSCTL_MON_CLK_USB0_PHY                    3
#define CHIP_SYSCTL_MON_CLK_USB1_PHY                    4
#define CHIP_SYSCTL_MON_CLK_24M                         8
#define CHIP_SYSCTL_MON_CLK_PLL0_CLK0                   9
#define CHIP_SYSCTL_MON_CLK_PLL1_CLK0                   10
#define CHIP_SYSCTL_MON_CLK_PLL1_CLK1                   11
#define CHIP_SYSCTL_MON_CLK_PLL2_CLK0                   12
#define CHIP_SYSCTL_MON_CLK_PLL2_CLK1                   13
#define CHIP_SYSCTL_MON_CLK_PLL3_CLK0                   14
#define CHIP_SYSCTL_MON_CLK_PLL4_CLK0                   15
#define CHIP_SYSCTL_MON_CLK_TOP_CPU0                    128
#define CHIP_SYSCTL_MON_CLK_TOP_MCT0                    129
#define CHIP_SYSCTL_MON_CLK_TOP_CPU1                    130
#define CHIP_SYSCTL_MON_CLK_TOP_MCT1                    131
#define CHIP_SYSCTL_MON_CLK_TOP_AXI                     132
#define CHIP_SYSCTL_MON_CLK_TOP_VIS                     133
#define CHIP_SYSCTL_MON_CLK_TOP_CONN                    134
#define CHIP_SYSCTL_MON_CLK_TOP_AHB                     135
#define CHIP_SYSCTL_MON_CLK_TOP_DRAM                    136
#define CHIP_SYSCTL_MON_CLK_TOP_XPI0                    137
#define CHIP_SYSCTL_MON_CLK_TOP_XPI1                    138
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR0                  139
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR1                  140
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR2                  141
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR3                  142
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR4                  143
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR5                  144
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR6                  145
#define CHIP_SYSCTL_MON_CLK_TOP_GPTMR7                  146
#define CHIP_SYSCTL_MON_CLK_TOP_UART0                   147
#define CHIP_SYSCTL_MON_CLK_TOP_UART1                   148
#define CHIP_SYSCTL_MON_CLK_TOP_UART2                   149
#define CHIP_SYSCTL_MON_CLK_TOP_UART3                   150
#define CHIP_SYSCTL_MON_CLK_TOP_UART4                   151
#define CHIP_SYSCTL_MON_CLK_TOP_UART5                   152
#define CHIP_SYSCTL_MON_CLK_TOP_UART6                   153
#define CHIP_SYSCTL_MON_CLK_TOP_UART7                   154
#define CHIP_SYSCTL_MON_CLK_TOP_UART8                   155
#define CHIP_SYSCTL_MON_CLK_TOP_UART9                   156
#define CHIP_SYSCTL_MON_CLK_TOP_UART10                  157
#define CHIP_SYSCTL_MON_CLK_TOP_UART11                  158
#define CHIP_SYSCTL_MON_CLK_TOP_UART12                  159
#define CHIP_SYSCTL_MON_CLK_TOP_UART13                  160
#define CHIP_SYSCTL_MON_CLK_TOP_UART14                  161
#define CHIP_SYSCTL_MON_CLK_TOP_UART15                  162
#define CHIP_SYSCTL_MON_CLK_TOP_I2C0                    163
#define CHIP_SYSCTL_MON_CLK_TOP_I2C1                    164
#define CHIP_SYSCTL_MON_CLK_TOP_I2C2                    165
#define CHIP_SYSCTL_MON_CLK_TOP_I2C3                    166
#define CHIP_SYSCTL_MON_CLK_TOP_SPI0                    167
#define CHIP_SYSCTL_MON_CLK_TOP_SPI1                    168
#define CHIP_SYSCTL_MON_CLK_TOP_SPI2                    169
#define CHIP_SYSCTL_MON_CLK_TOP_SPI3                    170
#define CHIP_SYSCTL_MON_CLK_TOP_CAN0                    171
#define CHIP_SYSCTL_MON_CLK_TOP_CAN1                    172
#define CHIP_SYSCTL_MON_CLK_TOP_CAN2                    173
#define CHIP_SYSCTL_MON_CLK_TOP_CAN3                    174
#define CHIP_SYSCTL_MON_CLK_TOP_PTPC                    175
#define CHIP_SYSCTL_MON_CLK_TOP_ANA0                    176
#define CHIP_SYSCTL_MON_CLK_TOP_ANA1                    177
#define CHIP_SYSCTL_MON_CLK_TOP_ANA2                    178
#define CHIP_SYSCTL_MON_CLK_TOP_AUD0                    179
#define CHIP_SYSCTL_MON_CLK_TOP_AUD1                    180
#define CHIP_SYSCTL_MON_CLK_TOP_AUD2                    181
#define CHIP_SYSCTL_MON_CLK_TOP_LCDC                    182
#define CHIP_SYSCTL_MON_CLK_TOP_CAM0                    183
#define CHIP_SYSCTL_MON_CLK_TOP_CAM1                    184
#define CHIP_SYSCTL_MON_CLK_TOP_ENET0                   185
#define CHIP_SYSCTL_MON_CLK_TOP_ENET1                   186
#define CHIP_SYSCTL_MON_CLK_TOP_PTP0                    187
#define CHIP_SYSCTL_MON_CLK_TOP_PTP1                    188
#define CHIP_SYSCTL_MON_CLK_TOP_REF0                    189
#define CHIP_SYSCTL_MON_CLK_TOP_REF1                    190
#define CHIP_SYSCTL_MON_CLK_TOP_NTMR0                   191
#define CHIP_SYSCTL_MON_CLK_TOP_NTMR1                   192
#define CHIP_SYSCTL_MON_CLK_TOP_SDXC0                   193
#define CHIP_SYSCTL_MON_CLK_TOP_SDXC1                   194




/*********** RESOURCE - Resource control register *****************************/
#define CHIP_REGFLD_SYSCTL_RESOURCE_GLB_BUSY            (0x00000001UL << 31U) /* (RO) Global busy flag */
#define CHIP_REGFLD_SYSCTL_RESOURCE_LOC_BUSY            (0x00000001UL << 30U) /* (RO) Current resource busy flag */
#define CHIP_REGFLD_SYSCTL_RESOURCE_MODE                (0x00000003UL <<  0U) /* (RW) Resource management mode */
#define CHIP_REGMSK_SYSCTL_RESOURCE_RESERVED            (0x3FFFFFFCUL)

#define CHIP_REGFLDVAL_SYSCTL_RESOURCE_MODE_AUTO        0
#define CHIP_REGFLDVAL_SYSCTL_RESOURCE_MODE_ON          1
#define CHIP_REGFLDVAL_SYSCTL_RESOURCE_MODE_OFF         2

/*********** POWER_STATUS - power switch control register *********************/
#define CHIP_REGFLD_SYSCTL_POWER_STATUS_FLAG            (0x00000001UL << 31U) /* (RW1C) Power-on flag, indicating that a power-on event has occurred. */
#define CHIP_REGFLD_SYSCTL_POWER_STATUS_FLAG_WAKE       (0x00000001UL << 30U) /* (RW1C) Power off wake-up flag, indicating that a power on event has occurred again after the first power on. */
#define CHIP_REGFLD_SYSCTL_POWER_STATUS_LF_DISABLE      (0x00000001UL << 12U) /* (RO)   Low fan-out power switch control signal */
#define CHIP_REGFLD_SYSCTL_POWER_STATUS_LF_ACK          (0x00000001UL <<  8U) /* (RO)   Low fan-out power switch control feedback signal */
#define CHIP_REGMSK_SYSCTL_POWER_STATUS_RESERVED        (0x3FFFEEFFUL)

/*********** RESET_CONTROL - reset control register ***************************/
#define CHIP_REGFLD_SYSCTL_RESET_CONTROL_FLAG           (0x00000001UL << 31U) /* (RW1C) Reset flag, indicates that a reset has occurred. */
#define CHIP_REGFLD_SYSCTL_RESET_CONTROL_FLAG_WAKE      (0x00000001UL << 30U) /* (RW1C) Reset flag, indicates that a wake-up reset has occurred. */
#define CHIP_REGFLD_SYSCTL_RESET_CONTROL_HOLD           (0x00000001UL <<  4U) /* (RW)   Wait mainual release or automatically release software reset */
#define CHIP_REGFLD_SYSCTL_RESET_CONTROL_RESET          (0x00000001UL <<  0U) /* (RW)   Software reset */
#define CHIP_REGMSK_SYSCTL_RESET_CONTROL_RESERVED       (0x3FFFFFEEUL)

/*********** RESET_CONFIG - Reset configuration register **********************/
#define CHIP_REGFLD_SYSCTL_RESET_CONFIG_PRE_WAIT        (0x000000FFUL << 16U) /* (RW)  Waiting period before reset in 24M clock cycles */
#define CHIP_REGFLD_SYSCTL_RESET_CONFIG_RSTCLK_NUM      (0x000000FFUL <<  8U) /* (RW)  The number of active reset clocks, must be even */
#define CHIP_REGFLD_SYSCTL_RESET_CONFIG_POST_WAIT       (0x000000FFUL <<  0U) /* (RW)  Waiting period after reset in 24M clock cycles */
#define CHIP_REGMSK_SYSCTL_RESET_CONFIG_RESERVED        (0xFF000000UL)

/*********** RESET_COUNTER - Reset counter ************************************/
#define CHIP_REGFLD_SYSCTL_RESET_COUNTER_COUNTER        (0x000FFFFFUL <<  0U) /* (RW)  Reset countdown counter */
#define CHIP_REGMSK_SYSCTL_RESET_COUNTER_RESERVED       (0xFFF00000UL)


/*********** CLOCK - Clock setting register ***********************************/
#define CHIP_REGFLD_SYSCTL_CLOCK_GLB_BUSY               (0x00000001UL << 31U) /* (RO) Global clock busy flag. */
#define CHIP_REGFLD_SYSCTL_CLOCK_LOC_BUSY               (0x00000001UL << 30U) /* (RO) Current clock busy flag. */
#define CHIP_REGFLD_SYSCTL_CLOCK_MUX                    (0x0000000FUL <<  8U) /* (RW) Clock selection. */
#define CHIP_REGFLD_SYSCTL_CLOCK_DIV                    (0x000000FFUL <<  0U) /* (RW) Frequency division number. */
#define CHIP_REGFLD_SYSCTL_CLOCK_RESERVED               (0x3FFFF000UL)


#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_OSC0_CLK0       0
#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_PLL0_CLK0       1
#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_PLL1_CLK0       2
#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_PLL1_CLK1       3
#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_PLL2_CLK0       4
#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_PLL2_CLK1       5
#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_PLL3_CLK0       6
#define CHIP_REGFLDVAL_SYSCTL_CLOCK_MUX_PLL4_CLK0       7


/*********** ADCCLK - ADC functional clock setting ****************************/
#define CHIP_REGFLD_ADCCLK_CLOCK_GLB_BUSY               (0x00000001UL << 31U) /* (RO) Global clock busy flag. */
#define CHIP_REGFLD_ADCCLK_CLOCK_LOC_BUSY               (0x00000001UL << 30U) /* (RO) Current clock busy flag. */
#define CHIP_REGFLD_ADCCLK_CLOCK_MUX                    (0x0000000FUL <<  8U) /* (RW) Clock selection. */
#define CHIP_REGFLD_ADCCLK_CLOCK_RESERVED               (0x3FFFF0FFUL)

#define CHIP_REGFLDVAL_ADCCLK_CLOCK_MUX_AHB             0
#define CHIP_REGFLDVAL_ADCCLK_CLOCK_MUX_ADC_CLK0        1
#define CHIP_REGFLDVAL_ADCCLK_CLOCK_MUX_ADC_CLK1        2
#define CHIP_REGFLDVAL_ADCCLK_CLOCK_MUX_ADC_CLK2        3

/*********** I2SCLK - I2S functional clock setting ****************************/
#define CHIP_REGFLD_I2SCLK_CLOCK_GLB_BUSY               (0x00000001UL << 31U) /* (RO) Global clock busy flag. */
#define CHIP_REGFLD_I2SCLK_CLOCK_LOC_BUSY               (0x00000001UL << 30U) /* (RO) Current clock busy flag. */
#define CHIP_REGFLD_I2SCLK_CLOCK_MUX                    (0x0000000FUL <<  8U) /* (RW) Clock selection. */
#define CHIP_REGFLD_I2SCLK_CLOCK_RESERVED               (0x3FFFF0FFUL)

#define CHIP_REGFLDVAL_I2SCLK_CLOCK_MUX_AHB             0
#define CHIP_REGFLDVAL_I2SCLK_CLOCK_MUX_I2S_CLK0        1
#define CHIP_REGFLDVAL_I2SCLK_CLOCK_MUX_I2S_CLK1        2
#define CHIP_REGFLDVAL_I2SCLK_CLOCK_MUX_I2S_CLK2        3


/*********** MONITOR_CONTROL - Clock measure and monitor control **************/
#define CHIP_REGFLD_MONITOR_CONTROL_VALID               (0x00000001UL << 31U) /* (RW) The result is valid. */
#define CHIP_REGFLD_MONITOR_CONTROL_DIV_BUSY            (0x00000001UL << 27U) /* (RO) The frequency divider is budy (changing settings) */
#define CHIP_REGFLD_MONITOR_CONTROL_OUTEN               (0x00000001UL << 24U) /* (RW) Output enable. */
#define CHIP_REGFLD_MONITOR_CONTROL_DIV                 (0x000000FFUL << 16U) /* (RW) Output divider. */
#define CHIP_REGFLD_MONITOR_CONTROL_HIGH                (0x00000001UL << 15U) /* (RW) Monitoring frequency is higher than the upper limit. */
#define CHIP_REGFLD_MONITOR_CONTROL_LOW                 (0x00000001UL << 14U) /* (RW) Monitoring frequency is lower than the lower limit. */
#define CHIP_REGFLD_MONITOR_CONTROL_START               (0x00000001UL << 12U) /* (RW) Start measuring. */
#define CHIP_REGFLD_MONITOR_CONTROL_MODE                (0x00000001UL << 10U) /* (RW) Mode selection. */
#define CHIP_REGFLD_MONITOR_CONTROL_ACCURACY            (0x00000001UL <<  9U) /* (RW) Measurement accuracy. */
#define CHIP_REGFLD_MONITOR_CONTROL_REFERENCE           (0x00000001UL <<  8U) /* (RW) Reference clock selection. */
#define CHIP_REGFLD_MONITOR_CONTROL_SELECTION           (0x000000FFUL <<  0U) /* (RW) Measurement clock selection. */


#define CHIP_REGFLDVAL_MONITOR_CONTROL_MODE_COMP        0 /* Comparison with min and max */
#define CHIP_REGFLDVAL_MONITOR_CONTROL_MODE_REC         1 /* Recording min and max */

#define CHIP_REGFLDVAL_MONITOR_CONTROL_ACCURACY_1KHZ    0 /* Accuracy up to 1KHz */
#define CHIP_REGFLDVAL_MONITOR_CONTROL_ACCURACY_1HZ     1 /* Accuracy up to 1Hz */

#define CHIP_REGFLDVAL_MONITOR_CONTROL_REFERENCE_32K    0 /* 32KHz */
#define CHIP_REGFLDVAL_MONITOR_CONTROL_REFERENCE_24M    1 /* 24MHz */

/*********** CPU_LP - CPU low power control  **********************************/
#define CHIP_REGFLD_CPU_LP_WAKE_CNT                     (0x000000FFUL << 24U) /* (RW)   Wake up counter */
#define CHIP_REGFLD_CPU_LP_HALT                         (0x00000001UL << 16U) /* (RW)   CPU stop running */
#define CHIP_REGFLD_CPU_LP_WAKE                         (0x00000001UL << 13U) /* (RO)   Wake state */
#define CHIP_REGFLD_CPU_LP_EXEC                         (0x00000001UL << 12U) /* (RO)   Operating status (0 - Sleep, 1 - Run) */
#define CHIP_REGFLD_CPU_LP_WAKE_FLAG                    (0x00000001UL << 10U) /* (RW1C) Wake-up flag */
#define CHIP_REGFLD_CPU_LP_SLEEP_FLAG                   (0x00000001UL <<  9U) /* (RW1C) Sleep flag */
#define CHIP_REGFLD_CPU_LP_RESET_FLAG                   (0x00000001UL <<  8U) /* (RW1C) Reset flag */
#define CHIP_REGFLD_CPU_LP_MODE                         (0x00000003UL <<  0U) /* (RW)   Low power mode, set behavior after CPU WFI */
#define CHIP_REGMSK_CPU_LP_RESERVED                     (0x00FEC8FCUL)

#define CHIP_REGFLDVAL_CPU_LP_MODE_WAIT                 0  /* only turn off the core clock after WFI */
#define CHIP_REGFLDVAL_CPU_LP_MODE_STOP                 1  /* trigger the low power consumption process of the system after WFI */
#define CHIP_REGFLDVAL_CPU_LP_MODE_RUN                  2  /* unchanged after WFI */

/*********** CPU_LOCK - CPU general registers lock control ********************/
#define CHIP_REGFLD_CPU_LOCK_GPR                        (0x00003FFFUL <<  2U) /* (RW)   Lock register, each bit corresponds to a GPR*/
#define CHIP_REGFLD_CPU_LOCK_LOCK                       (0x00000001UL <<  1U) /* (RW)   Lock LOCK register */
#define CHIP_REGMSK_CPU_LOCK_RESERVED                   (0xFFFF0001UL)




#endif // REGS_SYSCTL_H
