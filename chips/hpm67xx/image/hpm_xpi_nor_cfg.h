#ifndef HPM_XPI_NOR_CFG_H
#define HPM_XPI_NOR_CFG_H

/*********** WORD0 - Header *****************************/
#define HPM_XPINORCFG_FIELD_HDR_TAG             (0x000FFFFFUL << 12U) /* Tag */
#define HPM_XPINORCFG_FIELD_HDR_WORD_CNT        (0x0000000FUL <<  0U) /* Option words count */

#define HPM_XPINORCFG_FIELDVAL_HDR_TAG          0xFCF90


/*********** WORD1 - Option 1 ***************************/



#endif // HPM_XPI_NOR_CFG_H
