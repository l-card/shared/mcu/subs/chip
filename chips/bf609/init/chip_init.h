#ifndef BF609_CHIP_INIT_H
#define BF609_CHIP_INIT_H

#include "chip_init_values.h"
#include "chip_config.h"

#define CHIP_CLKF_PLL_MAX        1000000000
#define CHIP_CLKF_PLL_MIN         25000000


#define CHIP_CLKF_CCLK_MAX        500000000

#define CHIP_CLKF_SYSCLK_MAX      250000000
#define CHIP_CLKF_SCLK_MAX        125000000
#define CHIP_CLKF_SCLK_MIN        3000000

#define CHIP_CLKF_OUTCLK_MAX      125000000

#define CHIP_CLKF_DCLK_MAX        250000000



/* полученные значения частот */
#define CHIP_CLKF_PLL      ((CHIP_CLKF_OSC)/((CHIP_PLL_DF_VAL) + 1) * (CHIP_PLL_MSEL_VAL))
#define CHIP_CLKF_CCLK     (CHIP_CLKF_PLL/CHIP_CGU_DIV_CSEL_VAL)


#define CHIP_CLKF_SYSCLK  (CHIP_CLKF_PLL/CHIP_CGU_DIV_SYSSEL_VAL)
#define CHIP_CLKF_SCLK0   (CHIP_CLKF_SYSCLK/CHIP_CGU_DIV_S0SEL_VAL)
#define CHIP_CLKF_SCLK1   (CHIP_CLKF_SYSCLK/CHIP_CGU_DIV_S1SEL_VAL)
#define CHIP_CLKF_DCLK    (CHIP_CLKF_PLL/CHIP_CGU_DIV_DSEL_VAL)
#define CHIP_CLKF_OUTCLK  (CHIP_CLKF_PLL/CHIP_CGU_DIV_OSEL_VAL)


/* значение регистра WDT, соответствующее указанному кол-ву мс */
#define CHIP_WDT_TICKS_MS(ms)    ((CHIP_CLKF_SYSCLK/1000) * (ms))



#if (CHIP_PLL_MSEL_VAL < 1) || (CHIP_PLL_MSEL_VAL > 127)
    #error MSEL value must be from 1 to 127
#endif

#if (CHIP_PLL_DF_VAL < 0) || (CHIP_PLL_DF_VAL > 1)
    #error DF must be 0 or 1
#endif

#if (CHIP_CGU_DIV_SYSSEL_VAL < 1) || (CHIP_CGU_DIV_SYSSEL_VAL > 31)
    #error SYSEL must be from 1 to 31
#endif

#ifdef CHIP_CGU_DIV_OSEL_VAL
    #define CHIP_CGU_DIV_OSEL_VAL_EN

    #if (CHIP_CGU_DIV_OSEL_VAL < 1) || (CHIP_CGU_DIV_OSEL_VAL > 127)
        #error OSEL must be from 1 to 127
    #endif
#else
    #define CHIP_CGU_DIV_OSEL_VAL 127
#endif

#ifdef CHIP_CGU_DIV_DSEL_VAL
    #if (CHIP_CGU_DIV_DSEL_VAL < 1) || (CHIP_CGU_DIV_DSEL_VAL > 31)
        #error DSEL must be from 1 to 31
    #endif
    #define CHIP_CGU_DIV_DSEL_VAL_EN
#else
    #define CHIP_CGU_DIV_DSEL_VAL 31
#endif

#ifdef CHIP_CGU_DIV_S1SEL_VAL
    #if (CHIP_CGU_DIV_S1SEL_VAL < 1) || (CHIP_CGU_DIV_S1SEL_VAL > 7)
        #error S1SEL must be from 1 to 7
    #endif
#endif

#ifdef CHIP_CGU_DIV_S0SEL_VAL
    #if (CHIP_CGU_DIV_S0SEL_VAL < 1) || (CHIP_CGU_DIV_S0SEL_VAL > 7)
        #error S0SEL must be from 1 to 7
    #endif
#endif

#if (CHIP_CGU_DIV_CSEL_VAL < 1) || (CHIP_CGU_DIV_CSEL_VAL > 31)
    #error CSEL must be from 1 to 31
#endif


#if (CHIP_CLKF_PLL < CHIP_CLKF_PLL_MIN) || (CHIP_CLKF_PLL > CHIP_CLKF_PLL_MAX)
    #error PLL frequency out of range
#endif

#if (CHIP_CLKF_CCLK > CHIP_CLKF_CCLK_MAX ) || (CHIP_CLKF_CCLK < CHIP_CLKF_SYSCLK)
    #error CCLK frequency out of range
#endif

#if (CHIP_CLKF_SYSCLK > CHIP_CLKF_SYSCLK_MAX)
    #error SYSCLK frequency out of range
#endif

#if ((CHIP_CLKF_SCLK0 < CHIP_CLKF_SCLK_MIN) || (CHIP_CLKF_SCLK0 > CHIP_CLKF_SCLK_MAX))
    #error SCLK0 frequency out of range
#endif

#if (CHIP_CLKF_SCLK1 > CHIP_CLKF_SCLK_MAX)
    #error SCLK1 frequency out of range
#endif

#if (CHIP_CLKF_DCLK > CHIP_CLKF_DCLK_MAX) || (CHIP_CLKF_DCLK > CHIP_CLKF_SYSCLK)
    #error DCLK frequency out of range
#endif

#if (CHIP_CLKF_OUTCLK > CHIP_CLKF_OUTCLK_MAX)
    #error OUTCLK requency out of range
#endif


#define CHIP_DMC_TRTP_CYCLES_MIN  5  /* минимальное значение настройки TRTP (anamaly 16000007) */







static inline void chip_wdt_clear(void) {
    *pREG_WDOG0_STAT = 0;
}

static inline int chip_dmc_init_done(void) {
    return !(*pREG_DMC0_CTL & BITM_DMC_CTL_SRREQ);
}

#endif // BF609_CHIP_INIT_H

