#ifndef CHIP_CMSIS_N32G03X_H
#define CHIP_CMSIS_N32G03X_H

/**
 * @brief Configuration of the Cortex-M0 Processor and Core Peripherals
 */
#define __CM0_REV                 0
#define __MPU_PRESENT             0
#define __NVIC_PRIO_BITS          2
#define __Vendor_SysTickConfig    0

#endif // CHIP_CMSIS_N32G03X_H
