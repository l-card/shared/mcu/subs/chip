#include "archtypes/cortexm/init/chip_cortexm_isr.h"
#include <stddef.h>

CHIP_ISR_DUMMY_DECLARE()

CHIP_ISR_DECLARE(NMI_Handler);
CHIP_ISR_DECLARE(HardFault_Handler);
CHIP_ISR_DECLARE(SVC_Handler);
CHIP_ISR_DECLARE(PendSV_Handler);
CHIP_ISR_DECLARE(SysTick_IRQHandler);
CHIP_ISR_DECLARE(WWDG_IRQHandler);
CHIP_ISR_DECLARE(PVD_VDDIO2_IRQHandler);
CHIP_ISR_DECLARE(RTC_IRQHandler);
CHIP_ISR_DECLARE(FLASH_IRQHandler);
CHIP_ISR_DECLARE(RCC_CRS_IRQHandler);
CHIP_ISR_DECLARE(EXTI0_1_IRQHandler);
CHIP_ISR_DECLARE(EXTI2_3_IRQHandler);
CHIP_ISR_DECLARE(EXTI4_15_IRQHandler);
CHIP_ISR_DECLARE(TSC_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel1_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel2_3_IRQHandler);
CHIP_ISR_DECLARE(DMA1_Channel4_5_6_7_IRQHandler);
CHIP_ISR_DECLARE(ADC1_COMP_IRQHandler);
CHIP_ISR_DECLARE(TIM1_BRK_UP_TRG_COM_IRQHandler);
CHIP_ISR_DECLARE(TIM1_CC_IRQHandler);
CHIP_ISR_DECLARE(TIM2_IRQHandler);
CHIP_ISR_DECLARE(TIM3_IRQHandler);
CHIP_ISR_DECLARE(TIM6_DAC_IRQHandler);
CHIP_ISR_DECLARE(TIM7_IRQHandler);
CHIP_ISR_DECLARE(TIM14_IRQHandler);
CHIP_ISR_DECLARE(TIM15_IRQHandler);
CHIP_ISR_DECLARE(TIM16_IRQHandler);
CHIP_ISR_DECLARE(TIM17_IRQHandler);
CHIP_ISR_DECLARE(I2C1_IRQHandler);
CHIP_ISR_DECLARE(I2C2_IRQHandler);
CHIP_ISR_DECLARE(SPI1_IRQHandler);
CHIP_ISR_DECLARE(SPI2_IRQHandler);
CHIP_ISR_DECLARE(USART1_IRQHandler);
CHIP_ISR_DECLARE(USART2_IRQHandler);
CHIP_ISR_DECLARE(USART3_4_IRQHandler);
CHIP_ISR_DECLARE(CEC_CAN_IRQHandler);
CHIP_ISR_DECLARE(USB_IRQHandler);


CHIP_ISR_TABLE() = {
    CHIP_ISR_TABLE_ENTRY_STACKPTR,
    CHIP_ISR_TABLE_ENTRY_RESETPTR,
    NMI_Handler,
    HardFault_Handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    SVC_Handler,
    NULL,
    NULL,
    PendSV_Handler,
    SysTick_IRQHandler,
    WWDG_IRQHandler,                   /* Window WatchDog              */
    PVD_VDDIO2_IRQHandler,             /* PVD and VDDIO2 through EXTI Line detect */
    RTC_IRQHandler,                    /* RTC through the EXTI line    */
    FLASH_IRQHandler,                  /* FLASH                        */
    RCC_CRS_IRQHandler,                /* RCC and CRS                  */
    EXTI0_1_IRQHandler,                /* EXTI Line 0 and 1            */
    EXTI2_3_IRQHandler,                /* EXTI Line 2 and 3            */
    EXTI4_15_IRQHandler,               /* EXTI Line 4 to 15            */
    TSC_IRQHandler,                    /* TSC                          */
    DMA1_Channel1_IRQHandler,          /* DMA1 Channel 1               */
    DMA1_Channel2_3_IRQHandler,        /* DMA1 Channel 2 and Channel 3 */
    DMA1_Channel4_5_6_7_IRQHandler,    /* DMA1 Channel 4, Channel 5, Channel 6 and Channel 7 */
    ADC1_COMP_IRQHandler,              /* ADC1, COMP1 and COMP2        */
    TIM1_BRK_UP_TRG_COM_IRQHandler,    /* TIM1 Break, Update, Trigger and Commutation */
    TIM1_CC_IRQHandler,                /* TIM1 Capture Compare         */
    TIM2_IRQHandler,                   /* TIM2                         */
    TIM3_IRQHandler,                   /* TIM3                         */
    TIM6_DAC_IRQHandler,               /* TIM6 and DAC                 */
    TIM7_IRQHandler,                   /* TIM7                         */
    TIM14_IRQHandler,                  /* TIM14                        */
    TIM15_IRQHandler,                  /* TIM15                        */
    TIM16_IRQHandler,                  /* TIM16                        */
    TIM17_IRQHandler,                  /* TIM17                        */
    I2C1_IRQHandler,                   /* I2C1                         */
    I2C2_IRQHandler,                   /* I2C2                         */
    SPI1_IRQHandler,                   /* SPI1                         */
    SPI2_IRQHandler,                   /* SPI2                         */
    USART1_IRQHandler,                 /* USART1                       */
    USART2_IRQHandler,                 /* USART2                       */
    USART3_4_IRQHandler,               /* USART3 and USART4            */
    CEC_CAN_IRQHandler,                /* CEC and CAN                  */
    USB_IRQHandler                     /* USB */
};
