#ifndef CHIP_TARGET_ATMEGA8515_H
#define CHIP_TARGET_ATMEGA8515_H

#include "chip_devtype_spec_features.h"

#include <avr/io.h>
#ifndef __ASSEMBLER__
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include "chip_pins.h"


#include "regs/regs_cmu.h"
#include "regs/regs_rcu.h"
#include "regs/regs_pwr.h"
#include "regs/regs_gpio.h"
#include "regs/regs_flash.h"
#include "regs/regs_tmr.h"
#include "regs/regs_usart.h"
#include "regs/regs_spi.h"
#include "regs/regs_twi.h"
#include "regs/regs_adc.h"
#include "regs/regs_dac.h"
#include "regs/regs_ac.h"
#include "regs/regs_udsc.h"
#include "regs/regs_guid.h"

#include "init/chip_clk.h"

#endif

#define chip_delay_clk(clk_count) __builtin_avr_delay_cycles(clk_count)
#define chip_irq_enable()         sei();
#define chip_irq_disable()        cli();
#define chip_nop()                do {__asm__ __volatile__ ("nop");} while(0)

//#include "extint.h"
//#ifndef __ASSEMBLER__
//#include "init/chip_stack_check.h"
//#endifd


#endif // CHIP_TARGET_ATMEGA8515_H
