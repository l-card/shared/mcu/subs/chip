#ifndef CHIP_CLK_DEFS_H
#define CHIP_CLK_DEFS_H

#include "chip_std_defs.h"


/* исходное значение частоты HSI */
#define CHIP_CLK_HSI_FREQ           CHIP_MHZ(16)
#define CHIP_CLK_LSI_FREQ           CHIP_KHZ(32)


#define CHIP_CLK_CPU_INITIAL_FREQ   CHIP_CLK_HSI_FREQ




/* Номера идентификаторов различных клоков, которые могут использоваться в качестве
 * источников.
 * Номера используются исключительно для уникальной идентификации
 * для сравнения настроек, сами номера должны быть уникальные,
 * но не связаны с какими-либо значениеми самого контроллера.
 * В настройках источников сигнала используются значения без суффикса _ID,
 * а уже по этомим значениям с помощью макросов CHIP_CLK_ID_VAL получается
 * ID из таблицы. Это позволяет также по определению получить параметры выбранной
 * частоты с помощью CHIP_CLK_ID_EN() и CHIP_CLK_ID_FREQ() */
#define CHIP_CLK_HSE_ID             1
#define CHIP_CLK_LSE_ID             2
#define CHIP_CLK_HSI_ID             3
#define CHIP_CLK_LSI_ID             4

#define CHIP_CLK_PLL_ID             7
#define CHIP_CLK_SYS_ID             8
#define CHIP_CLK_PCLK_ID            9
#define CHIP_CLK_APB_ID             9
#define CHIP_CLK_PLL_MCO_ID         10 /* деленный выход с PLL для подачи на MCO */
#define CHIP_CLK_HSI_DIV244_ID      11 /* деленный на 244 клок HSI для подачи на CEC */

#endif // CHIP_CLK_DEFS_H
