#ifndef REGS_COMMON_TYPES_H
#define REGS_COMMON_TYPES_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t VALUE;
    __IO uint32_t SET;
    __IO uint32_t CLEAR;
    __IO uint32_t TOGGLE;
} CHIP_REGS_BITSETCLR_T;

#endif // REGS_COMMON_TYPES_H
