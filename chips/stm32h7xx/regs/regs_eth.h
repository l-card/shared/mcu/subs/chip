#ifndef CHIP_STM32H7XX_REGS_ETH_H_
#define CHIP_STM32H7XX_REGS_ETH_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"


/* Номера Auxiliary Timestamp триггеров */
#define CHIP_ETH_PTP_ATS_TRG_TIM2_TRGO          0
#define CHIP_ETH_PTP_ATS_TRG_TIM3_TRGO          1
#define CHIP_ETH_PTP_ATS_TRG_HRTIM_DAC_TRG2     2
#define CHIP_ETH_PTP_ATS_TRG_CAN_TMP            3

/* Ethernet MAC */
typedef struct  {
    __IO uint32_t MACCR;            /* Operating mode configuration register,                       Address offset: 0x0000 */
    __IO uint32_t MACECR;           /* Extended operating mode configuration register,              Address offset: 0x0004 */
    __IO uint32_t MACPFR;           /* Packet filtering control register,                           Address offset: 0x0008 */
    __IO uint32_t MACWTR;           /* Watchdog timeout register,                                   Address offset: 0x000C */
    __IO uint32_t MACHT0R;          /* Hash Table 0 register,                                       Address offset: 0x0010 */
    __IO uint32_t MACHT1R;          /* Hash Table 1 register,                                       Address offset: 0x0014 */
    uint32_t      RESERVED1[14];
    __IO uint32_t MACVTR;           /* VLAN tag register,                                           Address offset: 0x0050 */
    uint32_t      RESERVED2;
    __IO uint32_t MACVHTR;          /* VLAN Hash table register,                                    Address offset: 0x0058 */
    uint32_t      RESERVED3;
    __IO uint32_t MACVIR;           /* VLAN inclusion register,                                     Address offset: 0x0060 */
    __IO uint32_t MACIVIR;          /* Inner VLAN inclusion register,                               Address offset: 0x0064 */
    uint32_t      RESERVED4[2];
    __IO uint32_t MACQTXFCR;        /* Tx Queue flow control register,                              Address offset: 0x0070 */
    uint32_t      RESERVED5[7];
    __IO uint32_t MACRXFCR;         /* Rx flow control register,                                    Address offset: 0x0090 */
    uint32_t      RESERVED6[7];
    __IO uint32_t MACISR;           /* Interrupt status register,                                   Address offset: 0x00B0 */
    __IO uint32_t MACIER;           /* Interrupt enable register,                                   Address offset: 0x00B4 */
    __IO uint32_t MACRXTXSR;        /* Rx Tx status register,                                       Address offset: 0x00B8 */
    uint32_t      RESERVED7;
    __IO uint32_t MACPCSR;          /* PMT control status register,                                 Address offset: 0x00C0 */
    __IO uint32_t MACRWKPFR;        /* Remote wakeup packet filter register,                        Address offset: 0x00C4 */
    uint32_t      RESERVED8[2];
    __IO uint32_t MACLCSR;          /* LPI control status register,                                 Address offset: 0x00D0 */
    __IO uint32_t MACLTCR;          /* LPI timers control register,                                 Address offset: 0x00D4 */
    __IO uint32_t MACLETR;          /* LPI entry timer register,                                    Address offset: 0x00D8 */
    __IO uint32_t MAC1USTCR;        /* 1-microsecond-tick counter register,                         Address offset: 0x00DC */
    uint32_t      RESERVED9[12];
    __IO uint32_t MACVR;            /* Version register,                                            Address offset: 0x0110 */
    __IO uint32_t MACDR;            /* Debug register,                                              Address offset: 0x0114 */
    uint32_t      RESERVED10;
    __IO uint32_t MACHWF0R;         /* HW feature 1 register,                                       Address offset: 0x0120 */
    __IO uint32_t MACHWF1R;         /* HW feature 2 register,                                       Address offset: 0x0124 */
    __IO uint32_t MACHWF2R;         /* HW feature 3 register,                                       Address offset: 0x0128 */
    uint32_t      RESERVED11[54];
    __IO uint32_t MACMDIOAR;        /* MDIO address register,                                       Address offset: 0x0200 */
    __IO uint32_t MACMDIODR;        /* MDIO data register,                                          Address offset: 0x0204 */
    uint32_t      RESERVED12[2];
    __IO uint32_t MACARPAR;         /* ARP address register,                                        Address offset: 0x0210 */
    uint32_t      RESERVED13[59];
    __IO uint32_t MACA0HR;          /* Address 0 high register,                                     Address offset: 0x0300 */
    __IO uint32_t MACA0LR;          /* Address 0 low register,                                      Address offset: 0x0304 */
    __IO uint32_t MACA1HR;          /* Address 1 high register,                                     Address offset: 0x0308 */
    __IO uint32_t MACA1LR;          /* Address 1 low register,                                      Address offset: 0x030C */
    __IO uint32_t MACA2HR;          /* Address 2 high register,                                     Address offset: 0x0310 */
    __IO uint32_t MACA2LR;          /* Address 3 low register,                                      Address offset: 0x0314 */
    __IO uint32_t MACA3HR;          /* Address 3 high register,                                     Address offset: 0x0318 */
    __IO uint32_t MACA3LR;          /* Address 3 low register,                                      Address offset: 0x031C */
    uint32_t      RESERVED14[248];
    __IO uint32_t MMCCR;            /* MMC control register,                                        Address offset: 0x0700 */
    __IO uint32_t MMCRXIR;          /* MMC Rx interrupt register,                                   Address offset: 0x0704 */
    __IO uint32_t MMCTXIR;          /* MMC Tx interrupt register,                                   Address offset: 0x0708 */
    __IO uint32_t MMCRXIMR;         /* MMC Rx interrupt mask register,                              Address offset: 0x070C */
    __IO uint32_t MMCTXIMR;         /* MMC Tx interrupt mask register,                              Address offset: 0x0710 */
    uint32_t      RESERVED15[14];
    __IO uint32_t MMCTXSCGPR;       /* Tx single collision good packets register,                   Address offset: 0x074C */
    __IO uint32_t MMCTXMCGPR;       /* Tx multiple collision good packets register,                 Address offset: 0x0750 */
    int32_t       RESERVED16[5];
    __IO uint32_t MMCTXPCGR;        /* Tx packet count good register,                               Address offset: 0x0768 */
    uint32_t      RESERVED17[10];
    __IO uint32_t MMCRXCRCEPR;      /* Rx CRC error packets register,                               Address offset: 0x0794 */
    __IO uint32_t MMCRXAEPR;        /* Rx alignment error packets register,                         Address offset: 0x0798 */
    uint32_t      RESERVED18[10];
    __IO uint32_t MMCRXUPGR;        /* Rx unicast packets good register,                            Address offset: 0x07C4 */
    uint32_t      RESERVED19[9];
    __IO uint32_t MMCTXLPIMSTR;     /* Tx LPI microsecond timer register,                           Address offset: 0x07EC */
    __IO uint32_t MMCTXLPITCR;      /* Tx LPI transition counter register,                          Address offset: 0x07F0 */
    __IO uint32_t MMCRXLPIMSTR;     /* Rx LPI microsecond counter register,                         Address offset: 0x07F4 */
    __IO uint32_t MMCRXLPITCR;      /* Rx LPI transition counter register,                          Address offset: 0x07F8 */
    uint32_t      RESERVED20[65];
    __IO uint32_t MACL3L4C0R;       /* L3 and L4 control 0 register,                                Address offset: 0x0900 */
    __IO uint32_t MACL4A0R;         /* Layer4 address filter 0 register,                            Address offset: 0x0904 */
    uint32_t      RESERVED21[2];
    __IO uint32_t MACL3A00R;        /* Layer 3 Address 0 filter 0 register,                         Address offset: 0x0910 */
    __IO uint32_t MACL3A10R;        /* Layer 3 Address 1 filter 0 register,                         Address offset: 0x0914 */
    __IO uint32_t MACL3A20R;        /* Layer 3 Address 2 filter 0 register,                         Address offset: 0x0918 */
    __IO uint32_t MACL3A30R;        /* Layer 3 Address 3 filter 0 register,                         Address offset: 0x091C */
    uint32_t      RESERVED22[4];
    __IO uint32_t MACL3L4C1R;       /* L3 and L4 control 1 register,                                Address offset: 0x0930 */
    __IO uint32_t MACL4A1R;         /* Layer 4 address filter 1 register,                           Address offset: 0x0934 */
    uint32_t      RESERVED23[2];
    __IO uint32_t MACL3A01R;        /* Layer3 address 0 filter 1 Register,                          Address offset: 0x0940 */
    __IO uint32_t MACL3A11R;        /* Layer3 address 1 filter 1 Register,                          Address offset: 0x0944 */
    __IO uint32_t MACL3A21R;        /* Layer3 address 2 filter 1 Register,                          Address offset: 0x0948 */
    __IO uint32_t MACL3A31R;        /* Layer3 address 3 filter 1 Register,                          Address offset: 0x094C */
    uint32_t      RESERVED24[108];
    __IO uint32_t MACTSCR;          /* Timestamp control Register,                                  Address offset: 0x0B00 */
    __IO uint32_t MACSSIR;          /* Sub-second increment register,                               Address offset: 0x0B04 */
    __IO uint32_t MACSTSR;          /* System time seconds register,                                Address offset: 0x0B08 */
    __IO uint32_t MACSTNR;          /* System time nanoseconds register,                            Address offset: 0x0B0C */
    __IO uint32_t MACSTSUR;         /* System time seconds update register,                         Address offset: 0x0B10 */
    __IO uint32_t MACSTNUR;         /* System time nanoseconds update register,                     Address offset: 0x0B14 */
    __IO uint32_t MACTSAR;          /* Timestamp addend register,                                   Address offset: 0x0B18 */
    uint32_t      RESERVED25;
    __IO uint32_t MACTSSR;          /* Timestamp status register,                                   Address offset: 0x0B20 */
    uint32_t      RESERVED26[3];
    __IO uint32_t MACTXTSSNR;       /* Tx timestamp status nanoseconds register,                    Address offset: 0x0B30 */
    __IO uint32_t MACTXTSSSR;       /* Tx timestamp status seconds register,                        Address offset: 0x0B34 */
    uint32_t      RESERVED27[2];
    __IO uint32_t MACACR;           /* Auxiliary control register,                                  Address offset: 0x0B40 */
    uint32_t      RESERVED28;
    __IO uint32_t MACATSNR;         /* Auxiliary timestamp nanoseconds register,                    Address offset: 0x0B48 */
    __IO uint32_t MACATSSR;         /* Auxiliary timestamp seconds register,                        Address offset: 0x0B4C */
    __IO uint32_t MACTSIACR;        /* Timestamp Ingress asymmetric correction register,            Address offset: 0x0B50 */
    __IO uint32_t MACTSEACR;        /* Timestamp Egress asymmetric correction register,             Address offset: 0x0B54 */
    __IO uint32_t MACTSICNR;        /* Timestamp Ingress correction nanosecond register,            Address offset: 0x0B58 */
    __IO uint32_t MACTSECNR;        /* Timestamp Egress correction nanosecond register,             Address offset: 0x0B5C */
    uint32_t      RESERVED29[4];
    __IO uint32_t MACPPSCR;         /* PPS control register,                                        Address offset: 0x0B70 */
    uint32_t      RESERVED30[3];
    __IO uint32_t MACPPSTTSR;       /* PPS target time seconds register,                            Address offset: 0x0B80 */
    __IO uint32_t MACPPSTTNR;       /* PPS target time nanoseconds register,                        Address offset: 0x0B84 */
    __IO uint32_t MACPPSIR;         /* PPS interval register,                                       Address offset: 0x0B88 */
    __IO uint32_t MACPPSWR;         /* PPS width register,                                          Address offset: 0x0B8C */
    uint32_t      RESERVED31[12];
    __IO uint32_t MACPOCR;          /* PTP Offload control register,                                Address offset: 0x0BC0 */
    __IO uint32_t MACSPI0R;         /* PTP Source Port Identity 0 Register,                         Address offset: 0x0BC4 */
    __IO uint32_t MACSPI1R;         /* PTP Source Port Identity 1 Register,                         Address offset: 0x0BC8 */
    __IO uint32_t MACSPI2R;         /* PTP Source Port Identity 2 Register,                         Address offset: 0x0BCC */
    __IO uint32_t MACLMIR;          /* Log message interval register,                               Address offset: 0x0BD0 */
    uint32_t      RESERVED32[11];
    __IO uint32_t MTLOMR;           /* Operating mode Register,                                     Address offset: 0x0C00 */
    uint32_t      RESERVED33[7];
    __IO uint32_t MTLISR;           /* Interrupt status Register,                                   Address offset: 0x0C20 */
    uint32_t      RESERVED34[55];
    __IO uint32_t MTLTXQOMR;        /* Tx queue operating mode register,                            Address offset: 0x0D00 */
    __IO uint32_t MTLTXQUR;         /* Tx queue underflow register,                                 Address offset: 0x0D04 */
    __IO uint32_t MTLTXQDR;         /* Tx queue debug register,                                     Address offset: 0x0D08 */
    uint32_t      RESERVED35[8];
    __IO uint32_t MTLQICSR;         /* Queue interrupt control status register,                     Address offset: 0x0D2C */
    __IO uint32_t MTLRXQOMR;        /* Rx queue operating mode register,                            Address offset: 0x0D30 */
    __IO uint32_t MTLRXQMPOCR;      /* Rx queue missed packet and overflow counter register,        Address offset: 0x0D34 */
    __IO uint32_t MTLRXQDR;         /* Rx queue debug register,                                     Address offset: 0x0D38 */
    uint32_t      RESERVED36[177];
    __IO uint32_t DMAMR;            /* DMA mode register,                                           Address offset: 0x1000 */
    __IO uint32_t DMASBMR;          /* System bus mode register,                                    Address offset: 0x1004 */
    __IO uint32_t DMAISR;           /* Interrupt status register,                                   Address offset: 0x1008 */
    __IO uint32_t DMADSR;           /* Debug status register,                                       Address offset: 0x100C */
    uint32_t      RESERVED37[60];
    __IO uint32_t DMACCR;           /* Channel control register,                                    Address offset: 0x1100 */
    __IO uint32_t DMACTXCR;         /* Channel transmit control register,                           Address offset: 0x1104 */
    __IO uint32_t DMACRXCR;         /* Channel receive control register,                            Address offset: 0x1108 */
    uint32_t      RESERVED38[2];
    __IO uint32_t DMACTXDLAR;       /* Channel Tx descriptor list address register,                 Address offset: 0x1114 */
    uint32_t      RESERVED39;
    __IO uint32_t DMACRXDLAR;       /* Channel Rx descriptor list address register,                 Address offset: 0x111C */
    __IO uint32_t DMACTXDTPR;       /* Channel Tx descriptor tail pointer register,                 Address offset: 0x1120 */
    uint32_t      RESERVED40;
    __IO uint32_t DMACRXDTPR;       /* Channel Rx descriptor tail pointer register,                 Address offset: 0x1128 */
    __IO uint32_t DMACTXRLR;        /* Channel Tx descriptor ring length register,                  Address offset: 0x112C */
    __IO uint32_t DMACRXRLR;        /* Channel Rx descriptor ring length register,                  Address offset: 0x1130 */
    __IO uint32_t DMACIER;          /* Channel interrupt enable register,                           Address offset: 0x1134 */
    __IO uint32_t DMACRXIWTR;       /* Channel Rx interrupt watchdog timer register,                Address offset: 0x1138 */
    __IO uint32_t DMACSFCSR;
    uint32_t      RESERVED41;
    __IO uint32_t DMACCATXDR;       /* Channel current application transmit descriptor register,    Address offset: 0x1144 */
    uint32_t      RESERVED42;
    __IO uint32_t DMACCARXDR;       /* Channel current application receive descriptor register,     Address offset: 0x114C */
    uint32_t      RESERVED43;
    __IO uint32_t DMACCATXBR;       /* Channel current application transmit buffer register,        Address offset: 0x1154 */
    uint32_t      RESERVED44;
    __IO uint32_t DMACCARXBR;       /* Channel current application receive buffer register,         Address offset: 0x115C */
    __IO uint32_t DMACSR;           /* Channel status register,                                     Address offset: 0x1160 */
    uint32_t      RESERVED45[2];
    __IO uint32_t DMACMFCR;         /* Channel missed frame count register,                         Address offset: 0x116C */
} CHIP_REGS_ETH_T;

#define CHIP_REGS_ETH          ((CHIP_REGS_ETH_T *) CHIP_MEMRGN_ADDR_PERIPH_ETH)


/*********** Operating mode configuration register (ETH_MACCR) ****************/
#define CHIP_REGFLD_ETH_MACCR_RE                    (0x00000001UL <<  0U) /* (RW) Receiver enable */
#define CHIP_REGFLD_ETH_MACCR_TE                    (0x00000001UL <<  1U) /* (RW) Transmitter enable */
#define CHIP_REGFLD_ETH_MACCR_PRELEN                (0x00000003UL <<  2U) /* (RW) Preamble Length for Transmit packets */
#define CHIP_REGFLD_ETH_MACCR_DC                    (0x00000001UL <<  4U) /* (RW) Defferal check */
#define CHIP_REGFLD_ETH_MACCR_BL                    (0x00000003UL <<  5U) /* (RW) Back-off limit mask */
#define CHIP_REGFLD_ETH_MACCR_DR                    (0x00000001UL <<  8U) /* (RW) Disable Retry */
#define CHIP_REGFLD_ETH_MACCR_DCRS                  (0x00000001UL <<  9U) /* (RW) Disable Carrier Sense During Transmission */
#define CHIP_REGFLD_ETH_MACCR_DO                    (0x00000001UL << 10U) /* (RW) Disable Receive own  */
#define CHIP_REGFLD_ETH_MACCR_ECRSFD                (0x00000001UL << 11U) /* (RW) Enable Carrier Sense Before Transmission in Full-Duplex Mode */
#define CHIP_REGFLD_ETH_MACCR_LM                    (0x00000001UL << 12U) /* (RW) Loopback mode */
#define CHIP_REGFLD_ETH_MACCR_DM                    (0x00000001UL << 13U) /* (RW) Duplex mode */
#define CHIP_REGFLD_ETH_MACCR_FES                   (0x00000001UL << 14U) /* (RW) Fast ethernet speed */
#define CHIP_REGFLD_ETH_MACCR_JE                    (0x00000001UL << 16U) /* (RW) Jumbo Packet Enable */
#define CHIP_REGFLD_ETH_MACCR_JD                    (0x00000001UL << 17U) /* (RW) Jabber disable */
#define CHIP_REGFLD_ETH_MACCR_WD                    (0x00000001UL << 19U) /* (RW) Watchdog disable */
#define CHIP_REGFLD_ETH_MACCR_ACS                   (0x00000001UL << 20U) /* (RW) Automatic Pad or CRC Stripping */
#define CHIP_REGFLD_ETH_MACCR_CST                   (0x00000001UL << 21U) /* (RW) CRC stripping for Type packets */
#define CHIP_REGFLD_ETH_MACCR_S2KP                  (0x00000001UL << 22U) /* (RW) IEEE 802.3as Support for 2K Packets */
#define CHIP_REGFLD_ETH_MACCR_GPSLCE                (0x00000001UL << 23U) /* (RW) Giant Packet Size Limit Control Enable */
#define CHIP_REGFLD_ETH_MACCR_IPG                   (0x00000007UL << 24U) /* (RW) Inter-Packet Gap */
#define CHIP_REGFLD_ETH_MACCR_IPC                   (0x00000001UL << 27U) /* (RW) Checksum Offload */
#define CHIP_REGFLD_ETH_MACCR_SARC                  (0x00000007UL << 28U) /* (RW) Source Address Insertion or Replacement Control */
#define CHIP_REGFLD_ETH_MACCR_ARP                   (0x00000001UL << 31U) /* (RW) ARP Offload Enable */
#define CHIP_REGMSK_ETH_MACCR_RESERVED              (0x00048080UL)

#define CHIP_REGFLDVAL_ETH_MACCR_PRELEN_7           0
#define CHIP_REGFLDVAL_ETH_MACCR_PRELEN_5           1
#define CHIP_REGFLDVAL_ETH_MACCR_PRELEN_3           2

#define CHIP_REGFLDVAL_ETH_MACCR_BL_10              0
#define CHIP_REGFLDVAL_ETH_MACCR_BL_8               1
#define CHIP_REGFLDVAL_ETH_MACCR_BL_4               2
#define CHIP_REGFLDVAL_ETH_MACCR_BL_1               3

#define CHIP_REGFLDVAL_ETH_MACCR_IPG_96BIT          0 /* Minimum IFG between Packets during transmission is 96Bit */
#define CHIP_REGFLDVAL_ETH_MACCR_IPG_88BIT          1 /* Minimum IFG between Packets during transmission is 88Bit */
#define CHIP_REGFLDVAL_ETH_MACCR_IPG_80BIT          2 /* Minimum IFG between Packets during transmission is 80Bit */
#define CHIP_REGFLDVAL_ETH_MACCR_IPG_72BIT          3 /* Minimum IFG between Packets during transmission is 72Bit */
#define CHIP_REGFLDVAL_ETH_MACCR_IPG_64BIT          4 /* Minimum IFG between Packets during transmission is 64Bit */
#define CHIP_REGFLDVAL_ETH_MACCR_IPG_56BIT          5 /* Minimum IFG between Packets during transmission is 56Bit */
#define CHIP_REGFLDVAL_ETH_MACCR_IPG_48BIT          6 /* Minimum IFG between Packets during transmission is 48Bit */
#define CHIP_REGFLDVAL_ETH_MACCR_IPG_40BIT          7 /* Minimum IFG between Packets during transmission is 40Bit */

/*********** Extended operating mode configuration register (ETH_MACECR) ******/
#define CHIP_REGFLD_ETH_MACECR_GPSL                 (0x00003FFFUL <<  0U) /* (RW) Giant Packet Size Limit */
#define CHIP_REGFLD_ETH_MACECR_DCRCC                (0x00000001UL << 16U) /* (RW) Disable CRC Checking for Received Packets */
#define CHIP_REGFLD_ETH_MACECR_SPEN                 (0x00000001UL << 17U) /* (RW) Slow Protocol Detection Enable */
#define CHIP_REGFLD_ETH_MACECR_USP                  (0x00000001UL << 18U) /* (RW) Unicast Slow Protocol Packet Detect */
#define CHIP_REGFLD_ETH_MACECR_EIPGEN               (0x00000001UL << 24U) /* (RW) Extended Inter-Packet Gap Enable */
#define CHIP_REGFLD_ETH_MACECR_EIPG                 (0x0000001FUL << 25U) /* (RW) Extended Inter-Packet Gap */
#define CHIP_REGMSK_ETH_MACECR_RESERVED             (0xC0F8C000UL)

/*********** Packet filtering control register (ETH_MACPFR) *******************/
#define CHIP_REGFLD_ETH_MACPFR_PR                   (0x00000001UL <<  0U) /* (RW) Promiscuous mode */
#define CHIP_REGFLD_ETH_MACPFR_HUC                  (0x00000001UL <<  1U) /* (RW) Hash unicast */
#define CHIP_REGFLD_ETH_MACPFR_HMC                  (0x00000001UL <<  2U) /* (RW) Hash multicast */
#define CHIP_REGFLD_ETH_MACPFR_DAIF                 (0x00000001UL <<  3U) /* (RW) DA Inverse filtering */
#define CHIP_REGFLD_ETH_MACPFR_PM                   (0x00000001UL <<  4U) /* (RW) Pass all mutlicast */
#define CHIP_REGFLD_ETH_MACPFR_DBF                  (0x00000001UL <<  5U) /* (RW) Disable Broadcast Packets */
#define CHIP_REGFLD_ETH_MACPFR_PCF                  (0x00000003UL <<  6U) /* (RW) Pass control frames: 4 cases */
#define CHIP_REGFLD_ETH_MACPFR_SAIF                 (0x00000001UL <<  8U) /* (RW) SA inverse filtering */
#define CHIP_REGFLD_ETH_MACPFR_SAF                  (0x00000001UL <<  9U) /* (RW) Source address filter enable */
#define CHIP_REGFLD_ETH_MACPFR_HPF                  (0x00000001UL << 10U) /* (RW) Hash or perfect filter */
#define CHIP_REGFLD_ETH_MACPFR_VTFE                 (0x00000001UL << 16U) /* (RW) VLAN Tag Filter Enable */
#define CHIP_REGFLD_ETH_MACPFR_IPFE                 (0x00000001UL << 20U) /* (RW) Layer 3 and Layer 4 Filter Enable */
#define CHIP_REGFLD_ETH_MACPFR_DNTU                 (0x00000001UL << 21U) /* (RW) Drop Non-TCP/UDP over IP Packets */
#define CHIP_REGFLD_ETH_MACPFR_RA                   (0x00000001UL << 31U) /* (RW) Receive all */
#define CHIP_REGMSK_ETH_MACPFR_RESERVED             (0x7FCEF800UL)

#define CHIP_REGFLDVAL_ETH_MACPFR_PCF_FLT_ALL           0 /* MAC filters all control frames from reaching the application */
#define CHIP_REGFLDVAL_ETH_MACPFR_PCF_FWD_ALL_NO_PAUSE  1 /* MAC forwards all control frames except Pause packets to application even if they fail the Address Filter */
#define CHIP_REGFLDVAL_ETH_MACPFR_PCF_FWD_ALL           2 /* MAC forwards all control frames to application even if they fail the Address Filter */
#define CHIP_REGFLDVAL_ETH_MACPFR_PCF_FWD_PASS          3 /* MAC forwards control frames that pass the Address Filter. */

/*********** Watchdog timeout register (ETH_MACWTR) ***************************/
#define CHIP_REGFLD_ETH_MACWTR_WTO                  (0x0000000FUL <<  0U) /* (RW) Watchdog Timeout */
#define CHIP_REGFLD_ETH_MACWTR_PWE                  (0x00000001UL <<  8U) /* (RW) Programmable Watchdog Enable */
#define CHIP_REGMSK_ETH_MACWTR_RESERVED             (0xFFFFFEF0UL)

#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_2KB           0 /* Maximum received packet length 2KB*/
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_3KB           1 /* Maximum received packet length 3KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_4KB           2 /* Maximum received packet length 4KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_5KB           3 /* Maximum received packet length 5KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_6KB           4 /* Maximum received packet length 6KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_7KB           5 /* Maximum received packet length 7KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_8KB           6 /* Maximum received packet length 8KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_9KB           7 /* Maximum received packet length 9KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_10KB          8 /* Maximum received packet length 10KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_11KB          9 /* Maximum received packet length 11KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_12KB          10 /* Maximum received packet length 12KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_13KB          11 /* Maximum received packet length 13KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_14KB          12 /* Maximum received packet length 14KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_15KB          13 /* Maximum received packet length 15KB */
#define CHIP_REGFLDVAL_ETH_MACWTR_WTO_16KB          14 /* Maximum received packet length 16KB */

/*********** Hash Table 0 register (ETH_MACHT0R) ******************************/
#define CHIP_REGFLD_ETH_MACHTHR_HTH                 (0xFFFFFFFFUL <<  0U) /* (RW) Hash table high */

/*********** Hash Table 1 register (ETH_MACHT1R) ******************************/
#define CHIP_REGFLD_ETH_MACHTLR_HTL                 (0xFFFFFFFFUL <<  0U) /* (RW) Hash table low */

/*********** VLAN tag register (ETH_MACVTR) ***********************************/
#define CHIP_REGFLD_ETH_MACVTR_VL                   (0x0000FFFFUL <<  0U) /* (RW) VLAN Tag Identifier for Receive Packets */
#define CHIP_REGFLD_ETH_MACVTR_ETV                  (0x00000001UL << 16U) /* (RW) Enable 12-Bit VLAN Tag Comparison */
#define CHIP_REGFLD_ETH_MACVTR_VTIM                 (0x00000001UL << 17U) /* (RW) VLAN Tag Inverse Match Enable */
#define CHIP_REGFLD_ETH_MACVTR_ESVL                 (0x00000001UL << 18U) /* (RW) Enable S-VLAN */
#define CHIP_REGFLD_ETH_MACVTR_ERSVLM               (0x00000001UL << 19U) /* (RW) Enable Receive S-VLAN Match */
#define CHIP_REGFLD_ETH_MACVTR_DOVLTC               (0x00000001UL << 20U) /* (RW) Disable VLAN Type Check */
#define CHIP_REGFLD_ETH_MACVTR_EVLS                 (0x00000003UL << 21U) /* (RW) Enable VLAN Tag Stripping on Receive */
#define CHIP_REGFLD_ETH_MACVTR_EVLRXS               (0x00000001UL << 24U) /* (RW) Enable VLAN Tag in Rx status */
#define CHIP_REGFLD_ETH_MACVTR_VTHM                 (0x00000001UL << 25U) /* (RW) VLAN Tag Hash Table Match Enable */
#define CHIP_REGFLD_ETH_MACVTR_EDVLP                (0x00000001UL << 26U) /* (RW) Enable Double VLAN Processing */
#define CHIP_REGFLD_ETH_MACVTR_ERIVLT               (0x00000001UL << 27U) /* (RW) Enable Inner VLAN Tag */
#define CHIP_REGFLD_ETH_MACVTR_EIVLS                (0x00000003UL << 28U) /* (RW) Enable Inner VLAN Tag Stripping on Receive */
#define CHIP_REGFLD_ETH_MACVTR_EIVLRXS              (0x00000001UL << 31U) /* (RW) Enable Inner VLAN Tag in Rx Status */
#define CHIP_REGMSK_ETH_MACVTR_RESERVED             (0x40800000UL)

#define CHIP_REGFLDVAL_ETH_MACVTR_EIVLS_NOSTRIP     0 /* Do not strip */
#define CHIP_REGFLDVAL_ETH_MACVTR_EIVLS_STRIP_PASS  1 /* Strip if VLAN filter passes */
#define CHIP_REGFLDVAL_ETH_MACVTR_EIVLS_STRIP_FAIL  2 /* Strip if VLAN filter fails */
#define CHIP_REGFLDVAL_ETH_MACVTR_EIVLS_STRIP_ALL   3 /* Always strip */

#define CHIP_REGFLDVAL_ETH_MACVTR_EVLS_NOSTRIP      0 /* Do not strip */
#define CHIP_REGFLDVAL_ETH_MACVTR_EVLS_STRIP_PASS   1 /* Strip if VLAN filter passes */
#define CHIP_REGFLDVAL_ETH_MACVTR_EVLS_STRIP_FAIL   2 /* Strip if VLAN filter fails */
#define CHIP_REGFLDVAL_ETH_MACVTR_EVLS_STRIP_ALL    3 /* Always strip */

/*********** VLAN Hash table register (ETH_MACVHTR) ***************************/
#define CHIP_REGFLD_ETH_MACVHTR_VLHT                (0x0000FFFFUL <<  0U) /* (RW) VLAN Hash Table */
#define CHIP_REGMSK_ETH_MACVHTR_RESERVED            (0xFFFF0000UL)

/*********** VLAN inclusion register (ETH_MACVIR) *****************************/
#define CHIP_REGFLD_ETH_MACVIR_VLT                  (0x0000FFFFUL <<  0U) /* (RW) VLAN Tag for Transmit Packets */
#define CHIP_REGFLD_ETH_MACVIR_VLC                  (0x00000003UL << 16U) /* (RW) VLAN Tag Control in Transmit Packets */
#define CHIP_REGFLD_ETH_MACVIR_VLP                  (0x00000001UL << 18U) /* (RW) VLAN Priority Control */
#define CHIP_REGFLD_ETH_MACVIR_CSVL                 (0x00000001UL << 19U) /* (RW) C-VLAN or S-VLAN */
#define CHIP_REGFLD_ETH_MACVIR_VLTI                 (0x00000001UL << 20U) /* (RW) VLAN Tag Input */
#define CHIP_REGMSK_ETH_MACVIR_RESERVED             (0xFFE00000UL)

#define CHIP_REGFLDVAL_ETH_MACVIR_VLC_NOOP          0 /* No VLAN tag deletion, insertion, or replacement */
#define CHIP_REGFLDVAL_ETH_MACVIR_VLC_DEL           1 /* VLAN tag deletion */
#define CHIP_REGFLDVAL_ETH_MACVIR_VLC_INS           2 /* VLAN tag insertion */
#define CHIP_REGFLDVAL_ETH_MACVIR_VLC_REPL          3 /* VLAN tag replacement */

/*********** Inner VLAN inclusion register (ETH_MACIVIR) **********************/
#define CHIP_REGFLD_ETH_MACIVIR_VLT                 (0x0000FFFFUL <<  0U) /* (RW) VLAN Tag for Transmit Packets */
#define CHIP_REGFLD_ETH_MACIVIR_VLC                 (0x00000001UL << 16U) /* (RW) VLAN Tag Control in Transmit Packets */
#define CHIP_REGFLD_ETH_MACIVIR_VLP                 (0x00000001UL << 18U) /* (RW) VLAN Priority Control */
#define CHIP_REGFLD_ETH_MACIVIR_CSVL                (0x00000001UL << 19U) /* (RW) C-VLAN or S-VLAN */
#define CHIP_REGFLD_ETH_MACIVIR_VLTI                (0x00000001UL << 20U) /* (RW) VLAN Tag Input */
#define CHIP_REGMSK_ETH_MACIVIR_RESERVED            (0xFFE00000UL)

#define CHIP_REGFLDVAL_ETH_MACIVIR_VLC_NOOP         0 /* No VLAN tag deletion, insertion, or replacement */
#define CHIP_REGFLDVAL_ETH_MACIVIR_VLC_DEL          1 /* VLAN tag deletion */
#define CHIP_REGFLDVAL_ETH_MACIVIR_VLC_INS          2 /* VLAN tag insertion */
#define CHIP_REGFLDVAL_ETH_MACIVIR_VLC_REPL         3 /* VLAN tag replacement */


/*********** Tx Queue flow control register (ETH_MACQTXFCR) *******************/
#define CHIP_REGFLD_ETH_MACQTXFCR_FCB               (0x00000001UL <<  0U) /* (RW) Flow Control Busy or Backpressure Activate */
#define CHIP_REGFLD_ETH_MACQTXFCR_TFE               (0x00000001UL <<  1U) /* (RW) Transmit Flow Control Enable */
#define CHIP_REGFLD_ETH_MACQTXFCR_PLT               (0x00000007UL <<  4U) /* (RW) Pause Low Threshold */
#define CHIP_REGFLD_ETH_MACQTXFCR_DZPQ              (0x00000001UL <<  7U) /* (RW) Disable Zero-Quanta Pause */
#define CHIP_REGFLD_ETH_MACQTXFCR_PT                (0x0000FFFFUL << 16U) /* (RW) Pause Time */
#define CHIP_REGMSK_ETH_MACQTXFCR_RESERVED          (0x0000FF0CUL)

#define CHIP_REGFLDVAL_ETH_MACQTXFCR_PLT_N4         0 /* Pause time minus 4 slot times */
#define CHIP_REGFLDVAL_ETH_MACQTXFCR_PLT_N28        1 /* Pause time minus 28 slot times */
#define CHIP_REGFLDVAL_ETH_MACQTXFCR_PLT_N36        2 /* Pause time minus 36 slot times */
#define CHIP_REGFLDVAL_ETH_MACQTXFCR_PLT_N144       3 /* Pause time minus 144 slot times */
#define CHIP_REGFLDVAL_ETH_MACQTXFCR_PLT_N256       4 /* Pause time minus 256 slot times */
#define CHIP_REGFLDVAL_ETH_MACQTXFCR_PLT_N512       5 /* Pause time minus 512 slot times */

/*********** Rx flow control register (ETH_MACRXFCR) **************************/
#define CHIP_REGFLD_ETH_MACRXFCR_RFE                (0x00000001UL <<  0U) /* (RW) Receive Flow Control Enable */
#define CHIP_REGFLD_ETH_MACRXFCR_UP                 (0x00000001UL <<  1U) /* (RW) Unicast Pause Packet Detect */
#define CHIP_REGMSK_ETH_MACRXFCR_RESERVED           (0xFFFFFFFCUL)

/*********** Interrupt status register (ETH_MACISR) ***************************/
#define CHIP_REGFLD_ETH_MACISR_PHYIS                (0x00000001UL <<  3U) /* (RO) PHY Interrupt */
#define CHIP_REGFLD_ETH_MACISR_PMTIS                (0x00000001UL <<  4U) /* (RO) PMT Interrupt Status */
#define CHIP_REGFLD_ETH_MACISR_LPIIS                (0x00000001UL <<  5U) /* (RO) LPI Interrupt Status */
#define CHIP_REGFLD_ETH_MACISR_MMCIS                (0x00000001UL <<  8U) /* (RO) MMC Interrupt Status */
#define CHIP_REGFLD_ETH_MACISR_MMCRXIS              (0x00000001UL <<  9U) /* (RO) MMC Receive Interrupt Status */
#define CHIP_REGFLD_ETH_MACISR_MMCTXIS              (0x00000001UL << 10U) /* (RO) MMC Transmit Interrupt Status */
#define CHIP_REGFLD_ETH_MACISR_TSIS                 (0x00000001UL << 12U) /* (RO) Timestamp Interrupt Status */
#define CHIP_REGFLD_ETH_MACISR_TXSTSIS              (0x00000001UL << 13U) /* (RO) Transmit Status Interrupt */
#define CHIP_REGFLD_ETH_MACISR_RXSTSIS              (0x00000001UL << 14U) /* (RO) Receive Status Interrupt */

/*********** Interrupt enable register (ETH_MACIER) ***************************/
#define CHIP_REGFLD_ETH_MACIER_PHYIE                (0x00000001UL <<  3U) /* (RW) PHY Interrupt Enable */
#define CHIP_REGFLD_ETH_MACIER_PMTIE                (0x00000001UL <<  4U) /* (RW) PMT Interrupt Enable */
#define CHIP_REGFLD_ETH_MACIER_LPIIE                (0x00000001UL <<  5U) /* (RW) LPI Interrupt Enable */
#define CHIP_REGFLD_ETH_MACIER_TSIE                 (0x00000001UL << 12U) /* (RW) Timestamp Interrupt Enable */
#define CHIP_REGFLD_ETH_MACIER_TXSTSIE              (0x00000001UL << 13U) /* (RW) Transmit Status Interrupt Enable */
#define CHIP_REGFLD_ETH_MACIER_RXSTSIE              (0x00000001UL << 14U) /* (RW) Receive Status Interrupt Enable */
#define CHIP_REGMSK_ETH_MACIER_RESERVED             (0xFFFF8FC7UL)

/*********** Rx Tx status register (ETH_MACRXTXSR) ****************************/
#define CHIP_REGFLD_ETH_MACRXTXSR_TJT               (0x00000001UL <<  0U) /* (RO) Transmit Jabber Timeout */
#define CHIP_REGFLD_ETH_MACRXTXSR_NCARR             (0x00000001UL <<  1U) /* (RO) No Carrier */
#define CHIP_REGFLD_ETH_MACRXTXSR_LCARR             (0x00000001UL <<  2U) /* (RO) Loss of Carrier */
#define CHIP_REGFLD_ETH_MACRXTXSR_EXDEF             (0x00000001UL <<  3U) /* (RO) Excessive Deferral */
#define CHIP_REGFLD_ETH_MACRXTXSR_LCOL              (0x00000001UL <<  4U) /* (RO) Late Collision */
#define CHIP_REGFLD_ETH_MACRXTXSR_EXCOL             (0x00000001UL <<  5U) /* (RO) Excessive Collisions */
#define CHIP_REGFLD_ETH_MACRXTXSR_RWT               (0x00000001UL <<  8U) /* (RO) Receive Watchdog Timeout */

/*********** PMT control status register (ETH_MACPCSR) ************************/
#define CHIP_REGFLD_ETH_MACPCSR_PWRDWN              (0x00000001UL <<  0U) /* (RW) Power Down */
#define CHIP_REGFLD_ETH_MACPCSR_MGKPKTEN            (0x00000001UL <<  1U) /* (RW) Magic Packet Enable */
#define CHIP_REGFLD_ETH_MACPCSR_RWKPKTEN            (0x00000001UL <<  2U) /* (RW) Remote Wake-Up Packet Enable */
#define CHIP_REGFLD_ETH_MACPCSR_MGKPRCVD            (0x00000001UL <<  5U) /* (RO) Magic Packet Received */
#define CHIP_REGFLD_ETH_MACPCSR_RWKPRCVD            (0x00000001UL <<  6U) /* (RO) Remote Wake-Up Packet Received */
#define CHIP_REGFLD_ETH_MACPCSR_GLBLUCAST           (0x00000001UL <<  9U) /* (RW) Global Unicast */
#define CHIP_REGFLD_ETH_MACPCSR_RWKPFE              (0x00000001UL << 10U) /* (RW) Remote Wake-up Packet Forwarding Enable */
#define CHIP_REGFLD_ETH_MACPCSR_RWKPTR              (0x0000001FUL << 24U) /* (RW) Remote Wake-up FIFO Pointer */
#define CHIP_REGFLD_ETH_MACPCSR_RWKFILTRST          (0x00000001UL << 31U) /* (RW) Remote Wake-Up Packet Filter Register Pointer Reset */
#define CHIP_REGMSK_ETH_MACPCSR_RESERVED            (0x60FFF998UL)

/*********** Remote wakeup packet filter register (ETH_MACRWKPFR) *************/
#define CHIP_REGFLD_ETH_MACRWKPFR_DATA              (0xFFFFFFFFUL <<  0U) /* (RW) Wake-up Packet filter register data */

/*********** LPI control status register (ETH_MACLCSR) ************************/
#define CHIP_REGFLD_ETH_MACLCSR_TLPIEN              (0x00000001UL <<  0U) /* (RO) Transmit LPI Entry */
#define CHIP_REGFLD_ETH_MACLCSR_TLPIEX              (0x00000001UL <<  1U) /* (RO) Transmit LPI Exit */
#define CHIP_REGFLD_ETH_MACLCSR_RLPIEN              (0x00000001UL <<  2U) /* (RO) Receive LPI Entry */
#define CHIP_REGFLD_ETH_MACLCSR_RLPIEX              (0x00000001UL <<  3U) /* (RO) Receive LPI Exit */
#define CHIP_REGFLD_ETH_MACLCSR_TLPIST              (0x00000001UL <<  8U) /* (RO) Transmit LPI State */
#define CHIP_REGFLD_ETH_MACLCSR_RLPIST              (0x00000001UL <<  9U) /* (RO) Receive LPI State */
#define CHIP_REGFLD_ETH_MACLCSR_LPIEN               (0x00000001UL << 16U) /* (RW) LPI Enable */
#define CHIP_REGFLD_ETH_MACLCSR_PLS                 (0x00000001UL << 17U) /* (RW) PHY Link Status */
#define CHIP_REGFLD_ETH_MACLCSR_PLSEN               (0x00000001UL << 18U) /* (RW) PHY Link Status Enable */
#define CHIP_REGFLD_ETH_MACLCSR_LPITXA              (0x00000001UL << 19U) /* (RW) LPI Tx Automate */
#define CHIP_REGFLD_ETH_MACLCSR_LPITE               (0x00000001UL << 20U) /* (RW) LPI Timer Enable */
#define CHIP_REGFLD_ETH_MACLCSR_LPITCSE             (0x00000001UL << 21U) /* (RW) LPI Tx Clock Stop Enable */ /** @note нет в RM */

/*********** LPI timers control register (ETH_MACLTCR) ************************/
#define CHIP_REGFLD_ETH_MACLTCR_TWT                 (0x0000FFFFUL <<  0U) /* (RW) LPI TW Timer */
#define CHIP_REGFLD_ETH_MACLTCR_LST                 (0x000003FFUL << 16U) /* (RW) LPI LS Timer */

/*********** LPI entry timer register (ETH_MACLETR) ***************************/
#define CHIP_REGFLD_ETH_MACLETR_LPIET               (0x000FFFFFUL <<  0U) /* (RW) LPI Entry Timer */

/*********** 1-microsecond-tick counter register (ETH_MAC1USTCR) **************/
#define CHIP_REGFLD_ETH_MAC1USTCR_TIC1USCNTR        (0x00000FFFUL <<  0U) /* (RW) 1US TIC Counter */

/*********** Version register (ETH_MACVR) *************************************/
#define CHIP_REGFLD_ETH_MACVR_SNPSVER               (0x000000FFUL <<  0U) /* (RO) Synopsys-defined Version */
#define CHIP_REGFLD_ETH_MACVR_USERVER               (0x000000FFUL <<  8U) /* (RO) User-defined Version */

/*********** Debug register (ETH_MACDR) ***************************************/
#define CHIP_REGFLD_ETH_MACDR_RPESTS                (0x00000001UL <<  0U) /* (RO) MAC MII Receive Protocol Engine Status */
#define CHIP_REGFLD_ETH_MACDR_RFCFCSTS              (0x00000003UL <<  1U) /* (RO) MAC MII Transmit Protocol Engine Status */
#define CHIP_REGFLD_ETH_MACDR_TPESTS                (0x00000001UL << 16U) /* (RO) MAC Receive Packet Controller FIFO Status */
#define CHIP_REGFLD_ETH_MACDR_TFCSTS                (0x00000003UL << 17U) /* (RO) MAC Transmit Packet Controller Status */

/********** HW feature 0 register (ETH_MACHWF0R) ******************************/
#define CHIP_REGFLD_ETH_MACHWF0R_MIISEL             (0x00000001UL <<  0U) /* (RO) 10 or 100 Mbps Support */
#define CHIP_REGFLD_ETH_MACHWF0R_GMIISEL            (0x00000001UL <<  1U) /* (RO) 1000 Mbps Support */
#define CHIP_REGFLD_ETH_MACHWF0R_HDSEL              (0x00000001UL <<  2U) /* (RO) Half-duplex Support */
#define CHIP_REGFLD_ETH_MACHWF0R_PCSSEL             (0x00000001UL <<  3U) /* (RO) PCS Registers (TBI, SGMII, or RTBI PHY interface) */
#define CHIP_REGFLD_ETH_MACHWF0R_VLHASH             (0x00000001UL <<  4U) /* (RO) VLAN Hash Filter Selected */
#define CHIP_REGFLD_ETH_MACHWF0R_SMASEL             (0x00000001UL <<  5U) /* (RO) SMA (MDIO) Interface */
#define CHIP_REGFLD_ETH_MACHWF0R_RWKSEL             (0x00000001UL <<  6U) /* (RO) PMT Remote Wake-up Packet Enable */
#define CHIP_REGFLD_ETH_MACHWF0R_MGKSEL             (0x00000001UL <<  7U) /* (RO) PMT Magic Packet Enable */
#define CHIP_REGFLD_ETH_MACHWF0R_MMCSEL             (0x00000001UL <<  8U) /* (RO) RMON Module Enable */
#define CHIP_REGFLD_ETH_MACHWF0R_ARPOFFSEL          (0x00000001UL <<  9U) /* (RO) ARP Offload Enabled */
#define CHIP_REGFLD_ETH_MACHWF0R_TSSEL              (0x00000001UL << 12U) /* (RO) IEEE 1588-2008 Timestamp Enabled */
#define CHIP_REGFLD_ETH_MACHWF0R_EEESEL             (0x00000001UL << 13U) /* (RO) Energy Efficient Ethernet Enabled */
#define CHIP_REGFLD_ETH_MACHWF0R_TXCOESEL           (0x00000001UL << 14U) /* (RO) Transmit Checksum Offload Enabled */
#define CHIP_REGFLD_ETH_MACHWF0R_RXCOESEL           (0x00000001UL << 16U) /* (RO) Receive Checksum Offload Enabled */
#define CHIP_REGFLD_ETH_MACHWF0R_ADDMACADRSEL       (0x0000001FUL << 18U) /* (RO) MAC Addresses 1-31 Selected */
#define CHIP_REGFLD_ETH_MACHWF0R_MACADR32SEL        (0x00000001UL << 23U) /* (RO) MAC Addresses 32-63 Selected */
#define CHIP_REGFLD_ETH_MACHWF0R_MACADR64SEL        (0x00000001UL << 24U) /* (RO) MAC Addresses 64-127 Selected */
#define CHIP_REGFLD_ETH_MACHWF0R_TSSTSSEL           (0x00000003UL << 25U) /* (RO) Timestamp System Time Source */
#define CHIP_REGFLD_ETH_MACHWF0R_SAVLANINS          (0x00000001UL << 27U) /* (RO) Source Address or VLAN Insertion Enable */
#define CHIP_REGFLD_ETH_MACHWF0R_ACTPHYSEL          (0x00000007UL << 28U) /* (RO) Active PHY Selected */

#define CHIP_REGFLDVAL_ETH_MACHWF0R_ACTPHYSEL_MII       0
#define CHIP_REGFLDVAL_ETH_MACHWF0R_ACTPHYSEL_RMII      4 /* RMII */
#define CHIP_REGFLDVAL_ETH_MACHWF0R_ACTPHYSEL_REVMII    7 /* RevMII */

#define CHIP_REGFLDVAL_ETH_MACHWF0R_TSSTSSEL_INTERNAL   1
#define CHIP_REGFLDVAL_ETH_MACHWF0R_TSSTSSEL_EXTERNAL   2
#define CHIP_REGFLDVAL_ETH_MACHWF0R_TSSTSSEL_BOTH       3


/********** HW feature 1 register (ETH_MACHWF1R) ******************************/
#define CHIP_REGFLD_ETH_MACHWF1R_RXFIFOSIZE         (0x0000001FUL <<  0U) /* (RO) MTL Receive FIFO Size */
#define CHIP_REGFLD_ETH_MACHWF1R_TXFIFOSIZE         (0x0000001FUL <<  6U) /* (RO) MTL Transmit FIFO Size */
#define CHIP_REGFLD_ETH_MACHWF1R_OSTEN              (0x00000001UL << 11U) /* (RO) One-Step Timestamping Enable */
#define CHIP_REGFLD_ETH_MACHWF1R_PTOEN              (0x00000001UL << 12U) /* (RO) PTP Offload Enable */
#define CHIP_REGFLD_ETH_MACHWF1R_ADVTHWORD          (0x00000001UL << 13U) /* (RO) IEEE 1588 High Word Register Enable */
#define CHIP_REGFLD_ETH_MACHWF1R_ADDR64             (0x00000003UL << 14U) /* (RO) Address Width */
#define CHIP_REGFLD_ETH_MACHWF1R_DCBEN              (0x00000001UL << 16U) /* (RO) DCB Feature Enable */
#define CHIP_REGFLD_ETH_MACHWF1R_SPHEN              (0x00000001UL << 17U) /* (RO) Split Header Feature Enable */
#define CHIP_REGFLD_ETH_MACHWF1R_TSOEN              (0x00000001UL << 18U) /* (RO) TCP Segmentation Offload Enable */
#define CHIP_REGFLD_ETH_MACHWF1R_DBGMEMA            (0x00000001UL << 19U) /* (RO) Debug Memory Interface Enabled */
#define CHIP_REGFLD_ETH_MACHWF1R_AVSEL              (0x00000001UL << 20U) /* (RO) AV Feature Enabled */
#define CHIP_REGFLD_ETH_MACHWF1R_HASHTBLSZ          (0x00000003UL << 24U) /* (RO) Hash Table Size */
#define CHIP_REGFLD_ETH_MACHWF1R_L3L4FNUM           (0x0000000FUL << 27U) /* (RO) Total number of L3 or L4 Filters */

#define CHIP_REGFLDVAL_ETH_MACHWF1R_ADDR64_32       0
#define CHIP_REGFLDVAL_ETH_MACHWF1R_ADDR64_40       1
#define CHIP_REGFLDVAL_ETH_MACHWF1R_ADDR64_48       2

/********** HW feature 2 register (ETH_MACHWF2R) ******************************/
#define CHIP_REGFLD_ETH_MACHWF2R_RXQCNT             (0x0000000FUL <<  0U) /* (RO) Number of MTL Receive Queues */
#define CHIP_REGFLD_ETH_MACHWF2R_TXQCNT             (0x0000000FUL <<  6U) /* (RO) Number of MTL Transmit Queues */
#define CHIP_REGFLD_ETH_MACHWF2R_RXCHCNT            (0x0000000FUL << 13U) /* (RO) Number of DMA Receive Channels */
#define CHIP_REGFLD_ETH_MACHWF2R_TXCHCNT            (0x0000000FUL << 18U) /* (RO) Number of DMA Transmit Channels */
#define CHIP_REGFLD_ETH_MACHWF2R_PPSOUTNUM          (0x00000007UL << 24U) /* (RO) Number of PPS Outputs */
#define CHIP_REGFLD_ETH_MACHWF2R_AUXSNAPNUM         (0x00000007UL << 28U) /* (RO) Number of Auxiliary Snapshot Inputs */

/*********** MDIO address register (ETH_MACMDIOAR) ****************************/
#define CHIP_REGFLD_ETH_MACMDIOAR_MB                (0x00000001UL <<  0U) /* (RW SC) MII Busy */
#define CHIP_REGFLD_ETH_MACMDIOAR_C45E              (0x00000001UL <<  1U) /* (RW) Clause 45 PHY Enable */
#define CHIP_REGFLD_ETH_MACMDIOAR_MOC               (0x00000003UL <<  2U) /* (RW) MII Operation Command */
#define CHIP_REGFLD_ETH_MACMDIOAR_SKAP              (0x00000001UL <<  4U) /* (RW) Skip Address Packet */
#define CHIP_REGFLD_ETH_MACMDIOAR_CR                (0x0000000FUL <<  8U) /* (RW) CSR Clock Range */
#define CHIP_REGFLD_ETH_MACMDIOAR_NTC               (0x00000007UL << 12U) /* (RW) Number of Trailing Clocks */
#define CHIP_REGFLD_ETH_MACMDIOAR_RDA               (0x0000001FUL << 16U) /* (RW) Register/Device Address */
#define CHIP_REGFLD_ETH_MACMDIOAR_PA                (0x0000001FUL << 21U) /* (RW) Physical Layer Address */
#define CHIP_REGFLD_ETH_MACMDIOAR_BTB               (0x00000001UL << 26U) /* (RW) Back to Back transactions */
#define CHIP_REGFLD_ETH_MACMDIOAR_PSE               (0x00000001UL << 27U) /* (RW) Preamble Suppression Enable */
#define CHIP_REGMSK_ETH_MACMDIOAR_RESERVED          (0xF00080E0UL)

#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV42       0
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV62       1
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV16       2
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV26       3
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV102      4
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV124      5
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV4AR      8
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV6AR      9
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV8AR      10
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV10AR     11
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV12AR     12
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV14AR     13
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV16AR     14
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_CR_DIV18AR     15

#define CHIP_REGFLDVAL_ETH_MACMDIOAR_MOC_WR         1 /* Write */
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_MOC_RD_PINC    2 /* Post Read Increment Address for Clause 45 PHY */
#define CHIP_REGFLDVAL_ETH_MACMDIOAR_MOC_RD         3 /* Read */

/*********** MDIO data register (ETH_MACMDIODR) *******************************/
#define CHIP_REGFLD_ETH_MACMDIODR_MD                (0x0000FFFFUL <<  0U) /* (RW) MII Data */
#define CHIP_REGFLD_ETH_MACMDIODR_RA                (0x0000FFFFUL << 16U) /* (RW) Register Address */

/*********** ARP address register (ETH_MACARPAR) ******************************/
#define CHIP_REGFLD_ETH_MACARPAR_ARPPA              (0xFFFFFFFFUL <<  0U) /* (RW) ARP Protocol Address */

/*********** Address x high register (ETH_MACAxHR) ****************************/
#define CHIP_REGFLD_ETH_MACAHR_ADDRHI               (0x0000FFFFUL <<  0U) /* (RW) MAC Address [47:32] */
#define CHIP_REGFLD_ETH_MACAHR_MBC                  (0x0000003FUL << 24U) /* (RW) Mask Byte Control (for x > 0 only) */
#define CHIP_REGFLD_ETH_MACAHR_SA                   (0x00000001UL << 30U) /* (RW) Source Address (for x > 0 only) */
#define CHIP_REGFLD_ETH_MACAHR_AE                   (0x00000001UL << 31U) /* (RO) Address Enable */

/*********** Address x low register (ETH_MACAxLR) *****************************/
#define CHIP_REGFLD_ETH_MACALR_ADDRLO               (0xFFFFFFFFUL <<  0U) /* MAC Address [31:0] */

/*********** MMC control register (ETH_MMC_CONTROL) ***************************/
#define CHIP_REGFLD_ETH_MMCCR_CNTRST                (0x00000001UL <<  0U) /* (RSC) Counters Reset */
#define CHIP_REGFLD_ETH_MMCCR_CNTSTOPRO             (0x00000001UL <<  1U) /* (RW)  Counter Stop Rollover */
#define CHIP_REGFLD_ETH_MMCCR_RSTONRD               (0x00000001UL <<  2U) /* (RSC) Reset On Read */
#define CHIP_REGFLD_ETH_MMCCR_CNTFREEZ              (0x00000001UL <<  3U) /* (RW)  MMC Counter Freeze */
#define CHIP_REGFLD_ETH_MMCCR_CNTPRST               (0x00000001UL <<  4U) /* (RW)  Counters Reset */
#define CHIP_REGFLD_ETH_MMCCR_CNTPRSTLVL            (0x00000001UL <<  5U) /* (RW)  Full-Half Preset */
#define CHIP_REGFLD_ETH_MMCCR_UCDBC                 (0x00000001UL <<  8U) /* (RW)  Update MMC Counters for Dropped Broadcast Packets */
#define CHIP_REGMSK_ETH_MMCCR_RESERVED              (0xFFFFFEC0UL)

/*********** MMC Rx interrupt register (ETH_MMC_RX_INTERRUPT) *****************/
#define CHIP_REGFLD_ETH_MMCRXIR_RXCRCERPIS          (0x00000001UL <<  5U) /* (RO) MMC Receive CRC Error Packet Counter Interrupt Status */
#define CHIP_REGFLD_ETH_MMCRXIR_RXALGNERPIS         (0x00000001UL <<  6U) /* (RO) MMC Receive Alignment Error Packet Counter Interrupt Status */
#define CHIP_REGFLD_ETH_MMCRXIR_RXUCGPIS            (0x00000001UL << 17U) /* (RO) MMC Receive Unicast Good Packet Counter Interrupt Status */
#define CHIP_REGFLD_ETH_MMCRXIR_RXLPIUSCIS          (0x00000001UL << 26U) /* (RO) MMC Receive LPI microsecond counter interrupt status */
#define CHIP_REGFLD_ETH_MMCRXIR_RXLPITRCIS          (0x00000001UL << 27U) /* (RO) MMC Receive LPI transition counter interrupt status */

/*********** MMC Tx interrupt register (ETH_MMC_TX_INTERRUPT) *****************/
#define CHIP_REGFLD_ETH_MMCTXIR_TXSCOLGPIS          (0x00000001UL << 14U) /* (RO) MMC Transmit Single Collision Good Packet Counter Interrupt Status */
#define CHIP_REGFLD_ETH_MMCTXIR_TXMCOLGPIS          (0x00000001UL << 15U) /* (RO) MMC Transmit Multiple Collision Good Packet Counter Interrupt Status */
#define CHIP_REGFLD_ETH_MMCTXIR_TXGPKTIS            (0x00000001UL << 21U) /* (RO) MMC Transmit Good Packet Counter Interrupt Status */
#define CHIP_REGFLD_ETH_MMCTXIR_TXLPIUSCIS          (0x00000001UL << 26U) /* (RO) MMC Transmit LPI microsecond counter interrupt status */
#define CHIP_REGFLD_ETH_MMCTXIR_TXLPITRCIS          (0x00000001UL << 27U) /* (RO) MMC Transmit LPI transition counter interrupt status */

/*********** MMC Rx interrupt mask register (ETH_MMC_RX_INTERRUPT_MASK) *******/
#define CHIP_REGFLD_ETH_MMCRXIMR_RXCRCERPIM         (0x00000001UL <<  5U) /* (RW) MMC Receive CRC Error Packet Counter Interrupt Mask */
#define CHIP_REGFLD_ETH_MMCRXIMR_RXALGNERPIM        (0x00000001UL <<  6U) /* (RW) MMC Receive Alignment Error Packet Counter Interrupt Mask */
#define CHIP_REGFLD_ETH_MMCRXIMR_RXUCGPIM           (0x00000001UL << 17U) /* (RW) MMC Receive Unicast Good Packet Counter Interrupt Mask */
#define CHIP_REGFLD_ETH_MMCRXIMR_RXLPIUSCIM         (0x00000001UL << 26U) /* (RW) MMC Receive LPI microsecond counter interrupt Mask */
#define CHIP_REGFLD_ETH_MMCRXIMR_RXLPITRCIM         (0x00000001UL << 27U) /* (RW) MMC Receive LPI transition counter interrupt Mask */

/*********** MMC Tx interrupt mask register (ETH_MMC_TX_INTERRUPT_MASK) *******/
#define CHIP_REGFLD_ETH_MMCTXIMR_TXSCOLGPIM         (0x00000001UL << 14U) /* (RW) MMC Transmit Single Collision Good Packet Counter Interrupt Mask */
#define CHIP_REGFLD_ETH_MMCTXIMR_TXMCOLGPIM         (0x00000001UL << 15U) /* (RW) MMC Transmit Multiple Collision Good Packet Counter Interrupt Mask */
#define CHIP_REGFLD_ETH_MMCTXIMR_TXGPKTIM           (0x00000001UL << 21U) /* (RW) MMC Transmit Good Packet Counter Interrupt Mask*/
#define CHIP_REGFLD_ETH_MMCTXIMR_TXLPIUSCIM         (0x00000001UL << 26U) /* (RW) MMC Transmit LPI microsecond counter interrupt Mask*/
#define CHIP_REGFLD_ETH_MMCTXIMR_TXLPITRCIM         (0x00000001UL << 27U) /* (RW) MMC Transmit LPI transition counter interrupt Mask*/

/* Tx single collision good packets register (ETH_TX_SINGLE_COLLISION_GOOD_PACKETS) */
#define CHIP_REGFLD_ETH_MMCTXSCGPR_TXSNGLCOLG       (0xFFFFFFFFUL <<  0U) /* (RO) Tx Single Collision Good Packets */

/* Tx multiple collision good packets register (ETH_TX_MULTIPLE_COLLISION_GOOD_PACKETS) */
#define CHIP_REGFLD_ETH_MMCTXMCGPR_TXMULTCOLG       (0xFFFFFFFFUL <<  0U) /* (RO) Tx Multiple Collision Good Packets */

/*********** Tx packet count good register (ETH_TX_PACKET_COUNT_GOOD) *********/
#define CHIP_REGFLD_ETH_MMCTXPCGR_TXPKTG            (0xFFFFFFFFUL <<  0U) /* (RO) Tx Packet Count Good */

/*********** Rx CRC error packets register (ETH_RX_CRC_ERROR_PACKETS) *********/
#define CHIP_REGFLD_ETH_MMCRXCRCEPR_RXCRCERR        (0xFFFFFFFFUL <<  0U) /* (RO) Rx CRC Error Packets */

/****** Rx alignment error packets register (ETH_RX_ALIGNMENT_ERROR_PACKETS) **/
#define CHIP_REGFLD_ETH_MMCRXAEPR_RXALGNERR         (0xFFFFFFFFUL <<  0U) /* (RO) Rx Alignment Error Packets */

/*********** Rx unicast packets good register (ETH_RX_UNICAST_PACKETS_GOOD) ***/
#define CHIP_REGFLD_ETH_MMCRXUPGR_RXUCASTG          (0xFFFFFFFFUL <<  0U) /* (RO) Rx Unicast Packets Good */

/*********** Tx LPI microsecond timer register (ETH_TX_LPI_USEC_CNTR) *********/
#define CHIP_REGFLD_ETH_MMCTXLPIMSTR_TXLPIUSC       (0xFFFFFFFFUL <<  0U) /* (RO) Tx LPI Microseconds Counter */

/*********** Tx LPI transition counter register (ETH_TX_LPI_TRAN_CNTR) ********/
#define CHIP_REGFLD_ETH_MMCTXLPITCR_TXLPITRC        (0xFFFFFFFFUL <<  0U) /* (RO) Tx LPI Transition counter */

/*********** Rx LPI microsecond counter register (ETH_RX_LPI_USEC_CNTR) *******/
#define CHIP_REGFLD_ETH_MMCRXLPIMSTR_RXLPIUSC       (0xFFFFFFFFUL <<  0U) /* (RO) Rx LPI Microseconds Counter */

/*********** Rx LPI transition counter register (ETH_RX_LPI_TRAN_CNTR) ********/
#define CHIP_REGFLD_ETH_MMCRXLPITCR_RXLPITRC        (0xFFFFFFFFUL <<  0U) /* (RO) Rx LPI Transition counter */

/*********** L3 and L4 control x register (ETH_MACL3L4CxR) ********************/
#define CHIP_REGFLD_ETH_MACL3L4CR_L3PEN             (0x00000001UL <<  0U) /* (RW) Layer 3 Protocol Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L3SAM             (0x00000001UL <<  2U) /* (RW) Layer 3 IP SA Match Enable*/
#define CHIP_REGFLD_ETH_MACL3L4CR_L3SAIM            (0x00000001UL <<  3U) /* (RW) Layer 3 IP SA Inverse Match Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L3DAM             (0x00000001UL <<  4U) /* (RW) Layer 3 IP DA Match Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L3DAIM            (0x00000001UL <<  5U) /* (RW) Layer 3 IP DA Inverse Match Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L3HSBM            (0x0000001FUL <<  6U) /* (RW) Layer 3 IP SA Higher Bits Match */
#define CHIP_REGFLD_ETH_MACL3L4CR_L3HDBM            (0x0000001FUL << 11U) /* (RW) Layer 3 IP DA Higher Bits Match */
#define CHIP_REGFLD_ETH_MACL3L4CR_L4PEN             (0x00000001UL << 16U) /* (RW) Layer 4 Protocol Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L4SPM             (0x00000001UL << 18U) /* (RW) Layer 4 Source Port Match Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L4SPIM            (0x00000001UL << 19U) /* (RW) Layer 4 Source Port Inverse Match Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L4DPM             (0x00000001UL << 20U) /* (RW) Layer 4 Destination Port Match Enable */
#define CHIP_REGFLD_ETH_MACL3L4CR_L4DPIM            (0x00000001UL << 21U) /* (RW) Layer 4 Destination Port Inverse Match Enable */
#define CHIP_REGMSK_ETH_MACL3L4CR_RESERVED          (0xFFC20002UL)

/*********** Layer4 address filter x register (ETH_MACL4AxR) ******************/
#define CHIP_REGFLD_ETH_MACL4AR_L4SP                (0x0000FFFFUL <<  0U) /* Layer 4 Source Port Number Field */
#define CHIP_REGFLD_ETH_MACL4AR_L4DP                (0x0000FFFFUL << 16U) /* Layer 4 Destination Port Number Field */

/*********** Layer 3 Address x filter x register (ETH_MACL3AxxR) **************/
#define CHIP_REGFLD_ETH_MACL3AR_LA                  (0xFFFFFFFFUL <<  0U) /* Layer 3 Address x Field */

/*********** Timestamp control Register (ETH_MACTSCR) *************************/
#define CHIP_REGFLD_ETH_MACTSCR_TSENA               (0x00000001UL <<  0U) /* (RW) Enable Timestamp */
#define CHIP_REGFLD_ETH_MACTSCR_TSCFUPDT            (0x00000001UL <<  1U) /* (RW) Fine or Coarse Timestamp Update*/
#define CHIP_REGFLD_ETH_MACTSCR_TSINIT              (0x00000001UL <<  2U) /* (RW) Initialize Timestamp */
#define CHIP_REGFLD_ETH_MACTSCR_TSUPDT              (0x00000001UL <<  3U) /* (RW) Update Timestamp */
#define CHIP_REGFLD_ETH_MACTSCR_TSADDREG            (0x00000001UL <<  5U) /* (RW) Update Addend Register */
#define CHIP_REGFLD_ETH_MACTSCR_TSENALL             (0x00000001UL <<  8U) /* (RW) Enable Timestamp for All Packets */
#define CHIP_REGFLD_ETH_MACTSCR_TSCTRLSSR           (0x00000001UL <<  9U) /* (RW) Timestamp Digital or Binary Rollover Control */
#define CHIP_REGFLD_ETH_MACTSCR_TSVER2ENA           (0x00000001UL << 10U) /* (RW) Enable PTP Packet Processing for Version 2 Format */
#define CHIP_REGFLD_ETH_MACTSCR_TSIPENA             (0x00000001UL << 11U) /* (RW) Enable Processing of PTP over Ethernet Packets */
#define CHIP_REGFLD_ETH_MACTSCR_TSIPV6ENA           (0x00000001UL << 12U) /* (RW) Enable Processing of PTP Packets Sent over IPv6-UDP */
#define CHIP_REGFLD_ETH_MACTSCR_TSIPV4ENA           (0x00000001UL << 13U) /* (RW) Enable Processing of PTP Packets Sent over IPv4-UDP */
#define CHIP_REGFLD_ETH_MACTSCR_TSEVNTENA           (0x00000001UL << 14U) /* (RW) Enable Timestamp Snapshot for Event Messages */
#define CHIP_REGFLD_ETH_MACTSCR_TSMSTRENA           (0x00000001UL << 15U) /* (RW) Enable Snapshot for Messages Relevant to Master */
#define CHIP_REGFLD_ETH_MACTSCR_SNAPTYPSEL          (0x00000003UL << 16U) /* (RW) Select PTP packets for Taking Snapshots */
#define CHIP_REGFLD_ETH_MACTSCR_TSENMACADDR         (0x00000001UL << 18U) /* (RW) Enable MAC Address for PTP Packet Filtering */
#define CHIP_REGFLD_ETH_MACTSCR_CSC                 (0x00000001UL << 19U) /* (RW) Enable checksum correction during OST for PTP over UDP/IPv4 packets */
#define CHIP_REGFLD_ETH_MACTSCR_TXTSSTSM            (0x00000001UL << 24U) /* (RW) Transmit Timestamp Status Mode */
#define CHIP_REGMSK_ETH_MACTSCR_RESERVED            (0xFEF000D0UL)

/*********** Sub-second increment register (ETH_MACSSIR) **********************/
#define CHIP_REGFLD_ETH_MACSSIR_SSINC               (0x000000FFUL << 16U) /* (RW) Sub-second Increment Value */
#define CHIP_REGFLD_ETH_MACSSIR_SNSINC              (0x000000FFUL <<  8U) /* (RW) Sub-nanosecond Increment Value */ /** @note нет в RM */

/*********** System time seconds register (ETH_MACSTSR) ***********************/
#define CHIP_REGFLD_ETH_MACSTSR_TSS                 (0xFFFFFFFFUL <<  0U) /* (RO) Timestamp Second */

/*********** System time nanoseconds register (ETH_MACSTNR) *******************/
#define CHIP_REGFLD_ETH_MACSTNR_TSSS                (0x7FFFFFFFUL <<  0U) /* (RO) Timestamp Sub-seconds */

/*********** System time seconds update register (ETH_MACSTSUR) ***************/
#define CHIP_REGFLD_ETH_MACSTSUR_TSS                (0xFFFFFFFFUL <<  0U) /* (RW) Timestamp Seconds */

/********** System time nanoseconds update register (ETH_MACSTNUR) ************/
#define CHIP_REGFLD_ETH_MACSTNUR_TSSS               (0x7FFFFFFFUL <<  0U) /* (RW) Timestamp Sub-seconds */
#define CHIP_REGFLD_ETH_MACSTNUR_ADDSUB             (0x00000001UL << 31U) /* (RW) Add or Subtract Time */

/********** Timestamp addend register (ETH_MACTSAR) ***************************/
#define CHIP_REGFLD_ETH_MACTSAR_TSAR                (0xFFFFFFFFUL <<  0U) /* (RW) Timestamp Addend Register */

/*********** Timestamp status register (ETH_MACTSSR) **************************/
#define CHIP_REGFLD_ETH_MACTSSR_TSSOVF              (0x00000001UL <<  0U) /* (RO) Timestamp Seconds Overflow */
#define CHIP_REGFLD_ETH_MACTSSR_TSTARGT0            (0x00000001UL <<  1U) /* (RO) Timestamp Target Time Reached */
#define CHIP_REGFLD_ETH_MACTSSR_AUXTSTRIG           (0x00000001UL <<  2U) /* (RO) Auxiliary Timestamp Trigger Snapshot*/
#define CHIP_REGFLD_ETH_MACTSSR_TSTRGTERR0          (0x00000001UL <<  3U) /* (RO) Timestamp Target Time Error */
#define CHIP_REGFLD_ETH_MACTSSR_TXTSSIS             (0x00000001UL << 15U) /* (RO) Tx Timestamp Status Interrupt Status */
#define CHIP_REGFLD_ETH_MACTSSR_ATSSTN              (0x0000000FUL << 16U) /* (RO) Auxiliary Timestamp Snapshot Trigger Identifier */
#define CHIP_REGFLD_ETH_MACTSSR_ATSSTM              (0x00000001UL << 24U) /* (RO) Auxiliary Timestamp Snapshot Trigger Missed */
#define CHIP_REGFLD_ETH_MACTSSR_ATSNS               (0x0000001FUL << 25U) /* (RO) Number of Auxiliary Timestamp Snapshots */

/*********** Tx timestamp status nanoseconds register (ETH_MACTXTSSNR) ********/
#define CHIP_REGFLD_ETH_MACTXTSSNR_TXTSSLO          (0x7FFFFFFFUL <<  0U) /* (RO) Transmit Timestamp Status Low */
#define CHIP_REGFLD_ETH_MACTXTSSNR_TXTSSMIS         (0x00000001UL << 31U) /* (RO) Transmit Timestamp Status Missed */

/*********** Tx timestamp status seconds register (ETH_MACTXTSSSR) ************/
#define CHIP_REGFLD_ETH_MACTXTSSSR_TXTSSHI          (0xFFFFFFFFUL <<  0U) /* (RO) Transmit Timestamp Status High */

/*********** Auxiliary control register (ETH_MACACR) **************************/
#define CHIP_REGFLD_ETH_MACACR_ATSFC                (0x00000001UL <<  0U) /* (RSC) Auxiliary Snapshot FIFO Clear */
#define CHIP_REGFLD_ETH_MACACR_ATSEN0               (0x00000001UL <<  4U) /* (RW)  Auxiliary Snapshot 0 Enable */
#define CHIP_REGFLD_ETH_MACACR_ATSEN1               (0x00000001UL <<  5U) /* (RW)  Auxiliary Snapshot 1 Enable */
#define CHIP_REGFLD_ETH_MACACR_ATSEN2               (0x00000001UL <<  6U) /* (RW)  Auxiliary Snapshot 2 Enable */
#define CHIP_REGFLD_ETH_MACACR_ATSEN3               (0x00000001UL <<  7U) /* (RW)  Auxiliary Snapshot 3 Enable */
#define CHIP_REGFLD_ETH_MACACR_ATSEN(n)             (0x00000001UL <<  (4U + (n)))

/*********** Auxiliary timestamp nanoseconds register (ETH_MACATSNR) **********/
#define CHIP_REGFLD_ETH_MACATSNR_AUXTSLO            (0x7FFFFFFFUL <<  0U) /* (RO) Auxiliary Timestamp */

/*********** Auxiliary timestamp seconds register (ETH_MACATSSR) **************/
#define CHIP_REGFLD_ETH_MACATSSR_AUXTSHI            (0xFFFFFFFFUL <<  0U) /* (RO) Auxiliary Timestamp */

/*********** Timestamp Ingress asymmetric correction register (ETH_MACTSIACR) */
#define CHIP_REGFLD_ETH_MACTSIACR_OSTIAC            (0xFFFFFFFFUL <<  0U) /* (RW) One-Step Timestamp Ingress Asymmetry Correction */

/*********** Timestamp Egress asymmetric correction register (ETH_MACTSEACR) **/
#define CHIP_REGFLD_ETH_MACTSEACR_OSTEAC            (0xFFFFFFFFUL <<  0U) /* (RW) One-Step Timestamp Egress Asymmetry Correction */

/*********** Timestamp Ingress correction nanosecond register (ETH_MACTSICNR) */
#define CHIP_REGFLD_ETH_MACTSICNR_TSIC              (0xFFFFFFFFUL <<  0U) /* (RW) Timestamp Ingress Correction */

/*********** Timestamp Egress correction nanosecond register (ETH_MACTSECNR) **/
#define CHIP_REGFLD_ETH_MACTSECNR_TSEC              (0xFFFFFFFFUL <<  0U) /* (RW) Timestamp Egress Correction */

/*********** PPS control register [alternate] (ETH_MACPPSCR) ******************/
#define CHIP_REGFLD_ETH_MACPPSCR_PPSCTRL            (0x0000000FUL <<  0U) /* (RW) PPS Output Frequency Control (flexible mode disabled) */
#define CHIP_REGFLD_ETH_MACPPSCR_PPSCMD             (0x0000000FUL <<  0U) /* (RW) Flexible PPS Output Control (flexible mode enabled) */
#define CHIP_REGFLD_ETH_MACPPSCR_PPSEN0             (0x00000001UL <<  4U) /* (RW) Flexible PPS Output Mode Enable */
#define CHIP_REGFLD_ETH_MACPPSCR_TRGTMODSEL0        (0x00000003UL <<  5U) /* (RW) Target Time Register Mode for PPS Output */

#define CHIP_REGFLDVAL_ETH_MACPPSCR_PPSCMD_NOCMD            0 /* No Command */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_PPSCMD_START_SINGLE     1 /* START Single Pulse */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_PPSCMD_START_TRAIN      2 /* START Pulse Train */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_PPSCMD_START_CANCEL     3 /* Cancel START */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_PPSCMD_STOP_AT_TIME     4 /* STOP Pulse train at time */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_PPSCMD_STOP_IMM         5 /* STOP Pulse Train immediately */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_PPSCMD_STOP_CANCEL      6 /* Cancel STOP Pulse train */

#define CHIP_REGFLDVAL_ETH_MACPPSCR_TRGTMODSEL_INT_ONLY     0 /* Target Time registers are programmed only for generating the interrupt event */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_TRGTMODSEL_INT_PPS      2 /* Target Time registers are programmed for generating the interrupt event and starting or stopping the PPS output signal generation. */
#define CHIP_REGFLDVAL_ETH_MACPPSCR_TRGTMODSEL_PPS_ONLY     3 /*  Target Time registers are programmed only for starting or stopping the PPS output signal generation. No interrupt is asserted. */

/*********** PPS target time seconds register (ETH_MACPPSTTSR) ****************/
#define CHIP_REGFLD_ETH_MACPPSTTSR_TSTRH0           (0xFFFFFFFFUL <<  0U) /* (RW) PPS Target Time Seconds Register */

/*********** PPS target time nanoseconds register (ETH_MACPPSTTNR) ************/
#define CHIP_REGFLD_ETH_MACPPSTTNR_TTSL0            (0x7FFFFFFFUL <<  0U) /* (RW) Target Time Low for PPS Register */
#define CHIP_REGFLD_ETH_MACPPSTTNR_TRGTBUSY0        (0x00000001UL << 31U) /* (RW) PPS Target Time Register Busy */

/*********** PPS interval register (ETH_MACPPSIR) *****************************/
#define CHIP_REGFLD_ETH_MACPPSIR_PPSINT0            (0xFFFFFFFFUL <<  0U) /* (RW) PPS Output Signal Interval */

/*********** PPS width register (ETH_MACPPSWR) ********************************/
#define CHIP_REGFLD_ETH_MACPPSWR_PPSWIDTH0          (0xFFFFFFFFUL <<  0U) /* (RW) PPS Output Signal Width */

/*********** PTP Offload control register (ETH_MACPOCR) ***********************/
#define CHIP_REGFLD_ETH_MACPOCR_PTOEN               (0x00000001UL <<  0U) /* (RW) PTP Offload Enable */
#define CHIP_REGFLD_ETH_MACPOCR_ASYNCEN             (0x00000001UL <<  1U) /* (RW) Automatic PTP SYNC message Enable */
#define CHIP_REGFLD_ETH_MACPOCR_APDREQEN            (0x00000001UL <<  2U) /* (RW) Automatic PTP Pdelay_Req message Enable */
#define CHIP_REGFLD_ETH_MACPOCR_ASYNCTRIG           (0x00000001UL <<  4U) /* (RW) Automatic PTP SYNC message Trigger */
#define CHIP_REGFLD_ETH_MACPOCR_APDREQTRIG          (0x00000001UL <<  5U) /* (RW) Automatic PTP Pdelay_Req message Trigger */
#define CHIP_REGFLD_ETH_MACPOCR_DRRDIS              (0x00000001UL <<  6U) /* (RW) Disable PTO Delay Request/Response response generation */
#define CHIP_REGFLD_ETH_MACPOCR_DN                  (0x000000FFUL <<  8U) /* (RW) Domain Number */
#define CHIP_REGMSK_ETH_MACPOCR_RESERVED            (0xFFFF0088UL)

/*********** PTP Source Port Identity x Register (ETH_MACSPIxR) ***************/
#define CHIP_REGFLD_ETH_MACSPIR_SPI                 (0xFFFFFFFFUL <<  0U) /* Source Port Identity 0 */

/*********** Log message interval register (ETH_MACLMIR) **********************/
#define CHIP_REGFLD_ETH_MACLMIR_LSI                 (0x000000FFUL <<  0U) /* (RW) Log Sync Interval */
#define CHIP_REGFLD_ETH_MACLMIR_DRSYNCR             (0x00000007UL <<  8U) /* (RW) Delay_Req to SYNC Ratio */
#define CHIP_REGFLD_ETH_MACLMIR_LMPDRI              (0x000000FFUL << 24U) /* (RW) Log Min Pdelay_Req Interval */

/* ----------------------------- MLT Registers -------------------------------*/
/*********** Operating mode Register (ETH_MTLOMR) *****************************/
#define CHIP_REGFLD_ETH_MTLOMR_DTXSTS               (0x00000001UL <<  1U) /* (RW)  Drop Transmit Status */
#define CHIP_REGFLD_ETH_MTLOMR_CNTPRST              (0x00000001UL <<  8U) /* (RW)  Counters Preset */
#define CHIP_REGFLD_ETH_MTLOMR_CNTCLR               (0x00000001UL <<  9U) /* (RSC) Counters Reset */
#define CHIP_REGMSK_ETH_MTLOMR_RESERVED             (0xFFFFFCFDUL)

/*********** Interrupt status Register (ETH_MTLISR) ****************************/
#define CHIP_REGFLD_ETH_MTLISR_QIS                  (0x00000001UL <<  0U) /* (RO) Queue Interrupt status */
#define CHIP_REGFLD_ETH_MTLISR_MACIS                (0x00000001UL << 16U) /* (RO) MAC Interrupt Status */ /** @note нет в RM */

/*********** Tx queue operating mode Register (ETH_MTLTXQOMR) *****************/
#define CHIP_REGFLD_ETH_MTLTXQOMR_FTQ               (0x00000001UL <<  0U) /* (RSC) Flush Transmit Queue */
#define CHIP_REGFLD_ETH_MTLTXQOMR_TSF               (0x00000001UL <<  1U) /* (RW)  Transmit Store and Forward */
#define CHIP_REGFLD_ETH_MTLTXQOMR_TXQEN             (0x00000003UL <<  2U) /* (RO)  Transmit Queue Enable */
#define CHIP_REGFLD_ETH_MTLTXQOMR_TTC               (0x00000007UL <<  4U) /* (RW)  Transmit Threshold Control */
#define CHIP_REGFLD_ETH_MTLTXQOMR_TQS               (0x00000007UL << 16U) /* (RW)  Transmit queue size: (N+1)*256 */
#define CHIP_REGMSK_ETH_MTLTXQOMR_RESERVED          (0xFFF8FFC0UL)

#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_32         0
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_64         1
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_96         2
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_128        3
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_192        4
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_256        5
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_384        6
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TTC_512        7

#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TXQEN_EN       2
#define CHIP_REGFLDVAL_ETH_MTLTXQOMR_TXQEN_DIS      0


/*********** Tx queue underflow register (ETH_MTLTXQUR) ***********************/
#define CHIP_REGFLD_ETH_MTLTXQUR_UFPKTCNT           (0x000007FFUL <<  0U) /* (RO) Underflow Packet Counter */
#define CHIP_REGFLD_ETH_MTLTXQUR_UFCNTOVF           (0x00000001UL << 11U) /* (RO) Overflow Bit for Underflow Packet Counter */


/*********** Tx queue debug Register (ETH_MTLTXQDR) ***************************/
#define CHIP_REGFLD_ETH_MTLTXQDR_TXQPAUSED          (0x00000001UL <<  0U) /* (RO) Transmit Queue in Pause */
#define CHIP_REGFLD_ETH_MTLTXQDR_TRCSTS             (0x00000003UL <<  1U) /* (RO) MTL Tx Queue Read Controller Status */
#define CHIP_REGFLD_ETH_MTLTXQDR_TWCSTS             (0x00000001UL <<  3U) /* (RO) MTL Tx Queue Write Controller Status */
#define CHIP_REGFLD_ETH_MTLTXQDR_TXQSTS             (0x00000001UL <<  4U) /* (RO) MTL Tx Queue Not Empty Status */
#define CHIP_REGFLD_ETH_MTLTXQDR_TXSTSFSTS          (0x00000001UL <<  5U) /* (RO) MTL Tx Status FIFO Full Status */
#define CHIP_REGFLD_ETH_MTLTXQDR_PTXQ               (0x00000007UL << 16U) /* (RO) Number of Packets in the Transmit Queue */
#define CHIP_REGFLD_ETH_MTLTXQDR_STXSTSF            (0x00000007UL << 29U) /* (RO) Number of Status Words in the Tx Status FIFO of Queue */

#define CHIP_REGFLDVAL_ETH_MTLTXQDR_TRCSTS_IDLE     0 /* Idle state */
#define CHIP_REGFLDVAL_ETH_MTLTXQDR_TRCSTS_READ     1 /* Read state (transferring data to the MAC transmitter) */
#define CHIP_REGFLDVAL_ETH_MTLTXQDR_TRCSTS_WAITING  2 /* Waiting for pending Tx Status from the MAC transmitter */
#define CHIP_REGFLDVAL_ETH_MTLTXQDR_TRCSTS_FLUSHING 3 /* Flushing the Tx queue because of the Packet Abort request from the MAC */

/*********** Queue interrupt control status Register (ETH_MTLQICSR) ***********/
#define CHIP_REGFLD_ETH_MTLQICSR_TXUNFIS            (0x00000001UL <<  0U) /* (RW) Transmit Queue Underflow Interrupt Status */
#define CHIP_REGFLD_ETH_MTLQICSR_TXUIE              (0x00000001UL <<  8U) /* (RW) Transmit Queue Underflow Interrupt Enable */
#define CHIP_REGFLD_ETH_MTLQICSR_RXOVFIS            (0x00000001UL << 16U) /* (RW) Receive Queue Overflow Interrupt Status */
#define CHIP_REGFLD_ETH_MTLQICSR_RXOIE              (0x00000001UL << 24U) /* (RW) Receive Queue Overflow Interrupt Enable */

/*********** Rx queue operating mode register (ETH_MTLRXQOMR) *****************/
#define CHIP_REGFLD_ETH_MTLRXQOMR_RTC               (0x00000003UL <<  0U) /* (RW) Receive Queue Threshold Control */
#define CHIP_REGFLD_ETH_MTLRXQOMR_FUP               (0x00000001UL <<  3U) /* (RW) Forward Undersized Good Packets */
#define CHIP_REGFLD_ETH_MTLRXQOMR_FEP               (0x00000001UL <<  4U) /* (RW) Forward Error Packets */
#define CHIP_REGFLD_ETH_MTLRXQOMR_RSF               (0x00000001UL <<  5U) /* (RW) Receive Queue Store and Forward */
#define CHIP_REGFLD_ETH_MTLRXQOMR_DISTCPEF          (0x00000001UL <<  6U) /* (RW) Disable Dropping of TCP/IP Checksum Error Packets */
#define CHIP_REGFLD_ETH_MTLRXQOMR_EHFC              (0x00000001UL <<  7U) /* (RW) Enable Hardware Flow Control */
#define CHIP_REGFLD_ETH_MTLRXQOMR_RFA               (0x00000007UL <<  8U) /* (RW) Threshold for Activating Flow Control (in half-duplex and full-duplex */
#define CHIP_REGFLD_ETH_MTLRXQOMR_RFD               (0x00000003UL << 14U) /* (RW) Threshold for Deactivating Flow Control (in half-duplex and full-duplex modes) */
#define CHIP_REGFLD_ETH_MTLRXQOMR_RQS               (0x00000007UL << 20U) /* (RO) Receive Queue Size */
#define CHIP_REGMSK_ETH_MTLRXQOMR_RESERVED          (0xFF8E3804UL)

#define CHIP_REGFLDVAL_ETH_MTLRXQOMR_RTC_64         0
#define CHIP_REGFLDVAL_ETH_MTLRXQOMR_RTC_32         1
#define CHIP_REGFLDVAL_ETH_MTLRXQOMR_RTC_96         2
#define CHIP_REGFLDVAL_ETH_MTLRXQOMR_RTC_128        3

/*********** Rx queue missed packet and overflow counter register (ETH_MTLRXQMPOCR) */
#define CHIP_REGFLD_ETH_MTLRXQMPOCR_OVFPKTCNT       (0x000007FFUL <<  0U) /* (RO) Overflow Packet Counter */
#define CHIP_REGFLD_ETH_MTLRXQMPOCR_OVFCNTOVF       (0x00000001UL << 11U) /* (RO) Overflow Counter Overflow Bit */
#define CHIP_REGFLD_ETH_MTLRXQMPOCR_MISPKTCNT       (0x000007FFUL << 16U) /* (RO) Missed Packet Counter */
#define CHIP_REGFLD_ETH_MTLRXQMPOCR_MISCNTOVF       (0x00000001UL << 27U) /* (RO) Missed Packet Counter Overflow Bit */

/*********** Rx queue debug register (ETH_MTLRXQDR) ***************************/
#define CHIP_REGFLD_ETH_MTLRXQDR_RWCSTS             (0x00000001UL <<  0U) /* (RO) MTL Rx Queue Write Controller Active Status */
#define CHIP_REGFLD_ETH_MTLRXQDR_RRCSTS             (0x00000003UL <<  1U) /* (RO) MTL Rx Queue Read Controller State */
#define CHIP_REGFLD_ETH_MTLRXQDR_RXQSTS             (0x00000003UL <<  4U) /* (RO) MTL Rx Queue Fill-Level Status */
#define CHIP_REGFLD_ETH_MTLRXQDR_PRXQ               (0x00003FFFUL << 16U) /* (RO) Number of Packets in Receive Queue */

#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RXQSTS_EMPTY         0 /* Rx Queue empty */
#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RXQSTS_BELOW_DTHRESH 1 /* Rx Queue fill-level below flow-control deactivate threshold */
#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RXQSTS_ABOVE_ATHRESH 2 /* Rx Queue fill-level above flow-control activate threshold */
#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RXQSTS_FULL          3 /* Rx Queue full */

#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RRCSTS_IDLE      0 /* Idle state */
#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RRCSTS_RD_DATA   1 /* Reading packet data */
#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RRCSTS_RD_STATUS 2 /* Reading packet status (or timestamp) */
#define CHIP_REGFLDVAL_ETH_MTLRXQDR_RRCSTS_FLUSHING  3 /* Flushing the packet data and status */


/* -------------------- Ethernet DMA registers -------------------------------*/
/*********** DMA mode register (ETH_DMAMR) ************************************/
#define CHIP_REGFLD_ETH_DMAMR_SWR                   (0x00000001UL <<  0U) /* (RW) Software Reset */
#define CHIP_REGFLD_ETH_DMAMR_DA                    (0x00000001UL <<  1U) /* (RO) DMA Tx or Rx Arbitration Scheme */
#define CHIP_REGFLD_ETH_DMAMR_TXPR                  (0x00000001UL << 11U) /* (RO) Transmit Priority */
#define CHIP_REGFLD_ETH_DMAMR_PR                    (0x00000007UL << 12U) /* (RO) Priority Ratio */
#define CHIP_REGFLD_ETH_DMAMR_INTM                  (0x00000003UL << 16U) /* (RW) This field defines the interrupt mode */
#define CHIP_REGMSK_ETH_DMAMR_RESERVED              (0xFFFC87FCUL)

#define CHIP_REGFLDVAL_ETH_DMAMR_PR_1_1             0 /* The priority ratio is 1:1 */
#define CHIP_REGFLDVAL_ETH_DMAMR_PR_2_1             1 /* The priority ratio is 2:1 */
#define CHIP_REGFLDVAL_ETH_DMAMR_PR_3_1             2 /* The priority ratio is 3:1 */
#define CHIP_REGFLDVAL_ETH_DMAMR_PR_4_1             3 /* The priority ratio is 4:1 */
#define CHIP_REGFLDVAL_ETH_DMAMR_PR_5_1             4 /* The priority ratio is 5:1 */
#define CHIP_REGFLDVAL_ETH_DMAMR_PR_6_1             5 /* The priority ratio is 6:1 */
#define CHIP_REGFLDVAL_ETH_DMAMR_PR_7_1             6 /* The priority ratio is 7:1 */
#define CHIP_REGFLDVAL_ETH_DMAMR_PR_8_1             7 /* The priority ratio is 8:1 */

/*********** System bus mode register (ETH_DMASBMR) ***************************/
#define CHIP_REGFLD_ETH_DMASBMR_FB                  (0x00000001UL <<  0U) /* (RW) Fixed Burst Length */
#define CHIP_REGFLD_ETH_DMASBMR_AAL                 (0x00000001UL << 12U) /* (RW) Address-Aligned Beats */
#define CHIP_REGFLD_ETH_DMASBMR_MB                  (0x00000001UL << 14U) /* (RO) Mixed Burst */
#define CHIP_REGFLD_ETH_DMASBMR_RB                  (0x00000001UL << 15U) /* (RO) Rebuild INCRx Burst */
#define CHIP_REGMSK_ETH_DMASBMR_RESERVED            (0xFFFF2FFEUL)

/*********** Interrupt status register (ETH_DMAISR) ***************************/
#define CHIP_REGFLD_ETH_DMAISR_DC0IS                (0x00000001UL <<  0U) /* (RO) DMA Channel Interrupt Status */
#define CHIP_REGFLD_ETH_DMAISR_MTLIS                (0x00000001UL << 16U) /* (RO) MTL Interrupt Status */
#define CHIP_REGFLD_ETH_DMAISR_MACIS                (0x00000001UL << 17U) /* (RO) MAC Interrupt Status */
#define CHIP_REGMSK_ETH_DMAISR_RESERVED             (0xFFFCFFFEUL)

/*********** Debug status register (ETH_DMADSR) *******************************/
#define CHIP_REGFLD_ETH_DMADSR_AXWHSTS              (0x00000001UL <<  0U) /* (RO) AHB Master Write Channel*/
#define CHIP_REGFLD_ETH_DMADSR_RPS                  (0x0000000FUL <<  8U) /* (RO) DMA Channel Receive Process State */
#define CHIP_REGFLD_ETH_DMADSR_TPS                  (0x0000000FUL << 12U) /* (RO) DMA Channel Transmit Process State */
#define CHIP_REGMSK_ETH_DMADSR_RESERVED             (0xFFFF00FEUL)

#define CHIP_REGFLDVAL_ETH_DMADSR_TPS_STOPPED       0 /* Stopped (Reset or Stop Transmit Command issued) */
#define CHIP_REGFLDVAL_ETH_DMADSR_TPS_FETCHING      1 /* Running (Fetching Tx Transfer Descriptor) */
#define CHIP_REGFLDVAL_ETH_DMADSR_TPS_WAITING       2 /* Running (Waiting for status) */
#define CHIP_REGFLDVAL_ETH_DMADSR_TPS_READING       3 /* Running (Reading Data from system memory buffer and queuing it to the Tx buffer (Tx FIFO)) */
#define CHIP_REGFLDVAL_ETH_DMADSR_TPS_TIMESTAMP_WR  4 /* Timestamp write state */
#define CHIP_REGFLDVAL_ETH_DMADSR_TPS_SUSPENDED     6 /* Suspended (Tx Descriptor Unavailable or Tx Buffer Underflow) */
#define CHIP_REGFLDVAL_ETH_DMADSR_TPS_CLOSING       7 /* Running (Closing Tx Descriptor) */

#define CHIP_REGFLDVAL_ETH_DMADSR_RPS_STOPPED       0 /* Stopped (Reset or Stop Receive Command issued) */
#define CHIP_REGFLDVAL_ETH_DMADSR_RPS_FETCHING      1 /* Running (Fetching Rx Transfer Descriptor) */
#define CHIP_REGFLDVAL_ETH_DMADSR_RPS_WAITING       3 /* Running (Waiting for status) */
#define CHIP_REGFLDVAL_ETH_DMADSR_RPS_SUSPENDED     4 /* Suspended (Rx Descriptor Unavailable) */
#define CHIP_REGFLDVAL_ETH_DMADSR_RPS_CLOSING       5 /* Running (Closing the Rx Descriptor) */
#define CHIP_REGFLDVAL_ETH_DMADSR_RPS_TIMESTAMP_WR  6 /* Timestamp write state */
#define CHIP_REGFLDVAL_ETH_DMADSR_RPS_TRANSFERRING  7 /* Running (Transferring the received packet data from the Rx buffer to the system memory) */

/*********** Channel control register (ETH_DMACCR) ****************************/
#define CHIP_REGFLD_ETH_DMACCR_MSS                  (0x00003FFFUL <<  0U) /* (RW) Maximum Segment Size */
#define CHIP_REGFLD_ETH_DMACCR_PBLX8                (0x00000001UL << 16U) /* (RW) 8xPBL mode */
#define CHIP_REGFLD_ETH_DMACCR_DSL                  (0x00000007UL << 18U) /* (RW) Descriptor Skip Length (in 32-bit words) */
#define CHIP_REGMSK_ETH_DMACCR_RESERVED             (0xFFE2C000UL)

/*********** Channel transmit control register (ETH_DMACTXCR) *****************/
#define CHIP_REGFLD_ETH_DMACTXCR_ST                 (0x00000001UL <<  0U) /* (RW) Start or Stop Transmission Command */
#define CHIP_REGFLD_ETH_DMACTXCR_OSP                (0x00000001UL <<  4U) /* (RW) Operate on Second Packet */
#define CHIP_REGFLD_ETH_DMACTXCR_TSE                (0x00000001UL << 12U) /* (RW) TCP Segmentation Enabled */
#define CHIP_REGFLD_ETH_DMACTXCR_TPBL               (0x0000003FUL << 16U) /* (RW) Transmit Programmable Burst Length */
#define CHIP_REGMSK_ETH_DMACTXCR_RESERVED           (0xFFC0EFEEUL)

#define CHIP_REGFLDVAL_ETH_DMACTCR_TPBL_1PBL        1  /* Transmit Programmable Burst Length 1 */
#define CHIP_REGFLDVAL_ETH_DMACTCR_TPBL_2PBL        2  /* Transmit Programmable Burst Length 2 */
#define CHIP_REGFLDVAL_ETH_DMACTCR_TPBL_4PBL        4  /* Transmit Programmable Burst Length 4 */
#define CHIP_REGFLDVAL_ETH_DMACTCR_TPBL_8PBL        8  /* Transmit Programmable Burst Length 8 */
#define CHIP_REGFLDVAL_ETH_DMACTCR_TPBL_16PBL       16 /* Transmit Programmable Burst Length 16 */
#define CHIP_REGFLDVAL_ETH_DMACTCR_TPBL_32PBL       32 /* Transmit Programmable Burst Length 32 */

/*********** Channel receive control register (ETH_DMACRXCR) ******************/
#define CHIP_REGFLD_ETH_DMACRXCR_SR                 (0x00000001UL <<  0U) /* (RW) Start or Stop Receive */
#define CHIP_REGFLD_ETH_DMACRXCR_RBSZ               (0x00003FFFUL <<  1U) /* (RW) Receive Buffer size */
#define CHIP_REGFLD_ETH_DMACRXCR_RPBL               (0x0000003FUL << 16U) /* (RW) Receive Programmable Burst Length */
#define CHIP_REGFLD_ETH_DMACRXCR_RPF                (0x00000001UL << 31U) /* (RW) Rx Packet Flush */
#define CHIP_REGMSK_ETH_DMACRXCR_RESERVED           (0x7FC08000UL)

#define CHIP_REGFLDVAL_ETH_DMACRCR_RPBL_1PBL        1 /* Receive Programmable Burst Length 1 */
#define CHIP_REGFLDVAL_ETH_DMACRCR_RPBL_2PBL        2 /* Receive Programmable Burst Length 2 */
#define CHIP_REGFLDVAL_ETH_DMACRCR_RPBL_4PBL        4 /* Receive Programmable Burst Length 4 */
#define CHIP_REGFLDVAL_ETH_DMACRCR_RPBL_8PBL        8 /* Receive Programmable Burst Length 8 */
#define CHIP_REGFLDVAL_ETH_DMACRCR_RPBL_16PBL       16 /* Receive Programmable Burst Length 16 */
#define CHIP_REGFLDVAL_ETH_DMACRCR_RPBL_32PBL       32 /* Receive Programmable Burst Length 32 */

/*********** Channel Tx descriptor list address register (ETH_DMACTXDLAR) *****/
#define CHIP_REGFLD_ETH_DMACTXDLAR_TDESLA           (0x3FFFFFFFUL <<  3U) /* Start of Transmit List */

/*********** Channel Rx descriptor list address register (ETH_DMACRXDLAR) *****/
#define CHIP_REGFLD_ETH_DMACRXDLAR_RDESLA           (0x3FFFFFFFUL <<  3U) /* Start of Receive List */

/*********** Channel Tx descriptor tail pointer register (ETH_DMACTXDTPR) *****/
#define CHIP_REGFLD_ETH_DMACTXDTPR_TDT              (0x3FFFFFFFUL <<  3U) /* Transmit Descriptor Tail Pointer */

/*********** Channel Rx descriptor tail pointer register (ETH_DMACRXDTPR) *****/
#define CHIP_REGFLD_ETH_DMACRXDTPR_RDT              (0x3FFFFFFFUL <<  3U) /* Receive Descriptor Tail Pointer */

/*********** Channel Tx descriptor ring length register (ETH_DMACTXRLR) *******/
#define CHIP_REGFLD_ETH_DMACTXRLR_TDRL              (0x000003FFUL <<  0U) /* Transmit Descriptor Ring Length */

/*********** Channel Rx descriptor ring length register (ETH_DMACRXRLR) *******/
#define CHIP_REGFLD_ETH_DMACRDRLR_RDRL              (0x000003FFUL <<  0U) /* Receive Descriptor Ring Length */

/*********** Channel interrupt enable register (ETH_DMACIER) ******************/
#define CHIP_REGFLD_ETH_DMACIER_TIE                 (0x00000001UL <<  0U) /* (RW) Transmit Interrupt Enable */
#define CHIP_REGFLD_ETH_DMACIER_TXSE                (0x00000001UL <<  1U) /* (RW) Transmit Stopped Enable */
#define CHIP_REGFLD_ETH_DMACIER_TBUE                (0x00000001UL <<  2U) /* (RW) Transmit Buffer Unavailable Enable */
#define CHIP_REGFLD_ETH_DMACIER_RIE                 (0x00000001UL <<  6U) /* (RW) Receive Interrupt Enable */
#define CHIP_REGFLD_ETH_DMACIER_RBUE                (0x00000001UL <<  7U) /* (RW) Receive Buffer Unavailable Enable */
#define CHIP_REGFLD_ETH_DMACIER_RSE                 (0x00000001UL <<  8U) /* (RW) Receive Stopped Enable */
#define CHIP_REGFLD_ETH_DMACIER_RWTE                (0x00000001UL <<  9U) /* (RW) Receive Watchdog Timeout Enable */
#define CHIP_REGFLD_ETH_DMACIER_ETIE                (0x00000001UL << 10U) /* (RW) Early Transmit Interrupt Enable */
#define CHIP_REGFLD_ETH_DMACIER_ERIE                (0x00000001UL << 11U) /* (RW) Early Receive Interrupt Enable */
#define CHIP_REGFLD_ETH_DMACIER_FBEE                (0x00000001UL << 12U) /* (RW) Fatal Bus Error Enable */
#define CHIP_REGFLD_ETH_DMACIER_CDEE                (0x00000001UL << 13U) /* (RW) Context Descriptor Error Enable */
#define CHIP_REGFLD_ETH_DMACIER_AIE                 (0x00000001UL << 14U) /* (RW) Abnormal Interrupt Summary Enable */
#define CHIP_REGFLD_ETH_DMACIER_NIE                 (0x00000001UL << 15U) /* (RW) Normal Interrupt Summary Enable */
#define CHIP_REGMSK_ETH_DMACIER_RESERVED            (0xFFFF0038UL)

/*********** Channel Rx interrupt watchdog timer register (ETH_DMACRXIWTR) ****/
#define CHIP_REGFLD_ETH_DMACRXIWTR_RWT              (0x000000FFUL <<  0U) /* (RW) Receive Interrupt Watchdog Timer Count */
#define CHIP_REGMSK_ETH_DMACRXIWTR_RESERVED         (0xFFFFFF00UL)

/* Channel current application transmit descriptor register (ETH_DMACCATXDR) **/
#define CHIP_REGFLD_ETH_DMACCATXDR_CURTDESAPTR      (0xFFFFFFFFUL <<  0U) /* (RO) Application Transmit Descriptor Address Pointer */

/* Channel current application receive descriptor register (ETH_DMACCARXDR) ***/
#define CHIP_REGFLD_ETH_DMACCARXDR_CURRDESAPTR      (0xFFFFFFFFUL <<  0U) /* (RO) Application Receive Descriptor Address Pointer */

/* Channel current application transmit buffer register (ETH_DMACCATXBR) ******/
#define CHIP_REGFLD_ETH_DMACCATXBR_CURTBUFAPTR      (0xFFFFFFFFUL <<  0U) /* (RO) Application Transmit Buffer Address Pointer */

/* Channel current application receive buffer register (ETH_DMACCARXBR) *******/
#define CHIP_REGFLD_ETH_DMACCARXBR_CURRBUFAPTR      (0xFFFFFFFFUL <<  0U) /* (RO) Application Receive Buffer Address Pointer */

/*********** Channel status register (ETH_DMACSR) *****************************/
#define CHIP_REGFLD_ETH_DMACSR_TI                   (0x00000001UL <<  0U) /* (RW1C) Transmit Interrupt */
#define CHIP_REGFLD_ETH_DMACSR_TPS                  (0x00000001UL <<  1U) /* (RW1C) Transmit Process Stopped */
#define CHIP_REGFLD_ETH_DMACSR_TBU                  (0x00000001UL <<  2U) /* (RW1C) Transmit Buffer Unavailable */
#define CHIP_REGFLD_ETH_DMACSR_RI                   (0x00000001UL <<  6U) /* (RW1C) Receive Interrupt */
#define CHIP_REGFLD_ETH_DMACSR_RBU                  (0x00000001UL <<  7U) /* (RW1C) Receive Buffer Unavailable */
#define CHIP_REGFLD_ETH_DMACSR_RPS                  (0x00000001UL <<  8U) /* (RW1C) Receive Process Stopped */
#define CHIP_REGFLD_ETH_DMACSR_RWT                  (0x00000001UL <<  9U) /* (RW1C) Receive Watchdog Timeout */
#define CHIP_REGFLD_ETH_DMACSR_ETI                  (0x00000001UL << 10U) /* (RW1C) Early Transmit Interrupt */
#define CHIP_REGFLD_ETH_DMACSR_ERI                  (0x00000001UL << 11U) /* (RW1C) Early Receive Interrupt */
#define CHIP_REGFLD_ETH_DMACSR_FBE                  (0x00000001UL << 12U) /* (RW1C) Fatal Bus Error */
#define CHIP_REGFLD_ETH_DMACSR_CDE                  (0x00000001UL << 13U) /* (RW1C) Context Descriptor Error */
#define CHIP_REGFLD_ETH_DMACSR_AIS                  (0x00000001UL << 14U) /* (RW1C) Abnormal Interrupt Summary */
#define CHIP_REGFLD_ETH_DMACSR_NIS                  (0x00000001UL << 15U) /* (RW1C) Normal Interrupt Summary */
#define CHIP_REGFLD_ETH_DMACSR_TEB                  (0x00000007UL << 16U) /* (RO)   Tx DMA Error Bits */
#define CHIP_REGFLD_ETH_DMACSR_REB                  (0x00000007UL << 19U) /* (RO)   Rx DMA Error Bits */

/*********** Channel missed frame count register (ETH_DMACMFCR) ***************/
#define CHIP_REGFLD_ETH_DMACMFCR_MFC                (0x000007FFUL <<  0U) /* (RO) The number of packet counters dropped by the DMA */
#define CHIP_REGFLD_ETH_DMACMFCR_MFCO               (0x00000001UL << 15U) /* (RO) Overflow status of the MFC Counter */

#endif // CHIP_STM32H7XX_REGS_ETH_H_
