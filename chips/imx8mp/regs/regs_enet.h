#ifndef REGS_ENET_H
#define REGS_ENET_H

#include "chip_ioreg_defs.h"

typedef struct {
    uint8_t RESERVED_0[4];
    __IO uint32_t EIR;                               /**< Interrupt Event Register, offset: 0x4 */
    __IO uint32_t EIMR;                              /**< Interrupt Mask Register, offset: 0x8 */
    uint8_t RESERVED_1[4];
    __IO uint32_t RDAR;                              /**< Receive Descriptor Active Register - Ring 0, offset: 0x10 */
    __IO uint32_t TDAR;                              /**< Transmit Descriptor Active Register - Ring 0, offset: 0x14 */
    uint8_t RESERVED_2[12];
    __IO uint32_t ECR;                               /**< Ethernet Control Register, offset: 0x24 */
    uint8_t RESERVED_3[24];
    __IO uint32_t MMFR;                              /**< MII Management Frame Register, offset: 0x40 */
    __IO uint32_t MSCR;                              /**< MII Speed Control Register, offset: 0x44 */
    uint8_t RESERVED_4[28];
    __IO uint32_t MIBC;                              /**< MIB Control Register, offset: 0x64 */
    uint8_t RESERVED_5[28];
    __IO uint32_t RCR;                               /**< Receive Control Register, offset: 0x84 */
    uint8_t RESERVED_6[60];
    __IO uint32_t TCR;                               /**< Transmit Control Register, offset: 0xC4 */
    uint8_t RESERVED_7[28];
    __IO uint32_t PALR;                              /**< Physical Address Lower Register, offset: 0xE4 */
    __IO uint32_t PAUR;                              /**< Physical Address Upper Register, offset: 0xE8 */
    __IO uint32_t OPD;                               /**< Opcode/Pause Duration Register, offset: 0xEC */
    __IO uint32_t TXIC[3];                           /**< Transmit Interrupt Coalescing Register, array offset: 0xF0, array step: 0x4 */
    uint8_t RESERVED_8[4];
    __IO uint32_t RXIC[3];                           /**< Receive Interrupt Coalescing Register, array offset: 0x100, array step: 0x4 */
    uint8_t RESERVED_9[12];
    __IO uint32_t IAUR;                              /**< Descriptor Individual Upper Address Register, offset: 0x118 */
    __IO uint32_t IALR;                              /**< Descriptor Individual Lower Address Register, offset: 0x11C */
    __IO uint32_t GAUR;                              /**< Descriptor Group Upper Address Register, offset: 0x120 */
    __IO uint32_t GALR;                              /**< Descriptor Group Lower Address Register, offset: 0x124 */
    uint8_t RESERVED_10[28];
    __IO uint32_t TFWR;                              /**< Transmit FIFO Watermark Register, offset: 0x144 */
    uint8_t RESERVED_11[24];
    __IO uint32_t RDSR1;                             /**< Receive Descriptor Ring 1 Start Register, offset: 0x160 */
    __IO uint32_t TDSR1;                             /**< Transmit Buffer Descriptor Ring 1 Start Register, offset: 0x164 */
    __IO uint32_t MRBR1;                             /**< Maximum Receive Buffer Size Register - Ring 1, offset: 0x168 */
    __IO uint32_t RDSR2;                             /**< Receive Descriptor Ring 2 Start Register, offset: 0x16C */
    __IO uint32_t TDSR2;                             /**< Transmit Buffer Descriptor Ring 2 Start Register, offset: 0x170 */
    __IO uint32_t MRBR2;                             /**< Maximum Receive Buffer Size Register - Ring 2, offset: 0x174 */
    uint8_t RESERVED_12[8];
    __IO uint32_t RDSR;                              /**< Receive Descriptor Ring 0 Start Register, offset: 0x180 */
    __IO uint32_t TDSR;                              /**< Transmit Buffer Descriptor Ring 0 Start Register, offset: 0x184 */
    __IO uint32_t MRBR;                              /**< Maximum Receive Buffer Size Register - Ring 0, offset: 0x188 */
    uint8_t RESERVED_13[4];
    __IO uint32_t RSFL;                              /**< Receive FIFO Section Full Threshold, offset: 0x190 */
    __IO uint32_t RSEM;                              /**< Receive FIFO Section Empty Threshold, offset: 0x194 */
    __IO uint32_t RAEM;                              /**< Receive FIFO Almost Empty Threshold, offset: 0x198 */
    __IO uint32_t RAFL;                              /**< Receive FIFO Almost Full Threshold, offset: 0x19C */
    __IO uint32_t TSEM;                              /**< Transmit FIFO Section Empty Threshold, offset: 0x1A0 */
    __IO uint32_t TAEM;                              /**< Transmit FIFO Almost Empty Threshold, offset: 0x1A4 */
    __IO uint32_t TAFL;                              /**< Transmit FIFO Almost Full Threshold, offset: 0x1A8 */
    __IO uint32_t TIPG;                              /**< Transmit Inter-Packet Gap, offset: 0x1AC */
    __IO uint32_t FTRL;                              /**< Frame Truncation Length, offset: 0x1B0 */
    uint8_t RESERVED_14[12];
    __IO uint32_t TACC;                              /**< Transmit Accelerator Function Configuration, offset: 0x1C0 */
    __IO uint32_t RACC;                              /**< Receive Accelerator Function Configuration, offset: 0x1C4 */
    __IO uint32_t RCMR[2];                           /**< Receive Classification Match Register for Class n, array offset: 0x1C8, array step: 0x4 */
    uint8_t RESERVED_15[8];
    __IO uint32_t DMACFG[2];                         /**< DMA Class Based Configuration, array offset: 0x1D8, array step: 0x4 */
    __IO uint32_t RDAR1;                             /**< Receive Descriptor Active Register - Ring 1, offset: 0x1E0 */
    __IO uint32_t TDAR1;                             /**< Transmit Descriptor Active Register - Ring 1, offset: 0x1E4 */
    __IO uint32_t RDAR2;                             /**< Receive Descriptor Active Register - Ring 2, offset: 0x1E8 */
    __IO uint32_t TDAR2;                             /**< Transmit Descriptor Active Register - Ring 2, offset: 0x1EC */
    __IO uint32_t QOS;                               /**< QOS Scheme, offset: 0x1F0 */
    uint8_t RESERVED_16[12];
    uint32_t RMON_T_DROP;                       /**< Reserved Statistic Register, offset: 0x200 */
    __I  uint32_t RMON_T_PACKETS;                    /**< Tx Packet Count Statistic Register, offset: 0x204 */
    __I  uint32_t RMON_T_BC_PKT;                     /**< Tx Broadcast Packets Statistic Register, offset: 0x208 */
    __I  uint32_t RMON_T_MC_PKT;                     /**< Tx Multicast Packets Statistic Register, offset: 0x20C */
    __I  uint32_t RMON_T_CRC_ALIGN;                  /**< Tx Packets with CRC/Align Error Statistic Register, offset: 0x210 */
    __I  uint32_t RMON_T_UNDERSIZE;                  /**< Tx Packets Less Than Bytes and Good CRC Statistic Register, offset: 0x214 */
    __I  uint32_t RMON_T_OVERSIZE;                   /**< Tx Packets GT MAX_FL bytes and Good CRC Statistic Register, offset: 0x218 */
    __I  uint32_t RMON_T_FRAG;                       /**< Tx Packets Less Than 64 Bytes and Bad CRC Statistic Register, offset: 0x21C */
    __I  uint32_t RMON_T_JAB;                        /**< Tx Packets Greater Than MAX_FL bytes and Bad CRC Statistic Register, offset: 0x220 */
    __I  uint32_t RMON_T_COL;                        /**< Tx Collision Count Statistic Register, offset: 0x224 */
    __I  uint32_t RMON_T_P64;                        /**< Tx 64-Byte Packets Statistic Register, offset: 0x228 */
    __I  uint32_t RMON_T_P65TO127;                   /**< Tx 65- to 127-byte Packets Statistic Register, offset: 0x22C */
    __I  uint32_t RMON_T_P128TO255;                  /**< Tx 128- to 255-byte Packets Statistic Register, offset: 0x230 */
    __I  uint32_t RMON_T_P256TO511;                  /**< Tx 256- to 511-byte Packets Statistic Register, offset: 0x234 */
    __I  uint32_t RMON_T_P512TO1023;                 /**< Tx 512- to 1023-byte Packets Statistic Register, offset: 0x238 */
    __I  uint32_t RMON_T_P1024TO2047;                /**< Tx 1024- to 2047-byte Packets Statistic Register, offset: 0x23C */
    __I  uint32_t RMON_T_P_GTE2048;                  /**< Tx Packets Greater Than 2048 Bytes Statistic Register, offset: 0x240 */
    __I  uint32_t RMON_T_OCTETS;                     /**< Tx Octets Statistic Register, offset: 0x244 */
    uint32_t IEEE_T_DROP;                       /**< Reserved Statistic Register, offset: 0x248 */
    __I  uint32_t IEEE_T_FRAME_OK;                   /**< Frames Transmitted OK Statistic Register, offset: 0x24C */
    __I  uint32_t IEEE_T_1COL;                       /**< Frames Transmitted with Single Collision Statistic Register, offset: 0x250 */
    __I  uint32_t IEEE_T_MCOL;                       /**< Frames Transmitted with Multiple Collisions Statistic Register, offset: 0x254 */
    __I  uint32_t IEEE_T_DEF;                        /**< Frames Transmitted after Deferral Delay Statistic Register, offset: 0x258 */
    __I  uint32_t IEEE_T_LCOL;                       /**< Frames Transmitted with Late Collision Statistic Register, offset: 0x25C */
    __I  uint32_t IEEE_T_EXCOL;                      /**< Frames Transmitted with Excessive Collisions Statistic Register, offset: 0x260 */
    __I  uint32_t IEEE_T_MACERR;                     /**< Frames Transmitted with Tx FIFO Underrun Statistic Register, offset: 0x264 */
    __I  uint32_t IEEE_T_CSERR;                      /**< Frames Transmitted with Carrier Sense Error Statistic Register, offset: 0x268 */
    __I  uint32_t IEEE_T_SQE;                        /**< Reserved Statistic Register, offset: 0x26C */
    __I  uint32_t IEEE_T_FDXFC;                      /**< Flow Control Pause Frames Transmitted Statistic Register, offset: 0x270 */
    __I  uint32_t IEEE_T_OCTETS_OK;                  /**< Octet Count for Frames Transmitted w/o Error Statistic Register, offset: 0x274 */
    uint8_t RESERVED_17[12];
    __I  uint32_t RMON_R_PACKETS;                    /**< Rx Packet Count Statistic Register, offset: 0x284 */
    __I  uint32_t RMON_R_BC_PKT;                     /**< Rx Broadcast Packets Statistic Register, offset: 0x288 */
    __I  uint32_t RMON_R_MC_PKT;                     /**< Rx Multicast Packets Statistic Register, offset: 0x28C */
    __I  uint32_t RMON_R_CRC_ALIGN;                  /**< Rx Packets with CRC/Align Error Statistic Register, offset: 0x290 */
    __I  uint32_t RMON_R_UNDERSIZE;                  /**< Rx Packets with Less Than 64 Bytes and Good CRC Statistic Register, offset: 0x294 */
    __I  uint32_t RMON_R_OVERSIZE;                   /**< Rx Packets Greater Than MAX_FL and Good CRC Statistic Register, offset: 0x298 */
    __I  uint32_t RMON_R_FRAG;                       /**< Rx Packets Less Than 64 Bytes and Bad CRC Statistic Register, offset: 0x29C */
    __I  uint32_t RMON_R_JAB;                        /**< Rx Packets Greater Than MAX_FL Bytes and Bad CRC Statistic Register, offset: 0x2A0 */
    uint32_t RMON_R_RESVD_0;                    /**< Reserved Statistic Register, offset: 0x2A4 */
    __I  uint32_t RMON_R_P64;                        /**< Rx 64-Byte Packets Statistic Register, offset: 0x2A8 */
    __I  uint32_t RMON_R_P65TO127;                   /**< Rx 65- to 127-Byte Packets Statistic Register, offset: 0x2AC */
    __I  uint32_t RMON_R_P128TO255;                  /**< Rx 128- to 255-Byte Packets Statistic Register, offset: 0x2B0 */
    __I  uint32_t RMON_R_P256TO511;                  /**< Rx 256- to 511-Byte Packets Statistic Register, offset: 0x2B4 */
    __I  uint32_t RMON_R_P512TO1023;                 /**< Rx 512- to 1023-Byte Packets Statistic Register, offset: 0x2B8 */
    __I  uint32_t RMON_R_P1024TO2047;                /**< Rx 1024- to 2047-Byte Packets Statistic Register, offset: 0x2BC */
    __I  uint32_t RMON_R_P_GTE2048;                  /**< Rx Packets Greater than 2048 Bytes Statistic Register, offset: 0x2C0 */
    __I  uint32_t RMON_R_OCTETS;                     /**< Rx Octets Statistic Register, offset: 0x2C4 */
    __I  uint32_t IEEE_R_DROP;                       /**< Frames not Counted Correctly Statistic Register, offset: 0x2C8 */
    __I  uint32_t IEEE_R_FRAME_OK;                   /**< Frames Received OK Statistic Register, offset: 0x2CC */
    __I  uint32_t IEEE_R_CRC;                        /**< Frames Received with CRC Error Statistic Register, offset: 0x2D0 */
    __I  uint32_t IEEE_R_ALIGN;                      /**< Frames Received with Alignment Error Statistic Register, offset: 0x2D4 */
    __I  uint32_t IEEE_R_MACERR;                     /**< Receive FIFO Overflow Count Statistic Register, offset: 0x2D8 */
    __I  uint32_t IEEE_R_FDXFC;                      /**< Flow Control Pause Frames Received Statistic Register, offset: 0x2DC */
    __I  uint32_t IEEE_R_OCTETS_OK;                  /**< Octet Count for Frames Received without Error Statistic Register, offset: 0x2E0 */
    uint8_t RESERVED_18[284];
    __IO uint32_t ATCR;                              /**< Adjustable Timer Control Register, offset: 0x400 */
    __IO uint32_t ATVR;                              /**< Timer Value Register, offset: 0x404 */
    __IO uint32_t ATOFF;                             /**< Timer Offset Register, offset: 0x408 */
    __IO uint32_t ATPER;                             /**< Timer Period Register, offset: 0x40C */
    __IO uint32_t ATCOR;                             /**< Timer Correction Register, offset: 0x410 */
    __IO uint32_t ATINC;                             /**< Time-Stamping Clock Period Register, offset: 0x414 */
    __I  uint32_t ATSTMP;                            /**< Timestamp of Last Transmitted Frame, offset: 0x418 */
    uint8_t RESERVED_19[488];
    __IO uint32_t TGSR;                              /**< Timer Global Status Register, offset: 0x604 */
    struct {                                         /* offset: 0x608, array step: 0x8 */
        __IO uint32_t TCSR;                              /**< Timer Control Status Register, array offset: 0x608, array step: 0x8 */
        __IO uint32_t TCCR;                              /**< Timer Compare Capture Register, array offset: 0x60C, array step: 0x8 */
    } CHANNEL[4];
} CHIP_REGS_ENET_T;

typedef struct {
    uint16_t STATUS0;
    uint16_t DATALEN;
    uint16_t BUFADDR_H;
    uint16_t BUFADDR_L;
    uint16_t STATUS1;
    uint16_t STATUS2;
    uint16_t STATUS3;
    uint16_t PL_CHKSUM;
    uint16_t STATUS4;
    uint16_t RESERVED0;
    uint16_t TSTMP_H;
    uint16_t TSTMP_L;
    uint16_t RESERVED1[4];
} CHIP_ENET_RXBD_T;


typedef struct {
    uint16_t STATUS0;
    uint16_t DATALEN;
    uint16_t BUFADDR_H;
    uint16_t BUFADDR_L;
    uint16_t STATUS1;
    uint16_t STATUS2;
    uint16_t TLT_H;
    uint16_t TLT_L;
    uint16_t STATUS4;
    uint16_t RESERVED0;
    uint16_t TSTMP_H;
    uint16_t TSTMP_L;
    uint16_t RESERVED1[4];
} CHIP_ENET_TXBD_T;

#define CHIP_REGS_ENET1             ((CHIP_REGS_ENET_T *)CHIP_MEMRGN_ADDR_PERIPH_ENET1)

/************ Enhanced uDMA receive buffer descriptor *************************/
#define CHIP_ENET_RXBD_STATUS0_EMPTY                (0x0001UL << 15U) /* (RW) Empty. Written by the MAC (= 0) and user (= 1). */
#define CHIP_ENET_RXBD_STATUS0_RO1                  (0x0001UL << 14U) /* Reserved for use by software (not modified by HW) */
#define CHIP_ENET_RXBD_STATUS0_WRAP                 (0x0001UL << 13U) /* (W)  Wrap (end of ring). Written by user. 1=> 1 The next buffer descriptor is found at the location defined in ENETn_RDSR */
#define CHIP_ENET_RXBD_STATUS0_RO2                  (0x0001UL << 12U) /* Reserved for use by software (not modified by HW) */
#define CHIP_ENET_RXBD_STATUS0_LAST                 (0x0001UL << 11U) /* (R) The buffer is the last in a frame. Written by the uDMA. */
#define CHIP_ENET_RXBD_STATUS0_MISS                 (0x0001UL <<  8U) /* (R) Miss. Written by the MAC. 1 - frame accepted in promiscuous mode, but flagged as a miss by the internal address recognition */
#define CHIP_ENET_RXBD_STATUS0_BCAST                (0x0001UL <<  7U) /* (R) Set if the DA is broadcast */
#define CHIP_ENET_RXBD_STATUS0_MCAST                (0x0001UL <<  6U) /* (R) Set if the DA is multicast and not BC. */
#define CHIP_ENET_RXBD_STATUS0_ERR_LG               (0x0001UL <<  5U) /* (R) Receive frame length violation. A frame length greater than RCR[MAX_FL] was recognized. */
#define CHIP_ENET_RXBD_STATUS0_ERR_NO               (0x0001UL <<  4U) /* (R) Receive non-octet aligned frame.  A frame that contained a number of bits not divisible by 8 was received */
#define CHIP_ENET_RXBD_STATUS0_ERR_CRC              (0x0001UL <<  2U) /* (R) Receive CRC or frame error */
#define CHIP_ENET_RXBD_STATUS0_ERR_OVRN             (0x0001UL <<  1U) /* (R) A receive FIFO overrun occurred during frame reception */
#define CHIP_ENET_RXBD_STATUS1_ERR_MAC              (0x0001UL << 15U) /* (R) MAC Error. Frame stored in the system memory was received with an error */
#define CHIP_ENET_RXBD_STATUS1_ERR_PHY              (0x0001UL << 10U) /* (R) PHY Error. Set to "1"when the frame was received with an Error character on the PHY interface. */
#define CHIP_ENET_RXBD_STATUS1_ERR_COL              (0x0001UL <<  9U) /* (R) Frame was received with a collision detected during reception. */
#define CHIP_ENET_RXBD_STATUS1_UCAST                (0x0001UL <<  8U) /* (R) Frame is unicast */
#define CHIP_ENET_RXBD_STATUS1_INT                  (0x0001UL <<  7U) /* (W) Generate RXB/RXF interrupt. */
#define CHIP_ENET_RXBD_STATUS2_VLAN_PCP             (0x0007UL << 13U) /* (R) VLAN priority code point (7 - highest) */
#define CHIP_ENET_RXBD_STATUS2_ERR_IPHDR_CHK        (0x0001UL <<  5U) /* (R) IP header checksum error (accelerator option) */
#define CHIP_ENET_RXBD_STATUS2_ERR_PROTO_CHK        (0x0001UL <<  4U) /* (R) Protocol checksum error (accelerator option) */
#define CHIP_ENET_RXBD_STATUS2_VLAN                 (0x0001UL <<  2U) /* (R) Frame has a VLAN tag (accelerator option) */
#define CHIP_ENET_RXBD_STATUS2_IPV6                 (0x0001UL <<  1U) /* (R) Frame has an IPv6 frame type (accelerator option) */
#define CHIP_ENET_RXBD_STATUS2_FRAG                 (0x0001UL <<  0U) /* (R) Frame is an IPv4 fragment frame */
#define CHIP_ENET_RXBD_STATUS3_HDR_LEN              (0x001FUL << 11U) /* (R) Header length (accelerator option). Sum of 32-bit words found within the IP and its following protocol headers. */
#define CHIP_ENET_RXBD_STATUS3_PROTO_TYPE           (0x00FFUL <<  0U) /* (R) Protocol type (accelerator option). 8-bit protocol field found within the IP header of the frame */
#define CHIP_ENET_RXBD_STATUS4_BDU                  (0x0001UL << 15U) /* (RW) Last buffer descriptor update done. Indicates that the last BD data has been updated by uDMA. This field is written by the user (=0) and uDMA (=1).*/

/************ Enhanced transmit buffer descriptor (TxBD) **********************/
#define CHIP_ENET_TXBD_STATUS0_RDY                  (0x0001UL << 15U) /* (RW) Ready. Written by the MAC (=0) and user (=1). */
#define CHIP_ENET_TXBD_STATUS0_TO1                  (0x0001UL << 14U) /* Reserved for software use  */
#define CHIP_ENET_TXBD_STATUS0_WRAP                 (0x0001UL << 13U) /* (W)  Wrap (end of ring). Written by user. 1=> 1 The next buffer descriptor is found at the location defined in ENETn_ETDSR */
#define CHIP_ENET_TXBD_STATUS0_TO2                  (0x0001UL << 12U) /* Reserved for software use  */
#define CHIP_ENET_TXBD_STATUS0_LAST                 (0x0001UL << 11U) /* (W) The buffer is the last in a frame. Written by the user. */
#define CHIP_ENET_TXBD_STATUS0_TX_CRC               (0x0001UL << 10U) /* (W) Transmit the CRC sequence after the last data byte */
#define CHIP_ENET_TXBD_STATUS0_ADD_BAD_CRC          (0x0001UL <<  9U) /* (W) Append bad CRC (not supported) */
#define CHIP_ENET_TXBD_STATUS1_INT                  (0x0001UL << 14U) /* (W) Generate interrupt flags. Must be the same for all EBD for a given frame */
#define CHIP_ENET_TXBD_STATUS1_TSTMP                (0x0001UL << 13U) /* (W) Generate a timestamp frame to the MAC. Must be the same for all EBD for a given frame */
#define CHIP_ENET_TXBD_STATUS1_PROTO_CHK_INS        (0x0001UL << 12U) /* (W) Insert protocol specific checksum. MAC's IP accelerator calculates the protocol checksum and overwrites the corresponding checksum field with the calculated value (must be 0 in data).  */
#define CHIP_ENET_TXBD_STATUS1_IP_CHK_INS           (0x0001UL << 11U) /* (W) Insert IP header checksum. f set, the MAC's IP accelerator calculates the IP header checksum and overwrites the corresponding header field with the calculated value (must be 0 in data). */
#define CHIP_ENET_TXBD_STATUS1_USE_TLT              (0x0001UL <<  8U) /* (W) Use transmit launch time. This field must only be set in the first BD of a frame (DMA class must be enabled) */
#define CHIP_ENET_TXBD_STATUS1_FRAME_TYPE           (0x000FUL <<  4U) /* (W) Type of frame to be transmitted (0 - Non AVB (ENET_TDSR), 1 - AVB Class A (ENET_TDSR1), 2 - AVG Clss B (ENET_TDSR2) */
#define CHIP_ENET_TXBD_STATUS2_TX_ERR               (0x0001UL << 15U) /* (R) Transmit error occurred (OR of all err bits) */
#define CHIP_ENET_TXBD_STATUS2_ERR_UNFL             (0x0001UL << 13U) /* (R) Underflow error.  This field indicates that the MAC reported an underflow error on transmit. */
#define CHIP_ENET_TXBD_STATUS2_ERR_EXC_COL          (0x0001UL << 12U) /* (R) Excess Collision error. This field indicates that the MAC reported an excess collision error on transmit.  */
#define CHIP_ENET_TXBD_STATUS2_ERR_FRAME            (0x0001UL << 11U) /* (R) Frame with error. uDMA reported an error when providing the packet. */
#define CHIP_ENET_TXBD_STATUS2_ERR_LATE_COL         (0x0001UL << 10U) /* (R) Late collision error. MAC reported that there was a Late Collision on transmit. */
#define CHIP_ENET_TXBD_STATUS2_ERR_OVFL             (0x0001UL <<  9U) /* (R) Overflow error. MAC reported that there was a FIFO overflow condition on transmit. */
#define CHIP_ENET_TXBD_STATUS2_ERR_TSTMP            (0x0001UL <<  8U) /* (R) Timestamp error. MAC reported a different frame type then a timestamp frame */
#define CHIP_ENET_TXBD_STATUS4_BDU                  (0x0001UL << 15U) /* (RW) Last buffer descriptor update done. Indicates that the last BD data has been updated by uDMA. This field is written by the user (=0) and uDMA (=1).*/


/***********  Interrupt Event Register (EIR) **********************************/
#define CHIP_REGFLD_ENET_EIR_BABR                   (0x00000001UL << 30U) /* (RW1C) Babbling Receive Error (rx length > RCR[MAX_FL]) */
#define CHIP_REGFLD_ENET_EIR_BABT                   (0x00000001UL << 29U) /* (RW1C) Babbling Transmit Error (tx length > RCR[MAX_FL]) */
#define CHIP_REGFLD_ENET_EIR_GRA                    (0x00000001UL << 28U) /* (RW1C) Graceful Stop Complete (transmitter is put into a pause state after completion of the frame currently being transmitted) */
#define CHIP_REGFLD_ENET_EIR_TXF                    (0x00000001UL << 27U) /* (RW1C) Transmit Frame Interrupt (a frame has been transmitted and the last corresponding buffer descriptor has been updated) */
#define CHIP_REGFLD_ENET_EIR_TXB                    (0x00000001UL << 26U) /* (RW1C) Transmit Buffer Interrupt (transmit buffer descriptor has been updated) */
#define CHIP_REGFLD_ENET_EIR_RXF                    (0x00000001UL << 25U) /* (RW1C) Receive Frame Interrupt (frame has been received and the last corresponding buffer descriptor has been updated) */
#define CHIP_REGFLD_ENET_EIR_RXB                    (0x00000001UL << 24U) /* (RW1C) Receive Buffer Interrupt (receive buffer descriptor is not the last in the frame has been updated) */
#define CHIP_REGFLD_ENET_EIR_MII                    (0x00000001UL << 23U) /* (RW1C) MII Interrupt (the MII has completed the data transfer requested) */
#define CHIP_REGFLD_ENET_EIR_EBERR                  (0x00000001UL << 22U) /* (RW1C) Ethernet Bus Error (a system bus error occurred when a uDMA transaction is underway) */
#define CHIP_REGFLD_ENET_EIR_LC                     (0x00000001UL << 21U) /* (RW1C) Late Collision (hd: a collision occurred beyond the collision window (slot time))  */
#define CHIP_REGFLD_ENET_EIR_RL                     (0x00000001UL << 20U) /* (RW1C) Collision Retry Limit (hd: collision occurred on each of 16 successive attempts to transmit the frame)  */
#define CHIP_REGFLD_ENET_EIR_UN                     (0x00000001UL << 19U) /* (RW1C) Transmit FIFO Underrun (transmit FIFO became empty before the complete frame was transmitted) */
#define CHIP_REGFLD_ENET_EIR_PLR                    (0x00000001UL << 18U) /* (RW1C) Payload Receive Error */
#define CHIP_REGFLD_ENET_EIR_WAKEUP                 (0x00000001UL << 17U) /* (RO)   Node Wakeup Request Indication (a magic packet has been detected if ECR[MAGICEN] is set) */
#define CHIP_REGFLD_ENET_EIR_TS_AVAIL               (0x00000001UL << 16U) /* (RW1C) Transmit Timestamp Available ( timestamp of the last transmitted timing frame is available in the ATSTMP register) */
#define CHIP_REGFLD_ENET_EIR_TS_TIMER               (0x00000001UL << 15U) /* (RW1C) Timestamp Timer (The adjustable timer reached the period event) */
#define CHIP_REGFLD_ENET_EIR_RXFLUSH_2              (0x00000001UL << 14U) /* (RW1C) RX DMA Ring 2 flush indication */
#define CHIP_REGFLD_ENET_EIR_RXFLUSH_1              (0x00000001UL << 13U) /* (RW1C) RX DMA Ring 1 flush indication */
#define CHIP_REGFLD_ENET_EIR_RXFLUSH_0              (0x00000001UL << 12U) /* (RW1C) RX DMA Ring 0 flush indication */
#define CHIP_REGFLD_ENET_EIR_TXF2                   (0x00000001UL <<  7U) /* (RW1C) Transmit frame interrupt, class 2 */
#define CHIP_REGFLD_ENET_EIR_TXB2                   (0x00000001UL <<  6U) /* (RW1C) Transmit buffer interrupt, class 2 */
#define CHIP_REGFLD_ENET_EIR_RXF2                   (0x00000001UL <<  5U) /* (RW1C) Receive frame interrupt, class 2 */
#define CHIP_REGFLD_ENET_EIR_RXB2                   (0x00000001UL <<  4U) /* (RW1C) Receive buffer interrupt, class 2 */
#define CHIP_REGFLD_ENET_EIR_TXF1                   (0x00000001UL <<  3U) /* (RW1C) Transmit frame interrupt, class 1 */
#define CHIP_REGFLD_ENET_EIR_TXB1                   (0x00000001UL <<  2U) /* (RW1C) Transmit buffer interrupt, class 1 */
#define CHIP_REGFLD_ENET_EIR_RXF1                   (0x00000001UL <<  1U) /* (RW1C) Receive frame interrupt, class 1 */
#define CHIP_REGFLD_ENET_EIR_RXB1                   (0x00000001UL <<  0U) /* (RW1C) Receive buffer interrupt, class 1 */
#define CHIP_REGMSK_ENET_EIR_RESERVED               (0x80000F00UL)

/***********  Interrupt Mask Register (EIMR) **********************************/
#define CHIP_REGFLD_ENET_EIMR_BABR                  (0x00000001UL << 30U) /* (RW) BABR Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_BABT                  (0x00000001UL << 29U) /* (RW) BABT Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_GRA                   (0x00000001UL << 28U) /* (RW) GRA Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_TXF                   (0x00000001UL << 27U) /* (RW) TXF Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_TXB                   (0x00000001UL << 26U) /* (RW) TXB Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXF                   (0x00000001UL << 25U) /* (RW) RXF Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXB                   (0x00000001UL << 24U) /* (RW) RXB Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_MII                   (0x00000001UL << 23U) /* (RW) MII Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_EBERR                 (0x00000001UL << 22U) /* (RW) EBERR Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_LC                    (0x00000001UL << 21U) /* (RW) LC Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RL                    (0x00000001UL << 20U) /* (RW) RL Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_UN                    (0x00000001UL << 19U) /* (RW) UN Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_PLR                   (0x00000001UL << 18U) /* (RW) PLR Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_WAKEUP                (0x00000001UL << 17U) /* (RW) WAKEUP Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_TS_AVAIL              (0x00000001UL << 16U) /* (RW) TS_AVAIL Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_TS_TIMER              (0x00000001UL << 15U) /* (RW) TS_TIMER Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXFLUSH_2             (0x00000001UL << 14U) /* (RW) RXFLUSH_2 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXFLUSH_1             (0x00000001UL << 13U) /* (RW) RXFLUSH_1 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXFLUSH_0             (0x00000001UL << 12U) /* (RW) RXFLUSH_0 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_TXF2                  (0x00000001UL <<  7U) /* (RW) TXF2 Interrupt Mask *//
#define CHIP_REGFLD_ENET_EIMR_TXB2                  (0x00000001UL <<  6U) /* (RW) TXB2 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXF2                  (0x00000001UL <<  5U) /* (RW) RXF2 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXB2                  (0x00000001UL <<  4U) /* (RW) RXB2 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_TXF1                  (0x00000001UL <<  3U) /* (RW) TXF1 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_TXB1                  (0x00000001UL <<  2U) /* (RW) TXB1 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXF1                  (0x00000001UL <<  1U) /* (RW) RXF1 Interrupt Mask */
#define CHIP_REGFLD_ENET_EIMR_RXB1                  (0x00000001UL <<  0U) /* (RW) RXB1 Interrupt Mask */
#define CHIP_REGMSK_ENET_EIMR_RESERVED              (0x80000F00UL)


/***********  Receive Descriptor Active Register - Ring n (RDARn, n=0..2) *****/
#define CHIP_REGFLD_ENET_RDAR_RDAR                  (0x00000001UL << 24U) /* (RW1SC) Receive Descriptor Active (Always set to 1 when this register is written) */
#define CHIP_REGMSK_ENET_RDAR_RESERVED              (0xFEFFFFFFUL)

/***********  Transmit Descriptor Active Register - Ring n (TDARn, n=0..2) ****/
#define CHIP_REGFLD_ENET_TDAR_TDAR                  (0x00000001UL << 24U) /* (RW1SC) Transmit Descriptor Active (Always set to 1 when this register is written) */
#define CHIP_REGMSK_ENET_TDAR_RESERVED              (0xFEFFFFFFUL)

/***********  Ethernet Control Register (ECR) *********************************/
#define CHIP_REGFLD_ENET_ECR_SVLANDBL               (0x00000001UL << 11U) /* (RW) S-VLAN double tag (If enabled, S-VLAN detection requires a double-tagged frame to define a frame as being a VLAN frame) */
#define CHIP_REGFLD_ENET_ECR_VLANUSE2ND             (0x00000001UL << 10U) /* (RW) VLAN use second tag (0 - extract data from first VLAN, 1 - from second VLAN) */
#define CHIP_REGFLD_ENET_ECR_SVLANEN                (0x00000001UL <<  9U) /* (RW) S-VLAN enable (Enable additional detection of S-VLAN tag according to IEEE802.1Q) */
#define CHIP_REGFLD_ENET_ECR_DBSWP                  (0x00000001UL <<  8U) /* (RW) Descriptor Byte Swapping Enable (must be set to 1 to support little-endian devices) */
#define CHIP_REGFLD_ENET_ECR_DBGEN                  (0x00000001UL <<  6U) /* (RW) Debug Enable (MAC enters hardware freeze mode when the processor is in debug mode) */
#define CHIP_REGFLD_ENET_ECR_SPEED                  (0x00000001UL <<  5U) /* (RW) Selects between 10/100-Mbit/s (0) and 1000-Mbit/s (1) modes of operation. */
#define CHIP_REGFLD_ENET_ECR_EN1588                 (0x00000001UL <<  4U) /* (RW) Enables enhanced functionality of the MAC (Enhanced frame time-stamping functions enabled) */
#define CHIP_REGFLD_ENET_ECR_SLEEP                  (0x00000001UL <<  3U) /* (RW) Sleep Mode Enable */
#define CHIP_REGFLD_ENET_ECR_MAGICEN                (0x00000001UL <<  2U) /* (RW) Magic Packet Detection Enable */
#define CHIP_REGFLD_ENET_ECR_ETHEREN                (0x00000001UL <<  1U) /* (RW) Ethernet Enable (must be set at the very last step during ENET configuration) */
#define CHIP_REGFLD_ENET_ECR_RESET                  (0x00000001UL <<  0U) /* (RW) When this field is set, it clears the ETHEREN field. */
#define CHIP_REGMSK_ENET_ECR_RESERVED               (0xFFFFF080UL)
#define CHIP_REGMSK_ENET_ECR_W1                     (0x70000000UL)

/*********** MII Management Frame Register (MMFR) *****************************/
#define CHIP_REGFLD_ENET_MMFR_ST                    (0x00000003UL << 30U) /* (RW) Start Of Frame Delimiter */
#define CHIP_REGFLD_ENET_MMFR_OP                    (0x00000003UL << 28U) /* (RW) Operation Code */
#define CHIP_REGFLD_ENET_MMFR_PA                    (0x0000001FUL << 23U) /* (RW) PHY Address */
#define CHIP_REGFLD_ENET_MMFR_RA                    (0x0000001FUL << 18U) /* (RW) Register Address */
#define CHIP_REGFLD_ENET_MMFR_TA                    (0x00000003UL << 16U) /* (RW) Turn Around (must be 10) */
#define CHIP_REGFLD_ENET_MMFR_DATA                  (0x0000FFFFUL <<  0U) /* (RW) Management Frame Data */


/*********** MII Speed Control Register (MSCR) ********************************/
#define CHIP_REGFLD_ENET_MSCR_HOLDTIME              (0x00000007UL <<  8U) /* (RW) Hold time On MDIO Output (fld + 1 module clock cycles) */
#define CHIP_REGFLD_ENET_MSCR_DIS_PRE               (0x00000001UL <<  7U) /* (RW) Disable Preamble */
#define CHIP_REGFLD_ENET_MSCR_MII_SPEED             (0x0000003FUL <<  1U) /* (RW) MII Speed (0 - off, others - div = ((MII_SPEED + 1) x 2) */
#define CHIP_REGMSK_ENET_MSCR_RESERVED              (0xFFFFF801UL)


/*********** MIB Control Register (MIBC) **************************************/
#define CHIP_REGFLD_ENET_MIBC_MIB_DIS               (0x00000001UL << 31U) /* (RW) Disable MIB Logic (stop update counters) */
#define CHIP_REGFLD_ENET_MIBC_MIB_IDLE              (0x00000001UL << 30U) /* (RW) MIB Idle ( The MIB block is not currently updating any MIB counters) */
#define CHIP_REGFLD_ENET_MIBC_MIB_CLEAR             (0x00000001UL << 29U) /* (RW) MIB Clear (not self cleared!) */
#define CHIP_REGMSK_ENET_MIBC_RESERVED              (0x1FFFFFFFUL)


/*********** Receive Control Register (RCR) ***********************************/
#define CHIP_REGFLD_ENET_RCR_GRS                    (0x00000001UL << 31U) /* (RO) Read-only status indicating that the MAC receive datapath is stopped. */
#define CHIP_REGFLD_ENET_RCR_NLC                    (0x00000001UL << 30U) /* (RW) Payload Length Check Disable (0 - disabled, 1 - enabled). */
#define CHIP_REGFLD_ENET_RCR_MAX_FL                 (0x00003FFFUL << 16U) /* (RW) Maximum Frame Length (default 1518, must be changed to 1522 for VLAN?) */
#define CHIP_REGFLD_ENET_RCR_CFEN                   (0x00000001UL << 15U) /* (RW) MAC Control Frame Enable (all opcodes othern than 0x0001: 0 - forward, 1 - discarded) */
#define CHIP_REGFLD_ENET_RCR_CRCFWD                 (0x00000001UL << 14U) /* (RW) Terminate/Forward Received CRC (0 - transmitted to app, 1 - stripped) */
#define CHIP_REGFLD_ENET_RCR_PAUFWD                 (0x00000001UL << 13U) /* (RW) Terminate/Forward Pause Frames (0 - discard, 1 - forwarded) */
#define CHIP_REGFLD_ENET_RCR_PADEN                  (0x00000001UL << 12U) /* (RW) Enable Frame Padding Remove On Receive (1 - remove) */
#define CHIP_REGFLD_ENET_RCR_RMII_10T               (0x00000001UL <<  9U) /* (RW) Enables 10-Mbit/s mode of the RMII or RGMII . */
#define CHIP_REGFLD_ENET_RCR_RMII_MODE              (0x00000001UL <<  8U) /* (RW) RMII Mode Enable (0 - MII/RGMII, 1 - RMII)  */
#define CHIP_REGFLD_ENET_RCR_RGMII_EN               (0x00000001UL <<  6U) /* (RW) RGMII Mode Enable */
#define CHIP_REGFLD_ENET_RCR_FCE                    (0x00000001UL <<  5U) /* (RW) Flow Control Enable (enable receiver process PAUSE frame and stop transmission) */
#define CHIP_REGFLD_ENET_RCR_BC_REJ                 (0x00000001UL <<  4U) /* (RW) Broadcast Frame Reject */
#define CHIP_REGFLD_ENET_RCR_PROM                   (0x00000001UL <<  3U) /* (RW) Promiscuous Mode (All frames are accepted regardless of address matching) */
#define CHIP_REGFLD_ENET_RCR_MII_MODE               (0x00000001UL <<  2U) /* (RW) Media Independent Interface Mode (must be set to 1) */
#define CHIP_REGFLD_ENET_RCR_DRT                    (0x00000001UL <<  1U) /* (RW) Disable Receive On Transmit (Normally used for half-duplex mode) */
#define CHIP_REGFLD_ENET_RCR_LOOP                   (0x00000001UL <<  0U) /* (RW) Internal Loopback (must be set MII_MODE=1, RMII_MODE=0) */
#define CHIP_REGMSK_ENET_RCR_RESERVED               (0x00000C80UL)

#define CHIP_REGFLDVAL_ENET_RCR_NLC_DIS             0
#define CHIP_REGFLDVAL_ENET_RCR_NLC_EN              1

#define CHIP_REGFLDVAL_ENET_RCR_CFEN_FWD            0
#define CHIP_REGFLDVAL_ENET_RCR_CFEN_DISCARD        1

#define CHIP_REGFLDVAL_ENET_RCR_CRCFWD_FWD          0
#define CHIP_REGFLDVAL_ENET_RCR_CRCFWD_DISCARD      1

#define CHIP_REGFLDVAL_ENET_RCR_PAUFWD_DISCARD      0
#define CHIP_REGFLDVAL_ENET_RCR_PAUFWD_FWD          1

#define CHIP_REGFLDVAL_ENET_RCR_PADEN_FWD           0
#define CHIP_REGFLDVAL_ENET_RCR_PADEN_REMOVE        1


/*********** Transmit Control Register (TCR) **********************************/
#define CHIP_REGFLD_ENET_TCR_CRCFWD                 (0x00000001UL <<  9U) /* (RW) Forward Frame From Application With CRC (0 -> TxBD[TC], 1 - FWD from application) */
#define CHIP_REGFLD_ENET_TCR_ADDINS                 (0x00000001UL <<  8U) /* (RW) Set MAC Address On Transmit (1 - overwrite source MAC  according to ADDSEL) */
#define CHIP_REGFLD_ENET_TCR_ADDSEL                 (0x00000007UL <<  5U) /* (RW) Source MAC Address Select On Transmit (0 - Node MAC address programmed on PADDR1/2 registers, others - reserved) */
#define CHIP_REGFLD_ENET_TCR_RFC_PAUSE              (0x00000001UL <<  4U) /* (RO) Receive Frame Control Pause (set on PAUSE rx, clear on finish pause) */
#define CHIP_REGFLD_ENET_TCR_TFC_PAUSE              (0x00000001UL <<  3U) /* (RW) Transmit Frame Control Pause (The MAC stops transmission of data frames after the current transmission is complete) */
#define CHIP_REGFLD_ENET_TCR_FDEN                   (0x00000001UL <<  2U) /* (RW) Full-Duplex Enable (modify this bit when ECR[ETHEREN] is cleared). */
#define CHIP_REGFLD_ENET_TCR_GTS                    (0x00000001UL <<  0U) /* (RW) Graceful Transmit Stop (stop request, done when EIR[GRA] is set) */
#define CHIP_REGMSK_ENET_TCR_RESERVED               (0xFFFFF802UL)


/*********** Physical Address Lower Register (PALR) ***************************/
#define CHIP_REGFLD_ENET_PALR_PADDR1                (0xFFFFFFFFUL <<  0U) /* (RW) Bytes 0 (bits 31:24), 1 (bits 23:16), 2 (bits 15:8), and 3 (bits 7:0) of the 6-byte individual address are used for exact match and the source address field in PAUSE frames. */


/*********** Physical Address Upper Register (PAUR) ***************************/
#define CHIP_REGFLD_ENET_PAUR_PADDR2                (0x0000FFFFUL << 16U) /* (RW) Bytes 4 (bits 31:24) and 5 (bits 23:16) of the 6-byte individual address used for exact match, and the source address field in PAUSE frames. */
#define CHIP_REGFLD_ENET_PAUR_TYPE                  (0x0000FFFFUL <<  0U) /* (RO) Type Field In PAUSE Frames These fields have a constant value of 0x8808. */


/*********** Opcode/Pause Duration Register (OPD) *****************************/
#define CHIP_REGFLD_ENET_OPD_OPCODE                 (0x0000FFFFUL << 16U) /* (RO) Opcode Field In PAUSE Frames. These fields have a constant value of 0x0001.*/
#define CHIP_REGFLD_ENET_OPD_PAUSE_DUR              (0x0000FFFFUL <<  0U) /* (RW) Pause Duration Pause duration field used in PAUSE frames. */


/*********** Transmit Interrupt Coalescing Register (TXICx) *******************/
#define CHIP_REGFLD_ENET_TXIC_ICEN                  (0x00000001UL << 31U) /* (RW) Interrupt Coalescing Enable.*/
#define CHIP_REGFLD_ENET_TXIC_ICCS                  (0x00000001UL << 30U) /* (RW) Interrupt Coalescing Timer Clock Source Select. */
#define CHIP_REGFLD_ENET_TXIC_ICFT                  (0x000000FFUL << 20U) /* (RW) Interrupt coalescing frame count threshold. */
#define CHIP_REGFLD_ENET_TXIC_ICTT                  (0x0000FFFFUL <<  0U) /* (RW) Interrupt coalescing timer threshold (units of 64 clock periods). */
#define CHIP_REGMSK_ENET_TXIC_RESERVED              (0x300F0000UL)

#define CHIP_REGFLDVAL_ENET_TXIC_ICCS_MII_GMII_TX   0 /* Use MII/GMII TX clocks.*/
#define CHIP_REGFLDVAL_ENET_TXIC_ICCS_SYS           1 /* ENET System Clock */


/*********** Receive Interrupt Coalescing Register (RXICx) ********************/
#define CHIP_REGFLD_ENET_RXIC_ICEN                  (0x00000001UL << 31U) /* (RW) Interrupt Coalescing Enable.*/
#define CHIP_REGFLD_ENET_RXIC_ICCS                  (0x00000001UL << 30U) /* (RW) Interrupt Coalescing Timer Clock Source Select. */
#define CHIP_REGFLD_ENET_RXIC_ICFT                  (0x000000FFUL << 20U) /* (RW) Interrupt coalescing frame count threshold. */
#define CHIP_REGFLD_ENET_RXIC_ICTT                  (0x0000FFFFUL <<  0U) /* (RW) Interrupt coalescing timer threshold (units of 64 clock periods). */
#define CHIP_REGMSK_ENET_RXIC_RESERVED              (0x300F0000UL)

#define CHIP_REGFLDVAL_ENET_RXIC_ICCS_MII_GMII_TX   0 /* Use MII/GMII TX clocks.*/
#define CHIP_REGFLDVAL_ENET_RXIC_ICCS_SYS           1 /* ENET System Clock */


/*********** Transmit FIFO Watermark Register (TFWR) **************************/
#define CHIP_REGFLD_ENET_TFWR_STRFWD                (0x00000001UL <<  8U) /* (RW) Store And Forward Enable */
#define CHIP_REGFLD_ENET_TFWR_TFWR                  (0x0000003FUL <<  0U) /* (RW) Transmit FIFO Write (64*n bytes) */
#define CHIP_REGMSK_ENET_TFWR_RESERVED              (0xFFFFFEC0UL)

/*********** Maximum Receive Buffer Size Register - Ring x (MRBRx) ************/
#define CHIP_REGFLD_ENET_MRBR_R_BUF_SIZE            (0x00003FFFUL <<  0U) /* (RW) Receive buffer size in bytes (bits 3:0 - always zero). */
#define CHIP_REGMSK_ENET_MRBR_RESEVED               (0xFFFFC000UL)


/***********  Receive FIFO Section Full Threshold (RSFL) **********************/
#define CHIP_REGFLD_ENET_RSFL_RX_SECTION_FULL       (0x000003FFUL <<  0U) /* (RW) Value Of Receive FIFO Section Full Threshold in 64-bit words (cut through mode) */
#define CHIP_REGMSK_ENET_RSFL_RESERVED              (0xFFFFFC00UL)


/***********  Receive FIFO Section Empty Threshold (RSEM) **********************/
#define CHIP_REGFLD_ENET_RSEM_STAT_SECTION_EMPTY    (0x0000001FUL << 16U) /* (RW) RX Status FIFO Section Empty Threshold (in frames) for generate pause frames (0 - disable) */
#define CHIP_REGFLD_ENET_RSEM_RX_SECTION_EMPTY      (0x000003FFUL <<  0U) /* (RW) Value Of The Receive FIFO Section Empty Threshold in 64-bit words for generate pause frame (0 - disable) */
#define CHIP_REGMSK_ENET_RSEM_RESERVED              (0xFFE0FC00UL)


/***********  Receive FIFO Almost Empty Threshold (RAEM) **********************/
#define CHIP_REGFLD_ENET_RAEM_RX_ALMOST_EMPTY       (0x000003FFUL <<  0U) /* (RW) Value Of The Receive FIFO Almost Empty Threshold in 64-bit words (min = 4) */
#define CHIP_REGMSK_ENET_RAEM_RESERVED              (0xFFFFFC00UL)


/***********  Receive FIFO Almost Full Threshold (RAFL) ***********************/
#define CHIP_REGFLD_ENET_RAFL_RX_ALMOST_FUL         (0x000003FFUL <<  0U) /* (RW) Value Of The Receive FIFO Almost Full Threshold in 64-bit words (min = 4) */
#define CHIP_REGMSK_ENET_RAFL_RESERVED              (0xFFFFFC00UL)


/***********  Transmit FIFO Section Empty Threshold (TSEM) ********************/
#define CHIP_REGFLD_ENET_TSEM_TX_SECTION_EMPTY      (0x000003FFUL <<  0U) /* (RW) Value Of The Transmit FIFO Section Empty Threshold in 64-bit words (min = 4) */
#define CHIP_REGMSK_ENET_TSEM_RESERVED              (0xFFFFFC00UL)


/***********  Transmit FIFO Almost Empty Threshold (TAEM) *********************/
#define CHIP_REGFLD_ENET_TAEM_TX_ALMOST_EMPTY       (0x000003FFUL <<  0U) /* (RW) Value of Transmit FIFO Almost Empty Threshold in 64-bit words (min = 4). Underflow protection */
#define CHIP_REGMSK_ENET_TAEM_RESERVED              (0xFFFFFC00UL)


/***********  Transmit FIFO Almost Full Threshold (TAFL) **********************/
#define CHIP_REGFLD_ENET_TAFL_TX_ALMOST_FULL        (0x000003FFUL <<  0U) /* (RW) Value Of The Transmit FIFO Almost Full Threshold in 64-bit words (min = 6, recommended >= 0). Overflow protection */
#define CHIP_REGMSK_ENET_TAFL_RESERVED              (0xFFFFFC00UL)


/***********  Transmit Inter-Packet Gap (TIPG) ********************************/
#define CHIP_REGFLD_ENET_TIPG_IPG                   (0x0000001FUL <<  0U) /* (RW) Transmit Inter-Packet Gap in bytes (8-26) */
#define CHIP_REGMSK_ENET_TIPG_RESERVED              (0xFFFFFFE0UL)


/***********  Frame Truncation Length (FTRL) **********************************/
#define CHIP_REGFLD_ENET_FTRL_TRUNC_FL              (0x00003FFFUL <<  0U) /* (RW) Frame Truncation Length (must be >= RCR[MAX_FL]) */
#define CHIP_REGMSK_ENET_FTRL_RESERVED              (0xFFFFC000UL)


/***********  Transmit Accelerator Function Configuration (TACC) **************/
#define CHIP_REGFLD_ENET_TACC_PROCHK                (0x00000001UL <<  4U) /* (RW) Enables insertion of protocol checksum (tx IP frame with a known protocol) */
#define CHIP_REGFLD_ENET_TACC_IPCHK                 (0x00000001UL <<  3U) /* (RW) Enables insertion of IP header checksum. */
#define CHIP_REGFLD_ENET_TACC_SHIFT16               (0x00000001UL <<  0U) /* (RW) TX FIFO Shift-16 (2 dummy byte before frame, extend 14-byte ethernet header to 16-byte to align body)  */
#define CHIP_REGMSK_ENET_TACC_RESERVED              (0xFFFFFFE6UL)


/***********  Receive Accelerator Function Configuration (RACC) ***************/
#define CHIP_REGFLD_ENET_RACC_SHIFT16               (0x00000001UL <<  7U) /* (RW) RX FIFO Shift-16 (write dummy 2 byte before frame, extend 14-byte ethernet header to 16-byte to align body)  */
#define CHIP_REGFLD_ENET_RACC_LINEDIS               (0x00000001UL <<  6U) /* (RW) Enable Discard Of Frames With MAC Layer Errors (CRC, length, or PHY error) */
#define CHIP_REGFLD_ENET_RACC_PRODIS                (0x00000001UL <<  2U) /* (RW) Enable Discard Of Frames With Wrong Protocol Checksum (only if RSFL = 0) */
#define CHIP_REGFLD_ENET_RACC_IPDIS                 (0x00000001UL <<  1U) /* (RW) Enable Discard Of Frames With Wrong IPv4 Header Checksum (only if RSFL = 0) */
#define CHIP_REGFLD_ENET_RACC_PADREM                (0x00000001UL <<  0U) /* (RW) Enable Padding Removal For Short IP Frames */
#define CHIP_REGMSK_ENET_RACC_RESERVED              (0xFFFFFF38UL)


/***********  Receive Classification Match Register for Class n (RCMRn, n=1..2) */
#define CHIP_REGFLD_ENET_RCMR_MATCHEN               (0x00000001UL << 16U) /* (RW) Match Enable (Compare priority from VLAN to up to 4 values, RCM1 has priority to RCM2)  */
#define CHIP_REGFLD_ENET_RCMR_CMP(n)                (0x00000007UL << (4*(n) + 0U)) /* (RW) Compare n (n=0..3). A three-bit value that will be compared with the frame's VLAN priority field (if CMPn is not used set equal to CMP0) */
#define CHIP_REGMSK_ENET_RCMR_RESERVED              (0xFFFE8888UL)


/***********  DMA Class Based Configuration (DMAnCFG, n=1..2) *****************/
#define CHIP_REGFLD_ENET_DMACFG_CALC_NOIPG          (0x00000001UL << 17U) /* (RW) Disable inclusion of IPG bytes for bandwidth calculations. */
#define CHIP_REGFLD_ENET_DMACFG_DMA_CLASS_EN        (0x00000001UL << 16U) /* (RW) Enable the DMA controller to support the corresponding descriptor ring for this class of traffic (if 0, then RCMR_MATCHEN must be 0). */
#define CHIP_REGFLD_ENET_DMACFG_IDLE_SLOPE          (0x0000FFFFUL <<  0U) /* (RW) Idle slope for  credit based shaper.  BW fraction = 1/(1+512/IDLE_SLOPE), allowed values: 2^n for  n = 0..6 or 128×m, where m = 1..12 */
#define CHIP_REGMSK_ENET_DMACFG_RESERVED            (0xFFFC0000UL)


/***********  QOS Scheme (QOS) ************************************************/
#define CHIP_REGFLD_ENET_QOS_RX_FLUSH(x)            (0x00000001UL << (3U + (x))) /* (RW) Enable or disable RX Flush for ring x (x=0..2)*/
#define CHIP_REGFLD_ENET_QOS_TX_SCHEME              (0x00000007UL <<  0U) /* (RW) Configuration information for DMA to select transmitter queue selection/arbitration scheme. */
#define CHIP_REGMSK_ENET_QOS_RESERVED               (0xFFFFFFC0UL)

#define CHIP_REGFLDVAL_ENET_QOS_TX_SCHEME_CREDIT    0 /* Credit-based scheme */
#define CHIP_REGFLDVAL_ENET_QOS_TX_SCHEME_RR        1 /* Round-robin scheme */


/*********** Tx Packet Count Statistic Register (RMON_T_PACKETS) **************/
#define CHIP_REGFLD_ENET_RMON_T_PACKETS_TXPKTS      (0x0000FFFFUL <<  0U) /* (RO) Transmit packet count */
#define CHIP_REGMSK_ENET_RMON_T_PACKETS_RESERVED    (0xFFFF0000UL)


/*********** Tx Broadcast Packets Statistic Register (RMON_T_BC_PKT) **********/
#define CHIP_REGFLD_ENET_RMON_T_BC_PKT_TXPKTS       (0x0000FFFFUL <<  0U) /* (RO) Number of broadcast packets */
#define CHIP_REGMSK_ENET_RMON_T_BC_PKT_RESERVED     (0xFFFF0000UL)


/*********** Tx Multicast Packets Statistic Register (RMON_T_MC_PKT) **********/
#define CHIP_REGFLD_ENET_RMON_T_MC_PKT_TXPKTS       (0x0000FFFFUL <<  0U) /* (RO) Number of multicast packets */
#define CHIP_REGMSK_ENET_RMON_T_MC_PKT_RESERVED     (0xFFFF0000UL)


/*********** Tx Packets with CRC/Align Error Statistic Register (RMON_T_CRC_ALIGN) */
#define CHIP_REGFLD_ENET_RMON_T_CRC_ALIGN_TXPKTS    (0x0000FFFFUL <<  0U) /* (RO) Number of packets with CRC/align error */
#define CHIP_REGMSK_ENET_RMON_T_CRC_ALIGN_RESERVED  (0xFFFF0000UL)


/*********** Tx Packets Less Than Bytes and Good CRC Statistic Register (RMON_T_UNDERSIZE) */
#define CHIP_REGFLD_ENET_RMON_T_UNDERSIZE_TXPKTS    (0x0000FFFFUL <<  0U) /* (RO) Number of transmit packets less than 64 bytes with good CRC */
#define CHIP_REGMSK_ENET_RMON_T_UNDERSIZE_RESERVED  (0xFFFF0000UL)


/*********** Tx Packets GT MAX_FL bytes and Good CRC Statistic Register (RMON_T_OVERSIZE) */
#define CHIP_REGFLD_ENET_RMON_T_OVERSIZE_TXPKTS     (0x0000FFFFUL <<  0U) /* (RO) Number of transmit packets greater than MAX_FL bytes with good CRC */
#define CHIP_REGMSK_ENET_RMON_T_OVERSIZE_RESERVED   (0xFFFF0000UL)


/*********** Tx Packets Less Than 64 Bytes and Bad CRC Statistic Register (RMON_T_FRAG) */
#define CHIP_REGFLD_ENET_RMON_T_FRAG_TXPKTS         (0x0000FFFFUL <<  0U) /* (RO) Number of packets less than 64 bytes with bad CRC */
#define CHIP_REGMSK_ENET_RMON_T_FRAG_RESERVED       (0xFFFF0000UL)


/*********** Tx Packets Greater Than MAX_FL bytes and Bad CRC Statistic Register (RMON_T_JAB) */
#define CHIP_REGFLD_ENET_RMON_T_JAB_TXPKTS          (0x0000FFFFUL <<  0U) /* (RO) Number of transmit packets greater than MAX_FL bytes and bad CRC */
#define CHIP_REGMSK_ENET_RMON_T_JAB_RESERVED        (0xFFFF0000UL)



/* ............. */

/*********** Adjustable Timer Control Register (ATCR) *************************/
#define CHIP_REGFLD_ENET_ATCR_SLAVE                 (0x00000001UL << 13U) /* (RW) Enable Timer Slave Mode (The internal timer is disabled and the externally provided timer value is used. All other fields, except CAPTURE, in this register have no effect.)  */
#define CHIP_REGFLD_ENET_ATCR_CAPTURE               (0x00000001UL << 11U) /* (RW1SC) Capture Timer Value 6*max(register or 1588/timestamp clock cycles) wait before read. */
#define CHIP_REGFLD_ENET_ATCR_RESTART               (0x00000001UL <<  9U) /* (RW1SC) Resets the timer to zero */
#define CHIP_REGFLD_ENET_ATCR_PINPER                (0x00000001UL <<  7U) /* (RW) Enables event signal output assertion on period event. */
#define CHIP_REGFLD_ENET_ATCR_PEREN                 (0x00000001UL <<  4U) /* (RW) Enable Periodical Event (interrupt and output). */
#define CHIP_REGFLD_ENET_ATCR_OFFRST                (0x00000001UL <<  3U) /* (RW) Reset Timer On Offset Event (1 - reset to 0 on offset event) */
#define CHIP_REGFLD_ENET_ATCR_OFFEN                 (0x00000001UL <<  2U) /* (RWSC) Enable One-Shot Offset Event (autoclear on offset) */
#define CHIP_REGFLD_ENET_ATCR_EN                    (0x00000001UL <<  0U) /* (RW) Enable Timer */
#define CHIP_REGMSK_ENET_ATCR_RESERVED              (0xFFFFD542UL)
#define CHIP_REGMSK_ENET_ATCR_W1                    (0x00000020UL)        /* (W1) This field must be written always with one. */

/*********** Time-Stamping Clock Period Register (ATINC) **********************/
#define CHIP_REGFLD_ENET_ATINC_INC_CORR             (0x0000007FUL <<  8U) /* (RW) Correction Increment Value (added when correction timer expires (ATCOR). If < INC => slows down the timer, > INC => speeds up) */
#define CHIP_REGFLD_ENET_ATINC_INC                  (0x0000007FUL <<  0U) /* (RW) Clock Period Of The Timestamping Clock (ts_clk) In Nanoseconds (For highest precision, use a value that is an integer fraction of the period set in ATPER) */
#define CHIP_REGMSK_ENET_ATINC_RESERVED             (0xFFFF8080UL)


/*********** Timer Global Status Register (TGSR) ******************************/
#define CHIP_REGFLD_ENET_TGSR_TF(x)                 (0x00000001UL << (0U + (x)) /* (RW1C) Copy Of Timer Flag For Channel x (x=0..3) */


/*********** Timer Control Status Register (TCSR) *****************************/
#define CHIP_REGFLD_ENET_TCSR_TPWC                  (0x0000001FUL << 11U) /* (RW) Timer PulseWidth Control (TMODE = 1110 or 11X1). Not supported in ENET1G */
#define CHIP_REGFLD_ENET_TCSR_TF                    (0x00000001UL <<  7U) /* (RW1C) Timer Flag (Input Capture or Output Compare has occurred) */
#define CHIP_REGFLD_ENET_TCSR_TIE                   (0x00000001UL <<  6U) /* (RW) Timer Interrupt Enable */
#define CHIP_REGFLD_ENET_TCSR_TMODE                 (0x0000000FUL <<  2U) /* (RW) Timer Mode */
#define CHIP_REGFLD_ENET_TCSR_TDRE                  (0x00000001UL <<  0U) /* (RW) Timer DMA Request Enable */
#define CHIP_REGMSK_ENET_TCSR_RESERVED              (0xFFFF0702UL)

#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_DIS              0x0 /* Timer Channel is disabled */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_ICAP_RISE        0x1 /* Timer Channel is configured for Input Capture on rising edge */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_ICAP_FALL        0x2 /* Timer Channel is configured for Input Capture on falling edge */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_ICAP_BOTH        0x3 /* Timer Channel is configured for Input Capture on both edges */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_SW          0x4 /* Timer Channel is configured for Output Compare - software only */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_TOG         0x5 /* Timer Channel is configured for Output Compare - toggle output on compare */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_CLR         0x6 /* Timer Channel is configured for Output Compare - clear output on compare */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_SET         0x7 /* Timer Channel is configured for Output Compare - set output on compare */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_CLR_OV_SET  0xA /* Timer Channel is configured for Output Compare - clear output on compare, set output on overflow. */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_SET_OV_CLR  0xB /* Timer Channel is configured for Output Compare - set output on compare, clear output on overflow. */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_PULSE_LOW   0xE /* Timer Channel is configured for Output Compare - pulse output low on compare for 1 to 32 1588-clock cycles as specified by TPWC/1. */
#define CHIP_REGFLDVAL_ENET_TCSR_TMODE_OCMP_PULSE_HIGH  0xF /* Timer Channel is configured for Output Compare - pulse output high on compare for 1 to 32 1588-clock cycles as specified by TPWC/1 */

#endif // REGS_ENET_H


