#include "chip.h"

#define CHIP_IWDG_WDT_ENABLE_KEY      0xCCCC
#define CHIP_IWDG_ACCESS_ENABLE_KEY   0x5555
#define CHIP_IWDG_REFRESH_RLD_KEY     0xAAAA

#if CHIP_WDT_SETUP_EN

#if CHIP_WDT_NV_SETUP_EN

#if !CHIP_FLASH_FUNC_EN
    #error flash access functions required for nonvolatile wdt config update
#endif

static void wdt_update_nv_options(void) {
    static const uint32_t iwdg_opt_msk = 0
            | CHIP_WDT_OPTBIT_SW_MODE
            | CHIP_WDT_OPTBIT_ON_STOP_EN
            | CHIP_WDT_OPTBIT_ON_STANDBY_EN
        #if CHIP_WDT_REMCORE_SETUP_EN
            | CHIP_WDT_OPTBIT_REMCORE_SW_MODE
        #endif
            ;
    static const uint32_t iwdg_opt_val = 0
            | LBITFIELD_SET(CHIP_WDT_OPTBIT_SW_MODE, !CHIP_CFG_WDT_HW_MODE_EN)
            | LBITFIELD_SET(CHIP_WDT_OPTBIT_ON_STANDBY_EN, !!CHIP_CFG_WDT_ON_STANDBY_EN)
            | LBITFIELD_SET(CHIP_WDT_OPTBIT_ON_STOP_EN, !!CHIP_CFG_WDT_ON_STOP_EN)
        #if  CHIP_WDT_REMCORE_SETUP_EN
            | LBITFIELD_SET(CHIP_WDT_OPTBIT_REMCORE_SW_MODE, !CHIP_CFG_WDT_REMCORE_HW_MODE_EN)
        #endif
            ;

    if ((CHIP_WDT_GET_OPTBITS() & iwdg_opt_msk) != iwdg_opt_val) {
        chip_flash_opts_unlock();
        CHIP_WDT_SET_OPTBITS((CHIP_WDT_GET_OPTBITS() & ~iwdg_opt_msk) | iwdg_opt_val);
        chip_flash_opts_write(chip_wdt_reset);
        chip_flash_opts_lock();
    }
}
#endif



void chip_wdt_init(void) {
    /* разрешение и настройка WDT таймера (даже в HW режиме должны перенстроить
     * на нужный таймаут, т.к. кроме разрешения все настройки сбрасываются) */

    CHIP_WDT_REGS->KR = CHIP_IWDG_WDT_ENABLE_KEY;
    chip_wdt_set_interval(CHIP_CFG_WDT_TIMEOUT_MS);
#if CHIP_WDT_REMCORE_SETUP_EN
#if CHIP_WDT_REMCORE_EN
    chip_wdt_remcore_enable();
#endif
#if CHIP_WDT_REMCORE_EN || defined CHIP_CFG_WDT_REMCORE_TIMEOUT_MS
    chip_wdt_remcore_set_interval(CHIP_CFG_WDT_REMCORE_TIMEOUT_MS);
#endif
#endif
#if CHIP_WDT_NV_SETUP_EN
    wdt_update_nv_options();
#endif
}

#endif

void chip_wdt_reset(void) {
    CHIP_WDT_REGS->KR = CHIP_IWDG_REFRESH_RLD_KEY;
}


static void f_wdt_set_interval(CHIP_REGS_IWDG_T *regs, unsigned freq, int ms) {
    unsigned div =  ((freq + 999)/1000) * ms;
    unsigned presc_val, presc_code;
    if (div < (0xFFF * 4)) {
        presc_val = 4;
        presc_code = 0;
    } else if (div < (0xFFF * 8)) {
        presc_val = 8;
        presc_code = 1;
    } else if (div < (0xFFF * 16)) {
        presc_val = 16;
        presc_code = 2;
    } else if (div < (0xFFF * 32)) {
        presc_val = 32;
        presc_code = 3;
    } else if (div < (0xFFF * 64)) {
        presc_val = 64;
        presc_code = 4;
    } else if (div < (0xFFF * 128)) {
        presc_val = 128;
        presc_code = 5;
    } else {
        presc_val = 256;
        presc_code = 6;
    }

    div/= presc_val;
    if (div > 0xFFF)
        div = 0xFFF;

    regs->KR = CHIP_IWDG_ACCESS_ENABLE_KEY;
    LBITFIELD_UPD(regs->PR, CHIP_REGFLD_IWDG_PR_PR, presc_code);
    LBITFIELD_UPD(regs->RLR, CHIP_REGFLD_IWDG_RLR_RL, div);
    chip_dmb();
    while (regs->SR != 0) {
        continue;
    }

    regs->KR = CHIP_IWDG_REFRESH_RLD_KEY;
}




void chip_wdt_set_interval(int ms) {
    f_wdt_set_interval(CHIP_WDT_REGS, CHIP_WDT_CLK_FREQ, ms);
}

#if CHIP_WDT_SUPPORT_REMCORE
void chip_wdt_remcore_enable(void) {
    CHIP_WDT_REMCORE_REGS->KR = CHIP_IWDG_WDT_ENABLE_KEY;
}

void chip_wdt_remcore_set_interval(int ms) {
    f_wdt_set_interval(CHIP_WDT_REMCORE_REGS, CHIP_WDT_REMCORE_CLK_FREQ, ms);
}
#endif
