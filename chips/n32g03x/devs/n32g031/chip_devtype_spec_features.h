#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#define CHIP_DEV_N32G03X
#define CHIP_DEV_N32G031

#include "chip_dev_spec_features.h"



#define CHIP_DEV_TIM_GP_CNT             1
#define CHIP_DEV_TIM_GP32_CNT           0
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          1
#define CHIP_DEV_LPTIM_CNT              1
#define CHIP_DEV_SPI_CNT                2
#define CHIP_DEV_I2S_CNT                1
#define CHIP_DEV_I2S_FD_CNT             0
#define CHIP_DEV_I2C_CNT                2
#define CHIP_DEV_UART_CNT               2
#define CHIP_DEV_LPUART_CNT             1
#define CHIP_DEV_CAN_CNT                0
#define CHIP_DEV_USB_CNT                0
#define CHIP_DEV_ADC_CNT                1
#define CHIP_DEV_DAC_CNT                0
#define CHIP_DEV_OPA_CNT                1
#define CHIP_DEV_COMP_CNT               1
#define CHIP_DEV_BEEPER_CNT             1

#define CHIP_DEV_DMA_CH_CNT             5

#define CHIP_DEV_SUPPORT_TSC            0
#define CHIP_DEV_SUPPORT_RNG            0
#define CHIP_DEV_SUPPORT_SAC            0
#define CHIP_DEV_SUPPORT_HSI_TRIM       1
#define CHIP_DEV_SUPPORT_HSE_IOSEL      0
#define CHIP_DEV_SUPPORT_PORTD          0
#define CHIP_DEV_SUPPORT_TIM4           (CHIP_DEV_TIM_GP_CNT >= 2)
#define CHIP_DEV_SUPPORT_UART5          (CHIP_DEV_UART_CNT >= 4)
#define CHIP_DEV_SUPPORT_UART6          (CHIP_DEV_UART_CNT >= 4)

#define CHIP_SUPPORT_ADC_INJECTED       1
#define CHIP_SUPPORT_ADC_EOC_RD_CLR     0 /* признак, сбрасывается ли флаг EOC при чтении данных автоматом (stm), или нужно вручную (gd) */

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
