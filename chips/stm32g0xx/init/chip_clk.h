#ifndef CHIP_CLK_H
#define CHIP_CLK_H

#include "chip_config.h"
#include "chip_clk_defs.h"
#include "chip_dev_spec_features.h"

#define CHIP_CLK_HSE_EN  (CHIP_CFG_CLK_HSE_MODE != CHIP_CLK_EXTMODE_DIS)
#define CHIP_CLK_LSE_EN  (CHIP_CFG_CLK_LSE_MODE != CHIP_CLK_EXTMODE_DIS)

#if CHIP_CLK_HSE_EN
    #define CHIP_CLK_HSE_FREQ   CHIP_CFG_CLK_HSE_FREQ
#else
    #define CHIP_CLK_HSE_FREQ   0
#endif


/* в режиме осциллятора частота LSE должна быть 32768 кГц, поэтому ее можно
 *  не определять в конфигурации явно */
#define CHIP_CLK_LSE_OSC_FREQ               CHIP_HZ(32768) /* требуемая частота LSE в режиме CHIP_CLK_EXTMODE_OSC */
#if CHIP_CLK_LSE_EN
    #if (CHIP_CFG_CLK_LSE_MODE == CHIP_CLK_EXTMODE_OSC)
        #define CHIP_CLK_LSE_FREQ CHIP_CLK_LSE_OSC_FREQ
    #else
        #define CHIP_CLK_LSE_FREQ CHIP_CFG_CLK_LSE_FREQ
    #endif
#else
    #define CHIP_CLK_LSE_FREQ 0
#endif



#define CHIP_CLK_HSI16_EN           (CHIP_CFG_CLK_HSI16_EN)
#define CHIP_CLK_HSI16_FREQ         CHIP_MHZ(16)

#define CHIP_CLK_LSI_EN             (CHIP_CFG_WDT_SETUP_EN || CHIP_CFG_CLK_LSI_EN)
#define CHIP_CLK_LSI_FREQ           CHIP_KHZ(32)

#if CHIP_DEV_SUPPORT_HSI48
    #define CHIP_CLK_HSI48_FREQ     CHIP_MHZ(48)
    #define CHIP_CLK_HSI48_EN       CHIP_CFG_CLK_HSI48_EN
#else
    #define CHIP_CLK_HSI48_FREQ     0
    #define CHIP_CLK_HSI48_EN       0
#endif



#define CHIP_CLK_HSISYS_EN      CHIP_CLK_HSI16_EN
#define CHIP_CLK_HSISYS_FREQ    (CHIP_CLK_HSI16_FREQ/CHIP_CFG_CLK_HSISYS_DIV)






/* ------------------------- частоты PLL -----------------------------------*/

/* Определяем входную частоту.
 * Не можем использовать просто макрос FREQ, т.к. препроцессор не поддерживает
 * рекурсию и не сможем ссылаться на эту частоту для получения с помощью FREQ производных
 * от PLL частот */
#define CHIP_CLK_PLL_SRC_ID    CHIP_CLK_ID_VAL(CHIP_CFG_CLK_PLL_SRC)
#if CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI16_ID
    #define CHIP_CLK_PLL_SRC_EN   CHIP_CLK_HSI16_EN
    #define CHIP_CLK_PLL_SRC_FREQ CHIP_CLK_HSI16_FREQ
#elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_CLK_PLL_SRC_EN   CHIP_CLK_HSE_EN
    #define CHIP_CLK_PLL_SRC_FREQ CHIP_CLK_HSE_FREQ
#else
    #define CHIP_CLK_PLL_SRC_EN   0
    #define CHIP_CLK_PLL_SRC_FREQ 0
#endif



#define CHIP_CLK_PLL_IN_FREQ        (CHIP_CLK_PLL_SRC_FREQ / CHIP_CFG_CLK_PLL_DIVM)
#define CHIP_CLK_PLL_VCO_FREQ       (CHIP_CLK_PLL_IN_FREQ * CHIP_CFG_CLK_PLL_DIVN)

#define CHIP_CLK_PLLP_FREQ         (CHIP_CLK_PLL_VCO_FREQ / CHIP_CFG_CLK_PLL_DIVP)
#define CHIP_CLK_PLLQ_FREQ         (CHIP_CLK_PLL_VCO_FREQ / CHIP_CFG_CLK_PLL_DIVQ)
#define CHIP_CLK_PLLR_FREQ         (CHIP_CLK_PLL_VCO_FREQ / CHIP_CFG_CLK_PLL_DIVR)
#define CHIP_CLK_PLLP_EN           (CHIP_CFG_CLK_PLL_EN && CHIP_CFG_CLK_PLL_DIVP_EN)
#define CHIP_CLK_PLLQ_EN           (CHIP_CFG_CLK_PLL_EN && CHIP_CFG_CLK_PLL_DIVQ_EN)
#define CHIP_CLK_PLLR_EN           (CHIP_CFG_CLK_PLL_EN && CHIP_CFG_CLK_PLL_DIVR_EN)




/* --------------------- определение системной частоты -----------------------*/
#define CHIP_CLK_SYS_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_SYS_SRC)
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_LSE_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_LSE_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_LSE_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_LSI_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_LSI_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_LSI_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_HSE_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_HSE_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLLR_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_PLLR_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_PLLR_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSISYS_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_HSISYS_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_HSISYS_EN
#else
    #define CHIP_CLK_SYS_FREQ 0
    #define CHIP_CLK_SYS_EN   0
#endif



#define CHIP_CLK_AHB_FREQ       (CHIP_CLK_SYS_FREQ / CHIP_CFG_CLK_AHB_DIV)
#define CHIP_CLK_AHB_EN         CHIP_CLK_SYS_EN

#define CHIP_CLK_AHB_DIV8_FREQ      (CHIP_CLK_AHB_FREQ / 8)
#define CHIP_CLK_AHB_DIV8_EN        CHIP_CLK_AHB_EN

#define CHIP_CLK_APB_FREQ       (CHIP_CLK_AHB_FREQ / CHIP_CFG_CLK_APB_DIV)
#define CHIP_CLK_APB_EN         CHIP_CLK_AHB_EN


#define CHIP_CLK_CPU_FREQ       CHIP_CLK_AHB_FREQ

/** @note есть выбор, использовать ли для SysTicks AHB или AHB_DIV8, но в
 *  CMSIS всегда используется клок процессора в SysTick_Config().
 *  Так как сейчас используем CMSIS, то данный параметр устанавливаем без настройки */
#define CHIP_CLK_SYSTICK_FREQ   CHIP_CLK_AHB_FREQ









/*---------------------- параметры частоты RTC ------------------------------ */
#define CHIP_CLK_RTC_SRC_ID        CHIP_CLK_ID_VAL(CHIP_CFG_CLK_RTC_SRC)
#define CHIP_CLK_HSE_DIV32_FREQ    (CHIP_CFG_CLK_HSE_FREQ/32)
#define CHIP_CLK_HSE_DIV32_EN      CHIP_CLK_HSE_EN
#define CHIP_CLK_RTC_FREQ          CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_RTC_SRC)
#define CHIP_CLK_RTC_EN            (CHIP_CFG_CLK_RTC_EN && CHIP_CFG_CLK_RTC_SETUP_EN)

/*---------------------- параметры частоты LSCO ----------------------------- */
#define CHIP_CLK_LSCO_SRC_ID       CHIP_CLK_ID_VAL(CHIP_CFG_CLK_LSCO_SRC)
#define CHIP_CLK_LSCO_FREQ         CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_LSCO_SRC)
#define CHIP_CLK_LSCO_EN           (CHIP_CFG_CLK_LSCO_EN && CHIP_CFG_CLK_LSCO_SETUP_EN)


/*---------------------- параметры частоты MCO ------------------------------ */
#define CHIP_CLK_MCO_FREQ          (CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_MCO_SRC)/CHIP_CFG_CLK_MCO_DIV)
#if CHIP_DEV_SUPPORT_MC02
    #define CHIP_CLK_MCO2_FREQ         (CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_MCO2_SRC)/CHIP_CFG_CLK_MCO2_DIV)
#endif


#define CHIP_CLK_FLASH_FREQ     CHIP_CLK_AHB_FREQ
#define CHIP_CLK_FLASH_EN       CHIP_CLK_AHB_EN

#define CHIP_CLK_IWDG_FREQ      CHIP_CLK_LSI_FREQ

/* клок для TIMPCLK для таймеров равен PCLK при APB_DIV=1, а в остальных случаях в два раза больше */
#if CHIP_CFG_CLK_APB_DIV == 1
    #define CHIP_CLK_TIMP_FREQ  CHIP_CLK_APB_FREQ
#else
    #define CHIP_CLK_TIMP_FREQ  (2*CHIP_CLK_APB_FREQ)
#endif
#define CHIP_CLK_TIMP_EN    CHIP_CFG_CLK_APB_EN



/* ------------------------ TIM1 ---------------------------------*/
#if CHIP_CFG_CLK_TIM1_EN && !defined CHIP_CFG_CLK_TIM1_KER_SRC
    #error Source isn't specified for enabled TIM1 clock
#else
    #if defined CHIP_CFG_CLK_TIM1_KER_SRC
        #define CHIP_CLK_TIM1_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_TIM1_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_TIM1_KER_SRC)
    #else
        #define CHIP_CLK_TIM1_KER_SRC_SETUP_EN   0
    #endif
#endif



/* ------------------------ TIM15 ---------------------------------*/
#if CHIP_DEV_SUPPORT_TIM15
    #if CHIP_CFG_CLK_TIM15_EN && !defined CHIP_CFG_CLK_TIM15_KER_SRC
        #error Source isn't specified for enabled TIM15 clock
    #else
        #if defined CHIP_CFG_CLK_TIM15_KER_SRC
            #define CHIP_CLK_TIM15_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_TIM15_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_TIM15_KER_SRC)
        #else
            #define CHIP_CLK_TIM15_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_TIM15_KER_SRC_SETUP_EN 0
    #if defined CHIP_CFG_CLK_TIM15_KER_SRC
        #error TIM15 is not supported on specified device
    #endif
#endif

/* ---- TIM2,3,4,6,7,14,16,17 - частота не нестраивается - всегда TIMPCLK ---*/
#define CHIP_CLK_TIM2_KER_FREQ     CHIP_CLK_TIMP_FREQ
#define CHIP_CLK_TIM3_KER_FREQ     CHIP_CLK_TIMP_FREQ
#if CHIP_DEV_SUPPORT_TIM4
#define CHIP_CLK_TIM4_KER_FREQ     CHIP_CLK_TIMP_FREQ
#endif
#if CHIP_DEV_SUPPORT_TIM6
#define CHIP_CLK_TIM6_KER_FREQ     CHIP_CLK_TIMP_FREQ
#endif
#if CHIP_DEV_SUPPORT_TIM7
#define CHIP_CLK_TIM7_KER_FREQ     CHIP_CLK_TIMP_FREQ
#endif
#define CHIP_CLK_TIM14_KER_FREQ    CHIP_CLK_TIMP_FREQ
#define CHIP_CLK_TIM16_KER_FREQ    CHIP_CLK_TIMP_FREQ
#define CHIP_CLK_TIM17_KER_FREQ    CHIP_CLK_TIMP_FREQ



/* ------------------------ LPTIM1 ---------------------------------*/
#if CHIP_CFG_CLK_LPTIM1_EN && !defined CHIP_CFG_CLK_LPTIM1_KER_SRC
    #error Source isn't specified for enabled LPTIM1 clock
#else
    #if defined CHIP_CFG_CLK_LPTIM1_KER_SRC
        #define CHIP_CLK_LPTIM1_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_LPTIM1_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_LPTIM1_KER_SRC)
    #else
        #define CHIP_CLK_LPTIM1_KER_SRC_SETUP_EN   0
    #endif
#endif

/* ------------------------ LPTIM2 ---------------------------------*/
#if CHIP_CFG_CLK_LPTIM2_EN && !defined CHIP_CFG_CLK_LPTIM2_KER_SRC
    #error Source isn't specified for enabled LPTIM2 clock
#else
    #if defined CHIP_CFG_CLK_LPTIM2_KER_SRC
        #define CHIP_CLK_LPTIM2_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_LPTIM2_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_LPTIM2_KER_SRC)
    #else
        #define CHIP_CLK_LPTIM2_KER_SRC_SETUP_EN   0
    #endif
#endif


/* ------------------------ USART1 ---------------------------------*/
#if CHIP_CFG_CLK_USART1_EN && !defined CHIP_CFG_CLK_USART1_KER_SRC
    #error Source isn't specified for enabled USART1 clock
#else
    #if defined CHIP_CFG_CLK_USART1_KER_SRC
        #define CHIP_CLK_USART1_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_USART1_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_USART1_KER_SRC)
    #else
        #define CHIP_CLK_USART1_KER_SRC_SETUP_EN   0
    #endif
#endif

/* ------------------------ USART2 ---------------------------------*/
#if CHIP_DEV_SUPPORT_USART2_ICLKSEL
    #if CHIP_CFG_CLK_USART2_EN && !defined CHIP_CFG_CLK_USART2_KER_SRC
        #error Source isn't specified for enabled USART2 clock
    #else
        #if defined CHIP_CFG_CLK_USART2_KER_SRC
            #define CHIP_CLK_USART2_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_USART2_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_USART2_KER_SRC)
        #else
            #define CHIP_CLK_USART2_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_USART2_KER_SRC_SETUP_EN 0
    #define CHIP_CLK_USART2_KER_FREQ         CHIP_CLK_APB_FREQ
    #if defined CHIP_CFG_CLK_USART2_KER_SRC && (CHIP_CLK_ID_VAL(CHIP_CFG_CLK_USART2_KER_SRC) != CHIP_CLK_APB_ID)
        #error independet USART2 kernel clock source selection is not supported for specified device
    #endif
#endif

/* ------------------------ USART3 ---------------------------------*/
#if CHIP_DEV_SUPPORT_USART3 && CHIP_DEV_SUPPORT_USART3_ICLKSEL
    #if CHIP_CFG_CLK_USART3_EN && !defined CHIP_CFG_CLK_USART3_KER_SRC
        #error Source isn't specified for enabled USART3 clock
    #else
        #if defined CHIP_CFG_CLK_USART3_KER_SRC
            #define CHIP_CLK_USART3_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_USART3_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_USART3_KER_SRC)
        #else
            #define CHIP_CLK_USART3_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_USART3_KER_SRC_SETUP_EN 0
    #define CHIP_CLK_USART3_KER_FREQ         CHIP_CLK_APB_FREQ
    #if defined CHIP_CFG_CLK_USART3_KER_SRC && (!CHIP_DEV_SUPPORT_USART3 || (CHIP_CLK_ID_VAL(CHIP_CFG_CLK_USART3_KER_SRC) != CHIP_CLK_APB_ID))
        #error independet USART3 kernel clock source selection is not supported for specified device
    #endif
#endif

/* для USART4-6 базовая частота не настраивается и всегда APB */
#if CHIP_DEV_SUPPORT_USART4
    #define CHIP_CLK_USART4_KER_FREQ         CHIP_CLK_APB_FREQ
#endif
#if CHIP_DEV_SUPPORT_USART5
    #define CHIP_CLK_USART5_KER_FREQ         CHIP_CLK_APB_FREQ
#endif
#if CHIP_DEV_SUPPORT_USART6
    #define CHIP_CLK_USART6_KER_FREQ         CHIP_CLK_APB_FREQ
#endif

/* ------------------------ LPUART1 ---------------------------------*/
#if CHIP_CFG_CLK_LPUART1_EN && !defined CHIP_CFG_CLK_LPUART1_KER_SRC
    #error Source isn't specified for enabled LPUART1 clock
#else
    #if defined CHIP_CFG_CLK_LPUART1_KER_SRC
        #define CHIP_CLK_LPUART1_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_LPUART1_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_LPUART1_KER_SRC)
    #else
        #define CHIP_CLK_LPUART1_KER_SRC_SETUP_EN   0
    #endif
#endif

/* ------------------------ LPUART2 ---------------------------------*/
#if CHIP_DEV_SUPPORT_LPUART2
    #if CHIP_CFG_CLK_LPUART2_EN && !defined CHIP_CFG_CLK_LPUART2_KER_SRC
        #error Source isn't specified for enabled LPUART2 clock
    #else
        #if defined CHIP_CFG_CLK_LPUART2_KER_SRC
            #define CHIP_CLK_LPUART2_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_LPUART2_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_LPUART2_KER_SRC)
        #else
            #define CHIP_CLK_LPUART2_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_LPUART2_KER_SRC_SETUP_EN 0
    #if defined CHIP_CFG_CLK_LPUART2_KER_SRC
        #error LPUART2 is not supported on specified device
    #endif
#endif


/* -------------- для SPI1-3 базовая частота не настраивается и всегда APB ----------------*/
#define CHIP_CLK_SPI1_KER_FREQ        CHIP_CLK_APB_FREQ
#define CHIP_CLK_SPI2_KER_FREQ        CHIP_CLK_APB_FREQ
#if CHIP_DEV_SUPPORT_SPI3
    #define CHIP_CLK_SPI3_KER_FREQ          CHIP_CLK_APB_FREQ
#endif


/* ------------------------ I2C1 ---------------------------------*/
#if CHIP_CFG_CLK_I2C1_EN && !defined CHIP_CFG_CLK_I2C1_KER_SRC
    #error Source isn't specified for enabled I2C1 clock
#else
    #if defined CHIP_CFG_CLK_I2C1_KER_SRC
        #define CHIP_CLK_I2C1_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_I2C1_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_I2C1_KER_SRC)
    #else
        #define CHIP_CLK_I2C1_KER_SRC_SETUP_EN   0
    #endif
#endif


/* ------------------------ I2C2 ---------------------------------*/
#if CHIP_DEV_SUPPORT_I2C2_ICLKSEL
    #if CHIP_CFG_CLK_I2C2_EN && !defined CHIP_CFG_CLK_I2C2_KER_SRC
        #error Source isn't specified for enabled I2C2 clock
    #else
        #if defined CHIP_CFG_CLK_I2C2_KER_SRC
            #define CHIP_CLK_I2C2_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_I2C2_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_I2C2_KER_SRC)
        #else
            #define CHIP_CLK_I2C2_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_I2C2_KER_SRC_SETUP_EN 0
    #define CHIP_CLK_I2C2_KER_FREQ         CHIP_CLK_APB_FREQ
    #if defined CHIP_CFG_CLK_I2C2_KER_SRC && (CHIP_CLK_ID_VAL(CHIP_CFG_CLK_I2C2_KER_SRC) != CHIP_CLK_APB_ID)
        #error independet I2C2 kernel clock source selection is not supported for specified device
    #endif
#endif

/* --------- I2C3 базовая частота всегда APB --------------------*/
#if CHIP_DEV_SUPPORT_I2C3
    #define #define CHIP_CLK_I2C3_KER_FREQ         CHIP_CLK_APB_FREQ
#endif


/* ------------------------ ADC ---------------------------------*/
#if CHIP_CFG_CLK_ADC_EN && !defined CHIP_CFG_CLK_ADC_KER_SRC
    #error Source isn't specified for enabled ADC clock
#else
    #if defined CHIP_CFG_CLK_ADC_KER_SRC
        #define CHIP_CLK_ADC_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_ADC_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_ADC_KER_SRC)
    #else
        #define CHIP_CLK_ADC_KER_SRC_SETUP_EN   0
    #endif
#endif

/* ------------------------ RNG ---------------------------------*/
#if CHIP_DEV_SUPPORT_RNG
    #if CHIP_CFG_CLK_RNG_EN && !defined CHIP_CFG_CLK_RNG_KER_SRC
        #error Source isn't specified for enabled RNG clock
    #else
        #if defined CHIP_CFG_CLK_RNG_KER_SRC
            #define CHIP_CLK_RNG_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_RNG_KER_FREQ           (CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_RNG_KER_SRC)/CHIP_CFG_CLK_RNG_DIV)
        #else
            #define CHIP_CLK_RNG_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_RNG_KER_SRC_SETUP_EN 0
    #if defined CHIP_CFG_CLK_RNG_KER_SRC
        #error RNG is not supported on specified device
    #endif
#endif


/* ------------------------ I2S1 ---------------------------------*/
#if CHIP_CFG_CLK_I2S1_EN && !defined CHIP_CFG_CLK_I2S1_KER_SRC
    #error Source isn't specified for enabled I2S1 clock
#else
    #if defined CHIP_CFG_CLK_I2S1_KER_SRC
        #define CHIP_CLK_I2S1_KER_SRC_SETUP_EN   1
        #define CHIP_CLK_I2S1_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_I2S1_KER_SRC)
    #else
        #define CHIP_CLK_I2S1_KER_SRC_SETUP_EN   0
    #endif
#endif


/* ------------------------ I2S2 ---------------------------------*/
#if CHIP_DEV_SUPPORT_I2S2
    #if CHIP_CFG_CLK_I2S2_EN && !defined CHIP_CFG_CLK_I2S2_KER_SRC
        #error Source isn't specified for enabled I2S2 clock
    #else
        #if defined CHIP_CFG_CLK_I2S2_KER_SRC
            #define CHIP_CLK_I2S2_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_I2S2_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_I2S2_KER_SRC)
        #else
            #define CHIP_CLK_I2S2_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_I2S2_KER_SRC_SETUP_EN 0
    #if defined CHIP_CFG_CLK_I2S2_KER_SRC
        #error I2S2 is not supported on specified device
    #endif
#endif

/* ------------------------ CEC ---------------------------------*/
#if CHIP_DEV_SUPPORT_CEC
    #define CHIP_CLK_HSI16_DIV488_EN        CHIP_CFG_CLK_HSI16_EN
    #define CHIP_CLK_HSI16_DIV488_FREQ      (CHIP_CFG_CLK_HSI16_FREQ/488)

    #if CHIP_CFG_CLK_CEC_EN && !defined CHIP_CFG_CLK_CEC_KER_SRC
        #error Source isn't specified for enabled CEC clock
    #else
        #if defined CHIP_CFG_CLK_CEC_KER_SRC
            #define CHIP_CLK_CEC_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_CEC_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_CEC_KER_SRC)
        #else
            #define CHIP_CLK_CEC_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_CEC_KER_SRC_SETUP_EN   0
    #ifdef  CHIP_CFG_CLK_CEC_KER_SRC
        #error CEC is not supported on specified device
    #endif
#endif

/* ------------------------ FDCAN ---------------------------------*/
#if CHIP_DEV_SUPPORT_FDCAN
    #if CHIP_CFG_CLK_FDCAN_EN && !defined CHIP_CFG_CLK_FDCAN_KER_SRC
        #error Source isn't specified for enabled FDCAN clock
    #else
        #if defined CHIP_CFG_CLK_FDCAN_KER_SRC
            #define CHIP_CLK_FDCAN_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_FDCAN_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_FDCAN_KER_SRC)
        #else
            #define CHIP_CLK_FDCAN_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_FDCAN_KER_SRC_SETUP_EN   0
    #ifdef  CHIP_CFG_CLK_FDCAN_KER_SRC
        #error FDCAN is not supported on specified device
    #endif
#endif


/* ------------------------ USB ---------------------------------*/
#if CHIP_DEV_SUPPORT_USB
    #if CHIP_CFG_CLK_USB_EN && !defined CHIP_CFG_CLK_USB_KER_SRC
        #error Source isn't specified for enabled USB clock
    #else
        #if defined CHIP_CFG_CLK_USB_KER_SRC
            #define CHIP_CLK_USB_KER_SRC_SETUP_EN   1
            #define CHIP_CLK_USB_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_USB_KER_SRC)
        #else
            #define CHIP_CLK_USB_KER_SRC_SETUP_EN   0
        #endif
    #endif
#else
    #define CHIP_CLK_USB_KER_SRC_SETUP_EN   0
    #ifdef  CHIP_CFG_CLK_USB_KER_SRC
        #error USB is not supported on specified device
    #endif
#endif


#endif // CHIP_CLK_H
