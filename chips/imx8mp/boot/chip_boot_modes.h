#ifndef CHIP_BOOT_MODES_H
#define CHIP_BOOT_MODES_H

#include "regs/regs_src.h"
#include "lbitfield.h"
#include "lcspec.h"
#include <stdint.h>

#define CHIP_BOOT_MODE_FUSES                        0x00 /* Boot from internal fuses */
#define CHIP_BOOT_MODE_USB                          0x01 /* USB Serial Download */
#define CHIP_BOOT_MODE_USDHC3                       0x02 /* USDHC3 (eMMC boot only, SD3 8-bit) */
#define CHIP_BOOT_MODE_USDHC2                       0x03 /* USDHC2 (SD boot only, SD2) */
#define CHIP_BOOT_MODE_NAND_256_PG                  0x04 /* NAND 8-bit single device, 256 pages */
#define CHIP_BOOT_MODE_NAND_512_PG                  0x05 /* NAND 8-bit single device, 512 pages */
#define CHIP_BOOT_MODE_QSPI_NOR                     0x06 /* FlexSPI 3B Read */
#define CHIP_BOOT_MODE_QSPI_HYPERFLASH              0x07 /* FlexSPI Hyperflash 3.3V */
#define CHIP_BOOT_MODE_ECSPI                        0x08 /* eCSPI Boot */
#define CHIP_BOOT_MODE_QSPI_NAND_2K_PG              0x0A /* FLEXSPI Serial NAND 2k page */
#define CHIP_BOOT_MODE_QSPI_NAND_4K_PG              0x0B /* FLEXSPI Serial NAND 4k page */

static LINLINE uint8_t chip_get_cur_boot_mode(void) {
    return LBITFIELD_GET(CHIP_REGS_SRC->SBMR2, CHIP_REGFLD_SRC_SBMR2_IPP_BOOT_MODE);
}


#endif // CHIP_BOOT_MODES_H
