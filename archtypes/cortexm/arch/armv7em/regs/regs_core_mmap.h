#ifndef CHIP_ARMV7EM_REGS_MMAP_H
#define CHIP_ARMV7EM_REGS_MMAP_H

/* Memory mapping of Core Hardware */
#define CHIP_MEMRGN_ADDR_ARMV7EM_SCS                (0xE000E000UL)  /* System Control Space Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_ITM                (0xE0000000UL)  /* ITM Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_DWTE               (0xE0001000UL)  /* DWT Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_TPI                (0xE0040000UL)  /* TPI Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_COREDBG            (0xE000EDF0UL)  /* Core Debug Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_SYSTICK            (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0010UL) /* SysTick Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_NVIC               (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0100UL) /* NVIC Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_SCB                (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0D00UL) /* System Control Block Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_ARM_FEATURES       (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0D40UL) /* System Control Block Base Address */
#define CHIP_MEMRGN_ADDR_ARMV7EM_MPU                (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0D90UL) /* Memory Protection Unit */
#define CHIP_MEMRGN_ADDR_ARMV7EM_FPU                (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0F30UL) /* Floating Point Unit */
#define CHIP_MEMRGN_ADDR_ARMV7EM_CACHE              (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0F50UL) /* Cache maintenance operations */
#define CHIP_MEMRGN_ADDR_ARMV7EM_ACCCTL             (CHIP_MEMRGN_ADDR_ARMV7EM_SCS + 0x0F90UL) /* Access control */


#endif // CHIP_ARMV7EM_REGS_MMAP_H
