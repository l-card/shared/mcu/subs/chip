#ifndef CHIP_STM32H7XX_REGS_BDMA_H_
#define CHIP_STM32H7XX_REGS_BDMA_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

#define CHIP_BDMA_CHANNEL_CNT      8


typedef struct {
    __IO uint32_t CCR;          /* DMA channel x configuration register          */
    __IO uint32_t CNDTR;        /* DMA channel x number of data register         */
    __IO uint32_t CPAR;         /* DMA channel x peripheral address register     */
    __IO uint32_t CM0AR;        /* DMA channel x memory 0 address register       */
    __IO uint32_t CM1AR;        /* DMA channel x memory 1 address register       */
} CHIP_REGS_BDMA_CHANNEL_T;

typedef struct {
    __IO uint32_t ISR;          /* DMA interrupt status register,               Address offset: 0x00 */
    __IO uint32_t IFCR;         /* DMA interrupt flag clear register,           Address offset: 0x04 */
    CHIP_REGS_BDMA_CHANNEL_T CH[CHIP_BDMA_CHANNEL_CNT];
} CHIP_REGS_BDMA_T;

#define CHIP_REGS_BDMA         ((CHIP_REGS_BDMA_T *) CHIP_MEMRGN_ADDR_PERIPH_BDMA)


/*********** BDMA interrupt status/clear registers (BDMA_ISR, BDMA_IFCR) ******/
#define CHIP_REGFLD_BDMA_INT_FLAG(ch, intnum)   (0x00000001UL <<  ((intnum) + (ch * 4))
#define CHIP_REGFLD_BDMA_INT_GIF(x)             CHIP_REGFLD_BDMA_INT_FLAG(x, 0) /* Channel x Global interrupt flag */
#define CHIP_REGFLD_BDMA_INT_CIF(x)             CHIP_REGFLD_BDMA_INT_FLAG(x, 1) /* Channel x Transfer Complete flag */
#define CHIP_REGFLD_BDMA_INT_HTIF(x)            CHIP_REGFLD_BDMA_INT_FLAG(x, 2) /* Channel x Half Transfer flag */
#define CHIP_REGFLD_BDMA_INT_TEIF(x)            CHIP_REGFLD_BDMA_INT_FLAG(x, 3) /* Channel x Transfer Error flag  */

/*********** BDMA channel x configuration register (BDMA_CCRx) ****************/
#define CHIP_REGFLD_BDMA_CCR_EN                 (0x00000001UL <<  0U) /* Channel enable */
#define CHIP_REGFLD_BDMA_CCR_TCIE               (0x00000001UL <<  1U) /* Transfer complete interrupt enable */
#define CHIP_REGFLD_BDMA_CCR_HTIE               (0x00000001UL <<  2U) /* Half Transfer interrupt enable */
#define CHIP_REGFLD_BDMA_CCR_TEIE               (0x00000001UL <<  3U) /* Transfer error interrupt enable */
#define CHIP_REGFLD_BDMA_CCR_DIR                (0x00000001UL <<  4U) /* Data transfer direction */
#define CHIP_REGFLD_BDMA_CCR_CIRC               (0x00000001UL <<  5U) /* Circular mode */
#define CHIP_REGFLD_BDMA_CCR_PINC               (0x00000001UL <<  6U) /* Peripheral increment mode */
#define CHIP_REGFLD_BDMA_CCR_MINC               (0x00000001UL <<  7U) /* Memory increment mode */
#define CHIP_REGFLD_BDMA_CCR_PSIZE              (0x00000003UL <<  8U) /* Peripheral size */
#define CHIP_REGFLD_BDMA_CCR_MSIZE              (0x00000003UL << 10U) /* Memory size */
#define CHIP_REGFLD_BDMA_CCR_PL                 (0x00000003UL << 12U) /* Channel Priority level */
#define CHIP_REGFLD_BDMA_CCR_MEM2MEM            (0x00000001UL << 14U) /* Memory to memory mode */
#define CHIP_REGFLD_BDMA_CCR_DBM                (0x00000001UL << 15U) /* Double-buffer mode */
#define CHIP_REGFLD_BDMA_CCR_CT                 (0x00000001UL << 16U) /* Current target memory of DMA transfer in double-buffer mode */

#define CHIP_REGMSK_BDMA_CCR_RESERVED           (0xFFFE0000UL)

#define CHIP_REGFLDVAL_BDMA_CCR_DIR_PTOM        0 /* read from peripheral */
#define CHIP_REGFLDVAL_BDMA_CCR_DIR_MTOP        1 /* read from memory */

#define CHIP_REGFLDVAL_BDMA_CCR_SIZE_8          0
#define CHIP_REGFLDVAL_BDMA_CCR_SIZE_16         1
#define CHIP_REGFLDVAL_BDMA_CCR_SIZE_32         2

#define CHIP_REGFLDVAL_BDMA_CCR_PL_LOW          0
#define CHIP_REGFLDVAL_BDMA_CCR_PL_MEDIUM       1
#define CHIP_REGFLDVAL_BDMA_CCR_PL_HIGH         2
#define CHIP_REGFLDVAL_BDMA_CCR_PL_VHIGH        3



/*********** BDMA channel x number of data to transfer register (BDMA_CNDTRx) */
#define CHIP_REGFLD_BDMA_CNDTR_NDT              (0x0000FFFFUL <<  0U) /* Number of data to Transfer */
#define CHIP_REGMSK_BDMA_CNDTR_RESERVED         (0xFFFF0000UL)

/*********** BDMA channel x peripheral address register (BDMA_CPARx) **********/
#define CHIP_REGFLD_BDMA_CPAR_PA                (0xFFFFFFFFUL <<  0U) /* Peripheral Address */

/*********** BDMA channel x memory 0 address register (BDMA_CM0ARx) ***********/
#define CHIP_REGFLD_BDMA_CM0AR_MA               (0xFFFFFFFFUL <<  0U) /* Memory Address */

/*********** BDMA channel x memory 1 address register (BDMA_CM1ARx) ***********/
#define CHIP_REGFLD_BDMA_CM1AR_MA               (0xFFFFFFFFUL <<  0U) /* Memory Address */


#endif // CHIP_STM32H7XX_REGS_BDMA_H_
