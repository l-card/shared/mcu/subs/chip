#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#define CHIP_DEV_N32G45X
#define CHIP_DEV_N32G457

/** @todo not added to pins */

#define CHIP_DEV_SUPPORT_EXMC           0
#define CHIP_DEV_SUPPORT_TIM9           0
#define CHIP_DEV_SUPPORT_TIM10          0
#define CHIP_DEV_SUPPORT_TIM11          0
#define CHIP_DEV_SUPPORT_TIM12          0
#define CHIP_DEV_SUPPORT_TIM13          0
#define CHIP_DEV_SUPPORT_TIM14          0
#define CHIP_DEV_SUPPORT_TRACE          1

#define CHIP_DEV_TIM_GP_CNT             4
#define CHIP_DEV_TIM_GP32_CNT           0
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          2
#define CHIP_DEV_SPI_CNT                3
#define CHIP_DEV_I2S_CNT                2
#define CHIP_DEV_I2S_FD_CNT             0 /* ? check */
#define CHIP_DEV_I2C_CNT                4
#define CHIP_DEV_UART_CNT               7
#define CHIP_DEV_CAN_CNT                2
#define CHIP_DEV_USB_CNT                1
#define CHIP_DEV_SDIO_CNT               1
#define CHIP_DEV_ETH_CNT                1
#define CHIP_DEV_ADC_CNT                2
#define CHIP_DEV_DAC_CNT                2
#define CHIP_DEV_SPIM_CNT               0
#define CHIP_DEV_QSPI_CNT               1
#define CHIP_DEV_DVP_CNT                1

#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(144)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
