﻿#ifndef CHIP_PINS_H
#define CHIP_PINS_H

#include "chip_pin_defs_v1.h"
#include "init/chip_cfg_defs.h"
#include "init/chip_ioremap_defs.h"
#include "chip_devtype_spec_features.h"


#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4
#define CHIP_PORT_F 5
#define CHIP_PORT_G 6



/* ------------------------ Функции пинов ------------------------------------*/

/********************************** Порт A ************************************/
/*----- Пин PA0  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 0)
#define CHIP_PIN_PA0_ADC_IN0            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 0)                                                /* all */
#define CHIP_PIN_PA0_WKUP1              CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_WKUP,   CHIP_PIN_REMAP_NA)         /* all */
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_A2_4_A0_1)
#define CHIP_PIN_PA0_USART2_CTS         CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_3) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_1_B10_11)
#define CHIP_PIN_PA0_TIM2_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA0_TIM2_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 0, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA0_TIM2_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PA0_TIM5_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA0_TIM5_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 0, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA0_TIM8_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PA0_ETH_MII_CRS_WKUP   CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA0_UART4_TX           CHIP_PIN_ID_AF      (CHIP_PORT_A, 0, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA0_SPI3_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(3))         /* n45x */
#define CHIP_PIN_PA0_SPI3_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_A, 0, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(3))         /* n45x */
#endif
#if defined CHIP_DEV_CH32XXXX
#define CHIP_PIN_PA0_OPA4_OUT0          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 0)                                                /* chv307, chf20x */
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PA0_ETH_RGMII_RXD2     CHIP_PIN_ID_IN      (CHIP_PORT_A, 0, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif
#endif


/*----- Пин PA1  ------------------------------------------------------------ */
#define CHIP_PIN_PA1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 1)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA1_ADC1_IN1           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 1)                                                /* n45x */
#else
#define CHIP_PIN_PA1_ADC_IN1            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 1)                                                /* !n45x */
#endif
#if defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA1_WKUP2              CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_WKUP,   CHIP_PIN_REMAP_NA)         /* v84x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_A2_4_A0_1)
#define CHIP_PIN_PA1_USART2_RTS         CHIP_PIN_ID_AF      (CHIP_PORT_A, 1, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_3) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_1_B10_11)
#define CHIP_PIN_PA1_TIM2_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA1_TIM2_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 1, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PA1_TIM5_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA1_TIM5_CH2_OUT       CHIP_PIN_ID_OUT     (CHIP_PORT_A, 1, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PA1_ETH_MII_RX_CLK     CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#define CHIP_PIN_PA1_ETH_RMII_REF_CLK   CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA1_UART4_RX           CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA1_SPI3_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_A, 1, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(3))         /* n45x */
#define CHIP_PIN_PA1_SPI3_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(3))         /* n45x */
#define CHIP_PIN_PA1_I2S3_SD_TR         CHIP_PIN_ID_AF      (CHIP_PORT_A, 1, CHIP_PER_ID_I2S3,  CHIP_PIN_REMAP1(3))         /* n45x */
#define CHIP_PIN_PA1_I2S3_SD_RV         CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_I2S3,  CHIP_PIN_REMAP1(3))         /* n45x */
#endif
#if defined CHIP_DEV_CH32XXXX
#define CHIP_PIN_PA1_OPA3_OUT0          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 1)                                                /* chv307, chf20x */
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PA1_TIM9_BKIN          CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP1(1))        /* chv307, chf20x */
#endif
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PA1_ETH_RGMII_RXD3     CHIP_PIN_ID_IN      (CHIP_PORT_A, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PA2  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 2)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA2_ADC_IN10           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 2)                                                /* n45x */
#else
#define CHIP_PIN_PA2_ADC_IN2            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 2)                                                /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_A2_4_A0_1)
#define CHIP_PIN_PA2_USART2_TX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_3) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_A2_3)
#define CHIP_PIN_PA2_TIM2_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 2, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA2_TIM2_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PA2_TIM5_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 2, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA2_TIM5_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PA2_ETH_MII_MDIO       CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#define CHIP_PIN_PA2_ETH_RMII_MDIO      CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_CH32XXXX || defined CHIP_DEV_GD32F10X)
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5) || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PA2_TIM9_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 2, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, chv307, chf20x, gdf103zf/g/i/k */
#define CHIP_PIN_PA2_TIM9_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, chv307, chf20x, gdf103zf/g/i/k */
#endif
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA2_EXMC_D4            CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))      /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA2_SDIO2_CK           CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP1(1))       /* at40x */
#endif
#if defined CHIP_DEV_CH32XXXX
#define CHIP_PIN_PA2_OPA2_OUT0          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 2)                                                /* chv307, chf20x */
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5) || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PA2_TIM9_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_A, 2, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */
#endif
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PA2_ETH_RGMII_GTXC     CHIP_PIN_ID_AF      (CHIP_PORT_A, 2, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif
#endif


/*----- Пин PA3  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 3)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA3_ADC1_IN3           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 3)                                                /* n45x */
#else
#define CHIP_PIN_PA3_ADC_IN3            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 3)                                                /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_A2_4_A0_1)
#define CHIP_PIN_PA3_USART2_RX          CHIP_PIN_ID_IN      (CHIP_PORT_A, 3, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_3) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_A2_3)
#define CHIP_PIN_PA3_TIM2_CH4_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 3, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA3_TIM2_CH4_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 3, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PA3_TIM5_CH4_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 3, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA3_TIM5_CH4_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 3, CHIP_PER_ID_TIM5,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PA3_ETH_MII_COL        CHIP_PIN_ID_IN      (CHIP_PORT_A, 3, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_CH32XXXX || defined CHIP_DEV_GD32F10X)
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5) || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PA3_TIM9_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 3, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, chv307, chf20x, chf20x, gdf103zf/g/i/k */
#define CHIP_PIN_PA3_TIM9_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 3, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, chv307, chf20x, chf20x, gdf103zf/g/i/k */
#endif
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA3_EXMC_D5            CHIP_PIN_ID_AF      (CHIP_PORT_A, 3, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))      /* at40x, v84x */
#define CHIP_PIN_PA3_I2S2_MCK           CHIP_PIN_ID_AF      (CHIP_PORT_A, 3, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA3_SDIO2_CMD          CHIP_PIN_ID_AF      (CHIP_PORT_A, 3, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP1(1))       /* at40x */
#endif
#if defined CHIP_DEV_CH32XXXX
#define CHIP_PIN_PA3_OPA1_OUT0          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 3)                                                /* chv307, chf20x */
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PA3_ETH_RGMII_TXEN     CHIP_PIN_ID_AF      (CHIP_PORT_A, 3, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif
#endif


/*----- Пин PA4  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 4)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA4_ADC2_IN0           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 4)                                                /* n45x */
#elif CHIP_DEV_ADC_CNT >= 3
#define CHIP_PIN_PA4_ADC12_IN4          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 4)                                                /* !n45x & !v84x & !chv307 */
#else
#define CHIP_PIN_PA4_ADC_IN4            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 4)                                                /* v84x, chv307 */
#endif
#if CHIP_DEV_DAC_CNT >= 1
#define CHIP_PIN_PA4_DAC_OUT1           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 4)                                                /* !v84x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_A2_4_A0_1)
#define CHIP_PIN_PA4_USART2_CK          CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_USART2, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A4_7)
#define CHIP_PIN_PA4_SPI1_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA4_SPI1_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_A, 4, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_C10_12_A4))
#define CHIP_PIN_PA4_SPI3_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP2(1,3))      /* at40x, v84x, chv307, gd10x */
#define CHIP_PIN_PA4_SPI3_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_A, 4, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP2(1,3))      /* at40x, v84x, chv307, gd10x */
#endif
#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_I2S_CNT >= 2) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_C10_12_A4))
#define CHIP_PIN_PA4_I2S3_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP2(1,3))      /* at40x, v84x, chv307, gd10x */
#define CHIP_PIN_PA4_I2S3_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_A, 4, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP2(1,3))      /* at40x, v84x, chv307, gd10x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA4_I2S1_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PA4_I2S1_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_A, 4, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PA4_USART6_TX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_USART6, CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#define CHIP_PIN_PA4_EXMC_D6            CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))      /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA4_SDIO2_D4           CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)   /* at40x */
#define CHIP_PIN_PA4_SDIO2_D0           CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP1(1))       /* at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA4_QSPI_NSS           CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#define CHIP_PIN_PA4_I2C2_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_A, 4, CHIP_PER_ID_I2C2,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5) || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PA4_TIM9_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 4, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PA4_TIM9_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 4, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#define CHIP_PIN_PA4_DVP_HSYNC          CHIP_PIN_ID_IN      (CHIP_PORT_A, 4, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif



/*----- Пин PA5 ------------------------------------------------------------- */
#define CHIP_PIN_PA5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 5)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA5_ADC2_IN1           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 5)                                                /* n45x */
#elif CHIP_DEV_ADC_CNT >= 3
#define CHIP_PIN_PA5_ADC12_IN5          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 5)                                                /* !n45x && !v84x && !chv307 */
#else
#define CHIP_PIN_PA5_ADC_IN5            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 5)                                                /* v84x, chv307 */
#endif
#if CHIP_DEV_DAC_CNT >= 2
#define CHIP_PIN_PA5_DAC_OUT2           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 5)                                                /* !v84x */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A4_7)
#define CHIP_PIN_PA5_SPI1_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA5_SPI1_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_A, 5, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA5_I2S1_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PA5_I2S1_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_A, 5, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PA5_USART6_RX          CHIP_PIN_ID_IN      (CHIP_PORT_A, 5, CHIP_PER_ID_USART6, CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#define CHIP_PIN_PA5_EXMC_D7            CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))      /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA5_SDIO2_D5           CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_SDIO2,  CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#define CHIP_PIN_PA5_SDIO2_D1           CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_SDIO2,  CHIP_PIN_REMAP1(1))        /* at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA5_QSPI_SCK           CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#define CHIP_PIN_PA5_I2C2_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_A, 5, CHIP_PER_ID_I2C2,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PA5_DVP_VSYNC          CHIP_PIN_ID_IN      (CHIP_PORT_A, 5, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PA5_TIM10_CH1N         CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_A8_10_B15)
#define CHIP_PIN_PA5_USART1_CTS         CHIP_PIN_ID_IN      (CHIP_PORT_A, 5, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_7_C4_5)
#define CHIP_PIN_PA5_USART1_CK          CHIP_PIN_ID_AF      (CHIP_PORT_A, 5, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(3))        /* chv307 */
#endif
#endif


/*----- Пин PA6  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 6)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA6_ADC2_IN2           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 6)                                                /* n45x */
#elif CHIP_DEV_ADC_CNT >= 3
#define CHIP_PIN_PA6_ADC12_IN6          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 6)                                                /* !n45x && !v84x && !chv307*/
#else
#define CHIP_PIN_PA6_ADC_IN6            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 6)                                                /* v84x, chv307 */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A4_7)
#define CHIP_PIN_PA6_SPI1_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_A, 6, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA6_SPI1_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_A6_7_B0_1)
#define CHIP_PIN_PA6_TIM3_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 6, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA6_TIM3_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PA6_TIM8_BKIN          CHIP_PIN_ID_IN      (CHIP_PORT_A, 6, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PA6_TIM1_BKIN          CHIP_PIN_ID_IN      (CHIP_PORT_A, 6, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(1))        /* all */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_GD32F10X) && CHIP_DEV_SUPPORT_TIM13
#define CHIP_PIN_PA6_TIM13_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_A, 6, CHIP_PER_ID_TIM13,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, gdf103zf/g/i/k */
#define CHIP_PIN_PA6_TIM13_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_TIM13,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, gdf103zf/g/i/k */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX)
#define CHIP_PIN_PA6_I2S2_MCK           CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(2))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA6_SDIO2_D6           CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#define CHIP_PIN_PA6_SDIO2_D2           CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP1(1))        /* at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA6_QSPI_IO0           CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PA6_DVP_PCLK           CHIP_PIN_ID_IN      (CHIP_PORT_A, 6, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)     /* chv307 */
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_7_C4_5)
#define CHIP_PIN_PA6_USART1_TX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(3))        /* chv307 */
#endif
#if !defined CHIP_IOREMAP_USART7 || (CHIP_IOREMAP_USART7 == CHIP_IOREMAP_USART7_PA6_7)
#define CHIP_PIN_PA6_UART7_TX           CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PA6_TM10_CH2N          CHIP_PIN_ID_AF      (CHIP_PORT_A, 6, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#define CHIP_PIN_PA6_OPA1_CH1N          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 6)                                                /* chv307 */
#endif
#endif



/*----- Пин PA7  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 7)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA7_ADC2_IN3           CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 7)                                                /* n45x */
#elif CHIP_DEV_ADC_CNT >= 3
#define CHIP_PIN_PA7_ADC12_IN7          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 7)                                                /* !n45x && !v84x && !chv307*/
#else
#define CHIP_PIN_PA7_ADC_IN7            CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 7)                                                /* v84x, chv307 */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A4_7)
#define CHIP_PIN_PA7_SPI1_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA7_SPI1_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_A6_7_B0_1)
#define CHIP_PIN_PA7_TIM3_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA7_TIM3_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PA7_TIM8_CH1N          CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT))   /* all */
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PA7_TIM1_CH1N          CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(1))        /* all */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_A7_C4_5_B0_1))
#define CHIP_PIN_PA7_ETH_MII_RX_DV      CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#define CHIP_PIN_PA7_ETH_RMII_CRS_DV    CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_GD32F10X) && CHIP_DEV_SUPPORT_TIM14
#define CHIP_PIN_PA7_TIM14_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_TIM14,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, gdf103zf/g/i/k */
#define CHIP_PIN_PA7_TIM14_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_TIM14,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, gdf103zf/g/i/k */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA7_I2S1_SD_TR         CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PA7_I2S1_SD_RV         CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA7_SDIO2_D7           CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#define CHIP_PIN_PA7_SDIO2_D3           CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP1(1))        /* at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA7_QSPI_IO1           CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PA7_RGMII_TXD0         CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_7_C4_5)
#define CHIP_PIN_PA7_USART1_RX          CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(3))        /* chv307 */
#endif
#if !defined CHIP_IOREMAP_USART7 || (CHIP_IOREMAP_USART7 == CHIP_IOREMAP_USART7_PA6_7)
#define CHIP_PIN_PA7_UART7_RX           CHIP_PIN_ID_IN      (CHIP_PORT_A, 7, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PA7_TM10_CH3N          CHIP_PIN_ID_AF      (CHIP_PORT_A, 7, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#define CHIP_PIN_PA7_OPA2_CH1P          CHIP_PIN_ID_ANALOG  (CHIP_PORT_A, 7)                                                /* chv307 */
#endif
#endif


/*----- Пин PA8  ------------------------------------------------------------ */
#define CHIP_PIN_PA8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 8)
#define CHIP_PIN_PA8_MCO                CHIP_PIN_ID_AF      (CHIP_PORT_A, 8, CHIP_PER_ID_CLKOUT, CHIP_PIN_REMAP_DEFAULT)    /* all */
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A9_10_A8_A11_12) || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_B6_7_A8_A11_12)
#define CHIP_PIN_PA8_USART1_CK          CHIP_PIN_ID_AF      (CHIP_PORT_A, 8, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15) || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PA8_TIM1_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 8, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PA8_TIM1_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 8, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA8_I2C3_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_A, 8, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PA8_USB_SOF            CHIP_PIN_ID_AF      (CHIP_PORT_A, 8, CHIP_PER_ID_USB,    CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA8_SPIM_CS            CHIP_PIN_ID_AF      (CHIP_PORT_A, 8, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#endif
#if defined CHIP_DEV_CH32V307 && (!defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_A8_10_B15))
#define CHIP_PIN_PA8_USART1_RX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 8, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(2))        /* chv307 */
#endif


/*----- Пин PA9  ------------------------------------------------------------ */
#define CHIP_PIN_PA9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 9)
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A9_10_A8_A11_12)
#define CHIP_PIN_PA9_USART1_TX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 9, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15) || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PA9_TIM1_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_A, 9, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP2(0,1))      /* all */
#define CHIP_PIN_PA9_TIM1_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_A, 9, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP2(0,1))      /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA9_I2C3_SMBA          CHIP_PIN_ID_AF_OD   (CHIP_PORT_A, 9, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA9_I2C4_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_A, 9, CHIP_PER_ID_I2C4,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PA9_USB1_VBUS          CHIP_PIN_ID_AF      (CHIP_PORT_A, 9, CHIP_PER_ID_USB1,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PA9_DVP_D0             CHIP_PIN_ID_IN      (CHIP_PORT_A, 9, CHIP_PER_ID_DVP,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_A8_10_B15)
#define CHIP_PIN_PA9_USART1_RTS         CHIP_PIN_ID_AF      (CHIP_PORT_A, 9, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif


/*----- Пин PA10 ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 10)
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A9_10_A8_A11_12)
#define CHIP_PIN_PA10_USART1_RX         CHIP_PIN_ID_IN      (CHIP_PORT_A, 10, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15) || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PA10_TIM1_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_A, 10, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PA10_TIM1_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_A, 10, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA10_I2S4_MCLK         CHIP_PIN_ID_AF      (CHIP_PORT_A, 10, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA10_I2C4_SDA          CHIP_PIN_ID_AF_OD   (CHIP_PORT_A, 10, CHIP_PER_ID_I2C4,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PA10_USB1_ID           CHIP_PIN_ID_AF      (CHIP_PORT_A, 10, CHIP_PER_ID_USB1,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PA10_DVP_D1            CHIP_PIN_ID_IN      (CHIP_PORT_A, 10, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_A8_10_B15)
#define CHIP_PIN_PA10_USART1_CK         CHIP_PIN_ID_AF      (CHIP_PORT_A, 10, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PA11 ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA11_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 11)
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A9_10_A8_A11_12) || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_B6_7_A8_A11_12)
#define CHIP_PIN_PA11_USART1_CTS        CHIP_PIN_ID_IN      (CHIP_PORT_A, 11, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#define CHIP_PIN_PA11_USB1_DM           CHIP_PIN_ID_FORCED  (CHIP_PORT_A, 11, CHIP_PER_ID_USB1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if !defined CHIP_IOREMAP_CAN1 || (CHIP_IOREMAP_CAN1 == CHIP_IOREMAP_CAN1_A11_12)
#define CHIP_PIN_PA11_CAN1_RX           CHIP_PIN_ID_IN      (CHIP_PORT_A, 11, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15) || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PA11_TIM1_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_A, 11, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PA11_TIM1_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_A, 11, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA11_SPIM_IO0          CHIP_PIN_ID_AF      (CHIP_PORT_A, 11, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP_DEFAULT)   /* at40x */
#endif
#endif


/*----- Пин PA12 ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA12_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 12)
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A9_10_A8_A11_12) || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_B6_7_A8_A11_12)
#define CHIP_PIN_PA12_USART1_RTS        CHIP_PIN_ID_AF      (CHIP_PORT_A, 12, CHIP_PER_ID_USART1, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#define CHIP_PIN_PA12_USB1_DP           CHIP_PIN_ID_FORCED  (CHIP_PORT_A, 12, CHIP_PER_ID_USB1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if !defined CHIP_IOREMAP_CAN1 || (CHIP_IOREMAP_CAN1 == CHIP_IOREMAP_CAN1_A11_12)
#define CHIP_PIN_PA12_CAN1_TX           CHIP_PIN_ID_AF      (CHIP_PORT_A, 12, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15) || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PA12_TIM1_ETR          CHIP_PIN_ID_IN      (CHIP_PORT_A, 12, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PA12_SPIM_IO1          CHIP_PIN_ID_AF      (CHIP_PORT_A, 12, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP_DEFAULT)   /* at40x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PA12_TIM10_CH1N        CHIP_PIN_ID_AF      (CHIP_PORT_A, 12, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)   /* chv307 */
#endif
#endif
#endif


/*----- Пин PA13 (JTMS-SWDIO) ----------------------------------------------- */
#if !defined CHIP_IOREMAP_JTAG || (CHIP_IOREMAP_JTAG == CHIP_IOREMAP_JTAG_DIS)
#define CHIP_PIN_PA13_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 13)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA13_UART4_TX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 13, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(2))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PA13_TIM10_CH2N        CHIP_PIN_ID_AF      (CHIP_PORT_A, 13, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)   /* chv307 */
#endif
#define CHIP_PIN_PA13_TIM8_CH1N         CHIP_PIN_ID_AF      (CHIP_PORT_A, 13, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))       /* chv307 */
#endif
#if defined CHIP_DEV_CH32V3XX && (!defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 ==CHIP_IOREMAP_USART3_A13_14_D10_12))
#define CHIP_PIN_A13_USART3_TX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 13, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(2))       /* chv307 (r2.0) */
#endif
#endif


/*----- Пин PA14 (JTCK-SWCLK) ----------------------------------------------- */
#if !defined CHIP_IOREMAP_JTAG || (CHIP_IOREMAP_JTAG == CHIP_IOREMAP_JTAG_DIS)
#define CHIP_PIN_PA14_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 14)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA14_UART4_RX          CHIP_PIN_ID_IN      (CHIP_PORT_A, 14, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(2))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PA14_TIM10_CH3N        CHIP_PIN_ID_AF      (CHIP_PORT_A, 14, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)   /* chv307 */
#endif
#define CHIP_PIN_PA14_TIM8_CH2N         CHIP_PIN_ID_AF      (CHIP_PORT_A, 14, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))       /* chv307 */
#if !defined CHIP_IOREMAP_USART8 || (CHIP_IOREMAP_USART8 == CHIP_IOREMAP_USART8_PA14_15)
#define CHIP_PIN_PA14_UART8_TX          CHIP_PIN_ID_AF      (CHIP_PORT_A, 14, CHIP_PER_ID_UART8,  CHIP_PIN_REMAP1(1))       /* chv307 */
#endif
#endif
#if defined CHIP_DEV_CH32V3XX && (!defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 ==CHIP_IOREMAP_USART3_A13_14_D10_12))
#define CHIP_PIN_A14_USART3_RX          CHIP_PIN_ID_IN      (CHIP_PORT_A, 14, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(2))       /* chv307 (r2.0) */
#endif
#endif


/*----- Пин PA15 (JTDI) ----------------------------------------------------- */
#if !defined CHIP_IOREMAP_JTAG || (CHIP_IOREMAP_JTAG == CHIP_IOREMAP_JTAG_SWD) || (CHIP_IOREMAP_JTAG == CHIP_IOREMAP_JTAG_DIS)
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PA15_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_A, 15)
#if (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_B3_5_A15))
#define CHIP_PIN_PA15_SPI3_NSS_MST      CHIP_PIN_ID_AF      (CHIP_PORT_A, 15, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PA15_SPI3_NSS_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if (CHIP_DEV_I2S_CNT >= 2) && (!defined CHIP_IOREMAP_SPI3 && (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_B3_5_A15))
#define CHIP_PIN_PA15_I2S3_WS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_A, 15, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PA15_I2S3_WS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_A2_3) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_B10_11)
#define CHIP_PIN_PA15_TIM2_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP1(1,3))     /* all */
#define CHIP_PIN_PA15_TIM2_CH1_OUT      CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP1(1,3))     /* all */
#define CHIP_PIN_PA15_TIM2_ETR          CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP1(1,3))     /* all */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A15_B3_5)
#define CHIP_PIN_PA15_SPI1_NSS_MST      CHIP_PIN_ID_AF      (CHIP_PORT_A, 15, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(1,3))     /* all (в n45x другой map3) */
#define CHIP_PIN_PA15_SPI1_NSS_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(1,3))     /* all (в n45x другой map3) */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PA15_I2S1_WS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_A, 15, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP1(1,3))     /* at40x, v84x */
#define CHIP_PIN_PA15_I2S1_WS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP1(1,3))     /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PA15_UART2_CTS         CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_UART2,  CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PA15_TIM8_CH1N         CHIP_PIN_ID_AF      (CHIP_PORT_A, 15, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1,3))     /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PA15_TIM8_CH3N         CHIP_PIN_ID_AF      (CHIP_PORT_A, 15, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))       /* chv307 */
#if !defined CHIP_IOREMAP_USART8 || (CHIP_IOREMAP_USART8 == CHIP_IOREMAP_USART8_PA14_15)
#define CHIP_PIN_PA15_UART8_RX          CHIP_PIN_ID_IN      (CHIP_PORT_A, 15, CHIP_PER_ID_UART8,  CHIP_PIN_REMAP1(1))       /* chv307 */
#endif
#endif
#endif
#endif


/*********************************** Порт B ***********************************/
/*----- Пин PB0  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 0)
#if !defined CHIP_DEV_N32G45X
#if CHIP_DEV_ADC_CNT >= 2
#define CHIP_PIN_PB0_ADC12_IN8          CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 0)                                                /* st10x, at40x, gd10x */
#else
#define CHIP_PIN_PB0_ADC_IN8            CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 0)                                                /* v84x, chv307 */
#endif
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_A6_7_B0_1) || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_B4_5_B0_1)
#define CHIP_PIN_PB0_TIM3_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 0, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB0_TIM3_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 0, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PB0_TIM8_CH2N          CHIP_PIN_ID_AF      (CHIP_PORT_B, 0, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PB0_TIM1_CH2N          CHIP_PIN_ID_AF      (CHIP_PORT_B, 0, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(1))        /* all */
#endif
#if (CHIP_DEV_ETH_CNT >= 1)  && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_A7_C4_5_B0_1))
#define CHIP_PIN_PB0_ETH_MII_RXD2       CHIP_PIN_ID_IN      (CHIP_PORT_B, 0, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB0_I2S1_MCK           CHIP_PIN_ID_AF      (CHIP_PORT_B, 0, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP_DEFAULT)    /*  at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB0_UART6_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 0, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB0_ETH_RGMII_RXD3     CHIP_PIN_ID_IN      (CHIP_PORT_B, 0, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_UART4 || (CHIP_IOREMAP_UART4 == CHIP_IOREMAP_UART4_PB0_1)
#define CHIP_PIN_PB0_UART4_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 0, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PB0_TIM9_CH1N          CHIP_PIN_ID_AF      (CHIP_PORT_B, 0, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#define CHIP_PIN_PB0_OPA1_CH1P          CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 0)                                                /* chv307 */
#endif
#endif


/*----- Пин PB1  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 1)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB1_ADC2_IN2           CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 1)                                                /* n45x */
#elif CHIP_DEV_ADC_CNT >= 2
#define CHIP_PIN_PB1_ADC12_IN9          CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 1)                                                /* st10x, at40x, gd10x */
#else
#define CHIP_PIN_PB1_ADC_IN9            CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 1)                                                /* v84x, chv307 */
#endif
#if (CHIP_DEV_ETH_CNT >= 1)  && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_A7_C4_5_B0_1))
#define CHIP_PIN_PB1_ETH_MII_RXD3       CHIP_PIN_ID_IN      (CHIP_PORT_B, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_A6_7_B0_1) || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_B4_5_B0_1)
#define CHIP_PIN_PB1_TIM3_CH4_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 1, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB1_TIM3_CH4_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 1, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PB1_TIM8_CH3N          CHIP_PIN_ID_AF      (CHIP_PORT_B, 1, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_A6_7_B0_1)
#define CHIP_PIN_PB1_TIM1_CH3N          CHIP_PIN_ID_AF      (CHIP_PORT_B, 1, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(1))        /* all */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PB1_SPIM_SCK           CHIP_PIN_ID_AF      (CHIP_PORT_B, 1, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP_DEFAULT)    /*  at40x  */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB1_UART6_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 1, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB1_ETH_RGMII_125IN    CHIP_PIN_ID_IN      (CHIP_PORT_B, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_UART4 || (CHIP_IOREMAP_UART4 == CHIP_IOREMAP_UART4_PB0_1)
#define CHIP_PIN_PB1_UART4_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 1, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PB1_TIM9_CH2N          CHIP_PIN_ID_AF      (CHIP_PORT_B, 1, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#define CHIP_PIN_PB1_OPA4_CH0N          CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 1)                                                /* chv307 */
#endif
#endif


/*----- Пин PB2 (BOOT1) ----------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 2)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB2_ADC2_IN12          CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 2)                                                /* n45x */
#define CHIP_PIN_PB2_UART4_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 2, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PB2_SPI1_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_B, 2, CHIP_PER_ID_SPI1,  CHIP_PIN_REMAP2(2,3))       /* n45x */
#define CHIP_PIN_PB2_SPI1_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_B, 2, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP2(2,3))      /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PB2_TIM9_CH3N          CHIP_PIN_ID_AF      (CHIP_PORT_B, 2, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#define CHIP_PIN_PB2_OPA3_CH0N          CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 2)                                                /* chv307 */
#endif
#endif



/*----- Пин PB3 (JTDO/TRACESWO) --------------------------------------------- */
#if !defined CHIP_IOREMAP_JTAG || (CHIP_IOREMAP_JTAG == CHIP_IOREMAP_JTAG_SWD) || (CHIP_IOREMAP_JTAG == CHIP_IOREMAP_JTAG_DIS)
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 3)
#if (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_B3_5_A15))
#define CHIP_PIN_PB3_SPI3_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB3_SPI3_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_B, 3, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if (CHIP_DEV_I2S_CNT >= 2) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_B3_5_A15))
#define CHIP_PIN_PB3_I2S3_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB3_I2S3_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_B, 3, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_A2_3) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_B10_11)
#define CHIP_PIN_PB3_TIM2_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 3, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP2(1,3))      /* all */
#define CHIP_PIN_PB3_TIM2_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP2(1,3))      /* all */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A15_B3_5)
#define CHIP_PIN_PB3_SPI1_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP2(1,3))      /* all */
#define CHIP_PIN_PB3_SPI1_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_B, 3, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP2(1,3))      /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB3_I2S1_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP2(1,3))      /* at40x, v84x */
#define CHIP_PIN_PB3_I2S1_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_B, 3, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP2(1,3))      /* at40x, v84x */
#define CHIP_PIN_PB3_UART7_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 3, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB3_UART2_RTS          CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_UART2,  CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PB3_TIM8_BKIN          CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP2(1,3))      /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PB3_TIM10_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 3, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#define CHIP_PIN_PB3_TIM10_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 3, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#endif
#endif
#endif



/*----- Пин PB4 (NJTRST) ---------------------------------------------------- */
#if !defined CHIP_IOREMAP_JTAG || (CHIP_IOREMAP_JTAG != CHIP_IOREMAP_JTAG_FULL)
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 4)
#if (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_B3_5_A15))
#define CHIP_PIN_PB4_SPI3_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_B, 4, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB4_SPI3_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_B4_5_B0_1)
#define CHIP_PIN_PB4_TIM3_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 4, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(2))        /* all */
#define CHIP_PIN_PB4_TIM3_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(2))        /* all */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A15_B3_5)
#define CHIP_PIN_PB4_SPI1_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_B, 4, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP2(1,3))      /* all */
#define CHIP_PIN_PB4_SPI1_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP2(1,3))      /* all */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PB4_I2S3_SDEXT_TR      CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#define CHIP_PIN_PB4_I2S3_SDEXT_RV      CHIP_PIN_ID_IN      (CHIP_PORT_B, 4, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB4_UART7_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#define CHIP_PIN_PB4_I2C3_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 4, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB4_UART2_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_UART2,  CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PB4_TIM8_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_B, 4, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP2(1,3))      /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_UART5 || (CHIP_IOREMAP_UART5 == CHIP_IOREMAP_UART5_PB4_5)
#define CHIP_PIN_PB4_UART5_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PB4_TIM10_CH2_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 4, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#define CHIP_PIN_PB4_TIM10_CH2_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 4, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#endif
#endif
#endif


/*----- Пин PB5  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 5)
#define CHIP_PIN_PB5_I2C1_SMBA          CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 5, CHIP_PER_ID_I2C1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_B3_5_A15))
#define CHIP_PIN_PB5_SPI3_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_B, 5, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB5_SPI3_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if (CHIP_DEV_I2S_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_B3_5_A15))
#define CHIP_PIN_PB5_I2S3_SD_TR         CHIP_PIN_ID_AF      (CHIP_PORT_B, 5, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB5_I2S3_SD_RV         CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_B4_5_B0_1)
#define CHIP_PIN_PB5_TIM3_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(2))        /* all */
#define CHIP_PIN_PB5_TIM3_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 5, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(2))        /* all */
#endif
#if !defined CHIP_IOREMAP_SPI1 || (CHIP_IOREMAP_SPI1 == CHIP_IOREMAP_SPI1_A15_B3_5)
#define CHIP_PIN_PB5_SPI1_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_B, 5, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(1))        /* all */
#define CHIP_PIN_PB5_SPI1_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(1))        /* all */
#endif
#if (CHIP_DEV_CAN_CNT >= 2) && (!defined CHIP_IOREMAP_CAN2 || (CHIP_IOREMAP_CAN2 == CHIP_IOREMAP_CAN2_B5_6))
#define CHIP_PIN_PB5_CAN2_RX            CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_CAN2,   CHIP_PIN_REMAP1(1))        /* at40x, n45x, chv307 */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH_PPS || (CHIP_IOREMAP_ETH_PPS == CHIP_IOREMAP_ETH_PPS_PB5))
#define CHIP_PIN_PB5_ETH_PPS_OUT        CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB5_I2S1_SD_TR         CHIP_PIN_ID_AF      (CHIP_PORT_B, 5, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#define CHIP_PIN_PB5_I2S1_SD_RV         CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB5_USART2_RX          CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PB5_TIM1_BKIN          CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if (!defined CHIP_IOREMAP_UART5 || (CHIP_IOREMAP_UART5 == CHIP_IOREMAP_UART5_PB4_5))
#define CHIP_PIN_PB5_UART5_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PB5_TIM10_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 5, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#define CHIP_PIN_PB5_TIM10_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 5, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PB6  ------------------------------------------------------------ */
#define CHIP_PIN_PB6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 6)
#if !defined CHIP_IOREMAP_I2C1 || (CHIP_IOREMAP_I2C1 == CHIP_IOREMAP_I2C1_B6_7)
#define CHIP_PIN_PB6_I2C1_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 6, CHIP_PER_ID_I2C1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_B6_9)
#define CHIP_PIN_PB6_TIM4_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 6, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB6_TIM4_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_B6_7)
#define CHIP_PIN_PB6_USART1_TX          CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(1))        /* all */
#endif
#if (CHIP_DEV_CAN_CNT >= 2)  && (!defined CHIP_IOREMAP_CAN2 || (CHIP_IOREMAP_CAN2 == CHIP_IOREMAP_CAN2_B5_6))
#define CHIP_PIN_PB6_CAN2_TX            CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_CAN2,   CHIP_PIN_REMAP1(1))        /* at40x, n45x, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB6_I2S1_MCK           CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_I2S1,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB6_SPI4_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB6_SPI4_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_B, 6, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB6_I2S4_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB6_I2S4_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_B, 6, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PB6_SPIM_IO3           CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB6_DVP_D5             CHIP_PIN_ID_IN      (CHIP_PORT_B, 6, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PB6_USBHS_DM           CHIP_PIN_ID_FORCED  (CHIP_PORT_B, 6, CHIP_PER_ID_USBHS,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PB6_USBHD_DM           CHIP_PIN_ID_FORCED  (CHIP_PORT_B, 6, CHIP_PER_ID_USBHD,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PB6_TIM8_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 6, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))        /* chv307 */
#define CHIP_PIN_PB6_TIM8_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 6, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))        /* chv307 */
#endif


/*----- Пин PB7  ------------------------------------------------------------ */
#define CHIP_PIN_PB7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 7)
#if !defined CHIP_IOREMAP_I2C1 || (CHIP_IOREMAP_I2C1 == CHIP_IOREMAP_I2C1_B6_7)
#define CHIP_PIN_PB7_I2C1_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 7, CHIP_PER_ID_I2C1,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_B6_9)
#define CHIP_PIN_PB7_TIM4_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 7, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB7_TIM4_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 7, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_B6_7)
#define CHIP_PIN_PB7_USART1_RX          CHIP_PIN_ID_IN      (CHIP_PORT_B, 7, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(1))        /* all */
#endif
#if CHIP_DEV_SUPPORT_EXMC && (!defined CHIP_IOREMAP_EXMC_NADV || (CHIP_IOREMAP_EXMC_NADV == CHIP_IOREMAP_EXMC_NADV_PB7))
#define CHIP_PIN_PB7_EXMC_NADV          CHIP_PIN_ID_AF      (CHIP_PORT_B, 7, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB7_SPI4_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_B, 7, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84 */
#define CHIP_PIN_PB7_SPI4_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_B, 7, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84 */
#define CHIP_PIN_PB7_I2S4_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_B, 7, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84 */
#define CHIP_PIN_PB7_I2S4_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_B, 7, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84 */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PB7_SPIM_IO2           CHIP_PIN_ID_AF      (CHIP_PORT_B, 7, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP_DEFAULT)    /* at40x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB7_USBHS_DP           CHIP_PIN_ID_FORCED  (CHIP_PORT_B, 7, CHIP_PER_ID_USBHS,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PB7_USBHD_DP           CHIP_PIN_ID_FORCED  (CHIP_PORT_B, 7, CHIP_PER_ID_USBHD,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PB7_TIM8_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 7, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))        /* chv307 */
#define CHIP_PIN_PB7_TIM8_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 7, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))        /* chv307 */
#endif


/*----- Пин PB8  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 8)
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_B6_9)
#define CHIP_PIN_PB8_TIM4_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB8_TIM4_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PB8_SDIO1_D4           CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)    /* !v84x */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PB8_ETH_MII_TXD3       CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if !defined CHIP_IOREMAP_I2C1 || (CHIP_IOREMAP_I2C1 == CHIP_IOREMAP_I2C1_B8_9)
#define CHIP_PIN_PB8_I2C1_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 8, CHIP_PER_ID_I2C1,   CHIP_PIN_REMAP1(1))        /* all */
#endif
#if !defined CHIP_IOREMAP_CAN1 || (CHIP_IOREMAP_CAN1 == CHIP_IOREMAP_CAN1_B8_9)
#define CHIP_PIN_PB8_CAN1_RX            CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP1(2))        /* all */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_CH32V307 || defined CHIP_DEV_GD32F10X)
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PB8_TIM10_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, chv307, gdf103zf/g/i/k */
#define CHIP_PIN_PB8_TIM10_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, chv307, gdf103zf/g/i/k */
#endif
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB8_SPI4_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB8_SPI4_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB8_UART5_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB8_UART5_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB8_DVP_D6             CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_USART6 || (CHIP_IOREMAP_USART6 == CHIP_IOREMAP_USART6_PB8_9)
#define CHIP_PIN_PB8_UART6_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#define CHIP_PIN_PB8_TIM8_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))        /* chv307 */
#define CHIP_PIN_PB8_TIM8_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 8, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#endif


/*----- Пин PB9  ------------------------------------------------------------ */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PB9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 9)
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_B6_9)
#define CHIP_PIN_PB9_TIM4_CH4_IN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PB9_TIM4_CH4_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PB9_SDIO1_D5           CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)    /* !v84x */
#endif
#if !defined CHIP_IOREMAP_I2C1 || (CHIP_IOREMAP_I2C1 == CHIP_IOREMAP_I2C1_B8_9)
#define CHIP_PIN_PB9_I2C1_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 9, CHIP_PER_ID_I2C1,   CHIP_PIN_REMAP1(1))        /* all */
#endif
#if !defined CHIP_IOREMAP_CAN1 || (CHIP_IOREMAP_CAN1 == CHIP_IOREMAP_CAN1_B8_9)
#define CHIP_PIN_PB9_CAN1_TX            CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP1(2))        /* all */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_GD32F10X) && CHIP_DEV_SUPPORT_TIM11
#define CHIP_PIN_PB9_TIM11_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_TIM11,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, gdf103zf/g/i/k */
#define CHIP_PIN_PB9_TIM11_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_TIM11,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x, gdf103zf/g/i/k */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB9_SPI4_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB9_SPI4_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB9_I2S4_SD_TR         CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB9_I2S4_SD_RV         CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP2(2,3))      /* at40x, v84x */
#define CHIP_PIN_PB9_UART5_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB9_UART5_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB9_DVP_D7             CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PB9_TIM10_CH2_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PB9_TIM10_CH2_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 9, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#if !defined CHIP_IOREMAP_USART6 || (CHIP_IOREMAP_USART6 == CHIP_IOREMAP_USART6_PB8_9)
#define CHIP_PIN_PB9_UART6_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 8, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#define CHIP_PIN_PB9_TIM8_BKIN          CHIP_PIN_ID_IN      (CHIP_PORT_B, 9, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(1))        /* chv307 */
#endif
#endif


/*----- Пин PB10 ------------------------------------------------------------ */
#define CHIP_PIN_PB10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 10)
#define CHIP_PIN_PB10_I2C2_SCL          CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 10, CHIP_PER_ID_I2C2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_B10_14)
#define CHIP_PIN_PB10_USART3_TX         CHIP_PIN_ID_AF      (CHIP_PORT_B, 10, CHIP_PER_ID_USART3, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_1_B10_11) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_B10_11)
#define CHIP_PIN_PB10_TIM2_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 10, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP2(2,3))     /* all */
#define CHIP_PIN_PB10_TIM2_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 10, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP2(2,3))     /* all */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PB10_ETH_MII_RX_ER     CHIP_PIN_ID_IN      (CHIP_PORT_B, 10, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)   /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB10_I2S3_MCK          CHIP_PIN_ID_AF      (CHIP_PORT_B, 10, CHIP_PER_ID_I2S3,  CHIP_PIN_REMAP2(2,3)       /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PB10_SPIM_IO0          CHIP_PIN_ID_AF      (CHIP_PORT_B, 10, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP1(1))       /* at40x  */
#endif
#ifdef CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PB10_TIM10_BKIN        CHIP_PIN_ID_IN      (CHIP_PORT_B, 10, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))       /* chv307 */
#endif
#define CHIP_PIN_PB10_OPA2_CH0N         CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 10)                                               /* chv307 */
#endif


/*----- Пин PB11 ------------------------------------------------------------ */
#define CHIP_PIN_PB11_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 11)
#define CHIP_PIN_PB11_I2C2_SDA          CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 11, CHIP_PER_ID_I2C2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_B10_14)
#define CHIP_PIN_PB11_USART3_RX         CHIP_PIN_ID_IN      (CHIP_PORT_B, 11, CHIP_PER_ID_USART3, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM2 || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A0_1_B10_11) || (CHIP_IOREMAP_TIM2 == CHIP_IOREMAP_TIM2_A15_B3_B10_11)
#define CHIP_PIN_PB11_TIM2_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_B, 11, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP2(2,3))     /* all */
#define CHIP_PIN_PB11_TIM2_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_B, 11, CHIP_PER_ID_TIM2,   CHIP_PIN_REMAP2(2,3))     /* all */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PB11_ETH_MII_TX_EN     CHIP_PIN_ID_AF      (CHIP_PORT_B, 11, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)   /* st107, n457, at407, chv307 */
#define CHIP_PIN_PB11_ETH_RMII_TX_EN    CHIP_PIN_ID_AF      (CHIP_PORT_B, 11, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)   /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PB11_SPIM_IO1          CHIP_PIN_ID_AF      (CHIP_PORT_B, 11, CHIP_PER_ID_SPIM,   CHIP_PIN_REMAP1(1))       /* at40x */
#endif
#ifdef CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PB11_TIM10_ETR         CHIP_PIN_ID_IN      (CHIP_PORT_B, 11, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))       /* chv307 */
#endif
#define CHIP_PIN_PB11_OPA1_CH0N         CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 11)                                               /* chv307 */
#endif


/*----- Пин PB12 ------------------------------------------------------------ */
#define CHIP_PIN_PB12_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 12)
#define CHIP_PIN_PB12_SPI2_NSS_MST      CHIP_PIN_ID_AF      (CHIP_PORT_B, 12, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PB12_SPI2_NSS_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_B, 12, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if CHIP_DEV_I2S_CNT >= 1
#define CHIP_PIN_PB12_I2S2_WS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_B, 12, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PB12_I2S2_WS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_B, 12, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#define CHIP_PIN_PB12_I2C2_SMBA         CHIP_PIN_ID_AF_OD   (CHIP_PORT_B, 12, CHIP_PER_ID_I2C2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_B10_14)
#define CHIP_PIN_PB12_USART3_CK         CHIP_PIN_ID_AF      (CHIP_PORT_B, 12, CHIP_PER_ID_USART3, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15)
#define CHIP_PIN_PB12_TIM1_BKIN         CHIP_PIN_ID_IN      (CHIP_PORT_B, 12, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if (CHIP_DEV_CAN_CNT >= 2)  && (!defined CHIP_IOREMAP_CAN2 || (CHIP_IOREMAP_CAN2 == CHIP_IOREMAP_CAN2_B12_13))
#define CHIP_PIN_PB12_CAN2_RX           CHIP_PIN_ID_IN      (CHIP_PORT_B, 12, CHIP_PER_ID_CAN2,   CHIP_PIN_REMAP_DEFAULT)   /* at40x, n45x, chv307 */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PB12_ETH_MII_TXD0      CHIP_PIN_ID_AF      (CHIP_PORT_B, 12, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)   /* st107, n457, at407, chv307 */
#define CHIP_PIN_PB12_ETH_RMII_TXD0     CHIP_PIN_ID_AF      (CHIP_PORT_B, 12, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)   /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB12_EXMC_D13          CHIP_PIN_ID_AF      (CHIP_PORT_B, 12, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))     /* at40x, v84x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB12_OPA4_CH0P         CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 12)                                               /* chv307 */
#endif

/*----- Пин PB13 ------------------------------------------------------------ */
#define CHIP_PIN_PB13_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 13)
#define CHIP_PIN_PB13_SPI2_SCK_MST      CHIP_PIN_ID_AF      (CHIP_PORT_B, 13, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PB13_SPI2_SCK_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_B, 13, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if CHIP_DEV_I2S_CNT >= 1
#define CHIP_PIN_PB13_I2S2_CK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_B, 13, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PB13_I2S2_CK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_B, 13, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_B10_14) || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_C10_12_B13_14)
#define CHIP_PIN_PB13_USART3_CTS        CHIP_PIN_ID_IN      (CHIP_PORT_B, 13, CHIP_PER_ID_USART3, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15)
#define CHIP_PIN_PB13_TIM1_CH1N         CHIP_PIN_ID_AF      (CHIP_PORT_B, 13, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if (CHIP_DEV_CAN_CNT >= 2) && (!defined CHIP_IOREMAP_CAN2 || (CHIP_IOREMAP_CAN2 == CHIP_IOREMAP_CAN2_B12_13))
#define CHIP_PIN_PB13_CAN2_TX           CHIP_PIN_ID_AF      (CHIP_PORT_B, 13, CHIP_PER_ID_CAN2,   CHIP_PIN_REMAP_DEFAULT)   /* at40x, n45x, chv307 */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PB13_ETH_MII_TXD1      CHIP_PIN_ID_AF      (CHIP_PORT_B, 13, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)   /* st107, n457, at407, chv307 */
#define CHIP_PIN_PB13_ETH_RMII_TXD1     CHIP_PIN_ID_AF      (CHIP_PORT_B, 13, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)   /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB13_UART5_TX          CHIP_PIN_ID_AF      (CHIP_PORT_B, 13, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(1))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB13_OPA3_CH0P         CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 13)                                               /* chv307 */
#endif


/*----- Пин PB14 ------------------------------------------------------------ */
#define CHIP_PIN_PB14_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 14)
#define CHIP_PIN_PB14_SPI2_MISO_MST     CHIP_PIN_ID_IN      (CHIP_PORT_B, 14, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PB14_SPI2_MISO_SLV     CHIP_PIN_ID_AF      (CHIP_PORT_B, 14, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15)
#define CHIP_PIN_PB14_TIM1_CH2N         CHIP_PIN_ID_AF      (CHIP_PORT_B, 14, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_B10_14) || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_C10_12_B13_14)
#define CHIP_PIN_PB14_USART3_RTS        CHIP_PIN_ID_AF      (CHIP_PORT_B, 14, CHIP_PER_ID_USART3, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PB14_I2S2_SDEXT_TR     CHIP_PIN_ID_AF      (CHIP_PORT_B, 14, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* at40x */
#define CHIP_PIN_PB14_I2S2_SDEXT_RV     CHIP_PIN_ID_IN      (CHIP_PORT_B, 14, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* at40x */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_GD32F10X) && CHIP_DEV_SUPPORT_TIM12
#define CHIP_PIN_PB14_TIM12_CH1_IN      CHIP_PIN_ID_IN      (CHIP_PORT_B, 14, CHIP_PER_ID_TIM12,  CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84x, gdf103zf/g/i/k */
#define CHIP_PIN_PB14_TIM12_CH1_OUT     CHIP_PIN_ID_AF      (CHIP_PORT_B, 14, CHIP_PER_ID_TIM12,  CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84x, gdf103zf/g/i/k */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PB14_EXMC_D0           CHIP_PIN_ID_AF      (CHIP_PORT_B, 14, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))     /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PB14_UART5_RX          CHIP_PIN_ID_IN      (CHIP_PORT_B, 14, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(1))       /*  n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PB14_OPA2_CH0P         CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 14)                                               /* chv307 */
#endif



/*----- Пин PB15 ------------------------------------------------------------ */
#define CHIP_PIN_PB15_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_B, 15)
#define CHIP_PIN_PB15_SPI2_MOSI_MST     CHIP_PIN_ID_AF      (CHIP_PORT_B, 15, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PB15_SPI2_MOSI_SLV     CHIP_PIN_ID_IN      (CHIP_PORT_B, 15, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#if CHIP_DEV_I2S_CNT >= 1
#define CHIP_PIN_PB15_I2S2_SD_TR        CHIP_PIN_ID_AF      (CHIP_PORT_B, 15, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#define CHIP_PIN_PB15_I2S2_SD_RV        CHIP_PIN_ID_IN      (CHIP_PORT_B, 15, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_A12_A8_11_B12_15)
#define CHIP_PIN_PB15_TIM1_CH3N         CHIP_PIN_ID_AF      (CHIP_PORT_B, 15, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_GD32F10X) && CHIP_DEV_SUPPORT_TIM12
#define CHIP_PIN_PB15_TIM12_CH2_IN      CHIP_PIN_ID_IN      (CHIP_PORT_B, 15, CHIP_PER_ID_TIM12,  CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84x, gdf103zf/g/i/k */
#define CHIP_PIN_PB15_TIM12_CH2_OUT     CHIP_PIN_ID_AF      (CHIP_PORT_B, 15, CHIP_PER_ID_TIM12,  CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84x, gdf103zf/g/i/k */
#endif
#if defined CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_A8_10_B15)
#define CHIP_PIN_PB15_USART1_TX         CHIP_PIN_ID_AF      (CHIP_PORT_B, 15, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#define CHIP_PIN_PB15_OPA1_CH0P         CHIP_PIN_ID_ANALOG  (CHIP_PORT_B, 15)                                               /* chv307 */
#endif


/************************************ Порт C **********************************/
/*----- Пин PC0 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 0)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC0_ADC_IN7            CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 0)                                                /* n45x */
#else
#define CHIP_PIN_PC0_ADC_IN10           CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 0)                                                /* !n45x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PC0_SDIO2_D0           CHIP_PIN_ID_AF      (CHIP_PORT_C, 0, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)   /*  at40x */
#endif
#if defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC0_WKUP3              CHIP_PIN_ID_FORCED  (CHIP_PORT_C, 0, CHIP_PER_ID_WKUP)                              /* v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC0_I2C3_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_C, 0, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP_DEFAULT)    /*  n45x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC0_UART6_TX           CHIP_PIN_ID_AF      (CHIP_PORT_C, 0, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(2))        /*  n45x */
#endif
#ifdef CHIP_DEV_CH32XXXX
#if !defined CHIP_IOREMAP_USART6 || (CHIP_IOREMAP_USART6 == CHIP_IOREMAP_USART6_PC0_1)
#define CHIP_PIN_PC0_UART6_TX           CHIP_PIN_ID_AF      (CHIP_PORT_C, 0, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */ /** @todo - объединить с n45x */
#endif
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5))
#define CHIP_PIN_PC0_TIM9_CH1N          CHIP_PIN_ID_AF      (CHIP_PORT_C, 0, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */
#endif
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PC0_ETH_RGMII_RXC      CHIP_PIN_ID_IN      (CHIP_PORT_C, 0, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */ /** @todo check cfg */
#endif
#endif
#endif


/*----- Пин PC1 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 1)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC1_ADC_IN8            CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 1)                                                /* n45x */
#else
#define CHIP_PIN_PC1_ADC_IN11           CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 1)                                                /* !n45x */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PC1_ETH_MII_MDC        CHIP_PIN_ID_AF      (CHIP_PORT_C, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#define CHIP_PIN_PC1_ETH_RMII_MDC       CHIP_PIN_ID_AF      (CHIP_PORT_C, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PC1_SDIO2_D1           CHIP_PIN_ID_AF      (CHIP_PORT_C, 1, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)   /*  at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC1_I2C3_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_C, 1, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP_DEFAULT)    /*  n45x */
#define CHIP_PIN_PC1_UART6_RX           CHIP_PIN_ID_IN      (CHIP_PORT_C, 1, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(2))        /*  n45x */
#endif
#ifdef CHIP_DEV_CH32XXXX
#if !defined CHIP_IOREMAP_USART6 || (CHIP_IOREMAP_USART6 == CHIP_IOREMAP_USART6_PC0_1)
#define CHIP_PIN_PC1_UART6_RX           CHIP_PIN_ID_IN      (CHIP_PORT_C, 1, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */ /** @todo - объединить с n45x */
#endif
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5))
#define CHIP_PIN_PC1_TIM9_CH2N          CHIP_PIN_ID_AF      (CHIP_PORT_C, 1, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PC1_ETH_RGMII_RXCTL    CHIP_PIN_ID_IN      (CHIP_PORT_C, 1, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */ /** @todo check cfg */
#endif
#endif
#endif


/*----- Пин PC2 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 2)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC2_ADC_IN9            CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 2)                                                /* n45x */
#else
#define CHIP_PIN_PC2_ADC_IN12           CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 2)                                                /* !n45x */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PC2_ETH_MII_TXD2       CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PC2_SDIO2_D2           CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)   /*  at40x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC2_UART8_TX           CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_UART8,   CHIP_PIN_REMAP1(1))       /*  at40x, v84x */
#define CHIP_PIN_PC2_EXMC_NWE           CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP1(2))       /*  at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC2_UART7_TX           CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(2))        /*  n45x */
#define CHIP_PIN_PC2_SPI3_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(3))        /*  n45x */
#define CHIP_PIN_PC2_SPI3_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_C, 2, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(3))        /*  n45x */
#define CHIP_PIN_PC2_I2S3_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(3))        /*  n45x */
#define CHIP_PIN_PC2_I2S3_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_C, 2, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(3))        /*  n45x */
#endif
#ifdef CHIP_DEV_CH32XXXX
#if !defined CHIP_IOREMAP_USART7 || (CHIP_IOREMAP_USART7 == CHIP_IOREMAP_USART7_PC2_3)
#define CHIP_PIN_PC2_UART7_TX           CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */ /** @todo - объединить с n45x */
#endif
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5))
#define CHIP_PIN_PC2_TIM9_CH3N          CHIP_PIN_ID_AF      (CHIP_PORT_C, 2, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */
#endif
#define CHIP_PIN_PC2_OPA3_CH1N          CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 2)                                                /* chv307, chf20x */
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PC2_ETH_RGMII_RXD0     CHIP_PIN_ID_IN      (CHIP_PORT_C, 2, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif
#endif


/*----- Пин PC3 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 3)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC3_ADCX_IN10          CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 3)                                                /* n45x */
#else
#define CHIP_PIN_PC3_ADCX_IN13          CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 3)                                                /* !n45x */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PIN_PC3_ETH_TX_CLK         CHIP_PIN_ID_AF      (CHIP_PORT_C, 3, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PC3_SDIO2_D3           CHIP_PIN_ID_AF      (CHIP_PORT_C, 3, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)   /* at40x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC3_UART8_RX           CHIP_PIN_ID_IN      (CHIP_PORT_C, 3, CHIP_PER_ID_UART8,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PC3_EXMC_A0            CHIP_PIN_ID_AF      (CHIP_PORT_C, 3, CHIP_PER_ID_EXMC,    CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC3_UART7_RX           CHIP_PIN_ID_IN      (CHIP_PORT_C, 3, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PC3_SPI3_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_C, 3, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PC3_SPI3_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_C, 3, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PC3_I2S3_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_C, 3, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PC3_I2S3_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_C, 3, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32XXXX
#if !defined CHIP_IOREMAP_USART7 || (CHIP_IOREMAP_USART7 == CHIP_IOREMAP_USART7_PC2_3)
#define CHIP_PIN_PC3_UART7_RX           CHIP_PIN_ID_IN      (CHIP_PORT_C, 3, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */ /** @todo - объединить с n45x */
#endif
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PC3_TIM10_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_C, 3, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */
#define CHIP_PIN_PC3_TIM10_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_C, 3, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)    /* chv307, chf20x */
#endif
#define CHIP_PIN_PC3_OPA4_CH1N          CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 3)                                                /* chv307, chf20x */
#if CHIP_DEV_SUPPORT_ETH_RGMII
#define CHIP_PIN_PC3_ETH_RGMII_RXD1     CHIP_PIN_ID_IN      (CHIP_PORT_C, 3, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif
#endif


/*----- Пин PC4 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 4)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC4_ADC2_IN4           CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 4)                                                /* n45x */
#elif CHIP_DEV_ADC_CNT >= 2
#define CHIP_PIN_PC4_ADC12_IN14         CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 4)                                                /* st10x, at40x, gd10x */
#else
#define CHIP_PIN_PC4_ADC_IN14           CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 4)                                                /* v84x, chv307 */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_A7_C4_5_B0_1))
#define CHIP_PIN_PC4_ETH_MII_RXD0       CHIP_PIN_ID_IN      (CHIP_PORT_C, 4, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#define CHIP_PIN_PC4_ETH_RMII_RXD0      CHIP_PIN_ID_IN      (CHIP_PORT_C, 4, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC4_EXMC_NE4           CHIP_PIN_ID_AF      (CHIP_PORT_C, 4, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /*  at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PC4_SDIO2_CK           CHIP_PIN_ID_AF      (CHIP_PORT_C, 4, CHIP_PER_ID_SDIO2,  CHIP_PIN_REMAP_DEFAULT)   /*  at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC4_QSPI_IO2           CHIP_PIN_ID_AF      (CHIP_PORT_C, 4, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP_DEFAULT)    /*  n45x */
#define CHIP_PIN_PC4_UART7_TX           CHIP_PIN_ID_AF      (CHIP_PORT_C, 4, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP_DEFAULT)    /*  n45x */
#define CHIP_PIN_PC4_I2C3_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_C, 4, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP1(3))        /*  n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART8 || (CHIP_IOREMAP_USART8 == CHIP_IOREMAP_USART8_PC4_5)
#define CHIP_PIN_PC4_UART8_TX           CHIP_PIN_ID_AF      (CHIP_PORT_C, 4, CHIP_PER_ID_UART8,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5))
#define CHIP_PIN_PC4_TIM9_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 4, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#define CHIP_PIN_PC4_TIM9_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 4, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#define CHIP_PIN_PC4_ETH_RGMII_TXD1     CHIP_PIN_ID_AF      (CHIP_PORT_C, 4, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_7_C4_5)
#define CHIP_PIN_PC4_USART1_CTS         CHIP_PIN_ID_IN      (CHIP_PORT_C, 4, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(3))        /* chv307 */
#endif
#define CHIP_PIN_PC4_OPA4_CH1P          CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 4)                                                /* chv307 */
#endif
#endif


/*----- Пин PC5 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 5)
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC5_ADC2_IN11          CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 5)                                                /* n45x */
#elif CHIP_DEV_ADC_CNT >= 2
#define CHIP_PIN_PC5_ADC12_IN15         CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 5)                                                /* st10x, at40x, gd10x */
#else
#define CHIP_PIN_PC5_ADC_IN15           CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 5)                                                /* v84x, chv307 */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_A7_C4_5_B0_1))
#define CHIP_PIN_PC5_ETH_MII_RXD1       CHIP_PIN_ID_IN      (CHIP_PORT_C, 5, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#define CHIP_PIN_PC5_ETH_RMII_RXD1      CHIP_PIN_ID_IN      (CHIP_PORT_C, 5, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC5_EXMC_NOE           CHIP_PIN_ID_AF      (CHIP_PORT_C, 5, CHIP_PER_ID_EXMC,    CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PC5_SDIO2_CMD          CHIP_PIN_ID_AF      (CHIP_PORT_C, 5, CHIP_PER_ID_SDIO2,   CHIP_PIN_REMAP_DEFAULT)   /* at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC5_QSPI_IO3           CHIP_PIN_ID_AF      (CHIP_PORT_C, 5, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#define CHIP_PIN_PC5_UART7_RX           CHIP_PIN_ID_IN      (CHIP_PORT_C, 5, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#define CHIP_PIN_PC5_I2C3_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_C, 5, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART8 || (CHIP_IOREMAP_USART8 == CHIP_IOREMAP_USART8_PC4_5)
#define CHIP_PIN_PC5_UART8_RX           CHIP_PIN_ID_IN      (CHIP_PORT_C, 5, CHIP_PER_ID_UART8,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA2_4_C0_2_C4_5))
#define CHIP_PIN_PC5_TIM9_BKIN          CHIP_PIN_ID_IN      (CHIP_PORT_C, 5, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#define CHIP_PIN_PC5_ETH_RGMII_TXD2     CHIP_PIN_ID_AF      (CHIP_PORT_C, 5, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#if !defined CHIP_IOREMAP_USART1 || (CHIP_IOREMAP_USART1 == CHIP_IOREMAP_USART1_A5_7_C4_5)
#define CHIP_PIN_PC5_USART1_RTS         CHIP_PIN_ID_AF      (CHIP_PORT_C, 5, CHIP_PER_ID_USART1, CHIP_PIN_REMAP1(3))        /* chv307 */
#endif
#define CHIP_PIN_PC5_OPA3_CH1P          CHIP_PIN_ID_ANALOG  (CHIP_PORT_C, 5)                                                /* chv307 */
#endif
#endif


/*----- Пин PC6 ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64) || (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) == 20)
#define CHIP_PIN_PC6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 6)
#if CHIP_DEV_I2S_CNT >= 1
#define CHIP_PIN_PC6_I2S2_MCK           CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PC6_TIM8_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 6, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PC6_TIM8_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PC6_SDIO1_D6           CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)    /* !v84x */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_C6_9)
#define CHIP_PIN_PC6_TIM3_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 6, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#define CHIP_PIN_PC6_TIM3_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC6_USART6_TX          CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_USART6, CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PC6_EXMC_D1            CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))      /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC6_I2C4_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_C, 6, CHIP_PER_ID_I2C4,   CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#define CHIP_PIN_PC6_SPI2_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC6_SPI2_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_C, 6, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC6_I2S2_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_C, 6, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC6_I2S2_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_C, 6, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC6_USART2_CTS         CHIP_PIN_ID_IN      (CHIP_PORT_C, 6, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PC6_ETH_RXP            CHIP_PIN_ID_IN      (CHIP_PORT_C, 6, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */ /* @todo check cfg */
#endif
#endif


/*----- Пин PC7 ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64) || (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) == 20)
#define CHIP_PIN_PC7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 7)
#if CHIP_DEV_I2S_CNT >= 2
#define CHIP_PIN_PC7_I2S3_MCK           CHIP_PIN_ID_AF      (CHIP_PORT_C, 7, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#define CHIP_PIN_PC7_TIM8_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 7, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PC7_TIM8_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 7, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PC7_SDIO1_D7           CHIP_PIN_ID_AF      (CHIP_PORT_C, 7, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)    /* !v84x */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_C6_9)
#define CHIP_PIN_PC7_TIM3_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 7, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#define CHIP_PIN_PC7_TIM3_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 7, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC7_USART6_RX          CHIP_PIN_ID_IN      (CHIP_PORT_C, 7, CHIP_PER_ID_USART6, CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC7_I2C4_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_C, 7, CHIP_PER_ID_I2C4,   CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#define CHIP_PIN_PC7_SPI2_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_C, 7, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC7_SPI2_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_C, 7, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC7_I2S2_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_C, 7, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC7_I2S2_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_C, 7, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC7_USART2_RTS         CHIP_PIN_ID_AF      (CHIP_PORT_C, 7, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PC7_ETH_RXN            CHIP_PIN_ID_IN      (CHIP_PORT_C, 7, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */ /* @todo check cfg */
#endif
#endif


/*----- Пин PC8 ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64) || (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) == 20)
#define CHIP_PIN_PC8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 8)
#define CHIP_PIN_PC8_TIM8_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 8, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PC8_TIM8_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 8, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PC8_SDIO1_D0           CHIP_PIN_ID_AF      (CHIP_PORT_C, 8, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)    /* !v84x */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_C6_9)
#define CHIP_PIN_PC8_TIM3_CH3_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 8, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#define CHIP_PIN_PC8_TIM3_CH3_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 8, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC8_USART6_CK          CHIP_PIN_ID_IN      (CHIP_PORT_C, 8, CHIP_PER_ID_USART6, CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#define CHIP_PIN_PC8_I2S4_MCK           CHIP_PIN_ID_AF      (CHIP_PORT_C, 8, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC8_SPI2_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_C, 8, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC8_SPI2_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_C, 8, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC8_USART2_TX          CHIP_PIN_ID_AF      (CHIP_PORT_C, 8, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PC8_ETH_TXP            CHIP_PIN_ID_AF      (CHIP_PORT_C, 8, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */ /* @todo check cfg */
#define CHIP_PIN_PC8_DVP_D2             CHIP_PIN_ID_IN      (CHIP_PORT_C, 8, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PC9 ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64) || (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) == 20)
#define CHIP_PIN_PC9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 9)
#define CHIP_PIN_PC9_TIM8_CH4_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 9, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#define CHIP_PIN_PC9_TIM8_CH4_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 9, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PC9_SDIO1_D1           CHIP_PIN_ID_AF      (CHIP_PORT_C, 9, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)    /* !v84x */
#endif
#if !defined CHIP_IOREMAP_TIM3 || (CHIP_IOREMAP_TIM3 == CHIP_IOREMAP_TIM3_C6_9)
#define CHIP_PIN_PC9_TIM3_CH4_IN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 9, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#define CHIP_PIN_PC9_TIM3_CH4_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_C, 9, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC9_I2C3_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_C, 9, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC9_SPI2_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_C, 9, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC9_SPI2_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_C, 9, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC9_I2S2_SD_TR         CHIP_PIN_ID_AF      (CHIP_PORT_C, 9, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC9_I2S2_SD_RV         CHIP_PIN_ID_IN      (CHIP_PORT_C, 9, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PC9_USART2_RX          CHIP_PIN_ID_IN      (CHIP_PORT_C, 9, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PC9_ETH_TXN            CHIP_PIN_ID_AF      (CHIP_PORT_C, 9, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP_DEFAULT)    /* chv307 */ /* @todo check cfg */
#define CHIP_PIN_PC9_DVP_D3             CHIP_PIN_ID_IN      (CHIP_PORT_C, 9, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PC10 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 10)
#if !defined CHIP_IOREMAP_UART4 || (CHIP_IOREMAP_UART4 == CHIP_IOREMAP_UART4_PC10_11)
#define CHIP_PIN_PC10_UART4_TX          CHIP_PIN_ID_AF      (CHIP_PORT_C, 10, CHIP_PER_ID_UART4, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PC10_SDIO1_D2          CHIP_PIN_ID_AF      (CHIP_PORT_C, 10, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)   /* !v84x */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_C10_12_B13_14)
#define CHIP_PIN_PC10_USART3_TX         CHIP_PIN_ID_AF      (CHIP_PORT_C, 10, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(1))       /* all */
#endif
#if !defined CHIP_DEV_STM32F103
#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_C10_12_A4))
#define CHIP_PIN_PC10_SPI3_SCK_MST      CHIP_PIN_ID_AF      (CHIP_PORT_C, 10, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(1))       /* !st103 */
#define CHIP_PIN_PC10_SPI3_SCK_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_C, 10, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(1))       /* !st103 */
#endif
#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_I2S_CNT >= 2) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_C10_12_A4))
#define CHIP_PIN_PC10_I2S3_CK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_C, 10, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(1))       /* !st103 */
#define CHIP_PIN_PC10_I2S3_CK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_C, 10, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(1))       /* !st103 */
#endif
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC10_QSPI_NSS          CHIP_PIN_ID_AF      (CHIP_PORT_C, 10, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(3))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PC10_TIM10_ETR         CHIP_PIN_ID_IN      (CHIP_PORT_C, 10, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)   /* chv307 */
#endif
#define CHIP_PIN_PC10_DVP_D8            CHIP_PIN_ID_IN      (CHIP_PORT_C, 10, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PC11 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC11_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 11)
#if !defined CHIP_IOREMAP_UART4 || (CHIP_IOREMAP_UART4 == CHIP_IOREMAP_UART4_PC10_11)
#define CHIP_PIN_PC11_UART4_RX          CHIP_PIN_ID_IN      (CHIP_PORT_C, 11, CHIP_PER_ID_UART4, CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PC11_SDIO1_D3          CHIP_PIN_ID_AF      (CHIP_PORT_C, 11, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)   /* !v84x */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_C10_12_B13_14)
#define CHIP_PIN_PC11_USART3_RX         CHIP_PIN_ID_IN      (CHIP_PORT_C, 11, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(1))       /* all */
#endif
#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_C10_12_A4))
#define CHIP_PIN_PC11_SPI3_MISO_MST     CHIP_PIN_ID_IN      (CHIP_PORT_C, 11, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(1))       /* !st103 */
#define CHIP_PIN_PC11_SPI3_MISO_SLV     CHIP_PIN_ID_AF      (CHIP_PORT_C, 11, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(1))       /* !st103 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC11_EXMC_D2           CHIP_PIN_ID_AF      (CHIP_PORT_C, 11, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))     /* at40x, v84x */
#endif
#if defined CHIP_DEV_AT32F40X
#define CHIP_PIN_PC11_I2S3_SDEXT_TR     CHIP_PIN_ID_AF      (CHIP_PORT_C, 11, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(1))       /* at40x */
#define CHIP_PIN_PC11_I2S3_SDEXT_RV     CHIP_PIN_ID_IN      (CHIP_PORT_C, 11, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(1))       /* at40x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC11_QSPI_SCK          CHIP_PIN_ID_AF      (CHIP_PORT_C, 11, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(3))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PC11_TIM10_CH4_IN      CHIP_PIN_ID_IN      (CHIP_PORT_C, 11, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)   /* chv307 */
#define CHIP_PIN_PC11_TIM10_CH4_OUT     CHIP_PIN_ID_AF      (CHIP_PORT_C, 11, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)   /* chv307 */
#endif
#define CHIP_PIN_PC11_DVP_D4            CHIP_PIN_ID_IN      (CHIP_PORT_C, 11, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PC12 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC12_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 12)
#if !defined CHIP_IOREMAP_UART5 || (CHIP_IOREMAP_UART5 == CHIP_IOREMAP_UART5_PC12_D2)
#define CHIP_PIN_PC12_UART5_TX          CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_UART5, CHIP_PIN_REMAP_DEFAULT)   /* all */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PC12_SDIO1_CK          CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)   /* !v84x */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_C10_12_B13_14)
#define CHIP_PIN_PC12_USART3_CK         CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(1))       /* all */
#endif
#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_SPI_CNT >= 3) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_C10_12_A4))
#define CHIP_PIN_PC12_SPI3_MOSI_MST     CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(1))        /* !st103 */
#define CHIP_PIN_PC12_SPI3_MOSI_SLV     CHIP_PIN_ID_IN      (CHIP_PORT_C, 12, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(1))        /* !st103 */
#endif
#if !defined CHIP_DEV_STM32F103 && (CHIP_DEV_I2S_CNT >= 2) && (!defined CHIP_IOREMAP_SPI3 || (CHIP_IOREMAP_SPI3 == CHIP_IOREMAP_SPI3_C10_12_A4))
#define CHIP_PIN_PC12_I2S3_SD_TR        CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_I2S3,  CHIP_PIN_REMAP1(1))        /* !st103 */
#define CHIP_PIN_PC12_I2S3_SD_RV        CHIP_PIN_ID_IN      (CHIP_PORT_C, 12, CHIP_PER_ID_I2S3,  CHIP_PIN_REMAP1(1))        /* !st103 */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PC12_EXMC_D3           CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP2(1,2))     /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PC12_QSPI_IO0          CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PC12_TIM8_CH2N         CHIP_PIN_ID_AF      (CHIP_PORT_C, 12, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(3))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA12_14_B8_9_C3_C10_12))
#define CHIP_PIN_PC12_TIM10_BKIN        CHIP_PIN_ID_IN      (CHIP_PORT_C, 12, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP_DEFAULT)   /* chv307 */
#endif
#define CHIP_PIN_PC12_DVP_D9            CHIP_PIN_ID_IN      (CHIP_PORT_C, 12, CHIP_PER_ID_DVP,   CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PC13 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC13_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 13)
#define CHIP_PIN_PC13_TAMPER_RTC        CHIP_PIN_ID_FORCED  (CHIP_PORT_C, 13, CHIP_PER_ID_RTC,  CHIP_PIN_REMAP_DEFAULT)     /* all */
#if defined CHIP_DEV_CH32XXXX
#define CHIP_PIN_PC13_TIM8_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_C, 13, CHIP_PER_ID_TIM8, CHIP_PIN_REMAP1(1))         /* chv307, chf20x */
#define CHIP_PIN_PC13_TIM8_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_C, 13, CHIP_PER_ID_TIM8, CHIP_PIN_REMAP1(1))         /* chv307, chf20x */
#endif
#endif


/*----- Пин PC14, OSC32_IN ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC14_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 14)
#define CHIP_PIN_PC14_OSC32_IN          CHIP_PIN_ID_FORCED  (CHIP_PORT_C, 14, CHIP_PER_ID_RTC,  CHIP_PIN_REMAP_DEFAULT)     /* all */
#if defined CHIP_DEV_CH32XXXX
#if CHIP_DEV_SUPPORT_TIM9 && (!defined CHIP_IOREMAP_TIM9 || (CHIP_IOREMAP_TIM9 == CHIP_IOREMAP_TIM9_PA1_4_B0_2_C14))
#define CHIP_PIN_PC14_TIM9_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_C, 14, CHIP_PER_ID_TIM9, CHIP_PIN_REMAP1(1))         /* chv307, chf20x */
#define CHIP_PIN_PC14_TIM9_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_C, 14, CHIP_PER_ID_TIM9, CHIP_PIN_REMAP1(1))         /* chv307, chf20x */
#endif
#endif
#endif


/*----- Пин PC15, OSC32_OUT ----------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC15_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_C, 15)
#define CHIP_PIN_PC15_OSC32_OUT         CHIP_PIN_ID_FORCED  (CHIP_PORT_C, 15, CHIP_PER_ID_RTC,  CHIP_PIN_REMAP_DEFAULT)     /* all */
#if defined CHIP_DEV_CH32XXXX
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PC15_TIM10_CH4_IN      CHIP_PIN_ID_IN      (CHIP_PORT_C, 15, CHIP_PER_ID_TIM10, CHIP_PIN_REMAP1(1))        /* chv307, chf20x */
#define CHIP_PIN_PC15_TIM10_CH4_OUT     CHIP_PIN_ID_AF      (CHIP_PORT_C, 15, CHIP_PER_ID_TIM10, CHIP_PIN_REMAP1(1))        /* chv307, chf20x */
#endif
#endif
#endif


/*********************************** Порт D ***********************************/
/*----- Пин PD0, OSC_IN  ---------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80) || (defined CHIP_DEV_STM32F10X && (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64))
#define CHIP_PIN_PD0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 0)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD0_EXMC_D2            CHIP_PIN_ID_AF      (CHIP_PORT_D, 0, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_CAN1 || (CHIP_IOREMAP_CAN1 == CHIP_IOREMAP_CAN1_D0_1)
#define CHIP_PIN_PD0_CAN1_RX            CHIP_PIN_ID_IN      (CHIP_PORT_D, 0, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD0_UART4_TX           CHIP_PIN_ID_AF      (CHIP_PORT_D, 0, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PD0_QSPI_IO1           CHIP_PIN_ID_AF      (CHIP_PORT_D, 0, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PD0_TIM10_ETR          CHIP_PIN_ID_IN      (CHIP_PORT_D, 0, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PD1, OSC_OUT ---------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80) || (defined CHIP_DEV_STM32F10X && (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64))
#define CHIP_PIN_PD1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 1)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD1_EXMC_D3            CHIP_PIN_ID_AF      (CHIP_PORT_D, 1, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_CAN1 || (CHIP_IOREMAP_CAN1 == CHIP_IOREMAP_CAN1_D0_1)
#define CHIP_PIN_PD1_CAN1_TX            CHIP_PIN_ID_AF      (CHIP_PORT_D, 1, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD1_UART4_RX           CHIP_PIN_ID_IN      (CHIP_PORT_D, 1, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PD1_QSPI_IO2           CHIP_PIN_ID_AF      (CHIP_PORT_D, 1, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PD1_TIM10_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 1, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#define CHIP_PIN_PD1_TIM10_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 1, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PD2  ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PD2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 2)
#define CHIP_PIN_PD2_TIM3_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_D, 2, CHIP_PER_ID_TIM3,   CHIP_PIN_REMAP_DEFAULT)    /* all */
#if !defined CHIP_IOREMAP_UART5 || (CHIP_IOREMAP_UART5 == CHIP_IOREMAP_UART5_PC12_D2)
#define CHIP_PIN_PD2_UART5_RX           CHIP_PIN_ID_IN      (CHIP_PORT_D, 2, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP_DEFAULT)    /* all */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PIN_PD2_SDIO1_CMD          CHIP_PIN_ID_AF      (CHIP_PORT_D, 2, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP_DEFAULT)    /* !v84x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PD2_EXMC_NWE           CHIP_PIN_ID_AF      (CHIP_PORT_D, 2, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP1(1))        /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD2_SPI3_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_D, 2, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PD2_SPI3_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_D, 2, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PD2_I2S3_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_D, 2, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PD2_I2S3_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_D, 2, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PD2_QSPI_IO3           CHIP_PIN_ID_AF      (CHIP_PORT_D, 2, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PD2_TIM8_CH3N          CHIP_PIN_ID_AF      (CHIP_PORT_D, 2, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP2(1,3))      /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PD2_DVP_D11            CHIP_PIN_ID_IN      (CHIP_PORT_D, 2, CHIP_PER_ID_DVPI,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PD3  ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100)
#define CHIP_PIN_PD3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 3)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD3_EXMC_CLK           CHIP_PIN_ID_AF      (CHIP_PORT_D, 3, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_D5_7_D3_4)
#define CHIP_PIN_PD3_USART2_CTS         CHIP_PIN_ID_IN      (CHIP_PORT_D, 3, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(1))        /* all */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PD3_TIM10_CH2_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 3, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#define CHIP_PIN_PD3_TIM10_CH2_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 3, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PD4  ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100)
#define CHIP_PIN_PD4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 4)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD4_EXMC_NOE           CHIP_PIN_ID_AF      (CHIP_PORT_D, 4, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_D5_7_D3_4)
#define CHIP_PIN_PD4_USART2_RTS         CHIP_PIN_ID_AF      (CHIP_PORT_D, 4, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(1))        /* all */
#endif
#endif


/*----- Пин PD5  ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100)
#define CHIP_PIN_PD5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 5)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD5_EXMC_NWE           CHIP_PIN_ID_AF      (CHIP_PORT_D, 5, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_D5_7_D3_4)
#define CHIP_PIN_PD5_USART2_TX          CHIP_PIN_ID_AF      (CHIP_PORT_D, 5, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(1))        /* all */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PD5_TIM10_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 5, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#define CHIP_PIN_PD5_TIM10_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 5, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PD6  ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100)
#define CHIP_PIN_PD6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 6)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD6_EXMC_NWAIT         CHIP_PIN_ID_IN      (CHIP_PORT_D, 6, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_D5_7_D3_4)
#define CHIP_PIN_PD6_USART2_RX          CHIP_PIN_ID_IN      (CHIP_PORT_D, 6, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(1))        /* all */
#endif
#if defined CHIP_DEV_CH32V307
#define CHIP_PIN_PD6_DVP_D10            CHIP_PIN_ID_IN      (CHIP_PORT_D, 6, CHIP_PER_ID_DVPI,  CHIP_PIN_REMAP_DEFAULT)    /* chv307 */
#endif
#endif


/*----- Пин PD7  ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100)
#define CHIP_PIN_PD7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 7)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD7_EXMC_NE1           CHIP_PIN_ID_AF      (CHIP_PORT_D, 7, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#define CHIP_PIN_PD7_EXMC_NCE2          CHIP_PIN_ID_AF      (CHIP_PORT_D, 7, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART2 || (CHIP_IOREMAP_USART2 == CHIP_IOREMAP_USART2_D5_7_D3_4)
#define CHIP_PIN_PD7_USART2_CK          CHIP_PIN_ID_AF      (CHIP_PORT_D, 7, CHIP_PER_ID_USART2, CHIP_PIN_REMAP1(1))        /* all */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PD7_TIM10_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 7, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#define CHIP_PIN_PD7_TIM10_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 7, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PD8  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 68
#define CHIP_PIN_PD8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 8)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD8_EXMC_D13           CHIP_PIN_ID_AF      (CHIP_PORT_D, 8, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_D8_12)
#define CHIP_PIN_PD8_USART3_TX          CHIP_PIN_ID_AF      (CHIP_PORT_D, 8, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(3))        /* all */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_D8_12))
#define CHIP_PIN_PD8_ETH_MII_RX_DV      CHIP_PIN_ID_IN      (CHIP_PORT_D, 8, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))        /* st107, n457, at407, chv307 */
#define CHIP_PIN_PD8_ETH_RMII_CRS_DV    CHIP_PIN_ID_IN      (CHIP_PORT_D, 8, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))        /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD8_SPI3_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_D, 8, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD8_SPI3_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_D, 8, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD8_I2S3_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_D, 8, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD8_I2S3_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_D, 8, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD8_CAN1_RX            CHIP_PIN_ID_IN      (CHIP_PORT_D, 8, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP1(1))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD8_TIM9_CH1N          CHIP_PIN_ID_AF      (CHIP_PORT_D, 8, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PD9  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 68
#define CHIP_PIN_PD9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 9)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD9_EXMC_D14           CHIP_PIN_ID_AF      (CHIP_PORT_D, 9, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_D8_12)
#define CHIP_PIN_PD9_USART3_RX          CHIP_PIN_ID_IN      (CHIP_PORT_D, 9, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(3))        /* all */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_D8_12))
#define CHIP_PIN_PD9_ETH_MII_RXD0       CHIP_PIN_ID_IN      (CHIP_PORT_D, 9, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))        /* st107, n457, at407, chv307 */
#define CHIP_PIN_PD9_ETH_RMII_RXD0      CHIP_PIN_ID_IN      (CHIP_PORT_D, 9, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))        /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD9_SPI3_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_D, 9, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD9_SPI3_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_D, 9, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD9_I2S3_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_D, 9, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD9_I2S3_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_D, 9, CHIP_PER_ID_I2S3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD9_CAN1_TX            CHIP_PIN_ID_AF      (CHIP_PORT_D, 9, CHIP_PER_ID_CAN1,   CHIP_PIN_REMAP1(1))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD9_TIM9_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_D, 9, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))        /* chv307 */
#define CHIP_PIN_PD9_TIM9_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_D, 9, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))        /* chv307 */
#define CHIP_PIN_PD9_TIM9_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_D, 9, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PD10  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PD10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 10)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD10_EXMC_D15          CHIP_PIN_ID_AF      (CHIP_PORT_D, 10, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_D8_12) || (CHIP_IOREMAP_USART3 ==CHIP_IOREMAP_USART3_A13_14_D10_12)
#define CHIP_PIN_PD10_USART3_CK         CHIP_PIN_ID_AF      (CHIP_PORT_D, 10, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(3))       /* all */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_D8_12))
#define CHIP_PIN_PD10_ETH_MII_RXD1      CHIP_PIN_ID_IN      (CHIP_PORT_D, 10, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))       /* st107, n457, at407, chv307 */
#define CHIP_PIN_PD10_ETH_RMII_RXD1     CHIP_PIN_ID_IN      (CHIP_PORT_D, 10, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))       /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD10_CAN2_RX           CHIP_PIN_ID_IN      (CHIP_PORT_D, 10, CHIP_PER_ID_CAN2,   CHIP_PIN_REMAP1(3))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD10_TIM9_CH2N         CHIP_PIN_ID_AF      (CHIP_PORT_D, 10, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PD11 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD11_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 11)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD11_EXMC_A16          CHIP_PIN_ID_AF      (CHIP_PORT_D, 11, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#define CHIP_PIN_PD11_EXMC_CLE          CHIP_PIN_ID_AF      (CHIP_PORT_D, 11, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* gd103? */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_D8_12) || (CHIP_IOREMAP_USART3 ==CHIP_IOREMAP_USART3_A13_14_D10_12)
#define CHIP_PIN_PD11_USART3_CTS        CHIP_PIN_ID_IN      (CHIP_PORT_D, 11, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(3))       /* all */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_D8_12))
#define CHIP_PIN_PD11_ETH_MII_RXD2      CHIP_PIN_ID_IN      (CHIP_PORT_D, 11, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))       /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD11_CAN2_TX           CHIP_PIN_ID_AF      (CHIP_PORT_D, 11, CHIP_PER_ID_CAN2,   CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PD11_SPI3_MISO_MST     CHIP_PIN_ID_IN      (CHIP_PORT_D, 11, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(2))       /* n45x */
#define CHIP_PIN_PD11_SPI3_MISO_SLV     CHIP_PIN_ID_AF      (CHIP_PORT_D, 11, CHIP_PER_ID_SPI3,   CHIP_PIN_REMAP1(2))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD11_TIM9_CH2_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 11, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#define CHIP_PIN_PD11_TIM9_CH2_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 11, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PD12 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD12_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 12)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD12_EXMC_A17          CHIP_PIN_ID_AF      (CHIP_PORT_D, 12, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#define CHIP_PIN_PD12_EXMC_ALE          CHIP_PIN_ID_AF      (CHIP_PORT_D, 12, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* gd103? */
#endif
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_D12_15)
#define CHIP_PIN_PD12_TIM4_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 12, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#define CHIP_PIN_PD12_TIM4_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 12, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#endif
#if !defined CHIP_IOREMAP_USART3 || (CHIP_IOREMAP_USART3 == CHIP_IOREMAP_USART3_D8_12) || (CHIP_IOREMAP_USART3 ==CHIP_IOREMAP_USART3_A13_14_D10_12)
#define CHIP_PIN_PD12_USART3_RTS        CHIP_PIN_ID_AF      (CHIP_PORT_D, 12, CHIP_PER_ID_USART3, CHIP_PIN_REMAP1(3))       /* all */
#endif
#if (CHIP_DEV_ETH_CNT >= 1) && (!defined CHIP_IOREMAP_ETH || (CHIP_IOREMAP_ETH == CHIP_IOREMAP_ETH_D8_12))
#define CHIP_PIN_PD12_ETH_MII_RXD3      CHIP_PIN_ID_IN      (CHIP_PORT_D, 12, CHIP_PER_ID_ETH,    CHIP_PIN_REMAP1(1))       /* st107, n457, at407, chv307 */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PD12_SPI3_MOSI_MST     CHIP_PIN_ID_AF      (CHIP_PORT_D, 12, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD12_SPI3_MOSI_SLV     CHIP_PIN_ID_IN      (CHIP_PORT_D, 12, CHIP_PER_ID_SPI3,  CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD12_I2S3_SD_TR        CHIP_PIN_ID_AF      (CHIP_PORT_D, 12, CHIP_PER_ID_I2S3,  CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PD12_I2S3_SD_RV        CHIP_PIN_ID_IN      (CHIP_PORT_D, 12, CHIP_PER_ID_I2S3,  CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD12_TIM9_CH3N         CHIP_PIN_ID_AF      (CHIP_PORT_D, 12, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PD13 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD13_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 13)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD13_EXMC_A18          CHIP_PIN_ID_AF      (CHIP_PORT_D, 13, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_D12_15)
#define CHIP_PIN_PD13_TIM4_CH2_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 13, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#define CHIP_PIN_PD13_TIM4_CH2_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 13, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD13_TIM9_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 13, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#define CHIP_PIN_PD13_TIM9_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 13, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PD14 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PD14_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 14)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD14_EXMC_D0           CHIP_PIN_ID_AF      (CHIP_PORT_D, 14, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_D12_15)
#define CHIP_PIN_PD14_TIM4_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 14, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#define CHIP_PIN_PD14_TIM4_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 14, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PD14_I2C4_SCL          CHIP_PIN_ID_AF_OD   (CHIP_PORT_D, 14, CHIP_PER_ID_I2C4,   CHIP_PIN_REMAP1(1))       /* n45x */
#define CHIP_PIN_PD14_TIM8_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 14, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PD14_TIM8_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 14, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(3))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD14_TIM9_BKIN         CHIP_PIN_ID_IN      (CHIP_PORT_D, 14, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PD15 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PD15_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_D, 15)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PD15_EXMC_D1           CHIP_PIN_ID_AF      (CHIP_PORT_D, 15, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM4 || (CHIP_IOREMAP_TIM4 == CHIP_IOREMAP_TIM4_D12_15)
#define CHIP_PIN_PD15_TIM4_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 15, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#define CHIP_PIN_PD15_TIM4_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 15, CHIP_PER_ID_TIM4,   CHIP_PIN_REMAP1(1))       /* all */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PD15_I2C4_SDA          CHIP_PIN_ID_AF_OD   (CHIP_PORT_D, 15, CHIP_PER_ID_I2C4,   CHIP_PIN_REMAP1(1))       /* n45x */
#define CHIP_PIN_PD15_TIM8_CH2_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 15, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PD15_TIM8_CH2_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 15, CHIP_PER_ID_TIM8,   CHIP_PIN_REMAP1(3))       /* n45x */
#endif
#if defined CHIP_DEV_CH32V307
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PA5_7_B3_5_10_C15))
#define CHIP_PIN_PD15_TIM9_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_D, 15, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#define CHIP_PIN_PD15_TIM9_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_D, 15, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*********************************** Порт E ************************************/
/*----- Пин PE0  ------------------------------------------------------------- */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) || ((defined CHIP_DEV_CH32V307) && (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 68))
#define CHIP_PIN_PE0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 0)
#define CHIP_PIN_PE0_TIM4_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_E, 0, CHIP_PER_ID_TIM4,    CHIP_PIN_REMAP_DEFAULT)   /* all */
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE0_EXMC_NBL0          CHIP_PIN_ID_AF      (CHIP_PORT_E, 0, CHIP_PER_ID_EXMC,    CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE0_UART8_RX           CHIP_PIN_ID_IN      (CHIP_PORT_E, 0, CHIP_PER_ID_UART8,   CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84 */
#endif
#if defined CHIP_DEV_CH32V307 && (!defined CHIP_IOREMAP_UART4 || (CHIP_IOREMAP_UART4 == CHIP_IOREMAP_UART4_PE0_1))
#define CHIP_PIN_PE0_UART4_TX           CHIP_PIN_ID_AF      (CHIP_PORT_E, 0, CHIP_PER_ID_UART4,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif


/*----- Пин PE1  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 1)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE1_EXMC_NBL1          CHIP_PIN_ID_AF      (CHIP_PORT_E, 1, CHIP_PER_ID_EXMC,    CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE1_UART8_TX           CHIP_PIN_ID_AF      (CHIP_PORT_E, 1, CHIP_PER_ID_UART8,   CHIP_PIN_REMAP_DEFAULT)   /* at40x, v84 */
#endif
#if defined CHIP_DEV_CH32V307 && (!defined CHIP_IOREMAP_UART4 || (CHIP_IOREMAP_UART4 == CHIP_IOREMAP_UART4_PE0_1))
#define CHIP_PIN_PE1_UART4_RX           CHIP_PIN_ID_IN      (CHIP_PORT_E, 1, CHIP_PER_ID_UART4,   CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif


/*----- Пин PE2  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 2)
#if CHIP_DEV_SUPPORT_TRACE
#define CHIP_PIN_PE2_TRACE_CK           CHIP_PIN_ID_AF      (CHIP_PORT_E, 2, CHIP_PER_ID_TRACE,  CHIP_PIN_REMAP_DEFAULT)    /* !chx */
#endif
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE2_EXMC_A23           CHIP_PIN_ID_AF      (CHIP_PORT_E, 2, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE2_SPI4_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_E, 2, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE2_SPI4_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_E, 2, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE2_I2S4_CK_MST        CHIP_PIN_ID_AF      (CHIP_PORT_E, 2, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE2_I2S4_CK_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_E, 2, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#endif
#if (CHIP_DEV_UART_CNT >= 6) && defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE2_UART6_TX           CHIP_PIN_ID_AF      (CHIP_PORT_E, 2, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#endif
#if defined CHIP_DEV_CH32V307 || defined CHIP_DEV_CH32F2XX
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PE2_TIM10_BKIN         CHIP_PIN_ID_IN      (CHIP_PORT_E, 2, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307, chf20x */
#endif
#endif
#endif


/*----- Пин PE3  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 3)
#if CHIP_DEV_SUPPORT_TRACE
#define CHIP_PIN_PE3_TRACE_D0           CHIP_PIN_ID_AF      (CHIP_PORT_E, 3, CHIP_PER_ID_TRACE,  CHIP_PIN_REMAP_DEFAULT)    /* !chx */
#endif
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE3_EXMC_A19           CHIP_PIN_ID_AF      (CHIP_PORT_E, 3, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if (CHIP_DEV_UART_CNT >= 6) && defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE3_UART6_RX           CHIP_PIN_ID_IN      (CHIP_PORT_E, 3, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP_DEFAULT)    /* n45x */
#endif
#if defined CHIP_DEV_CH32V307 || defined CHIP_DEV_CH32F2XX
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PE3_TIM10_CH1N         CHIP_PIN_ID_AF      (CHIP_PORT_E, 3, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307, chf20x */
#endif
#endif
#endif

/*----- Пин PE4  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 4)
#if CHIP_DEV_SUPPORT_TRACE
#define CHIP_PIN_PE4_TRACE_D1           CHIP_PIN_ID_AF      (CHIP_PORT_E, 4, CHIP_PER_ID_TRACE,  CHIP_PIN_REMAP_DEFAULT)    /* !chx */
#endif
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE4_EXMC_A20           CHIP_PIN_ID_AF      (CHIP_PORT_E, 4, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE4_SPI4_NSS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_E, 4, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE4_SPI4_NSS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_E, 4, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE4_I2S4_WS_MST        CHIP_PIN_ID_AF      (CHIP_PORT_E, 4, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE4_I2S4_WS_SLV        CHIP_PIN_ID_IN      (CHIP_PORT_E, 4, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#endif
#if defined CHIP_DEV_CH32V307 || defined CHIP_DEV_CH32F2XX
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PE4_TIM10_CH2N         CHIP_PIN_ID_AF      (CHIP_PORT_E, 4, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307, ch20x */
#endif
#endif
#endif


/*----- Пин PE5  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 5)
#if CHIP_DEV_SUPPORT_TRACE
#define CHIP_PIN_PE5_TRACE_D2           CHIP_PIN_ID_AF      (CHIP_PORT_E, 5, CHIP_PER_ID_TRACE,  CHIP_PIN_REMAP_DEFAULT)    /* !chx */
#endif
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE5_EXMC_A21           CHIP_PIN_ID_AF      (CHIP_PORT_E, 5, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_GD32F10X) && CHIP_DEV_SUPPORT_TIM9
#define CHIP_PIN_PE5_TIM9_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_E, 5, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(1))        /* at40x, v84, gdf103zf/g/i/k */
#define CHIP_PIN_PE5_TIM9_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_E, 5, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(1))        /* at40x, v84, gdf103zf/g/i/k */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE5_SPI4_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_E, 5, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE5_SPI4_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_E, 5, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#endif
#if defined CHIP_DEV_CH32V307 || defined CHIP_DEV_CH32F2XX
#if CHIP_DEV_SUPPORT_TIM10 && (!defined CHIP_IOREMAP_TIM10 || (CHIP_IOREMAP_TIM10 == CHIP_IOREMAP_TIM10_PD0_5_7_E3_5))
#define CHIP_PIN_PE5_TIM10_CH3N         CHIP_PIN_ID_AF      (CHIP_PORT_E, 4, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(2))        /* chv307, chf20x */
#endif
#endif
#endif


/*----- Пин PE6  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 6)
#if CHIP_DEV_SUPPORT_TRACE
#define CHIP_PIN_PE6_TRACE_D3           CHIP_PIN_ID_AF      (CHIP_PORT_E, 6, CHIP_PER_ID_TRACE,  CHIP_PIN_REMAP_DEFAULT)    /* !chx */
#endif
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE6_EXMC_A22           CHIP_PIN_ID_AF      (CHIP_PORT_E, 6, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if (defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX || defined CHIP_DEV_GD32F10X) && CHIP_DEV_SUPPORT_TIM9
#define CHIP_PIN_PE6_TIM9_CH2_IN        CHIP_PIN_ID_IN      (CHIP_PORT_E, 6, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(1))        /* at40x, v84, gdf103zf/g/i/k */
#define CHIP_PIN_PE6_TIM9_CH2_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_E, 6, CHIP_PER_ID_TIM9,   CHIP_PIN_REMAP1(1))        /* at40x, v84, gdf103zf/g/i/k */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE6_SPI4_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_E, 6, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE6_SPI4_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_E, 6, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE6_I2S4_SD_TR         CHIP_PIN_ID_AF      (CHIP_PORT_E, 6, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#define CHIP_PIN_PE6_I2S4_SD_RV         CHIP_PIN_ID_IN      (CHIP_PORT_E, 6, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84 */
#endif
#endif


/*----- Пин PE7  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 7)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE7_EXMC_D4            CHIP_PIN_ID_AF      (CHIP_PORT_E, 7, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE7_TIM1_ETR           CHIP_PIN_ID_IN      (CHIP_PORT_E, 7, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE7_UART7_RX           CHIP_PIN_ID_IN      (CHIP_PORT_E, 7, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE7_UART4_RX           CHIP_PIN_ID_IN      (CHIP_PORT_E, 7, CHIP_PER_ID_UART4,  CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PE7_SPI1_SCK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_E, 7, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE7_SPI1_SCK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_E, 7, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#define CHIP_PIN_PE7_OPA3_OUT1          CHIP_PIN_ID_ANALOG  (CHIP_PORT_E, 7)                                                /* chv307 */
#endif
#endif


/*----- Пин PE8  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 8)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE8_EXMC_D5            CHIP_PIN_ID_AF      (CHIP_PORT_E, 8, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE8_TIM1_CH1N          CHIP_PIN_ID_AF      (CHIP_PORT_E, 8, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE8_UART7_TX           CHIP_PIN_ID_AF      (CHIP_PORT_E, 8, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP_DEFAULT)    /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE8_UART5_TX           CHIP_PIN_ID_AF      (CHIP_PORT_E, 8, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PE8_SDIO1_D0           CHIP_PIN_ID_AF      (CHIP_PORT_E, 8, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PE8_SPI1_MISO_MST      CHIP_PIN_ID_IN      (CHIP_PORT_E, 8, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE8_SPI1_MISO_SLV      CHIP_PIN_ID_AF      (CHIP_PORT_E, 8, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_UART5 || (CHIP_IOREMAP_UART5 == CHIP_IOREMAP_UART5_PE8_9)
#define CHIP_PIN_PE8_UART5_TX           CHIP_PIN_ID_AF      (CHIP_PORT_E, 8, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#define CHIP_PIN_PE8_OPA4_OUT1          CHIP_PIN_ID_ANALOG  (CHIP_PORT_E, 8)                                                /* chv307 */
#endif
#endif


/*----- Пин PE9  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 9)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE9_EXMC_D6            CHIP_PIN_ID_AF      (CHIP_PORT_E, 9, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE9_TIM1_CH1_IN        CHIP_PIN_ID_IN      (CHIP_PORT_E, 9, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))        /* all */
#define CHIP_PIN_PE9_TIM1_CH1_OUT       CHIP_PIN_ID_AF      (CHIP_PORT_E, 9, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))        /* all */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE9_UART5_RX           CHIP_PIN_ID_IN      (CHIP_PORT_E, 9, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PE9_SDIO1_D1           CHIP_PIN_ID_AF      (CHIP_PORT_E, 9, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PE9_SPI1_MOSI_MST      CHIP_PIN_ID_AF      (CHIP_PORT_E, 9, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE9_SPI1_MOSI_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_E, 9, CHIP_PER_ID_SPI1,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_UART5 || (CHIP_IOREMAP_UART5 == CHIP_IOREMAP_UART5_PE8_9)
#define CHIP_PIN_PE9_UART5_RX           CHIP_PIN_ID_IN      (CHIP_PORT_E, 9, CHIP_PER_ID_UART5,  CHIP_PIN_REMAP1(2))        /* chv307 */
#endif
#endif
#endif


/*----- Пин PE10 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 10)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE10_EXMC_D7           CHIP_PIN_ID_AF      (CHIP_PORT_E, 10, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE10_TIM1_CH2N         CHIP_PIN_ID_AF      (CHIP_PORT_E, 10, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE10_SDIO1_D2          CHIP_PIN_ID_AF      (CHIP_PORT_E, 10, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP1(1))       /* n45x */
#define CHIP_PIN_PE10_SPI2_NSS_MST      CHIP_PIN_ID_AF      (CHIP_PORT_E, 10, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PE10_SPI2_NSS_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_E, 10, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PE10_I2S2_WS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_E, 10, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))       /* n45x */
#define CHIP_PIN_PE10_I2S2_WS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_E, 10, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))       /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART6 || (CHIP_IOREMAP_USART6 == CHIP_IOREMAP_USART6_PE10_11)
#define CHIP_PIN_PE10_UART6_TX          CHIP_PIN_ID_AF      (CHIP_PORT_E, 10, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PE11 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE11_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 11)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE11_EXMC_D8           CHIP_PIN_ID_AF      (CHIP_PORT_E, 11, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE11_TIM1_CH2_IN       CHIP_PIN_ID_IN      (CHIP_PORT_E, 11, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#define CHIP_PIN_PE11_TIM1_CH2_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_E, 11, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE11_SPI4_SCK_MST      CHIP_PIN_ID_AF      (CHIP_PORT_E, 11, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE11_SPI4_SCK_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_E, 11, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE11_I2S4_CK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_E, 11, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE11_I2S4_CK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_E, 11, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE11_SDIO1_D3          CHIP_PIN_ID_AF      (CHIP_PORT_E, 11, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PE11_SPI2_SCK_MST      CHIP_PIN_ID_AF      (CHIP_PORT_E, 11, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE11_SPI2_SCK_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_E, 11, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE11_I2S2_CK_MST       CHIP_PIN_ID_AF      (CHIP_PORT_E, 11, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE11_I2S2_CK_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_E, 11, CHIP_PER_ID_I2S2,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART6 || (CHIP_IOREMAP_USART6 == CHIP_IOREMAP_USART6_PE10_11)
#define CHIP_PIN_PE11_UART6_RX          CHIP_PIN_ID_IN      (CHIP_PORT_E, 11, CHIP_PER_ID_UART6,  CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PE12 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE12_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 12)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE12_EXMC_D9           CHIP_PIN_ID_AF      (CHIP_PORT_E, 12, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE12_TIM1_CH3N         CHIP_PIN_ID_AF      (CHIP_PORT_E, 12, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE12_SPI4_NSS_MST      CHIP_PIN_ID_AF      (CHIP_PORT_E, 12, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE12_SPI4_NSS_SLV      CHIP_PIN_ID_IN      (CHIP_PORT_E, 12, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE12_SPI4_WS_MST       CHIP_PIN_ID_AF      (CHIP_PORT_E, 12, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE12_SPI4_WS_SLV       CHIP_PIN_ID_IN      (CHIP_PORT_E, 12, CHIP_PER_ID_I2S4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE12_SDIO1_CK          CHIP_PIN_ID_AF      (CHIP_PORT_E, 12, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PE12_SPI2_MISO_MST     CHIP_PIN_ID_IN      (CHIP_PORT_E, 12, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE12_SPI2_MISO_SLV     CHIP_PIN_ID_AF      (CHIP_PORT_E, 12, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART7 || (CHIP_IOREMAP_USART7 == CHIP_IOREMAP_USART7_PE12_13)
#define CHIP_PIN_PE12_UART7_TX          CHIP_PIN_ID_AF      (CHIP_PORT_E, 12, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PE13 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE13_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 13)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE13_EXMC_D10          CHIP_PIN_ID_AF      (CHIP_PORT_E, 13, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE13_TIM1_CH3_IN       CHIP_PIN_ID_IN      (CHIP_PORT_E, 13, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#define CHIP_PIN_PE13_TIM1_CH3_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_E, 13, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE13_SPI4_MISO_MST     CHIP_PIN_ID_IN      (CHIP_PORT_E, 13, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE13_SPI4_MISO_SLV     CHIP_PIN_ID_AF      (CHIP_PORT_E, 13, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PE13_SDIO1_CMD         CHIP_PIN_ID_AF      (CHIP_PORT_E, 13, CHIP_PER_ID_SDIO1,  CHIP_PIN_REMAP1(1))        /* n45x */
#define CHIP_PIN_PE13_SPI2_MOSI_MST     CHIP_PIN_ID_AF      (CHIP_PORT_E, 13, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE13_SPI2_MOSI_SLV     CHIP_PIN_ID_IN      (CHIP_PORT_E, 13, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE13_I2S2_SD_TR        CHIP_PIN_ID_AF      (CHIP_PORT_E, 13, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#define CHIP_PIN_PE13_I2S2_SD_RV        CHIP_PIN_ID_IN      (CHIP_PORT_E, 13, CHIP_PER_ID_SPI2,   CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART7 || (CHIP_IOREMAP_USART7 == CHIP_IOREMAP_USART7_PE12_13)
#define CHIP_PIN_PE13_UART7_RX          CHIP_PIN_ID_IN      (CHIP_PORT_E, 13, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#endif
#endif


/*----- Пин PE14 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE14_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 14)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE14_EXMC_D11          CHIP_PIN_ID_AF      (CHIP_PORT_E, 14, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE14_TIM1_CH4_IN       CHIP_PIN_ID_IN      (CHIP_PORT_E, 14, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#define CHIP_PIN_PE14_TIM1_CH4_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_E, 14, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#endif
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_PIN_PE14_SPI4_MOSI_MST     CHIP_PIN_ID_AF      (CHIP_PORT_E, 14, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE14_SPI4_MOSI_SLV     CHIP_PIN_ID_IN      (CHIP_PORT_E, 14, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE14_I2S4_SD_TR        CHIP_PIN_ID_AF      (CHIP_PORT_E, 14, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#define CHIP_PIN_PE14_I2S4_SD_RV        CHIP_PIN_ID_IN      (CHIP_PORT_E, 14, CHIP_PER_ID_SPI4,   CHIP_PIN_REMAP1(1))       /* at40x, v84x */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART8 || (CHIP_IOREMAP_USART8 == CHIP_IOREMAP_USART8_PE14_15)
#define CHIP_PIN_PE14_UART8_TX          CHIP_PIN_ID_AF      (CHIP_PORT_E, 14, CHIP_PER_ID_UART8,  CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#define CHIP_PIN_PE14_OPA2_OUT1         CHIP_PIN_ID_ANALOG  (CHIP_PORT_E, 14)                                               /* chv307 */
#endif
#endif


/*----- Пин PE15 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 80
#define CHIP_PIN_PE15_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_E, 15)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PE15_EXMC_D12          CHIP_PIN_ID_AF      (CHIP_PORT_E, 15, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#if !defined CHIP_IOREMAP_TIM1 || (CHIP_IOREMAP_TIM1 == CHIP_IOREMAP_TIM1_E7_15)
#define CHIP_PIN_PE15_TIM1_BRK          CHIP_PIN_ID_IN      (CHIP_PORT_E, 15, CHIP_PER_ID_TIM1,   CHIP_PIN_REMAP1(3))       /* all */
#endif
#ifdef CHIP_DEV_CH32V307
#if !defined CHIP_IOREMAP_USART8 || (CHIP_IOREMAP_USART8 == CHIP_IOREMAP_USART8_PE14_15)
#define CHIP_PIN_PE15_UART8_RX          CHIP_PIN_ID_IN      (CHIP_PORT_E, 15, CHIP_PER_ID_UART8,  CHIP_PIN_REMAP1(2))       /* chv307 */
#endif
#define CHIP_PIN_PE15_OPA1_OUT1         CHIP_PIN_ID_ANALOG  (CHIP_PORT_E, 15)                                               /* chv307 */
#endif
#endif


/*********************************** Порт F ***********************************/
/*----- Пин PF0  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 0)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF0_EXMC_A0            CHIP_PIN_ID_AF      (CHIP_PORT_F, 0, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PF0_QSPI_NSS           CHIP_PIN_ID_AF      (CHIP_PORT_F, 0, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#endif

/*----- Пин PF1  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 1)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF1_EXMC_A1            CHIP_PIN_ID_AF      (CHIP_PORT_F, 1, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PF1_QSPI_CLK           CHIP_PIN_ID_AF      (CHIP_PORT_F, 1, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#endif

/*----- Пин PF2  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 2)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF2_EXMC_A2            CHIP_PIN_ID_AF      (CHIP_PORT_F, 2, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PF2_QSPI_IO0           CHIP_PIN_ID_AF      (CHIP_PORT_F, 2, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PF2_ADC12_IN10         CHIP_PIN_ID_ANALOG  (CHIP_PORT_F, 2) /* n45x */
#endif
#endif

/*----- Пин PF3  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 3)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF3_EXMC_A3            CHIP_PIN_ID_AF      (CHIP_PORT_F, 3, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PF3_QSPI_IO1           CHIP_PIN_ID_AF      (CHIP_PORT_F, 3, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#endif

/*----- Пин PF4  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 4)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF4_EXMC_A4            CHIP_PIN_ID_AF      (CHIP_PORT_F, 4, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PF4_QSPI_IO2           CHIP_PIN_ID_AF      (CHIP_PORT_F, 4, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PF4_I2C3_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_F, 4, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PF4_ADC1_IN5           CHIP_PIN_ID_ANALOG  (CHIP_PORT_F, 4) /* n45x */
#endif
#endif

/*----- Пин PF5  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 5)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF5_EXMC_A5            CHIP_PIN_ID_AF      (CHIP_PORT_F, 5, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* st10x */
#endif
#ifdef CHIP_DEV_N32G45X
#define CHIP_PIN_PF5_QSPI_IO2           CHIP_PIN_ID_AF      (CHIP_PORT_F, 5, CHIP_PER_ID_QSPI,   CHIP_PIN_REMAP1(2))        /* n45x */
#define CHIP_PIN_PF5_I2C3_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_F, 5, CHIP_PER_ID_I2C3,   CHIP_PIN_REMAP1(2))        /* n45x */
#endif
#endif

/*----- Пин PF6  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PF6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 6)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF6_EXMC_NIORD         CHIP_PIN_ID_AF      (CHIP_PORT_F, 6, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_GD32F10X && CHIP_DEV_SUPPORT_TIM10
#define CHIP_PIN_PF6_TIM10_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_F, 6, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#define CHIP_PIN_PF6_TIM10_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_F, 6, CHIP_PER_ID_TIM10,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#endif
#define CHIP_PIN_PF6_ADC3_IN4           CHIP_PIN_ID_ANALOG  (CHIP_PORT_F, 6)                                                /* all */
#endif


/*----- Пин PF7  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PF7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 7)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF7_EXMC_NREG          CHIP_PIN_ID_AF      (CHIP_PORT_F, 7, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_GD32F10X && CHIP_DEV_SUPPORT_TIM11
#define CHIP_PIN_PF7_TIM11_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_F, 7, CHIP_PER_ID_TIM11,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#define CHIP_PIN_PF7_TIM11_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_F, 7, CHIP_PER_ID_TIM11,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#endif
#define CHIP_PIN_PF7_ADC3_IN5           CHIP_PIN_ID_ANALOG  (CHIP_PORT_F, 7)                                                /* all */
#endif


/*----- Пин PF8  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PF8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 8)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF8_EXMC_NIOWR         CHIP_PIN_ID_AF      (CHIP_PORT_F, 8, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_GD32F10X && CHIP_DEV_SUPPORT_TIM13
#define CHIP_PIN_PF8_TIM13_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_F, 8, CHIP_PER_ID_TIM13,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#define CHIP_PIN_PF8_TIM13_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_F, 8, CHIP_PER_ID_TIM13,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#endif
#define CHIP_PIN_PF8_ADC3_IN6           CHIP_PIN_ID_ANALOG  (CHIP_PORT_F, 8)                                                /* all */
#endif


/*----- Пин PF9  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PF9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 9)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF9_EXMC_CD            CHIP_PIN_ID_IN      (CHIP_PORT_F, 9, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#ifdef CHIP_DEV_GD32F10X && CHIP_DEV_SUPPORT_TIM14
#define CHIP_PIN_PF9_TIM14_CH1_IN       CHIP_PIN_ID_IN      (CHIP_PORT_F, 9, CHIP_PER_ID_TIM14,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#define CHIP_PIN_PF9_TIM14_CH1_OUT      CHIP_PIN_ID_AF      (CHIP_PORT_F, 9, CHIP_PER_ID_TIM14,  CHIP_PIN_REMAP1(1))        /* gdf103zf/g/i/k */
#endif
#define CHIP_PIN_PF9_ADC3_IN7           CHIP_PIN_ID_ANALOG  (CHIP_PORT_F, 9)                                                /* all */
#endif


/*----- Пин PF10  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PF10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 10)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF10_EXMC_INTR         CHIP_PIN_ID_IN      (CHIP_PORT_F, 10, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#define CHIP_PIN_PF10_ADC3_IN8          CHIP_PIN_ID_ANALOG  (CHIP_PORT_F, 10)                                               /* all */
#endif


/*----- Пин PF11 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF11_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 11)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF11_EXMC_NIOS16       CHIP_PIN_ID_IN      (CHIP_PORT_F, 11, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif


/*----- Пин PF12 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF12_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 12)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF12_EXMC_A6           CHIP_PIN_ID_AF      (CHIP_PORT_F, 12, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif


/*----- Пин PF13 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF13_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 13)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF13_EXMC_A7           CHIP_PIN_ID_AF      (CHIP_PORT_F, 13, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif


/*----- Пин PF14 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF14_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 14)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF14_EXMC_A8           CHIP_PIN_ID_AF      (CHIP_PORT_F, 14, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif


/*----- Пин PF15 ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PF15_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_F, 15)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PF15_EXMC_A9           CHIP_PIN_ID_AF      (CHIP_PORT_F, 15, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif




/*********************************** Порт G ***********************************/
/*----- Пин PG0  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PG0_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 0)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG0_EXMC_A10           CHIP_PIN_ID_AF      (CHIP_PORT_G, 0, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PG0_UART7_TX           CHIP_PIN_ID_AF      (CHIP_PORT_G, 0, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#endif

/*----- Пин PG1  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PG1_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 1)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG1_EXMC_A11           CHIP_PIN_ID_AF      (CHIP_PORT_G, 1, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PG1_UART7_RX           CHIP_PIN_ID_IN      (CHIP_PORT_G, 1, CHIP_PER_ID_UART7,  CHIP_PIN_REMAP1(3))        /* n45x */
#endif
#endif

/*----- Пин PG2  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PG2_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 2)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG2_EXMC_A12           CHIP_PIN_ID_AF      (CHIP_PORT_G, 2, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PG2_I2C2_SCL           CHIP_PIN_ID_AF_OD   (CHIP_PORT_G, 2, CHIP_PER_ID_I2C2,   CHIP_PIN_REMAP1(1))        /* n45x */
#endif
#endif

/*----- Пин PG3  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PG3_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 3)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG3_EXMC_A13           CHIP_PIN_ID_AF      (CHIP_PORT_G, 3, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#if defined CHIP_DEV_N32G45X
#define CHIP_PIN_PG3_I2C2_SDA           CHIP_PIN_ID_AF_OD   (CHIP_PORT_G, 3, CHIP_PER_ID_I2C2,   CHIP_PIN_REMAP1(1))        /* n45x */
#endif
#endif

/*----- Пин PG4  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PG4_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 4)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG4_EXMC_A14           CHIP_PIN_ID_AF      (CHIP_PORT_G, 4, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#endif

/*----- Пин PG5  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PG5_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 5)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG5_EXMC_A15           CHIP_PIN_ID_AF      (CHIP_PORT_G, 5, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#endif

/*----- Пин PG6  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG6_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 6)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG6_EXMC_INT2          CHIP_PIN_ID_AF      (CHIP_PORT_G, 6, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#endif

/*----- Пин PG7  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG7_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 7)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG7_EXMC_INT3          CHIP_PIN_ID_AF      (CHIP_PORT_G, 7, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#endif

/*----- Пин PG8  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG8_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 8)
#endif

/*----- Пин PG9  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128
#define CHIP_PIN_PG9_GPIO               CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 9)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG9_EXMC_NE2           CHIP_PIN_ID_AF      (CHIP_PORT_G, 9, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#define CHIP_PIN_PG9_EXMC_NCE3          CHIP_PIN_ID_AF      (CHIP_PORT_G, 9, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)    /* !n45x */
#endif
#endif

/*----- Пин PG10  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG10_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 10)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG10_EXMC_NE3          CHIP_PIN_ID_AF      (CHIP_PORT_G, 10, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#define CHIP_PIN_PG10_EXMC_NCE4_1       CHIP_PIN_ID_AF      (CHIP_PORT_G, 10, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif

/*----- Пин PG11  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG11_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 11)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG11_EXMC_NCE4_2       CHIP_PIN_ID_AF      (CHIP_PORT_G, 11, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif

/*----- Пин PG12  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG12_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 12)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG12_EXMC_NE4          CHIP_PIN_ID_AF      (CHIP_PORT_G, 12, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif

/*----- Пин PG13  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG13_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 13)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG13_EXMC_A24          CHIP_PIN_ID_AF      (CHIP_PORT_G, 13, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif

/*----- Пин PG14  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG14_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 14)
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PIN_PG14_EXMC_A25          CHIP_PIN_ID_AF      (CHIP_PORT_G, 14, CHIP_PER_ID_EXMC,   CHIP_PIN_REMAP_DEFAULT)   /* !n45x */
#endif
#endif

/*----- Пин PG15  ------------------------------------------------------------- */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144
#define CHIP_PIN_PG15_GPIO              CHIP_PIN_ID_GPIO    (CHIP_PORT_G, 15)
#endif


#endif // CHIP_PINS_H

