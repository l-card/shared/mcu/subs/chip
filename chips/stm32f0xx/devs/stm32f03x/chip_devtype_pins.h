#ifndef CHIP_DEVTYPE_PINS_H
#define CHIP_DEVTYPE_PINS_H

#include "chip_dev_spec_features.h"




/* ------------------------ Функции пинов ------------------------------------*/

/* ----------- Порт A ---------------*/
/* Пин PA0, WKUP1, RTC_TAMP2,  CKI_4 (hk030, hk04A) */
#define CHIP_PIN_PA0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 0) */
#if CHIP_DEV_SUPPORT_USART2
#define CHIP_PIN_PA0_USART2_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 1) /* stm030x8/xC, hk030x8, hk04Ax8 */
#else
#define CHIP_PIN_PA0_USART1_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PA0_TIM2_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 2)  /* stm031, hk031, hk04A */
#define CHIP_PIN_PA0_TIM2_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 2)  /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 3) */
#if CHIP_DEV_SUPPORT_USART4
#define CHIP_PIN_PA0_USART4_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 4) /* stm030xC */
#endif
#define CHIP_PIN_PA0_ADC_IN0              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 0)    /* all */


/* Пин PA1 */
#define CHIP_PIN_PA1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 0) /* all */
#if CHIP_DEV_SUPPORT_USART2
#define CHIP_PIN_PA1_USART2_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 1) /* stm030x8/xC, hk030x8, hk04Ax8 */
#else
#define CHIP_PIN_PA1_USART1_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PA1_TIM2_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 2) /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 3) */
#if CHIP_DEV_SUPPORT_USART4
#define CHIP_PIN_PA1_USART4_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 4) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_TIM15 && !defined CHIP_DEV_STM32F030X8
#define CHIP_PIN_PA1_TIM15_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 5) /* stm030xC, hk030, hk04A */
#endif
#define CHIP_PIN_PA1_ADC_IN1              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)    /* all */


/* Пин PA2, WKUP4 */
#define CHIP_PIN_PA2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 2)
#if CHIP_DEV_SUPPORT_TIM15
#define CHIP_PIN_PA2_TIM15_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 0) /* stm030x8/xC, hk030, hk04A */
#endif
#if CHIP_DEV_SUPPORT_USART2
#define CHIP_PIN_PA2_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 1) /* stm030x4/x6, stm031, hk030x8, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA2_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 1) /* hk030x8, hk04Ax8 */
#endif
#else
#define CHIP_PIN_PA2_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA2_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 1) /* hk030x4/x6, hk031, hk04Ax4 */
#endif
#endif
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PA2_TIM2_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 2) /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 5) */
#define CHIP_PIN_PA2_ADC_IN2              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 2)    /* all */


/* Пин PA3 */
#define CHIP_PIN_PA3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 3)
#if CHIP_DEV_SUPPORT_TIM15
#define CHIP_PIN_PA3_TIM15_CH2            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 0) /* stm030x8/xC, hk030, hk04A */
#endif
#if CHIP_DEV_SUPPORT_USART2
#define CHIP_PIN_PA3_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 1) /* stm030x8/xC, hk030x8, hk04Ax8 */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA3_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 1) /* hk030x8, hk04Ax8 */
#endif
#else
#define CHIP_PIN_PA3_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA3_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 1) /* hk030x8, hk031, hk04Ax8 */
#endif
#endif
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PA3_TIM2_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 2) /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 5) */
#define CHIP_PIN_PA3_ADC_IN3              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 3)    /* all */


/* Пин PA4, CKI_1 (hk030, hk04A) */
#define CHIP_PIN_PA4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_SPI1_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PA4_I2S1_WS              CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 0) /* hk030, stm031, hk031, hk04A */
#endif
#if CHIP_DEV_SUPPORT_USART_CK_EMAP
#if CHIP_DEV_SUPPORT_USART2
#define CHIP_PIN_PA4_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 1) /* stm030x8/xC, hk04Ax8 */
#else
#define CHIP_PIN_PA4_USART1_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 1) /* stm030x4/x6, hk04Ax4/x6 */
#endif
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 3) */
#define CHIP_PIN_PA4_TIM14_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 4) /* all */
#if CHIP_DEV_SUPPORT_USART6
#define CHIP_PIN_PA4_USART6_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 5) /* stm030xC */
#endif
#define CHIP_PIN_PA4_ADC_IN4              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)    /* all */



/* Пин PA5 */
#define CHIP_PIN_PA5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PA5_I2S1_CK              CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 0) /* stm031, hk030, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 1) */
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PA5_TIM2_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 2) /* stm031, hk031, hk04A */
#define CHIP_PIN_PA5_TIM2_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 2) /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 4) */
#if CHIP_DEV_SUPPORT_USART6
#define CHIP_PIN_PA5_USART6_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 5) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 7) */
#define CHIP_PIN_PA5_ADC_IN5              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)    /* all */


/* Пин PA6, WKUP10 (hk04A) */
#define CHIP_PIN_PA6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 6)
#define CHIP_PIN_PA6_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PA6_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 0) /* stm031, hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PA6_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 1) /* all */
#define CHIP_PIN_PA6_TIM1_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PA6_USART3_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 4) /* stm030xC */
#endif
#define CHIP_PIN_PA6_TIM16_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 5) /* all */
#define CHIP_PIN_PA6_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 6) /* all */
#ifdef CHIP_DEV_HK320XX
#define CHIP_PIN_PA6_HIS_TRIM_OUT         CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 15) /* hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PA6_ADC_IN6              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 6)    /* all */


/* Пин PA7, WKUP11 (hk04A) */
#define CHIP_PIN_PA7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 7)
#define CHIP_PIN_PA7_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PA7_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 0) /* stm031, hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PA7_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 1) /* all */
#define CHIP_PIN_PA7_TIM1_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 3) */
#define CHIP_PIN_PA7_TIM14_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 4) /* all */
#define CHIP_PIN_PA7_TIM17_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 5) /* all */
#define CHIP_PIN_PA7_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 6) /* all */
#ifdef CHIP_DEV_HK320XX
#define CHIP_PIN_PA7_MCO                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 15) /* hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PA7_ADC_IN7              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 7)    /* all */


/* Пин PA8 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PA8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 8)
#define CHIP_PIN_PA8_MCO                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 0) /* all */
#define CHIP_PIN_PA8_USART1_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 1) /* all */
#define CHIP_PIN_PA8_TIM1_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 2) /* all */
#define CHIP_PIN_PA8_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 3) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 5) */
#endif


/* Пин PA9, WKUP12 (hk04A) */
#define CHIP_PIN_PA9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 9)
#if CHIP_DEV_SUPPORT_TIM15
#define CHIP_PIN_PA9_TIM15_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 0) /* stm030x8/xC, hk030, hk04A */
#endif
#define CHIP_PIN_PA9_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 1) /* all */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA9_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 1) /* hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PA9_TIM1_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 3) */
#if CHIP_DEV_SUPPORT_I2C1_EMAP
#define CHIP_PIN_PA9_I2C1_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 4) /* stm030x4/x6/xC, stm031, hk030 */
#endif
#if CHIP_DEV_SUPPORT_MCO_EMAP
#define CHIP_PIN_PA9_MCO                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 5) /* stm030x4xC, hk030 */
#endif


/* Пин PA10, WKUP13 (hk04A) */
#define CHIP_PIN_PA10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 10)
#define CHIP_PIN_PA10_TIM17_BKIN          CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 0) /* all */
#define CHIP_PIN_PA10_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 1) /* all */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA10_USART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 1) /* hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PA10_TIM1_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 3) */
#if CHIP_DEV_SUPPORT_I2C1_EMAP
#define CHIP_PIN_PA10_I2C1_SDA            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 4) /* stm030x4/x6/xC, stm031, hk030 */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 5) */


/* Пин PA11 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PA11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 11)
#define CHIP_PIN_PA11_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 0) /* all */
#define CHIP_PIN_PA11_USART1_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 1) /* all */
#define CHIP_PIN_PA11_TIM1_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 4) */
#if CHIP_DEV_SUPPORT_I2C2
#define CHIP_PIN_PA11_I2C2_SCL            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 5) /* stm030, hk030, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 7) */
#endif


/* Пин PA12 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PA12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A)
#define CHIP_PIN_PA12_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 0) /* all */
#define CHIP_PIN_PA12_USART1_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 1) /* all */
#define CHIP_PIN_PA12_TIM1_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 4) */
#if CHIP_DEV_SUPPORT_I2C2
#define CHIP_PIN_PA12_I2C2_SDA            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 5) /* stm030, hk030, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 6) */
#endif


/* Пин PA13, CKI_2 (hk030, hk04A), SWDIO */
#define CHIP_PIN_PA13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 13)
#define CHIP_PIN_PA13_SWDIO               CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 0) /* all */
#define CHIP_PIN_PA13_IR_OUT              CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 7) */


/* Пин PA14, CKI_3 (hk030, hk04A), SWCLK */
#define CHIP_PIN_PA14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 14)
#define CHIP_PIN_PA14_SWCLK               CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 0)
#if CHIP_DEV_SUPPORT_USART2
#define CHIP_PIN_PA14_USART2_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 1) /* stm030x8/xC, hk030x8, hk04Ax8  */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA14_USART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 1) /* hk030x8, hk04Ax8  */
#endif
#else
#define CHIP_PIN_PA14_USART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA14_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 1) /* hk030x4/x6, hk031, hk04Ax4/x6  */
#endif
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 6) */



/* Пин PA15 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 28)
#define CHIP_PIN_PA15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 15)
#define CHIP_PIN_PA15_SPI1_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PA15_I2S1_WS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 0) /* stm031, hk030, hk031, hk04A */
#endif
#if CHIP_DEV_SUPPORT_USART2
#define CHIP_PIN_PA15_USART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1) /* stm030x8/xC, hk030x8, hk04Ax8  */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA15_USART2_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1) /* hk030x8, hk04Ax8  */
#endif
#else
#define CHIP_PIN_PA15_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PA15_USART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1) /* hk030x4/x6, hk031, hk04Ax4/x6  */
#endif
#endif
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PA15_TIM2_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 2) /* stm031, hk031, hk04A */
#define CHIP_PIN_PA15_TIM2_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 2) /* stm031, hk031, hk04A */
#endif
#define CHIP_PIN_PA15_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 3) /* all */
#if CHIP_DEV_SUPPORT_USART4
#define CHIP_PIN_PA15_USART4_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 4) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 5) */
#endif


/* ----------- Порт B ---------------*/
/* Пин PB0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25
#define CHIP_PIN_PB0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 0)
#define CHIP_PIN_PB0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 0) /* all */
#define CHIP_PIN_PB0_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 1) /* all */
#define CHIP_PIN_PB0_TIM1_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PB0_USART3_CK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 4) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 5) */
#define CHIP_PIN_PB0_ADC_IN8              CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 0)    /* all */
#endif


/* Пин PB1 */
#define CHIP_PIN_PB1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 1)
#define CHIP_PIN_PB1_TIM14_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 0) /* all */
#define CHIP_PIN_PB1_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 1) /* all */
#define CHIP_PIN_PB1_TIM1_CH3N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PB1_USART3_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 4) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 5) */
#define CHIP_PIN_PB1_ADC_IN9              CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 1)    /* all */


/* Пин PB2 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32) && (CHIP_DEV_PKG != CHIP_PKG_LQFP32)
#define CHIP_PIN_PB2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 2)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 0) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 1) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 5) */
#ifdef CHIP_DEV_HK32F0XX
#if CHIP_DEV_SUPPORT_I2C2_EMAP
#define CHIP_PIN_PB2_I2C2_SMBA            CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 15) /* hk030x8, hk04Ax8 */
#else
#define CHIP_PIN_PB2_I2C1_SMBA            CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 15) /* hk030x4/x6, hk04Ax4/x6 */
#endif
#endif
#endif


/* Пин PB3 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 28)
#define CHIP_PIN_PB3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 3)
#define CHIP_PIN_PB3_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PB3_I2S1_CK              CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 0) /* stm031, hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PB3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 1) /* all */
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PB3_TIM2_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 2) /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 3) */
#if CHIP_DEV_SUPPORT_USART5
#define CHIP_PIN_PB3_USART5_TX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 4) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 5) */
#endif


/* Пин PB4 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 28)
#define CHIP_PIN_PB4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 4)
#define CHIP_PIN_PB4_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PB4_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 0) /* stm031, hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PB4_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 1) /* all */
#define CHIP_PIN_PB4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 3) */
#if CHIP_DEV_SUPPORT_USART5
#define CHIP_PIN_PB4_USART5_RX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 4) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_TIM17_EMAP
#define CHIP_PIN_PB4_TIM17_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 5) /* stm030xC, hk030, hk031, hk04A */
#endif
#endif


/* Пин PB5 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25)
#define CHIP_PIN_PB5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 5)
#define CHIP_PIN_PB5_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 0) /* all */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PB5_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 0) /* stm031, hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PB5_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 1) /* all */
#define CHIP_PIN_PB5_TIM16_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 2) /* all */
#define CHIP_PIN_PB5_I2C1_SMBA            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 3) /* all */
#if CHIP_DEV_SUPPORT_USART5
#define CHIP_PIN_PB5_USART5_CK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 4) /* stm030xC */
#define CHIP_PIN_PB5_USART5_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 4) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 5) */
#endif


/* Пин PB6, WKUP6 (stm030xC) */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25)
#define CHIP_PIN_PB6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 6)
#define CHIP_PIN_PB6_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 0) /* all */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PB6_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 0) /* hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PB6_I2C1_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 1) /* all */
#define CHIP_PIN_PB6_TIM16_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 5) */
#endif


/* Пин PB7 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25)
#define CHIP_PIN_PB7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 7)
#define CHIP_PIN_PB7_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 0) /* all */
#if CHIP_DEV_SUPPORT_USART_SWAP
#define CHIP_PIN_PB7_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 0) /* hk030, hk031, hk04A */
#endif
#define CHIP_PIN_PB7_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 7, 1) /* all */
#define CHIP_PIN_PB7_TIM17_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 3) */
#if CHIP_DEV_SUPPORT_USART4
#define CHIP_PIN_PB7_USART4_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 4) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 5) */
#endif


/* Пин PB8 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32) && (CHIP_DEV_PKG != CHIP_PKG_LQFP32)
#define CHIP_PIN_PB8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 8)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 0) */
#define CHIP_PIN_PB8_I2C1_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 1) /* all */
#define CHIP_PIN_PB8_TIM16_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 5) */
#endif


/* Пин PB9 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 9)
#define CHIP_PIN_PB9_IR_OUT               CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 0) /* all */
#define CHIP_PIN_PB9_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 9, 1) /* all */
#define CHIP_PIN_PB9_TIM17_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 2) /* all */
#define CHIP_PIN_PB9_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 3) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 4) */
#if CHIP_DEV_SUPPORT_SPI2
#define CHIP_PIN_PB9_SPI2_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 5) /* stm030, hk030, hk04a */
#endif
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PB9_I2S2_WS              CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 5) /* hk030, hk04A */
#endif
#endif


/* Пин PB10 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 0) */
#if CHIP_DEV_SUPPORT_I2C2_EMAP
#define CHIP_PIN_PB10_I2C2_SCL            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 1) /* stm030x8/xC, hk030x8, hk04Ax8 */
#else
#define CHIP_PIN_PB10_I2C1_SCL            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PB10_TIM2_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 2) /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PB10_USART3_TX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 4) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_SPI2
#define CHIP_PIN_PB10_SPI2_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 5) /* stm030, hk030, hk04a */
#endif
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PB10_I2S2_CK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 5) /* hk030, hk04A */
#endif
#endif


/* Пин PB11 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 11)
#define CHIP_PIN_PB11_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 0) /* all */
#if CHIP_DEV_SUPPORT_I2C2_EMAP
#define CHIP_PIN_PB11_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 11, 1) /* stm030x8/xC, hk030x8, hk04Ax8 */
#else
#define CHIP_PIN_PB11_I2C1_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 11, 1) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#if CHIP_DEV_SUPPORT_TIM2
#define CHIP_PIN_PB11_TIM2_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 2) /* stm031, hk031, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PB11_USART3_RX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 4) /* stm030xC */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 5) */
#endif


/* Пин PB12 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 12)
#if CHIP_DEV_SUPPORT_SPI2_EMAP
#define CHIP_PIN_PB12_SPI2_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0) /* stm030x8/xC, hk030x8, hk04Ax8 */
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PB12_I2S2_WS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0) /* hk030x8, hk04Ax8 */
#endif
#else
#define CHIP_PIN_PB12_SPI1_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_I2S1_EMAP
#define CHIP_PIN_PB12_I2S1_WS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0) /* hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#endif
#define CHIP_PIN_PB12_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 1) /* all */
#define CHIP_PIN_PB12_TIM1_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PB12_USART3_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 4) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_TIM15
#define CHIP_PIN_PB12_TIM15_BKIN          CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 5) /* stm030x8/xC, hk030, hk04A */
#endif
#if defined CHIP_DEV_HK32F0XX && CHIP_DEV_SUPPORT_I2C2
#define CHIP_PIN_PB12_I2C2_SMBA           CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 15) /* hk030, hk04A */
#endif
#endif


/* Пин PB13 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 13)
#if CHIP_DEV_SUPPORT_SPI2_EMAP
#define CHIP_PIN_PB13_SPI2_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0) /* stm030x8/xC, hk030x8, hk04Ax8 */
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PB13_I2S2_CK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0) /* hk030x8, hk04Ax8 */
#endif
#else
#define CHIP_PIN_PB13_SPI1_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PB13_I2S1_CK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0) /* hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 1) */
#define CHIP_PIN_PB13_TIM1_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PB13_USART3_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 4) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_I2C2_EMAP
#define CHIP_PIN_PB13_I2C2_SCL            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 5) /* stm030x8/xC, hk030x8, hk04Ax8 */
#endif
#endif


/* Пин PB14 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 14)
#if CHIP_DEV_SUPPORT_SPI2_EMAP
#define CHIP_PIN_PB14_SPI2_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0) /* stm030x8/xC, hk030x8, hk04Ax8 */
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PB14_I2S2_MCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0) /* hk030x8, hk04Ax8 */
#endif
#else
#define CHIP_PIN_PB14_SPI1_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PB14_I2S1_MCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0) /* hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#endif
#if CHIP_DEV_SUPPORT_TIM15
#define CHIP_PIN_PB14_TIM15_CH1           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 1) /* stm030x8/xC, hk030, hk04A */
#endif
#define CHIP_PIN_PB14_TIM1_CH2N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 2) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 3) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PB14_USART3_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 4) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_I2C2_EMAP
#define CHIP_PIN_PB14_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 14, 5) /* stm030x8/xC, hk030x8, hk04Ax8 */
#endif
#endif


/* Пин PB15,  RTC_REFIN, WKUP7  */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 15)
#if CHIP_DEV_SUPPORT_SPI2_EMAP
#define CHIP_PIN_PB15_SPI2_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0) /* stm030x8/xC, hk030x8, hk04Ax8 */
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PB15_I2S2_SD             CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0) /* hk030x8, hk04Ax8 */
#endif
#else
#define CHIP_PIN_PB15_SPI1_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0) /* stm030x4/x6, stm031, hk030x4/x6, hk031, hk04Ax4/x6 */
#if CHIP_DEV_SUPPORT_I2S1
#define CHIP_PIN_PB15_I2S1_SD             CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0) /* hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#endif
#if CHIP_DEV_SUPPORT_TIM15
#define CHIP_PIN_PB15_TIM15_CH2           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 1) /* stm030x8/xC, hk030, hk04A */
#endif
#define CHIP_PIN_PB15_TIM1_CH3N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 2) /* all */
#if CHIP_DEV_SUPPORT_TIM15
#define CHIP_PIN_PB15_TIM15_CH1N          CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 3) /* stm030x8/xC, hk030, hk04A */
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 5) */
#endif



/* ----------- Порт C ---------------*/
/* Пин PC0 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 0)
#define CHIP_PIN_PC0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 0) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 1) */
#if CHIP_DEV_SUPPORT_USART6
#define CHIP_PIN_PC0_USART6_TX            CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 2) /* stm030xC */
#endif
#define CHIP_PIN_PC0_ADC_IN10             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 0)
#endif


/* Пин PC1 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 1)
#define CHIP_PIN_PC1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 0) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 1) */
#if CHIP_DEV_SUPPORT_USART6
#define CHIP_PIN_PC1_USART6_RX            CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 2) /* stm030xC */
#endif
#define CHIP_PIN_PC1_ADC_IN11             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 1)
#endif


/* Пин PC2 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 2)
#define CHIP_PIN_PC2_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 0) /* all */
#if CHIP_DEV_SUPPORT_SPI2
#define CHIP_PIN_PC2_SPI2_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 1) /* stm030, hk030, hk04a */
#endif
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PC2_I2S2_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 1) /* hk030, hk04A */
#endif
#define CHIP_PIN_PC2_ADC_IN12             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 2)
#endif


/* Пин PC3 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 3)
#define CHIP_PIN_PC3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 0) /* all */
#if CHIP_DEV_SUPPORT_SPI2
#define CHIP_PIN_PC3_SPI2_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 1) /* stm030, hk030, hk04a */
#endif
#if CHIP_DEV_SUPPORT_I2S2
#define CHIP_PIN_PC3_I2S2_SD              CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 1) /* hk030, hk04A */
#endif
//#define CHIP_PIN_PC3_ADC_IN13             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 3)
#endif


/* Пин PC4 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 4)
#define CHIP_PIN_PC4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 4, 0) /* all */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PC4_USART3_TX            CHIP_PIN_ID_AF    (CHIP_PORT_C, 4, 1) /* stm030xC */
#endif
#define CHIP_PIN_PC4_ADC_IN14             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 4)    /* all */
#endif


/* Пин PC5, WKUP5 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 5)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 5, 0) */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PC5_USART3_RX            CHIP_PIN_ID_AF    (CHIP_PORT_C, 5, 1) /* stm030xC */
#endif
#define CHIP_PIN_PC5_ADC_IN15             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 5)    /* all */
#endif

/* Пин PC6 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 6)
#define CHIP_PIN_PC6_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_C, 6, 0) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 6, 1) */
#endif

/* Пин PC7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 7)
#define CHIP_PIN_PC7_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_C, 7, 0) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 7, 1) */
#endif

/* Пин PC8 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 8)
#define CHIP_PIN_PC8_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_C, 8, 0) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 8, 1) */
#endif

/* Пин PC9 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 9)
#define CHIP_PIN_PC9_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_C, 9, 0) /* all */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 9, 1) */
#endif

/* Пин PC10 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 10)
#if CHIP_DEV_SUPPORT_USART4
#define CHIP_PIN_PC10_USART4_TX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 10, 0) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PC10_USART3_TX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 10, 1) /* stm030xC */
#endif
#endif

/* Пин PC11 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 11)
#if CHIP_DEV_SUPPORT_USART4
#define CHIP_PIN_PC11_USART4_RX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 11, 0) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PC11_USART3_RX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 11, 1) /* stm030xC */
#endif
#endif

/* Пин PC12 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 12)
#if CHIP_DEV_SUPPORT_USART4
#define CHIP_PIN_PC12_USART4_CK           CHIP_PIN_ID_AF    (CHIP_PORT_C, 12, 0) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PC12_USART3_CK           CHIP_PIN_ID_AF    (CHIP_PORT_C, 12, 1) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_USART5
#define CHIP_PIN_PC12_USART5_TX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 12, 2) /* stm030xC */
#endif
#endif

/* Пин PC13, WKUP2, RTC_TAMP1, RTC_TS, RTC_OUT */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 13)
#endif

/* Пин PC14, OSC32_IN */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 14)
#endif

/* Пин PC15, OSC32_OUT */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 15)
#endif


/* ----------- Порт D ---------------*/
/* Пин PD2 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PD2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 2)
#define CHIP_PIN_PD2_TIM3_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 0) /* all */
#if CHIP_DEV_SUPPORT_USART3
#define CHIP_PIN_PD2_USART3_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 1) /* stm030xC */
#endif
#if CHIP_DEV_SUPPORT_USART5
#define CHIP_PIN_PD2_USART5_RX            CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 2) /* stm030xC */
#endif
#endif



/* ----------- Порт F ---------------*/
/* Пин PF0, OSC_IN */
#define CHIP_PIN_PF0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 0)
#if CHIP_DEV_SUPPORT_I2C1_EMAP
#define CHIP_PIN_PF0_I2C1_SDA             CHIP_PIN_ID_AF    (CHIP_PORT_F, 0, 0) /* stm030xC, stm031, hk030, hk031, hk04a */
#endif

/* Пин PF1, OSC_OUT */
#define CHIP_PIN_PF1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 1)
#if CHIP_DEV_SUPPORT_I2C1_EMAP
#define CHIP_PIN_PF0_I2C1_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_F, 1, 0) /* stm030xC, stm031, hk030, hk031, hk04a */
#endif


/* Пин PF4 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PF4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 4)
#define CHIP_PIN_PF4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_F, 4, 0) /* all */
#endif


/* Пин PF5 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PF5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 4)
#define CHIP_PIN_PF5_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_F, 4, 0) /* all */
#endif



/* Пин PF6 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PF6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 6)
#if CHIP_DEV_SUPPORT_I2C2_EMAP
#define CHIP_PIN_PF6_I2C2_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_F, 6, 0) /* stm030x8/xC, hk030x8, hk04Ax8 */
#else
#define CHIP_PIN_PF6_I2C1_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_F, 6, 0) /* stm030x4/x6, hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#endif


/* Пин PF7 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PF7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 7)
#if CHIP_DEV_SUPPORT_I2C2_EMAP
#define CHIP_PIN_PF7_I2C2_SDA             CHIP_PIN_ID_AF    (CHIP_PORT_F, 7, 0) /* stm030x8/xC, hk030x8, hk04Ax8 */
#else
#define CHIP_PIN_PF7_I2C1_SDA             CHIP_PIN_ID_AF    (CHIP_PORT_F, 7, 0) /* stm030x4/x6, hk030x4/x6, hk031, hk04Ax4/x6 */
#endif
#endif

#endif // CHIP_DEVTYPE_PINS_H
