#ifndef REGS_ECSPI_H
#define REGS_ECSPI_H

#include "chip_ioreg_defs.h"

#define CHIP_ECSPI_FIFO_WRD_SIZE        64
#define CHIP_ECSPI_CHANNELS_CNT         1

/** ECSPI - Register Layout Typedef */
typedef struct {
    __I  uint32_t RXDATA;                            /**< Receive Data Register, offset: 0x0 */
    __O  uint32_t TXDATA;                            /**< Transmit Data Register, offset: 0x4 */
    __IO uint32_t CONREG;                            /**< Control Register, offset: 0x8 */
    __IO uint32_t CONFIGREG;                         /**< Config Register, offset: 0xC */
    __IO uint32_t INTREG;                            /**< Interrupt Control Register, offset: 0x10 */
    __IO uint32_t DMAREG;                            /**< DMA Control Register, offset: 0x14 */
    __IO uint32_t STATREG;                           /**< Status Register, offset: 0x18 */
    __IO uint32_t PERIODREG;                         /**< Sample Period Control Register, offset: 0x1C */
    __IO uint32_t TESTREG;                           /**< Test Control Register, offset: 0x20 */
    uint8_t RESERVED_0[28];
    __O  uint32_t MSGDATA;                           /**< Message Data Register, offset: 0x40 */
} CHIP_REGS_ECSPI_T;

#define CHIP_REGS_ECSPI1         ((CHIP_REGS_ECSPI_T *)CHIP_MEMRGN_ADDR_PERIPH_ECSPI1)
#define CHIP_REGS_ECSPI2         ((CHIP_REGS_ECSPI_T *)CHIP_MEMRGN_ADDR_PERIPH_ECSPI2)
#define CHIP_REGS_ECSPI3         ((CHIP_REGS_ECSPI_T *)CHIP_MEMRGN_ADDR_PERIPH_ECSPI3)


/*********** Control Register (ECSPI_CONREG) **********************************/
#define CHIP_REGFLD_ECSPI_CONREG_BURST_LENGTH       (0x00000FFFUL << 20U) /* (RW) Burst Length. Total transferred bits with CS asserted (0 - 1 bit, 0xFFF - 2^12 bit) */
#define CHIP_REGFLD_ECSPI_CONREG_CH_SELECT          (0x00000003UL << 18U) /* (RW) Selects the external SPI Master/Slave Device (0 - CS0, others - reserved) */
#define CHIP_REGFLD_ECSPI_CONREG_DRCTL              (0x00000003UL << 16U) /* (RW) SPI Data Ready Control. */
#define CHIP_REGFLD_ECSPI_CONREG_PRE_DIVIDER        (0x0000000FUL << 12U) /* (RW) SPI Pre Divider (0x0 - 1, 0xF - 16) */
#define CHIP_REGFLD_ECSPI_CONREG_POST_DIVIDER       (0x0000000FUL <<  8U) /* (RW) SPI Post Divider 2^n (0x0 - 1, 0xF - 2^15) */
#define CHIP_REGFLD_ECSPI_CONREG_CH_MODE(n)         (0x00000001UL << (4U + (n))) /* (RW) Channel n Slave (0) or Master (1) mode (n = 0) */
#define CHIP_REGFLD_ECSPI_CONREG_SMC                (0x00000001UL <<  3U) /* (RW) Start Mode Control (start on XCH or data write) */
#define CHIP_REGFLD_ECSPI_CONREG_XCH                (0x00000001UL <<  2U) /* (RW1SC) SPI Exchange Bit. If SMC = 0 start XPI exchange */
#define CHIP_REGFLD_ECSPI_CONREG_HT                 (0x00000001UL <<  1U) /* (RW) Hardware Trigger Enable (not supported) */
#define CHIP_REGFLD_ECSPI_CONREG_EN                 (0x00000001UL <<  0U) /* (RW) SPI Block Enable Control */


#define CHIP_REGFLDVAL_ECSPI_CONREG_DRCTL_NOTUSED   0 /* The SPI_RDY signal is a don't care. */
#define CHIP_REGFLDVAL_ECSPI_CONREG_DRCTL_FALL      1 /* Burst will be triggered by the falling edge of the SPI_RDY signal (edge-triggered). */
#define CHIP_REGFLDVAL_ECSPI_CONREG_DRCTL_LOW       2 /* Burst will be triggered by a low level of the SPI_RDY signal (level-triggered).*/

#define CHIP_REGFLDVAL_ECSPI_CONREG_CH_MODE_SLAVE   0 /* Slave mode */
#define CHIP_REGFLDVAL_ECSPI_CONREG_CH_MODE_MASTER  1 /* Master mode */

#define CHIP_REGFLDVAL_ECSPI_CONREG_SMC_XCH         0 /* SPI Exchange Bit (XCH) controls when a SPI burst can start */
#define CHIP_REGFLDVAL_ECSPI_CONREG_SMC_DRDY        1 /* Immediately starts a SPI burst when data is written in TXFIFO */


/*********** Config Register (ECSPI_CONFIGREG) ********************************/
#define CHIP_REGFLD_ECSPI_CONFIGREG_HT_LENGTH       (0x0000001FUL << 24U) /* (RW) The message length in HT Mode (not supported) */
#define CHIP_REGFLD_ECSPI_CONFIGREG_SCLK_CTL(n)     (0x00000001UL <<(20U + (n))) /* (RW) Inactive state of SCLK for SPI channel n (n = 0) */
#define CHIP_REGFLD_ECSPI_CONFIGREG_DATA_CTL(n)     (0x00000001UL <<(16U + (n))) /* (RW) Inactive state of the data line for SPI channel n (n = 0) */
#define CHIP_REGFLD_ECSPI_CONFIGREG_SS_POL(n)       (0x00000001UL <<(12U + (n))) /* (RW) SS (chip select) polarity select for SPI channel n (n = 0) */
#define CHIP_REGFLD_ECSPI_CONFIGREG_SS_CTL(n)       (0x00000001UL << (8U + (n))) /* (RW) SS (chip select) wave form select (multiburst for master) for SPI channel n (n = 0) */
#define CHIP_REGFLD_ECSPI_CONFIGREG_SCLK_POL(n)     (0x00000001UL << (4U + (n))) /* (RW) SS (chip select) clock polarity control for SPI channel n (n = 0) */
#define CHIP_REGFLD_ECSPI_CONFIGREG_SCLK_PHA(n)     (0x00000001UL << (0U + (n))) /* (RW) SS (chip select) clock/data phase control for SPI channel n (n = 0) */
#define CHIP_REGMSK_ECSPI_CONFIGREG_RESERVED        (0xE0000000UL)


/*********** Interrupt Control Register (ECSPI_INTREG) ************************/
#define CHIP_REGFLD_ECSPI_INTREG_TCEN               (0x00000001UL <<  7U) /* (RW) Transfer Completed Interrupt enable */
#define CHIP_REGFLD_ECSPI_INTREG_ROEN               (0x00000001UL <<  6U) /* (RW) RXFIFO Overflow Interrupt enable */
#define CHIP_REGFLD_ECSPI_INTREG_RFEN               (0x00000001UL <<  5U) /* (RW) RXFIFO Full Interrupt enable. */
#define CHIP_REGFLD_ECSPI_INTREG_RDREN              (0x00000001UL <<  4U) /* (RW) RXFIFO Data Request Interrupt enable (number of data entries in the RXFIFO is greater than RX_THRESHOLD). */
#define CHIP_REGFLD_ECSPI_INTREG_RREN               (0x00000001UL <<  3U) /* (RW) RXFIFO Ready Interrupt enable */
#define CHIP_REGFLD_ECSPI_INTREG_TFEN               (0x00000001UL <<  2U) /* (RW) TXFIFO Full Interrupt enable */
#define CHIP_REGFLD_ECSPI_INTREG_TDREN              (0x00000001UL <<  1U) /* (RW) TXFIFO Data Request Interrupt enable (valid data entries in TXFIFO is greater than TX_THRESHOLD) */
#define CHIP_REGFLD_ECSPI_INTREG_TEEN               (0x00000001UL <<  0U) /* (RW) TXFIFO Empty Interrupt enable */
#define CHIP_REGMSK_ECSPI_INTREG_RESERVED           (0xFFFFFF00UL)


/*********** DMA Control Register (ECSPI_DMAREG) ******************************/
#define CHIP_REGFLD_ECSPI_DMAREG_RXTDEN             (0x00000001UL << 31U) /* (RW) RXFIFO TAIL DMA Request/Interrupt Enable */
#define CHIP_REGFLD_ECSPI_DMAREG_RX_DMA_LEN         (0x0000003FUL << 24U) /* (RW) Burst length of a DMA operation if RXTDEN = 1 */
#define CHIP_REGFLD_ECSPI_DMAREG_RXDEN              (0x00000001UL << 23U) /* (RW) RXFIFO DMA Request Enable */
#define CHIP_REGFLD_ECSPI_DMAREG_RX_THRESHOLD       (0x0000003FUL << 16U) /* (RW) FIFO threshold that triggers a RX DMA/INT request. */
#define CHIP_REGFLD_ECSPI_DMAREG_TEDEN              (0x00000001UL <<  7U) /* (RW) TXFIFO Empty DMA Request Enable */
#define CHIP_REGFLD_ECSPI_DMAREG_TX_THRESHOLD       (0x0000003FUL <<  0U) /* (RW) e FIFO threshold that triggers a TX DMA/INT request. */
#define CHIP_REGMSK_ECSPI_DMAREG_RESERVED           (0x4040FF40UL)


/*********** Status Register (ECSPIx_STATREG) *********************************/
#define CHIP_REGFLD_ECSPI_STATREG_TC                (0x00000001UL <<  7U) /* (RW1C) Transfer Completed Status */
#define CHIP_REGFLD_ECSPI_STATREG_RO                (0x00000001UL <<  6U) /* (RW1C) RXFIFO Overflow */
#define CHIP_REGFLD_ECSPI_STATREG_RF                (0x00000001UL <<  5U) /* (RW1C) RXFIFO Full. */
#define CHIP_REGFLD_ECSPI_STATREG_RDR               (0x00000001UL <<  4U) /* (RW1C) RXFIFO Data Request (number of data entries in the RXFIFO is greater than RX_THRESHOLD). */
#define CHIP_REGFLD_ECSPI_STATREG_RR                (0x00000001UL <<  3U) /* (RW1C) RXFIFO Ready */
#define CHIP_REGFLD_ECSPI_STATREG_TF                (0x00000001UL <<  2U) /* (RW1C) TXFIFO Full */
#define CHIP_REGFLD_ECSPI_STATREG_TDR               (0x00000001UL <<  1U) /* (RW1C) TXFIFO Data Request (valid data entries in TXFIFO is greater than TX_THRESHOLD) */
#define CHIP_REGFLD_ECSPI_STATREG_TE                (0x00000001UL <<  0U) /* (RW1C) TXFIFO Empty */
#define CHIP_REGMSK_ECSPI_STATREG_RESERVED          (0xFFFFFF00UL)

/*********** Sample Period Control Register (ECSPI_PERIODREG) *****************/
#define CHIP_REGFLD_ECSPI_PERIODREG_CSD_CTL         (0x0000003FUL << 16U) /* (RW) Chip Select Delay Control (CS active to first bit) (0-63) */
#define CHIP_REGFLD_ECSPI_PERIODREG_CSRC            (0x00000001UL << 15U) /* (RW) Clock Source Control (1 - low freq 32k) */
#define CHIP_REGFLD_ECSPI_PERIODREG_SAMPLE_PERIOD   (0x00007FFFUL <<  0U) /* (RW) Sample Period Control (wait states between CS) */
#define CHIP_REGMSK_ECSPI_PERIODREG_RESERVED        (0xFFC00000UL)

#define CHIP_REGFLDVAL_ECSPI_PERIODREG_CSRC_SCLK    0 /* SPI Clock (SCLK) */
#define CHIP_REGFLDVAL_ECSPI_PERIODREG_CSRC_32K     1 /* Low-Frequency Reference Clock (32.768 KHz) */

/*********** Test Control Register (ECSPI_TESTREG) ****************************/
#define CHIP_REGFLD_ECSPI_TESTREG_LBC               (0x00000001UL << 31U) /* (RW) Loop Back Control */
#define CHIP_REGFLD_ECSPI_TESTREG_RXCNT             (0x0000007FUL <<  8U) /* (RW) RXFIFO Counter.*/
#define CHIP_REGFLD_ECSPI_TESTREG_TXCNT             (0x0000007FUL <<  0U) /* (RW) TXFIFO Counter.*/
#define CHIP_REGMSK_ECSPI_TESTREG_RESERVED          (0x7FFF8080UL)


#endif // REGS_ECSPI_H
