#ifndef CHIP_PER_IDS_H
#define CHIP_PER_IDS_H

#define CHIP_PER_ID_DMA1        0 /* clk only */
#define CHIP_PER_ID_DMA2        1 /* clk only */
#define CHIP_PER_ID_SRAM        2 /* clk only */
#define CHIP_PER_ID_FLASH       3 /* clk only */
#define CHIP_PER_ID_CRC         4 /* clk only */
#define CHIP_PER_ID_XMC         5 /* clk only */

#define CHIP_PER_ID_AFIO        6
#define CHIP_PER_ID_GPIOA       7
#define CHIP_PER_ID_GPIOB       8
#define CHIP_PER_ID_GPIOC       9
#define CHIP_PER_ID_GPIOD       10
#define CHIP_PER_ID_GPIOE       11
#define CHIP_PER_ID_PWR         12
#define CHIP_PER_ID_BKP         13

#define CHIP_PER_ID_WWDG        14
#define CHIP_PER_ID_TMR1        15
#define CHIP_PER_ID_TMR2        16
#define CHIP_PER_ID_TMR3        17
#define CHIP_PER_ID_TMR4        18
#define CHIP_PER_ID_TMR5        19
#define CHIP_PER_ID_TMR6        20
#define CHIP_PER_ID_TMR7        21
#define CHIP_PER_ID_TMR8        22
#define CHIP_PER_ID_TMR9        23
#define CHIP_PER_ID_TMR10       24
#define CHIP_PER_ID_TMR11       25
#define CHIP_PER_ID_TMR12       26
#define CHIP_PER_ID_TMR13       27
#define CHIP_PER_ID_TMR14       28

#define CHIP_PER_ID_USART1      29
#define CHIP_PER_ID_USART2      30
#define CHIP_PER_ID_USART3      31
#define CHIP_PER_ID_UART4       32
#define CHIP_PER_ID_UART5       33
#define CHIP_PER_ID_USART6      34
#define CHIP_PER_ID_UART7       35
#define CHIP_PER_ID_UART8       36

#define CHIP_PER_ID_SPI1        37
#define CHIP_PER_ID_SPI2        38
#define CHIP_PER_ID_SPI3        39
#define CHIP_PER_ID_SPI4        40

#define CHIP_PER_ID_I2S1        CHIP_PER_ID_SPI1
#define CHIP_PER_ID_I2S2        CHIP_PER_ID_SPI2
#define CHIP_PER_ID_I2S3        CHIP_PER_ID_SPI3
#define CHIP_PER_ID_I2S4        CHIP_PER_ID_SPI4

#define CHIP_PER_ID_I2C1        41
#define CHIP_PER_ID_I2C2        42
#define CHIP_PER_ID_I2C3        43

#define CHIP_PER_ID_USB         44
#define CHIP_PER_ID_CAN         45

#define CHIP_PER_ID_ADC         46

#define CHIP_PER_ID_WKUP        47 /* pins only */
#define CHIP_PER_ID_CLKOUT      48 /* pins only */

#endif // CHIP_PER_IDS_H
