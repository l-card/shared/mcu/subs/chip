#ifndef CHIP_WDT_H
#define CHIP_WDT_H


#define CHIP_WDT_REGS       CHIP_REGS_IWDG
#define CHIP_WDT_CLK_FREQ   CHIP_CLK_IWDG_FREQ

#define CHIP_WDT_OPTBIT_SW_MODE                 CHIP_REGFLD_FLASH_OPTR_IWDG_SW
#define CHIP_WDT_OPTBIT_ON_STOP_EN              CHIP_REGFLD_FLASH_OPTR_IWDG_STOP
#define CHIP_WDT_OPTBIT_ON_STANDBY_EN           CHIP_REGFLD_FLASH_OPTR_IWDG_STDBY


#define CHIP_WDT_GET_OPTBITS()                  (CHIP_REGS_FLASH->OPTR)
#define CHIP_WDT_SET_OPTBITS(val)               (CHIP_REGS_FLASH->OPTR = val)


#include "stm32_iwdg.h"

#endif // CHIP_WDT_H
