#ifndef CHIP_HSEM_H
#define CHIP_HSEM_H

#include "lcspec.h"
#include <stdbool.h>

#ifndef CHIP_HSEM_CLR_KEY_VALUE
    #define CHIP_HSEM_CLR_KEY_VALUE 0x5AA7
#endif

/*******************************************************************************
 * 2-stage функции работы с семофором. позволяют синхронизовать не только
 * ядра, но и процессы на одном ядре (принимают доп. параметр procid),
 * но для попытки захвата требуют двух операций - чтение и запись
 ******************************************************************************/

/* попытка захвата семафора заданным процессом */
static LINLINE bool chip_hsem_process_try_acquire(unsigned semid, unsigned procid) {
    CHIP_REGS_HSEM->R[semid] = (CHIP_REGFLD_HSEM_R_LOCK | LBITFIELD_SET(CHIP_REGFLD_HSEM_R_PROCID, procid));
    return CHIP_REGS_HSEM->R[semid] ==
        (CHIP_REGFLD_HSEM_R_LOCK | LBITFIELD_SET(CHIP_REGFLD_HSEM_R_PROCID, procid)
            | LBITFIELD_SET(CHIP_REGFLD_HSEM_R_COREID, CHIP_HSEM_CUR_COREID));
}


/* захват семофора до успешного завершения */
static LINLINE void chip_hsem_process_acquire(unsigned semid, unsigned procid) {
    while (!chip_hsem_process_try_acquire(semid, procid)) {
        continue;
    };
    chip_dmb();
}

/* освобождение захваченного семофора */
static LINLINE void chip_hsem_process_release(unsigned semid, unsigned procid) {
    chip_dmb();
    CHIP_REGS_HSEM->R[semid] =
        LBITFIELD_SET(CHIP_REGFLD_HSEM_RLR_PROCID, procid)
        | LBITFIELD_SET(CHIP_REGFLD_HSEM_RLR_COREID, CHIP_HSEM_CUR_COREID);
}


/*******************************************************************************
 * 1-stage функции работы с семофором. попытка захвата и проверка выполняются
 * одним чтением, но синхрозация только ядер (procid всегда 0)
 ******************************************************************************/
static LINLINE bool chip_hsem_try_acquire(unsigned semid) {
    return CHIP_REGS_HSEM->RLR[semid] ==
        (CHIP_REGFLD_HSEM_RLR_LOCK | LBITFIELD_SET(CHIP_REGFLD_HSEM_RLR_PROCID, 0)
        | LBITFIELD_SET(CHIP_REGFLD_HSEM_RLR_COREID, CHIP_HSEM_CUR_COREID));
}

/* захват семофора до успешного завершения */
static LINLINE void chip_hsem_acquire(unsigned semid) {
    while (!chip_hsem_try_acquire(semid)) {
        continue;
    };
    chip_dmb();
}

static LINLINE void chip_hsem_release(unsigned semid) {
    chip_dmb();
    CHIP_REGS_HSEM->R[semid] =
        LBITFIELD_SET(CHIP_REGFLD_HSEM_RLR_PROCID, 0)
        | LBITFIELD_SET(CHIP_REGFLD_HSEM_RLR_COREID, CHIP_HSEM_CUR_COREID);
}




/******************************* управление прерываниями **********************/
static LINLINE void chip_hsem_irq_enable(unsigned semid) {
    CHIP_REGS_HSEM->CUR_CORE.IER |= (1UL << (semid));
}

static LINLINE void chip_hsem_irq_disable(unsigned semid) {
    CHIP_REGS_HSEM->CUR_CORE.IER &= ~(1UL << (semid));
}

static LINLINE void chip_hsem_irq_clear(unsigned semid) {
    CHIP_REGS_HSEM->CUR_CORE.ICR = (1UL << (semid));
}

static LINLINE bool chip_hsem_irq_is_set(unsigned semid) {
    return !!(CHIP_REGS_HSEM->CUR_CORE.MISR & (1UL << (semid)));
}


/* управление сбросом семофоров, захваченных другим ядром */

/* сброс всех семофоров, захваченных указанным ядром (значение из CHIP_HSEM_COREID_xxx ) */
static LINLINE void chip_hsem_release_all_for_core(unsigned coreid) {
    CHIP_REGS_HSEM->CR = LBITFIELD_SET(CHIP_REGFLD_HSEM_CR_KEY, CHIP_HSEM_CLR_KEY_VALUE)
                         |  LBITFIELD_SET(CHIP_REGFLD_HSEM_CR_COREID, coreid);
}

#if CHIP_DEV_SUPPORT_CORE_CM4
/* сброс всех семофоров, захваченных удаленным ядром */
static LINLINE void chip_hsem_release_all_rem_core(void) {
    chip_hsem_release_all_for_core(CHIP_HSEM_REM_COREID);
}
#endif



static LINLINE void chip_hsem_init() {
    CHIP_REGS_HSEM->KEYR = LBITFIELD_SET(CHIP_REGFLD_HSEM_KEYR_KEY, CHIP_HSEM_CLR_KEY_VALUE);
}

#endif // CHIP_HSEM_H
