#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_pkg_types.h"

#define CHIP_DEV_V84XXX

#define CHIP_DEV_SUPPORT_EXMC           1
#define CHIP_DEV_SUPPORT_TIM9           1
#define CHIP_DEV_SUPPORT_TIM10          1
#define CHIP_DEV_SUPPORT_TIM11          1
#define CHIP_DEV_SUPPORT_TIM12          1
#define CHIP_DEV_SUPPORT_TIM13          1
#define CHIP_DEV_SUPPORT_TIM14          1
#define CHIP_DEV_SUPPORT_TRACE          1

#define CHIP_DEV_TIM_GP_CNT             8
#define CHIP_DEV_TIM_GP32_CNT           2
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          2
#define CHIP_DEV_SPI_CNT                ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) ? 3 : 4)
#define CHIP_DEV_I2S_CNT                ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) ? 3 : 4)
#define CHIP_DEV_I2S_FD_CNT             0 /* ? check */
#define CHIP_DEV_I2C_CNT                ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) ? 1 : 3)
#define CHIP_DEV_UART_CNT               ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) ? 5 : 8)
#define CHIP_DEV_CAN_CNT                ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) ? 0 : 1)
#define CHIP_DEV_USB_CNT                1
#define CHIP_DEV_SDIO_CNT               0 /* ? check */
#define CHIP_DEV_ETH_CNT                0
#define CHIP_DEV_ADC_CNT                1
#define CHIP_DEV_DAC_CNT                0
#define CHIP_DEV_SPIM_CNT               ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 128) ? 1 : 0)
#define CHIP_DEV_QSPI_CNT               0
#define CHIP_DEV_DVP_CNT                0


#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(180)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
