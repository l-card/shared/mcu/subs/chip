#ifndef CHIP_LPC11U6X_REGS_IOCON_H
#define CHIP_LPC11U6X_REGS_IOCON_H

#include <stdint.h>
/**
 * @brief LPC11u6x IO Configuration Unit register block structure
 */
typedef struct {            /*!< LPC11U6X IOCON Structure */
    __IO uint32_t  PIO0[24];
    __IO uint32_t  PIO1[32];
    __I  uint32_t  reserved[4];
    __IO uint32_t  PIO2A[2];/* PIO2_0/1 only */
    __I  uint32_t  reserved1[1];
    __IO uint32_t  PIO2B[22];    /* PIO2_2 to PIO_2_23 */
} CHIP_REGS_IOCON_T;

#define CHIP_REGS_IOCON         ((CHIP_REGS_IOCON_T *) CHIP_MEMRGN_ADDR_PERIPH_IOCON)

/*********** Pin control registers (IOCON_PIO) *********************************/
#define CHIP_REGFLD_IOCON_PIO_FUNC                  (0x00000007UL <<  0U)   /* (RW) Select pin function */
#define CHIP_REGFLD_IOCON_PIO_MODE                  (0x00000003UL <<  3U)   /* (RW) (!OD) Select function mode (pull-up/pull-down control) */
#define CHIP_REGFLD_IOCON_PIO_HYS                   (0x00000001UL <<  5U)   /* (RW) (!OD) Hysteresis enable */
#define CHIP_REGFLD_IOCON_PIO_INV                   (0x00000001UL <<  6U)   /* (RW) (!OD) Inverted input */
#define CHIP_REGFLD_IOCON_PIO_ADMODE                (0x00000001UL <<  7U)   /* (RW) (ANA) Analog mode disable */
#define CHIP_REGFLD_IOCON_PIO_FILTR                 (0x00000001UL <<  8U)   /* (RW) (ANA) Glitch filter disable */
#define CHIP_REGFLD_IOCON_PIO_I2CMODE               (0x00000003UL <<  8U)   /* (RW) (OD)  I2C mode */
#define CHIP_REGFLD_IOCON_PIO_OD                    (0x00000001UL << 10U)   /* (RW) (!OD) Open-drain mode */
#define CHIP_REGFLD_IOCON_PIO_SMODE                 (0x00000003UL << 11U)   /* (RW) Digital filter sample mode */
#define CHIP_REGFLD_IOCON_PIO_CLKDIV                (0x00000007UL << 13U)   /* (RW) Select peripheral clock divider for input filter sampling clock */


#define CHIP_REGFLDVAL_IOCON_PIO_MODE_NO_PULL       0 /* inactive (no pull-down/pull-up resistor enabled) */
#define CHIP_REGFLDVAL_IOCON_PIO_MODE_PD            1 /* pull-down */
#define CHIP_REGFLDVAL_IOCON_PIO_MODE_PU            2 /* pull-up */
#define CHIP_REGFLDVAL_IOCON_PIO_MODE_REPEATER      3 /* repeater */

#define CHIP_REGFLDVAL_IOCON_PIO_SMODE_BYPASS       0 /* bypass input filter */
#define CHIP_REGFLDVAL_IOCON_PIO_SMODE_CLK_1        1 /* 1 clock cycle */
#define CHIP_REGFLDVAL_IOCON_PIO_SMODE_CLK_2        2 /* 2 clock cycle */
#define CHIP_REGFLDVAL_IOCON_PIO_SMODE_CLK_3        3 /* 3 clock cycle */

#define CHIP_REGFLDVAL_IOCON_PIO_I2CMODE_STD_I2C    0 /* Standard mode/ Fast-mode I2C */
#define CHIP_REGFLDVAL_IOCON_PIO_I2CMODE_STD_GPIO   1 /* Standard GPIO functionality. Requires external pull-up for GPIO output function */
#define CHIP_REGFLDVAL_IOCON_PIO_I2CMODE_I2C_PLUS   2 /* Fast-mode Plus I2C */

#endif /* CHIP_LPC11U6X_REGS_IOCON_H */
