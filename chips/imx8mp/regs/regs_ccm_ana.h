#ifndef REGS_CCM_ANA_H
#define REGS_CCM_ANA_H

#include "chip_ioreg_defs.h"

#define CHIP_PLL_NUM_AUDIO1             0
#define CHIP_PLL_NUM_AUDIO2             1
#define CHIP_PLL_NUM_VIDEO1             2
#define CHIP_PLL_NUM_DRAM               4
#define CHIP_PLL_NUM_GPU                4
#define CHIP_PLL_NUM_VPU                5
#define CHIP_PLL_NUM_ARM                6
#define CHIP_PLL_NUM_SYS1               7
#define CHIP_PLL_NUM_SYS2               8
#define CHIP_PLL_NUM_SYS3               9


typedef struct {
    __IO uint32_t GEN_CTRL;    /* PLL General Function Control Register */
    __IO uint32_t FDIV_CTRL0;  /* PLL Divide and Fraction Data Control 0 */
    __IO uint32_t FDIV_CTRL1;  /* PLL Divide and Fraction Data Control 1 */
    __IO uint32_t SSCG_CTRL;   /* PLL SSCG Control Register */
    __IO uint32_t MNIT_CTRL;   /* PLL Monitoring Control Register */
} CHIP_REGS_CCM_PLL_EX_T;

typedef struct {
    __IO uint32_t GEN_CTRL;    /* PLL General Function Control Register */
    __IO uint32_t FDIV_CTRL0;  /* PLL Divide and Fraction Data Control 0 */
    __IO uint32_t LOCKD;       /* PLL Lock Detector Control Register */
    __IO uint32_t MNIT_CTRL;   /* PLL Monitoring Control Register */
} CHIP_REGS_CCM_PLL_T;

typedef struct {
    __IO uint32_t GEN_CTRL;    /* General Function Control Register */
    __IO uint32_t FDIV_CTRL0;  /* Divide and Fraction Data Control 0 */
    __IO uint32_t LOCKD;       /* PLL Lock Detector Control Register */
    uint32_t RES[24];
    __IO uint32_t MNIT_CTRL;   /* PLL Monitoring Control Register */
} CHIP_REGS_CCM_PLL_RES_T;


typedef struct {
    CHIP_REGS_CCM_PLL_EX_T  PLL_AUDIO1;         /* AUDIO_PLL1,                  Address offset: 0x0000-0x0010 */
    CHIP_REGS_CCM_PLL_EX_T  PLL_AUDIO2;         /* AUDIO_PLL2,                  Address offset: 0x0014-0x0024 */
    CHIP_REGS_CCM_PLL_EX_T  PLL_VIDEO1;         /* VIDEO_PLL1,                  Address offset: 0x0028-0x0038 */
    CHIP_REGS_CCM_PLL_EX_T  RESERVED0;
    CHIP_REGS_CCM_PLL_EX_T  PLL_DRAM;           /* DRAM_PLL,                    Address offset: 0x0050-0x0060 */
    CHIP_REGS_CCM_PLL_T     PLL_GPU;            /* GPU_PLL,                     Address offset: 0x0064-0x0070 */
    CHIP_REGS_CCM_PLL_T     PLL_VPU;            /* VPU_PLL,                     Address offset: 0x0074-0x0080 */
    CHIP_REGS_CCM_PLL_T     PLL_ARM;            /* ARM_PLL,                     Address offset: 0x0084-0x0090 */
    CHIP_REGS_CCM_PLL_RES_T PLL_SYS1;           /* SYS_PLL1,                    Address offset: 0x0094-0x0100 */
    CHIP_REGS_CCM_PLL_T     PLL_SYS2;           /* SYS_PLL2,                    Address offset: 0x0104-0x0110 */
    CHIP_REGS_CCM_PLL_T     PLL_SYS3;           /* SYS_PLL3,                    Address offset: 0x0114-0x0120 */
    __IO uint32_t           OSC_MISC_CFG;       /* Osc Misc Configuration Register,                      Address offset: 0x0124 */
    __IO uint32_t           ANAMIX_MNIT_CTL;    /* PLL Clock Output for Test Enable and Select Register, Address offset: 0x0128 */
    uint32_t                RESERED1[437];
    __I  uint32_t           DIGPROG;            /* DIGPROG Register,                                     Address offset: 0x0800 */
} CHIP_REGS_CCM_ANA_T;


#define CHIP_REGS_CCM_ANA        ((CHIP_REGS_CCM_ANA_T *)CHIP_MEMRGN_ADDR_PERIPH_ANA_PLL)

/*********** EX PLL General Function Control Register (CCM_ANA_PLLEX_GEN_CTRL) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_REF_CLK_SEL      (0x00000003UL <<  0U)  /* (RW) PLL reference clock select (24M or PAD) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_PAD_CLK_SEL      (0x00000003UL <<  2U)  /* (RW) PAD clock select */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_BYPASS           (0x00000001UL <<  4U)  /* (RW) PLL output clock bypass */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_RST_OVRD         (0x00000001UL <<  8U)  /* (RW) PLL reset overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_RST              (0x00000001UL <<  9U)  /* (RW) PLL reset (active low) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_CLKE_OVRD        (0x00000001UL << 12U)  /* (RW) Override the PLL_CLKE, clock gating enable signal from CCM */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_CLKE             (0x00000001UL << 13U)  /* (RW) PLL output clock clock gating enable  */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_EXT_BYPASS       (0x00000001UL << 16U)  /* (RW) PLL analog block bypass, clock output traces to PLL source */
#define CHIP_REGFLD_CCM_ANA_PLLEX_GEN_CTRL_LOCK             (0x00000001UL << 31U)  /* (RO) PLL lock signal */
#define CHIP_REGMSK_CCM_ANA_PLLEX_GEN_CTRL_RESERVED         (0x7FFECCE0UL)


#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_GEN_CTRL_REF_CLK_SEL_24M     0 /* 24M_REF_CLK */
#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_GEN_CTRL_REF_CLK_SEL_PAD     1 /* from PAD_CLK_SEL */

#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_GEN_CTRL_PAD_CLK_SEL_XOR     0 /* CLKIN1 XOR CLKIN2 */
#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_GEN_CTRL_PAD_CLK_SEL_CLKIN1  1 /* CLKIN1 */
#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_GEN_CTRL_PAD_CLK_SEL_CLKIN2  2 /* CLKIN2 */

/*********** EX PLL Divide and Fraction Data Control 0 (CCM_ANA_PLLEX_FDIV_CTRL0) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_FDIV_CTRL0_POST_DIV       (0x00000007UL <<  0U)  /* (RW) Value of the post-divider */
#define CHIP_REGFLD_CCM_ANA_PLLEX_FDIV_CTRL0_PRE_DIV        (0x0000003FUL <<  4U)  /* (RW) Value of the pre-divider */
#define CHIP_REGFLD_CCM_ANA_PLLEX_FDIV_CTRL0_MAIN_DIV       (0x000003FFUL << 12U)  /* (RW) Value of the main-divider */
#define CHIP_REGMSK_CCM_ANA_PLLEX_FDIV_CTRL0_RESERVED       (0xFFC00C08UL)

/*********** EX PLL Divide and Fraction Data Control 1 (CCM_ANA_PLLEX_FDIV_CTRL1) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_FDIV_CTRL1_DSM            (0x0000FFFFUL <<  0U)  /* (RW) Value of the DSM */
#define CHIP_REGMSK_CCM_ANA_PLLEX_FDIV_CTRL1_RESERVED       (0xFFFF0000UL)

/*********** EX PLL SSCG Control Register (CCM_ANA_PLLEX_SSCG_CTRL) ***********/
#define CHIP_REGFLD_CCM_ANA_PLLEX_SSCG_CTRL_SEL_PF          (0x00000003UL <<  0U)  /* (RW) Value of modulation method control */
#define CHIP_REGFLD_CCM_ANA_PLLEX_SSCG_CTRL_MRAT_CTL        (0x0000003FUL <<  4U)  /* (RW) Value of modulation rate control. MR = mfr x mrr /m /(2^6) x 100 [%] */
#define CHIP_REGFLD_CCM_ANA_PLLEX_SSCG_CTRL_MFREQ_CTL       (0x000000FFUL << 12U)  /* (RW) Value of modulation frequency control. MF = FFIN/p/mfr/(2^5) Hz */
#define CHIP_REGFLD_CCM_ANA_PLLEX_SSCG_CTRL_SSCG_EN         (0x00000001UL << 31U)  /* (RW) Enable Spread Spectrum Mode */
#define CHIP_REGMSK_CCM_ANA_PLLEX_SSCG_CTRL_RESERVED        (0x7FF00C0CUL)

#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_SSCG_CTRL_SEL_PF_DOWN    0 /* Down spread */
#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_SSCG_CTRL_SEL_PF_UP      1 /* Up spread */
#define CHIP_REGFLDVAL_CCM_ANA_PLLEX_SSCG_CTRL_SEL_PF_CENTER  2 /* Center spread */

/*********** EX PLL Monitoring Control Register (CCM_ANA_PLLEX_MNIT_CTRL) *****/
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_ICP             (0x00000007UL <<  0U)  /* (RW) Controls the charge-pump current */
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_AFC_EN          (0x00000001UL <<  3U)  /* (RW) manual VCO is calibrated manually by EXTAFC */
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_EXTAFC          (0x0000001FUL <<  4U)  /* (RW) Monitoring pin. Test of VCO range for AFC_EN=0*/
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_FEED_EN         (0x00000001UL << 14U)  /* (RW) FEED_OUT enable pin */
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_FSEL            (0x00000001UL << 15U)  /* (RW) Monitoring frequency select pin (0 - FREF, 1 - FEED) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_AFCINIT_SEL     (0x00000001UL << 17U)  /* (RW) AFC initial delay select pin (0 - nominal delay, 1 - 2 nominal delay) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_PBIAS_CTRL_EN   (0x00000001UL << 18U)  /* (RW) PBIAS voltage pull-down enable pin */
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_PBIAS_CTRL      (0x00000001UL << 19U)  /* (RW) PBIAS pull-down initial voltage control pin (0 - 0.50*VDD, 1 - 0.67*VDD) */
#define CHIP_REGFLD_CCM_ANA_PLLEX_MNIT_CTRL_AFC_SEL         (0x00000001UL << 20U)  /* (RW) AFC Mode select */
#define CHIP_REGMSK_CCM_ANA_PLLEX_MNIT_CTRL_RESERVED        (0xFFE13E00UL)


/*********** PLL General Function Control Register (CCM_ANA_PLL_GEN_CTRL) *****/
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_REF_CLK_SEL        (0x00000003UL <<  0U)  /* (RW) PLL reference clock select (24M or PAD) */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_PAD_CLK_SEL        (0x00000003UL <<  2U)  /* (RW) PAD clock select */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_BYPASS             (0x00000001UL <<  4U)  /* (RW) PLL output clock bypass */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_RST_OVRD           (0x00000001UL <<  8U)  /* (RW) PLL reset overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_RST                (0x00000001UL <<  9U)  /* (RW) PLL reset (active low) */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_CLKE_OVRD          (0x00000001UL << 10U)  /* (RW) Override the PLL_CLKE, clock gating enable signal from CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_CLKE               (0x00000001UL << 11U)  /* (RW) PLL output clock clock gating enable  */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV2_CLKE_OVRD     (0x00000001UL << 12U)  /* (RW) SYSPLL1/2 clock divided by 2 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV2_CLKE          (0x00000001UL << 13U)  /* (RW) SYSPLL1/2 clock divided by 2 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV3_CLKE_OVRD     (0x00000001UL << 14U)  /* (RW) SYSPLL1/2 clock divided by 3 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV3_CLKE          (0x00000001UL << 15U)  /* (RW) SYSPLL1/2 clock divided by 3 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV4_CLKE_OVRD     (0x00000001UL << 16U)  /* (RW) SYSPLL1/2 clock divided by 4 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV4_CLKE          (0x00000001UL << 17U)  /* (RW) SYSPLL1/2 clock divided by 4 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV5_CLKE_OVRD     (0x00000001UL << 18U)  /* (RW) SYSPLL1/2 clock divided by 5 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV5_CLKE          (0x00000001UL << 19U)  /* (RW) SYSPLL1/2 clock divided by 5 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV6_CLKE_OVRD     (0x00000001UL << 20U)  /* (RW) SYSPLL1/2 clock divided by 6 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV6_CLKE          (0x00000001UL << 21U)  /* (RW) SYSPLL1/2 clock divided by 6 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV8_CLKE_OVRD     (0x00000001UL << 22U)  /* (RW) SYSPLL1/2 clock divided by 8 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV8_CLKE          (0x00000001UL << 23U)  /* (RW) SYSPLL1/2 clock divided by 8 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV10_CLKE_OVRD    (0x00000001UL << 24U)  /* (RW) SYSPLL1/2 clock divided by 10 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV10_CLKE         (0x00000001UL << 25U)  /* (RW) SYSPLL1/2 clock divided by 10 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV20_CLKE_OVRD    (0x00000001UL << 26U)  /* (RW) SYSPLL1/2 clock divided by 20 output gating enable overrided by CCM */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_DIV20_CLKE         (0x00000001UL << 27U)  /* (RW) SYSPLL1/2 clock divided by 20 output gating enable */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_EXT_BYPASS         (0x00000001UL << 28U)  /* (RW) PLL analog block bypass, clock output traces to PLL source */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_LOCK_SEL           (0x00000001UL << 29U)  /* (RW) PLL lock select (0 - Using PLL maximum lock time, 1 - Using PLL output lock) */
#define CHIP_REGFLD_CCM_ANA_PLL_GEN_CTRL_LOCK               (0x00000001UL << 31U)  /* (RO) PLL lock signal */
#define CHIP_REGMSK_CCM_ANA_PLL_GEN_CTRL_RESERVED           (0x400000E0UL)

#define CHIP_REGFLDVAL_CCM_ANA_PLL_GEN_CTRL_REF_CLK_SEL_24M     0 /* 24M_REF_CLK */
#define CHIP_REGFLDVAL_CCM_ANA_PLL_GEN_CTRL_REF_CLK_SEL_PAD     1 /* from PAD_CLK_SEL */

#define CHIP_REGFLDVAL_CCM_ANA_PLL_GEN_CTRL_PAD_CLK_SEL_XOR     0 /* CLKIN1 XOR CLKIN2 */
#define CHIP_REGFLDVAL_CCM_ANA_PLL_GEN_CTRL_PAD_CLK_SEL_CLKIN1  1 /* CLKIN1 */
#define CHIP_REGFLDVAL_CCM_ANA_PLL_GEN_CTRL_PAD_CLK_SEL_CLKIN2  2 /* CLKIN2 */

/*********** PLL Divide and Fraction Data Control 0 (CCM_ANA_PLL_FDIV_CTRL0) **/
#define CHIP_REGFLD_CCM_ANA_PLL_FDIV_CTRL0_POST_DIV         (0x00000007UL <<  0U)  /* (RW) Value of the post-divider */
#define CHIP_REGFLD_CCM_ANA_PLL_FDIV_CTRL0_PRE_DIV          (0x0000003FUL <<  4U)  /* (RW) Value of the pre-divider */
#define CHIP_REGFLD_CCM_ANA_PLL_FDIV_CTRL0_MAIN_DIV         (0x000003FFUL << 12U)  /* (RW) Value of the main-divider */
#define CHIP_REGMSK_CCM_ANA_PLL_FDIV_CTRL0_RESERVED         (0xFFC00C08UL)

/*********** PLL Lock Detector Control Register (CCM_ANA_PLL_LOCKD_CTRL) ******/
#define CHIP_REGFLD_CCM_ANA_PLL_LOCKD_CTRL_LOCK_CON_IN      (0x00000003UL <<  0U)  /* (RW) Lock detector setting of the detection resolution */
#define CHIP_REGFLD_CCM_ANA_PLL_LOCKD_CTRL_LOCK_CON_OUT     (0x00000003UL <<  2U)  /* (RW) Lock detector setting of the output margin */
#define CHIP_REGFLD_CCM_ANA_PLL_LOCKD_CTRL_LOCK_CON_DLY     (0x00000003UL <<  4U)  /* (RW) Lock detector setting of the input margin */
#define CHIP_REGMSK_CCM_ANA_PLL_LOCKD_CTRL_RESERVED         (0xFFFFFFC0UL)

/*********** PLL Monitoring Control Register (CCM_ANALOG_MNIT_CTRL) ***********/
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_ICP               (0x00000003UL <<  0U)  /* (RW) Controls the charge-pump current */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_AFC_EN            (0x00000001UL <<  2U)  /* (RW) manual VCO is calibrated manually by EXTAFC */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_EXTAFC            (0x0000001FUL <<  3U)  /* (RW) Monitoring pin. Test of VCO range for AFC_EN=0*/
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_FEED_EN           (0x00000001UL << 13U)  /* (RW) FEED_OUT enable pin */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_FSEL              (0x00000001UL << 14U)  /* (RW) Monitoring frequency select pin (0 - FREF, 1 - FEED) */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_AFCINIT_SEL       (0x00000001UL << 16U)  /* (RW) AFC initial delay select pin (0 - nominal delay, 1 - 2 nominal delay) */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_PBIAS_CTRL_EN     (0x00000001UL << 17U)  /* (RW) PBIAS voltage pull-down enable pin */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_PBIAS_CTRL        (0x00000001UL << 18U)  /* (RW) PBIAS pull-down initial voltage control pin (0 - 0.50*VDD, 1 - 0.67*VDD) */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_AFC_SEL           (0x00000001UL << 19U)  /* (RW) AFC Mode select */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_FOUT_MASK         (0x00000001UL << 20U)  /* (RW) Scaler's re-initialization time control pin[3] */
#define CHIP_REGFLD_CCM_ANA_PLL_MNIT_CTRL_LDR_EN            (0x00000001UL << 21U)  /* (RW) Monitoring pin. AFC operation mode select pin */
#define CHIP_REGMSK_CCM_ANA_PLL_MNIT_CTRL_RESERVED          (0xFFC09F00UL)


/*********** Osc Misc Configuration Register (CCM_ANA_OSC_MISC_CFG) ***********/
#define CHIP_REGFLD_CCM_ANA_OSC_MISC_CFG_OSC_32K_SEL        (0x00000001UL <<  0U)  /* (RW) 32KHz OSC input select */
#define CHIP_REGMSK_CCM_ANA_OSC_MISC_CFG_RESERVED           (0xFFFFFFFEUL)

#define CHIP_REGFLDVAL_CCM_ANA_OSC_MISC_CFG_OSC_32K_SEL_24M_DIV 0 /* Divided by 24M clock */
#define CHIP_REGFLDVAL_CCM_ANA_OSC_MISC_CFG_OSC_32K_SEL_OSC     1 /* 32K Oscillator */


/*********** PLL Clock Output for Test Enable and Select Register (CCM_ANA_ANAMIX_MNIT_CTL) */
#define CHIP_REGFLD_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_DIV_VAL(n)   (0x0000000FUL <<  (0U + 16*(n))) /* (RW) CLKOUT1/2 Monitor output divide value */
#define CHIP_REGFLD_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL(n)       (0x0000000FUL <<  (4U + 16*(n))) /* (RW) CLKOUT1/2 Monitor output clock select */
#define CHIP_REGFLD_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_CKE(n)       (0x00000001UL <<  (8U + 16*(n))) /* (RW) CLKOUT1/2 Monitor output enable */
#define CHIP_REGMSK_CCM_ANA_ANAMIX_MNIT_CTL_RESERVED            (0xFE00FE00UL)


#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_AUDIO1    0
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_AUDIO2    1
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_VIDEO1    2
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_HSIO      3
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_MISC_MNIT     4
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_GPU       5
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_VPU       6
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_ARM       7
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_SYS1      8
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_SYS2      9
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_SYS3      10
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_CLKIN1        11
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_CLKIN2        12
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_SYSOSC_24M    13
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_PLL_SAI       14
#define CHIP_REGFLDVAL_CCM_ANA_ANAMIX_MNIT_CTL_CLKOUT_SEL_OSC_32K       15


/*********** DIGPROG Register(CCM_ANA_DIGPROG) ********************************/
#define CHIP_REGFLD_CCM_ANA_DIGPROG_MINOR                       (0x000000FFUL <<  0U)  /* (RO) Bit[7:4] is the base layer revision, Bit[3:0] is the metal layer revision 0x10 stands for Tapeout 1.0 */
#define CHIP_REGFLD_CCM_ANA_DIGPROG_MAJOR_LOWER                 (0x000000FFUL <<  8U)  /* (RO) Bit[7:4] is 0x4, stands for “Quad” Bit[3:0] is 0x3, stands for “Plus” */
#define CHIP_REGFLD_CCM_ANA_DIGPROG_MAJOR_UPPER                 (0x000000FFUL << 16U)  /* (RO) Bit[7:4] is 0x8, stands for “i.MX8” Bit[3:0] is 0x2, stands for ”M” */
#define CHIP_REGMSK_CCM_ANA_DIGPROG_RESERVED                    (0xFF000000UL)

#define CHIP_REGFLDVAL_CCM_ANA_DIGPROG_MAJOR_UPPER_IMX8M        0x82
#define CHIP_REGFLDVAL_CCM_ANA_DIGPROG_MAJOR_LOWER_QPLUS        0x43

#endif // REGS_CCM_ANA_H
