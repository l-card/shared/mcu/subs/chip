#ifndef REGS_SYNT_H
#define REGS_SYNT_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t GCR;                         /* 0x0: Global control register */
    __IO uint32_t RLD;                         /* 0x4: Counter reload register */
#if CHIP_DEV_SUPPORT_SYNT_TSTMP
    __IO uint32_t TIMESTAMP_NEW;               /* 0x8: timestamp new value register */
#else
    __I uint32_t RESERVED1;
#endif
    __I  uint32_t CNT;                         /* 0xC: Counter */
#if CHIP_DEV_SUPPORT_SYNT_TSTMP
    __I  uint32_t TIMESTAMP_SAV;               /* 0x10: timestamp trig save value */
    __I  uint32_t TIMESTAMP_CUR;               /* 0x14: timestamp read value */
#else
    __I uint32_t RESERVED2[2];
#endif
    __I  uint8_t  RESERVED0[8];                /* 0x18 - 0x1F: Reserved */
    __IO uint32_t CMP[4];                      /* 0x20 - 0x2C: Comparator */
} CHIP_REGS_SYNT_T;


#define CHIP_REGS_SYNT            ((CHIP_REGS_SYNT_T *)CHIP_MEMRGN_ADDR_SYNT)


/*********** SYNT_GCR - Global control register *******************************/
#define CHIP_REGFLD_SYNT_GCR_CRST                       (0x00000001UL <<  1U) /* (RW) Reset counter */
#define CHIP_REGFLD_SYNT_GCR_CEN                        (0x00000001UL <<  0U) /* (RW) Enable counter */
#define CHIP_REGMSK_SYNT_GCR_RESERVED                   (0xFFFFFFFCUL)



#endif // REGS_SYNT_H
