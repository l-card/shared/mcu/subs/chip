#ifndef CHIP_PIN_DEFS_H
#define CHIP_PIN_DEFS_H

#include "lbitfield.h"

typedef unsigned t_chip_pin_id;

#define CHIP_PIN_MODE_INPUT     0x0
#define CHIP_PIN_MODE_OUTPUT    0x1
#define CHIP_PIN_MODE_AF        0x2
#define CHIP_PIN_MODE_ANALOG    0x3
#define CHIP_PIN_MODE_UNKNOWN   0xF

#define CHIP_PIN_FUNC_ANALOG    0xFE
#define CHIP_PIN_FUNC_GPIO      0xFF


#define CHIP_PIN_CFG_OTYPE          (0x1UL << 0)
#define CHIP_PIN_CFG_OTYPE_VAL(v)   (((v) & 0x1UL) << 0)
#define CHIP_PIN_CFG_OTYPE_PP       CHIP_PIN_CFG_OTYPE_VAL(0) /* push-pull */
#define CHIP_PIN_CFG_OTYPE_OD       CHIP_PIN_CFG_OTYPE_VAL(1) /* open-drain */

#define CHIP_PIN_CFG_OSPEED         (0x3UL << 2)
#define CHIP_PIN_CFG_OSPEED_VAL(v)  (((v) & 0x3UL) << 2)

#ifdef CHIP_PIN_USE_OSPEED_VLOW
#define CHIP_PIN_CFG_OSPEED_VLOW    CHIP_PIN_CFG_OSPEED_VAL(0)
#define CHIP_PIN_CFG_OSPEED_LOW     CHIP_PIN_CFG_OSPEED_VAL(1)
#else
#define CHIP_PIN_CFG_OSPEED_LOW     CHIP_PIN_CFG_OSPEED_VAL(0)
#define CHIP_PIN_CFG_OSPEED_MED     CHIP_PIN_CFG_OSPEED_VAL(1)
#endif
#define CHIP_PIN_CFG_OSPEED_HIGH    CHIP_PIN_CFG_OSPEED_VAL(2)
#ifdef CHIP_PIN_USE_OSPEED_VHIGH
#define CHIP_PIN_CFG_OSPEED_VHIGH   CHIP_PIN_CFG_OSPEED_VAL(3)
#endif




#define CHIP_PIN_CFG_PUPD           (0x3UL << 4)
#define CHIP_PIN_CFG_PUPD_VAL(v)    (((v) & 0x3UL) << 4)
#define CHIP_PIN_CFG_NOPULL         CHIP_PIN_CFG_PUPD_VAL(0)
#define CHIP_PIN_CFG_PULLUP         CHIP_PIN_CFG_PUPD_VAL(1)
#define CHIP_PIN_CFG_PULLDOWN       CHIP_PIN_CFG_PUPD_VAL(2)

#ifndef CHIP_PIN_SPEED_STD
    #ifdef CHIP_PIN_USE_OSPEED_VHIGH
        #define CHIP_PIN_SPEED_STD CHIP_PIN_CFG_OSPEED_VHIGH
    #else
        #define CHIP_PIN_SPEED_STD CHIP_PIN_CFG_OSPEED_HIGH
    #endif
#endif

#ifndef CHIP_PIN_CFG_STD
    #define CHIP_PIN_CFG_STD      (CHIP_PIN_SPEED_STD | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_PP)
#endif
#ifndef CHIP_PIN_CFG_STD_OD
    #define CHIP_PIN_CFG_STD_OD   (CHIP_PIN_SPEED_STD | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_OD)
#endif


#define CHIP_PIN_ID_EX(port, pin, func, mode, cfg) ((((port) & 0xF)  << 28) | \
                                                    (((pin)  & 0xF)  << 24) | \
                                                    (((func) & 0xFF) << 16) | \
                                                    (((mode) & 0xF)  << 8)  | \
                                                    (((cfg)  & 0xFF) << 0))

#define CHIP_PIN_ID(port, pin, func) CHIP_PIN_ID_EX(port, pin, func, \
    (func == CHIP_PIN_FUNC_GPIO) ? CHIP_PIN_MODE_UNKNOWN :  \
    (func == CHIP_PIN_FUNC_ANALOG) ? CHIP_PIN_MODE_ANALOG : CHIP_PIN_MODE_AF, \
    CHIP_PIN_CFG_STD)

#define CHIP_PIN_ID_OD(port, pin, func) CHIP_PIN_ID_EX(port, pin, func, \
    (func == CHIP_PIN_FUNC_GPIO) ? CHIP_PIN_MODE_UNKNOWN :  \
    (func == CHIP_PIN_FUNC_ANALOG) ? CHIP_PIN_MODE_ANALOG : CHIP_PIN_MODE_AF, \
    CHIP_PIN_CFG_STD_OD)

#define CHIP_PIN_ID_GPIO(port, pin)         CHIP_PIN_ID_EX(port, pin, CHIP_PIN_FUNC_GPIO, CHIP_PIN_MODE_UNKNOWN, CHIP_PIN_CFG_STD)
#define CHIP_PIN_ID_AF(port, pin, func)     CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_MODE_AF, CHIP_PIN_CFG_STD)
#define CHIP_PIN_ID_AF_OD(port, pin, func)  CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_MODE_AF, CHIP_PIN_CFG_STD_OD)
#define CHIP_PIN_ID_ANALOG(port, pin)       CHIP_PIN_ID_EX(port, pin, CHIP_PIN_FUNC_ANALOG, CHIP_PIN_MODE_ANALOG, CHIP_PIN_CFG_STD)



#define CHIP_PIN_ID_PORT(id) (((id) >> 28) & 0xF)
#define CHIP_PIN_ID_PIN(id)  (((id) >> 24) & 0xF)
#define CHIP_PIN_ID_FUNC(id) (((id) >> 16) & 0xFF)
#define CHIP_PIN_ID_MODE(id) (((id) >>  8) & 0xF)
#define CHIP_PIN_ID_CFG(id)  (((id) >>  0) & 0xFF)

#define CHIP_PIN_ID_INVALID  0xFFFFFFFF

#define CHIP_PIN_GPIOREG(reg, id)    CHIP_REGS_GPIO(CHIP_PIN_ID_PORT(id))->reg
#define CHIP_PIN_BITMSK(id)          (1UL << CHIP_PIN_ID_PIN(id))
#define CHIP_PIN_2BIT_MSK(id)        (0x3 << (CHIP_PIN_ID_PIN(id) << 1))



/* функции для отдельной настройки различных дополнительных конфигураций пина */
#define CHIP_PIN_CONFIG_OTYPE(id, cfg) \
        LBITFIELD_UPD(CHIP_PIN_GPIOREG(OTYPER,id), CHIP_PIN_BITMSK(id), LBITFIELD_GET(cfg, CHIP_PIN_CFG_OTYPE))
#define CHIP_PIN_CONFIG_OSPEED(id, cfg) \
        LBITFIELD_UPD(CHIP_PIN_GPIOREG(OSPEEDR,id), CHIP_PIN_2BIT_MSK(id), LBITFIELD_GET(cfg, CHIP_PIN_CFG_OSPEED))
#define CHIP_PIN_CONFIG_PUPD(id, cfg) \
        LBITFIELD_UPD(CHIP_PIN_GPIOREG(PUPDR,id), CHIP_PIN_2BIT_MSK(id), LBITFIELD_GET(cfg, CHIP_PIN_CFG_PUPD))


/* функция, настраивающая пин по ID, с явным заданием специальных режимов через объединение по или CHIP_PIN_CFG */
#define CHIP_PIN_CONFIG_EX(id, cfg) do { \
        if ((CHIP_PIN_ID_FUNC(id) != CHIP_PIN_FUNC_GPIO) && (CHIP_PIN_ID_FUNC(id) != CHIP_PIN_FUNC_ANALOG)) { \
            LBITFIELD_UPD(CHIP_PIN_GPIOREG(AFR, id)[CHIP_PIN_ID_PIN(id) & 0x8 ? 1 : 0], \
                              0xFU << ((CHIP_PIN_ID_PIN(id) & 0x7) *4), \
                              CHIP_PIN_ID_FUNC(id)); \
        }  \
        if (CHIP_PIN_ID_MODE(id) != CHIP_PIN_MODE_UNKNOWN) { \
            LBITFIELD_UPD(CHIP_PIN_GPIOREG(MODER,id), \
                          CHIP_PIN_2BIT_MSK(id), \
                          CHIP_PIN_ID_MODE(id)); \
        } \
        CHIP_PIN_CONFIG_OTYPE(id, cfg); \
        CHIP_PIN_CONFIG_OSPEED(id, cfg); \
        CHIP_PIN_CONFIG_PUPD(id, cfg);\
    } while (0)

/* функция, настраивающая пин по ID, использующая спец. настройки по умолчанию */
#define CHIP_PIN_CONFIG(id) do { \
        CHIP_PIN_CONFIG_EX(id, CHIP_PIN_ID_CFG(id)); \
    } while(0)

/* настройка пина на оптимальный режим, если не используется (аналоговый режим для отключения входа/выхода) */
#define CHIP_PIN_CONFIG_DIS(id) do { \
    CHIP_PIN_CONFIG(CHIP_PIN_ID(CHIP_PIN_ID_PORT(id), CHIP_PIN_ID_PIN(id), CHIP_PIN_FUNC_ANALOG)); \
} while(0)




#define CHIP_PIN_SET_DIR_IN(id)       (LBITFIELD_UPD(CHIP_PIN_GPIOREG(MODER,id), \
                                                     CHIP_PIN_2BIT_MSK(id), \
                                                     CHIP_PIN_MODE_INPUT))
#define CHIP_PIN_SET_DIR_OUT(id)      (LBITFIELD_UPD(CHIP_PIN_GPIOREG(MODER,id), \
                                                    CHIP_PIN_2BIT_MSK(id), \
                                                    CHIP_PIN_MODE_OUTPUT))

/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val)        CHIP_PIN_GPIOREG(BSRR,id) = (1 << (((val) ? 0 : 16) + CHIP_PIN_ID_PIN(id)))
/* чтение уровня на ножке (0 или 1) */
#define CHIP_PIN_IN(id)              (!!(CHIP_PIN_GPIOREG(IDR,id) & CHIP_PIN_BITMSK(id)))
/* состояние регистра выхода (независимо от физического уровня) */
#define CHIP_PIN_GET_OUT_VAL(id)     (!!(CHIP_PIN_GPIOREG(ODR,id) & CHIP_PIN_BITMSK(id)))
/* атомарное инвертирование уровня на ножке */
#define CHIP_PIN_TOGGLE(id)          CHIP_PIN_OUT(id, CHIP_PIN_GET_OUT_VAL(id) ? 0 : 1)


/* вспомогательный макрос, который конфигурирует порт на вывод с установленным
   начальным значением */
#define CHIP_PIN_CONFIG_OUT(id, val) do { \
                                        CHIP_PIN_OUT(id, val); \
                                        CHIP_PIN_SET_DIR_OUT(id); \
                                        CHIP_PIN_CONFIG(id); \
                                    } while(0)
/* вспомогательный макрос для настройки пина и конфигурирования его на вход */
#define CHIP_PIN_CONFIG_IN(id) do { \
                                    CHIP_PIN_SET_DIR_IN(id); \
                                    CHIP_PIN_CONFIG(id); \
                                } while(0)

#define CHIP_PIN_CONFIG_OUT_EX(id, cfg, val) do { \
                                        CHIP_PIN_OUT(id, val); \
                                        CHIP_PIN_SET_DIR_OUT(id); \
                                        CHIP_PIN_CONFIG_EX(id, cfg); \
                                    } while(0)

#define CHIP_PIN_CONFIG_IN_EX(id, cfg) do { \
                                        CHIP_PIN_SET_DIR_IN(id); \
                                        CHIP_PIN_CONFIG_EX(id, cfg); \
                                    } while(0)

#endif // CHIP_PIN_DEFS_H
