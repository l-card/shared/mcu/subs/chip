#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H


#define CHIP_CFG_STACK_WORDS_LEN    1024 /* размер стека */

/* ------------------ Внешний высокочастотный источник частоты HSE -----------*/
#define CHIP_CFG_CLK_HSE_FREQ       CHIP_MHZ(8)               /* значение внешней частоты HSE */
#define CHIP_CFG_CLK_HSE_MODE       CHIP_CLK_EXTMODE_OSC      /* режим внешнего источника HSE  */
#define CHIP_CFG_CLK_HSE_CFD_EN     1                         /* разрешение контроля внешней частоты HSE */

/* ------------------ Внутренний высокочастотный источник частоты HSI --------*/
#define CHIP_CFG_CLK_HSI_EN         1            /* разрешение источника частоты HSI */
#define CHIP_CFG_CLK_HSISYS_DIV_EN  0            /* разрешение деления HSI на 6 для получения частоты 8 МГц */

/* ------------------ Внутренний низкочастотный источник частоты HSI ---------*/
#define CHIP_CFG_CLK_LSI_EN         1           /* разрешение источника частоты HSI */
#define CHIP_CFG_WDT_SETUP_EN       1
#define CHIP_CFG_WDT_TIMEOUT_MS     1000



/* ------------------ Настройки PLL ------------------------------------------*/

#define CHIP_CFG_CLK_PLL1_EN        1                     /* разрешение PLL */
#define CHIP_CFG_CLK_HSE_PLLIN_DIV  1                     /* делитель частоты HSE на входе PLL - 1 или 2 (HSI всегда делится на 2) */
#define CHIP_CFG_CLK_PLL1_SRC       CHIP_CLK_HSE          /* источник входной частоты для PLL: HSE или HSI (HSE делится на CHIP_CFG_CLK_HSE_PLLIN_DIV, HSI - на 2) */
#define CHIP_CFG_CLK_PLL1_MUL       9                     /* множитель для результирующей частоты (2..16, для gd 2..32, вместо 15 - 6.5 (задается как 13/2 для возможности обработки препроцессором)) */

/* источник системной частоты */
#define CHIP_CFG_CLK_SYS_SRC        CHIP_CLK_PLL1         /* источник частоты для системного клока: HSI, HSE или PLL1 */


#define CHIP_CFG_CLK_AHB_DIV        1 /* делитель частоты шыны AHB из CLK_SYS (1,2,4,8,16,64,128,256,512) */
#define CHIP_CFG_CLK_APB1_DIV       2 /* делитель частоты APB1 из AHB (1,2,4,8,16) */
#define CHIP_CFG_CLK_APB2_DIV       1 /* делитель частоты APB2 из AHB (1,2,4,8,16) */


#define CHIP_CFG_CLK_MCO_EN         0
#define CHIP_CFG_CLK_MCO_SRC        CHIP_CLK_PLL1         /* источник частоты для выхода MCO: PLL1, HSI, HSE, SYS */

#define CHIP_CFG_CLK_ADC_DIV        8

#define CHIP_CFG_CODE_EFFICIENCY    1 /* только для Gigadevice (работает ли вообще?) */

#define CHIP_CFG_PWR_PVD_EN         1       /* разрешение Programmable Voltage Detector */
#define CHIP_CFG_PWR_PVD_LVL        2900    /* уровень напряжения для PVD в мВ */




/* разрешение клоков периферии при старте */
#define CHIP_CFG_CLK_DMA1_EN        0
#define CHIP_CFG_CLK_DMA2_EN        0
#define CHIP_CFG_CLK_SRAM_EN        1
#define CHIP_CFG_CLK_FLASH_EN       1
#define CHIP_CFG_CLK_CRC_EN         0
#define CHIP_CFG_CLK_EXMC_EN        0

#define CHIP_CFG_CLK_AFIO_EN        1
#define CHIP_CFG_CLK_GPIOA_EN       1
#define CHIP_CFG_CLK_GPIOB_EN       1
#define CHIP_CFG_CLK_GPIOC_EN       1
#define CHIP_CFG_CLK_GPIOD_EN       0
#define CHIP_CFG_CLK_GPIOE_EN       0
#define CHIP_CFG_CLK_GPIOF_EN       0
#define CHIP_CFG_CLK_GPIOG_EN       0
#define CHIP_CFG_CLK_PWR_EN         1
#define CHIP_CFG_CLK_BKP_EN         0

#define CHIP_CFG_CLK_WWDG_EN        0
#define CHIP_CFG_CLK_TIM1_EN        0
#define CHIP_CFG_CLK_TIM2_EN        0
#define CHIP_CFG_CLK_TIM3_EN        0
#define CHIP_CFG_CLK_TIM4_EN        0
#define CHIP_CFG_CLK_TIM5_EN        0
#define CHIP_CFG_CLK_TIM6_EN        0
#define CHIP_CFG_CLK_TIM7_EN        0
#define CHIP_CFG_CLK_TIM8_EN        0
#define CHIP_CFG_CLK_TIM9_EN        0
#define CHIP_CFG_CLK_TIM10_EN       0
#define CHIP_CFG_CLK_TIM11_EN       0
#define CHIP_CFG_CLK_TIM12_EN       0
#define CHIP_CFG_CLK_TIM13_EN       0
#define CHIP_CFG_CLK_TIM14_EN       0

#define CHIP_CFG_CLK_USART1_EN      0
#define CHIP_CFG_CLK_USART2_EN      0
#define CHIP_CFG_CLK_USART3_EN      0
#define CHIP_CFG_CLK_UART4_EN       0
#define CHIP_CFG_CLK_UART5_EN       0
#define CHIP_CFG_CLK_UART6_EN       0
#define CHIP_CFG_CLK_UART7_EN       0
#define CHIP_CFG_CLK_UART8_EN       0

#define CHIP_CFG_CLK_SPI1_EN        0
#define CHIP_CFG_CLK_SPI2_EN        0
#define CHIP_CFG_CLK_SPI3_EN        0

#define CHIP_CFG_CLK_I2C1_EN        0
#define CHIP_CFG_CLK_I2C2_EN        0


#define CHIP_CFG_CLK_USBD_EN        0
#define CHIP_CFG_CLK_USBHD_EN       0
#define CHIP_CFG_CLK_USBHS_EN       0

#define CHIP_CFG_CLK_CAN1_EN        0
#define CHIP_CFG_CLK_CAN2_EN        0

#define CHIP_CFG_CLK_DVP_EN         0
#define CHIP_CFG_CLK_SDIO1_EN       0
#define CHIP_CFG_CLK_ETH_EN         0
#define CHIP_CFG_CLK_BLES_EN        0

#define CHIP_CFG_CLK_ADC1_EN        0
#define CHIP_CFG_CLK_ADC2_EN        0
#define CHIP_CFG_CLK_ADC3_EN        0
#define CHIP_CFG_CLK_DAC_EN         0
#define CHIP_CFG_CLK_RNG_EN         0


/* -------------------- IOREMAP -----------------------------------------------*/
#define CHIP_CFG_IOREMAP_JTAG       CHIP_IOREMAP_JTAG_DIS  /* разрешение JTAG (FULL, NO_NJTRST, SWD, DIS) */
#define CHIP_CFG_IOREMAP_USART1     CHIP_IOREMAP_USART1_A9_10_A8_A11_12
#define CHIP_CFG_IOREMAP_USART2     CHIP_IOREMAP_USART2_A2_4_A0_1
#define CHIP_CFG_IOREMAP_USART3     CHIP_IOREMAP_USART3_B10_14
#define CHIP_CFG_IOREMAP_SPI1       CHIP_IOREMAP_SPI1_A4_7
#define CHIP_CFG_IOREMAP_I2C1       CHIP_IOREMAP_I2C1_B6_7
#define CHIP_CFG_IOREMAP_TIM1       CHIP_IOREMAP_TIM1_A12_A8_11_B12_15
#define CHIP_CFG_IOREMAP_TIM2       CHIP_IOREMAP_TIM2_A0_3
#define CHIP_CFG_IOREMAP_TIM3       CHIP_IOREMAP_TIM3_A6_7_B0_1
#define CHIP_CFG_IOREMAP_TIM4       CHIP_IOREMAP_TIM4_B6_9
#define CHIP_CFG_IOREMAP_SPI3       CHIP_IOREMAP_SPI3_C10_12_A4
#define CHIP_CFG_IOREMAP_CAN1       CHIP_IOREMAP_CAN1_A11_12
#define CHIP_CFG_IOREMAP_CAN2       CHIP_IOREMAP_CAN2_B12_13
#define CHIP_CFG_IOREMAP_ETH        CHIP_IOREMAP_ETH_A7_C4_5_B0_1

#endif // CHIP_CONFIG_H

