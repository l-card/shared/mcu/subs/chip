#ifndef CHIP_REGS_MDIOS_STM32H7XX_H_
#define CHIP_REGS_MDIOS_STM32H7XX_H_

#include <stdint.h>

#define CHIP_MDIOS_DATA_REG_CNT         32

typedef struct {
  __IO uint32_t CR;       /* MDIOS configuration register,      Address offset: 0x00 */
  __IO uint32_t WRFR;     /* MDIOS write flag register,         Address offset: 0x04 */
  __IO uint32_t CWRFR;    /* MDIOS clear write flag register,   Address offset: 0x08 */
  __IO uint32_t RDFR;     /* MDIOS read flag register,          Address offset: 0x0C */
  __IO uint32_t CRDFR;    /* MDIOS clear read flag register,    Address offset: 0x10 */
  __IO uint32_t SR;       /* MDIOS status register,             Address offset: 0x14 */
  __IO uint32_t CLRFR;    /* MDIOS clear flag register,         Address offset: 0x18 */
  uint32_t RESERVED[57];
  __IO uint32_t DINR[CHIP_MDIOS_DATA_REG_CNT];  /* MDIOS input data register x,  Address offset: 0x100 + 0x04 * x, (x = 0 to 31) */
  __IO uint32_t DOUTR[CHIP_MDIOS_DATA_REG_CNT]; /* MDIOS output data register x, Address offset: 0x180 + 0x04 * x, (x = 0 to 31) */
} CHIP_REGS_MDIOS_T;

#define CHIP_REGS_MDIOS          ((CHIP_REGS_MDIOS_T *) CHIP_MEMRGN_ADDR_PERIPH_MDIOS)


/*********** MDIOS configuration register (MDIOS_CR)  *************************/
#define CHIP_REGFLD_MDIOS_CR_EN                 (0x00000001UL <<  0U) /* peripheral enable */
#define CHIP_REGFLD_MDIOS_CR_WRIE               (0x00000001UL <<  1U) /* register write interrupt enable. */
#define CHIP_REGFLD_MDIOS_CR_RDIE               (0x00000001UL <<  2U) /* register read interrupt enable. */
#define CHIP_REGFLD_MDIOS_CR_EIE                (0x00000001UL <<  3U) /* register error interrupt enable. */
#define CHIP_REGFLD_MDIOS_CR_DPC                (0x00000001UL <<  7U) /* disable preamble check. */
#define CHIP_REGFLD_MDIOS_CR_PORT_ADDRESS       (0x0000001FUL <<  8U) /* port address mask. */

/*********** MDIOS status/clear flags register (MDIOS_SR/MDIOS_CLRFR) *********/
#define CHIP_REGFLD_MDIOS_SR_PERF               (0x00000001UL <<  0U) /* preamble error flag */
#define CHIP_REGFLD_MDIOS_SR_SERF               (0x00000001UL <<  1U) /* start error flag */
#define CHIP_REGFLD_MDIOS_SR_TERF               (0x00000001UL <<  2U) /* turnaround error flag */

/*********** MDIOS input data register x (MDIOS_DINRx) ************************/
#define CHIP_REGFLD_DINR_VAL                    (0x0000FFFFUL <<  0U) /* input data received from MDIO Master during write frames */

/*********** MDIOS output data register x (MDIOS_DOUTRx) **********************/
#define CHIP_REGFLD_DOUTR_VAL                   (0x0000FFFFUL <<  0U) /* output data sent to MDIO Master during read frames */

#endif // CHIP_REGS_MDIOS_STM32H7XX_H_
