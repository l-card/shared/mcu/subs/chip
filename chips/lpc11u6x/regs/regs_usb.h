#ifndef CHIP_REGS_LPC11U6X_USB_H
#define CHIP_REGS_LPC11U6X_USB_H


/**
 * @brief USB device register block structure
 */
typedef struct {                /*!< USB Structure */
    __IO uint32_t DEVCMDSTAT;   /*!< USB Device Command/Status register */
    __IO uint32_t INFO;         /*!< USB Info register */
    __IO uint32_t EPLISTSTART;  /*!< USB EP Command/Status List start address */
    __IO uint32_t DATABUFSTART; /*!< USB Data buffer start address */
    __IO uint32_t LPM;          /*!< Link Power Management register */
    __IO uint32_t EPSKIP;       /*!< USB Endpoint skip */
    __IO uint32_t EPINUSE;      /*!< USB Endpoint Buffer in use */
    __IO uint32_t EPBUFCFG;     /*!< USB Endpoint Buffer Configuration register */
    __IO uint32_t INTSTAT;      /*!< USB interrupt status register */
    __IO uint32_t INTEN;        /*!< USB interrupt enable register */
    __IO uint32_t INTSETSTAT;   /*!< USB set interrupt status register */
    __IO uint32_t INTROUTING;   /*!< USB interrupt routing register */
    __I  uint32_t RESERVED0[1];
    __I  uint32_t EPTOGGLE;     /*!< USB Endpoint toggle register */
} CHIP_REGS_USB_T;

#define CHIP_REGS_USB               ((CHIP_REGS_USB_T *) CHIP_MEMRGN_ADDR_PERIPH_USB)


#endif /* CHIP_REGS_LPC11U6X_USB_H */
