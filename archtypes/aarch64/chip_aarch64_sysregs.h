#ifndef CHIP_AARCH64_REGS_H
#define CHIP_AARCH64_REGS_H

#include "lcspec.h"
#include "chip_arm.h"
#include "chip_arm_feat_defs.h"
#include "lbitfield.h"
#include <stdint.h>

/* DAIF, Interrupt Mask Bits of PSTATE */
#define CHIP_REGFLD_AARCH_DAIF_D                (0x0000000000000001ULL <<  9U) /* (RW) Mask Watchpoint, Breakpoint, and Software Step */
#define CHIP_REGFLD_AARCH_DAIF_A                (0x0000000000000001ULL <<  8U) /* (RW) Mask SError exception */
#define CHIP_REGFLD_AARCH_DAIF_I                (0x0000000000000001ULL <<  7U) /* (RW) Mask IRQ */
#define CHIP_REGFLD_AARCH_DAIF_F                (0x0000000000000001ULL <<  6U) /* (RW) Mask FIQ */


/* MPIDR (EL1), Multiprocessor Affinity Register */
#define CHIP_REGFLD_AARCH_MPIDR_AFF0            (0x00000000000000FFULL <<  0U) /* (RO) Affinity level 0 (Core number in cluster) */
#define CHIP_REGFLD_AARCH_MPIDR_AFF1            (0x00000000000000FFULL <<  8U) /* (RO) Affinity level 1 */
#define CHIP_REGFLD_AARCH_MPIDR_AFF2            (0x00000000000000FFULL << 16U) /* (RO) Affinity level 2 */
#define CHIP_REGFLD_AARCH_MPIDR_MT              (0x0000000000000001ULL << 24U) /* (RO) Affinity level 0 consists of logical PEs that are implemented using a multithreading type approach */
#define CHIP_REGFLD_AARCH_MPIDR_U               (0x0000000000000001ULL << 30U) /* (RO) Indicates a Uniprocessor system, as distinct from PE 0 in a multiprocessor system. */
#define CHIP_REGFLD_AARCH_MPIDR_AFF3            (0x00000000000000FFULL << 32U) /* (RO) Affinity level 3 */

/* CurrentEL, Current Exception Level */
#define CHIP_REGFLD_AARCH_CURRENTEL_EL          (0x0000000000000003ULL <<  2U) /* (RO) Current Exception level. */

static LINLINE unsigned chip_get_current_el(void) {
    uint64_t rval;
    chip_mrs("CurrentEL", rval);
    return LBITFIELD_GET(rval, CHIP_REGFLD_AARCH_CURRENTEL_EL);
}

/* HCR, Hypervisor Configuration Register (EL2) */
#define CHIP_REGFLD_AARCH_HCR_VM                (0x0000000000000001ULL <<  0U) /*  */
#define CHIP_REGFLD_AARCH_HCR_SWIO              (0x0000000000000001ULL <<  1U) /*  */
#define CHIP_REGFLD_AARCH_HCR_PTW               (0x0000000000000001ULL <<  2U) /*  */
#define CHIP_REGFLD_AARCH_HCR_FMO               (0x0000000000000001ULL <<  3U) /*  */
#define CHIP_REGFLD_AARCH_HCR_IMO               (0x0000000000000001ULL <<  4U) /*  */
#define CHIP_REGFLD_AARCH_HCR_AMO               (0x0000000000000001ULL <<  5U) /*  */
#define CHIP_REGFLD_AARCH_HCR_VF                (0x0000000000000001ULL <<  6U) /*  */
#define CHIP_REGFLD_AARCH_HCR_VI                (0x0000000000000001ULL <<  7U) /*  */
#define CHIP_REGFLD_AARCH_HCR_VSE               (0x0000000000000001ULL <<  8U) /*  */
#define CHIP_REGFLD_AARCH_HCR_FB                (0x0000000000000001ULL <<  9U) /*  */
#define CHIP_REGFLD_AARCH_HCR_BSU               (0x0000000000000003ULL << 10U) /*  */
#define CHIP_REGFLD_AARCH_HCR_DC                (0x0000000000000001ULL << 12U) /* Default Cacheability. */
#define CHIP_REGFLD_AARCH_HCR_TWI               (0x0000000000000001ULL << 13U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TWE               (0x0000000000000001ULL << 14U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TID0              (0x0000000000000001ULL << 15U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TID1              (0x0000000000000001ULL << 16U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TID2              (0x0000000000000001ULL << 17U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TID3              (0x0000000000000001ULL << 18U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TSC               (0x0000000000000001ULL << 19U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TIDCP             (0x0000000000000001ULL << 20U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TACR              (0x0000000000000001ULL << 21U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TSW               (0x0000000000000001ULL << 22U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TPU               (0x0000000000000001ULL << 24U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TTLB              (0x0000000000000001ULL << 25U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TVM               (0x0000000000000001ULL << 26U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TGE               (0x0000000000000001ULL << 27U) /* Trap General Exceptions, from EL0. */
#define CHIP_REGFLD_AARCH_HCR_TDZ               (0x0000000000000001ULL << 28U) /*  */
#define CHIP_REGFLD_AARCH_HCR_HCD               (0x0000000000000001ULL << 29U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TRVM              (0x0000000000000001ULL << 30U) /*  */
#define CHIP_REGFLD_AARCH_HCR_RW                (0x0000000000000001ULL << 31U) /*  */
#define CHIP_REGFLD_AARCH_HCR_CD                (0x0000000000000001ULL << 32U) /*  */
#define CHIP_REGFLD_AARCH_HCR_ID                (0x0000000000000001ULL << 33U) /*  */
#if CHIP_AARCH_FEAT_VHE
#define CHIP_REGFLD_AARCH_HCR_E2H               (0x0000000000000001ULL << 34U) /* EL2 Host (OS in EL2, app - EL0) */
#endif
#define CHIP_REGFLD_AARCH_HCR_TLOR              (0x0000000000000001ULL << 35U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TERR              (0x0000000000000001ULL << 36U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TEA               (0x0000000000000001ULL << 37U) /*  */
#define CHIP_REGFLD_AARCH_HCR_MIOCNCE           (0x0000000000000001ULL << 38U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TME               (0x0000000000000001ULL << 39U) /*  */
#define CHIP_REGFLD_AARCH_HCR_APK               (0x0000000000000001ULL << 40U) /*  */
#define CHIP_REGFLD_AARCH_HCR_API               (0x0000000000000001ULL << 41U) /*  */
#if CHIP_AARCH_FEAT_NV || CHIP_AARCH_FEAT_NV2
#define CHIP_REGFLD_AARCH_HCR_NV                (0x0000000000000001ULL << 42U) /* Nested Virtualization */
#define CHIP_REGFLD_AARCH_HCR_NV1               (0x0000000000000001ULL << 43U) /* Nested Virtualization */
#endif
#define CHIP_REGFLD_AARCH_HCR_AT                (0x0000000000000001ULL << 44U) /*  */
#if CHIP_AARCH_FEAT_NV2
#define CHIP_REGFLD_AARCH_HCR_NV2               (0x0000000000000001ULL << 45U) /* Nested Virtualization */
#endif
#define CHIP_REGFLD_AARCH_HCR_FWB               (0x0000000000000001ULL << 46U) /*  */
#define CHIP_REGFLD_AARCH_HCR_FIEN              (0x0000000000000001ULL << 47U) /*  */
#define CHIP_REGFLD_AARCH_HCR_GPF               (0x0000000000000001ULL << 48U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TID4              (0x0000000000000001ULL << 49U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TICAB             (0x0000000000000001ULL << 50U) /*  */
#define CHIP_REGFLD_AARCH_HCR_AMVOFFEN          (0x0000000000000001ULL << 51U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TOCU              (0x0000000000000001ULL << 52U) /*  */
#define CHIP_REGFLD_AARCH_HCR_ENSCXT            (0x0000000000000001ULL << 53U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TTLBIS            (0x0000000000000001ULL << 54U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TTLBOS            (0x0000000000000001ULL << 55U) /*  */
#define CHIP_REGFLD_AARCH_HCR_ATA               (0x0000000000000001ULL << 56U) /*  */
#define CHIP_REGFLD_AARCH_HCR_DCT               (0x0000000000000001ULL << 57U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TID5              (0x0000000000000001ULL << 58U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TWEDEn            (0x0000000000000001ULL << 59U) /*  */
#define CHIP_REGFLD_AARCH_HCR_TWEDEL            (0x000000000000000FULL << 60U) /*  */


/* SCR, Secure Configuration Register (EL3) */
#define CHIP_REGFLD_AARCH_SCR_NS                (0x0000000000000001ULL <<  0U) /* Non-secure bit EL0,EL1 */
#define CHIP_REGFLD_AARCH_SCR_IRQ               (0x0000000000000001ULL <<  1U) /* Physical IRQ Routing (1 - to EL3, 0 - to cur lvl, disabled at EL3) */
#define CHIP_REGFLD_AARCH_SCR_FIQ               (0x0000000000000001ULL <<  2U) /* Physical FIQ Routing (1 - to EL3, 0 - to cur lvl, disabled at EL3) */
#define CHIP_REGFLD_AARCH_SCR_EA                (0x0000000000000001ULL <<  3U) /* External Abort and SError interrupt routing (1 - to EL3) */
#define CHIP_REGFLD_AARCH_SCR_SMD               (0x0000000000000001ULL <<  7U) /* Secure Monitor Call disable (0 - SMC instructions enabled at EL1-3, 1 - always disabled) */
#define CHIP_REGFLD_AARCH_SCR_HCE               (0x0000000000000001ULL <<  8U) /* Hypervisor Call instruction enable (enable HVC instructions at EL1-3) */
#define CHIP_REGFLD_AARCH_SCR_SIF               (0x0000000000000001ULL <<  9U) /* Secure instruction fetch (disable execution from non-secure memory in secure state) */
#define CHIP_REGFLD_AARCH_SCR_RW                (0x0000000000000001ULL << 10U) /* Execution state control for lower Exception levels (0 - AArch32, 1 -  next lower level is AArch64) */
#define CHIP_REGFLD_AARCH_SCR_ST                (0x0000000000000001ULL << 11U) /* Traps Secure EL1 accesses to the Counter-timer Physical Secure timer registers to EL3 */
#define CHIP_REGFLD_AARCH_SCR_TWI               (0x0000000000000001ULL << 12U) /* Traps EL2, EL1, and EL0 execution of WFI instructions to EL3 */
#define CHIP_REGFLD_AARCH_SCR_TWE               (0x0000000000000001ULL << 13U) /* Traps EL2, EL1, and EL0 execution of WFE instructions to EL3 */
#if CHIP_AARCH_FEAT_LOR
#define CHIP_REGFLD_AARCH_SCR_TLOR              (0x0000000000000001ULL << 14U) /* Trap LOR registers */
#endif
#define CHIP_REGFLD_AARCH_SCR_TERR              (0x0000000000000001ULL << 15U) /*  */
#define CHIP_REGFLD_AARCH_SCR_APK               (0x0000000000000001ULL << 16U) /*  */
#define CHIP_REGFLD_AARCH_SCR_API               (0x0000000000000001ULL << 17U) /*  */
#if CHIP_AARCH_FEAT_SEL2
#define CHIP_REGFLD_AARCH_SCR_EEL2              (0x0000000000000001ULL << 18U) /* Secure EL2 Enable */
#endif
#define CHIP_REGFLD_AARCH_SCR_EASE              (0x0000000000000001ULL << 19U) /*  */
#define CHIP_REGFLD_AARCH_SCR_NMEA              (0x0000000000000001ULL << 20U) /*  */
#define CHIP_REGFLD_AARCH_SCR_FIEN              (0x0000000000000001ULL << 21U) /*  */
#define CHIP_REGFLD_AARCH_SCR_EnSCXT            (0x0000000000000001ULL << 25U) /*  */
#define CHIP_REGFLD_AARCH_SCR_ATA               (0x0000000000000001ULL << 26U) /*  */
#define CHIP_REGFLD_AARCH_SCR_FGTEn             (0x0000000000000001ULL << 27U) /*  */
#define CHIP_REGFLD_AARCH_SCR_ECVEn             (0x0000000000000001ULL << 28U) /*  */
#define CHIP_REGFLD_AARCH_SCR_TWEDEn            (0x0000000000000001ULL << 29U) /*  */
#define CHIP_REGFLD_AARCH_SCR_TWEDEL            (0x000000000000000FULL << 30U) /*  */
#define CHIP_REGFLD_AARCH_SCR_TME               (0x0000000000000001ULL << 34U) /*  */
#define CHIP_REGFLD_AARCH_SCR_AMVOFFEN          (0x0000000000000001ULL << 35U) /*  */
#define CHIP_REGFLD_AARCH_SCR_EnAS0             (0x0000000000000001ULL << 36U) /*  */
#define CHIP_REGFLD_AARCH_SCR_ADEn              (0x0000000000000001ULL << 37U) /*  */
#define CHIP_REGFLD_AARCH_SCR_HXEn              (0x0000000000000001ULL << 38U) /*  */
#define CHIP_REGFLD_AARCH_SCR_TRNDR             (0x0000000000000001ULL << 40U) /*  */
#define CHIP_REGFLD_AARCH_SCR_EnTP2             (0x0000000000000001ULL << 41U) /*  */
#define CHIP_REGFLD_AARCH_SCR_TCR2En            (0x0000000000000001ULL << 43U) /*  */
#define CHIP_REGFLD_AARCH_SCR_SCTLR2En          (0x0000000000000001ULL << 44U) /*  */
#define CHIP_REGFLD_AARCH_SCR_GPF               (0x0000000000000001ULL << 48U) /*  */
#define CHIP_REGFLD_AARCH_SCR_MECEn             (0x0000000000000001ULL << 49U) /*  */
#if CHIP_AARCH_FEAT_RME
#define CHIP_REGFLD_AARCH_SCR_NSE               (0x0000000000000001ULL << 62U) /*  Security state of EL2 and lower Exception levels. */
#endif
#define CHIP_REGMSK_AARCH_SCR_RES1              (0x0000000000000030ULL)





#endif // CHIP_AARCH64_REGS_H
