#ifndef CHIP_STD_DEFS_H
#define CHIP_STD_DEFS_H

#define CHIP_MHZ(n)          (1000000 * (n))
#define CHIP_KHZ(n)          (1000 * (n))
#define CHIP_HZ(n)           (1 * (n))

#define CHIP_B(n)            (1 * (n))
#define CHIP_KB(n)           (1024 * (n))
#define CHIP_MB(n)           (1024 * 1024 * (n))
#define CHIP_GB(n)           (1024 * 1024 * 1024 *(n))



/* режим, определяющий тип используемого внешнего источника частоты OSC */
#define CHIP_CLK_EXTMODE_DIS        0 /* внешний источник частоты не используется */
#define CHIP_CLK_EXTMODE_OSC        1 /* используется внешний осциллятор */
#define CHIP_CLK_EXTMODE_CLOCK      2 /* используется внешний генератор */




#define CHIP_CLK_ID_EN_(id)     id ## _EN
#define CHIP_CLK_ID_EN(id)      CHIP_CLK_ID_EN_(id)

#define CHIP_CLK_ID_FREQ_(id)   id ## _FREQ
#define CHIP_CLK_ID_FREQ(id)    CHIP_CLK_ID_FREQ_(id)

#define CHIP_CLK_ID_VAL_(id)    (id ## _ID)
#define CHIP_CLK_ID_VAL(id)     CHIP_CLK_ID_VAL_(id)




#endif // CHIP_CLK_STD_DEFS_H
