#ifndef CHIP_STM32F10X_REGS_DBGMCU_H
#define CHIP_STM32F10X_REGS_DBGMCU_H

#include "chip_ioreg_defs.h">
#include "chip_devtype_spec_features.h"

typedef struct {
  __IO uint32_t IDCODE;
  __IO uint32_t CR;
} CHIP_REGS_DBGMCU_T;

#define CHIP_REGS_DBGMCU                ((CHIP_REGS_DBGMCU_T *) CHIP_MEMRGN_ADDR_DBGMCU)


/****** DBGMCU identity code register (DBGMCU_IDC) ****************************/
#define CHIP_REGFLD_DBGMCU_IDCODE_DEV_ID        (0x00000FFFUL <<  0U)
#define CHIP_REGFLD_DBGMCU_IDCODE_REV_ID        (0x0000FFFFUL << 16U)

/* --------------------- ST32F103 --------------------------------------------*/
#define CHIP_DEV_ID_ST32F10X_LD         0x412 /* STM32F10x Low-Density */
#define CHIP_DEV_ID_ST32F10X_MD         0x410 /* STM32F10x Medium-Density */
#define CHIP_DEV_ID_ST32F10X_HD         0x414 /* STM32F10x High-Density */
#define CHIP_DEV_ID_ST32F10X_XL         0x430 /* STM32F10x XL-Density */
#define CHIP_DEV_ID_ST32F10X_CON        0x418 /* STM32F10x connetivity devices */

#define CHIP_REV_ID_ST32F10X_LD_A       0x1000
#define CHIP_REV_ID_ST32F10X_MD_A       0x0000
#define CHIP_REV_ID_ST32F10X_MD_B       0x2000
#define CHIP_REV_ID_ST32F10X_MD_Z       0x2001
#define CHIP_REV_ID_ST32F10X_MD_XY123   0x2003
#define CHIP_REV_ID_ST32F10X_HD_A1      0x1000
#define CHIP_REV_ID_ST32F10X_HD_Z       0x1001
#define CHIP_REV_ID_ST32F10X_HD_XY123   0x1003
#define CHIP_REV_ID_ST32F10X_XL_A1      0x1000
#define CHIP_REV_ID_ST32F10X_CON_A      0x1000
#define CHIP_REV_ID_ST32F10X_CON_Z      0x1001


#define CHIP_PID_GD32F10X               0x790007A3

/* --------------------- AT32F403A/AT32F407 ----------------------------------*/
#define CHIP_PID_AT32F403AVCT7          0x70050240 /*  256KB LQFP100 */
#define CHIP_PID_AT32F403ARCT7          0x70050241 /*  256KB LQFP64  */
#define CHIP_PID_AT32F403ACCT7          0x70050242 /*  256KB LQFP48  */
#define CHIP_PID_AT32F403ACCU7          0x70050243 /*  256KB QFN48   */
#define CHIP_PID_AT32F403AVGT7          0x70050344 /* 1024KB LQFP100 */
#define CHIP_PID_AT32F403ARGT7          0x70050345 /* 1024KB LQFP64  */
#define CHIP_PID_AT32F403ACGT7          0x70050346 /* 1024KB LQFP48  */
#define CHIP_PID_AT32F403ACGU7          0x70050347 /* 1024KB QFN48   */
#define CHIP_PID_AT32F403AVET7          0x700502CD /*  512KB LQFP100 */
#define CHIP_PID_AT32F403ARET7          0x700502CE /*  512KB LQFP64  */
#define CHIP_PID_AT32F403ACET7          0x700502CF /*  512KB LQFP48  */
#define CHIP_PID_AT32F403ACEU7          0x700502D0 /*  512KB QFN48   */

#define CHIP_PID_AT32F407VCT7           0x70050249 /*  256KB LQFP100 */
#define CHIP_PID_AT32F407RCT7           0x7005024A /*  256KB LQFP64  */
#define CHIP_PID_AT32F407VGT7           0x7005034B /* 1024KB LQFP100 */
#define CHIP_PID_AT32F407RGT7           0x7005034C /* 1024KB LQFP64  */
#define CHIP_PID_AT32F407VET7           0x700502D1 /*  512KB LQFP100 */
#define CHIP_PID_AT32F407RET7           0x700502D2 /*  512KB LQFP64  */
#define CHIP_PID_AT32F407AVGT7          0x70050353 /* 1024KB LQFP100 */
#define CHIP_PID_AT32F407AVCT7          0x70050254 /* 256KB LQFP100 */



#endif // CHIP_STM32F10X_REGS_DBGMCU_H
