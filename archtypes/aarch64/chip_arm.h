#ifndef CHIP_ARM_H
#define CHIP_ARM_H


#ifndef __STRINGIFY
#define __STRINGIFY(x)                         #x
#endif

/*================================================================================================*
 * ---- Intrinsic функции управления процессором ----
 * (В CMSIS описаны неудачно, без memory clobber. В линуксе - clobber есть.)
 *================================================================================================*/
#define chip_sev() __asm__ volatile ("sev" ::: "memory")   /* send event (multiproc) */
#define chip_wfe() __asm__ volatile ("wfe" ::: "memory")   /* wait for event (multiproc) */
#define chip_wfi() __asm__ volatile ("wfi" ::: "memory")   /* wait for interrupt */
#define chip_isb() __asm__ volatile ("isb" ::: "memory")   /* instruction sync barrier */
#define chip_dsb() __asm__ volatile ("dsb sy" ::: "memory")   /* data sync barrier, e.g. after disable IRQ */
#define chip_dmb() __asm__ volatile ("dmb sy" ::: "memory")   /* data memory barrier, e.g. before DMA */




#define chip_msr(sysreg, val)  __asm volatile ("msr " sysreg ", %0\n" : : "r"((uint64_t)(val)))
#define chip_mrs(sysreg, val) __asm volatile ("mrs  %0, " sysreg "\n" : "=r"(val))


#define chip_sysreg_wr(sysreg, val) chip_msr(sysreg, val)
#define chip_sysreg_rd(sysreg, val) chip_mrs(sysreg, val)

#define chip_sysreg_update(sysreg, mask, val) do { \
    uint64_t regval; \
    chip_sysreg_rd(sysreg, regval); \
    regval = (regval & ~(mask)) | ((val) & (mask)); \
    chip_sysreg_wr(sysreg, regval); \
} while(0)

#define chip_interrupts_enable()           chip_sysreg_wr("DAIF", 0);
#define chip_interrupts_disable()          chip_sysreg_wr("DAIF", CHIP_REGFLD_AARCH_DAIF_I | CHIP_REGFLD_AARCH_DAIF_F | CHIP_REGFLD_AARCH_DAIF_A | CHIP_REGFLD_AARCH_DAIF_D);

#define chip_interrupts_irq_enable()       chip_sysreg_update("DAIF", CHIP_REGFLD_AARCH_DAIF_I, 0);
#define chip_interrupts_irq_disable()      chip_sysreg_update("DAIF", CHIP_REGFLD_AARCH_DAIF_I, CHIP_REGFLD_AARCH_DAIF_I);

#define chip_interrupts_fiq_enable()       chip_sysreg_update("DAIF", CHIP_REGFLD_AARCH_DAIF_F, 0);
#define chip_interrupts_fiq_disable()      chip_sysreg_update("DAIF", CHIP_REGFLD_AARCH_DAIF_F, CHIP_REGFLD_AARCH_DAIF_F);

#define chip_interrupts_serror_enable()    chip_sysreg_update("DAIF", CHIP_REGFLD_AARCH_DAIF_A, 0);
#define chip_interrupts_serror_disable()   chip_sysreg_update("DAIF", CHIP_REGFLD_AARCH_DAIF_A, CHIP_REGFLD_AARCH_DAIF_A);



#endif // CHIP_ARM_H
