#ifndef CHIP_ARMV7EM_REGS_CORE_CACHE_H
#define CHIP_ARMV7EM_REGS_CORE_CACHE_H

#include "stdint.h"

typedef struct {
    __OM  uint32_t ICIALLU;                /* Offset: 0x250 ( /W)  I-Cache Invalidate All to PoU */
    uint32_t RESERVED0[1U];
    __OM  uint32_t ICIMVAU;                /* Offset: 0x258 ( /W)  I-Cache Invalidate by MVA to PoU */
    __OM  uint32_t DCIMVAC;                /* Offset: 0x25C ( /W)  D-Cache Invalidate by MVA to PoC */
    __OM  uint32_t DCISW;                  /* Offset: 0x260 ( /W)  D-Cache Invalidate by Set-way */
    __OM  uint32_t DCCMVAU;                /* Offset: 0x264 ( /W)  D-Cache Clean by MVA to PoU */
    __OM  uint32_t DCCMVAC;                /* Offset: 0x268 ( /W)  D-Cache Clean by MVA to PoC */
    __OM  uint32_t DCCSW;                  /* Offset: 0x26C ( /W)  D-Cache Clean by Set-way */
    __OM  uint32_t DCCIMVAC;               /* Offset: 0x270 ( /W)  D-Cache Clean and Invalidate by MVA to PoC */
    __OM  uint32_t DCCISW;                 /* Offset: 0x274 ( /W)  D-Cache Clean and Invalidate by Set-way */
} CHIP_REGS_ARMV7EM_CACHE_T;

#define CHIP_REGS_CACHE                 ((CHIP_REGS_ARMV7EM_CACHE_T *) CHIP_MEMRGN_ADDR_ARMV7EM_CACHE)

/*********** Data cache operations by set-way (DCISW, DCCSW, DCCISW) **********/
#define CHIP_REGFLD_CACHE_SW_WAY        (0x00000003UL << 30U) /* (WO) Way that operation applies to. For the data cache, values 0, 1, 2 and 3 are supported. */
#define CHIP_REGFLD_CACHE_SW_SET        (0x000001FFUL <<  5U) /* (WO) Set/index that operation applies to. */

#endif // CHIP_ARMV7EM_REGS_CORE_CACHE_H
