#ifndef CHIP_PIN_DEFS_V1_H
#define CHIP_PIN_DEFS_V1_H

#include "chip_ioreg_defs.h"
#include "lbitfield.h"


/* General Purpose I/O */
typedef struct {
    union {
        struct {
            __IO uint32_t CRL; /* Port configuration register low,   Address offset: 0x00 */
            __IO uint32_t CRH; /* Port configuration register high,  Address offset: 0x04 */
        };
        __IO uint32_t CR[2];
    };
    __I  uint32_t IDR;    /* Port input data register,          Address offset: 0x08 */
    __IO uint32_t ODR ;   /* Port output data register,         Address offset: 0x0C */
    __O  uint32_t BSRR;   /* Port bit set/reset register,       Address offset: 0x10 */
    __O  uint32_t BRR;    /* Port bit reset register,           Address offset: 0x14 */
    __IO uint32_t LCKR;   /* Port configuration lock register,  Address offset: 0x18 */
    uint32_t RESERVED1;
#if CHIP_SUPPORT_GPIO_SRCTL
    __IO uint32_t SRCTR;    /* GPIO port slew rate control register,   Address offset: 0x20 */
#endif
    uint32_t RESERVED2[5];
#if CHIP_SUPPORT_GPIO_HDRV
    __IO uint32_t HDRV;     /* GPIO port huge driven control register, Address offset: 0x3C */
#endif
} CHIP_REGS_GPIO_T;

/*********** GPIO port configuration register low/high (GPIO_CRL/GPIO_CRH) */
#define CHIP_REGFLD_GPIO_CR_CNF(pinnum)             (0x00000003UL << (4 * ((pinnum) & 0x7) + 2U)) /* (RW) port configuration */
#define CHIP_REGFLD_GPIO_CR_MODE(pinnum)            (0x00000003UL << (4 * ((pinnum) & 0x7) + 0U)) /* (RW) port mode */
#define CHIP_REGNUM_GPIO_CR(pinnum)                 ((pinnum) / 8)

#define CHIP_REGFLDVAL_GPIO_CR_MODE_IN             0 /* Input mode */
#define CHIP_REGFLDVAL_GPIO_CR_MODE_OUT_MED        1 /* Medium speed/strength output */
#define CHIP_REGFLDVAL_GPIO_CR_MODE_OUT_LOW        2 /* Low speed/strength output */
#define CHIP_REGFLDVAL_GPIO_CR_MODE_OUT_HIGH       3 /* High speed/strength output */
/* настройки выходного режима разные в разных процессорах, где-то по силе тока, где-то по частоте */

#define CHIP_REGFLDVAL_GPIO_CR_CNF_IN_ANALOG     0 /* Analog mode */
#define CHIP_REGFLDVAL_GPIO_CR_CNF_IN_FLOATING   1 /* Floating input */
#define CHIP_REGFLDVAL_GPIO_CR_CNF_IN_PUPD       2 /* Input with pull-up/pull-down */
#define CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_GP_PP     0 /* General purpose output push-pull */
#define CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_GP_OD     1 /* General purpose output open drain */
#define CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_ALT_PP    2 /* Alternate output push-pull */
#define CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_ALT_OD    3 /* Alternate output open drain */

/*********** GPIO port bit set/reset register (GPIO_BSRR) ********************/
#define CHIP_REGFLD_GPIO_BSRR_BR(pinnum)           (0x00000001UL << (16U + (pinnum))) /* (WO) port reset bit */
#define CHIP_REGFLD_GPIO_BSRR_BS(pinnum)           (0x00000001UL << ( 0U + (pinnum))) /* (WO) port set bit */

/*********** GPIO port configuration lock register (GPIO_LCKR) ***************/
#define CHIP_REGFLD_GPIO_LCKR_KEY                   (0x00000001UL << 16UL)     /* (RW) Lock key */
#define CHIP_REGFLD_GPIO_LCKR(pinnum)               (0x00000001UL << (pinnum)) /* (RW) Port lock bit */









typedef unsigned t_chip_pin_id;

#define CHIP_PIN_MODE_INPUT     0x0
#define CHIP_PIN_MODE_OUTPUT    0x1
#define CHIP_PIN_MODE_AF        0x2
#define CHIP_PIN_MODE_ANALOG    0x3
#define CHIP_PIN_MODE_UNKNOWN   0xF

#define CHIP_PIN_FUNC_ANALOG    0xFE
#define CHIP_PIN_FUNC_GPIO      0xFF


/* -------------- различные параметры конфигурации пинов -------------------- */
/* тип выхода (push-pull или open-drain) */
#define CHIP_PIN_CFG_OTYPE              (0x1UL << 0)
#define CHIP_PIN_CFG_OTYPE_VAL(v)       (((v) & 0x1UL) << 0)
#define CHIP_PIN_CFG_OTYPE_PP           CHIP_PIN_CFG_OTYPE_VAL(0) /* push-pull */
#define CHIP_PIN_CFG_OTYPE_OD           CHIP_PIN_CFG_OTYPE_VAL(1) /* open-drain */

/* скорость переключения (slew rate) */
#define CHIP_PIN_CFG_OSPEED             (0x1UL << 1)
#define CHIP_PIN_CFG_OSPEED_VAL(v)      (((v) & 0x1UL) << 1)
#define CHIP_PIN_CFG_OSPEED_LOW         CHIP_PIN_CFG_OSPEED_VAL(0)
#define CHIP_PIN_CFG_OSPEED_HIGH        CHIP_PIN_CFG_OSPEED_VAL(1)


/* нагр. спасобность (мА)  */
#define CHIP_PIN_CFG_OSTRENGTH          (0x3UL << 2)
#define CHIP_PIN_CFG_OSTRENGTH_VAL(v)   (((v) & 0x3UL) << 2)
#define CHIP_PIN_CFG_OSTRENGTH_LOW      CHIP_PIN_CFG_OSTRENGTH_VAL(0)
#define CHIP_PIN_CFG_OSTRENGTH_MED      CHIP_PIN_CFG_OSTRENGTH_VAL(1)
#define CHIP_PIN_CFG_OSTRENGTH_HIGH     CHIP_PIN_CFG_OSTRENGTH_VAL(2)
#define CHIP_PIN_CFG_OSTRENGTH_VHIGH    CHIP_PIN_CFG_OSTRENGTH_VAL(3)

/* включение подтяжек (только для входа) */
#define CHIP_PIN_CFG_PUPD               (0x3UL << 4)
#define CHIP_PIN_CFG_PUPD_VAL(v)        (((v) & 0x3UL) << 4)
#define CHIP_PIN_CFG_NOPULL             CHIP_PIN_CFG_PUPD_VAL(0)
#define CHIP_PIN_CFG_PULLUP             CHIP_PIN_CFG_PUPD_VAL(1)
#define CHIP_PIN_CFG_PULLDOWN           CHIP_PIN_CFG_PUPD_VAL(2)


/* конфигурации по умолчанию */
#ifndef CHIP_PIN_CFG_STD
    #define CHIP_PIN_CFG_STD      (CHIP_PIN_CFG_OSPEED_HIGH | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_PP)
#endif

#ifndef CHIP_PIN_CFG_STD_OD
    #define CHIP_PIN_CFG_STD_OD   (CHIP_PIN_CFG_OSPEED_HIGH | CHIP_PIN_CFG_NOPULL | CHIP_PIN_CFG_OTYPE_OD)
#endif




#define CHIP_PIN_ID_EX(port, pin, func, mode, cfg) ((((port)       &  0xF) << 28) | \
                                                                (((pin)       &  0xF) << 24) | \
                                                                (((func)      & 0xFF) << 16) | \
                                                                (((mode)      &  0xF) <<  8) | \
                                                                (((cfg)       & 0xFF) <<  0))

#define CHIP_PIN_ID(port, pin, func, mode)          CHIP_PIN_ID_EX(port, pin, func, mode, CHIP_PIN_CFG_STD)
#define CHIP_PIN_ID_GPIO(port, pin)                 CHIP_PIN_ID(port, pin,  CHIP_PIN_FUNC_GPIO,   CHIP_PIN_MODE_UNKNOWN)
#define CHIP_PIN_ID_ANALOG(port, pin)               CHIP_PIN_ID(port, pin,  CHIP_PIN_FUNC_ANALOG, CHIP_PIN_MODE_ANALOG)
#define CHIP_PIN_ID_AF(port, pin, func)             CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_MODE_AF, CHIP_PIN_CFG_STD)
#define CHIP_PIN_ID_AF_OD(port, pin, func)          CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_MODE_AF, CHIP_PIN_CFG_STD_OD)
#define CHIP_PIN_ID_FORCED(port, pin, func)         CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_MODE_UNKNOWN, CHIP_PIN_CFG_STD_OD)
#define CHIP_PIN_ID_IN_EX(port, pin, func, cfg)     CHIP_PIN_ID_EX(port, pin, func, CHIP_PIN_MODE_INPUT, cfg)
#define CHIP_PIN_ID_IN(port, pin, func)             CHIP_PIN_ID_IN_EX(port, pin, func, CHIP_PIN_CFG_STD)




#define CHIP_PIN_ID_PORT(id)        (((id) >> 28) & 0xF)
#define CHIP_PIN_ID_PIN(id)         (((id) >> 24) & 0xF)
#define CHIP_PIN_ID_FUNC(id)        (((id) >> 16) & 0xFF)
#define CHIP_PIN_ID_MODE(id)        (((id) >>  8) & 0xF)
#define CHIP_PIN_ID_CFG(id)         (((id) >>  0) & 0xFF)

#define CHIP_PIN_ID_INVALID         0xFFFFFFFF

#define CHIP_PIN_GPIOREG(reg, id)    CHIP_REGS_GPIO(CHIP_PIN_ID_PORT(id))->reg
#define CHIP_PIN_GPIOREG_CR(id)      CHIP_PIN_GPIOREG(CR[CHIP_REGNUM_GPIO_CR(CHIP_PIN_ID_PIN(id))],id)
#define CHIP_PIN_BITMSK(id)          (1UL << CHIP_PIN_ID_PIN(id))
#define CHIP_PIN_CTL_MSK(id)         (CHIP_REGFLD_GPIO_CR_CNF(CHIP_PIN_ID_PIN(id)) | CHIP_REGFLD_GPIO_CR_MODE(CHIP_PIN_ID_PIN(id)))


#define CHIP_PIN_REGVAL_CR_CNF(id, cfg, mode) (((mode) == CHIP_PIN_MODE_OUTPUT) ? \
                                              ((((cfg) & CHIP_PIN_CFG_OTYPE) == CHIP_PIN_CFG_OTYPE_PP) ? \
                                                 CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_GP_PP : CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_GP_OD) : \
                                           ((mode) == CHIP_PIN_MODE_AF) ? \
                                              ((((cfg) & CHIP_PIN_CFG_OTYPE) == CHIP_PIN_CFG_OTYPE_PP) ? \
                                                 CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_ALT_PP : CHIP_REGFLDVAL_GPIO_CR_CNF_OUT_ALT_OD) : \
                                           ((mode) == CHIP_PIN_MODE_ANALOG) ? CHIP_REGFLDVAL_GPIO_CR_CNF_IN_ANALOG : \
                                           ((mode) == CHIP_PIN_MODE_INPUT) ? \
                                              ((((cfg) & CHIP_PIN_CFG_PUPD) == CHIP_PIN_CFG_NOPULL) ? \
                                                CHIP_REGFLDVAL_GPIO_CR_CNF_IN_FLOATING : CHIP_REGFLDVAL_GPIO_CR_CNF_IN_PUPD) : 0)

#define CHIP_PIN_REGVAL_CR_MODE(id, cfg, mode) ((((mode) == CHIP_PIN_MODE_OUTPUT) || ((mode) == CHIP_PIN_MODE_AF)) ? \
                                            ((((cfg) & CHIP_PIN_CFG_OSTRENGTH) == CHIP_PIN_CFG_OSTRENGTH_LOW) ? CHIP_REGFLDVAL_GPIO_CR_MODE_OUT_LOW : \
                                             (((cfg) & CHIP_PIN_CFG_OSTRENGTH) == CHIP_PIN_CFG_OSTRENGTH_MED) ? CHIP_REGFLDVAL_GPIO_CR_MODE_OUT_MED :\
                                                                                                                CHIP_REGFLDVAL_GPIO_CR_MODE_OUT_HIGH) : \
                                                                                                                CHIP_REGFLDVAL_GPIO_CR_MODE_IN)
/** @todo выбор по поддержке hdrv и srctr */
/* функция, настраивающая пин по ID, с явным заданием специальных режимов через объединение по или CHIP_PIN_CFG */
#define CHIP_PIN_CONFIG_EX_MODE(id, cfg, mode) do { \
        if ((mode) != CHIP_PIN_MODE_UNKNOWN) { \
            CHIP_PIN_GPIOREG_CR(id) = (CHIP_PIN_GPIOREG_CR(id) & ~CHIP_PIN_CTL_MSK(id)) \
                | LBITFIELD_SET(CHIP_REGFLD_GPIO_CR_CNF(CHIP_PIN_ID_PIN(id)), CHIP_PIN_REGVAL_CR_CNF(id, cfg, mode)) \
                | LBITFIELD_SET(CHIP_REGFLD_GPIO_CR_MODE(CHIP_PIN_ID_PIN(id)), CHIP_PIN_REGVAL_CR_MODE(id, cfg, mode)); \
            if ((mode) == CHIP_PIN_MODE_INPUT) { \
                CHIP_PIN_OUT(id, ((cfg) & CHIP_PIN_CFG_PUPD) == CHIP_PIN_CFG_PULLUP); \
            } \
        } \
        if ((CHIP_PIN_ID_FUNC(id) != CHIP_PIN_FUNC_GPIO) && (CHIP_PIN_ID_FUNC(id) != CHIP_PIN_FUNC_ANALOG)) { \
        }  \
    } while (0)

//LBITFIELD_UPD(CHIP_PIN_GPIOREG(HDRV,id), CHIP_PIN_BITMSK(id), ((cfg) & CHIP_PIN_CFG_OSTRENGTH) == CHIP_PIN_CFG_OSTRENGTH_VHIGH); \
//LBITFIELD_UPD(CHIP_PIN_GPIOREG(SRCTR,id), CHIP_PIN_BITMSK(id), ((cfg) & CHIP_PIN_CFG_OSPEED) == CHIP_PIN_CFG_OSPEED_HIGH); \

/* функция, настраивающая пин по ID, использующая указанные настройки */
#define CHIP_PIN_CONFIG_EX(id, cfg) CHIP_PIN_CONFIG_EX_MODE(id, cfg, CHIP_PIN_ID_MODE(id))


/* функция, настраивающая пин по ID, использующая спец. настройки по умолчанию.
   Используется для альтернативных функций и АЦП. Для GPIO используются CHIP_PIN_CONFIG_OUT и CHIP_PIN_CONFIG_IN */
#define CHIP_PIN_CONFIG(id) do { \
        CHIP_PIN_CONFIG_EX(id, CHIP_PIN_ID_CFG(id)); \
    } while(0)


/* настройка пина на оптимальный режим, если не используется (аналоговый режим для отключения входа/выхода) */
#define CHIP_PIN_CONFIG_DIS(id) do { \
    CHIP_PIN_CONFIG(CHIP_PIN_ID(CHIP_PIN_ID_PORT(id), CHIP_PIN_ID_PIN(id), CHIP_PIN_FUNC_ANALOG)); \
} while(0)


/* установка уровня на ножке (0 или 1) */
#define CHIP_PIN_OUT(id, val)        CHIP_PIN_GPIOREG(BSRR,id) = ((val) ? CHIP_REGFLD_GPIO_BSRR_BS(CHIP_PIN_ID_PIN(id)) : CHIP_REGFLD_GPIO_BSRR_BR(CHIP_PIN_ID_PIN(id)))
/* чтение уровня на ножке (0 или 1) */
#define CHIP_PIN_IN(id)              (!!(CHIP_PIN_GPIOREG(IDR,id) & CHIP_PIN_BITMSK(id)))
/* состояние регистра выхода (независимо от физического уровня) */
#define CHIP_PIN_GET_OUT_VAL(id)     (!!(CHIP_PIN_GPIOREG(ODR,id) & CHIP_PIN_BITMSK(id)))
/* атомарное инвертирование уровня на ножке */
#define CHIP_PIN_TOGGLE(id)          CHIP_PIN_OUT(id, (CHIP_PIN_GET_OUT_VAL(id) ? 0 : 1))


/* Конфигурация пина как General Purpose Output с указанной доп. конфигурацией и начальным значением */
#define CHIP_PIN_CONFIG_OUT_EX(id, cfg, val) do { \
                                        CHIP_PIN_OUT(id, val); \
                                        CHIP_PIN_CONFIG_EX_MODE(id, cfg, CHIP_PIN_MODE_OUTPUT); \
                                    } while(0)

/* Конфигурация пина как General Purpose Input с указанной доп. конфигурацией */
#define CHIP_PIN_CONFIG_IN_EX(id, cfg) CHIP_PIN_CONFIG_EX_MODE(id, cfg, CHIP_PIN_MODE_INPUT);

/* Конфигурация пина как General Purpose Output с конфигурацией по умолчанию и начальным значением */
#define CHIP_PIN_CONFIG_OUT(id, val) CHIP_PIN_CONFIG_OUT_EX(id, CHIP_PIN_ID_CFG(id), val)
/* вспомогательный макрос для настройки пина и конфигурирования его на вход */
#define CHIP_PIN_CONFIG_IN(id) CHIP_PIN_CONFIG_IN_EX(id, CHIP_PIN_ID_CFG(id))



#endif // CHIP_PIN_DEFS_V1_H
