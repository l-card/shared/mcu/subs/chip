#include "chip.h"

void chip_clk_init(void);
void chip_pwr_init(void);
void chip_ioremap_init(void);


void chip_dummy_init_fault(int code) {
    (void)code;

    for (;;) {
        continue;
    }
}

void chip_init(void) {
    CHIP_REGS_RCC->APB2ENR = 0
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_AFIO_EN, 1)
                             | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2ENR_IOPA_EN, 1);
    //CHIP_PIN_CONFIG_OUT(CHIP_PIN_PA12_GPIO, 1);
    //CHIP_PIN_CONFIG_OUT(CHIP_PIN_PA11_GPIO, 0);
    //for (;;) {

    //}
#ifdef CHIP_DEV_GD32F10X
    CHIP_REGS_RCC->SYSCFG = (CHIP_REGS_RCC->SYSCFG & CHIP_REGMSK_RCC_SYSCFG_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_SYSCFG_CEE, CHIP_CODE_EFFICIENCY);
#endif
    chip_pwr_init();
    chip_clk_init();
    chip_ioremap_init();
}


void chip_main_prestart(void) {

}
