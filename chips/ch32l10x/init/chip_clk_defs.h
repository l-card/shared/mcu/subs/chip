#ifndef CHIP_CLK_DEFS_H
#define CHIP_CLK_DEFS_H

/* исходное значение частоты HSI */
#define CHIP_CLK_HSI_FREQ           CHIP_MHZ(8)
#define CHIP_CLK_LSI_FREQ           CHIP_KHZ(40)

#define CHIP_CLK_CPU_INITIAL_FREQ   CHIP_CLK_HSI_FREQ


#define CHIP_CLK_HSE_ID             1
#define CHIP_CLK_LSE_ID             2
#define CHIP_CLK_HSI_ID             3
#define CHIP_CLK_LSI_ID             4
#define CHIP_CLK_PLL1_ID            5
#define CHIP_CLK_SYS_ID             7
#define CHIP_CLK_AHB_ID             8
#define CHIP_CLK_APB1_ID            9
#define CHIP_CLK_APB2_ID            10

#endif // CHIP_CLK_DEFS_H

