#ifndef CHIP_STM32F10X_REGS_RCC_H
#define CHIP_STM32F10X_REGS_RCC_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR;         /*!< RCC clock control register,                                  Address offset: 0x00 */
    __IO uint32_t CFGR;       /*!< RCC clock configuration register,                            Address offset: 0x04 */
    __IO uint32_t CIR;        /*!< RCC clock interrupt register,                                Address offset: 0x08 */
    __IO uint32_t APB2RSTR;   /*!< RCC APB2 peripheral reset register,                          Address offset: 0x0C */
    __IO uint32_t APB1RSTR;   /*!< RCC APB1 peripheral reset register,                          Address offset: 0x10 */
    __IO uint32_t AHBENR;     /*!< RCC AHB peripheral clock register,                           Address offset: 0x14 */
    __IO uint32_t APB2ENR;    /*!< RCC APB2 peripheral clock enable register,                   Address offset: 0x18 */
    __IO uint32_t APB1ENR;    /*!< RCC APB1 peripheral clock enable register,                   Address offset: 0x1C */
    __IO uint32_t BDCR;       /*!< RCC Backup domain control register,                          Address offset: 0x20 */
    __IO uint32_t CSR;        /*!< RCC clock control & status register,                         Address offset: 0x24 */
#if defined CHIP_DEV_ST32F105_XC || defined CHIP_DEV_ST32F107_XC || defined CHIP_DEV_CH32XXXX
    __IO uint32_t AHBRSTR;    /*!< RCC AHB peripheral reset register,                           Address offset: 0x28 */
    __IO uint32_t CFGR2;      /*!< RCC clock configuration register 2,                          Address offset: 0x2C */
#else
    uint32_t RESERVED1[2];
#endif
    uint32_t RESERVED2;       /*!< Reserved                                                     Address offset: 0x30 */
#ifdef CHIP_DEV_GD32F10X
    __IO uint32_t DSV;        /*!< RCC Deep-sleep mode voltage register,                        Address offset: 0x34 */
#else
    uint32_t RESERVED3;
#endif
    uint32_t RESERVED4;       /*!< Reserved                                                     Address offset: 0x38 */
#ifdef CHIP_DEV_GD32F10X
    __IO uint32_t SYSCFG;     /*!< System configuration register                                Address offset: 0x3C */
#else
    uint32_t RESERVED5;
#endif
} CHIP_REGS_RCC_T;

#define CHIP_REGS_RCC                   ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)

#if defined CHIP_DEV_CH32F2_V30X_D8W
#define CHIP_REG_HSE_CAL_CTRL           (*((__O uint32_t *) (CHIP_MEMRGN_ADDR_AHB + 0x2202C))) /* HSE crystal oscillator calibration control register */
#define CHIP_REG_LSI32K_TUNE            (*((__O uint16_t *) (CHIP_MEMRGN_ADDR_AHB + 0x22036))) /* LSI crystal oscillator calibration tune register */
#define CHIP_REG_LSI32K_CAL_CFG         (*((__O uint8_t *)  (CHIP_MEMRGN_ADDR_AHB + 0x22049))) /* LSI crystal oscillator calibration configuration register */
#define CHIP_REG_LSI32K_CAL_STATR       (*((__O uint16_t *) (CHIP_MEMRGN_ADDR_AHB + 0x2204C))) /* LSI crystal oscillator calibration status register */
#define CHIP_REG_LSI32K_CAL_OV_CNT      (*((__O uint8_t *)  (CHIP_MEMRGN_ADDR_AHB + 0x2204E))) /* LSI crystal oscillator calibration counter */
#define CHIP_REG_LSI32K_CAL_CTRL        (*((__O uint8_t *)  (CHIP_MEMRGN_ADDR_AHB + 0x2204F))) /* LSI crystal oscillator calibration control register */
#endif

/*********** Clock control register (RCC_CR) **********************************/
#define CHIP_REGFLD_RCC_CR_HSI_ON                   (0x00000001UL <<  0U)   /* (RW) HSI clock enable */
#define CHIP_REGFLD_RCC_CR_HSI_RDY                  (0x00000001UL <<  1U)   /* (RO) HSI clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSI_TRIM                 (0x0000001FUL <<  3U)   /* (RW) HSI clock trimming */
#define CHIP_REGFLD_RCC_CR_HSI_CAL                  (0x000000FFUL <<  8U)   /* (RO) HSI clock calibration */
#define CHIP_REGFLD_RCC_CR_HSE_ON                   (0x00000001UL << 16U)   /* (RW) HSE clock enable */
#define CHIP_REGFLD_RCC_CR_HSE_RDY                  (0x00000001UL << 17U)   /* (RO) HSE clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSE_BYP                  (0x00000001UL << 18U)   /* (RW) HSE crystal oscillator bypass */
#define CHIP_REGFLD_RCC_CR_CSS_ON                   (0x00000001UL << 19U)   /* (RW) Clock security system enable */
#define CHIP_REGFLD_RCC_CR_PLL1_ON                  (0x00000001UL << 24U)   /* (RW) PLL1 enable */
#define CHIP_REGFLD_RCC_CR_PLL1_RDY                 (0x00000001UL << 25U)   /* (RO) PLL1 clock ready flag */
#if CHIP_DEV_PLL_CNT >= 2
#define CHIP_REGFLD_RCC_CR_PLL2_ON                  (0x00000001UL << 26U)   /* (RW) PLL2 enable */
#define CHIP_REGFLD_RCC_CR_PLL2_RDY                 (0x00000001UL << 27U)   /* (RO) PLL2 clock ready flag */
#endif
#if CHIP_DEV_PLL_CNT >= 3
#define CHIP_REGFLD_RCC_CR_PLL3_ON                  (0x00000001UL << 28U)   /* (RW) PLL3 enable */
#define CHIP_REGFLD_RCC_CR_PLL3_RDY                 (0x00000001UL << 29U)   /* (RO) PLL3 clock ready flag */
#endif
#define CHIP_REGMSK_RCC_CR_RESERVED                 (0xC0F00004UL)


/*********** Clock configuration register (RCC_CFGR) **************************/
#define CHIP_REGFLD_RCC_CFGR_SW                     (0x00000003UL <<  0U)   /* (RW) System clock switch */
#define CHIP_REGFLD_RCC_CFGR_SWS                    (0x00000003UL <<  2U)   /* (RO) System clock switch status */
#define CHIP_REGFLD_RCC_CFGR_HPRE                   (0x0000000FUL <<  4U)   /* (RW) HCLK prescaler */
#define CHIP_REGFLD_RCC_CFGR_PPRE1                  (0x00000007UL <<  8U)   /* (RW) PCLK1 prescaler */
#define CHIP_REGFLD_RCC_CFGR_PPRE2                  (0x00000007UL << 11U)   /* (RW) PCLK2 prescaler */
#define CHIP_REGFLD_RCC_CFGR_ADC_PRE                (0x00000003UL << 14U)   /* (RW) ADC prescaler  */
#define CHIP_REGFLD_RCC_CFGR_PLL1_SRC               (0x00000001UL << 16U)   /* (RW) PLL1 input clock source */
#define CHIP_REGFLD_RCC_CFGR_PLL1_XTPRE             (0x00000001UL << 17U)   /* (RW) HSE divider for PLL1 input clock (= CFGR2_PREDIV) */
#define CHIP_REGFLD_RCC_CFGR_PLL1_MUL               (0x0000000FUL << 18U)   /* (RW) PLL1 multiplication factor */
#if defined CHIP_DEV_GD32F10X || defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_RCC_CFGR_USBHD_PRE              (0x00000003UL << 22U)   /* (RW) USB OTG FS prescaler */
#else
#define CHIP_REGFLD_RCC_CFGR_USBHD_PRE              (0x00000001UL << 22U)   /* (RW) USB OTG FS prescaler */
#endif
#if defined CHIP_DEV_GD32F10X
#define CHIP_REGFLD_RCC_CFGR_MCO                    (0x00000007UL << 24U)   /* (RW) Microcontroller clock output */
#else
#define CHIP_REGFLD_RCC_CFGR_MCO                    (0x0000000FUL << 24U)   /* (RW) Microcontroller clock output */
#endif
#ifdef CHIP_DEV_GD32F10X
#define CHIP_REGFLD_RCC_CFGR_PLL1_MUL_H             (0x00000001UL << 27U)   /* (RW) PLL1_MUL[4] */
#define CHIP_REGFLD_RCC_CFGR_ADC_PRE_H              (0x00000001UL << 28U)   /* (RW) ADC_PRE[2] */
#endif
#ifdef CHIP_DEV_CH32V3XX
#if CHIP_DEV_ETH_CNT > 0
#define CHIP_REGFLD_RCC_CFGR_ETH_PRE                (0x00000001UL << 28U)   /* (RW) Ethernet clock source prescaler control (0 - div 1, 1 - div 2) */
#endif
#if CHIP_REV_NUM > 0
#define CHIP_REGFLD_RCC_CFGR_ADC_DUTY_SEL           (0x00000001UL << 30U)   /* (RW) ADC clock duty cycle selection  (0 - ADC clock duty cycle is 50%, 1 - 75 %);*/
#endif
#define CHIP_REGFLD_RCC_CFGR_ADC_CLK_ADJ            (0x00000001UL << 31U)   /* (RW) ADC clock duty cycle adjustment (0 - 50 % duty, 1 - lon low) */
#endif


#ifdef CHIP_REGFLD_RCC_CFGR_ADC_PRE_H
#define CHIP_REGMSK_RCC_CFGR_ADC_PRE                (CHIP_REGFLD_RCC_CFGR_ADC_PRE | CHIP_REGFLD_RCC_CFGR_ADC_PRE_H)
#define CHIP_REGFLDGET_RCC_CFGR_ADC_PRE(wrd)        (LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_ADC_PRE) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_ADC_PRE_H)) << 2))
#define CHIP_REGFLDSET_RCC_CFGR_ADC_PRE(val)        (LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_ADC_PRE, val) | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_ADC_PRE_H, ((val) >> 2)))
#else
#define CHIP_REGMSK_RCC_CFGR_ADC_PRE                (CHIP_REGFLD_RCC_CFGR_ADC_PRE)
#define CHIP_REGFLDGET_RCC_CFGR_ADC_PRE(wrd)        (LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_ADC_PRE))
#define CHIP_REGFLDSET_RCC_CFGR_ADC_PRE(val)        (LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_ADC_PRE, val))
#endif

#ifdef CHIP_REGFLD_RCC_CFGR_PLL1_MUL_H
#define CHIP_REGMSK_RCC_CFGR_PLL1_MUL                (CHIP_REGFLD_RCC_CFGR_PLL1_MUL | CHIP_REGFLD_RCC_CFGR_PLL1_MUL_H)
#define CHIP_REGFLDGET_RCC_CFGR_PLL1_MUL(wrd)        (LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_PLL1_MUL) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_PLL1_MUL_H)) << 4))
#define CHIP_REGFLDSET_RCC_CFGR_PLL1_MUL(val)        (LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL1_MUL, val) | LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL1_MUL_H, ((val) >> 4)))
#else
#define CHIP_REGMSK_RCC_CFGR_PLL1_MUL                (CHIP_REGFLD_RCC_CFGR_PLL1_MUL)
#define CHIP_REGFLDGET_RCC_CFGR_PLL1_MUL(wrd)        (LBITFIELD_GET(wrd, CHIP_REGFLD_RCC_CFGR_PLL1_MUL))
#define CHIP_REGFLDSET_RCC_CFGR_PLL1_MUL(val)        (LBITFIELD_SET(CHIP_REGFLD_RCC_CFGR_PLL1_MUL, val))
#endif





#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSI              0
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSE              1
#define CHIP_REGFLDVAL_RCC_CFGR_SW_PLL1             2



#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_2              8
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_4              9
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_8              10
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_16             11
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_64             12
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_128            13
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_256            14
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_512            15

#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_2              4
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_4              5
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_8              6
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_16             7

#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_2           0
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_4           1
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_6           2
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_8           3
#ifdef CHIP_DEV_GD32F10X
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_12          5
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_16          7
#endif

#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_SRC_HSI        0 /* HSI/2  */
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_SRC_HSE        1 /* HSE/PREDIV */

#if defined CHIP_DEV_GD32F10X || defined CHIP_DEV_CH32XXXX
#if defined CHIP_DEV_CH32F20X_D8C || defined CHIP_DEV_CH32V30X_D8C
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_18         0
#else
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_2          0
#endif
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_3          1
#endif
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_4          2
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_5          3
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_6          4
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_7          5
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_8          6
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_9          7
#if defined CHIP_DEV_GD32F10X || defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_10         8
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_11         9
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_12         10
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_13         11
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_14         12
#endif
#if defined CHIP_DEV_STM32F10X
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_6_5        13
#elif defined CHIP_DEV_CH32XXXX
#if defined CHIP_DEV_CH32F20X_D8C || defined CHIP_DEV_CH32V30X_D8C
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_6_5        13
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_15         14
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_16         15
#else
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_15         13
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_16         14
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_18         15
#endif
#elif defined CHIP_DEV_GD32F10X
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_15         13
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_16         14
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_17         16
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_18         17
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_19         18
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_20         19
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_21         20
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_22         21
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_23         22
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_24         23
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_25         24
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_26         25
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_27         26
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_28         27
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_29         28
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_30         29
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_31         30
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_32         31
#endif

#if defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_1         0
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_2         1
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_3         2
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_5         3
#else
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_1_5       0
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_1         1
#if defined CHIP_DEV_GD32F10X
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_2_5       2
#define CHIP_REGFLDVAL_RCC_CFGR_USBHD_PRE_2         3
#endif
#endif


#define CHIP_REGFLDVAL_RCC_CFGR_MCO_DIS             0x0 /* MCO output disabled, no clock on MCO */
//#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI14           1 /* Internal RC 14 MHz (HSI14) oscillator clock selected */
//#define CHIP_REGFLDVAL_RCC_CFGR_MCO_LSI             2 /* Internal low speed (LSI) oscillator clock selected */
//#define CHIP_REGFLDVAL_RCC_CFGR_MCO_LSE             3 /* External low speed (LSE) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_SYSCLK          0x4 /* System clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI             0x5 /* Internal RC 8 MHz (HSI) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSE             0x6 /* External 4-32 MHz (HSE) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL1_DIV2       0x7 /* PLL1 clock divided by 2 selected */
#if (CHIP_DEV_PLL_CNT >= 2)
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL2            0x8 /* PLL2 clock selected */
#endif

#if (CHIP_DEV_PLL_CNT >= 3)
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL3_DIV2       0x9 /* PLL3 clock divided by 2 selected */
#endif
#if defined CHIP_DEV_ST32F105_XC || defined CHIP_DEV_ST32F107_XC || defined CHIP_DEV_CH32V307
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_XT1             0xA /* XT1 external 3-25 MHz oscillator clock selected (for Ethernet) */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL3            0xB /* PLL3 clock selected (for Ethernet) */
#endif

/*********** Clock interrupt register (RCC_CIR) *******************************/
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_F               (0x00000001UL <<  0U)   /* (RO) LSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_F               (0x00000001UL <<  1U)   /* (RO) LSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_F               (0x00000001UL <<  2U)   /* (RO) HSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_F               (0x00000001UL <<  3U)   /* (RO) HSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_PLL1_RDY_F              (0x00000001UL <<  4U)   /* (RO) PLL1 ready interrupt flag */
#if CHIP_DEV_PLL_CNT >= 2
#define CHIP_REGFLD_RCC_CIR_PLL2_RDY_F              (0x00000001UL <<  5U)   /* (RO) PLL2 ready interrupt flag */
#endif
#if CHIP_DEV_PLL_CNT >= 3
#define CHIP_REGFLD_RCC_CIR_PLL3_RDY_F              (0x00000001UL <<  6U)   /* (RO) PLL3 ready interrupt flag */
#endif
#define CHIP_REGFLD_RCC_CIR_CSS_F                   (0x00000001UL <<  7U)   /* (RO) Clock security system interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_IE              (0x00000001UL <<  8U)   /* (RW) LSI ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_IE              (0x00000001UL <<  9U)   /* (RW) LSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_IE              (0x00000001UL << 10U)   /* (RW) HSI ready interrupt enable  */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_IE              (0x00000001UL << 11U)   /* (RW) HSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_PLL1_RDY_IE             (0x00000001UL << 12U)   /* (RW) PLL1 ready interrupt enable */
#if CHIP_DEV_PLL_CNT >= 2
#define CHIP_REGFLD_RCC_CIR_PLL2_RDY_IE             (0x00000001UL << 13U)   /* (RW) PLL2 ready interrupt enable */
#endif
#if CHIP_DEV_PLL_CNT >= 3
#define CHIP_REGFLD_RCC_CIR_PLL3_RDY_IE             (0x00000001UL << 14U)   /* (RW) PLL3 ready interrupt enable */
#endif
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_C               (0x00000001UL << 16U)   /* (W1C) LSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_C               (0x00000001UL << 17U)   /* (W1C) LSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_C               (0x00000001UL << 18U)   /* (W1C) HSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_C               (0x00000001UL << 19U)   /* (W1C) HSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_PLL1_RDY_C              (0x00000001UL << 20U)   /* (W1C) PLL ready interrupt clear */
#if CHIP_DEV_PLL_CNT >= 2
#define CHIP_REGFLD_RCC_CIR_PLL2_RDY_C              (0x00000001UL << 21U)   /* (W1C) PLL ready interrupt clear */
#endif
#if CHIP_DEV_PLL_CNT >= 3
#define CHIP_REGFLD_RCC_CIR_PLL3_RDY_C              (0x00000001UL << 22U)   /* (W1C) PLL ready interrupt clear */
#endif
#define CHIP_REGFLD_RCC_CIR_CSS_C                   (0x00000001UL << 23U)   /* (W1C) Clock security system interrupt clear */
#define CHIP_REGMSK_RCC_CIR_RESERVED                (0xFF008000UL)

/*********** APB peripheral reset register 2 (RCC_APB2RSTR) *******************/
#define CHIP_REGFLD_RCC_APB2RSTR_AFIO_RST           (0x00000001UL <<  0U)   /* (RW) AFIO reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPA_RST           (0x00000001UL <<  2U)   /* (RW) IP Port A reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPB_RST           (0x00000001UL <<  3U)   /* (RW) IP Port B reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPC_RST           (0x00000001UL <<  4U)   /* (RW) IP Port C reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPD_RST           (0x00000001UL <<  5U)   /* (RW) IP Port D reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPE_RST           (0x00000001UL <<  6U)   /* (RW) IP Port E reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPF_RST           (0x00000001UL <<  7U)   /* (RW) IP Port F reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPG_RST           (0x00000001UL <<  8U)   /* (RW) IP Port G reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_ADC1_RST           (0x00000001UL <<  9U)   /* (RW) ADC1 interface reset */
#define CHIP_REGFLD_RCC_APB2RSTR_ADC2_RST           (0x00000001UL << 10U)   /* (RW) ADC2 interface reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM1_RST           (0x00000001UL << 11U)   /* (RW) TIM1 timer reset */
#define CHIP_REGFLD_RCC_APB2RSTR_SPI1_RST           (0x00000001UL << 12U)   /* (RW) SPI1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM8_RST           (0x00000001UL << 13U)   /* (RW) TIM8 timer reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART1_RST         (0x00000001UL << 14U)   /* (RW) USART1 reset */
#if CHIP_DEV_ADC_CNT >=3
#define CHIP_REGFLD_RCC_APB2RSTR_ADC3_RST           (0x00000001UL << 15U)   /* (RW) ADC3 interface reset */
#endif
#if CHIP_DEV_SUPPORT_TIM9
#define CHIP_REGFLD_RCC_APB2RSTR_TIM9_RST           (0x00000001UL << 19U)   /* (RW) TIM9 timer reset */
#endif
#if CHIP_DEV_SUPPORT_TIM10
#define CHIP_REGFLD_RCC_APB2RSTR_TIM10_RST          (0x00000001UL << 20U)   /* (RW) TIM10 timer reset */
#endif
#if CHIP_DEV_SUPPORT_TIM11
#define CHIP_REGFLD_RCC_APB2RSTR_TIM11_RST          (0x00000001UL << 21U)   /* (RW) TIM11 timer reset */
#endif


/*********** APB peripheral reset register 1 (RCC_APB1RSTR) *******************/
#define CHIP_REGFLD_RCC_APB1RSTR_TIM2_RST           (0x00000001UL <<  0U)   /* (RW) TIM2 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM3_RST           (0x00000001UL <<  1U)   /* (RW) TIM3 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM4_RST           (0x00000001UL <<  2U)   /* (RW) TIM4 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM5_RST           (0x00000001UL <<  3U)   /* (RW) TIM5 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM6_RST           (0x00000001UL <<  4U)   /* (RW) TIM6 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM7_RST           (0x00000001UL <<  5U)   /* (RW) TIM7 timer reset */
#ifdef CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_RCC_APB1RSTR_UART6_RST          (0x00000001UL <<  6U)   /* (RW) UART6 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_UART7_RST          (0x00000001UL <<  7U)   /* (RW) UART7 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_UART8_RST          (0x00000001UL <<  8U)   /* (RW) UART8 timer reset */
#else
#if CHIP_DEV_SUPPORT_TIM12
#define CHIP_REGFLD_RCC_APB1RSTR_TIM12_RST          (0x00000001UL <<  6U)   /* (RW) TIM12 timer reset */
#endif
#if CHIP_DEV_SUPPORT_TIM13
#define CHIP_REGFLD_RCC_APB1RSTR_TIM13_RST          (0x00000001UL <<  7U)   /* (RW) TIM13 timer reset */
#endif
#if CHIP_DEV_SUPPORT_TIM14
#define CHIP_REGFLD_RCC_APB1RSTR_TIM14_RST          (0x00000001UL <<  8U)   /* (RW) TIM14 timer reset */
#endif
#endif
#define CHIP_REGFLD_RCC_APB1RSTR_WWDG_RST           (0x00000001UL << 11U)   /* (RW) Window watchdog reset */
#define CHIP_REGFLD_RCC_APB1RSTR_SPI2_RST           (0x00000001UL << 14U)   /* (RW) SPI2 reset */
#if CHIP_DEV_SPI_CNT >= 3
#define CHIP_REGFLD_RCC_APB1RSTR_SPI3_RST           (0x00000001UL << 15U)   /* (RW) SPI3 reset */
#endif
#define CHIP_REGFLD_RCC_APB1RSTR_USART2_RST         (0x00000001UL << 17U)   /* (RW) USART2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART3_RST         (0x00000001UL << 18U)   /* (RW) USART3 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_UART4_RST          (0x00000001UL << 19U)   /* (RW) UART4 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_UART5_RST          (0x00000001UL << 20U)   /* (RW) UART5 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C1_RST           (0x00000001UL << 21U)   /* (RW) I2C1 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C2_RST           (0x00000001UL << 22U)   /* (RW) I2C2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USBD_RST           (0x00000001UL << 23U)   /* (RW) USB device FS reset */
#define CHIP_REGFLD_RCC_APB1RSTR_CAN1_RST           (0x00000001UL << 25U)   /* (RW) CAN1 interface reset */
#if CHIP_DEV_CAN_CNT >= 2
#define CHIP_REGFLD_RCC_APB1RSTR_CAN2_RST           (0x00000001UL << 26U)   /* (RW) CAN2 interface reset */
#endif
#define CHIP_REGFLD_RCC_APB1RSTR_BKP_RST            (0x00000001UL << 27U)   /* (RW) Clock Recovery System interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_PWR_RST            (0x00000001UL << 28U)   /* (RW) Power interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_DAC_RST            (0x00000001UL << 29U)   /* (RW) DAC interface reset */


/*********** AHB peripheral clock enable register (RCC_AHBENR) ****************/
#define CHIP_REGFLD_RCC_AHBENR_DMA1_EN              (0x00000001UL <<  0U)   /* (RW) DMA1 clock enable */
#define CHIP_REGFLD_RCC_AHBENR_DMA2_EN              (0x00000001UL <<  1U)   /* (RW) DMA2 clock enable */
#define CHIP_REGFLD_RCC_AHBENR_SRAM_EN              (0x00000001UL <<  2U)   /* (RW) SRAM interface clock enable */
#if !defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_RCC_AHBENR_FLITF_EN             (0x00000001UL <<  4U)   /* (RW) FLITF clock enable */
#endif
#define CHIP_REGFLD_RCC_AHBENR_CRC_EN               (0x00000001UL <<  6U)   /* (RW) CRC clock enable */
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_REGFLD_RCC_AHBENR_EXMC_EN              (0x00000001UL <<  8U)   /* (RW) EXMC clock enable */
#endif
#if CHIP_DEV_SUPPORT_RNG
#define CHIP_REGFLD_RCC_AHBENR_RNG_EN               (0x00000001UL <<  9U)   /* (RW) EXMC clock enable */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_REGFLD_RCC_AHBENR_SDIO1_EN             (0x00000001UL << 10U)   /* (RW) SDIO clock enable */
#endif
#if CHIP_DEV_USBHS_CNT >= 1
#define CHIP_REGFLD_RCC_AHBENR_USBHS_EN             (0x00000001UL << 11U)   /* (RW) USB HS clock enable */
#endif
#define CHIP_REGFLD_RCC_AHBENR_USBHD_EN             (0x00000001UL << 12U)   /* (RW) USB OTG FS clock enable */
#if CHIP_DEV_DVP_CNT >= 1
#define CHIP_REGFLD_RCC_AHBENR_DVP_EN               (0x00000001UL << 13U)   /* (RW) DVP clock enable */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_REGFLD_RCC_AHBENR_ETH_MAC_EN           (0x00000001UL << 14U)   /* (RW) Ethernet MAC clock enable */
#define CHIP_REGFLD_RCC_AHBENR_ETH_MAC_TX_EN        (0x00000001UL << 15U)   /* (RW) Ethernet transmit clock enable */
#define CHIP_REGFLD_RCC_AHBENR_ETH_MAC_RX_EN        (0x00000001UL << 16U)   /* (RW) Ethernet receive clock enable */
#endif
#if CHIP_DEV_BLES_CNT >= 1
#define CHIP_REGFLD_RCC_AHBENR_BLEC                 (0x00000001UL << 16U)   /* (RW) BLEC clock enable */
#define CHIP_REGFLD_RCC_AHBENR_BLES                 (0x00000001UL << 17U)   /* (RW) BLES clock enable */
#endif


/*********** APB peripheral clock enable register 2 (RCC_APB2ENR) ****************/
#define CHIP_REGFLD_RCC_APB2ENR_AFIO_EN             (0x00000001UL <<  0U)   /* (RW) AFIO clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPA_EN             (0x00000001UL <<  2U)   /* (RW) IO Port A clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPB_EN             (0x00000001UL <<  3U)   /* (RW) IO Port B clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPC_EN             (0x00000001UL <<  4U)   /* (RW) IO Port C clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPD_EN             (0x00000001UL <<  5U)   /* (RW) IO Port D clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPE_EN             (0x00000001UL <<  6U)   /* (RW) IO Port E clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPF_EN             (0x00000001UL <<  7U)   /* (RW) IO Port F clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPG_EN             (0x00000001UL <<  8U)   /* (RW) IO Port G clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_ADC1_EN             (0x00000001UL <<  9U)   /* (RW) ADC1 interface clock enable */
#if CHIP_DEV_ADC_CNT >= 2
#define CHIP_REGFLD_RCC_APB2ENR_ADC2_EN             (0x00000001UL << 10U)   /* (RW) ADC2 interface clock enable */
#endif
#define CHIP_REGFLD_RCC_APB2ENR_TIM1_EN             (0x00000001UL << 11U)   /* (RW) TIM1 timer clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_SPI1_EN             (0x00000001UL << 12U)   /* (RW) SPI1 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM8_EN             (0x00000001UL << 13U)   /* (RW) TIM8 timer clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART1_EN           (0x00000001UL << 14U)   /* (RW) USART1 clock enable */
#if CHIP_DEV_ADC_CNT >=3
#define CHIP_REGFLD_RCC_APB2ENR_ADC3_EN             (0x00000001UL << 15U)   /* (RW) ADC3 interface clock enable */
#endif
#if CHIP_DEV_SUPPORT_TIM9
#define CHIP_REGFLD_RCC_APB2ENR_TIM9_EN             (0x00000001UL << 19U)   /* (RW) TIM15 timer clock enable */
#endif
#if CHIP_DEV_SUPPORT_TIM10
#define CHIP_REGFLD_RCC_APB2ENR_TIM10_EN            (0x00000001UL << 20U)   /* (RW) TIM16 timer clock enable */
#endif
#if CHIP_DEV_SUPPORT_TIM11
#define CHIP_REGFLD_RCC_APB2ENR_TIM11_EN            (0x00000001UL << 21U)   /* (RW) TIM17 timer clock enable */
#endif

/*********** APB peripheral clock enable register 1 (RCC_APB1ENR) ****************/
#define CHIP_REGFLD_RCC_APB1ENR_TIM2_EN             (0x00000001UL <<  0U)   /* (RW) TIM2 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM3_EN             (0x00000001UL <<  1U)   /* (RW) TIM3 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM4_EN             (0x00000001UL <<  2U)   /* (RW) TIM4 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM5_EN             (0x00000001UL <<  3U)   /* (RW) TIM5 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM6_EN             (0x00000001UL <<  4U)   /* (RW) TIM6 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM7_EN             (0x00000001UL <<  5U)   /* (RW) TIM7 timer clock enable */
#ifdef  CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_RCC_APB1ENR_UART6_EN            (0x00000001UL <<  6U)   /* (RW) UART6 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_UART7_EN            (0x00000001UL <<  7U)   /* (RW) UART7 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_UART8_EN            (0x00000001UL <<  8U)   /* (RW) UART8 timer clock enable */
#else
#if CHIP_DEV_SUPPORT_TIM12
#define CHIP_REGFLD_RCC_APB1ENR_TIM12_EN            (0x00000001UL <<  6U)   /* (RW) TIM12 timer clock enable */
#endif
#if CHIP_DEV_SUPPORT_TIM13
#define CHIP_REGFLD_RCC_APB1ENR_TIM13_EN            (0x00000001UL <<  7U)   /* (RW) TIM13 timer clock enable */
#endif
#if CHIP_DEV_SUPPORT_TIM14
#define CHIP_REGFLD_RCC_APB1ENR_TIM14_EN            (0x00000001UL <<  8U)   /* (RW) TIM14 timer clock enable */
#endif
#endif
#define CHIP_REGFLD_RCC_APB1ENR_WWDG_EN             (0x00000001UL << 11U)   /* (RW) Window watchdog clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_SPI2_EN             (0x00000001UL << 14U)   /* (RW) SPI2 clock enable */
#if CHIP_DEV_SPI_CNT >= 3
#define CHIP_REGFLD_RCC_APB1ENR_SPI3_EN             (0x00000001UL << 15U)   /* (RW) SPI3 clock enable */
#endif
#define CHIP_REGFLD_RCC_APB1ENR_USART2_EN           (0x00000001UL << 17U)   /* (RW) USART2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART3_EN           (0x00000001UL << 18U)   /* (RW) USART3 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_UART4_EN            (0x00000001UL << 19U)   /* (RW) UART4 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_UART5_EN            (0x00000001UL << 20U)   /* (RW) UART5 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C1_EN             (0x00000001UL << 21U)   /* (RW) I2C1 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C2_EN             (0x00000001UL << 22U)   /* (RW) I2C2 clock enable */
#ifndef CHIP_DEV_STM32F10X
#define CHIP_REGFLD_RCC_APB1ENR_USBD_EN             (0x00000001UL << 23U)   /* (RW) USB FS clock enable */
#endif
#if CHIP_DEV_CAN_CNT >= 1
#define CHIP_REGFLD_RCC_APB1ENR_CAN1_EN             (0x00000001UL << 25U)   /* (RW) CAN1 interface clock enable */
#endif
#if CHIP_DEV_CAN_CNT >= 2
#define CHIP_REGFLD_RCC_APB1ENR_CAN2_EN             (0x00000001UL << 26U)   /* (RW) CAN2 interface clock enable */
#endif
#define CHIP_REGFLD_RCC_APB1ENR_BKP_EN              (0x00000001UL << 27U)   /* (RW) Backup interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_PWR_EN              (0x00000001UL << 28U)   /* (RW) Power interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_DAC_EN              (0x00000001UL << 29U)   /* (RW) DAC interface clock enable */


/*********** RTC domain control register (RCC_BDCR) ***************************/
#define CHIP_REGFLD_RCC_BDCR_LSE_ON                 (0x00000001UL <<  0U)   /* (RW) LSE oscillator enable */
#define CHIP_REGFLD_RCC_BDCR_LSE_RDY                (0x00000001UL <<  1U)   /* (RO) LSE oscillator ready */
#define CHIP_REGFLD_RCC_BDCR_LSE_BYP                (0x00000001UL <<  2U)   /* (RW) LSE oscillator bypass */
#define CHIP_REGFLD_RCC_BDCR_RTC_SEL                (0x00000003UL <<  8U)   /* (RW) RTC clock source selection */
#define CHIP_REGFLD_RCC_BDCR_RTC_EN                 (0x00000001UL << 15U)   /* (RW) RTC clock enable */
#define CHIP_REGFLD_RCC_BDCR_BD_RST                 (0x00000001UL << 16U)   /* (RW) RTC domain software reset */
#define CHIP_REGMSK_RCC_BDCR_RESERVED               (0xFFFE7CF8UL)

#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_DIS         0
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSE         1
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSI         2
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_HSE         3 /* ch32v307 - HSE/128 */

/*********** Control/status register (RCC_CSR) ********************************/
#define CHIP_REGFLD_RCC_CSR_LSI_ON                  (0x00000001UL <<  0U)   /* (RW) LSI oscillator enable */
#define CHIP_REGFLD_RCC_CSR_LSI_RDY                 (0x00000001UL <<  1U)   /* (RO) LSI oscillator ready */
#define CHIP_REGFLD_RCC_CSR_RMVF                    (0x00000001UL << 24U)   /* (W1) Remove reset flag */
#define CHIP_REGFLD_RCC_CSR_PIN_RSTF                (0x00000001UL << 26U)   /* (RO) PIN reset flag */
#define CHIP_REGFLD_RCC_CSR_POR_RSTF                (0x00000001UL << 27U)   /* (RO) POR/PDR reset flag */
#define CHIP_REGFLD_RCC_CSR_SFT_RSTF                (0x00000001UL << 28U)   /* (RO) Software reset flag */
#define CHIP_REGFLD_RCC_CSR_IWDG_RSTF               (0x00000001UL << 29U)   /* (RO) Independent watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_WWDG_RSTF               (0x00000001UL << 30U)   /* (RO) Window watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_LPWR_RSTF               (0x00000001UL << 31U)   /* (RO) Low-power reset flag */
#define CHIP_REGMSK_RCC_CSR_RESERVED                (0x02FFFFFCUL)

/*********** AHB peripheral reset register (RCC_AHBRSTR) **********************/
#define CHIP_REGFLD_RCC_AHBRSTR_USBHD_RST           (0x00000001UL << 12U)   /* (RW) USB OTG FS reset */
#if CHIP_DEV_DVP_CNT >= 1
#define CHIP_REGFLD_RCC_AHBRSTR_DVP_RST             (0x00000001UL << 13U)   /* (RW) DVP reset */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_REGFLD_RCC_AHBRSTR_ETHMAC_RST          (0x00000001UL << 14U)   /* (RW) Ethernet MAC reset */
#endif
#define CHIP_REGMSK_RCC_AHBRSTR_RESERVED            (0xFFFF8FFFUL)

/*********** Clock configuration register 2 (RCC_CFGR2) ***********************/
#define CHIP_REGFLD_RCC_CFGR2_PREDIV1               (0x0000000FUL <<  0U)   /* (RW) PREDIV1 division factor */
#define CHIP_REGFLD_RCC_CFGR2_PREDIV2               (0x0000000FUL <<  4U)   /* (RW) PREDIV2 division factor */
#if CHIP_DEV_PLL_CNT >= 2
#define CHIP_REGFLD_RCC_CFGR2_PLL2_MUL              (0x0000000FUL <<  8U)   /* (RW) PLL2 Multiplication Factor */
#endif
#if CHIP_DEV_PLL_CNT >= 3
#define CHIP_REGFLD_RCC_CFGR2_PLL3_MUL              (0x0000000FUL << 12U)   /* (RW) PLL3 Multiplication Factor */
#endif
#define CHIP_REGFLD_RCC_CFGR2_PREDIV1_SRC           (0x00000001UL << 16U)   /* (RW) PREDIV1 entry clock source */
#define CHIP_REGFLD_RCC_CFGR2_I2S2_SRC              (0x00000001UL << 17U)   /* (RW) I2S2 clock source */
#define CHIP_REGFLD_RCC_CFGR2_I2S3_SRC              (0x00000001UL << 18U)   /* (RW) I2S3 clock source */
#ifdef CHIP_DEV_SUPPORT_RNG
#define CHIP_REGFLD_RCC_CFGR2_RNG_SRC               (0x00000001UL << 19U)   /* (RW) RNG clock source selection */
#endif
#if defined CHIP_DEV_CH32XXXX && (CHIP_DEV_ETH_CNT > 0)
#define CHIP_REGFLD_RCC_CFGR2_ETH1G_SRC             (0x00000003UL << 20U)   /* (RW) Gigabit Ethernet 125M clock selection */
#define CHIP_REGFLD_RCC_CFGR2_ETH1G_125_EN          (0x00000001UL << 22U)   /* (RW) Gigabit Ethernet 125M clock enable  */
#endif
#if defined CHIP_DEV_CH32XXXX && (CHIP_DEV_USBHS_CNT > 0)
#define CHIP_REGFLD_RCC_CFGR2_USBHS_PREDIV          (0x00000007UL << 24U)   /* (RW) USBHS PLL prescaler */
#define CHIP_REGFLD_RCC_CFGR2_USBHS_PLL_SRC         (0x00000001UL << 27U)   /* (RW) USBHS PLL reference source selection */
#define CHIP_REGFLD_RCC_CFGR2_USBHS_CK_REFSEL       (0x00000003UL << 28U)   /* (RW) USBHS PLL reference clock selection (USBHS_PLL_SRC/ USBHS_PREDIV) */
#define CHIP_REGFLD_RCC_CFGR2_USBHS_PLL_ACTIVE      (0x00000001UL << 30U)   /* (RW) USBHS PHY internal PLL control bit */
#define CHIP_REGFLD_RCC_CFGR2_USBHS_CLK_SRC         (0x00000001UL << 31U)   /* (RW) USBHS 48MHz clock source  */
#endif

#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_CLK_SRC_PLL      0
#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_CLK_SRC_PHY      1

#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_CK_REFSEL_3_MHZ  0
#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_CK_REFSEL_4_MHZ  1
#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_CK_REFSEL_8_MHZ  2
#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_CK_REFSEL_5_MHZ  3

#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_PLL_SRC_HSE      0
#define CHIP_REGFLDVAL_RCC_CFGR2_USBHS_PLL_SRC_HSI      1

#define CHIP_REGFLDVAL_RCC_CFGR2_ETH1G_SRC_PLL2_VCO     0
#define CHIP_REGFLDVAL_RCC_CFGR2_ETH1G_SRC_PLL3_VCO     1
#define CHIP_REGFLDVAL_RCC_CFGR2_ETH1G_SRC_PB1_VCO      2

#define CHIP_REGFLDVAL_RCC_CFGR2_RNG_SRC_SYSCLK         0
#define CHIP_REGFLDVAL_RCC_CFGR2_RNG_SRC_PLL3_VCO       1

#define CHIP_REGFLDVAL_RCC_CFGR2_I2S_SRC_SYSCLK         0
#define CHIP_REGFLDVAL_RCC_CFGR2_I2S_SRC_PLL3_VCO       1

#define CHIP_REGFLDVAL_RCC_CFGR2_PREDIV1_SRC_HSE        0
#define CHIP_REGFLDVAL_RCC_CFGR2_PREDIV1_SRC_PLL2       1

#ifdef CHIP_DEV_CH32XXXX
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_2_5            0x0
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_12_5           0x1
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_4              0x2
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_5              0x3
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_6              0x4
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_7              0x5
#endif
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_8              0x6
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_9              0x7
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_10             0x8
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_11             0x9
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_12             0xA
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_13             0xB
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_14             0xC
#ifdef CHIP_DEV_CH32XXXX
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_15             0xD
#endif
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_16             0xE
#define CHIP_REGFLDVAL_RCC_CFGR2_PLL_MUL_20             0xF

/*********** RCC Deep-sleep mode voltage register (RCC_DSV) ***********************/
#define CHIP_REGFLD_RCC_DSV_DSLPVS                      (0x00000007UL <<  0U)   /* (RW) Deep-sleep mode voltage select */
#define CHIP_REGMSK_RCC_DSV_RESERVED                    (0xFFFFFFF8UL)

#define CHIP_REGFLDVAL_RCC_DSV_DSLPVS_1_2V              0
#define CHIP_REGFLDVAL_RCC_DSV_DSLPVS_1_1V              1
#define CHIP_REGFLDVAL_RCC_DSV_DSLPVS_1_0V              2
#define CHIP_REGFLDVAL_RCC_DSV_DSLPVS_0_9V              3


/*********** System configuration registers (RCC_SYSCFG) **********************/
#define CHIP_REGFLD_RCC_SYSCFG_CEE                      (0x00000001UL <<  7U)   /* (RW) Code Execution Effifiency */
#define CHIP_REGMSK_RCC_SYSCFG_RESERVED                 (0xFFFFFF7FUL)


#if defined CHIP_DEV_CH32XXXX
/*********** HSE crystal oscillator calibration control (HSE_CAL_CTRL) ****/
#define CHIP_REGFLD_HSE_CAL_CTRL_C                      (0x0000000FUL << 28U)   /* (RW) HSE internal match capacitor adjustment */
#define CHIP_REGFLD_HSE_CAL_CTRL_FAULT_DIS              (0x00000001UL << 27U)   /* (RW) HSE fault detection disable */
#define CHIP_REGFLD_HSE_CAL_CTRL_ITRIM                  (0x00000003UL << 24U)   /* (RW)  */
#define CHIP_REGMSK_HSE_CAL_CTRL_RESERVED               (0x04FFFFFFUL)

#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_22PF              7
#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_20PF              6
#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_18PF              5
#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_16PF              4
#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_14PF              3
#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_12PF              2
#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_10PF              1
#define CHIP_REGFLDVAL_HSE_CAL_CTRL_C_8PF               0


/*********** LSI crystal oscillator calibration tune  (LSI32K_TUNE) ***********/
#define CHIP_REGFLD_LSI32K_TUNE_HTUNE                   (0x00FFUL << 5) /* (RW) LSI32K fine tune */
#define CHIP_REGFLD_LSI32K_TUNE_LTUNE                   (0x001FUL << 0) /* (RW) LSI32K coarse tune */
#define CHIP_REGMSK_LSI32K_TUNE_RESERVED                (0xE000UL)

/****** LSI crystal oscillator calibration configuration (LSI32K_CAL_CFG) *****/
#define CHIP_REGFLD_LSI32K_CAL_CFG_LP_EN                (0x01UL << 6) /* (RW) Calibration enable in low power mode */
#define CHIP_REGFLD_LSI32K_CAL_CFG_WKUP_EN              (0x01UL << 5) /* (RW) LSI32K wake-up interrupt enable */
#define CHIP_REGFLD_LSI32K_CAL_CFG_HALT_MD              (0x01UL << 4) /* (RW) LSI32K calibration count halt duration configuration */
#define CHIP_REGFLD_LSI32K_CAL_CFG_CNT_VLU              (0x0FUL << 0) /* (RW) LSI32K calibration sampling duration configuration */
#define CHIP_REGMSK_LSI32K_CAL_CFG_RESERVED             (0x80)

#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_HALT_MD_1CK       0 /* Count halts for 1 CK32K cycle */
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_HALT_MD_3CK       1 /* Count halts for 3 CK32K cycle */

#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_2CK       0
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_4CK       1
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_32CK      2
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_64CK      3
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_128CK     4
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_256CK     5
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_512CK     6
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_1024_CK   7
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_1088CK    8
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_1152CK    9
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_1216CK    10
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_1280CK    11
#define CHIP_REGFLDVAL_LSI32K_CAL_CFG_CNT_VLU_2000CK    12

/****** LSI crystal oscillator calibration status (LSI32K_CAL_STATR) **********/
#define CHIP_REGFLD_LSI32K_CAL_STATR_IF_END             (0x0001UL << 15) /* (RW1C) LSI32K calibration count end interrupt flag */
#define CHIP_REGFLD_LSI32K_CAL_STATR_CNT_OV             (0x0001UL << 14) /* (RW1C) LSI32K sampling counter overflow flag */
#define CHIP_REGFLD_LSI32K_CAL_STATR_CNT                (0x3FFFUL <<  0) /* (RO) Count for several CK32K cycles based on system clock frequency */

/****** LSI crystal oscillator calibration status (LSI32K_CAL_CTRL) ***********/
#define CHIP_REGFLD_LSI32K_CAL_CTRL_HALT                (0x01UL << 7)   /* (RO) LSI32K calibration count halt. */
#define CHIP_REGFLD_LSI32K_CAL_CTRL_EN                  (0x01UL << 1)   /* (RO) LSI32K calibration enable */
#define CHIP_REGFLD_LSI32K_CAL_CTRL_IN                  (0x01UL << 0)   /* (RO) LSI32K calibration interrupt enable */
#define CHIP_REGMSK_LSI32K_RESERVED                     (0x7CUL)
#endif



#endif // CHIP_STM32F10X_REGS_RCC_H
