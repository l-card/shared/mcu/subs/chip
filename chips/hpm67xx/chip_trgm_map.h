#ifndef CHIP_TRGM_MAP_H
#define CHIP_TRGM_MAP_H

/*********** TRGM0_INPUT MUX list *********************************************/
#define CHIP_TRGM0_IN_MUX_VSS                0  /* Low level */
#define CHIP_TRGM0_IN_MUX_VDD                1  /* High level */
#define CHIP_TRGM0_IN_MUX_TRGM0_P0           2  /* Trigger Manager 0 input 0 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P1           3  /* Trigger Manager 0 input 1 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P2           4  /* Trigger Manager 0 input 2 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P3           5  /* Trigger Manager 0 input 3 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P4           6  /* Trigger Manager 0 input 4 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P5           7  /* Trigger Manager 0 input 5 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P6           8  /* Trigger Manager 0 input 6 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P7           9  /* Trigger Manager 0 input 7 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P8          10  /* Trigger Manager 0 input 8 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P9          11  /* Trigger Manager 0 input 9 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P10         12  /* Trigger Manager 0 input 10 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM0_P11         13  /* Trigger Manager 0 input 11 (from IO) */
#define CHIP_TRGM0_IN_MUX_TRGM3_OUTX0       14  /* Trigger Manager 3 output X0 */
#define CHIP_TRGM0_IN_MUX_TRGM3_OUTX1       15  /* Trigger Manager 3 output X1 */
#define CHIP_TRGM0_IN_MUX_TRGM2_OUTX0       16  /* Trigger Manager 2 output X0 */
#define CHIP_TRGM0_IN_MUX_TRGM2_OUTX1       17  /* Trigger Manager 2 output X1 */
#define CHIP_TRGM0_IN_MUX_TRGM1_OUTX0       18  /* Trigger Manager 1 output X0 */
#define CHIP_TRGM0_IN_MUX_TRGM1_OUTX1       19  /* Trigger Manager 1 output X1 */
#define CHIP_TRGM0_IN_MUX_PWM0_CH8REF       20  /* PWM Timer 0 Channel 8 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH9REF       21  /* PWM Timer 0 Channel 9 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH10REF      22  /* PWM Timer 0 Channel 10 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH11REF      23  /* PWM Timer 0 Channel 11 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH12REF      24  /* PWM Timer 0 Channel 12 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH13REF      25  /* PWM Timer 0 Channel 13 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH14REF      26  /* PWM Timer 0 Channel 14 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH15REF      27  /* PWM Timer 0 Channel 15 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH16REF      28  /* PWM Timer 0 Channel 16 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH17REF      29  /* PWM Timer 0 Channel 17 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH18REF      30  /* PWM Timer 0 Channel 18 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH19REF      31  /* PWM Timer 0 Channel 19 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH20REF      32  /* PWM Timer 0 Channel 20 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH21REF      33  /* PWM Timer 0 Channel 21 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH22REF      34  /* PWM Timer 0 Channel 22 Reference Output */
#define CHIP_TRGM0_IN_MUX_PWM0_CH23REF      35  /* PWM Timer 0 Channel 23 Reference Output */
#define CHIP_TRGM0_IN_MUX_QEI0_TRGO         36  /* Quadrature decoder 0 trigger output */
#define CHIP_TRGM0_IN_MUX_HALL0_TRGO        37  /* Hall sensor 0 interface trigger output */
#define CHIP_TRGM0_IN_MUX_USB0_SOF          38  /* USB0 starts of frame */
#define CHIP_TRGM0_IN_MUX_USB1_SOF          39  /* USB1 starts of frame */
#define CHIP_TRGM0_IN_MUX_ENET0_PTP_OUT3    40  /* Ethernet controller 0 PTP output 3 */
#define CHIP_TRGM0_IN_MUX_ENET1_PTP_OUT3    41  /* Ethernet controller 1 PTP output 3 */
#define CHIP_TRGM0_IN_MUX_PTPC_CMP0         42  /* Precision Time Protocol Module PTPC Output Compare 0 */
#define CHIP_TRGM0_IN_MUX_PTPC_CMP1         43  /* Precision Time Protocol Module PTPC Output Compare 1 */
#define CHIP_TRGM0_IN_MUX_SYNT0_CH0         44  /* Timer synchronization channel 0 */
#define CHIP_TRGM0_IN_MUX_SYNT0_CH1         45  /* Timer synchronization channel 1 */
#define CHIP_TRGM0_IN_MUX_SYNT0_CH2         46  /* Timer synchronization channel 2 */
#define CHIP_TRGM0_IN_MUX_SYNT0_CH3         47  /* Timer synchronization channel 3 */
#define CHIP_TRGM0_IN_MUX_GPTMR0_OUT2       48  /* General purpose timer 0 channel 2 */
#define CHIP_TRGM0_IN_MUX_GPTMR0_OUT3       49  /* General purpose timer 0 channel 3 */
#define CHIP_TRGM0_IN_MUX_GPTMR1_OUT2       50  /* General purpose timer 1 channel 2 */
#define CHIP_TRGM0_IN_MUX_GPTMR1_OUT3       51  /* General purpose timer 1 channel 3 */
#define CHIP_TRGM0_IN_MUX_CMP0_OUT          52  /* Comparator 0 output */
#define CHIP_TRGM0_IN_MUX_CMP1_OUT          53  /* Comparator 1 output */
#define CHIP_TRGM0_IN_MUX_CMP2_OUT          54  /* Comparator 2 output */
#define CHIP_TRGM0_IN_MUX_CMP3_OUT          55  /* Comparator 3 output */
#define CHIP_TRGM0_IN_MUX_DEBUG_FLAG        56  /* Debug mode entry flag */


/*********** TRGM1_INPUT MUX list *********************************************/
#define CHIP_TRGM1_IN_MUX_VSS                0  /* Low level */
#define CHIP_TRGM1_IN_MUX_VDD                1  /* High level */
#define CHIP_TRGM1_IN_MUX_TRGM1_P0           2  /* Trigger Manager 1 input 0 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P1           3  /* Trigger Manager 1 input 1 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P2           4  /* Trigger Manager 1 input 2 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P3           5  /* Trigger Manager 1 input 3 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P4           6  /* Trigger Manager 1 input 4 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P5           7  /* Trigger Manager 1 input 5 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P6           8  /* Trigger Manager 1 input 6 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P7           9  /* Trigger Manager 1 input 7 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P8          10  /* Trigger Manager 1 input 8 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P9          11  /* Trigger Manager 1 input 9 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P10         12  /* Trigger Manager 1 input 10 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM1_P11         13  /* Trigger Manager 1 input 11 (from IO) */
#define CHIP_TRGM1_IN_MUX_TRGM3_OUTX0       14  /* Trigger Manager 3 output X0 */
#define CHIP_TRGM1_IN_MUX_TRGM3_OUTX1       15  /* Trigger Manager 3 output X1 */
#define CHIP_TRGM1_IN_MUX_TRGM2_OUTX0       16  /* Trigger Manager 2 output X0 */
#define CHIP_TRGM1_IN_MUX_TRGM2_OUTX1       17  /* Trigger Manager 2 output X1 */
#define CHIP_TRGM1_IN_MUX_TRGM0_OUTX0       18  /* Trigger Manager 0 output X0 */
#define CHIP_TRGM1_IN_MUX_TRGM0_OUTX1       19  /* Trigger Manager 0 output X1 */
#define CHIP_TRGM1_IN_MUX_PWM1_CH8REF       20  /* PWM Timer 1 Channel 8 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH9REF       21  /* PWM Timer 1 Channel 9 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH10REF      22  /* PWM Timer 1 Channel 10 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH11REF      23  /* PWM Timer 1 Channel 11 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH12REF      24  /* PWM Timer 1 Channel 12 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH13REF      25  /* PWM Timer 1 Channel 13 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH14REF      26  /* PWM Timer 1 Channel 14 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH15REF      27  /* PWM Timer 1 Channel 15 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH16REF      28  /* PWM Timer 1 Channel 16 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH17REF      29  /* PWM Timer 1 Channel 17 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH18REF      30  /* PWM Timer 1 Channel 18 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH19REF      31  /* PWM Timer 1 Channel 19 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH20REF      32  /* PWM Timer 1 Channel 20 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH21REF      33  /* PWM Timer 1 Channel 21 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH22REF      34  /* PWM Timer 1 Channel 22 Reference Output */
#define CHIP_TRGM1_IN_MUX_PWM1_CH23REF      35  /* PWM Timer 1 Channel 23 Reference Output */
#define CHIP_TRGM1_IN_MUX_QEI1_TRGO         36  /* Quadrature decoder 1 trigger output */
#define CHIP_TRGM1_IN_MUX_HALL1_TRGO        37  /* Hall sensor 1 interface trigger output */
#define CHIP_TRGM1_IN_MUX_USB0_SOF          38  /* USB0 starts of frame */
#define CHIP_TRGM1_IN_MUX_USB1_SOF          39  /* USB1 starts of frame */
#define CHIP_TRGM1_IN_MUX_ENET0_PTP_OUT3    40  /* Ethernet controller 0 PTP output 3 */
#define CHIP_TRGM1_IN_MUX_ENET1_PTP_OUT3    41  /* Ethernet controller 1 PTP output 3 */
#define CHIP_TRGM1_IN_MUX_PTPC_CMP0         42  /* Precision Time Protocol Module PTPC Output Compare 0 */
#define CHIP_TRGM1_IN_MUX_PTPC_CMP1         43  /* Precision Time Protocol Module PTPC Output Compare 1 */
#define CHIP_TRGM1_IN_MUX_SYNT0_CH0         44  /* Timer synchronization channel 0 */
#define CHIP_TRGM1_IN_MUX_SYNT0_CH1         45  /* Timer synchronization channel 1 */
#define CHIP_TRGM1_IN_MUX_SYNT0_CH2         46  /* Timer synchronization channel 2 */
#define CHIP_TRGM1_IN_MUX_SYNT0_CH3         47  /* Timer synchronization channel 3 */
#define CHIP_TRGM1_IN_MUX_GPTMR2_OUT2       48  /* General purpose timer 2 channel 2 */
#define CHIP_TRGM1_IN_MUX_GPTMR2_OUT3       49  /* General purpose timer 2 channel 3 */
#define CHIP_TRGM1_IN_MUX_GPTMR3_OUT2       50  /* General purpose timer 3 channel 2 */
#define CHIP_TRGM1_IN_MUX_GPTMR3_OUT3       51  /* General purpose timer 3 channel 3 */
#define CHIP_TRGM1_IN_MUX_CMP0_OUT          52  /* Comparator 0 output */
#define CHIP_TRGM1_IN_MUX_CMP1_OUT          53  /* Comparator 1 output */
#define CHIP_TRGM1_IN_MUX_CMP2_OUT          54  /* Comparator 2 output */
#define CHIP_TRGM1_IN_MUX_CMP3_OUT          55  /* Comparator 3 output */
#define CHIP_TRGM1_IN_MUX_DEBUG_FLAG        56  /* Debug mode entry flag */



/*********** TRGM2_INPUT MUX list *********************************************/
#define CHIP_TRGM2_IN_MUX_VSS                0  /* Low level */
#define CHIP_TRGM2_IN_MUX_VDD                1  /* High level */
#define CHIP_TRGM2_IN_MUX_TRGM2_P0           2  /* Trigger Manager 2 input 0 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P1           3  /* Trigger Manager 2 input 1 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P2           4  /* Trigger Manager 2 input 2 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P3           5  /* Trigger Manager 2 input 3 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P4           6  /* Trigger Manager 2 input 4 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P5           7  /* Trigger Manager 2 input 5 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P6           8  /* Trigger Manager 2 input 6 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P7           9  /* Trigger Manager 2 input 7 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P8          10  /* Trigger Manager 2 input 8 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P9          11  /* Trigger Manager 2 input 9 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P10         12  /* Trigger Manager 2 input 10 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM2_P11         13  /* Trigger Manager 2 input 11 (from IO) */
#define CHIP_TRGM2_IN_MUX_TRGM3_OUTX0       14  /* Trigger Manager 3 output X0 */
#define CHIP_TRGM2_IN_MUX_TRGM3_OUTX1       15  /* Trigger Manager 3 output X1 */
#define CHIP_TRGM2_IN_MUX_TRGM1_OUTX0       16  /* Trigger Manager 1 output X0 */
#define CHIP_TRGM2_IN_MUX_TRGM1_OUTX1       17  /* Trigger Manager 1 output X1 */
#define CHIP_TRGM2_IN_MUX_TRGM0_OUTX0       18  /* Trigger Manager 0 output X0 */
#define CHIP_TRGM2_IN_MUX_TRGM0_OUTX1       19  /* Trigger Manager 0 output X1 */
#define CHIP_TRGM2_IN_MUX_PWM2_CH8REF       20  /* PWM Timer 2 Channel 8 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH9REF       21  /* PWM Timer 2 Channel 9 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH10REF      22  /* PWM Timer 2 Channel 10 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH11REF      23  /* PWM Timer 2 Channel 11 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH12REF      24  /* PWM Timer 2 Channel 12 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH13REF      25  /* PWM Timer 2 Channel 13 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH14REF      26  /* PWM Timer 2 Channel 14 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH15REF      27  /* PWM Timer 2 Channel 15 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH16REF      28  /* PWM Timer 2 Channel 16 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH17REF      29  /* PWM Timer 2 Channel 17 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH18REF      30  /* PWM Timer 2 Channel 18 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH19REF      31  /* PWM Timer 2 Channel 19 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH20REF      32  /* PWM Timer 2 Channel 20 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH21REF      33  /* PWM Timer 2 Channel 21 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH22REF      34  /* PWM Timer 2 Channel 22 Reference Output */
#define CHIP_TRGM2_IN_MUX_PWM2_CH23REF      35  /* PWM Timer 2 Channel 23 Reference Output */
#define CHIP_TRGM2_IN_MUX_QEI2_TRGO         36  /* Quadrature decoder 2 trigger output */
#define CHIP_TRGM2_IN_MUX_HALL2_TRGO        37  /* Hall sensor 2 interface trigger output */
#define CHIP_TRGM2_IN_MUX_USB0_SOF          38  /* USB0 starts of frame */
#define CHIP_TRGM2_IN_MUX_USB1_SOF          39  /* USB1 starts of frame */
#define CHIP_TRGM2_IN_MUX_ENET0_PTP_OUT3    40  /* Ethernet controller 0 PTP output 3 */
#define CHIP_TRGM2_IN_MUX_ENET1_PTP_OUT3    41  /* Ethernet controller 1 PTP output 3 */
#define CHIP_TRGM2_IN_MUX_PTPC_CMP0         42  /* Precision Time Protocol Module PTPC Output Compare 0 */
#define CHIP_TRGM2_IN_MUX_PTPC_CMP1         43  /* Precision Time Protocol Module PTPC Output Compare 1 */
#define CHIP_TRGM2_IN_MUX_SYNT0_CH0         44  /* Timer synchronization channel 0 */
#define CHIP_TRGM2_IN_MUX_SYNT0_CH1         45  /* Timer synchronization channel 1 */
#define CHIP_TRGM2_IN_MUX_SYNT0_CH2         46  /* Timer synchronization channel 2 */
#define CHIP_TRGM2_IN_MUX_SYNT0_CH3         47  /* Timer synchronization channel 3 */
#define CHIP_TRGM2_IN_MUX_GPTMR4_OUT2       48  /* General purpose timer 4 channel 2 */
#define CHIP_TRGM2_IN_MUX_GPTMR4_OUT3       49  /* General purpose timer 4 channel 3 */
#define CHIP_TRGM2_IN_MUX_GPTMR5_OUT2       50  /* General purpose timer 5 channel 2 */
#define CHIP_TRGM2_IN_MUX_GPTMR5_OUT3       51  /* General purpose timer 5 channel 3 */
#define CHIP_TRGM2_IN_MUX_CMP0_OUT          52  /* Comparator 0 output */
#define CHIP_TRGM2_IN_MUX_CMP1_OUT          53  /* Comparator 1 output */
#define CHIP_TRGM2_IN_MUX_CMP2_OUT          54  /* Comparator 2 output */
#define CHIP_TRGM2_IN_MUX_CMP3_OUT          55  /* Comparator 3 output */
#define CHIP_TRGM2_IN_MUX_DEBUG_FLAG        56  /* Debug mode entry flag */



/*********** TRGM3_INPUT MUX list *********************************************/
#define CHIP_TRGM3_IN_MUX_VSS                0  /* Low level */
#define CHIP_TRGM3_IN_MUX_VDD                1  /* High level */
#define CHIP_TRGM3_IN_MUX_TRGM3_P0           2  /* Trigger Manager 3 input 0 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P1           3  /* Trigger Manager 3 input 1 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P2           4  /* Trigger Manager 3 input 2 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P3           5  /* Trigger Manager 3 input 3 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P4           6  /* Trigger Manager 3 input 4 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P5           7  /* Trigger Manager 3 input 5 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P6           8  /* Trigger Manager 3 input 6 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P7           9  /* Trigger Manager 3 input 7 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P8          10  /* Trigger Manager 3 input 8 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P9          11  /* Trigger Manager 3 input 9 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P10         12  /* Trigger Manager 3 input 10 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM3_P11         13  /* Trigger Manager 3 input 11 (from IO) */
#define CHIP_TRGM3_IN_MUX_TRGM2_OUTX0       14  /* Trigger Manager 2 output X0 */
#define CHIP_TRGM3_IN_MUX_TRGM2_OUTX1       15  /* Trigger Manager 2 output X1 */
#define CHIP_TRGM3_IN_MUX_TRGM1_OUTX0       16  /* Trigger Manager 1 output X0 */
#define CHIP_TRGM3_IN_MUX_TRGM1_OUTX1       17  /* Trigger Manager 1 output X1 */
#define CHIP_TRGM3_IN_MUX_TRGM0_OUTX0       18  /* Trigger Manager 0 output X0 */
#define CHIP_TRGM3_IN_MUX_TRGM0_OUTX1       19  /* Trigger Manager 0 output X1 */
#define CHIP_TRGM3_IN_MUX_PWM3_CH8REF       20  /* PWM Timer 3 Channel 8 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH9REF       21  /* PWM Timer 3 Channel 9 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH10REF      22  /* PWM Timer 3 Channel 10 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH11REF      23  /* PWM Timer 3 Channel 11 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH12REF      24  /* PWM Timer 3 Channel 12 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH13REF      25  /* PWM Timer 3 Channel 13 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH14REF      26  /* PWM Timer 3 Channel 14 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH15REF      27  /* PWM Timer 3 Channel 15 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH16REF      28  /* PWM Timer 3 Channel 16 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH17REF      29  /* PWM Timer 3 Channel 17 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH18REF      30  /* PWM Timer 3 Channel 18 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH19REF      31  /* PWM Timer 3 Channel 19 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH20REF      32  /* PWM Timer 3 Channel 20 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH21REF      33  /* PWM Timer 3 Channel 21 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH22REF      34  /* PWM Timer 3 Channel 22 Reference Output */
#define CHIP_TRGM3_IN_MUX_PWM3_CH23REF      35  /* PWM Timer 3 Channel 23 Reference Output */
#define CHIP_TRGM3_IN_MUX_QEI3_TRGO         36  /* Quadrature decoder 3 trigger output */
#define CHIP_TRGM3_IN_MUX_HALL3_TRGO        37  /* Hall sensor 3 interface trigger output */
#define CHIP_TRGM3_IN_MUX_USB0_SOF          38  /* USB0 starts of frame */
#define CHIP_TRGM3_IN_MUX_USB1_SOF          39  /* USB1 starts of frame */
#define CHIP_TRGM3_IN_MUX_ENET0_PTP_OUT3    40  /* Ethernet controller 0 PTP output 3 */
#define CHIP_TRGM3_IN_MUX_ENET1_PTP_OUT3    41  /* Ethernet controller 1 PTP output 3 */
#define CHIP_TRGM3_IN_MUX_PTPC_CMP0         42  /* Precision Time Protocol Module PTPC Output Compare 0 */
#define CHIP_TRGM3_IN_MUX_PTPC_CMP1         43  /* Precision Time Protocol Module PTPC Output Compare 1 */
#define CHIP_TRGM3_IN_MUX_SYNT0_CH0         44  /* Timer synchronization channel 0 */
#define CHIP_TRGM3_IN_MUX_SYNT0_CH1         45  /* Timer synchronization channel 1 */
#define CHIP_TRGM3_IN_MUX_SYNT0_CH2         46  /* Timer synchronization channel 2 */
#define CHIP_TRGM3_IN_MUX_SYNT0_CH3         47  /* Timer synchronization channel 3 */
#define CHIP_TRGM3_IN_MUX_GPTMR6_OUT2       48  /* General purpose timer 6 channel 2 */
#define CHIP_TRGM3_IN_MUX_GPTMR6_OUT3       49  /* General purpose timer 6 channel 3 */
#define CHIP_TRGM3_IN_MUX_GPTMR7_OUT2       50  /* General purpose timer 7 channel 2 */
#define CHIP_TRGM3_IN_MUX_GPTMR7_OUT3       51  /* General purpose timer 7 channel 3 */
#define CHIP_TRGM3_IN_MUX_CMP0_OUT          52  /* Comparator 0 output */
#define CHIP_TRGM3_IN_MUX_CMP1_OUT          53  /* Comparator 1 output */
#define CHIP_TRGM3_IN_MUX_CMP2_OUT          54  /* Comparator 2 output */
#define CHIP_TRGM3_IN_MUX_CMP3_OUT          55  /* Comparator 3 output */
#define CHIP_TRGM3_IN_MUX_DEBUG_FLAG        56  /* Debug mode entry flag */



/*********** TRGM0_OUTPUT MUX list ********************************************/
#define CHIP_TRGM0_OUT_MUX_TRGM0_P0          0  /* Trigger Manager 0 output 0 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P1          1  /* Trigger Manager 0 output 1 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P2          2  /* Trigger Manager 0 output 2 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P3          3  /* Trigger Manager 0 output 3 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P4          4  /* Trigger Manager 0 output 4 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P5          5  /* Trigger Manager 0 output 5 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P6          6  /* Trigger Manager 0 output 6 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P7          7  /* Trigger Manager 0 output 7 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P8          8  /* Trigger Manager 0 output 8 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P9          9  /* Trigger Manager 0 output 9 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P10        10  /* Trigger Manager 0 output 10 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_P11        11  /* Trigger Manager 0 output 11 (output to IO) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_OUTX0      12  /* Trigger Manager 0 output X0 (output to other TRGM) */
#define CHIP_TRGM0_OUT_MUX_TRGM0_OUTX1      13  /* Trigger Manager 0 output X1 (output to other TRGM) */
#define CHIP_TRGM0_OUT_MUX_PWM0_SYNCI       14  /* PWM timer 0 counter sync trigger input */
#define CHIP_TRGM0_OUT_MUX_PWM0_FRCI        15  /* PWM timer 0 force output control input */
#define CHIP_TRGM0_OUT_MUX_PWM0_FRCSYNCI    16  /* PWM timer 0 forced output control synchronization active input */
#define CHIP_TRGM0_OUT_MUX_PWM0_SHRLDSYNCI  17  /* PWM timer 0 shadow register activate trigger input */
#define CHIP_TRGM0_OUT_MUX_PWM0_FAULTI0     18  /* PWM timer 0 fault protection input 0 */
#define CHIP_TRGM0_OUT_MUX_PWM0_FAULTI1     19  /* PWM timer 0 fault protection input 1 */
#define CHIP_TRGM0_OUT_MUX_PWM0_FAULTI2     20  /* PWM timer 0 fault protection input 2 */
#define CHIP_TRGM0_OUT_MUX_PWM0_FAULTI3     21  /* PWM timer 0 fault protection input 3 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN8         22  /* PWM timer 0 input capture 8 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN9         23  /* PWM timer 0 input capture 9 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN10        24  /* PWM timer 0 input capture 10 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN11        25  /* PWM timer 0 input capture 11 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN12        26  /* PWM timer 0 input capture 12 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN13        27  /* PWM timer 0 input capture 13 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN14        28  /* PWM timer 0 input capture 14 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN15        29  /* PWM timer 0 input capture 15 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN16        30  /* PWM timer 0 input capture 16 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN17        31  /* PWM timer 0 input capture 17 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN18        32  /* PWM timer 0 input capture 18 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN19        33  /* PWM timer 0 input capture 19 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN20        34  /* PWM timer 0 input capture 20 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN21        35  /* PWM timer 0 input capture 21 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN22        36  /* PWM timer 0 input capture 22 */
#define CHIP_TRGM0_OUT_MUX_PWM0_IN23        37  /* PWM timer 0 input capture 23 */
#define CHIP_TRGM0_OUT_MUX_QEI0_A           38  /* Quadrature encoder interface 0 phase A input */
#define CHIP_TRGM0_OUT_MUX_QEI0_B           39  /* Quadrature encoder interface 0 phase B input */
#define CHIP_TRGM0_OUT_MUX_QEI0_Z           40  /* Quadrature encoder interface 0 phase Z input */
#define CHIP_TRGM0_OUT_MUX_QEI0_H           41  /* Quadrature encoder interface 0 phase H input */
#define CHIP_TRGM0_OUT_MUX_QEI0_PAUSE       42  /* Quadrature encoder interface 0 counter pause */
#define CHIP_TRGM0_OUT_MUX_QEI0_SNAPI       43  /* Quadrature encoder interface 0 snapshot trigger input */
#define CHIP_TRGM0_OUT_MUX_HALL0_U          44  /* Hall sensor interface 0 phase U input */
#define CHIP_TRGM0_OUT_MUX_HALL0_V          45  /* Hall sensor interface 0 phase V input */
#define CHIP_TRGM0_OUT_MUX_HALL0_W          46  /* Hall sensor interface 0 phase W input */
#define CHIP_TRGM0_OUT_MUX_HALL0_SNAPI      47  /* Hall sensor interface 0 snapshot trigger input */
#define CHIP_TRGM0_OUT_MUX_ADC0_STRGI       48  /* Sequence conversion trigger input for ADC0 */
#define CHIP_TRGM0_OUT_MUX_ADC1_STRGI       49  /* Sequence conversion trigger input for ADC1 */
#define CHIP_TRGM0_OUT_MUX_ADC2_STRGI       50  /* Sequence conversion trigger input for ADC2 */
#define CHIP_TRGM0_OUT_MUX_ADC3_STRGI       51  /* Sequence conversion trigger input for ADC3 */
#define CHIP_TRGM0_OUT_MUX_ADCX_PTRGI0A     52  /* Preemptive conversion trigger input 0A for ADC0, 1, 2, 3 */
#define CHIP_TRGM0_OUT_MUX_ADCX_PTRGI0B     53  /* Preemptive conversion trigger input 0B for ADC0, 1, 2, 3 */
#define CHIP_TRGM0_OUT_MUX_ADCX_PTRGI0C     54  /* Preemptive conversion trigger input 0C for ADC0, 1, 2, 3 */
#define CHIP_TRGM0_OUT_MUX_GPTMR0_SYNCI     55  /* General purpose timer 0 counter synchronization input */
#define CHIP_TRGM0_OUT_MUX_GPTMR0_IN2       56  /* General purpose timer 0 channel 2 input */
#define CHIP_TRGM0_OUT_MUX_GPTMR0_IN3       57  /* General purpose timer 0 channel 3 input */
#define CHIP_TRGM0_OUT_MUX_GPTMR1_SYNCI     58  /* General purpose timer 1 counter synchronization input */
#define CHIP_TRGM0_OUT_MUX_GPTMR1_IN2       59  /* General purpose timer 1 channel 2 input */
#define CHIP_TRGM0_OUT_MUX_GPTMR1_IN3       60  /* General purpose timer 1 channel 3 input */
#define CHIP_TRGM0_OUT_MUX_ACMP0_WIN        61  /* Comparator 0 window mode input */
#define CHIP_TRGM0_OUT_MUX_PTPC_CAP0        62  /* Precision Time Protocol module PTPC input capture 0 */
#define CHIP_TRGM0_OUT_MUX_PTPC_CAP1        63  /* Precision Time Protocol module PTPC input capture 1 */

/*********** TRGM1_OUTPUT MUX list ********************************************/
#define CHIP_TRGM1_OUT_MUX_TRGM1_P0          0  /* Trigger Manager 1 output 0 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P1          1  /* Trigger Manager 1 output 1 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P2          2  /* Trigger Manager 1 output 2 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P3          3  /* Trigger Manager 1 output 3 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P4          4  /* Trigger Manager 1 output 4 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P5          5  /* Trigger Manager 1 output 5 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P6          6  /* Trigger Manager 1 output 6 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P7          7  /* Trigger Manager 1 output 7 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P8          8  /* Trigger Manager 1 output 8 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P9          9  /* Trigger Manager 1 output 9 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P10        10  /* Trigger Manager 1 output 10 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_P11        11  /* Trigger Manager 1 output 11 (output to IO) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_OUTX0      12  /* Trigger Manager 1 output X0 (output to other TRGM) */
#define CHIP_TRGM1_OUT_MUX_TRGM1_OUTX1      13  /* Trigger Manager 1 output X1 (output to other TRGM) */
#define CHIP_TRGM1_OUT_MUX_PWM1_SYNCI       14  /* PWM timer 1 counter sync trigger input */
#define CHIP_TRGM1_OUT_MUX_PWM1_FRCI        15  /* PWM timer 1 force output control input */
#define CHIP_TRGM1_OUT_MUX_PWM1_FRCSYNCI    16  /* PWM timer 1 forced output control synchronization active input */
#define CHIP_TRGM1_OUT_MUX_PWM1_SHRLDSYNCI  17  /* PWM timer 1 shadow register activate trigger input */
#define CHIP_TRGM1_OUT_MUX_PWM1_FAULTI0     18  /* PWM timer 1 fault protection input 0 */
#define CHIP_TRGM1_OUT_MUX_PWM1_FAULTI1     19  /* PWM timer 1 fault protection input 1 */
#define CHIP_TRGM1_OUT_MUX_PWM1_FAULTI2     20  /* PWM timer 1 fault protection input 2 */
#define CHIP_TRGM1_OUT_MUX_PWM1_FAULTI3     21  /* PWM timer 1 fault protection input 3 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN8         22  /* PWM timer 1 input capture 8 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN9         23  /* PWM timer 1 input capture 9 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN10        24  /* PWM timer 1 input capture 10 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN11        25  /* PWM timer 1 input capture 11 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN12        26  /* PWM timer 1 input capture 12 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN13        27  /* PWM timer 1 input capture 13 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN14        28  /* PWM timer 1 input capture 14 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN15        29  /* PWM timer 1 input capture 15 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN16        30  /* PWM timer 1 input capture 16 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN17        31  /* PWM timer 1 input capture 17 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN18        32  /* PWM timer 1 input capture 18 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN19        33  /* PWM timer 1 input capture 19 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN20        34  /* PWM timer 1 input capture 20 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN21        35  /* PWM timer 1 input capture 21 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN22        36  /* PWM timer 1 input capture 22 */
#define CHIP_TRGM1_OUT_MUX_PWM1_IN23        37  /* PWM timer 1 input capture 23 */
#define CHIP_TRGM1_OUT_MUX_QEI1_A           38  /* Quadrature encoder interface 1 phase A input */
#define CHIP_TRGM1_OUT_MUX_QEI1_B           39  /* Quadrature encoder interface 1 phase B input */
#define CHIP_TRGM1_OUT_MUX_QEI1_Z           40  /* Quadrature encoder interface 1 phase Z input */
#define CHIP_TRGM1_OUT_MUX_QEI1_H           41  /* Quadrature encoder interface 1 phase H input */
#define CHIP_TRGM1_OUT_MUX_QEI1_PAUSE       42  /* Quadrature encoder interface 1 counter pause */
#define CHIP_TRGM1_OUT_MUX_QEI1_SNAPI       43  /* Quadrature encoder interface 1 snapshot trigger input */
#define CHIP_TRGM1_OUT_MUX_HALL1_U          44  /* Hall sensor interface 1 phase U input */
#define CHIP_TRGM1_OUT_MUX_HALL1_V          45  /* Hall sensor interface 1 phase V input */
#define CHIP_TRGM1_OUT_MUX_HALL1_W          46  /* Hall sensor interface 1 phase W input */
#define CHIP_TRGM1_OUT_MUX_HALL1_SNAPI      47  /* Hall sensor interface 1 snapshot trigger input */
#define CHIP_TRGM1_OUT_MUX_ADC0_STRGI       48  /* Sequence conversion trigger input for ADC0 */
#define CHIP_TRGM1_OUT_MUX_ADC1_STRGI       49  /* Sequence conversion trigger input for ADC1 */
#define CHIP_TRGM1_OUT_MUX_ADC2_STRGI       50  /* Sequence conversion trigger input for ADC2 */
#define CHIP_TRGM1_OUT_MUX_ADC3_STRGI       51  /* Sequence conversion trigger input for ADC3 */
#define CHIP_TRGM1_OUT_MUX_ADCX_PTRGI1A     52  /* Preemptive conversion trigger input 1A for ADC0, 1, 2, 3 */
#define CHIP_TRGM1_OUT_MUX_ADCX_PTRGI1B     53  /* Preemptive conversion trigger input 1B for ADC0, 1, 2, 3 */
#define CHIP_TRGM1_OUT_MUX_ADCX_PTRGI1C     54  /* Preemptive conversion trigger input 1C for ADC0, 1, 2, 3 */
#define CHIP_TRGM1_OUT_MUX_GPTMR2_SYNCI     55  /* General purpose timer 2 counter synchronization input */
#define CHIP_TRGM1_OUT_MUX_GPTMR2_IN2       56  /* General purpose timer 2 channel 2 input */
#define CHIP_TRGM1_OUT_MUX_GPTMR2_IN3       57  /* General purpose timer 2 channel 3 input */
#define CHIP_TRGM1_OUT_MUX_GPTMR3_SYNCI     58  /* General purpose timer 3 counter synchronization input */
#define CHIP_TRGM1_OUT_MUX_GPTMR3_IN2       59  /* General purpose timer 3 channel 2 input */
#define CHIP_TRGM1_OUT_MUX_GPTMR3_IN3       60  /* General purpose timer 3 channel 3 input */
#define CHIP_TRGM1_OUT_MUX_ACMP1_WIN        61  /* Comparator 1 window mode input */
#define CHIP_TRGM1_OUT_MUX_PTPC_CAP0        62  /* Precision Time Protocol module PTPC input capture 0 */
#define CHIP_TRGM1_OUT_MUX_PTPC_CAP1        63  /* Precision Time Protocol module PTPC input capture 1 */

/*********** TRGM2_OUTPUT MUX list ********************************************/
#define CHIP_TRGM2_OUT_MUX_TRGM2_P0          0  /* Trigger Manager 2 output 0 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P1          1  /* Trigger Manager 2 output 1 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P2          2  /* Trigger Manager 2 output 2 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P3          3  /* Trigger Manager 2 output 3 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P4          4  /* Trigger Manager 2 output 4 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P5          5  /* Trigger Manager 2 output 5 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P6          6  /* Trigger Manager 2 output 6 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P7          7  /* Trigger Manager 2 output 7 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P8          8  /* Trigger Manager 2 output 8 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P9          9  /* Trigger Manager 2 output 9 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P10        10  /* Trigger Manager 2 output 10 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_P11        11  /* Trigger Manager 2 output 11 (output to IO) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_OUTX0      12  /* Trigger Manager 2 output X0 (output to other TRGM) */
#define CHIP_TRGM2_OUT_MUX_TRGM2_OUTX1      13  /* Trigger Manager 2 output X1 (output to other TRGM) */
#define CHIP_TRGM2_OUT_MUX_PWM2_SYNCI       14  /* PWM timer 2 counter sync trigger input */
#define CHIP_TRGM2_OUT_MUX_PWM2_FRCI        15  /* PWM timer 2 force output control input */
#define CHIP_TRGM2_OUT_MUX_PWM2_FRCSYNCI    16  /* PWM timer 2 forced output control synchronization active input */
#define CHIP_TRGM2_OUT_MUX_PWM2_SHRLDSYNCI  17  /* PWM timer 2 shadow register activate trigger input */
#define CHIP_TRGM2_OUT_MUX_PWM2_FAULTI0     18  /* PWM timer 2 fault protection input 0 */
#define CHIP_TRGM2_OUT_MUX_PWM2_FAULTI1     19  /* PWM timer 2 fault protection input 1 */
#define CHIP_TRGM2_OUT_MUX_PWM2_FAULTI2     20  /* PWM timer 2 fault protection input 2 */
#define CHIP_TRGM2_OUT_MUX_PWM2_FAULTI3     21  /* PWM timer 2 fault protection input 3 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN8         22  /* PWM timer 2 input capture 8 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN9         23  /* PWM timer 2 input capture 9 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN10        24  /* PWM timer 2 input capture 10 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN11        25  /* PWM timer 2 input capture 11 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN12        26  /* PWM timer 2 input capture 12 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN13        27  /* PWM timer 2 input capture 13 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN14        28  /* PWM timer 2 input capture 14 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN15        29  /* PWM timer 2 input capture 15 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN16        30  /* PWM timer 2 input capture 16 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN17        31  /* PWM timer 2 input capture 17 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN18        32  /* PWM timer 2 input capture 18 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN19        33  /* PWM timer 2 input capture 19 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN20        34  /* PWM timer 2 input capture 20 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN21        35  /* PWM timer 2 input capture 21 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN22        36  /* PWM timer 2 input capture 22 */
#define CHIP_TRGM2_OUT_MUX_PWM2_IN23        37  /* PWM timer 2 input capture 23 */
#define CHIP_TRGM2_OUT_MUX_QEI2_A           38  /* Quadrature encoder interface 2 phase A input */
#define CHIP_TRGM2_OUT_MUX_QEI2_B           39  /* Quadrature encoder interface 2 phase B input */
#define CHIP_TRGM2_OUT_MUX_QEI2_Z           40  /* Quadrature encoder interface 2 phase Z input */
#define CHIP_TRGM2_OUT_MUX_QEI2_H           41  /* Quadrature encoder interface 2 phase H input */
#define CHIP_TRGM2_OUT_MUX_QEI2_PAUSE       42  /* Quadrature encoder interface 2 counter pause */
#define CHIP_TRGM2_OUT_MUX_QEI2_SNAPI       43  /* Quadrature encoder interface 2 snapshot trigger input */
#define CHIP_TRGM2_OUT_MUX_HALL2_U          44  /* Hall sensor interface 2 phase U input */
#define CHIP_TRGM2_OUT_MUX_HALL2_V          45  /* Hall sensor interface 2 phase V input */
#define CHIP_TRGM2_OUT_MUX_HALL2_W          46  /* Hall sensor interface 2 phase W input */
#define CHIP_TRGM2_OUT_MUX_HALL2_SNAPI      47  /* Hall sensor interface 2 snapshot trigger input */
#define CHIP_TRGM2_OUT_MUX_ADC0_STRGI       48  /* Sequence conversion trigger input for ADC0 */
#define CHIP_TRGM2_OUT_MUX_ADC1_STRGI       49  /* Sequence conversion trigger input for ADC1 */
#define CHIP_TRGM2_OUT_MUX_ADC2_STRGI       50  /* Sequence conversion trigger input for ADC2 */
#define CHIP_TRGM2_OUT_MUX_ADC3_STRGI       51  /* Sequence conversion trigger input for ADC3 */
#define CHIP_TRGM2_OUT_MUX_ADCX_PTRGI2A     52  /* Preemptive conversion trigger input 2A for ADC0, 1, 2, 3 */
#define CHIP_TRGM2_OUT_MUX_ADCX_PTRGI2B     53  /* Preemptive conversion trigger input 2B for ADC0, 1, 2, 3 */
#define CHIP_TRGM2_OUT_MUX_ADCX_PTRGI2C     54  /* Preemptive conversion trigger input 2C for ADC0, 1, 2, 3 */
#define CHIP_TRGM2_OUT_MUX_GPTMR4_SYNCI     55  /* General purpose timer 4 counter synchronization input */
#define CHIP_TRGM2_OUT_MUX_GPTMR4_IN2       56  /* General purpose timer 4 channel 2 input */
#define CHIP_TRGM2_OUT_MUX_GPTMR4_IN3       57  /* General purpose timer 4 channel 3 input */
#define CHIP_TRGM2_OUT_MUX_GPTMR5_SYNCI     58  /* General purpose timer 5 counter synchronization input */
#define CHIP_TRGM2_OUT_MUX_GPTMR5_IN2       59  /* General purpose timer 5 channel 2 input */
#define CHIP_TRGM2_OUT_MUX_GPTMR5_IN3       60  /* General purpose timer 5 channel 3 input */
#define CHIP_TRGM2_OUT_MUX_ACMP2_WIN        61  /* Comparator 2 window mode input */
#define CHIP_TRGM2_OUT_MUX_PTPC_CAP0        62  /* Precision Time Protocol module PTPC input capture 0 */
#define CHIP_TRGM2_OUT_MUX_PTPC_CAP1        63  /* Precision Time Protocol module PTPC input capture 1 */


/*********** TRGM3_OUTPUT MUX list ********************************************/
#define CHIP_TRGM3_OUT_MUX_TRGM3_P0          0  /* Trigger Manager 3 output 0 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P1          1  /* Trigger Manager 3 output 1 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P2          2  /* Trigger Manager 3 output 2 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P3          3  /* Trigger Manager 3 output 3 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P4          4  /* Trigger Manager 3 output 4 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P5          5  /* Trigger Manager 3 output 5 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P6          6  /* Trigger Manager 3 output 6 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P7          7  /* Trigger Manager 3 output 7 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P8          8  /* Trigger Manager 3 output 8 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P9          9  /* Trigger Manager 3 output 9 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P10        10  /* Trigger Manager 3 output 10 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_P11        11  /* Trigger Manager 3 output 11 (output to IO) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_OUTX0      12  /* Trigger Manager 3 output X0 (output to other TRGM) */
#define CHIP_TRGM3_OUT_MUX_TRGM3_OUTX1      13  /* Trigger Manager 3 output X1 (output to other TRGM) */
#define CHIP_TRGM3_OUT_MUX_PWM3_SYNCI       14  /* PWM timer 3 counter sync trigger input */
#define CHIP_TRGM3_OUT_MUX_PWM3_FRCI        15  /* PWM timer 3 force output control input */
#define CHIP_TRGM3_OUT_MUX_PWM3_FRCSYNCI    16  /* PWM timer 3 forced output control synchronization active input */
#define CHIP_TRGM3_OUT_MUX_PWM3_SHRLDSYNCI  17  /* PWM timer 3 shadow register activate trigger input */
#define CHIP_TRGM3_OUT_MUX_PWM3_FAULTI0     18  /* PWM timer 3 fault protection input 0 */
#define CHIP_TRGM3_OUT_MUX_PWM3_FAULTI1     19  /* PWM timer 3 fault protection input 1 */
#define CHIP_TRGM3_OUT_MUX_PWM3_FAULTI2     20  /* PWM timer 3 fault protection input 2 */
#define CHIP_TRGM3_OUT_MUX_PWM3_FAULTI3     21  /* PWM timer 3 fault protection input 3 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN8         22  /* PWM timer 3 input capture 8 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN9         23  /* PWM timer 3 input capture 9 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN10        24  /* PWM timer 3 input capture 10 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN11        25  /* PWM timer 3 input capture 11 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN12        26  /* PWM timer 3 input capture 12 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN13        27  /* PWM timer 3 input capture 13 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN14        28  /* PWM timer 3 input capture 14 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN15        29  /* PWM timer 3 input capture 15 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN16        30  /* PWM timer 3 input capture 16 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN17        31  /* PWM timer 3 input capture 17 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN18        32  /* PWM timer 3 input capture 18 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN19        33  /* PWM timer 3 input capture 19 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN20        34  /* PWM timer 3 input capture 20 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN21        35  /* PWM timer 3 input capture 21 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN22        36  /* PWM timer 3 input capture 22 */
#define CHIP_TRGM3_OUT_MUX_PWM3_IN23        37  /* PWM timer 3 input capture 23 */
#define CHIP_TRGM3_OUT_MUX_QEI3_A           38  /* Quadrature encoder interface 3 phase A input */
#define CHIP_TRGM3_OUT_MUX_QEI3_B           39  /* Quadrature encoder interface 3 phase B input */
#define CHIP_TRGM3_OUT_MUX_QEI3_Z           40  /* Quadrature encoder interface 3 phase Z input */
#define CHIP_TRGM3_OUT_MUX_QEI3_H           41  /* Quadrature encoder interface 3 phase H input */
#define CHIP_TRGM3_OUT_MUX_QEI3_PAUSE       42  /* Quadrature encoder interface 3 counter pause */
#define CHIP_TRGM3_OUT_MUX_QEI3_SNAPI       43  /* Quadrature encoder interface 3 snapshot trigger input */
#define CHIP_TRGM3_OUT_MUX_HALL3_U          44  /* Hall sensor interface 3 phase U input */
#define CHIP_TRGM3_OUT_MUX_HALL3_V          45  /* Hall sensor interface 3 phase V input */
#define CHIP_TRGM3_OUT_MUX_HALL3_W          46  /* Hall sensor interface 3 phase W input */
#define CHIP_TRGM3_OUT_MUX_HALL3_SNAPI      47  /* Hall sensor interface 3 snapshot trigger input */
#define CHIP_TRGM3_OUT_MUX_ADC0_STRGI       48  /* Sequence conversion trigger input for ADC0 */
#define CHIP_TRGM3_OUT_MUX_ADC1_STRGI       49  /* Sequence conversion trigger input for ADC1 */
#define CHIP_TRGM3_OUT_MUX_ADC2_STRGI       50  /* Sequence conversion trigger input for ADC2 */
#define CHIP_TRGM3_OUT_MUX_ADC3_STRGI       51  /* Sequence conversion trigger input for ADC3 */
#define CHIP_TRGM3_OUT_MUX_ADCX_PTRGI3A     52  /* Preemptive conversion trigger input 3A for ADC0, 1, 2, 3 */
#define CHIP_TRGM3_OUT_MUX_ADCX_PTRGI3B     53  /* Preemptive conversion trigger input 3B for ADC0, 1, 2, 3 */
#define CHIP_TRGM3_OUT_MUX_ADCX_PTRGI3C     54  /* Preemptive conversion trigger input 3C for ADC0, 1, 2, 3 */
#define CHIP_TRGM3_OUT_MUX_GPTMR6_SYNCI     55  /* General purpose timer 6 counter synchronization input */
#define CHIP_TRGM3_OUT_MUX_GPTMR6_IN2       56  /* General purpose timer 6 channel 2 input */
#define CHIP_TRGM3_OUT_MUX_GPTMR6_IN3       57  /* General purpose timer 6 channel 3 input */
#define CHIP_TRGM3_OUT_MUX_GPTMR7_SYNCI     58  /* General purpose timer 7 counter synchronization input */
#define CHIP_TRGM3_OUT_MUX_GPTMR7_IN2       59  /* General purpose timer 7 channel 2 input */
#define CHIP_TRGM3_OUT_MUX_GPTMR7_IN3       60  /* General purpose timer 7 channel 3 input */
#define CHIP_TRGM3_OUT_MUX_ACMP3_WIN        61  /* Comparator 3 window mode input */
#define CHIP_TRGM3_OUT_MUX_PTPC_CAP0        62  /* Precision Time Protocol module PTPC input capture 0 */
#define CHIP_TRGM3_OUT_MUX_PTPC_CAP1        63  /* Precision Time Protocol module PTPC input capture 1 */



/*********** TRGMx_DMA MUX list ***********************************************/
#define CHIP_TRGM_DMA_MUX_PWM_CMP0          0  /* PWM timer x comparator input 0 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP1          1  /* PWM timer x comparator input 1 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP2          2  /* PWM timer x comparator input 2 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP3          3  /* PWM timer x comparator input 3 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP4          4  /* PWM timer x comparator input 4 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP5          5  /* PWM timer x comparator input 5 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP6          6  /* PWM timer x comparator input 6 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP7          7  /* PWM timer x comparator input 7 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP8          8  /* PWM timer x comparator input 8 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP9          9  /* PWM timer x comparator input 9 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP10        10  /* PWM timer x comparator input 10 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP11        11  /* PWM timer x comparator input 11 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP12        12  /* PWM timer x comparator input 12 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP13        13  /* PWM timer x comparator input 13 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP14        14  /* PWM timer x comparator input 14 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP15        15  /* PWM timer x comparator input 15 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP16        16  /* PWM timer x comparator input 16 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP17        17  /* PWM timer x comparator input 17 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP18        18  /* PWM timer x comparator input 18 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP19        19  /* PWM timer x comparator input 19 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP20        20  /* PWM timer x comparator input 20 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP21        21  /* PWM timer x comparator input 21 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP22        22  /* PWM timer x comparator input 22 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_CMP23        23  /* PWM timer x comparator input 23 capture or output compare match */
#define CHIP_TRGM_DMA_MUX_PWM_RLD          24  /* PWM timer x counter reload */
#define CHIP_TRGM_DMA_MUX_PWM_HALFRLD      25  /* PWM timer x half cycle reload */
#define CHIP_TRGM_DMA_MUX_PWM_XRLD         26  /* PWM timer x extended counter reload */
#define CHIP_TRGM_DMA_MUX_QEI              27  /* DMA request for quadrature decoder x */
#define CHIP_TRGM_DMA_MUX_HALL             28  /* DMA request for Hall sensor x */




/*********** TRGMx_FILTER MUX ***********************************************/
#define CHIP_TRGM_FILTER_MUX_PWM_IN0        0  /* PWM timer x input capture 0 */
#define CHIP_TRGM_FILTER_MUX_PWM_IN1        1  /* PWM timer x input capture 1 */
#define CHIP_TRGM_FILTER_MUX_PWM_IN2        2  /* PWM timer x input capture 2 */
#define CHIP_TRGM_FILTER_MUX_PWM_IN3        3  /* PWM timer x input capture 3 */
#define CHIP_TRGM_FILTER_MUX_PWM_IN4        4  /* PWM timer x input capture 4 */
#define CHIP_TRGM_FILTER_MUX_PWM_IN5        5  /* PWM timer x input capture 5 */
#define CHIP_TRGM_FILTER_MUX_PWM_IN6        6  /* PWM timer x input capture 6 */
#define CHIP_TRGM_FILTER_MUX_PWM_IN7        7  /* PWM timer x input capture 7 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN0       8  /* Trigger Manager x Input 0 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN1       9  /* Trigger Manager x Input 1 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN2      10  /* Trigger Manager x Input 2 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN3      11  /* Trigger Manager x Input 3 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN4      12  /* Trigger Manager x Input 4 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN5      13  /* Trigger Manager x Input 5 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN6      14  /* Trigger Manager x Input 6 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN7      15  /* Trigger Manager x Input 7 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN8      16  /* Trigger Manager x Input 8 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN9      17  /* Trigger Manager x Input 9 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN10     18  /* Trigger Manager x Input 10 */
#define CHIP_TRGM_FILTER_MUX_TRGM_IN11     19  /* Trigger Manager x Input 11 */



#endif // CHIP_TRGM_MAP_H
