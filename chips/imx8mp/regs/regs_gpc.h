#ifndef REGS_GPC_H
#define REGS_GPC_H

#include "chip_ioreg_defs.h"

/** GPC - Register Layout Typedef */
typedef struct {
    __IO uint32_t LPCR_A53_BSC;                      /**< Basic Low power control register of A53 platform, offset: 0x0 */
    __IO uint32_t LPCR_A53_AD;                       /**< Advanced Low power control register of A53 platform, offset: 0x4 */
    __IO uint32_t LPCR_M7;                           /**< Low power control register of CPU1, offset: 0x8 */
    uint8_t RESERVED_0[8];
    __IO uint32_t SLPCR;                             /**< System low power control register, offset: 0x14 */
    __IO uint32_t MST_CPU_MAPPING;                   /**< MASTER LPM Handshake, offset: 0x18 */
    uint8_t RESERVED_1[4];
    __IO uint32_t MLPCR;                             /**< Memory low power control register, offset: 0x20 */
    __IO uint32_t PGC_ACK_SEL_A53;                   /**< PGC acknowledge signal selection of A53 platform, offset: 0x24 */
    __IO uint32_t PGC_ACK_SEL_M7;                    /**< PGC acknowledge signal selection of M7 platform, offset: 0x28 */
    __IO uint32_t MISC;                              /**< GPC Miscellaneous register, offset: 0x2C */
    __IO uint32_t IMR_CORE0_A53[5];                  /**< IRQ masking register 1 of A53 core0..IRQ masking register 5 of A53 core0, array offset: 0x30, array step: 0x4 */
    __IO uint32_t IMR_CORE1_A53[5];                  /**< IRQ masking register 1 of A53 core1..IRQ masking register 5 of A53 core1, array offset: 0x44, array step: 0x4 */
    __IO uint32_t IMR_M7[5];                         /**< IRQ masking register 1 of M7..IRQ masking register 5 of M7, array offset: 0x58, array step: 0x4 */
    uint8_t RESERVED_2[20];
    __I  uint32_t ISR_A53[5];                        /**< IRQ status register 1 of A53..IRQ status register 5 of A53, array offset: 0x80, array step: 0x4 */
    __I  uint32_t ISR_M7[5];                         /**< IRQ status register 1 of M7..IRQ status register 5 of M7, array offset: 0x94, array step: 0x4 */
    uint8_t RESERVED_3[40];
    __IO uint32_t CPU_PGC_SW_PUP_REQ;                /**< CPU PGC software power up trigger, offset: 0xD0 */
    __IO uint32_t MIX_PGC_SW_PUP_REQ;                /**< MIX PGC software power up trigger, offset: 0xD4 */
    __IO uint32_t PU_PGC_SW_PUP_REQ;                 /**< PU PGC software up trigger, offset: 0xD8 */
    __IO uint32_t CPU_PGC_SW_PDN_REQ;                /**< CPU PGC software down trigger, offset: 0xDC */
    __IO uint32_t MIX_PGC_SW_PDN_REQ;                /**< MIX PGC software power down trigger, offset: 0xE0 */
    __IO uint32_t PU_PGC_SW_PDN_REQ;                 /**< PU PGC software down trigger, offset: 0xE4 */
    uint8_t RESERVED_4[32];
    __I  uint32_t CPU_PGC_PUP_STATUS1;               /**< CPU PGC software up trigger status1, offset: 0x108 */
    __I  uint32_t A53_MIX_PGC_PUP_STATUS[3];         /**< A53 MIX software up trigger status register, array offset: 0x10C, array step: 0x4 */
    __I  uint32_t M7_MIX_PGC_PUP_STATUS[3];          /**< M7 MIX PGC software up trigger status register, array offset: 0x118, array step: 0x4 */
    __I  uint32_t A53_PU_PGC_PUP_STATUS[3];          /**< A53 PU software up trigger status register, array offset: 0x124, array step: 0x4 */
    __I  uint32_t M7_PU_PGC_PUP_STATUS[3];           /**< M7 PU PGC software up trigger status register, array offset: 0x130, array step: 0x4 */
    __I  uint32_t CPU_PGC_PDN_STATUS1;               /**< CPU PGC software dn trigger status1, offset: 0x13C */
    __I  uint32_t A53_MIX_PGC_PDN_STATUS[3];         /**< A53 MIX software down trigger status register, array offset: 0x140, array step: 0x4 */
    __I  uint32_t M7_MIX_PGC_PDN_STATUS[3];          /**< M7 MIX PGC software power down trigger status register, array offset: 0x14C, array step: 0x4 */
    __I  uint32_t A53_PU_PGC_PDN_STATUS[3];          /**< A53 PU PGC software down trigger status, array offset: 0x158, array step: 0x4 */
    __I  uint32_t M7_PU_PGC_PDN_STATUS[3];           /**< M7 PU PGC software down trigger status, array offset: 0x164, array step: 0x4 */
    __IO uint32_t A53_MIX_PDN_FLG;                   /**< A53 MIX PDN FLG, offset: 0x170 */
    __IO uint32_t A53_PU_PDN_FLG;                    /**< A53 PU PDN FLG, offset: 0x174 */
    __IO uint32_t M7_MIX_PDN_FLG;                    /**< M7 MIX PDN FLG, offset: 0x178 */
    __IO uint32_t M7_PU_PDN_FLG;                     /**< M7 PU PDN FLG, offset: 0x17C */
    __IO uint32_t LPCR_A53_BSC2;                     /**< Basic Low power control register of A53 platform, offset: 0x180 */
    uint8_t RESERVED_5[12];
    __IO uint32_t PU_PWRHSK;                         /**< Power handshake register, offset: 0x190 */
    __IO uint32_t IMR_CORE2_A53[5];                  /**< IRQ masking register 1 of A53 core2..IRQ masking register 5 of A53 core2, array offset: 0x194, array step: 0x4 */
    __IO uint32_t IMR_CORE3_A53[5];                  /**< IRQ masking register 1 of A53 core3..IRQ masking register 5 of A53 core3, array offset: 0x1A8, array step: 0x4 */
    __IO uint32_t ACK_SEL_A53_PU;                    /**< PGC acknowledge signal selection of A53 platform for PUs, offset: 0x1BC */
    __IO uint32_t ACK_SEL_A53_PU1;                   /**< PGC acknowledge signal selection of A53 platform for PUs, offset: 0x1C0 */
    __IO uint32_t ACK_SEL_M7_PU;                     /**< PGC acknowledge signal selection of M7 platform for PUs, offset: 0x1C4 */
    __IO uint32_t ACK_SEL_M7_PU1;                    /**< PGC acknowledge signal selection of M7 platform for PUs, offset: 0x1C8 */
    __IO uint32_t PGC_CPU_A53_MAPPING;               /**< PGC CPU A53 mapping, offset: 0x1CC */
    __IO uint32_t PGC_CPU_M7_MAPPING;                /**< PGC CPU M7 mapping, offset: 0x1D0 */
    uint8_t RESERVED_6[44];
    __IO uint32_t SLT_CFG[27];                       /**< Slot configure register for CPUs, array offset: 0x200, array step: 0x4 */
    uint8_t RESERVED_7[20];
    struct {                                         /* offset: 0x280, array step: 0x8 */
        __IO uint32_t SLT_CFG_PU;                        /**< Slot configure register for PGC PUs, array offset: 0x280, array step: 0x8 */
        __IO uint32_t SLT_CFG_PU1;                       /**< Extended slot configure register for PGC PUs, array offset: 0x284, array step: 0x8 */
    } SLTn_CFG_PU[27];
} CHIP_REGS_GPC_T;

#define CHIP_REGS_GPC            ((CHIP_REGS_GPC_T *)CHIP_MEMRGN_ADDR_PERIPH_GPC)


/**** Basic Low power control register of A53 platform (GPC_LPCR_A53_BSC) *****/
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MASK_DSM_TRIGGER           (0x00000001UL << 31U) /* (RW) DSM trigger of A53 platform will be masked */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_IRQ_SRC_A53_WUP            (0x00000001UL << 30U) /* (RW) LPM wakeup source (0 - OR C0-C3, 1 -  external INT[127:0], masked by IMR0) */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_IRQ_SRC_C1                 (0x00000001UL << 29U) /* (RW) core1 wakeup source (0 - external INT[127:0], masked by IMR1, 1 - GIC nIRQ[1]/nFIQ[1]) */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_IRQ_SRC_C0                 (0x00000001UL << 28U) /* (RW) core0 wakeup source (0 - external INT[127:0], masked by IMR0, 1 - GIC nIRQ[0]/nFIQ[0]) */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MASK_L2CC_WFI              (0x00000001UL << 26U) /* (RW) WFI for L2 cache controller is masked */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MASK_SCU_WFI               (0x00000001UL << 24U) /* (RW) WFI for SCU is masked */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_IRQ_SRC_C3                 (0x00000001UL << 23U) /* (RW) core3 wakeup source (0 - external INT[127:0], masked by IMR1, 1 - GIC nIRQ[1]/nFIQ[1]) */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_IRQ_SRC_C2                 (0x00000001UL << 22U) /* (RW) core2 wakeup source (0 - external INT[127:0], masked by IMR1, 1 - GIC nIRQ[1]/nFIQ[1]) */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MASK_CORE3_WFI             (0x00000001UL << 19U) /* (RW) WFI for core3 is masked */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MASK_CORE2_WFI             (0x00000001UL << 18U) /* (RW) WFI for core2 is masked */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MASK_CORE1_WFI             (0x00000001UL << 17U) /* (RW) WFI for core1 is masked */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MASK_CORE0_WFI             (0x00000001UL << 16U) /* (RW) WFI for core0 is masked */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_CPU_CLK_ON_LPM             (0x00000001UL << 14U) /* (RW) A53 clock enabled on wait/stop mode */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MST2_LPM_HSK_MASK          (0x00000001UL <<  8U) /* (RW) Disable MASTER2 supermix2noc ADB LPM handshake, mask ACK from MASTER2. Use together with MST_CPU_MAPPING[2] */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MST1_LPM_HSK_MASK          (0x00000001UL <<  7U) /* (RW) Disable MASTER1 supermix2noc ADB LPM handshake, mask ACK from MASTER1. Use together with MST_CPU_MAPPING[1] */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_MST0_LPM_HSK_MASK          (0x00000001UL <<  6U) /* (RW) Disable MASTER0 supermix2noc ADB LPM handshake, mask ACK from MASTER0. Use together with MST_CPU_MAPPING[0] */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_LPM1                       (0x00000003UL <<  2U) /* (RW) CORE1 Setting the low power mode that system will enter on next assertion of dsm_request signal. */
#define CHIP_REGFLD_GPC_LPCR_A53_BSC_LPM0                       (0x00000003UL <<  0U) /* (RW) CORE0 Setting the low power mode that system will enter on next assertion of dsm_request signal. */
#define CHIP_REGMSK_GPC_LPCR_A53_BSC_RESERVED                   (0x0A30BE30UL)

#define CHIP_REGFLDVAL_GPC_LPCR_A53_BSC_LPM_RUN                 0 /* Remain in RUN mode */
#define CHIP_REGFLDVAL_GPC_LPCR_A53_BSC_LPM_WAIT                1 /* Transfer to WAIT mode */
#define CHIP_REGFLDVAL_GPC_LPCR_A53_BSC_LPM_STOP                2 /* Transfer to STOP mode */

/**** Advanced Low power control register of A53 platform (GPC_LPCR_A53_AD) ***/
#define CHIP_REGFLD_GPC_LPCR_A53_AD_L2PGE                       (0x00000001UL << 31U) /* (RW) L2 cache RAM will not power down with SCU power domain in A53 platform (used for ALL_OFF mode) */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C3_PUP                   (0x00000001UL << 27U) /* (RW) CORE3 will power up with low power mode request (only used wake up from CPU_OFF) */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C3_PUP_IRQ               (0x00000001UL << 26U) /* (RW) CORE3 will power up with IRQ request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C2_PUP                   (0x00000001UL << 25U) /* (RW) CORE2 will power up with low power mode request (only used wake up from CPU_OFF) */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C2_PUP_IRQ               (0x00000001UL << 24U) /* (RW) CORE2 will power up with IRQ request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C3_PDN                   (0x00000001UL << 19U) /* (RW) CORE3 will be power down with low power mode request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C3_WFI_PDN               (0x00000001UL << 18U) /* (RW) CORE3 will be power down with WFI request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C2_PDN                   (0x00000001UL << 17U) /* (RW) CORE2 will be power down with low power mode request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C2_WFI_PDN               (0x00000001UL << 16U) /* (RW) CORE2 will be power down with WFI request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C1_PUP                   (0x00000001UL << 11U) /* (RW) CORE1 will power up with low power mode request (only used wake up from CPU_OFF) */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C1_PUP_IRQ               (0x00000001UL << 10U) /* (RW) CORE1 will power up with IRQ request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C0_PUP                   (0x00000001UL <<  9U) /* (RW) CORE0 will power up with low power mode request (only used wake up from CPU_OFF) */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C0_PUP_IRQ               (0x00000001UL <<  8U) /* (RW) CORE0 will power up with IRQ request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_L2_WFI_PDN               (0x00000001UL <<  5U) /* (RW) SCU and L2 will not be power down with WFI request (1 on reset to mask error request on reset, must to set to 0 in sw) */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_PLAT_PDN                 (0x00000001UL <<  4U) /* (RW) SCU and L2 cache RAM will be power down with low power mode request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C1_PDN                   (0x00000001UL <<  3U) /* (RW) CORE1 will be power down with low power mode request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C1_WFI_PDN               (0x00000001UL <<  2U) /* (RW) CORE1 will be power down with WFI request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C0_PDN                   (0x00000001UL <<  1U) /* (RW) CORE0 will be power down with low power mode request */
#define CHIP_REGFLD_GPC_LPCR_A53_AD_EN_C0_WFI_PDN               (0x00000001UL <<  0U) /* (RW) CORE0 will be power down with WFI request */
#define CHIP_REGMSK_GPC_LPCR_A53_AD_RESERVED                    (0x70F0F0C0UL)


/**** Low power control register of CPU1 (GPC_LPCR_M7) ************************/
#define CHIP_REGFLD_GPC_LPCR_M7_MASK_DSM_TRIGGER                (0x00000001UL << 31U) /* (RW) DSM trigger of M7 platform will be masked */
#define CHIP_REGFLD_GPC_LPCR_M7_MASK_M7_WFI                     (0x00000001UL << 16U) /* (RW) WFI for M7 is masked */
#define CHIP_REGFLD_GPC_LPCR_M7_CPU_CLK_ON_LPM                  (0x00000001UL << 14U) /* (RW) M7 clock enabled on wait/stop mode */
#define CHIP_REGFLD_GPC_LPCR_M7_EN_M7_PUP                       (0x00000001UL <<  3U) /* (RW) Enable M7 virtual PGC power up with LPM enter */
#define CHIP_REGFLD_GPC_LPCR_M7_EN_M7_PDN                       (0x00000001UL <<  2U) /* (RW) Enable M7 virtual PGC power down with LPM enter */
#define CHIP_REGFLD_GPC_LPCR_M7_EN_M7_LPM                       (0x00000003UL <<  0U) /* (RW) Setting the low power mode that system will enter on next assertion of dsm_request signal. */
#define CHIP_REGMSK_GPC_LPCR_M7_RESERVED                        (0x7FFEBFF0UL)

/**** System low power control register (GPC_SLPCR) ***************************/
#define CHIP_REGFLD_GPC_SLPCR_EN_DSM                            (0x00000001UL << 31U) /* (RW) DSM enable */
#define CHIP_REGFLD_GPC_SLPCR_RBC_EN                            (0x00000001UL << 30U) /* (RW) REG_BYPASS_COUNTER enabled (start after standby voltage is requested) */
#define CHIP_REGFLD_GPC_SLPCR_REG_BYPASS_COUNT                  (0x0000003FUL << 24U) /* (RW) Counter for REG_BYPASS signal assertion after standby voltage request by PMIC_STBY_REQ (0-63 CKIL clock period delay) */
#define CHIP_REGFLD_GPC_SLPCR_DISABLE_A53_IS_DSM                (0x00000001UL << 23U) /* (RW) Disable A53 isolation signal in DSM */
#define CHIP_REGFLD_GPC_SLPCR_EN_M7_FASTWUP_STOP_MODE           (0x00000001UL << 19U) /* (RW) Enable M7 fast wake up stop mode, relevant PLLs will not be closed in this mode. */
#define CHIP_REGFLD_GPC_SLPCR_EN_M7_FASTWUP_WAIT_MODE           (0x00000001UL << 18U) /* (RW) Enable M7 fast wake up wait mode, relevant PLLs will not be closed in this mode. */
#define CHIP_REGFLD_GPC_SLPCR_EN_A53_FASTWUP_STOP_MODE          (0x00000001UL << 17U) /* (RW) Enable A53 fast wake up stop mode, relevant PLLs will not be closed in this mode. */
#define CHIP_REGFLD_GPC_SLPCR_EN_A53_FASTWUP_WAIT_MODE          (0x00000001UL << 16U) /* (RW) Enable A53 fast wake up wait mode, relevant PLLs will not be closed in this mode. */
#define CHIP_REGFLD_GPC_SLPCR_OSCCNT                            (0x000000FFUL <<  8U) /* (RW) Oscillator ready counter value (1-256 32KHz cycles) */
#define CHIP_REGFLD_GPC_SLPCR_COSC_EN                           (0x00000001UL <<  7U) /* (RW) Enable on-chip oscillator */
#define CHIP_REGFLD_GPC_SLPCR_COSC_PWRDOWN                      (0x00000001UL <<  6U) /* (RW) Manually powering down of on chip oscillator */
#define CHIP_REGFLD_GPC_SLPCR_STBY_COUNT                        (0x00000007UL <<  3U) /* (RW) Standby counter (wait from PMIC_STBY_REQ negation and the check of assertion of PMIC_READY), 2^(2+x) ckil */
#define CHIP_REGFLD_GPC_SLPCR_VSTBY                             (0x00000001UL <<  2U) /* (RW) Voltage standby request (if PMIC_STBY_REQ to external circuit will be asserted in stop mode) */
#define CHIP_REGFLD_GPC_SLPCR_SBYOS                             (0x00000001UL <<  1U) /* (RW) Standby clock oscillator (if cosc_pwrdown will be asserted in DSM) */
#define CHIP_REGFLD_GPC_SLPCR_BYPASS_PMIC_READY                 (0x00000001UL <<  0U) /* (RW) GPC will bypass waiting for PMIC_READY signal when coming out of DSM*/
#define CHIP_REGMSK_GPC_SLPCR_RESERVED                          (0x00700000UL)

/**** MASTER LPM Handshake (GPC_MST_CPU_MAPPING) ******************************/
#define CHIP_REGFLD_GPC_MST_CPU_MAPPING_MST2_CPU_MAPPING        (0x00000001UL <<  2U) /* (RW) MASTER2 CPU Mapping. GPC will send out power off requirement. Should be opposite to LPCR_A53_BSC[8] */
#define CHIP_REGFLD_GPC_MST_CPU_MAPPING_MST1_CPU_MAPPING        (0x00000001UL <<  1U) /* (RW) MASTER1 CPU Mapping. GPC will send out power off requirement. Should be opposite to LPCR_A53_BSC[7] */
#define CHIP_REGFLD_GPC_MST_CPU_MAPPING_MST0_CPU_MAPPING        (0x00000001UL <<  0U) /* (RW) MASTER0 CPU Mapping. GPC will send out power off requirement. Should be opposite to LPCR_A53_BSC[6] */
#define CHIP_REGMSK_GPC_MST_CPU_MAPPING_RESERVED                (0xFFFFFFF8UL)


/* ------------- */




/**** CPU PGC software power up trigger (GPC_CPU_PGC_SW_PUP_REQ) **************/
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PUP_REQ_SCU_A53              (0x00000001UL <<  4U) /* (RWSC) Software power up trigger for SCU A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PUP_REQ_CORE3_A53            (0x00000001UL <<  3U) /* (RWSC) Software power up trigger for Core3 A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PUP_REQ_CORE2_A53            (0x00000001UL <<  2U) /* (RWSC) Software power up trigger for Core2 A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PUP_REQ_CORE1_A53            (0x00000001UL <<  1U) /* (RWSC) Software power up trigger for Core1 A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PUP_REQ_CORE0_A53            (0x00000001UL <<  0U) /* (RWSC) Software power up trigger for Core0 A53 PGC */
#define CHIP_REGMSK_GPC_CPU_PGC_SW_PUP_REQ_RESERVED             (0xFFFFFFE0UL)


/**** MIX PGC software power up trigger (GPC_MIX_PGC_SW_PUP_REQ) **************/
#define CHIP_REGFLD_GPC_MIX_PGC_SW_PUP_REQ_NOC                  (0x00000001UL <<  1U) /* (RWSC) Software power up trigger for NOC PGC */
#define CHIP_REGFLD_GPC_MIX_PGC_SW_PUP_REQ_MF                   (0x00000001UL <<  0U) /* (RWSC) Software power up trigger for MIX PGC */
#define CHIP_REGMSK_GPC_MIX_PGC_SW_PUP_REQ_RESERVED             (0xFFFFFFFCUL)


/**** PU PGC software up trigger (GPC_PU_PGC_SW_PUP_REQ) **********************/
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_DDRMIX                (0x00000001UL << 19U) /* (RWSC) Software power up trigger for DDRMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_MEDIA_ISP_DWP         (0x00000001UL << 18U) /* (RWSC) Software power up trigger for MEDIA_ISP_DWP */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_MHSIOMIX              (0x00000001UL << 17U) /* (RWSC) Software power up trigger for MHSIOMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_MIPI_PHY2             (0x00000001UL << 16U) /* (RWSC) Software power up trigger for MIPI_PHY2 */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_HDMI_PHY              (0x00000001UL << 15U) /* (RWSC) Software power up trigger for HDMI_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_HDMIMIX               (0x00000001UL << 14U) /* (RWSC) Software power up trigger for HDMIMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_VPU_VC8K              (0x00000001UL << 13U) /* (RWSC) Software power up trigger for VPU_VC8K */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_VPU_G2                (0x00000001UL << 12U) /* (RWSC) Software power up trigger for VPU_G2 */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_VPU_G1                (0x00000001UL << 11U) /* (RWSC) Software power up trigger for VPU_G1 */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_MEDIMIX               (0x00000001UL << 10U) /* (RWSC) Software power up trigger for MEDIMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_GPU_3D                (0x00000001UL <<  9U) /* (RWSC) Software power up trigger for GPU_3D */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_VPUMIX                (0x00000001UL <<  8U) /* (RWSC) Software power up trigger for VPUMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_GPU_SHARE_LOGIC       (0x00000001UL <<  7U) /* (RWSC) Software power up trigger for GPU_SHARE_LOGIC */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_GPU_2D                (0x00000001UL <<  6U) /* (RWSC) Software power up trigger for GPU_2D */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_AUDIOMIX              (0x00000001UL <<  5U) /* (RWSC) Software power up trigger for AUDIOMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_MLMIX_PHY             (0x00000001UL <<  4U) /* (RWSC) Software power up trigger for MLMIX_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_USB2_PHY              (0x00000001UL <<  3U) /* (RWSC) Software power up trigger for USB2_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_USB1_PHY              (0x00000001UL <<  2U) /* (RWSC) Software power up trigger for USB1_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_PCIE_PHY              (0x00000001UL <<  1U) /* (RWSC) Software power up trigger for PCIE_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PUP_REQ_MIPI_PHY1             (0x00000001UL <<  0U) /* (RWSC) Software power up trigger for MIPI_PHY1 */
#define CHIP_REGMSK_GPC_PU_PGC_SW_PUP_REQ_RESERVED              (0xFFF00000UL)


/**** CPU PGC software power down trigger (GPC_CPU_PGC_SW_PDN_REQ) ************/
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PDN_REQ_SCU_A53              (0x00000001UL <<  4U) /* (RWSC) Software power down trigger for SCU A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PDN_REQ_CORE3_A53            (0x00000001UL <<  3U) /* (RWSC) Software power down trigger for Core3 A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PDN_REQ_CORE2_A53            (0x00000001UL <<  2U) /* (RWSC) Software power down trigger for Core2 A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PDN_REQ_CORE1_A53            (0x00000001UL <<  1U) /* (RWSC) Software power down trigger for Core1 A53 PGC */
#define CHIP_REGFLD_GPC_CPU_PGC_SW_PDN_REQ_CORE0_A53            (0x00000001UL <<  0U) /* (RWSC) Software power down trigger for Core0 A53 PGC */
#define CHIP_REGMSK_GPC_CPU_PGC_SW_PDN_REQ_RESERVED             (0xFFFFFFE0UL)


/**** MIX PGC software power down trigger (GPC_MIX_PGC_SW_PDN_REQ) ************/
#define CHIP_REGFLD_GPC_MIX_PGC_SW_PDN_REQ_NOC                  (0x00000001UL <<  1U) /* (RWSC) Software power down trigger for NOC PGC */
#define CHIP_REGFLD_GPC_MIX_PGC_SW_PDN_REQ_MF                   (0x00000001UL <<  0U) /* (RWSC) Software power down trigger for MIX PGC */
#define CHIP_REGMSK_GPC_MIX_PGC_SW_PDN_REQ_RESERVED             (0xFFFFFFFCUL)

/**** PU PGC software down trigger (GPC_PU_PGC_SW_PDN_REQ) ********************/
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_DDRMIX                (0x00000001UL << 19U) /* (RWSC) Software power down trigger for DDRMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_MEDIA_ISP_DWP         (0x00000001UL << 18U) /* (RWSC) Software power down trigger for MEDIA_ISP_DWP */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_MHSIOMIX              (0x00000001UL << 17U) /* (RWSC) Software power down trigger for MHSIOMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_MIPI_PHY2             (0x00000001UL << 16U) /* (RWSC) Software power down trigger for MIPI_PHY2 */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_HDMI_PHY              (0x00000001UL << 15U) /* (RWSC) Software power down trigger for HDMI_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_HDMIMIX               (0x00000001UL << 14U) /* (RWSC) Software power down trigger for HDMIMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_VPU_VC8K              (0x00000001UL << 13U) /* (RWSC) Software power down trigger for VPU_VC8K */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_VPU_G2                (0x00000001UL << 12U) /* (RWSC) Software power down trigger for VPU_G2 */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_VPU_G1                (0x00000001UL << 11U) /* (RWSC) Software power down trigger for VPU_G1 */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_MEDIMIX               (0x00000001UL << 10U) /* (RWSC) Software power down trigger for MEDIMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_GPU_3D                (0x00000001UL <<  9U) /* (RWSC) Software power down trigger for GPU_3D */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_VPUMIX                (0x00000001UL <<  8U) /* (RWSC) Software power down trigger for VPUMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_GPU_SHARE_LOGIC       (0x00000001UL <<  7U) /* (RWSC) Software power down trigger for GPU_SHARE_LOGIC */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_GPU_2D                (0x00000001UL <<  6U) /* (RWSC) Software power down trigger for GPU_2D */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_AUDIOMIX              (0x00000001UL <<  5U) /* (RWSC) Software power down trigger for AUDIOMIX */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_MLMIX_PHY             (0x00000001UL <<  4U) /* (RWSC) Software power down trigger for MLMIX_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_USB2_PHY              (0x00000001UL <<  3U) /* (RWSC) Software power down trigger for USB2_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_USB1_PHY              (0x00000001UL <<  2U) /* (RWSC) Software power down trigger for USB1_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_PCIE_PHY              (0x00000001UL <<  1U) /* (RWSC) Software power down trigger for PCIE_PHY */
#define CHIP_REGFLD_GPC_PU_PGC_SW_PDN_REQ_MIPI_PHY1             (0x00000001UL <<  0U) /* (RWSC) Software power down trigger for MIPI_PHY1 */
#define CHIP_REGMSK_GPC_PU_PGC_SW_PDN_REQ_RESERVED              (0xFFF00000UL)




/* ------------- */

/**** Power handshake register (GPC_PU_PWRHSK) ********************************/
#define CHIP_REGFLD_GPC_PU_PWRHSK_AUDIOMIX_PWRDNACKN            (0x00000001UL << 31U) /* (RO) Audiomix noc power down ackn. Read 0 before power down audiomix switchable domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_MEDIAMIX_NOC_ADBS_PWRDNACKN   (0x00000001UL << 30U) /* (RO?) Mediamix noc and adbs power down ackn. Read 0 before power down mediamix default domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_HDMIMIX_NOC_PWRDNACKN         (0x00000001UL << 29U) /* (RO?) Hdmimix noc power down ackn. Read 0 before power down hdmimix switchable domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2HSIO_ADBS_PWDWNACKN       (0x00000001UL << 28U) /* (RO?) Main noc 2 hsio and adbs power down ackn. Read 0 before power down main noc or hsio switchable domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2DDRMIX_PWRDNACKN          (0x00000001UL << 27U) /* (RO?) Main noc 2 ddrmix power down ackn. Read 0 before power down main noc or ddrmix switchable domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_VPUMIX_NOX_PWDWNACKN          (0x00000001UL << 26U) /* (RO?) Vpumix noc power down ackn. Read 0 before power down vpumix default share logic domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_GPUMIX_NOC_ADBS_PWRDNACKN     (0x00000001UL << 25U) /* (RO?) gpumix noc and adbs power down ackn. Read 0 before power down gpumix default share logic domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2MLMIX_PWDWNACKN           (0x00000001UL << 24U) /* (RO?) Main noc 2 mlmix power down ackn. Read 0 before power down mlmix switchable domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_MLMIX_ADBS_PWRDNACKN          (0x00000001UL << 23U) /* (RO?) Mlmix adbs power down ackn. Read 0 before power down mlmix switchable domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_SUPERMIX2NOC_ADBS_PWDWNACKN   (0x00000001UL << 22U) /* (RO?) Supermix 2 noc adbs power down ackn. Read 0 before power down main noc domain. It can becovered by hardware handshake follow. */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2SUPERMIX_ADBS_PWDWNACKN   (0x00000001UL << 21U) /* (RO?) Main noc 2 Supermix adbs power down ackn. Read 0 before power down main noc domain. It can be covered by hardware handshake follow. */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2AUDIOMIX_PWDWNACKN        (0x00000001UL << 20U) /* (RO?) Main noc 2 audiomix power down ackn. Read 0 before power down main noc or audiomix switchable domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_DDR1_CACTIVE                  (0x00000001UL << 19U) /* (RO?) DDR1 AXI Clock Active */
#define CHIP_REGFLD_GPC_PU_PWRHSK_DDR1_CTRL_REQACK              (0x00000001UL << 18U) /* (RO?) DDR1 AXI Low-Power Request ack */
#define CHIP_REGFLD_GPC_PU_PWRHSK_DDR1_CTRL_CLKACTIVE           (0x00000001UL << 17U) /* (RO?) DDR1 controller Hardware Low-Power Clock active */
#define CHIP_REGFLD_GPC_PU_PWRHSK_DDR1_CTRL_LWPWACKN            (0x00000001UL << 16U) /* (RO?) DDR1 controller Hardware Low_Power ack */
#define CHIP_REGFLD_GPC_PU_PWRHSK_AUDIOMIX_NOC_PWRDNREQN        (0x00000001UL << 15U) /* (RW) Audiomix noc power down request. Write 0 and wait according ackn before power down audiomix switchable domain or main noc domain*/
#define CHIP_REGFLD_GPC_PU_PWRHSK_MEDIAMIX_NOC_ADBS_PWRDNREQN   (0x00000001UL << 14U) /* (RW) Mediamix noc and adbs power down request. Write 0 and wait according ackn before power down mediamix default domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_HDMIMIX_NOC_PWRDNREQN         (0x00000001UL << 13U) /* (RW) Hdmimix noc power down request. Write 0 and wait according ackn before power down hdmimix switchable domain or main noc domain  */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2HSIO_ADBS_PWRDNREQN       (0x00000001UL << 12U) /* (RW) Main noc 2 hsio and adbs power down request. Write 0 and wait according ackn before power down main noc or hsio switchable domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2DDRMIX_PWRDNREQN          (0x00000001UL << 11U) /* (RW) Main noc 2 ddrmix power down request. Write 0 and wait according ackn before power down main noc or ddrmix switchable domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_VPUMIX_NOC_PWRDNREQN          (0x00000001UL << 10U) /* (RW) Vpumix noc power down request. Write 0 and wait according ackn before power down vpumix defaut share logic domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_GPUMIX_NOC_ADBS_PWRDNREQN     (0x00000001UL <<  9U) /* (RW) gpumix noc and adbs power down request. Write 0 and wait according ackn before power down gpumix default share logic domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2MLMIX_PWRDNREQN           (0x00000001UL <<  8U) /* (RW) Main noc 2 mlmix power down request. Write 0 and wait according ackn before power down mlmix switchable domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_MLMIX_ADBS_PWRDNREQ           (0x00000001UL <<  7U) /* (RW) Mlmix adbs power down request. Write 0 and wait according ackn before power down mlmix switchable domain or main noc domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_SUPERMIX2NOC_PWRDNREQN        (0x00000001UL <<  6U) /* (RW) Supermix 2 noc adbs power down request. Write 0 and wait according ackn before power down main noc domain. It can be covered by hardware handshake follow */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2SUPERMIX_PWRDNREQN        (0x00000001UL <<  5U) /* (RW) DISPMIX ADB400 power down request. Active 0 */
#define CHIP_REGFLD_GPC_PU_PWRHSK_NOC2AUDIOMIX_PWRDNREQN        (0x00000001UL <<  4U) /* (RW) Main noc 2 audiomix power down request. Write 0 and wait according ackn before power down main noc or audiomix switchable domain */
#define CHIP_REGFLD_GPC_PU_PWRHSK_DDR1_AXI_CSYSREQ              (0x00000001UL <<  1U) /* (RW) DDR1 AXI Low-Power Request */
#define CHIP_REGFLD_GPC_PU_PWRHSK_DDR1_CORE_CSYSREQ             (0x00000001UL <<  0U) /* (RW) DDR1 controller Hardware Low-Power Request */
#define CHIP_REGMSK_GPC_PU_PWRHSK_RESERVED                      (0x0000000CUL)




/* ------------- */

/**** PGC CPU A53/M7 mapping (GPC_PGC_CPU_A53_MAPPING, GPC_PGC_CPU_M7_MAPPING) */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_DDRMIX                  (0x00000001UL << 21U) /* (RW) Map DDR to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_MEDIA_ISP_DWP           (0x00000001UL << 20U) /* (RW) Map MEDIA_ISP_DWP_DOMAIN to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_HSIOMIX                 (0x00000001UL << 19U) /* (RW) Map HSIOMIX to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_MIPI_PHY2               (0x00000001UL << 18U) /* (RW) Map MIPI PHY2 to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_HDMI_PHY                (0x00000001UL << 17U) /* (RW) Map HDMI PHY to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_HDMIMIX                 (0x00000001UL << 16U) /* (RW) Map HDMI to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_VPU_VC8K                (0x00000001UL << 15U) /* (RW) Map VPU_VC8K to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_VPU_G2                  (0x00000001UL << 14U) /* (RW) Map VPU_G2 to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_VPU_G1                  (0x00000001UL << 13U) /* (RW) Map VPU_G1 to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_MEDIMIX                 (0x00000001UL << 12U) /* (RW) Map MEDIMIX to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_GPU_3D                  (0x00000001UL << 11U) /* (RW) Map GPU 3D to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_VPUMIX_SHARE_LOGIC      (0x00000001UL << 10U) /* (RW) Map VPUMIX Share Logic to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_GPU_SHARE_LOGIC         (0x00000001UL <<  9U) /* (RW) Map GPU Share Logic to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_GPU_2D                  (0x00000001UL <<  8U) /* (RW) Map GPU 2D to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_AUDIOMIX                (0x00000001UL <<  7U) /* (RW) Map AUDIOMIX to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_MLMIX                   (0x00000001UL <<  6U) /* (RW) Map MLMIX to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_USB2_PHY                (0x00000001UL <<  5U) /* (RW) Map USB2_PHY to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_USB1_PHY                (0x00000001UL <<  4U) /* (RW) Map USB1_PHY to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_PCIE_PHY                (0x00000001UL <<  3U) /* (RW) Map PCIE_PHY to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_MIPI_PHY1               (0x00000001UL <<  2U) /* (RW) Map MIPI_PHY1 to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_MIX1_NOC                (0x00000001UL <<  1U) /* (RW) Map MIX1 (NOC)to A53/M7 domain */
#define CHIP_REGFLD_GPC_PGC_CPU_MAPPING_MIX0_SUPERMIXM7         (0x00000001UL <<  0U) /* (RW) Map M7 MIX0 (SUPERMIXM7) to A53/M7 domain */
#define CHIP_REGMSK_GPC_PGC_CPU_MAPPING_RESERVED                (0xFFC00000UL)

#endif // REGS_GPC_H


