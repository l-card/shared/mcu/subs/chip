#ifndef REGS_USBPD_H
#define REGS_USBPD_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

/* PD Registers */
typedef struct {
    union {
        __IO uint32_t R32_CONFIG;           /* PD configuration register,           Address offset: 0x00 */
        struct {
            __IO uint16_t CONFIG;           /* PD Interrupt Enable Register,        Address offset: 0x00 */
            __IO uint16_t BMC_CLK_CNT;      /* BMC sampling clock counter,          Address offset: 0x02 */
        };
    };
    union {
        __IO uint32_t R32_CONTROL;          /* PD control register,                 Address offset: 0x04 */
        struct {
            union {
                __IO uint16_t R16_CONTROL;  /* PD transceiver control register,     Address offset: 0x04 */
                struct {
                    __IO uint8_t  CONTROL;  /* PD transceiver enable register,      Address offset: 0x04 */
                    __IO uint8_t  TX_SEL;   /* PD transmit SOP selection register,  Address offset: 0x05 */
                };
            };
            __IO uint16_t BMC_TX_SZ;        /* PD transmit length register,         Address offset: 0x06 */
        };
    };
    union {
        __IO uint32_t R32_STATUS;           /* PD status register,                  Address offset: 0x08 */
        struct {
            union  {
                __IO uint16_t R16_STATUS;   /* PD interrupt and data register,      Address offset: 0x08 */
                struct {
                    __IO uint8_t  DATA_BUF; /* DMA cache data register,             Address offset: 0x08 */
                    __IO uint8_t  STATUS;   /* PD interrupt flag register,          Address offset: 0x09 */
                };
            };
            __IO uint16_t BMC_BYTE_CNT;     /* Byte counter,                        Address offset: 0x0A */
        };
    };
    union {
        __IO uint32_t R32_PORT;             /* Port control register,               Address offset: 0x0C */
        union {
            struct {
                __IO uint16_t PORT_CC1;         /* CC1 port control register,           Address offset: 0x0C */
                __IO uint16_t PORT_CC2;         /* CC2 port control register,           Address offset: 0x0E */
            };
            __IO uint16_t PORT_CC[2];
        };
    };
    union {
        __IO uint32_t R32_DMA;              /* DMA cache address register,          Address offset: 0x10 */
        struct  {
            __IO uint16_t DMA;              /* PD buffer start address register,    Address offset: 0x10 */
            __IO uint16_t RESERVED;
        };
    };
} CHIP_REGS_USBPD_T;


#define CHIP_REGS_USBPD                             ((CHIP_REGS_USBPD_T *) CHIP_MEMRGN_ADDR_PERIPH_USBPD)


/*********** PD Interrupt Enable Register (USBPD_CONFIG) **********************/
#define CHIP_REGFLD_USBPD_CONFIG_IE_TX_END          (0x0001UL << 15U) /* (RW) End-of-transmit interrupt enable */
#define CHIP_REGFLD_USBPD_CONFIG_IE_RX_RESET        (0x0001UL << 14U) /* (RW) Receive reset interrupt enable */
#define CHIP_REGFLD_USBPD_CONFIG_IE_RX_ACT          (0x0001UL << 13U) /* (RW) Receive completion interrupt enable */
#define CHIP_REGFLD_USBPD_CONFIG_IE_RX_BYTE         (0x0001UL << 12U) /* (RW) Receive byte interrupt enable */
#define CHIP_REGFLD_USBPD_CONFIG_IE_RX_BIT          (0x0001UL << 11U) /* (RW) Receive bit interrupt enable */
#define CHIP_REGFLD_USBPD_CONFIG_IE_PD_IO           (0x0001UL << 10U) /* (RW) PD IO interrupt enable */
#define CHIP_REGFLD_USBPD_CONFIG_RTX_BIT0           (0x0001UL <<  9U) /* (RO) Current value of send/receive shift register bit 0 */
#define CHIP_REGFLD_USBPD_CONFIG_MULTI_0            (0x0001UL <<  8U) /* (RO) 5 consecutive bits of the 0 value have been received. */
#define CHIP_REGFLD_USBPD_CONFIG_WAKE_POLAR         (0x0001UL <<  5U) /* (RW) PD port wake-up leave. */
#define CHIP_REGFLD_USBPD_CONFIG_PD_RST_EN          (0x0001UL <<  4U) /* (RW) The PD mode reset command enable. */
#define CHIP_REGFLD_USBPD_CONFIG_PD_DMA_EN          (0x0001UL <<  3U) /* (RW) Enable DMA function and DMA interrupt. */
#define CHIP_REGFLD_USBPD_CONFIG_CC_SEL             (0x0001UL <<  2U) /* (RW) Select the current PD communication port (0 - CC1, 1 - CC2). */
#define CHIP_REGFLD_USBPD_CONFIG_PD_ALL_CLR         (0x0001UL <<  1U) /* (RW) PD mode clears all interrupt flags */
#define CHIP_REGFLD_USBPD_CONFIG_PD_FILT_ED         (0x0001UL <<  0U) /* (RW) PD pin input filter enable (!from src!) */
#define CHIP_REGMSK_USBPD_CONFIG_RESERVED           (0x00C0UL)

/*********** BMC Sampling Clock Counter (USBPD_BMC_CLK_CNT) *******************/
#define CHIP_REGFLD_USBPD_BMC_CLK_CNT               (0x01FFUL <<  0U) /* (RW) BMC transmits or receives sampling clock counters */
#define CHIP_REGMSK_USBPD_BMC_CLK_CNT_RESERVED      (0xFE00UL)

/*********** PD Transceiver Enable (USBPD_CONTROL) ****************************/
#define CHIP_REGFLD_USBPD_CONTROL_RX_ST_H           (0x01UL <<  7U) /* (RO) The receiving status is high */
#define CHIP_REGFLD_USBPD_CONTROL_RX_ST_L           (0x01UL <<  6U) /* (RO) The receiving status is low */
#define CHIP_REGFLD_USBPD_CONTROL_DATA_FLAG         (0x01UL <<  5U) /* (RO) Valid flag bits for cached data. */
#define CHIP_REGFLD_USBPD_CONTROL_BMC_START         (0x01UL <<  1U) /* (RW) BMC transmits a start signal */
#define CHIP_REGFLD_USBPD_CONTROL_PD_TX_EN          (0x01UL <<  0U) /* (RW) USBPD transceiver mode and transmit enable (0 - rx, 1 - tx). */
#define CHIP_REGMSK_USBPD_CONTROL_RESERVED          (0x1CUL)

/*********** PD Transmit SOP Selection (USBPD_TX_SEL) ************************/
#define CHIP_REGFLD_USBPD_TX_SEL_4                  (0x03UL <<  6U) /* (RW) Select the K-CODE4 type in PD transmitting mode */
#define CHIP_REGFLD_USBPD_TX_SEL_3                  (0x03UL <<  4U) /* (RW) Select the K-CODE3 type in PD transmitting mode */
#define CHIP_REGFLD_USBPD_TX_SEL_2                  (0x03UL <<  2U) /* (RW) Select the K-CODE2 type in PD transmitting mode */
#define CHIP_REGFLD_USBPD_TX_SEL_1                  (0x01UL <<  0U) /* (RW) Select the K-CODE1 type in PD transmitting mode */
#define CHIP_REGMSK_USBPD_TX_SEL_RESERVED           (0x02UL)


#define CHIP_REGFLDVAL_USBPD_TX_SEL_4_SYNC2         0
#define CHIP_REGFLDVAL_USBPD_TX_SEL_4_SYNC3         1
#define CHIP_REGFLDVAL_USBPD_TX_SEL_4_RST2          2

#define CHIP_REGFLDVAL_USBPD_TX_SEL_3_SYNC1         0
#define CHIP_REGFLDVAL_USBPD_TX_SEL_3_SYNC3         1
#define CHIP_REGFLDVAL_USBPD_TX_SEL_3_RST1          2

#define CHIP_REGFLDVAL_USBPD_TX_SEL_2_SYNC1         0
#define CHIP_REGFLDVAL_USBPD_TX_SEL_2_SYNC3         1
#define CHIP_REGFLDVAL_USBPD_TX_SEL_2_RST1          2

#define CHIP_REGFLDVAL_USBPD_TX_SEL_1_SYNC1         0
#define CHIP_REGFLDVAL_USBPD_TX_SEL_1_RST1          1

#define CHIP_USBPD_TX_SEL_OSET(k1,k2,k3,k4)         (LBITFIELD_SET(CHIP_REGFLD_USBPD_TX_SEL_1, k1) | \
                                                     LBITFIELD_SET(CHIP_REGFLD_USBPD_TX_SEL_2, k2) | \
                                                     LBITFIELD_SET(CHIP_REGFLD_USBPD_TX_SEL_3, k3) | \
                                                     LBITFIELD_SET(CHIP_REGFLD_USBPD_TX_SEL_4, k4))
#define CHIP_USBPD_TX_SEL_SOP                       CHIP_USBPD_TX_SEL_OSET(CHIP_REGFLDVAL_USBPD_TX_SEL_1_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_2_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_3_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_4_SYNC2)
#define CHIP_USBPD_TX_SEL_SOP1                      CHIP_USBPD_TX_SEL_OSET(CHIP_REGFLDVAL_USBPD_TX_SEL_1_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_2_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_3_SYNC3, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_4_SYNC3)
#define CHIP_USBPD_TX_SEL_SOP2                      CHIP_USBPD_TX_SEL_OSET(CHIP_REGFLDVAL_USBPD_TX_SEL_1_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_2_SYNC3, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_3_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_4_SYNC3)
#define CHIP_USBPD_TX_SEL_HARD_RST                  CHIP_USBPD_TX_SEL_OSET(CHIP_REGFLDVAL_USBPD_TX_SEL_1_RST1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_2_RST1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_3_RST1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_4_RST2)
#define CHIP_USBPD_TX_SEL_CABLE_RST                 CHIP_USBPD_TX_SEL_OSET(CHIP_REGFLDVAL_USBPD_TX_SEL_1_RST1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_2_SYNC1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_3_RST1, \
                                                                           CHIP_REGFLDVAL_USBPD_TX_SEL_4_SYNC3)


/*********** PD Transmit Length (USBPD_BMC_TX_SZ) *****************************/
#define CHIP_REGFLD_USBPD_BMC_TX_SZ                 (0x01FFUL <<  0U) /* (RW) The total length transmitted in PD mode */
#define CHIP_REGMSK_USBPD_BMC_TX_SZ_RESERVED        (0xFE00UL)

/*********** PD Interrupt Flag (USBPD_STATUS) *********************************/
#define CHIP_REGFLD_USBPD_STATUS_IF_TX_END          (0x01UL <<  7U) /* (RW1C) Transfer completed interrupt flag */
#define CHIP_REGFLD_USBPD_STATUS_IF_RX_RESET        (0x01UL <<  6U) /* (RW1C) Receive reset interrupt flag */
#define CHIP_REGFLD_USBPD_STATUS_IF_RX_ACT          (0x01UL <<  5U) /* (RW1C) Transfer completed interrupt flag */
#define CHIP_REGFLD_USBPD_STATUS_IF_RX_BYTE         (0x01UL <<  4U) /* (RW1C) Receive byte or SOP interrupt flag */
#define CHIP_REGFLD_USBPD_STATUS_IF_RX_BIT          (0x01UL <<  3U) /* (RW1C) Receive bit or 5bit interrupt flag */
#define CHIP_REGFLD_USBPD_STATUS_BUF_ERR            (0x01UL <<  2U) /* (RW1C) BUFFER or DMA error interrupt flag */
#define CHIP_REGFLD_USBPD_STATUS_BMC_AUX            (0x03UL <<  0U) /* (RO) Indicates the current PD status (SOP type on RX, CRC byte on TX) */
#define CHIP_REGFLD_USBPD_STATUS_IF_ALL             (0xFCUL)

#define CHIP_REGFLDVAL_USBPD_STATUS_BMC_AUX_RX_IDLE 0 /* Receiving idle or no valid packet received */
#define CHIP_REGFLDVAL_USBPD_STATUS_BMC_AUX_RX_SOP  1 /* SOP */
#define CHIP_REGFLDVAL_USBPD_STATUS_BMC_AUX_RX_SOP1 2 /* SOP' or Hard Reset */
#define CHIP_REGFLDVAL_USBPD_STATUS_BMC_AUX_RX_SOP2 3 /* SOP'' or Cable Reset */

/*********** Byte Counter (USBPD_BMC_BYTE_CN) *********************************/
#define CHIP_REGFLD_USBPD_BMC_BYTE_CN               (0x01FFUL <<  0U) /* (RO) Byte counter */
#define CHIP_REGMSK_USBPD_BMC_BYTE_CN_RESERVED      (0xFE00UL)

/*********** CC1/CC2 Port Control Register (USBPD_PORT_CC1/CC2) ***************/
#define CHIP_REGFLD_USBPD_PORT_CC_CE                (0x0007UL <<  5U) /* (RW) Enable the CCx port voltage comparator */
#define CHIP_REGFLD_USBPD_PORT_CC_LVE               (0x0001UL <<  4U) /* (RW) The CCx port outputs low voltage enable */
#define CHIP_REGFLD_USBPD_PORT_CC_PU                (0x0003UL <<  2U) /* (RW) CCx port pull-up current selection */
#define CHIP_REGFLD_USBPD_PORT_CC_PD                (0x0001UL <<  1U) /* (RW) Enable Rd pull-down resistor, about 5.1K Ω (not all packages!) */
#define CHIP_REGFLD_USBPD_PORT_CC_AI                (0x0001UL <<  0U) /* (RO) The CC port comparator analog input */
#define CHIP_REGMSK_USBPD_PORT_CC_RESERVED          (0xFF00UL)


#define CHIP_REGFLDVAL_USBPD_PORT_CC_CE_OFF         0 /* Off */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_CE_0_22V       2 /* 0.22V */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_CE_0_43V       3 /* 0.43V */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_CE_0_55V       4 /* 0.55V */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_CE_0_66V       5 /* 0.66V */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_CE_0_96V       6 /* 0.96V */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_CE_1_23V       7 /* 1.23V */

#define CHIP_REGFLDVAL_USBPD_PORT_CC_PU_DIS         0 /* No pull-up current */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_PU_330_UA      1 /* 330μA */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_PU_180_UA      2 /* 180μA */
#define CHIP_REGFLDVAL_USBPD_PORT_CC_PU_80_UA       3 /* 80μA */


#endif // REGS_USBPD_H
