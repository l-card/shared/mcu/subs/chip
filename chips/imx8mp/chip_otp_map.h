#ifndef CHIP_OTP_MAP_H
#define CHIP_OTP_MAP_H


#define CHIP_OTP_WORD_ADDR_LOCK0        0x00
#define CHIP_OTP_WORD_ADDR_LOCK1        0x01
#define CHIP_OTP_WORD_ADDR_UID0         0x02
#define CHIP_OTP_WORD_ADDR_UID1         0x03
#define CHIP_OTP_WORD_ADDR_MFG0         0x04
#define CHIP_OTP_WORD_ADDR_MFG1         0x05
#define CHIP_OTP_WORD_ADDR_GP4          0x06
#define CHIP_OTP_WORD_ADDR_BOOT_CFG0    0x07
#define CHIP_OTP_WORD_ADDR_BOOT_CFG1    0x08
#define CHIP_OTP_WORD_ADDR_BOOT_CFG2    0x09
#define CHIP_OTP_WORD_ADDR_BOOT_CFG3    0x0A
#define CHIP_OTP_WORD_ADDR_BOOT_CFG4    0x0B
#define CHIP_OTP_WORD_ADDR_USB_ID       0x22
#define CHIP_OTP_WORD_ADDR_MAC0         0x24
#define CHIP_OTP_WORD_ADDR_MAC1         0x25
#define CHIP_OTP_WORD_ADDR_MAC2         0x26
#define CHIP_OTP_WORD_ADDR_GP1_0        0x38
#define CHIP_OTP_WORD_ADDR_GP1_1        0x39
#define CHIP_OTP_WORD_ADDR_GP2_0        0x3A
#define CHIP_OTP_WORD_ADDR_GP2_1        0x3B
#define CHIP_OTP_WORD_ADDR_UID2         0xA0
#define CHIP_OTP_WORD_ADDR_UID3         0xA1
#define CHIP_OTP_WORD_ADDR_GP6_0        0xA4
#define CHIP_OTP_WORD_ADDR_GP6_1        0xA5
#define CHIP_OTP_WORD_ADDR_GP6_2        0xA6
#define CHIP_OTP_WORD_ADDR_GP6_3        0xA7
#define CHIP_OTP_WORD_ADDR_GP7_0        0xA8
#define CHIP_OTP_WORD_ADDR_GP7_1        0xA9
#define CHIP_OTP_WORD_ADDR_GP7_2        0xAA
#define CHIP_OTP_WORD_ADDR_GP7_3        0xAB
#define CHIP_OTP_WORD_ADDR_GP8_0        0xAC
#define CHIP_OTP_WORD_ADDR_GP8_1        0xAD
#define CHIP_OTP_WORD_ADDR_GP8_2        0xAE
#define CHIP_OTP_WORD_ADDR_GP8_3        0xAF
#define CHIP_OTP_WORD_ADDR_GP9_0        0xB0
#define CHIP_OTP_WORD_ADDR_GP9_1        0xB1
#define CHIP_OTP_WORD_ADDR_GP9_2        0xB2
#define CHIP_OTP_WORD_ADDR_GP9_3        0xB3

/* Table 6-35 */

/************ Value of OTP Bank0 Word0 (Lock control 0) ***********************/
#define CHIP_OTP_WORD_FLD_LOCK0_GP2                                     (0x00000003UL << 22U)   /* (RW)  Status of shadow register and OTP write lock for gp2 region */
#define CHIP_OTP_WORD_FLD_LOCK0_GP1                                     (0x00000003UL << 20U)   /* (RW)  Status of shadow register and OTP write lock for gp1 region */
#define CHIP_OTP_WORD_FLD_LOCK0_MAC_ADDR                                (0x00000003UL << 14U)   /* (RW)  Status of shadow register and OTP write lock for mac_addr region */
#define CHIP_OTP_WORD_FLD_LOCK0_USB_ID                                  (0x00000003UL << 12U)   /* (RW)  Status of shadow register and OTP write lock for usb_id region */
#define CHIP_OTP_WORD_MSK_LOCK0_RESERVED                                (0xFF0F0FFFUL)

#define CHIP_OTP_WORD_FLDVAL_LOCK_UNLOCK                                0 /* The controlled field can be read, sensed, burned or overridden  */
#define CHIP_OTP_WORD_FLDVAL_LOCK_OP                                    1 /* Override Protect, the controlled field can be read, sensed or burned, but can't be overridden */
#define CHIP_OTP_WORD_FLDVAL_LOCK_WP                                    2 /* Write Protect, the controlled field can be read, sensed or overridden, but can't be burned */
#define CHIP_OTP_WORD_FLDVAL_LOCK_OPWP                                  3 /* The controlled field can be read or sensed only, but can't be burned or overridden */

/************ Value of OTP Bank0 Word1 (Lock control 1) ***********************/
#define CHIP_OTP_WORD_FLD_LOCK1_GP9                                     (0x00000003UL << 16U)   /* (RW)  Status of shadow register and OTP write lock for g9 region */
#define CHIP_OTP_WORD_FLD_LOCK1_GP8                                     (0x00000003UL << 14U)   /* (RW)  Status of shadow register and OTP write lock for g8 region */
#define CHIP_OTP_WORD_FLD_LOCK1_GP7                                     (0x00000003UL << 12U)   /* (RW)  Status of shadow register and OTP write lock for g7 region */
#define CHIP_OTP_WORD_FLD_LOCK1_GP6                                     (0x00000003UL << 10U)   /* (RW)  Status of shadow register and OTP write lock for g6 region */
#define CHIP_OTP_WORD_FLD_LOCK1_GP4                                     (0x00000003UL <<  4U)   /* (RW)  Status of shadow register and OTP write lock for g4 region */
#define CHIP_OTP_WORD_MSK_LOCK1_RESERVED                                (0xFFFC03C0UL)


/*********** MFG0 *************************************************************/
#define CHIP_OTP_WORD_FLD_MFG0_SPEED_GRADING                            (0x0000003FUL <<  8U) /* (RO) Burned by tester program, for indicating IC core speed */
#define CHIP_OTP_WORD_FLD_MFG0_M7_DISABLE                               (0x00000001UL << 21U) /* Disable M7 Core */
#define CHIP_OTP_WORD_FLD_MFG0_VPU_G1_DISABLE                           (0x00000001UL << 24U) /* Disable G1 Decoder in VPU */
#define CHIP_OTP_WORD_FLD_MFG0_VPU_G2_DISABLE                           (0x00000001UL << 25U) /* Disable G2 Decoder in VPU */
#define CHIP_OTP_WORD_FLD_MFG0_CAN_DISABLE                              (0x00000001UL << 28U) /* Disable CAN functionality */
#define CHIP_OTP_WORD_FLD_MFG0_CAN_FD_DISABLE                           (0x00000001UL << 29U) /* Disable CAN-FD functionality */
#define CHIP_OTP_WORD_FLD_MFG0_VPU_VC8000E_DISABLE                      (0x00000001UL << 30U) /* Disable VC8000E Encoder in VPU */
#define CHIP_OTP_WORD_MSK_MFG0_RESERVED                                 (0x8CDFC0FFUL)

/*********** MFG1 *************************************************************/
#define CHIP_OTP_WORD_FLD_MFG1_IMG_ISP1_DISABLE                         (0x00000001UL <<  0U) /* Disable IMAGING_ISP1 IP */
#define CHIP_OTP_WORD_FLD_MFG1_IMG_ISP2_DISABLE                         (0x00000001UL <<  1U) /* Disable IMAGING_ISP2 IP */
#define CHIP_OTP_WORD_FLD_MFG1_IMG_DEWARP_DISABLE                       (0x00000001UL <<  2U) /* Disable IMAGING_DEWARP IP */
#define CHIP_OTP_WORD_FLD_MFG1_NPU_DISABLE                              (0x00000001UL <<  3U) /* Disable NPU IP */
#define CHIP_OTP_WORD_FLD_MFG1_AUDIO_PROCESSOR_DISABLE                  (0x00000001UL <<  4U) /* Disable HiFi4 DSP IP */
#define CHIP_OTP_WORD_FLD_MFG1_ASRC_DISABLE                             (0x00000001UL <<  5U) /* Disable Audio AS IP */
#define CHIP_OTP_WORD_FLD_MFG1_GPU2D_DISABLE                            (0x00000001UL <<  6U) /* Disable GPU 2D IP */
#define CHIP_OTP_WORD_FLD_MFG1_GPU3D_DISABLE                            (0x00000001UL <<  7U) /* Disable GPU 3D IP */
#define CHIP_OTP_WORD_FLD_MFG1_USB1_DISABLE                             (0x00000001UL <<  8U) /* Disable USB1 Controller IP */
#define CHIP_OTP_WORD_FLD_MFG1_USB2_DISABLE                             (0x00000001UL <<  9U) /* Disable USB2 Controller IP */
#define CHIP_OTP_WORD_FLD_MFG1_PCIE1_DISABLE                            (0x00000001UL << 11U) /* Disable PCIe-1 IP */
#define CHIP_OTP_WORD_FLD_MFG1_ENET1_DISABLE                            (0x00000001UL << 13U) /* Disable ENET-1 IP */
#define CHIP_OTP_WORD_FLD_MFG1_ENET2_DISABLE                            (0x00000001UL << 14U) /* Disable ENET-2 IP */
#define CHIP_OTP_WORD_FLD_MFG1_MIPI_CSI1_DISABLE                        (0x00000001UL << 15U) /* Disable MIPI CSI-1 IP */
#define CHIP_OTP_WORD_FLD_MFG1_MIPI_CSI2_DISABLE                        (0x00000001UL << 16U) /* Disable MIPI CSI-2 IP */
#define CHIP_OTP_WORD_FLD_MFG1_MIPI_DSI1_DISABLE                        (0x00000001UL << 17U) /* Disable MIPI DSI-1 IP */
#define CHIP_OTP_WORD_FLD_MFG1_LVDS1_DISABLE                            (0x00000001UL << 19U) /* Disable LVDS-1 IP */
#define CHIP_OTP_WORD_FLD_MFG1_LVDS2_DISABLE                            (0x00000001UL << 20U) /* Disable LVDS-2 IP */
#define CHIP_OTP_WORD_FLD_MFG1_EARC_RX_DISABLE                          (0x00000001UL << 30U) /* Disable eARC-RX IP */
#define CHIP_OTP_WORD_MSK_MFG1_RESERVED                                 (0xBFE41400UL)

/*********** USB_ID *************************************************************/
#define CHIP_OTP_WORD_FLD_USB_ID_VID                                    (0x0000FFFFUL <<  0U) /* USB VID */
#define CHIP_OTP_WORD_FLD_USB_ID_PID                                    (0x0000FFFFUL << 16U) /* USB PID */

/*********** BOOT_CFG0 ********************************************************/
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_FLEXSPI_AUTO_PROBE_TYPE             (0x00000003UL <<  0U) /* FLEXSPI Flash AutoProbe Type */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_FLEXSPI_AUTO_PROBE_EN               (0x00000001UL <<  2U) /* FLEXSPI Flash AutoProbe Enable */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_OVERRIDE_FLEXSPI_BT_SEL_VAL         (0x00000003UL <<  3U) /* FlexSPI Boot Selection Override Value */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_OVERRIDE_FLEXSPI_BT_SEL             (0x00000001UL <<  5U) /* Override FlexSPI Boot Selection (0 - Do not override, 1 - override) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL        (0x00000003UL <<  6U) /* Pages in block override value */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK            (0x00000001UL <<  8U) /* Override pages in block (0 - Do not override, 1 - override) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_OVERRIDE_USDHC_BT_SEL_VAL           (0x00000003UL <<  9U) /* USDHC boot Select override value */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_OVERRIDE_USDHC_BT_SEL               (0x00000001UL << 11U) /* Override USDHC boot selection (0 - Do not override, 1 - override) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_BOOT_MODE_FUSES                     (0x0000000FUL << 12U) /* Boot Rom will retrieve boot mode from these fuses instead of BOOT_MODE pins if: (BOOT_MODE_PINS=0 or FORCE_BT_FROM_FUSE =1) and BT_FUSE_SEL=1 */
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_BT_FUSE_SEL                         (0x00000001UL << 28U) /* BOOT configuration eFuses are programmed.*/
#define CHIP_OTP_WORD_FLD_BOOT_CFG0_FORCE_COLD_BOOT                     (0x00000001UL << 29U) /* Force cold boot when core comes out of reset. Reflected in SBMR register of SRC. */
#define CHIP_OTP_WORD_MSK_BOOT_CFG0_RESERVED                            (0xCFFF0000UL)


#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_FLEXSPI_AUTO_PROBE_TYPE_QSPI_NOR          0 /* QuadSPI NOR */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_FLEXSPI_AUTO_PROBE_TYPE_OCTAL_MACRONIX    1 /* MacronixOctal */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_FLEXSPI_AUTO_PROBE_TYPE_OCTAL_MICRON      2 /* MicronOctal */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_FLEXSPI_AUTO_PROBE_TYPE_OCTAL_ADESTO      3 /* AdestoOctal */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_FLEXSPI_BT_SEL_VAL_HYPERFLASH    0 /* FlexSPI (Hyperflash 1.8V) */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_FLEXSPI_BT_SEL_VAL_4B_RD         1 /* FlexSPI (Flash with 4B READ (1x13 default supported))*/
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_FLEXSPI_BT_SEL_VAL_OCTAL_MICRON  2 /* Default Octal mode (Micron) */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_FLEXSPI_BT_SEL_VAL_OCTAL_MACRONIX 3 /* Default Octal mode (Macronix) */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL_RAW_32       0 /* RAWNAND 32 pages */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL_RAW_64       1 /* RAWNAND 64 pages */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL_RAW_128      2 /* RAWNAND 128 pages */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL_QSPI_64      0 /* FlexSPI NAND 64 pages */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL_QSPI_128     1 /* FlexSPI NAND 128 pages */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL_QSPI_256     2 /* FlexSPI NAND 256 pages */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_NAND_PG_PER_BLK_VAL_QSPI_32      3 /* FlexSPI NAND 32 pages */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_USDHC_BT_SEL_VAL_USDHC1_SD       0 /* uSDHC1 SD */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_USDHC_BT_SEL_VAL_USDHC1_EMMC     1 /* uSDHC1 eMMC */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_USDHC_BT_SEL_VAL_USDHC2_EMMC     2 /* uSDHC2 eMMC */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG0_OVERRIDE_USDHC_BT_SEL_VAL_USDHC3_SD       3 /* uSDHC3 SD */


/*********** BOOT_CFG1 ********************************************************/
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_FLEXSPI_FEQ_SEL                      (0x00000007UL <<  0U) /* FLEXSPI Flash Frequency */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_FLEXSPI_DUMMY_CYCLE_SEL              (0x0000000FUL <<  3U) /* FLEXSPI Flash Dummy Cycle (0 – auto-probed, other - Actual dummy cycles for Read command) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_DCACHE_DIS                           (0x00000001UL <<  8U) /* Disable L1 and L2 D-Cache */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_BT_FREQ_SEL                          (0x00000001UL <<  9U) /* ARM/DDR Boot frequency selection */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_WDOG_EN                              (0x00000001UL << 10U) /* Watchdog reset counter enable */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_ICACHE_DIS                           (0x00000001UL << 12U) /* Disable L1 I-Cache */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_BT_LPB_POLARITY                      (0x00000001UL << 13U) /* USB Low-Power Boot GPIO polarity */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_BT_LPB                               (0x00000003UL << 14U) /* USB Low-Power Boot (Core/DDR/Bus div) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_WDOG_TIMEOUT_SEL                     (0x00000003UL << 16U) /* Watchdog timeout select */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_FLEXSPI_HOLD_TIME_SEL                (0x00000003UL << 18U) /* Hold time before read from device */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_FORCE_BT_FROM_FUSE                   (0x00000001UL << 20U) /* Boot from programmed fuses, not Boot Mode Pins (disable boot pins check) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_SDP_DISABLE                          (0x00000001UL << 21U) /* Disable USB serial download */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_NOC_ID_REMAP_BYPASS                  (0x00000001UL << 23U)
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_DCACHE_BYPASS_DIS                    (0x00000001UL << 24U)
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_RECOVER_ECSPI_BOOT_EN                (0x00000001UL << 25U) /* OEM Recovery boot enable */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_ECSPI_CS_SEL                         (0x00000003UL << 26U) /* CS selection (SPI only) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_ECSPI_ADDR_SEL                       (0x00000001UL << 28U) /* SPI addressing */
#define CHIP_OTP_WORD_FLD_BOOT_CFG1_ECSPI_PORT_SEL                       (0x00000007UL << 29U) /* ECSPI port selection */
#define CHIP_OTP_WORD_MSK_BOOT_CFG1_RESERVED                             (0x00400480UL)


#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_FEQ_SEL_100MHZ            0 /* 100 MHz */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_FEQ_SEL_133MHZ            1 /* 133 MHz */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_FEQ_SEL_166MHZ            2 /* 166 MHz */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_FEQ_SEL_200MHZ            3 /* 200 MHz */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_FEQ_SEL_80MHZ             4 /*  80 MHz */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_FEQ_SEL_20MHZ             5 /*  20 MHz */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_BT_FREQ_SEL_800MHZ                0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_BT_FREQ_SEL_400MHZ                1

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_BT_LPB_DIS                        0 /* LPB Disable */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_BT_LPB_DIV2                       2 /* Div by 2 */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_BT_LPB_DIV4                       3 /* Div by 4 */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_WDOG_TIMEOUT_SEL_2_0S             0 /* 2.0 s */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_WDOG_TIMEOUT_SEL_1_5S             1 /* 1.5 s */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_WDOG_TIMEOUT_SEL_1_0S             2 /* 1.0 s */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_WDOG_TIMEOUT_SEL_0_5S             3 /* 0.5 s */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_HOLD_TIME_SEL_0_5MS       0 /* 500 us */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_HOLD_TIME_SEL_1MS         1 /* 1 ms */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_HOLD_TIME_SEL_3MS         2 /* 3 ms */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_FLEXSPI_HOLD_TIME_SEL_10MS        3 /* 10 ms */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_CS_SEL_CS0                  0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_CS_SEL_CS1                  1
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_CS_SEL_CS2                  2
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_CS_SEL_CS3                  3

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_ADDR_SEL_3B                 0 /* 3-bytes (24-bit) */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_ADDR_SEL_2B                 1 /* 2-bytes (16-bit) */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_PORT_SEL_ECSPI1             0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_PORT_SEL_ECSPI2             1
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG1_ECSPI_PORT_SEL_ECSPI3             2


/*********** BOOT_CFG2 ********************************************************/
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_USDHC_MFG_VOL_SEL                    (0x00000001UL <<  0U) /* For Mfg Mode IO Voltage */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_USDHC_VOL_SEL                        (0x00000001UL <<  1U) /* For Normal Boot Mode IO Voltage */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_SDMMC_SPEED                          (0x00000003UL <<  2U) /* SD/eMMC Speed mode */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_SDMMC_BUS_WIDTH                      (0x00000003UL <<  4U) /* SDMMC Bus width */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_EMMC_FAST_BT                         (0x00000001UL <<  6U) /* Fast boot support (0 - Regular, 1 - Fast boot) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_USDHC_PWR_EN                         (0x00000001UL <<  7U) /* SD power cycle enable/eMMC reset enable (0 - No power cycle, 1 - enabled) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_SDMMC_DLL_DLY                        (0x0000007FUL <<  8U) /* Delay target for USDHC DLL, it is applied to slave mode target delay or override mode target delay depends on DLL Override fuse bit value. */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_USDHC_DLL_SEL                        (0x00000001UL << 15U) /* USDHC DLL override enable */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_USDHC_DLL_EN                         (0x00000001UL << 16U) /* USDHC DLL enable */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_USDHC_PAD_SION_EN                    (0x00000001UL << 18U) /* USDHC IOMUX SION bit enable */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_IMG_CNTN_SET1_OFFSET                 (0x0000000FUL << 19U) /* Secondary Image Boot Offset (0 - 4MB, 2 - 1 MB, 1,3..10 - 1MB*2^n, >10 - disable) */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_RECOVERY_SDMMC_BOOT_DIS              (0x00000001UL << 23U) /* Disable SDMMC manufacture mode */
#define CHIP_OTP_WORD_FLD_BOOT_CFG2_USDHC_OVRD_PAD_SETTING_LOW8          (0x000000FFUL << 24U) /* USDHC pad setting override */
#define CHIP_OTP_WORD_MSK_BOOT_CFG2_RESERVED                             (0x00020000UL)


#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_USDHC_MFG_VOL_SEL_3_3V            0 /* 3.3 V */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_USDHC_MFG_VOL_SEL_1_8V            1 /* 1.8 V */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_USDHC_VOL_SEL_3_3V                0 /* 3.3 V */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_USDHC_VOL_SEL_1_8V                1 /* 1.8 V */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_SPEED_NORM_SDR12            0 /* EMMC Normal/SDR12 */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_SPEED_HIGH_SDR25            1 /* EMMC High/SDR25 */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_SPEED_SDR50                 2 /* SD SDR50 */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_SPEED_SDR104                3 /* SD SDR104 */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_BUS_WIDTH_SDR8              0 /* 8-bit */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_BUS_WIDTH_SDR4              1 /* 4-bit */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_BUS_WIDTH_DDR8              2 /* 8-bit DDR (MMC 4.4) */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_SDMMC_BUS_WIDTH_DDR4              3 /* 4-bit DDR (MMC 4.4) */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_USDHC_DLL_SEL_SLAVE               0 /* DLL Slave Mode */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG2_USDHC_DLL_SEL_OVERRIDE            1 /* DLL Override Mode */

#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_4MB          0
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_2MB          1
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_1MB          2
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_8MB          3
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_16MB         4
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_32MB         5
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_64MB         6
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_128MB        7
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_256MB        8
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_512MB        9
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_1024MB       10
#define CHIP_OTP_WORD_FLD_BOOTVAL_CFG2_IMG_CNTN_SET1_OFFSET_DIS          15


/*********** BOOT_CFG3 ********************************************************/
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_EMMC_FAST_BT_ACK                     (0x00000001UL <<  0U) /* Fast boot acknowledge enable */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_USDHC_OVRD_PAD_SETTING_UP1           (0x00000001UL <<  1U)
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_USDHC_PWR_POLARITY                   (0x00000001UL <<  2U) /* USDHC power-off polarity selection */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_USDHC_PWR_DELAY                      (0x00000001UL <<  3U) /* USDHC power cycle delay selection */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_USDHC_PWR_INTERVAL                   (0x00000003UL <<  4U) /* USDHC power cycle interval */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_SD_CALI_STEP                         (0x00000003UL <<  6U)
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_NAND_RST_TIME                        (0x00000001UL <<  8U) /* NAND reset time */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY          (0x0000000FUL <<  9U) /* Toggle Mode 33MHz Preamble Delay, Read Latency */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_NAND_FCB_SERCH_COUNT                 (0x00000003UL << 13U) /* Boot search count */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_BT_TOGGLE_MODE                       (0x00000001UL << 15U) /* NAND Boot toggle mode */
#define CHIP_OTP_WORD_FLD_BOOT_CFG3_NAND_OVERRIDE_PAD_SETTING            (0x000000FFUL << 24U) /* NAND pad settings value */
#define CHIP_OTP_WORD_MSK_BOOT_CFG3_RESERVED                             (0x00FF0000UL)


#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_POLARITY_LOW            0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_POLARITY_HIGH           1

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_DELAY_5MS               0 /* 5ms */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_DELAY_2_5MS             1 /* 2.5ms */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_INTERVAL_20MS           0 /* 20 ms */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_INTERVAL_10MS           1 /* 10 ms */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_INTERVAL_5MS            2 /* 5 ms */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_USDHC_PWR_INTERVAL_2_5MS          3 /* 2.5 ms */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_SD_CALI_STEP_1                    0

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_16    0 /* 16 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_1     1 /* 1 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_2     2 /* 2 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_3     3 /* 3 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_4     4 /* 4 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_5     5 /* 5 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_6     6 /* 6 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_7     7 /* 7 GPMICLK cycles */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_TG_PREAMBLE_RD_LATENCY_15    15 /* 15 GPMICLK cycles */

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_FCB_SERCH_COUNT_2            0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_FCB_SERCH_COUNT_4            2
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG3_NAND_FCB_SERCH_COUNT_8            3


/*********** BOOT_CFG4 ********************************************************/
#define CHIP_OTP_WORD_FLD_BOOT_CFG4_NAND_CS_NUM                          (0x00000003UL <<  0U) /* NAND Number Of Devices */
#define CHIP_OTP_WORD_FLD_BOOT_CFG4_NAND_GPMI_DDR_DLL_VAL                (0x0000000FUL <<  3U) /* GPMI Read DDR DLL Target Value */
#define CHIP_OTP_WORD_FLD_BOOT_CFG4_NAND_ROW_ADDR_BYTES                  (0x00000003UL << 10U) /* Row address cycles */
#define CHIP_OTP_WORD_FLD_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID               (0x0000000FUL << 12U)

#define CHIP_OTP_WORD_MSK_BOOT_CFG4_RESERVED                             (0xFFFF0384UL)

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_CS_NUM_1                     0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_CS_NUM_2                     1
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_CS_NUM_4                     2

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_GPMI_DDR_DLL_VAL_7           0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_GPMI_DDR_DLL_VAL_1           1
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_GPMI_DDR_DLL_VAL_0           7
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_GPMI_DDR_DLL_VAL_15          15

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_ROW_ADDR_BYTES_3             0
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_ROW_ADDR_BYTES_2             1
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_ROW_ADDR_BYTES_4             2
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_ROW_ADDR_BYTES_5             3

#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_DIS        0 /* Do not use read retry (RR) sequence embedded in ROM */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_MICRON_20  1 /* Micron 20nm RR sequence */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_KIOXIA_A19 2 /* Kioxia A19nm RR sequence */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_KIOXIA_19  3 /* Kioxia 19nm RR sequence */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_SANDISK_19 4 /* SanDisk 19nm RR sequence */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_SANDISK_1Y 5 /* SanDisk 1ynmRR sequence */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_HYNIX_20   6 /* SK Hynix 20nm A Die RR sequence */
#define CHIP_OTP_WORD_FLDVAL_BOOT_CFG4_NAND_READ_RETRY_SEQ_ID_HYNIX_26   7 /* SK Hynix 26nm RR sequence */

#endif // CHIP_OTP_MAP_H
