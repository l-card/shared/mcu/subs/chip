#ifndef CHIP_FLASH_H
#define CHIP_FLASH_H

#include <stdint.h>
#include "chip_config.h"

typedef uint16_t t_chip_flash_pagenum;

typedef enum {
    CHIP_FLASH_ERR_OK = 0,
    CHIP_FLASH_ERR_INVALID_ADDR             = -2000,
    CHIP_FLASH_ERR_DATA_ALIGN               = -2001,
    CHIP_FLASH_ERR_DATA_SIZE                = -2002,

    CHIP_FLASH_ERR_WAIT_OPRDY_TOUT          = -2010, /* ошибка ожидания признака завершения операции */
    CHIP_FLASH_ERR_NOT_RDY                  = -2011, /* память занята (идет незаконченная операция) */
    CHIP_FLASH_ERR_LOCKED                   = -2012, /* попытка выполнить запись/стирание без chip_flash_unlock() */
    CHIP_FLASH_ERR_UNPROTECTED              = -2013, /* область на защищена (возвращается в функциях проверки наличия защиты) */
    CHIP_FLASH_ERR_INVALID_AREA_NUM         = -2014, /* неверно задан номер области */


    CHIP_FLASH_ERR_STAT_PROG_NOT_EMPTY      = -2020, /* запись в непустую ячейку (статус-регистр) */
    CHIP_FLASH_ERR_STAT_WRITE_PROTECT       = -2021, /* попытка записи/стирания защищенной области */
    CHIP_FLASH_ERR_STAT_PROG_NOT_ALIGN      = -2022, /* ошибка выраванивания записи (статус-регистр) */
    CHIP_FLASH_ERR_STAT_PROG_SIZE           = -2023, /* ошибка размера записи (статус-регистр) */
    CHIP_FLASH_ERR_STAT_PROG_SEQ            = -2024, /* неверная последовательность команд (статус-регистр)*/
    CHIP_FLASH_ERR_STAT_FASTPROG_DATAMISS   = -2025, /* не весь блок данных был послан в режиме быстрого программирования (статус-регистр) */
    CHIP_FLASH_ERR_STAT_FASTPROG            = -2026, /* общая ошибка быстрого программирования, не попадающая под остальные */
    CHIP_FLASH_ERR_STAT_READ_PROT           = -2027, /* попытка чтения защищенной области */
    CHIP_FLASH_ERR_STAT_OPT_VALIDITY        = -2028, /* ошибка проверки действительности option & engineering битов */
} t_chip_flash_err;

/* readout protection level - уровень защиты всей flash на чтение */
typedef  enum {
    CHIP_FLASH_RDP_LEVEL0, /* нет защиты */
    CHIP_FLASH_RDP_LEVEL1, /* защита на чтение, от отладки и загрузки из RAM. понижение со стиранием */
    CHIP_FLASH_RDP_LEVEL2, /* полная защита. нет возможности отменить */
} t_chip_flash_rdp_level;

#define CHIP_FLASH_PAGE_SIZE                (2*1024)
#define CHIP_FLASH_WRROW_SIZE               256
#define CHIP_FLASH_WRWORD_SIZE              8
#define CHIP_FLASH_PROT_AREA_COUNT          2

#define CHIP_FLASH_PAGE_ERASE_TIME_US   40000
#define CHIP_FLASH_WRROW_TIME_US         2800
#define CHIP_FLASH_WRWORD_TIME_US         125
#define CHIP_FLASH_WROPT_TIME_US        (CHIP_FLASH_PAGE_ERASE_TIME_US + 16*CHIP_FLASH_WRWORD_TIME_US)



/* Инициализация временных задержек flash-памяти в соответствии с настройками из
 * частот chip_config.h. Вызывается при инициализации до или после смены частоты */
void chip_flash_init_freq(void);

#if CHIP_CFG_FLASH_FUNC_EN
/* разблокировка памяти для записи/стирания
 * Перед набором вызов chip_flash_erase/chip_flash_write необходимо
 * вызвать данную функцию, а после - chip_flash_lock() */
void chip_flash_unlock(void);
void chip_flash_lock(void);


/* получение адреса в едином пространстве контроллера, соответствующего
 * заданному смещению от начала заданного банка FLASH-памяти.
 * Может использоваться для чтения данных из FLASH-памяти */
const void *chip_flash_rd_ptr(uint32_t addr);


/* Стирание заданной области falsh-памяти.
 * Стирание происходит по страницам 2К. Если адрес или размер не выравнены на границу
 * страницы, то будет стерто все содержимое страниц, в которые попадает указанная область.
 * Flash-память должна быть разблокирована с помощью chip_flash_unlock() и
 * стираемые сектора не должны быть защищены от записи */
t_chip_flash_err chip_flash_erase(uint32_t start_addr, uint32_t size, void (*cb_func)(void));



/* Запись блока данных во flash-память.
 * Адрес start_addr должан быть выравнен на 8 байта (слово записи во Flash).
 * Flash-память должна быть разблокирована с помощью chip_flash_unlock(), стерта и
 * перезаписываемые сектора не должны быть защищены от записи */
t_chip_flash_err chip_flash_write(uint32_t start_addr, const void *data, uint32_t size, void (*cb_func)(void));



/* Получение кода ошибки, соответствующего текущим статусным битам flash-памяти
 * с их сбросом. Функции записи автоматически проверяют результат их выполнения.
 * Данная функция может использоваться при чтении блока памяти, чтобу убедиться
 * в отсутствии ошибок */
t_chip_flash_err chip_flash_check_err(void);


/* ------- низкоуровневые функции ---------------------------------------------*/

/* возвращение номеров секторов, которые занимает указанная область */
t_chip_flash_err chip_flash_get_page_nums(uint32_t start_addr, uint32_t size,
                                          t_chip_flash_pagenum *pstart_page,
                                          t_chip_flash_pagenum *pend_page);
/* проверка, что операции с flash завершены. если нет - возвращает CHIP_FLASH_ERR_NOT_RDY */
t_chip_flash_err chip_flash_check_rdy();
/* запуск стирания выбранного сектора. завершение необходимо проверить с помощью
 * chip_flash_check_rdy() и затем проверить статус с помощью chip_flash_check_err().
 * по завершению стирания всех секторов, вызвать chip_flash_erase_finish() */
t_chip_flash_err chip_flash_erase_page_start(t_chip_flash_pagenum sector);
/* функция должна вызываться после вызовов chip_flash_erase_sector_start() для
 * запрета разрешения стирания */
void chip_flash_erase_finish();



/* --------- работа с опциями ------------------------------------------------*/
/* Функции для работы с опциями чипа во flash-памяти. Для смены опций необходимо:
 * 1. Разблокировать доступ с помощью chip_flash_options_unlock()
 * 2. Изменить значения опций в нужных *_PRG регистрах
 * 3. Записать изменения с помощью chip_flash_options_write();
 * 4. Заблокировать доступ с помощью chip_flash_options_lock() */
void chip_flash_opts_unlock(void);
t_chip_flash_err chip_flash_opts_write(void (*cb_func)(void));
void chip_flash_opts_lock(void);



/* ------------------ защита памяти -----------------------------------------*/
/* функции устанавливают соответствующие значения байт опций. Дополнительно после этого
 * требуется выполнить chip_flash_options_write() при разлоченном доступе через chip_flash_options_unlock() */
t_chip_flash_err chip_flash_opts_wrp_area_set_pages(uint8_t area_num, t_chip_flash_pagenum start_page, t_chip_flash_pagenum end_page);
t_chip_flash_err chip_flash_opts_wrp_area_set(uint8_t area_num, uint32_t start_addr, uint32_t size);
t_chip_flash_err chip_flash_opts_wrp_area_clear(uint8_t area_num);
t_chip_flash_err chip_flash_opts_wrp_area_is_protected(uint8_t area_num);
t_chip_flash_err chip_flash_opts_wrp_area_get_pages(uint8_t area_num, t_chip_flash_pagenum *start_page, t_chip_flash_pagenum *end_page);
t_chip_flash_err chip_flash_opts_check_wr_protected(uint32_t start_addr, uint32_t size);


/* получить текущий уровень защиты всей памяти от постороннего чтения */
t_chip_flash_rdp_level chip_flash_opts_get_rdp_level(void);
/* установить уровень защиты всей памяти от постороннего чтения */
t_chip_flash_err chip_flash_opts_set_rdp_level(t_chip_flash_rdp_level lvl);


#endif

#endif // CHIP_FLASH_H
