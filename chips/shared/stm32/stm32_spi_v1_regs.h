#ifndef STM32_SPI_V1_REGS_H
#define STM32_SPI_V1_REGS_H

#include "chip_ioreg_defs.h"

/* CHIP_SUPPORT_SPI_NSSP                - управление NSS
   CHIP_SUPPORT_SPI_FRAME_FMT           - поддержка настройки формата кадра
   CHIP_SUPPORT_SPI_CUSTOM_DATA_SIZE    - поддержка настройки произвольного размера кадра данных (иначе только 8 или 16 бит)
   CHIP_SUPPORT_SPI_FIFO                - поддержка FIFO
   CHIP_SUPPORT_SPI_LAST_DMA            - поддержка флагов последнего DMA
   CHIP_SUPPORT_SPI_FRAME_ERR           - поддержка флага ошибки кадра   
   CHIP_SUPPORT_SPI_I2S                 - поддержка I2S
   CHIP_SUPPORT_SPI_I2S_ASYNC_START     - поддержка I2S async start
   CHIP_SUPPORT_SPI_HS_CTL              - поддержка High Speed control (WCH)
*/

typedef struct {
  __IO uint32_t CR1;      /* SPI Control register 1 (not used in I2S mode),       Address offset: 0x00 */
  __IO uint32_t CR2;      /* SPI Control register 2,                              Address offset: 0x04 */
  __IO uint32_t SR;       /* SPI Status register,                                 Address offset: 0x08 */
  union {                 /* SPI data register,                                   Address offset: 0x0C  */
    struct {
        __IO uint16_t DR16;
        uint16_t tpad16;
    };
    struct {
        __IO uint8_t DR8;
        uint8_t tpad8[3];
    };
  };
  __IO uint32_t CRCPR;    /* SPI CRC polynomial register (not used in I2S mode),  Address offset: 0x10 */
  __IO uint32_t RXCRCR;   /* SPI Rx CRC register (not used in I2S mode),          Address offset: 0x14 */
  __IO uint32_t TXCRCR;   /* SPI Tx CRC register (not used in I2S mode),          Address offset: 0x18 */
#if CHIP_SUPPORT_SPI_I2S
  __IO uint32_t I2SCFGR;  /* SPI_I2S configuration register,                      Address offset: 0x1C */
  __IO uint32_t I2SPR;    /* SPI_I2S prescaler register,                          Address offset: 0x20 */
#else
  uint32_t RESERVED[2];
#endif
#if CHIP_SUPPORT_SPI_HS_CTL
  __IO uint32_t HSCR;     /* SPI high-speed control register,                     Address offset: 0x24 */
#endif
} CHIP_REGS_SPI_T;


/*******************  Bit definition for SPI_CR1 register  ********************/
#define CHIP_REGFLD_SPI_CR1_CPHA                (0x00000001UL <<  0U) /* Clock Phase */
#define CHIP_REGFLD_SPI_CR1_CPOL                (0x00000001UL <<  1U) /* Clock Polarity   */
#define CHIP_REGFLD_SPI_CR1_MSTR                (0x00000001UL <<  2U) /* Master Selection */
#define CHIP_REGFLD_SPI_CR1_BR                  (0x00000007UL <<  3U) /* Baud Rate Control */
#define CHIP_REGFLD_SPI_CR1_SPE                 (0x00000001UL <<  6U) /* SPI Enable */
#define CHIP_REGFLD_SPI_CR1_LSB_FIRST           (0x00000001UL <<  7U) /* Frame Format */
#define CHIP_REGFLD_SPI_CR1_SSI                 (0x00000001UL <<  8U) /* Internal slave select */
#define CHIP_REGFLD_SPI_CR1_SSM                 (0x00000001UL <<  9U) /* Software slave management */
#define CHIP_REGFLD_SPI_CR1_RX_ONLY             (0x00000001UL << 10U) /* Receive only */
#if CHIP_SUPPORT_SPI_CUSTOM_DATA_SIZE
#define CHIP_REGFLD_SPI_CR1_CRCL                (0x00000001UL << 11U) /* CRC Length */
#else
#define CHIP_REGFLD_SPI_CR1_DFF                 (0x00000001UL << 11U) /* Data Frame format */
#endif
#define CHIP_REGFLD_SPI_CR1_CRC_NEXT            (0x00000001UL << 12U) /* Transmit CRC next */
#define CHIP_REGFLD_SPI_CR1_CRC_EN              (0x00000001UL << 13U) /* Hardware CRC calculation enable */
#define CHIP_REGFLD_SPI_CR1_BIDI_OE             (0x00000001UL << 14U) /* Output enable in bidirectional mode */
#define CHIP_REGFLD_SPI_CR1_BIDI_MODE           (0x00000001UL << 15U) /* Bidirectional data mode enable */

#define CHIP_REGFLDVAL_SPI_CR1_DFF_8BIT         0
#define CHIP_REGFLDVAL_SPI_CR1_DFF_16BIT        1

#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV2          0
#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV4          1
#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV8          2
#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV16         3
#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV32         4
#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV64         5
#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV128        6
#define CHIP_REGFLDVAL_SPI_CR1_BR_DIV256        7

/*******************  Bit definition for SPI_CR2 register  ********************/
#define CHIP_REGFLD_SPI_CR2_RXDMA_EN            (0x00000001UL <<  0U) /* Rx Buffer DMA Enable */
#define CHIP_REGFLD_SPI_CR2_TXDMA_EN            (0x00000001UL <<  1U) /* Tx Buffer DMA Enable */
#define CHIP_REGFLD_SPI_CR2_SS_OE               (0x00000001UL <<  2U) /* SS Output Enable */
#if CHIP_SUPPORT_SPI_NSSP
#define CHIP_REGFLD_SPI_CR2_NSSP                (0x00000001UL <<  3U) /* NSS pulse management Enable */
#endif
#if CHIP_SUPPORT_SPI_FRAME_FMT
#define CHIP_REGFLD_SPI_CR2_FRF                 (0x00000001UL <<  4U) /* Frame Format Enable */
#endif
#define CHIP_REGFLD_SPI_CR2_ERR_IE              (0x00000001UL <<  5U) /* Error Interrupt Enable */
#define CHIP_REGFLD_SPI_CR2_RXNE_IE             (0x00000001UL <<  6U) /* RX buffer Not Empty Interrupt Enable */
#define CHIP_REGFLD_SPI_CR2_TXE_IE              (0x00000001UL <<  7U) /* Tx buffer Empty Interrupt Enable */
#if CHIP_SUPPORT_SPI_CUSTOM_DATA_SIZE
#define CHIP_REGFLD_SPI_CR2_DS                  (0x0000000FUL <<  8U) /* Data Size */
#endif
#if CHIP_SUPPORT_SPI_FIFO
#define CHIP_REGFLD_SPI_CR2_FRXTH               (0x00000001UL << 12U) /* FIFO reception Threshold */
#endif
#if CHIP_SUPPORT_SPI_LAST_DMA
#define CHIP_REGFLD_SPI_CR2_LDMARX              (0x00000001UL << 13U) /* Last DMA transfer for reception */
#define CHIP_REGFLD_SPI_CR2_LDMATX              (0x00000001UL << 14U) /* Last DMA transfer for transmission */
#endif

#define CHIP_REGFLDVAL_SPI_CR2_FRXTH_16BIT      0
#define CHIP_REGFLDVAL_SPI_CR2_FRXTH_8BIT       1


/********************  Bit definition for SPI_SR register  ********************/
#define CHIP_REGFLD_SPI_SR_RXNE                 (0x00000001UL <<  0U) /* Receive buffer Not Empty */
#define CHIP_REGFLD_SPI_SR_TXE                  (0x00000001UL <<  1U) /* Transmit buffer Empty */
#define CHIP_REGFLD_SPI_SR_CH_SIDE              (0x00000001UL <<  2U) /* Channel side */
#define CHIP_REGFLD_SPI_SR_UDR                  (0x00000001UL <<  3U) /* Underrun flag */
#define CHIP_REGFLD_SPI_SR_CRC_ERR              (0x00000001UL <<  4U) /* CRC Error flag */
#define CHIP_REGFLD_SPI_SR_MODF                 (0x00000001UL <<  5U) /* Mode fault */
#define CHIP_REGFLD_SPI_SR_OVR                  (0x00000001UL <<  6U) /* Overrun flag */
#define CHIP_REGFLD_SPI_SR_BSY                  (0x00000001UL <<  7U) /* Busy flag */
#if CHIP_SUPPORT_SPI_FRAME_ERR
#define CHIP_REGFLD_SPI_SR_FRE                  (0x00000001UL <<  8U) /* TI frame format error */
#endif
#if CHIP_SUPPORT_SPI_FIFO
#define CHIP_REGFLD_SPI_SR_FRLVL                (0x00000003UL <<  9U) /* FIFO Reception Level */
#define CHIP_REGFLD_SPI_SR_FTLVL                (0x00000003UL << 11U) /* FIFO Transmission Level */
#endif

#define CHIP_REGFLDVAL_SPI_SR_FLVL_EMPTY        0
#define CHIP_REGFLDVAL_SPI_SR_FLVL_1_4          1
#define CHIP_REGFLDVAL_SPI_SR_FLVL_1_2          2
#define CHIP_REGFLDVAL_SPI_SR_FLVL_FULL         3

/********************  Bit definition for SPI_DR register  ********************/
#define CHIP_REGFLD_SPI_DR_DR                   (0x0000FFFFUL <<  0U) /* Data Register */

/*******************  Bit definition for SPI_CRCPR register  ******************/
#define CHIP_REGFLD_SPI_CRCPR_CRCPOLY           (0x0000FFFFUL <<  0U) /* CRC polynomial register */

/******************  Bit definition for SPI_RXCRCR register  ******************/
#define CHIP_REGFLD_SPI_RXCRCR_RXCRC            (0x0000FFFFUL <<  0U) /* Rx CRC Register */

/******************  Bit definition for SPI_TXCRCR register  ******************/
#define CHIP_REGFLD_SPI_TXCRCR_TXCRC            (0x0000FFFFUL <<  0U) /* Tx CRC Register */

/******************  Bit definition for SPI_I2SCFGR register  *****************/
#define CHIP_REGFLD_SPI_I2SCFGR_CH_LEN          (0x00000001UL <<  0U) /* Channel length (number of bits per audio channel) */
#define CHIP_REGFLD_SPI_I2SCFGR_DAT_LEN         (0x00000003UL <<  1U) /* Data length to be transferred */
#define CHIP_REGFLD_SPI_I2SCFGR_CK_POL          (0x00000001UL <<  3U) /* steady state clock polarity */
#define CHIP_REGFLD_SPI_I2SCFGR_I2S_STD         (0x00000003UL <<  4U) /* I2S standard selection */
#define CHIP_REGFLD_SPI_I2SCFGR_PCM_SYNC        (0x00000001UL <<  7U) /* PCM frame synchronization */
#define CHIP_REGFLD_SPI_I2SCFGR_I2S_CFG         (0x00000003UL <<  8U) /* I2S configuration mode */
#define CHIP_REGFLD_SPI_I2SCFGR_I2SE            (0x00000001UL << 10U) /* I2S Enable */
#define CHIP_REGFLD_SPI_I2SCFGR_I2S_MOD         (0x00000001UL << 11U) /* I2S mode selection */
#if CHIP_SUPPORT_I2S_ASYNC_START
#define CHIP_REGFLD_SPI_I2SCFGR_ASTRT_EN        (0x00000001UL << 12U) /* Asynchronous start enable */
#endif

#define CHIP_REGFLDVAL_SPI_I2SCFGR_CH_LEN_16BIT 0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_CH_LEN_32BIT 1

#define CHIP_REGFLDVAL_SPI_I2SCFGR_DAT_LEN_16BIT 0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_DAT_LEN_24BIT 1
#define CHIP_REGFLDVAL_SPI_I2SCFGR_DAT_LEN_32BIT 2

#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_STD_PHILIPS  0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_STD_LJ       1
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_STD_RJ       2
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_STD_PCM      3

#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_CFG_SLAVE_TX  0
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_CFG_SLAVE_RX  1
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_CFG_MASTER_TX 2
#define CHIP_REGFLDVAL_SPI_I2SCFGR_I2S_CFG_MASTER_RX 3


/******************  Bit definition for SPI_I2SPR register  *******************/
#define CHIP_REGFLD_SPI_I2SPR_I2SDIV            (0x000000FFUL <<  0U) /* I2S Linear prescaler */
#define CHIP_REGFLD_SPI_I2SPR_ODD               (0x00000001UL <<  8U) /* Odd factor for the prescaler */
#define CHIP_REGFLD_SPI_I2SPR_MCKOE             (0x00000001UL <<  9U) /* Master Clock Output Enable */

/******************  Bit definition for SPI_HSCR register  *******************/
#define CHIP_REGFLD_SPI_HSCR_HS_RX_EN           (0x00000001UL <<  0U) /* Read enable in SPI high-speed mode. */

#endif // STM32_SPI_V1_REGS_H
