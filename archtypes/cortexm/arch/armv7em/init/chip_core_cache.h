#ifndef CHIP_CORE_CACHE_H
#define CHIP_CORE_CACHE_H

#include "chip_config.h"
#include "chip_core_features.h"

#if defined CHIP_CFG_CACHE_SETUP_EN && (CHIP_CORE_SUPPORT_DCACHE || CHIP_CORE_SUPPORT_ICACHE)
    #define CHIP_CACHE_SETUP_EN     CHIP_CFG_CACHE_SETUP_EN
#else
    #define CHIP_CACHE_SETUP_EN     0
#endif

#if CHIP_CACHE_SETUP_EN
    #ifdef CHIP_CFG_ICACHE_EN
        #define CHIP_ICACHE_EN CHIP_CFG_ICACHE_EN
    #else
        #define CHIP_ICACHE_EN 0
    #endif

    #ifdef CHIP_CFG_DCACHE_EN
        #define CHIP_DCACHE_EN CHIP_CFG_DCACHE_EN
    #else
        #define CHIP_DCACHE_EN 0
    #endif
#else
    #define CHIP_ICACHE_EN 0
    #define CHIP_DCACHE_EN 0
#endif



#define CHIP_DCACHE_LINE_SIZE   32U /* Cortex-M7 cache line size is fixed to 32 bytes (8 words). */
#define CHIP_ICACHE_LINE_SIZE   32U /* Cortex-M7 cache line size is fixed to 32 bytes (8 words). */

#define CHIP_DCACHE_LINE_MSK    (CHIP_DCACHE_LINE_SIZE-1)
#define CHIP_ICACHE_LINE_MSK    (CHIP_ICACHE_LINE_SIZE-1)



static inline void chip_icache_features_sel(void) {
    CHIP_REGS_ARM_FEATURES->CSSELR = LBITFIELD_SET(CHIP_REGFLD_ARM_FEATURES_CSSELR_LEVEL, 0) |
                                  LBITFIELD_SET(CHIP_REGFLD_ARM_FEATURES_CSSELR_IND, CHIP_REGFLDVAL_ARM_FEATURES_CSSELR_ICACHE);
}

static inline void chip_dcache_features_sel(void) {
    CHIP_REGS_ARM_FEATURES->CSSELR = LBITFIELD_SET(CHIP_REGFLD_ARM_FEATURES_CSSELR_LEVEL, 0) |
                                  LBITFIELD_SET(CHIP_REGFLD_ARM_FEATURES_CSSELR_IND, CHIP_REGFLDVAL_ARM_FEATURES_CSSELR_DCACHE);
}



static inline void chip_icache_full_invalidate(void) {
#if CHIP_CORE_SUPPORT_ICACHE
    chip_dsb();
    chip_isb();
    CHIP_REGS_CACHE->ICIALLU = 0UL;
    chip_dsb();
    chip_isb();
#endif
}


static inline void chip_icache_invalidate(uint32_t addr, uint32_t size) {
#if CHIP_CORE_SUPPORT_ICACHE
    int32_t op_size = size + (addr & CHIP_ICACHE_LINE_MSK);
    uint32_t op_addr = addr;

    chip_dsb();

   while (op_size > 0) {
        CHIP_REGS_CACHE->ICIMVAU = op_addr;             /* register accepts only 32byte aligned values, only bits 31..5 are valid */
        op_addr += CHIP_ICACHE_LINE_SIZE;
        op_size -= CHIP_ICACHE_LINE_SIZE;
   }

   chip_dsb();
   chip_isb();
#endif
}

static inline void chip_dcache_setway_all_op(__OM  uint32_t *reg) {
    uint32_t ccsidr;
    unsigned sets;
    unsigned ways;

    chip_dcache_features_sel();

    ccsidr = CHIP_REGS_ARM_FEATURES->CCSIDR;

    chip_dsb();

    sets = LBITFIELD_GET(ccsidr, CHIP_REGFLD_ARM_FEATURES_CCSIDR_NUMSETS) + 1;
    ways = LBITFIELD_GET(ccsidr, CHIP_REGFLD_ARM_FEATURES_CCSIDR_ASSOCIATIVITY) + 1;

    for (unsigned set_idx = 0; set_idx < sets; ++set_idx) {
        for (unsigned way_idx = 0; way_idx < ways; ++way_idx) {
            *reg = LBITFIELD_SET(CHIP_REGFLD_CACHE_SW_SET, set_idx) |
                   LBITFIELD_SET(CHIP_REGFLD_CACHE_SW_WAY, way_idx);
        }
    }

    chip_dsb();
}

static inline void chip_dcache_addr_op(__OM  uint32_t *reg, uint32_t addr, uint32_t size) {
    uint32_t op_addr = (addr & ~(CHIP_DCACHE_LINE_MSK));
    int32_t op_size = size + (addr & CHIP_DCACHE_LINE_MSK);


    chip_dsb();

   while (op_size > 0) {
        *reg = op_addr;             /* register accepts only 32byte aligned values, only bits 31..5 are valid */
        op_addr += CHIP_DCACHE_LINE_SIZE;
        op_size -= CHIP_DCACHE_LINE_SIZE;
   }

   chip_dsb();
}

/** Операция flush корректно работает только при включенном кэше, иначе может
 *  быть exception, поэтому явно проверяем флаг.
 *  Операция invalidate может работать с запрещенным кэшем,
 *  full_invalidate используется перед разрешением, поэтому
 *  может выполняться всегда, если ядро поддерживает кэш */

static inline void chip_dcache_full_invalidate(void) {
#if CHIP_CORE_SUPPORT_DCACHE
    chip_dcache_setway_all_op(&CHIP_REGS_CACHE->DCISW);
#endif
}

static inline void chip_dcache_full_flush(void) {
#if CHIP_CORE_SUPPORT_DCACHE
    if (CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_DC) {
        chip_dcache_setway_all_op(&CHIP_REGS_CACHE->DCCSW);
    }
#endif
}

static inline void chip_dcache_full_flush_invalidate(void) {
#if CHIP_CORE_SUPPORT_DCACHE
    if (CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_DC) {
        chip_dcache_setway_all_op(&CHIP_REGS_CACHE->DCCISW);
    }
#endif
}

static inline void chip_dcache_invalidate(uint32_t addr, uint32_t size) {
#if CHIP_CORE_SUPPORT_DCACHE
    if (CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_DC) {
        chip_dcache_addr_op(&CHIP_REGS_CACHE->DCIMVAC, addr, size);
    }
#endif
}

static inline void chip_dcache_flush(uint32_t addr, uint32_t size) {
#if CHIP_CORE_SUPPORT_DCACHE
    if (CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_DC) {
        chip_dcache_addr_op(&CHIP_REGS_CACHE->DCCMVAC, addr, size);
    }
#endif
}

static inline void chip_dcache_flush_invalidate(uint32_t addr, uint32_t size) {
#if CHIP_CORE_SUPPORT_DCACHE
    if (CHIP_REGS_SCB->CCR & CHIP_REGFLD_SCB_CCR_DC) {
        chip_dcache_addr_op(&CHIP_REGS_CACHE->DCCIMVAC, addr, size);
    }
#endif
}


void chip_icache_enable(void);
void chip_icache_disable(void);
void chip_dcache_enable(void);
void chip_dcache_disable(void);

#if CHIP_CACHE_SETUP_EN
void chip_cache_init(void);
#endif

#endif // CHIP_CORE_CACHE_H
