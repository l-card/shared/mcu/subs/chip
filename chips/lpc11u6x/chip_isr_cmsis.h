#ifndef CHIP_ISR_CMSIS_N32G03X_H
#define CHIP_ISR_CMSIS_N32G03X_H

typedef enum {
    NonMaskableInt_IRQn           = -14,    /*!< 2 Non Maskable Interrupt                         */
    HardFault_IRQn                = -13,    /*!< 3 Cortex-M0 Hard Fault Interrupt                 */
    SVCall_IRQn                   = -5,     /*!< 11 Cortex-M0 SV Call Interrupt                   */
    PendSV_IRQn                   = -2,     /*!< 14 Cortex-M0 Pend SV Interrupt                   */
    SysTick_IRQn                  = -1,     /*!< 15 Cortex-M0 System Tick Interrupt               */

    PIN_INT0_IRQn                 = 0,      /*!< Pin Interrupt 0                                  */
    PIN_INT1_IRQn                 = 1,      /*!< Pin Interrupt 1                                  */
    PIN_INT2_IRQn                 = 2,      /*!< Pin Interrupt 2                                  */
    PIN_INT3_IRQn                 = 3,      /*!< Pin Interrupt 3                                  */
    PIN_INT4_IRQn                 = 4,      /*!< Pin Interrupt 4                                  */
    PIN_INT5_IRQn                 = 5,      /*!< Pin Interrupt 5                                  */
    PIN_INT6_IRQn                 = 6,      /*!< Pin Interrupt 6                                  */
    PIN_INT7_IRQn                 = 7,      /*!< Pin Interrupt 7                                  */
    GINT0_IRQn                    = 8,      /*!< GPIO interrupt status of port 0                  */
    GINT1_IRQn                    = 9,      /*!< GPIO interrupt status of port 1                  */
    I2C1_IRQn                     = 10,     /*!< I2C1 Interrupt                                   */
    USART1_4_IRQn                 = 11,     /*!< Combined USART1 and USART4 interrupts            */
    USART2_3_IRQn                 = 12,     /*!< Combined USART2 and USART3 interrupts            */
    SCT0_1_IRQn                   = 13,     /*!< Combined SCT0 and SCT1 interrupts                */
    SSP1_IRQn                     = 14,     /*!< SSP1 Interrupt                                   */
    I2C0_IRQn                     = 15,     /*!< I2C0 Interrupt                                   */
    TIMER_16_0_IRQn               = 16,     /*!< 16-bit Timer0 Interrupt                          */
    TIMER_16_1_IRQn               = 17,     /*!< 16-bit Timer1 Interrupt                          */
    TIMER_32_0_IRQn               = 18,     /*!< 32-bit Timer0 Interrupt                          */
    TIMER_32_1_IRQn               = 19,     /*!< 32-bit Timer1 Interrupt                          */
    SSP0_IRQn                     = 20,     /*!< SSP0 Interrupt                                   */
    USART0_IRQn                   = 21,     /*!< USART0 interrupt                                 */
    USB0_IRQn                     = 22,     /*!< USB IRQ interrupt                                */
    USB0_FIQ_IRQn                 = 23,     /*!< USB FIQ interrupt                                */
    ADC_A_IRQn                    = 24,     /*!< ADC_A Converter Interrupt                        */
    RTC_IRQn                      = 25,     /*!< RTC Interrupt                                    */
    BOD_WDT_IRQn                  = 26,     /*!< Shared Brown Out Detect(BOD) and WDT Interrupts  */
    FMC_IRQn                      = 27,     /*!< FLASH Interrupt                                  */
    DMA_IRQn                      = 28,     /*!< DMA Interrupt                                    */
    ADC_B_IRQn                    = 29,     /*!< ADC_B Interrupt                                  */
    USB_WAKEUP_IRQn               = 30,     /*!< USB wake-up interrupt Interrupt                  */
    RESERVED31_IRQn               = 31,
} IRQn_Type;


/* IRQ Handler Alias list */
#define UART1_4_IRQHandler        USART1_4_IRQHandler
#define UART2_3_IRQHandler        USART2_3_IRQHandler
#define UART0_IRQHandler          USART0_IRQHandler


#endif // CHIP_ISR_CMSIS_H
