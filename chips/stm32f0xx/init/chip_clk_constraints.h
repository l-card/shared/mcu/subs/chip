#ifndef CHIP_CLK_CONSTRAINTS_H
#define CHIP_CLK_CONSTRAINTS_H

#include "chip_std_defs.h"
#include "chip_dev_spec_features.h"

/* граничные условия для внешней частоты HSE */
#define CHIP_CLK_HSE_FREQ_MIN               CHIP_MHZ(4)  /* минимальная  внешняя частота HSE */
#define CHIP_CLK_HSE_FREQ_MAX               CHIP_MHZ(32) /* максимальная внешняя частота HSE */


#define CHIP_CLK_PLL_FREQ_MIN               CHIP_MHZ(16) /* минимальная  выходная частота PLL */
#define CHIP_CLK_PLL_FREQ_MAX               CHIP_MHZ(48) /* максимальная выходная частота PLL */
#define CHIP_CLK_AHB_FREQ_MAX               CHIP_MHZ(48)
#define CHIP_CLK_APB_FREQ_MAX               CHIP_MHZ(48)

#endif // CHIP_CLK_CONSTRAINTS_H
