#ifndef CHIP_STM32F0XX_REGS_RCC_H
#define CHIP_STM32F0XX_REGS_RCC_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
  __IO uint32_t CR;         /*!< RCC clock control register,                                  Address offset: 0x00 */
  __IO uint32_t CFGR;       /*!< RCC clock configuration register,                            Address offset: 0x04 */
  __IO uint32_t CIR;        /*!< RCC clock interrupt register,                                Address offset: 0x08 */
  __IO uint32_t APB2RSTR;   /*!< RCC APB2 peripheral reset register,                          Address offset: 0x0C */
  __IO uint32_t APB1RSTR;   /*!< RCC APB1 peripheral reset register,                          Address offset: 0x10 */
  __IO uint32_t AHBENR;     /*!< RCC AHB peripheral clock register,                           Address offset: 0x14 */
  __IO uint32_t APB2ENR;    /*!< RCC APB2 peripheral clock enable register,                   Address offset: 0x18 */
  __IO uint32_t APB1ENR;    /*!< RCC APB1 peripheral clock enable register,                   Address offset: 0x1C */
  __IO uint32_t BDCR;       /*!< RCC Backup domain control register,                          Address offset: 0x20 */
  __IO uint32_t CSR;        /*!< RCC clock control & status register,                         Address offset: 0x24 */
  __IO uint32_t AHBRSTR;    /*!< RCC AHB peripheral reset register,                           Address offset: 0x28 */
  __IO uint32_t CFGR2;      /*!< RCC clock configuration register 2,                          Address offset: 0x2C */
  __IO uint32_t CFGR3;      /*!< RCC clock configuration register 3,                          Address offset: 0x30 */
  __IO uint32_t CR2;        /*!< RCC clock control register 2,                                Address offset: 0x34 */
} CHIP_REGS_RCC_T;

#define CHIP_REGS_RCC          ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)

/*********** Clock control register (RCC_CR) **********************************/
#define CHIP_REGFLD_RCC_CR_HSI_ON                   (0x00000001UL <<  0U)   /* (RW) HSI clock enable */
#define CHIP_REGFLD_RCC_CR_HSI_RDY                  (0x00000001UL <<  1U)   /* (RO) HSI clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSI_TRIM                 (0x0000001FUL <<  3U)   /* (RW) HSI clock trimming */
#define CHIP_REGFLD_RCC_CR_HSI_CAL                  (0x000000FFUL <<  8U)   /* (RO) HSI clock calibration */
#define CHIP_REGFLD_RCC_CR_HSE_ON                   (0x00000001UL << 16U)   /* (RW) HSE clock enable */
#define CHIP_REGFLD_RCC_CR_HSE_RDY                  (0x00000001UL << 17U)   /* (RO) HSE clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSE_BYP                  (0x00000001UL << 18U)   /* (RW) HSE crystal oscillator bypass */
#define CHIP_REGFLD_RCC_CR_CSS_ON                   (0x00000001UL << 19U)   /* (RW) Clock security system enable */
#define CHIP_REGFLD_RCC_CR_PLL_ON                   (0x00000001UL << 24U)   /* (RW) PLL enable */
#define CHIP_REGFLD_RCC_CR_PLL_RDY                  (0x00000001UL << 25U)   /* (RO) PLL clock ready flag */
#define CHIP_REGMSK_RCC_CR_RESERVED                 (0xFCF00004UL)

/*********** Clock configuration register (RCC_CFGR) **************************/
#define CHIP_REGFLD_RCC_CFGR_SW                     (0x00000003UL <<  0U)   /* (RW) System clock switch */
#define CHIP_REGFLD_RCC_CFGR_SWS                    (0x00000003UL <<  2U)   /* (RO) System clock switch status */
#define CHIP_REGFLD_RCC_CFGR_HPRE                   (0x0000000FUL <<  4U)   /* (RW) HCLK prescaler */
#define CHIP_REGFLD_RCC_CFGR_PPRE                   (0x00000007UL <<  8U)   /* (RW) PCLK prescaler */
#define CHIP_REGFLD_RCC_CFGR_ADC_PRE                (0x00000001UL << 14U)   /* (RW) ADC prescaler (obsolete)  */
#define CHIP_REGFLD_RCC_CFGR_PLL_SRC                (0x00000003UL << 15U)   /* (RW) PLL input clock source */
#define CHIP_REGFLD_RCC_CFGR_PLL_XTPRE              (0x00000001UL << 17U)   /* (RW) HSE divider for PLL input clock (= CFGR2_PREDIV) */
#define CHIP_REGFLD_RCC_CFGR_PLL_MUL                (0x0000000FUL << 18U)   /* (RW) PLL multiplication factor */
#define CHIP_REGFLD_RCC_CFGR_MCO                    (0x0000000FUL << 24U)   /* (RW) Microcontroller clock output */
#if CHIP_DEV_SUPPORT_MCO_PRE
#define CHIP_REGFLD_RCC_CFGR_MCO_PRE                (0x00000007UL << 28U)   /* (RW) Microcontroller Clock Output Prescaler */
#define CHIP_REGFLD_RCC_CFGR_PLL_NODIV              (0x00000001UL << 31U)   /* (RW) PLL clock not divided for MCO (default - div 2) */
#endif
#define CHIP_REGMSK_RCC_CFGR_RESERVED               (0x00C03800UL)

#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSI              0
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSE              1
#define CHIP_REGFLDVAL_RCC_CFGR_SW_PLL              2
#if CHIP_DEV_SUPPORT_HSI48
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSI48            3
#endif

#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_2              8
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_4              9
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_8              10
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_16             11
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_64             12
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_128            13
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_256            14
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_512            15

#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_2              4
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_4              5
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_8              6
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_16             7

/** @todo Bit PLLSRC[0] is available only on STM32F04x, STM32F07x and STM32F09x */
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_SRC_HSI_DIV2    0 /* HSI/2 selected as PLL input clock */
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_SRC_HSI         1 /* HSI/PREDIV selected as PLL input clock */
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_SRC_HSE         2 /* HSE/PREDIV selected as PLL input clock */
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_SRC_HSI48       3 /* HSI48/PREDIV selected as PLL input clock */

#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_2           0
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_3           1
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_4           2
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_5           3
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_6           4
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_7           5
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_8           6
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_9           7
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_10          8
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_11          9
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_12          10
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_13          11
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_14          12
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_15          13
#define CHIP_REGFLDVAL_RCC_CFGR_PLL_MUL_16          14

#define CHIP_REGFLDVAL_RCC_CFGR_MCO_DIS             0 /* MCO output disabled, no clock on MCO */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI14           1 /* Internal RC 14 MHz (HSI14) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_LSI             2 /* Internal low speed (LSI) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_LSE             3 /* External low speed (LSE) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_SYSCLK          4 /* System clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI             5 /* Internal RC 8 MHz (HSI) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSE             6 /* External 4-32 MHz (HSE) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL             7 /* PLL clock selected (divided by 1 or 2, depending on PLL_NODIV) */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI48           8 /* Internal RC 48 MHz (HSI48) oscillator clock selected */

#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_1           0
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_2           1
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_4           2
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_8           3
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_16          4
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_32          5
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_64          6
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_128         7

/*********** Clock interrupt register (RCC_CIR) *******************************/
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_F               (0x00000001UL <<  0U)   /* (RO) LSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_F               (0x00000001UL <<  1U)   /* (RO) LSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_F               (0x00000001UL <<  2U)   /* (RO) HSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_F               (0x00000001UL <<  3U)   /* (RO) HSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_F               (0x00000001UL <<  4U)   /* (RO) PLL ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSI14_RDY_F             (0x00000001UL <<  5U)   /* (RO) HSI14 ready interrupt flag */
#if CHIP_DEV_SUPPORT_HSI48
#define CHIP_REGFLD_RCC_CIR_HSI48_RDY_F             (0x00000001UL <<  6U)   /* (RO) HSI48 ready interrupt flag */
#endif
#define CHIP_REGFLD_RCC_CIR_CSS_F                   (0x00000001UL <<  7U)   /* (RO) Clock security system interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_IE              (0x00000001UL <<  8U)   /* (RW) LSI ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_IE              (0x00000001UL <<  9U)   /* (RW) LSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_IE              (0x00000001UL << 10U)   /* (RW) HSI ready interrupt enable  */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_IE              (0x00000001UL << 11U)   /* (RW) HSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_IE              (0x00000001UL << 12U)   /* (RW) PLL ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_HSI14_RDY_IE            (0x00000001UL << 13U)   /* (RW) HSI14 ready interrupt enable */
#if CHIP_DEV_SUPPORT_HSI48
#define CHIP_REGFLD_RCC_CIR_HSI48_RDY_IE            (0x00000001UL << 14U)   /* (RW) HSI48 ready interrupt enable */
#endif
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_C               (0x00000001UL << 16U)   /* (W1C) LSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_C               (0x00000001UL << 17U)   /* (W1C) LSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_C               (0x00000001UL << 18U)   /* (W1C) HSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_C               (0x00000001UL << 19U)   /* (W1C) HSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_PLL_RDY_C               (0x00000001UL << 20U)   /* (W1C) PLL ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSI14_RDY_C             (0x00000001UL << 21U)   /* (W1C) HSI14 ready interrupt clear */
#if CHIP_DEV_SUPPORT_HSI48
#define CHIP_REGFLD_RCC_CIR_HSI48_RDY_C             (0x00000001UL << 22U)   /* (W1C) HSI48 Ready Interrupt Clear */
#endif
#define CHIP_REGFLD_RCC_CIR_CSS_C                   (0x00000001UL << 23U)   /* (W1C) Clock security system interrupt clear */
#define CHIP_REGMSK_RCC_CIR_RESERVED                (0xFF008000UL)

/*********** APB peripheral reset register 2 (RCC_APB2RSTR) *******************/
#define CHIP_REGFLD_RCC_APB2RSTR_SYSCF_RST          (0x00000001UL <<  0U)   /* (RW) SYSCFG reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_USART6_RST         (0x00000001UL <<  5U)   /* (RW) USART6 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART7_RST         (0x00000001UL <<  6U)   /* (RW) USART7 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART8_RST         (0x00000001UL <<  7U)   /* (RW) USART8 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_ADC_RST            (0x00000001UL <<  9U)   /* (RW) ADC interface reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM1_RST           (0x00000001UL << 11U)   /* (RW) TIM1 timer reset */
#define CHIP_REGFLD_RCC_APB2RSTR_SPI1_RST           (0x00000001UL << 12U)   /* (RW) SPI1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART1_RST         (0x00000001UL << 14U)   /* (RW) USART1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM15_RST          (0x00000001UL << 16U)   /* (RW) TIM15 timer reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM16_RST          (0x00000001UL << 17U)   /* (RW) TIM16 timer reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM17_RST          (0x00000001UL << 18U)   /* (RW) TIM17 timer reset */
#define CHIP_REGFLD_RCC_APB2RSTR_DBGMCU_RST         (0x00000001UL << 22U)   /* (RW) Debug MCU reset */
#define CHIP_REGMSK_RCC_APB2RSTR_RESERVED           (0xFFB8A51EUL)

/*********** APB peripheral reset register 1 (RCC_APB1RSTR) *******************/
#define CHIP_REGFLD_RCC_APB1RSTR_TIM2_RST           (0x00000001UL <<  0U)   /* (RW) TIM2 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM3_RST           (0x00000001UL <<  1U)   /* (RW) TIM3 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM6_RST           (0x00000001UL <<  4U)   /* (RW) TIM6 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM7_RST           (0x00000001UL <<  5U)   /* (RW) TIM7 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM14_RST          (0x00000001UL <<  8U)   /* (RW) TIM14 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_WWDG_RST           (0x00000001UL << 11U)   /* (RW) Window watchdog reset */
#define CHIP_REGFLD_RCC_APB1RSTR_SPI2_RST           (0x00000001UL << 14U)   /* (RW) SPI2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART2_RST         (0x00000001UL << 17U)   /* (RW) USART2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART3_RST         (0x00000001UL << 18U)   /* (RW) USART3 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART4_RST         (0x00000001UL << 19U)   /* (RW) USART4 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART5_RST         (0x00000001UL << 20U)   /* (RW) USART5 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C1_RST           (0x00000001UL << 21U)   /* (RW) I2C1 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C2_RST           (0x00000001UL << 22U)   /* (RW) I2C2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USB_RST            (0x00000001UL << 23U)   /* (RW) USB interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_CAN_RST            (0x00000001UL << 25U)   /* (RW) CAN interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_CRS_RST            (0x00000001UL << 27U)   /* (RW) Clock Recovery System interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_PWR_RST            (0x00000001UL << 28U)   /* (RW) Power interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_DAC_RST            (0x00000001UL << 29U)   /* (RW) DAC interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_CEC_RST            (0x00000001UL << 30U)   /* (RW) HDMI CEC reset*/
#define CHIP_REGMSK_RCC_APB1RSTR_RESERVED           (0x8501B6CCUL)

/*********** AHB peripheral clock enable register (RCC_AHBENR) ****************/
#define CHIP_REGFLD_RCC_AHBENR_DMA1_EN              (0x00000001UL <<  0U)   /* (RW) DMA1 clock enable */
#define CHIP_REGFLD_RCC_AHBENR_DMA2_EN              (0x00000001UL <<  1U)   /* (RW) DMA2 clock enable */
#define CHIP_REGFLD_RCC_AHBENR_SRAM_EN              (0x00000001UL <<  2U)   /* (RW) SRAM interface clock enable */
#define CHIP_REGFLD_RCC_AHBENR_FLITF_EN             (0x00000001UL <<  4U)   /* (RW) FLITF clock enable */
#define CHIP_REGFLD_RCC_AHBENR_CRC_EN               (0x00000001UL <<  6U)   /* (RW) CRC clock enable */
#define CHIP_REGFLD_RCC_AHBENR_IOPA_EN              (0x00000001UL << 17U)   /* (RW) I/O port A clock enable */
#define CHIP_REGFLD_RCC_AHBENR_IOPB_EN              (0x00000001UL << 18U)   /* (RW) I/O port B clock enable */
#define CHIP_REGFLD_RCC_AHBENR_IOPC_EN              (0x00000001UL << 19U)   /* (RW) I/O port C clock enable */
#define CHIP_REGFLD_RCC_AHBENR_IOPD_EN              (0x00000001UL << 20U)   /* (RW) I/O port D clock enable */
#define CHIP_REGFLD_RCC_AHBENR_IOPE_EN              (0x00000001UL << 21U)   /* (RW) I/O port E clock enable */
#define CHIP_REGFLD_RCC_AHBENR_IOPF_EN              (0x00000001UL << 22U)   /* (RW) I/O port F clock enable */
#define CHIP_REGFLD_RCC_AHBENR_TSC_EN               (0x00000001UL << 24U)   /* (RW) Touch sensing controller clock enable */
#define CHIP_REGMSK_RCC_AHBENR_RESERVED             (0xFE81FFA8UL)

/*********** APB peripheral clock enable register 2 (RCC_APB2ENR) ****************/
#define CHIP_REGFLD_RCC_APB2ENR_SYSCG_COMP_EN       (0x00000001UL <<  0U)   /* (RW) SYSCFG & COMP clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART6_EN           (0x00000001UL <<  5U)   /* (RW) USART6 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART7_EN           (0x00000001UL <<  6U)   /* (RW) USART7 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART8_EN           (0x00000001UL <<  7U)   /* (RW) USART8 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_ADC_EN              (0x00000001UL <<  9U)   /* (RW) ADC interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM1_EN             (0x00000001UL << 11U)   /* (RW) TIM1 timer clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_SPI1_EN             (0x00000001UL << 12U)   /* (RW) SPI1 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART1_EN           (0x00000001UL << 14U)   /* (RW) USART1 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM15_EN            (0x00000001UL << 16U)   /* (RW) TIM15 timer clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM16_EN            (0x00000001UL << 17U)   /* (RW) TIM16 timer clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM17_EN            (0x00000001UL << 18U)   /* (RW) TIM17 timer clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_DBGMCU_EN           (0x00000001UL << 22U)   /* (RW) MCU debug module clock enable */
#define CHIP_REGMSK_RCC_APB2ENR_RESERVED            (0xFFB8A51EUL)

/*********** APB peripheral clock enable register 1 (RCC_APB1ENR) ****************/
#define CHIP_REGFLD_RCC_APB1ENR_TIM2_EN             (0x00000001UL <<  0U)   /* (RW) TIM2 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM3_EN             (0x00000001UL <<  1U)   /* (RW) TIM3 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM6_EN             (0x00000001UL <<  4U)   /* (RW) TIM6 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM7_EN             (0x00000001UL <<  5U)   /* (RW) TIM7 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM14_EN            (0x00000001UL <<  8U)   /* (RW) TIM14 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_WWDG_EN             (0x00000001UL << 11U)   /* (RW) Window watchdog clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_SPI2_EN             (0x00000001UL << 14U)   /* (RW) SPI2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART2_EN           (0x00000001UL << 17U)   /* (RW) USART2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART3_EN           (0x00000001UL << 18U)   /* (RW) USART3 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART4_EN           (0x00000001UL << 19U)   /* (RW) USART4 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART5_EN           (0x00000001UL << 20U)   /* (RW) USART5 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C1_EN             (0x00000001UL << 21U)   /* (RW) I2C1 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C2_EN             (0x00000001UL << 22U)   /* (RW) I2C2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USB_EN              (0x00000001UL << 23U)   /* (RW) USB interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_CAN_EN              (0x00000001UL << 25U)   /* (RW) CAN interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_CRS_EN              (0x00000001UL << 27U)   /* (RW) Clock Recovery System interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_PWR_EN              (0x00000001UL << 28U)   /* (RW) Power interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_DAC_EN              (0x00000001UL << 29U)   /* (RW) DAC interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_CEC_EN              (0x00000001UL << 30U)   /* (RW) HDMI CEC clock enable */
#define CHIP_REGMSK_RCC_APB1ENR_RESERVED            (0x8501B6CCUL)

/*********** RTC domain control register (RCC_BDCR) ***************************/
#define CHIP_REGFLD_RCC_BDCR_LSE_ON                 (0x00000001UL <<  0U)   /* (RW) LSE oscillator enable */
#define CHIP_REGFLD_RCC_BDCR_LSE_RDY                (0x00000001UL <<  1U)   /* (RO) LSE oscillator ready */
#define CHIP_REGFLD_RCC_BDCR_LSE_BYP                (0x00000001UL <<  2U)   /* (RW) LSE oscillator bypass */
#define CHIP_REGFLD_RCC_BDCR_LSE_DRV                (0x00000003UL <<  3U)   /* (RW) LSE oscillator drive capability */
#define CHIP_REGFLD_RCC_BDCR_RTC_SEL                (0x00000003UL <<  8U)   /* (RW) RTC clock source selection */
#define CHIP_REGFLD_RCC_BDCR_RTC_EN                 (0x00000001UL << 15U)   /* (RW) RTC clock enable */
#define CHIP_REGFLD_RCC_BDCR_BD_RST                 (0x00000001UL << 16U)   /* (RW) RTC domain software reset */
#define CHIP_REGMSK_RCC_BDCR_RESERVED               (0xFFFE7CE0UL)

#define CHIP_REGFLDVAL_RCC_BDCR_LSE_DRV_LOW         0
#define CHIP_REGFLDVAL_RCC_BDCR_LSE_DRV_MED_HIGH    1
#define CHIP_REGFLDVAL_RCC_BDCR_LSE_DRV_MED_LOW     2
#define CHIP_REGFLDVAL_RCC_BDCR_LSE_DRV_HIGH        3

#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_DIS         0
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSE         1
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSI         2
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_HSE         3

/*********** Control/status register (RCC_CSR) ********************************/
#define CHIP_REGFLD_RCC_CSR_LSI_ON                  (0x00000001UL <<  0U)   /* (RW) LSI oscillator enable */
#define CHIP_REGFLD_RCC_CSR_LSI_RDY                 (0x00000001UL <<  1U)   /* (RO) LSI oscillator ready */
#define CHIP_REGFLD_RCC_CSR_V18PWR_RST              (0x00000001UL << 23U)   /* (RO) Reset flag of the 1.8 V domain */
#define CHIP_REGFLD_RCC_CSR_RMVF                    (0x00000001UL << 24U)   /* (W1) Remove reset flag */
#define CHIP_REGFLD_RCC_CSR_OBL_RSTF                (0x00000001UL << 25U)   /* (RO) Option byte loader reset flag */
#define CHIP_REGFLD_RCC_CSR_PIN_RSTF                (0x00000001UL << 26U)   /* (RO) PIN reset flag */
#define CHIP_REGFLD_RCC_CSR_POR_RSTF                (0x00000001UL << 27U)   /* (RO) POR/PDR reset flag */
#define CHIP_REGFLD_RCC_CSR_SFT_RSTF                (0x00000001UL << 28U)   /* (RO) Software reset flag */
#define CHIP_REGFLD_RCC_CSR_IWDG_RSTF               (0x00000001UL << 29U)   /* (RO) Independent watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_WWDG_RSTF               (0x00000001UL << 30U)   /* (RO) Window watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_LPWR_RSTF               (0x00000001UL << 31U)   /* (RO) Low-power reset flag */
#define CHIP_REGMSK_RCC_CSR_RESERVED                (0x007FFFFCUL)

/*********** AHB peripheral reset register (RCC_AHBRSTR) **********************/
#define CHIP_REGFLD_RCC_AHBRSTR_IOPA_RST            (0x00000001UL << 17U)   /* (RW) I/O port A reset */
#define CHIP_REGFLD_RCC_AHBRSTR_IOPB_RST            (0x00000001UL << 18U)   /* (RW) I/O port B reset */
#define CHIP_REGFLD_RCC_AHBRSTR_IOPC_RST            (0x00000001UL << 19U)   /* (RW) I/O port C reset */
#define CHIP_REGFLD_RCC_AHBRSTR_IOPD_RST            (0x00000001UL << 20U)   /* (RW) I/O port D reset */
#define CHIP_REGFLD_RCC_AHBRSTR_IOPE_RST            (0x00000001UL << 21U)   /* (RW) I/O port E reset */
#define CHIP_REGFLD_RCC_AHBRSTR_IOPF_RST            (0x00000001UL << 22U)   /* (RW) I/O port F reset */
#define CHIP_REGFLD_RCC_AHBRSTR_TSC_RST             (0x00000001UL << 24U)   /* (RW) Touch sensing controller reset */
#define CHIP_REGMSK_RCC_AHBRSTR_RESERVED            (0xFE81FFFFUL)

/*********** Clock configuration register 2 (RCC_CFGR2) ***********************/
#define CHIP_REGFLD_RCC_CFGR2_PREDIV                (0x0000000FUL <<  0U)   /* (RW) PREDIV division factor */
#define CHIP_REGMSK_RCC_CFGR2_RESERVED              (0xFFFFFFF0UL)

/*********** Clock configuration register 3 (RCC_CFGR3) ***********************/
#define CHIP_REGFLD_RCC_CFGR3_USART1_SW             (0x00000003UL <<  0U)   /* (RW) USART1 clock source selection */
#define CHIP_REGFLD_RCC_CFGR3_I2C1_SW               (0x00000001UL <<  4U)   /* (RW) I2C1 clock source selection */
#define CHIP_REGFLD_RCC_CFGR3_CEC_SW                (0x00000001UL <<  6U)   /* (RW) HDMI CEC clock source selection */
#define CHIP_REGFLD_RCC_CFGR3_USB_SW                (0x00000001UL <<  7U)   /* (RW) USB clock source selection */
#define CHIP_REGFLD_RCC_CFGR3_ADC_SW                (0x00000001UL <<  8U)   /* (RW) ADC clock source selection (obsolete) */
#if CHIP_DEV_SUPPORT_USART2_CLKSEL
#define CHIP_REGFLD_RCC_CFGR3_USART2_SW             (0x00000003UL << 16U)   /* (RW) USART2 clock source selection */
#endif
#if CHIP_DEV_SUPPORT_USART3_CLKSEL
#define CHIP_REGFLD_RCC_CFGR3_USART3_SW             (0x00000003UL << 18U)   /* (RW) USART3 clock source selection */
#endif
#define CHIP_REGMSK_RCC_CFGR3_RESERVED              (0xFFF0FE2CUL)

#define CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_PCLK     0
#define CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_SYSCLK   1
#define CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_LSE      2
#define CHIP_REGFLDVAL_RCC_CFGR3_USART1_SW_HSI      3

#define CHIP_REGFLDVAL_RCC_CFGR3_I2C1_SW_HSI        0
#define CHIP_REGFLDVAL_RCC_CFGR3_I2C1_SW_SYSCLK     1

#define CHIP_REGFLDVAL_RCC_CFGR3_CEC_SW_HSI_DIV244  0
#define CHIP_REGFLDVAL_RCC_CFGR3_CEC_SW_LSE         1

#define CHIP_REGFLDVAL_RCC_CFGR3_USB_SW_HSI48       0
#define CHIP_REGFLDVAL_RCC_CFGR3_USB_SW_PLL         1

#define CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_PCLK     0
#define CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_SYSCLK   1
#define CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_LSE      2
#define CHIP_REGFLDVAL_RCC_CFGR3_USART2_SW_HSI      3

#define CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_PCLK     0
#define CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_SYSCLK   1
#define CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_LSE      2
#define CHIP_REGFLDVAL_RCC_CFGR3_USART3_SW_HSI      3

/*********** Clock control register 2 (RCC_CR2) *******************************/
#define CHIP_REGFLD_RCC_CR2_HSI14_ON                (0x00000001UL <<  0U)   /* (RW) */
#define CHIP_REGFLD_RCC_CR2_HSI14_RDY               (0x00000001UL <<  1U)   /* (RO) */
#define CHIP_REGFLD_RCC_CR2_HSI14_DIS               (0x00000001UL <<  2U)   /* (RW) */
#define CHIP_REGFLD_RCC_CR2_HSI14_TRIM              (0x0000001FUL <<  3U)   /* (RW) */
#define CHIP_REGFLD_RCC_CR2_HSI14_CAL               (0x000000FFUL <<  8U)   /* (RO) */
#define CHIP_REGFLD_RCC_CR2_HSI48_ON                (0x00000001UL << 16U)   /* (RW) */
#define CHIP_REGFLD_RCC_CR2_HSI48_RDY               (0x00000001UL << 17U)   /* (RO) */
#define CHIP_REGFLD_RCC_CR2_HSI48_CAL               (0x000000FFUL << 24U)   /* (RO) */
#define CHIP_REGMSK_RCC_CR2_RESERVED                (0x00FC0000UL)

#endif // CHIP_STM32F0XX_REGS_RCC_H
