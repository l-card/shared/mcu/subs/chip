#ifndef CHIP_DEV_SPEC_FEATURES_H
#define CHIP_DEV_SPEC_FEATURES_H




#define CHIP_DEV_SUPPORT_CRS            0
#define CHIP_DEV_SUPPORT_RNG            0
#define CHIP_DEV_SUPPORT_AES            0
#define CHIP_DEV_SUPPORT_DAC            0
#define CHIP_DEV_SUPPORT_COMP1          0
#define CHIP_DEV_SUPPORT_COMP2          0
#define CHIP_DEV_SUPPORT_COMP3          0
#define CHIP_DEV_SUPPORT_TIM4           0
#define CHIP_DEV_SUPPORT_TIM6           0
#define CHIP_DEV_SUPPORT_TIM7           0
#define CHIP_DEV_SUPPORT_TIM15          0
#define CHIP_DEV_SUPPORT_I2C3           0
#define CHIP_DEV_SUPPORT_I2C2_ICLKSEL   0
#define CHIP_DEV_SUPPORT_SPI3           0
#define CHIP_DEV_SUPPORT_I2S2           0
#define CHIP_DEV_SUPPORT_USART3         0
#define CHIP_DEV_SUPPORT_USART4         0
#define CHIP_DEV_SUPPORT_USART5         0
#define CHIP_DEV_SUPPORT_USART6         0
#define CHIP_DEV_SUPPORT_USART2_ICLKSEL 0
#define CHIP_DEV_SUPPORT_USART3_ICLKSEL 0
#define CHIP_DEV_SUPPORT_LPUART2        0
#define CHIP_DEV_SUPPORT_USB            0
#define CHIP_DEV_SUPPORT_UCPD           0
#define CHIP_DEV_SUPPORT_FDCAN          0
#define CHIP_DEV_SUPPORT_CEC            0
#define CHIP_DEV_SUPPORT_DMA2           0
#define CHIP_DEV_SUPPORT_MC02           0
#define CHIP_DEV_SUPPORT_MCO_CFG_EX     0 /* дополнительный выбор источников MCO (PLLPCLK, PLLQCLK, RTCCLK, RTC WAKEUP) */
#define CHIP_DEV_SUPPORT_HSI48          0 /* поддержка внутреннего источника частоты HSI48 для USB */
#define CHIP_DEV_SUPPORT_CRS            0
#define CHIP_DEV_SUPPORT_PORTE          0
#define CHIP_DEV_SUPPORT_SWIOCLAMPDIODE 1
#define CHIP_DEV_SUPPORT_VDDIO2_MON     0
#define CHIP_DEV_SUPPORT_SPI_I2S        1 /* поддержка I2S для SPI интерфейса */



#define CHIP_DEV_UART_CNT              2
#define CHIP_DEV_LPUART_CNT             1

#if 0
#define PWR_PVM_SUPPORT                       /*!< PWR feature available only on specific devices: Power Voltage Monitoring feature */
#define PWR_PVD_SUPPORT                       /*!< PWR feature available only on specific devices: Power Voltage Detection feature */
#define PWR_BOR_SUPPORT                       /*!< PWR feature available only on specific devices: Brown-Out Reset feature         */
#define PWR_SHDW_SUPPORT                      /*!< PWR feature available only on specific devices: Shutdown mode */
#define RCC_PLLQ_SUPPORT

#define GPIO_NRST_CONFIG_SUPPORT         /*!< GPIO feature available only on specific devices: Configure NRST pin */
#define FLASH_SECURABLE_MEMORY_SUPPORT   /*!< Flash feature available only on specific devices: allow to secure memory */
#define FLASH_PCROP_SUPPORT              /*!< Flash feature available only on specific devices: proprietary code read protection areas selected by option */
#define FLASH_DBANK_SUPPORT              /*!< Flash feature available only on specific devices: dualbank */

#define SYSCFG_CDEN_SUPPORT
#endif

#endif // CHIP_DEV_SPEC_FEATURES_H
