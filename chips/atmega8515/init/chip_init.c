#include "chip.h"
#include "chip_config.h"

#ifndef CHIP_CFG_XMEM_INIT
    #define CHIP_CFG_XMEM_INIT 0
#endif

#if CHIP_CFG_XMEM_INIT
    #define CHIP_XMEM_WS_MIN  CHIP_XMEM_WS_0_0
    #define CHIP_XMEM_WS_MAX  CHIP_XMEM_WS_2_1

    #ifndef CHIP_CFG_XMEM_SECTORS_DIV_ADDR
        #define CHIP_CFG_XMEM_SECTORS_DIV_ADDR 0
    #endif


    #if (CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0) || (CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0x260)
        #define XMEM_SRL_CODE 0
    #elif CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0x2000
        #define XMEM_SRL_CODE 1
    #elif CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0x4000
        #define XMEM_SRL_CODE 2
    #elif CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0x6000
        #define XMEM_SRL_CODE 3
    #elif CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0x8000
        #define XMEM_SRL_CODE 4
    #elif CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0xA000
        #define XMEM_SRL_CODE 5
    #elif CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0xC000
        #define XMEM_SRL_CODE 6
    #elif CHIP_CFG_XMEM_SECTORS_DIV_ADDR == 0xE000
        #define XMEM_SRL_CODE 5
    #else
        #error invalid CHIP_CFG_XMEM_SECTORS_DIV_ADDR value
    #endif

    #if XMEM_SRL_CODE == 0
        /* если нет разделений на сектора, то CHIP_CFG_XMEM_WS определяет
         * задержки для всей памяти */
        #ifndef CHIP_CFG_XMEM_WS
            #error CHIP_CFG_XMEM_WS must be defined to set xmem waitstates configuration
        #elif ((CHIP_CFG_XMEM_WS < CHIP_XMEM_WS_MIN) || (CHIP_CFG_XMEM_WS > CHIP_XMEM_WS_MAX))
            #error invalid CHIP_CFG_XMEM_WS value
        #endif
        /* определяем макросы с UPPER и LOWER для единообразной настройки */
        #ifdef CHIP_CFG_XMEM_WS_UPPER_SEC
            #undef CHIP_CFG_XMEM_WS_UPPER_SEC
        #endif
        #ifdef CHIP_CFG_XMEM_WS_LOWER_SEC
            #undef CHIP_CFG_XMEM_WS_LOWER_SEC
        #endif

        #define CHIP_CFG_XMEM_WS_UPPER_SEC CHIP_CFG_XMEM_WS
        #define CHIP_CFG_XMEM_WS_LOWER_SEC CHIP_XMEM_WS_0_0
    #else
        #ifndef CHIP_CFG_XMEM_WS_UPPER_SEC
            #error CHIP_CFG_XMEM_WS_UPPER_SEC must be defined to set xmem upper sector waitstates configuration
        #elif ((CHIP_CFG_XMEM_WS_UPPER_SEC < CHIP_XMEM_WS_MIN) || (CHIP_CFG_XMEM_WS_UPPER_SEC > CHIP_XMEM_WS_MAX))
            #error invalid CHIP_CFG_XMEM_WS_UPPER_SEC value
        #endif
        #ifndef CHIP_CFG_XMEM_WS_LOWER_SEC
            #error CHIP_CFG_XMEM_WS_LOWER_SEC must be defined to set xmem lower sector waitstates configuration
        #elif ((CHIP_CFG_XMEM_WS_LOWER_SEC < CHIP_XMEM_WS_MIN) || (CHIP_CFG_XMEM_WS_LOWER_SEC > CHIP_XMEM_WS_MAX))
            #error invalid CHIP_CFG_XMEM_WS_LOWER_SEC value
        #endif
    #endif


    #ifndef CHIP_CFG_XMEM_BUSKEEPER_EN
        #define CHIP_CFG_XMEM_BUSKEEPER_EN 0
    #endif

    #ifndef CHIP_CFG_XMEM_HIGHBITS_CNT
        #error CHIP_CFG_XMEM_HIGHBITS_CNT must be defined to set PC pins count for xmem address
    #else
        #if CHIP_CFG_XMEM_HIGHBITS_CNT == 8
            #define XMEM_XMM_CODE  0
        #elif CHIP_CFG_XMEM_HIGHBITS_CNT == 7
            #define XMEM_XMM_CODE  1
        #elif CHIP_CFG_XMEM_HIGHBITS_CNT == 6
            #define XMEM_XMM_CODE  2
        #elif CHIP_CFG_XMEM_HIGHBITS_CNT == 5
            #define XMEM_XMM_CODE  3
        #elif CHIP_CFG_XMEM_HIGHBITS_CNT == 4
            #define XMEM_XMM_CODE  4
        #elif CHIP_CFG_XMEM_HIGHBITS_CNT == 3
            #define XMEM_XMM_CODE  5
        #elif CHIP_CFG_XMEM_HIGHBITS_CNT == 2
            #define XMEM_XMM_CODE  6
        #elif CHIP_CFG_XMEM_HIGHBITS_CNT == 0
            #define XMEM_XMM_CODE  7
        #else
            #error Invalid CHIP_CFG_XMEM_HIGHBITS_CNT value
        #endif

    #endif
#endif




/* вызов функции из init3 идет при инциализации, после установки стека и zero_reg,
 * необходимых для C. Так как функция вызывается прямым переходом по метке
 * необходимо объявить как naked */
void chip_init(void) __attribute__ ((naked)) __attribute__ ((section (".init3")));



void chip_init(void) {
#if CHIP_CFG_XMEM_INIT
    /* запрет памяти перед конфигурацией */
    MCUCR &= ~_BV(SRE);

    /* настройка используемых пинов PC под старший байт адреса */
    LBITFIELD_UPD(SFIOR, _BV(XMM2) | _BV(XMM1) | _BV(XMM0), XMEM_XMM_CODE);
    /* разрешение удержания линии */
    LBITFIELD_UPD(SFIOR, _BV(XMBK),  CHIP_CFG_XMEM_BUSKEEPER_EN ? 1 : 0);
    /* один бит настройки циклов ожидания вынесен в MCUCR */
    LBITFIELD_UPD(MCUCR, _BV(SRW10), CHIP_CFG_XMEM_WS_UPPER_SEC & 1);

    EMCUCR = (EMCUCR & 0x81)
               | LBITFIELD_SET(_BV(SRL2) | _BV(SRL1) | _BV(SRL0), XMEM_SRL_CODE)
               | LBITFIELD_SET(_BV(SRW01) | _BV(SRW00), CHIP_CFG_XMEM_WS_LOWER_SEC)
               | LBITFIELD_SET(_BV(SRW11), (CHIP_CFG_XMEM_WS_UPPER_SEC >> 1) & 1)
                  ;

    MCUCR |= _BV(SRE); /* разрешаем внешнюю память */
#endif
}

