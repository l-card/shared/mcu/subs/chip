#ifndef CHIP_CFG_DEFS_H
#define CHIP_CFG_DEFS_H

#include "chip_config.h"

#ifdef CHIP_CFG_CLK_HSE_MODE
    #define CHIP_CLK_HSE_MODE   CHIP_CFG_CLK_HSE_MODE
#else
    #define CHIP_CLK_HSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_HSE_PIN_PA0
    #define CHIP_CLK_HSE_PIN_PA0 CHIP_CFG_CLK_HSE_PIN_PA0
#else
    #define CHIP_CLK_HSE_PIN_PA0 0
#endif

#ifdef CHIP_CFG_CLK_HSE_CSS_EN
    #define CHIP_CLK_HSE_CSS_EN CHIP_CFG_CLK_HSE_CSS_EN
#else
    #define CHIP_CLK_HSE_CSS_EN 0
#endif


#ifdef CHIP_CFG_CLK_LSE_MODE
    #define CHIP_CLK_LSE_MODE   CHIP_CFG_CLK_LSE_MODE
#else
    #define CHIP_CLK_LSE_MODE   CHIP_CLK_EXTMODE_DIS
#endif

#ifdef CHIP_CFG_CLK_HSI_EN
    #define CHIP_CLK_HSI_EN     CHIP_CFG_CLK_HSI_EN
#else
    #define CHIP_CLK_HSI_EN     1
#endif


#ifdef CHIP_CFG_CLK_CRS_SETUP_EN
    #define CHIP_CLK_CRS_SETUP_EN CHIP_CFG_CLK_CRS_SETUP_EN
#else
    #define CHIP_CLK_CRS_SETUP_EN 0
#endif


#ifdef CHIP_CFG_CLK_PLL_EN
    #define CHIP_CLK_PLL_EN CHIP_CFG_CLK_PLL_EN

    #ifdef CHIP_CFG_CLK_PLL_SRC
        #define CHIP_CLK_PLL_SRC CHIP_CFG_CLK_PLL_SRC
    #else
        #define CHIP_CLK_PLL_SRC CHIP_CLK_HSI
    #endif

    #ifdef CHIP_CFG_CLK_PLL_PREDIV
        #define CHIP_CLK_PLL_PREDIV CHIP_CFG_CLK_PLL_PREDIV
    #else
        #define CHIP_CLK_PLL_PREDIV 1
    #endif

    #ifdef CHIP_CFG_CLK_PLL_OUTDIV
        #define CHIP_CLK_PLL_OUTDIV CHIP_CFG_CLK_PLL_OUTDIV
    #else
        #define CHIP_CLK_PLL_OUTDIV 1
    #endif

    #ifdef CHIP_CFG_CLK_PLL_BYPASS
        #define CHIP_CLK_PLL_BYPASS CHIP_CFG_CLK_PLL_BYPASS
    #else
        #define CHIP_CLK_PLL_BYPASS 0
    #endif

    #ifdef CHIP_CFG_CLK_PLL_MUL
        #define CHIP_CLK_PLL_MUL CHIP_CFG_CLK_PLL_MUL
    #else
        #error PLL multiplier is not specified
    #endif
#else
    #define CHIP_CLK_PLL_EN     0
    #define CHIP_CLK_PLL_PREDIV 0
    #define CHIP_CLK_PLL_MUL    0
    #define CHIP_CLK_PLL_BYPASS 0
#endif


#ifdef CHIP_CFG_CLK_SYS_SRC
    #define CHIP_CLK_SYS_SRC CHIP_CFG_CLK_SYS_SRC
#else
    #define CHIP_CLK_SYS_SRC CHIP_CLK_HSI
#endif


#ifdef CHIP_CFG_CLK_AHB_DIV
    #define CHIP_CLK_AHB_DIV    CHIP_CFG_CLK_AHB_DIV
#else
    #define CHIP_CLK_AHB_DIV    1
#endif

#ifdef CHIP_CFG_CLK_APB1_DIV
    #define CHIP_CLK_APB1_DIV    CHIP_CFG_CLK_APB1_DIV
#else
    #define CHIP_CLK_APB1_DIV   1
#endif

#ifdef CHIP_CFG_CLK_APB2_DIV
    #define CHIP_CLK_APB2_DIV    CHIP_CFG_CLK_APB2_DIV
#else
    #define CHIP_CLK_APB2_DIV   1
#endif

#ifdef CHIP_CFG_CLK_MCO_EN
    #define CHIP_CLK_MCO_EN     CHIP_CFG_CLK_MCO_EN
#else
    #define CHIP_CLK_MCO_EN     0
#endif

#if CHIP_CLK_MCO_EN
    #ifndef CHIP_CFG_CLK_MCO_SRC
        #error MCO source clock is not specified
    #else
        #define CHIP_CLK_MCO_SRC CHIP_CFG_CLK_MCO_SRC
    #endif

    #ifdef CHIP_CFG_CLK_MCO_PLL_DIV
        #define CHIP_CLK_MCO_PLL_DIV    CHIP_CFG_CLK_MCO_PLL_DIV
    #else
        #define CHIP_CLK_MCO_PLL_DIV    CHIP_REGFLDVAL_RCC_CFGR_MCO_PRE_MIN
    #endif

    #ifdef CHIP_CFG_CLK_MCO_PIN
       #define CHIP_CLK_MCO_PIN CHIP_CFG_CLK_MCO_PIN
    #else
       #define CHIP_CLK_MCO_PIN CHIP_PIN_PA8_MCO
    #endif
#endif



#ifdef CHIP_CFG_CLK_LPUART1_SRC
    #define CHIP_CLK_LPUART1_SRC    CHIP_CFG_CLK_LPUART1_SRC
#else
    #define CHIP_CLK_LPUART1_SRC    CHIP_CLK_APB1
#endif

#if CHIP_DEV_LPUART_CNT >= 2
    #ifdef CHIP_CFG_CLK_LPUART2_SRC
        #define CHIP_CLK_LPUART2_SRC    CHIP_CFG_CLK_LPUART2_SRC
    #else
        #define CHIP_CLK_LPUART2_SRC    CHIP_CLK_APB1
    #endif
#endif

#ifdef CHIP_CFG_CLK_LPTIM_SRC
    #define CHIP_CLK_LPTIM_SRC      CHIP_CFG_CLK_LPTIM_SRC
#else
    #define CHIP_CLK_LPTIM_SRC      CHIP_CLK_APB1
#endif


#if CHIP_DEV_SUPPORT_RNG
    #ifdef CHIP_CFG_CLK_RNG_PREDIV
        #define CHIP_CLK_RNG_PREDIV     CHIP_CFG_CLK_RNG_PREDIV
    #else
        #define CHIP_CLK_RNG_PREDIV     1
    #endif
#endif

#ifdef CHIP_CFG_CLK_ADC_SRC
    #define CHIP_CLK_ADC_SRC        CHIP_CFG_CLK_ADC_SRC
#else
    #define CHIP_CLK_ADC_SRC        CHIP_CLK_ADC_HCLK
#endif

#ifdef CHIP_CFG_CLK_ADC_PLL_DIV
    #define CHIP_CLK_ADC_PLL_DIV    CHIP_CFG_CLK_ADC_PLL_DIV
#else
    #define CHIP_CLK_ADC_PLL_DIV    0
#endif

#ifdef CHIP_CFG_CLK_ADC_HCLK_DIV
    #define CHIP_CLK_ADC_HCLK_DIV    CHIP_CFG_CLK_ADC_HCLK_DIV
#else
    #define CHIP_CLK_ADC_HCLK_DIV    1
#endif


#ifdef CHIP_CFG_CLK_ADC_1M_SRC
    #define CHIP_CLK_ADC_1M_SRC     CHIP_CFG_CLK_ADC_1M_SRC
#else
    #define CHIP_CLK_ADC_1M_SRC     CHIP_CLK_HSI
#endif


#ifdef CHIP_CFG_CLK_ADC_1M_DIV
    #define CHIP_CLK_ADC_1M_DIV         CHIP_CFG_CLK_ADC_1M_DIV
#else
    #define CHIP_CLK_ADC_1M_DIV         1
#endif


#ifdef CHIP_CFG_CLK_DMA_EN
    #define CHIP_CLK_DMA_EN         CHIP_CFG_CLK_DMA_EN
#else
    #define CHIP_CLK_DMA_EN         0
#endif
#ifdef CHIP_CFG_CLK_SRAM_EN
    #define CHIP_CLK_SRAM_EN        CHIP_CFG_CLK_SRAM_EN
#else
    #define CHIP_CLK_SRAM_EN        1
#endif
#ifdef CHIP_CFG_CLK_FLASH_EN
    #define CHIP_CLK_FLASH_EN       CHIP_CFG_CLK_FLASH_EN
#else
    #define CHIP_CLK_FLASH_EN       1
#endif
#ifdef CHIP_CFG_CLK_HSQRT_EN
    #define CHIP_CLK_HSQRT_EN       CHIP_CFG_CLK_HSQRT_EN
#else
    #define CHIP_CLK_HSQRT_EN       0
#endif
#ifdef CHIP_CFG_CLK_CRC_EN
    #define CHIP_CLK_CRC_EN         CHIP_CFG_CLK_HSQRT_EN
#else
    #define CHIP_CLK_CRC_EN         0
#endif
#ifdef CHIP_CFG_CLK_HDIV_EN
    #define CHIP_CLK_HDIV_EN        CHIP_CFG_CLK_HSQRT_EN
#else
    #define CHIP_CLK_HDIV_EN        0
#endif
#if CHIP_DEV_SUPPORT_RNG
    #if CHIP_CFG_CLK_RNG_EN
        #define CHIP_CLK_RNG_EN     CHIP_CFG_CLK_RNG_EN
    #else
        #define CHIP_CLK_RNG_EN     0
    #endif
#endif
#if CHIP_DEV_SUPPORT_SAC
    #if CHIP_CFG_CLK_SAC_EN
        #define CHIP_CLK_SAC_EN     CHIP_CFG_CLK_SAC_EN
    #else
        #define CHIP_CLK_SAC_EN     0
    #endif
#endif
#ifdef CHIP_CFG_CLK_ADC_EN
    #define CHIP_CLK_ADC_EN         CHIP_CFG_CLK_ADC_EN
#else
    #define CHIP_CLK_ADC_EN         0
#endif

#ifdef CHIP_CFG_CLK_AFIO_EN
    #define CHIP_CLK_AFIO_EN        CHIP_CFG_CLK_AFIO_EN
#else
    #define CHIP_CLK_AFIO_EN        0
#endif
#if CHIP_CLK_MCO_EN
    #define CHIP_CLK_GPIOA_EN       1
#elif defined CHIP_CFG_CLK_GPIOA_EN
    #define CHIP_CLK_GPIOA_EN       CHIP_CFG_CLK_GPIOA_EN
#else
    #define CHIP_CLK_GPIOA_EN       0
#endif
#ifdef CHIP_CFG_CLK_GPIOB_EN
    #define CHIP_CLK_GPIOB_EN       CHIP_CFG_CLK_GPIOB_EN
#else
    #define CHIP_CLK_GPIOB_EN       0
#endif
#ifdef CHIP_CFG_CLK_GPIOC_EN
    #define CHIP_CLK_GPIOC_EN       CHIP_CFG_CLK_GPIOC_EN
#else
    #define CHIP_CLK_GPIOC_EN       0
#endif
#if CHIP_DEV_SUPPORT_PORTD
    #ifdef CHIP_CFG_CLK_GPIOD_EN
        #define CHIP_CLK_GPIOD_EN   CHIP_CFG_CLK_GPIOD_EN
    #else
        #define CHIP_CLK_GPIOD_EN   0
    #endif
#endif
#ifdef CHIP_CFG_CLK_GPIOF_EN
    #define CHIP_CLK_GPIOF_EN       CHIP_CFG_CLK_GPIOF_EN
#else
    #define CHIP_CLK_GPIOF_EN       0
#endif

#ifdef CHIP_CFG_CLK_TIM1_EN
    #define CHIP_CLK_TIM1_EN        CHIP_CFG_CLK_TIM1_EN
#else
    #define CHIP_CLK_TIM1_EN        0
#endif
#ifdef CHIP_CFG_CLK_TIM3_EN
    #define CHIP_CLK_TIM3_EN        CHIP_CFG_CLK_TIM3_EN
#else
    #define CHIP_CLK_TIM3_EN        0
#endif
#if CHIP_DEV_SUPPORT_TIM4
    #ifdef CHIP_CFG_CLK_TIM4_EN
        #define CHIP_CLK_TIM4_EN        CHIP_CFG_CLK_TIM4_EN
    #else
        #define CHIP_CLK_TIM4_EN        0
    #endif
#endif
#ifdef CHIP_CFG_CLK_TIM6_EN
    #define CHIP_CLK_TIM6_EN        CHIP_CFG_CLK_TIM6_EN
#else
    #define CHIP_CLK_TIM6_EN        0
#endif
#ifdef CHIP_CFG_CLK_TIM8_EN
    #define CHIP_CLK_TIM8_EN        CHIP_CFG_CLK_TIM8_EN
#else
    #define CHIP_CLK_TIM8_EN        0
#endif
#ifdef CHIP_CFG_CLK_LPTIM_EN
    #define CHIP_CLK_LPTIM_EN       CHIP_CFG_CLK_LPTIM_EN
#else
    #define CHIP_CLK_LPTIM_EN       0
#endif

#ifdef CHIP_CFG_CLK_USART1_EN
    #define CHIP_CLK_USART1_EN      CHIP_CFG_CLK_USART1_EN
#else
    #define CHIP_CLK_USART1_EN      0
#endif
#ifdef CHIP_CFG_CLK_USART2_EN
    #define CHIP_CLK_USART2_EN      CHIP_CFG_CLK_USART2_EN
#else
    #define CHIP_CLK_USART2_EN      0
#endif
#if CHIP_DEV_SUPPORT_UART5
    #ifdef CHIP_CFG_CLK_UART5_EN
        #define CHIP_CLK_UART5_EN   CHIP_CFG_CLK_UART5_EN
    #else
        #define CHIP_CLK_UART5_EN   0
    #endif
#endif
#if CHIP_DEV_SUPPORT_UART6
    #ifdef CHIP_CFG_CLK_UART6_EN
        #define CHIP_CLK_UART6_EN   CHIP_CFG_CLK_UART6_EN
    #else
        #define CHIP_CLK_UART6_EN   0
    #endif
#endif
#ifdef CHIP_CFG_CLK_LPUART1_EN
    #define CHIP_CLK_LPUART1_EN     CHIP_CFG_CLK_LPUART1_EN
#else
    #define CHIP_CLK_LPUART1_EN     0
#endif
#if CHIP_DEV_LPUART_CNT >= 2
    #ifdef CHIP_CFG_CLK_LPUART2_EN
        #define CHIP_CLK_LPUART2_EN CHIP_CFG_CLK_LPUART2_EN
    #else
        #define CHIP_CLK_LPUART2_EN 0
    #endif
#endif

#ifdef CHIP_CFG_CLK_SPI1_EN
    #define CHIP_CLK_SPI1_EN        CHIP_CFG_CLK_SPI1_EN
#else
    #define CHIP_CLK_SPI1_EN        0
#endif
#ifdef CHIP_CFG_CLK_SPI2_EN
    #define CHIP_CLK_SPI2_EN        CHIP_CFG_CLK_SPI2_EN
#else
    #define CHIP_CLK_SPI2_EN        0
#endif
#if CHIP_DEV_SPI_CNT >= 3
    #ifdef CHIP_CFG_CLK_SPI3_EN
        #define CHIP_CLK_SPI3_EN    CHIP_CFG_CLK_SPI3_EN
    #else
        #define CHIP_CLK_SPI3_EN    0
    #endif
#endif

#ifdef CHIP_CFG_CLK_I2C1_EN
    #define CHIP_CLK_I2C1_EN        CHIP_CFG_CLK_I2C1_EN
#else
    #define CHIP_CLK_I2C1_EN        0
#endif
#ifdef CHIP_CFG_CLK_I2C2_EN
    #define CHIP_CLK_I2C2_EN        CHIP_CFG_CLK_I2C2_EN
#else
    #define CHIP_CLK_I2C2_EN        0
#endif
#if CHIP_DEV_CAN_CNT >= 1
    #ifdef CHIP_CFG_CLK_CAN1_EN
        #define CHIP_CLK_CAN1_EN    CHIP_CFG_CLK_CAN1_EN
    #else
        #define CHIP_CLK_CAN1_EN    0
    #endif
#endif

#ifdef CHIP_CFG_CLK_WWDG_EN
    #define CHIP_CLK_WWDG_EN        CHIP_CFG_CLK_WWDG_EN
#else
    #define CHIP_CLK_WWDG_EN        0
#endif
#ifdef CHIP_CFG_CLK_PWR_EN
    #define CHIP_CLK_PWR_EN        CHIP_CFG_CLK_PWR_EN
#else
    #define CHIP_CLK_PWR_EN        0
#endif

#ifdef CHIP_CFG_CLK_ADC_EN
    #define CHIP_CLK_ADC_EN        CHIP_CFG_CLK_ADC_EN
#else
    #define CHIP_CLK_ADC_EN        0
#endif
#ifdef CHIP_CFG_CLK_COMP_EN
    #define CHIP_CLK_COMP_EN       CHIP_CFG_CLK_COMP_EN
#else
    #define CHIP_CLK_COMP_EN       0
#endif
#ifdef CHIP_CFG_CLK_OPA_EN
    #define CHIP_CLK_OPA_EN        CHIP_CFG_CLK_OPA_EN
#else
    #define CHIP_CLK_OPA_EN        0
#endif

#ifdef CHIP_CFG_CLK_TSC_EN
    #define CHIP_CLK_TSC_EN        CHIP_CFG_CLK_TSC_EN
#else
    #define CHIP_CLK_TSC_EN        0
#endif

#ifdef CHIP_CFG_CLK_PWR_EN
    #define CHIP_CLK_PWR_EN        CHIP_CFG_CLK_PWR_EN
#else
    #define CHIP_CLK_PWR_EN        0
#endif

#ifdef CHIP_CFG_CLK_BEEP1_EN
    #define CHIP_CLK_BEEP1_EN       CHIP_CFG_CLK_BEEP1_EN
#else
    #define CHIP_CLK_BEEP1_EN       0
#endif
#if CHIP_DEV_BEEPER_CNT >= 2
#ifdef CHIP_CFG_CLK_BEEP2_EN
    #define CHIP_CLK_BEEP2_EN       CHIP_CFG_CLK_BEEP2_EN
#else
    #define CHIP_CLK_BEEP2_EN       0
#endif
#endif


#ifdef CHIP_CFG_LTIMER_EN
    #define CHIP_LTIMER_EN   CHIP_CFG_LTIMER_EN
#else
    #define CHIP_LTIMER_EN   1
#endif


#endif // CHIP_CFG_DEFS_H
