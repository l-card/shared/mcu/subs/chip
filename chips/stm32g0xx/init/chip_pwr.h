#ifndef CHIP_PWR_H
#define CHIP_PWR_H

#include "chip_pwr_defs.h"
#include "chip_config.h"


void chip_pwr_bd_cfg_write_enable(void);
void chip_pwr_bd_cfg_write_disable(void);

#endif // CHIP_PWR_H
