#ifndef REGS_PPOR_H
#define REGS_PPOR_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO  uint32_t RESET_FLAG;                 /* 0x0: reset source indication flag */
    __IO uint32_t RESET_STATUS;                /* 0x4: reset source status */
    __IO uint32_t RESET_HOLD;                  /* 0x8: reset hold attribute */
    __IO uint32_t RESET_ENABLE;                /* 0xC: reset source enable */
    __IO uint32_t RESET_HOT;                   /* 0x10: reset type triggered by reset */
    __IO uint32_t RESET_COLD;                  /* 0x14: reset type attribute */
    uint8_t  RESERVED0[4];                     /* 0x18 - 0x1B: Reserved */
    __IO uint32_t SOFTWARE_RESET;              /* 0x1C: Software reset counter */
} CHIP_REGS_PPOR_T;


#define CHIP_REGS_PPOR            ((CHIP_REGS_PPOR_T *)CHIP_MEMRGN_ADDR_PPOR)


/*********** Reset source - RESET_FLAG/STATUS/HOLD/ENABLE/HOT/COLD ************/
#define CHIP_REGFLD_PPOR_RESET_FLAG_SOFT                (0x00000001UL << 31U) /* (RW1C) Software reset */
#define CHIP_REGFLD_PPOR_RESET_FLAG_PWDG                (0x00000001UL << 20U) /* (RW1C) PWDG */
#define CHIP_REGFLD_PPOR_RESET_FLAG_WDG3                (0x00000001UL << 19U) /* (RW1C) WDG3 */
#define CHIP_REGFLD_PPOR_RESET_FLAG_WDG2                (0x00000001UL << 18U) /* (RW1C) WDG2 */
#define CHIP_REGFLD_PPOR_RESET_FLAG_WDG1                (0x00000001UL << 17U) /* (RW1C) WDG1 */
#define CHIP_REGFLD_PPOR_RESET_FLAG_WDG0                (0x00000001UL << 16U) /* (RW1C) WDG0 */
#define CHIP_REGFLD_PPOR_RESET_FLAG_DEBUG               (0x00000001UL <<  4U) /* (RW1C) Debug reset */
#define CHIP_REGFLD_PPOR_RESET_FLAG_BROWNOUT            (0x00000001UL <<  0U) /* (RW1C) Voltage too low */



#endif // REGS_PPOR_H
