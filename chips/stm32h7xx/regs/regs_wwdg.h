#ifndef CHIP_STM32H7XX_REGS_WWDG_H_
#define CHIP_STM32H7XX_REGS_WWDG_H_


#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
  __IO uint32_t CR;   /* WWDG Control register,       Address offset: 0x00 */
  __IO uint32_t CFR;  /* WWDG Configuration register, Address offset: 0x04 */
  __IO uint32_t SR;   /* WWDG Status register,        Address offset: 0x08 */
} CHIP_REGS_WWDG_T ;

#define CHIP_REGS_WWDG1          ((CHIP_REGS_WWDG_T *) CHIP_MEMRGN_ADDR_PERIPH_WWDG1)
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_REGS_WWDG2          ((CHIP_REGS_WWDG_T *) CHIP_MEMRGN_ADDR_PERIPH_WWDG2)
#endif


#define CHIP_REGFLD_WWDG_RST_VALUE              (0x40)

/*********** WWDG control register (WWDG_CR) **********************************/
#define CHIP_REGFLD_WWDG_CR_T                   (0x0000007FUL <<  0U) /* 7-Bit counter (MSB to LSB) */
#define CHIP_REGFLD_WWDG_CR_WDGA                (0x00000001UL <<  7U) /* Activation bit */
#define CHIP_REGMSK_WWDG_CR_RESERVED            (0xFFFFFF00UL)

/*********** WWDG configuration register (WWDG_CFR) ***************************/
#define CHIP_REGFLD_WWDG_CFR_W                  (0x0000007FUL <<  0U) /* 7-bit window value */
#define CHIP_REGFLD_WWDG_CFR_EWI                (0x00000001UL <<  9U) /* Early Wakeup Interrupt */
#define CHIP_REGFLD_WWDG_CFR_WDGTB              (0x00000007UL << 11U) /* Timer Base */
#define CHIP_REGMSK_WWDG_CFR_RESERVED           (0xFFFFC5C0UL)

#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_1         0
#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_2         1
#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_4         2
#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_8         3
#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_16        4
#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_32        5
#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_64        6
#define CHIP_REGFLDVAL_WWDG_CFR_WDGTB_128       7

/*********** WWDG status register (WWDG_SR) ***********************************/
#define CHIP_REGFLD_WWDG_SR_EWIF                (0x00000001UL <<  0U) /* Early Wakeup Interrupt Flag */

#endif // CHIP_STM32H7XX_REGS_WWDG_H_
