#ifndef REGS_TMU_H
#define REGS_TMU_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t TER;             /* TMU Enable register,                                              Address offset: 0x00 */
    __IO uint32_t TPS;             /* TMU Probe Select register,                                        Address offset: 0x04 */
    __IO uint32_t TIER;            /* TMU Interrupt Enable register,                                    Address offset: 0x08 */
    __IO uint32_t TIDR;            /* TMU Interrupt Detect register,                                    Address offset: 0x0C */
    __IO uint32_t TMHTITR;         /* TMU Monitor High Temperature Immediate Threshold register,        Address offset: 0x10 */
    __IO uint32_t TMHTATR;         /* TMU Monitor High Temperature Average Threshold register,          Address offset: 0x14 */
    __IO uint32_t TMHTACTR;        /* TMU Monitor High Temperature Average Critical Threshold register, Address offset: 0x18 */
    __I  uint32_t TSCR;            /* TMU Sensor Calibration register,                                  Address offset: 0x1C */
    __I  uint32_t TRITSR;          /* TMU Report Immediate Temperature Site register,                   Address offset: 0x20 */
    __I  uint32_t TRATSR;          /* TMU Report Average Temperature Site register,                     Address offset: 0x24 */
    __IO uint32_t TASR;            /* TMU AS Register,                                                  Address offset: 0x28 */
    __IO uint32_t TTMC;            /* TMU TMC Register,                                                 Address offset: 0x2C */
    __IO uint32_t TCALIV0;         /* TMU CALIV0 Register,                                              Address offset: 0x30 */
    __IO uint32_t TCALIV1;         /* TMU CALIV1 Register,                                              Address offset: 0x34 */
    __IO uint32_t TCALIV_M40;      /* TMU CALIV_M40 Register,                                           Address offset: 0x38 */
    __IO uint32_t TRIM;            /* TMU TRIM register,                                                Address offset: 0x3C */
} CHIP_REGS_TMU_T;

#define CHIP_REGS_TMU          ((CHIP_REGS_TMU_T *) CHIP_MEMRGN_ADDR_PERIPH_TMU)

/*********** TMU Enable register (TMU_TER) ************************************/
#define CHIP_REGFLD_TMU_TER_ALPF                (0x00000003UL <<  0U)  /* (RW) Average low pass filter setting */
#define CHIP_REGFLD_TMU_TER_ADC_PD              (0x00000001UL << 30U)  /* (RW) ADC power down control */
#define CHIP_REGFLD_TMU_TER_EN                  (0x00000001UL << 31U)  /* (RW) Enable the temperature sensor */
#define CHIP_REGMSK_TMU_TER_RESERVED            (0x3FFFFFFCUL)

#define CHIP_REGFLDVAL_TMU_TER_ALPF_1_0         0
#define CHIP_REGFLDVAL_TMU_TER_ALPF_0_5         1
#define CHIP_REGFLDVAL_TMU_TER_ALPF_0_25        2
#define CHIP_REGFLDVAL_TMU_TER_ALPF_0_125       3


/*********** TMU Probe Select register (TMU_TPS) ******************************/
#define CHIP_REGFLD_TMU_TPS_PROBE_SEL           (0x00000003UL << 30U)  /* (RW) Probe selection  */
#define CHIP_REGMSK_TMU_TPS_RESERVED            (0x3FFFFFFFUL)

#define CHIP_REGFLDVAL_TMU_TPS_PROBE_SEL_MAIN   0 /* select the main probe only */
#define CHIP_REGFLDVAL_TMU_TPS_PROBE_SEL_A53    1 /* select the remote probe(near A53) only */
#define CHIP_REGFLDVAL_TMU_TPS_PROBE_SEL_BOTH   2 /* select both 2 probes */


/*********** TMU Interrupt Enable/Detect register (TMU_TIER/TIDR) *************/
#define CHIP_REGFLD_TMU_TIR_ATCTE(i)            (0x00000001UL << (25U + 4*(i))) /* (RW/RW1C) Average temperature critical threshold exceeded of probe i (0,1) */
#define CHIP_REGFLD_TMU_TIR_ATTE(i)             (0x00000001UL << (26U + 4*(i))) /* (RW/RW1C) Average temperature threshold exceeded of probe i (0,1)  */
#define CHIP_REGFLD_TMU_TIR_ITTE(i)             (0x00000001UL << (27U + 4*(i))) /* (RW/RW1C) Immediate temperature threshold exceeded of probe i (0,1)  */

/***********  TMU Monitor High Temperature Immediate/Average/Average Critical Threshold (TMU_TMHTITR/TMHTATR/TMHTACTR) **/
#define CHIP_REGFLD_TMU_TR_TEMP(i)              (0x000000FFUL << (0U + 16*(i))) /* (RW) High temperature average critical threshold value for probe i (0,1). */
#define CHIP_REGFLD_TMU_TR_EN(i)                (0x00000001UL << (30U +   (i))) /* (RW) Enable threshold for probe i (0,1) */

/*********** TMU Sensor Calibration register (TMU_TSCR)************************/
#define CHIP_REGFLD_TMU_TSCR_SNSR(i)            (0x00000FFFUL << (0U + 16*(i))) /* (RO) Raw sensor value of probe i (0,1). */
#define CHIP_REGFLD_TMU_TSCR_V(i)               (0x00000001UL << (30U +   (i))) /* (RO) Measured temperature ready of probe i (0,1) */

/*********** TMU Report Immediate/Average Temperature Site register (TMU_TRITSR/TRATSR) */
#define CHIP_REGFLD_TMU_TRTSR_TEMP(i)           (0x000000FFUL << (0U + 16*(i))) /* (RW) Last calibratied temperature of probe i (0,1) reading at site when V=1. */
#define CHIP_REGFLD_TMU_TRTSR_V(i)              (0x00000001UL << (30U +   (i))) /* (RW) Measured temperature ready for probe i (0,1) */

/*********** TMU AS Register (TMU_TASR) ***************************************/
#define CHIP_REGFLD_TMU_TASR_BUF_VERF_SEL       (0x00000003UL <<  0U)  /* (RW) Reference voltage setting bits for the amplifier in the Positive-TC generator block. */
#define CHIP_REGFLD_TMU_TASR_BUF_SLOP_SEL       (0x0000000FUL << 16U)  /* (RW) Amplifier gain setting bits. */

/*********** TMU TMC Register (TMU_TTMC) **************************************/
#define CHIP_REGFLD_TMU_TTMC_TMUX               (0x00000007UL <<  0U)  /* (RW)Test-mux address setting bits. Only used for debugging purpose */

/*********** TMU CALIV0/1 Register (TMU_TCALIV0/TMU_TCALIV1) ******************/
#define CHIP_REGFLD_TMU_TCALIV_SNSR25C          (0x00000FFFUL <<  0U)  /* (RW) 25C sensor value of probe i (0,1) read from FUSE(default 1p value), system should program it before enable TMU if enable 1P calibration. */
#define CHIP_REGFLD_TMU_TCALIV_SNSR105C         (0x00000FFFUL << 16U)  /* (RW) 105C sensor value of probe i (0,1) read from FUSE, system should program it before enable TMU if enable 1P calibration. */
#define CHIP_REGFLD_TMU_TCALIV_EN               (0x00000001UL << 31U)  /* (RW) Enable 105C calibration value used in 1p calibration. If enable, 105C sensor value take the priority. */

/*********** TMU CALIV_M40 Register (TMU_TCALIV_M40) **************************/
#define CHIP_REGFLD_TMU_TMU_TCALIV_M40_SNSR(i)  (0x00000FFFUL <<  (0U + 16*(i)))  /* (RW) m40C sensor value of probe i (0,1) read from FUSE, used for software 3p calibration if needed */

/*********** TMU TRIM register (TMU_TRIM) *************************************/
#define CHIP_REGFLD_TMU_TRIM_VREFT_FLAG         (0x00000001UL <<  0U)  /* (RO) Flag of the Vref trim */
#define CHIP_REGFLD_TMU_TRIM_VBE_FLAG           (0x00000001UL <<  1U)  /* (RO) Flag of the Vbe trim */
#define CHIP_REGFLD_TMU_EN_VBE_TRIM             (0x00000001UL <<  5U)  /* (RW) Vbe calibration enable */
#define CHIP_REGFLD_TMU_EN_VREFT_TRIM           (0x00000001UL <<  6U)  /* (RW) Vref calibration enable */
#define CHIP_REGFLD_TMU_EN_CH                   (0x00000001UL <<  7U)  /* (RW) Offset calibration enable */
#define CHIP_REGFLD_TMU_VLSB                    (0x0000000FUL << 12U)  /* (RW) LSB voltage of DAC trimming */
#define CHIP_REGFLD_TMU_BJT_CUR                 (0x0000000FUL << 20U)  /* (RW) Current trimming ports for BJT */
#define CHIP_REGFLD_TMU_BGR                     (0x0000000FUL << 28U)  /* (RW) Curvature trimming bits for voltage change in BGR */



#endif // REGS_TMU_H
