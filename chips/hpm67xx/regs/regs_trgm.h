#ifndef REGS_TRGM_H
#define REGS_TRGM_H

#include "chip_ioreg_defs.h"
typedef struct {
    __IO uint32_t FILTCFG[20];                 /* 0x0 - 0x4C: Filter configure register */
    __I  uint8_t  RESERVED0[176];              /* 0x50 - 0xFF: Reserved */
    __IO uint32_t TRGOCFG[64];                 /* 0x100 - 0x1FC: Trigger manager output configure register */
    __IO uint32_t DMACFG[4];                   /* 0x200 - 0x20C: DMA request configure register */
    __I  uint8_t  RESERVED1[496];              /* 0x210 - 0x3FF: Reserved */
    __IO uint32_t GCR;                         /* 0x400: General Control Register */
} CHIP_REGS_TRGM_T;

#define CHIP_REGS_TRGM0           ((CHIP_REGS_TRGM_T *)CHIP_MEMRGN_ADDR_TRGM0)
#define CHIP_REGS_TRGM1           ((CHIP_REGS_TRGM_T *)CHIP_MEMRGN_ADDR_TRGM1)
#define CHIP_REGS_TRGM2           ((CHIP_REGS_TRGM_T *)CHIP_MEMRGN_ADDR_TRGM2)
#define CHIP_REGS_TRGM3           ((CHIP_REGS_TRGM_T *)CHIP_MEMRGN_ADDR_TRGM3)


/*********** TRGM_FILTCFG - Filter configure register *************************/
#define CHIP_REGFLD_TRGM_FILTCFG_OUTINV                 (0x00000001UL << 16U) /* (RW) Filter output is inversion */
#define CHIP_REGFLD_TRGM_FILTCFG_MODE                   (0x00000007UL << 13U) /* (RW) Filter mode */
#define CHIP_REGFLD_TRGM_FILTCFG_SYNCEN                 (0x00000001UL << 12U) /* (RW) Synchronize the filter input signal to the TRGM clock */
#define CHIP_REGFLD_TRGM_FILTCFG_FILTLEN                (0x00000FFFUL <<  0U) /* (RW) Length of the filter in TRGM clock cycles. */
#define CHIP_REGMSK_TRGM_FILTCFG_RESERVED               (0xFFFE0000UL)


#define CHIP_REGFLDVAL_TRGM_FILTCFG_MODE_BYPASS         0 /* Bypass mode */
#define CHIP_REGFLDVAL_TRGM_FILTCFG_MODE_FILTER         4 /* Filter mode (no delay) */
#define CHIP_REGFLDVAL_TRGM_FILTCFG_MODE_DELAY          5 /* Delay filter */
#define CHIP_REGFLDVAL_TRGM_FILTCFG_MODE_PEAK           6 /* Peak filter */
#define CHIP_REGFLDVAL_TRGM_FILTCFG_MODE_NOTCH          7 /* Notch filter */


/*********** TRGM_TRGOCFG - Trigger manager output configure register *********/
#define CHIP_REGFLD_TRGM_TRGOCFG_OUTINV                 (0x00000001UL << 8U) /* (RW) TRGM output is inversion */
#define CHIP_REGFLD_TRGM_TRGOCFG_FEDG2PEN               (0x00000001UL << 7U) /* (RW) Convert the falling edge of the input signal into a output pulse */
#define CHIP_REGFLD_TRGM_TRGOCFG_REDG2PEN               (0x00000001UL << 6U) /* (RW) Convert the rising edge of the input signal into a output pulse */
#define CHIP_REGFLD_TRGM_TRGOCFG_TRIGOSEL               (0x0000003FUL << 0U) /* (RW) Selects one of the TRGM inputs as the TRGM output */
#define CHIP_REGMSK_TRGM_TRGOCFG_RESERVED               (0xFFFFFE00UL)


/*********** TRGM_DMACFG - DMA request configure register *********************/
#define CHIP_REGFLD_TRGM_DMACFG_DMASRCSEL               (0x0000001FUL << 0U) /* (RW) Select one of the DMA requests as the TRGM's DMA request */
#define CHIP_REGMSK_TRGM_DMACFG_RESERVED                (0xFFFFFFE0UL)

/*********** TRGM_GCR - General Control Register ******************************/
#define CHIP_REGFLD_TRGM_GCR_TRGOPEN(x)                 (0x00000001UL << (x)) /* (RW) Enable TRGM output x to IO */
#define CHIP_REGMSK_TRGM_GCR_RESERVED                   (0xFFFFF000UL)


#endif // REGS_TRGM_H
