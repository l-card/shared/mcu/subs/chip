#ifndef REGS_RDC_H
#define REGS_RDC_H

#include "chip_ioreg_defs.h"
#include "regs_aipstz.h"

#define CHIP_RDC_DOMAIN_CNT                 4   /* Количество доменов */
#define CHIP_RDC_MASTER_CNT                 40  /* Количество bus-master */
#define CHIP_RDC_PER_CNT                    114 /* Количество блоков периферии */
#define CHIP_RDC_MEMRGN_CNT                 77  /* Количество регионов памяти (распределение по областям - см. Table 3-4 */
#define CHIP_RDC_SEMA_CNT                   64  /* Количество семафоров в одном блоке RDC_SEMA */

/* Master Assignment Mapping (Table 3-2) */
#define CHIP_RDC_MDA_A53                    0
#define CHIP_RDC_MDA_M7                     1
#define CHIP_RDC_MDA_PCIE_CTRL1             2
#define CHIP_RDC_MDA_SDMA3_P                3
#define CHIP_RDC_MDA_SDMA3_B                4
#define CHIP_RDC_MDA_LCDIF                  5
#define CHIP_RDC_MDA_ISI                    6
#define CHIP_RDC_MDA_NPU                    7
#define CHIP_RDC_MDA_CORESIGHT              8
#define CHIP_RDC_MDA_DAP                    9
#define CHIP_RDC_MDA_CAAM                   10
#define CHIP_RDC_MDA_SDMA1_P                11
#define CHIP_RDC_MDA_SDMA1_B                12
#define CHIP_RDC_MDA_APB_HDMA               13
#define CHIP_RDC_MDA_RAW_NAND               14
#define CHIP_RDC_MDA_USDHC1                 15
#define CHIP_RDC_MDA_USDHC2                 15
#define CHIP_RDC_MDA_USDHC3                 17
#define CHIP_RDC_MDA_ADSP                   18
#define CHIP_RDC_MDA_USB1                   19
#define CHIP_RDC_MDA_USB2                   20
#define CHIP_RDC_MDA_ENET1_TX               22
#define CHIP_RDC_MDA_ENET1_RX               23
#define CHIP_RDC_MDA_SDMA2                  24 /* SDMA2(p), SDMA2(b), SDMA2 to SPBA2 */
#define CHIP_RDC_MDA_SDMA3_SPBA2            25
#define CHIP_RDC_MDA_SDMA1_SPBA1            26
#define CHIP_RDC_MDA_LCDIF2                 27
#define CHIP_RDC_MDA_HDMI_TX                28
#define CHIP_RDC_MDA_ENET2_QOS              29
#define CHIP_RDC_MDA_GPU_3D                 30
#define CHIP_RDC_MDA_GPU_2D                 31
#define CHIP_RDC_MDA_VPU_G1                 32
#define CHIP_RDC_MDA_VPU_G2                 33
#define CHIP_RDC_MDA_VPU_VC8000E            34
#define CHIP_RDC_MDA_AUDIO_EDMA             35
#define CHIP_RDC_MDA_ISP1                   36
#define CHIP_RDC_MDA_ISP2                   37
#define CHIP_RDC_MDA_DEWARP                 38
#define CHIP_RDC_MDA_GIC500                 39


/* RDC Peripheral Mapping (Table 3-3) */
#define CHIP_RDC_PER_ID_GPIO1               0
#define CHIP_RDC_PER_ID_GPIO2               1
#define CHIP_RDC_PER_ID_GPIO3               2
#define CHIP_RDC_PER_ID_GPIO4               3
#define CHIP_RDC_PER_ID_GPIO5               4
#define CHIP_RDC_PER_ID_MU_2_A              5 /* A53, Audio Processor */
#define CHIP_RDC_PER_ID_ANA_TSENSOR         6
#define CHIP_RDC_PER_ID_ANA_OSC             7
#define CHIP_RDC_PER_ID_WDOG1               8
#define CHIP_RDC_PER_ID_WDOG2               9
#define CHIP_RDC_PER_ID_WDOG3               10
#define CHIP_RDC_PER_ID_OCRAM               11
#define CHIP_RDC_PER_ID_OCRAM_S             12
#define CHIP_RDC_PER_ID_GPT1                13
#define CHIP_RDC_PER_ID_GPT2                14
#define CHIP_RDC_PER_ID_GPT3                15
#define CHIP_RDC_PER_ID_MU_2_B              16 /* A53, Audio Processor */
#define CHIP_RDC_PER_ID_MU_3_A              18 /* M7, Audio Processor */
#define CHIP_RDC_PER_ID_IOMUXC              19
#define CHIP_RDC_PER_ID_IOMUXC_GPR          20
#define CHIP_RDC_PER_ID_OCOTP_CTRL          21
#define CHIP_RDC_PER_ID_ANA_PLL             22
#define CHIP_RDC_PER_ID_SNVS_HP             23
#define CHIP_RDC_PER_ID_CCM                 24
#define CHIP_RDC_PER_ID_SRC                 25 /*?*/
#define CHIP_RDC_PER_ID_GPC                 26 /* General Power Controller */
#define CHIP_RDC_PER_ID_RDC_SEMA1           27
#define CHIP_RDC_PER_ID_RDC_SEMA2           28
#define CHIP_RDC_PER_ID_RDC                 29
#define CHIP_RDC_PER_ID_CSU                 30 /*?*/
#define CHIP_RDC_PER_ID_MU_3_B              31 /* M7, Audio Processor */
#define CHIP_RDC_PER_ID_ISI                 32 /*?*/
#define CHIP_RDC_PER_ID_ISP1                33 /*?*/
#define CHIP_RDC_PER_ID_ISP2                34
#define CHIP_RDC_PER_ID_IPS_DEWARP          35 /*?*/
#define CHIP_RDC_PER_ID_MIPI_CSI1           36
#define CHIP_RDC_PER_ID_HSIOMIX_BLK_CTL     37 /*?*/
#define CHIP_RDC_PER_ID_PWM1                38
#define CHIP_RDC_PER_ID_PWM2                39
#define CHIP_RDC_PER_ID_PWM3                40
#define CHIP_RDC_PER_ID_PWM4                41
#define CHIP_RDC_PER_ID_SYSCTR_RD           42 /* System Counter Read block */
#define CHIP_RDC_PER_ID_SYSCTR_CMP          43 /* System Counter Compare block */
#define CHIP_RDC_PER_ID_SYSCTR_CTRL         44 /* System Counter Control block*/
#define CHIP_RDC_PER_ID_I2C5                45
#define CHIP_RDC_PER_ID_GPT6                46
#define CHIP_RDC_PER_ID_GPT5                47
#define CHIP_RDC_PER_ID_GPT4                48
#define CHIP_RDC_PER_ID_MIPI_CSI2           49
#define CHIP_RDC_PER_ID_MIPI_DSI1           50
#define CHIP_RDC_PER_ID_MEDIAMIX_BLK_CTRL   51
#define CHIP_RDC_PER_ID_LCDIF1              52
#define CHIP_RDC_PER_ID_EDMA_MNGMT_PAGE     53
#define CHIP_RDC_PER_ID_EDMA_CH0_15         54
#define CHIP_RDC_PER_ID_EDMA_CH16_31        55
#define CHIP_RDC_PER_ID_TZASC               56 /*?*/
#define CHIP_RDC_PER_ID_I2C6                57
#define CHIP_RDC_PER_ID_CAAM                58 /*?*/
#define CHIP_RDC_PER_ID_LCDIF2              59
#define CHIP_RDC_PER_ID_PERFMON1            60 /*?*/
#define CHIP_RDC_PER_ID_PERFMON2            61 /*?*/
#define CHIP_RDC_PER_ID_NOC_BLK_CTL         62 /*?*/
#define CHIP_RDC_PER_ID_QOSC                63
#define CHIP_RDC_PER_ID_LVDS1               64
#define CHIP_RDC_PER_ID_LVDS2               65
#define CHIP_RDC_PER_ID_I2C1                66
#define CHIP_RDC_PER_ID_I2C2                67
#define CHIP_RDC_PER_ID_I2C3                68
#define CHIP_RDC_PER_ID_I2C4                69
#define CHIP_RDC_PER_ID_UART4               70
#define CHIP_RDC_PER_ID_HDMI_TX             71
#define CHIP_RDC_PER_ID_IRQ_STEER           72 /* Audio Processor */
#define CHIP_RDC_PER_ID_SDMA2               73
#define CHIP_RDC_PER_ID_MU_1_A              74 /* A53, M7 */
#define CHIP_RDC_PER_ID_MU_1_B              75 /* A53, M7 */
#define CHIP_RDC_PER_ID_SEMA_HS             76
#define CHIP_RDC_PER_ID_SAI1                78
#define CHIP_RDC_PER_ID_SAI2                79
#define CHIP_RDC_PER_ID_SAI3                80
#define CHIP_RDC_PER_ID_CAN_FD1             81
#define CHIP_RDC_PER_ID_SAI5                82
#define CHIP_RDC_PER_ID_SAI6                83
#define CHIP_RDC_PER_ID_USDHC1              84
#define CHIP_RDC_PER_ID_USDHC2              85
#define CHIP_RDC_PER_ID_USDHC3              86
#define CHIP_RDC_PER_ID_PCIE_PHY1           87
#define CHIP_RDC_PER_ID_HDMI_TX_AUDLNK_MSTR 88
#define CHIP_RDC_PER_ID_CAN_FD2             89
#define CHIP_RDC_PER_ID_SPBA2               90 /*?*/
#define CHIP_RDC_PER_ID_QSPI                91
#define CHIP_RDC_PER_ID_AUDIO_BLK_CTRL      92
#define CHIP_RDC_PER_ID_SDMA1               93
#define CHIP_RDC_PER_ID_ENET1               94
#define CHIP_RDC_PER_ID_ENET2_TSN           95
#define CHIP_RDC_PER_ID_ASRC                97 /*?*/
#define CHIP_RDC_PER_ID_ECSPI1              98
#define CHIP_RDC_PER_ID_ECSPI2              99
#define CHIP_RDC_PER_ID_ECSPI3              100
#define CHIP_RDC_PER_ID_SAI7                101
#define CHIP_RDC_PER_ID_UART1               102
#define CHIP_RDC_PER_ID_UART3               104
#define CHIP_RDC_PER_ID_UART2               105
#define CHIP_RDC_PER_ID_PDM_MICFIL          106
#define CHIP_RDC_PER_ID_AUDIO_XCVR_RX       107 /* eARC */
#define CHIP_RDC_PER_ID_SDMA3               109
#define CHIP_RDC_PER_ID_SPBA1               111

#define CHIP_RDC_PER_ID_SEMA_NUM(p)         ((p)/CHIP_RDC_SEMA_CNT) /* номер семафора по PER_ID */
#define CHIP_RDC_PER_ID_SEMA_REGNUM(p)      ((p)%CHIP_RDC_SEMA_CNT) /* номер регистра соответствующего семафора для PER_ID */


/** @todo - Table 3-4.Memory Region Mapping  */

typedef struct {
    __IO uint32_t MRSA;             /* Memory Region Start Address */
    __IO uint32_t MREA;             /* Memory Region End Address */
    __IO uint32_t MRC;              /* Memory Region Control  */
    __IO uint32_t MRVS;             /* Memory Region Violation Status  */
} CHIP_REGS_RDC_REGION;


typedef struct {
    __I  uint32_t VIR;              /* Version Information,                     Address offset: 0x0000 */
    uint32_t RESERVED0[8];
    __IO uint32_t STAT;             /* Status,                                  Address offset: 0x0024 */
    __IO uint32_t INTCTRL;          /* Interrupt and Control,                   Address offset: 0x0028 */
    __IO uint32_t INTSTAT;          /* Interrupt Status,                        Address offset: 0x002C */
    uint32_t RESERVED1[116];
    __IO uint32_t MDA[CHIP_RDC_MASTER_CNT];  /* Master Domain Assignment,                Address offset: 0x0200 - 0x029C */
    uint32_t RESERVED2[88];
    __IO uint32_t PDAP[CHIP_RDC_PER_CNT];    /* Peripheral Domain Access Permissions,    Address offset: 0x0400 - 0x05BC */
    uint32_t RESERVED3[142];
    CHIP_REGS_RDC_REGION MEMRGN[CHIP_RDC_MEMRGN_CNT]; /* Memory Regions,                 Address offset: 0x0800 - 0x0CCC */
} CHIP_REGS_RDC_T;


typedef struct {
    __IO uint8_t GATE[CHIP_RDC_SEMA_CNT]; /* Gate Register,                           Address offset: 0x0000 - 0x003F */
    uint16_t RESERVED0;
    __IO uint16_t RSTGT;                       /* Reset Gate Register,                     Address offset: 0x0042 */
} CHIP_REGS_RDC_SEMA_T;


#define CHIP_REGS_RDC                   ((CHIP_REGS_RDC_T *) CHIP_MEMRGN_ADDR_PERIPH_RDC)
#define CHIP_REGS_RDC_SEMA1             ((CHIP_REGS_RDC_SEMA_T *) CHIP_MEMRGN_ADDR_PERIPH_RDC_SEMA1)
#define CHIP_REGS_RDC_SEMA2             ((CHIP_REGS_RDC_SEMA_T *) CHIP_MEMRGN_ADDR_PERIPH_RDC_SEMA2)
#define CHIP_REGS_RDC_SEMA(i)           ((CHIP_REGS_RDC_SEMA_T *) (CHIP_MEMRGN_ADDR_PERIPH_RDC_SEMA1 + (i)*(CHIP_MEMRGN_ADDR_PERIPH_RDC_SEMA2-CHIP_MEMRGN_ADDR_PERIPH_RDC_SEMA1)))
#define CHIP_REGS_RDC_SEMA_PERID(p)     CHIP_REGS_RDC_SEMA(CHIP_RDC_PER_ID_SEMA_NUM(p))


/* Макросы для управления семафороми для доступа к периферии */
#define CHIP_RDC_SEMA_PER_GATEREG(p)    CHIP_REGS_RDC_SEMA_PERID(p)->GATE[CHIP_RDC_PER_ID_SEMA_REGNUM(p)]
#define CHIP_RDC_SEMA_LOCK_MST(p, mst)  CHIP_RDC_SEMA_PER_GATEREG(p) = LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_GATE_GTFSM, (mst+1))
#define CHIP_RDC_SEMA_LOCK(p)           CHIP_RDC_SEMA_LOCK_MST(p, CHIP_AIPSTZ_CURRENT_MASTER)
#define CHIP_RDC_SEMA_UNLOCK(p, mst)    CHIP_RDC_SEMA_PER_GATEREG(p) = 0

/** @todo exception on read/write.... */
#define CHIP_RDC_SEMA_RST(p) \
    do { \
        CHIP_REGS_RDC_SEMA_PERID(p)->RSTGT = LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGDP, 0xE2); \
        CHIP_REGS_RDC_SEMA_PERID(p)->RSTGT = LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGDP, 0xD1) | \
                                             LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGTN, p); \
        } while(0)

#define CHIP_RDC_SEMA_RST_ALL \
    do { \
        CHIP_REGS_RDC_SEMA(0)->RSTGT = LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGDP, 0xE2); \
        CHIP_REGS_RDC_SEMA(0)->RSTGT = LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGDP, 0xD1) | \
                                       LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGTN, 64) ; \
        CHIP_REGS_RDC_SEMA(1)->RSTGT = LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGDP, 0xE2); \
        CHIP_REGS_RDC_SEMA(1)->RSTGT = LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGDP, 0xD1) | \
                                       LBITFIELD_SET(CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGTN, 64) ; \
    while (0)



/*********** Version Information (RDC_VIR) ************************************/
#define CHIP_REGFLD_RDC_VIR_NDID                (0x0000000FUL <<  0U)  /* (RO) Number of Domains  */
#define CHIP_REGFLD_RDC_VIR_NMSTR               (0x000000FFUL <<  4U)  /* (RO) Number of Masters */
#define CHIP_REGFLD_RDC_VIR_NPER                (0x000000FFUL << 12U)  /* (RO) Number of Peripherals */
#define CHIP_REGFLD_RDC_VIR_NRGN                (0x000000FFUL << 20U)  /* (RO) Number of Memory Regions  */

/*********** Status (RDC_STAT) ************************************************/
#define CHIP_REGFLD_RDC_STAT_DID                (0x0000000FUL <<  0U)  /* (RW) Domain ID. The Domain ID of the core or bus master that is reading this */
#define CHIP_REGFLD_RDC_STAT_PDS                (0x00000001UL <<  8U)  /* (RW) Power Domain Status  (if Power Down memory regions are powered) */

/*********** Interrupt and Control (RDC_INTCTRL) ******************************/
#define CHIP_REGFLD_RDC_INTCTRL_RCI_EN          (0x00000001UL <<  0U)  /* (RW) Restoration Complete Interrupt Enable */

/*********** Interrupt Status (RDC_INTSTAT) ***********************************/
#define CHIP_REGFLD_RDC_INTSTAT_INT             (0x00000001UL <<  0U)  /* (RW1C) Interrupt Status */

/*********** Master Domain Assignment (RDC_MDAn) ******************************/
#define CHIP_REGFLD_RDC_MDA_DID                 (0x00000003UL <<  0U)  /* (RW) Domain ID - domain to which the Master is assigned */
#define CHIP_REGFLD_RDC_MDA_LCK                 (0x00000001UL << 31U)  /* (RW) Assignment Lock */

/*********** Peripheral Domain Access Permissions (RDC_PDAPn) *****************/
#define CHIP_REGFLD_RDC_PDAP_DW(d)              (0x00000001UL << (0U + 2*(d)))  /* (RW) Domain d Write Access (d = 0..3)  */
#define CHIP_REGFLD_RDC_PDAP_DR(d)              (0x00000001UL << (1U + 2*(d)))  /* (RW) Domain d Read Access (d = 0..3) */
#define CHIP_REGFLD_RDC_PDAP_SREQ               (0x00000001UL << 30U)           /* (RW) Semaphore Required */
#define CHIP_REGFLD_RDC_PDAP_LCK                (0x00000001UL << 31U)           /* (RW) Peripheral Permissions Lock */

/*********** Memory Region Control (RDC_MRCn) *********************************/
#define CHIP_REGFLD_RDC_MRC_DW(d)               (0x00000001UL << (0U + 2*(d)))  /* (RW) Domain d Write Access (d = 0..3)  */
#define CHIP_REGFLD_RDC_MRC_DR(d)               (0x00000001UL << (1U + 2*(d)))  /* (RW) Domain d Read Access (d = 0..3) */
#define CHIP_REGFLD_RDC_MRC_ENA                 (0x00000001UL << 30U)           /* (RW) Region Enable. Activates the memory region.  */
#define CHIP_REGFLD_RDC_MRC_LCK                 (0x00000001UL << 31U)           /* (RW) Peripheral Permissions Lock */

/*********** Memory Region Violation Status (RDC_MRVSn) ***********************/
#define CHIP_REGFLD_RDC_MRVS_VDID               (0x00000003UL <<  0U) /* (RO)   Violating Domain ID */
#define CHIP_REGFLD_RDC_MRVS_AD                 (0x00000001UL <<  4U) /* (RW1C) Access Denied */
#define CHIP_REGFLD_RDC_MRVS_VADR               (0x07FFFFFFUL <<  5U) /* (RO)   Violating Address */


/*********** Semaphore Gate Register (RDC_SEMA_GATEx) *************************/
#define CHIP_REGFLD_RDC_SEMA_GATE_LDOM          (0x00000003UL <<  4U) /* (RO) Indicate which domain had currently locked the gate. */
#define CHIP_REGFLD_RDC_SEMA_GATE_GTFSM         (0x0000000FUL <<  0U) /* (RW) Gate Finite State Machine (0 - unlocked, 1-15 locked by master 0-14). */

/*********** Semaphore Reset Gate Register (RDC_SEMA_RSTGT) *******************/
#define CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGTN       (0x000000FFUL <<  8U) /* (RW) Reset Gate Number */
#define CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGDP       (0x000000FFUL <<  0U) /* (WO) Reset Gate Data Pattern (wr seq: 0xE2, 0x1D) */
#define CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGSM       (0x00000003UL <<  4U) /* (RO) Reset Gate Finite State Machine */
#define CHIP_REGFLD_RDC_SEMA_RSTGT_RSTGMS       (0x0000000FUL <<  0U) /* (RO) Reset Gate Bus Master */



#endif // REGS_RDC_H
