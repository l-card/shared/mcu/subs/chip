#ifndef CHIP_CORTEXM_ISR_H
#define CHIP_CORTEXM_ISR_H

#include <stdint.h>
#include "chip_config.h"

/* Определение размера стека. Определяется с помощью настройки CHIP_CFG_STACK_WORDS_LEN,
 * либо используется значение по умолчнию  */
#ifdef CHIP_CFG_STACK_WORDS_LEN
    #define CHIP_STACK_WORDS_LEN CHIP_CFG_STACK_WORDS_LEN
#else
    #define CHIP_STACK_WORDS_LEN 512
#endif

#define CHIP_STACK_TYPE uint32_t
extern CHIP_STACK_TYPE g_cpu_stack[CHIP_STACK_WORDS_LEN];

typedef void (*t_chip_irq_handler)(void);
extern const t_chip_irq_handler g_isr_table[];
#define CHIP_ISR_TABLE()  __attribute__ ((section(".isr_vector"))) const t_chip_irq_handler g_isr_table[]

extern void dummy_handler(void);
extern void Reset_Handler(void);

#define CHIP_ISR_TABLE_ENTRY_STACKPTR ((t_chip_irq_handler)(g_cpu_stack + CHIP_STACK_WORDS_LEN))
#define CHIP_ISR_TABLE_ENTRY_RESETPTR (Reset_Handler)

#define CHIP_ISR_DECLARE(handler_name) \
    void handler_name(void) __attribute__ ((__weak__, __alias__("dummy_handler")))

#define CHIP_ISR_DUMMY_DECLARE()  void dummy_handler(void) { for (;;); }

#endif // CHIP_CORTEXM_ISR_H
