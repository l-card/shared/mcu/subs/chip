#ifndef REGS_SPI_H
#define REGS_SPI_H

#define CHIP_REG_SPCR       _MMIO_BYTE(0x4C) /* SPI Control Register */
#define CHIP_REG_SPSR       _MMIO_BYTE(0x4D) /* SPI Status Register */
#define CHIP_REG_SPDR       _MMIO_BYTE(0x4E) /* SPI Data Register */
#ifdef CHIP_LGT8FX8P
#define CHIP_REG_SDFR       _MMIO_BYTE(0x39) /* SPI Buffer Register */
#endif


/* ---------------- SPCR – SPI Control Register ------------------------------*/
#define CHIP_REGFLD_SPCR_SPIE               (0x01U << 7) /* (RW) SPI interrupt enable */
#define CHIP_REGFLD_SPCR_SPE                (0x01U << 6) /* (RW) SPI enable */
#define CHIP_REGFLD_SPCR_DORD               (0x01U << 5) /* (RW) Data order control */
#define CHIP_REGFLD_SPCR_MSTR               (0x01U << 4) /* (RW) Master/slave selection */
#define CHIP_REGFLD_SPCR_CPOL               (0x01U << 3) /* (RW) Clock polarity */
#define CHIP_REGFLD_SPCR_CPHA               (0x01U << 2) /* (RW) Clock phase */
#define CHIP_REGFLD_SPCR_SPR                (0x03U << 0) /* (RW) Clock rate select */

#define CHIP_REGFLDVAL_SPCR_DORD_MSB_FIRST  0
#define CHIP_REGFLDVAL_SPCR_DORD_LSB_FIRST  1
/* Значения деления системного клока без учета удвоения скорости с SPI2X */
#define CHIP_REGFLDVAL_SPCR_SPR_DIV4        0
#define CHIP_REGFLDVAL_SPCR_SPR_DIV16       1
#define CHIP_REGFLDVAL_SPCR_SPR_DIV64       2
#define CHIP_REGFLDVAL_SPCR_SPR_DIV128      3

/* ---------------- SPSR – SPI Status Register -------------------------------*/
#define CHIP_REGFLD_SPSR_SPIF               (0x01U << 7) /* (RO) SPI interrupt flag */
#define CHIP_REGFLD_SPSR_WCOL               (0x01U << 6) /* (RO) Write collision flag */
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_SPSR_DUAL               (0x01U << 2) /* (RW) Two-wire mode */
#endif
#define CHIP_REGFLD_SPSR_SPI2X              (0x01U << 0) /* (RW) SPI speed control */


/* ---------------- SPFR – SPI Buffer Register -------------------------------*/
#ifdef CHIP_LGT8FX8P
#define CHIP_REGFLD_SPFR_RDFULL             (0x01U << 7) /* (RO) Receive buffer full flag */
#define CHIP_REGFLD_SPFR_RDEMPT             (0x01U << 6) /* (RW) Receive buffer empty flag */
#define CHIP_REGFLD_SPFR_RDPTR              (0x03U << 4) /* (RO) Receive buffer address */
#define CHIP_REGFLD_SPFR_WRFULL             (0x01U << 3) /* (RO) Transmit buffer full flag */
#define CHIP_REGFLD_SPFR_WREMPT             (0x01U << 2) /* (RW) Transmit buffer empty flag */
#define CHIP_REGFLD_SPFR_WRPTR              (0x03U << 0) /* (RO) Transmit buffer address */
#endif


#endif // REGS_SPI_H
