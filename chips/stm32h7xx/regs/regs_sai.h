#ifndef CHIP_STM32H7XX_REGS_SAI_H_
#define CHIP_STM32H7XX_REGS_SAI_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR1;      /* SAI block x configuration register 1,     Address offset: 0x04 */
    __IO uint32_t CR2;      /* SAI block x configuration register 2,     Address offset: 0x08 */
    __IO uint32_t FRCR;     /* SAI block x frame configuration register, Address offset: 0x0C */
    __IO uint32_t SLOTR;    /* SAI block x slot register,                Address offset: 0x10 */
    __IO uint32_t IMR;      /* SAI block x interrupt mask register,      Address offset: 0x14 */
    __IO uint32_t SR;       /* SAI block x status register,              Address offset: 0x18 */
    __IO uint32_t CLRFR;    /* SAI block x clear flag register,          Address offset: 0x1C */
    __IO uint32_t DR;       /* SAI block x data register,                Address offset: 0x20 */
} CHIP_REGS_SAI_BLOCK_T;

typedef struct {
    __IO uint32_t GCR;           /* SAI global configuration register, Address offset: 0x00 */
    union {
        CHIP_REGS_SAI_BLOCK_T BLOCK[2];
        struct {
            CHIP_REGS_SAI_BLOCK_T A;
            CHIP_REGS_SAI_BLOCK_T B;
        };
    };
    __IO uint32_t PDMCR;         /* SAI PDM control register,          Address offset: 0x44 */
    __IO uint32_t PDMDLY;        /* SAI PDM delay register,            Address offset: 0x48 */
} CHIP_REGS_SAI_T;


#define CHIP_REGS_SAI1         ((CHIP_REGS_SAI_T *) CHIP_MEMRGN_ADDR_PERIPH_SAI1)
#if CHIP_DEV_SUPPORT_SAI2_3
#define CHIP_REGS_SAI2         ((CHIP_REGS_SAI_T *) CHIP_MEMRGN_ADDR_PERIPH_SAI2)
#define CHIP_REGS_SAI3         ((CHIP_REGS_SAI_T *) CHIP_MEMRGN_ADDR_PERIPH_SAI3)
#endif
#define CHIP_REGS_SAI4         ((CHIP_REGS_SAI_T *) CHIP_MEMRGN_ADDR_PERIPH_SAI4)


#define CHIP_SAI_FIFO_WORD_SIZE                 8 /* размер FIFO в количестве слов */


/*********** SAI global configuration register (SAI_GCR)  *********************/
#define CHIP_REGFLD_SAI_GCR_SYNCIN              (0x00000003UL <<  0U) /* Synchronization inputs */
#define CHIP_REGFLD_SAI_GCR_SYNCOUT             (0x00000003UL <<  4U) /* Synchronization outputs */
#define CHIP_REGMSK_SAI_GCR_RESERVED            (0xFFFFFFCCUL)

/*********** SAI configuration register 1 (SAI_xCR1) **************************/
#define CHIP_REGFLD_SAI_CR1_MODE                (0x00000003UL <<  0U) /* SAIx audio block mode */
#define CHIP_REGFLD_SAI_CR1_PRTCFG              (0x00000003UL <<  2U) /* Protocol configuration */
#define CHIP_REGFLD_SAI_CR1_DS                  (0x00000007UL <<  5U) /* Data size */
#define CHIP_REGFLD_SAI_CR1_LSBFIRST            (0x00000001UL <<  8U) /* Least significant bit first */
#define CHIP_REGFLD_SAI_CR1_CKSTR               (0x00000001UL <<  9U) /* Clock strobing edge */
#define CHIP_REGFLD_SAI_CR1_SYNCEN              (0x00000003UL << 10U) /* Synchronization enable */
#define CHIP_REGFLD_SAI_CR1_MONO                (0x00000001UL << 12U) /* Mono mode */
#define CHIP_REGFLD_SAI_CR1_OUTDRIV             (0x00000001UL << 13U) /* Output Drive */
#define CHIP_REGFLD_SAI_CR1_SAIEN               (0x00000001UL << 16U) /* Audio Block enable */
#define CHIP_REGFLD_SAI_CR1_DMAEN               (0x00000001UL << 17U) /* DMA enable */
#define CHIP_REGFLD_SAI_CR1_NODIV               (0x00000001UL << 19U) /* No divider */
#define CHIP_REGFLD_SAI_CR1_MCKDIV              (0x0000003FUL << 20U) /* Master clock divider */
#define CHIP_REGFLD_SAI_CR1_OSR                 (0x00000001UL << 26U) /* Oversampling ratio for master clock (256 or 512) */
#define CHIP_REGFLD_SAI_CR1_MCKEN               (0x00000001UL << 27U) /* Master clock generation enable */
#define CHIP_REGMSK_SAI_CR1_RESERVED            (0xF004C010UL)

#define CHIP_REGFLDVAL_SAI_CR1_MODE_MASTER_TX   0
#define CHIP_REGFLDVAL_SAI_CR1_MODE_MASTER_RX   1
#define CHIP_REGFLDVAL_SAI_CR1_MODE_SLAVE_TX    2
#define CHIP_REGFLDVAL_SAI_CR1_MODE_SLAVE_RX    3

#define CHIP_REGFLDVAL_SAI_CR1_PRTCFG_FREE      0
#define CHIP_REGFLDVAL_SAI_CR1_PRTCFG_SPDIF     1
#define CHIP_REGFLDVAL_SAI_CR1_PRTCFG_AC97      2

#define CHIP_REGFLDVAL_SAI_CR1_DS_8             2
#define CHIP_REGFLDVAL_SAI_CR1_DS_10            3
#define CHIP_REGFLDVAL_SAI_CR1_DS_16            4
#define CHIP_REGFLDVAL_SAI_CR1_DS_20            5
#define CHIP_REGFLDVAL_SAI_CR1_DS_24            6
#define CHIP_REGFLDVAL_SAI_CR1_DS_32            7

#define CHIP_REGFLDVAL_SAI_CR1_CKSTR_TXRISE     0 /* Signals generated on SCK rising edge, sampled on the SCK falling edge */
#define CHIP_REGFLDVAL_SAI_CR1_CKSTR_TXFALL     1 /* Signals generated on SCK falling edge, sampled on the SCK rising edge */

#define CHIP_REGFLDVAL_SAI_CR1_SYNCEN_ASYNC     0 /* audio subblock in asynchronous mode */
#define CHIP_REGFLDVAL_SAI_CR1_SYNCEN_INT       1 /* audio subblock is synchronous with the other internal audio subblock */
#define CHIP_REGFLDVAL_SAI_CR1_SYNCEN_EXT       2 /* audio subblock is synchronous with an external SAI embedded peripheral */

#define CHIP_REGFLDVAL_SAI_CR1_NODIV_OSR        0 /* F_mclk to F_fs depends on OSR (256 or 512)*/
#define CHIP_REGFLDVAL_SAI_CR1_NODIV_FRL        0 /* F_mclk to F_fs depends on FRL[7:0] */

#define CHIP_REGFLDVAL_SAI_CR1_OSR_256          0 /* F_mclk = 256 * F_fs */
#define CHIP_REGFLDVAL_SAI_CR1_OSR_512          1 /* F_mclk = 512 * F_fs */


/*********** SAI configuration register 2 (SAI_xCR2) **************************/
#define CHIP_REGFLD_SAI_CR2_FTH                 (0x00000007UL <<  0U) /* FIFO threshold */
#define CHIP_REGFLD_SAI_CR2_FFLUSH              (0x00000001UL <<  3U) /* FIFO flush */
#define CHIP_REGFLD_SAI_CR2_TRIS                (0x00000001UL <<  4U) /* Tristate management on data line */
#define CHIP_REGFLD_SAI_CR2_MUTE                (0x00000001UL <<  5U) /* Mute mode */
#define CHIP_REGFLD_SAI_CR2_MUTEVAL             (0x00000001UL <<  6U) /* Mute value */
#define CHIP_REGFLD_SAI_CR2_MUTECNT             (0x0000003FUL <<  7U) /* MUTE counter */
#define CHIP_REGFLD_SAI_CR2_CPL                 (0x00000001UL << 13U) /* Complement bit */
#define CHIP_REGFLD_SAI_CR2_COMP                (0x00000003UL << 14U) /* Companding mode */
#define CHIP_REGMSK_SAI_CR2_RESERVED            (0xFFFF0000UL)

#define CHIP_REGFLDVAL_SAI_CR2_FTH_EMPTY        0
#define CHIP_REGFLDVAL_SAI_CR2_FTH_1_4          1
#define CHIP_REGFLDVAL_SAI_CR2_FTH_1_2          2
#define CHIP_REGFLDVAL_SAI_CR2_FTH_3_4          3
#define CHIP_REGFLDVAL_SAI_CR2_FTH_FULL         4

#define CHIP_REGFLDVAL_SAI_CR2_COMP_NO          0
#define CHIP_REGFLDVAL_SAI_CR2_COMP_U_LAW       2
#define CHIP_REGFLDVAL_SAI_CR2_COMP_A_LAW       3


/*********** SAI frame configuration register (SAI_xFRCR) *********************/
#define CHIP_REGFLD_SAI_FRCR_FRL                (0x000000FFUL <<  0U) /*FRL[7:0](FRame Length)  */
#define CHIP_REGFLD_SAI_FRCR_FSALL              (0x0000007FUL <<  8U) /* Frame synchronization active level length  */
#define CHIP_REGFLD_SAI_FRCR_FSDEF              (0x00000001UL << 16U) /* Frame synchronization definition */
#define CHIP_REGFLD_SAI_FRCR_FSPOL              (0x00000001UL << 17U) /* Frame synchronization polarity */
#define CHIP_REGFLD_SAI_FRCR_FSOFF              (0x00000001UL << 18U) /* Frame synchronization offset */
#define CHIP_REGMSK_SAI_FRCR_RESERVED           (0xFFF88000UL)


/*********** SAI slot register (SAI_xSLOTR) ***********************************/
#define CHIP_REGFLD_SAI_SLOTR_FBOFF             (0x0000001FUL <<  0U) /* First bit offset */
#define CHIP_REGFLD_SAI_SLOTR_SLOTSZ            (0x00000003UL <<  6U) /* Slot size */
#define CHIP_REGFLD_SAI_SLOTR_NBSLOT            (0x0000000FUL <<  8U) /* Number of slots in an audio frame */
#define CHIP_REGFLD_SAI_SLOTR_SLOTEN            (0x0000FFFFUL << 16U) /* Slot enable */
#define CHIP_REGMSK_SAI_SLOTR_RESERVED          (0x0000F020UL)

#define CHIP_REGFLDVAL_SAI_SLOTR_SLOTSZ_DS      0 /* slot size = data size */
#define CHIP_REGFLDVAL_SAI_SLOTR_SLOTSZ_16      1
#define CHIP_REGFLDVAL_SAI_SLOTR_SLOTSZ_32      2

/*********** SAI interrupt registers (SAI_xIM / SAI_xSR / SAI_xCLRFR) *********/
#define CHIP_REGFLD_SAI_INT_OVRUDR              (0x00000001UL <<  0U) /* Overrun underrun interrupt  */
#define CHIP_REGFLD_SAI_INT_MUTEDET             (0x00000001UL <<  1U) /* Mute detection interrupt  */
#define CHIP_REGFLD_SAI_INT_WCKCFG              (0x00000001UL <<  2U) /* Wrong Clock Configuration interrupt */
#define CHIP_REGFLD_SAI_INT_FREQ                (0x00000001UL <<  3U) /* FIFO request interrupt */
#define CHIP_REGFLD_SAI_INT_CNRDY               (0x00000001UL <<  4U) /* Codec not ready interrupt */
#define CHIP_REGFLD_SAI_INT_AFSDET              (0x00000001UL <<  5U) /* Anticipated frame synchronization detection interrupt */
#define CHIP_REGFLD_SAI_INT_LFSDET              (0x00000001UL <<  6U) /* Late frame synchronization detection interrupt */

#define CHIP_REGFLD_SAI_SR_FLVL                 (0x00000007UL << 16U) /* FIFO level threshold */
#define CHIP_REGMSK_SAI_SR_RESERVED             (0xFFF8FF80UL)

#define CHIP_REGFLDVAL_SAI_SR_FLVL_EMPTY        0
#define CHIP_REGFLDVAL_SAI_SR_FLVL_LESS_1_4     1
#define CHIP_REGFLDVAL_SAI_SR_FLVL_LESS_1_2     2
#define CHIP_REGFLDVAL_SAI_SR_FLVL_LESS_3_4     3
#define CHIP_REGFLDVAL_SAI_SR_FLVL_LESS_FULL    4
#define CHIP_REGFLDVAL_SAI_SR_FLVL_FULL         5

/*********** SAI data register (SAI_xDR) **************************************/
#define CHIP_REGFLD_SAI_DR_DATA                 (0xFFFFFFFFUL <<  0U)

/*********** SAI PDM control register (SAI_PDMCR) *****************************/
#define CHIP_REGFLD_SAI_PDMCR_PDMEN             (0x00000001UL <<  0U) /* PDM Enable */
#define CHIP_REGFLD_SAI_PDMCR_MICNBR            (0x00000003UL <<  4U) /* Number of microphones */
#define CHIP_REGFLD_SAI_PDMCR_CKEN1             (0x00000001UL <<  8U) /* Clock enable of bitstream clock number 1 */
#define CHIP_REGFLD_SAI_PDMCR_CKEN2             (0x00000001UL <<  9U) /* Clock enable of bitstream clock number 1 */
#define CHIP_REGFLD_SAI_PDMCR_CKEN3             (0x00000001UL << 10U) /* Clock enable of bitstream clock number 1 */
#define CHIP_REGFLD_SAI_PDMCR_CKEN4             (0x00000001UL << 11U) /* Clock enable of bitstream clock number 1 */
#define CHIP_REGFLD_SAI_PDMCR_CKEN(i)           (0x00000001UL << (8U + i))
#define CHIP_REGMSK_SAI_PDMCR_RESERVED          (0xFFFFF0CEUL)

#define CHIP_REGFLDVAL_SAI_PDMCR_MICNBR_2       0
#define CHIP_REGFLDVAL_SAI_PDMCR_MICNBR_4       1
#define CHIP_REGFLDVAL_SAI_PDMCR_MICNBR_6       2
#define CHIP_REGFLDVAL_SAI_PDMCR_MICNBR_8       3

/*********** SAI PDM delay register (SAI_PDMDLY) ******************************/
#define CHIP_REGFLD_SAI_PDMDLY_DLYM1L           (0x00000007UL <<  0U) /* Delay line adjust for left microphone of pair 1 */
#define CHIP_REGFLD_SAI_PDMDLY_DLYM1R           (0x00000007UL <<  4U) /* Delay line adjust for right microphone of pair 1 */
#define CHIP_REGFLD_SAI_PDMDLY_DLYM2L           (0x00000007UL <<  8U) /* Delay line adjust for left microphone of pair 2 */
#define CHIP_REGFLD_SAI_PDMDLY_DLYM2R           (0x00000007UL << 12U) /* Delay line adjust for right microphone of pair 2 */
#define CHIP_REGFLD_SAI_PDMDLY_DLYM3L           (0x00000007UL << 16U) /* Delay line adjust for left microphone of pair 3 */
#define CHIP_REGFLD_SAI_PDMDLY_DLYM3R           (0x00000007UL << 20U) /* Delay line adjust for right microphone of pair 3 */
#define CHIP_REGFLD_SAI_PDMDLY_DLYM4L           (0x00000007UL << 24U) /* Delay line adjust for left microphone of pair 4 */
#define CHIP_REGFLD_SAI_PDMDLY_DLYM4R           (0x00000007UL << 28U) /* Delay line adjust for right microphone of pair 4 */
#define CHIP_REGMSK_SAI_PDMDLY_RESERVED         (0x88888888UL)

#endif // CHIP_STM32H7XX_REGS_SAI_H_
