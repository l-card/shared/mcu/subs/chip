#ifndef CHIP_STM32F10X_REGS_AFIO_H
#define CHIP_STM32F10X_REGS_AFIO_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

/* Alternate Function I/O */
typedef struct {
    __IO uint32_t EVCR;       /* Event control register,                                    Address offset: 0x00  */
    __IO uint32_t MAPR;       /* AF remap and debug I/O configuration register,             Address offset: 0x04  */
    union {
        struct {
            __IO uint32_t EXTICR1; /* Alternate external interrupt configuration register 1, Address offset: 0x08  */
            __IO uint32_t EXTICR2; /* Alternate external interrupt configuration register 2, Address offset: 0x0C  */
            __IO uint32_t EXTICR3; /* Alternate external interrupt configuration register 3, Address offset: 0x10  */
            __IO uint32_t EXTICR4; /* Alternate external interrupt configuration register 4, Address offset: 0x14  */
        };
        __IO uint32_t EXTICR[4];
    };
    uint32_t RESERVED;           /*                                                          Address offset: 0x18  */
    union {
        struct {
            __IO uint32_t MAPR2; /* AF remap and debug I/O configuration register 2,         Address offset: 0x1C  */
#if defined CHIP_DEV_V84XXX
            __IO uint32_t MAPR3; /* AF remap and debug I/O configuration register 3,         Address offset: 0x20  */
            __IO uint32_t MAPR4; /* AF remap and debug I/O configuration register 4,         Address offset: 0x24  */
            __IO uint32_t MAPR5; /* AF remap and debug I/O configuration register 5,         Address offset: 0x28  */
            __IO uint32_t MAPR6; /* AF remap and debug I/O configuration register 6,         Address offset: 0x2C  */
            __IO uint32_t MAPR7; /* AF remap and debug I/O configuration register 7,         Address offset: 0x30  */
            __IO uint32_t MAPR8; /* AF remap and debug I/O configuration register 8,         Address offset: 0x34  */
#endif
        };
    };
} CHIP_REGS_AFIO_T;


#define CHIP_REGS_AFIO                              ((CHIP_REGS_AFIO_T *) CHIP_MEMRGN_ADDR_PERIPH_AFIO)

/*********** Event control register (AFIO_EVCR) *******************************/
#define CHIP_REGFLD_AFIO_EVCR_EVOE                (0x00000001UL <<  7U) /* (RW) Event output enable */
#define CHIP_REGFLD_AFIO_EVCR_PORT                (0x00000007UL <<  4U) /* (RW) Event output port selection */
#define CHIP_REGFLD_AFIO_EVCR_PIN                 (0x0000000FUL <<  0U) /* (RW) Event output pin selection */
#define CHIP_REGMSK_AFIO_EVCR_RESERVED            (0xFFFFFF00UL)

/*********** AF remap and debug I/O configuration register (AFIO_MAPR) ********/
#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_REGFLD_AFIO_MAPR_SPI1_H                (0x00000001UL << 31U) /* (RW)  SPI1 remapping bit [1] */
#endif
#if (CHIP_DEV_ETH_CNT >= 1)
#define CHIP_REGFLD_AFIO_MAPR_PTP_PPS               (0x00000001UL << 30U) /* (RW)  Ethernet PTP PPS remapping  */
#endif
#if defined CHIP_DEV_ST32F105_XC || defined CHIP_DEV_ST32F107_XC || defined CHIP_DEV_CH32V3XX
#define CHIP_REGFLD_AFIO_MAPR_TIM2_ITR1             (0x00000001UL << 29U) /* (RW)  TIM2 internal trigger 1 remapping  */
#endif
#if (CHIP_DEV_SPI_CNT >= 3)
#define CHIP_REGFLD_AFIO_MAPR_SPI3                  (0x00000001UL << 28U) /* (RW)  SPI3 alternate function remapping  */
#endif
#define CHIP_REGFLD_AFIO_MAPR_SWJ_CFG               (0x00000007UL << 24U) /* (RW)  JTAG configuration  */
#if (CHIP_DEV_ETH_CNT >= 1)
#define CHIP_REGFLD_AFIO_MAPR_MII_RMII_SEL          (0x00000001UL << 23U) /* (RW)  MII or RMII selection */
#endif
#if (CHIP_DEV_CAN_CNT >= 2)
#define CHIP_REGFLD_AFIO_MAPR_CAN2                  (0x00000001UL << 22U) /* (RW)  CAN2 I/O remapping */
#endif
#if (CHIP_DEV_ETH_CNT >= 1)
#define CHIP_REGFLD_AFIO_MAPR_ETH                   (0x00000001UL << 21U) /* (RW) Ethernet MAC I/O remapping  */
#endif
#if !(defined CHIP_DEV_ST32F105_XC || defined CHIP_DEV_ST32F107_XC)
#if CHIP_DEV_ADC_CNT >= 2
#define CHIP_REGFLD_AFIO_MAPR_ADC2_ETRG_REG         (0x00000001UL << 20U) /* (RW)  ADC2 external trigger regular conversion remapping  */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_AFIO_MAPR_ADC2_ETRG_INJ         (0x00000001UL << 19U) /* (RW)  ADC2 external trigger injected conversion remapping  */
#endif
#endif
#define CHIP_REGFLD_AFIO_MAPR_ADC1_EXTRG_REG        (0x00000001UL << 18U) /* (RW)  ADC1 external trigger regular conversion remapping  */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_AFIO_MAPR_ADC1_EXTRG_INJ        (0x00000001UL << 17U) /* (RW)  ADC1 external trigger injected conversion remapping  */
#endif
#endif
#define CHIP_REGFLD_AFIO_MAPR_TIM5_CH4_IREMAP       (0x00000001UL << 16U) /* (RW)  Timer 5 channel 4 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR_PD01_REMAP            (0x00000001UL << 15U) /* (RW)  Port D0/Port D1 mapping on OSC_IN/OSC_OUT */
#define CHIP_REGFLD_AFIO_MAPR_CAN1                  (0x00000003UL << 13U) /* (RW)  CAN alternate function remapping  */
#define CHIP_REGFLD_AFIO_MAPR_TIM4                  (0x00000001UL << 12U) /* (RW)  Timer 4 remapping */
#define CHIP_REGFLD_AFIO_MAPR_TIM3                  (0x00000003UL << 10U) /* (RW)  Timer 3 remapping */
#define CHIP_REGFLD_AFIO_MAPR_TIM2                  (0x00000003UL <<  8U) /* (RW)  Timer 2 remapping */
#define CHIP_REGFLD_AFIO_MAPR_TIM1                  (0x00000003UL <<  6U) /* (RW)  Timer 1 remapping */
#define CHIP_REGFLD_AFIO_MAPR_USART3                (0x00000003UL <<  4U) /* (RW)  USART3 remapping */
#define CHIP_REGFLD_AFIO_MAPR_USART2                (0x00000001UL <<  3U) /* (RW)  USART2 remapping */
#define CHIP_REGFLD_AFIO_MAPR_USART1                (0x00000001UL <<  2U) /* (RW)  USART1 remapping bit[0] */
#define CHIP_REGFLD_AFIO_MAPR_I2C1                  (0x00000001UL <<  1U) /* (RW)  I2C1 remapping */
#define CHIP_REGFLD_AFIO_MAPR_SPI1                  (0x00000001UL <<  0U) /* (RW)  SPI1 remapping bit [0] */

#if defined CHIP_DEV_AT32F40X || defined CHIP_DEV_V84XXX
#define CHIP_REGFLDGET_AFIO_MAPR_SPI1(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_AFIO_MAPR_SPI1) | ((LBITFIELD_GET(wrd, CHIP_REGFLD_AFIO_MAPR_SPI1_H)) << 1))
#define CHIP_REGFLDSET_AFIO_MAPR_SPI1(val)          (LBITFIELD_SET(CHIP_REGFLD_AFIO_MAPR_SPI1, val) | ((LBITFIELD_SET(CHIP_REGFLD_AFIO_MAPR_SPI1_H, ((val) >> 1)))))
#else
#define CHIP_REGFLDGET_AFIO_MAPR_SPI1(wrd)          (LBITFIELD_GET(wrd, CHIP_REGFLD_AFIO_MAPR_SPI1))
#define CHIP_REGFLDSET_AFIO_MAPR_SPI1(val)          (LBITFIELD_SET(CHIP_REGFLD_AFIO_MAPR_SPI1, val))
#endif


#define CHIP_REGFLDVAL_AFIO_MAPR_SWJTAG_FULL                0
#define CHIP_REGFLDVAL_AFIO_MAPR_SWJTAG_NO_NJTRST           1
#define CHIP_REGFLDVAL_AFIO_MAPR_SWJTAG_SWD                 2
#define CHIP_REGFLDVAL_AFIO_MAPR_SWJTAG_DIS                 4

#define CHIP_REGFLDVAL_AFIO_MAPR_ADC1_EXTRG_REG_EXTINT11    0
#define CHIP_REGFLDVAL_AFIO_MAPR_ADC1_EXTRG_REG_TMR8_TRGO   1

#define CHIP_REGFLDVAL_AFIO_MAPR_ADC1_EXTRG_INJ_EXTINT15    0
#define CHIP_REGFLDVAL_AFIO_MAPR_ADC1_EXTRG_INJ_TMR8_CH4    1

#define CHIP_REGFLDVAL_AFIO_MAPR_TMR5_CH4_IRMP_PA3          0
#define CHIP_REGFLDVAL_AFIO_MAPR_TMR5_CH4_IRMP_LSI          1

/*********** AF remap and debug I/O configuration register 2 (AFIO_MAP2) ******/
#if defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_AFIO_MAPR2_USART1_H             (0x00000001UL << 26U) /* (RW)  USART1 remapping bit[1]  */
#define CHIP_REGFLD_AFIO_MAPR2_USART8               (0x00000003UL << 24U) /* (RW)  USART8 remapping  */
#define CHIP_REGFLD_AFIO_MAPR2_USART7               (0x00000003UL << 22U) /* (RW)  USART7 remapping  */
#define CHIP_REGFLD_AFIO_MAPR2_USART6               (0x00000003UL << 20U) /* (RW)  USART6 remapping  */
#define CHIP_REGFLD_AFIO_MAPR2_USART5               (0x00000003UL << 18U) /* (RW)  USART5 remapping  */
#define CHIP_REGFLD_AFIO_MAPR2_USART4               (0x00000003UL << 16U) /* (RW)  USART4 remapping  */
#endif
#if defined CHIP_DEV_V84XXX
#define CHIP_REGFLD_AFIO_MAPR2_EXT_SPIF_EN          (0x00000001UL << 21U) /* (RW)  External SPI flash interface enable */
#define CHIP_REGFLD_AFIO_MAPR2_I2C3                 (0x00000001UL << 18U) /* (RW)  I2C3 internal remap */
#define CHIP_REGFLD_AFIO_MAPR2_SPI4                 (0x00000001UL << 17U) /* (RW)  SPI4 internal remap */
#endif
#define CHIP_REGFLD_AFIO_MAPR2_EXMC_NADV            (0x00000001UL << 10U) /* (RW)  XMCMADV connect */
#if !defined CHIP_DEV_V84XXX && !defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_AFIO_MAPR2_TIM14                (0x00000001UL <<  9U) /* (RW)  Timer 14 remap */
#define CHIP_REGFLD_AFIO_MAPR2_TIM13                (0x00000001UL <<  8U) /* (RW)  Timer 13 remap */
#define CHIP_REGFLD_AFIO_MAPR2_TIM11                (0x00000001UL <<  7U) /* (RW)  Timer 11 remap */
#define CHIP_REGFLD_AFIO_MAPR2_TIM10                (0x00000001UL <<  6U) /* (RW)  Timer 10 remap */
#endif
#if !defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_AFIO_MAPR2_TIM9                 (0x00000001UL <<  5U) /* (RW)  Timer 9 channel 1/2 internal remap */
#endif
#if defined CHIP_DEV_CH32XXXX
#define CHIP_REGFLD_AFIO_MAPR2_TIM10                (0x00000003UL <<  5U) /* (RW)  Timer 10 remap */
#endif
#define CHIP_REGMSK_AFIO_MAPR2_RESERVED             (0xFFD9FBDF)

#if defined CHIP_DEV_V84XXX
/*********** AF remap and debug I/O configuration register 3 (AFIO_MAP3) ******/
#define CHIP_REGFLD_AFIO_MAPR3_TMR9_GRMP            (0x0000000FUL <<  0U) /* (RW)  Timer 9 remapping */
#define CHIP_REGMSK_AFIO_MAPR3_RESERVED             (0xFFFFFFF0UL)

/*********** AF remap and debug I/O configuration register 4 (AFIO_MAP5) ******/
#define CHIP_REGFLD_AFIO_MAPR4_TMR5_CH4_IRMP        (0x00000001UL << 19U) /* (RW)  Timer 5 channel 4 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR4_TMR4_GRMP            (0x0000000FUL << 12U) /* (RW)  Timer 4 remapping */
#define CHIP_REGFLD_AFIO_MAPR4_TMR3_GRMP            (0x0000000FUL <<  8U) /* (RW)  Timer 3 remapping */
#define CHIP_REGFLD_AFIO_MAPR4_TMR2_GRMP            (0x00000007UL <<  4U) /* (RW)  Timer 2 remapping */
#define CHIP_REGFLD_AFIO_MAPR4_TMR1_GRMP            (0x0000000FUL <<  0U) /* (RW)  Timer 1 remapping */
#define CHIP_REGMSK_AFIO_MAPR4_RESERVED             (0xFFF70080UL)

/*********** AF remap and debug I/O configuration register 5 (AFIO_MAP5) ******/
#define CHIP_REGFLD_AFIO_MAPR5_SPI4_GRMP            (0x0000000FUL << 28U) /* (RW)  SPI4 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR5_SPI3_GRMP            (0x0000000FUL << 24U) /* (RW)  SPI3 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR5_SPI2_GRMP            (0x0000000FUL << 20U) /* (RW)  SPI2 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR5_SPI1_GRMP            (0x0000000FUL << 16U) /* (RW)  SPI1 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR5_I2C3_GRMP            (0x0000000FUL << 12U) /* (RW)  I2C3 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR5_I2C1_GRMP            (0x0000000FUL <<  4U) /* (RW)  I2C1 internal remapping */
#define CHIP_REGFLD_AFIO_MAPR5_UART5_GRMP           (0x0000000FUL <<  0U) /* (RW)  USART5 internal remapping */
#define CHIP_REGMSK_AFIO_MAPR5_RESERVED             (0x0000F00UL)

/*********** AF remap and debug I/O configuration register 6 (AFIO_MAP6) ******/
#define CHIP_REGFLD_AFIO_MAPR6_UART4_GRMP           (0x0000000FUL << 28U) /* (RW)  UART4 remapping */
#define CHIP_REGFLD_AFIO_MAPR6_USART3_GRMP          (0x0000000FUL << 24U) /* (RW)  USART3 remapping */
#define CHIP_REGFLD_AFIO_MAPR6_USART2_GRMP          (0x0000000FUL << 20U) /* (RW)  USART2 remapping */
#define CHIP_REGFLD_AFIO_MAPR6_USART1_GRMP          (0x0000000FUL << 16U) /* (RW)  USART1 remapping */
#define CHIP_REGFLD_AFIO_MAPR6_CAN1_GRMP            (0x0000000FUL <<  0U) /* (RW)  CAN1 remapping */
#define CHIP_REGMSK_AFIO_MAPR6_RESERVED             (0x0000FFF0UL)

/*********** AF remap and debug I/O configuration register 7 (AFIO_MAP7) ******/
#define CHIP_REGFLD_AFIO_MAPR7_XMC_NADV_GRMP        (0x00000001UL << 27U) /* (RW)  XMC_NADV remapping (disable) */
#define CHIP_REGFLD_AFIO_MAPR7_XMC_GRMP             (0x00000007UL << 24U) /* (RW)  XMC remapping */
#define CHIP_REGFLD_AFIO_MAPR7_SWJTAG_GRMP          (0x00000007UL << 16U) /* (RW)  SWJTAG remapping */
#define CHIP_REGFLD_AFIO_MAPR7_ADC1_ETR_GRMP        (0x00000007UL <<  5U) /* (RW)  ADC1 extenal trigger regular conversion remapping */
#define CHIP_REGFLD_AFIO_MAPR7_ADC1_ETI_GRMP        (0x00000007UL <<  4U) /* (RW)  ADC1 extenal trigger injected conversion remapping */
#define CHIP_REGFLD_AFIO_MAPR7_EXT_SPIF_GEN         (0x00000001UL <<  3U) /* (RW)  External SPI flash interface enable */
#define CHIP_REGMSK_AFIO_MAPR7_RESERVED             (0xF0F8FFC7UL)

#define CHIP_REGFLDVAL_AFIO_MAPR7_ADC1_ETR_EXTINT11  0
#define CHIP_REGFLDVAL_AFIO_MAPR7_ADC1_ETR_TMR8_TRGO 1

#define CHIP_REGFLDVAL_AFIO_MAPR7_ADC1_ETI_EXTINT15  0
#define CHIP_REGFLDVAL_AFIO_MAPR7_ADC1_ETI_TMR8_CH4  1

/*********** AF remap and debug I/O configuration register 8 (AFIO_MAPR8) ******/
#define CHIP_REGFLD_AFIO_MAPR8_UART8_GRMP          (0x0000000FUL << 28U) /* (RW)  UART8 remapping */
#define CHIP_REGFLD_AFIO_MAPR8_UART7_GRMP          (0x0000000FUL << 24U) /* (RW)  UART7 remapping */
#define CHIP_REGFLD_AFIO_MAPR8_USART6_GRMP         (0x0000000FUL << 20U) /* (RW)  USART6 remapping */
#define CHIP_REGMSK_AFIO_MAPR8_RESERVED            (0x000FFFFFUL)
#endif

#endif // CHIP_STM32F10X_REGS_AFIO_H
