#ifndef REGS_I2S_H
#define REGS_I2S_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t CTRL;                        /* 0x0: Control Register */
    __I  uint32_t RFIFO_FILLINGS;              /* 0x4: Rx FIFO  Filling Level */
    __I  uint32_t TFIFO_FILLINGS;              /* 0x8: Tx FIFO  Filling Level */
    __IO uint32_t FIFO_THRESH;                 /* 0xC: TX/RX FIFO Threshold setting. */
    __IO uint32_t STA;                         /* 0x10: Status Registers */
    __I  uint8_t  RESERVED0[12];               /* 0x14 - 0x1F: Reserved */
    __I  uint32_t RXD[4];                      /* 0x20 - 0x2C: Rx Data0 */
    __O  uint32_t TXD[4];                      /* 0x30 - 0x3C: Tx Data0 */
    __I  uint8_t  RESERVED1[16];               /* 0x40 - 0x4F: Reserved */
    __IO uint32_t CFGR;                        /* 0x50: Configruation Regsiter */
    __I  uint8_t  RESERVED2[4];                /* 0x54 - 0x57: Reserved */
    __IO uint32_t MISC_CFGR;                   /* 0x58: Misc configuration register */
    __I  uint8_t  RESERVED3[4];                /* 0x5C - 0x5F: Reserved */
    __IO uint32_t RXDSLOT[4];                  /* 0x60 - 0x6C: Rx Slots Enable for Rx Data0 */
    __IO uint32_t TXDSLOT[4];                  /* 0x70 - 0x7C: Tx Slots Enable for Tx Data0. */
} CHIP_REGS_I2S_T;

#define CHIP_REGS_I2S0            ((CHIP_REGS_I2S_T *)CHIP_MEMRGN_ADDR_I2S0)
#define CHIP_REGS_I2S1            ((CHIP_REGS_I2S_T *)CHIP_MEMRGN_ADDR_I2S1)
#define CHIP_REGS_I2S2            ((CHIP_REGS_I2S_T *)CHIP_MEMRGN_ADDR_I2S2)
#define CHIP_REGS_I2S3            ((CHIP_REGS_I2S_T *)CHIP_MEMRGN_ADDR_I2S3)


/*********** I2S_CTRL - Control Register **************************************/
#define CHIP_REGFLD_I2S_CTRL_SFTRST_RX                  (0x00000001UL << 18U) /* (RW1SC) Receive module software reset. */
#define CHIP_REGFLD_I2S_CTRL_SFTRST_TX                  (0x00000001UL << 17U) /* (RW1SC) Transmit module software reset. */
#define CHIP_REGFLD_I2S_CTRL_SFTRST_CLKGEN              (0x00000001UL << 16U) /* (RW1SC) Clock generation module module software reset. */
#define CHIP_REGFLD_I2S_CTRL_TXDNIE                     (0x00000001UL << 15U) /* (RW) Transmit data demand interrupt enable. */
#define CHIP_REGFLD_I2S_CTRL_RXDAIE                     (0x00000001UL << 14U) /* (RW) Receive data available interrupt enable. */
#define CHIP_REGFLD_I2S_CTRL_ERRIE                      (0x00000001UL << 13U) /* (RW) Error interrupt enable (UD, OV). */
#define CHIP_REGFLD_I2S_CTRL_TX_DMA_EN                  (0x00000001UL << 12U) /* (RW) Use DMA for transmit. */
#define CHIP_REGFLD_I2S_CTRL_RX_DMA_EN                  (0x00000001UL << 11U) /* (RW) Use DMA for receive. */
#define CHIP_REGFLD_I2S_CTRL_TXFIFOCLR                  (0x00000001UL << 10U) /* (RW1SC) Transmit FIFO clear. */
#define CHIP_REGFLD_I2S_CTRL_RXFIFOCLR                  (0x00000001UL <<  9U) /* (RW1SC) Receive FIFO clear. */
#define CHIP_REGFLD_I2S_CTRL_TX_EN(x)                   (0x00000001UL <<  (5U + (x))) /* (RW) Transmit module x enable (x=0..3) */
#define CHIP_REGFLD_I2S_CTRL_RX_EN(x)                   (0x00000001UL <<  (1U + (x))) /* (RW) Receive module x enable (x=0..3) */
#define CHIP_REGFLD_I2S_CTRL_I2S_EN                     (0x00000001UL <<  0U) /* (RW) Overall enable control of the entire I2S module. */
#define CHIP_REGMSK_I2S_CTRL_RESERVED                   (0xFFF80000UL)

/*********** RFIFO_FILLINGS - Rx FIFO  Filling Level **************************/
#define CHIP_REGFLD_I2S_RFIFO_FILLINGS_RX(x)            (0x000000FFUL << (8U*(x))) /* (RO) The number of FIFO buffer words stored in receiver module x (x=0..3). */

/*********** TFIFO_FILLINGS - Tx FIFO  Filling Level **************************/
#define CHIP_REGFLD_I2S_TFIFO_FILLINGS_TX(x)            (0x000000FFUL << (8U*(x))) /* (RO) The number of FIFO buffer words stored in transmit module x (x=0..3). */

/*********** FIFO_THRESH - TX/RX FIFO Threshold setting ***********************/
#define CHIP_REGFLD_I2S_FIFO_THRESH_TX                  (0x000000FFUL << 8U) /* (RW) Transmit module FIFO buffer threshold */
#define CHIP_REGFLD_I2S_FIFO_THRESH_RX                  (0x000000FFUL << 0U) /* (RW) Receive module FIFO buffer threshold */

/*********** STA - Status Register ********************************************/
#define CHIP_REGFLD_I2S_STA_TX_UD(x)                    (0x00000001UL << (13U + (x))) /* (RW1C) transmit module x FIFO underflow (x=0..3). */
#define CHIP_REGFLD_I2S_STA_RX_OV(x)                    (0x00000001UL << (9U + (x)))  /* (RW1C) recieve module x FIFO overflow (x=0..3). */
#define CHIP_REGFLD_I2S_STA_TX_DN(x)                    (0x00000001UL << (5U + (x)))  /* (RO)   transmit module x FIFO data is less than or equal to the threshold (x=0..3). */
#define CHIP_REGFLD_I2S_STA_RX_DA(x)                    (0x00000001UL << (1U + (x)))  /* (RO)   receive module x FIFO data is greater than or equal to the threshold (x=0..3). */

/*********** CFGR - Configruation Regsiter ************************************/
#define CHIP_REGFLD_I2S_CFGR_BCLK_GATEOFF               (0x00000001UL << 30U) /* (RW) Turn off BCLK control. Set to 1 to turn off BCLK */
#define CHIP_REGFLD_I2S_CFGR_BCLK_DIV                   (0x000001FFUL << 21U) /* (RW) Prescaler control for generate BCLK from MCLK */
#define CHIP_REGFLD_I2S_CFGR_INV_BCLK_OUT               (0x00000001UL << 20U) /* (RW) BCLK output inversion */
#define CHIP_REGFLD_I2S_CFGR_INV_BCLK_IN                (0x00000001UL << 19U) /* (RW) BCLK input inversion */
#define CHIP_REGFLD_I2S_CFGR_INV_FCLK_OUT               (0x00000001UL << 18U) /* (RW) FCLK output inversion */
#define CHIP_REGFLD_I2S_CFGR_INV_FCLK_IN                (0x00000001UL << 17U) /* (RW) FCLK input inversion */
#define CHIP_REGFLD_I2S_CFGR_INV_MCLK_OUT               (0x00000001UL << 16U) /* (RW) MCLK output inversion */
#define CHIP_REGFLD_I2S_CFGR_INV_MCLK_IN                (0x00000001UL << 15U) /* (RW) MCLK input inversion */
#define CHIP_REGFLD_I2S_CFGR_BCLK_SEL_OP                (0x00000001UL << 14U) /* (RW) BCLK source selection (0 - internal, 1 - from pin) */
#define CHIP_REGFLD_I2S_CFGR_FCLK_SEL_OP                (0x00000001UL << 13U) /* (RW) FCLK source selection (0 - internal, 1 - from pin) */
#define CHIP_REGFLD_I2S_CFGR_MCK_SEL_OP                 (0x00000001UL << 12U) /* (RW) MCLK source selection (0 - internal, 1 - from pin) */
#define CHIP_REGFLD_I2S_CFGR_FRAME_EDGE                 (0x00000001UL << 11U) /* (RW) Frame starting edge selection (0 - fall, 1 - rise) */
#define CHIP_REGFLD_I2S_CFGR_CH_MAX                     (0x0000001FUL <<  6U) /* (RW) Number of slot channels supported in TDM mode (2,4,..16) */
#define CHIP_REGFLD_I2S_CFGR_TDM_EN                     (0x00000001UL <<  5U) /* (RW) TDM mode selection */
#define CHIP_REGFLD_I2S_CFGR_STD                        (0x00000003UL <<  3U) /* (RW) I2S standard selection */
#define CHIP_REGFLD_I2S_CFGR_DATSIZ                     (0x00000003UL <<  1U) /* (RW) Bit length selection of transmitted data */
#define CHIP_REGFLD_I2S_CFGR_CHSIZ                      (0x00000001UL <<  0U) /* (RW) Slot channel length (number of bits per audio slot channel) */
#define CHIP_REGMSK_I2S_CFGR_RESERVED                   (0x80000000UL)


#define CHIP_REGFLDVAL_I2S_CFGR_FRAME_EDGE_FALL         0 /* Falling edge indicates new frame (same as Philips I2S standard) */
#define CHIP_REGFLDVAL_I2S_CFGR_FRAME_EDGE_RISE         1 /* Rising edge indicates new frame */

#define CHIP_REGFLDVAL_I2S_CFGR_STD_PHILIPS             0 /* Philips I2S standard */
#define CHIP_REGFLDVAL_I2S_CFGR_STD_LJ                  1 /* MSB alignment standard (left justified) */
#define CHIP_REGFLDVAL_I2S_CFGR_STD_RJ                  2 /* LSB alignment standard (right justified) */
#define CHIP_REGFLDVAL_I2S_CFGR_STD_PCM                 3 /* PCM standard */

#define CHIP_REGFLDVAL_I2S_CFGR_DATSIZ_16               0 /* 16 bit data length */
#define CHIP_REGFLDVAL_I2S_CFGR_DATSIZ_24               1 /* 24 bit data length */
#define CHIP_REGFLDVAL_I2S_CFGR_DATSIZ_32               2 /* 32 bit data length */

#define CHIP_REGFLDVAL_I2S_CFGR_CHSIZ_16                0 /* 16 bit width */
#define CHIP_REGFLDVAL_I2S_CFGR_CHSIZ_32                1 /* 32 bit width */

/*********** MISC_CFGR - Misc configuration register **************************/
#define CHIP_REGFLD_I2S_MISC_CFGR_MCLK_GATEOFF          (0x00000001UL << 13U) /* (RW) Turn off the MCLK */
#define CHIP_REGFLD_I2S_MISC_CFGR_MCLKOE                (0x00000001UL <<  0U) /* (RW) MCLK output to pin enable */
#define CHIP_REGMSK_I2S_MISC_CFGR_RESERVED              (0xFFFFDFFEUL)



#endif // REGS_I2S_H
