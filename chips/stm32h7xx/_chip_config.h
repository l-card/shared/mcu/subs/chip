﻿#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H

/* включение опций, поддержанных в ревизии RevV чипа */
#define CHIP_CFG_REV_V_EN           1

/* ------------------ Настройки питания --------------------------------------*/
#define CHIP_CFG_PWR_LDO_EN         1 /* использование LDO для питания ядра (по умолчанию) */
#define CHIP_CFG_PWR_BYPASS_EN      0 /* режим без использования power manager */


#define CHIP_CFG_PWR_VOS            CHIP_PWR_VOS1 /* управление напряжением ядра:
                                                     VOS0 (только для rev V), VOS1, VOS2 или VOS3
                                                     влияет на потеребление и макс. частоту с выхода
                                                     PLL (VOS1 - наибольшая производительность) */

#define CHIP_CFG_PWR_BACKUP_REG_EN        1 /* Разрешения backup regulator для питания от батареи.
                                               Для применения настройки должен быть разрешен
                                               CHIP_CFG_PWR_BACKUP_REG_SETUP_EN */
#define CHIP_CFG_PWR_BACKUP_REG_SETUP_EN  0 /* Разрешение изменения конфигурации CHIP_CFG_PWR_BACKUP_REG_EN */


#define CHIP_CFG_PWR_MON_EN         0 /* Разрешение мониторинга Vbat и температуры.
                                          Для применения настройки должен быть разрешен CHIP_CFG_PWR_MON_SETUP_EN */
#define CHIP_CFG_PWR_MON_SETUP_EN   0 /* Разрешение изменения настройки CHIP_PWR_MON_EN */



#define CHIP_CFG_CLK_CPUFREQ_BOOST  1 /* поддержка опции увеличения скорости ядра, но без ECC  (stm32h72x) */

/* ------------------ Внешний высокочастотный источник частоты HSE -----------*/
#define CHIP_CFG_CLK_HSE_FREQ       CHIP_MHZ(24)               /* значение внешней частоты HSE */
#define CHIP_CFG_CLK_HSE_MODE       CHIP_CLK_EXTMODE_CLOCK    /* режим внешнего источника HSE - внешний клок */
#define CHIP_CFG_CLK_HSE_CSS_EN     1                         /* разрешение контроля внешней частоты HSE */


/* ------------------ Внешний низкочастотный источник частоты LSE ------------*/
#define CHIP_CFG_CLK_LSE_SETUP_EN   1                          /* Разрешение изменения конфигурации LSE. */
#define CHIP_CFG_CLK_LSE_MODE       CHIP_CLK_EXTMODE_OSC       /* Режим внешнего источника LSE - осциллятор.
                                                                  Для осциллятора можно не указывать частоты, т.к. фиксированная */
#define CHIP_CFG_CLK_LSE_DRV        CHIP_CLK_LSE_DRV_LOWEST    /* Сила драйвера LSE осциллятора (LOWEST, MEDIUM_LOW, MEDIUM_HIGH, HIGHEST) */
#define CHIP_CFG_CLK_LSE_CSS_EN     1                          /* Разрешение контроля внешней частоты LSE */

#define CHIP_CFG_CLK_LSE_RDY_TOUT_MS 100                       /* время ожидания готовности LSE. Если < 0 - бесконечное */



/* ------------------ Внутренний высокочастотный источник частоты HSI --------*/
#define CHIP_CFG_CLK_HSI_EN         1            /* разрешение источника частоты HSI */
#define CHIP_CFG_CLK_HSI_FREQ       CHIP_MHZ(64) /* частота источника HSI (из набора 8, 16, 32 или 64 МГц) */
#define CHIP_CFG_CLK_HSI_ON_STOP_EN 0            /* разрешение источника частоты HSI в режиме STOP */


/* ------------------ Внутренний низкочастотный источник частоты LSI ---------*/
#define CHIP_CFG_CLK_LSI_EN  1  /* разрешение источника частоты LSI */

/* ------------------ Внутренний низкопотребляющий источник частоты CSI ------*/
#define CHIP_CFG_CLK_CSI_EN          1        /* разрешение источника частоты CSI */
#define CHIP_CFG_CLK_CSI_ON_STOP_EN  0        /* разрешение источника частоты CSI в режиме STOP */

/*  ----------------- Внутренний источник частоты 48 МГц ---------------------*/
#define CHIP_CFG_CLK_HSI48_EN        0

/* ------------------ Внешняя частота, подаваемая на пин I2S_CKIN ------------*/
#define CHIP_CFG_CLK_I2S_CKIN_EN     0
#define CHIP_CFG_CLK_I2S_CKIN_FREQ   CHIP_MHZ(60)

/* ---------------------- Настройки PLL ---------------------------------------*/
/*******************************************************************************
 * Может быть настроено до 3-х PLL, каждая из которых имеет до 3-х выходов (P,R,Q).
 * Источник частоты для всех PLL один и задается CHIP_CFG_CLK_PLL_SRC.
 * Для каждой PLL настраивается независимо:
 *  1. Разрешение PLL и всех ее настроек (EN)
 *  2. Делитель входной частоты DIVM от 1 до 63 (результирующая частота fin должна быть от 1 до 16МГц)
 *  3. Множитель DIVN (4..512) и его дробная часть (FRACN) для получения частоты
 *      fvco = fin * (divn + fracn/2^13)
 *     fvco должна быть в диапазоне:
 *       192-960 МГц для fin > 2 МГц
 *       150-420 МГц для fin <= 2 МГц
 *  4. Для каждого выхода настраивается его разрешение (DIVP_EN, DIVR_EN, DIVQ_EN)
 *      и собственно значение делителя (DIVP, DIVR, DIVQ) от 1 до 128
 *      (PLL1_DIVP может быть только четным)
 ******************************************************************************/
#define CHIP_CFG_CLK_PLL_SRC      CHIP_CLK_HSE  /* источник частоты (HSI, CSI, HSE) для
                                               всех 3-х PLL.
                                               Если все PLL запрещены, то не используется */
#define CHIP_CFG_CLK_PLL1_EN       1
#define CHIP_CFG_CLK_PLL1_DIVM     1   /* делитель входной частоты PLL1 (результирующая частота должна быть 1-16 МГц */
#define CHIP_CFG_CLK_PLL1_DIVN     100 /* множитель, для получения частоты fvco = fin * (divn + fracn/2^13) */
#define CHIP_CFG_CLK_PLL1_FRACN    0   /* дробная часть множителя */
#define CHIP_CFG_CLK_PLL1_DIVP_EN  1 /* разреение выхода P */
#define CHIP_CFG_CLK_PLL1_DIVP     2 /* частота 400 МГц для процессора */
#define CHIP_CFG_CLK_PLL1_DIVR_EN  0
#define CHIP_CFG_CLK_PLL1_DIVQ_EN  1
#define CHIP_CFG_CLK_PLL1_DIVQ     8 /* частота 100 МГц */

/* на PLL2 получаем частоту для UART - 221184 КHz, чтобы можно было точно выставить вплоть до 921600 */
#define CHIP_CFG_CLK_PLL2_EN      1
#define CHIP_CFG_CLK_PLL2_DIVM    8
#define CHIP_CFG_CLK_PLL2_DIVN    221
#define CHIP_CFG_CLK_PLL2_FRACN   1507
#define CHIP_CFG_CLK_PLL2_DIVP_EN  0
#define CHIP_CFG_CLK_PLL2_DIVR_EN  0
#define CHIP_CFG_CLK_PLL2_DIVQ_EN  1
#define CHIP_CFG_CLK_PLL2_DIVQ     3 /* выходная частота для UART 73728 KHz  */

#define CHIP_CFG_CLK_PLL3_EN       0


/* --------------------- Системный клок --------------------------------------*/
#define CHIP_CFG_CLK_SYS_SRC     CHIP_CLK_PLL1_P /* источник частоты для системного клока: HSI, CSI, HSE или PLL1_P */

/* --------------------- Частоты CPU, шин и таймеров -------------------------*/
#define CHIP_CFG_CLK_D1_CPU_DIV      1      /* делитель входной системной частоты (SYS) домена D1 для
                                           получения частоты CPU из набора 1,2,4,8,16,64,128,256,512. */

#define CHIP_CFG_CLK_HOST_DIV        2      /* дополнительный делитель частоты CPU для получения частот AHB/AXI из набора 1,2,4,8,16,64,128,256,512 */
#define CHIP_CFG_CLK_APB1_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB1 в D2 из набора 1,2,4,8,16 */
#define CHIP_CFG_CLK_APB2_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB2 в D2 из набора 1,2,4,8,16 */
#define CHIP_CFG_CLK_APB3_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB3 в D1 из набора 1,2,4,8,16 */
#define CHIP_CFG_CLK_APB4_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB4 в D3 из набора 1,2,4,8,16 */


#define CHIP_CFG_CLK_TIMX_DIV       1      /* делитель частоты таймеров X относительно AHB1 (1, 2, 4, 8). Возможные значения зависят от CHIP_CFG_CLK_APB1_DIV */
#define CHIP_CFG_CLK_TIMY_DIV       1      /* делитель частоты таймеров Y относительно AHB2 (1, 2, 4, 8). Возможные значения зависят от CHIP_CFG_CLK_APB2_DIV */
#define CHIP_CFG_CLK_HRTIM_KER_SRC  CHIP_CLK_TIMY_KER /* источник клока для High Resolution Timer - TIMY_KER или CPU */


/* ------------------------- RTC ---------------------------------------------*/
#define CHIP_CFG_CLK_RTC_EN          1 /* разрешение клока RTC */
#define CHIP_CFG_CLK_RTC_SETUP_EN    1 /* разрешение изменения конфигурации клока RTC. */
#define CHIP_CFG_CLK_RTC_SRC         CHIP_CLK_LSE /* источник частоты для RTC (LSE, LSI, RTC_HSE).
                                                     Если RTC_HSE, то должен быть задан делитель CHIP_CFG_CLK_RTC_HSE_DIV */
#define CHIP_CFG_CLK_RTC_HSE_DIV     2 /* делитель клока HSE при подаче на RTC (2-63) */

/* ------------------ Источники частот при пробуждении -----------------------*/
#define CHIP_CFG_CLK_WU_SRC         CHIP_CLK_HSI  /* источник клока при пробуждении (wakeup): HSI или CSI */
#define CHIP_CFG_CLK_WU_KER_SRC     CHIP_CLK_HSI  /* источник kernel-клока при пробуждении (wakeup): HSI или CSI */

/* ----------------------- Настройки MCO (выходы частоты) --------------------*/
#define CHIP_CFG_CLK_MCO1_EN         1 /* разрешение генерации выходной частоты MCO1 */
#define CHIP_CFG_CLK_MCO1_SRC        CHIP_CLK_PLL1_Q /* Источник частоты для MCO1 (HSI, LSE, HSE, PLL1_Q, HSI48) */
#define CHIP_CFG_CLK_MCO1_DIV        4 /* делитель частоты от 1 до 15 */

#define CHIP_CFG_CLK_MCO2_EN         0 /* разрешение генерации выходной частоты MCO1 */
#define CHIP_CFG_CLK_MCO2_SRC        CHIP_CLK_SYS /* Источник частоты для MCO1 (SYS, PLL2_P, HSE, PLL1_P, CSI, LSI) */
#define CHIP_CFG_CLK_MCO2_DIV        1 /* делитель частоты от 1 до 15 */


/* -------------------------- Периферия ------------------------------------- */
#define CHIP_CFG_CLK_PER_SRC         CHIP_CLK_HSI_KER /* Источник частоты клока per_ck (HSI_KER, CSI_KER, HSE).
                                                         Промежуточная частота, которая может использоваться
                                                         как источник ker_ck для некоторой периферии */

/*******************************************************************************
 * Настройки источников частот для kernel clock (ker_ck) периферии, для которой
 * данный клок может независимо настраиваться. Одна настройка может определять
 * источник ker_ck сразу для нескольких однотипных блоков.
 *
 * Настройка источника соответствующего ker_ck с проверкой диапазона и разрешения
 * выбранной частоты происходит, если выполняется одно из условий:
 *   - разрешен клок хотя бы одного блока, для которого настройка определяет источник ker_ck.
 *      Например для CHIP_CFG_CLK_USART1_6_9_10_KER_SRC включение конфигурации произойдет,
 *       если разрешена одна из настроек CHIP_CLK_USART1_EN, CHIP_CLK_USART6_EN,
 *       CHIP_CLK_UART9_EN или CHIP_CLK_USART10_EN.
 *   - явно разрешена конфигурация источника частоты через определение значения
 *      ***_KER_SRC_SETUP_EN, отличного от нуля. При одной настройке для нескольких
 *      блоков в качестве префикса может использоваться как общее название,
 *      так и отдельного блока. Например для CHIP_CFG_CLK_USART1_6_9_10_KER_SRC включение
 *      конфигурации произойдет при определении одного из следующих параметров:
 *      CHIP_CFG_CLK_USART1_6_9_10_KER_SRC_EN, CHIP_CFG_CLK_USART1_KER_SRC_SETUP_EN,
 *      CHIP_CFG_CLK_USART6_KER_SRC_SETUP_EN, CHIP_CFG_CLK_UART9_KER_SRC_SETUP_EN,
 *      CHIP_CFG_CLK_USART10_KER_SRC_SETUP_EN. Данная возможность используется,
 *      если клок блока не требуется разрешать при старте, но будет разрешен
 *      во время работы с помощью chip_per_clk_en()/chip_per_clk_dis(), в результате
 *      чего при старте требуется только настройка источника частоты
 * Если ни одно из этих условий не выполненно, то соответствующий параметр не
 *      будет использоваться и может быть не определен
 ******************************************************************************/
#define CHIP_CFG_CLK_EXMC_KER_SRC               CHIP_CLK_AHB3   /* AHB3, PLL1_Q, PLL2_R, PER */

#define CHIP_CFG_CLK_LPTIM1_KER_SRC             CHIP_CLK_APB1   /* APB1, PLL2_P, PLL3_R, LSE, LSI, PER */
#define CHIP_CFG_CLK_LPTIM2_KER_SRC             CHIP_CLK_APB4   /* APB4, PLL2_P, PLL3_R, LSE, LSI, PER */
#define CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC         CHIP_CLK_APB4   /* APB4, PLL2_P, PLL3_R, LSE, LSI, PER */

#define CHIP_CFG_CLK_USART1_6_9_10_KER_SRC      CHIP_CLK_APB2   /* APB2, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, LSE */
#define CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC   CHIP_CLK_PLL2_Q /* APB1, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, LSE */
#define CHIP_CFG_CLK_LPUART1_KER_SRC            CHIP_CLK_APB4   /* APB4, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, LSE */

#define CHIP_CFG_CLK_QSPI_KER_SRC               CHIP_CLK_AHB3   /* AHB3, PLL1_Q, PLL2_R, PER (!stm32h72x) */
#define CHIP_CFG_CLK_OCTOSPI_KER_SRC            CHIP_CLK_AHB3   /* AHB3, PLL1_Q, PLL2_R, PER (stm32h72x) */
#define CHIP_CFG_CLK_SPI1_2_3_KER_SRC           CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER */
#define CHIP_CFG_CLK_SPI4_5_KER_SRC             CHIP_CLK_PLL2_Q /* APB2, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, HSE */
#define CHIP_CFG_CLK_SPI6_KER_SRC               CHIP_CLK_APB4   /* APB4, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, HSE, I2S_CKIN (stm32h72x) */

#define CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC         CHIP_CLK_APB1   /* APB1, PLL3_R, HSI_KER, CSI_KER */
#define CHIP_CFG_CLK_I2C4_KER_SRC               CHIP_CLK_APB4   /* APB4, PLL3_R, HSI_KER, CSI_KER */

#define CHIP_CFG_CLK_SDMMC_KER_SRC              CHIP_CLK_PLL1_Q /* PLL1_Q или PLL2_R */
#define CHIP_CFG_CLK_USB_KER_SRC                CHIP_CLK_PLL1_Q /* PLL1_Q, PLL3_Q, HSI48 */
#define CHIP_CFG_CLK_ADC_KER_SRC                CHIP_CLK_PER    /* PLL2_P, PLL3_R, PER */

#define CHIP_CFG_CLK_SAI1_KER_SRC               CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER */
#define CHIP_CFG_CLK_SAI2_3_KER_SRC             CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER (!stm32h72x) */
#define CHIP_CFG_CLK_SAI4A_KER_SRC              CHIP_CLK_PER    /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER, SPIDIFRX_SYM (stm32h72x) */
#define CHIP_CFG_CLK_SAI4B_KER_SRC              CHIP_CLK_PER    /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER, SPIDIFRX_SYM (stm32h72x) */
#define CHIP_CFG_CLK_SPDIFRX_KER_SRC            CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_R, PLL3_R, HSI_KER */
#define CHIP_CFG_CLK_DFSDM1_KER_SRC             CHIP_CLK_APB2    /* APB2, SYS */

#define CHIP_CFG_CLK_FDCAN_KER_SRC              CHIP_CLK_HSE     /* HSE, PLL1_Q, PLL2_Q */
#define CHIP_CFG_CLK_SWPMI_KER_SRC              CHIP_CLK_HSI_KER /* APB1, HSI_KER */
#define CHIP_CFG_CLK_RNG_KER_SRC                CHIP_CLK_LSE     /* HSI48, PLL1_Q, LSE, LSI */
#define CHIP_CFG_CLK_CEC_KER_SRC                CHIP_CLK_LSE     /* LSE, LSI, CSI */
#define CHIP_CFG_CLK_DSI_KER_SRC                CHIP_CLK_DSI_PHY /* DSI_PHY, PLL2_Q (stm32h747) */


/* Опциональные параметры для принудительного разрешения настройки источника
 * ker_ck блока без разрешения самого клока блока */
/* #define CHIP_CFG_CLK_EXMC_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_LPTIM1_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_LPTIM2_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_LPTIM3_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_LPTIM4_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_LPTIM5_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_USART1_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_USART2_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_USART3_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_UART4_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_UART5_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_USART6_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_UART7_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_UART8_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_UART9_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_USART10_KER_SRC_SETUP_EN     1 */
/* #define CHIP_CFG_CLK_LPUART1_KER_SRC_SETUP_EN     1 */
/* #define CHIP_CFG_CLK_QSPI_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_OCTOSPI_KER_SRC_SETUP_EN     1 */
/* #define CHIP_CFG_CLK_SPI1_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SPI2_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SPI3_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SPI4_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SPI5_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SPI6_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_I2C1_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_I2C2_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_I2C3_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_I2C4_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_I2C5_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SDMMC1_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_SDMMC2_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_ADC1_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_ADC2_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_ADC3_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_USB1_OTGHS_KER_SRC_SETUP_EN  1 */
/* #define CHIP_CFG_CLK_USB2_OTGHS_KER_SRC_SETUP_EN  1 */
/* #define CHIP_CFG_CLK_SAI1_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SAI2_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SAI3_KER_SRC_SETUP_EN        1 */
/* #define CHIP_CFG_CLK_SAI4A_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_SAI4B_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_SPDIFRX_KER_SRC_SETUP_EN     1 */
/* #define CHIP_CFG_CLK_DFSDM1_KER_SRC_SETUP_EN      1 */
/* #define CHIP_CFG_CLK_FDCAN_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_SWPMI_KER_SRC_SETUP_EN       1 */
/* #define CHIP_CFG_CLK_RNG_KER_SRC_SETUP_EN         1 */



/* *****************************************************************************
   Разрашение клоков для периферийных блоков.
   Настройка определяет, нужно ли включать клок для этого блока при начальной
   инициализации.

   Помимо основного клока от шины APB/AHB, блок может использовать kernel clock,
   источник которого должен быть определен соответствующим параметром, указанным
   в коментарии. Разрешение клока блока при инициализации автоамтически разрешает
   настройку источника kernel clock (при наличии).

   Нулевое значеие настройки означает, что клок будет запрещен при инициализации.
   При этом его можно включить уже во время работы через chip_per_clk_en(),
   однако в случае наличия у блока отдельного kernel clock, его источник должен
   быть в этом случае настроен при инициализации, для чего его настройка должна
   быть в этом случае явно разрешена через параметр ***_KER_SRC_SETUP_EN (см. выше)

   Для таймеров (не LP) ker_ck всегда настраивается и определяется:
    - TIM2,3,4,5,6,7,12,13,14: делителем CHIP_CFG_CLK_TIMX_DIV относительно AHB1
    - TIM1,8,15,16,17: делителем CHIP_CFG_CLK_TIMY_DIV относительно AHB2.
    - HTRIM: CHIP_CFG_CLK_HRTIM_KER_SRC
 ******************************************************************************/
/* system */
#define CHIP_CFG_CLK_SYSCFG_EN              1   /* APB4 */
#define CHIP_CFG_CLK_HSEM_EN                0   /* AHB4 */
#define CHIP_CFG_CLK_RTC_APB_EN             0   /* APB4 */
/* ram */
#define CHIP_CFG_CLK_SRAM1_EN               1   /* AHB2 */
#define CHIP_CFG_CLK_SRAM2_EN               1   /* AHB2 */
#define CHIP_CFG_CLK_SRAM3_EN               1   /* AHB2 (!stm32h72x) */
#define CHIP_CFG_CLK_BKPRAM_EN              0   /* AHB4 */
#define CHIP_CFG_CLK_EXMC_EN                0   /* AHB3, ker_ck: CHIP_CFG_CLK_EXMC_KER_SRC */
/* cm4 core */
#define CHIP_CFG_CLK_ART_EN                 0   /* AHB1 (stm32h745/7) */
#define CHIP_CFG_CLK_CM4_FLASH_EN           0   /* AHB3 (stm32h745/7) */
#define CHIP_CFG_CLK_CM4_DTCM1_EN           0   /* AHB3 (stm32h745/7) */
#define CHIP_CFG_CLK_CM4_DTCM2_EN           0   /* AHB3 (stm32h745/7) */
#define CHIP_CFG_CLK_CM4_ITCM_EN            0   /* AHB3 (stm32h745/7) */
#define CHIP_CFG_CLK_CM4_AXISRAM            0   /* AHB3 (stm32h745/7) */
/* gpio */
#define CHIP_CFG_CLK_GPIOA_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOB_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOC_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOD_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOE_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOF_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOG_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOH_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOI_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOJ_EN               1   /* AHB4 */
#define CHIP_CFG_CLK_GPIOK_EN               1   /* AHB4 */
/* dma */
#define CHIP_CFG_CLK_MDMA_EN                0   /* AHB3 */
#define CHIP_CFG_CLK_DMA2D_EN               0   /* AHB3 */
#define CHIP_CFG_CLK_BDMA_EN                0   /* AHB4 */
#define CHIP_CFG_CLK_DMA1_EN                0   /* AHB1 */
#define CHIP_CFG_CLK_DMA2_EN                0   /* AHB1 */
/* timers */
#define CHIP_CFG_CLK_HRTIM_EN               0   /* APB2, ker_ck: CHIP_CFG_CLK_HRTIM_KER_SRC */
#define CHIP_CFG_CLK_TIM1_EN                0   /* APB2, ker_ck: APB2/CHIP_CFG_CLK_TIMY_DIV */
#define CHIP_CFG_CLK_TIM2_EN                0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM3_EN                0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM4_EN                0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM5_EN                0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM6_EN                0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM7_EN                0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM8_EN                0   /* APB2, ker_ck: APB2/CHIP_CFG_CLK_TIMY_DIV */
#define CHIP_CFG_CLK_TIM12_EN               0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM13_EN               0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM14_EN               0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV */
#define CHIP_CFG_CLK_TIM15_EN               0   /* APB2, ker_ck: APB2/CHIP_CFG_CLK_TIMY_DIV */
#define CHIP_CFG_CLK_TIM16_EN               0   /* APB2, ker_ck: APB2/CHIP_CFG_CLK_TIMY_DIV */
#define CHIP_CFG_CLK_TIM17_EN               0   /* APB2, ker_ck: APB2/CHIP_CFG_CLK_TIMY_DIV */
#define CHIP_CFG_CLK_TIM23_EN               0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV (stm32h72x) */
#define CHIP_CFG_CLK_TIM24_EN               0   /* APB1, ker_ck: APB1/CHIP_CFG_CLK_TIMX_DIV (stm32h72x) */
#define CHIP_CFG_CLK_LPTIM1_EN              0   /* APB1, ker_ck: CHIP_CFG_CLK_LPTIM1_KER_SRC */
#define CHIP_CFG_CLK_LPTIM2_EN              0   /* APB4, ker_ck: CHIP_CFG_CLK_LPTIM2_KER_SRC */
#define CHIP_CFG_CLK_LPTIM3_EN              0   /* APB4, ker_ck: CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC */
#define CHIP_CFG_CLK_LPTIM4_EN              0   /* APB4, ker_ck: CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC */
#define CHIP_CFG_CLK_LPTIM5_EN              0   /* APB4, ker_ck: CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC */
/* uart */
#define CHIP_CFG_CLK_USART1_EN              0   /* APB2, ker_ck: CHIP_CFG_CLK_USART1_6_9_10_KER_SRC */
#define CHIP_CFG_CLK_USART2_EN              0   /* APB1, ker_ck: CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC */
#define CHIP_CFG_CLK_USART3_EN              0   /* APB1, ker_ck: CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC */
#define CHIP_CFG_CLK_UART4_EN               0   /* APB1, ker_ck: CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC */
#define CHIP_CFG_CLK_UART5_EN               0   /* APB1, ker_ck: CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC */
#define CHIP_CFG_CLK_USART6_EN              0   /* APB2, ker_ck: CHIP_CFG_CLK_USART1_6_9_10_KER_SRC */
#define CHIP_CFG_CLK_UART7_EN               0   /* APB1, ker_ck: CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC */
#define CHIP_CFG_CLK_UART8_EN               0   /* APB1, ker_ck: CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC */
#define CHIP_CFG_CLK_UART9_EN               0   /* APB2, ker_ck: CHIP_CFG_CLK_USART1_6_9_10_KER_SRC (stm32h72x) */
#define CHIP_CFG_CLK_USART10_EN             0   /* APB2, ker_ck: CHIP_CFG_CLK_USART1_6_9_10_KER_SRC (stm32h72x) */
#define CHIP_CFG_CLK_LPUART1_EN             0   /* APB4, ker_ck: CHIP_CFG_CLK_LPUART1_KER_SRC */
/* spi */
#define CHIP_CFG_CLK_SPI1_EN                0   /* APB2, ker_ck: CHIP_CFG_CLK_SPI1_2_3_KER_SRC */
#define CHIP_CFG_CLK_SPI2_EN                0   /* APB1, ker_ck: CHIP_CFG_CLK_SPI1_2_3_KER_SRC */
#define CHIP_CFG_CLK_SPI3_EN                0   /* APB1, ker_ck: CHIP_CFG_CLK_SPI1_2_3_KER_SRC */
#define CHIP_CFG_CLK_SPI4_EN                0   /* APB2, ker_ck: CHIP_CFG_CLK_SPI4_5_KER_SRC */
#define CHIP_CFG_CLK_SPI5_EN                0   /* APB2, ker_ck: CHIP_CFG_CLK_SPI4_5_KER_SRC */
#define CHIP_CFG_CLK_SPI6_EN                0   /* APB4, ker_ck: CHIP_CFG_CLK_SPI6_KER_SRC */
/* qspi/octospi */
#define CHIP_CFG_CLK_QSPI_EN                0   /* AHB3, ker_ck: CHIP_CFG_CLK_QSPI_KER_SRC (!stm32h72x) */
#define CHIP_CFG_CLK_OCTOSPI1_EN            0   /* AHB3, ker_ck: CHIP_CFG_CLK_OCTOSPI_KER_SRC (stm32h72x) */
#define CHIP_CFG_CLK_OCTOSPI2_EN            0   /* AHB3, ker_ck: CHIP_CFG_CLK_OCTOSPI_KER_SRC (stm32h72x) */
#define CHIP_CFG_CLK_IOMNGR_EN              0   /* AHB3 */
#define CHIP_CFG_CLK_OTFD1_EN               0   /* AHB3 */
#define CHIP_CFG_CLK_OTFD2_EN               0   /* AHB3 */
/* i2c */
#define CHIP_CFG_CLK_I2C1_EN                0   /* APB1, ker_ck: CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC */
#define CHIP_CFG_CLK_I2C2_EN                0   /* APB1, ker_ck: CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC */
#define CHIP_CFG_CLK_I2C3_EN                0   /* APB1, ker_ck: CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC */
#define CHIP_CFG_CLK_I2C4_EN                0   /* APB4, ker_ck: CHIP_CFG_CLK_I2C4_KER_SRC */
#define CHIP_CFG_CLK_I2C5_EN                0   /* APB1, ker_ck: CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC (stm32h72x) */
/* sdmmc */
#define CHIP_CFG_CLK_SDMMC1_EN              0   /* AHB3, ker_ck: CHIP_CFG_CLK_SDMMC_KER_SRC */
#define CHIP_CFG_CLK_SDMMC2_EN              0   /* AHB2, ker_ck: CHIP_CFG_CLK_SDMMC_KER_SRC */
/* usb */
#define CHIP_CFG_CLK_USB1_OTGHS_EN          0   /* AHB1, ker_ck: CHIP_CFG_CLK_USB_KER_SRC */
#define CHIP_CFG_CLK_USB1_OTGHS_ULPI_EN     0   /* AHB1 */
#define CHIP_CFG_CLK_USB2_OTGHS_EN          0   /* AHB1, ker_ck: CHIP_CFG_CLK_USB_KER_SRC */
#define CHIP_CFG_CLK_USB2_OTGHS_ULPI_EN     0   /* AHB1 */
/* ethernet */
#define CHIP_CFG_CLK_ETH1_MAC_EN            0   /* AHB1 */
#define CHIP_CFG_CLK_ETH1_TX_EN             0   /* AHB1 */
#define CHIP_CFG_CLK_ETH1_RX_EN             0   /* AHB1 */
/* adc/dac */
#define CHIP_CFG_CLK_ADC1_EN                0   /* AHB1, ker_ck: CHIP_CFG_CLK_ADC_KER_SRC */
#define CHIP_CFG_CLK_ADC2_EN                0   /* AHB1, ker_ck: CHIP_CFG_CLK_ADC_KER_SRC */
#define CHIP_CFG_CLK_ADC3_EN                0   /* AHB4, ker_ck: CHIP_CFG_CLK_ADC_KER_SRC */
#define CHIP_CFG_CLK_DAC1_EN                0   /* APB1 */
#define CHIP_CFG_CLK_DAC2_EN                0   /* APB1 */
/* audio */
#define CHIP_CFG_CLK_SAI1_EN                0   /* APB2, ker_ck: CHIP_CFG_CLK_SAI1_KER_SRC */
#define CHIP_CFG_CLK_SAI2_EN                0   /* APB2, ker_ck: CHIP_CFG_CLK_SAI2_3_KER_SRC (!stm32h72x) */
#define CHIP_CFG_CLK_SAI3_EN                0   /* APB2, ker_ck: CHIP_CFG_CLK_SAI2_3_KER_SRC (!stm32h72x) */
#define CHIP_CFG_CLK_SAI4_EN                0   /* APB4, ker_ck: CHIP_CFG_CLK_SAI4A_KER_SRC, CHIP_CFG_CLK_SAI4B_KER_SRC */
#define CHIP_CFG_CLK_DFSDM1_EN              0   /* APB2, ker_ck: CHIP_CFG_CLK_DFSDM1_KER_SRC */
#define CHIP_CFG_CLK_SPDIFRX_EN             0   /* APB1, ker_ck: CHIP_CFG_CLK_SPDIFRX_KER_SRC */
/* video */
#define CHIP_CFG_CLK_DSI_EN                 0   /* APB3, ker_ck: CHIP_CFG_CLK_DSI_KER_SRC (stm32h747) */
#define CHIP_CFG_CLK_DVP_EN                 0   /* AHB2 */
#define CHIP_CFG_CLK_PSSI_EN                0   /* AHB2 (stm32h72x) */
#define CHIP_CFG_CLK_CEC_EN                 0   /* APB1 */
/* others */
#define CHIP_CFG_CLK_FDCAN_EN               0   /* APB1, ker_ck: CHIP_CFG_CLK_FDCAN_KER_SRC */
#define CHIP_CFG_CLK_SWPMI_EN               0   /* APB1, ker_ck: CHIP_CFG_CLK_SWPMI_KER_SRC */
#define CHIP_CFG_CLK_JPGDEC_EN              0   /* AHB3 (!stm32h72x) */
#define CHIP_CFG_CLK_CRYPT_EN               0   /* AHB2 */
#define CHIP_CFG_CLK_HASH_EN                0   /* AHB2 */
#define CHIP_CFG_CLK_RNG_EN                 0   /* AHB2, ker_ck: CHIP_CFG_CLK_RNG_KER_SRC */
#define CHIP_CFG_CLK_CRC_EN                 0   /* AHB4 */
#define CHIP_CFG_CLK_LTDC_EN                0   /* APB3 */
#define CHIP_CFG_CLK_WWDG1_EN               0   /* APB3 */
#define CHIP_CFG_CLK_CRS_EN                 0   /* APB1 */
#define CHIP_CFG_CLK_OPAMP_EN               0   /* APB1 */
#define CHIP_CFG_CLK_MDIOS_EN               0   /* APB1 */
#define CHIP_CFG_CLK_COMP12_EN              0   /* APB4 */
#define CHIP_CFG_CLK_VREF_EN                0   /* APB4 */
#define CHIP_CFG_CLK_DTS_EN                 0   /* APB4 (stm32h72x) */
#define CHIP_CFG_CLK_FMAC_EN                0   /* AHB2 (stm32h72x) */
#define CHIP_CFG_CLK_CORDIC_EN              0   /* AHB2 (stm32h72x) */



/* ---------------------------- Watchdog -------------------------------------*/
/* включать ли Watchdog (используется IWDG) */
#define CHIP_CFG_WDT_SETUP_EN               0
/* разрешение обновления энергонезависимых настроек WDT (требует CHIP_CFG_FLASH_FUNC_EN) */
#define CHIP_CFG_WDT_NV_SETUP_EN            0
/* Время сброса контроллера в мс, если WDT не будет сброшен  */
#define CHIP_CFG_WDT_TIMEOUT_MS             10000
/* Разрешение аппаратного режима WDT (включается всегда по сбросу,
 * иначе запускается программно при старте) */
#define CHIP_CFG_WDT_HW_MODE_EN             0
/* Разрешение работы WDT в Standby режиме процессора (иначе - останов) */
#define CHIP_CFG_WDT_ON_STANDBY_EN          1
/* Разрешение работы WDT в Stop режиме процессора (иначе - останов) */
#define CHIP_CFG_WDT_ON_STOP_EN             1

/* Разрешить ли настройку WDT для второго ядра (только при наличии).
   Остальные настройки WDT_REMCORE имеют значение, только если данная разрешена */
#define CHIP_CFG_WDT_REMCORE_SETUP_EN       0
/* Разрешение аппаратного режима WDT для второго ядра */
#define CHIP_CFG_WDT_REMCORE_HW_MODE_EN     0
/* Разрешение запуска WDT при старте для второго ядра */
#define CHIP_CFG_WDT_REMCORE_EN             0
/* Время сброса контроллера в мс, если WDT второго ядра не будет сброшен */
#define CHIP_CFG_WDT_REMCORE_TIMEOUT_MS     10000



/* ------------------------ cache --------------------------------------------*/
#define CHIP_CFG_CACHE_SETUP_EN             1  /* разрешение инициализации кеш-памяти */
#define CHIP_CFG_DCACHE_EN                  1  /* разрешение кэша данных */
#define CHIP_CFG_ICACHE_EN                  1  /* разрешение кэша кода */

/* --------------------------- MPU -------------------------------------------*/
#define CHIP_CFG_MPU_SETUP_EN               1 /* разрешение инициализации MPU */
#define CHIP_CFG_MPU_EN                     1 /* должен ли быть разрешен MPU */
#define CHIP_CFG_MPU_DEF_RGN_EN             1 /* разрешение фонового региона - для ненастроенных областей
                                                 используются аттрибуты по умолчанию
                                                 (только для привелегированного доступа),
                                                 иначе - нет доступа к этим регионам */
#define CHIP_CFG_MPU_NMI_FLT_EN             0 /* разрешение MPU при обработке NMI и Hard Fault */
#define CHIP_CFG_MPU_RGN_CNT                1 /* количество настраиваемых регионов MPU */


/* настройки для каждого региона CHIP_CFG_MPU_RGNx, где x = 1..CHIP_CFG_MPU_RGN_CNT */
#define CHIP_CFG_MPU_RGN1_ADDR              0xD0000000UL /* Адрес региона, должен быть выравнен на размер */
#define CHIP_CFG_MPU_RGN1_SIZE              CHIP_MB(32)  /* Размер региона. Должен быть степенью 2, от 32 байт до 4 Гбайт */
#define CHIP_CFG_MPU_RGN1_ATTR              CHIP_MPU_RGN_ATTR_NORMAL_NONCACHE_SHARE /* Атрибуты памяти. Одно из значений CHIP_MPU_RGN_ATTR_xxx из chip_core_mpu_defs.h */
#define CHIP_CFG_MPU_RGN1_ACC               CHIP_MPU_RGN_ACC_RW_NA /* Атрибуты доступа для привилегированного и непривилегированного ПО.
                                                                      Одно из значений CHIP_MPU_RGN_ACC_xxx  из chip_core_mpu_defs.h.
                                                                      Если не задано - полный доступ. */
#define CHIP_CFG_MPU_RGN1_NOEXEC            1  /* Признак запрета выполнения кода из данног региона (если не задано - резрешено) */
#define CHIP_CFG_MPU_RGN1_SRGN_DISMSK       0  /* Маска для исключения субрегионов (регион делится на 8 равных субрегионов) из региона.
                                                  Доступно только для региона размером 256 байт и более. Если не задано - 0 */


/* ----------------------------- FPU -----------------------------------------*/
#define CHIP_CFG_FPU_EN                     1 /* разрешение FPU (по умолчанию 1) */

/* ----------------------------- Flash ----------------------------------------*/
/* включение функций управление flash-памятью в сборку */
#define CHIP_CFG_FLASH_FUNC_EN              1
/* Максимальное кол-во параллельно изменяемых битов во flash-памяти (64, 32, 16, 8).
   Влияет на скорость записи/старания и максимальное пиковое потребление */
#define CHIP_CFG_FLASH_WR_PARALLEL_BITS     64


/* включение функций инициализации и назначения обработчиков прерываний MDMA */
#define CHIP_CFG_MDMA_FUNC_EN               1


/* ------------------------------- AXI QoS ----------------------------------*/
/* Приоритеты для ведущих устройств на AXI (0..15, по умолчанию все 0) */
#define CHIP_CFG_GPV_ARTD_RD_QOS        0 /* INI1: ART (non-buffered data) read  */
#define CHIP_CFG_GPV_ARTD_WR_QOS        0 /* INI1: ART (non-buffered data) write */
#define CHIP_CFG_GPV_CM7_RD_QOS         0 /* INI2: Cortex-M7 read */
#define CHIP_CFG_GPV_CM7_WR_QOS         0 /* INI2: Cortex-M7 write */
#define CHIP_CFG_GPV_SDMMC1_RD_QOS      0 /* INI3: SDMMC1 read */
#define CHIP_CFG_GPV_SDMMC1_WR_QOS      0 /* INI3: SDMMC1 write */
#define CHIP_CFG_GPV_MDMA_RD_QOS        0 /* INI4: MDMA read */
#define CHIP_CFG_GPV_MDMA_WR_QOS        0 /* INI4: MDMA write */
#define CHIP_CFG_GPV_DMA2D_RD_QOS       0 /* INI5: DMA2D read */
#define CHIP_CFG_GPV_DMA2D_WR_QOS       0 /* INI5: DMA2D write */
#define CHIP_CFG_GPV_LTDC_RD_QOS        0 /* INI6: LTDC read */
#define CHIP_CFG_GPV_LTDC_WR_QOS        0 /* INI6: LTDC write */
#define CHIP_CFG_GPV_ARTI_RD_QOS        0 /* INI7: ART (instruction fetch) read */
#define CHIP_CFG_GPV_ARTI_WR_QOS        0 /* INI7: ART (instruction fetch) write */




/* ----------------------------- ltimer --------------------------------------*/
/* Разрешение инициализации ltimer и включение возможности использования его для
 * вычисления таймаутов */
#define CHIP_CFG_LTIMER_EN              1

/* ----------------------------- PIN Interrupts ------------------------------*/
/* Разрешения кода обработки прерываний по внешним пинам. Код позволяет
 * независимо назначать обработчики и скрывает разделяемойсть одного вектора
 * несколькими ножками, а также выполняет сам анализ и сброс флагов PEND
 * (см. описание в chip_pinint.h) */
#define CHIP_CFG_PININT_ISR_PROC_EN      1

#endif /* CHIP_CONFIG_H */

