#ifndef RISCV_CORE_H
#define RISCV_CORE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief clear bits in csr
 *
 * @param csr_num specific csr
 * @param bit bits to be cleared
 */
#define chip_csr_clear(csr_num, bit) __asm volatile("csrc %0, %1" : : "i"(csr_num), "r"(bit))

/**
 * @brief read and clear bits in csr
 *
 * @param csr_num specific csr
 * @param bit bits to be cleared
 *
 * @return csr value before cleared
 */
#define chip_csr_read_clear(csr_num, bit) ({ volatile uint32_t v = 0; __asm volatile("csrrc %0, %1, %2" : "=r"(v) : "i"(csr_num), "r"(bit)); v; })

/**
 * @brief read and set bits in csr
 *
 * @param csr_num specific csr
 * @param bit bits to be set
 *
 * @return csr value before set
 */
#define chip_read_csr_set(csr_num, bit) ({ volatile uint32_t v = 0; __asm volatile("csrrs %0, %1, %2" : "=r"(v) : "i"(csr_num), "r"(bit)); v; })

/**
 * @brief set bits in csr
 *
 * @param csr_num specific csr
 * @param bit bits to be set
 */
#define chip_csr_set(csr_num, bit) __asm volatile("csrs %0, %1" : : "i"(csr_num), "r"(bit))

/**
 * @brief write value to csr
 *
 * @param csr_num specific csr
 * @param v value to be written
 */
#define chip_csr_write(csr_num, v) __asm volatile("csrw %0, %1" : : "i"(csr_num), "r"(v))

/**
 * @brief read value of specific csr
 *
 * @param csr_num specific csr
 *
 * @return csr value
 */
#define chip_csr_read(csr_num) ({ uint32_t v; __asm volatile("csrr %0, %1" : "=r"(v) : "i"(csr_num)); v; })

#define chip_csr_read_v(csr_num, v) __asm volatile("csrr %0, %1" : "=r"(v) : "i"(csr_num));

/**
 * @brief execute fence.i
 *
 */
#define chip_fencei() __asm volatile("fence.i")

/**
 * @brief enable fpu
 */
#define chip_fpu_enable() chip_read_csr_set(CSR_MSTATUS, CSR_MSTATUS_FS_MASK)

/**
 * @brief disable fpu
 */
#define chip_fpu_disable() chip_csr_read_clear(CSR_MSTATUS, CSR_MSTATUS_FS_MASK)


/**
 * @brief  write fp csr
 *
 * @param v value to be set
 */
#define chip_fcsr_write(v) __asm volatile("fscsr %0" : : "r"(v))

/**
 * @brief read fp csr
 *
 * @return fp csr value
 */
#define chip_fcsr_read() ({ uint32_t v; __asm volatile("frcsr %0" : "=r"(v)); v; })

/**
 * @brief clear fcsr
 */
#define chip_fcsr_clear() chip_fcsr_write(0)

#ifdef __cplusplus
}
#endif


#endif /* RISCV_CORE_H */
