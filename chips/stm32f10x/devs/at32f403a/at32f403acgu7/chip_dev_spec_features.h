#ifndef CHIP_DEV_SPEC_FEATURES_H
#define CHIP_DEV_SPEC_FEATURES_H

#include "chip_pkg_types.h"

#define CHIP_DEV_PKG                    CHIP_PKG_QFN48
#define CHIP_FLASH_MEM_SIZE             (1024*1024)
#define CHIP_RAM_MEM_SIZE               ((96 + 128)*1024)

#endif // CHIP_DEV_SPEC_FEATURES_H
