#ifndef REGS_RCC_H
#define REGS_RCC_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR;         /*!< RCC clock control register,                                  Address offset: 0x00 */
    __IO uint32_t CFGR;       /*!< RCC clock configuration register,                            Address offset: 0x04 */
    __IO uint32_t CIR;        /*!< RCC clock interrupt register,                                Address offset: 0x08 */
    __IO uint32_t APB2RSTR;   /*!< RCC APB2 peripheral reset register,                          Address offset: 0x0C */
    __IO uint32_t APB1RSTR;   /*!< RCC APB1 peripheral reset register,                          Address offset: 0x10 */
    __IO uint32_t AHBENR;     /*!< RCC AHB peripheral clock register,                           Address offset: 0x14 */
    __IO uint32_t APB2ENR;    /*!< RCC APB2 peripheral clock enable register,                   Address offset: 0x18 */
    __IO uint32_t APB1ENR;    /*!< RCC APB1 peripheral clock enable register,                   Address offset: 0x1C */
    __IO uint32_t BDCR;       /*!< RCC Backup domain control register,                          Address offset: 0x20 */
    __IO uint32_t CSR;        /*!< RCC clock control & status register,                         Address offset: 0x24 */
    __IO uint32_t AHBRSTR;    /*!< RCC AHB peripheral reset register,                           Address offset: 0x28 */
} CHIP_REGS_RCC_T;

#define CHIP_REGS_RCC                   ((CHIP_REGS_RCC_T *) CHIP_MEMRGN_ADDR_PERIPH_RCC)

#define CHIP_REG_HSI_LP_TRIM            (*((__I uint32_t *) (CHIP_MEMRGN_ADDR_VENDOR + 0x2A))) /* HSI low power mode trim value */

/*********** Clock control register (RCC_CR) **********************************/
#define CHIP_REGFLD_RCC_CR_HSI_ON                   (0x00000001UL <<  0U)   /* (RW) HSI clock enable */
#define CHIP_REGFLD_RCC_CR_HSI_RDY                  (0x00000001UL <<  1U)   /* (RO) HSI clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSI_LP                   (0x00000001UL <<  2U)   /* (RW) HIS internal low-power mode (frequency is reduced to 1MHz, use TRIM from CHIP_REG_HSI_LP_TRIM) */
#define CHIP_REGFLD_RCC_CR_HSI_TRIM                 (0x0000001FUL <<  3U)   /* (RW) HSI clock trimming */
#define CHIP_REGFLD_RCC_CR_HSI_CAL                  (0x000000FFUL <<  8U)   /* (RO) HSI clock calibration */
#define CHIP_REGFLD_RCC_CR_HSE_ON                   (0x00000001UL << 16U)   /* (RW) HSE clock enable */
#define CHIP_REGFLD_RCC_CR_HSE_RDY                  (0x00000001UL << 17U)   /* (RO) HSE clock ready flag */
#define CHIP_REGFLD_RCC_CR_HSE_BYP                  (0x00000001UL << 18U)   /* (RW) HSE crystal oscillator bypass */
#define CHIP_REGFLD_RCC_CR_CSS_ON                   (0x00000001UL << 19U)   /* (RW) Clock security system enable */
#define CHIP_REGFLD_RCC_CR_HSE_LP                   (0x00000001UL << 20U)   /* (RW) HSE low-power mode */
#define CHIP_REGFLD_RCC_CR_PLL1_ON                  (0x00000001UL << 24U)   /* (RW) PLL1 enable */
#define CHIP_REGFLD_RCC_CR_PLL1_RDY                 (0x00000001UL << 25U)   /* (RO) PLL1 clock ready flag */
#define CHIP_REGMSK_RCC_CR_RESERVED                 (0xFCE00000UL)


/*********** Clock configuration register (RCC_CFGR) **************************/
#define CHIP_REGFLD_RCC_CFGR_SW                     (0x00000003UL <<  0U)   /* (RW) System clock switch */
#define CHIP_REGFLD_RCC_CFGR_SWS                    (0x00000003UL <<  2U)   /* (RO) System clock switch status */
#define CHIP_REGFLD_RCC_CFGR_HPRE                   (0x0000000FUL <<  4U)   /* (RW) HCLK prescaler */
#define CHIP_REGFLD_RCC_CFGR_PPRE1                  (0x00000007UL <<  8U)   /* (RW) PCLK1 prescaler */
#define CHIP_REGFLD_RCC_CFGR_PPRE2                  (0x00000007UL << 11U)   /* (RW) PCLK2 prescaler */
#define CHIP_REGFLD_RCC_CFGR_ADC_PRE                (0x00000003UL << 14U)   /* (RW) ADC prescaler  */
#define CHIP_REGFLD_RCC_CFGR_PLL1_SRC               (0x00000001UL << 16U)   /* (RW) PLL1 input clock source */
#define CHIP_REGFLD_RCC_CFGR_PLL1_XTPRE             (0x00000001UL << 17U)   /* (RW) HSE divider for PLL1 input clock (= CFGR2_PREDIV) */
#define CHIP_REGFLD_RCC_CFGR_PLL1_MUL               (0x0000000FUL << 18U)   /* (RW) PLL1 multiplication factor */
#define CHIP_REGFLD_RCC_CFGR_USB_PRE                (0x00000003UL << 22U)   /* (RW) USB OTG FS prescaler */
#define CHIP_REGFLD_RCC_CFGR_MCO                    (0x00000007UL << 24U)   /* (RW) Microcontroller clock output */
#define CHIP_REGFLD_RCC_CFGR_ADC_DUTY_CHG           (0x00000007UL << 28U)   /* (RW) ADC clock duty cycle control bit  (ADC clock high is 50% + n HCLK);*/
#define CHIP_REGFLD_RCC_CFGR_ADC_PRE_ADJ            (0x00000001UL << 31U)   /* (RW) ADC input clock selection */


#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSI              0
#define CHIP_REGFLDVAL_RCC_CFGR_SW_HSE              1
#define CHIP_REGFLDVAL_RCC_CFGR_SW_PLL1             2


#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_2              8
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_4              9
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_8              10
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_16             11
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_64             12
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_128            13
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_256            14
#define CHIP_REGFLDVAL_RCC_CFGR_HPRE_512            15

#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_1              0
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_2              4
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_4              5
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_8              6
#define CHIP_REGFLDVAL_RCC_CFGR_PPRE_16             7

#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_2           0
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_4           1
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_6           2
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_8           3

#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_SRC_HSI        0 /* HSI/(1 or 2)  */
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_SRC_HSE        1 /* HSE/PREDIV */


#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_2          0
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_3          1
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_4          2
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_5          3
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_6          4
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_7          5
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_8          6
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_9          7
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_10         8
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_11         9
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_12         10
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_13         11
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_14         12
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_15         13
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_16         14
#define CHIP_REGFLDVAL_RCC_CFGR_PLL1_MUL_18         15


#define CHIP_REGFLDVAL_RCC_CFGR_USB_PRE_1           0
#define CHIP_REGFLDVAL_RCC_CFGR_USB_PRE_2           1
#define CHIP_REGFLDVAL_RCC_CFGR_USB_PRE_1_5         2


#define CHIP_REGFLDVAL_RCC_CFGR_MCO_DIS             0x0 /* MCO output disabled, no clock on MCO */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_SYSCLK          0x4 /* System clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSI             0x5 /* Internal RC 8 MHz (HSI) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_HSE             0x6 /* External 4-32 MHz (HSE) oscillator clock selected */
#define CHIP_REGFLDVAL_RCC_CFGR_MCO_PLL1_DIV2       0x7 /* PLL1 clock divided by 2 selected */


#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_ADJ_HCLK    0 /* HCLK */
#define CHIP_REGFLDVAL_RCC_CFGR_ADC_PRE_ADJ_PCLK2   1 /* PCLK2  div ADCPRE */


/*********** Clock interrupt register (RCC_CIR) *******************************/
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_F               (0x00000001UL <<  0U)   /* (RO) LSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_F               (0x00000001UL <<  1U)   /* (RO) LSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_F               (0x00000001UL <<  2U)   /* (RO) HSI ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_F               (0x00000001UL <<  3U)   /* (RO) HSE ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_PLL1_RDY_F              (0x00000001UL <<  4U)   /* (RO) PLL1 ready interrupt flag */
#define CHIP_REGFLD_RCC_CIR_CSS_F                   (0x00000001UL <<  7U)   /* (RO) Clock security system interrupt flag */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_IE              (0x00000001UL <<  8U)   /* (RW) LSI ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_IE              (0x00000001UL <<  9U)   /* (RW) LSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_IE              (0x00000001UL << 10U)   /* (RW) HSI ready interrupt enable  */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_IE              (0x00000001UL << 11U)   /* (RW) HSE ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_PLL1_RDY_IE             (0x00000001UL << 12U)   /* (RW) PLL1 ready interrupt enable */
#define CHIP_REGFLD_RCC_CIR_LSI_RDY_C               (0x00000001UL << 16U)   /* (W1C) LSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_LSE_RDY_C               (0x00000001UL << 17U)   /* (W1C) LSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSI_RDY_C               (0x00000001UL << 18U)   /* (W1C) HSI ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_HSE_RDY_C               (0x00000001UL << 19U)   /* (W1C) HSE ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_PLL1_RDY_C              (0x00000001UL << 20U)   /* (W1C) PLL ready interrupt clear */
#define CHIP_REGFLD_RCC_CIR_CSS_C                   (0x00000001UL << 23U)   /* (W1C) Clock security system interrupt clear */
#define CHIP_REGMSK_RCC_CIR_RESERVED                (0xFF60E0C0UL)


/*********** APB peripheral reset register 2 (RCC_APB2RSTR) *******************/
#define CHIP_REGFLD_RCC_APB2RSTR_AFIO_RST           (0x00000001UL <<  0U)   /* (RW) AFIO reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPA_RST           (0x00000001UL <<  2U)   /* (RW) IP Port A reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPB_RST           (0x00000001UL <<  3U)   /* (RW) IP Port B reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPC_RST           (0x00000001UL <<  4U)   /* (RW) IP Port C reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_IOPD_RST           (0x00000001UL <<  5U)   /* (RW) IP Port D reset*/
#define CHIP_REGFLD_RCC_APB2RSTR_ADC1_RST           (0x00000001UL <<  9U)   /* (RW) ADC1 interface reset */
#define CHIP_REGFLD_RCC_APB2RSTR_TIM1_RST           (0x00000001UL << 11U)   /* (RW) TIM1 timer reset */
#define CHIP_REGFLD_RCC_APB2RSTR_SPI1_RST           (0x00000001UL << 12U)   /* (RW) SPI1 reset */
#define CHIP_REGFLD_RCC_APB2RSTR_USART1_RST         (0x00000001UL << 14U)   /* (RW) USART1 reset */


/*********** APB peripheral reset register 1 (RCC_APB1RSTR) *******************/
#define CHIP_REGFLD_RCC_APB1RSTR_TIM2_RST           (0x00000001UL <<  0U)   /* (RW) TIM2 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM3_RST           (0x00000001UL <<  1U)   /* (RW) TIM3 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_TIM4_RST           (0x00000001UL <<  2U)   /* (RW) TIM4 timer reset */
#define CHIP_REGFLD_RCC_APB1RSTR_WWDG_RST           (0x00000001UL << 11U)   /* (RW) Window watchdog reset */
#define CHIP_REGFLD_RCC_APB1RSTR_SPI2_RST           (0x00000001UL << 14U)   /* (RW) SPI2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART2_RST         (0x00000001UL << 17U)   /* (RW) USART2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART3_RST         (0x00000001UL << 18U)   /* (RW) USART3 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_USART4_RST         (0x00000001UL << 19U)   /* (RW) USART4 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C1_RST           (0x00000001UL << 21U)   /* (RW) I2C1 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_I2C2_RST           (0x00000001UL << 22U)   /* (RW) I2C2 reset */
#define CHIP_REGFLD_RCC_APB1RSTR_CAN1_RST           (0x00000001UL << 25U)   /* (RW) CAN1 interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_BKP_RST            (0x00000001UL << 27U)   /* (RW) Clock Recovery System interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_PWR_RST            (0x00000001UL << 28U)   /* (RW) Power interface reset */
#define CHIP_REGFLD_RCC_APB1RSTR_LPTIM1_RST         (0x00000001UL << 31U)   /* (RW) LPTIM interface reset */


/*********** AHB peripheral clock enable register (RCC_AHBENR) ****************/
#define CHIP_REGFLD_RCC_AHBENR_DMA1_EN              (0x00000001UL <<  0U)   /* (RW) DMA1 clock enable */
#define CHIP_REGFLD_RCC_AHBENR_SRAM_EN              (0x00000001UL <<  2U)   /* (RW) SRAM interface clock enable */
#define CHIP_REGFLD_RCC_AHBENR_CRC_EN               (0x00000001UL <<  6U)   /* (RW) CRC clock enable */
#define CHIP_REGFLD_RCC_AHBENR_USBFS_EN             (0x00000001UL << 12U)   /* (RW) USB FS clock enable */
#define CHIP_REGFLD_RCC_AHBENR_USBPD_EN             (0x00000001UL << 17U)   /* (RW) USB PD clock enable */


/*********** APB peripheral clock enable register 2 (RCC_APB2ENR) ****************/
#define CHIP_REGFLD_RCC_APB2ENR_AFIO_EN             (0x00000001UL <<  0U)   /* (RW) AFIO clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPA_EN             (0x00000001UL <<  2U)   /* (RW) IO Port A clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPB_EN             (0x00000001UL <<  3U)   /* (RW) IO Port B clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPC_EN             (0x00000001UL <<  4U)   /* (RW) IO Port C clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_IOPD_EN             (0x00000001UL <<  5U)   /* (RW) IO Port D clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_ADC1_EN             (0x00000001UL <<  9U)   /* (RW) ADC1 interface clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_TIM1_EN             (0x00000001UL << 11U)   /* (RW) TIM1 timer clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_SPI1_EN             (0x00000001UL << 12U)   /* (RW) SPI1 clock enable */
#define CHIP_REGFLD_RCC_APB2ENR_USART1_EN           (0x00000001UL << 14U)   /* (RW) USART1 clock enable */


/*********** APB peripheral clock enable register 1 (RCC_APB1ENR) ****************/
#define CHIP_REGFLD_RCC_APB1ENR_TIM2_EN             (0x00000001UL <<  0U)   /* (RW) TIM2 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM3_EN             (0x00000001UL <<  1U)   /* (RW) TIM3 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_TIM4_EN             (0x00000001UL <<  2U)   /* (RW) TIM4 timer clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_WWDG_EN             (0x00000001UL << 11U)   /* (RW) Window watchdog clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_SPI2_EN             (0x00000001UL << 14U)   /* (RW) SPI2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART2_EN           (0x00000001UL << 17U)   /* (RW) USART2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART3_EN           (0x00000001UL << 18U)   /* (RW) USART3 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_USART4_EN           (0x00000001UL << 19U)   /* (RW) USART4 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C1_EN             (0x00000001UL << 21U)   /* (RW) I2C1 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_I2C2_EN             (0x00000001UL << 22U)   /* (RW) I2C2 clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_CAN1_EN             (0x00000001UL << 25U)   /* (RW) CAN1 interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_BKP_EN              (0x00000001UL << 27U)   /* (RW) Backup interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_PWR_EN              (0x00000001UL << 28U)   /* (RW) Power interface clock enable */
#define CHIP_REGFLD_RCC_APB1ENR_LPTIM1_EN           (0x00000001UL << 31U)   /* (RW) LPTIM interface clock enable */


/*********** RTC domain control register (RCC_BDCR) ***************************/
#define CHIP_REGFLD_RCC_BDCR_LSE_ON                 (0x00000001UL <<  0U)   /* (RW) LSE oscillator enable */
#define CHIP_REGFLD_RCC_BDCR_LSE_RDY                (0x00000001UL <<  1U)   /* (RO) LSE oscillator ready */
#define CHIP_REGFLD_RCC_BDCR_LSE_BYP                (0x00000001UL <<  2U)   /* (RW) LSE oscillator bypass */
#define CHIP_REGFLD_RCC_BDCR_RTC_SEL                (0x00000003UL <<  8U)   /* (RW) RTC clock source selection */
#define CHIP_REGFLD_RCC_BDCR_RTC_EN                 (0x00000001UL << 15U)   /* (RW) RTC clock enable */
#define CHIP_REGFLD_RCC_BDCR_BD_RST                 (0x00000001UL << 16U)   /* (RW) RTC domain software reset */
#define CHIP_REGMSK_RCC_BDCR_RESERVED               (0xFFFE7CF8UL)

#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_DIS         0
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSE         1
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_LSI         2
#define CHIP_REGFLDVAL_RCC_BDCR_RTC_SEL_HSE         3 /* ch32v307 - HSE/128 */

/*********** Control/status register (RCC_CSR) ********************************/
#define CHIP_REGFLD_RCC_CSR_LSI_ON                  (0x00000001UL <<  0U)   /* (RW) LSI oscillator enable */
#define CHIP_REGFLD_RCC_CSR_LSI_RDY                 (0x00000001UL <<  1U)   /* (RO) LSI oscillator ready */
#define CHIP_REGFLD_RCC_CSR_RMVF                    (0x00000001UL << 24U)   /* (W1) Remove reset flag */
#define CHIP_REGFLD_RCC_CSR_PIN_RSTF                (0x00000001UL << 26U)   /* (RO) PIN reset flag */
#define CHIP_REGFLD_RCC_CSR_POR_RSTF                (0x00000001UL << 27U)   /* (RO) POR/PDR reset flag */
#define CHIP_REGFLD_RCC_CSR_SFT_RSTF                (0x00000001UL << 28U)   /* (RO) Software reset flag */
#define CHIP_REGFLD_RCC_CSR_IWDG_RSTF               (0x00000001UL << 29U)   /* (RO) Independent watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_WWDG_RSTF               (0x00000001UL << 30U)   /* (RO) Window watchdog reset flag */
#define CHIP_REGFLD_RCC_CSR_LPWR_RSTF               (0x00000001UL << 31U)   /* (RO) Low-power reset flag */
#define CHIP_REGMSK_RCC_CSR_RESERVED                (0x02FFFFFCUL)

/*********** AHB peripheral reset register (RCC_AHBRSTR) **********************/
#define CHIP_REGFLD_RCC_AHBRSTR_USBFS_RST           (0x00000001UL << 12U)   /* (RW) USB FS reset */
#define CHIP_REGFLD_RCC_AHBRSTR_USBPD_RST           (0x00000001UL << 15U)   /* (RW) USB PD reset */
#define CHIP_REGMSK_RCC_AHBRSTR_RESERVED            (0xFFFDEFFFUL)


#endif // REGS_RCC_H
