#ifndef REGS_VENDOR_INFO_H
#define REGS_VENDOR_INFO_H

#include "chip_ioreg_defs.h"

typedef struct {
    uint32_t RESERVED;
    __I uint32_t CHIPID;        /* Chip ID,         Address 0x1FFFF704 */
    uint32_t RESERVED2[6];
    __I uint32_t TS;            /* TS,              Address 0x1FFFF720 */
    __I uint32_t OPA_TRIM;      /* OPA_TRIM,        Address 0x1FFFF724 */
    __I uint16_t ADC_TRIM;      /* ADC_TRIM,        Address 0x1FFFF728 */
    uint16_t RESERVED3;
    __I uint32_t HSI_LP_TRIM;   /* HSI_LP_TRIM,     Address 0x1FFFF72A */
    __I uint16_t USBPD_CFG;     /* USBPD_CFG,       Address 0x1FFFF730 */
} CHIP_REGS_VENDOR_INFO_T;

#define CHIP_REGS_VENDOR_INFO                       ((CHIP_REGS_VENDOR_INFO_T *) CHIP_MEMRGN_ADDR_VENDOR)

#endif // REGS_VENDOR_INFO_H
