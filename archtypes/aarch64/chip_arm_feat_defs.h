#ifndef CHIP_ARM_FEAT_H
#define CHIP_ARM_FEAT_H



/* ID_AA64ISAR0_EL1, AArch64 Instruction Set Attribute Register 0 */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_AES          (0x000000000000000FULL <<  4U) /* (FEAT_AES, FEAT_PMULL) Support for AES instructions in AArch64 state */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_SHA1         (0x000000000000000FULL <<  8U) /* (FEAT_SHA1) Support for SHA1 instructions in AArch64 state */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_SHA2         (0x000000000000000FULL << 12U) /* (FEAT_SHA256, FEAT_SHA512) Support for SHA2 instructions in AArch64 state */ */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_CRC32        (0x000000000000000FULL << 16U) /* (FEAT_CRC32) Support for CRC32 instructions in AArch64 state*/
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_Atomic       (0x000000000000000FULL << 20U) /* (FEAT_LSE) Support for Atomic instructions in AArch64 state */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_TME          (0x000000000000000FULL << 24U) /* Support for TME instructions */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_RDM          (0x000000000000000FULL << 28U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_SHA3         (0x000000000000000FULL << 32U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_SM3          (0x000000000000000FULL << 36U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_SM4          (0x000000000000000FULL << 40U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_DP           (0x000000000000000FULL << 44U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_FHM          (0x000000000000000FULL << 48U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_TS           (0x000000000000000FULL << 52U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_TLB          (0x000000000000000FULL << 56U) /* (FEAT_TLBIOS, FEAT_TLBIRANGE) Support for Outer Shareable and TLB range maintenance instructions */
#define CHIP_REGFLD_AARCH_ID_AA64ISAR0_RNDR         (0x000000000000000FULL << 60U) /* (FEAT_RNG) support for Random Number instructions in AArch64 state */




/* AArch64 Memory Model Feature Register 0 (EL1) */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_PARange      (0x000000000000000FULL <<  0U) /* Physical Address range supported */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_ASIDBits     (0x000000000000000FULL <<  4U) /* Number of ASID bits (0 - 8, 2 - 16) */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_BigEnd       (0x000000000000000FULL <<  8U) /* Indicates support for mixed-endian configuration */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_SNSMem       (0x000000000000000FULL << 12U) /* Indicates support for a distinction between Secure and Non-secure Memory */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_BigEndEL0    (0x000000000000000FULL << 16U) /* Indicates support for mixed-endian at EL0 only */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_TGran16      (0x000000000000000FULL << 20U) /* Indicates support for 16KB memory translation granule size */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_TGran64      (0x000000000000000FULL << 24U) /* Indicates support for 64KB memory translation granule size */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_TGran4       (0x000000000000000FULL << 28U) /* Indicates support for 4KB memory translation granule size */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_TGran16_2    (0x000000000000000FULL << 32U) /* Indicates support for 16KB memory granule size at stage 2 */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_TGran64_2    (0x000000000000000FULL << 36U) /* Indicates support for 64KB memory granule size at stage 2 */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_TGran4_2     (0x000000000000000FULL << 40U) /* Indicates support for 4KB memory granule size at stage 2 */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_ExS          (0x000000000000000FULL << 44U) /* (FEAT_ExS) Indicates support for disabling context synchronizing exception entry and exit */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_FGT          (0x000000000000000FULL << 56U) /* Indicates presence of the Fine-Grained Trap controls */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR0_ECV          (0x000000000000000FULL << 60U) /* Presence of Enhanced Counter Virtualization  */

/* AArch64 Memory Model Feature Register 1 (EL1) */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_HAFDBS       (0x000000000000000FULL <<  0U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_VMIDBits     (0x000000000000000FULL <<  4U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_VH           (0x000000000000000FULL <<  8U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_HPDS         (0x000000000000000FULL << 12U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_LO           (0x000000000000000FULL << 16U) /*  */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_PAN          (0x000000000000000FULL << 20U) /* (FEAT_PAN1/2/3) Privileged Access Never */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_SpecSEI      (0x000000000000000FULL << 24U) /* PE can generate SError interrupt exceptions from speculative reads of memory, including speculative instruction fetches. */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_XNX          (0x000000000000000FULL << 28U) /* (FEAT_XNX)    Support for execute-never control distinction by Exception level at stage 2 */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_TWED         (0x000000000000000FULL << 32U) /* (FEAT_TWED)   Support for the configurable delayed trapping of WFE */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_ETS          (0x000000000000000FULL << 36U) /* (FEAT_ETS2)   Support for Enhanced Translation Synchronization */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_HCX          (0x000000000000000FULL << 40U) /* (FEAT_HCX)    Support for HCRX_EL2 and its associated EL3 trap */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_AFP          (0x000000000000000FULL << 44U) /* (FEAT_AFP)    Support for FPCR.{AH, FIZ, NEP} */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_nTLBPA       (0x000000000000000FULL << 48U) /* (FEAT_nTLBPA) Support for intermediate caching of translation table walks */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_TIDCP1       (0x000000000000000FULL << 52U) /* (FEAT_TIDCP1) SCTLR.TIDCP is implemented in AArch64 state */
#define CHIP_REGFLD_AARCH_ID_AA64MMFR1_CMOW         (0x000000000000000FULL << 56U) /* (FEAT_CMOW)   Indicates support for cache maintenance instruction permission */


/*.... */

/* ID_AA64PFR0, AArch64 Processor Feature Register 0 (EL1)*/
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_EL0           (0x000000000000000FULL <<  0U) /* EL0 Exception level handling. (1 - aarch64, 2 - both) */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_EL1           (0x000000000000000FULL <<  4U) /* EL1 Exception level handling. (1 - aarch64, 2 - both) */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_EL2           (0x000000000000000FULL <<  8U) /* EL2 Exception level handling. (1 - aarch64, 2 - both) */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_EL3           (0x000000000000000FULL << 12U) /* EL3 Exception level handling. (1 - aarch64, 2 - both) */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_FP            (0x000000000000000FULL << 16U) /* Floating-point (0 - implemented, 1 - +half-prec, 0xF - not implemented) */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_AdvSIMD       (0x000000000000000FULL << 20U) /* Advanced SIMD  (0 - implemented, 1 - +half-prec, 0xF - not implemented) */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_GIC           (0x000000000000000FULL << 24U) /* System register GIC CPU interface (1 - v3/v4, 3 - v4.1) */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_RAS           (0x000000000000000FULL << 28U) /* (FEAT_RAS, FEAT_RASv1p1, FEAT_DoubleFault) RAS Extension version */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_SVE           (0x000000000000000FULL << 32U) /* (FEAT_SVE) Scalable Vector Extension */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_SEL2          (0x000000000000000FULL << 36U) /* (FEAT_SEL2) Secure EL2 */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_MPAM          (0x000000000000000FULL << 40U) /* (FEAT_MPAM) Major version number of support for the MPAM Extension. */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_AMU           (0x000000000000000FULL << 44U) /* (FEAT_AMUv1, FEAT_AMUv1p1) support for Activity Monitors Extension.  */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_DIT           (0x000000000000000FULL << 48U) /* (FEAT_DIT) Data Independent Timing. */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_RME           (0x000000000000000FULL << 52U) /* (FEAT_RME) Realm Management Extension */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_CSV2          (0x000000000000000FULL << 56U) /* (FEAT_CSV2, FEAT_CSV2_2, FEAT_CSV2_3) Speculative use of out of context branch targets */
#define CHIP_REGFLD_AARCH_ID_AA64PFR0_CSV3          (0x000000000000000FULL << 60U) /* (FEAT_CSV3) Speculative use of faulting data */

/* ID_AA64PFR1, AArch64 Processor Feature Register 1 (EL1)*/
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_BT            (0x000000000000000FULL <<  0U) /* (FEAT_BTI) Branch Target Identification mechanism support in AArch64 state.  */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_SSBS          (0x000000000000000FULL <<  4U) /* (FEAT_SSBS, FEAT_SSBS2) Speculative Store Bypassing controls in AArch64 state */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_MTE           (0x000000000000000FULL <<  8U) /* (FEAT_MTE, FEAT_MTE2, FEAT_MTE3) Support for the Memory Tagging Extension */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_RAS_frac      (0x000000000000000FULL << 12U) /* RAS Extension fractional field */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_MPAM_frac     (0x000000000000000FULL << 16U) /* The minor version number of support for the MPAM Extension */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_SME           (0x000000000000000FULL << 24U) /* (FEAT_SME, FEAT_SME2) Scalable Matrix Extension.  */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_RNDR_trap     (0x000000000000000FULL << 28U) /* (FEAT_RNG_TRAP) Random Number trap to EL3 field */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_CSV2_frac     (0x000000000000000FULL << 32U) /* CSV2 fractional field */
#define CHIP_REGFLD_AARCH_ID_AA64PFR1_NMI           (0x000000000000000FULL << 36U) /* (FEAT_NMI) Support for Non-maskable interrupts (SCTLR_ELx.{SPINTMASK, NMI} and PSTATE.ALLINT) */




#endif // CHIP_ARM_FEAT_H
