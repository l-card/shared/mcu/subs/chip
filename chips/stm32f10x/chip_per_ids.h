#ifndef CHIP_PER_IDS_H
#define CHIP_PER_IDS_H

#include "chip_devtype_spec_features.h"

#define CHIP_PER_ID_DMA1        0 /* AHB, clk only */
#define CHIP_PER_ID_DMA2        1 /* AHB, clk only */
#define CHIP_PER_ID_SRAM        2 /* AHB, clk only */
#define CHIP_PER_ID_FLASH       3 /* AHB, clk only */
#define CHIP_PER_ID_CRC         4 /* AHB, clk only */
#if CHIP_DEV_SUPPORT_EXMC
#define CHIP_PER_ID_EXMC        4 /* AHB, clk only */
#endif

#define CHIP_PER_ID_AFIO        5 /* APB2 */
#define CHIP_PER_ID_GPIOA       6 /* APB2 */
#define CHIP_PER_ID_GPIOB       7 /* APB2 */
#define CHIP_PER_ID_GPIOC       8 /* APB2 */
#define CHIP_PER_ID_GPIOD       9 /* APB2 */
#define CHIP_PER_ID_GPIOE       10 /* APB2 */
#define CHIP_PER_ID_GPIOF       11 /* APB2 */
#define CHIP_PER_ID_GPIOG       12 /* APB2 */



#define CHIP_PER_ID_TIM1        13 /* APB2 */
#define CHIP_PER_ID_TIM2        14 /* APB1 */
#define CHIP_PER_ID_TIM3        15 /* APB1 */
#define CHIP_PER_ID_TIM4        16 /* APB1 */
#define CHIP_PER_ID_TIM5        17 /* APB1 */
#define CHIP_PER_ID_TIM6        18 /* APB1 */
#define CHIP_PER_ID_TIM7        19 /* APB1 */
#define CHIP_PER_ID_TIM8        20 /* APB1 */
#define CHIP_PER_ID_TIM9        21 /* APB2 */
#define CHIP_PER_ID_TIM10       22 /* APB2 */
#define CHIP_PER_ID_TIM11       23 /* APB2 */
#if CHIP_DEV_SUPPORT_TIM12
#define CHIP_PER_ID_TIM12       24 /* APB1 */
#endif
#if CHIP_DEV_SUPPORT_TIM13
#define CHIP_PER_ID_TIM13       25 /* APB1 */
#endif
#if CHIP_DEV_SUPPORT_TIM14
#define CHIP_PER_ID_TIM14       26 /* APB1 */
#endif

#define CHIP_PER_ID_USART1      27 /* APB2 */
#define CHIP_PER_ID_USART2      28 /* APB1 */
#define CHIP_PER_ID_USART3      29 /* APB1 */
#if CHIP_DEV_UART_CNT >= 4
#define CHIP_PER_ID_UART4       30 /* APB1 */
#endif
#if CHIP_DEV_UART_CNT >= 5
#define CHIP_PER_ID_UART5       31 /* APB1 */
#endif
#if CHIP_DEV_UART_CNT >= 6
#define CHIP_PER_ID_UART6       32 /* APB1 */
#endif
#if CHIP_DEV_UART_CNT >= 7
#define CHIP_PER_ID_UART7       33 /* APB1 */
#endif
#if CHIP_DEV_UART_CNT >= 8
#define CHIP_PER_ID_UART8       34 /* APB1 */
#endif

#define CHIP_PER_ID_SPI1        35 /* APB2 */
#define CHIP_PER_ID_SPI2        36 /* APB1 */
#if CHIP_DEV_SPI_CNT >= 3
#define CHIP_PER_ID_SPI3        37 /* APB1 */
#endif

#define CHIP_PER_ID_I2C1        38 /* APB1 */
#define CHIP_PER_ID_I2C2        39 /* APB1 */
#define CHIP_PER_ID_USBD        40 /* APB1 */
#define CHIP_PER_ID_USBHD       41 /* AHB  */
#if CHIP_DEV_USBHS_CNT >= 1
#define CHIP_PER_ID_USBHS       42 /* AHB, clk only */
#endif

#if CHIP_DEV_CAN_CNT >= 1
#define CHIP_PER_ID_CAN1        43 /* APB1 */
#endif
#if CHIP_DEV_CAN_CNT >= 2
#define CHIP_PER_ID_CAN2        44 /* APB1 */
#endif
#if CHIP_DEV_DVP_CNT >= 1
#define CHIP_PER_ID_DVP         45 /* AHB */
#endif
#if CHIP_DEV_SDIO_CNT >= 1
#define CHIP_PER_ID_SDIO1       46 /* AHB, clk only */
#endif
#if CHIP_DEV_ETH_CNT >= 1
#define CHIP_PER_ID_ETH_MAC     47 /* AHB */
#define CHIP_PER_ID_ETH_TX      48 /* AHB, clk only */
#define CHIP_PER_ID_ETH_RX      49 /* AHB, clk only */
#endif
#if CHIP_DEV_BLES_CNT >= 1
#define CHIP_PER_ID_BLES        50 /* AHB, clk only */
#endif

#define CHIP_PER_ID_WWDG        51 /* APB1 */
#define CHIP_PER_ID_PWR         52 /* APB1 */
#define CHIP_PER_ID_BKP         53 /* APB1 */
#define CHIP_PER_ID_ADC1        54 /* APB2 */
#if CHIP_DEV_ADC_CNT >= 2
#define CHIP_PER_ID_ADC2        55 /* APB2 */
#endif
#if CHIP_DEV_ADC_CNT >= 3
#define CHIP_PER_ID_ADC3        56 /* APB2 */
#endif
#define CHIP_PER_ID_DAC         57 /* APB1 */
#if CHIP_DEV_SUPPORT_RNG
#define CHIP_PER_ID_RNG         58 /* AHB */
#endif
#define CHIP_PER_ID_CLKOUT      59
#endif // CHIP_PER_IDS_H
