#ifndef CHIP_N32G03X_REGS_FLASH_H
#define CHIP_N32G03X_REGS_FLASH_H

#include <stdint.h>
#include "init/chip_clk_constraints.h"

#define CHIP_SUPPORT_FLASH_BANK2                0
#define CHIP_SUPPORT_FLASH_FORCE_OB_LOAD        0
#define CHIP_SUPPORT_FLASH_OPTION_NBOOT0        0
#define CHIP_SUPPORT_FLASH_OPTION_BOOT_SEL      0
#define CHIP_SUPPORT_FLASH_REG_WS_EN            0
#define CHIP_SUPPORT_FLASH_REG_PID              0
#define CHIP_SUPPORT_FLASH_ECC                  1
#define CHIP_FLASH_CUSTOM_OPTIONS               1

#include "stm32_flash_v1.h"

#endif // CHIP_N32G03X_REGS_FLASH_H
