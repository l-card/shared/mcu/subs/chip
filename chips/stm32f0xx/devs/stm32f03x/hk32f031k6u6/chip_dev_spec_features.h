#ifndef CHIP_DEV_SPEC_FEATURES_H
#define CHIP_DEV_SPEC_FEATURES_H

#include "chip_pkg_types.h"

#define CHIP_DEV_PKG                             CHIP_PKG_UFQFPN32

#define CHIP_DEV_SUPPORT_TIM2           1 /* stm031, hk031, hk04A */
#define CHIP_DEV_SUPPORT_TIM15          0 /* stm030x8/xC, hk030, hk04A */
#define CHIP_DEV_SUPPORT_TIM17_EMAP     1 /* stm030xC, hk030, hk031, hk04A */
#define CHIP_DEV_SUPPORT_USART2         0 /* stm030x8/xC, hk030x8, hk04Ax8 */
#define CHIP_DEV_SUPPORT_USART3         0 /* stm030xC */
#define CHIP_DEV_SUPPORT_USART4         0 /* stm030xC */
#define CHIP_DEV_SUPPORT_USART5         0 /* stm030xC */
#define CHIP_DEV_SUPPORT_USART6         0 /* stm030xC */
#define CHIP_DEV_SUPPORT_USART_SWAP     1 /* hk030, hk031, hk04A */
#define CHIP_DEV_SUPPORT_USART_CK_EMAP  0 /* stm030, hk04a */
#define CHIP_DEV_SUPPORT_I2S1           1 /* stm031, hk030, hk031, hk04A */
#define CHIP_DEV_SUPPORT_I2S1_EMAP      1 /* hk030, hk031, hk04A */
#define CHIP_DEV_SUPPORT_I2C1_EMAP      1 /* stm030x4/x6/xC, stm031, hk030, hk031, hk04a */
#define CHIP_DEV_SUPPORT_MCO_EMAP       0 /* stm030x4xC, hk030 */
#define CHIP_DEV_SUPPORT_I2C2           0 /* stm030, hk030, hk04a */
#define CHIP_DEV_SUPPORT_I2C2_EMAP      0 /* stm030x8/xC, hk030x8, hk04Ax8 */
#define CHIP_DEV_SUPPORT_SPI2           0 /* stm030, hk030, hk04a */
#define CHIP_DEV_SUPPORT_I2S2           0 /* hk030, hk04A */
#define CHIP_DEV_SUPPORT_SPI2_EMAP      0 /* stm030x8/xC, hk030x8, hk04Ax8 */
#define CHIP_DEV_SUPPORT_CKI            0 /* hk030, hk04A */



#endif // CHIP_DEV_SPEC_FEATURES_H
