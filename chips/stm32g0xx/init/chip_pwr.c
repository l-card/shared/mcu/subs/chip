#include "chip_config.h"
#include "chip_pwr_defs.h"
#include "chip.h"

#ifndef CHIP_CFG_PWR_VOS
    #error volatage scaling range is not specified. CHIP_CFG_PWR_VOS config parameter must be defined!
#else
    #if CHIP_CFG_PWR_VOS == CHIP_PWR_VOS1
        #define CHIP_CR1_RVAL_VOS 1
    #elif CHIP_CFG_PWR_VOS == CHIP_PWR_VOS2
        #define CHIP_CR1_RVAL_VOS 2
    #else
        #error CHIP_CFG_PWR_VOS specified unsupported voltage scaling range
    #endif
#endif

void chip_pwr_init(void) {
    /* включение клока для изменения регистров PWR
     * (тут полагаемся, что значение будет обновлено в конце инициализации в соответствии
        с конфигурацией, поэтому можем не возвращать в старое состояние явно) */
    chip_per_clk_en(CHIP_PER_ID_PWR);

    CHIP_REGS_PWR->CR1 =  (CHIP_REGS_PWR->CR1 & ~(CHIP_REGFLD_PWR_CR1_VOS)) |
            LBITFIELD_SET(CHIP_REGFLD_PWR_CR1_VOS, CHIP_CR1_RVAL_VOS);

    /* ожидание завершения смены VOS */
    while ((CHIP_REGS_PWR->SR2 & CHIP_REGFLD_PWR_SR2_VOSF) != 0) {
        continue;
    }
}

void chip_pwr_bd_cfg_write_enable(void) {
    CHIP_REGS_PWR->CR1 |= CHIP_REGFLD_PWR_CR1_DBP;
}

void chip_pwr_bd_cfg_write_disable(void) {
    CHIP_REGS_PWR->CR1 &= ~CHIP_REGFLD_PWR_CR1_DBP;
}
