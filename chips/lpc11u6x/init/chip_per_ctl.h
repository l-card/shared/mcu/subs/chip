#ifndef CHIP_PER_CTL_H
#define CHIP_PER_CTL_H

#include "chip_per_ids.h"
#include "chip_mmap.h"
#include "chip_clk.h"
#include "lbitfield.h"
#include "regs/regs_syscon.h"
#include <stdbool.h>


static inline void chip_per_rst(int per_id) {
    if (CHIP_PER_ID_GET_RST(per_id) != 0xFF) {
        CHIP_REGS_SYSCON->PRESETCTRL  &= ~(1U << CHIP_PER_ID_GET_RST(per_id));
        CHIP_REGS_SYSCON->PRESETCTRL  |= (1U << CHIP_PER_ID_GET_RST(per_id));
    }
}
static inline void chip_per_clk_en(int per_id) {
    if (CHIP_PER_ID_GET_CLKEN(per_id) != 0xFF) {
        CHIP_REGS_SYSCON->SYSAHBCLKCTRL |= (1U << CHIP_PER_ID_GET_CLKEN(per_id));
    }
    if (CHIP_PER_ID_GET_RST(per_id) != 0xFF) {
        CHIP_REGS_SYSCON->PRESETCTRL |= (1U << CHIP_PER_ID_GET_RST(per_id));
    }
    if (per_id == CHIP_PER_ID_FRG) {
        CHIP_REGS_SYSCON->FRGCLKDIV = LBITFIELD_SET(CHIP_REGFLD_SYSCON_FRGCLKDIV_DIV, CHIP_CLK_FRG_DIV);
    } else if (per_id == CHIP_PER_ID_USART0) {
        CHIP_REGS_SYSCON->USART0CLKDIV = LBITFIELD_SET(CHIP_REGFLD_SYSCON_USART0CLKDIV_DIV,  CHIP_CLK_USART0_DIV);
    } else if (per_id == CHIP_PER_ID_SSP0) {
        CHIP_REGS_SYSCON->SSP0CLKDIV = LBITFIELD_SET(CHIP_REGFLD_SYSCON_SSP0CLKDIV_DIV,  CHIP_CLK_SSP0_DIV);
    } else if (per_id == CHIP_PER_ID_SSP1) {
        CHIP_REGS_SYSCON->SSP1CLKDIV = LBITFIELD_SET(CHIP_REGFLD_SYSCON_SSP1CLKDIV_DIV,  CHIP_CLK_SSP1_DIV);
    }
}
static inline void chip_per_clk_dis(int per_id) {
    if (CHIP_PER_ID_GET_RST(per_id) != 0xFF) {
        CHIP_REGS_SYSCON->PRESETCTRL &= ~(1U << CHIP_PER_ID_GET_RST(per_id));
    }
    if (CHIP_PER_ID_GET_CLKEN(per_id) != 0xFF) {
        /* запрещаем клок блока. Для SCT0/1 клок общий, поэтому для них отключаем, только
         * если запрещен второй SCT, что проверяем по битам регистра PRESETCTRL,
         * которые независимы для каждого SCT */
        if (!((per_id == CHIP_PER_ID_SCT0) && (CHIP_REGS_SYSCON->PRESETCTRL & (1U << CHIP_PER_ID_GET_RST(CHIP_PER_ID_SCT1))))
                && !((per_id == CHIP_PER_ID_SCT1) && (CHIP_REGS_SYSCON->PRESETCTRL & (1U << CHIP_PER_ID_GET_RST(CHIP_PER_ID_SCT0))))) {
            CHIP_REGS_SYSCON->SYSAHBCLKCTRL &= ~(1U << CHIP_PER_ID_GET_CLKEN(per_id));
        }
    }
    if (per_id == CHIP_PER_ID_FRG) {
        CHIP_REGS_SYSCON->FRGCLKDIV = 0;
    } else if (per_id == CHIP_PER_ID_USART0) {
        CHIP_REGS_SYSCON->USART0CLKDIV = 0;
    } else if (per_id == CHIP_PER_ID_SSP0) {
        CHIP_REGS_SYSCON->SSP0CLKDIV = 0;
    } else if (per_id == CHIP_PER_ID_SSP1) {
        CHIP_REGS_SYSCON->SSP1CLKDIV = 0;
    }
}
static inline bool chip_per_clk_is_en(int per_id) {
    return ((CHIP_PER_ID_GET_CLKEN(per_id) != 0xFF) ? (CHIP_REGS_SYSCON->SYSAHBCLKCTRL & (1U << CHIP_PER_ID_GET_CLKEN(per_id))) : true)
         && ((CHIP_PER_ID_GET_RST(per_id) != 0xFF) ? (CHIP_REGS_SYSCON->PRESETCTRL & (1U << CHIP_PER_ID_GET_RST(per_id))) : true);
}


#endif // CHIP_PER_CTL_H
