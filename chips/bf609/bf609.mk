CHIP_DEFS     = CHIP_BF609

CHIP_STARTUP_SRC = $(CHIP_TARGET_DIR)/init/chip_startup.s
CHIP_GEN_SRC += $(CHIP_TARGET_DIR)/chip_interrupt.c

CHIP_CFLAGS = -mcpu=bf609 -mspecld-anomaly -mcsync-anomaly
CHIP_ASFLAGS = $(CHIP_CFLAGS)
CHIP_LDFLAGS = $(CHIP_CFLAGS)
CHIP_LDRFLAGS = -T BF609

CHIP_TOOLCHAIN_TARGET ?= bfin-elf-
