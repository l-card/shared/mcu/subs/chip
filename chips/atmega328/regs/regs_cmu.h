#ifndef REGS_CMU_H
#define REGS_CMU_H

#include "chip_devtype_spec_features.h"

#ifdef CHIP_LGT8FX8P
#define CHIP_REG_RCMCAL     _MMIO_BYTE(0x66) /* 32MHz HFRC Oscillator Calibration Register */
#define CHIP_REG_RCKCAL     _MMIO_BYTE(0x67) /* 32KHz RC Calibration Register */
#define CHIP_REG_PMCR       _MMIO_BYTE(0xF2) /* Clock Source Management Register */
#define CHIP_REG_CLKPR      _MMIO_BYTE(0x61) /* Master Clock Prescaler Register */
#else
#define CHIP_REG_OSCCAL     _MMIO_BYTE(0x66)
#endif


#ifdef CHIP_LGT8FX8P
/* ---------------- PMCR -----------------------------------------------------*/
#define CHIP_REGFLD_PMCR_RCM_EN             (0x01U << 0) /* (RW) Internal 32MHz RC oscillator enable */
#define CHIP_REGFLD_PMCR_RCK_EN             (0x01U << 1) /* (RW) Internal 32KHz RC oscillator enable */
#define CHIP_REGFLD_PMCR_OSCM_EN            (0x01U << 2) /* (RW) External high frequency crystal oscillator enable */
#define CHIP_REGFLD_PMCR_OSCK_EN            (0x01U << 3) /* (RW) External low frequency crystal oscillator enable */
#define CHIP_REGFLD_PMCR_WCLKS              (0x01U << 4) /* (RW) WDT clock source selection */
#define CHIP_REGFLD_PMCR_SCLKS              (0x03U << 5) /* (RW) Min clock source selection */
#define CHIP_REGFLD_PMCR_PMCE               (0x01U << 7) /* (RW) PMCR register change enable control bit */

#define CHIP_REGFLDVAL_PMCR_WCLKS_HFRC_D16  0 /* Internal 32MHz HFRC oscillator divided by 16 */
#define CHIP_REGFLDVAL_PMCR_WCLKS_LFRC      1 /* Internal 32KHz LFRC oscillator */

#define CHIP_REGFLDVAL_PMCR_SCLKS_HFRC      0 /* Internal 32MHz RC oscillator (system default) */
#define CHIP_REGFLDVAL_PMCR_SCLKS_OSC_HF    1 /* External 400K ~ 32MHz high-speed crystal */
#define CHIP_REGFLDVAL_PMCR_SCLKS_LFRC      2 /* Internal 32KHz RC oscillator */
#define CHIP_REGFLDVAL_PMCR_SCLKS_OSC_LF    3 /* External 32K ~ 400KHz low-speed crystal oscillator */

/* ---------------- CLKPR -----------------------------------------------------*/
#define CHIP_REGFLD_CLKPR_CLKPS             (0x0FU << 0) /* (RW) Clock prescaler selection */
#define CHIP_REGFLD_CLKPR_CKO_EN0           (0x01U << 5) /* (RW) System clock is output on the PB0 pin enable */
#define CHIP_REGFLD_CLKPR_CKO_EN1           (0x01U << 6) /* (RW) System clock is output on the PE5 pin enable */
#define CHIP_REGFLD_CLKPR_WCE               (0x01U << 7) /* (RW) Clock Prescaler Change Enable */


#define CHIP_REGFLDVAL_CLKPR_CLKPS_1        0
#define CHIP_REGFLDVAL_CLKPR_CLKPS_2        1
#define CHIP_REGFLDVAL_CLKPR_CLKPS_4        2
#define CHIP_REGFLDVAL_CLKPR_CLKPS_8        3
#define CHIP_REGFLDVAL_CLKPR_CLKPS_16       4
#define CHIP_REGFLDVAL_CLKPR_CLKPS_32       5
#define CHIP_REGFLDVAL_CLKPR_CLKPS_64       6
#define CHIP_REGFLDVAL_CLKPR_CLKPS_128      7
#define CHIP_REGFLDVAL_CLKPR_CLKPS_256      8

#endif

#endif // REGS_CMU_H
