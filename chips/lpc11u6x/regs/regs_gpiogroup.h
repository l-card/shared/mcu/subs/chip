#ifndef CHIP_LPC11U6X_REGS_GPIOGROUP_H
#define CHIP_LPC11U6X_REGS_GPIOGROUP_H


/**
 * @brief GPIO grouped interrupt register block structure
 */
typedef struct {                    /*!< GPIO_GROUP_INTn Structure */
    __IO uint32_t  CTRL;            /*!< GPIO grouped interrupt control register */
    __I  uint32_t  RESERVED0[7];
    __IO uint32_t  PORT_POL[8];        /*!< GPIO grouped interrupt port polarity register */
    __IO uint32_t  PORT_ENA[8];        /*!< GPIO grouped interrupt port m enable register */
    uint32_t       RESERVED1[4072];
} LPC_GPIOGROUPINT_T;

/**
 * LPC11u6x GPIO group bit definitions
 */
#define GPIOGR_INT      (1 << 0)    /*!< GPIO interrupt pending/clear bit */
#define GPIOGR_COMB     (1 << 1)    /*!< GPIO interrupt OR(0)/AND(1) mode bit */
#define GPIOGR_TRIG     (1 << 2)    /*!< GPIO interrupt edge(0)/level(1) mode bit */


#endif /* CHIP_LPC11U6X_REGS_GPIOGROUP_H */
