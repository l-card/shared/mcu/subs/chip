#ifndef CHIP_CLK_H
#define CHIP_CLK_H

#include "chip_clk_defs.h"
#include "chip_config.h"
#include "chip_devtype_spec_features.h"




#ifdef CHIP_CFG_LTIMER_EN
    #define CHIP_LTIMER_EN   CHIP_CFG_LTIMER_EN
#else
    #define CHIP_LTIMER_EN   1
#endif



#define CHIP_CLK_SYSTICK_FREQ CHIP_MHZ(16)

#define CHIP_CLK_USART1_FREQ  CHIP_CLK_SYSTICK_FREQ

#endif // CHIP_CLK_H
