#ifndef REGS_AIPSTZ_H
#define REGS_AIPSTZ_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t  MPR;                /* Master Priviledge Registers.                          Address offset: 0x0000 */
    uint32_t       RESERVED[15];
    __IO uint32_t  OPACR[5];           /* Off-Platform Peripheral Access Control Registers,     Address offset: 0x0040 - 0x0050 */
} CHIP_REGS_AIPSTZ_T;


#define CHIP_REGS_AIPSTZ1 ((CHIP_REGS_AIPSTZ_T *)CHIP_MEMRGN_ADDR_PERIPH_AIPS1_CFG)
#define CHIP_REGS_AIPSTZ2 ((CHIP_REGS_AIPSTZ_T *)CHIP_MEMRGN_ADDR_PERIPH_AIPS2_CFG)
#define CHIP_REGS_AIPSTZ3 ((CHIP_REGS_AIPSTZ_T *)CHIP_MEMRGN_ADDR_PERIPH_AIPS3_CFG)
#define CHIP_REGS_AIPSTZ4 ((CHIP_REGS_AIPSTZ_T *)CHIP_MEMRGN_ADDR_PERIPH_AIPS4_CFG)
#define CHIP_REGS_AIPSTZ5 ((CHIP_REGS_AIPSTZ_T *)CHIP_MEMRGN_ADDR_PERIPH_AIPS5_CFG)

#define CHIP_AIPSTZ_MASTER_MISC        0
#define CHIP_AIPSTZ_MASTER_A53         1
#define CHIP_AIPSTZ_MASTER_SDMA        3
#define CHIP_AIPSTZ_MASTER_M7          5
#define CHIP_AIPSTZ_MASTER_ADSP        6


#if defined CHIP_CORETYPE_CA53
    #define CHIP_AIPSTZ_CURRENT_MASTER      CHIP_AIPSTZ_MASTER_A53
#elif  defined CHIP_CORETYPE_CM7
    #define CHIP_AIPSTZ_CURRENT_MASTER      CHIP_AIPSTZ_MASTER_M7
#endif

#endif // REGS_AIPSTZ_H
