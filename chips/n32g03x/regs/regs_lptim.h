#ifndef CHIP_N32G03X_REGS_LPTIM_H
#define CHIP_N32G03X_REGS_LPTIM_H

#include <stdint.h>

typedef struct {
    __I uint16_t INTSTS;
    uint16_t  RESERVED0;
    __IO uint16_t INTCLR;
    uint16_t  RESERVED1;
    __IO uint16_t INTEN;
    uint16_t  RESERVED2;
    __IO uint32_t CFG;      /* 0x0C: LPTIM Configuration Register */
    __IO uint16_t CTRL;     /* 0x10: LPTIM Control Register */
    uint16_t  RESERVED3;
    __IO uint16_t CMP;
    uint16_t  RESERVED4;
    __IO uint16_t ARR;
    uint16_t  RESERVED5;
    __I uint16_t CNT;
    uint16_t  RESERVED6;
} CHIP_REGS_LPTIM_T;

#define CHIP_REGS_LPTIM         ((CHIP_REGS_LPTIM_T *)CHIP_MEMRGN_ADDR_PERIPH_LPTIM)

/*********** LPTIM Configuration Register (LPTIM_CFG)  ************************/
#define CHIP_REGFLD_LPTIM_CFG_NENC                  (0x00000001UL << 25U)     /* (RW) Non-orthogonal encoder mode enable */
#define CHIP_REGFLD_LPTIM_CFG_ENC                   (0x00000001UL << 24U)     /* (RW) Encoder mode enable */
#define CHIP_REGFLD_LPTIM_CFG_CNTM_EN               (0x00000001UL << 23U)     /* (RW) Counter mode enable */
#define CHIP_REGFLD_LPTIM_CFG_RUPDM                 (0x00000001UL << 22U)     /* (RW) Register update mode */
#define CHIP_REGFLD_LPTIM_CFG_WAVE_POL              (0x00000001UL << 21U)     /* (RW) Waveform polarity */
#define CHIP_REGFLD_LPTIM_CFG_WAVE                  (0x00000001UL << 20U)     /* (RW) Waveform shape bits */
#define CHIP_REGFLD_LPTIM_CFG_TIMOUT_EN             (0x00000001UL << 19U)     /* (RW) Timeout enable bit */
#define CHIP_REGFLD_LPTIM_CFG_TRG_EN                (0x00000003UL << 17U)     /* (RW) Trigger polarity enable */
#define CHIP_REGFLD_LPTIM_CFG_TRG_SEL               (0x00000007UL << 13U)     /* (RW) Trigger selection */
#define CHIP_REGFLD_LPTIM_CFG_CLK_PRE               (0x00000007UL <<  9U)     /* (RW) Clock divider factor */
#define CHIP_REGFLD_LPTIM_CFG_TRIG_FLT              (0x00000003UL <<  6U)     /* (RW) Trigger data filter configuration */
#define CHIP_REGFLD_LPTIM_CFG_CLK_FLT               (0x00000003UL <<  3U)     /* (RW) External clock signal level data filter configuration */
#define CHIP_REGFLD_LPTIM_CFG_CLK_POL               (0x00000003UL <<  1U)     /* (RW) Clock polarity */
#define CHIP_REGFLD_LPTIM_CFG_CLK_SEL               (0x00000001UL <<  0U)     /* (RW) Clock source selection */
#define CHIP_REGMSK_LPTIM_CFG_RESERVED              (0xFC011120)


#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_EN_SW          0 /* Software trigger */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_EN_RISE        1 /* Rising edge trigger */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_EN_FALL        2 /* Falling edge trigger */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_EN_BOTH        3 /* Rising and falling edge trigger */

#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_SEL_GPIO       0 /* PB6 or PA6 */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_SEL_RTC_ALM_A  1 /* RTC Alarm A */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_SEL_RTC_ALM_B  2 /* RTC Alarm B */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_SEL_RTC_TAMP1  3 /* RTC Tamper 1 */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_SEL_RTC_TAMP2  4 /* RTC Tamper 2 */
#define CHIP_REGFLDVAL_LPTIM_CFG_TRG_SEL_COMP_OUT   6 /* Comp Out */

#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_1          0
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_2          1
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_4          2
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_8          3
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_16         4
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_32         5
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_64         6
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_PRE_128        7

#define CHIP_REGFLDVAL_LPTIM_CFG_TRIG_FLT_DIS       0
#define CHIP_REGFLDVAL_LPTIM_CFG_TRIG_FLT_2         1
#define CHIP_REGFLDVAL_LPTIM_CFG_TRIG_FLT_4         2
#define CHIP_REGFLDVAL_LPTIM_CFG_TRIG_FLT_8         3

#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_FLT_DIS        0
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_FLT_2          1
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_FLT_4          2
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_FLT_8          3

#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_POL_RISE       0
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_POL_FALL       1
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_POL_BOTH       2
#define CHIP_REGFLDVAL_LPTIM_CFG_CLK_POL_DIS        3

/*********** LPTIM Control Register (LPTIM_CTRL) ******************************/
#define CHIP_REGFLD_LPTIM_CTRL_TSTCM                (0x00000001UL << 2U)     /* (W1SC) Start timer in continuous mode */
#define CHIP_REGFLD_LPTIM_CTRL_SNGMST               (0x00000001UL << 1U)     /* (W1SC) Start timer in one-shot mode */
#define CHIP_REGFLD_LPTIM_CTRL_EN                   (0x00000001UL << 0U)     /* (RW) LPTIM enable */
#define CHIP_REGMSK_LPTIM_CTRL_RESERVED             (0xFFF8UL)

#endif // CHIP_N32G03X_REGS_LPTIM_H

