#include "chip.h"

#if CHIP_CLK_CRS_SETUP_EN

#ifdef CHIP_CFG_CLK_CRS_SRC
    #define CHIP_CLK_CRS_SRC        CHIP_CFG_CLK_CRS_SRC
#else
    #define CHIP_CLK_CRS_SRC        CHIP_CLK_CRS_SRC_USB
#endif

#ifdef CHIP_CFG_CLK_CRS_AUTOTRIM
    #define CHIP_CLK_CRS_AUTOTRIM   CHIP_CFG_CLK_CRS_AUTOTRIM
#else
    #define CHIP_CLK_CRS_AUTOTRIM   1
#endif

#ifdef CHIP_CFG_CLK_CRS_SYNCDIV
    #define CHIP_CLK_CRS_SYNCDIV    CHIP_CFG_CLK_CRS_SYNCDIV
#else
    #define CHIP_CLK_CRS_SYNCDIV    1
#endif

#ifdef CHIP_CFG_CLK_CRS_FE_LIMIT
    #define CHIP_CLK_CRS_FE_LIMIT   CHIP_CFG_CLK_CRS_FE_LIMIT
#else
    #define CHIP_CLK_CRS_FE_LIMIT   0x22
#endif

#ifdef CHIP_CFG_CLK_CRS_RELOAD
    #define CHIP_CLK_CRS_RELOAD     CHIP_CFG_CLK_CRS_RELOAD
#else
    #define CHIP_CLK_CRS_RELOAD     0xBB7F
#endif


#if CHIP_CLK_CRS_SRC == CHIP_CLK_CRS_SRC_USB
    #define CHIP_RVAL_CRS_CFGR_SYNCSRC  CHIP_REGFLDVAL_CRS_CFGR_SYNC_SRC_USB
    #define CHIP_RVAL_CRS_CFGR_SYNCPOL  0
#elif CHIP_CLK_CRS_SRC == CHIP_CLK_CRS_SRC_LSE
    #define CHIP_RVAL_CRS_CFGR_SYNCSRC  CHIP_REGFLDVAL_CRS_CFGR_SYNC_SRC_LSE
    #define CHIP_RVAL_CRS_CFGR_SYNCPOL  0
#elif CHIP_CLK_CRS_SRC == CHIP_CLK_CRS_SRC_GPIO_RISE
    #define CHIP_RVAL_CRS_CFGR_SYNCSRC  CHIP_REGFLDVAL_CRS_CFGR_SYNC_SRC_GPIO
    #define CHIP_RVAL_CRS_CFGR_SYNCPOL  CHIP_REGFLDVAL_CRS_CFGR_SYNC_POL_RISE
#elif CHIP_CLK_CRS_SRC == CHIP_CLK_CRS_SRC_GPIO_FALL
    #define CHIP_RVAL_CRS_CFGR_SYNCSRC  CHIP_REGFLDVAL_CRS_CFGR_SYNC_SRC_GPIO
    #define CHIP_RVAL_CRS_CFGR_SYNCPOL  CHIP_REGFLDVAL_CRS_CFGR_SYNC_POL_FALL
#else
    #error "invalid CRS sync source specified!"
#endif

#if CHIP_CLK_CRS_SYNCDIV == 1
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_1
#elif CHIP_CLK_CRS_SYNCDIV == 2
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_2
#elif CHIP_CLK_CRS_SYNCDIV == 4
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_4
#elif CHIP_CLK_CRS_SYNCDIV == 8
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_8
#elif CHIP_CLK_CRS_SYNCDIV == 16
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_16
#elif CHIP_CLK_CRS_SYNCDIV == 32
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_32
#elif CHIP_CLK_CRS_SYNCDIV == 64
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_64
#elif CHIP_CLK_CRS_SYNCDIV == 128
    #define CHIP_RVAL_CRS_CFGR_SYNCDIV  CHIP_REGFLDVAL_CRS_CFGR_SYNC_DIV_128
#else
    #error "Invalid CRS sync freq divider specified"
#endif





void chip_crs_init(void) {
    CHIP_REGS_RCC->APB1ENR |= CHIP_REGFLD_RCC_APB1ENR_CRS_EN;

    /* изменение настроек CRS возможно только при запрещении коррекции */
    if (CHIP_REGS_CRS->CR & CHIP_REGFLD_CRS_CR_CEN) {
        CHIP_REGS_CRS->CR &= ~CHIP_REGFLD_CRS_CR_CEN;
    }

    CHIP_REGS_CRS->CFGR = (CHIP_REGS_CRS->CFGR & CHIP_REGMSK_CRS_CFGR_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_CRS_CFGR_SYNC_POL,  CHIP_RVAL_CRS_CFGR_SYNCPOL)
            | LBITFIELD_SET(CHIP_REGFLD_CRS_CFGR_SYNC_SRC,  CHIP_RVAL_CRS_CFGR_SYNCSRC)
            | LBITFIELD_SET(CHIP_REGFLD_CRS_CFGR_SYNC_DIV,  CHIP_RVAL_CRS_CFGR_SYNCDIV)
            | LBITFIELD_SET(CHIP_REGFLD_CRS_CFGR_FE_LIM,    CHIP_CLK_CRS_FE_LIMIT)
            | LBITFIELD_SET(CHIP_REGFLD_CRS_CFGR_RELOAD,    CHIP_CLK_CRS_RELOAD)
            ;

    CHIP_REGS_CRS->CR   = (CHIP_REGS_CRS->CR & (CHIP_REGMSK_CRS_CR_RESERVED | CHIP_REGFLD_CRS_CR_TRIM))
            | LBITFIELD_SET(CHIP_REGFLD_CRS_CR_AUTOTRIM_EN, CHIP_CLK_CRS_AUTOTRIM)
            | LBITFIELD_SET(CHIP_REGFLD_CRS_CR_CEN,         1)
            ;

}

#endif
