#include "chip.h"
#include <stdbool.h>

#if CHIP_CLK_ROOT_ENET_AXI_SETUP_EN
    #if CHIP_CLK_ROOT_ENET_AXI_EN
        #if CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_24M_REF_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     0
        #elif CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_SYS_PLL1_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     2
        #elif CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_SYS_PLL1_DIV3_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     1
        #elif CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_SYS_PLL2_DIV4_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     3
        #elif CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_SYS_PLL2_DIV5_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     4
        #elif CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_SYS_PLL3_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     7
        #elif CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_AUDIO_PLL1_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     5
        #elif CHIP_CLK_ROOT_ENET_AXI_SRC_ID == CHIP_CLK_VIDEO_PLL_ID
            #define CHIP_RVAL_ENET_AXI_ROOT_MUX     6
        #else
            #error Invalid ENET_AXI_ROOT clock source (must be from 24M_REF, SYS_PLL1, SYS_PLL1_DIV3, SYS_PLL2_DIV4, SYS_PLL2_DIV5, SYS_PLL3, AUDIO_PLL1, VIDEO_PLL)
        #endif

        #if (CHIP_CLK_ROOT_ENET_AXI_PREDIV < 1) || (CHIP_CLK_ROOT_ENET_AXI_PREDIV > CHIP_CLK_ROOT_BUS_PREDIV_MAX)
            #error ENET_AXI_ROOT predivider is out of range
        #endif

        #if (CHIP_CLK_ROOT_ENET_AXI_POSTDIV < 1) || (CHIP_CLK_ROOT_ENET_AXI_POSTDIV > CHIP_CLK_ROOT_BUS_POSTDIV_MAX)
            #error ENET_AXI_ROOT predivider is out of range
        #endif

        #if !CHIP_CLK_ROOT_ENET_AXI_SRC_EN
            #error ENET_AXI_ROOT clock source is not enabled
        #endif


        #if CHIP_CLK_ROOT_ENET_AXI_FREQ > CHIP_CLK_ROOT_ENET_AXI_FREQ_MAX
            #error ENET_AXI_ROOT clock is out of range
        #endif
    #endif
#endif


#if CHIP_CLK_ROOT_ENET_REF_SETUP_EN
    #if CHIP_CLK_ROOT_ENET_REF_EN
        #if CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_24M_REF_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     0
        #elif CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_SYS_PLL1_DIV5_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     4
        #elif CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_SYS_PLL2_DIV8_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     1
        #elif CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_SYS_PLL2_DIV10_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     3
        #elif CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_SYS_PLL2_DIV20_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     2
        #elif CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_AUDIO_PLL1_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     5
        #elif CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_VIDEO_PLL_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     6
        #elif CHIP_CLK_ROOT_ENET_REF_SRC_ID == CHIP_CLK_EXT_CLK4_ID
            #define CHIP_RVAL_ENET_REF_CLK_ROOT_MUX     7
        #else
        #error Invalid ENET_REF_CLK_ROOT clock source (must be from 24M_REF, SYS_PLL1, SYS_PLL1_DIV3, SYS_PLL2_DIV4, SYS_PLL2_DIV5, SYS_PLL3, AUDIO_PLL1, VIDEO_PLL)
        #endif

        #if (CHIP_CLK_ROOT_ENET_REF_PREDIV < 1) || (CHIP_CLK_ROOT_ENET_REF_PREDIV > CHIP_CLK_ROOT_BUS_PREDIV_MAX)
            #error ENET_REF_CLK_ROOT predivider is out of range
        #endif

        #if (CHIP_CLK_ROOT_ENET_REF_POSTDIV < 1) || (CHIP_CLK_ROOT_ENET_REF_POSTDIV > CHIP_CLK_ROOT_BUS_POSTDIV_MAX)
            #error ENET_REF_CLK_ROOT predivider is out of range
        #endif

        #if !CHIP_CLK_ROOT_ENET_REF_SRC_EN
            #error ENET_REF_CLK_ROOT clock source is not enabled
        #endif


        #if CHIP_CLK_ROOT_ENET_REF_FREQ > CHIP_CLK_ROOT_ENET_REF_FREQ_MAX
            #error ENET_REF_CLK_ROOT clock is out of range
        #endif
    #endif
#endif

/*  */


#if CHIP_CLK_ROOT_ENET_TIMER_SETUP_EN
    #if CHIP_CLK_ROOT_ENET_TIMER_EN
        #if CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_24M_REF_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     0
        #elif CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_SYS_PLL2_DIV10_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     1
        #elif CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_AUDIO_PLL1_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     2
        #elif CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_VIDEO_PLL_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     7
        #elif CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_EXT_CLK1_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     3
        #elif CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_EXT_CLK2_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     4
        #elif CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_EXT_CLK3_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     5
        #elif CHIP_CLK_ROOT_ENET_TIMER_SRC_ID == CHIP_CLK_EXT_CLK4_ID
            #define CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX     6
        #else
            #error Invalid CLK_ROOT_ENET_TIMER clock source (must be from 24M_REF, SYS_PLL2_DIV10, AUDIO_PLL1, VIDEO_PLL, EXT_CLK1, EXT_CLK2, EXT_CLK3, EXT_CLK4)
        #endif

        #if (CHIP_CLK_ROOT_ENET_TIMER_PREDIV < 1) || (CHIP_CLK_ROOT_ENET_TIMER_PREDIV > CHIP_CLK_ROOT_BUS_PREDIV_MAX)
            #error ENET_REF_CLK_ROOT predivider is out of range
        #endif

        #if (CHIP_CLK_ROOT_ENET_TIMER_POSTDIV < 1) || (CHIP_CLK_ROOT_ENET_TIMER_POSTDIV > CHIP_CLK_ROOT_BUS_POSTDIV_MAX)
            #error ENET_REF_CLK_ROOT predivider is out of range
        #endif

        #if !CHIP_CLK_ROOT_ENET_TIMER_SRC_EN
            #error ENET_REF_CLK_ROOT clock source is not enabled
        #endif


        #if CHIP_CLK_ROOT_ENET_TIMER_FREQ > CHIP_CLK_ROOT_ENET_TIMER_FREQ_MAX
            #error ENET_REF_CLK_ROOT clock is out of range
        #endif
    #endif
#endif





#define CHIP_ROOT_CLK_INIT(clkid, en, mux, prediv, postdiv) do { \
        if (!en) {\
            CHIP_REGS_CCM->ROOT[CHIP_CLK_ID_ROOT_GET_IDX(clkid)].TARGET.VAL = 0; \
        } else  \
            CHIP_REGS_CCM->ROOT[CHIP_CLK_ID_ROOT_GET_IDX(clkid)].TARGET.VAL = 0 \
                | LBITFIELD_SET(CHIP_REGFLD_CCM_ROOT_TARGET_ENABLE, 1) \
                | LBITFIELD_SET(CHIP_REGFLD_CCM_ROOT_TARGET_MUX, mux) \
                | LBITFIELD_SET(CHIP_REGFLD_CCM_ROOT_TARGET_PRE_PODF, prediv-1) \
                | LBITFIELD_SET(CHIP_REGFLD_CCM_ROOT_TARGET_POST_PODF, postdiv-1) \
                ; \
        } \
    while (0)


void chip_clk_init(void) {

#if CHIP_CLK_ROOT_ENET_AXI_SETUP_EN
    CHIP_ROOT_CLK_INIT(CHIP_CLK_ROOT_ENET_AXI_ID, CHIP_CLK_ROOT_ENET_AXI_EN, CHIP_RVAL_ENET_AXI_ROOT_MUX, CHIP_CLK_ROOT_ENET_AXI_PREDIV, CHIP_CLK_ROOT_ENET_AXI_POSTDIV);
#endif

#if CHIP_CLK_ROOT_ENET_REF_SETUP_EN
    CHIP_ROOT_CLK_INIT(CHIP_CLK_ROOT_ENET_REF_ID, CHIP_CLK_ROOT_ENET_REF_EN, CHIP_RVAL_ENET_REF_CLK_ROOT_MUX, CHIP_CLK_ROOT_ENET_REF_PREDIV, CHIP_CLK_ROOT_ENET_REF_POSTDIV);
#endif
#if CHIP_CLK_ROOT_ENET_TIMER_SETUP_EN
    CHIP_ROOT_CLK_INIT(CHIP_CLK_ROOT_ENET_TIMER_ID, CHIP_CLK_ROOT_ENET_TIMER_EN, CHIP_RVAL_ENET_TIMER_CLK_ROOT_MUX, CHIP_CLK_ROOT_ENET_TIMER_PREDIV, CHIP_CLK_ROOT_ENET_TIMER_POSTDIV);
#endif

}

