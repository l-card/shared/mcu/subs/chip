#ifndef CHIP_PER_IDS_N32G03X_H
#define CHIP_PER_IDS_N32G03X_H

#include "chip_devtype_spec_features.h"

#define CHIP_PER_ID_DMA         0 /* AHB, clk only */
#define CHIP_PER_ID_SRAM        1 /* AHB, clk only */
#define CHIP_PER_ID_FLASH       2 /* AHB, clk only */
#define CHIP_PER_ID_CRC         4 /* AHB, clk only */

#define CHIP_PER_ID_AFIO        5 /* APB2 */
#define CHIP_PER_ID_GPIOA       6 /* APB2 */
#define CHIP_PER_ID_GPIOB       7 /* APB2 */
#define CHIP_PER_ID_GPIOC       8 /* APB2 */
#if CHIP_DEV_SUPPORT_PORTD
#define CHIP_PER_ID_GPIOD       9 /* APB2 */
#endif
#define CHIP_PER_ID_GPIOF       10 /* APB2 */


#define CHIP_PER_ID_TIM1        11 /* APB2 */
#define CHIP_PER_ID_TIM3        12 /* APB1 */
#if CHIP_DEV_SUPPORT_TIM4
#define CHIP_PER_ID_TIM4        13 /* APB1 */
#endif
#define CHIP_PER_ID_TIM6        14 /* APB1 */
#define CHIP_PER_ID_TIM8        15 /* APB2 */
#define CHIP_PER_ID_LPTIM       16 /* APB1 */

#define CHIP_PER_ID_USART1      17 /* APB2 */
#define CHIP_PER_ID_USART2      18 /* APB1 */
#if CHIP_DEV_SUPPORT_UART5
#define CHIP_PER_ID_UART5       19 /* APB1 */
#endif
#if CHIP_DEV_SUPPORT_UART6
#define CHIP_PER_ID_UART6       20 /* APB2 */
#endif
#define CHIP_PER_ID_LPUART1     21 /* APB1 */
#if CHIP_DEV_LPUART_CNT >= 2
#define CHIP_PER_ID_LPUART2     22 /* APB1 */
#endif

#define CHIP_PER_ID_SPI1        23 /* APB2 */
#define CHIP_PER_ID_SPI2        24 /* APB2 */
#if CHIP_DEV_SPI_CNT >= 3
#define CHIP_PER_ID_SPI3        25 /* APB2 */
#endif

#define CHIP_PER_ID_I2C1        26 /* APB1 */
#define CHIP_PER_ID_I2C2        27 /* APB1 */

#if CHIP_DEV_CAN_CNT >= 1
#define CHIP_PER_ID_CAN1        28  /* APB1 */
#endif


#define CHIP_PER_ID_WWDG        29 /* APB1 */
#define CHIP_PER_ID_PWR         30 /* APB1 */
#define CHIP_PER_ID_ADC         31 /* AHB */
#define CHIP_PER_ID_COMP        32 /* APB1, clk only */
#define CHIP_PER_ID_COMP_FILT   33 /* APB1, clk only */
#define CHIP_PER_ID_OPA         34 /* APB1, clk only */
#if CHIP_DEV_SUPPORT_TSC
#define CHIP_PER_ID_TSC         35 /* APB1, clk only */
#endif

#define CHIP_PER_ID_HSQRT       36 /* AHB */
#define CHIP_PER_ID_HDIV        37 /* AHB */
#if CHIP_DEV_SUPPORT_RNG
#define CHIP_PER_ID_RNG         38 /* AHB */
#define CHIP_PER_ID_RNG_ANA     39 /* AHB, clk only (RNG analog clock) */
#endif
#if CHIP_DEV_SUPPORT_SAC
#define CHIP_PER_ID_SAC         40 /* AHB */
#endif
#define CHIP_PER_ID_BEEP1       41 /* APB1 */
#if CHIP_DEV_BEEPER_CNT >= 2
#define CHIP_PER_ID_BEEP2       42 /* APB1 */
#endif


#endif // CHIP_PER_IDS_N32G03X_H


