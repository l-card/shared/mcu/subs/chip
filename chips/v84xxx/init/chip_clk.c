#include "chip.h"
#include "chip_clk_constraints.h"
#include "chip_init_faults.h"
#if CHIP_LTIMER_EN
    #include "lclock.h"
#endif

/*---------- определение параметров основных источников частоты --------------*/
#if CHIP_CFG_CLK_HSE_MODE != CHIP_CLK_EXTMODE_DIS
    #if CHIP_CFG_CLK_HSE_FREQ < CHIP_CLK_HSE_FREQ_MIN
        #error HSE frequency must be no less than 4 MHz
    #endif
    #if CHIP_CFG_CLK_HSE_FREQ > CHIP_CLK_HSE_FREQ_MAX
        #error HSE frequency must be up to 25 MHz
    #endif
#endif

/* --------------------- Проверка параметров PLL -----------------------------*/
#if CHIP_CLK_HSE_PLLIN_EN
    #if CHIP_CLK_HSE_PLLIN_DIV == 1
         #define CHIP_RVAL_RCC_MISC2_HSE_DIV_CTRL   0
         #define CHIP_RVAL_RCC_CFG_PLL_HSE_PSC      0
    #elif CHIP_CLK_HSE_PLLIN_DIV == 2
        #define CHIP_RVAL_RCC_MISC2_HSE_DIV_CTRL    CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_2
        #define CHIP_RVAL_RCC_CFG_PLL_HSE_PSC       1
    #elif CHIP_CLK_HSE_PLLIN_DIV == 3
        #define CHIP_RVAL_RCC_MISC2_HSE_DIV_CTRL    CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_3
        #define CHIP_RVAL_RCC_CFG_PLL_HSE_PSC       1
    #elif CHIP_CLK_HSE_PLLIN_DIV == 4
        #define CHIP_RVAL_RCC_MISC2_HSE_DIV_CTRL    CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_4
        #define CHIP_RVAL_RCC_CFG_PLL_HSE_PSC       1
    #elif CHIP_CLK_HSE_PLLIN_DIV == 5
        #define CHIP_RVAL_RCC_MISC2_HSE_DIV_CTRL    CHIP_REGFLDVAL_RCC_MISC2_HSE_DIV_CTRL_5
        #define CHIP_RVAL_RCC_CFG_PLL_HSE_PSC       1
    #else
        #error Invalid HSE PLL prescaler value (must be 1,2,3,4,5)
    #endif
#else
    #define CHIP_RVAL_RCC_MISC2_HSE_DIV_CTRL   0
    #define CHIP_RVAL_RCC_CFG_PLL_HSE_PSC      0
#endif

#if CHIP_CLK_PLL_EN
    #ifndef CHIP_CFG_CLK_PLL_MUL
        #error PLL Multipler is not specified
    #elif (CHIP_CFG_CLK_PLL_MUL >= 2) && (CHIP_CFG_CLK_PLL_MUL <= 64)
        #define CHIP_RVAL_RCC_CFG_PLLMUL      (CHIP_CFG_CLK_PLL_MUL - 2)
        #define CHIP_RVAL_RCC_CFG_PLLMUL_L    (CHIP_RVAL_RCC_CFG_PLLMUL & 0xF)
        #define CHIP_RVAL_RCC_CFG_PLLMUL_H    ((CHIP_RVAL_RCC_CFG_PLLMUL >> 4) & 0x3)
    #else
        #error Invalid PLL multiplier
    #endif

    #if !CHIP_CLK_PLLIN_EN
        #error PLL clock source is not enabled
    #endif
    #if CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI_PLLIN_ID
        #define CHIP_RVAL_RCC_CFG_PLLRC     0
    #elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSE_PLLIN_ID
        #define CHIP_RVAL_RCC_CFG_PLLRC     1
    #else
         #error Invalid PLL clock source
    #endif

    #if CHIP_CLK_PLL_FREQ > CHIP_CLK_PLL_RANGE_LVL
        #define CHIP_RVAL_RCC_CFG_PLLRANGE   1
    #else
        #define CHIP_RVAL_RCC_CFG_PLLRANGE   0
    #endif

    #if ((CHIP_CLK_PLLIN_FREQ < CHIP_CLK_PLLIN_FREQ_MIN) || (CHIP_CLK_PLLIN_FREQ > CHIP_CLK_PLLIN_FREQ_MAX))
        #error PLL input freq out of range
    #endif

    #if ((CHIP_CLK_PLL_FREQ < CHIP_CLK_PLL_FREQ_MIN) || (CHIP_CLK_PLL_FREQ > CHIP_CLK_PLL_FREQ_MAX))
        #error PLL output freq out of range
    #endif

    #ifdef CHIP_CFG_PLL_AUTOSTEP_EN
        #define CHIP_PLL_AUTOSTEP_EN    CHIP_CFG_PLL_AUTOSTEP_EN
    #elif CHIP_CLK_PLL_FREQ > CHIP_CLK_PLL_AUTOSTEP_LVL
        #define CHIP_PLL_AUTOSTEP_EN    1
    #else
        #define CHIP_PLL_AUTOSTEP_EN    0
    #endif

    #if CHIP_PLL_AUTOSTEP_EN
        #define CHIP_RVAL_RCC_MISC2_AUTO_STEP_EN CHIP_REGFLDVAL_RCC_MISC2_AUTO_STEP_EN
    #else
        #define CHIP_RVAL_RCC_MISC2_AUTO_STEP_EN CHIP_REGFLDVAL_RCC_MISC2_AUTO_STEP_DIS
    #endif
#endif



/* --------------------- Проверка параметров SystemClock -------------------- */
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSI_SYSIN_ID
    #define CHIP_RVAL_RCC_CFG_SYSCLK    CHIP_REGFLDVAL_RCC_CFG_SYSCLK_HSI
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_RVAL_RCC_CFG_SYSCLK    CHIP_REGFLDVAL_RCC_CFG_SYSCLK_HSE
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLL_ID
    #define CHIP_RVAL_RCC_CFG_SYSCLK    CHIP_REGFLDVAL_RCC_CFG_SYSCLK_PLLOUT
#else
    #error Invalid System Clock Source (must be from HSI_SYSIN, HSE, PLL)
#endif

#if !CHIP_CLK_SYS_EN
    #error System clock source is not enabled
#endif



/* -------------------- Проверка частот AHB ----------------------------------*/
#if CHIP_CLK_AHB_DIV == 1
    #define CHIP_RVAL_RCC_CFG_AHBPSC           0
#elif CHIP_CLK_AHB_DIV == 2
    #define CHIP_RVAL_RCC_CFG_AHBPSC           8
#elif CHIP_CLK_AHB_DIV == 4
    #define CHIP_RVAL_RCC_CFG_AHBPSC           9
#elif CHIP_CLK_AHB_DIV == 8
    #define CHIP_RVAL_RCC_CFG_AHBPSC           10
#elif CHIP_CLK_AHB_DIV == 16
    #define CHIP_RVAL_RCC_CFG_AHBPSC           11
#elif CHIP_CLK_AHB_DIV == 64
    #define CHIP_RVAL_RCC_CFG_AHBPSC           12
#elif CHIP_CLK_AHB_DIV == 128
    #define CHIP_RVAL_RCC_CFG_AHBPSC           13
#elif CHIP_CLK_AHB_DIV == 256
    #define CHIP_RVAL_RCC_CFG_AHBPSC           14
#elif CHIP_CLK_AHB_DIV == 512
    #define CHIP_RVAL_RCC_CFG_AHBPSC           15
#else
    #error invalid AHB clock divider
#endif

#if CHIP_CLK_AHB_FREQ > CHIP_CLK_AHB_FREQ_MAX
    #error AHB freq is out of range
#endif

/* -------------------- Проверка частот APB1/APB2 ----------------------------*/
#if CHIP_CLK_APB1_DIV == 1
    #define CHIP_RVAL_RCC_CFG_APB1PSC          0
#elif CHIP_CLK_APB1_DIV == 2
    #define CHIP_RVAL_RCC_CFG_APB1PSC          4
#elif CHIP_CLK_APB1_DIV == 4
    #define CHIP_RVAL_RCC_CFG_APB1PSC          5
#elif CHIP_CLK_APB1_DIV == 8
    #define CHIP_RVAL_RCC_CFG_APB1PSC          6
#elif CHIP_CLK_APB1_DIV == 16
    #define CHIP_RVAL_RCC_CFG_APB1PSC          7
#else
    #error invalid APB1 clock divider
#endif

#if CHIP_CLK_APB1_FREQ > CHIP_CLK_APB1_FREQ_MAX
    #error APB1 freq is out of range
#endif

#if CHIP_CLK_APB2_DIV == 1
    #define CHIP_RVAL_RCC_CFG_APB2PSC          0
#elif CHIP_CLK_APB2_DIV == 2
    #define CHIP_RVAL_RCC_CFG_APB2PSC          4
#elif CHIP_CLK_APB2_DIV == 4
    #define CHIP_RVAL_RCC_CFG_APB2PSC          5
#elif CHIP_CLK_APB2_DIV == 8
    #define CHIP_RVAL_RCC_CFG_APB2PSC          6
#elif CHIP_CLK_APB2_DIV == 16
    #define CHIP_RVAL_RCC_CFG_APB2PSC          7
#else
    #error invalid APB2 clock divider
#endif

#if CHIP_CLK_APB2_FREQ > CHIP_CLK_APB2_FREQ_MAX
    #error APB2 freq is out of range
#endif


#if CHIP_CLK_ADC_SETUP_EN
    #if CHIP_CFG_CLK_ADC_DIV == 2
        #define CHIP_RVAL_RCC_CFG_ADCPSC   0
    #elif CHIP_CFG_CLK_ADC_DIV == 4
        #define CHIP_RVAL_RCC_CFG_ADCPSC   1
    #elif CHIP_CFG_CLK_ADC_DIV == 6
        #define CHIP_RVAL_RCC_CFG_ADCPSC   2
    #elif CHIP_CFG_CLK_ADC_DIV == 8
        #define CHIP_RVAL_RCC_CFG_ADCPSC   3
    #elif CHIP_CFG_CLK_ADC_DIV == 12
        #define CHIP_RVAL_RCC_CFG_ADCPSC   5
    #elif CHIP_CFG_CLK_ADC_DIV == 16
        #define CHIP_RVAL_RCC_CFG_ADCPSC   7
    #else
        #error invalid ADC clock divider
    #endif

    #define CHIP_RVAL_RCC_CFG_ADCPSC_L   (CHIP_RVAL_RCC_CFG_ADCPSC & 0x3)
    #define CHIP_RVAL_RCC_CFG_ADCPSC_H   ((CHIP_RVAL_RCC_CFG_ADCPSC >> 2) & 0x1)

    #if CHIP_CLK_ADC_FREQ > CHIP_CLK_ADC_FREQ_MAX
        #error ADC frequency is out of range
    #endif
#else
    #define CHIP_RVAL_RCC_CFG_ADCPSC   0
#endif

#if CHIP_CLK_USB_SETUP_EN
    #if CHIP_CLK_USB_MUL2_DIV == 2
        #define CHIP_RVAL_RCC_CFG_USBPSC   1
    #elif CHIP_CLK_USB_MUL2_DIV == 3
        #define CHIP_RVAL_RCC_CFG_USBPSC   0
    #elif CHIP_CLK_USB_MUL2_DIV == 4
        #define CHIP_RVAL_RCC_CFG_USBPSC   3
    #elif CHIP_CLK_USB_MUL2_DIV == 5
        #define CHIP_RVAL_RCC_CFG_USBPSC   2
    #elif CHIP_CLK_USB_MUL2_DIV == 6
        #define CHIP_RVAL_RCC_CFG_USBPSC   5
    #elif CHIP_CLK_USB_MUL2_DIV == 7
        #define CHIP_RVAL_RCC_CFG_USBPSC   4
    #elif CHIP_CLK_USB_MUL2_DIV == 8
        #define CHIP_RVAL_RCC_CFG_USBPSC   7
    #else
        #error invalid ADC clock divider
    #endif

    #define CHIP_RVAL_RCC_CFG_USBPSC_L   (CHIP_RVAL_RCC_CFG_USBPSC & 0x3)
    #define CHIP_RVAL_RCC_CFG_USBPSC_H   ((CHIP_RVAL_RCC_CFG_USBPSC >> 2) & 0x1)

    #if CHIP_CLK_USB_FREQ != CHIP_CLK_USB_FREQ_REQ
        #error USB frequency is out of range
    #endif
#endif


void chip_clk_init(void) {

    /* Если процессор работает не от HSI, то переходим в режим по умолчанию:
     * включаем HSI и ждем готовности, переводим процессор на работу от него */
    if (LBITFIELD_GET(CHIP_REGS_RCC->CFG, CHIP_REGFLD_RCC_CFG_SYSCLK_STS) != CHIP_REGFLDVAL_RCC_CFG_SYSCLK_HSI) {
        CHIP_REGS_RCC->MISC2 =  CHIP_REGS_RCC->MISC2 & ~CHIP_REGFLD_RCC_MISC2_HSI_SYS_CTRL;
        CHIP_REGS_RCC->CTRL |= CHIP_REGFLD_RCC_CTRL_HSI_EN;

        CHIP_INIT_WAIT((CHIP_REGS_RCC->CTRL & CHIP_REGFLD_RCC_CTRL_HSI_STBL) == CHIP_REGFLD_RCC_CTRL_HSI_STBL,
                           CHIP_INIT_FAULT_WAIT_HSI_RDY, 10000);


        LBITFIELD_UPD(CHIP_REGS_RCC->CFG, CHIP_REGFLD_RCC_CFG_SYSCLK_SEL, CHIP_REGFLDVAL_RCC_CFG_SYSCLK_HSI);
    }
    /* запрещаем PLL, если была разрешена, т.к. настройки поменять не получится иначе */
    LBITFIELD_UPD(CHIP_REGS_RCC->CFG, CHIP_REGFLD_RCC_CTRL_PLL_EN, 0);


    /* настраиваем делители системных шин перед остальными настройками, чтобы
     * не превысить их пределы */
    CHIP_REGS_RCC->CFG = (CHIP_REGS_RCC->CFG & ~(CHIP_REGFLD_RCC_CFG_AHB_PSC
                                                 | CHIP_REGFLD_RCC_CFG_APB1_PSC
                                                 | CHIP_REGFLD_RCC_CFG_APB2_PSC
#if CHIP_CLK_ADC_SETUP_EN
                                                 | CHIP_REGFLD_RCC_CFG_ADC_PSC_H
                                                 | CHIP_REGFLD_RCC_CFG_ADC_PSC_L
#endif
 #if CHIP_CLK_USB_SETUP_EN
                                                  | CHIP_REGFLD_RCC_CFG_USB_PSC_H
                                                  | CHIP_REGFLD_RCC_CFG_USB_PSC_L
 #endif
                                                 ))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_AHB_PSC, CHIP_RVAL_RCC_CFG_AHBPSC)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_APB1_PSC, CHIP_RVAL_RCC_CFG_APB1PSC)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_APB2_PSC, CHIP_RVAL_RCC_CFG_APB2PSC)
#if CHIP_CLK_ADC_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_ADC_PSC_H, CHIP_RVAL_RCC_CFG_ADCPSC_H)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_ADC_PSC_L, CHIP_RVAL_RCC_CFG_ADCPSC_L)
#endif
#if CHIP_CLK_USB_SETUP_EN
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_USB_PSC_H, CHIP_RVAL_RCC_CFG_USBPSC_H)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_USB_PSC_L, CHIP_RVAL_RCC_CFG_USBPSC_L)
#endif
            ;


    /* включение базовых источников частоты.
     * HSI оставляем включенным независимо от настроек, т.к. от него
     * изначально работает CPU. Выключить можно будет только в конце */

    /* настройка деления HSI и HSE */
    CHIP_REGS_RCC->MISC =  (CHIP_REGS_RCC->MISC & ~(CHIP_REGFLD_RCC_MISC_HSI_DIV_EN))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_MISC_HSI_DIV_EN, !CHIP_CLK_HSISYS_DIV_EN);

    CHIP_REGS_RCC->MISC2 =  (CHIP_REGS_RCC->MISC2 & ~(CHIP_REGFLD_RCC_MISC2_HSI_SYS_CTRL))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_MISC2_HSI_SYS_CTRL, !CHIP_CLK_HSISYS_DIV_EN);


    /* включение HSE */
    CHIP_REGS_RCC->CTRL = (CHIP_REGS_RCC->CTRL & ~(CHIP_REGFLD_RCC_CTRL_CFD_EN
                                                   | CHIP_REGFLD_RCC_CTRL_HSE_BYPS
                                                   | CHIP_REGFLD_RCC_CTRL_HSE_EN))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CTRL_HSE_EN, CHIP_CLK_HSE_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CTRL_HSE_BYPS, (CHIP_CLK_HSE_MODE == CHIP_CLK_EXTMODE_CLOCK))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CTRL_CFD_EN,   CHIP_CLK_HSE_CFD_EN);


#if CHIP_CLK_HSE_EN
    CHIP_INIT_WAIT((CHIP_REGS_RCC->CTRL & CHIP_REGFLD_RCC_CTRL_HSE_STBL) == CHIP_REGFLD_RCC_CTRL_HSE_STBL,
                       CHIP_INIT_FAULT_WAIT_BASECLK_RDY, 10000);
#endif


#if CHIP_CLK_PLL_EN
    /* настройки параметров PLL выполняем после всех исходных клоков и до включения PLL */
#if CHIP_CLK_HSE_PLLIN_EN
    CHIP_REGS_RCC->MISC2 =  (CHIP_REGS_RCC->MISC2 & ~(CHIP_REGFLD_RCC_MISC2_HSE_DIV_CTRL |
                                                     CHIP_REGFLD_RCC_MISC2_AUTO_STEP_EN))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_MISC2_HSE_DIV_CTRL, CHIP_RVAL_RCC_MISC2_HSE_DIV_CTRL)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_MISC2_AUTO_STEP_EN, CHIP_RVAL_RCC_MISC2_AUTO_STEP_EN)
            ;
#endif


    CHIP_REGS_RCC->CFG = (CHIP_REGS_RCC->CFG & ~(CHIP_REGFLD_RCC_CFG_PLL_RANGE
                                                 | CHIP_REGFLD_RCC_CFG_PLLMUL_H
                                                 | CHIP_REGFLD_RCC_CFG_PLLMUL_L
                                                 | CHIP_REGFLD_RCC_CFG_PLL_RC
                                                 | CHIP_REGFLD_RCC_CFG_PLL_HSE_PSC))
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_PLL_RANGE,  CHIP_RVAL_RCC_CFG_PLLRANGE)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_PLLMUL_H,   CHIP_RVAL_RCC_CFG_PLLMUL_H)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_PLLMUL_L,   CHIP_RVAL_RCC_CFG_PLLMUL_L)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_PLL_RC,     CHIP_RVAL_RCC_CFG_PLLRC)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_CFG_PLL_HSE_PSC,CHIP_RVAL_RCC_CFG_PLL_HSE_PSC);

    /* разрешение PLL и ожидание установки */
    LBITFIELD_UPD(CHIP_REGS_RCC->CTRL, CHIP_REGFLD_RCC_CTRL_PLL_EN, 1);

    CHIP_INIT_WAIT((CHIP_REGS_RCC->CTRL & CHIP_REGFLD_RCC_CTRL_PLL_STBL) == CHIP_REGFLD_RCC_CTRL_PLL_STBL,
                   CHIP_INIT_FAULT_WAIT_PLL_RDY, 10000);
#endif

    /* выбор источника системного клока */
    LBITFIELD_UPD(CHIP_REGS_RCC->CFG, CHIP_REGFLD_RCC_CFG_SYSCLK_SEL, CHIP_RVAL_RCC_CFG_SYSCLK);


    CHIP_REGS_RCC->AHBEN = (CHIP_REGS_RCC->AHBEN & CHIP_REGMSK_RCC_AHBEN_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBEN_XMC_EN,       CHIP_CLK_XMC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBEN_CRC_EN,       CHIP_CLK_CRC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBEN_FLASH_EN,     CHIP_CLK_FLASH_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBEN_SRAM_EN,      CHIP_CLK_SRAM_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBEN_DMA2_EN,      CHIP_CLK_DMA2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_AHBEN_DMA1_EN,      CHIP_CLK_DMA1_EN)
            ;

    CHIP_REGS_RCC->APB2EN = (CHIP_REGS_RCC->APB2EN & CHIP_REGMSK_RCC_APB2EN_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_UART8_EN,    CHIP_CLK_UART8_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_UART7_EN,    CHIP_CLK_UART7_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_USART6_EN,   CHIP_CLK_USART6_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_TMR11_EN,    CHIP_CLK_TMR11_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_TMR10_EN,    CHIP_CLK_TMR10_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_TMR9_EN,     CHIP_CLK_TMR9_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_USART1_EN,   CHIP_CLK_USART1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_TMR8_EN,     CHIP_CLK_TMR8_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_SPI1_EN,     CHIP_CLK_SPI1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_TMR1_EN,     CHIP_CLK_TMR1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_ADC1_EN,     CHIP_CLK_ADC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_GPIOE_EN,    CHIP_CLK_GPIOE_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_GPIOD_EN,    CHIP_CLK_GPIOD_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_GPIOC_EN,    CHIP_CLK_GPIOC_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_GPIOB_EN,    CHIP_CLK_GPIOB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_GPIOA_EN,    CHIP_CLK_GPIOA_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB2EN_AFIO_EN,     CHIP_CLK_AFIO_EN)
            ;

    CHIP_REGS_RCC->APB1EN = (CHIP_REGS_RCC->APB1EN & CHIP_REGMSK_RCC_APB1EN_RESERVED)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_PWR_EN,      CHIP_CLK_PWR_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_BKP_EN,      CHIP_CLK_BKP_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_I2C3_EN,     CHIP_CLK_I2C3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_CAN_EN,      CHIP_CLK_CAN_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_USB_EN,      CHIP_CLK_USB_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_I2C2_EN,     CHIP_CLK_I2C2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_I2C1_EN,     CHIP_CLK_I2C1_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_UART5_EN,    CHIP_CLK_UART5_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_UART4_EN,    CHIP_CLK_UART4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_USART3_EN,   CHIP_CLK_USART3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_USART2_EN,   CHIP_CLK_USART2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_SPI4_EN,     CHIP_CLK_SPI4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_SPI3_EN,     CHIP_CLK_SPI3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_SPI2_EN,     CHIP_CLK_SPI2_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_WWDG_EN,     CHIP_CLK_WWDG_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR14_EN,    CHIP_CLK_TMR14_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR13_EN,    CHIP_CLK_TMR13_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR12_EN,    CHIP_CLK_TMR12_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR7_EN,     CHIP_CLK_TMR7_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR6_EN,     CHIP_CLK_TMR6_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR5_EN,     CHIP_CLK_TMR5_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR4_EN,     CHIP_CLK_TMR4_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR3_EN,     CHIP_CLK_TMR3_EN)
            | LBITFIELD_SET(CHIP_REGFLD_RCC_APB1EN_TMR2_EN,     CHIP_CLK_TMR2_EN)
            ;

#if CHIP_LTIMER_EN
    /* разрешение ltimer */
    lclock_init();
#endif
}
