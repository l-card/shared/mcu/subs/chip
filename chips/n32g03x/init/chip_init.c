#include "chip.h"

void chip_clk_init(void);
void chip_pwr_init(void);


void chip_dummy_init_fault(int code) {
    (void)code;

    for (;;) {
        continue;
    }
}


void chip_init(void) {
    chip_clk_init();
}


void chip_main_prestart(void) {

}
