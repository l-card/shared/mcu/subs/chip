#ifndef REGS_GUID_H
#define REGS_GUID_H

#include "chip_devtype_spec_features.h"

#ifdef CHIP_LGT8FX8P

#define CHIP_REG_GUID(n)      _MMIO_BYTE(0xF3 + (n))

#endif

#endif // REGS_GUID_H
