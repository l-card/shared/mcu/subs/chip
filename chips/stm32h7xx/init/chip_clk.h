#ifndef CHIP_CLK_H
#define CHIP_CLK_H


#include "chip_clk_defs.h"
#include "chip_config.h"
#include "chip_devtype_spec_features.h"



#define CHIP_CLK_HSE_EN  (CHIP_CFG_CLK_HSE_MODE != CHIP_CLK_EXTMODE_DIS)
#define CHIP_CLK_LSE_EN  (CHIP_CFG_CLK_LSE_MODE != CHIP_CLK_EXTMODE_DIS)

#if CHIP_CLK_HSE_EN
    #define CHIP_CLK_HSE_FREQ   CHIP_CFG_CLK_HSE_FREQ
#else
    #define CHIP_CLK_HSE_FREQ   0
#endif

#define CHIP_CLK_HSI_EN CHIP_CFG_CLK_HSI_EN
#if CHIP_CLK_HSI_EN
    #define CHIP_CLK_HSI_FREQ CHIP_CFG_CLK_HSI_FREQ
#else
    #define CHIP_CLK_HSI_FREQ 0
#endif
#define CHIP_CLK_CSI_EN CHIP_CFG_CLK_CSI_EN

/* в режиме осциллятора частота LSE должна быть 32768 кГц, поэтому ее можно
 *  не определять в конфигурации явно */
#define CHIP_CLK_LSE_OSC_FREQ           CHIP_HZ(32768) /* требуемая частота LSE в режиме CHIP_CLK_EXTMODE_OSC */
#if CHIP_CLK_LSE_EN
    #if (CHIP_CFG_CLK_LSE_MODE == CHIP_CLK_EXTMODE_OSC)
        #define CHIP_CLK_LSE_FREQ CHIP_CLK_LSE_OSC_FREQ
    #else
        #define CHIP_CLK_LSE_FREQ CHIP_CFG_CLK_LSE_FREQ
    #endif
#else
    #define CHIP_CLK_LSE_FREQ 0
#endif


#define CHIP_CLK_LSI_EN             (CHIP_CFG_WDT_SETUP_EN || CHIP_CFG_CLK_LSI_EN)

#ifndef CHIP_CFG_CLK_WU_SRC
    #define CHIP_CFG_CLK_WU_SRC CHIP_CLK_HSI
#endif

#ifndef CHIP_CFG_CLK_WU_KER_SRC
    #define CHIP_CFG_CLK_WU_KER_SRC CHIP_CLK_HSI
#endif

/* частоты и разрешение hsi_ker, csi_ker соответствуют частотам и разрешению
 * исходных клоков hsi и csi */
#define CHIP_CLK_HSI_KER_EN    CHIP_CLK_HSI_EN
#define CHIP_CLK_HSI_KER_FREQ  CHIP_CLK_HSI_FREQ
#define CHIP_CLK_CSI_KER_EN    CHIP_CLK_CSI_EN
#define CHIP_CLK_CSI_KER_FREQ  CHIP_CLK_CSI_FREQ

/* ------------------------- частоты PLL -----------------------------------*/

/* Определяем входную частоту.
 * Не можем использовать просто макрос FREQ, т.к. препроцессор не поддерживает
 * рекурсию и не сможем ссылаться на эту частоту для получения с помощью FREQ производных
 * от PLL частот */
#define CHIP_CLK_PLL_SRC_ID    CHIP_CLK_ID_VAL(CHIP_CFG_CLK_PLL_SRC)
#if CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_CLK_PLL_SRC_EN   CHIP_CFG_CLK_HSI_EN
    #define CHIP_CLK_PLL_SRC_FREQ CHIP_CFG_CLK_HSI_FREQ
#elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_CSI_ID
    #define CHIP_CLK_PLL_SRC_EN   CHIP_CFG_CLK_CSI_EN
    #define CHIP_CLK_PLL_SRC_FREQ CHIP_CLK_CSI_FREQ
#elif CHIP_CLK_PLL_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_CLK_PLL_SRC_EN   CHIP_CLK_HSE_EN
    #define CHIP_CLK_PLL_SRC_FREQ CHIP_CLK_HSE_FREQ
#else
    #define CHIP_CLK_PLL_SRC_EN   0
    #define CHIP_CLK_PLL_SRC_FREQ 0
#endif

/* общие макросы для получения параметров PLL по ее номеру */
#define CHIP_CLK_PLLX_EN_(n)        CHIP_CFG_CLK_PLL ## n ## _EN
#define CHIP_CLK_PLLX_EN(n)         CHIP_CLK_PLLX_EN_(n)

#define CHIP_CLK_PLLX_DIVM_(n)      CHIP_CFG_CLK_PLL ## n ## _DIVM
#define CHIP_CLK_PLLX_DIVM(n)       CHIP_CLK_PLLX_DIVM_(n)

#define CHIP_CLK_PLLX_DIVN_(n)      CHIP_CFG_CLK_PLL ## n ## _DIVN
#define CHIP_CLK_PLLX_DIVN(n)       CHIP_CLK_PLLX_DIVN_(n)

#define CHIP_CLK_PLLX_FRACN_(n)     CHIP_CFG_CLK_PLL ## n ## _FRACN
#define CHIP_CLK_PLLX_FRACN(n)      CHIP_CLK_PLLX_FRACN_(n)

#define CHIP_CLK_PLLX_DIVP_EN_(n)   CHIP_CFG_CLK_PLL ## n ## _DIVP_EN
#define CHIP_CLK_PLLX_DIVP_EN(n)    CHIP_CLK_PLLX_DIVP_EN_(n)
#define CHIP_CLK_PLLX_DIVP_(n)      CHIP_CFG_CLK_PLL ## n ## _DIVP
#define CHIP_CLK_PLLX_DIVP(n)       CHIP_CLK_PLLX_DIVP_(n)

#define CHIP_CLK_PLLX_DIVQ_EN_(n)   CHIP_CFG_CLK_PLL ## n ## _DIVQ_EN
#define CHIP_CLK_PLLX_DIVQ_EN(n)    CHIP_CLK_PLLX_DIVQ_EN_(n)
#define CHIP_CLK_PLLX_DIVQ_(n)      CHIP_CFG_CLK_PLL ## n ## _DIVQ
#define CHIP_CLK_PLLX_DIVQ(n)       CHIP_CLK_PLLX_DIVQ_(n)

#define CHIP_CLK_PLLX_DIVR_EN_(n)   CHIP_CFG_CLK_PLL ## n ## _DIVR_EN
#define CHIP_CLK_PLLX_DIVR_EN(n)    CHIP_CLK_PLLX_DIVR_EN_(n)
#define CHIP_CLK_PLLX_DIVR_(n)      CHIP_CFG_CLK_PLL ## n ## _DIVR
#define CHIP_CLK_PLLX_DIVR(n)       CHIP_CLK_PLLX_DIVR_(n)


#define CHIP_CLK_PLLX_IN_FREQ(n)   (CHIP_CLK_PLL_SRC_FREQ/CHIP_CLK_PLLX_DIVM(n))
/* @note: деление на 100 выполняется, чтобы избежать переполнения при вычислении делителя при макс. входных параметрах (16e6 * 8191) */
#define CHIP_CLK_PLLX_VCO_FREQ(n)  (CHIP_CLK_PLLX_IN_FREQ(n) * CHIP_CLK_PLLX_DIVN(n) + \
                                     (((CHIP_CLK_PLLX_IN_FREQ(n)/100) * CHIP_CLK_PLLX_FRACN(n))/8192)*100)
#define CHIP_CLK_PLLX_P_FREQ(n)    (CHIP_CLK_PLLX_VCO_FREQ(n)/CHIP_CLK_PLLX_DIVP(n))
#define CHIP_CLK_PLLX_Q_FREQ(n)    (CHIP_CLK_PLLX_VCO_FREQ(n)/CHIP_CLK_PLLX_DIVQ(n))
#define CHIP_CLK_PLLX_R_FREQ(n)    (CHIP_CLK_PLLX_VCO_FREQ(n)/CHIP_CLK_PLLX_DIVR(n))
#define CHIP_CLK_PLLX_P_EN(n)      (CHIP_CLK_PLLX_EN(n) && CHIP_CLK_PLLX_DIVP_EN(n))
#define CHIP_CLK_PLLX_Q_EN(n)      (CHIP_CLK_PLLX_EN(n) && CHIP_CLK_PLLX_DIVQ_EN(n))
#define CHIP_CLK_PLLX_R_EN(n)      (CHIP_CLK_PLLX_EN(n) && CHIP_CLK_PLLX_DIVR_EN(n))



/* переопределение макросов для каждой отдельной PLL */
#define CHIP_CLK_PLL1_IN_FREQ       CHIP_CLK_PLLX_IN_FREQ(1)
#define CHIP_CLK_PLL1_VCO_FREQ      CHIP_CLK_PLLX_VCO_FREQ(1)
#define CHIP_CLK_PLL1_P_FREQ        CHIP_CLK_PLLX_P_FREQ(1)
#define CHIP_CLK_PLL1_Q_FREQ        CHIP_CLK_PLLX_Q_FREQ(1)
#define CHIP_CLK_PLL1_R_FREQ        CHIP_CLK_PLLX_R_FREQ(1)
#define CHIP_CLK_PLL1_P_EN          CHIP_CLK_PLLX_P_EN(1)
#define CHIP_CLK_PLL1_Q_EN          CHIP_CLK_PLLX_Q_EN(1)
#define CHIP_CLK_PLL1_R_EN          CHIP_CLK_PLLX_R_EN(1)


#define CHIP_CLK_PLL2_IN_FREQ       CHIP_CLK_PLLX_IN_FREQ(2)
#define CHIP_CLK_PLL2_VCO_FREQ      CHIP_CLK_PLLX_VCO_FREQ(2)
#define CHIP_CLK_PLL2_P_FREQ        CHIP_CLK_PLLX_P_FREQ(2)
#define CHIP_CLK_PLL2_Q_FREQ        CHIP_CLK_PLLX_Q_FREQ(2)
#define CHIP_CLK_PLL2_R_FREQ        CHIP_CLK_PLLX_R_FREQ(2)
#define CHIP_CLK_PLL2_P_EN          CHIP_CLK_PLLX_P_EN(2)
#define CHIP_CLK_PLL2_Q_EN          CHIP_CLK_PLLX_Q_EN(2)
#define CHIP_CLK_PLL2_R_EN          CHIP_CLK_PLLX_R_EN(2)

#define CHIP_CLK_PLL3_IN_FREQ       CHIP_CLK_PLLX_IN_FREQ(3)
#define CHIP_CLK_PLL3_VCO_FREQ      CHIP_CLK_PLLX_VCO_FREQ(3)
#define CHIP_CLK_PLL3_P_FREQ        CHIP_CLK_PLLX_P_FREQ(3)
#define CHIP_CLK_PLL3_Q_FREQ        CHIP_CLK_PLLX_Q_FREQ(3)
#define CHIP_CLK_PLL3_R_FREQ        CHIP_CLK_PLLX_R_FREQ(3)
#define CHIP_CLK_PLL3_P_EN          CHIP_CLK_PLLX_P_EN(3)
#define CHIP_CLK_PLL3_Q_EN          CHIP_CLK_PLLX_Q_EN(3)
#define CHIP_CLK_PLL3_R_EN          CHIP_CLK_PLLX_R_EN(3)



/*---------------------- параметры частоты RTC ------------------------------ */
#define CHIP_CLK_RTC_SRC_ID        CHIP_CLK_ID_VAL(CHIP_CFG_CLK_RTC_SRC)
#define CHIP_CLK_RTC_HSE_FREQ     (CHIP_CLK_HSE_FREQ/CHIP_CFG_CLK_RTC_HSE_DIV)
#define CHIP_CLK_RTC_HSE_EN       CHIP_CLK_HSE_EN

#define CHIP_CLK_RTC_FREQ         CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_RTC_SRC)

/*---------------------- параметры частоты MCO ------------------------------ */
#define CHIP_CLK_MCO1_FREQ         (CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_MCO1_SRC)/CHIP_CFG_CLK_MCO1_DIV)
#define CHIP_CLK_MCO2_FREQ         (CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_MCO2_SRC)/CHIP_CFG_CLK_MCO2_DIV)




/* --------------------- определение системной частоты -----------------------*/
#define CHIP_CLK_SYS_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_SYS_SRC)
#if CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CFG_CLK_HSI_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CFG_CLK_HSI_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_CSI_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_CSI_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CFG_CLK_CSI_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_HSE_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_HSE_EN
#elif CHIP_CLK_SYS_SRC_ID == CHIP_CLK_PLL1_P_ID
    #define CHIP_CLK_SYS_FREQ CHIP_CLK_PLL1_P_FREQ
    #define CHIP_CLK_SYS_EN   CHIP_CLK_PLL1_P_EN
#else
    #define CHIP_CLK_SYS_FREQ 0
    #define CHIP_CLK_SYS_EN   0
#endif




/* --------------- определение базовых частот на основе системной -------------*/
#define CHIP_CLK_CPU_CM7_FREQ   (CHIP_CLK_SYS_FREQ / CHIP_CFG_CLK_D1_CPU_DIV) /* частота CPU (D1) */
#define CHIP_CLK_CPU_CM7_EN     1

#define CHIP_CLK_HOST_FREQ      (CHIP_CLK_CPU_CM7_FREQ / CHIP_CFG_CLK_HOST_DIV)
#define CHIP_CLK_HOST_EN        1

#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_CLK_CPU_M4_FREQ    CHIP_CLK_HOST_FREQ
#define CHIP_CLK_CPU_M4_EN      1
#endif

#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
#define CHIP_CLK_CPU_FREQ       CHIP_CLK_CPU_M4_FREQ
#define CHIP_CLK_CPU_EN         CHIP_CLK_CPU_M4_EN
#else
#define CHIP_CLK_CPU_FREQ       CHIP_CLK_CPU_CM7_FREQ
#define CHIP_CLK_CPU_EN         CHIP_CLK_CPU_CM7_EN
#endif

#define CHIP_CLK_AXI_FREQ       CHIP_CLK_HOST_FREQ   /* частота шины AXI (D1)*/
#define CHIP_CLK_AHB1_FREQ      CHIP_CLK_HOST_FREQ   /* частота шины AHB1 (D2)*/
#define CHIP_CLK_AHB2_FREQ      CHIP_CLK_HOST_FREQ   /* частота шины AHB2 (D2)*/
#define CHIP_CLK_AHB3_FREQ      CHIP_CLK_HOST_FREQ   /* частота шины AHB3 (D1)*/
#define CHIP_CLK_AHB4_FREQ      CHIP_CLK_HOST_FREQ   /* частота шины AHB4 (D3)*/
#define CHIP_CLK_AXI_EN         1
#define CHIP_CLK_AHB1_EN        1
#define CHIP_CLK_AHB2_EN        1
#define CHIP_CLK_AHB3_EN        1
#define CHIP_CLK_AHB4_EN        1

#define CHIP_CLK_APB1_FREQ      (CHIP_CLK_AHB1_FREQ / CHIP_CFG_CLK_APB1_DIV) /* частота шины APB1 (D2)*/
#define CHIP_CLK_APB2_FREQ      (CHIP_CLK_AHB2_FREQ / CHIP_CFG_CLK_APB2_DIV) /* частота шины APB2 (D2)*/
#define CHIP_CLK_APB3_FREQ      (CHIP_CLK_AHB3_FREQ / CHIP_CFG_CLK_APB3_DIV)  /* частота шины APB3 (D1)*/
#define CHIP_CLK_APB4_FREQ      (CHIP_CLK_AHB4_FREQ / CHIP_CFG_CLK_APB4_DIV) /* частота шины APB4 (D3)*/
#define CHIP_CLK_APB1_EN        1
#define CHIP_CLK_APB2_EN        1
#define CHIP_CLK_APB3_EN        1
#define CHIP_CLK_APB4_EN        1


#define CHIP_CLK_SYSTICK_FREQ   CHIP_CLK_CPU_FREQ /* частота SysTick (D1). Согласно errata 2.2.3 нет деления на 8 в STM32H743 revY, но также нет поддержки в CMSIS и для других */
#define CHIP_CLK_SYSTICK_EN     1



#if CHIP_DEV_SUPPORT_HRTIM
/* определение частоты High Resolution таймера из двух вариантов */
#define CHIP_CLK_HRTIM_EN           CHIP_CFG_CLK_HRTIM_EN
#define CHIP_CLK_HRTIM_KER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_HRTIM_KER_SRC)
#if CHIP_CLK_HRTIM_KER_SRC_ID == CHIP_CLK_TIMY_KER_ID
    #define CHIP_CLK_HRTIM_KER_FREQ CHIP_CLK_TIMY_KER_FREQ
    #define CHIP_CLK_HRTIM_KER_EN   CHIP_CLK_TIMY_KER_EN
#elif CHIP_CLK_HRTIM_KER_SRC_ID == CHIP_CLK_CPU_CM7_ID
    #define CHIP_CLK_HRTIM_KER_FREQ CHIP_CLK_CPU_CM7_FREQ
    #define CHIP_CLK_HRTIM_KER_EN   CHIP_CLK_CPU_CM7_EN
#endif
#else
#define CHIP_CLK_HRTIM_EN           0
#define CHIP_CLK_HRTIM_KER_EN       0
#endif


#define CHIP_CLK_TIMX_KER_FREQ      (CHIP_CLK_AHB1_FREQ / CHIP_CFG_CLK_TIMX_DIV)
#define CHIP_CLK_TIMY_KER_FREQ      (CHIP_CLK_AHB2_FREQ / CHIP_CFG_CLK_TIMY_DIV)
#define CHIP_CLK_TIMX_KER_EN        CHIP_CLK_AHB1_EN
#define CHIP_CLK_TIMY_KER_EN        CHIP_CLK_AHB2_EN

#define CHIP_CLK_TIM1_KER_FREQ      CHIP_CLK_TIMY_KER_FREQ
#define CHIP_CLK_TIM2_KER_FREQ      CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM3_KER_FREQ      CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM4_KER_FREQ      CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM5_KER_FREQ      CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM6_KER_FREQ      CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM7_KER_FREQ      CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM8_KER_FREQ      CHIP_CLK_TIMY_KER_FREQ
#define CHIP_CLK_TIM12_KER_FREQ     CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM13_KER_FREQ     CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM14_KER_FREQ     CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM15_KER_FREQ     CHIP_CLK_TIMY_KER_FREQ
#define CHIP_CLK_TIM16_KER_FREQ     CHIP_CLK_TIMY_KER_FREQ
#define CHIP_CLK_TIM17_KER_FREQ     CHIP_CLK_TIMY_KER_FREQ
#define CHIP_CLK_TIM23_KER_FREQ     CHIP_CLK_TIMX_KER_FREQ
#define CHIP_CLK_TIM24_KER_FREQ     CHIP_CLK_TIMX_KER_FREQ

#ifdef CHIP_CFG_CLK_TIM1_EN
    #define CHIP_CLK_TIM1_EN CHIP_CFG_CLK_TIM1_EN
#else
    #define CHIP_CLK_TIM1_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM2_EN
    #define CHIP_CLK_TIM2_EN CHIP_CFG_CLK_TIM2_EN
#else
    #define CHIP_CLK_TIM2_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM3_EN
    #define CHIP_CLK_TIM3_EN CHIP_CFG_CLK_TIM3_EN
#else
    #define CHIP_CLK_TIM3_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM4_EN
    #define CHIP_CLK_TIM4_EN CHIP_CFG_CLK_TIM4_EN
#else
    #define CHIP_CLK_TIM4_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM5_EN
    #define CHIP_CLK_TIM5_EN CHIP_CFG_CLK_TIM5_EN
#else
    #define CHIP_CLK_TIM5_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM6_EN
    #define CHIP_CLK_TIM6_EN CHIP_CFG_CLK_TIM6_EN
#else
    #define CHIP_CLK_TIM6_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM7_EN
    #define CHIP_CLK_TIM7_EN CHIP_CFG_CLK_TIM7_EN
#else
    #define CHIP_CLK_TIM7_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM8_EN
    #define CHIP_CLK_TIM8_EN CHIP_CFG_CLK_TIM8_EN
#else
    #define CHIP_CLK_TIM8_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM12_EN
    #define CHIP_CLK_TIM12_EN CHIP_CFG_CLK_TIM12_EN
#else
    #define CHIP_CLK_TIM12_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM13_EN
    #define CHIP_CLK_TIM13_EN CHIP_CFG_CLK_TIM13_EN
#else
    #define CHIP_CLK_TIM13_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM14_EN
    #define CHIP_CLK_TIM14_EN CHIP_CFG_CLK_TIM14_EN
#else
    #define CHIP_CLK_TIM14_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM15_EN
    #define CHIP_CLK_TIM15_EN CHIP_CFG_CLK_TIM15_EN
#else
    #define CHIP_CLK_TIM15_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM16_EN
    #define CHIP_CLK_TIM16_EN CHIP_CFG_CLK_TIM16_EN
#else
    #define CHIP_CLK_TIM16_EN 0
#endif
#ifdef CHIP_CFG_CLK_TIM17_EN
    #define CHIP_CLK_TIM17_EN CHIP_CFG_CLK_TIM17_EN
#else
    #define CHIP_CLK_TIM17_EN 0
#endif
#if defined CHIP_CFG_CLK_TIM23_EN  && CHIP_DEV_SUPPORT_TIM23_24
    #define CHIP_CLK_TIM23_EN CHIP_CFG_CLK_TIM23_EN
#else
    #define CHIP_CLK_TIM23_EN 0
#endif
#if defined CHIP_CFG_CLK_TIM24_EN  && CHIP_DEV_SUPPORT_TIM23_24
    #define CHIP_CLK_TIM24_EN CHIP_CFG_CLK_TIM24_EN
#else
    #define CHIP_CLK_TIM24_EN 0
#endif


/* --------------------------- периферия  ----------------*/
#define CHIP_CLK_IWDG1_FREQ   CHIP_CLK_LSI_FREQ
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_CLK_IWDG2_FREQ   CHIP_CLK_LSI_FREQ
#endif
#define CHIP_CLK_FLASH_FREQ   CHIP_CLK_AXI_FREQ


#define CHIP_CLK_PER_SRC_ID   CHIP_CLK_ID_VAL(CHIP_CFG_CLK_PER_SRC)
#if CHIP_CLK_PER_SRC_ID == CHIP_CLK_HSI_ID
    #define CHIP_CLK_PER_FREQ   CHIP_CFG_CLK_HSI_FREQ
    #define CHIP_CLK_PER_EN     CHIP_CFG_CLK_HSI_EN
#elif CHIP_CLK_PER_SRC_ID == CHIP_CLK_CSI_ID
    #define CHIP_CLK_PER_FREQ   CHIP_CLK_CSI_FREQ
    #define CHIP_CLK_PER_EN     CHIP_CFG_CLK_CSI_EN
#elif CHIP_CLK_PER_SRC_ID == CHIP_CLK_HSE_ID
    #define CHIP_CLK_PER_FREQ   CHIP_CLK_HSE_FREQ
    #define CHIP_CLK_PER_EN     CHIP_CLK_HSE_EN
#endif


/* -------------- EXMC -------------------------*/
#ifdef CHIP_CFG_CLK_EXMC_EN
    #define CHIP_CLK_EXMC_EN     CHIP_CFG_CLK_EXMC_EN
#else
    #define CHIP_CLK_EXMC_EN     0
#endif

#ifdef CHIP_CFG_CLK_EXMC_KER_SRC_SETUP_EN
    #define CHIP_CLK_EXMC_KER_SRC_SETUP_EN CHIP_CFG_CLK_EXMC_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_EXMC_KER_SRC_SETUP_EN 0
#endif

#ifdef CHIP_CFG_EXMC_SETUP_EN
    #define CHIP_EXMC_SETUP_EN CHIP_CFG_EXMC_SETUP_EN
#else
    #define CHIP_EXMC_SETUP_EN 0
#endif

#define CHIP_CLK_EXMC_KER_EN          (CHIP_CLK_EXMC_EN || CHIP_EXMC_SETUP_EN)
#define CHIP_CLK_EXMC_KER_SRC_EN      (CHIP_CLK_EXMC_KER_EN || CHIP_CLK_EXMC_KER_SRC_SETUP_EN)
#if CHIP_CLK_EXMC_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_EXMC_KER_SRC
        #define CHIP_CLK_EXMC_KER_SRC CHIP_CFG_CLK_EXMC_KER_SRC
    #else
        #erro "EXMC kernel clock source is not specified"
    #endif
    #define CHIP_CLK_EXMC_KER_FREQ   CHIP_CLK_ID_FREQ(CHIP_CFG_CLK_EXMC_KER_SRC)
#else
    #define CHIP_CLK_EXMC_KER_FREQ   0
#endif

#define CHIP_CLK_SDRAM_FREQ          (CHIP_CLK_EXMC_KER_FREQ/CHIP_CFG_SDRAM_CLK_DIV)


/* -------------- QSPI / OCTOSPI  -------------------------*/
#if CHIP_DEV_SUPPORT_OCTOSPI

#ifdef CHIP_CFG_CLK_OCTOSPI1_EN
    #define CHIP_CLK_OCTOSPI1_EN    CHIP_CFG_CLK_OCTOSPI1_EN
#else
    #define CHIP_CLK_OCTOSPI1_EN    0
#endif
#ifdef CHIP_CFG_CLK_OCTOSPI2_EN
    #define CHIP_CLK_OCTOSPI2_EN    CHIP_CFG_CLK_OCTOSPI2_EN
#else
    #define CHIP_CLK_OCTOSPI2_EN    0
#endif

#ifdef CHIP_CFG_CLK_OCTOSPI_KER_SRC_SETUP_EN
    #define CHIP_CLK_OCTOSPI_KER_SRC_SETUP_EN CHIP_CFG_CLK_OCTOSPI_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_OCTOSPI_KER_SRC_SETUP_EN 0
#endif

#ifdef CHIP_CFG_CLK_OCTOSPI1_KER_SRC_SETUP_EN
    #define CHIP_CLK_OCTOSPI1_KER_SRC_SETUP_EN CHIP_CFG_CLK_OCTOSPI1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_OCTOSPI1_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_OCTOSPI2_KER_SRC_SETUP_EN
    #define CHIP_CLK_OCTOSPI2_KER_SRC_SETUP_EN CHIP_CFG_CLK_OCTOSPI2_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_OCTOSPI2_KER_SRC_SETUP_EN 0
#endif

#define CHIP_CLK_OCTOSPI_KER_SRC_EN   (CHIP_CLK_OCTOSPI1_EN || CHIP_CLK_OCTOSPI2_EN \
                                     || CHIP_CLK_OCTOSPI_KER_SRC_SETUP_EN \
                                     || CHIP_CLK_OCTOSPI1_KER_SRC_SETUP_EN \
                                     || CHIP_CLK_OCTOSPI2_KER_SRC_SETUP_EN)
#if CHIP_CLK_OCTOSPI_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_OCTOSPI_KER_SRC
        #define CHIP_CLK_OCTOSPI_KER_SRC CHIP_CFG_CLK_OCTOSPI_KER_SRC
    #else
        #error "OCTOSPI1,2 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_OCTOSPI_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_OCTOSPI_KER_SRC)
#else
    #define CHIP_CLK_OCTOSPI_KER_FREQ     0
#endif

#define CHIP_CLK_OCTOSPI1_KER_SRC_EN  CHIP_CLK_OCTOSPI_KER_SRC_EN
#define CHIP_CLK_OCTOSPI2_KER_SRC_EN  CHIP_CLK_OCTOSPI_KER_SRC_EN
#define CHIP_CLK_OCTOSPI1_KER_FREQ    CHIP_CLK_OCTOSPI_KER_FREQ
#define CHIP_CLK_OCTOSPI2_KER_FREQ    CHIP_CLK_OCTOSPI_KER_FREQ
#define CHIP_CLK_OCTOSPI1_KER_EN      (CHIP_CLK_OCTOSPI1_EN && CHIP_CLK_OCTOSPI1_KER_SRC_EN)
#define CHIP_CLK_OCTOSPI2_KER_EN      (CHIP_CLK_OCTOSPI2_EN && CHIP_CLK_OCTOSPI2_KER_SRC_EN)

#ifdef CHIP_CFG_CLK_IOMNGR_EN
    #define CHIP_CLK_IOMNGR_EN      CHIP_CFG_CLK_IOMNGR_EN
#else
    #define CHIP_CLK_IOMNGR_EN      0
#endif
#ifdef CHIP_CFG_CLK_OTFD1_EN
    #define CHIP_CLK_OTFD1_EN      CHIP_CFG_CLK_OTFD1_EN
#else
    #define CHIP_CLK_OTFD1_EN      0
#endif
#ifdef CHIP_CFG_CLK_OTFD2_EN
    #define CHIP_CLK_OTFD2_EN      CHIP_CFG_CLK_OTFD2_EN
#else
    #define CHIP_CLK_OTFD2_EN      0
#endif

#define CHIP_CLK_QSPI_EN            0
#define CHIP_CLK_QSPI_KER_SRC_EN    0
#else
#ifdef CHIP_CFG_CLK_QSPI_EN
    #define CHIP_CLK_QSPI_EN    CHIP_CFG_CLK_QSPI_EN
#else
    #define CHIP_CLK_QSPI_EN    0
#endif
#ifdef CHIP_CFG_CLK_QSPI_KER_SRC_SETUP_EN
    #define CHIP_CLK_QSPI_KER_SRC_SETUP_EN  CHIP_CFG_CLK_QSPI_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_QSPI_KER_SRC_SETUP_EN  0
#endif
#define CHIP_CLK_QSPI_KER_SRC_EN   (CHIP_CLK_QSPI_EN || CHIP_CLK_QSPI_KER_SRC_SETUP_EN)
#if CHIP_CLK_QSPI_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_QSPI_KER_SRC
        #define CHIP_CLK_QSPI_KER_SRC CHIP_CFG_CLK_QSPI_KER_SRC
    #else
        #error "QSPI kernel clock soucre is not specified"
    #endif
    #define CHIP_CLK_QSPI_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_QSPI_KER_SRC)
#else
    #define CHIP_CLK_QSPI_KER_FREQ 0
#endif
#define CHIP_CLK_QSPI_KER_EN       (CHIP_CLK_QSPI_EN && CHIP_CLK_QSPI_KER_SRC_EN)


#define CHIP_CLK_OCTOSPI1_EN        0
#define CHIP_CLK_OCTOSPI2_EN        0
#define CHIP_CLK_OCTOSPI_KER_SRC_EN 0
#define CHIP_CLK_IOMNGR_EN          0
#define CHIP_CLK_OTFD1_EN           0
#define CHIP_CLK_OTFD2_EN           0
#endif

/* -------------- SDMMC -------------------------*/
#ifdef CHIP_CFG_CLK_SDMMC1_EN
    #define CHIP_CLK_SDMMC1_EN  CHIP_CFG_CLK_SDMMC1_EN
#else
    #define CHIP_CLK_SDMMC1_EN  0
#endif
#ifdef CHIP_CFG_CLK_SDMMC2_EN
    #define CHIP_CLK_SDMMC2_EN  CHIP_CFG_CLK_SDMMC2_EN
#else
    #define CHIP_CLK_SDMMC2_EN  0
#endif

#ifdef CHIP_CFG_CLK_SDMMC1_KER_SRC_SETUP_EN
    #define CHIP_CLK_SDMMC1_KER_SRC_SETUP_EN    CHIP_CFG_CLK_SDMMC1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SDMMC1_KER_SRC_SETUP_EN    0
#endif
#ifdef CHIP_CFG_CLK_SDMMC2_KER_SRC_SETUP_EN
    #define CHIP_CLK_SDMMC2_KER_SRC_SETUP_EN    CHIP_CFG_CLK_SDMMC2_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SDMMC2_KER_SRC_SETUP_EN    0
#endif
#ifdef CHIP_CFG_CLK_SDMMC_KER_SRC_SETUP_EN
    #define CHIP_CLK_SDMMC_KER_SRC_SETUP_EN     CHIP_CFG_CLK_SDMMC_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SDMMC_KER_SRC_SETUP_EN     0
#endif

#define CHIP_CLK_SDMMC_KER_SRC_EN   (CHIP_CLK_SDMMC1_EN || CHIP_CLK_SDMMC2_EN \
                                     || CHIP_CLK_SDMMC_KER_SRC_SETUP_EN \
                                     || CHIP_CLK_SDMMC1_KER_SRC_SETUP_EN \
                                     || CHIP_CLK_SDMMC2_KER_SRC_SETUP_EN)
#if CHIP_CLK_SDMMC_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_SDMMC_KER_SRC
        #define CHIP_CLK_SDMMC_KER_SRC CHIP_CFG_CLK_SDMMC_KER_SRC
    #else
        #error "SDMMC1,2 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_SDMMC_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_SDMMC_KER_SRC)
#else
    #define CHIP_CLK_SDMMC_KER_FREQ     0
#endif

#define CHIP_CLK_SDMMC1_KER_SRC_EN  CHIP_CLK_SDMMC_KER_SRC_EN
#define CHIP_CLK_SDMMC2_KER_SRC_EN  CHIP_CLK_SDMMC_KER_SRC_EN
#define CHIP_CLK_SDMMC1_KER_FREQ    CHIP_CLK_SDMMC_KER_FREQ
#define CHIP_CLK_SDMMC2_KER_FREQ    CHIP_CLK_SDMMC_KER_FREQ
#define CHIP_CLK_SDMMC1_KER_EN      (CHIP_CLK_SDMMC1_EN && CHIP_CLK_SDMMC1_KER_SRC_EN)
#define CHIP_CLK_SDMMC2_KER_EN      (CHIP_CLK_SDMMC2_EN && CHIP_CLK_SDMMC2_KER_SRC_EN)



/* -------------------- SAI1 ---------------------*/
#ifdef CHIP_CFG_CLK_SAI1_EN
    #define CHIP_CLK_SAI1_EN           CHIP_CFG_CLK_SAI1_EN
#else
    #define CHIP_CLK_SAI1_EN           0
#endif
#ifdef CHIP_CFG_CLK_SAI1_KER_SRC_SETUP_EN
    #define CHIP_CLK_SAI1_KER_SRC_SETUP_EN  CHIP_CFG_CLK_SAI1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SAI1_KER_SRC_SETUP_EN  0
#endif
#define CHIP_CLK_SAI1_KER_SRC_EN   (CHIP_CLK_SAI1_EN || CHIP_CLK_SAI1_KER_SRC_SETUP_EN)
#if CHIP_CLK_SAI1_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_SAI1_KER_SRC
        #define CHIP_CLK_SAI1_KER_SRC CHIP_CFG_CLK_SAI1_KER_SRC
    #else
        #erro "SAI1 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_SAI1_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_SAI1_KER_SRC)
#else
    #define CHIP_CLK_SAI1_KER_FREQ     0
#endif
#define CHIP_CLK_SAI1_KER_EN       (CHIP_CLK_SAI1_KER_SRC_EN)

#if CHIP_DEV_SUPPORT_SAI2_3
/* -------------------- SAI2,3 ---------------------*/
#ifdef CHIP_CFG_CLK_SAI2_EN
#define CHIP_CLK_SAI2_EN    CHIP_CFG_CLK_SAI2_EN
#else
#define CHIP_CLK_SAI2_EN    0
#endif
#ifdef CHIP_CFG_CLK_SAI3_EN
#define CHIP_CLK_SAI3_EN    CHIP_CFG_CLK_SAI3_EN
#else
#define CHIP_CLK_SAI3_EN    0
#endif

#ifdef CHIP_CFG_CLK_SAI2_KER_SRC_SETUP_EN
    #define CHIP_CLK_SAI2_KER_SRC_SETUP_EN  CHIP_CFG_CLK_SAI2_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SAI2_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_SAI3_KER_SRC_SETUP_EN
    #define CHIP_CLK_SAI3_KER_SRC_SETUP_EN  CHIP_CFG_CLK_SAI3_KER_SRC_SETUP_EN 0
#else
    #define CHIP_CLK_SAI3_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_SAI2_3_KER_SRC_SETUP_EN
    #define CHIP_CLK_SAI2_3_KER_SRC_SETUP_EN CHIP_CFG_CLK_SAI2_3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SAI2_3_KER_SRC_SETUP_EN 0
#endif

#define CHIP_CLK_SAI2_3_KER_SRC_EN   (CHIP_CLK_SAI2_EN || CHIP_CLK_SAI3_EN  \
                                     || CHIP_CLK_SAI2_KER_SRC_SETUP_EN  \
                                     || CHIP_CLK_SAI3_KER_SRC_SETUP_EN  \
                                     || CHIP_CLK_SAI2_3_KER_SRC_SETUP_EN)
#if CHIP_CLK_SAI2_3_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_SAI2_3_KER_SRC
        #define CHIP_CLK_SAI2_3_KER_SRC CHIP_CFG_CLK_SAI2_3_KER_SRC
    #elif defined CHIP_CFG_CLK_SAI23_KER_SRC
        #define CHIP_CLK_SAI2_3_KER_SRC CHIP_CFG_CLK_SAI23_KER_SRC
    #else
        #error "SAI2,3 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_SAI2_3_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_SAI2_3_KER_SRC)
#else
    #define CHIP_CLK_SAI2_3_KER_FREQ     0
#endif
#define CHIP_CLK_SAI2_KER_SRC_EN    CHIP_CLK_SAI2_3_KER_SRC_EN
#define CHIP_CLK_SAI3_KER_SRC_EN    CHIP_CLK_SAI2_3_KER_SRC_EN
#define CHIP_CLK_SAI2_KER_FREQ      CHIP_CLK_SAI2_3_KER_FREQ
#define CHIP_CLK_SAI3_KER_FREQ      CHIP_CLK_SAI2_3_KER_FREQ
#define CHIP_CLK_SAI2_KER_EN       (CHIP_CLK_SAI2_EN && CHIP_CLK_SAI2_3_KER_SRC_EN)
#define CHIP_CLK_SAI3_KER_EN       (CHIP_CLK_SAI3_EN && CHIP_CLK_SAI2_3_KER_SRC_EN)
#else
#define CHIP_CLK_SAI2_EN            0
#define CHIP_CLK_SAI3_EN            0
#define CHIP_CLK_SAI2_KER_SRC_EN    0
#define CHIP_CLK_SAI3_KER_SRC_EN    0
#define CHIP_CLK_SAI2_KER_EN        0
#define CHIP_CLK_SAI3_KER_EN        0
#endif


/* ---------------- SPI1,2,3 -----------------------*/
#ifdef CHIP_CFG_CLK_SPI1_EN
    #define CHIP_CLK_SPI1_EN CHIP_CFG_CLK_SPI1_EN
#else
    #define CHIP_CLK_SPI1_EN 0
#endif
#ifdef CHIP_CFG_CLK_SPI2_EN
    #define CHIP_CLK_SPI2_EN CHIP_CFG_CLK_SPI2_EN
#else
    #define CHIP_CLK_SPI2_EN 0
#endif
#ifdef CHIP_CFG_CLK_SPI3_EN
    #define CHIP_CLK_SPI3_EN CHIP_CFG_CLK_SPI3_EN
#else
    #define CHIP_CLK_SPI3_EN 0
#endif

#ifdef CHIP_CFG_CLK_SPI1_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI1_KER_SRC_SETUP_EN CHIP_CFG_CLK_SPI1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI1_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_SPI2_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI2_KER_SRC_SETUP_EN CHIP_CFG_CLK_SPI2_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI2_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_SPI3_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI3_KER_SRC_SETUP_EN CHIP_CFG_CLK_SPI3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI3_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_SPI1_2_3_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI1_2_3_KER_SRC_SETUP_EN CHIP_CFG_CLK_SPI1_2_3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI1_2_3_KER_SRC_SETUP_EN 0
#endif

#define CHIP_CLK_SPI1_2_3_KER_SRC_EN  (CHIP_CLK_SPI1_EN || CHIP_CLK_SPI2_EN || CHIP_CLK_SPI3_EN \
                                        || CHIP_CLK_SPI1_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_SPI2_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_SPI3_KER_SRC_SETUP_EN \
                                        || CHIP_CFG_CLK_SPI1_2_3_KER_SRC_SETUP_EN)

#if CHIP_CLK_SPI1_2_3_KER_SRC_EN
#if defined CHIP_CFG_CLK_SPI1_2_3_KER_SRC
    #define CHIP_CLK_SPI1_2_3_KER_SRC CHIP_CFG_CLK_SPI1_2_3_KER_SRC
#elif defined CHIP_CFG_CLK_SPI123_KER_SRC
    #define CHIP_CLK_SPI1_2_3_KER_SRC CHIP_CFG_CLK_SPI123_KER_SRC
#else
    #error "SPI1,2,3 kernel clock source is not spefified"
#endif
#define CHIP_CLK_SPI1_2_3_KER_FREQ  CHIP_CLK_ID_FREQ(CHIP_CLK_SPI1_2_3_KER_SRC)
#else
#define CHIP_CLK_SPI1_2_3_KER_FREQ  0
#endif

#define CHIP_CLK_SPI1_KER_SRC_EN    CHIP_CLK_SPI1_2_3_KER_SRC_EN
#define CHIP_CLK_SPI2_KER_SRC_EN    CHIP_CLK_SPI1_2_3_KER_SRC_EN
#define CHIP_CLK_SPI3_KER_SRC_EN    CHIP_CLK_SPI1_2_3_KER_SRC_EN
#define CHIP_CLK_SPI1_KER_FREQ      CHIP_CLK_SPI1_2_3_KER_FREQ
#define CHIP_CLK_SPI2_KER_FREQ      CHIP_CLK_SPI1_2_3_KER_FREQ
#define CHIP_CLK_SPI3_KER_FREQ      CHIP_CLK_SPI1_2_3_KER_FREQ
#define CHIP_CLK_SPI1_KER_EN        (CHIP_CLK_SPI1_EN && CHIP_CLK_SPI1_KER_SRC_EN)
#define CHIP_CLK_SPI2_KER_EN        (CHIP_CLK_SPI2_EN && CHIP_CLK_SPI2_KER_SRC_EN)
#define CHIP_CLK_SPI3_KER_EN        (CHIP_CLK_SPI3_EN && CHIP_CLK_SPI3_KER_SRC_EN)


/* ------------------ SPI4,5 ------------------------------*/
#ifdef CHIP_CFG_CLK_SPI4_EN
    #define CHIP_CLK_SPI4_EN CHIP_CFG_CLK_SPI4_EN
#else
    #define CHIP_CLK_SPI4_EN 0
#endif
#ifdef CHIP_CFG_CLK_SPI5_EN
    #define CHIP_CLK_SPI5_EN CHIP_CFG_CLK_SPI5_EN
#else
    #define CHIP_CLK_SPI5_EN 0
#endif

#ifdef CHIP_CFG_CLK_SPI4_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI4_KER_SRC_SETUP_EN  CHIP_CFG_CLK_SPI4_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI4_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI5_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI5_KER_SRC_SETUP_EN  CHIP_CFG_CLK_SPI5_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI5_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_SPI4_5_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI4_5_KER_SRC_SETUP_EN CHIP_CFG_CLK_SPI4_5_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI4_5_KER_SRC_SETUP_EN 0
#endif

#define CHIP_CLK_SPI4_5_KER_SRC_EN  (CHIP_CLK_SPI4_EN || CHIP_CLK_SPI5_EN \
                                    || CHIP_CLK_SPI4_KER_SRC_SETUP_EN \
                                    || CHIP_CLK_SPI5_KER_SRC_SETUP_EN \
                                    || CHIP_CLK_SPI4_5_KER_SRC_SETUP_EN)
#if CHIP_CLK_SPI4_5_KER_SRC_EN
#ifdef CHIP_CFG_CLK_SPI4_5_KER_SRC
    #define CHIP_CLK_SPI4_5_KER_SRC CHIP_CFG_CLK_SPI4_5_KER_SRC
#elif defined CHIP_CFG_CLK_SPI45_KER_SRC
    #define CHIP_CLK_SPI4_5_KER_SRC CHIP_CFG_CLK_SPI45_KER_SRC
#else
    #error "SPI 4,5 kernel clock source is not specified"
#endif
#define CHIP_CLK_SPI4_5_KER_FREQ    CHIP_CLK_ID_FREQ(CHIP_CLK_SPI4_5_KER_SRC)
#else
#define CHIP_CLK_SPI4_5_KER_FREQ 0
#endif

#define CHIP_CLK_SPI4_KER_SRC_EN    CHIP_CLK_SPI4_5_KER_SRC_EN
#define CHIP_CLK_SPI5_KER_SRC_EN    CHIP_CLK_SPI4_5_KER_SRC_EN
#define CHIP_CLK_SPI4_KER_FREQ      CHIP_CLK_SPI4_5_KER_FREQ
#define CHIP_CLK_SPI5_KER_FREQ      CHIP_CLK_SPI4_5_KER_FREQ
#define CHIP_CLK_SPI4_KER_EN        (CHIP_CLK_SPI4_EN && CHIP_CLK_SPI4_KER_SRC_EN)
#define CHIP_CLK_SPI5_KER_EN        (CHIP_CLK_SPI5_EN && CHIP_CLK_SPI5_KER_SRC_EN)


/* ---------------- SPDIFRX ------------------------*/
#ifdef CHIP_CFG_CLK_SPDIFRX_EN
    #define CHIP_CLK_SPDIFRX_EN     CHIP_CFG_CLK_SPDIFRX_EN
#else
    #define CHIP_CLK_SPDIFRX_EN     0
#endif

#ifdef CHIP_CFG_CLK_SPDIFRX_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPDIFRX_KER_SRC_SETUP_EN CHIP_CFG_CLK_SPDIFRX_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPDIFRX_KER_SRC_SETUP_EN 0
#endif
#define CHIP_CLK_SPDIFRX_KER_SRC_EN   (CHIP_CLK_SPDIFRX_EN || CHIP_CLK_SPDIFRX_KER_SRC_SETUP_EN)

#if CHIP_CLK_SPDIFRX_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_SPDIFRX_KER_SRC
        #define CHIP_CLK_SPDIFRX_KER_SRC CHIP_CFG_CLK_SPDIFRX_KER_SRC
    #elif defined CHIP_CFG_CLK_SPDIF_KER_SRC
        #define CHIP_CLK_SPDIFRX_KER_SRC CHIP_CFG_CLK_SPDIF_KER_SRC
    #else
        #error "SPDIFRX kernel clock source is not specified"
    #endif
    #define CHIP_CLK_SPDIFRX_KER_FREQ   CHIP_CLK_ID_FREQ(CHIP_CLK_SPDIFRX_KER_SRC)
#else
    #define CHIP_CLK_SPDIFRX_KER_FREQ   0
#endif
#define CHIP_CLK_SPDIFRX_KER_EN       (CHIP_CFG_CLK_SPDIFRX_EN && CHIP_CLK_SPDIFRX_KER_SRC_EN)


/* --------------- DFSDM1 ------------------------*/
#ifdef CHIP_CFG_CLK_DFSDM1_EN
    #define CHIP_CLK_DFSDM1_EN                  CHIP_CFG_CLK_DFSDM1_EN
#else
    #define CHIP_CLK_DFSDM1_EN                  0
#endif
#ifdef CHIP_CFG_CLK_DFSDM1_KER_SRC_SETUP_EN
    #define CHIP_CLK_DFSDM1_KER_SRC_SETUP_EN    CHIP_CFG_CLK_DFSDM1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_DFSDM1_KER_SRC_SETUP_EN    0
#endif


#define CHIP_CLK_DFSDM1_KER_SRC_EN   (CHIP_CLK_DFSDM1_EN || CHIP_CLK_DFSDM1_KER_SRC_SETUP_EN)

#if CHIP_CLK_DFSDM1_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_DFSDM1_KER_SRC
        #define CHIP_CLK_DFSDM1_KER_SRC CHIP_CFG_CLK_DFSDM1_KER_SRC
    #else
        #error DFSDM1 kernel clock source is not specified
    #endif
    #define CHIP_CLK_DFSDM1_KER_FREQ    CHIP_CLK_ID_FREQ(CHIP_CLK_DFSDM1_KER_SRC)
#else
    #define CHIP_CLK_DFSDM1_KER_FREQ    0
#endif
#define CHIP_CLK_DFSDM1_KER_EN      (CHIP_CLK_DFSDM1_EN && CHIP_CLK_DFSDM1_KER_SRC_EN)


/* -------------- FDCAN ----------------------- */
#ifdef CHIP_CFG_CLK_FDCAN_EN
    #define CHIP_CLK_FDCAN_EN   CHIP_CFG_CLK_FDCAN_EN
#else
    #define CHIP_CLK_FDCAN_EN   0
#endif
#ifdef CHIP_CFG_CLK_FDCAN_KER_SRC_SETUP_EN
    #define CHIP_CLK_FDCAN_KER_SRC_SETUP_EN CHIP_CFG_CLK_FDCAN_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_FDCAN_KER_SRC_SETUP_EN 0
#endif

#define CHIP_CLK_FDCAN_KER_SRC_EN   (CHIP_CLK_FDCAN_EN || CHIP_CLK_FDCAN_KER_SRC_SETUP_EN)

#if CHIP_CLK_FDCAN_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_FDCAN_KER_SRC
        #define CHIP_CLK_FDCAN_KER_SRC CHIP_CFG_CLK_FDCAN_KER_SRC
    #else
        #error "FDCAN kernel clock source is not specified"
    #endif
    #define CHIP_CLK_FDCAN_KER_FREQ CHIP_CLK_ID_FREQ(CHIP_CLK_FDCAN_KER_SRC)
#else
    #define CHIP_CLK_FDCAN_KER_FREQ 0
#endif
#define CHIP_CLK_FDCAN_KER_EN       (CHIP_CLK_FDCAN_EN && CHIP_CLK_FDCAN_KER_SRC_EN)


/* -------------- SWPMI ---------------------------*/
#ifdef CHIP_CFG_CLK_SWPMI_EN
    #define CHIP_CLK_SWPMI_EN CHIP_CFG_CLK_SWPMI_EN
#else
    #define CHIP_CLK_SWPMI_EN 0
#endif

#ifdef CHIP_CFG_CLK_SWPMI_KER_SRC_SETUP_EN
    #define CHIP_CLK_SWPMI_KER_SRC_SETUP_EN CHIP_CFG_CLK_SWPMI_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SWPMI_KER_SRC_SETUP_EN 0
#endif
#define CHIP_CLK_SWPMI_KER_SRC_EN   (CHIP_CLK_SWPMI_EN || CHIP_CLK_SWPMI_KER_SRC_SETUP_EN)

#if CHIP_CLK_SWPMI_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_SWPMI_KER_SRC
        #define CHIP_CLK_SWPMI_KER_SRC CHIP_CFG_CLK_SWPMI_KER_SRC
    #else
        #error "SWPMI kernel clock source is not specified"
    #endif
    #define CHIP_CLK_SWPMI_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_SWPMI_KER_SRC)
#else
    #define CHIP_CLK_SWPMI_KER_FREQ     0
#endif
#define CHIP_CLK_SWPMI_KER_EN       (CHIP_CLK_SWPMI_EN && CHIP_CLK_SWPMI_KER_SRC_EN)
#define CHIP_CLK_SWPMI              CHIP_CLK_APB1_FREQ


/* -------------- USART1,6,10, UART9 -----------------------*/
#ifdef CHIP_CFG_CLK_USART1_EN
    #define CHIP_CLK_USART1_EN CHIP_CFG_CLK_USART1_EN
#else
    #define CHIP_CLK_USART1_EN  0
#endif

#ifdef CHIP_CFG_CLK_USART6_EN
    #define CHIP_CLK_USART6_EN CHIP_CFG_CLK_USART6_EN
#else
    #define CHIP_CLK_USART6_EN  0
#endif

#ifdef CHIP_CFG_CLK_USART1_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART1_KER_SRC_SETUP_EN CHIP_CFG_CLK_USART1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USART1_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_USART6_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART6_KER_SRC_SETUP_EN CHIP_CFG_CLK_USART6_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USART6_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_USART1_6_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART1_6_KER_SRC_SETUP_EN CHIP_CFG_CLK_USART1_6_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USART1_6_KER_SRC_SETUP_EN 0
#endif

#if defined CHIP_CFG_CLK_UART9_EN && CHIP_DEV_SUPPORT_USART9_10
    #define CHIP_CLK_UART9_EN       CHIP_CFG_CLK_UART9_EN
#else
    #define CHIP_CLK_UART9_EN       0
#endif
#if defined CHIP_CFG_CLK_USART10_EN && CHIP_DEV_SUPPORT_USART9_10
    #define CHIP_CLK_USART10_EN     CHIP_CFG_CLK_USART10_EN
#else
    #define CHIP_CLK_USART10_EN     0
#endif
#if defined CHIP_CFG_CLK_UART9_KER_SRC_SETUP_EN  && CHIP_DEV_SUPPORT_USART9_10
    #define CHIP_CLK_UART9_KER_SRC_SETUP_EN CHIP_CFG_CLK_UART9_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_UART9_KER_SRC_SETUP_EN 0
#endif
#if defined CHIP_CFG_CLK_USART10_KER_SRC_SETUP_EN && CHIP_DEV_SUPPORT_USART9_10
    #define CHIP_CLK_USART10_KER_SRC_SETUP_EN CHIP_CFG_CLK_USART10_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USART10_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_USART1_6_9_10_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART1_6_9_10_KER_SRC_SETUP_EN CHIP_CFG_CLK_USART1_6_9_10_KER_SRC_SETUP_EN 0
#else
    #define CHIP_CLK_USART1_6_9_10_KER_SRC_SETUP_EN 0
#endif



#define CHIP_CLK_USART1_6_9_10_KER_SRC_EN  (CHIP_CLK_USART1_EN || CHIP_CLK_USART6_EN \
                                           || CHIP_CLK_USART10_EN || CHIP_CLK_UART9_EN \
                                           || CHIP_CLK_USART1_KER_SRC_SETUP_EN \
                                           || CHIP_CLK_USART6_KER_SRC_SETUP_EN \
                                           || CHIP_CLK_USART1_6_KER_SRC_SETUP_EN \
                                           || CHIP_CLK_UART9_KER_SRC_SETUP_EN \
                                           || CHIP_CLK_USART10_KER_SRC_SETUP_EN \
                                           || CHIP_CLK_USART1_6_9_10_KER_SRC_SETUP_EN \
                                          )

#if CHIP_CLK_USART1_6_9_10_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_USART1_6_9_10_KER_SRC
        #define CHIP_CLK_USART1_6_9_10_KER_SRC CHIP_CFG_CLK_USART1_6_9_10_KER_SRC
    #elif defined CHIP_CFG_CLK_USART1_6_KER_SRC
        #define CHIP_CLK_USART1_6_9_10_KER_SRC CHIP_CFG_CLK_USART1_6_KER_SRC
    #elif defined CHIP_CFG_CLK_USART16_KER_SRC
        #define CHIP_CLK_USART1_6_9_10_KER_SRC CHIP_CFG_CLK_USART16_KER_SRC
    #else
        #error "USART1,6,9,10 clock source is not specified"
    #endif
#endif

#define CHIP_CLK_USART1_6_9_10_KER_FREQ CHIP_CLK_ID_FREQ(CHIP_CLK_USART1_6_9_10_KER_SRC)
#define CHIP_CLK_USART1_6_KER_FREQ      CHIP_CLK_USART1_6_9_10_KER_FREQ
#define CHIP_CLK_USART1_KER_FREQ        CHIP_CLK_USART1_6_9_10_KER_FREQ
#define CHIP_CLK_USART6_KER_FREQ        CHIP_CLK_USART1_6_9_10_KER_FREQ
#define CHIP_CLK_UART9_KER_FREQ         CHIP_CLK_USART1_6_9_10_KER_FREQ
#define CHIP_CLK_USART10_KER_FREQ       CHIP_CLK_USART1_6_9_10_KER_FREQ
#define CHIP_CLK_USART1_KER_SRC_EN      CHIP_CLK_USART1_6_9_10_KER_SRC_EN
#define CHIP_CLK_USART6_KER_SRC_EN      CHIP_CLK_USART1_6_9_10_KER_SRC_EN
#define CHIP_CLK_USART1_KER_EN          (CHIP_CLK_USART1_EN && CHIP_CLK_USART1_6_9_10_KER_SRC_EN)
#define CHIP_CLK_USART6_KER_EN          (CHIP_CLK_USART6_EN && CHIP_CLK_USART1_6_9_10_KER_SRC_EN)
#define CHIP_CLK_UART9_KER_EN           (CHIP_CLK_UART9_EN && CHIP_CLK_USART1_6_9_10_KER_SRC_EN)
#define CHIP_CLK_USART10_KER_EN         (CHIP_CLK_USART10_EN) && CHIP_CLK_USART1_6_9_10_KER_SRC_EN

#define CHIP_CLK_USART1_FREQ            CHIP_CLK_USART1_KER_FREQ
#define CHIP_CLK_USART6_FREQ            CHIP_CLK_USART6_KER_FREQ
#define CHIP_CLK_UART9_FREQ             CHIP_CLK_UART9_KER_FREQ
#define CHIP_CLK_USART10_FREQ           CHIP_CLK_USART10_KER_FREQ


/* -------------- USART2,3 UART4,5,7,8 -----------------------*/
#ifdef CHIP_CFG_CLK_USART2_EN
    #define CHIP_CLK_USART2_EN CHIP_CFG_CLK_USART2_EN
#else
    #define CHIP_CLK_USART2_EN  0
#endif
#ifdef CHIP_CFG_CLK_USART3_EN
    #define CHIP_CLK_USART3_EN CHIP_CFG_CLK_USART3_EN
#else
    #define CHIP_CLK_USART3_EN  0
#endif
#ifdef CHIP_CFG_CLK_UART4_EN
    #define CHIP_CLK_UART4_EN   CHIP_CFG_CLK_UART4_EN
#else
    #define CHIP_CLK_UART4_EN   0
#endif
#ifdef CHIP_CFG_CLK_UART5_EN
    #define CHIP_CLK_UART5_EN   CHIP_CFG_CLK_UART5_EN
#else
    #define CHIP_CLK_UART5_EN   0
#endif
#ifdef CHIP_CFG_CLK_UART7_EN
    #define CHIP_CLK_UART7_EN   CHIP_CFG_CLK_UART7_EN
#else
    #define CHIP_CLK_UART7_EN   0
#endif
#ifdef CHIP_CFG_CLK_UART8_EN
    #define CHIP_CLK_UART8_EN   CHIP_CFG_CLK_UART8_EN
#else
    #define CHIP_CLK_UART8_EN   0
#endif

#ifdef CHIP_CFG_CLK_USART2_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART2_KER_SRC_SETUP_EN    CHIP_CFG_CLK_USART2_KER_SRC_SETUP_EN 0
#else
    #define CHIP_CLK_USART2_KER_SRC_SETUP_EN    0
#endif
#ifdef CHIP_CFG_CLK_USART3_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART3_KER_SRC_SETUP_EN    CHIP_CFG_CLK_USART3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USART3_KER_SRC_SETUP_EN    0
#endif
#ifdef CHIP_CFG_CLK_UART4_KER_SRC_SETUP_EN
    #define CHIP_CLK_UART4_KER_SRC_SETUP_EN     CHIP_CFG_CLK_UART4_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_UART4_KER_SRC_SETUP_EN     0
#endif
#ifdef CHIP_CFG_CLK_UART5_KER_SRC_SETUP_EN
    #define CHIP_CLK_UART5_KER_SRC_SETUP_EN     CHIP_CFG_CLK_UART5_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_UART5_KER_SRC_SETUP_EN     0
#endif
#ifdef CHIP_CFG_CLK_UART7_KER_SRC_SETUP_EN
    #define CHIP_CLK_UART7_KER_SRC_SETUP_EN     CHIP_CFG_CLK_UART7_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_UART7_KER_SRC_SETUP_EN     0
#endif
#ifdef CHIP_CFG_CLK_UART8_KER_SRC_SETUP_EN
    #define CHIP_CLK_UART8_KER_SRC_SETUP_EN     CHIP_CFG_CLK_UART8_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_UART8_KER_SRC_SETUP_EN     0
#endif
#ifdef CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC_SETUP_EN
    #define CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_SETUP_EN  CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_SETUP_EN  0
#endif

#define CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN (CHIP_CLK_USART2_EN \
                                          || CHIP_CLK_USART3_EN \
                                          || CHIP_CLK_UART4_EN \
                                          || CHIP_CLK_UART5_EN \
                                          || CHIP_CLK_UART7_EN \
                                          || CHIP_CLK_UART8_EN \
                                          || CHIP_CLK_USART2_KER_SRC_SETUP_EN \
                                          || CHIP_CLK_USART3_KER_SRC_SETUP_EN \
                                          || CHIP_CLK_UART4_KER_SRC_SETUP_EN \
                                          || CHIP_CLK_UART5_KER_SRC_SETUP_EN \
                                          || CHIP_CLK_UART7_KER_SRC_SETUP_EN \
                                          || CHIP_CLK_UART8_KER_SRC_SETUP_EN \
                                          || CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_SETUP_EN \
                                          )

#if CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC
        #define CHIP_CLK_USART2_3_4_5_7_8_KER_SRC CHIP_CFG_CLK_USART2_3_4_5_7_8_KER_SRC
    #elif defined CHIP_CFG_CLK_USART234578_KER_SRC
        #define CHIP_CLK_USART2_3_4_5_7_8_KER_SRC CHIP_CFG_CLK_USART234578_KER_SRC
    #else
        #error "USART2,3,4,5,7,8 clock source is not specified"
    #endif
#endif

#define CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ   CHIP_CLK_ID_FREQ(CHIP_CLK_USART2_3_4_5_7_8_KER_SRC)
#define CHIP_CLK_USART2_KER_SRC_EN      CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
#define CHIP_CLK_USART3_KER_SRC_EN      CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
#define CHIP_CLK_UART4_KER_SRC_EN       CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
#define CHIP_CLK_UART5_KER_SRC_EN       CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
#define CHIP_CLK_UART7_KER_SRC_EN       CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
#define CHIP_CLK_UART8_KER_SRC_EN       CHIP_CLK_USART2_3_4_5_7_8_KER_SRC_EN
#define CHIP_CLK_USART2_KER_FREQ        CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ
#define CHIP_CLK_USART3_KER_FREQ        CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ
#define CHIP_CLK_UART4_KER_FREQ         CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ
#define CHIP_CLK_UART5_KER_FREQ         CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ
#define CHIP_CLK_UART7_KER_FREQ         CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ
#define CHIP_CLK_UART8_KER_FREQ         CHIP_CLK_USART2_3_4_5_7_8_KER_FREQ
#define CHIP_CLK_USART2_KER_EN          (CHIP_CLK_USART2_EN && CHIP_CLK_USART2_KER_SRC_EN)
#define CHIP_CLK_USART3_KER_EN          (CHIP_CLK_USART3_EN && CHIP_CLK_USART3_KER_SRC_EN)
#define CHIP_CLK_UART4_KER_EN           (CHIP_CLK_UART4_EN  && CHIP_CLK_UART4_KER_SRC_EN)
#define CHIP_CLK_UART5_KER_EN           (CHIP_CLK_UART5_EN  && CHIP_CLK_UART5_KER_SRC_EN)
#define CHIP_CLK_UART7_KER_EN           (CHIP_CLK_UART7_EN  && CHIP_CLK_UART7_KER_SRC_EN)
#define CHIP_CLK_UART8_KER_EN           (CHIP_CLK_UART8_EN  && CHIP_CLK_UART8_KER_SRC_EN)


#define CHIP_CLK_USART2_FREQ            CHIP_CLK_USART2_KER_FREQ
#define CHIP_CLK_USART3_FREQ            CHIP_CLK_USART3_KER_FREQ
#define CHIP_CLK_UART4_FREQ             CHIP_CLK_UART4_KER_FREQ
#define CHIP_CLK_UART5_FREQ             CHIP_CLK_UART5_KER_FREQ
#define CHIP_CLK_UART7_FREQ             CHIP_CLK_UART7_KER_FREQ
#define CHIP_CLK_UART8_FREQ             CHIP_CLK_UART8_KER_FREQ

/* ------------------------- RNG -------------------------*/
#ifdef CHIP_CFG_CLK_RNG_EN
    #define CHIP_CLK_RNG_EN     CHIP_CFG_CLK_RNG_EN
#else
    #define CHIP_CLK_RNG_EN     0
#endif
#ifdef CHIP_CFG_CLK_RNG_KER_SRC_SETUP_EN
    #define CHIP_CLK_RNG_KER_SRC_SETUP_EN   CHIP_CFG_CLK_RNG_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_RNG_KER_SRC_SETUP_EN   0
#endif
#define CHIP_CLK_RNG_KER_SRC_EN         (CHIP_CLK_RNG_EN || CHIP_CLK_RNG_KER_SRC_SETUP_EN)
#if CHIP_CLK_RNG_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_RNG_KER_SRC
        #define CHIP_CLK_RNG_KER_SRC CHIP_CFG_CLK_RNG_KER_SRC
    #else
        #error "RNG kernel clock source is not specified"
    #endif
    #define CHIP_CLK_RNG_KER_FREQ       CHIP_CLK_ID_FREQ(CHIP_CLK_RNG_KER_SRC)
#else
    #define CHIP_CLK_RNG_KER_FREQ       0
#endif
#define CHIP_CLK_RNG_KER_EN             (CHIP_CFG_CLK_RNG_EN && CHIP_CLK_RNG_KER_SRC_EN)

/* ------------------------ I2C1,2,3,5 ----------------------*/
#ifdef CHIP_CFG_CLK_I2C1_EN
    #define CHIP_CLK_I2C1_EN    CHIP_CFG_CLK_I2C1_EN
#else
    #define CHIP_CLK_I2C1_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C2_EN
    #define CHIP_CLK_I2C2_EN    CHIP_CFG_CLK_I2C2_EN
#else
    #define CHIP_CLK_I2C2_EN    0
#endif
#ifdef CHIP_CFG_CLK_I2C3_EN
    #define CHIP_CLK_I2C3_EN    CHIP_CFG_CLK_I2C3_EN
#else
    #define CHIP_CLK_I2C3_EN    0
#endif
#if defined CHIP_CFG_CLK_I2C5_EN && CHIP_DEV_SUPPORT_I2C5
    #define CHIP_CLK_I2C5_EN    CHIP_CFG_CLK_I2C5_EN
#else
    #define CHIP_CLK_I2C5_EN    0
#endif

#ifdef CHIP_CFG_CLK_I2C1_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C1_KER_SRC_SETUP_EN  CHIP_CFG_CLK_I2C1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_I2C1_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_I2C2_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C2_KER_SRC_SETUP_EN  CHIP_CFG_CLK_I2C2_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_I2C2_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_I2C3_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C3_KER_SRC_SETUP_EN  CHIP_CFG_CLK_I2C3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_I2C3_KER_SRC_SETUP_EN  0
#endif
#if defined CHIP_CFG_CLK_I2C5_KER_SRC_SETUP_EN && CHIP_DEV_SUPPORT_I2C5
    #define CHIP_CLK_I2C5_KER_SRC_SETUP_EN  CHIP_CFG_CLK_I2C5_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_I2C5_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_I2C1_2_3_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C1_2_3_KER_SRC_SETUP_EN CHIP_CFG_CLK_I2C1_2_3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_I2C1_2_3_KER_SRC_SETUP_EN  0
#endif
#ifdef CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C1_2_3_5_KER_SRC_SETUP_EN CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_I2C1_2_3_5_KER_SRC_SETUP_EN  0
#endif

#define CHIP_CLK_I2C1_2_3_5_KER_SRC_EN  (CHIP_CLK_I2C1_EN || CHIP_CLK_I2C2_EN \
                                        || CHIP_CLK_I2C3_EN  || CHIP_CLK_I2C5_EN\
                                        || CHIP_CLK_I2C1_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_I2C2_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_I2C3_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_I2C5_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_I2C1_2_3_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_I2C1_2_3_5_KER_SRC_SETUP_EN \
                                        )

#if CHIP_CLK_I2C1_2_3_5_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC
        #define CHIP_CLK_I2C1_2_3_5_KER_SRC     CHIP_CFG_CLK_I2C1_2_3_5_KER_SRC
    #elif defined CHIP_CFG_CLK_I2C1_2_3_KER_SRC
        #define CHIP_CLK_I2C1_2_3_5_KER_SRC     CHIP_CFG_CLK_I2C1_2_3_KER_SRC
    #elif defined CHIP_CFG_CLK_I2C123_KER_SRC
        #define CHIP_CLK_I2C1_2_3_5_KER_SRC     CHIP_CFG_CLK_I2C123_KER_SRC
    #else
        #error "I2C1,2,3,5 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_I2C1_2_3_5_KER_FREQ        CHIP_CLK_ID_FREQ(CHIP_CLK_I2C1_2_3_5_KER_SRC)
#else
    #define CHIP_CLK_I2C1_2_3_5_KER_FREQ        0
#endif

#define CHIP_CLK_I2C1_KER_SRC_EN      CHIP_CLK_I2C1_2_3_5_KER_SRC_EN
#define CHIP_CLK_I2C2_KER_SRC_EN      CHIP_CLK_I2C1_2_3_5_KER_SRC_EN
#define CHIP_CLK_I2C3_KER_SRC_EN      CHIP_CLK_I2C1_2_3_5_KER_SRC_EN
#define CHIP_CLK_I2C5_KER_SRC_EN      (CHIP_CLK_I2C1_2_3_5_KER_SRC_EN && CHIP_DEV_SUPPORT_I2C5)
#define CHIP_CLK_I2C1_KER_FREQ        CHIP_CLK_I2C1_2_3_5_KER_FREQ
#define CHIP_CLK_I2C2_KER_FREQ        CHIP_CLK_I2C1_2_3_5_KER_FREQ
#define CHIP_CLK_I2C3_KER_FREQ        CHIP_CLK_I2C1_2_3_5_KER_FREQ
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_CLK_I2C5_KER_FREQ        CHIP_CLK_I2C1_2_3_5_KER_FREQ
#else
#define CHIP_CLK_I2C5_KER_FREQ        0
#endif
#define CHIP_CLK_I2C1_KER_EN          (CHIP_CLK_I2C1_EN && CHIP_CLK_I2C1_KER_SRC_EN)
#define CHIP_CLK_I2C2_KER_EN          (CHIP_CLK_I2C2_EN && CHIP_CLK_I2C2_KER_SRC_EN)
#define CHIP_CLK_I2C3_KER_EN          (CHIP_CLK_I2C3_EN && CHIP_CLK_I2C3_KER_SRC_EN)
#define CHIP_CLK_I2C5_KER_EN          (CHIP_CLK_I2C5_EN && CHIP_CLK_I2C5_KER_SRC_EN)



/* -------------------------- USB -----------------------------------*/
#ifdef CHIP_CFG_CLK_USB1_OTGHS_EN
    #define CHIP_CLK_USB1_OTGHS_EN  CHIP_CFG_CLK_USB1_OTGHS_EN
#else
    #define CHIP_CLK_USB1_OTGHS_EN  0
#endif
#if defined CHIP_CFG_CLK_USB2_OTGHS_EN && CHIP_DEV_SUPPORT_USB2
    #define CHIP_CLK_USB2_OTGHS_EN  CHIP_CFG_CLK_USB2_OTGHS_EN
#else
    #define CHIP_CLK_USB2_OTGHS_EN  0
#endif

#ifdef CHIP_CFG_CLK_USB1_OTGHS_KER_SRC_SETUP_EN
    #define CHIP_CLK_USB1_OTGHS_KER_SRC_SETUP_EN CHIP_CFG_CLK_USB1_OTGHS_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USB1_OTGHS_KER_SRC_SETUP_EN 0
#endif
#if defined CHIP_CFG_CLK_USB2_OTGHS_KER_SRC_SETUP_EN && CHIP_DEV_SUPPORT_USB2
    #define CHIP_CLK_USB2_OTGHS_KER_SRC_SETUP_EN CHIP_CFG_CLK_USB2_OTGHS_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USB2_OTGHS_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_USB_KER_SRC_SETUP_EN
    #define CHIP_CLK_USB_KER_SRC_SETUP_EN CHIP_CLK_CFG_USB_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_USB_KER_SRC_SETUP_EN 0
#endif

#define CHIP_CLK_USB_KER_SRC_EN         (CHIP_CLK_USB1_OTGHS_EN || CHIP_CLK_USB2_OTGHS_EN \
                                        || CHIP_CLK_USB1_OTGHS_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_USB2_OTGHS_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_USB_KER_SRC_SETUP_EN)

#if CHIP_CLK_USB_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_USB_KER_SRC
        #define CHIP_CLK_USB_KER_SRC CHIP_CFG_CLK_USB_KER_SRC
    #else
        #error "USB kernel clock source is not selected"
    #endif
    #define CHIP_CLK_USB_KER_FREQ           CHIP_CLK_ID_FREQ(CHIP_CLK_USB_KER_SRC)
#else
    #define CHIP_CLK_USB_KER_FREQ           0
#endif

#define CHIP_CLK_USB1_OTGHS_KER_SRC_EN  CHIP_CLK_USB_KER_SRC_EN
#define CHIP_CLK_USB2_OTGHS_KER_SRC_EN  CHIP_CLK_USB_KER_SRC_EN
#define CHIP_CLK_USB1_OTGHS_KER_FREQ    CHIP_CLK_USB_KER_FREQ
#define CHIP_CLK_USB2_OTGHS_KER_FREQ    CHIP_CLK_USB_KER_FREQ
#define CHIP_CLK_USB1_OTGHS_KER_EN      (CHIP_CLK_USB1_OTGHS_EN && CHIP_CLK_USB_KER_SRC_EN)
#define CHIP_CLK_USB2_OTGHS_KER_EN      (CHIP_CLK_USB2_OTGHS_EN && CHIP_CLK_USB_KER_SRC_EN)


/* -------------------------- CEC ----------------------------------*/
#ifdef CHIP_CFG_CLK_CEC_EN
    #define CHIP_CLK_CEC_EN CHIP_CFG_CLK_CEC_EN
#else
    #define CHIP_CLK_CEC_EN 0
#endif
#ifdef CHIP_CFG_CLK_CEC_KER_SRC_SETUP_EN
    #define CHIP_CLK_CEC_KER_SRC_SETUP_EN CHIP_CFG_CLK_CEC_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_CEC_KER_SRC_SETUP_EN 0
#endif
#define CHIP_CLK_CEC_KER_SRC_EN         (CHIP_CLK_CEC_EN || CHIP_CLK_CEC_KER_SRC_SETUP_EN)
#if CHIP_CLK_CEC_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_CEC_KER_SRC
        #define CHIP_CLK_CEC_KER_SRC CHIP_CFG_CLK_CEC_KER_SRC
    #else
        #error "CEC kernel clock source is not specified"
    #endif
    #define CHIP_CLK_CEC_KER_FREQ       CHIP_CLK_ID_FREQ(CHIP_CLK_CEC_KER_SRC)
#else
    #define CHIP_CLK_CEC_KER_FREQ       0
#endif
#define CHIP_CLK_CEC_KER_EN             (CHIP_CLK_CEC_EN && CHIP_CLK_CEC_KER_SRC_EN)

/* ------------------------ LPTIM1 ---------------------------------*/
#ifdef CHIP_CFG_CLK_LPTIM1_EN
    #define CHIP_CLK_LPTIM1_EN          CHIP_CFG_CLK_LPTIM1_EN
#else
    #define CHIP_CLK_LPTIM1_EN          0
#endif
#ifdef CHIP_CFG_CLK_LPTIM1_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM1_KER_SRC_SETUP_EN    CHIP_CFG_CLK_LPTIM1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_LPTIM1_KER_SRC_SETUP_EN    0
#endif
#define CHIP_CLK_LPTIM1_KER_SRC_EN     (CHIP_CLK_LPTIM1_EN || CHIP_CLK_LPTIM1_KER_SRC_SETUP_EN)
#if CHIP_CLK_LPTIM1_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_LPTIM1_KER_SRC
        #define CHIP_CLK_LPTIM1_KER_SRC CHIP_CFG_CLK_LPTIM1_KER_SRC
    #else
        #error "LPTIM kernel clock source is not specified"
    #endif
    #define CHIP_CLK_LPTIM1_KER_FREQ    CHIP_CLK_ID_FREQ(CHIP_CLK_LPTIM1_KER_SRC)
#else
    #define CHIP_CLK_LPTIM1_KER_FREQ    0
#endif
#define CHIP_CLK_LPTIM1_KER_EN          (CHIP_CLK_LPTIM1_EN && CHIP_CLK_LPTIM1_KER_SRC_EN)


/* ------------------------ LPUART1 ---------------------------------*/
#if defined CHIP_CFG_CLK_LPUART1_EN
    #define CHIP_CLK_LPUART1_EN         CHIP_CFG_CLK_LPUART1_EN
#else
    #define CHIP_CLK_LPUART1_EN         0
#endif
#if defined CHIP_CFG_CLK_LPUART1_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPUART1_KER_SRC_SETUP_EN   CHIP_CFG_CLK_LPUART1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_LPUART1_KER_SRC_SETUP_EN   0
#endif
#define CHIP_CLK_LPUART1_KER_SRC_EN     (CHIP_CLK_LPUART1_EN || CHIP_CLK_LPUART1_KER_SRC_SETUP_EN)
#if CHIP_CLK_LPUART1_KER_SRC_EN
    #if defined CHIP_CFG_CLK_LPUART1_KER_SRC
        #define CHIP_CLK_LPUART1_KER_SRC CHIP_CFG_CLK_LPUART1_KER_SRC
    #else
        #error "LPUART kernel clock source is not specified"
    #endif
    #define CHIP_CLK_LPUART1_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_LPUART1_KER_SRC)
#else
    #define CHIP_CLK_LPUART1_KER_FREQ   0
#endif
#define CHIP_CLK_LPUART1_KER_EN     (CHIP_CLK_LPUART1_EN && CHIP_CLK_LPUART1_KER_SRC_EN)


/* -------------------------- I2C4 ----------------------------------*/
#if defined CHIP_CFG_CLK_I2C4_EN
    #define CHIP_CLK_I2C4_EN            CHIP_CFG_CLK_I2C4_EN
#else
    #define CHIP_CLK_I2C4_EN            0
#endif
#if defined CHIP_CFG_CLK_I2C4_KER_SRC_SETUP_EN
    #define CHIP_CLK_I2C4_KER_SRC_SETUP_EN CHIP_CFG_CLK_I2C4_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_I2C4_KER_SRC_SETUP_EN 0
#endif
#define CHIP_CLK_I2C4_KER_SRC_EN        (CHIP_CLK_I2C4_EN || CHIP_CLK_I2C4_KER_SRC_SETUP_EN)
#if CHIP_CLK_I2C4_KER_SRC_EN
    #if defined CHIP_CFG_CLK_I2C4_KER_SRC
        #define CHIP_CLK_I2C4_KER_SRC CHIP_CFG_CLK_I2C4_KER_SRC
    #else
        #error I2C4 kernel clock source is not specified
    #endif
    #define CHIP_CLK_I2C4_KER_FREQ      CHIP_CLK_ID_FREQ(CHIP_CLK_I2C4_KER_SRC)
#else
    #define CHIP_CLK_I2C4_KER_FREQ      0
#endif
#define CHIP_CLK_I2C4_KER_EN            CHIP_CFG_CLK_I2C4_EN


/* ------------------------ LPTIM2 ---------------------------------*/
#ifdef CHIP_CFG_CLK_LPTIM2_EN
    #define CHIP_CLK_LPTIM2_EN          CHIP_CFG_CLK_LPTIM2_EN
#else
    #define CHIP_CLK_LPTIM2_EN          0
#endif
#ifdef CHIP_CFG_CLK_LPTIM2_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM2_KER_SRC_SETUP_EN    CHIP_CFG_CLK_LPTIM2_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_LPTIM2_KER_SRC_SETUP_EN    0
#endif
#define CHIP_CLK_LPTIM2_KER_SRC_EN   (CHIP_CLK_LPTIM2_EN || CHIP_CLK_LPTIM2_KER_SRC_SETUP_EN)
#if CHIP_CLK_LPTIM2_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_LPTIM2_KER_SRC
        #define CHIP_CLK_LPTIM2_KER_SRC CHIP_CFG_CLK_LPTIM2_KER_SRC
    #else
        #error "LPTIM2 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_LPTIM2_KER_FREQ     CHIP_CLK_ID_FREQ(CHIP_CLK_LPTIM2_KER_SRC)
#else
    #define CHIP_CLK_LPTIM2_KER_FREQ     0
#endif
#define CHIP_CLK_LPTIM2_KER_EN          (CHIP_CLK_LPTIM2_EN && CHIP_CLK_LPTIM2_KER_SRC_EN)


/* --------------------- LPTIM3,4,5 --------------------------------*/
#ifdef CHIP_CFG_CLK_LPTIM3_EN
    #define CHIP_CLK_LPTIM3_EN          CHIP_CFG_CLK_LPTIM3_EN
#else
    #define CHIP_CLK_LPTIM3_EN          0
#endif
#ifdef CHIP_CFG_CLK_LPTIM4_EN
    #define CHIP_CLK_LPTIM4_EN          CHIP_CFG_CLK_LPTIM4_EN
#else
    #define CHIP_CLK_LPTIM4_EN          0
#endif
#ifdef CHIP_CFG_CLK_LPTIM5_EN
    #define CHIP_CLK_LPTIM5_EN          CHIP_CFG_CLK_LPTIM5_EN
#else
    #define CHIP_CLK_LPTIM5_EN          0
#endif
#ifdef CHIP_CFG_CLK_LPTIM3_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM3_KER_SRC_SETUP_EN    CHIP_CFG_CLK_LPTIM3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_LPTIM3_KER_SRC_SETUP_EN    0
#endif
#ifdef CHIP_CFG_CLK_LPTIM4_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM4_KER_SRC_SETUP_EN    CHIP_CFG_CLK_LPTIM4_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_LPTIM4_KER_SRC_SETUP_EN    0
#endif
#ifdef CHIP_CFG_CLK_LPTIM5_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM5_KER_SRC_SETUP_EN    CHIP_CFG_CLK_LPTIM5_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_LPTIM5_KER_SRC_SETUP_EN    0
#endif
#ifdef CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC_SETUP_EN
    #define CHIP_CLK_LPTIM3_4_5_KER_SRC_SETUP_EN    CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_LPTIM3_4_5_KER_SRC_SETUP_EN    0
#endif

#define CHIP_CLK_LPTIM3_4_5_KER_SRC_EN  (CHIP_CLK_LPTIM3_EN || CHIP_CLK_LPTIM4_EN || CHIP_CLK_LPTIM5_EN \
                                        || CHIP_CLK_LPTIM3_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_LPTIM4_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_LPTIM5_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_LPTIM3_4_5_KER_SRC_SETUP_EN)

#if CHIP_CLK_LPTIM3_4_5_KER_SRC_EN
    #if defined CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC
        #define CHIP_CLK_LPTIM3_4_5_KER_SRC CHIP_CFG_CLK_LPTIM3_4_5_KER_SRC
    #elif defined CHIP_CFG_CLK_LPTIM345_KER_SRC
        #define CHIP_CLK_LPTIM3_4_5_KER_SRC CHIP_CFG_CLK_LPTIM345_KER_SRC
    #else
        #error "LPTIM3,4,5 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_LPTIM3_4_5_KER_FREQ    CHIP_CLK_ID_FREQ(CHIP_CLK_LPTIM3_4_5_KER_SRC)
#else
    #define CHIP_CLK_LPTIM3_4_5_KER_FREQ    0
#endif
#define CHIP_CLK_LPTIM3_KER_SRC_EN     CHIP_CLK_LPTIM3_4_5_KER_SRC_EN
#define CHIP_CLK_LPTIM4_KER_SRC_EN     CHIP_CLK_LPTIM3_4_5_KER_SRC_EN
#define CHIP_CLK_LPTIM5_KER_SRC_EN     CHIP_CLK_LPTIM3_4_5_KER_SRC_EN
#define CHIP_CLK_LPTIM3_KER_FREQ       CHIP_CLK_LPTIM3_4_5_KER_FREQ
#define CHIP_CLK_LPTIM4_KER_FREQ       CHIP_CLK_LPTIM3_4_5_KER_FREQ
#define CHIP_CLK_LPTIM5_KER_FREQ       CHIP_CLK_LPTIM3_4_5_KER_FREQ
#define CHIP_CLK_LPTIM3_KER_EN         (CHIP_CLK_LPTIM3_EN && CHIP_CLK_LPTIM3_KER_SRC_EN)
#define CHIP_CLK_LPTIM4_KER_EN         (CHIP_CLK_LPTIM4_EN && CHIP_CLK_LPTIM4_KER_SRC_EN)
#define CHIP_CLK_LPTIM5_KER_EN         (CHIP_CLK_LPTIM5_EN && CHIP_CLK_LPTIM5_KER_SRC_EN)


/* ---------------------- ADC1,2,3 ------------------------------*/
#ifdef CHIP_CFG_CLK_ADC1_EN
    #define CHIP_CLK_ADC1_EN    CHIP_CFG_CLK_ADC1_EN
#else
    #define CHIP_CLK_ADC1_EN    0
#endif
#ifdef CHIP_CFG_CLK_ADC2_EN
    #define CHIP_CLK_ADC2_EN    CHIP_CFG_CLK_ADC2_EN
#else
    #define CHIP_CLK_ADC2_EN    0
#endif
#ifdef CHIP_CFG_CLK_ADC3_EN
    #define CHIP_CLK_ADC3_EN    CHIP_CFG_CLK_ADC3_EN
#else
    #define CHIP_CLK_ADC3_EN    0
#endif

#ifdef CHIP_CFG_CLK_ADC_KER_SRC_SETUP_EN
    #define CHIP_CLK_ADC_KER_SRC_SETUP_EN CHIP_CFG_CLK_ADC_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_ADC_KER_SRC_SETUP_EN 0
#endif

#ifdef CHIP_CFG_CLK_ADC1_KER_SRC_SETUP_EN
    #define CHIP_CLK_ADC1_KER_SRC_SETUP_EN CHIP_CFG_CLK_ADC1_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_ADC1_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_ADC2_KER_SRC_SETUP_EN
    #define CHIP_CLK_ADC2_KER_SRC_SETUP_EN CHIP_CFG_CLK_ADC2_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_ADC2_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_ADC3_KER_SRC_SETUP_EN
    #define CHIP_CLK_ADC3_KER_SRC_SETUP_EN CHIP_CFG_CLK_ADC3_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_ADC3_KER_SRC_SETUP_EN 0
#endif

#ifdef CHIP_CFG_CLK_ADC_SCLK_EN
    #define CHIP_CLK_ADC_SCLK_EN    CHIP_CFG_CLK_ADC_SCLK_EN
#else
    #define CHIP_CLK_ADC_SCLK_EN    0
#endif

#ifdef CHIP_CFG_CLK_ADC1_SCLK_EN
    #define CHIP_CLK_ADC1_SCLK_EN   CHIP_CFG_CLK_ADC1_SCLK_EN
#else
    #define CHIP_CLK_ADC1_SCLK_EN   0
#endif
#ifdef CHIP_CFG_CLK_ADC2_SCLK_EN
    #define CHIP_CLK_ADC2_SCLK_EN   CHIP_CFG_CLK_ADC2_SCLK_EN
#else
    #define CHIP_CLK_ADC2_SCLK_EN   0
#endif
#ifdef CHIP_CFG_CLK_ADC3_SCLK_EN
    #define CHIP_CLK_ADC3_SCLK_EN   CHIP_CFG_CLK_ADC3_SCLK_EN
#else
    #define CHIP_CLK_ADC3_SCLK_EN   0
#endif


#define CHIP_CLK_ADC1_2_EN          (CHIP_CLK_ADC1_EN || CHIP_CLK_ADC2_EN)
#define CHIP_CLK_ADC1_2_SCLK_EN     (CHIP_CLK_ADC1_SCLK_EN || CHIP_CLK_ADC2_SCLK_EN || CHIP_CLK_ADC_SCLK_EN)

#define CHIP_CLK_ADC_KER_SRC_EN     (CHIP_CLK_ADC1_2_EN || CHIP_CLK_ADC3_EN \
                                        || CHIP_CLK_ADC_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_ADC1_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_ADC2_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_ADC3_KER_SRC_SETUP_EN)
#if CHIP_CLK_ADC_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_ADC_KER_SRC
        #define CHIP_CLK_ADC_KER_SRC CHIP_CFG_CLK_ADC_KER_SRC
    #else
        #error "ADC1,2,3 kernel clock source is not specified"
    #endif
    #define CHIP_CLK_ADC_KER_FREQ   CHIP_CLK_ID_FREQ(CHIP_CLK_ADC_KER_SRC)
#else
    #define CHIP_CLK_ADC_KER_FREQ   0
#endif

#define CHIP_CLK_ADC1_KER_FREQ        CHIP_CLK_ADC_KER_FREQ
#define CHIP_CLK_ADC2_KER_FREQ        CHIP_CLK_ADC_KER_FREQ
#define CHIP_CLK_ADC3_KER_FREQ        CHIP_CLK_ADC_KER_FREQ
#define CHIP_CLK_ADC1_KER_EN          (CHIP_CLK_ADC1_1_EN && CHIP_CLK_ADC_KER_SRC_EN)
#define CHIP_CLK_ADC2_KER_EN          (CHIP_CLK_ADC1_2_EN && CHIP_CLK_ADC_KER_SRC_EN)
#define CHIP_CLK_ADC3_KER_EN          (CHIP_CLK_ADC3_EN && CHIP_CLK_ADC_KER_SRC_EN)

#if CHIP_CFG_CLK_HOST_DIV == 1
    #define CHIP_CLK_ADC_SCLK_FREQ    (CHIP_CLK_SYS_FREQ/2)
#else
    #define CHIP_CLK_ADC_SCLK_FREQ    (CHIP_CLK_SYS_FREQ)
#endif

/* -------------------------- SAI4A ----------------------------*/
#ifdef CHIP_CFG_CLK_SAI4_EN
    #define CHIP_CLK_SAI4_EN    CHIP_CFG_CLK_SAI4_EN
#else
    #define CHIP_CLK_SAI4_EN    0
#endif

#ifdef CHIP_CFG_CLK_SAI4_KER_SRC_SETUP_EN
    #define CHIP_CLK_SAI4_KER_SRC_SETUP_EN CHIP_CFG_CLK_SAI4_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SAI4_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_SAI4A_KER_SRC_SETUP_EN
    #define CHIP_CLK_SAI4A_KER_SRC_SETUP_EN CHIP_CFG_CLK_SAI4A_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SAI4A_KER_SRC_SETUP_EN 0
#endif
#ifdef CHIP_CFG_CLK_SAI4B_KER_SRC_SETUP_EN
    #define CHIP_CLK_SAI4B_KER_SRC_SETUP_EN CHIP_CFG_CLK_SAI4B_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SAI4B_KER_SRC_SETUP_EN 0
#endif


#define CHIP_CLK_SAI4A_KER_SRC_EN    (CHIP_CLK_SAI4_EN || CHIP_CLK_SAI4_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_SAI4A_KER_SRC_SETUP_EN)
#if CHIP_CLK_SAI4A_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_SAI4A_KER_SRC
        #define CHIP_CLK_SAI4A_KER_SRC CHIP_CFG_CLK_SAI4A_KER_SRC
    #else
        #error "SAI4A kernel clock source is not specified"
    #endif
    #define CHIP_CLK_SAI4A_KER_FREQ      CHIP_CLK_ID_FREQ(CHIP_CLK_SAI4A_KER_SRC)
#else
    #define CHIP_CLK_SAI4A_KER_FREQ      0
#endif
#define CHIP_CLK_SAI4A_KER_EN           (CHIP_CLK_SAI4_EN && CHIP_CLK_SAI4A_KER_SRC_EN)

#define CHIP_CLK_SAI4B_KER_SRC_EN    (CHIP_CLK_SAI4_EN || CHIP_CLK_SAI4_KER_SRC_SETUP_EN \
                                        || CHIP_CLK_SAI4B_KER_SRC_SETUP_EN)
#if CHIP_CLK_SAI4B_KER_SRC_EN
    #ifdef CHIP_CFG_CLK_SAI4B_KER_SRC
        #define CHIP_CLK_SAI4B_KER_SRC CHIP_CFG_CLK_SAI4B_KER_SRC
    #else
        #error "SAI4B kernel clock source is not specified"
    #endif
    #define CHIP_CLK_SAI4B_KER_FREQ      CHIP_CLK_ID_FREQ(CHIP_CLK_SAI4B_KER_SRC)
#else
    #define CHIP_CLK_SAI4B_KER_FREQ      0
#endif
#define CHIP_CLK_SAI4B_KER_EN        CHIP_CLK_SAI4B_EN

#define CHIP_CLK_SAI4_KER_SRC_EN     (CHIP_CLK_SAI4A_KER_SRC_EN || CHIP_CLK_SAI4B_KER_SRC_EN)



/* -------------------------- SPI6 ----------------------------*/
#if defined CHIP_CFG_CLK_SPI6_EN
    #define CHIP_CLK_SPI6_EN CHIP_CFG_CLK_SPI6_EN
#else
    #define CHIP_CLK_SPI6_EN 0
#endif
#if defined CHIP_CFG_CLK_SPI6_KER_SRC_SETUP_EN
    #define CHIP_CLK_SPI6_KER_SRC_SETUP_EN CHIP_CFG_CLK_SPI6_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_SPI6_KER_SRC_SETUP_EN 0
#endif
#define CHIP_CLK_SPI6_KER_SRC_EN    (CHIP_CLK_SPI6_EN || CHIP_CLK_SPI6_KER_SRC_SETUP_EN)
#if CHIP_CLK_SPI6_KER_SRC_EN
#if defined CHIP_CFG_CLK_SPI6_KER_SRC
    #define CHIP_CLK_SPI6_KER_SRC CHIP_CFG_CLK_SPI6_KER_SRC
#else
    #error "SPI6 clock source is not specified"
#endif
#define CHIP_CLK_SPI6_KER_FREQ      CHIP_CLK_ID_FREQ(CHIP_CLK_SPI6_KER_SRC)
#else
#define CHIP_CLK_SPI6_KER_FREQ      0
#endif
#define CHIP_CLK_SPI6_KER_EN        (CHIP_CLK_SPI6_EN && CHIP_CLK_SPI6_KER_SRC_EN)

/* --------------------------- DSI --------------------------------------------*/
#if defined CHIP_CFG_CLK_DSI_EN && CHIP_DEV_SUPPORT_DSI
    #define CHIP_CLK_DSI_EN         CHIP_CFG_CLK_DSI_EN
#else
    #define CHIP_CLK_DSI_EN         0
#endif

#if defined CHIP_CFG_CLK_DSI_KER_SRC_SETUP_EN && CHIP_DEV_SUPPORT_DSI
    #define CHIP_CLK_DSI_KER_SRC_SETUP_EN   CHIP_CFG_CLK_DSI_KER_SRC_SETUP_EN
#else
    #define CHIP_CLK_DSI_KER_SRC_SETUP_EN   0
#endif
#define CHIP_CLK_DSI_KER_SRC_EN     (CHIP_CLK_DSI_EN || CHIP_CLK_DSI_KER_SRC_SETUP_EN)
#if CHIP_CLK_DSI_KER_SRC_EN
#if defined CHIP_CFG_CLK_DSI_KER_SRC
    #define CHIP_CLK_DSI_KER_SRC   CHIP_CFG_CLK_DSI_KER_SRC
#else
    #error "DSI clock source is not specified"
#endif
#define CHIP_CLK_DSI_KER_FREQ      CHIP_CLK_ID_FREQ(CHIP_CLK_DSI_KER_SRC)
#else
#define CHIP_CLK_DSI_KER_FREQ      0
#endif
#define CHIP_CLK_DSI_KER_EN        (CHIP_CLK_DSI_EN && CHIP_CLK_DSI_KER_SRC_EN)


/* --------------------------- DAC1, DAC2 -------------------------------------*/
#ifdef CHIP_CFG_CLK_DAC1_EN
    #define CHIP_CLK_DAC1_EN    CHIP_CFG_CLK_DAC1_EN
#else
    #define CHIP_CLK_DAC1_EN    0
#endif
#ifdef CHIP_CFG_CLK_DAC2_EN
    #define CHIP_CLK_DAC2_EN    CHIP_CFG_CLK_DAC2_EN
#else
    #define CHIP_CLK_DAC2_EN    0
#endif
#define CHIP_CLK_DAC12_EN       (CHIP_CLK_DAC1_EN || CHIP_CLK_DAC2_EN)
#define CHIP_CLK_DAC12_FREQ     CHIP_CLK_APB1_FREQ
#define CHIP_CLK_DAC1_FREQ      CHIP_CLK_DAC12_FREQ
#define CHIP_CLK_DAC2_FREQ      CHIP_CLK_DAC12_FREQ

/* ----------- фиксированные частоты без выбора источника --------------------*/
#ifdef CHIP_CFG_CLK_MDMA_EN
    #define CHIP_CLK_MDMA_EN        CHIP_CFG_CLK_MDMA_EN
#else
    #define CHIP_CLK_MDMA_EN        0
#endif
#define CHIP_CLK_MDMA_FREQ          CHIP_CLK_AHB3_FREQ

#ifdef CHIP_CFG_CLK_DMA2D_EN
    #define CHIP_CLK_DMA2D_EN       CHIP_CFG_CLK_DMA2D_EN
#else
    #define CHIP_CLK_DMA2D_EN       0
#endif
#define CHIP_CLK_DMA2D_FREQ         CHIP_CLK_AHB3_FREQ

#if defined CHIP_CFG_CLK_JPGDEC_EN && CHIP_DEV_SUPPORT_JPGDEC
    #define CHIP_CLK_JPGDEC_EN      CHIP_CFG_CLK_JPGDEC_EN
#else
    #define CHIP_CLK_JPGDEC_EN      0
#endif
#define CHIP_CLK_JPGDEC_FREQ        CHIP_CLK_AHB3_FREQ



#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
    #ifdef CHIP_CFG_CLK_CM4_FLASH_EN
        #define CHIP_CLK_CM4_FLASH_EN       CHIP_CFG_CLK_CM4_FLASH_EN
    #else
        #define CHIP_CLK_CM4_FLASH_EN       0
    #endif
    #ifdef CHIP_CFG_CLK_CM4_DTCM1_EN
        #define CHIP_CLK_CM4_DTCM1_EN       CHIP_CFG_CLK_CM4_DTCM1_EN
    #else
        #define CHIP_CLK_CM4_DTCM1_EN       0
    #endif
    #ifdef CHIP_CFG_CLK_CM4_DTCM2_EN
        #define CHIP_CLK_CM4_DTCM2_EN       CHIP_CFG_CLK_CM4_DTCM2_EN
    #else
        #define CHIP_CLK_CM4_DTCM2_EN       0
    #endif
    #ifdef CHIP_CFG_CLK_CM4_ITCM_EN
        #define CHIP_CLK_CM4_ITCM_EN        CHIP_CFG_CLK_CM4_ITCM_EN
    #else
        #define CHIP_CLK_CM4_ITCM_EN        0
    #endif
    #ifdef CHIP_CFG_CLK_CM4_AXISRAM_EN
        #define CHIP_CLK_CM4_AXISRAM_EN     CHIP_CFG_CLK_CM4_AXISRAM_EN
    #else
        #define CHIP_CLK_CM4_AXISRAM_EN     0
    #endif
#else
    #define CHIP_CLK_CM4_FLASH_EN           0
    #define CHIP_CLK_CM4_DTCM1_EN           0
    #define CHIP_CLK_CM4_DTCM2_EN           0
    #define CHIP_CLK_CM4_ITCM_EN            0
    #define CHIP_CLK_CM4_AXISRAM            0
#endif


#ifdef CHIP_CFG_CLK_DMA1_EN
    #define CHIP_CLK_DMA1_EN        CHIP_CFG_CLK_DMA1_EN
#else
    #define CHIP_CLK_DMA1_EN        0
#endif
#define CHIP_CLK_DMA1_FREQ          CHIP_CLK_AHB1_FREQ

#ifdef CHIP_CFG_CLK_DMA2_EN
    #define CHIP_CLK_DMA2_EN        CHIP_CFG_CLK_DMA1_EN
#else
    #define CHIP_CLK_DMA2_EN        0
#endif
#define CHIP_CLK_DMA2_FREQ          CHIP_CLK_AHB1_FREQ

#if defined CHIP_CFG_CLK_ART_EN && CHIP_DEV_SUPPORT_CORE_CM4
    #define CHIP_CLK_ART_EN         CHIP_CFG_CLK_ART_EN
#else
    #define CHIP_CLK_ART_EN         0
#endif
#define CHIP_CLK_ART_FREQ           CHIP_CLK_AHB1_FREQ

#ifdef CHIP_CFG_CLK_ETH1_MAC_EN
    #define CHIP_CLK_ETH1_MAC_EN    CHIP_CFG_CLK_ETH1_MAC_EN
#else
    #define CHIP_CLK_ETH1_MAC_EN    0
#endif
#define CHIP_CLK_ETH1_MAC_FREQ      CHIP_CLK_AHB1_FREQ
#define CHIP_CLK_ETH_MAC_FREQ       CHIP_CLK_ETH1_MAC_FREQ

#ifdef CHIP_CFG_CLK_ETH1_TX_EN
    #define CHIP_CLK_ETH1_TX_EN     CHIP_CFG_CLK_ETH1_TX_EN
#else
    #define CHIP_CLK_ETH1_TX_EN     0
#endif
#define CHIP_CLK_ETH1_TX_FREQ      CHIP_CLK_AHB1_FREQ

#ifdef CHIP_CFG_CLK_ETH1_RX_EN
    #define CHIP_CLK_ETH1_RX_EN     CHIP_CFG_CLK_ETH1_RX_EN
#else
    #define CHIP_CLK_ETH1_RX_EN     0
#endif
#define CHIP_CLK_ETH1_RX_FREQ      CHIP_CLK_AHB1_FREQ

#define CHIP_CLK_PTP_FREQ          CHIP_CLK_ETH1_MAC_FREQ


#ifdef CHIP_CFG_CLK_USB1_OTGHS_ULPI_EN
    #define CHIP_CLK_USB1_OTGHS_ULPI_EN CHIP_CFG_CLK_USB1_OTGHS_ULPI_EN
#else
    #define CHIP_CLK_USB1_OTGHS_ULPI_EN 0
#endif
#define CHIP_CLK_USB1_OTGHS_ULPI_FREQ CHIP_CLK_AHB1_FREQ

#if defined CHIP_CFG_CLK_USB2_OTGHS_ULPI_EN && CHIP_DEV_SUPPORT_USB2
    #define CHIP_CLK_USB2_OTGHS_ULPI_EN CHIP_CFG_CLK_USB2_OTGHS_ULPI_EN
#else
    #define CHIP_CLK_USB2_OTGHS_ULPI_EN 0
#endif
#define CHIP_CLK_USB2_OTGHS_ULPI_FREQ CHIP_CLK_AHB1_FREQ

#ifdef CHIP_CFG_CLK_CRYPT_EN
    #define CHIP_CLK_CRYPT_EN   CHIP_CFG_CLK_CRYPT_EN
#else
    #define CHIP_CLK_CRYPT_EN   0
#endif
#define CHIP_CLK_CRYPT_FREQ     CHIP_CLK_AHB2_FREQ

#ifdef CHIP_CFG_CLK_HASH_EN
    #define CHIP_CLK_HASH_EN   CHIP_CFG_CLK_HASH_EN
#else
    #define CHIP_CLK_HASH_EN   0
#endif
#define CHIP_CLK_HASH_FREQ      CHIP_CLK_AHB2_FREQ

#if defined CHIP_CFG_CLK_FMAC_EN && CHIP_DEV_SUPPORT_FMAC
    #define CHIP_CLK_FMAC_EN   CHIP_CFG_CLK_FMAC_EN
#else
    #define CHIP_CLK_FMAC_EN   0
#endif
#define CHIP_CLK_FMAC_FREQ      CHIP_CLK_AHB2_FREQ

#if defined CHIP_CFG_CLK_CORDIC_EN && CHIP_DEV_SUPPORT_CORDIC
    #define CHIP_CLK_CORDIC_EN   CHIP_CFG_CLK_CORDIC_EN
#else
    #define CHIP_CLK_CORDIC_EN   0
#endif
#define CHIP_CLK_CORDIC_FREQ      CHIP_CLK_AHB2_FREQ




#ifdef CHIP_CFG_CLK_SRAM1_EN
    #define CHIP_CLK_SRAM1_EN   CHIP_CFG_CLK_SRAM1_EN
#else
    #define CHIP_CLK_SRAM1_EN   0
#endif
#define CHIP_CLK_SRAM1_FREQ     CHIP_CLK_AHB2_FREQ

#ifdef CHIP_CFG_CLK_SRAM2_EN
    #define CHIP_CLK_SRAM2_EN   CHIP_CFG_CLK_SRAM2_EN
#else
    #define CHIP_CLK_SRAM2_EN   0
#endif
#define CHIP_CLK_SRAM2_FREQ     CHIP_CLK_AHB2_FREQ

#if defined CHIP_CFG_CLK_SRAM3_EN && CHIP_DEV_SUPPORT_SRAM3
    #define CHIP_CLK_SRAM3_EN   CHIP_CFG_CLK_SRAM3_EN
#else
    #define CHIP_CLK_SRAM3_EN   0
#endif
#define CHIP_CLK_SRAM3_FREQ     CHIP_CLK_AHB2_FREQ


#ifdef CHIP_CFG_CLK_DVP_EN
    #define CHIP_CLK_DVP_EN     CHIP_CFG_CLK_DVP_EN
#else
    #define CHIP_CLK_DVP_EN     0
#endif
#if defined CHIP_CFG_CLK_PSSI_EN && CHIP_DEV_SUPPORT_PSSI
    #define CHIP_CLK_PSSI_EN    CHIP_CFG_CLK_PSSI_EN
#else
    #define CHIP_CLK_PSSI_EN    0
#endif

#define CHIP_CLK_CAMITF_EN      (CHIP_CLK_DVP_EN || CHIP_CLK_PSSI_EN)
#define CHIP_CLK_DVP_FREQ       CHIP_CLK_FREQ_AHB2
#if CHIP_DEV_SUPPORT_PSSI
#define CHIP_CLK_PSSI_FREQ      CHIP_CLK_FREQ_AHB2
#endif





#ifdef CHIP_CFG_CLK_GPIOA_EN
    #define CHIP_CLK_GPIOA_EN   CHIP_CFG_CLK_GPIOA_EN
#else
    #define CHIP_CLK_GPIOA_EN   0
#endif
#define CHIP_CLK_GPIOA_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOB_EN
    #define CHIP_CLK_GPIOB_EN   CHIP_CFG_CLK_GPIOB_EN
#else
    #define CHIP_CLK_GPIOB_EN   0
#endif
#define CHIP_CLK_GPIOB_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOC_EN
    #define CHIP_CLK_GPIOC_EN   CHIP_CFG_CLK_GPIOC_EN
#else
    #define CHIP_CLK_GPIOC_EN   0
#endif
#define CHIP_CLK_GPIOC_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOD_EN
    #define CHIP_CLK_GPIOD_EN   CHIP_CFG_CLK_GPIOD_EN
#else
    #define CHIP_CLK_GPIOD_EN   0
#endif
#define CHIP_CLK_GPIOD_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOE_EN
    #define CHIP_CLK_GPIOE_EN   CHIP_CFG_CLK_GPIOE_EN
#else
    #define CHIP_CLK_GPIOE_EN   0
#endif
#define CHIP_CLK_GPIOE_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOF_EN
    #define CHIP_CLK_GPIOF_EN   CHIP_CFG_CLK_GPIOF_EN
#else
    #define CHIP_CLK_GPIOF_EN   0
#endif
#define CHIP_CLK_GPIOF_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOG_EN
    #define CHIP_CLK_GPIOG_EN   CHIP_CFG_CLK_GPIOG_EN
#else
    #define CHIP_CLK_GPIOG_EN   0
#endif
#define CHIP_CLK_GPIOG_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOH_EN
    #define CHIP_CLK_GPIOH_EN   CHIP_CFG_CLK_GPIOH_EN
#else
    #define CHIP_CLK_GPIOH_EN   0
#endif
#define CHIP_CLK_GPIOH_FREQ     CHIP_CLK_AHB4_FREQ

#if defined CHIP_CFG_CLK_GPIOI_EN && CHIP_DEV_SUPPORT_PORTI
    #define CHIP_CLK_GPIOI_EN   CHIP_CFG_CLK_GPIOI_EN
#else
    #define CHIP_CLK_GPIOI_EN   0
#endif
#define CHIP_CLK_GPIOI_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOJ_EN
    #define CHIP_CLK_GPIOJ_EN   CHIP_CFG_CLK_GPIOJ_EN
#else
    #define CHIP_CLK_GPIOJ_EN   0
#endif
#define CHIP_CLK_GPIOJ_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_GPIOK_EN
    #define CHIP_CLK_GPIOK_EN   CHIP_CFG_CLK_GPIOK_EN
#else
    #define CHIP_CLK_GPIOK_EN   0
#endif
#define CHIP_CLK_GPIOK_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_CRC_EN
    #define CHIP_CLK_CRC_EN     CHIP_CFG_CLK_CRC_EN
#else
    #define CHIP_CLK_CRC_EN     0
#endif
#define CHIP_CLK_CRC_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_BDMA_EN
    #define CHIP_CLK_BDMA_EN   CHIP_CFG_CLK_BDMA_EN
#else
    #define CHIP_CLK_BDMA_EN   0
#endif
#define CHIP_CLK_BDMA_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_HSEM_EN
    #define CHIP_CLK_HSEM_EN   CHIP_CFG_CLK_HSEM_EN
#else
    #define CHIP_CLK_HSEM_EN   0
#endif
#define CHIP_CLK_HSEM_FREQ     CHIP_CLK_AHB4_FREQ

#ifdef CHIP_CFG_CLK_BKPRAM_EN
    #define CHIP_CLK_BKPRAM_EN  CHIP_CFG_CLK_BKPRAM_EN
#else
    #define CHIP_CLK_BKPRAM_EN  0
#endif
#define CHIP_CLK_BKPRAM_FREQ      CHIP_CLK_AHB4_FREQ


#ifdef CHIP_CFG_CLK_LTDC_EN
    #define CHIP_CLK_LTDC_EN    CHIP_CFG_CLK_LTDC_EN
#else
    #define CHIP_CLK_LTDC_EN    0
#endif
#define CHIP_CLK_LTDC_FREQ      CHIP_CLK_APB3_FREQ

#ifdef CHIP_CFG_CLK_WWDG1_EN
    #define CHIP_CLK_WWDG1_EN   CHIP_CFG_CLK_WWDG1_EN
#else
    #define CHIP_CLK_WWDG1_EN   0
#endif
#define CHIP_CLK_WWDG1_FREQ     (CHIP_CLK_APB3_FREQ/4096)

#if CHIP_DEV_SUPPORT_CORE_CM4
#ifdef CHIP_CFG_CLK_WWDG2_EN
    #define CHIP_CLK_WWDG2_EN   CHIP_CFG_CLK_WWDG2_EN
#else
    #define CHIP_CLK_WWDG2_EN   0
#endif
#define CHIP_CLK_WWDG2_FREQ     (CHIP_CLK_APB1_FREQ/4096)
#endif

#ifdef CHIP_CFG_CLK_CRS_EN
    #define CHIP_CLK_CRS_EN     CHIP_CFG_CLK_CRS_EN
#else
    #define CHIP_CLK_CRS_EN     0
#endif
#define CHIP_CLK_CRS_FREQ       CHIP_CLK_APB1_FREQ

#ifdef CHIP_CFG_CLK_OPAMP_EN
    #define CHIP_CLK_OPAMP_EN   CHIP_CFG_CLK_OPAMP_EN
#else
    #define CHIP_CLK_OPAMP_EN   0
#endif
#define CHIP_CLK_OPAMP_FREQ     CHIP_CLK_APB1_FREQ

#ifdef CHIP_CFG_CLK_MDIOS_EN
    #define CHIP_CLK_MDIOS_EN   CHIP_CFG_CLK_MDIOS_EN
#else
    #define CHIP_CLK_MDIOS_EN   0
#endif
#define CHIP_CLK_MDIOS_FREQ     CHIP_CLK_APB1_FREQ


#ifdef CHIP_CFG_CLK_SYSCFG_EN
    #define CHIP_CLK_SYSCFG_EN  CHIP_CFG_CLK_SYSCFG_EN
#else
    #define CHIP_CLK_SYSCFG_EN  0
#endif
#define CHIP_CLK_SYSCFG_FREQ    CHIP_CLK_APB4_FREQ

#ifdef CHIP_CFG_CLK_COMP12_EN
    #define CHIP_CLK_COMP12_EN  CHIP_CFG_CLK_COMP12_EN
#else
    #define CHIP_CLK_COMP12_EN  0
#endif
#define CHIP_CLK_COMP12_FREQ    CHIP_CLK_APB4_FREQ

#ifdef CHIP_CFG_CLK_VREF_EN
    #define CHIP_CLK_VREF_EN    CHIP_CFG_CLK_VREF_EN
#else
    #define CHIP_CLK_VREF_EN    0
#endif
#define CHIP_CLK_VREF_FREQ      CHIP_CLK_APB4_FREQ

#ifdef CHIP_CFG_CLK_RTC_APB_EN
    #define CHIP_CLK_RTC_APB_EN CHIP_CFG_CLK_RTC_APB_EN
#else
    #define CHIP_CLK_RTC_APB_EN 0
#endif
#define CHIP_CLK_RTC_APB_FREQ   CHIP_CLK_APB4_FREQ

#if defined CHIP_CFG_CLK_DTS_EN && CHIP_DEV_SUPPORT_DTS
    #define CHIP_CLK_DTS_EN     CHIP_CFG_CLK_DTS_EN
#else
    #define CHIP_CLK_DTS_EN     0
#endif
#define CHIP_CLK_DTS_FREQ       CHIP_CLK_APB4_FREQ


#endif // CHIP_CLK_H
