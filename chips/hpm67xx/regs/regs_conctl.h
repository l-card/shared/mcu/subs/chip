#ifndef REGS_CONCTL_H
#define REGS_CONCTL_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t CTRL0;                       /* 0x0:  */
    __I  uint32_t RESERVED0;                   /* 0x4:  */
    __IO uint32_t CTRL2;                       /* 0x8:  */
    __IO uint32_t CTRL3;                       /* 0xC:  */
    __IO uint32_t CTRL4;                       /* 0x10: */
    __IO uint32_t CTRL5;                       /* 0x14: */
} CHIP_REGS_CONCTL_T;

#define CHIP_REGS_CONCTL            ((CHIP_REGS_CONCTL_T *)CHIP_MEMRGN_ADDR_CONCTL)

/*********** CTRL0 ************************************************************/
#define CHIP_REGFLD_CONCTL_CTRL0_ENET1_RXCLK_DLY_SEL            (0x0000001FUL << 15U) /* (RW) ENET1 receive clock delay line number selection */
#define CHIP_REGFLD_CONCTL_CTRL0_ENET1_TXCLK_DLY_SEL            (0x0000001FUL << 10U) /* (RW) ENET1 transmit clock delay line number selection*/
#define CHIP_REGFLD_CONCTL_CTRL0_ENET0_RXCLK_DLY_SEL            (0x0000001FUL <<  5U) /* (RW) ENET0 receive clock delay line number selection */
#define CHIP_REGFLD_CONCTL_CTRL0_ENET0_TXCLK_DLY_SEL            (0x0000001FUL <<  0U) /* (RW) ENET0 transmit clock delay line number selection*/
#define CHIP_REGMSK_CONCTL_CTRL0_RESERVED                       (0xFFF00000UL)

/*********** CTRL2 ************************************************************/
#define CHIP_REGFLD_CONCTL_CTRL2_ENET0_LPI_IRQ_EN               (0x00000001UL << 29U) /* (RW) ENET0 LPI interrupt enable */
#define CHIP_REGFLD_CONCTL_CTRL2_ENET0_REFCLK_OE                (0x00000001UL << 19U) /* (RW) ENET0 RMII clock output enable */
#define CHIP_REGFLD_CONCTL_CTRL2_ENET0_PHY_INTF_SEL             (0x00000007UL << 13U) /* (RW) ENET0 PHY mode selection */
#define CHIP_REGFLD_CONCTL_CTRL2_ENET0_FLOWCTRL                 (0x00000001UL << 12U) /* (RW) ENET0 sideband flow control */
#define CHIP_REGFLD_CONCTL_CTRL2_ENET0_RMII_TXCLK_SEL           (0x00000001UL << 10U) /* (RW) ENET0 RMII clock selection */
#define CHIP_REGMSK_CONCTL_CTRL2_RESERVED                       (0xDFF70BFFUL)

#define CHIP_REGFLDVAL_CONCTL_CTRL2_ENET0_PHY_INTF_SEL_RGMII    1
#define CHIP_REGFLDVAL_CONCTL_CTRL2_ENET0_PHY_INTF_SEL_RMII     4

#define CHIP_REGFLDVAL_CONCTL_CTRL2_ENET0_RMII_TXCLK_SEL_PAD    1 /*  From the chip pad (external or internal loopback) */
#define CHIP_REGFLDVAL_CONCTL_CTRL2_ENET0_RMII_TXCLK_SEL_INT    0 /*  Direct internal clock, without going through the pin */


/*********** CTRL3 ************************************************************/
#define CHIP_REGFLD_CONCTL_CTRL3_ENET1_LPI_IRQ_EN               (0x00000001UL << 29U) /* (RW) ENET1 LPI interrupt enable */
#define CHIP_REGFLD_CONCTL_CTRL3_ENET1_REFCLK_OE                (0x00000001UL << 19U) /* (RW) ENET1 RMII clock output enable */
#define CHIP_REGFLD_CONCTL_CTRL3_ENET1_PHY_INTF_SEL             (0x00000007UL << 13U) /* (RW) ENET1 PHY mode selection */
#define CHIP_REGFLD_CONCTL_CTRL3_ENET1_FLOWCTRL                 (0x00000001UL << 12U) /* (RW) ENET1 sideband flow control */
#define CHIP_REGFLD_CONCTL_CTRL3_ENET1_RMII_TXCLK_SEL           (0x00000001UL << 10U) /* (RW) ENET1 RMII clock selection */
#define CHIP_REGMSK_CONCTL_CTRL3_RESERVED                       (0xDFF70BFFUL)

#define CHIP_REGFLDVAL_CONCTL_CTRL3_ENET1_PHY_INTF_SEL_RGMII    1
#define CHIP_REGFLDVAL_CONCTL_CTRL3_ENET1_PHY_INTF_SEL_RMII     4

#define CHIP_REGFLDVAL_CONCTL_CTRL3_ENET1_RMII_TXCLK_SEL_PAD    1 /*  From the chip pad (external or internal loopback) */
#define CHIP_REGFLDVAL_CONCTL_CTRL3_ENET1_RMII_TXCLK_SEL_INT    0 /*  Direct internal clock, without going through the pin */

/*********** CTRL4 ************************************************************/
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_SYS_IRQ_EN               (0x00000001UL << 31U) /* (RW) SDXC0 system interrupt enable */
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_WKP_IRQ_EN               (0x00000001UL << 30U) /* (RW) SDXC0 wake-up interrupt enable */
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_CARDCLK_INV_EN           (0x00000001UL << 28U) /* (RW) SDXC0 card clock reverse enable */
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_GPR_TUNING_CARD_CLK_SEL  (0x0000001FUL << 23U) /* (RW) SDXC0 card clock delay line selection */
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_GPR_TUNING_STROBE_SEL    (0x0000001FUL << 18U) /* (RW) SDXC0 strobe delay line series selection */
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_GPR_STROBE_IN_ENABLE     (0x00000001UL << 17U) /* (RW) SDXC0 strobe input enable */
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_GPR_CCLK_RX_DLY_SW_SEL   (0x0000001FUL << 12U) /* (RW) */
#define CHIP_REGFLD_CONCTL_CTRL4_SDXC0_GPR_CCLK_RX_DLY_SW_FORCE (0x00000001UL << 11U) /* (RW) */
#define CHIP_REGMSK_CONCTL_CTRL4_RESERVED                       (0x200007FFUL)

/*********** CTRL5 ************************************************************/
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_SYS_IRQ_EN               (0x00000001UL << 31U) /* (RW) SDXC1 system interrupt enable */
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_WKP_IRQ_EN               (0x00000001UL << 30U) /* (RW) SDXC1 wake-up interrupt enable */
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_CARDCLK_INV_EN           (0x00000001UL << 28U) /* (RW) SDXC1 card clock reverse enable */
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_GPR_TUNING_CARD_CLK_SEL  (0x0000001FUL << 23U) /* (RW) SDXC1 card clock delay line selection */
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_GPR_TUNING_STROBE_SEL    (0x0000001FUL << 18U) /* (RW) SDXC1 strobe delay line series selection */
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_GPR_STROBE_IN_ENABLE     (0x00000001UL << 17U) /* (RW) SDXC1 strobe input enable */
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_GPR_CCLK_RX_DLY_SW_SEL   (0x0000001FUL << 12U) /* (RW) */
#define CHIP_REGFLD_CONCTL_CTRL5_SDXC1_GPR_CCLK_RX_DLY_SW_FORCE (0x00000001UL << 11U) /* (RW) */
#define CHIP_REGMSK_CONCTL_CTRL5_RESERVED                       (0x200007FFUL)




#endif // REGS_CONCTL_H
