#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H

#define CHIP_CFG_CRP_MODE               CHIP_CRP_DISABLED  /* Code Read Protection (CRP_DISABLED, CRP_NO_ISP, CRP1, CRP2, CRP3) */

/* ------------------ Настройки источников частот ----------------------------*/
#define CHIP_CFG_CLK_SYSOSC_MODE        CHIP_CLK_EXTMODE_OSC   /* режим внешней частоты (DIS, OSC, CLOCK)  */
#define CHIP_CFG_CLK_SYSOSC_FREQ        CHIP_MHZ(16)           /* значение частоты внешнего осциллятора */

#define CHIP_CFG_CLK_IRC_EN             0           /* разрешение внутреннего источника частоты (если не разрешен, отключается после инициализации) */
#define CHIP_CFG_CLK_RTCOSC_EN          0           /* разрешение 32КГц клока для RTC */


/* ------------------ Настройки SYSPLL ---------------------------------------*/
#define CHIP_CFG_CLK_SYSPLLIN_EN        1                     /* возможность отдельно разрешить настройку входной частоты SYSPLL,
                                                                 для использования без PLL как источник MAINCLK */
#define CHIP_CFG_CLK_SYSPLL_EN          1                     /* разрешение SYSPLL */
#define CHIP_CFG_CLK_SYSPLL_SRC         CHIP_CLK_SYSOSC       /* источник входной частоты для PLL: IRC, SYSOSC, RTCOSC (только для MAINCLK) */
#define CHIP_CFG_CLK_SYSPLL_MUL         3                     /* множитель для результирующей частоты (1..32) */


/* ------------------ Настройки основной частоты -----------------------------*/
#define CHIP_CFG_CLK_MAIN_SRC           CHIP_CLK_SYSPLL       /* источник основной частоты: IRC, SYSPLLIN, WDTOSC, SYSPLL */
#define CHIP_CFG_CLK_SYS_DIV            1                     /* делитель частоты ядра из MAIN (1..255) */


/* ------------------ Настройки частоты USB ----------------------------------*/
#define CHIP_CFG_CLK_USB_SETUP_EN       1                     /* разрешение клока USB. Если включено, то USBPLL должно
                                                                 содержать действительные параметры, даже если работает от MAINCLK,
                                                                 так как по умолчанию USB работает от USBPLL и для переключения нужного
                                                                 его включать */
#define CHIP_CFG_CLK_USBPLL_EN          1                     /* разрешение USBPLL */
#define CHIP_CFG_CLK_USBPLL_SRC         CHIP_CLK_SYSOSC       /* источник входной частоты для PLL: IRC, SYSOSC */
#define CHIP_CFG_CLK_USBPLL_MUL         3                     /* множитель для результирующей частоты (1..32) */
#define CHIP_CFG_CLK_USB_SRC            CHIP_CLK_USBPLL       /* источник частоты для USB (USBPLL или MAIN) */
#define CHIP_CFG_CLK_USB_DIV            1                     /* делитель частоты USB */


/* ------------------- Выход частоты CLKOUT ----------------------------------*/
#define CHIP_CFG_CLK_CLKOUT_EN          0                     /* Разрешение генерации выходной частоты CLKOUT */
#define CHIP_CFG_CLK_CLKOUT_SRC         CHIP_CLK_IRC          /* Источник частоты для CLKOUT (IRC, SYSOSC, WDTOSC, MAIN) */
#define CHIP_CFG_CLK_CLKOUT_DIV         1                     /* Делитель частоты от 1 до 255 */



/* ------------------ Настройки делителей периферии --------------------------*/
/* включаются при старте только если разрешен соответствующий клок, иначе
 * включаются по chip_per_clk_en() */
#define CHIP_CFG_CLK_SSP0_DIV           0
#define CHIP_CFG_CLK_SSP1_DIV           0
#define CHIP_CFG_CLK_USART0_DIV         1
#define CHIP_CFG_CLK_FRG_DIV            1       /* входной делитель частоты для UART1-4 */
#define CHIP_CFG_CLK_FRG_FRACT_MUL      0       /* множитель дробной части 0-255, частота UART1-4 = (MAIN_FREQ/FRG_DIV)/(1 + FRG_FRACT_MUL/256) */


/* ------------------------------ WDT ----------------------------------------*/
#define CHIP_CFG_CLK_WDTOSC_SETUP_EN    0
#define CHIP_CFG_CLK_WDTOSC_EN          1
#define CHIP_CFG_CLK_WDTOSC_SRC_FREQ    CHIP_KHZ(600)           /* приблизительная частота WDTANA (от 0.6 МГц до 4.6 МГц) */
#define CHIP_CFG_CLK_WDTOSC_DIV         2                       /* делитель частоты WDTANA (четное от 2 до 64) */

#define CHIP_CFG_WDT_SETUP_EN           0                       /* разрешение настройки watchdog-timer */
#define CHIP_CFG_WDT_CLK_SRC            CHIP_CLK_WDTOSC         /* выбор источника частоты WDT (IRC, WDTOSC) */
#define CHIP_CFG_WDT_CLK_LOCK           1                       /* запрет изменения или отключения клока WDT после настройки */
#define CHIP_CFG_WDT_TC_PROTECT         1                       /* вкл. защиты от изменения времени сброса, если вкл. перезпустить
                                                                   wdt можно только, когда осталось не более CHIP_CFG_WDT_WARNTIME_VAL времени до сброса... */
#define CHIP_CFG_WDT_TIMEOUT_VAL        CHIP_WDT_TICKS_MS(1000) /* определяет время по которому будут сброс чипа, если не было перезапуска wdt */
#define CHIP_CFG_WDT_WARNTIME_VAL       0                       /* за сколько времени до сброса чипа генерировать прерывание (1-1024) */
#define CHIP_CFG_WDT_WINDOW_VAL         CHIP_WDT_WINDOW_MAX_VAL /* за какое макс. время до истечения таймера можно перезапускать wdt */




/* разрешение клоков периферии при старте */
#define CHIP_CFG_CLK_ROM_EN             1
#define CHIP_CFG_CLK_SRAM0_EN           1
#define CHIP_CFG_CLK_SRAM1_EN           1
#define CHIP_CFG_CLK_USBSRAM_EN         1
#define CHIP_CFG_CLK_FLASHREG_EN        1
#define CHIP_CFG_CLK_FLASH_EN           1

#define CHIP_CFG_CLK_GPIO_EN            1
#define CHIP_CFG_CLK_PINT_EN            0
#define CHIP_CFG_CLK_GROUP0INT_EN       0
#define CHIP_CFG_CLK_GROUP1INT_EN       0

#define CHIP_CFG_CLK_CT16B0_EN          0
#define CHIP_CFG_CLK_CT16B1_EN          0
#define CHIP_CFG_CLK_CT32B0_EN          0
#define CHIP_CFG_CLK_CT32B1_EN          0
#define CHIP_CFG_CLK_SCT0_EN            0
#define CHIP_CFG_CLK_SCT1_EN            0

#define CHIP_CFG_CLK_FRG_EN             0
#define CHIP_CFG_CLK_USART0_EN          0
#define CHIP_CFG_CLK_USART1_EN          0
#define CHIP_CFG_CLK_USART2_EN          0
#define CHIP_CFG_CLK_USART3_EN          0
#define CHIP_CFG_CLK_USART4_EN          0

#define CHIP_CFG_CLK_SSP0_EN            0
#define CHIP_CFG_CLK_SSP1_EN            0

#define CHIP_CFG_CLK_I2C0_EN            0
#define CHIP_CFG_CLK_I2C1_EN            0


#define CHIP_CFG_CLK_DMA_EN             0
#define CHIP_CFG_CLK_ADC_EN             0
#define CHIP_CFG_CLK_USB_EN             0
#define CHIP_CFG_CLK_WWDT_EN            1
#define CHIP_CFG_CLK_IOCON_EN           0
#define CHIP_CFG_CLK_CRC_EN             0
#define CHIP_CFG_CLK_RTC_EN             0


#endif // CHIP_CONFIG_H

