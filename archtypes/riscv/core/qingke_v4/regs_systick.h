#ifndef REGS_SYSTICK_H
#define REGS_SYSTICK_H

#include "chip_ioreg_defs.h"
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CTLR;     /* System Count Control Register,               Address offset: 0x00 */
    __IO uint32_t SR;       /* System Count Status Register,                Address offset: 0x04 */
    __IO uint32_t CNTL;     /* System Counter Low Register,                 Address offset: 0x08 */
    __IO uint32_t CNTH;     /* System Counter High Register,                Address offset: 0x0C */
    __IO uint32_t CMPLR;    /* System Count Comparison Value Low Register,  Address offset: 0x10 */
    __IO uint32_t CMPHR;    /* System Count Comparison Value High Register, Address offset: 0x14 */
} CHIP_REGS_STK_T;

#define CHIP_REGS_STK                   ((CHIP_REGS_STK_T *) (CHIP_MEMRGN_ADDR_CORE_INTERNAL + 0xF000))

/*********** STK_CTLR - System Count Control Register *************************/
#define CHIP_REGFLD_STK_CTLR_SWIE                   (0x00000001UL << 31U)   /* (RW) Software interrupt trigger enable */
#define CHIP_REGFLD_STK_CTLR_INIT                   (0x00000001UL <<  5U)   /* (W1) Counter initial value update */
#define CHIP_REGFLD_STK_CTLR_MODE                   (0x00000001UL <<  4U)   /* (RW) Counting mode (1 - down, 0 - up) */
#define CHIP_REGFLD_STK_CTLR_STRE                   (0x00000001UL <<  3U)   /* (RW) Auto reload count enable */
#define CHIP_REGFLD_STK_CTLR_STCLK                  (0x00000001UL <<  2U)   /* (RW) Counter clock source selection */
#define CHIP_REGFLD_STK_CTLR_STIE                   (0x00000001UL <<  1U)   /* (RW) Counter interrupt enable control */
#define CHIP_REGFLD_STK_CTLR_STE                    (0x00000001UL <<  0U)   /* (RW) System counter enable control */
#define CHIP_REGMSK_STK_CTRL_RESERVED               (0x7FFFFFC0UL)


#define CHIP_REGFLDVAL_STK_CTLR_MODE_UP             0
#define CHIP_REGFLDVAL_STK_CTLR_MODE_DOWN           1

#define CHIP_REGFLD_STK_CTLR_STCLK_HCLK_8           0 /* HCLK/8 */
#define CHIP_REGFLD_STK_CTLR_STCLK_HCLK             1

/*********** STK_SR - System Count Status Register ****************************/
#define CHIP_REGFLD_STK_SR_CNTIF                    (0x00000001UL <<  0U)   /* (RW0) Counting value comparison flag, write 0 to clear */

#endif // REGS_SYSTICK_H
