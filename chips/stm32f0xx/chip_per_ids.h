#ifndef CHIP_PER_IDS_H
#define CHIP_PER_IDS_H

#define CHIP_PER_ID_DMA1        0 /* clk only */
#define CHIP_PER_ID_DMA2        1 /* clk only */
#define CHIP_PER_ID_SRAM        2 /* clk only */
#define CHIP_PER_ID_FLASH       3 /* clk only */
#define CHIP_PER_ID_CRC         4 /* clk only */

#define CHIP_PER_ID_GPIOA       5
#define CHIP_PER_ID_GPIOB       6
#define CHIP_PER_ID_GPIOC       7
#define CHIP_PER_ID_GPIOD       8
#define CHIP_PER_ID_GPIOE       9
#define CHIP_PER_ID_GPIOF       10

#define CHIP_PER_ID_CRS         11
#define CHIP_PER_ID_PWR         12
#define CHIP_PER_ID_DBG         13
#define CHIP_PER_ID_WWDG        14

#define CHIP_PER_ID_TIM1        15
#define CHIP_PER_ID_TIM2        16
#define CHIP_PER_ID_TIM3        17
#define CHIP_PER_ID_TIM6        18
#define CHIP_PER_ID_TIM7        19
#define CHIP_PER_ID_TIM14       20
#define CHIP_PER_ID_TIM15       21
#define CHIP_PER_ID_TIM16       22
#define CHIP_PER_ID_TIM17       23

#define CHIP_PER_ID_USART1      24
#define CHIP_PER_ID_USART2      25
#define CHIP_PER_ID_USART3      26
#define CHIP_PER_ID_USART4      27
#define CHIP_PER_ID_USART5      28
#define CHIP_PER_ID_USART6      29
#define CHIP_PER_ID_USART7      30
#define CHIP_PER_ID_USART8      31

#define CHIP_PER_ID_SPI1        32
#define CHIP_PER_ID_SPI2        33

#define CHIP_PER_ID_I2C1        34
#define CHIP_PER_ID_I2C2        35

#define CHIP_PER_ID_USB         36
#define CHIP_PER_ID_CAN         37

#define CHIP_PER_ID_ADC         38
#define CHIP_PER_ID_DAC         39

#define CHIP_PER_ID_CEC         39
#define CHIP_PER_ID_TSC         40


#endif // CHIP_PER_IDS_H
