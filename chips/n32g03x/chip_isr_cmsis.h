#ifndef CHIP_ISR_CMSIS_N32G03X_H
#define CHIP_ISR_CMSIS_N32G03X_H

/**
 * @brief N32G031 Interrupt Number Definition
 */
typedef enum IRQn {
    /******  Cortex-M0 Processor Exceptions Numbers ***************************************************/
    NonMaskableInt_IRQn      = -14, /*!< 2 Non Maskable Interrupt                            */
    HardFault_IRQn           = -13, /*!< 3 Hard Fault Interrupt                              */
    SVCall_IRQn              = -5,  /*!< 11 Cortex-M0 SV Call Interrupt                      */
    PendSV_IRQn              = -2,  /*!< 14 Cortex-M0 Pend SV Interrupt                      */
    SysTick_IRQn             = -1,  /*!< 15 Cortex-M0 System Tick Interrupt                  */

    /******  N32G031 specific Interrupt Numbers ********************************************************/
    WWDG_IRQn                = 0,  /*!< Window WatchDog Interrupt                            */
    PVD_IRQn                 = 1,  /*!< PVD through EXTI Line 16 detection Interrupt         */
    RTC_IRQn                 = 2,  /*!< RTC interrupt through EXTI line 17/19/20             */
    MMU_IRQn                 = 3,  /*!< MMU global Interrupt                                 */
    FLASH_IRQn               = 4,  /*!< FLASH global Interrupt                               */
    RCC_IRQn                 = 5,  /*!< RCC global Interrupt                                 */
    EXTI0_1_IRQn             = 6,  /*!< EXTI Line0/1 Interrupt                               */
    EXTI2_3_IRQn             = 7,  /*!< EXTI Line2/3 Interrupt                               */
    EXTI4_15_IRQn            = 8,  /*!< EXTI Line4 ~ 15 Interrupt                            */
    DMA_Channel1_2_IRQn      = 10, /*!< DMA Channel 1/2 global Interrupt                     */
    DMA_Channel1_2_3_4_IRQn  = 11, /*!< DMA Channel 1/2/3/4 global Interrupt                 */
    DMA_Channel5_IRQn        = 12, /*!< DMA Channel 5 global Interrupt                       */
    TIM1_BRK_UP_TRG_COM_IRQn = 13, /*!< TIM1 Break Update Trigger and Commutation Interrupt  */
    TIM1_CC_IRQn             = 14, /*!< TIM1 Capture Compare Interrupt                       */
    DMA_Channel3_4_IRQn      = 15, /*!< DMA Channel 3/4 global Interrupt                     */
    TIM3_IRQn                = 16, /*!< TIM3 global Interrupt                                */
    USART2_IRQn              = 17, /*!< USART2 global Interrupt                              */
    TIM8_BRK_UP_TRG_COM_IRQn = 18, /*!< TIM8 Break Update Trigger and Commutation Interrupt  */
    TIM8_CC_IRQn             = 19, /*!< TIM8 Capture Compare Interrupt                       */
    LPTIM_TIM6_IRQn          = 20, /*!< LPTIM / TIM6 global Interrupts                       */
    ADC_IRQn                 = 21, /*!< ADC global Interrupts                                */
    SPI2_IRQn                = 22, /*!< SPI2 global Interrupts                               */
    I2C1_IRQn                = 23, /*!< I2C1 global Interrupts                               */
    I2C2_IRQn                = 24, /*!< I2C2 global Interrupts                               */
    SPI1_IRQn                = 25, /*!< SPI1 global Interrupts                               */
    HDIV_SQRT_IRQn           = 26, /*!< HDIV / SQRT global Interrupts                        */
    RAMC_PERR_IRQn           = 27, /*!< RAMC PARITY ERR Interrupt                            */
    USART1_2_IRQn            = 28, /*!< USART1 / USART2 global Interrupt                     */
    LPUART_IRQn              = 29, /*!< LPUART global Interrupt                              */
    USART1_IRQn              = 30, /*!< USART1 global Interrupt                              */
    COMP_IRQn                = 31, /*!< COMP1(through EXTI line 18) Interrupt                */
} IRQn_Type;

#endif // CHIP_ISR_CMSIS_H
