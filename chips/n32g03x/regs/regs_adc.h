#ifndef CHIP_N32G03X_REGS_ADC_H
#define CHIP_N32G03X_REGS_ADC_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t SR;               /* ADC Status Register,                                        Address offset: 0x00 */
    __IO uint32_t CR1;              /* ADC control register 1,                                     Address offset: 0x04 */
    __IO uint32_t CR2;              /* ADC control register 2,                                     Address offset: 0x08 */
    union {
        struct {
            __IO uint32_t SMPR1;            /* ADC sample time register 1,                         Address offset: 0x0C */
            __IO uint32_t SMPR2;            /* ADC sample time register 2,                         Address offset: 0x10 */
            __IO uint32_t SMPR3;            /* (N!) ADC sample time register 3,                         Address offset: 0x14 */
        };
        __IO uint32_t SMPR[3];
    };
#if CHIP_SUPPORT_ADC_INJECTED
    union {
        struct {
            __IO uint32_t JOFR1;             /* ADC injected channel data offset register 1,       Address offset: 0x18 */
            __IO uint32_t JOFR2;             /* ADC injected channel data offset register 2,       Address offset: 0x1C */
            __IO uint32_t JOFR3;             /* ADC injected channel data offset register 3,       Address offset: 0x20 */
            __IO uint32_t JOFR4;             /* ADC injected channel data offset register 4,       Address offset: 0x24 */
        };
        __IO uint32_t JOFR[4];
    };
#else
    uint32_t RESERVED1[4];
#endif
    __IO uint32_t HTR;              /* ADC watchdog higher threshold register,                     Address offset: 0x28 */
    __IO uint32_t LTR;              /* ADC watchdog Lower threshold register,                      Address offset: 0x2C */

    union {
        struct {
            __IO uint32_t SQR1;             /* ADC regular sequence register 1,                    Address offset: 0x30 */
            __IO uint32_t SQR2;             /* ADC regular sequence register 2,                    Address offset: 0x34 */
            __IO uint32_t SQR3;             /* ADC regular sequence register 3,                    Address offset: 0x38 */
        };
        __IO uint32_t SQR[3];
    };
#if CHIP_SUPPORT_ADC_INJECTED
    __IO uint32_t JSQR;             /* ADC injected sequence register,                             Address offset: 0x3C */
    union {
        struct {
            __IO uint32_t JDR1;             /* ADC injected data register 1,                       Address offset: 0x40 */
            __IO uint32_t JDR2;             /* ADC injected data register 2,                       Address offset: 0x44 */
            __IO uint32_t JDR3;             /* ADC injected data register 3,                       Address offset: 0x48 */
            __IO uint32_t JDR4;             /* ADC injected data register 4,                       Address offset: 0x4C */
        };
        __IO uint32_t JDR[4];
    };
#else
    uint32_t RESERVED2[5];
#endif
    __IO uint32_t DR;               /* ADC regular data register,                                  Address offset: 0x50 */
    __IO uint32_t CR3;              /* (N!) ADC control register 3,                                Address offset: 0x54 */
    __IO uint32_t TEST;             /* (N!) ADC test register,                                     Address offset: 0x58 */
    __IO uint32_t OPACTRL;          /* (N!) ADC OPA control register,                              Address offset: 0x5C */
} CHIP_REGS_ADC_T;


#define CHIP_REGS_ADC1                          ((CHIP_REGS_ADC_T *) CHIP_MEMRGN_ADDR_PERIPH_ADC1)

/* специальные входы для ADC */
#define CHIP_ADC_IN_NUM_TS                     12 /* Temperature Sensor */
#define CHIP_ADC_IN_NUM_VREFINT                13 /* The internal reference voltage VREFINT */
#define CHIP_ADC_IN_NUM_VREFP                  14 /* The internal reference voltage VREFP */
#define CHIP_ADC_IN_NUM_VDDA                   15 /* VDDA pin voltage */


#define CHIP_ADC_POWERUP_CLK_CNT                32
#define CHIP_ADC_POWERUP_TIME_US                (CHIP_ADC_POWERUP_CLK_CNT * 1000000 / CHIP_CLK_FREQ_ADC)

/* параметры внутреннего датчика температуры */
#define CHIP_ADC_TS_POWERUP_TIME_US             22  /* Время включения датчика температуры при включении */
#define CHIP_ADC_TS_V25                         1.3 /* Напряжение в канале TS, соответствующее температуре 25 град. C */
#define CHIP_ADC_TS_AVG_SLOPE                   3.9 /* Чувствительность датчика температуры mV/град. C */
#define CHIP_ADC_TS_CONV_V_TO_TEMP(v)           (((v) - CHIP_ADC_TS_V25) * 1000. / CHIP_ADC_TS_AVG_SLOPE + 25) /* преобразование напряжения в температуру */

/*********** ADC status register (ADC_SR) *************************************/
#define CHIP_REGFLD_ADC_SR_AWD                  (0x00000001UL <<  0U) /* (RCW0) Analog watchdog flag  */
#define CHIP_REGFLD_ADC_SR_EOC                  (0x00000001UL <<  1U) /* (RCW0) End of conversion sequence (regular or injected) */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_SR_JEOC                 (0x00000001UL <<  2U) /* (RCW0) Injected channel sequence end of conversion  */
#define CHIP_REGFLD_ADC_SR_JSTRT                (0x00000001UL <<  3U) /* (RCW0) Injected channel start flag  */
#endif
#define CHIP_REGFLD_ADC_SR_STRT                 (0x00000001UL <<  4U) /* (RCW0) Regular channel start flag  */
#define CHIP_REGFLD_ADC_SR_ENDCA                (0x00000001UL <<  5U) /* (RCW0) (N!) End of any (regular or injected) conversion  */
#define CHIP_REGFLD_ADC_SR_JENDCA               (0x00000001UL <<  5U) /* (RCW0) (N!) End of any injected conversion  */



/*********** ADC control register 1 (ADC_CR1)**********************************/
#define CHIP_REGFLD_ADC_CR1_AWD_CH              (0x0000001FUL <<  0U) /* (RW) Analog watchdog channel select bits */
#define CHIP_REGFLD_ADC_CR1_EOC_IE              (0x00000001UL <<  5U) /* (RW) Interrupt enable for EOC */
#define CHIP_REGFLD_ADC_CR1_AWD_IE              (0x00000001UL <<  6U) /* (RW) Analog watchdog interrupt enable */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JEOC_IE             (0x00000001UL <<  7U) /* (RW) Interrupt enable for injected channels */
#endif
#define CHIP_REGFLD_ADC_CR1_SCAN                (0x00000001UL <<  8U) /* (RW) Scan mode */
#define CHIP_REGFLD_ADC_CR1_AWD_SGL             (0x00000001UL <<  9U) /* (RW) Enable the watchdog on a single channel in scan mode */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JAUTO               (0x00000001UL << 10U) /* (RW) Automatic Injected Group conversion */
#endif
#define CHIP_REGFLD_ADC_CR1_DISC_EN             (0x00000001UL << 11U) /* (RW) Discontinuous mode on regular channels */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JDISC_EN            (0x00000001UL << 12U) /* (RW) Discontinuous mode on injected channels */
#endif
#define CHIP_REGFLD_ADC_CR1_DISC_NUM            (0x00000007UL << 13U) /* (RW) Discontinuous mode channel count */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR1_JAWD_EN             (0x00000001UL << 22U) /* (RW) Analog watchdog enable on injected channels */
#endif
#define CHIP_REGFLD_ADC_CR1_AWD_EN              (0x00000001UL << 23U) /* (RW) Analog watchdog enable on regular channels */




/*********** ADC control register 2 (ADC_CR2)**********************************/
#define CHIP_REGFLD_ADC_CR2_AD_ON               (0x00000001UL <<  0U) /* (RW) A/D converter ON / OFF */
#define CHIP_REGFLD_ADC_CR2_CONT                (0x00000001UL <<  1U) /* (RW) Continuous conversion */
#define CHIP_REGFLD_ADC_CR2_DMA                 (0x00000001UL <<  8U) /* (RW) Direct memory access mode enable */
#define CHIP_REGFLD_ADC_CR2_ALIGN               (0x00000001UL << 11U) /* (RW) Data alignment  */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR2_JEXT_SEL            (0x00000007UL << 12U) /* (RW) External event select for injected group */
#define CHIP_REGFLD_ADC_CR2_JEXT_TRIG           (0x00000001UL << 15U) /* (RW) Injected conversion on external event enable */
#endif
#define CHIP_REGFLD_ADC_CR2_EXT_SEL             (0x00000007UL << 17U) /* (RW) External event select for regular group */
#define CHIP_REGFLD_ADC_CR2_EXT_TRIG            (0x00000001UL << 20U) /* (RW) Regular conversion on external event enable */
#if CHIP_SUPPORT_ADC_INJECTED
#define CHIP_REGFLD_ADC_CR2_JSW_START           (0x00000001UL << 21U) /* (RW) Start conversion of injected channels */
#endif
#define CHIP_REGFLD_ADC_CR2_SW_START            (0x00000001UL << 22U) /* (RW) Start conversion of regular channels */
#define CHIP_REGFLD_ADC_CR2_TS_E                (0x00000001UL << 23U) /* (RW) Temperature sensor enable (VREF отдельно!) */



#define CHIP_REGFLDVAL_ADC_CR2_ALIGN_RIGHT              0
#define CHIP_REGFLDVAL_ADC_CR2_ALIGN_LEFT               1

#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM1_CC1         0
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM1_CC2         1
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM1_CC3         2
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_TIM3_TRGO        4
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_EXTI_TIM8_TRGO   6
#define CHIP_REGFLDVAL_ADC_CR2_EXT_SEL_SW_START         7

#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM1_TRGO       0
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM1_CC4        1
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_TIM3_CC4        4
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_EXTI_TIM8_CC4   6
#define CHIP_REGFLDVAL_ADC_CR2_JEXT_SEL_JSW_START       7

/*********** ADC control register 3 (ADC_CR3)**********************************/
#define CHIP_REGFLD_ADC_CR3_REF_SEL             (0x00000001UL <<  0U) /* (RW) ADC reference source selset */
#define CHIP_REGFLD_ADC_CR3_VREF_EN             (0x00000001UL <<  1U) /* (RW) ADC internal ref enable */
#define CHIP_REGFLD_ADC_CR3_VREF_RDY            (0x00000001UL <<  2U) /* (RO) ADC internal ref ready status */
#define CHIP_REGFLD_ADC_CR3_CKMOD               (0x00000001UL <<  4U) /* (RW) Clock mode  */
#define CHIP_REGFLD_ADC_CR3_RDY                 (0x00000001UL <<  5U) /* (RO) ADC ready */
#define CHIP_REGFLD_ADC_CR3_PD_RDY              (0x00000001UL <<  6U) /* (RO) ADC Power Ready */
#define CHIP_REGFLD_ADC_CR3_ENDCAI_EN           (0x00000001UL <<  8U) /* (RW) Conversion end interrupt enable for any channel */
#define CHIP_REGFLD_ADC_CR3_JENDCAI_EN          (0x00000001UL <<  9U) /* (RW) Conversion end interrupt enable for any injected channel */

#define CHIP_REGFLDVAL_ADC_CR3_CKMOD_AHB        0
#define CHIP_REGFLDVAL_ADC_CR3_CKMOD_PLL        1

#define CHIP_REGFLDVAL_ADC_CR3_REF_SEL_VDD      0 /*  reference source is the external reference VDD */
#define CHIP_REGFLDVAL_ADC_CR3_REF_SEL_INTREF   1 /* reference source is the internal voltage 2.4V */


/*********** ADC sample time register 1/2 (ADC_SMPR1/2) ***********************/
#define CHIP_REGFLD_ADC_SMPR_SMP(x)             (0x0000000FUL << (4*((x) % 8)))  /* (RW) (N!) ADC Channel x Sampling time selection  */
#define CHIP_REGNUM_ADC_SMPR_SMP(x)             (2 - (x) / 8)
#define CHIP_REGMSK_ADC_SMPR_RESERVED           (0xC0000000UL)
#define CHIP_REGFLDSET_ADC_SMPR_SMP(ch, x)      (LBITFIELD_UPD(CHIP_REGS_ADC1->SMPR[CHIP_REGNUM_ADC_SMPR_SMP(ch)], \
                                                 CHIP_REGFLD_ADC_SMPR_SMP(ch), x))




#define CHIP_REGFLDVAL_ADC_SMPR_SMP_6         0 /* 6 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_8         1 /* 8 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_14        2 /* 14 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_20        3 /* 20 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_29        4 /* 29 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_42        5 /* 42 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_56        6 /* 56 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_72        7 /* 72 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_88        8 /* 88 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_120       9 /* 120 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_182      10 /* 182 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_240      11 /* 240 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_300      12 /* 300 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_400      13 /* 400 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_480      14 /* 480 ADC clock cycles */
#define CHIP_REGFLDVAL_ADC_SMPR_SMP_600      15 /* 600 ADC clock cycles */



/*********** ADC injected channel data offset register x (ADC_JOFRx) (x=1..4) */
#define CHIP_REGFLD_ADC_JOFR_JOFFSET            (0x00000FFFUL <<  0U) /* (RW) Data offset for injected channel x */

/*********** ADC watchdog threshold register (ADC_LTR) ************************/
#define CHIP_REGFLD_ADC_LTR_LT                  (0x00000FFFUL <<  0U) /* (RW) Analog watchdog low threshold */

/*********** ADC watchdog threshold register (ADC_HTR) ************************/
#define CHIP_REGFLD_ADC_HTR_HT                  (0x00000FFFUL <<  0U) /* (RW) ADC Analog watchdog 1,2 and 3 higher threshold */

/*********** ADC regular sequence register n (ADC_SQRn)************************/
#define CHIP_REGFLD_ADC_SQR1_L                  (0x0000000FUL << 20U) /* (RW) ADC regular channel sequence length */
#define CHIP_REGFLD_ADC_SQR_SQ(x)               (0x0000001FUL <<  5U * ((x) % 6)) /* (RW) ADC x-st conversion in regular sequence */
#define CHIP_REGNUM_ADC_SQR_SQ(x)               (2 - ((x) / 6))
#define CHIP_REGFLDSET_ADC_SQR_SQ(idx, ch)      (LBITFIELD_UPD(CHIP_REGS_ADC1->SQR[CHIP_REGNUM_ADC_SQR_SQ(idx)], \
                                                 CHIP_REGFLD_ADC_SQR_SQ(idx), ch))

/*********** ADC injected sequence register (ADC_JSQR) ************************/
#define CHIP_REGFLD_ADC_JSQR_JL                 (0x00000003UL << 20U) /* (RW) ADC injected channel sequence length */
#define CHIP_REGFLD_ADC_JSQR_JSQ(x)             (0x0000001FUL <<  (5 * (x)))  /* (RW) ADC x-st conversion in injected sequence */

/*********** ADC injected channel y data register (ADC_JDRy) ******************/
#define CHIP_REGFLD_ADC_JDR_JDATA               (0x0000FFFFUL <<  0U) /* (RW) ADC Injected DATA */

/*********** ADC regular data register (ADC_DR) *******************************/
#define CHIP_REGFLD_ADC_DR_DATA                 (0x0000FFFFUL <<  0U) /* (RW) ADC regular DATA */
#define CHIP_REGFLD_ADC_DR_ADC2_DATA            (0x0000FFFFUL << 16U) /* (RW) ADC1: In dual mode, these bits contain the regular data of ADC2. */

/*********** ADC test register (ADC_TEST) *************************************/
#define CHIP_REGFLD_ADC_TEST_EN                 (0x00000001UL <<  0U) /* (RW) ADC test mode enable */

/*********** ADC OPA control register (ADC_OPACTRL) ***************************/
#define CHIP_REGFLD_ADC_OPACTRL_JSQ_OPA_EN(x)   (0x00000001UL <<  (0U + (x)))   /* (RW) Injected channel x for OPA mux enable (x=0..3)*/
#define CHIP_REGFLD_ADC_OPACTRL_JSQ_OPA_SEL(x)  (0x00000007UL <<  (4U + 3*(x))) /* (RW) Injected channel x for OPA mux selection (x=0..3)*/
#define CHIP_REGFLD_ADC_OPACTRL_OPA_SETUP_TIME  (0x000003FFUL <<  16U)          /* (RW) Setup time for OPA mux (0..1023 ADC clock cycles) */


#endif // CHIP_N32G03X_REGS_ADC_H
