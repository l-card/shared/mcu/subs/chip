#ifndef CHIP_LPC11U6X_REGS_PININT_H
#define CHIP_LPC11U6X_REGS_PININT_H


/**
 * @brief LPC11u6x Pin Interrupt and Pattern Match register block structure
 */
typedef struct {            /*!< PIN_INT Structure */
    __IO uint32_t ISEL;     /*!< Pin Interrupt Mode register */
    __IO uint32_t IENR;     /*!< Pin Interrupt Enable (Rising) register */
    __IO uint32_t SIENR;    /*!< Set Pin Interrupt Enable (Rising) register */
    __IO uint32_t CIENR;    /*!< Clear Pin Interrupt Enable (Rising) register */
    __IO uint32_t IENF;     /*!< Pin Interrupt Enable Falling Edge / Active Level register */
    __IO uint32_t SIENF;    /*!< Set Pin Interrupt Enable Falling Edge / Active Level register */
    __IO uint32_t CIENF;    /*!< Clear Pin Interrupt Enable Falling Edge / Active Level address */
    __IO uint32_t RISE;     /*!< Pin Interrupt Rising Edge register */
    __IO uint32_t FALL;     /*!< Pin Interrupt Falling Edge register */
    __IO uint32_t IST;      /*!< Pin Interrupt Status register */
    __IO uint32_t PMCTRL;   /*!< GPIO pattern match interrupt control register          */
    __IO uint32_t PMSRC;    /*!< GPIO pattern match interrupt bit-slice source register */
    __IO uint32_t PMCFG;    /*!< GPIO pattern match interrupt bit slice configuration register */
} LPC_PIN_INT_T;

/**
 * LPC11u6x Pin Interrupt and Pattern match engine register
 * bit fields and macros
 */
/* PININT interrupt control register */
#define PININT_PMCTRL_PMATCH_SEL (1 << 0)
#define PININT_PMCTRL_RXEV_ENA   (1 << 1)

/* PININT Bit slice source register bits */
#define PININT_SRC_BITSOURCE_START  8
#define PININT_SRC_BITSOURCE_MASK   7

/* PININT Bit slice configuration register bits */
#define PININT_SRC_BITCFG_START  8
#define PININT_SRC_BITCFG_MASK   7

/**
 * LPC11u6x Pin Interrupt channel values
 */
#define PININTCH0         (1 << 0)
#define PININTCH1         (1 << 1)
#define PININTCH2         (1 << 2)
#define PININTCH3         (1 << 3)
#define PININTCH4         (1 << 4)
#define PININTCH5         (1 << 5)
#define PININTCH6         (1 << 6)
#define PININTCH7         (1 << 7)
#define PININTCH(ch)      (1 << (ch))

/**
 * LPC11u6x Pin Matching Interrupt bit slice enum values
 */
typedef enum Chip_PININT_BITSLICE {
    PININTBITSLICE0 = 0,    /*!< PININT Bit slice 0 */
    PININTBITSLICE1 = 1,    /*!< PININT Bit slice 1 */
    PININTBITSLICE2 = 2,    /*!< PININT Bit slice 2 */
    PININTBITSLICE3 = 3,    /*!< PININT Bit slice 3 */
    PININTBITSLICE4 = 4,    /*!< PININT Bit slice 4 */
    PININTBITSLICE5 = 5,    /*!< PININT Bit slice 5 */
    PININTBITSLICE6 = 6,    /*!< PININT Bit slice 6 */
    PININTBITSLICE7 = 7    /*!< PININT Bit slice 7 */
} Chip_PININT_BITSLICE_T;

/**
 * LPC11u6x Pin Matching Interrupt bit slice configuration enum values
 */
typedef enum Chip_PININT_BITSLICE_CFG {
    PININT_PATTERNCONST1           = 0x0,    /*!< Contributes to product term match */
    PININT_PATTERNRISING           = 0x1,    /*!< Rising edge */
    PININT_PATTERNFALLING          = 0x2,    /*!< Falling edge */
    PININT_PATTERNRISINGRFALLING   = 0x3,    /*!< Rising or Falling edge */
    PININT_PATTERNHIGH             = 0x4,    /*!< High level */
    PININT_PATTERNLOW              = 0x5,    /*!< Low level */
    PININT_PATTERCONST0            = 0x6,    /*!< Never contributes for match */
    PININT_PATTEREVENT             = 0x7    /*!< Match occurs on event */
} Chip_PININT_BITSLICE_CFG_T;



#endif /* CHIP_LPC11U6X_REGS_PININT_H */
