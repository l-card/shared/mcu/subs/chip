#ifndef CHIP_STM32F0XX_REGS_GPIO_H
#define CHIP_STM32F0XX_REGS_GPIO_H

#include <stdint.h>

typedef struct {
  __IO uint32_t MODER;       /*!< GPIO port mode register,               Address offset: 0x00      */
  __IO uint32_t OTYPER;      /*!< GPIO port output type register,        Address offset: 0x04      */
  __IO uint32_t OSPEEDR;     /*!< GPIO port output speed register,       Address offset: 0x08      */
  __IO uint32_t PUPDR;       /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
  __IO uint32_t IDR;         /*!< GPIO port input data register,         Address offset: 0x10      */
  __IO uint32_t ODR;         /*!< GPIO port output data register,        Address offset: 0x14      */
  __IO uint32_t BSRR;        /*!< GPIO port bit set/reset  register,     Address offset: 0x18      */
  __IO uint32_t LCKR;        /*!< GPIO port configuration lock register, Address offset: 0x1C      */
  __IO uint32_t AFR[2];      /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
  __IO uint32_t BRR;         /*!< GPIO Bit Reset register,               Address offset: 0x28      */
} CHIP_REGS_GPIO_T;


#define CHIP_REGS_GPIO(i)        ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIO(i))
#define CHIP_REGS_GPIOA          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOA)
#define CHIP_REGS_GPIOB          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOB)
#define CHIP_REGS_GPIOC          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOC)
#define CHIP_REGS_GPIOD          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOD)
#define CHIP_REGS_GPIOE          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOE)
#define CHIP_REGS_GPIOF          ((CHIP_REGS_GPIO_T *)CHIP_MEMRGN_ADDR_PERIPH_GPIOF)



#endif // CHIP_STM32F0XX_REGS_GPIO_H
