#ifndef CHIP_TARGET_H
#define CHIP_TARGET_H


#include "chip_cfg_defs.h"
#include "chip_core_num.h"

#include "chip_std_defs.h"
#include "chip_devtype_spec_features.h"

#include "chip_ioreg_defs.h"

#include "chip_irq_nums.h"
#include "chip_mmap.h"
#include "chip_spec_csr.h"

#include "regs/regs_bcfg.h"
#include "regs/regs_bpor.h"
#include "regs/regs_butn.h"
#include "regs/regs_pcfg.h"
#include "regs/regs_ppor.h"
#include "regs/regs_mchtmr.h"
#include "regs/regs_otp.h"
#include "regs/regs_plic.h"
#include "regs/regs_plicsw.h"
#include "regs/regs_sysctl.h"
#include "regs/regs_pllcctl.h"

#include "regs/regs_ioc.h"
#include "regs/regs_gpio.h"
#include "regs/regs_gpiom.h"
#include "regs/regs_gptmr.h"
#include "regs/regs_uart.h"

#include "regs/regs_i2s.h"

#include "regs/regs_pwm.h"
#include "regs/regs_qei.h"
#include "regs/regs_hall.h"
#include "regs/regs_trgm.h"
#include "regs/regs_synt.h"

#include "regs/regs_enet.h"
#include "regs/regs_conctl.h"

#include "init/chip_clk.h"

#include "chip_pins.h"
#include "chip_trgm_map.h"
#include "chip_interrupt.h"

#endif // CHIP_TARGET_H
