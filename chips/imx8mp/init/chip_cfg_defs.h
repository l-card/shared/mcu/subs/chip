#ifndef CHIP_CFG_DEFS_H
#define CHIP_CFG_DEFS_H

#include "chip_config.h"

/* -------------- ENET_AXI_ROOT clock ----------------------------------------*/
#ifdef CHIP_CFG_CLK_ROOT_ENET_AXI_SETUP_EN
    #define CHIP_CLK_ROOT_ENET_AXI_SETUP_EN         CHIP_CFG_CLK_ROOT_ENET_AXI_SETUP_EN
#else
    #define CHIP_CLK_ROOT_ENET_AXI_SETUP_EN         0
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_AXI_EN
    #define CHIP_CLK_ROOT_ENET_AXI_EN                   CHIP_CFG_CLK_ROOT_ENET_AXI_EN
#else
    #define CHIP_CLK_ROOT_ENET_AXI_EN                   0
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_AXI_SRC
    #define CHIP_CLK_ROOT_ENET_AXI_SRC                  CHIP_CFG_CLK_ROOT_ENET_AXI_SRC
#else
    #define CHIP_CLK_ROOT_ENET_AXI_SRC                  CHIP_CLK_REF_24M
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_AXI_PREDIV
    #define CHIP_CLK_ROOT_ENET_AXI_PREDIV               CHIP_CFG_CLK_ROOT_ENET_AXI_PREDIV
#else
    #define CHIP_CLK_ROOT_ENET_AXI_PREDIV               1
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_AXI_POSTDIV
    #define CHIP_CLK_ROOT_ENET_AXI_POSTDIV              CHIP_CFG_CLK_ROOT_ENET_AXI_POSTDIV
#else
    #define CHIP_CLK_ROOT_ENET_AXI_POSTDIV              1
#endif


/* -------------- ENET_REF_CLK_ROOT clock ----------------------------------------*/
#ifdef CHIP_CFG_CLK_ROOT_ENET_REF_SETUP_EN
    #define CHIP_CLK_ROOT_ENET_REF_SETUP_EN             CHIP_CFG_CLK_ROOT_ENET_REF_SETUP_EN
#else
    #define CHIP_CLK_ROOT_ENET_REF_SETUP_EN             0
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_REF_EN
    #define CHIP_CLK_ROOT_ENET_REF_EN                   CHIP_CFG_CLK_ROOT_ENET_REF_EN
#else
    #define CHIP_CLK_ROOT_ENET_REF_EN                   0
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_REF_SRC
    #define CHIP_CLK_ROOT_ENET_REF_SRC                  CHIP_CFG_CLK_ROOT_ENET_REF_SRC
#else
    #define CHIP_CLK_ROOT_ENET_REF_SRC                  CHIP_CLK_REF_24M
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_REF_PREDIV
    #define CHIP_CLK_ROOT_ENET_REF_PREDIV               CHIP_CFG_CLK_ROOT_ENET_REF_PREDIV
#else
    #define CHIP_CLK_ROOT_ENET_REF_PREDIV               1
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_REF_POSTDIV
    #define CHIP_CLK_ROOT_ENET_REF_POSTDIV              CHIP_CFG_CLK_ROOT_ENET_REF_POSTDIV
#else
    #define CHIP_CLK_ROOT_ENET_REF_POSTDIV              1
#endif

/* -------------- ENET_TIMER_CLK_ROOT clock ----------------------------------------*/
#ifdef CHIP_CFG_CLK_ROOT_ENET_TIMER_SETUP_EN
    #define CHIP_CLK_ROOT_ENET_TIMER_SETUP_EN           CHIP_CFG_CLK_ROOT_ENET_TIMER_SETUP_EN
#else
    #define CHIP_CLK_ROOT_ENET_TIMER_SETUP_EN           0
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_TIMER_EN
    #define CHIP_CLK_ROOT_ENET_TIMER_EN                 CHIP_CFG_CLK_ROOT_ENET_TIMER_EN
#else
    #define CHIP_CLK_ROOT_ENET_TIMER_EN                 0
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_TIMER_SRC
    #define CHIP_CLK_ROOT_ENET_TIMER_SRC                CHIP_CFG_CLK_ROOT_ENET_TIMER_SRC
#else
    #define CHIP_CLK_ROOT_ENET_TIMER_SRC                CHIP_CLK_REF_24M
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_TIMER_PREDIV
    #define CHIP_CLK_ROOT_ENET_TIMER_PREDIV             CHIP_CFG_CLK_ROOT_ENET_TIMER_PREDIV
#else
    #define CHIP_CLK_ROOT_ENET_TIMER_PREDIV             1
#endif
#ifdef CHIP_CFG_CLK_ROOT_ENET_TIMER_POSTDIV
    #define CHIP_CLK_ROOT_ENET_TIMER_POSTDIV            CHIP_CFG_CLK_ROOT_ENET_TIMER_POSTDIV
#else
    #define CHIP_CLK_ROOT_ENET_TIMER_POSTDIV            1
#endif





#endif // CHIP_CFG_DEFS_H
