#ifndef CHIP_CFG_DEFS_H
#define CHIP_CFG_DEFS_H

#include "chip_config.h"
#include "chip_std_defs.h"


#ifdef CHIP_CFG_CLK_OSC_MODE
    #define CHIP_CLK_OSC_MODE       CHIP_CFG_CLK_OSC_MODE
#else
    #define CHIP_CLK_OSC_MODE       CHIP_CLK_EXTMODE_DIS
#endif

#define CHIP_CLK_OSC_EN             (CHIP_CLK_OSC_MODE != CHIP_CLK_EXTMODE_DIS)
#if CHIP_CLK_OSC_EN
    #ifndef CHIP_CFG_CLK_OSC_FREQ
        #error extenal clock frequency is not specified
    #endif
    #define CHIP_CLK_OSC_FREQ       CHIP_CFG_CLK_OSC_FREQ
    #ifdef CHIP_CFG_CLK_OSC_STARTUP_DELAY_US
        #define CHIP_CLK_OSC_STARTUP_DELAY_US  CHIP_CFG_CLK_OSC_STARTUP_DELAY_US
    #else
        #define CHIP_CLK_OSC_STARTUP_DELAY_US  2
    #endif
#else
    #define CHIP_CLK_OSC_FREQ       0
#endif



#ifdef CHIP_CFG_CLK_HFRC_EN
    #define CHIP_CLK_HFRC_EN        CHIP_CFG_CLK_HFRC_EN
#else
    #define CHIP_CLK_HFRC_EN        1
#endif

#ifdef CHIP_CFG_CLK_LFRC_EN
    #define CHIP_CLK_LFRC_EN        CHIP_CFG_CLK_LFRC_EN
#else
    #define CHIP_CLK_LFRC_EN        1
#endif

#ifdef CHIP_CFG_CLK_SYS_SRC
    #define CHIP_CLK_SYS_SRC        CHIP_CFG_CLK_SYS_SRC
#else
    #define CHIP_CLK_SYS_SRC        CHIP_CLK_HFRC
#endif

#ifdef CHIP_CFG_CLK_SYS_DIV
    #define CHIP_CLK_SYS_DIV        CHIP_CFG_CLK_SYS_DIV
#else
    #define CHIP_CLK_SYS_DIV        8
#endif

#ifdef CHIP_CFG_CLK_CLKO0_EN
    #define CHIP_CLK_CLKO0_EN       CHIP_CFG_CLK_CLKO0_EN
#else
    #define CHIP_CLK_CLKO0_EN       0
#endif

#ifdef CHIP_CFG_CLK_CLKO1_EN
    #define CHIP_CLK_CLKO1_EN       CHIP_CFG_CLK_CLKO1_EN
#else
    #define CHIP_CLK_CLKO1_EN       0
#endif

#ifdef CHIP_CFG_CLK_WDT_SRC
    #define CHIP_CLK_WDT_SRC        CHIP_CFG_CLK_WDT_SRC
#else
    #define CHIP_CLK_WDT_SRC        CHIP_CLK_HFRC_DIV16
#endif


#ifdef CHIP_CFG_LTIMER_EN
    #define CHIP_LTIMER_EN          CHIP_CFG_LTIMER_EN
#else
    #define CHIP_LTIMER_EN          1
#endif


#ifdef CHIP_CFG_CLK_ADC_EN
    #define CHIP_CLK_ADC_EN         CHIP_CFG_CLK_ADC_EN
#else
    #define CHIP_CLK_ADC_EN         1
#endif
#ifdef CHIP_CFG_CLK_USART_EN
    #define CHIP_CLK_USART_EN       CHIP_CFG_CLK_USART_EN
#else
    #define CHIP_CLK_USART_EN       1
#endif
#ifdef CHIP_CFG_CLK_SPI_EN
    #define CHIP_CLK_SPI_EN         CHIP_CFG_CLK_SPI_EN
#else
    #define CHIP_CLK_SPI_EN         1
#endif
#ifdef CHIP_CFG_CLK_TIM0_EN
    #define CHIP_CLK_TIM0_EN        CHIP_CFG_CLK_TIM0_EN
#else
    #define CHIP_CLK_TIM0_EN        1
#endif
#ifdef CHIP_CFG_CLK_TIM1_EN
    #define CHIP_CLK_TIM1_EN        CHIP_CFG_CLK_TIM1_EN
#else
    #define CHIP_CLK_TIM1_EN        1
#endif
#ifdef CHIP_CFG_CLK_TIM2_EN
    #define CHIP_CLK_TIM2_EN        CHIP_CFG_CLK_TIM2_EN
#else
    #define CHIP_CLK_TIM2_EN        1
#endif
#ifdef CHIP_CFG_CLK_TIM3_EN
    #define CHIP_CLK_TIM3_EN        CHIP_CFG_CLK_TIM3_EN
#else
    #define CHIP_CLK_TIM3_EN        1
#endif
#ifdef CHIP_CFG_CLK_TWI_EN
    #define CHIP_CLK_TWI_EN         CHIP_CFG_CLK_TWI_EN
#else
    #define CHIP_CLK_TWI_EN         1
#endif
#ifdef CHIP_CFG_CLK_PCI_EN
    #define CHIP_CLK_PCI_EN         CHIP_CFG_CLK_PCI_EN
#else
    #define CHIP_CLK_PCI_EN         1
#endif
#ifdef CHIP_CFG_CLK_FLASH_EN
    #define CHIP_CLK_FLASH_EN       CHIP_CFG_CLK_FLASH_EN
#else
    #define CHIP_CLK_FLASH_EN       1
#endif
#ifdef CHIP_CFG_CLK_WDT_EN
    #define CHIP_CLK_WDT_EN         CHIP_CFG_CLK_WDT_EN
#else
    #define CHIP_CLK_WDT_EN       1
#endif



#endif // CHIP_CFG_DEFS_H
