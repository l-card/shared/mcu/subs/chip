#ifndef STM32_DMA_V1_REGS_H
#define STM32_DMA_V1_REGS_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t CCR;          /* DMA channel x configuration register          */
    __IO uint32_t CNDTR;        /* DMA channel x number of data register         */
    __IO uint32_t CPAR;         /* DMA channel x peripheral address register     */
    __IO uint32_t CMAR;         /* DMA channel x memory address register       */
#if CHIP_SUPPORT_DMA_CHSEL
    __IO uint32_t CHSEL;        /* DMA Channel x Request Select Register        */
#else
    uint32_t Reserved;
#endif
} CHIP_REGS_DMA_CHANNEL_T;

typedef struct {
    __IO uint32_t ISR;          /* DMA interrupt status register,               Address offset: 0x00 */
    __IO uint32_t IFCR;         /* DMA interrupt flag clear register,           Address offset: 0x04 */
    CHIP_REGS_DMA_CHANNEL_T CH[CHIP_DMA_CHANNEL_CNT];
} CHIP_REGS_DMA_T;


/*********** DMA interrupt status/clear registers (DMA_ISR, DMA_IFCR) ******/
#define CHIP_REGFLD_DMA_INT_FLAG(ch, intnum)    (0x00000001UL <<  ((intnum) + (ch * 4)))
#define CHIP_REGFLD_DMA_INT_GIF(x)              CHIP_REGFLD_DMA_INT_FLAG(x, 0) /* Channel x Global interrupt flag */
#define CHIP_REGFLD_DMA_INT_TCIF(x)             CHIP_REGFLD_DMA_INT_FLAG(x, 1) /* Channel x Transfer Complete flag */
#define CHIP_REGFLD_DMA_INT_HTIF(x)             CHIP_REGFLD_DMA_INT_FLAG(x, 2) /* Channel x Half Transfer flag */
#define CHIP_REGFLD_DMA_INT_TEIF(x)             CHIP_REGFLD_DMA_INT_FLAG(x, 3) /* Channel x Transfer Error flag  */


/*********** DMA channel x configuration register (DMA_CCRx) ****************/
#define CHIP_REGFLD_DMA_CCR_EN                 (0x00000001UL <<  0U) /* Channel enable */
#define CHIP_REGFLD_DMA_CCR_TCIE               (0x00000001UL <<  1U) /* Transfer complete interrupt enable */
#define CHIP_REGFLD_DMA_CCR_HTIE               (0x00000001UL <<  2U) /* Half Transfer interrupt enable */
#define CHIP_REGFLD_DMA_CCR_TEIE               (0x00000001UL <<  3U) /* Transfer error interrupt enable */
#define CHIP_REGFLD_DMA_CCR_DIR                (0x00000001UL <<  4U) /* Data transfer direction */
#define CHIP_REGFLD_DMA_CCR_CIRC               (0x00000001UL <<  5U) /* Circular mode */
#define CHIP_REGFLD_DMA_CCR_PINC               (0x00000001UL <<  6U) /* Peripheral increment mode */
#define CHIP_REGFLD_DMA_CCR_MINC               (0x00000001UL <<  7U) /* Memory increment mode */
#define CHIP_REGFLD_DMA_CCR_PSIZE              (0x00000003UL <<  8U) /* Peripheral size */
#define CHIP_REGFLD_DMA_CCR_MSIZE              (0x00000003UL << 10U) /* Memory size */
#define CHIP_REGFLD_DMA_CCR_PL                 (0x00000003UL << 12U) /* Channel Priority level */
#define CHIP_REGFLD_DMA_CCR_MEM2MEM            (0x00000001UL << 14U) /* Memory to memory mode */
#define CHIP_REGMSK_DMA_CCR_RESERVED           (0xFFFF8000UL)

#define CHIP_REGFLDVAL_DMA_CCR_DIR_PTOM        0 /* read from peripheral */
#define CHIP_REGFLDVAL_DMA_CCR_DIR_MTOP        1 /* read from memory */

#define CHIP_REGFLDVAL_DMA_CCR_SIZE_8          0
#define CHIP_REGFLDVAL_DMA_CCR_SIZE_16         1
#define CHIP_REGFLDVAL_DMA_CCR_SIZE_32         2

#define CHIP_REGFLDVAL_DMA_CCR_PL_LOW          0
#define CHIP_REGFLDVAL_DMA_CCR_PL_MEDIUM       1
#define CHIP_REGFLDVAL_DMA_CCR_PL_HIGH         2
#define CHIP_REGFLDVAL_DMA_CCR_PL_VHIGH        3



/*********** DMA channel x number of data to transfer register (DMA_CNDTRx) */
#define CHIP_REGFLD_DMA_CNDTR_NDT              (0x0000FFFFUL <<  0U) /* Number of data to Transfer */
#define CHIP_REGMSK_DMA_CNDTR_RESERVED         (0xFFFF0000UL)

/*********** DMA channel x peripheral address register (DMA_CPARx) **********/
#define CHIP_REGFLD_DMA_CPAR_PA                (0xFFFFFFFFUL <<  0U) /* Peripheral Address */

/*********** DMA channel x memory 0 address register (DMA_CM0ARx) ***********/
#define CHIP_REGFLD_DMA_CM0AR_MA               (0xFFFFFFFFUL <<  0U) /* Memory Address */




#endif // STM32_DMA_V1_REGS_H
