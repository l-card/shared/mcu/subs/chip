#ifndef CHIP_PINS_N32G03X_H
#define CHIP_PINS_N32G03X_H

#define CHIP_PIN_USE_OSPEED_VLOW
#include "chips/shared/stm32/chip_pin_defs_v2.h"
#include "chip_devtype_spec_features.h"
#include "chip_config.h"

#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_F 5

/* ------------------------ Функции пинов ------------------------------------*/

/* ----------- Порт A ---------------*/
/* Пин PA0, WKUP0 */
#define CHIP_PIN_PA0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 0)
#define CHIP_PIN_PA0_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 0)
#define CHIP_PIN_PA0_I2S1_CK              CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 0)
#define CHIP_PIN_PA0_USART1_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 1) /* *hk, py */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA0_OSC_IN               CHIP_PIN_ID_AF    (CHIP_PORT_F, 0, 2) /* Bypass mode only */
#endif
#define CHIP_PIN_PA0_TIM8_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 3) /* ~hk(tim2), ~py(tim1) */
#define CHIP_PIN_PA0_USART2_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 4) /* ~*hk */
#define CHIP_PIN_PA0_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 5)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA0_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 6)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA0_LPUART2_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 6)
#endif
#define CHIP_PIN_PA0_LPTIM_IN1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 7)
#define CHIP_PIN_PA0_COMP1_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 8)  /* ~py */
#define CHIP_PIN_PA0_RTC_TAMP2            CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 9)  /* ~hk */
#define CHIP_PIN_PA0_TIM8_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 10) /* ~hk(tim2) */
#define CHIP_PIN_PA0_LPUART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 11)
#define CHIP_PIN_PA0_ADC_IN0              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 0)     /* hk */
#define CHIP_PIN_PA0_COMP1_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 0)
#define CHIP_PIN_PA0_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 0)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA0_TSC0                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 0)
#endif


/* Пин PA1 */
#define CHIP_PIN_PA1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_SPI1_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 0)
#define CHIP_PIN_PA1_I2S1_WS              CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 0)
#define CHIP_PIN_PA1_USART1_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 1) /* *hk, py */
#define CHIP_PIN_PA1_TIM8_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 2) /* ~hk(tim2), ~py(tim1) */
#define CHIP_PIN_PA1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 3) /* ~py */
#define CHIP_PIN_PA1_USART2_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 4) /* ~*hk,  py */
#define CHIP_PIN_PA1_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 5)
#ifdef CHIP_DEV_N32G031
#define CHIP_PIN_PA1_LPTIM_IN2            CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 6)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA1_LPUART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 6)
#endif
#define CHIP_PIN_PA1_I2C1_SMBA            CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 1, 7)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA1_LPTIM_IN2            CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 9)
#endif
#define CHIP_PIN_PA1_TIM3_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 10)
#define CHIP_PIN_PA1_ADC_IN1              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)     /* hk */
#define CHIP_PIN_PA1_COMP1_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA1_COMP2_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_TSC1                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)
#endif


/* Пин PA2, WKUP2 */
#if (CHIP_DEV_PKG != CHIP_PKG_UFQFPN20)
#define CHIP_PIN_PA2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 2)
#define CHIP_PIN_PA2_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 0)
#define CHIP_PIN_PA2_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 0)
#define CHIP_PIN_PA2_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 1) /* *hk */
#define CHIP_PIN_PA2_TIM8_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 2) /* ~hk(tim2) */
#define CHIP_PIN_PA2_TIM1_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 3)
#define CHIP_PIN_PA2_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 4) /* ~*hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA2_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 5)
#define CHIP_PIN_PA2_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 6)
#define CHIP_PIN_PA2_COMP2_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 8)
#endif
#define CHIP_PIN_PA2_ADC_IN2              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 2)    /* hk */
#define CHIP_PIN_PA2_OPAMP_VINM           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 2)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA2_COMP2_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 2)
#define CHIP_PIN_PA2_TSC2                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 2)
#endif
#endif


/* Пин PA3 */
#if (CHIP_DEV_PKG != CHIP_PKG_UFQFPN20)
#define CHIP_PIN_PA3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 3)
#define CHIP_PIN_PA3_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 0)
#define CHIP_PIN_PA3_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 0)
#define CHIP_PIN_PA3_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 1) /* *hk */
#define CHIP_PIN_PA3_TIM8_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 2) /* ~hk(tim2) */
#define CHIP_PIN_PA3_TIM1_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 3) /* ~*hk(tim15) */
#define CHIP_PIN_PA3_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 4) /* ~*hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA3_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 5)
#endif
#define CHIP_PIN_PA3_LPUART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 6)
#define CHIP_PIN_PA3_ADC_IN3              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 3)    /* hk */
#define CHIP_PIN_PA3_COMP1_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 3)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA3_COMP2_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 3)
#define CHIP_PIN_PA3_TSC3                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 3)
#endif
#endif


/* Пин PA4 */
#define CHIP_PIN_PA4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_SPI1_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 0) /* hk */
#define CHIP_PIN_PA4_I2S1_WS              CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 0) /* *hk */
#define CHIP_PIN_PA4_USART1_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 1) /* *hk */
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA4_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 2)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA4_TIM4_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 2)
#endif
#define CHIP_PIN_PA4_TIM1_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 3) /* ~hk(tim14) */
#define CHIP_PIN_PA4_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 4) /* ~*hk */
#define CHIP_PIN_PA4_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 5)
#define CHIP_PIN_PA4_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 5)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA4_UART6_TX             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 6)
#endif
#define CHIP_PIN_PA4_I2C1_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 4, 7)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA4_COMP2_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 8)
#define CHIP_PIN_PA4_CAN1_TX              CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 9)
#endif
#define CHIP_PIN_PA4_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 10)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA4_LPTIM_IN1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 11)
#define CHIP_PIN_PA4_LPTIM_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 12)
#endif
#define CHIP_PIN_PA4_TIM8_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 13)
#define CHIP_PIN_PA4_ADC_IN4              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)    /* hk */
#define CHIP_PIN_PA4_COMP1_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA4_COMP2_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_TSC4                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)
#endif


/* Пин PA5 */
#define CHIP_PIN_PA5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 0) /* hk */
#define CHIP_PIN_PA5_I2S1_CK              CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 0) /* *hk */
#define CHIP_PIN_PA5_TIM8_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 1) /* ~hk(tim2) */
#define CHIP_PIN_PA5_TIM8_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 2) /* ~hk(tim2) */
#define CHIP_PIN_PA5_TIM1_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 3)
#define CHIP_PIN_PA5_TIM1_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 4)
#define CHIP_PIN_PA5_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 5)
#define CHIP_PIN_PA5_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 5)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA5_UART6_RX             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 6)
#define CHIP_PIN_PA5_CAN1_RX              CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 9)
#endif
#define CHIP_PIN_PA5_ADC_IN5              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)    /* hk */
#define CHIP_PIN_PA5_COMP1_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_OPAMP_VINM           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA5_COMP2_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_TSC5                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)
#endif


/* Пин PA6 */
#define CHIP_PIN_PA6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 6)
#define CHIP_PIN_PA6_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 0) /* hk */
#define CHIP_PIN_PA6_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 0) /* *hk */ /* нет в datasheet?? */
#define CHIP_PIN_PA6_TIM1_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 1) /* ~hk */
#define CHIP_PIN_PA6_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 2) /* ~hk */
#define CHIP_PIN_PA6_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 3) /* ~hk */
#define CHIP_PIN_PA6_TIM8_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 4) /* ~hk(tim16) */
#define CHIP_PIN_PA6_LPUART1_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 5)
#define CHIP_PIN_PA6_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 6)
#define CHIP_PIN_PA6_I2C2_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 6, 7)
#define CHIP_PIN_PA6_COMP1_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 8)
#define CHIP_PIN_PA6_LPTIM_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 9)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA6_EVENTOUT_2           CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 10) /* dublicate ??? */
#endif
#define CHIP_PIN_PA6_BEEPER1_OUT          CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 11)
#define CHIP_PIN_PA6_ADC_IN6              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 6)    /* hk */
#define CHIP_PIN_PA6_OPAMP_VOUT           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 6)    /* настройки?? */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA6_COMP1_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 6)
#define CHIP_PIN_PA6_TSC6                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 6)
#endif


/* Пин PA7 */
#define CHIP_PIN_PA7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 7)
#define CHIP_PIN_PA7_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 0) /* hk */
#define CHIP_PIN_PA7_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 0) /* *hk */
#define CHIP_PIN_PA7_TIM8_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 1)
#define CHIP_PIN_PA7_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 2) /* ~hk */
#define CHIP_PIN_PA7_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 3) /* ~hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA7_TIM4_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 4)
#endif
#define CHIP_PIN_PA7_TIM1_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 5)
#define CHIP_PIN_PA7_LPUART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 6)
#define CHIP_PIN_PA7_I2C2_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 7, 7)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA7_COMP2_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 8)
#endif
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA7_SPI2_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 9)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA7_LPTIM_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 9)
#endif
#define CHIP_PIN_PA7_USART2_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 10)
#define CHIP_PIN_PA7_BEEPER1_N_OUT        CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 11)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA7_TIM3_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 12)
#define CHIP_PIN_PA7_EVENTOUT_2           CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 13) /* dublicate ??? */
#endif
#define CHIP_PIN_PA7_ADC_IN7              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 7)    /* hk */
#define CHIP_PIN_PA7_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 7)
#define CHIP_PIN_PA7_COMP1_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 7)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA7_TSC7                 CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 7)
#endif

/* Пин PA8 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PA8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 8)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA8_SPI2_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 0)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA8_SPI3_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 0)
#endif
#define CHIP_PIN_PA8_TIM8_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 1)
#define CHIP_PIN_PA8_TIM1_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 2) /* hk */
#define CHIP_PIN_PA8_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 3) /* hk */
#define CHIP_PIN_PA8_USART1_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 4) /* ~hk */
#define CHIP_PIN_PA8_MCO                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 5) /* ~hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA8_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 6)
#define CHIP_PIN_PA8_EVENTOUT_2           CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 8) /* dublicate ??? */
#define CHIP_PIN_PA8_TIM8_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 9)
#define CHIP_PIN_PA8_LPTIM_IN1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 10)
#define CHIP_PIN_PA8_COMP2_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 8)
#endif
#endif


/* Пин PA9 */
#define CHIP_PIN_PA9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 9)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA9_SPI2_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 0)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA9_SPI3_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 0)
#endif
#define CHIP_PIN_PA9_TIM8_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 1) /* ~*hk(tim15) */
#define CHIP_PIN_PA9_TIM1_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 2) /* hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA9_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 3)
#endif
#define CHIP_PIN_PA9_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 4) /* *hk */
#define CHIP_PIN_PA9_TIM8_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 5)
#define CHIP_PIN_PA9_I2C1_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 9, 6) /* *hk */
#define CHIP_PIN_PA9_I2C2_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 9, 7)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA9_COMP1_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 8)
#endif
#define CHIP_PIN_PA9_LPTIM_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 9)
#define CHIP_PIN_PA9_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 10) /* ~*hk */
#define CHIP_PIN_PA9_MCO                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 11) /* *hk */ /* ?? нет в datasheet */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA9_COMP2_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 9)
#define CHIP_PIN_PA9_TSC_R_CHG            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 9)
#endif


/* Пин PA10 */
#define CHIP_PIN_PA10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 10)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA10_SPI2_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 0)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA10_SPI3_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 0)
#endif
#define CHIP_PIN_PA10_TIM8_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 1) /* ~hk(tim17) */
#define CHIP_PIN_PA10_TIM1_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 2) /* hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA10_TIM3_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 3)
#endif
#define CHIP_PIN_PA10_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 4) /* *hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA10_TIM3_CH1_2          CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 5) /* для чего повтор?? */
#endif
#define CHIP_PIN_PA10_I2C1_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 10, 6) /* *hk */
#define CHIP_PIN_PA10_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 10, 7)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA10_COMP2_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 8)
#define CHIP_PIN_PA10_TIM8_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 9)
#endif
#define CHIP_PIN_PA10_USART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 10)
#define CHIP_PIN_PA10_RTC_REFIN           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 11)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA10_OPAMP_VINP          CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 10)
#endif


/* Пин PA11 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PA11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 11)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA11_SPI2_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 0)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA11_SPI3_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 0)
#endif
#define CHIP_PIN_PA11_TIM1_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 2) /* hk */
#define CHIP_PIN_PA11_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 3) /* *hk */
#define CHIP_PIN_PA11_USART1_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 4) /* *hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA11_UART6_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 6)
#endif
#define CHIP_PIN_PA11_I2C2_SCL            CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 11, 7) /* ~hk */
#define CHIP_PIN_PA11_COMP1_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 8)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA11_COMP3_INP           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 11)
#define CHIP_PIN_PA11_TSC18               CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 11)
#endif
#endif


/* Пин PA12 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PA12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 12)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA12_SPI2_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 0)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA12_SPI3_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 0)
#endif
#define CHIP_PIN_PA12_TIM1_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 2) /* hk */
#define CHIP_PIN_PA12_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 3) /* *hk */
#define CHIP_PIN_PA12_USART1_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 4) /* *hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA12_UART6_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 6)
#endif
#define CHIP_PIN_PA12_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 12, 7) /* *hk */
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PA12_COMP1_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 8)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PA12_COMP2_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 8)
#endif
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PA12_COMP1_INP           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 12)
#define CHIP_PIN_PA12_COMP3_INM           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 12)
#define CHIP_PIN_PA12_TSC19               CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 12)
#endif
#endif


/* Пин PA13, SWDIO */
#define CHIP_PIN_PA13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 13)
#define CHIP_PIN_PA13_SWDIO               CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 0) /* hk */
#define CHIP_PIN_PA13_USART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 1)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA13_LPUART1_RX          CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 2)
#define CHIP_PIN_PA13_LPTIM_ETR           CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 3)
#endif
#define CHIP_PIN_PA13_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 4)
#define CHIP_PIN_PA13_SPI1_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 5)
#define CHIP_PIN_PA13_I2S1_CK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 5)
#define CHIP_PIN_PA13_USART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 6)
#define CHIP_PIN_PA13_I2C1_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 13, 7)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA13_COMP1_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 8)
#endif


/* Пин PA14, SWCLK */
#define CHIP_PIN_PA14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 14)
#define CHIP_PIN_PA14_SWCLK               CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 0) /* hk */
#define CHIP_PIN_PA14_USART2_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 1) /* *hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA14_LPTIM_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 3)
#endif
#define CHIP_PIN_PA14_USART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 4) /* ~*hk */
#define CHIP_PIN_PA14_SPI1_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 5)
#define CHIP_PIN_PA14_I2S1_MCK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 5) /* ?? нет в datasheet */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA14_LPUART1_TX          CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 6)
#endif
#define CHIP_PIN_PA14_I2C1_SMBA           CHIP_PIN_ID_AF_OD (CHIP_PORT_A, 14, 7)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA14_COMP2_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 8)
#endif


/* Пин PA15 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PA15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 15)
#define CHIP_PIN_PA15_SPI1_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 0) /* hk */
#define CHIP_PIN_PA15_I2S1_WS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 0) /* *hk */
#define CHIP_PIN_PA15_USART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1) /* *hk */
#define CHIP_PIN_PA15_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 3) /* hk */
#define CHIP_PIN_PA15_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 4) /* ~*hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PA15_SPI3_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 5)
#define CHIP_PIN_PA15_LPUART2_RTS         CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 6)
#define CHIP_PIN_PA15_COMP1_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 8)
#define CHIP_PIN_PA15_OPAMP_VINP          CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 15)
#endif
#endif



/* ----------- Порт B ---------------*/
/* Пин PB0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25
#define CHIP_PIN_PB0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 0)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PB0_SPI2_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 0)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB0_SPI3_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 0)
#endif
#define CHIP_PIN_PB0_TIM1_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 1) /* ~hk */
#define CHIP_PIN_PB0_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 2) /* ~hk */
#define CHIP_PIN_PB0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 3) /* ~hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PB0_USART2_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 4)
#define CHIP_PIN_PB0_UART5_TX             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 5)
#define CHIP_PIN_PB0_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 6)
#define CHIP_PIN_PB0_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 6)
#define CHIP_PIN_PB0_EVENTOUT_2           CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 7) /* ??? duplicate */
#define CHIP_PIN_PB0_TIM8_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 9)
#endif
#define CHIP_PIN_PB0_ADC_IN8              CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 0)    /* hk */
#define CHIP_PIN_PB0_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 0)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PB0_TSC8                 CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 0)
#endif
#endif


/* Пин PB1 */
#define CHIP_PIN_PB1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 1)
#define CHIP_PIN_PB1_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 0)
#define CHIP_PIN_PB1_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 0)
#define CHIP_PIN_PB1_TIM1_CH3N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 1) /* ~hk */
#define CHIP_PIN_PB1_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 2) /* ~hk */
#define CHIP_PIN_PB1_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 3)
#define CHIP_PIN_PB1_LPUART1_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 4)
#ifdef CHIP_DEV_N32G031
#define CHIP_PIN_PB1_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 5)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB1_TIM4_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 5)
#define CHIP_PIN_PB1_UART5_RX             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 6)
#define CHIP_PIN_PB1_COMP3_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 8)
#endif
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PB1_SPI2_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 8)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB1_LPTIM_IN1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 8)
#define CHIP_PIN_PB1_TIM8_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 9)
#endif
#define CHIP_PIN_PB1_ADC_IN9              CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 1)    /* hk */
#ifdef CHIP_DEV_N32G031
#define CHIP_PIN_PB1_OPAMP_VINM           CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 1)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB1_TSC9                 CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 1)
#endif


/* Пин PB2 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32) && (CHIP_DEV_PKG != CHIP_PKG_LQFP32)
#define CHIP_PIN_PB2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 2)
#ifdef CHIP_DEV_N32G031
#define CHIP_PIN_PB2_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 5)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB2_TIM4_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 5)
#endif
#define CHIP_PIN_PB2_I2C1_SMBA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 2, 6)
#define CHIP_PIN_PB2_I2C2_SMBA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 2, 7)
#define CHIP_PIN_PB2_LPTIM_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 8)
#define CHIP_PIN_PB2_ADC_IN10             CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 2)
#define CHIP_PIN_PB2_OPAMP_VINM           CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 2)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB2_TSC10                CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 2)
#endif
#endif


/* Пин PB3 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25)
#define CHIP_PIN_PB3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 3)
#define CHIP_PIN_PB3_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 0) /* hk */
#define CHIP_PIN_PB3_I2S1_CK              CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 0) /* *hk */
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PB3_TIM3_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 1)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB3_TIM8_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 1)
#endif
#define CHIP_PIN_PB3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 3) /* ~hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB3_UART5_TX             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 4)
#endif
#define CHIP_PIN_PB3_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 5)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB3_OPAMP_VINM           CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 3)
#define CHIP_PIN_PB3_COMP2_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 3)
#endif
#endif


/* Пин PB4 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PB4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 4)
#define CHIP_PIN_PB4_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 0) /* hk */
#define CHIP_PIN_PB4_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 0) /* *hk */ /* ?? нет в datasheet */
#define CHIP_PIN_PB4_TIM8_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 1) /* ~hk (tim17) */
#define CHIP_PIN_PB4_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 2) /* ~hk */
#define CHIP_PIN_PB4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 3) /* ~hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB4_UART5_RX             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 4)
#endif
#define CHIP_PIN_PB4_LPUART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 5)
#define CHIP_PIN_PB4_LPTIM_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 8)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB4_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 4)
#define CHIP_PIN_PB4_COMP2_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 4)
#endif
#endif


/* Пин PB5 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32)
#define CHIP_PIN_PB5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 5)
#define CHIP_PIN_PB5_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 0) /* hk */
#define CHIP_PIN_PB5_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 0) /* *hk */
#define CHIP_PIN_PB5_TIM8_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 1) /* ~hk (tim16) */
#define CHIP_PIN_PB5_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 2) /* ~hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB5_LPUART2_TX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 3)
#endif
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PB5_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 4)
#endif
#define CHIP_PIN_PB5_TIM8_CH3N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 5)
#define CHIP_PIN_PB5_I2C1_SMBA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 5, 6) /* ~hk */
#define CHIP_PIN_PB5_LPTIM_IN1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 8)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB5_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 5)
#define CHIP_PIN_PB5_COMP2_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 5)
#endif
#endif


/* Пин PB6 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25) || (CHIP_DEV_PKG == CHIP_PKG_UFQFPN20)
#define CHIP_PIN_PB6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 6)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB6_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 1)
#endif
#define CHIP_PIN_PB6_TIM8_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 2)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB6_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 3)
#define CHIP_PIN_PB6_LPUART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 3) /* ??? conflict */
#endif
#define CHIP_PIN_PB6_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 4) /* ~hk */
#define CHIP_PIN_PB6_TIM8_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 5) /* ~hk (tim16) */
#define CHIP_PIN_PB6_I2C1_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 6, 6) /* ~hk */
#define CHIP_PIN_PB6_LPTIM_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 8)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB6_BEEPER2_OUT          CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 11)
#define CHIP_PIN_PB6_COMP2_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 6)
#define CHIP_PIN_PB6_TSC20                CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 6)
#endif
#endif


/* Пин PB7 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 25) || (CHIP_DEV_PKG == CHIP_PKG_UFQFPN20)
#define CHIP_PIN_PB7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 7)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB7_LPUART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 1)
#endif
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PB7_LPUART1_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 2)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB7_LPUART2_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 2)
#endif
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB7_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 3)
#endif
#define CHIP_PIN_PB7_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 4) /* ~hk */
#define CHIP_PIN_PB7_TIM8_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 5) /* ~hk (tim17ch1) */
#define CHIP_PIN_PB7_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 7, 6) /* ~hk */
#define CHIP_PIN_PB7_TIM8_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 7)
#define CHIP_PIN_PB7_LPTIM_IN2            CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 8)
#define CHIP_PIN_PB7_LPUART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 9)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB7_PVD_IN               CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 10)
#define CHIP_PIN_PB7_BEEPER2_N_OUT        CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 11)
#define CHIP_PIN_PB7_COMP2_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 7)
#endif
#endif


/* Пин PB8 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 32) && (CHIP_DEV_PKG != CHIP_PKG_LQFP32)
#define CHIP_PIN_PB8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 8)
#define CHIP_PIN_PB8_TIM8_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 5) /* ~hk (tim16) */
#define CHIP_PIN_PB8_I2C1_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 8, 6) /* ~hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB8_TSC21                CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 8)
#endif
#endif


/* Пин PB9 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 9)
#define CHIP_PIN_PB9_SPI2_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 0) /* ~*hk */
#define CHIP_PIN_PB9_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 3) /* hk */
#define CHIP_PIN_PB9_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 4)
#define CHIP_PIN_PB9_TIM8_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 5) /* ~hk (tim17ch1) */
#define CHIP_PIN_PB9_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 9, 6) /* ~hk */
#endif


/* Пин PB10 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 10)
#define CHIP_PIN_PB10_SPI1_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 0)
#define CHIP_PIN_PB10_I2S1_SD             CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 0)
#ifdef CHIP_DEV_N32G031
#define CHIP_PIN_PB10_TIM3_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 1)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB10_TIM4_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 1)
#endif
#define CHIP_PIN_PB10_SPI2_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 2) /* ~*hk */
#define CHIP_PIN_PB10_LPUART1_TX          CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 4)
#define CHIP_PIN_PB10_I2C1_SCL            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 10, 6) /* ~*hk */
#define CHIP_PIN_PB10_I2C2_SCL            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 10, 7) /* ~*hk */
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PB10_ADC_IN11            CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 10)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PB10_OPAMP_VINP          CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 10)
#endif
#endif


/* Пин PB11 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 11)
#define CHIP_PIN_PB11_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 3) /* ~hk */
#define CHIP_PIN_PB11_LPUART1_RX          CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 4)
#define CHIP_PIN_PB11_TIM8_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 5) /* ~*hk (tim2ch4)*/
#define CHIP_PIN_PB11_I2C1_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 11, 6) /* ~*hk */
#define CHIP_PIN_PB11_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 11, 7) /* ~*hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB11_CAN1_RX             CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 9)
#endif
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB11_TSC11               CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 11)
#endif
#endif


/* Пин PB12 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 12)
#define CHIP_PIN_PB12_SPI1_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0) /* ~*hk */
#define CHIP_PIN_PB12_I2S1_WS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0) /* ~*hk */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB12_USART1_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 1)
#endif
#define CHIP_PIN_PB12_TIM1_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 1) /* ~hk */ /** @todo - conflict */
#define CHIP_PIN_PB12_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 3) /* ~hk */
#define CHIP_PIN_PB12_TIM8_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 5)
#define CHIP_PIN_PB12_SPI2_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 8)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB12_CAN1_TX             CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 9)
#endif
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB12_TSC12               CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 12)
#endif
#endif


/* Пин PB13 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 13)
#define CHIP_PIN_PB13_SPI1_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0) /* ~*hk */
#define CHIP_PIN_PB13_I2S1_CK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0) /* ~*hk */
#define CHIP_PIN_PB13_TIM1_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 1) /* ~hk */
#define CHIP_PIN_PB13_LPUART1_CTS         CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 4)
#define CHIP_PIN_PB13_TIM8_CH2            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 5)
#define CHIP_PIN_PB13_I2C2_SCL            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 13, 7) /* ~*hk */
#define CHIP_PIN_PB13_SPI2_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 8)
#endif


/* Пин PB14 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 14)
#define CHIP_PIN_PB14_SPI1_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0) /* ~*hk */
#define CHIP_PIN_PB14_I2S1_MCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0) /* ~*hk */ /* ?? нет в datasheet */
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB14_LPTIM_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 1)
#endif
#define CHIP_PIN_PB14_TIM1_CH2N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 1) /* ~*hk */
#define CHIP_PIN_PB14_LPUART1_RTS         CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 4)
#define CHIP_PIN_PB14_TIM8_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 5) /* ~*hk (tim15ch1) */
#define CHIP_PIN_PB14_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 14, 7) /* ~*hk */
#define CHIP_PIN_PB14_SPI2_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 8)
#define CHIP_PIN_PB14_OPAMP_VINP          CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 14)
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB14_TSC13               CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 14)
#endif
#endif


/* Пин PB15 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PB15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 15)
#define CHIP_PIN_PB15_SPI1_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0) /* ~*hk */
#define CHIP_PIN_PB15_I2S1_SD             CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0) /* ~*hk */
#define CHIP_PIN_PB15_TIM1_CH3N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 1) /* ~hk */
#define CHIP_PIN_PB15_TIM8_CH3N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 5) /* ~*hk (tim15ch1n)*/
#if defined CHIP_DEV_N32G032
#define CHIP_PIN_PB15_TIM4_CH2            CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 6)
#endif
#define CHIP_PIN_PB15_TIM8_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 7)
#define CHIP_PIN_PB15_SPI2_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 8)
#define CHIP_PIN_PB15_RTC_REFIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 9)
#endif


/* ----------- Порт C ---------------*/
/* Пин PC0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 0)
#define CHIP_PIN_PC0_LPTIM_IN1            CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 0)
#define CHIP_PIN_PC0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 3) /* ~hk */
#define CHIP_PIN_PC0_UART6_TX             CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 4)
#define CHIP_PIN_PC0_ADC_IN10             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 0)    /* hk */
#endif


/* Пин PC1 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 1)
#define CHIP_PIN_PC1_LPTIM_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 0)
#define CHIP_PIN_PC1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 3) /* ~hk */
#define CHIP_PIN_PC1_UART6_RX             CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 4)
#define CHIP_PIN_PC1_BEEPER1_OUT          CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 7)
#define CHIP_PIN_PC1_ADC_IN11             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 1)    /* hk */
#endif

/* Пин PC2 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 2)
#define CHIP_PIN_PC2_LPTIM_IN2            CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 0)
#define CHIP_PIN_PC2_SPI2_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 1) /* *hk */
#define CHIP_PIN_PC2_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 3) /* ~hk */
#define CHIP_PIN_PC2_BEEPER1_N_OUT        CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 7)
#define CHIP_PIN_PC2_ADC_IN12             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 2)    /* hk */
#endif


/* Пин PC3 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 3)
#define CHIP_PIN_PC3_LPTIM_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 0)
#define CHIP_PIN_PC3_SPI2_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 1) /* *hk */
#define CHIP_PIN_PC3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 3) /* ~hk */
#define CHIP_PIN_PC3_ADC_IN13             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 3)    /* hk */
#endif


/* Пин PC4 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 4)
#define CHIP_PIN_PC4_CAN1_TX              CHIP_PIN_ID_AF    (CHIP_PORT_C, 4, 2)
#define CHIP_PIN_PC4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 4, 3) /* ~hk */
#define CHIP_PIN_PC4_LPUART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 4, 4)
#define CHIP_PIN_PC4_ADC_IN14             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 4)    /* hk */
#endif


/* Пин PC5 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 5)
#define CHIP_PIN_PC5_CAN1_RX              CHIP_PIN_ID_AF    (CHIP_PORT_C, 5, 2)
#define CHIP_PIN_PC5_LPUART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 5, 4)
#define CHIP_PIN_PC5_ADC_IN15             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 5)    /* hk */
#define CHIP_PIN_PC5_OPAMP_VINM           CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 5)
#endif


/* Пин PC6 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 6)
#define CHIP_PIN_PC6_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_C, 6, 1) /* ~hk */
#define CHIP_PIN_PC6_COMP3_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 6)
#define CHIP_PIN_PC6_TSC14                CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 6)
#endif


/* Пин PC7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 7)
#define CHIP_PIN_PC7_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_C, 7, 1) /* ~hk */
#define CHIP_PIN_PC7_COMP3_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 7)
#define CHIP_PIN_PC7_TSC15                CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 7)
#endif


/* Пин PC8 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 8)
#define CHIP_PIN_PC8_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_C, 8, 1) /* ~hk */
#define CHIP_PIN_PC8_COMP3_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_C, 8, 4)
#define CHIP_PIN_PC8_TSC16                CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 8)
#endif


/* Пин PC9 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 9)
#define CHIP_PIN_PC9_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_C, 9, 1) /* ~hk */
#define CHIP_PIN_PC9_SPI3_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 6)
#define CHIP_PIN_PC9_TSC17                CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 9)
#endif


/* Пин PC10 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 10)
#define CHIP_PIN_PC10_LPUART1_TX          CHIP_PIN_ID_AF    (CHIP_PORT_C, 10, 4)
#define CHIP_PIN_PC10_LPUART2_TX          CHIP_PIN_ID_AF    (CHIP_PORT_C, 10, 5)
#define CHIP_PIN_PC10_SPI3_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_C, 10, 6)
#endif


/* Пин PC11 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 11)
#define CHIP_PIN_PC11_LPUART1_RX          CHIP_PIN_ID_AF    (CHIP_PORT_C, 11, 4)
#define CHIP_PIN_PC11_LPUART2_RX          CHIP_PIN_ID_AF    (CHIP_PORT_C, 11, 5)
#endif


/* Пин PC12 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 12)
#define CHIP_PIN_PC12_UART5_TX            CHIP_PIN_ID_AF    (CHIP_PORT_C, 12, 6)
#endif


/* Пин PC13, WKUP1 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48
#define CHIP_PIN_PC13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 13)
#define CHIP_PIN_PC13_RTC_TS              CHIP_PIN_ID_AF    (CHIP_PORT_C, 13, 1)
#define CHIP_PIN_PC13_RTC_TAMP1           CHIP_PIN_ID_AF    (CHIP_PORT_C, 13, 2)
#define CHIP_PIN_PC13_RTC_OUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 13, 3)
#endif

/* Пин PC14, OSC32_IN */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48) || (CHIP_DEV_PKG == CHIP_PKG_QFN32_4x4) || (CHIP_DEV_PKG == CHIP_PKG_UFQFPN20) || (CHIP_DEV_PKG == CHIP_PKG_WLCSP25)
#define CHIP_PIN_PC14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 14)
#endif

/* Пин PC15, OSC32_OUT */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48) || (CHIP_DEV_PKG == CHIP_PKG_QFN32_4x4) || (CHIP_DEV_PKG == CHIP_PKG_UFQFPN20) || (CHIP_DEV_PKG == CHIP_PKG_WLCSP25)
#define CHIP_PIN_PC15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 15)
#endif



/* ----------- Порт D ---------------*/
/* Пин PD0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PD0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 0)

#endif


/* Пин PD1 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PD1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 1)

#endif

/* Пин PD2 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PD2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 2)
#define CHIP_PIN_PD2_TIM3_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 0) /* hk */
#define CHIP_PIN_PD2_LPUART1_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 1)
#define CHIP_PIN_PD2_UART5_RX             CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 2)
#endif



/* ----------- Порт F ---------------*/
/* Пин PF0, OSC_IN */
#if (CHIP_DEV_PKG != CHIP_PKG_QFN32_4x4) && (CHIP_DEV_PKG != CHIP_PKG_UFQFPN20) && (CHIP_DEV_PKG != CHIP_PKG_WLCSP25)
#define CHIP_PIN_PF0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 0)
#define CHIP_PIN_PF0_OSC_IN               CHIP_PIN_ID_AF    (CHIP_PORT_F, 0, 0)
#define CHIP_PIN_PF0_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_F, 0, 1) /* *hk */
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PF0_CAN1_TX              CHIP_PIN_ID_AF    (CHIP_PORT_F, 0, 4)
#define CHIP_PIN_PF0_COMP3_OUT            CHIP_PIN_ID_AF_OD (CHIP_PORT_F, 0, 5)
#endif
#define CHIP_PIN_PF0_OPAMP_VINP           CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 0)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PF0_COMP3_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 0)
#define CHIP_PIN_PF0_TSC22                CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 0)
#endif
#endif

/* Пин PF1, OSC_OUT */
#if (CHIP_DEV_PKG != CHIP_PKG_QFN32_4x4) && (CHIP_DEV_PKG != CHIP_PKG_UFQFPN20)
#define CHIP_PIN_PF1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 1)
#define CHIP_PIN_PF1_OSC_OUT              CHIP_PIN_ID_AF    (CHIP_PORT_F, 1, 0)
#define CHIP_PIN_PF1_I2C1_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_F, 1, 1) /* *hk */
#define CHIP_PIN_PF1_USART1_CK            CHIP_PIN_ID_AF    (CHIP_PORT_F, 1, 2)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PF1_CAN1_RX              CHIP_PIN_ID_AF    (CHIP_PORT_F, 0, 4)
#endif
#define CHIP_PIN_PF1_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_F, 1, 5)
#ifdef CHIP_DEV_N32G032
#define CHIP_PIN_PF1_OPAMP_VINM           CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 1)
#define CHIP_PIN_PF1_COMP3_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 1)
#define CHIP_PIN_PF1_TSC23                CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 1)
#endif
#endif



/* Пин PF2, BOOT0 */
#define CHIP_PIN_PF2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 2)


/* Пин PF3 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PF3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 3)
#endif


/* Пин PF4 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PF4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 4)
#define CHIP_PIN_PF4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_F, 4, 3)  /* hk */
#endif


/* Пин PF5 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PF5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 5)
#define CHIP_PIN_PF5_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_F, 5, 3) /* hk */
#endif


/* Пин PF6 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PF6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 6)
#define CHIP_PIN_PF6_I2C2_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_F, 6, 0) /* *hk */
#define CHIP_PIN_PF6_I2C1_SCL             CHIP_PIN_ID_AF_OD (CHIP_PORT_F, 6, 1)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PF6_SPI2_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_F, 6, 4)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PF6_SPI3_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_F, 6, 4)
#define CHIP_PIN_PF6_COMP1_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 6)
#define CHIP_PIN_PF6_COMP3_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 6)
#endif
#endif


/* Пин PF7 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 48)
#define CHIP_PIN_PF7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 7)
#define CHIP_PIN_PF7_I2C2_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_F, 7, 0) /* *hk */
#define CHIP_PIN_PF7_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_F, 7, 1)
#if defined CHIP_DEV_N32G031
#define CHIP_PIN_PF7_SPI2_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_F, 7, 4)
#elif defined CHIP_DEV_N32G032
#define CHIP_PIN_PF7_SPI3_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_F, 7, 4)
#define CHIP_PIN_PF7_COMP1_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 7)
#define CHIP_PIN_PF7_COMP3_INM            CHIP_PIN_ID_ANALOG(CHIP_PORT_F, 7)
#endif
#endif

#endif // CHIP_PINS_N32G03X_H
