#include "chip.h"

static t_chip_isr_func f_isr[CHIP_IRQNUM_MAX+1];


#define CHIP_GIC_IRQ_GROUP0_EN              1
#define CHIP_GIC_IRQ_GROUP1NS_EN            1
#define CHIP_GIC_IRQ_GROUP1S_EN             1
#define CHIP_GIC_DEFAULT_PRIORITY_MASK      0xFF

extern const char gic_vector_table_el3[];

void gic_isr_dummy(const t_chip_isr_ctx *ctx) {
    for (;;) {}
}

void gic_isr_unreg_int(const t_chip_isr_ctx *ctx) __attribute__ ((__weak__, __alias__("gic_isr_dummy")));
void gic_isr_sync_exc(const t_chip_isr_ctx *ctx) __attribute__ ((__weak__, __alias__("gic_isr_dummy")));
void gic_isr_serror(const t_chip_isr_ctx *ctx) __attribute__ ((__weak__, __alias__("gic_isr_dummy")));






void chip_gic_init(void) {
    /* Установка адреса таблицы векторо прерываний */
    chip_sysreg_wr("VBAR_EL3", &gic_vector_table_el3);

    /* 4.1 Global settings */
    /* Переход в GICv3 (ARE=1) и разрешение групп прерываний */
    CHIP_REGS_GICD->CTLR =  0
                           | LBITFIELD_SET(CHIP_REGFLD_GICD_CTLR_ARE_NS, 1)
                           | LBITFIELD_SET(CHIP_REGFLD_GICD_CTLR_ARE_S, 1)
                           | LBITFIELD_SET(CHIP_REGFLD_GICD_CTLR_EN_GRP0, CHIP_GIC_IRQ_GROUP0_EN)
                           | LBITFIELD_SET(CHIP_REGFLD_GICD_CTLR_EN_GRP1_NS, CHIP_GIC_IRQ_GROUP1NS_EN)
                           | LBITFIELD_SET(CHIP_REGFLD_GICD_CTLR_EN_GRP1_S, CHIP_GIC_IRQ_GROUP1S_EN)
        ;

    /* Ожидание заверешния записи */
    while ((CHIP_REGS_GICD->CTLR & CHIP_REGFLD_GICD_CTLR_RWP) != 0) {
        continue;
    };

    /* 4.2 Individual PE settings */
    /* 4.2.1 Redistributor configuration */
    /* Пробуждение PE интерфейса (до этого в другие регистры нельзя обращаться */
    CHIP_REGS_GICR->WAKER &= ~CHIP_REGFLD_GICR_WAKER_PROCESSOR_SLEEP;
    while (CHIP_REGS_GICR->WAKER & CHIP_REGFLD_GICR_WAKER_CHILDREN_ASLEEP) {
        continue;
    }

    /* 4.2.2 CPU interface configuration */

    /* Разрешение работы с ICC через system registers в EL3 и EL1*/
    chip_sysreg_wr("ICC_SRE_EL3", CHIP_REGFLD_GIC_ICC_SRE_SRE);
    chip_sysreg_wr("ICC_SRE_EL1", CHIP_REGFLD_GIC_ICC_SRE_SRE);

    /* Маска приоритетов, с которой разрешено прерывание */
    chip_interrupts_set_priority_mask(CHIP_GIC_DEFAULT_PRIORITY_MASK);

    /** @todo
     * Возможно настройка на разбиения прироритета на группу и подгруппу (группа может вытеснить, подгруппа - нет)
     * пока preemtion не реализован в любом случае.
     * Регистры ICC_BPR0/ICC_BPR1. */


    /* Разрешение прерываний для группы 0 и 1S/NS */
    chip_sysreg_wr("ICC_IGRPEN0_EL1", LBITFIELD_SET(CHIP_REGFLD_GIC_ICC_IGRPEN_ENABLE, CHIP_GIC_IRQ_GROUP0_EN));
    chip_sysreg_wr("ICC_IGRPEN1_EL3", LBITFIELD_SET(CHIP_REGFLD_GIC_ICC_IGRPEN1_ENABLE_S, CHIP_GIC_IRQ_GROUP1S_EN)
                                          | LBITFIELD_SET(CHIP_REGFLD_GIC_ICC_IGRPEN1_ENABLE_NS, CHIP_GIC_IRQ_GROUP1NS_EN));


    /* 4.2.3 PE configuration */
    /* разрешение FIQ */
    chip_sysreg_wr("SCR_EL3", CHIP_REGMSK_AARCH_SCR_RES1
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_SCR_NS, 0) /* EL0/1 is secure (0) or not (1) */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_SCR_IRQ, 1) /* Разерешение IRQ на EL3 */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_SCR_FIQ, 1) /* Разерешение FIQ на EL3 */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_SCR_EA, 1)  /* Разрешение External Abort and SError на EL3 */
                                  | LBITFIELD_SET(CHIP_REGFLD_AARCH_SCR_RW, 0)  /* Next lower level is AArch64 */
                   );
}



void chip_interrupts_set_priority_mask(uint8_t prio) {
    chip_sysreg_wr("ICC_PMR_EL1", prio);
}






void chip_interrupt_set_isr(int irq_num, t_chip_isr_func isr) {
    if ((irq_num > 0) && (irq_num <= CHIP_IRQNUM_MAX)) {
        f_isr[irq_num] = isr;
    }
}

void chip_interrupt_set_trig_mode(int irq_num, t_chip_irq_trigmode mode) {
    if (irq_num >= CHIP_GIC_IRQNUM_SPI_MIN) {
        LBITFIELD_UPD(CHIP_REGS_GICD->ICFGR[CHIP_GIC_INT_REGNUM_2B(irq_num)], CHIP_GIC_INT_REGFLD_2B(irq_num), mode);
    } else {
        LBITFIELD_UPD(CHIP_REGS_GICR->ICFGR[CHIP_GIC_INT_REGNUM_2B(irq_num)], CHIP_GIC_INT_REGFLD_2B(irq_num), mode);
    }
}

void chip_interrupt_set_priority(int irq_num, uint8_t prio) {
    if (irq_num >= CHIP_GIC_IRQNUM_SPI_MIN) {
        LBITFIELD_UPD(CHIP_REGS_GICD->IPRIORITYR[CHIP_GIC_INT_REGNUM_8B(irq_num)], CHIP_GIC_INT_REGFLD_8B(irq_num), prio);
    } else {
        LBITFIELD_UPD(CHIP_REGS_GICR->IPRIORITYR[CHIP_GIC_INT_REGNUM_8B(irq_num)], CHIP_GIC_INT_REGFLD_8B(irq_num), prio);
    }
}

void chip_interrupt_set_group(int irq_num, t_chip_irq_group group) {
    if (irq_num >= CHIP_GIC_IRQNUM_SPI_MIN) {
        LBITFIELD_UPD(CHIP_REGS_GICD->IGRPMODR[CHIP_GIC_INT_REGNUM(irq_num)], CHIP_GIC_INT_REGFLD(irq_num), (group & 1));
        LBITFIELD_UPD(CHIP_REGS_GICD->IGROUPR[CHIP_GIC_INT_REGNUM(irq_num)], CHIP_GIC_INT_REGFLD(irq_num), ((group >> 1) & 1));
    } else {
        LBITFIELD_UPD(CHIP_REGS_GICR->IGRPMODR[CHIP_GIC_INT_REGNUM(irq_num)], CHIP_GIC_INT_REGFLD(irq_num), (group & 1));
        LBITFIELD_UPD(CHIP_REGS_GICR->IGROUPR[CHIP_GIC_INT_REGNUM(irq_num)], CHIP_GIC_INT_REGFLD(irq_num), ((group >> 1) & 1));
    }
}


static void f_proc_int_from_tbl(t_chip_isr_ctx *ctx) {
    uint32_t irqnum;
    chip_sysreg_rd("ICC_IAR0_EL1", irqnum);
    if (irqnum != CHIP_GIC_IRQNUM_SPEC_NO_PEND) {
        ctx->irq_num = irqnum;
        if ((irqnum <= CHIP_IRQNUM_MAX) && f_isr[irqnum]) {
            f_isr[irqnum](ctx);
        } else {
            gic_isr_unreg_int(ctx);
        }
        chip_sysreg_wr("ICC_EOIR0_EL1", irqnum);
    }
}


void gic_isr_curr_el3_sp0_sync(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SP0_SYNC, .chip_regs = chip_regs};
    gic_isr_sync_exc(&ctx);
}

void gic_isr_curr_el3_sp0_irq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SP0_IRQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_curr_el3_sp0_fiq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SP0_FIQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_curr_el3_sp0_serror(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SP0_SERROR, .chip_regs = chip_regs};
    gic_isr_serror(&ctx);
}


void gic_isr_curr_el3_spx_sync(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SPX_SYNC, .chip_regs = chip_regs};
    gic_isr_sync_exc(&ctx);
}

void gic_isr_curr_el3_spx_irq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SPX_IRQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_curr_el3_spx_fiq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SPX_FIQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_curr_el3_spx_serror(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_CUREL_SPX_SERROR, .chip_regs = chip_regs};
    gic_isr_serror(&ctx);
}


void gic_isr_lower_el3_aarch64_sync(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH64_SYNC, .chip_regs = chip_regs};
    gic_isr_sync_exc(&ctx);
}

void gic_isr_lower_el3_aarch64_irq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH64_IRQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_lower_el3_aarch64_fiq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH64_FIQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_lower_el3_aarch64_serror(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH64_SERROR, .chip_regs = chip_regs};
    gic_isr_serror(&ctx);
}


void gic_isr_lower_el3_aarch32_sync(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH32_SYNC, .chip_regs = chip_regs};
    gic_isr_sync_exc(&ctx);
}

void gic_isr_lower_el3_aarch32_irq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH32_IRQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_lower_el3_aarch32_fiq(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH32_FIQ, .chip_regs = chip_regs};
    f_proc_int_from_tbl(&ctx);
}

void gic_isr_lower_el3_aarch32_serror(t_chip_isr_regs *chip_regs) {
    t_chip_isr_ctx ctx = {.vtype = CHIP_GIC_VTYPE_LOWEL_AARCH64_SERROR, .chip_regs = chip_regs};
    gic_isr_serror(&ctx);
}

