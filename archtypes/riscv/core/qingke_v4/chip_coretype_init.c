#include "chip.h"

extern const void *chip_isr_table;

void chip_coretype_init(void) {
    chip_csr_write(CHIP_CSR_INTSYSCR,
                   LBITFIELD_SET(CHIP_REGFLD_CSR_INTSYSCR_HWSTK_EN, 1)
                       | LBITFIELD_SET(CHIP_REGFLD_CSR_INTSYSCR_INEST_EN, 1));
    chip_csr_write(CHIP_CSR_MTVEC,
                   (intptr_t)&chip_isr_table |
                    LBITFIELD_SET(CHIP_REGFLD_CSR_MTVEC_MODE,
                                    CHIP_REGFLDVAL_CSR_MTVEC_MODE_VECTORED
                                    | CHIP_REGFLDVAL_CSR_MTVEC_MODE1_ABS_ADDR));
}
