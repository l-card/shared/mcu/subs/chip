#include "chip.h"
#if CHIP_CFG_EXMC_SETUP_EN



/* определение общих параметров EXMC. Сейчас на основе SDRAM (в будущем на основе настроек всех блоков EXMC) */
#define CHIP_EXMC_DATA_WIDTH    CHIP_SDRAM_DATA_WIDTH
#define CHIP_EXMC_ADDR_WIDTH    CHIP_SDRAM_ADDR_WIDTH
#define CHIP_EXMC_BA_WIDTH      CHIP_SDRAM_BA_WIDTH


#if CHIP_SDRAM_EN
static void f_sdram_init(void) {
    CHIP_PIN_CONFIG(CHIP_PIN_PG8_EXMC_SDCLK);
    CHIP_PIN_CONFIG(CHIP_PIN_PF11_EXMC_SDNRAS);
    CHIP_PIN_CONFIG(CHIP_PIN_PG15_EXMC_SDNCAS);
    CHIP_PIN_CONFIG(CHIP_CFG_SDRAM_PIN_WE);
#if CHIP_SDRAM_DATA_WIDTH > 8
    CHIP_PIN_CONFIG(CHIP_PIN_PE0_EXMC_NBL0);
    CHIP_PIN_CONFIG(CHIP_PIN_PE1_EXMC_NBL1);
#endif
#if CHIP_SDRAM_DATA_WIDTH > 16
    CHIP_PIN_CONFIG(CHIP_PIN_PI4_EXMC_NBL2);
    CHIP_PIN_CONFIG(CHIP_PIN_PI5_EXMC_NBL3);
#endif
#if CHIP_SDRAM1_EN
    CHIP_PIN_CONFIG(CHIP_CFG_SDRAM1_PIN_CKE);
    CHIP_PIN_CONFIG(CHIP_CFG_SDRAM1_PIN_CS);
#endif
#if CHIP_SDRAM2_EN
    CHIP_PIN_CONFIG(CHIP_CFG_SDRAM2_PIN_CKE);
    CHIP_PIN_CONFIG(CHIP_CFG_SDRAM2_PIN_CS);
#endif

    /* 1. Program the memory device features into the EXMC_SDCR register. The SDRAM
        clock frequency, RBURST and RPIPE must be programmed in the EXMC_SDCR1
        register. */

    /* поля RPIPE, RBURST и SDCLK настраиваются общие в SDCR1, даже если
     * используется только SDRAM2.
     * Остальные поля заполняются только при разрешенной SDRAM1 */
    CHIP_REGS_EXMC->SDCR[0] = (CHIP_REGS_EXMC->SDCR[0] & ~(CHIP_REGFLD_EXMC_SDCR_RPIPE
                                                         | CHIP_REGFLD_EXMC_SDCR_RBURST
                                                         | CHIP_REGFLD_EXMC_SDCR_SDCLK
#if CHIP_SDRAM1_EN
                                                         | CHIP_REGFLD_EXMC_SDCR_WP
                                                         | CHIP_REGFLD_EXMC_SDCR_CAS
                                                         | CHIP_REGFLD_EXMC_SDCR_NB
                                                         | CHIP_REGFLD_EXMC_SDCR_MWID
                                                         | CHIP_REGFLD_EXMC_SDCR_NR
                                                         | CHIP_REGFLD_EXMC_SDCR_NC

#endif
                                                         ))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_RPIPE,  0)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_RBURST, 1)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_SDCLK,  CHIP_RVAL_SDCR_SDCLK)
#if CHIP_SDRAM1_EN
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_WP, 0)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_CAS,  CHIP_RVAL_SDCR1_CAS)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_NB,   CHIP_RVAL_SDCR1_NB)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_MWID, CHIP_RVAL_SDCR1_MWID)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_NR,   CHIP_RVAL_SDCR1_NR)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_NC,   CHIP_RVAL_SDCR1_NC)
#endif
        ;
#if CHIP_SDRAM2_EN
    CHIP_REGS_EXMC->SDCR[1] = (CHIP_REGS_EXMC->SDCR[1] & ~(CHIP_REGFLD_EXMC_SDCR_WP
                                                         | CHIP_REGFLD_EXMC_SDCR_CAS
                                                         | CHIP_REGFLD_EXMC_SDCR_NB
                                                         | CHIP_REGFLD_EXMC_SDCR_MWID
                                                         | CHIP_REGFLD_EXMC_SDCR_NR
                                                         | CHIP_REGFLD_EXMC_SDCR_NC
                                                         ))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_WP,   0)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_CAS,  CHIP_RVAL_SDCR2_CAS)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_NB,   CHIP_RVAL_SDCR2_NB)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_MWID, CHIP_RVAL_SDCR2_MWID)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_NR,   CHIP_RVAL_SDCR2_NR)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCR_NC,   CHIP_RVAL_SDCR2_NC)
            ;
#endif

    /* 2. Program the memory device timing into the EXMC_SDTRx register. The TRP and TRC
        timings must be programmed in the EXMC_SDTR1 register. */
    CHIP_REGS_EXMC->SDTR[0] = (CHIP_REGS_EXMC->SDTR[0] & ~(CHIP_REGFLD_EXMC_SDTR_TRP
                                                         | CHIP_REGFLD_EXMC_SDTR_TRC
#if CHIP_SDRAM1_EN
                                                         | CHIP_REGFLD_EXMC_SDTR_TRCD
                                                         | CHIP_REGFLD_EXMC_SDTR_TWR
                                                         | CHIP_REGFLD_EXMC_SDTR_TRAS
                                                         | CHIP_REGFLD_EXMC_SDTR_TXSR
                                                         | CHIP_REGFLD_EXMC_SDTR_TMRD
#endif
                                                         ))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TRP,   CHIP_RVAL_SDTR_TRP)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TRC,   CHIP_RVAL_SDTR_TRC)
#if CHIP_SDRAM1_EN
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TRCD,  CHIP_RVAL_SDTR1_TRCD)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TWR,   CHIP_RVAL_SDTR_TWR)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TRAS,  CHIP_RVAL_SDTR1_TRAS)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TXSR,  CHIP_RVAL_SDTR_TXSR)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TMRD,  CHIP_RVAL_SDTR1_TMRD)
#endif
            ;
#if CHIP_SDRAM2_EN
    CHIP_REGS_EXMC->SDTR[1] = (CHIP_REGS_EXMC->SDTR[1] & ~(CHIP_REGFLD_EXMC_SDTR_TRCD
                                                         | CHIP_REGFLD_EXMC_SDTR_TWR
                                                         | CHIP_REGFLD_EXMC_SDTR_TRAS
                                                         | CHIP_REGFLD_EXMC_SDTR_TXSR
                                                         | CHIP_REGFLD_EXMC_SDTR_TMRD
                                                         ))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TRCD,  CHIP_RVAL_SDTR2_TRCD)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TWR,   CHIP_RVAL_SDTR_TWR)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TRAS,  CHIP_RVAL_SDTR2_TRAS)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TXSR,  CHIP_RVAL_SDTR_TXSR)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDTR_TMRD,  CHIP_RVAL_SDTR2_TMRD)
            ;
#endif
    /* 3. Set MODE bits to ‘001’ and configure the Target Bank bits (CTB1 and/or CTB2) in the
          EXMC_SDCMR register to start delivering the clock to the memory (SDCKE is driven
          high). */
    CHIP_REGS_EXMC->SDCMR = (CHIP_REGS_EXMC->SDCMR & ~(CHIP_REGFLD_EXMC_SDCMR_CTB1
                                                     | CHIP_REGFLD_EXMC_SDCMR_CTB2
                                                     | CHIP_REGFLD_EXMC_SDCMR_MODE))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MODE, CHIP_REGFLDVAL_EXMC_SDCMR_MODE_CLK_CFG_EN)
            | CHIP_RVAL_SDCMR_CTB;

    /* 4. Wait during the prescribed delay period. Typical delay is around 100 μs (refer to the
          SDRAM datasheet for the required delay after power-up). */
    chip_wdt_reset();
    for (int i = 0; i < CHIP_SDRAM_POWERUP_TIME_US; ++i) {
        chip_delay_clk((CHIP_CLK_CPU_FREQ + 999999)/1000000);
        chip_wdt_reset();
    }
    /* 5. Set MODE bits to ‘010’ and configure the Target Bank bits (CTB1 and/or CTB2) in the
          EXMC_SDCMR register to issue a “Precharge All” command. */
    CHIP_REGS_EXMC->SDCMR = (CHIP_REGS_EXMC->SDCMR & ~(CHIP_REGFLD_EXMC_SDCMR_CTB1
                                                     | CHIP_REGFLD_EXMC_SDCMR_CTB2
                                                     | CHIP_REGFLD_EXMC_SDCMR_MODE))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MODE, CHIP_REGFLDVAL_EXMC_SDCMR_MODE_PALL)
            | CHIP_RVAL_SDCMR_CTB;
    /* 6. Set MODE bits to ‘011’, and configure the Target Bank bits (CTB1 and/or CTB2) as well
          as the number of consecutive Auto-refresh commands (NRFS) in the EXMC_SDCMR
          register. Refer to the SDRAM datasheet for the number of Auto-refresh commands that
          should be issued. Typical number is 8. */
    CHIP_REGS_EXMC->SDCMR = (CHIP_REGS_EXMC->SDCMR & ~(CHIP_REGFLD_EXMC_SDCMR_CTB1
                                                     | CHIP_REGFLD_EXMC_SDCMR_CTB2
                                                     | CHIP_REGFLD_EXMC_SDCMR_MODE
                                                     | CHIP_REGFLD_EXMC_SDCMR_NRFS))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MODE, CHIP_REGFLDVAL_EXMC_SDCMR_MODE_AUTO_REF)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_NRFS, CHIP_RVAL_SDCMR_NRFS)
            | CHIP_RVAL_SDCMR_CTB;

    /* 7. Configure the MRD field, set the MODE bits to ‘100’, and configure the Target Bank bits
          (CTB1 and/or CTB2) in the EXMC_SDCMR register to issue a “Load Mode Register”
          command and program the SDRAM device. In particular the Burst Length (BL) has to
          be set to ‘1’) and the CAS latency has to be selected. If the Mode Register is not the
          same for both SDRAM banks, this step has to be repeated twice, once for each bank
          and the Target Bank bits set accordingly. For mobile SDRAM devices, the MRD field is
          also used to configure the extended mode register while issuing the Load Mode
          Register” */
#ifdef CHIP_RVAL_SDRAM_MODE_REG
    CHIP_REGS_EXMC->SDCMR = (CHIP_REGS_EXMC->SDCMR & ~(CHIP_REGFLD_EXMC_SDCMR_CTB1
                                                     | CHIP_REGFLD_EXMC_SDCMR_CTB2
                                                     | CHIP_REGFLD_EXMC_SDCMR_MODE
                                                     | CHIP_REGFLD_EXMC_SDCMR_MRD))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MODE, CHIP_REGFLDVAL_EXMC_SDCMR_MODE_LOAD_MODE)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MRD,  CHIP_RVAL_SDRAM_MODE_REG)
            | CHIP_RVAL_SDCMR_CTB;
#else
    CHIP_REGS_EXMC->SDCMR = (CHIP_REGS_EXMC->SDCMR & ~(CHIP_REGFLD_EXMC_SDCMR_CTB1
                                                     | CHIP_REGFLD_EXMC_SDCMR_CTB2
                                                     | CHIP_REGFLD_EXMC_SDCMR_MODE
                                                     | CHIP_REGFLD_EXMC_SDCMR_MRD))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MODE, CHIP_REGFLDVAL_EXMC_SDCMR_MODE_LOAD_MODE)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MRD,  CHIP_RVAL_SDRAM1_MODE_REG)
            | CHIP_REGFLD_EXMC_SDCMR_CTB1;
    CHIP_REGS_EXMC->SDCMR = (CHIP_REGS_EXMC->SDCMR & ~(CHIP_REGFLD_EXMC_SDCMR_CTB1
                                                     | CHIP_REGFLD_EXMC_SDCMR_CTB2
                                                     | CHIP_REGFLD_EXMC_SDCMR_MODE
                                                     | CHIP_REGFLD_EXMC_SDCMR_MRD))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MODE, CHIP_REGFLDVAL_EXMC_SDCMR_MODE_LOAD_MODE)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDCMR_MRD,  CHIP_RVAL_SDRAM2_MODE_REG)
            | CHIP_REGFLD_EXMC_SDCMR_CTB2;
#endif

    /* 8. Program the refresh rate in the EXMC_SDRTR register
          The refresh rate corresponds to the delay between refresh cycles. Its value must be
          adapted to SDRAM devices. */
    CHIP_REGS_EXMC->SDRTR = (CHIP_REGS_EXMC->SDRTR & ~(CHIP_REGFLD_EXMC_SDRTR_COUNT))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_SDRTR_COUNT, CHIP_RVAL_SDRTR_COUNT);
}
#endif



void chip_exmc_init(void) {
    chip_per_rst(CHIP_PER_ID_EXMC);

    CHIP_REGS_EXMC->BTCR[0] = (CHIP_REGS_EXMC->BTCR[0] & !~(CHIP_REGFLD_EXMC_BCR1_EN
                                                          | CHIP_REGFLD_EXMC_BCR1_BMAP
                                                          ))
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_BCR1_EN, 0)
            | LBITFIELD_SET(CHIP_REGFLD_EXMC_BCR1_BMAP,  CHIP_EXMC_BANK_MAP)
            ;

    CHIP_PIN_CONFIG(CHIP_PIN_PD14_EXMC_D0);
    CHIP_PIN_CONFIG(CHIP_PIN_PD15_EXMC_D1);
    CHIP_PIN_CONFIG(CHIP_PIN_PD0_EXMC_D2);
    CHIP_PIN_CONFIG(CHIP_PIN_PD1_EXMC_D3);
    CHIP_PIN_CONFIG(CHIP_PIN_PE7_EXMC_D4);
    CHIP_PIN_CONFIG(CHIP_PIN_PE8_EXMC_D5);
    CHIP_PIN_CONFIG(CHIP_PIN_PE9_EXMC_D6);
    CHIP_PIN_CONFIG(CHIP_PIN_PE10_EXMC_D7);
#if CHIP_EXMC_DATA_WIDTH > 8
    CHIP_PIN_CONFIG(CHIP_PIN_PE11_EXMC_D8);
    CHIP_PIN_CONFIG(CHIP_PIN_PE12_EXMC_D9);
    CHIP_PIN_CONFIG(CHIP_PIN_PE13_EXMC_D10);
    CHIP_PIN_CONFIG(CHIP_PIN_PE14_EXMC_D11);
    CHIP_PIN_CONFIG(CHIP_PIN_PE15_EXMC_D12);
    CHIP_PIN_CONFIG(CHIP_PIN_PD8_EXMC_D13);
    CHIP_PIN_CONFIG(CHIP_PIN_PD9_EXMC_D14);
    CHIP_PIN_CONFIG(CHIP_PIN_PD10_EXMC_D15);
#endif
#if CHIP_EXMC_DATA_WIDTH > 16
    CHIP_PIN_CONFIG(CHIP_PIN_PH8_EXMC_D16);
    CHIP_PIN_CONFIG(CHIP_PIN_PH9_EXMC_D17);
    CHIP_PIN_CONFIG(CHIP_PIN_PH10_EXMC_D18);
    CHIP_PIN_CONFIG(CHIP_PIN_PH11_EXMC_D19);
    CHIP_PIN_CONFIG(CHIP_PIN_PH12_EXMC_D20);
    CHIP_PIN_CONFIG(CHIP_PIN_PH13_EXMC_D21);
    CHIP_PIN_CONFIG(CHIP_PIN_PH14_EXMC_D22);
    CHIP_PIN_CONFIG(CHIP_PIN_PH15_EXMC_D23);
    CHIP_PIN_CONFIG(CHIP_PIN_PI0_EXMC_D24);
    CHIP_PIN_CONFIG(CHIP_PIN_PI1_EXMC_D25);
    CHIP_PIN_CONFIG(CHIP_PIN_PI2_EXMC_D26);
    CHIP_PIN_CONFIG(CHIP_PIN_PI3_EXMC_D27);
    CHIP_PIN_CONFIG(CHIP_PIN_PI6_EXMC_D28);
    CHIP_PIN_CONFIG(CHIP_PIN_PI7_EXMC_D29);
    CHIP_PIN_CONFIG(CHIP_PIN_PI9_EXMC_D30);
    CHIP_PIN_CONFIG(CHIP_PIN_PI10_EXMC_D31);
#endif

#if CHIP_EXMC_ADDR_WIDTH > 0
    CHIP_PIN_CONFIG(CHIP_PIN_PF0_EXMC_A0);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 1
    CHIP_PIN_CONFIG(CHIP_PIN_PF1_EXMC_A1);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 2
    CHIP_PIN_CONFIG(CHIP_PIN_PF2_EXMC_A2);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 3
    CHIP_PIN_CONFIG(CHIP_PIN_PF3_EXMC_A3);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 4
    CHIP_PIN_CONFIG(CHIP_PIN_PF4_EXMC_A4);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 5
    CHIP_PIN_CONFIG(CHIP_PIN_PF5_EXMC_A5);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 6
    CHIP_PIN_CONFIG(CHIP_PIN_PF12_EXMC_A6);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 7
    CHIP_PIN_CONFIG(CHIP_PIN_PF13_EXMC_A7);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 8
    CHIP_PIN_CONFIG(CHIP_PIN_PF14_EXMC_A8);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 9
    CHIP_PIN_CONFIG(CHIP_PIN_PF15_EXMC_A9);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 10
    CHIP_PIN_CONFIG(CHIP_PIN_PG0_EXMC_A10);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 11
    CHIP_PIN_CONFIG(CHIP_PIN_PG1_EXMC_A11);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 12
    CHIP_PIN_CONFIG(CHIP_PIN_PG2_EXMC_A12);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 13
    CHIP_PIN_CONFIG(CHIP_PIN_PG3_EXMC_A13);
#endif
#if (CHIP_EXMC_ADDR_WIDTH > 14) || (CHIP_EXMC_BA_WIDTH > 0)
    CHIP_PIN_CONFIG(CHIP_PIN_PG4_EXMC_BA0);
#endif
#if (CHIP_EXMC_ADDR_WIDTH > 15) || (CHIP_EXMC_BA_WIDTH > 1)
    CHIP_PIN_CONFIG(CHIP_PIN_PG5_EXMC_BA1);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 16
    CHIP_PIN_CONFIG(CHIP_PIN_PD11_EXMC_A16);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 17
    CHIP_PIN_CONFIG(CHIP_PIN_PD12_EXMC_A17);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 18
    CHIP_PIN_CONFIG(CHIP_PIN_PD13_EXMC_A18);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 19
    CHIP_PIN_CONFIG(CHIP_PIN_PE3_EXMC_A19);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 20
    CHIP_PIN_CONFIG(CHIP_PIN_PE4_EXMC_A20);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 21
    CHIP_PIN_CONFIG(CHIP_PIN_PE5_EXMC_A21);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 22
    CHIP_PIN_CONFIG(CHIP_PIN_PE6_EXMC_A22);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 23
    CHIP_PIN_CONFIG(CHIP_PIN_PE2_EXMC_A23);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 24
    CHIP_PIN_CONFIG(CHIP_PIN_PG13_EXMC_A24);
#endif
#if CHIP_EXMC_ADDR_WIDTH > 25
    CHIP_PIN_CONFIG(CHIP_PIN_PG14_EXMC_A25);
#endif

#if CHIP_SDRAM_EN
    f_sdram_init();
#endif

    CHIP_REGS_EXMC->BTCR[0] |= LBITFIELD_SET(CHIP_REGFLD_EXMC_BCR1_EN,1);
}

#endif
