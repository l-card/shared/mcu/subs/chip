#ifndef CHIP_AARCH64_GTIM_H
#define CHIP_AARCH64_GTIM_H

#include "chip_arm.h"
#include "lcspec.h"
#include <stdint.h>
#include "chip_arm_feat_defs.h"
#include "lbitfield.h"

/* CNTHCTL (EL2), Counter-timer Hypervisor Control register */
#define CHIP_REGFLD_CNTHCTL_EL0PCTEN            (0x00000001UL <<  0U) /* (RW) (HCR_EL2.TGE == 1) traps EL0 accesses to the frequency register and physical counter register to EL2 */
#define CHIP_REGFLD_CNTHCTL_EL0VCTEN            (0x00000001UL <<  1U) /* (RW) (HCR_EL2.TGE == 1) traps EL0 accesses to the frequency register and virtual counter register to EL2 */
#define CHIP_REGFLD_CNTHCTL_EVNTEN              (0x00000001UL <<  2U) /* (RW) Enables the generation of an event stream from CNTPCT_EL0 */
#define CHIP_REGFLD_CNTHCTL_EVNTDIR             (0x00000001UL <<  3U) /* (RW) (CNTHCTL_EL2.EVNTEN == 1) which transition of the CNTPCT_EL0 trigger bit generates an event (0: 0->1, 1: 1->0) */
#define CHIP_REGFLD_CNTHCTL_EVNTI               (0x0000000FUL <<  4U) /* (RW) (CNTHCTL_EL2.EVNTEN == 1) which bit of CNTPCT_EL0 is the trigger for the event stream generated */
#if CHIP_AARCH_FEAT_VHE
#define CHIP_REGFLD_CNTHCTL_EL0VTEN             (0x00000001UL <<  8U) /* (RW) (HCR_EL2.E2 == 1, HCR_EL2.TGE == 1) traps EL0 accesses to the virtual timer registers to EL2 */
#define CHIP_REGFLD_CNTHCTL_EL0PTEN             (0x00000001UL <<  9U) /* (RW) (HCR_EL2.E2 == 1, HCR_EL2.TGE == 1) traps EL0 accesses to the physical timer registers to EL2 */
#define CHIP_REGFLD_CNTHCTL_EL1PCTEN            (0x00000001UL << 10U) /* (RW) (HCR_EL2.E2 == 1, HCR_EL2.TGE == 0) traps EL0 and EL1 accesses to the EL1 physical counter register to EL2 */
#define CHIP_REGFLD_CNTHCTL_EL1PTEN             (0x00000001UL << 11U) /* (RW) (HCR_EL2.E2 == 1, HCR_EL2.TGE == 0) traps EL0 and EL1 accesses to the E1 physical timer registers to EL2 */
#endif
#if CHIP_AARCH_FEAT_ECV
#define CHIP_REGFLD_CNTHCTL_ECV                 (0x00000001UL << 12U) /* (RW) Enables the Enhanced Counter Virtualization functionality registers. */
#define CHIP_REGFLD_CNTHCTL_EL1TVT              (0x00000001UL << 13U) /* (RW) Traps EL0 and EL1 accesses to the EL1 virtual timer registers to EL2 */
#define CHIP_REGFLD_CNTHCTL_EL1TVCT             (0x00000001UL << 14U) /* (RW) Traps EL0 and EL1 accesses to the EL1 virtual counter registers to EL2 */
#define CHIP_REGFLD_CNTHCTL_EL1NVPCT            (0x00000001UL << 15U) /* (RW) Traps EL1 accesses to the specified EL1 physical timer registers to EL2 */
#define CHIP_REGFLD_CNTHCTL_EL1NVVCT            (0x00000001UL << 16U) /* (RW) Traps EL1 accesses to the specified EL1 virtual timer registers to EL2 */
#define CHIP_REGFLD_CNTHCTL_EVNTIS              (0x00000001UL << 17U) /* (RW) CNTHCTL_EL2.EVNTI field applies to CNTPCT_EL0[23:8] instead CNTPCT_EL0[15:0]. */
#endif
#if CHIP_AARCH_FEAT_RME
#define CHIP_REGFLD_CNTHCTL_CNTVMASK            (0x00000001UL << 18U) /* (RW) set CNTV_CTL_EL0.IMASK  */
#define CHIP_REGFLD_CNTHCTL_CNTPMASK            (0x00000001UL << 19U) /* (RW) set CNTP_CTL_EL0.IMASK  */
#endif



/* CNTPS_CTL (EL1), Counter-timer Physical Secure Timer Control register */
#define CHIP_REGFLD_CNTPS_CTL_ENABLE            (0x00000001UL << 0) /* (RW) Enables the timer signal */
#define CHIP_REGFLD_CNTPS_CTL_IMASK             (0x00000001UL << 1) /* (RW) Timer interrupt mask bit */
#define CHIP_REGFLD_CNTPS_CTL_ISTATUS           (0x00000001UL << 2) /* (RO) The status of the timer (whether the timer condition is met) */


static LINLINE uint64_t chip_gtim_freq(void) {
    uint64_t cnt;
    chip_mrs("CNTFRQ_EL0", cnt);
    return cnt;
}

static LINLINE void chip_gtim_set_freq(uint64_t freq) {
    chip_msr("CNTFRQ_EL0", freq);
}


static LINLINE uint64_t chip_gtim_cntr(void) {
    uint64_t cnt;
    chip_isb();
    chip_mrs("CNTPCT_EL0", cnt);
    /** @note в Uboot есть обход SYS_FSL_ERRATUM_A008585 и SUNXI_A64_TIMER_ERRATUM - посмотреть, применеимы ли */
    return cnt;
}

static LINLINE void chip_gtim_start(void) {
    uint64_t rval;
    chip_mrs("CNTPS_CTL_EL1", rval);

    rval |= LBITFIELD_SET(CHIP_REGFLD_CNTPS_CTL_ENABLE, 1) |
        LBITFIELD_SET(CHIP_REGFLD_CNTPS_CTL_IMASK,1);
    chip_msr("CNTPS_CTL_EL1", rval);
}





#endif // CHIP_AARCH64_GTIM_H
