#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_DEV_STM32F429
#define CHIP_DEV_STM32F42X


#define CHIP_DEV_SUPPORT_LCD            1
#define CHIP_DEV_SUPPORT_EXMC           1
#define CHIP_DEV_SUPPORT_SDRAM          (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 144)
#define CHIP_DEV_SUPPORT_EXMC_D32       1
#define CHIP_DEV_SUPPORT_SAI            1
#define CHIP_DEV_SUPPORT_DVP            1
#define CHIP_DEV_SUPPORT_ETH            1
#define CHIP_DEV_SUPPORT_USBHS          1
#define CHIP_DEV_SUPPORT_USBHS_PHY      0
#define CHIP_DEV_SUPPORT_CRS            0
#define CHIP_DEV_SUPPORT_QSPI           0
#define CHIP_DEV_SUPPORT_I2C_TXFRAME    0
#define CHIP_DEV_SUPPORT_TRACE          1

#define CHIP_DEV_SPI_CNT                ((CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100) ? 6 : 4)
#define CHIP_DEV_I2S_CNT                2
#define CHIP_DEV_I2S_FD_CNT             2
#define CHIP_DEV_I2C_CNT                3
#define CHIP_DEV_UART_CNT              8
#define CHIP_DEV_TIM_GP_CNT             10
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          2


#define CHIP_CLK_CPU_FREQ_MAX           CHIP_MHZ(180)

#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
