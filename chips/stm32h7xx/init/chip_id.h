#ifndef CHIP_ID_H
#define CHIP_ID_H

#include "lcspec.h"
#include <stdint.h>

#define CHIP_UID_BITSIZE  96

static LINLINE uint16_t chip_get_device_id(void) {
    return LBITFIELD_GET(CHIP_REGS_DBGMCU->IDCODE, CHIP_REGFLD_DBGMCU_IDCODE_DEV_ID);
}


static LINLINE uint16_t chip_get_revision_id(void) {
    return LBITFIELD_GET(CHIP_REGS_DBGMCU->IDCODE, CHIP_REGFLD_DBGMCU_IDCODE_REV_ID);
}


static LINLINE const uint8_t *chip_get_uid(void) {
    return (const uint8_t *)CHIP_MEMRGN_ADDR_UID;
}


const char *chip_get_device_name(void);
char chip_get_revision_symbol(void);


#endif // CHIP_ID_H
