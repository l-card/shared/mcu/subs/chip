#ifndef CHIP_DEV_PINS_H
#define CHIP_DEV_PINS_H

#include "chip_dev_spec_features.h"

/* ------------------------ Функции пинов ------------------------------------*/

/* ----------- Порт A ---------------*/
/* Пин PA0, RTC_TAMP2, WKUP1 */
#define CHIP_PIN_PA0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 0) */
#define CHIP_PIN_PA0_USART2_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 1)
#define CHIP_PIN_PA0_TIM2_CH1_ETR         CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 2)
#define CHIP_PIN_PA0_TSC_G1_IO1           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 3)
#define CHIP_PIN_PA0_USART4_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 6) */
#define CHIP_PIN_PA0_COMP1_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 7)
#define CHIP_PIN_PA0_ADC_IN0              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 0)
#define CHIP_PIN_PA0_COMP1_INM6           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 0)

/* Пин PA1 */
#define CHIP_PIN_PA1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 0)
#define CHIP_PIN_PA1_USART2_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 1)
#define CHIP_PIN_PA1_TIM2_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 2)
#define CHIP_PIN_PA1_TSC_G1_IO2           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 3)
#define CHIP_PIN_PA1_USART4_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 4)
#define CHIP_PIN_PA1_TIM15_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 5)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 7) */
#define CHIP_PIN_PA1_ADC_IN1              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)
#define CHIP_PIN_PA1_COMP1_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 1)

/* Пин PA2, WKUP4 */
#define CHIP_PIN_PA2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 2)
#define CHIP_PIN_PA2_TIM15_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 0)
#define CHIP_PIN_PA2_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 1)
#define CHIP_PIN_PA2_TIM2_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 2)
#define CHIP_PIN_PA2_TSC_G1_IO3           CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 6) */
#define CHIP_PIN_PA2_COMP2_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 7)
#define CHIP_PIN_PA2_ADC_IN2              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 2)
#define CHIP_PIN_PA2_COMP2_INM6           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 2)

/* Пин PA3 */
#define CHIP_PIN_PA3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 3)
#define CHIP_PIN_PA3_TIM15_CH2            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 0)
#define CHIP_PIN_PA3_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 1)
#define CHIP_PIN_PA3_TIM2_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 2)
#define CHIP_PIN_PA3_TSC_G1_IO4           CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 7) */
#define CHIP_PIN_PA3_ADC_IN3              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 3)
#define CHIP_PIN_PA3_COMP2_INP            CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 3)

/* Пин PA4 */
#define CHIP_PIN_PA4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_SPI1_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 0)
#define CHIP_PIN_PA4_I2S1_WS              CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 0)
#define CHIP_PIN_PA4_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 2) */
#define CHIP_PIN_PA4_TSC_G2_IO1           CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 3)
#define CHIP_PIN_PA4_TIM14_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 7) */
#define CHIP_PIN_PA4_ADC_IN4              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_DAC_OUT1             CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_COMP1_INM4           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)
#define CHIP_PIN_PA4_COMP2_INM4           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 4)

/* Пин PA5 */
#define CHIP_PIN_PA5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 0)
#define CHIP_PIN_PA5_I2S1_CK              CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 0)
#define CHIP_PIN_PA5_CEC                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 1)
#define CHIP_PIN_PA5_TIM2_CH1_ETR         CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 2)
#define CHIP_PIN_PA5_TSC_G2_IO2           CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 7) */
#define CHIP_PIN_PA5_ADC_IN5              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_DAC_OUT2             CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_COMP1_INM5           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)
#define CHIP_PIN_PA5_COMP2_INM5           CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 5)

/* Пин PA6 */
#define CHIP_PIN_PA6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 6)
#define CHIP_PIN_PA6_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 0)
#define CHIP_PIN_PA6_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 0)
#define CHIP_PIN_PA6_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 1)
#define CHIP_PIN_PA6_TIM1_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 2)
#define CHIP_PIN_PA6_TSC_G2_IO3           CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 3)
#define CHIP_PIN_PA6_USART3_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 4)
#define CHIP_PIN_PA6_TIM16_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 5)
#define CHIP_PIN_PA6_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 6)
#define CHIP_PIN_PA6_COMP1_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 7)
#define CHIP_PIN_PA6_ADC_IN6              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 6)

/* Пин PA7 */
#define CHIP_PIN_PA7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 7)
#define CHIP_PIN_PA7_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 0)
#define CHIP_PIN_PA7_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 0)
#define CHIP_PIN_PA7_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 1)
#define CHIP_PIN_PA7_TIM1_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 2)
#define CHIP_PIN_PA7_TSC_G2_IO4           CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 3)
#define CHIP_PIN_PA7_TIM14_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 4)
#define CHIP_PIN_PA7_TIM17_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 5)
#define CHIP_PIN_PA7_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 6)
#define CHIP_PIN_PA7_COMP2_OUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 7)
#define CHIP_PIN_PA7_ADC_IN7              CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 7)

/* Пин PA8 */
#define CHIP_PIN_PA8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 8)
#define CHIP_PIN_PA8_MCO                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 0)
#define CHIP_PIN_PA8_USART1_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 1)
#define CHIP_PIN_PA8_TIM1_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 2)
#define CHIP_PIN_PA8_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 3)
#define CHIP_PIN_PA8_CRS_SYNC             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 7) */
#define CHIP_PIN_PA8_ADC_IN14             CHIP_PIN_ID_ANALOG(CHIP_PORT_A, 8)

/* Пин PA9 */
#define CHIP_PIN_PA9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 9)
#define CHIP_PIN_PA9_TIM15_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 0)
#define CHIP_PIN_PA9_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 1)
#define CHIP_PIN_PA9_TIM1_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 2)
#define CHIP_PIN_PA9_TSC_G4_IO1           CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 7) */

/* Пин PA10 */
#define CHIP_PIN_PA10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 10)
#define CHIP_PIN_PA10_TIM17_BKIN          CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 0)
#define CHIP_PIN_PA10_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 1)
#define CHIP_PIN_PA10_TIM1_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 2)
#define CHIP_PIN_PA10_TSC_G4_IO2          CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 7) */

/* Пин PA11, USB_DM */
#define CHIP_PIN_PA11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 11)
#define CHIP_PIN_PA11_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 0)
#define CHIP_PIN_PA11_USART1_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 1)
#define CHIP_PIN_PA11_TIM1_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 2)
#define CHIP_PIN_PA11_TSC_G4_IO3          CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 3)
#define CHIP_PIN_PA11_CAN_RX              CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 6) */
#define CHIP_PIN_PA11_COMP1_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 7)

/* Пин PA12, USB_DP */
#define CHIP_PIN_PA12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A)
#define CHIP_PIN_PA12_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 0)
#define CHIP_PIN_PA12_USART1_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 1)
#define CHIP_PIN_PA12_TIM1_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 2)
#define CHIP_PIN_PA12_TSC_G4_IO4          CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 3)
#define CHIP_PIN_PA12_CAN_TX              CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 6) */
#define CHIP_PIN_PA12_COMP2_OUT           CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 7)

/* Пин PA13 */
#define CHIP_PIN_PA13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 13)
#define CHIP_PIN_PA13_SWDIO               CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 0)
#define CHIP_PIN_PA13_IR_OUT              CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 1)
#define CHIP_PIN_PA13_USB_NOE             CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 2)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 7) */

/* Пин PA14 */
#define CHIP_PIN_PA14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 14)
#define CHIP_PIN_PA14_SWCLK               CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 0)
#define CHIP_PIN_PA14_USART2_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 7) */

/* Пин PA15 */
#define CHIP_PIN_PA15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 15)
#define CHIP_PIN_PA15_SPI1_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 0)
#define CHIP_PIN_PA15_I2S1_WS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 0)
#define CHIP_PIN_PA15_USART2_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1)
#define CHIP_PIN_PA15_TIM2_CH1_ETR        CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 2)
#define CHIP_PIN_PA15_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 3)
#define CHIP_PIN_PA15_USART4_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 7) */


/* ----------- Порт B ---------------*/
/* Пин PB0 */
#define CHIP_PIN_PB0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 0)
#define CHIP_PIN_PB0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 0)
#define CHIP_PIN_PB0_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 1)
#define CHIP_PIN_PB0_TIM1_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 2)
#define CHIP_PIN_PB0_TSC_G3_IO2           CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 3)
#define CHIP_PIN_PB0_USART3_CK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 5) */
#define CHIP_PIN_PB0_ADC_IN8              CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 0)

/* Пин PB1 */
#define CHIP_PIN_PB1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 1)
#define CHIP_PIN_PB1_TIM14_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 0)
#define CHIP_PIN_PB1_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 1)
#define CHIP_PIN_PB1_TIM1_CH3N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 2)
#define CHIP_PIN_PB1_TSC_G3_IO3           CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 3)
#define CHIP_PIN_PB1_USART3_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 5) */
#define CHIP_PIN_PB1_ADC_IN9              CHIP_PIN_ID_ANALOG(CHIP_PORT_B, 1)

/* Пин PB2 */
#define CHIP_PIN_PB2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 2)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 0) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 1) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 2) */
#define CHIP_PIN_PB2_TSC_G3_IO4           CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 2, 5) */

/* Пин PB3 */
#define CHIP_PIN_PB3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 3)
#define CHIP_PIN_PB3_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 0)
#define CHIP_PIN_PB3_I2S1_CK              CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 0)
#define CHIP_PIN_PB3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 1)
#define CHIP_PIN_PB3_TIM2_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 2)
#define CHIP_PIN_PB3_TSC_G5_IO1           CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 3, 5) */


/* Пин PB4 */
#define CHIP_PIN_PB4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 4)
#define CHIP_PIN_PB4_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 0)
#define CHIP_PIN_PB4_I2S1_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 0)
#define CHIP_PIN_PB4_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 1)
#define CHIP_PIN_PB4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 2)
#define CHIP_PIN_PB4_TSC_G5_IO2           CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 4) */
#define CHIP_PIN_PB4_TIM17_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 4, 5)

/* Пин PB5, WKUP6 */
#define CHIP_PIN_PB5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 5)
#define CHIP_PIN_PB5_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 0)
#define CHIP_PIN_PB5_I2S1_SD              CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 0)
#define CHIP_PIN_PB5_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 1)
#define CHIP_PIN_PB5_TIM16_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 2)
#define CHIP_PIN_PB5_I2C1_SMBA            CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 5, 5) */

/* Пин PB6 */
#define CHIP_PIN_PB6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 6)
#define CHIP_PIN_PB6_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 0)
#define CHIP_PIN_PB6_I2C1_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 1)
#define CHIP_PIN_PB6_TIM16_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 2)
#define CHIP_PIN_PB6_TSC_G5_IO3           CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 6, 5) */

/* Пин PB7 */
#define CHIP_PIN_PB7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 7)
#define CHIP_PIN_PB7_USART1_RX            CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 0)
#define CHIP_PIN_PB7_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 7, 1)
#define CHIP_PIN_PB7_TIM17_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 2)
#define CHIP_PIN_PB7_TSC_G5_IO4           CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 3)
#define CHIP_PIN_PB7_USART4_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 7, 5) */

/* Пин PB8 */
#define CHIP_PIN_PB8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 8)
#define CHIP_PIN_PB8_CEC                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 0)
#define CHIP_PIN_PB8_I2C1_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 1)
#define CHIP_PIN_PB8_TIM16_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 2)
#define CHIP_PIN_PB8_TSC_SYNC             CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 3)
#define CHIP_PIN_PB8_CAN_RX               CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 8, 5) */

/* Пин PB9 */
#define CHIP_PIN_PB9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 9)
#define CHIP_PIN_PB9_IR_OUT               CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 0)
#define CHIP_PIN_PB9_I2C1_SDA             CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 9, 1)
#define CHIP_PIN_PB9_TIM17_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 2)
#define CHIP_PIN_PB9_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 3)
#define CHIP_PIN_PB9_CAN_TX               CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 4)
#define CHIP_PIN_PB9_SPI2_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 5)
#define CHIP_PIN_PB9_I2S2_WS              CHIP_PIN_ID_AF    (CHIP_PORT_B, 9, 5)

/* Пин PB10 */
#define CHIP_PIN_PB10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 10)
#define CHIP_PIN_PB10_CEC                 CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 0)
#define CHIP_PIN_PB10_I2C2_SCL            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 1)
#define CHIP_PIN_PB10_TIM2_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 2)
#define CHIP_PIN_PB10_TSC_SYNC            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 3)
#define CHIP_PIN_PB10_USART3_TX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 4)
#define CHIP_PIN_PB10_SPI2_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 5)
#define CHIP_PIN_PB10_I2S2_CK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 10, 5)

/* Пин PB11 */
#define CHIP_PIN_PB11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 11)
#define CHIP_PIN_PB11_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 0)
#define CHIP_PIN_PB11_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 11, 1)
#define CHIP_PIN_PB11_TIM2_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 2)
#define CHIP_PIN_PB11_TSC_G6_IO1          CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 3)
#define CHIP_PIN_PB11_USART3_RX           CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 11, 5) */

/* Пин PB12 */
#define CHIP_PIN_PB12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 12)
#define CHIP_PIN_PB12_SPI2_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0)
#define CHIP_PIN_PB12_I2S2_WS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 0)
#define CHIP_PIN_PB12_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 1)
#define CHIP_PIN_PB12_TIM1_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 2)
#define CHIP_PIN_PB12_TSC_G6_IO2          CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 3)
#define CHIP_PIN_PB12_USART3_CK           CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 4)
#define CHIP_PIN_PB12_TIM15_BKIN          CHIP_PIN_ID_AF    (CHIP_PORT_B, 12, 5)

/* Пин PB13 */
#define CHIP_PIN_PB13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 13)
#define CHIP_PIN_PB13_SPI2_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0)
#define CHIP_PIN_PB13_I2S2_CK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 1) */
#define CHIP_PIN_PB13_TIM1_CH1N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 2)
#define CHIP_PIN_PB13_TSC_G6_IO3          CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 3)
#define CHIP_PIN_PB13_USART3_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 4)
#define CHIP_PIN_PB13_I2C2_SCL            CHIP_PIN_ID_AF    (CHIP_PORT_B, 13, 5)

/* Пин PB14 */
#define CHIP_PIN_PB14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 14)
#define CHIP_PIN_PB14_SPI2_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0)
#define CHIP_PIN_PB14_I2S2_MCK            CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 0)
#define CHIP_PIN_PB14_TIM15_CH1           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 1)
#define CHIP_PIN_PB14_TIM1_CH2N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 2)
#define CHIP_PIN_PB14_TSC_G6_IO4          CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 3)
#define CHIP_PIN_PB14_USART3_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_B, 14, 4)
#define CHIP_PIN_PB14_I2C2_SDA            CHIP_PIN_ID_AF_OD (CHIP_PORT_B, 14, 5)

/* Пин PB15, WKUP7, RTC_REFIN */
#define CHIP_PIN_PB15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 15)
#define CHIP_PIN_PB15_SPI2_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0)
#define CHIP_PIN_PB15_I2S2_SD             CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 0)
#define CHIP_PIN_PB15_TIM15_CH2           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 1)
#define CHIP_PIN_PB15_TIM1_CH3N           CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 2)
#define CHIP_PIN_PB15_TIM15_CH1N          CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 15, 5) */

/* ----------- Порт C ---------------*/
/* Пин PC0 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 0)
#define CHIP_PIN_PC0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 0, 1) */
#define CHIP_PIN_PC0_ADC_IN10             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 0)
#endif

/* Пин PC1 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 1)
#define CHIP_PIN_PC1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 1, 1) */
#define CHIP_PIN_PC1_ADC_IN11             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 1)
#endif

/* Пин PC2 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 2)
#define CHIP_PIN_PC2_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 0)
#define CHIP_PIN_PC2_SPI2_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 1)
#define CHIP_PIN_PC2_I2S2_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_C, 2, 1)
#define CHIP_PIN_PC2_ADC_IN12             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 2)
#endif

/* Пин PC3 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 3)
#define CHIP_PIN_PC3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 0)
#define CHIP_PIN_PC3_SPI2_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 1)
#define CHIP_PIN_PC3_I2S2_SD              CHIP_PIN_ID_AF    (CHIP_PORT_C, 3, 1)
#define CHIP_PIN_PC3_ADC_IN13             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 3)
#endif

/* Пин PC4 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 4)
#define CHIP_PIN_PC4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_C, 4, 0)
#define CHIP_PIN_PC4_USART3_TX            CHIP_PIN_ID_AF    (CHIP_PORT_C, 4, 1)
#define CHIP_PIN_PC4_ADC_IN14             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 4)
#endif

/* Пин PC5, WKUP5 */
#if (CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64)
#define CHIP_PIN_PC5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 5)
#define CHIP_PIN_PC5_TSC_G3_IO1           CHIP_PIN_ID_AF    (CHIP_PORT_C, 5, 0)
#define CHIP_PIN_PC5_USART3_RX            CHIP_PIN_ID_AF    (CHIP_PORT_C, 5, 1)
#define CHIP_PIN_PC5_ADC_IN15             CHIP_PIN_ID_ANALOG(CHIP_PORT_C, 5)
#endif

/* Пин PC6 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 6)
#define CHIP_PIN_PC6_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_C, 6, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 6, 1) */
#endif

/* Пин PC7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 7)
#define CHIP_PIN_PC7_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_C, 7, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 7, 1) */
#endif

/* Пин PC8 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 8)
#define CHIP_PIN_PC8_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_C, 8, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 8, 1) */
#endif

/* Пин PC9 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 9)
#define CHIP_PIN_PC9_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_C, 9, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_C, 9, 1) */
#endif

/* Пин PC10 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 10)
#define CHIP_PIN_PC10_USART4_TX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 10, 0)
#define CHIP_PIN_PC10_USART3_TX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 10, 1)
#endif

/* Пин PC11 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 11)
#define CHIP_PIN_PC11_USART4_RX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 11, 0)
#define CHIP_PIN_PC11_USART3_RX           CHIP_PIN_ID_AF    (CHIP_PORT_C, 11, 1)
#endif

/* Пин PC12 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PC12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 12)
#define CHIP_PIN_PC12_USART4_CK           CHIP_PIN_ID_AF    (CHIP_PORT_C, 12, 0)
#define CHIP_PIN_PC12_USART3_CK           CHIP_PIN_ID_AF    (CHIP_PORT_C, 12, 1)
#endif

/* Пин PC13, WKUP2, RTC_TAMP1, RTC_TS, RTC_OUT */
#define CHIP_PIN_PC13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 13)

/* Пин PC14, OSC32_IN */
#define CHIP_PIN_PC14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 14)

/* Пин PC15, OSC32_OUT */
#define CHIP_PIN_PC15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_C, 15)

/* ----------- Порт D ---------------*/
/* Пин PD0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 0)
#define CHIP_PIN_PD0_CAN_RX               CHIP_PIN_ID_AF    (CHIP_PORT_D, 0, 0)
#define CHIP_PIN_PD0_SPI2_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_D, 0, 1)
#define CHIP_PIN_PD0_I2S2_WS              CHIP_PIN_ID_AF    (CHIP_PORT_D, 0, 1)
#endif

/* Пин PD1 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 1)
#define CHIP_PIN_PD1_CAN_TX               CHIP_PIN_ID_AF    (CHIP_PORT_D, 1, 0)
#define CHIP_PIN_PD1_SPI2_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_D, 1, 1)
#define CHIP_PIN_PD1_I2S2_CK              CHIP_PIN_ID_AF    (CHIP_PORT_D, 1, 1)
#endif

/* Пин PD2 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 64
#define CHIP_PIN_PD2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 2)
#define CHIP_PIN_PD2_TIM3_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 0)
#define CHIP_PIN_PD2_USART3_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_D, 2, 1)
#endif

/* Пин PD3 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 3)
#define CHIP_PIN_PD3_USART2_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_D, 3, 0)
#define CHIP_PIN_PD3_SPI2_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_D, 3, 1)
#define CHIP_PIN_PD3_I2S2_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_D, 3, 1)
#endif

/* Пин PD4 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 4)
#define CHIP_PIN_PD4_USART2_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_D, 4, 0)
#define CHIP_PIN_PD4_SPI2_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_D, 4, 1)
#define CHIP_PIN_PD4_I2S2_SD              CHIP_PIN_ID_AF    (CHIP_PORT_D, 4, 1)
#endif

/* Пин PD5 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 5)
#define CHIP_PIN_PD5_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_D, 5, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 5, 1) */
#endif

/* Пин PD6 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 6)
#define CHIP_PIN_PD6_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_D, 6, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 6, 1) */
#endif

/* Пин PD7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 7)
#define CHIP_PIN_PD7_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_D, 7, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 7, 1) */
#endif

/* Пин PD8 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 8)
#define CHIP_PIN_PD8_USART3_TX            CHIP_PIN_ID_AF    (CHIP_PORT_D, 8, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 8, 1) */
#endif

/* Пин PD9 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 9)
#define CHIP_PIN_PD9_USART3_RX            CHIP_PIN_ID_AF    (CHIP_PORT_D, 9, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 9, 1) */
#endif

/* Пин PD10 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 10)
#define CHIP_PIN_PD10_USART3_CK           CHIP_PIN_ID_AF    (CHIP_PORT_D, 10, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 10, 1) */
#endif

/* Пин PD11 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 11)
#define CHIP_PIN_PD11_USART3_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_D, 11, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 11, 1) */
#endif

/* Пин PD12 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 12)
#define CHIP_PIN_PD12_USART3_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_D, 12, 0)
#define CHIP_PIN_PD12_TSC_G8_IO1          CHIP_PIN_ID_AF    (CHIP_PORT_D, 12, 1)
#endif

/* Пин PD13 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 13)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 13, 0) */
#define CHIP_PIN_PD13_TSC_G8_IO2          CHIP_PIN_ID_AF    (CHIP_PORT_D, 13, 1)
#endif

/* Пин PD14 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 14)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_D, 14, 0) */
#define CHIP_PIN_PD14_TSC_G8_IO3          CHIP_PIN_ID_AF    (CHIP_PORT_D, 14, 1)
#endif

/* Пин PD15 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PD15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_D, 15)
#define CHIP_PIN_PD15_CRS_SYNC            CHIP_PIN_ID_AF    (CHIP_PORT_D, 12, 0)
#define CHIP_PIN_PD15_TSC_G8_IO4          CHIP_PIN_ID_AF    (CHIP_PORT_D, 15, 1)
#endif

/* ----------- Порт E ---------------*/
/* Пин PE0 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 0)
#define CHIP_PIN_PE0_TIM16_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_E, 0, 0)
#define CHIP_PIN_PE0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_E, 0, 1)
#endif

/* Пин PE1 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 1)
#define CHIP_PIN_PE1_TIM17_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_E, 1, 0)
#define CHIP_PIN_PE1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_E, 1, 1)
#endif

/* Пин PE2 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 2)
#define CHIP_PIN_PE2_TIM3_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 0)
#define CHIP_PIN_PE2_TSC_G7_IO1           CHIP_PIN_ID_AF    (CHIP_PORT_E, 2, 1)
#endif

/* Пин PE3 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 3)
#define CHIP_PIN_PE3_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 0)
#define CHIP_PIN_PE3_TSC_G7_IO2           CHIP_PIN_ID_AF    (CHIP_PORT_E, 3, 1)
#endif

/* Пин PE4 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 4)
#define CHIP_PIN_PE4_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 0)
#define CHIP_PIN_PE4_TSC_G7_IO3           CHIP_PIN_ID_AF    (CHIP_PORT_E, 4, 1)
#endif

/* Пин PE5 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 5)
#define CHIP_PIN_PE5_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_E, 5, 0)
#define CHIP_PIN_PE5_TSC_G7_IO4           CHIP_PIN_ID_AF    (CHIP_PORT_E, 5, 1)
#endif

/* Пин PE6, WKUP3, RTC_TAMP3 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 6)
#define CHIP_PIN_PE6_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_E, 6, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 6, 1) */
#endif

/* Пин PE7 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 7)
#define CHIP_PIN_PE7_TIM1_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_E, 7, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 7, 1) */
#endif

/* Пин PE8 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 8)
#define CHIP_PIN_PE8_TIM1_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_E, 8, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 8, 1) */
#endif

/* Пин PE9 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 9)
#define CHIP_PIN_PE9_TIM1_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_E, 9, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 9, 1) */
#endif

/* Пин PE10 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 10)
#define CHIP_PIN_PE10_TIM1_CH2N           CHIP_PIN_ID_AF    (CHIP_PORT_E, 10, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 10, 1) */
#endif

/* Пин PE11 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 11)
#define CHIP_PIN_PE11_TIM1_CH2            CHIP_PIN_ID_AF    (CHIP_PORT_E, 11, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_E, 11, 1) */
#endif

/* Пин PE12 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 12)
#define CHIP_PIN_PE12_TIM1_CH3N           CHIP_PIN_ID_AF    (CHIP_PORT_E, 12, 0)
#define CHIP_PIN_PE12_SPI1_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_E, 12, 1)
#define CHIP_PIN_PE12_I2S1_WS             CHIP_PIN_ID_AF    (CHIP_PORT_E, 12, 1)
#endif

/* Пин PE13 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 13)
#define CHIP_PIN_PE13_TIM1_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_E, 13, 0)
#define CHIP_PIN_PE13_SPI1_SCK            CHIP_PIN_ID_AF    (CHIP_PORT_E, 13, 1)
#define CHIP_PIN_PE13_I2S1_CK             CHIP_PIN_ID_AF    (CHIP_PORT_E, 13, 1)
#endif

/* Пин PE14 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 14)
#define CHIP_PIN_PE14_TIM1_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_E, 14, 0)
#define CHIP_PIN_PE14_SPI1_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_E, 14, 1)
#define CHIP_PIN_PE14_I2S1_MCK            CHIP_PIN_ID_AF    (CHIP_PORT_E, 14, 1)
#endif

/* Пин PE15 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PE15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_E, 15)
#define CHIP_PIN_PE15_TIM1_BKIN           CHIP_PIN_ID_AF    (CHIP_PORT_E, 15, 0)
#define CHIP_PIN_PE15_SPI1_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_E, 15, 1)
#define CHIP_PIN_PE15_I2S1_SD             CHIP_PIN_ID_AF    (CHIP_PORT_E, 15, 1)
#endif


/* ----------- Порт F ---------------*/
/* Пин PF0, OSC_IN */
#define CHIP_PIN_PF0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 0)
#define CHIP_PIN_PF0_CRS_SYNC             CHIP_PIN_ID_AF    (CHIP_PORT_F, 0, 0)

/* Пин PF1, OSC_OUT */
#define CHIP_PIN_PF1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_F, 1, 0) */

/* Пин PF2, WKUP8 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PF2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 2)
#define CHIP_PIN_PF2_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_F, 2, 0)
#endif

/* Пин PF3 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PF3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 3)
#define CHIP_PIN_PF3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_F, 3, 0)
#endif

/* Пин PF6 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PF6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 6)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_F, 6, 0) */
#endif

/* Пин PF9 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PF9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 9)
#define CHIP_PIN_PF9_TIM15_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_F, 9, 0)
#endif

/* Пин PF10 */
#if CHIP_PKG_ID_PINCNT(CHIP_DEV_PKG) >= 100
#define CHIP_PIN_PF10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_F, 10)
#define CHIP_PIN_PF10_TIM15_CH2           CHIP_PIN_ID_AF    (CHIP_PORT_F, 10, 0)
#endif


#endif // CHIP_DEV_PINS_H
