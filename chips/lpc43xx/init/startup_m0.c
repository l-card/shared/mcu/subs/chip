/*================================================================================================*
 * Код начальной инициализации LPC43xx M0APP
 *================================================================================================*/
#include <stdint.h>
#include <stddef.h>
#include "chip_init.h"

/*================================================================================================*/
/* Настройки стека */
#ifndef STACK_SIZE
#error "STACK_SIZE should be defined in makefile"
#endif
/*================================================================================================*/

/*================================================================================================*/
/* Объявление символов, на которые ссылается таблица векторов */
/*================================================================================================*/

#define WEAK_DECL(handler_name) \
    void __attribute__ ((__interrupt__)) handler_name(void) __attribute__ ((__weak__, __alias__("dummy_handler_m0")))

static __attribute__ ((__interrupt__)) void dummy_handler_m0(void);

/* Standard Cortex-M0 interrupts */
void __attribute__ ((__interrupt__)) Reset_ExHandler_m0(void);
WEAK_DECL(NMI_ExHandler_m0);
WEAK_DECL(HardFault_ExHandler_m0);
WEAK_DECL(SVCall_ExHandler_m0);
WEAK_DECL(Debug_ExHandler_m0);
WEAK_DECL(PendSV_ExHandler_m0);
WEAK_DECL(SysTick_IRQHandler_m0);

/* Chip specific interrupts */
WEAK_DECL(RTC_IRQHandler_m0);
WEAK_DECL(M4_IRQHandler_m0);
WEAK_DECL(DMA_IRQHandler_m0);
WEAK_DECL(FLASHEEPROM_ATIMER_IRQHandler_m0);    /* FLASHEEPROM | ATIMER */
WEAK_DECL(ETH_IRQHandler_m0);
WEAK_DECL(SDIO_IRQHandler_m0);
WEAK_DECL(LCD_IRQHandler_m0);
WEAK_DECL(USB0_IRQHandler_m0);
WEAK_DECL(USB1_IRQHandler_m0);
WEAK_DECL(SCT_IRQHandler_m0);
WEAK_DECL(RIT_WDT_IRQHandler_m0);               /* RITIMER | WWDT */
WEAK_DECL(TIMER0_IRQHandler_m0);
WEAK_DECL(GINT1_IRQHandler_m0);
WEAK_DECL(GPIO4_IRQHandler_m0);
WEAK_DECL(TIMER3_IRQHandler_m0);
WEAK_DECL(MCPWM_IRQHandler_m0);
WEAK_DECL(ADC0_IRQHandler_m0);
WEAK_DECL(I2C0_I2C1_IRQHandler_m0);             /* I2C0 | I2C1 */
WEAK_DECL(SGPIO_IRQHandler_m0);
WEAK_DECL(SPI_DAC_IRQHandler_m0);               /* SPI | DAC */
WEAK_DECL(ADC1_IRQHandler_m0);
WEAK_DECL(SSP0_SSP1_IRQHandler_m0);             /* SSP0 | SSP1 */
WEAK_DECL(EVRT_IRQHandler_m0);
WEAK_DECL(UART0_IRQHandler_m0);
WEAK_DECL(UART1_IRQHandler_m0);
WEAK_DECL(UART2_CAN1_IRQHandler_m0);            /* USART2 | CAN1 */
WEAK_DECL(UART3_IRQHandler_m0);
WEAK_DECL(I2S0_I2S1_QEI_IRQHandler_m0);         /* I2S0 | I2S1 | QEI */
WEAK_DECL(CAN0_IRQHandler_m0);
WEAK_DECL(ADCHS_IRQHandler_m0);
WEAK_DECL(M0SUB_IRQHandler_m0);

void m0_main(void);
/*================================================================================================*/

/*================================================================================================*/
/* Стек M0 */
__attribute__ ((section(".stackarea_m0"))) uint32_t g_cpu_stack_m0[STACK_SIZE];

/* Таблица векторов прерываний */
typedef void (*t_irq_handler)(void);

__attribute__ ((section(".isr_vector_m0"))) const t_irq_handler g_int_vectors_m0[] =
    {
    /* IRQ# */
    /* SP */    (t_irq_handler)(g_cpu_stack_m0 + STACK_SIZE),
    /* RESET */ Reset_ExHandler_m0,
    /* -14 */   NMI_ExHandler_m0, HardFault_ExHandler_m0,
    /* -12 */   /* reserved */ NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    /*  -5 */   SVCall_ExHandler_m0, Debug_ExHandler_m0,
    /*  -3 */   /* reserved */ NULL,
    /*  -2 */   PendSV_ExHandler_m0,
    /*  -1 */   SysTick_IRQHandler_m0,
    /*   0 */   RTC_IRQHandler_m0,          M4_IRQHandler_m0,       DMA_IRQHandler_m0,          NULL,
    /*   4 */   FLASHEEPROM_ATIMER_IRQHandler_m0, ETH_IRQHandler_m0, SDIO_IRQHandler_m0,        LCD_IRQHandler_m0,
    /*   8 */   USB0_IRQHandler_m0,         USB1_IRQHandler_m0,     SCT_IRQHandler_m0,          RIT_WDT_IRQHandler_m0,
    /*  12 */   TIMER0_IRQHandler_m0,       GINT1_IRQHandler_m0,    GPIO4_IRQHandler_m0,        TIMER3_IRQHandler_m0,
    /*  16 */   MCPWM_IRQHandler_m0,        ADC0_IRQHandler_m0,     I2C0_I2C1_IRQHandler_m0,    SGPIO_IRQHandler_m0,
    /*  20 */   SPI_DAC_IRQHandler_m0,      ADC1_IRQHandler_m0,     SSP0_SSP1_IRQHandler_m0,    EVRT_IRQHandler_m0,
    /*  24 */   UART0_IRQHandler_m0,        UART1_IRQHandler_m0,    UART2_CAN1_IRQHandler_m0,   UART3_IRQHandler_m0,
    /*  28 */   I2S0_I2S1_QEI_IRQHandler_m0,CAN0_IRQHandler_m0,     ADCHS_IRQHandler_m0,        M0SUB_IRQHandler_m0
    };
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static __attribute__ ((__interrupt__, section(".text_m0"))) void dummy_handler_m0
    (void)
    {
    for (;;);
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void __attribute__ ((__noreturn__, __interrupt__, section(".text_m0"))) Reset_ExHandler_m0(void)
    {
    m0_main();
    for (;;);
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
