#ifndef CHIP_ATMEGA8515_XMEM_H
#define CHIP_ATMEGA8515_XMEM_H

#define CHIP_XMEM_WS_0_0    0 /* без доп. циклов */
#define CHIP_XMEM_WS_1_0    1 /* 1 доп. цикл  во время RD/WR */
#define CHIP_XMEM_WS_2_0    2 /* 2 доп. цикла во время RD/WR */
#define CHIP_XMEM_WS_2_1    3 /* 2 доп. цикла во время RD/WR + 1 доп. цикл после RD/WR перед след. циклом */

#endif // CHIP_ATMEGA8515_XMEM_H
