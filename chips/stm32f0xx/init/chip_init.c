#include "chip.h"
#include "chip_init.h"

void chip_clk_init(void);
void chip_pwr_init(void);


void chip_dummy_init_fault(int code) {
    (void)code;

    for (;;) {
        continue;
    }
}




void chip_init(void) {
    /* Устанавливаем mapping нулевого адреса на внутреннюю flash,
       так как в случае старта из системного загрузкчика может быть другой mapping,
       в результате чего обработчики прерывания будут использованы из загрузчика
       (т.к. в Cortex-M0 нельзя переместить VTOR) */
    LBITFIELD_UPD(CHIP_REGS_SYSCFG->CFGR1, CHIP_REGFLD_SYSCFG_CFGR1_MEM_MODE,
                  CHIP_REGFLDVAL_SYSCFG_CFGR1_MEM_MODE_MAIN_FLASH);

    chip_clk_init();
}


void chip_main_prestart() {

}
