#ifndef REGS_SRC_H
#define REGS_SRC_H

#include "chip_ioreg_defs.h"

#define CHIP_RCR_NUM_SCR            0
#define CHIP_RCR_NUM_A53_0          1
#define CHIP_RCR_NUM_A53_1          2
#define CHIP_RCR_NUM_M7             3
#define CHIP_RCR_NUM_SUPERMIX       6
#define CHIP_RCR_NUM_AUDIOMIX       7
#define CHIP_RCR_NUM_USB_PHY1       8
#define CHIP_RCR_NUM_USB_PHY2       9
#define CHIP_RCR_NUM_MLMIX          10
#define CHIP_RCR_NUM_PCIE_PHY       11
#define CHIP_RCR_NUM_HDMI           12
#define CHIP_RCR_NUM_MEDIAMIX       13
#define CHIP_RCR_NUM_GPU2D          14
#define CHIP_RCR_NUM_GPU3D          15
#define CHIP_RCR_NUM_GPU            16
#define CHIP_RCR_NUM_VPU            17
#define CHIP_RCR_NUM_VPU_G1         18
#define CHIP_RCR_NUM_VPU_G2         19
#define CHIP_RCR_NUM_VPU_VC8K       20
#define CHIP_RCR_NUM_NOC            21
#define CHIP_RCR_NUM_DDRC           1024
#define CHIP_RCR_NUM_HDMI_PHY       1026
#define CHIP_RCR_NUM_MIPI_PHY1      1027
#define CHIP_RCR_NUM_MIPI_PHY2      1028
#define CHIP_RCR_NUM_HSIO           1029
#define CHIP_RCR_NUM_MEDIA_ISP_DW   1030


#define CHIP_SCR_MASKCODE_MASKED    0x5
#define CHIP_SCR_MASKCODE_UNMASKED  0xA

typedef struct {
    __IO uint32_t SCR;              /* SRC Reset Control Register,              Address offset: 0x00 */
    __IO uint32_t A53RCR0;          /* A53 Reset Control Register 0,            Address offset: 0x04 */
    __IO uint32_t A53RCR1;          /* A53 Reset Control Register 1,            Address offset: 0x08 */
    __IO uint32_t M7RCR;            /* M7 Reset Control Register,               Address offset: 0x0C */
     uint32_t RESERVED0[2];
    __IO uint32_t SUPERMIX_RCR;     /* SUPERMIX Reset Control Register,         Address offset: 0x18 */
    __IO uint32_t AUDIOMIX_RCR;     /* AUDIOMIX Reset Control Register,         Address offset: 0x1C */
    __IO uint32_t USBPHY1_RCR;      /* USB PHY1 Reset Control Register,         Address offset: 0x20 */
    __IO uint32_t USBPHY2_RCR;      /* USB PHY2 Reset Control Register,         Address offset: 0x24 */
    __IO uint32_t MLMIX_RCR;        /* MLMIX Reset Control Register,            Address offset: 0x28 */
    __IO uint32_t PCIEPHY_RCR;      /* PCIE PHY Reset Control Register,         Address offset: 0x2C */
    __IO uint32_t HDMI_RCR;         /* HDMI Reset Control Register,             Address offset: 0x30 */
    __IO uint32_t MEDIA_RCR;        /* MEDIAMIX Reset Control Register,         Address offset: 0x34 */
    __IO uint32_t GPU2D_RCR;        /* GPU2D Reset Control Register,            Address offset: 0x38 */
    __IO uint32_t GPU3D_RCR;        /* GPU3D Reset Control Register,            Address offset: 0x3C */
    __IO uint32_t GPU_RCR;          /* GPU Reset Control Register,              Address offset: 0x40 */
    __IO uint32_t VPU_RCR;          /* VPU Reset Control Register,              Address offset: 0x44 */
    __IO uint32_t VPU_G1_RCR;       /* VPU G1 Reset Control Register,           Address offset: 0x48 */
    __IO uint32_t VPU_G2_RCR;       /* VPU G2 Reset Control Register,           Address offset: 0x4C */
    __IO uint32_t VPU_VC8KE_RCR;    /* VPU VC8000E Reset Control Register,      Address offset: 0x50 */
    __IO uint32_t NOC_RCR;          /* NOC Wrapper  Reset Control Register,     Address offset: 0x54 */
    __I  uint32_t SBMR1;            /* SRC Boot Mode Register 1,                Address offset: 0x58 */
    __IO uint32_t SRSR;             /* SRC Reset Status Register,               Address offset: 0x5C */
    uint32_t RESERVED1[2];
    __IO uint32_t SISR;             /* SRC Interrupt Status Register,           Address offset: 0x68 */
    __IO uint32_t SIMR;             /* SRC Interrupt Mask Register,             Address offset: 0x6C */
    __I  uint32_t SBMR2;            /* SRC Boot Mode Register 2,                Address offset: 0x70 */
    __I  uint32_t GPR1;             /* SRC General Purpose Register 1,          Address offset: 0x74 */
    __I  uint32_t GPR2;             /* SRC General Purpose Register 2,          Address offset: 0x78 */
    __I  uint32_t GPR3;             /* SRC General Purpose Register 3,          Address offset: 0x7C */
    __I  uint32_t GPR4;             /* SRC General Purpose Register 4,          Address offset: 0x80 */
    __I  uint32_t GPR5;             /* SRC General Purpose Register 5,          Address offset: 0x84 */
    __I  uint32_t GPR6;             /* SRC General Purpose Register 6,          Address offset: 0x88 */
    __I  uint32_t GPR7;             /* SRC General Purpose Register 7,          Address offset: 0x8C */
    __I  uint32_t GPR8;             /* SRC General Purpose Register 8,          Address offset: 0x90 */
    __I  uint32_t GPR9;             /* SRC General Purpose Register 9,          Address offset: 0x94 */
    __I  uint32_t GPR10;            /* SRC General Purpose Register 10,         Address offset: 0x98 */
    uint32_t RESERVED2[985];
    __IO uint32_t DDRC_RCR;         /* DDR Controller Reset Control Register,   Address offset: 0x1000 */
    uint32_t RESERVED3;
    __IO uint32_t HDMIPHY_RCR;      /* HDMIPHY Reset Control Register,          Address offset: 0x1008 */
    __IO uint32_t MIPIPHY1_RCR;     /* MIPI PHY1 Reset Control Register,        Address offset: 0x100C */
    __IO uint32_t MIPIPHY2_RCR;     /* MIPI PHY2 Reset Control Register,        Address offset: 0x1010 */
    __IO uint32_t HSIO_RCR;         /* HSIO Reset Control Register,             Address offset: 0x1014 */
    __IO uint32_t MEDIAISPDW_RCR;   /* MEDIAMIX ISP and Dewarp Reset Control Register,  Address offset: 0x1018 */

} CHIP_REGS_SRC_T;


#define CHIP_REGS_SRC               ((CHIP_REGS_SRC_T *) CHIP_MEMRGN_ADDR_PERIPH_SRC)

/*********** RCR General regs bits (SRC_SCR/SRC_xxx_RCR) *********************/
#define CHIP_REGFLD_SRC_RCR_DOM_EN                  (0x00000001UL << 31U)   /* (RW)  Domain Control enable for this register (allow access to bits 23:0 only to specified domains) */
#define CHIP_REGFLD_SRC_RCR_LOCK                    (0x00000001UL << 30U)   /* (RW1) Domain control bits lock (disable modify for bits 31 and 27:24) */
#define CHIP_REGFLD_SRC_RCR_DOMAIN(n)               (0x00000001UL << (24U + (n)))  /* (RW) Domain n (0..3) assignment control. Effective when dom_en is set to 1. Allow masters from domain n write to bits 23:0 of this register*/

/*********** SRC Reset Control Register (SRC_SCR) *****************************/
#define CHIP_REGFLD_SRC_SCR_MASK_TEMPSENSE_RST      (0x0000000FUL <<  4U)   /* (RW)  Mask tempsense_reset source (0xA->0x5 to mask) */
#define CHIP_REGMSK_SRC_SCR_RESERVED                (0x30FFFF0FUL)


/*********** A53 Reset Control Register 0 (SRC_A53RCR0) ***********************/
#define CHIP_REGFLD_SRC_A53RCR0_A53_L2_RST          (0x00000001UL << 21U)   /* (RW1SC)  Software reset for A53 Snoop Control Unit (SCU). */
#define CHIP_REGFLD_SRC_A53RCR0_SOC_DBG_RST         (0x00000001UL << 20U)   /* (RW1SC)  Software reset for system level debug reset. */
#define CHIP_REGFLD_SRC_A53RCR0_MASK_WDOG1_RST      (0x0000000FUL << 16U)   /* (RW)     Mask wdog1_rst_b source (0xA->0x5 to mask). */
#define CHIP_REGFLD_SRC_A53RCR0_A53_ETM_RST(n)      (0x00000001UL << (12U + (n))) /* (RW1SC)  Software reset for core n (0..3) ETM only. */
#define CHIP_REGFLD_SRC_A53RCR0_A53_DBG_RST(n)      (0x00000001UL << (8U + (n)))  /* (RW1SC)  Software reset for core n (0..3) debug only. */
#define CHIP_REGFLD_SRC_A53RCR0_A53_CORE_RST(n)     (0x00000001UL << (4U + (n)))  /* (RW1SC)  Software reset for core n (0..3) only. */
#define CHIP_REGFLD_SRC_A53RCR0_A53_POR_RST(n)      (0x00000001UL << (0U + (n)))  /* (RW1SC)  POR reset for core n (0..3) only. */
#define CHIP_REGMSK_SRC_A53RCR0_RESERVED            (0x30C00000UL)

/*********** A53 Reset Control Register 1 (SRC_A53RCR1) ***********************/
#define CHIP_REGFLD_SRC_A53RCR1_A53_RST_SLOW        (0x00000007UL <<  4U)   /* (RW) ?? */
#define CHIP_REGFLD_SRC_A53RCR1_A53_CORE_EN(n)      (0x00000001UL <<  (0U + (n))) /* (RW/(core0 - RO)) core n (0..3) enable */
#define CHIP_REGMSK_SRC_A53RCR1_RESERVED            (0x30FFFF80UL)

/*********** M7 Reset Control Register (SRC_M7RCR) ****************************/
#define CHIP_REGFLD_SRC_M7RCR_WDOG3_RST_OPT         (0x00000001UL <<  9U)   /* (RW) Wdog3_rst_b asserts M7 (0) or global (1) reset */
#define CHIP_REGFLD_SRC_M7RCR_WDOG3_RST_OPT_M7      (0x00000001UL <<  8U)   /* (RW) wdgo3_rst_b Reset M7 core only (0) or both M7 core and platform (1). Only for WDOG3_RST_OPT = 1 */
#define CHIP_REGFLD_SRC_M7RCR_MASK_WDOG3_RST        (0x0000000FUL <<  4U)   /* (RW) Mask wdog3_rst_b source (0xA->0x5 to mask). */
#define CHIP_REGFLD_SRC_M7RCR_M7_EN                 (0x00000001UL <<  3U)   /* (RW) Enable M7 */
#define CHIP_REGFLD_SRC_M7RCR_M7_SW_M7C_RST         (0x00000001UL <<  1U)   /* (RW1SC) Self-clearing SW reset for M7 core */
#define CHIP_REGFLD_SRC_M7RCR_M7_SW_M7C_NONSC_RST   (0x00000001UL <<  0U)   /* (RW) Non-self-clearing SW reset for M7 core */
#define CHIP_REGMSK_SRC_M7RCR_RESERVED              (0x30FFFC04UL)


/* ........... */


/*********** SRC Boot Mode Register 1 (SRC_SBMR1) *****************************/
#define CHIP_REGFLD_SRC_SBMR1_BOOT_CFG              (0x000FFFFFUL <<  0U)   /* (RO) fusemap boot config */
#define CHIP_REGMSK_SRC_SBMR1_RESERVED              (0xFFF00000UL)

/*********** SRC Reset Status Register (SRC_SRSR) *****************************/
#define CHIP_REGFLD_SRC_SRSR_TEMPSENSE_RST          (0x00000001UL <<  9U)   /* (RW1C) Temper Sensor software reset. */
#define CHIP_REGFLD_SRC_SRSR_WDOG2_RST              (0x00000001UL <<  8U)   /* (RW1C) IC Watchdog2 Time-out reset. */
#define CHIP_REGFLD_SRC_SRSR_WDOG3_RST              (0x00000001UL <<  7U)   /* (RW1C) IC Watchdog3 Time-out reset. */
#define CHIP_REGFLD_SRC_SRSR_JTAG_SW_RST            (0x00000001UL <<  6U)   /* (RW1C) JTAG software reset. */
#define CHIP_REGFLD_SRC_SRSR_JTAG_RST               (0x00000001UL <<  5U)   /* (RW1C) HIGH - Z JTAG reset. */
#define CHIP_REGFLD_SRC_SRSR_WDOG1_RST              (0x00000001UL <<  4U)   /* (RW1C) IC Watchdog1 Time-out reset. */
#define CHIP_REGFLD_SRC_SRSR_IPP_USER_RST           (0x00000001UL <<  3U)   /* (RW1C) ipp_user_reset_b qualified as COLD reset event. */
#define CHIP_REGFLD_SRC_SRSR_CSU_RST                (0x00000001UL <<  2U)   /* (RW1C) csu_reset_b event. */
#define CHIP_REGFLD_SRC_SRSR_IPP_RST                (0x00000001UL <<  0U)   /* (RW1C) ipp_reset_b pin (Power-up sequence) */
#define CHIP_REGMSK_SRC_SRSR_RESERVED               (0xFFFFFC02UL)

/*********** SRC Interrupt Status/Interrupt Mask (SRC_SISR/SRC_SIMR) **********/
#define CHIP_REGFLD_SRC_SISR_VPU_PASSED_RST         (0x00000001UL << 11U)   /* (RW1C/RW) VPU passed software reset and is ready to be used */
#define CHIP_REGFLD_SRC_SISR_GPU_PASSED_RST         (0x00000001UL << 10U)   /* (RW1C/RW) GPU passed software reset and is ready to be used */
#define CHIP_REGFLD_SRC_SISR_M7P_PASSED_RST         (0x00000001UL <<  9U)   /* (RW1C/RW) M7 platform passed software reset and is ready to be used */
#define CHIP_REGFLD_SRC_SISR_M7C_PASSED_RST         (0x00000001UL <<  8U)   /* (RW1C/RW) M7 core passed software reset and is ready to be used */
#define CHIP_REGFLD_SRC_SISR_DISPLAY_PASSED_RST     (0x00000001UL <<  7U)   /* (RW1C/RW) DISPLAY platform passed software reset and is ready to be used */
#define CHIP_REGFLD_SRC_SISR_PCIE1_PHY_PASSED_RST   (0x00000001UL <<  5U)   /* (RW1C/RW) PCIE1 PHY passed software reset and is ready to be used */
#define CHIP_REGFLD_SRC_SISR_USB_PHY2_PASSED_RST    (0x00000001UL <<  3U)   /* (RW1C/RW) USB PHY2 passed software reset and is ready to be used */
#define CHIP_REGFLD_SRC_SISR_USB_PHY1_PASSED_RST    (0x00000001UL <<  2U)   /* (RW1C/RW) USB PHY1 passed software reset and is ready to be used */
#define CHIP_REGMSK_SRC_SISR_RESERVED               (0xFFFFF053UL)

/*********** SRC Boot Mode Register 2 (SRC_SBMR2) *****************************/
#define CHIP_REGFLD_SRC_SBMR2_IPP_BOOT_MODE         (0x0000000FUL << 24U)   /* (RO) Latched BOOT_MODE3..0 signals */
#define CHIP_REGFLD_SRC_SBMR2_FORCE_COLD_BOOT       (0x00000007UL <<  5U)   /* (RO) From fusemap */
#define CHIP_REGFLD_SRC_SBMR2_BT_FUSE_SEL           (0x00000001UL <<  4U)   /* (RO) BT_FUSE_SEL fuse */
#define CHIP_REGFLD_SRC_SBMR2_SEC_CONFIG            (0x00000003UL <<  0U)   /* (RO) SEC_CONFIG1..0 fuse */
#define CHIP_REGMSK_SRC_SBMR2_RESERVED              (0xF0FFFF0CUL)

/* ........... */

#endif // REGS_SRC_H
