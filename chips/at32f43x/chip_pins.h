#ifndef STM32G0XX_CHIP_PIN_H
#define STM32G0XX_CHIP_PIN_H



#define CHIP_PIN_USE_OSPEED_VHIGH
#include "chips/shared/stm32/chip_pin_defs.h"
#include "chip_devtype_spec_features.h"
#include "chip_config.h"

#define CHIP_PORT_A 0
#define CHIP_PORT_B 1
#define CHIP_PORT_C 2
#define CHIP_PORT_D 3
#define CHIP_PORT_E 4
#define CHIP_PORT_F 5
#define CHIP_PORT_G 6
#define CHIP_PORT_H 7
#define CHIP_PORT_I 8



/* ------------------------ Функции пинов ------------------------------------*/

/* ------------------------------- Порт A ------------------------------------*/
/* Пин PA0  */
#define CHIP_PIN_PA0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 0) */
#define CHIP_PIN_PA0_TIM2_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 1)
#define CHIP_PIN_PA0_TIM2_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 1)
#define CHIP_PIN_PA0_TIM5_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 2)
#define CHIP_PIN_PA0_TIM8_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 6) */
#define CHIP_PIN_PA0_USART2_CTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 7)
#define CHIP_PIN_PA0_UART4_TX             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 8)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 9) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 10) */
#if CHIP_DEV_SUPPORT_ETH
#define CHIP_PIN_PA0_ETH_MII_CRS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 11)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 14) */
#define CHIP_PIN_PA0_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 0, 15)


/* Пин PA1 */
#define CHIP_PIN_PA1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 0) */
#define CHIP_PIN_PA1_TIM2_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 1)
#define CHIP_PIN_PA1_TIM5_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 2)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 4) */
#if defined CHIP_DEV_GD32F4XX && (CHIP_DEV_SPI_CNT >= 4)
#define CHIP_PIN_PA1_SPI4_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 5)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 6) */
#define CHIP_PIN_PA1_USART2_RTS           CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 7)
#define CHIP_PIN_PA1_UART4_RX             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 8)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 9) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 10) */
#if CHIP_DEV_SUPPORT_ETH
#define CHIP_PIN_PA1_ETH_MII_RX_CLK       CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 11)
#define CHIP_PIN_PA1_ETH_RMII_REF_CLK     CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 11)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 14) */
#define CHIP_PIN_PA1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 1, 15)


/* Пин PA2 */
#define CHIP_PIN_PA2_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 2)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 0) */
#define CHIP_PIN_PA2_TIM2_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 1)
#define CHIP_PIN_PA2_TIM5_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 2)
#define CHIP_PIN_PA2_TIM9_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 4) */
#ifdef CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA2_I2S_CKIN             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 5)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 6) */
#define CHIP_PIN_PA2_USART2_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 7)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 8) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 9) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 10) */
#if CHIP_DEV_SUPPORT_ETH
#define CHIP_PIN_PA2_ETH_MDIO             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 11)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 13) */
#if defined CHIP_DEV_AT32F43X
#define CHIP_PIN_PA2_EXMC_D4              CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 14)
#endif
#define CHIP_PIN_PA2_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 2, 15)


/* Пин PA3 */
#define CHIP_PIN_PA3_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 0) */
#define CHIP_PIN_PA3_TIM2_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 1)
#define CHIP_PIN_PA3_TIM5_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 2)
#define CHIP_PIN_PA3_TIM9_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 4) */
#if defined CHIP_DEV_GD32F4XX || defined CHIP_DEV_AT32F43X
#define CHIP_PIN_PA3_I2S2_MCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 5)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 6) */
#define CHIP_PIN_PA3_USART2_RX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 7)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 8) */
#if defined CHIP_DEV_AT32F43X
#define CHIP_PIN_PA3_QSPI2_IO3_ULPI_D0    CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 9)
#define CHIP_PIN_PA3_SDIO2_CMD            CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 10)
#else
#define CHIP_PIN_PA3_USBHS_ULPI_D0        CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 10)
#endif
#define CHIP_PIN_PA3_ETH_MII_COL          CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 11)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 13) */
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PA3_LCD_B5               CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 14)
#elif defined CHIP_DEV_AT32F43X
#define CHIP_PIN_PA3_EXMC_D5              CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 14)
#endif
#define CHIP_PIN_PA3_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 3, 15)

/* Пин PA4 */
#define CHIP_PIN_PA4_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 0) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 1) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 4) */
#define CHIP_PIN_PA4_SPI1_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 5)
#define CHIP_PIN_PA4_SPI3_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 6)
#define CHIP_PIN_PA4_I2S3_WS              CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 6)
#define CHIP_PIN_PA4_USART2_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 7)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 8) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 9) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 10) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 11) */
#define CHIP_PIN_PA4_USBHS_SOF            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 12)
#define CHIP_PIN_PA4_DVP_HSYNC            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 13)
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PA4_LCD_VSYNC            CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 14)
#endif
#define CHIP_PIN_PA4_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 4, 15)

/* Пин PA5 */
#define CHIP_PIN_PA5_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 5)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 0) */
#define CHIP_PIN_PA5_TIM2_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 1)
#define CHIP_PIN_PA5_TIM2_ETR             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 2) */
#define CHIP_PIN_PA5_TIM8_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 4) */
#define CHIP_PIN_PA5_SPI1_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 5)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 7) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 8) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 9) */
#define CHIP_PIN_PA5_USBHS_ULPI_CK        CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 11) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 14) */
#define CHIP_PIN_PA5_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 5, 15)


/* Пин PA6 */
#define CHIP_PIN_PA6_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 6)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 0) */
#define CHIP_PIN_PA6_TIM1_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 1)
#define CHIP_PIN_PA6_TIM3_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 2)
#define CHIP_PIN_PA6_TIM8_BKIN            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 4) */
#define CHIP_PIN_PA6_SPI1_MISO            CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 5)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 7) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 8) */
#define CHIP_PIN_PA6_TM13_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 9)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 10) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 11) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 12) */
#define CHIP_PIN_PA6_DVP_PIXCLK           CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 13)
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PA6_LCD_G2               CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 14)
#endif
#define CHIP_PIN_PA6_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 6, 15)


/* Пин PA7 */
#define CHIP_PIN_PA7_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 7)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 0) */
#define CHIP_PIN_PA7_TIM1_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 1)
#define CHIP_PIN_PA7_TIM3_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 2)
#define CHIP_PIN_PA7_TIM8_CH1N            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 4) */
#define CHIP_PIN_PA7_SPI1_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 5)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 6) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 7) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 8) */
#define CHIP_PIN_PA7_TIM14_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 9)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 10) */
#define CHIP_PIN_PA7_ETH_MII_RX_DV        CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 11)
#define CHIP_PIN_PA7_ETH_RMII_CRS_DV      CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 11)
#if defined CHIP_DEV_GD32F4XX && CHIP_DEV_SUPPORT_SDRAM
#define CHIP_PIN_PA7_EXMC_SDNWE           CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 12)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 14) */
#define CHIP_PIN_PA7_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 7, 15)


/* Пин PA8 */
#define CHIP_PIN_PA8_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 8)
#define CHIP_PIN_PA8_MCO1                 CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 0)
#define CHIP_PIN_PA8_TIM1_CH1             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 3) */
#define CHIP_PIN_PA8_I2C3_SCL             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 4)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 5) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 6) */
#define CHIP_PIN_PA8_USART1_CK            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 7)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 8) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA8_CRS_SYNC             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 9)*/
#endif
#define CHIP_PIN_PA8_USBFS_SOF            CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 11) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA8_SDIO_D1              CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 12)*/
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 13) */
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PA8_LCD_R6               CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 14)
#endif
#define CHIP_PIN_PA8_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 8, 15)


/* Пин PA9 */
#define CHIP_PIN_PA9_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 9)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 0) */
#define CHIP_PIN_PA9_TIM1_CH2             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 3) */
#define CHIP_PIN_PA9_I2C3_SMBA            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 4)
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA9_SPI2_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 5)
#define CHIP_PIN_PA9_I2S2_CK              CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 5)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 6) */
#define CHIP_PIN_PA9_USART1_TX            CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 7)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 8) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 9) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 10) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 11) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA9_SDIO_D2              CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 12)
#endif
#define CHIP_PIN_PA9_DVP_D0               CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 13)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 14) */
#define CHIP_PIN_PA9_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_A, 9, 15)


/* Пин PA10 */
#define CHIP_PIN_PA10_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 0) */
#define CHIP_PIN_PA10_TIM1_CH3            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 3) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA10_I2C3_TXFRAME        CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 4)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 5) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA10_SPI5_MOSI           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 6)
#endif

#define CHIP_PIN_PA10_USART1_RX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 7)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 8) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 9) */
#define CHIP_PIN_PA10_USBFS_ID            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 11) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 12) */
#define CHIP_PIN_PA10_DVP_D1              CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 13)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 14) */
#define CHIP_PIN_PA10_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 10, 15)


/* Пин PA11 */
#define CHIP_PIN_PA11_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 11)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 0) */
#define CHIP_PIN_PA11_TIM1_CH4            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 5) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA11_SPI4_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 6)
#endif
#define CHIP_PIN_PA11_USART1_CTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 7)
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA11_USART6_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 8)
#endif
#define CHIP_PIN_PA11_CAN1_RX             CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 9)
#define CHIP_PIN_PA11_USBFS_DM            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 11) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 13) */
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PA11_LCD_R4              CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 14)
#endif
#define CHIP_PIN_PA11_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 11, 15)


/* Пин PA12 */
#define CHIP_PIN_PA12_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 12)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 0) */
#define CHIP_PIN_PA12_TIM1_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 5) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA12_SPI5_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 6)
#endif
#define CHIP_PIN_PA12_USART1_RTS          CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 7)
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA12_SPI5_MISO           CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 8)
#endif
#define CHIP_PIN_PA12_CAN1_TX             CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 9)
#define CHIP_PIN_PA12_USBFS_DP            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 10)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 11) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 13) */
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PA12_LCD_R5              CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 14)
#endif
#define CHIP_PIN_PA12_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 12, 15)


/* Пин PA13 */
#define CHIP_PIN_PA13_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 13)
#define CHIP_PIN_PA13_JTMS_SWDIO          CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 0)
#define CHIP_PIN_PA13_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 13, 15)


/* Пин PA14 */
#define CHIP_PIN_PA14_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 14)
#define CHIP_PIN_PA14_JTCK_SWCLK          CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 0)
#define CHIP_PIN_PA14_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 14, 15)


/* Пин PA15 */
#define CHIP_PIN_PA15_GPIO                CHIP_PIN_ID_GPIO  (CHIP_PORT_A, 15)
#define CHIP_PIN_PA15_JTDI                CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 0)
#define CHIP_PIN_PA15_TIM2_CH1            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1)
#define CHIP_PIN_PA15_TIM2_ETR            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 2) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 3) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 4) */
#define CHIP_PIN_PA15_SPI1_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 5)
#define CHIP_PIN_PA15_SPI3_NSS            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 6)
#define CHIP_PIN_PA15_SPI3_WC             CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 6)
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PA15_USART1_TX           CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 7)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 8) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 9) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 10) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 11) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 12) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 14) */
#define CHIP_PIN_PA15_EVENTOUT            CHIP_PIN_ID_AF    (CHIP_PORT_A, 15, 15)


/* ------------------------------- Порт B ------------------------------------*/
/* Пин PB0  */
#define CHIP_PIN_PB0_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 0)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 0) */
#define CHIP_PIN_PB0_TIM2_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 1)
#define CHIP_PIN_PB0_TIM3_CH3             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 2)
#define CHIP_PIN_PB0_TIM8_CH2N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 5) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PB0_SPI5_SCK             CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 6)
#define CHIP_PIN_PB0_SPI3_MOSI            CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 7)
#define CHIP_PIN_PB0_I2S3_SD              CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 7)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 8) */
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PB0_LCD_R3               CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 9)
#endif
#define CHIP_PIN_PB0_USBHS_ULPI_D1        CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 10)
#define CHIP_PIN_PB0_USBHS_ETH_MII_RXD2   CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 11)
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PB0_USBHS_SDIO_D1        CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 12)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 14) */
#define CHIP_PIN_PB0_USBHS_EVENTOUT       CHIP_PIN_ID_AF    (CHIP_PORT_B, 0, 15)


/* Пин PB1  */
#define CHIP_PIN_PB1_GPIO                 CHIP_PIN_ID_GPIO  (CHIP_PORT_B, 1)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 0) */
#define CHIP_PIN_PB1_TIM1_CH3N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 1)
#define CHIP_PIN_PB1_TIM3_CH4             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 2)
#define CHIP_PIN_PB1_TIM8_CH3N            CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 3)
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 4) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 5) */
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PB1_SPI5_NSS             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 6)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 7) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 8) */
#if CHIP_DEV_SUPPORT_LCD
#define CHIP_PIN_PB1_LCD_R6               CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 9)
#endif
#define CHIP_PIN_PB1_USBHS_ULPI_D2        CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 10)
#define CHIP_PIN_PB1_ETH_MII_RXD3         CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 11)
#if defined CHIP_DEV_GD32F4XX
#define CHIP_PIN_PB1_USBHS_SDIO_D2        CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 12)
#endif
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 13) */
/* -----                                  CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 14) */
#define CHIP_PIN_PB1_EVENTOUT             CHIP_PIN_ID_AF    (CHIP_PORT_B, 1, 15)







#endif // STM32G0XX_CHIP_PIN_H
