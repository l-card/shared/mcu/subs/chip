#ifndef REGS_GPT_H
#define REGS_GPT_H

#include "chip_ioreg_defs.h"

#define CHIP_GPT_OUTPUT_CNT     3
#define CHIP_GPT_INPUT_CNT      2

typedef struct {
    __IO uint32_t CR;                                /**< GPT Control Register, offset: 0x0 */
    __IO uint32_t PR;                                /**< GPT Prescaler Register, offset: 0x4 */
    __IO uint32_t SR;                                /**< GPT Status Register, offset: 0x8 */
    __IO uint32_t IR;                                /**< GPT Interrupt Register, offset: 0xC */
    __IO uint32_t OCR[CHIP_GPT_OUTPUT_CNT];          /**< GPT Output Compare Register 1..GPT Output Compare Register 3, array offset: 0x10, array step: 0x4 */
    __I  uint32_t ICR[CHIP_GPT_INPUT_CNT];           /**< GPT Input Capture Register 1..GPT Input Capture Register 2, array offset: 0x1C, array step: 0x4 */
    __I  uint32_t CNT;                               /**< GPT Counter Register, offset: 0x24 */
} CHIP_REGS_GPT_T;

#define CHIP_REGS_GPT1           ((CHIP_REGS_GPT_T *)CHIP_MEMRGN_ADDR_PERIPH_GPT1)
#define CHIP_REGS_GPT2           ((CHIP_REGS_GPT_T *)CHIP_MEMRGN_ADDR_PERIPH_GPT2)
#define CHIP_REGS_GPT3           ((CHIP_REGS_GPT_T *)CHIP_MEMRGN_ADDR_PERIPH_GPT3)
#define CHIP_REGS_GPT4           ((CHIP_REGS_GPT_T *)CHIP_MEMRGN_ADDR_PERIPH_GPT4)
#define CHIP_REGS_GPT5           ((CHIP_REGS_GPT_T *)CHIP_MEMRGN_ADDR_PERIPH_GPT5)
#define CHIP_REGS_GPT6           ((CHIP_REGS_GPT_T *)CHIP_MEMRGN_ADDR_PERIPH_GPT6)


/*********** GPT Control Register (GPT_CR) ************************************/
#define CHIP_REGFLD_GPT_CR_FO(n)                    (0x00000001UL << (29U + (n)))   /* (W1SC) Force Output Compare for Channel n (n=0..2) */
#define CHIP_REGFLD_GPT_CR_OM(n)                    (0x00000007UL << (20U + 3*(n))) /* (RW) Output Compare Operating Mode for Channel n (n=0..2) */
#define CHIP_REGFLD_GPT_CR_IM(n)                    (0x00000003UL << (16U + 2*(n))) /* (RW) Input Capture Operating Mode for Channel n (n=0..1) */
#define CHIP_REGFLD_GPT_CR_SWR                      (0x00000001UL << 15U) /* (W1SC) Software Reset (except for the DOZEEN, EN, ENMOD, STOPEN, WAITEN, and DBGEN bits). */
#define CHIP_REGFLD_GPT_CR_EN_24M                   (0x00000001UL << 10U) /* (RW) Enable Oscillator Clock Input (24 MHz) */
#define CHIP_REGFLD_GPT_CR_FRR                      (0x00000001UL <<  9U) /* (RW) Free-Run (1) or Restart (0) Mode */
#define CHIP_REGFLD_GPT_CR_CLKSRC                   (0x00000007UL <<  6U) /* (RW) Clock Source Select */
#define CHIP_REGFLD_GPT_CR_STOPEN                   (0x00000001UL <<  5U) /* (RW) GPT Stop Mode Enable */
#define CHIP_REGFLD_GPT_CR_DOZEEN                   (0x00000001UL <<  4U) /* (RW) GPT Doze Mode Enable */
#define CHIP_REGFLD_GPT_CR_WAITEN                   (0x00000001UL <<  3U) /* (RW) GPT Wait Mode Enable */
#define CHIP_REGFLD_GPT_CR_DBGEN                    (0x00000001UL <<  2U) /* (RW) GPT Debug Mode Enable */
#define CHIP_REGFLD_GPT_CR_ENMOD                    (0x00000001UL <<  1U) /* (RW) GPT Enable Mode (restart from frozen (0) or zero (1)) */
#define CHIP_REGFLD_GPT_CR_EN                       (0x00000001UL <<  0U) /* (RW) GPT Enable */


#define CHIP_REGFLDVAL_GPT_CR_OM_DIS                0 /* Output disabled. No response on pin. */
#define CHIP_REGFLDVAL_GPT_CR_OM_TOGGLE             1 /* Toggle output pin. */
#define CHIP_REGFLDVAL_GPT_CR_OM_CLEAR              2 /* Clear output pin. */
#define CHIP_REGFLDVAL_GPT_CR_OM_SET                3 /* Set output pin. */
#define CHIP_REGFLDVAL_GPT_CR_OM_PULSE              4 /* Generate a low pulse that is one input clock cycle wide on the output pin. */

#define CHIP_REGFLDVAL_GPT_CR_IM_DIS                0 /* Capture disabled. */
#define CHIP_REGFLDVAL_GPT_CR_IM_RISE               1 /* Capture on rising edge only. */
#define CHIP_REGFLDVAL_GPT_CR_IM_FALL               2 /* Capture on falling edge only. */
#define CHIP_REGFLDVAL_GPT_CR_IM_BOTH               3 /* Capture on both edges. */

#define CHIP_REGFLDVAL_GPT_CR_CLKSRC_NOCLK          0 /* No clock */
#define CHIP_REGFLDVAL_GPT_CR_CLKSRC_IPG            1 /* Peripheral Clock (ipg_clk) */
#define CHIP_REGFLDVAL_GPT_CR_CLKSRC_IPG_HF         2 /* High Frequency Reference Clock (ipg_clk_highfreq) */
#define CHIP_REGFLDVAL_GPT_CR_CLKSRC_EXT            3 /* External Clock */
#define CHIP_REGFLDVAL_GPT_CR_CLKSRC_32K            4 /* Low Frequency Reference Clock (ipg_clk_32k) */
#define CHIP_REGFLDVAL_GPT_CR_CLKSRC_OSC            5 /* Oscillator as Reference Clock (24M) */


/*********** GPT Prescaler Register (PR) **************************************/
#define CHIP_REGFLD_GPT_PR_PRESCALER24              (0x0000000FUL << 12U) /* (RW) Prescaler divide value for the oscillator clock (0x0 - 1 .. 0xF - 16) */
#define CHIP_REGFLD_GPT_PR_PRESCALER                (0x00000FFFUL <<  0U) /* (RW)  Prescaler divide value (0x000 - 1 .. 0xFFF - 4096)*/


/*********** GPT Status Register / Interrupt Register (SR/IR) *****************/
#define CHIP_REGFLD_GPT_SR_ROV                      (0x00000001UL <<  5U) /* (RW1C/RW) Rollover Flag */
#define CHIP_REGFLD_GPT_SR_IF(n)                    (0x00000001UL << (3U + (n))) /* (RW1C/RW) Input Capture Flag for Channel n (n = 0..1)*/
#define CHIP_REGFLD_GPT_SR_OF(n)                    (0x00000001UL << (0U + (n))) /* (RW1C/RW) Output Compare Flag for Channel n (n = 0..2) */


#endif // REGS_GPT_H
