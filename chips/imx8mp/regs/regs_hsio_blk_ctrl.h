#ifndef REGS_HSIO_BLK_CTRL_H
#define REGS_HSIO_BLK_CTRL_H

#include "chip_ioreg_defs.h"

typedef struct {
    __IO uint32_t GPR_REG0;                          /**< Clock select reset and debug info select, offset: 0x0 */
    __I  uint32_t GPR_REG1;                          /**< PCIE controller status, offset: 0x4 */
    __IO uint32_t GPR_REG2;                          /**< PLL configuration 0, offset: 0x8 */
    __IO uint32_t GPR_REG3;                          /**< PLL configuration 1, offset: 0xC */
    __IO uint32_t GPR_REG4;                          /**< PCIE PME message and error detect register, offset: 0x10 */
    __IO uint32_t GPR_REG5;                          /**< PCIE PME message and error detect interrupt enable register, offset: 0x14 */
    __IO uint32_t GPR_REG6;                          /**< PCIE PME message and error detect interrupt detect disable register, offset: 0x18 */
    __IO uint32_t GPR_REG7;                          /**< USB1 beat limit and enable, offset: 0x1C */
    __IO uint32_t GPR_REG8;                          /**< USB2 beat limit and enable, offset: 0x20 */
    __IO uint32_t GPR_REG9;                          /**< PCIE beat limit and enable, offset: 0x24 */
    uint8_t RESERVED_0[216];
    __IO uint32_t USB1_WAKEUP_CTRL;                  /**< Register for USB1 wakeup, offset: 0x100 */
    __I  uint32_t USB1_WAKEUP_STATUS;                /**< Status of USB1 wakeup, offset: 0x104 */
    __IO uint32_t USB2_WAKEUP_CTRL;                  /**< Register for USB2 wakeup, offset: 0x108 */
    __I  uint32_t USB2_WAKEUP_STATUS;                /**< Status of USB2 wakeup, offset: 0x10C */
}  CHIP_REGS_HSIO_BLK_CTRL_T;


#define CHIP_REGS_HSIO_BLK_CTRL     ((CHIP_REGS_HSIO_BLK_CTRL_T *)CHIP_MEMRGN_ADDR_PERIPH_HSIO_BLK_CTRL)

/*********** Clock select reset and debug info select (GPR_REG0) **************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG0_CRS_CLEAR                    (0x00000001UL <<  8U) /* (RW) Clear CSR (PCIe  config retry status) interrupt */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG0_CFG_READY                    (0x00000001UL <<  7U) /* (RW) Configuration ready. Must be set after configuration PCIe controller */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG0_USB_PHY_REF_CLK_SEL          (0x00000001UL <<  6U) /* (RW) USB (or PCIe??) PHY ref clock selection */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG0_USB_CLOCK_MODULE_EN          (0x00000001UL <<  1U) /* (RW) USB related clock enable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG0_PCIE_CLOCK_MODULE_EN         (0x00000001UL <<  0U) /* (RW) PCIE related clock enable */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG0_RESERVED                     (0xFFFFFE3CUL)

#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG0_USB_PHY_REF_CLK_SEL_24_OSC   0 /* 24Mhz exteral osc */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG0_USB_PHY_REF_CLK_SEL_100_PLL  1 /* 100Mhz high performace PLL */


/*********** PCIE controller status (GPR_REG1) ********************************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG1_PLL_LOCK                     (0x00000001UL << 13U) /* (RO) High performance PLL lock status */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG1_PCIE_CTRL_PM_DSTATE          (0x00000007UL << 10U) /* (RO) PCIE ctrl's pm dstate */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG1_PCIE_CTRL_PM_LINKST_IN_L0S   (0x00000001UL <<  9U) /* (RO) PCIE ctrl link in l0s state */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG1_PCIE_CTRL_PM_LINKST_IN_L1    (0x00000001UL <<  8U) /* (RO) PCIE ctrl link in l1 state */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG1_PCIE_CTRL_PM_LINKST_IN_L1SUB (0x00000001UL <<  7U) /* (RO) PCIE ctrl link in l1sub state */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG1_SMLH_LTSSM_STATE             (0x0000003FUL <<  1U) /* (RO) PCIE link state */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG1_PM_EN_CORE_CLK               (0x00000001UL <<  0U) /* (RO) pm_en_core_clk pin status of pcie ctrl */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG1_RESERVED                     (0xFFFFC000UL)


/*********** PLL configuration 0 (GPR_REG2) ***********************************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_AFC_ENB_PLL                  (0x00000001UL << 31U) /* (RW) AFC_ENB input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_LOCK_CON_REV_PLL             (0x00000003UL << 29U) /* (RW) Lock con rev pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_LOCK_CON_DLY_PLL             (0x00000003UL << 27U) /* (RW) Lock con delay input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_LOCK_CON_OUT_PLL             (0x00000003UL << 25U) /* (RW) Lock con input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_LOCK_CON_IN_PLL              (0x00000003UL << 23U) /* (RW) Lock con in pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_LOCK_EN_PLL                  (0x00000001UL << 22U) /* (RW) locken pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_BYPASS_PLL                   (0x00000001UL << 21U) /* (RW) Bypass pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_ICP_PLL                      (0x00000003UL << 19U) /* (RW) ICP pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_S_PLL                        (0x00000007UL << 16U) /* (RW) S pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_M_PLL                        (0x000003FFUL <<  6U) /* (RW) M pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG2_P_PLL                        (0x0000003FUL <<  0U) /* (RW) P pin input of high performance PLL */


/*********** PLL configuration 1 (GPR_REG3) ***********************************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_PLL_RESETB                   (0x00000001UL << 31U) /* (RW) reset pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_PLL_EXT_BYPASS               (0x00000001UL << 18U) /* (RW) PLL ext bypass pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_PLL_CKE                      (0x00000001UL << 17U) /* (RW) PLL cke pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_RSEL_PLL                     (0x0000000FUL << 13U) /* (RW) RSEL pin input of high performance PL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_LRD_EN_PLL                   (0x00000001UL << 12U) /* (RW) LRD EN pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_PBIAS_CTRL_PLL               (0x00000001UL << 11U) /* (RW) PBIAS CTRL pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_PBIAS_CTRL_EN_PLL            (0x00000001UL << 10U) /* (RW) PBIAS CTRL EN pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_VCO_BOOST_PLL                (0x00000001UL <<  9U) /* (RW) VCO BOOST pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_FOUT_MASK_PLL                (0x00000001UL <<  8U) /* (RW) FOUT MASK pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_AFCINIT_SEL_PLL              (0x00000001UL <<  7U) /* (RW) AFCINT SEL input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_FSEL_PLL                     (0x00000001UL <<  6U) /* (RW) FSEL pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_FEED_EN_PLL                  (0x00000001UL <<  5U) /* (RW) Feed en pin input of high performance PLL */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG3_EXTAFC_PLL                   (0x0000001FUL <<  0U) /* (RW) Extafc pin input of high performance PLL */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG3_RESERVED                     (0x7FF80000UL)


/*********** PCIE PME message and error detect register (GPR_REG4) ************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_INTM                         (0x00000001UL << 31U) /* (RO) Per PF dependent message interrupt is pending */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_INTE                         (0x00000001UL << 30U) /* (RO) Per PF dependent error interrupt is pending */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_ME                           (0x00000001UL <<  8U) /* (RW1C) Multiple errors of same type was detected. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_PCT                          (0x00000001UL <<  7U) /* (RW1C) Completion timeout was detected. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_PCAC                         (0x00000001UL <<  6U) /* (RW1C) Completer abort was detected. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_CDNSC                        (0x00000001UL <<  5U) /* (RW1C) Completion with data not succsessful was detected */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_UREP                         (0x00000001UL <<  4U) /* (RW1C) Unsupported request in EP mode was detected */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_PTO                          (0x00000001UL <<  3U) /* (RW1C) PME turn off was detected */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_HRD                          (0x00000001UL <<  2U) /* (RW1C) hot reset was detected */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_LDD                          (0x00000001UL <<  1U) /* (RW1C) link down was detected */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG4_LUD                          (0x00000001UL <<  0U) /* (RW1C) link up was detected. */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG4_RESERVED                     (0x3FFFFE00UL)


/*********** PCIE PME message and error detect interrupt enable register (GPR_REG5) */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_PCT_IE                       (0x00000001UL <<  7U) /* (RW) Completion timeout interrupt enable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_PCAC_IE                      (0x00000001UL <<  6U) /* (RW) Completer abort interrupt enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_CDNSC_IE                     (0x00000001UL <<  5U) /* (RW) Completion with data not succsessful interrupt enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_UREP_IE                      (0x00000001UL <<  4U) /* (RW) Unsupported request in EP mode interrupt enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_PTO_IE                       (0x00000001UL <<  3U) /* (RW) PME turn off detect interrupt enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_HRD_IE                       (0x00000001UL <<  2U) /* (RW) Hot reset detect interrupt enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_LDD_IE                       (0x00000001UL <<  1U) /* (RW) Link down detect interrupt enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG5_LUD_IE                       (0x00000001UL <<  0U) /* (RW) Link up detect interrupt enable. */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG5_RESERVED                     (0xFFFFFF00UL)


/*********** PCIE PME message and error detect interrupt detect disable register (GPR_REG6) */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_ME_DIS                       (0x00000001UL <<  8U) /* (RW) Multiple errors of same type detection disable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_PCT_DIS                      (0x00000001UL <<  7U) /* (RW) Completion detection disable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_PCAC_DIS                     (0x00000001UL <<  6U) /* (RW) Completer abort detection disable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_CDNSC_DIS                    (0x00000001UL <<  5U) /* (RW) Completion with data not succsessful detection disable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_UREP_DIS                     (0x00000001UL <<  4U) /* (RW) Unsupported request in EP mode detection disable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_PTO_DIS                      (0x00000001UL <<  3U) /* (RW) PME turn off detect disabled. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_HRD_DIS                      (0x00000001UL <<  2U) /* (RW) Hot reset detect disable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_LDD_DIS                      (0x00000001UL <<  1U) /* (RW) Link down detect disable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG6_LUD_DIS                      (0x00000001UL <<  0U) /* (RW) Link up detect disable. */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG6_RESERVED                     (0xFFFFFE00UL)


/*********** USB1 beat limit and enable (GPR_REG7) ****************************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG7_USB1_BEAT_LIMIT_EN           (0x00000001UL << 16U) /* (RW) USB1 beat limit enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG7_USB1_BEAT_LIMIT              (0x0000FFFFUL <<  0U) /* (RW) USB1 beat limit number. */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG7_RESERVED                     (0xFFFE0000UL)


/*********** USB2 beat limit and enable (GPR_REG8) ****************************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG8_USB2_BEAT_LIMIT_EN           (0x00000001UL << 16U) /* (RW) USB2 beat limit enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG8_USB2_BEAT_LIMIT              (0x0000FFFFUL <<  0U) /* (RW) USB2 beat limit number. */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG8_RESERVED                     (0xFFFE0000UL)


/*********** PCIE beat limit and enable (GPR_REG9) ****************************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG9_PCIE_BEAT_LIMIT_EN           (0x00000001UL << 16U) /* (RW) PCIE beat limit enable. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_GPR_REG9_PCIE_BEAT_LIMIT              (0x0000FFFFUL <<  0U) /* (RW) PCIE beat limit number. */
#define CHIP_REGMSK_HSIO_BLK_CTRL_GPR_REG9_RESERVED                     (0xFFFE0000UL)


/*********** Register for USBa wakeup (USBx_WAKEUP_CTRL) **********************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_OTG_WAKE_ENABLE               (0x00000001UL << 31U) /* (RW) Global wakeup interrupt enable. Used to clear interrupt. Default 0, same as WIE before. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_PHY_BYPASS_SEL                (0x00000001UL << 16U) /* (RW) Transmitter Digital Bypass Select. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_PHY_BYPASS_DP_EN              (0x00000001UL << 15U) /* (RW) DP Transmitter Digital Bypass Enable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_PHY_BYPASS_DP_DATA            (0x00000001UL << 14U) /* (RW) Data for DP Transmitter Digital Bypass */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_PHY_BYPASS_DM_EN              (0x00000001UL << 13U) /* (RW) DM Transmitter Digital Bypass Enable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_PHY_BYPASS_DM_DATA            (0x00000001UL << 12U) /* (RW) Data for DM Transmitter Digital Bypass */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_LOWSPEED_EN                   (0x00000001UL << 11U) /* (RW) Enable lowspeed autoresume feature */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_AUTORESUME_EN_DLY             (0x00000001UL << 10U) /* (RW) Tuning the timing between dp/dm data and enable signal when autoresume finish */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_AUTORESUME_DATA_DLY           (0x00000001UL <<  9U) /* (RW) Tuning the timing between dp/dm data and enable signal when autoresume finish */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_AUTORESUME_EN                 (0x00000001UL <<  8U) /* (RW) Enable autoresume feature. When using phy_bypass signal, it should be 1'b0. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_OTG_CONN_WAKEUP_EN            (0x00000001UL <<  5U) /* (RW) Enable signal for wakeup from connection or disconnection, only for superspeed. */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_OTG_VBUS_SOURCE_SEL           (0x00000001UL <<  4U) /* (RW) otg_vbus_source_sel (0 -  vbus_valid, 1 - sessvld) */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_OTG_U3_WAKE_EN                (0x00000001UL <<  3U) /* (RW) Enable signal for wake up from u3 state, only for superspeed */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_OTG_ID_WAKEUP_EN              (0x00000001UL <<  2U) /* (RW) Enable signal for wake up from id change */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_OTG_VBUS_WAKE_EN              (0x00000001UL <<  1U) /* (RW) Enable signal for wake up from vbus valid or session valid changes */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_OTG_WK_DPDM_CHG_EN            (0x00000001UL <<  0U) /* (RW) Enable signal for wake up from dp/dm change. */
#define CHIP_REGMSK_HSIO_BLK_CTRL_USB_WAKEUP_CTRL_RESERVED                      (0x7FFE00C0UL)

/*********** RStatus of USBa wakeup (USBx_WAKEUP_STATUS) **********************/
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_WAKEUP_INT              (0x00000001UL << 31U) /* (RO) OR of all wakeup interrupt, then AND with otg_wakeup_enable */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_CONN_WAKEUP_INT         (0x00000001UL << 13U) /* (RO) Interrupt status of connection */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_PIPE3_POWERDOWN             (0x00000003UL << 11U) /* (RO) Pipe powerdown */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_HOST_MODE               (0x00000001UL << 10U) /* (RO) USB drd mode indicator (1 - host, 0 - devie) */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_PIPE_RXELECIDLE             (0x00000001UL <<  9U) /* (RO) pipe_rxelecidel, wakeup source */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_PHY_OTGSESSVLD0         (0x00000001UL <<  8U) /* (RO) session valid, wakeup source */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_PHY_VBUSVALID0          (0x00000001UL <<  7U) /* (RO) vbus valid, wakeup source */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_PHY_IDDIG0              (0x00000001UL <<  6U) /* (RO) ID status, wakeup source */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_PHY_LINESTATE0_1        (0x00000001UL <<  5U) /* (RO) linestate[1], wakeup source */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_PHY_LINESTATE0_0        (0x00000001UL <<  4U) /* (RO) linestate[0], wakeup source */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_U3_WAKEUP_INT           (0x00000001UL <<  3U) /* (RO) Interrupt status of wakeup from u3 state */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_ID_WAKEUP_INT           (0x00000001UL <<  2U) /* (RO) Interrupt status of wakeup from id */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_VBUS_SESSVLD_WAKEUP_INT (0x00000001UL <<  1U) /* (RO) Interrupt status of vbus or session valid signal. otg_vbus_wakeup_interrupt | otg_sessvld_wakeup_interrupt OTG_VBUS_SE */
#define CHIP_REGFLD_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_OTG_DP_DM_WAKEUP_INT        (0x00000001UL <<  0U) /* (RO) Interrupt status of dm or dp, otg_dp_wakeup_interrupt | otg_dm_wakeup_interrupt*/
#define CHIP_REGMSK_HSIO_BLK_CTRL_USB_WAKEUP_STATUS_RESERVED                    (0x7FFFC000UL)


#endif // REGS_HSIO_BLK_CTRL_H
