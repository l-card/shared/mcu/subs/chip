#ifndef REGS_IWDG_H
#define REGS_IWDG_H


#define CHIP_SUPPORT_IWDG_WINDOW        0

#include "stm32_iwdg_regs.h"

#define CHIP_REGS_IWDG          ((CHIP_REGS_IWDG_T *) CHIP_MEMRGN_ADDR_PERIPH_IWDG)


#endif // REGS_IWDG_H
