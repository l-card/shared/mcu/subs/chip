#ifndef CHIP_STM32H7XX_REGS_EXTI_H_
#define CHIP_STM32H7XX_REGS_EXTI_H_

#include <stdint.h>
#include "chip_devtype_spec_features.h"

#if CHIP_DEV_SUPPORT_CORE_CM4
    #define CHIP_EXTI_CORE_BLOCK_CNT 2
#else
    #define CHIP_EXTI_CORE_BLOCK_CNT 1
#endif


#if CHIP_DEV_SUPPORT_CORE_CM4 && defined CHIP_CORETYPE_CM4
    #define CHIP_EXTI_CUR_CORE_NUM 1
#else
    #define CHIP_EXTI_CUR_CORE_NUM 0
#endif


typedef struct {
    __IO uint32_t RTSR;               /*!< EXTI Rising trigger selection register,          Address offset: 0x00 */
    __IO uint32_t FTSR;               /*!< EXTI Falling trigger selection register,         Address offset: 0x04 */
    __IO uint32_t SWIER;              /*!< EXTI Software interrupt event register,          Address offset: 0x08 */
    __IO uint32_t D3PMR;              /*!< EXTI D3 Pending mask register, (same register as to SRDPMR1) Address offset: 0x0C */
    union {
        struct {
            __IO uint32_t D3PCRL;     /*!< EXTI D3 Pending clear selection register low, (same register as to SRDPCR1L)     Address offset: 0x10 */
            __IO uint32_t D3PCRH;     /*!< EXTI D3 Pending clear selection register High, (same register as to SRDPCR1H)   Address offset: 0x14 */
        };
        __IO uint32_t D3PCR[2];
    };
    uint32_t      RESERVED[2];        /*!< Reserved,                                        0x18 to 0x1C         */
} CHIP_REGS_EXTI_COMMON_BLOCK_T;

typedef struct {
    __IO uint32_t IMR;                /*!< EXTI Interrupt mask register,                    Address offset: 0x80 */
    __IO uint32_t EMR;                /*!< EXTI Event mask register,                        Address offset: 0x84 */
    __IO uint32_t PR;                 /*!< EXTI Pending register,                           Address offset: 0x88 */
    uint32_t      RESERVED;           /*!< Reserved,                                        0x8C                 */
} CHIP_REGS_EXTI_CORE_BLOCK_T;


typedef struct {
    union {
        struct {
            __IO uint32_t RTSR1;               /*!< EXTI Rising trigger selection register,          Address offset: 0x00 */
            __IO uint32_t FTSR1;               /*!< EXTI Falling trigger selection register,         Address offset: 0x04 */
            __IO uint32_t SWIER1;              /*!< EXTI Software interrupt event register,          Address offset: 0x08 */
            __IO uint32_t D3PMR1;              /*!< EXTI D3 Pending mask register, (same register as to SRDPMR1) Address offset: 0x0C */
            __IO uint32_t D3PCR1L;             /*!< EXTI D3 Pending clear selection register low, (same register as to SRDPCR1L)     Address offset: 0x10 */
            __IO uint32_t D3PCR1H;             /*!< EXTI D3 Pending clear selection register High, (same register as to SRDPCR1H)   Address offset: 0x14 */
            uint32_t      RESERVED1[2];        /*!< Reserved,                                        0x18 to 0x1C         */
            __IO uint32_t RTSR2;               /*!< EXTI Rising trigger selection register,          Address offset: 0x20 */
            __IO uint32_t FTSR2;               /*!< EXTI Falling trigger selection register,         Address offset: 0x24 */
            __IO uint32_t SWIER2;              /*!< EXTI Software interrupt event register,          Address offset: 0x28 */
            __IO uint32_t D3PMR2;              /*!< EXTI D3 Pending mask register, (same register as to SRDPMR2) Address offset: 0x2C */
            __IO uint32_t D3PCR2L;             /*!< EXTI D3 Pending clear selection register low, (same register as to SRDPCR2L)  Address offset: 0x30 */
            __IO uint32_t D3PCR2H;             /*!< EXTI D3 Pending clear selection register High, (same register as to SRDPCR2H) Address offset: 0x34 */
            uint32_t      RESERVED2[2];        /*!< Reserved,                                        0x38 to 0x3C         */
            __IO uint32_t RTSR3;               /*!< EXTI Rising trigger selection register,          Address offset: 0x40 */
            __IO uint32_t FTSR3;               /*!< EXTI Falling trigger selection register,         Address offset: 0x44 */
            __IO uint32_t SWIER3;              /*!< EXTI Software interrupt event register,          Address offset: 0x48 */
            __IO uint32_t D3PMR3;              /*!< EXTI D3 Pending mask register, (same register as to SRDPMR3) Address offset: 0x4C */
            __IO uint32_t D3PCR3L;             /*!< EXTI D3 Pending clear selection register low, (same register as to SRDPCR3L) Address offset: 0x50 */
            __IO uint32_t D3PCR3H;             /*!< EXTI D3 Pending clear selection register High, (same register as to SRDPCR3H) Address offset: 0x54 */
            uint32_t      RESERVED3[10];       /*!< Reserved,                                        0x58 to 0x5C         */
        };
        CHIP_REGS_EXTI_COMMON_BLOCK_T COMMON[4];
    };
    union {
        struct {
            __IO uint32_t IMR1;                /*!< EXTI Interrupt mask register,                    Address offset: 0x80 */
            __IO uint32_t EMR1;                /*!< EXTI Event mask register,                        Address offset: 0x84 */
            __IO uint32_t PR1;                 /*!< EXTI Pending register,                           Address offset: 0x88 */
            uint32_t      RESERVED5;           /*!< Reserved,                                        0x8C                 */
            __IO uint32_t IMR2;                /*!< EXTI Interrupt mask register,                    Address offset: 0x90 */
            __IO uint32_t EMR2;                /*!< EXTI Event mask register,                        Address offset: 0x94 */
            __IO uint32_t PR2;                 /*!< EXTI Pending register,                           Address offset: 0x98 */
            uint32_t      RESERVED6;           /*!< Reserved,                                        0x9C                 */
            __IO uint32_t IMR3;                /*!< EXTI Interrupt mask register,                    Address offset: 0xA0 */
            __IO uint32_t EMR3;                /*!< EXTI Event mask register,                        Address offset: 0xA4 */
            __IO uint32_t PR3;                 /*!< EXTI Pending register,                           Address offset: 0xA8 */
            uint32_t      RESERVED7[5];        /*!< Reserved,                                        0xAC to 0xBC         */
#if CHIP_DEV_SUPPORT_CORE_CM4
            __IO uint32_t C2IMR1;              /*!< EXTI Interrupt mask register,                    Address offset: 0xC0 */
            __IO uint32_t C2EMR1;              /*!< EXTI Event mask register,                        Address offset: 0xC4 */
            __IO uint32_t C2PR1;               /*!< EXTI Pending register,                           Address offset: 0xC8 */
            uint32_t      RESERVED8;           /*!< Reserved,                                        0xCC                 */
            __IO uint32_t C2IMR2;              /*!< EXTI Interrupt mask register,                    Address offset: 0xD0 */
            __IO uint32_t C2EMR2;              /*!< EXTI Event mask register,                        Address offset: 0xD4 */
            __IO uint32_t C2PR2;               /*!< EXTI Pending register,                           Address offset: 0xD8 */
            uint32_t      RESERVED9;           /*!< Reserved,                                        0xDC                 */
            __IO uint32_t C2IMR3;              /*!< EXTI Interrupt mask register,                    Address offset: 0xE0 */
            __IO uint32_t C2EMR3;              /*!< EXTI Event mask register,                        Address offset: 0xE4 */
            __IO uint32_t C2PR3;               /*!< EXTI Pending register,                           Address offset: 0xE8 */
#endif
        };
        struct {
            CHIP_REGS_EXTI_CORE_BLOCK_T CM7[4];
#if CHIP_DEV_SUPPORT_CORE_CM4
            CHIP_REGS_EXTI_CORE_BLOCK_T CM4[4];
#endif
        };
        CHIP_REGS_EXTI_CORE_BLOCK_T CORE[CHIP_EXTI_CORE_BLOCK_CNT][4];
    };
} CHIP_REGS_EXTI_T;


#define CHIP_REGS_EXTI                          ((CHIP_REGS_EXTI_T *) CHIP_MEMRGN_ADDR_PERIPH_EXTI)


/* номера входов событий для блока EXTI */
#define CHIP_EXTI_EVTIN_EXTI(x)                 (x)     /* EXTI[15:0]         - Configurable / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_PVD_AVD                 16      /* PVD and AVD        - Configurable / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_RTC_ALARM               17      /* RTC alarms         - Configurable / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_TAMP_STAMP              18      /* RTC tamper, RTC timestamp, RCC LSECSS - Configurable / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_RTC_WKUP                19      /* RTC wakeup timer   - Configurable / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_COMP1                   20      /* COMP1              - Configurable / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_COMP2                   21      /* COMP2              - Configurable / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_I2C1_WKUP               22      /* I2C1 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_I2C2_WKUP               23      /* I2C2 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_I2C3_WKUP               24      /* I2C3 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_I2C4_WKUP               25      /* I2C4 wakeup        - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_USART1_WKUP             26      /* USART1 wakeup      - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_USART2_WKUP             27      /* USART2 wakeup      - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_USART3_WKUP             28      /* USART3 wakeup      - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_USART6_WKUP             29      /* USART6 wakeup      - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_UART4_WKUP              30      /* UART4 wakeup       - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_UART5_WKUP              31      /* UART5 wakeup       - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_UART7_WKUP              32      /* UART7 wakeup       - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_UART8_WKUP              33      /* UART8 wakeup       - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_LPUART1_RX_WKUP         34      /* LPUART1 RX wakeup  - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_LPUART1_TX_WKUP         35      /* LPUART1 TX wakeup  - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_SPI1_WKUP               36      /* SPI1 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_SPI2_WKUP               37      /* SPI2 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_SPI3_WKUP               38      /* SPI3 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_SPI4_WKUP               39      /* SPI4 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_SPI5_WKUP               40      /* SPI5 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_SPI6_WKUP               41      /* SPI6 wakeup        - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_MDIO_WKUP               42      /* MDIO wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_USB1_WKUP               43      /* USB1 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#if CHIP_DEV_SUPPORT_USB2
#define CHIP_EXTI_EVTIN_USB2_WKUP               44      /* USB2 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#endif
/* 45 - Reserved */
#if CHIP_DEV_SUPPORT_DSI
#define CHIP_EXTI_EVTIN_DSI_WKUP                46      /* DSI wakeup         - Direct       / CPU1 or CPU2 / Event to NVIC */
#endif
#define CHIP_EXTI_EVTIN_LPTIM1_WKUP             47      /* LPTIM1 wakeup      - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_LPTIM2_WKUP             48      /* LPTIM2 wakeup      - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_LPTIM2_OUT              49      /* LPTIM2 output      - Configurable / Any          / Event only */
#define CHIP_EXTI_EVTIN_LPTIM3_WKUP             50      /* LPTIM3 wakeup      - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_LPTIM3_OUT              51      /* LPTIM3 output      - Configurable / Any          / Event only */
#define CHIP_EXTI_EVTIN_LPTIM4_WKUP             52      /* LPTIM4 wakeup      - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_LPTIM5_WKUP             53      /* LPTIM5 wakeup      - Direct       / Any          / Event to NVIC */
#define CHIP_EXTI_EVTIN_SWPMI_WKUP              54      /* SWPMI wakeup       - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_WKUP(x)                 (55+(x))/* WKUP[1-6] pin      - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_RCC_IT                  61      /* RCC interrupt      - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_I2C4_EV_IT              62      /* I2C4 Event int     - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_I2C4_ER_IT              63      /* I2C4 Error int     - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_LPUART1_GLOB_IT         64      /* LPUART1 global int - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_SPI6_IT                 65      /* SPI6 interrupt     - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_BDMA_CH_IT(x)           (66+(x))/* BDMA CH[0-7] int   - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_DMAMUX2_IT              74      /* SDMAMUX2 int       - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_ADC3_IT                 75      /* ADC3 interrupt     - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_SAI4_IT                 76      /* SAI4 interrupt     - Direct       / CPU1 or CPU2 / NVIC Directly */
#define CHIP_EXTI_EVTIN_HSEM1_IT                77      /* HSEM0 interrupt    - Direct       / CPU1 only    / NVIC Directly */
#if CHIP_DEV_SUPPORT_CORE_CM4
#define CHIP_EXTI_EVTIN_HSEM2_IT                78      /* HSEM1 interrupt    - Direct       / CPU2 only    / NVIC Directly */
#define CHIP_EXTI_EVTIN_CM4_SEV_IT              79      /* CortexM4 SEV int   - Direct       / CPU1 only    / Event only */
#define CHIP_EXTI_EVTIN_CM7_SEV_IT              80      /* CortexM7 SEV int   - Direct       / CPU2 only    / Event only */
/* 81 - Reserved */
#define CHIP_EXTI_EVTIN_WWDG1_RST               82      /* WWDG1 reset        - Configurable / CPU2 only    / Event to NVIC */
/* 83 - Reserved */
#define CHIP_EXTI_EVTIN_WWDG2_RST               84      /* WWDG1 reset        - Configurable / CPU1 only    / Event to NVIC */
#endif
#define CHIP_EXTI_EVTIN_HDMI_CEC_WKUP           85      /* HDMI-CEC wakeup    - Configurable / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_ETH_WKUP                86      /* ETHERNET wakeup    - Configurable / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_HSECSS_IT               87      /* HSECSS interrupt   - Direct       / CPU1 or CPU2 / NVIC Directly */
#if CHIP_DEV_SUPPORT_DTS
#define CHIP_EXTI_EVTIN_TEMP_WKUP               88      /* TEMP wakeup        - Direct       / Any          / Event to NVIC */
#endif
#if CHIP_DEV_SUPPORT_USART9_10
#define CHIP_EXTI_EVTIN_UART9_WKUP              89      /* UART9 wakeup       - Direct       / CPU1 or CPU2 / Event to NVIC */
#define CHIP_EXTI_EVTIN_USART10_WKUP            90      /* USART10 wakeup     - Direct       / CPU1 or CPU2 / Event to NVIC */
#endif
#if CHIP_DEV_SUPPORT_I2C5
#define CHIP_EXTI_EVTIN_I2C5_WKUP               91      /* I2C5 wakeup        - Direct       / CPU1 or CPU2 / Event to NVIC */
#endif


/* общие определения номеров регистров и битов для входов событий из CHIP_EXTI_EVTIN_xxx */

/* для большинства регистров на один источник используется один бит и в каждом блоке
 * COMMON[] и CORE[] присутствует один регистр с выбранной функцией на 32 события */
#define CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)         ((x) / 32) /* номер блока регистров COMMON[] или CORE[][cpu] для данного номера события */
#define CHIP_EXTI_EVTIN_BITMSK(x)               (0x00000001UL << ((x) % 32)) /* битовая маска в регистре для данного номера события */
/* для D3 pending clear selection источник задается двумя битами, в результате
 * в каждом блоке по два регистра, каждый на 16 источников */
#define CHIP_EXTI_D3PCS_REGBLOCK_NUM(x)         ((x) / 32)
#define CHIP_EXTI_D3PCS_BLOCK_REGNUM(x)         (((x) % 32) / 16)
#define CHIP_EXTI_D3PCS_BITMSK(x)               (0x00000003UL << (((x) % 16) * 2))



#define CHIP_EXTI_EVTIN_CFG_RISE(x, en)         LBITFIELD_UPD(CHIP_REGS_EXTI->COMMON[CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].RTSR, CHIP_EXTI_EVTIN_BITMSK(x), (en ? 1 : 0))
#define CHIP_EXTI_EVTIN_CFG_FALL(x, en)         LBITFIELD_UPD(CHIP_REGS_EXTI->COMMON[CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].FTSR, CHIP_EXTI_EVTIN_BITMSK(x), (en ? 1 : 0))
#define CHIP_EXTI_EVTIN_SW_TRIG(x)              (CHIP_REGS_EXTI->COMMON[CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].SWIER = LBITFIELD_SET(CHIP_EXTI_EVTIN_BITMSK(x), 1))
#define CHIP_EXTI_EVTIN_CORE_INT_EN(x, core)    (CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].IMR |= CHIP_EXTI_EVTIN_BITMSK(x))
#define CHIP_EXTI_EVTIN_CORE_INT_DIS(x, core)   (CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].IMR &= ~CHIP_EXTI_EVTIN_BITMSK(x))
#define CHIP_EXTI_EVTIN_CORE_INT_IS_EN(x, core) ((CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].IMR &= CHIP_EXTI_EVTIN_BITMSK(x)) != 0)
#define CHIP_EXTI_EVTIN_CORE_EVT_EN(x, core)    (CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].EMR |= CHIP_EXTI_EVTIN_BITMSK(x))
#define CHIP_EXTI_EVTIN_CORE_EVT_DIS(x, core)   (CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].EMR &= ~CHIP_EXTI_EVTIN_BITMSK(x))
#define CHIP_EXTI_EVTIN_CORE_EVT_IS_EN(x, core) ((CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].EMR &= CHIP_EXTI_EVTIN_BITMSK(x)) != 0)
#define CHIP_EXTI_EVTIN_CORE_IS_PEND(x, core)   ((CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].PR &= CHIP_EXTI_EVTIN_BITMSK(x)) != 0)
#define CHIP_EXTI_EVTIN_CORE_PEND_CLR(x, core)  (CHIP_REGS_EXTI->CORE[core][CHIP_EXTI_EVTIN_REGBLOCK_NUM(x)].PR = CHIP_EXTI_EVTIN_BITMSK(x))


#define CHIP_EXTI_EVTIN_INT_EN(x)               CHIP_EXTI_EVTIN_CORE_INT_EN(x, CHIP_EXTI_CUR_CORE_NUM)
#define CHIP_EXTI_EVTIN_INT_DIS(x)              CHIP_EXTI_EVTIN_CORE_INT_DIS(x, CHIP_EXTI_CUR_CORE_NUM)
#define CHIP_EXTI_EVTIN_INT_IS_EN(x)            CHIP_EXTI_EVTIN_CORE_INT_IS_EN(x, CHIP_EXTI_CUR_CORE_NUM)
#define CHIP_EXTI_EVTIN_EVT_EN(x)               CHIP_EXTI_EVTIN_CORE_EVT_EN(x, CHIP_EXTI_CUR_CORE_NUM)
#define CHIP_EXTI_EVTIN_EVT_DIS(x)              CHIP_EXTI_EVTIN_CORE_EVT_DIS(x, CHIP_EXTI_CUR_CORE_NUM)
#define CHIP_EXTI_EVTIN_EVT_IS_EN(x)            CHIP_EXTI_EVTIN_CORE_EVT_IS_EN(x, CHIP_EXTI_CUR_CORE_NUM)
#define CHIP_EXTI_EVTIN_IS_PEND(x)              CHIP_EXTI_EVTIN_CORE_IS_PEND(x, CHIP_EXTI_CUR_CORE_NUM)
#define CHIP_EXTI_EVTIN_PEND_CLR(x)             CHIP_EXTI_EVTIN_CORE_PEND_CLR(x, CHIP_EXTI_CUR_CORE_NUM)



/*********** EXTI rising trigger selection register (EXTI_RTSRx) **************/
#define CHIP_REGFLD_EXTI_RTSR_TR(x)             (0x00000001UL <<  CHIP_EXTI_BIT_NUM(x)) /* Rising trigger event configuration bit of Configurable Event input x */

/*********** EXTI falling trigger selection register (EXTI_FTSRx) *************/
#define CHIP_REGFLD_EXTI_FTSR_TR(x)             (0x00000001UL <<  CHIP_EXTI_BIT_NUM(x)) /* Falling trigger event configuration bit of Configurable Event input x */

/*********** EXTI software interrupt event register (EXTI_SWIERx) *************/
#define CHIP_REGFLD_EXTI_SWIER_SWIER(x)         (0x00000001UL <<  CHIP_EXTI_BIT_NUM(x)) /* Software interrupt on line x */

/*********** EXTI D3 pending mask register (EXTI_D3PMRx) **********************/
#define CHIP_REGFLD_EXTI_D3PMR_MR(x)            (0x00000001UL <<  CHIP_EXTI_BIT_NUM(x)) /* D3 Pending Mask on Event input x */

/*********** EXTI D3 pending clear selection register low/high (EXTI_D3PCRxL/EXTI_D3PCRxH) */
#define CHIP_REGFLD_EXTI_D3PCR_PCS(x)           (0x00000003UL <<  CHIP_EXTI_PCS_BIT_NUM(x)) /* D3 Pending request clear input signal selection on Event input x */

#define CHIP_REGFLDVAL_EXTI_D3PCR_PCS_DMA_CH6   0
#define CHIP_REGFLDVAL_EXTI_D3PCR_PCS_DMA_CH7   1
#define CHIP_REGFLDVAL_EXTI_D3PCR_PCS_LPTIM4    2
#define CHIP_REGFLDVAL_EXTI_D3PCR_PCS_LPTIM5    3


/*********** EXTI interrupt mask register (EXTI_CnIMRx) ***********************/
#define CHIP_REGFLD_EXTI_IMR_MR(x)              (0x00000001UL <<  CHIP_EXTI_BIT_NUM(x)) /*  CPUn interrupt Mask on Event input x */

/*********** EXTI event mask register (EXTI_CnEMRx) ***************************/
#define CHIP_REGFLD_EXTI_EMR_MR(x)              (0x00000001UL <<  CHIP_EXTI_BIT_NUM(x)) /*  CPUn Event Mask on input x */

/*********** EXTI pending register (EXTI_CnPRx) *******************************/
#define CHIP_REGFLD_EXTI_PR_PR(x)               (0x00000001UL <<  CHIP_EXTI_BIT_NUM(x)) /*  Configurable event inputs x Pending bit */



#endif // CHIP_STM32H7XX_REGS_EXTI_H_
