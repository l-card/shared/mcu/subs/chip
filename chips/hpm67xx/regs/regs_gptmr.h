#ifndef REGS_GPTMR_H
#define REGS_GPTMR_H

#include "chip_ioreg_defs.h"

#define CHIP_GPTMR_CHANNEL_CNT            4

typedef struct {
    __IO uint32_t CR;                      /* 0x0: Control Register */
    __IO uint32_t CMP[2];                  /* 0x4 - 0x8: Comparator register 0 */
    __IO uint32_t RLD;                     /* 0xC: Reload register */
    __IO uint32_t CNTUPTVAL;               /* 0x10: Counter update value register */
    __I  uint8_t  RESERVED0[12];           /* 0x14 - 0x1F: Reserved */
    __I  uint32_t CAPPOS;                  /* 0x20: Capture rising edge register */
    __I  uint32_t CAPNEG;                  /* 0x24: Capture falling edge register */
    __I  uint32_t CAPPRD;                  /* 0x28: PWM period measure register */
    __I  uint32_t CAPDTY;                  /* 0x2C: PWM duty cycle measure register */
    __I  uint32_t CNT;                     /* 0x30: Counter */
    __I  uint8_t  RESERVED1[12];           /* 0x34 - 0x3F: Reserved */
} CHIP_REGS_GPTMR_CHANNEL_T;



typedef struct {
    CHIP_REGS_GPTMR_CHANNEL_T CHANNEL[CHIP_GPTMR_CHANNEL_CNT];
    __I  uint8_t  RESERVED0[256];              /* 0x100 - 0x1FF: Reserved */
    __IO uint32_t SR;                          /* 0x200: Status register */
    __IO uint32_t IRQEN;                       /* 0x204: Interrupt request enable register */
    __IO uint32_t GCR;                         /* 0x208: Global control register */
} CHIP_REGS_GPTMR_T;


#define CHIP_REGS_GPTMR0            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR0)
#define CHIP_REGS_GPTMR1            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR1)
#define CHIP_REGS_GPTMR2            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR2)
#define CHIP_REGS_GPTMR3            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR3)
#define CHIP_REGS_GPTMR4            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR4)
#define CHIP_REGS_GPTMR5            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR5)
#define CHIP_REGS_GPTMR6            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR6)
#define CHIP_REGS_GPTMR7            ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_GPTMR7)
#define CHIP_REGS_NTMR0             ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_NTMR0)
#define CHIP_REGS_NTMR1             ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_NTMR1)
#define CHIP_REGS_PTMR              ((CHIP_REGS_GPTMR_T *)CHIP_MEMRGN_ADDR_PTMR)


/*********** GPTMR_CH_CR - Channel Control Register ***************************/
#define CHIP_REGFLD_GPTMR_CH_CR_CNTUPT                  (0x00000001UL << 31U) /* (RW1SC) Update the counter value to the value of the CNTUPTVAL register */
#define CHIP_REGFLD_GPTMR_CH_CR_CNTRST                  (0x00000001UL << 14U) /* (RW) Reset counter */
#define CHIP_REGFLD_GPTMR_CH_CR_SYNCFLW                 (0x00000001UL << 13U) /* (RW) The channel counter will be reloaded with the previous channel counter */
#define CHIP_REGFLD_GPTMR_CH_CR_SYNCIFEN                (0x00000001UL << 12U) /* (RW) Timer synchronization input SYNCI falling edge is valid */
#define CHIP_REGFLD_GPTMR_CH_CR_SYNCIREN                (0x00000001UL << 11U) /* (RW) Timer synchronization input SYNCI rising edge is valid */
#define CHIP_REGFLD_GPTMR_CH_CR_CEN                     (0x00000001UL << 10U) /* (RW) Enable counter */
#define CHIP_REGFLD_GPTMR_CH_CR_CMPINIT                 (0x00000001UL <<  9U) /* (RW) Output comparison initial polarity */
#define CHIP_REGFLD_GPTMR_CH_CR_CMPEN                   (0x00000001UL <<  8U) /* (RW) Enable the output comparison function of this channel */
#define CHIP_REGFLD_GPTMR_CH_CR_DMASEL                  (0x00000003UL <<  6U) /* (RW) DMA request selection bits */
#define CHIP_REGFLD_GPTMR_CH_CR_DMAEN                   (0x00000001UL <<  5U) /* (RW) Enable DMA */
#define CHIP_REGFLD_GPTMR_CH_CR_SWSYNCIEN               (0x00000001UL <<  4U) /* (RW) Enable software counter synchronization. When this bit is set, the counter will be reloaded when the SWSYNCT bit is set. */
#define CHIP_REGFLD_GPTMR_CH_CR_DBGPAUSE                (0x00000001UL <<  3U) /* (RW) The counter will be paused in debug mode. */
#define CHIP_REGFLD_GPTMR_CH_CR_CAPMODE                 (0x00000007UL <<  0U) /* (RW) Input capture mode configuration bits. */


#define CHIP_REGFLDVAL_GPTMR_CH_CR_DMASEL_CMP0          0 /* Generate DMA request when comparator 0 flag CMP0 is set to 1 */
#define CHIP_REGFLDVAL_GPTMR_CH_CR_DMASEL_CMP1          1 /* Generate DMA request when comparator 1 flag CMP1 is set to 1 */
#define CHIP_REGFLDVAL_GPTMR_CH_CR_DMASEL_ICAP          2 /* Generate DMA request when input capture signal toggles */
#define CHIP_REGFLDVAL_GPTMR_CH_CR_DMASEL_RLD           3 /* Generate DMA request when reload flag RLD is set to 1 */

#define CHIP_REGFLDVAL_GPTMR_CH_CR_CAPMODE_PWM          4 /* PWM measurement mode, the timer will measure the period and duty cycle of the input PWM */
#define CHIP_REGFLDVAL_GPTMR_CH_CR_CAPMODE_CAP_BOTH     3 /* Capture rising and falling edges */
#define CHIP_REGFLDVAL_GPTMR_CH_CR_CAPMODE_CAP_FALL     2 /* Capture falling edge */
#define CHIP_REGFLDVAL_GPTMR_CH_CR_CAPMODE_CAP_RISE     1 /* Capture rising edge */
#define CHIP_REGFLDVAL_GPTMR_CH_CR_CAPMODE_OFF          0 /* Turn off input capture */

/*********** GPTMR_SR - Status register ***************************************/
#define CHIP_REGFLD_GPTMR_SR_CH_CMP(ch,cmp)             (0x00000001UL << (4U * (ch) + 2 + (cmp))) /* (RW1C) Channel (ch) comparator (cmp) flag, ch=0..3, cmp=0..1 */
#define CHIP_REGFLD_GPTMR_SR_CH_CAP(ch)                 (0x00000001UL << (4U * (ch) + 1))         /* (RW1C) Channel (ch) input capture flag, ch=0..3 */
#define CHIP_REGFLD_GPTMR_SR_CH_RLD(ch)                 (0x00000001UL << (4U * (ch)))             /* (RW1C) Channel (ch)eload flag, ch=0..3 */

/*********** GPTMR_IRQEN - Interrupt request enable register ******************/
#define CHIP_REGFLD_GPTMR_IRQEN_CH_CMP(ch,cmp)          (0x00000001UL << (4U * (ch) + 2 + (cmp))) /* (RW) Channel (ch) comparator (cmp) flag  interrupt enable, ch=0..3, cmp=0..1 */
#define CHIP_REGFLD_GPTMR_IRQEN_CH_CAP(ch)              (0x00000001UL << (4U * (ch) + 1))         /* (RW) Channel (ch) input capture flag interrupt enable, ch=0..3 */
#define CHIP_REGFLD_GPTMR_IRQEN_CH_RLD(ch)              (0x00000001UL << (4U * (ch)))             /* (RW) Channel (ch)eload flag interrupt enable, ch=0..3 */




#endif // REGS_GPTMR_H
