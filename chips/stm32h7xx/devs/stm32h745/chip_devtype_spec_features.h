#ifndef CHIP_DEVTYPE_SPEC_FEATURES_H
#define CHIP_DEVTYPE_SPEC_FEATURES_H

#include "chip_dev_spec_features.h"

#define CHIP_REV_Y_COMPAT_EN                    0 /* ревизии начались сразу с V */

#define CHIP_DEV_SUPPORT_CORE_CM4               1
#define CHIP_DEV_SUPPORT_ITCM_AXI_SHARE         0
#define CHIP_DEV_SUPPORT_FLASH_BANK2            1 /* multibank */
#define CHIP_DEV_SUPPORT_FLASH_MER              1 /* mass erase */
#define CHIP_DEV_SUPPORT_SRAM3                  1
#define CHIP_DEV_SUPPORT_SMPS                   1
#define CHIP_DEV_SUPPORT_SEP_CSICFG             1 /* выделенный регистр калибровки CSI */
#define CHIP_DEV_SUPPORT_DSI                    0
#define CHIP_DEV_SUPPORT_JPGDEC                 1
#define CHIP_DEV_SUPPORT_HRTIM                  1
#define CHIP_DEV_SUPPORT_OCTOSPI                0
#define CHIP_DEV_SUPPORT_CPURST                 0
#define CHIP_DEV_SUPPORT_SAI2_3                 1
#define CHIP_DEV_SUPPORT_USART9_10              0
#define CHIP_DEV_SUPPORT_USB2                   1
#define CHIP_DEV_SUPPORT_FMAC                   0 /* Filter math accelerator */
#define CHIP_DEV_SUPPORT_CORDIC                 0
#define CHIP_DEV_SUPPORT_PORTI                  1
#define CHIP_DEV_SUPPORT_I2C5                   0
#define CHIP_DEV_SUPPORT_TIM23_24               0
#define CHIP_DEV_SUPPORT_DTS                    0 /* Digital temperature sensor  */
#define CHIP_DEV_SUPPORT_FDCAN3                 0
#define CHIP_DEV_SUPPORT_PSSI                   0
#define CHIP_DEV_SUPPORT_D2_RSTCTL              1 /* управление сбросом домена D2 */
#define CHIP_DEV_SUPPORT_ADC2_ALTCON            0 /* ADC2 alternate connection */
#define CHIP_DEV_SUPPORT_CPUFREQ_BOOST          0
#define CHIP_DEV_SUPPORT_I2S6                   0
#define CHIP_DEV_SUPPORT_TIM_BKI                1 /* TIM Break Input Support */
#define CHIP_DEV_SUPPORT_PLL1_DIVP_ODD          0 /* нечетные значения делителя DIVP PLL1 */
#define CHIP_DEV_SUPPORT_SPDIFRX_SYM_FREQGEN    0 /* генерация spdifrx_sym_ck для SAI4 */
#define CHIP_DEV_SUPPORT_WKUP_3_5               1 /* наличие пинов WKUP3 и WKUP5 */

#define CHIP_DEV_DFSDM1_ALTPOS                  0 /* бит управления DFSDM1 смещен из 28 в 30 и изменен базовый адрес DFSDM1 */



#define CHIP_DEV_TIM_GP_CNT             8
#define CHIP_DEV_TIM_GP32_CNT           2
#define CHIP_DEV_TIM_ADV_CNT            2
#define CHIP_DEV_TIM_BASIC_CNT          2
#define CHIP_DEV_SPI_CNT                6
#define CHIP_DEV_I2S_CNT                3
#define CHIP_DEV_I2S_FD_CNT             3
#define CHIP_DEV_I2C_CNT                4
#define CHIP_DEV_UART_CNT               9
#define CHIP_DEV_CAN_CNT                1
#define CHIP_DEV_USB_CNT                1
#define CHIP_DEV_SDIO_CNT               2
#define CHIP_DEV_ETH_CNT                1
#define CHIP_DEV_ADC_CNT                3
#define CHIP_DEV_DAC_CNT                2
#define CHIP_DEV_QSPI_CNT               1
#define CHIP_DEV_DVP_CNT                1
#define CHIP_DEV_USBHS_CNT              1
#define CHIP_DEV_SAI_CNT                2


#endif // CHIP_DEVTYPE_SPEC_FEATURES_H
