#include "../chip.h"

#if CHIP_USE_INTERRUPT
ISR(sec_isr);

static t_chip_isr_func f_isr[CHIP_INTERRUPT_CNT];
static volatile uint32_t *preg_cstat;
static volatile uint32_t *preg_csid;


#define SCTL_REG(_int_id_)  (*(pREG_SEC0_SCTL0 + 2*(_int_id_)))
#define CCTL_REG(_core_id_) (*((_core_id_) == 0 ? pREG_SEC0_CCTL0 : pREG_SEC0_CCTL1))


void chip_interrupt_cfg(uint8_t int_id, t_chip_isr_func isr, int prio) {
    f_isr[int_id] = isr;
    SCTL_REG(int_id) = BF_SET(BITM_SEC_SCTL_PRIO, prio) | BITM_SEC_SCTL_SEN | BF_SET(BITM_SEC_SCTL_CTG, CHIP_CUR_CORE());
}

void chip_interrupt_set_core(uint8_t int_id, int core) {
    SCTL_REG(int_id) = (SCTL_REG(int_id) & ~BITM_SEC_SCTL_CTG) | BF_SET(BITM_SEC_SCTL_CTG, core);
}

void chip_interrupt_set_group(uint8_t int_id, int group) {
    SCTL_REG(int_id) = (SCTL_REG(int_id) & ~BITM_SEC_SCTL_GRP) | BF_SET(BITM_SEC_SCTL_GRP, group);
}

void chip_interrupt_set_edge(uint8_t int_id, int edge) {
    SCTL_REG(int_id) = (SCTL_REG(int_id) & ~BITM_SEC_SCTL_ES) | BF_SET(BITM_SEC_SCTL_ES, edge);
}

void chip_interrupt_init(void) {
    if (CHIP_CUR_CORE() == 1) {
        preg_cstat = pREG_SEC0_CSTAT1;
        preg_csid = pREG_SEC0_CSID1;
    } else {
        preg_cstat = pREG_SEC0_CSTAT0;
        preg_csid = pREG_SEC0_CSID0;
    }

    if (!(*pREG_SEC0_GCTL & BITM_SEC_GCTL_EN))
        *pREG_SEC0_GCTL = BITM_SEC_GCTL_EN;

    REGISTER_ISR(11, sec_isr);
}


void chip_interrupt_enable(uint8_t int_id) {
    SCTL_REG(int_id) |= BITM_SEC_SCTL_IEN;
}

void chip_interrupt_disable(uint8_t int_id) {
    SCTL_REG(int_id) &= ~BITM_SEC_SCTL_IEN;
}

void chip_interrupt_core_enable(int core) {
    CCTL_REG(core) |= BITM_SEC_CCTL_EN;
}

void chip_interrupt_core_disable(int core) {
    CCTL_REG(core) &= ~BITM_SEC_CCTL_EN;
}

void chip_interrupt_raise(uint8_t int_id) {
    *pREG_SEC0_RAISE = int_id;
}

ISR(sec_isr) {
    if (*preg_cstat & BITM_SEC_CSTAT_SIDV) {
        uint8_t sid = *preg_csid;
        *preg_csid = 0;
        if (f_isr[sid]) {
            f_isr[sid](sid);
        }
        *pREG_SEC0_END = sid;
    }
}
#endif

#if CHIP_USE_INTERRUPT || CHIP_INTERRUPT_RESET
void chip_interrupt_reset(void) {
    *pREG_SEC0_GCTL = BITM_SEC_GCTL_RESET;
}
#endif
