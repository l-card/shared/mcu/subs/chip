#ifndef CHIP_CORE_FEATURES_H
#define CHIP_CORE_FEATURES_H



#if defined CHIP_CORETYPE_CM4
    #define CHIP_CORE_SUPPORT_FPU            1
    #define CHIP_CORE_SUPPORT_MPU            1
    #define CHIP_CORE_SUPPORT_ICACHE         0
    #define CHIP_CORE_SUPPORT_DCACHE         0

    #define CHIP_CORE_MPU_REGIONS_CNT        8
#else
    #define CHIP_CORE_SUPPORT_FPU            1
    #define CHIP_CORE_SUPPORT_MPU            1
    #define CHIP_CORE_SUPPORT_ICACHE         1
    #define CHIP_CORE_SUPPORT_DCACHE         1

    #define CHIP_CORE_MPU_REGIONS_CNT        16
    #define CHIP_CORE_ITCM_SIZE              (64*1024)
    #define CHIP_CORE_DTCM_SIZE              (128*1024)
    #define CHIP_CORE_ICACHE_SIZE            (16*1024)
    #define CHIP_CORE_DCACHE_SIZE            (16*1024)
#endif



#endif // CHIP_CORE_FEATURES_H
