#ifndef CHIP_STM32F0XX_REGS_PWR_H
#define CHIP_STM32F0XX_REGS_PWR_H

#include <stdint.h>
#include "chip_devtype_spec_features.h"

typedef struct {
    __IO uint32_t CR;   /*!< PWR power control register,                          Address offset: 0x00 */
    __IO uint32_t CSR;  /*!< PWR power control/status register,                   Address offset: 0x04 */
} CHIP_REGS_PWR_T;

#define CHIP_REGS_PWR          ((CHIP_REGS_PWR_T *) CHIP_MEMRGN_ADDR_PERIPH_PWR)

/*********** Power control register (PWR_CR) **********************************/
#define CHIP_REGFLD_PWR_CR_LPDS                 (0x00000001UL <<  0U)  /* (RW) Low Power consumption in Deepsleep mode when PDDS = 0 */
#define CHIP_REGFLD_PWR_CR_PDDS                 (0x00000001UL <<  1U)  /* (RW)  Power down deepsleep. */
#define CHIP_REGFLD_PWR_CR_CWUF                 (0x00000001UL <<  2U)  /* (RW1C) Clear wakeup flag. */
#define CHIP_REGFLD_PWR_CR_CSBF                 (0x00000001UL <<  3U)  /* (RW1C) Clear standby flag. */
#define CHIP_REGFLD_PWR_CR_PVDE                 (0x00000001UL <<  4U)  /* (RW) Power voltage detector enable. */
#define CHIP_REGFLD_PWR_CR_PLS                  (0x00000007UL <<  5U)  /* (RW) PVD level selection */
#define CHIP_REGFLD_PWR_CR_DBP                  (0x00000001UL <<  8U)  /* (RW) Disable RTC domain write protection */
#define CHIP_REGMSK_PWR_CR_RESERBED             (0xFFFFFE00UL)

/*********** Power control/status register (PWR_CSR) **************************/
#define CHIP_REGFLD_PWR_CSR_WUF                 (0x00000001UL <<  0U)  /* (RO) Wakeup flag */
#define CHIP_REGFLD_PWR_CSR_SBF                 (0x00000001UL <<  1U)  /* (RO) Standby flag */
#define CHIP_REGFLD_PWR_CSR_PVDO                (0x00000001UL <<  2U)  /* (RO) PVD output */
#if CHIP_DEV_SUPPORT_EXTERNAL_POR
#define CHIP_REGFLD_PWR_CSR_VREFINT_RDY         (0x00000001UL <<  3U)  /* (RO) VREFINT reference voltage ready */
#endif
#define CHIP_REGFLD_PWR_CSR_EWUP(x)             (0x00000001UL <<  (8U + (x)))  /* (RW) Enable WKUPx pin (x = 0..7) */
#define CHIP_REGMSK_PWR_CSR_RESERVED            (0xFFFF00F0UL)



#endif // CHIP_STM32F0XX_REGS_PWR_H
