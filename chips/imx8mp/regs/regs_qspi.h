#ifndef REGS_QSPI_H
#define REGS_QSPI_H

#include "chip_ioreg_defs.h"

#define CHIP_QSPI_AHB_RXBUF_CNT                 8
#define CHIP_QSPI_AHB_CTRL_CNT                  8
#define CHIP_QSPI_FLASH_CNT                     4
#define CHIP_QSPI_IP_RXFIFO_SIZE                32
#define CHIP_QSPI_IP_TXFIFO_SIZE                32
#define CHIP_QSPI_AHB_RXFIFO_SIZE               512
#define CHIP_QSPI_AHB_TXFIFO_SIZE               512
#define CHIP_QSPI_FIFO_WRD_SIZE                 8  /* Размер в байтах одного слова очереди чтения/pfgbcb */
#define CHIP_QSPI_LUT_SEQ_REG_CNT               4
#define CHIP_QSPI_LUT_SEQ_OP_CNT                8
#define CHIP_QSPI_LUT_SEQ_CNT                   32


#define CHIP_QSPI_FLASH_NUM_A1                  0
#define CHIP_QSPI_FLASH_NUM_A2                  1
#define CHIP_QSPI_FLASH_NUM_B1                  2
#define CHIP_QSPI_FLASH_NUM_B2                  3

#define CHIP_QSPI_LUT_KEY_VALUE                 0x5AF05AF0




typedef struct {
    __IO uint32_t MCR0;     /* Module Control 0,                                            Address offset: 0x00 */
    __IO uint32_t MCR1;     /* Module Control 1,                                            Address offset: 0x04 */
    __IO uint32_t MCR2;     /* Module Control 2,                                            Address offset: 0x08 */
    __IO uint32_t AHBCR;    /* AHB Bus Control,                                             Address offset: 0x0C */
    __IO uint32_t INTEN;    /* Interrupt Enable,                                            Address offset: 0x10 */
    __IO uint32_t INTR;     /* Interrupt,                                                   Address offset: 0x14 */
    __IO uint32_t LUTKEY;   /* LUT Key,                                                     Address offset: 0x18 */
    __IO uint32_t LUTCR;    /* LUT Control                                                  Address offset: 0x1C */
    __IO uint32_t AHBRXBUFCR0[CHIP_QSPI_AHB_RXBUF_CNT]; /* AHB Receive Buffer x Control,    Address offset: 0x20-0x3C */
    uint32_t RESERVED0[8];
    __IO uint32_t FLSHCR0[CHIP_QSPI_FLASH_CNT];         /* Flash Control 0,                 Address offset: 0x60-0x6C */
    __IO uint32_t FLSHCR1[CHIP_QSPI_FLASH_CNT];         /* Flash Control 1,                 Address offset: 0x70-0x7C */
    __IO uint32_t FLSHCR2[CHIP_QSPI_FLASH_CNT];         /* Flash Control 2,                 Address offset: 0x80-0x8C */
    uint32_t RESERVED1;
    __IO uint32_t FLSHCR4; /* Flash Control 4,                                              Address offset: 0x94 */
    uint32_t RESERVED2[2];
    __IO uint32_t IPCR0;    /* IP Control 0,                                                Address offset: 0xA0 */
    __IO uint32_t IPCR1;    /* IP Control 1,                                                Address offset: 0xA4 */
    uint32_t RESERVED3[2];
    __IO uint32_t IPCMD;    /* IP Command,                                                  Address offset: 0xB0 */
    __IO uint32_t DLPR;     /* Data Learning Pattern,                                       Address offset: 0xB4 */
    __IO uint32_t IPRXFCR;  /* IP Receive FIFO Control,                                     Address offset: 0xB8 */
    __IO uint32_t IPTXFCR;  /* IP Transmit FIFO Control,                                    Address offset: 0xBC */
    __IO uint32_t DLLCR[2]; /* DLL Control 0 (A&B),                                         Address offset: 0xC0-0xC4 */
    uint32_t RESERVED4[6];
    __I  uint32_t STS0;     /* Status 0,                                                    Address offset: 0xE0 */
    __I  uint32_t STS1;     /* Status 1,                                                    Address offset: 0xE4 */
    __I  uint32_t STS2;     /* Status 2,                                                    Address offset: 0xE8 */
    __I  uint32_t AHBSPNDSTS; /* AHB Suspend Status,                                        Address offset: 0xEC */
    __I  uint32_t IPRXFSTS;   /* IP Receive FIFO Status,                                    Address offset: 0xF0 */
    __I  uint32_t IPTXFSTS;   /* IP Transmit FIFO Status,                                   Address offset: 0xF4 */
    uint32_t RESERVED5[2];
    __I  uint32_t RFDR[CHIP_QSPI_IP_RXFIFO_SIZE]; /* IP Receive FIFO Data,                  Address offset: 0x100-0x17C */
    __O  uint32_t TFDR[CHIP_QSPI_IP_TXFIFO_SIZE]; /* IP TX FIFO Data,                       Address offset: 0x180-0x1FC */
    __IO uint32_t LUT[CHIP_QSPI_LUT_SEQ_CNT][CHIP_QSPI_LUT_SEQ_REG_CNT]; /* Lookup Table,   Address offset: 0x200-0x3FC */
    __IO uint32_t HMSTRCR[CHIP_QSPI_AHB_CTRL_CNT]; /* AHB Controller ID x Control,          Address offset: 0x400-0x41C */
    __IO uint32_t HADDRSTART;  /* HADDR REMAP Start Address,                                Address offset: 0x420 */
    __IO uint32_t HADDREND;    /* HADDR REMAP End Address,                                  Address offset: 0x424 */
    __IO uint32_t HADDROFFSET; /* HADDR Remap Offset,                                       Address offset: 0x428 */
} CHIP_REGS_QSPI_T;

#define CHIP_REGS_QSPI          ((CHIP_REGS_QSPI_T *) CHIP_MEMRGN_ADDR_PERIPH_QSPI)


/*********** Module Control 0 (QSPI_MCR0) *************************************/
#define CHIP_REGFLD_QSPI_MCR0_AHB_GRANT_WAIT        (0x000000FFUL << 24U)   /* (RW)  Timeouts Wait Cycle for AHB command Grant ( × 1024 CLK) (for debug only)*/
#define CHIP_REGFLD_QSPI_MCR0_IPG_GRANT_WAIT        (0x000000FFUL << 16U)   /* (RW)  Timeout Wait Cycle for IP Command Grant ( × 1024 CLK) (for debug only) */
#define CHIP_REGFLD_QSPI_MCR0_LEARN_EN              (0x00000001UL << 15U)   /* (RW)  Data Learning Enable */
#define CHIP_REGFLD_QSPI_MCR0_SCK_FREERUN_EN        (0x00000001UL << 14U)   /* (RW)  SCLK Free-running Enable */
#define CHIP_REGFLD_QSPI_MCR0_COMBINATION_EN        (0x00000001UL << 13U)   /* (RW)  Combination Mode Enable */
#define CHIP_REGFLD_QSPI_MCR0_DOZE_EN               (0x00000001UL << 12U)   /* (RW)  Doze Mode Enable */
#define CHIP_REGFLD_QSPI_MCR0_HS_EN                 (0x00000001UL << 11U)   /* (RW)  Half Speed Serial Flash Memory Access Enable (modify only when MDIS=1) */
#define CHIP_REGFLD_QSPI_MCR0_SERCLK_DIV            (0x00000007UL <<  8U)   /* (RW)  Serial Root Clock Divider (1-8) (modify only when MDIS=1) */
#define CHIP_REGFLD_QSPI_MCR0_ATDF_EN               (0x00000001UL <<  7U)   /* (RW)  AHB Write Access to IP Transmit FIFO Enable */
#define CHIP_REGFLD_QSPI_MCR0_ARDF_EN               (0x00000001UL <<  6U)   /* (RW)  AHB Read Access to IP Receive FIFO Enable */
#define CHIP_REGFLD_QSPI_MCR0_RX_CLK_SRC            (0x00000003UL <<  4U)   /* (RW)  Sample Clock Source for Flash Reading */
#define CHIP_REGFLD_QSPI_MCR0_MDIS                  (0x00000001UL <<  1U)   /* (RW)  Module Disable */
#define CHIP_REGFLD_QSPI_MCR0_SW_RESET              (0x00000001UL <<  0U)   /* (RW1SC) Software Reset */
#define CHIP_REGMSK_QSPI_MCR0_RESERVED              (0x0000000CUL)

#define CHIP_REGFLDVAL_QSPI_MCR0_RX_CLK_SRC_LP_INT  0  /* Dummy Read strobe that FlexSPI generates, looped back internally */
#define CHIP_REGFLDVAL_QSPI_MCR0_RX_CLK_SRC_LP_DQS  1  /* Dummy Read strobe that FlexSPI generates, looped back from DQS pad */
#define CHIP_REGFLDVAL_QSPI_MCR0_RX_CLK_SRC_EXT_DQS 3  /* Flash-memory-provided read strobe and input from DQS pad */

/*********** Module Control 1 (QSPI_MCR1) *************************************/
#define CHIP_REGFLD_QSPI_MCR1_SEQ_WAIT              (0x0000FFFFUL << 16U)   /* (RW)  Command Sequence Wait ( × 1024 CLK) */
#define CHIP_REGFLD_QSPI_MCR1_AHB_BUS_WAIT          (0x0000FFFFUL <<  0U)   /* (RW)  AHB Bus Wait ( × 1024 CLK) */

/*********** Module Control 2 (QSPI_MCR2) *************************************/
#define CHIP_REGFLD_QSPI_MCR2_RESUME_WAIT           (0x000000FFUL << 24U)   /* (RW)  Resume Wait Duration */
#define CHIP_REGFLD_QSPI_MCR2_RX_CLK_SRC_DIFF       (0x00000001UL << 23U)   /* (RW)  Sample Clock Source Different */
#define CHIP_REGFLD_QSPI_MCR2_RX_CLK_SRC_B          (0x00000003UL << 21U)   /* (RW)  Port B Receiver Clock Source */
#define CHIP_REGFLD_QSPI_MCR2_SCKB_DIFF_OPT         (0x00000001UL << 19U)   /* (RW)  SCLK Port B Differential Output (modify only when MDIS=1 + RST) */
#define CHIP_REGFLD_QSPI_MCR2_SAME_DEVICE_EN        (0x00000001UL << 15U)   /* (RW)  Same Device Enable */
#define CHIP_REGFLD_QSPI_MCR2_CLR_LEARN_PHASE       (0x00000001UL << 14U)   /* (W1SC)  Clear Learn Phase Selection */
#define CHIP_REGFLD_QSPI_MCR2_CLR_AHB_BUF_OPT       (0x00000001UL << 11U)   /* (RW)   Atomatically Clear AHB Buffer */
#define CHIP_REGMSK_QSPI_MCR2_RESERVED              (0x001737FFUL)

/*********** AHB Bus Control (QSPI_AHBCR) *************************************/
#define CHIP_REGFLD_QSPI_AHBCR_AFLASH_BASE          (0x0000000FUL << 24U)   /* (RW)  AHB Memory-Mapped Flash Base Address */
#define CHIP_REGFLD_QSPI_AHBCR_HMSTR_ID_REMAP       (0x00000001UL << 18U)   /* (RW)  AHB Controller ID Remapping Enable */
#define CHIP_REGFLD_QSPI_AHBCR_READ_SZ_ALIGN        (0x00000001UL << 10U)   /* (RW)  AHB Read Size Alignment */
#define CHIP_REGFLD_QSPI_AHBCR_READ_ADDR_OPT        (0x00000001UL <<  6U)   /* (RW)  AHB Read Address Option (1 - AHB read burst start address alignment is not limited) */
#define CHIP_REGFLD_QSPI_AHBCR_PREFETCH_EN          (0x00000001UL <<  5U)   /* (RW)  Enables AHB read prefetch */
#define CHIP_REGFLD_QSPI_AHBCR_BUFFERABLE_EN        (0x00000001UL <<  4U)   /* (RW)  Bufferable Write Access Enable */
#define CHIP_REGFLD_QSPI_AHBCR_CACHABLE_EN          (0x00000001UL <<  3U)   /* (RW)  Cacheable Read Access Enable */
#define CHIP_REGFLD_QSPI_AHBCR_CLR_AHB_TXBUF        (0x00000001UL <<  2U)   /* (W1SC) Clear AHB transmit buffer */
#define CHIP_REGFLD_QSPI_AHBCR_APAR_EN              (0x00000001UL <<  0U)   /* (RW)  AHB Parallel Mode Enable */
#define CHIP_REGMSK_QSPI_AHBCR_RESERVED             (0x0FFDFD82UL)

/*********** Interrupt Enable / Interrupt (QSPI_INTEN/INTR) *******************/
#define CHIP_REGFLD_QSPI_INT_SEQ_TIMEOUT            (0x00000001UL << 11U)   /* (RW/W1C)  Sequence Execution Timeout Interrupt */
#define CHIP_REGFLD_QSPI_INT_AHB_BUS_TIMEOUT        (0x00000001UL << 10U)   /* (RW/W1C)  AHB Bus Timeout Interrupt */
#define CHIP_REGFLD_QSPI_INT_SCK_STOP_BY_WR         (0x00000001UL <<  9U)   /* (RW/W1C)  SCLK Stopped By Write Interrupt */
#define CHIP_REGFLD_QSPI_INT_SCK_STOP_BY_RD         (0x00000001UL <<  8U)   /* (RW/W1C)  SCLK Stopped By Read Interrupt */
#define CHIP_REGFLD_QSPI_INT_DATA_LEARN_FAIL        (0x00000001UL <<  7U)   /* (RW/W1C)  Data Learning Failed */
#define CHIP_REGFLD_QSPI_INT_IP_TX_WE               (0x00000001UL <<  6U)   /* (RW/W1C)  IP Transmit FIFO Watermark Empty */
#define CHIP_REGFLD_QSPI_INT_IP_RX_WA               (0x00000001UL <<  5U)   /* (RW/W1C)  IP Receive FIFO Watermark Available */
#define CHIP_REGFLD_QSPI_INT_AHB_CMD_ERR            (0x00000001UL <<  4U)   /* (RW/W1C)  AHB-Triggered Command Sequences Error Detected */
#define CHIP_REGFLD_QSPI_INT_IP_CMD_ERR             (0x00000001UL <<  3U)   /* (RW/W1C)  IP-Triggered Command Sequences Error Detected */
#define CHIP_REGFLD_QSPI_INT_AHB_CMD_GE             (0x00000001UL <<  2U)   /* (RW/W1C)  AHB-Triggered Command Sequences Grant Timeout */
#define CHIP_REGFLD_QSPI_INT_IP_CMD_GE              (0x00000001UL <<  1U)   /* (RW/W1C)  IP-Triggered Command Sequences Grant Timeout */
#define CHIP_REGFLD_QSPI_INT_IP_CMD_DONE            (0x00000001UL <<  0U)   /* (RW/W1C)  IP-Triggered Command Sequences Execution Finished */

/*********** LUT Control (QSPI_LUTCR) *****************************************/
#define CHIP_REGFLD_QSPI_LUTCR_UNLOCK               (0x00000001UL <<  1U)   /* (RW) Unlock LUT */
#define CHIP_REGFLD_QSPI_LUTCR_LOCK                 (0x00000001UL <<  0U)   /* (RW) Lock LUT */

/*********** AHB Receive Buffer x Control (QSPI_AHBRXBUFCR) *******************/
#define CHIP_REGFLD_QSPI_AHBRXBUFCR_PREFETCH_EN     (0x00000001UL << 31U)   /* (RW) AHB Read Prefetch Enable */
#define CHIP_REGFLD_QSPI_AHBRXBUFCR_PRIORITY        (0x00000007UL << 24U)   /* (RW) AHB Controller Read Priority */
#define CHIP_REGFLD_QSPI_AHBRXBUFCR_MSTR_ID         (0x00000007UL << 16U)   /* (RW) AHB Controller ID */
#define CHIP_REGFLD_QSPI_AHBRXBUFCR_BUF_SZ          (0x000001FFUL <<  0U)   /* (RW) AHB Receive Buffer Size */
#define CHIP_REGMSK_QSPI_AHBRXBUFCR_RESERVED        (0x78F0FE00UL)

/*********** Flash Control 0 (QSPI_FLSHCR0) ***********************************/
#define CHIP_REGFLD_QSPI_FLSHCR0_FLSHSZ             (0x007FFFFFUL <<  0U)   /* (RW) Flash Size in KB */
#define CHIP_REGMSK_QSPI_FLSHCR0_RESERVED           (0xFF800000UL)

/*********** Flash Control 1 (QSPI_FLSHCR1) ***********************************/
#define CHIP_REGFLD_QSPI_FLSHCR1_CS_INTERVAL        (0x0000FFFFUL << 16U)   /* (RW) Chip Select Interval */
#define CHIP_REGFLD_QSPI_FLSHCR1_CS_INTERVAL_UNIT   (0x00000001UL << 15U)   /* (RW) Chip Select Interval Unit (0 - 1clk, 1 - 256clk) */
#define CHIP_REGFLD_QSPI_FLSHCR1_CAS                (0x0000000FUL << 11U)   /* (RW) Column Address Size */
#define CHIP_REGFLD_QSPI_FLSHCR1_WA                 (0x00000001UL << 10U)   /* (RW) Word-Addressable */
#define CHIP_REGFLD_QSPI_FLSHCR1_TCSH               (0x0000001FUL <<  5U)   /* (RW) Serial Flash CS Hold Time */
#define CHIP_REGFLD_QSPI_FLSHCR1_TCSS               (0x0000001FUL <<  0U)   /* (RW) Serial Flash CS Setup Time */

/*********** Flash Control 2 (QSPI_FLSHCR2) ***********************************/
#define CHIP_REGFLD_QSPI_FLSHCR2_CLR_INSTR_PTR      (0x00000001UL << 31U)   /* (W1SC) Clear Instruction Pointer */
#define CHIP_REGFLD_QSPI_FLSHCR2_AWR_WAIT_UNIT      (0x00000007UL << 28U)   /* (RW) AWRWAIT Unit */
#define CHIP_REGFLD_QSPI_FLSHCR2_AWR_WAIT           (0x00000FFFUL << 16U)   /* (RW) AHB Write Wait */
#define CHIP_REGFLD_QSPI_FLSHCR2_AWR_SEQ_NUM        (0x00000007UL << 13U)   /* (RW) Sequence Number for AHB Write-Triggered Command */
#define CHIP_REGFLD_QSPI_FLSHCR2_AWR_SEQ_ID         (0x0000001FUL <<  8U)   /* (RW) Sequence Index for AHB Write-Triggered Command */
#define CHIP_REGFLD_QSPI_FLSHCR2_ARD_SEQ_NUM        (0x00000007UL <<  5U)   /* (RW) Sequence Number for AHB Read-Triggered Command */
#define CHIP_REGFLD_QSPI_FLSHCR2_ARD_SEQ_ID         (0x0000001FUL <<  0U)   /* (RW) Sequence Index for AHB Read-Triggered Command */

#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_2     0
#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_8     1
#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_32    2
#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_128   3
#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_512   4
#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_2048  5
#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_8192  6
#define CHIP_REGFLDVAL_QSPI_FLSHCR2_AWR_WAIT_UNIT_32768 7

/*********** Flash Control 4 (QSPI_FLSHCR4) ***********************************/
#define CHIP_REGFLD_QSPI_FLSHCR4_WM_EN_B            (0x00000001UL <<  3U)   /* (RW) Write Mask Enable for Port B (DQS/RWDS pins) */
#define CHIP_REGFLD_QSPI_FLSHCR4_WM_EN_A            (0x00000001UL <<  2U)   /* (RW) Write Mask Enable for Port A (DQS/RWDS pins) */
#define CHIP_REGFLD_QSPI_FLSHCR4_WM_OPT1            (0x00000001UL <<  0U)   /* (RW) Write Mask Option 1 (limit address alignment) */
#define CHIP_REGMSK_QSPI_FLSHCR4_RESERVED           (0xFFFFFFF2UL)

/*********** IP Control 0 (QSPI_IPCR0) ****************************************/
#define CHIP_REGFLD_QSPI_IPCR0_SFAR                 (0xFFFFFFFFUL <<  0U)   /* (RW) Serial Flash Address */

/*********** IP Control 1 (QSPI_IPCR1) ****************************************/
#define CHIP_REGFLD_QSPI_IPCR1_IPAR_EN              (0x00000001UL << 31U)   /* (RW) Parallel Mode Enable for IP Commands */
#define CHIP_REGFLD_QSPI_IPCR1_ISEQ_NUM             (0x00000007UL << 24U)   /* (RW) Sequence Number for IP command (ISEQNUM+1) */
#define CHIP_REGFLD_QSPI_IPCR1_ISEQ_ID              (0x0000001FUL << 16U)   /* (RW) Sequence Index in LUT for IP command */
#define CHIP_REGFLD_QSPI_IPCR1_IDAT_SZ              (0x0000FFFFUL <<  0U)   /* (RW) Flash Read/Program Data Size (in bytes) for IP command. */

/*********** IP Command (QSPI_IPCMD) ******************************************/
#define CHIP_REGFLD_QSPI_IPCMD_TRG                  (0x00000001UL <<  0U)   /* (W1SC) Triggers an IP command */

/*********** IP Receive FIFO Control (QSPI_IPRXFCR) ***************************/
#define CHIP_REGFLD_QSPI_IPRXFCR_RX_WMRK            (0x0000003FUL <<  2U)   /* (RW) IP Receive FIFO Watermark Level ((RX_WMRK + 1) × 64 bits) */
#define CHIP_REGFLD_QSPI_IPRXFCR_RX_DMA_EN          (0x00000001UL <<  1U)   /* (RW) IP Receive FIFO Reading by DMA Enable */
#define CHIP_REGFLD_QSPI_IPRXFCR_CLR_IP_RXF         (0x00000001UL <<  0U)   /* (W1SC) Clear IP Receive FIFO */
#define CHIP_REGMSK_QSPI_IPRXFCR_RESERVED           (0xFFFFFF00UL)

/*********** IP Transmit FIFO Control (QSPI_IPTXFCR) **************************/
#define CHIP_REGFLD_QSPI_IPTXFCR_TX_WMRK            (0x0000007FUL <<  2U)   /* (RW) Transmit Watermark Level ((TX_WMRK + 1) × 64 bits) */
#define CHIP_REGFLD_QSPI_IPTXFCR_TX_DMA_EN          (0x00000001UL <<  1U)   /* (RW) Transmit FIFO DMA Enable */
#define CHIP_REGFLD_QSPI_IPTXFCR_CLR_IP_TXF         (0x00000001UL <<  0U)   /* (W1SC) Clear IP Transmit FIFO */
#define CHIP_REGMSK_QSPI_IPTXFCR_RESERVED           (0xFFFFFE00UL)

/*********** DLL Control 0 A/B (QSPI_DLLCR) ***********************************/
#define CHIP_REGFLD_QSPI_DLLCR_REF_PHASE_GAP        (0x00000003UL << 15U)   /* (RW) Reference Clock Delay Line Phase Adjust Gap (Recommended - 2) */
#define CHIP_REGFLD_QSPI_DLLCR_OVRD_VAL             (0x0000003FUL <<  9U)   /* (RW) Target Clock Delay Line Override Value */
#define CHIP_REGFLD_QSPI_DLLCR_OVRD_EN              (0x00000001UL <<  8U)   /* (RW) Target Clock Delay Line Override Value Enable */
#define CHIP_REGFLD_QSPI_DLLCR_SLV_DLY_TARGET       (0x0000000FUL <<  3U)   /* (RW) Target Delay Line: (x + 1) * 1/32 * refclk */
#define CHIP_REGFLD_QSPI_DLLCR_DLL_RESET            (0x00000001UL <<  1U)   /* (RW) Forces a DLL reset (clear by software) */
#define CHIP_REGFLD_QSPI_DLLCR_DLL_EN               (0x00000001UL <<  0U)   /* (RW) DLL Calibration Enable */
#define CHIP_REGMSK_QSPI_DLLCR_RESERVED             (0xFFFE0084UL)

/*********** Status 0 (QSPI_STS0) *********************************************/
#define CHIP_REGFLD_QSPI_STS0_DATA_LEARN_PHASEB     (0x0000000FUL <<  8U)   /* (RO) Data Learning Phase Selection on Port B */
#define CHIP_REGFLD_QSPI_STS0_DATA_LEARN_PHASEA     (0x0000000FUL <<  4U)   /* (RO) Data Learning Phase Selection on Port A */
#define CHIP_REGFLD_QSPI_STS0_ARB_CMD_SRC           (0x00000003UL <<  2U)   /* (RO) ARB Command Source */
#define CHIP_REGFLD_QSPI_STS0_ARB_IDLE              (0x00000001UL <<  1U)   /* (RO) ARB_CTL State Machine Idle */
#define CHIP_REGFLD_QSPI_STS0_SEQ_IDLE              (0x00000001UL <<  0U)   /* (RO) SEQ_CTL State Machine Idle */

#define CHIP_REGFLDVAL_QSPI_STS0_ARB_CMD_SRC_AHB_RD 0   /* (RO) AHB read command */
#define CHIP_REGFLDVAL_QSPI_STS0_ARB_CMD_SRC_AHB_WR 1   /* (RO) AHB write command */
#define CHIP_REGFLDVAL_QSPI_STS0_ARB_CMD_SRC_IP_CMD 2   /* (RO) IP command (by writing 1 to IPCMD[TRG]) */
#define CHIP_REGFLDVAL_QSPI_STS0_ARB_CMD_SRC_SUSP   3   /* (RO) A suspended command that has resumed */

/*********** Status 1 (QSPI_STS1) *********************************************/
#define CHIP_REGFLD_QSPI_STS1_IP_CMD_ERR_CODE       (0x0000000FUL << 24U)   /* (RO) IP Command Error Code */
#define CHIP_REGFLD_QSPI_STS1_IP_CMD_ERR_ID         (0x0000001FUL << 16U)   /* (RO) IP Command Error ID */
#define CHIP_REGFLD_QSPI_STS1_AHB_CMD_ERR_CODE      (0x0000000FUL <<  8U)   /* (RO) AHB Command Error Code */
#define CHIP_REGFLD_QSPI_STS1_AHB_CMD_ERR_ID        (0x0000001FUL <<  0U)   /* (RO) AHB Command Error ID */

/*********** Status 1 (QSPI_STS2) *********************************************/
#define CHIP_REGFLD_QSPI_STS2_BREF_SEL              (0x0000003FUL << 24U)   /* (RO) Flash B Sample Clock Reference Delay Line Delay Cell Number */
#define CHIP_REGFLD_QSPI_STS2_BSLV_SEL              (0x0000003FUL << 18U)   /* (RO) Flash B Sample Clock Target Delay Line Delay Cell Number */
#define CHIP_REGFLD_QSPI_STS2_BREF_LOCK             (0x00000001UL << 17U)   /* (RO) Flash B Sample Clock Reference Delay Line Locked */
#define CHIP_REGFLD_QSPI_STS2_BSLV_LOCK             (0x00000001UL << 16U)   /* (RO) Flash B Sample Target Reference Delay Line Locked */
#define CHIP_REGFLD_QSPI_STS2_AREF_SEL              (0x0000003FUL <<  8U)   /* (RO) Flash A Sample Clock Reference Delay Line Delay Cell Number */
#define CHIP_REGFLD_QSPI_STS2_ASLV_SEL              (0x0000003FUL <<  2U)   /* (RO) Flash A Sample Clock Target Delay Line Delay Cell Number */
#define CHIP_REGFLD_QSPI_STS2_AREF_LOCK             (0x00000001UL <<  1U)   /* (RO) Flash A Sample Clock Reference Delay Line Locked */
#define CHIP_REGFLD_QSPI_STS2_ASLV_LOCK             (0x00000001UL <<  0U)   /* (RO) Flash A Sample Target Reference Delay Line Locked */

/*********** AHB Suspend Status (QSPI_AHBSPNDSTS) *****************************/
#define CHIP_REGFLD_QSPI_AHBSPNDSTS_DAT_LFT         (0x0000FFFFUL << 16U)   /* (RO) Data Left */
#define CHIP_REGFLD_QSPI_AHBSPNDSTS_BUF_ID          (0x00000007UL <<  1U)   /* (RO) AHB Receive Buffer ID for Suspended Command Sequence */
#define CHIP_REGFLD_QSPI_AHBSPNDSTS_ACTIVE          (0x00000001UL <<  0U)   /* (RO) Active AHB Read Prefetch Suspended */

/*********** IP Receive FIFO Status (QSPI_IPRXFSTS) ***************************/
#define CHIP_REGFLD_QSPI_IPRXFSTS_RD_CNTR           (0x0000FFFFUL << 16U)   /* (RO) Read Data Counter (x64 bits) */
#define CHIP_REGFLD_QSPI_IPRXFSTS_FILL              (0x000000FFUL <<  0U)   /* (RO) Fill Level of IP Receive FIFO (x64 bits) */

/*********** IP Transmit FIFO Status (QSPI_IPTXFSTS) ***************************/
#define CHIP_REGFLD_QSPI_IPTXFSTS_WR_CNTR           (0x0000FFFFUL << 16U)   /* (RO) Write Data Counter (x64 bits) */
#define CHIP_REGFLD_QSPI_IPTXFSTS_FILL              (0x000000FFUL <<  0U)   /* (RO) Fill Level of IP Transmit FIFO (x64 bits) */

/*********** Lookup Table (QSPI_LUT) ******************************************/
#define CHIP_REGFLD_QSPI_LUT_OPCODE1                (0x0000003FUL << 26U)   /* (RW) OPCODE1 */
#define CHIP_REGFLD_QSPI_LUT_NUM_PADS1              (0x00000003UL << 24U)   /* (RW) NUM_PADS1 */
#define CHIP_REGFLD_QSPI_LUT_OPERAND1               (0x000000FFUL << 16U)   /* (RW) OPERAND1 */
#define CHIP_REGFLD_QSPI_LUT_OPCODE0                (0x0000003FUL << 10U)   /* (RW) OPCODE0 */
#define CHIP_REGFLD_QSPI_LUT_NUM_PADS0              (0x00000003UL <<  8U)   /* (RW) NUM_PADS0 */
#define CHIP_REGFLD_QSPI_LUT_OPERAND0               (0x000000FFUL <<  0U)   /* (RW) OPERAND0 */

/*********** AHB Controller ID n Control (QSPI_HMSTRCR) ***********************/
#define CHIP_REGFLD_QSPI_HMSTRCR_MSTR_ID            (0x0000FFFFUL << 16U)   /* (RW) Controller ID */
#define CHIP_REGFLD_QSPI_HMSTRCR_MASK               (0x0000FFFFUL <<  0U)   /* (RW) Mask bits for AHB Controller ID. */

/*********** HADDR REMAP Start Address (QSPI_HADDRSTART) **********************/
#define CHIP_REGFLD_QSPI_HADDRSTART_ADDR_START      (0x000FFFFFUL << 12U)   /* (RW) HADDR Start Address */
#define CHIP_REGFLD_QSPI_HADDRSTART_REMAP_EN        (0x00000001UL <<  0U)   /* (RW) AHB Bus Address Remap Enable */

/*********** HADDR REMAP End Address (QSPI_HADDREND) **************************/
#define CHIP_REGFLD_QSPI_HADDREND_ADDR_END          (0x000FFFFFUL << 12U)   /* (RW)  End Address of HADDR Remap Range */

/*********** HADDR Remap Offset (QSPI_HADDROFFSET) ****************************/
#define CHIP_REGFLD_QSPI_HADDROFFSET_ADDR_OFFSET    (0x000FFFFFUL << 12U)   /* (RW) HADDR Offset */


#define CHIP_QSPI_LUT_WRD(opcode, pads, operand) \
    (LBITFIELD_SET(CHIP_REGFLD_QSPI_LUT_OPCODE0, opcode) | \
    LBITFIELD_SET(CHIP_REGFLD_QSPI_LUT_NUM_PADS0, pads) | \
    LBITFIELD_SET(CHIP_REGFLD_QSPI_LUT_OPERAND0, operand))


#define CHIP_QSPI_LUT_OPCODE_JMP_ON_CS          0x00001F /* AHB XIP only: stop execution, deassert CS and save operand[7:0] as instruction start pointer for next sequence */
#define CHIP_QSPI_LUT_OPCODE_STOP               0x000000 /* Stop execution anddeassert CS */

#define CHIP_QSPI_LUT_OPCODE_CMD_SDR            0x000001 /* Transmit command code to flash from operand[7:0] */
#define CHIP_QSPI_LUT_OPCODE_RADDR_SDR          0x000002 /* Transmit row address to flash (bitlen from operand[7:0]) */
#define CHIP_QSPI_LUT_OPCODE_CADDR_SDR          0x000003 /* Transmit column address to flash (bitlen from operand[7:0]) */
#define CHIP_QSPI_LUT_OPCODE_MODE1_SDR          0x000004 /* Transmit mode bit to flash (operand[0]) */
#define CHIP_QSPI_LUT_OPCODE_MODE2_SDR          0x000005 /* Transmit 2 mode bits to flash (operand[1:0]) */
#define CHIP_QSPI_LUT_OPCODE_MODE4_SDR          0x000006 /* Transmit 4 mode bits to flash (operand[3:0]) */
#define CHIP_QSPI_LUT_OPCODE_MODE8_SDR          0x000007 /* Transmit 8 mode bits to flash (operand[7:0]) */
#define CHIP_QSPI_LUT_OPCODE_WRITE_SDR          0x000008 /* Transmit programdata to flash device (len = AHB burst size/type or IPCR1.IDAT_SZ) */
#define CHIP_QSPI_LUT_OPCODE_READ_SDR           0x000009 /* Receive read data from flash device (len = AHB burst size/type or IPCR1.IDAT_SZ) */
#define CHIP_QSPI_LUT_OPCODE_DATSZ_SDR          0x00000B /* Transmit read or program data size (bitlen from operand[7:0]) */
#define CHIP_QSPI_LUT_OPCODE_DUMMY_SDR          0x00000C /* Leave data lines undriven (cycles count from operand[7:0]) */
#define CHIP_QSPI_LUT_OPCODE_DUMMY_RWDS_SDR     0x00000D /* Dummy for HyperBus (RD - DQS=1: operand[7:0] × 4 - 1, DQS=0: operand[7:0] × 2 - 1; WR - DQS=1: operand[7:0] × 4 - 2, DQS=0: operand[7:0] × 2 - 2) */
#define CHIP_QSPI_LUT_OPCODE_LEARN_SDR          0x00000A /* Receive read data or preamble bit from flash device (bitlen from operand[7:0]) */

#define CHIP_QSPI_LUT_OPCODE_CMD_DDR            0x000021
#define CHIP_QSPI_LUT_OPCODE_RADDR_DDR          0x000022
#define CHIP_QSPI_LUT_OPCODE_CADDR_DDR          0x000023
#define CHIP_QSPI_LUT_OPCODE_MODE1_DDR          0x000024
#define CHIP_QSPI_LUT_OPCODE_MODE2_DDR          0x000025
#define CHIP_QSPI_LUT_OPCODE_MODE4_DDR          0x000026
#define CHIP_QSPI_LUT_OPCODE_MODE8_DDR          0x000027
#define CHIP_QSPI_LUT_OPCODE_WRITE_DDR          0x000028
#define CHIP_QSPI_LUT_OPCODE_READ_DDR           0x000029
#define CHIP_QSPI_LUT_OPCODE_DATSZ_DDR          0x00002B
#define CHIP_QSPI_LUT_OPCODE_DUMMY_DDR          0x00002C
#define CHIP_QSPI_LUT_OPCODE_DUMMY_RWDS_DDR     0x00002D
#define CHIP_QSPI_LUT_OPCODE_LEARN_DDR          0x00002A

#define CHIP_QSPI_LUT_NUM_PADS_1                0
#define CHIP_QSPI_LUT_NUM_PADS_2                1
#define CHIP_QSPI_LUT_NUM_PADS_4                2
#define CHIP_QSPI_LUT_NUM_PADS_8                3

#endif // REGS_QSPI_H


